Compliance Quest - Supplier Quality Exchange Code Repository
=============================================================

Schema Dumper: 
-------------
Use it to update any changes to schema/object model in Salesforce. Then upload the newly generated schema to Salesforce as static resource 'CAPASchema'

Approval Process:
---------------

Use it to deploy SF approval process default one to any new orgs.