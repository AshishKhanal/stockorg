<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SQX_Set_Is_Locked_To_True</fullName>
        <description>Sets the is locked field to true, ensuring that no changes can be made to it.</description>
        <field>compliancequest__Is_Locked__c</field>
        <literalValue>1</literalValue>
        <name>CQ Set is locked to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Set_status_to_active</fullName>
        <description>Sets the status to active for an audit program</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Active</literalValue>
        <name>CQ Set status to active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Set_Approval_Status_to_Approved</fullName>
        <description>Set the audit programs approval status to Approved.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Set Approval Status to  Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Set_Approval_Status_to_Blank</fullName>
        <description>Set the approval status to blank on recalling.</description>
        <field>compliancequest__Approval_Status__c</field>
        <name>CQ Set Approval Status to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Set_Approval_Status_to_Plan_Approval</fullName>
        <description>Set the audit programs approval status to plan approval upon submission.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Plan Approval</literalValue>
        <name>CQ Set Approval Status to Plan Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Set_Approval_Status_to_Rejected</fullName>
        <description>sets the approval status of the audit program to &apos;Rejected&apos; upon rejection</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Set Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
