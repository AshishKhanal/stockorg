<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_approval_email_to_Response_Owner</fullName>
        <description>CQ Send an approval email to Response Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Send_email_when_response_is_approved</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_response_creator</fullName>
        <description>CQ Send email to response creator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Send_email_when_response_is_rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_approval_status_to_approved</fullName>
        <description>Set approval status to approved</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Set approval status to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_none</fullName>
        <description>Set approval status to blank</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>CQ Set approval status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_pending</fullName>
        <description>Set approval status to pending</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>CQ Set approval status to pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_rejected</fullName>
        <description>Set approval status to rejected</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Set approval status to rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_draft</fullName>
        <description>Set status to draft</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Completed</literalValue>
        <name>CQ Set status to Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_in_approval</fullName>
        <description>Set status to in approval</description>
        <field>compliancequest__Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>CQ Set status to in approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_published</fullName>
        <description>Set status to published</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Completed</literalValue>
        <name>CQ Set status to completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
