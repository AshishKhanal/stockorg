<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_Controlled_Document_Owner_Regarding_Approval_Submission</fullName>
        <description>CQ Email to Controlled Document Owner Regarding Approval Submission</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/Approval_Submission_Email_To_Owner</template>
    </alerts>    
    <alerts>
        <fullName>Send_email_to_document_owner_when_the_document_is_rejected_by_approver</fullName>
        <description>CQ Send email to document owner when the document is rejected by the approver.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/Send_email_when_docapprover_Reject_doc</template>
    </alerts>
    <alerts>
        <fullName>CQ_Send_email_to_owner_when_approver_rejects_document_obsolescence</fullName>
        <description>CQ Send email to owner when approver rejects document obsolescence.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/CQ_Send_Eml_On_Aprover_Reject_DocRetire</template>
    </alerts>
    <fieldUpdates>
        <fullName>Increase_Current_Approval_Step_By_1</fullName>
        <description>Increase Current Approval Step by 1 to advance to next step after each step is approved in approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>compliancequest__Current_Approval_Step__c + 1</formula>
        <name>CQ Increase Current Approval Step By 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Doc_In_Approval</fullName>
        <description>Set document status to in approval</description>
        <field>compliancequest__Document_Status__c</field>
        <name>CQ Mark Doc In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Document_Approved</fullName>
        <description>Set document status as approved</description>
        <field>compliancequest__Document_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Mark Document Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Current_Approval_Step</fullName>
        <description>Set Current Approval Step to 0 when recalled or rejected using approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>0</formula>
        <name>CQ Reset Current Approval Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Is_Locked</fullName>
        <description>This field is used to set the is locked field value to false</description>
        <field>compliancequest__Is_Locked__c</field>
        <literalValue>0</literalValue>
        <name>CQ Reset Is Locked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Revert_To_Draft</fullName>
        <description>Set document status back to draft</description>
        <field>compliancequest__Document_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>CQ Revert To Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_None</fullName>
        <description>This field update is used to set the approval status to none.</description>
        <field>compliancequest__Approval_Status__c</field>
        <name>CQ Set Approval Status To None</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Apvl_Status_To_Obslesence_Aprval</fullName>
        <description>Set Approval Status To Obsolescence Approval.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Obsolescence Approval</literalValue>
        <name>CQ Set Apvl Status To Obslesence Aprval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Apvl_Status_To_Obslesence_Rejctd</fullName>
        <description>Update approval status to Obslescence Rejected.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Obsolescence Rejected</literalValue>
        <name>CQ Set Apvl Status To Obslesence Rejctd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Apvl_Status_To_Obslesence_Aprovd</fullName>
        <description>update approval status field to &apos;Obsolescence Approved&apos;</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Obsolescence Approved</literalValue>
        <name>CQ Set Apvl Status to Obslesence Aprovd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Current_Approval_Step_To_1</fullName>
        <description>Set Current Approval Step to 1 as initial step when submitted for approval using approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>1</formula>
        <name>CQ Set Current Approval Step To 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Document_Status_To_PreExpire</fullName>
        <description>This rule will set the document status to pre-expire</description>
        <field>compliancequest__Document_Status__c</field>
        <literalValue>Pre-Expire</literalValue>
        <name>CQ Set Document Status To PreExpire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Expiration_Date</fullName>
        <description>set the expiration if already in document or use today&apos;s date.</description>
        <field>compliancequest__Expiration_Date__c</field>
        <formula>IF(ISBLANK( compliancequest__Expiration_Date__c ), TODAY() , compliancequest__Expiration_Date__c )</formula>
        <name>CQ Set Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Expiration_Date_To_Null</fullName>
        <description>set the expiration date to null as the approver reject it.</description>
        <field>compliancequest__Expiration_Date__c</field>
        <name>CQ Set Expiration Date To Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_In_Change_Approval_Status</fullName>
        <description>Sets Approval Status value to &quot;In Change Approval&quot;</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>In Change Approval</literalValue>
        <name>CQ Set In Change Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Approved</fullName>
        <description>This field update will set the approval status to approved.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Set Status To Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Is_Locked_To_True</fullName>
        <description>This field will set is locked field to true.</description>
        <field>compliancequest__Is_Locked__c</field>
        <literalValue>1</literalValue>
        <name>CQ Set Is Locked To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Rejected</fullName>
        <description>This rule will set the approval status of doc to rejected.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Set Status To Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Release_Approval</fullName>
        <description>This rule will set the approval status of the controlled doc to release approval.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Release Approval</literalValue>
        <name>CQ Set Status To Release Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Date</fullName>
        <description>Set Approved Date to today</description>
        <field>compliancequest__Date_Approved__c</field>
        <formula>Today()</formula>
        <name>CQ Update Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
