<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Audit_Criteria_Status_to_Active</fullName>
        <description>This rule will set the approval status of audit criteria to active.</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Active</literalValue>
        <name>CQ Change Audit Criteria Status Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Audit_Criteria_Status_to_Approval</fullName>
        <description>This rule changes the audit criteria record submitted for approval to in approval status.</description>
        <field>compliancequest__Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>CQ Change Audit Criteria Status Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Audit_Criteria_Status_to_Draft</fullName>
        <description>This will change status to draft</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>CQ Change Audit Criteria Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Audit_Criteria_Status_to_Rejected</fullName>
        <description>This rule will set the status of audit criteria record to rejected.</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Change Audit Criteria Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Is_Locked_To_True</fullName>
        <description>This field sets the is locked field to true</description>
        <field>compliancequest__Is_Locked__c</field>
        <literalValue>1</literalValue>
        <name>CQ Set Is Locked To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
