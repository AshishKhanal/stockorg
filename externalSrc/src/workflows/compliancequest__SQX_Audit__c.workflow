<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CQ_Set_Stage_to_Plan_Approval</fullName>
        <description>Set Stage to Plan Approval</description>
        <field>compliancequest__Stage__c</field>
        <literalValue>Plan Approval</literalValue>
        <name>CQ Set Stage to Plan Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Stage_to_Scheduled</fullName>
        <description>Set Stage to Scheduled</description>
        <field>compliancequest__Stage__c</field>
        <literalValue>Scheduled</literalValue>
        <name>CQ Set Stage to Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Stage_to_plan</fullName>
        <description>Set Stage to plan</description>
        <field>compliancequest__Stage__c</field>
        <literalValue>Plan</literalValue>
        <name>CQ Set Stage to plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_status_to_Open</fullName>
        <description>Set status to Open</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Open</literalValue>
        <name>CQ Set status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
