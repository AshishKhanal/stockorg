<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CQ_Email_notification_to_queue_member_when_Interaction_record_is_submitted</fullName>
        <description>CQ Email notification to queue member when Supplier Interaction record is submitted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Queue_Members_When_Supplier_Interaction_record_is_submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>CQ_Change_Sup_Interaction_Owner_to_Queue</fullName>
        <description>Change Supplier Interaction record owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>CQ_Supplier_Interaction_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CQ Change Sup Interaction Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CQ_Supplier_Interaction_Set_Assignee_to_Queue_and_Send_Email</fullName>
        <actions>
            <name>CQ_Email_notification_to_queue_member_when_Interaction_record_is_submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CQ_Change_Sup_Interaction_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>compliancequest__SQX_Supplier_Interaction__c.compliancequest__Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>compliancequest__SQX_Supplier_Interaction__c.compliancequest__Record_Stage__c</field>
            <operation>equals</operation>
            <value>Triage</value>
        </criteriaItems>
        <criteriaItems>
            <field>compliancequest__SQX_Supplier_Interaction__c.compliancequest__Prevent_Name_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule change assign current owner to queue and send email notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
