<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CQ_Email_Notification_to_Queue_Member_When_Escalation_Record_is_Submitted</fullName>
        <description>CQ Email notification to queue member when Supplier Escalation record is submitted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Queue_Members_When_Supplier_Escalation_record_is_submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>CQ_Change_Supp_Escalation_Owner_to_Queue</fullName>
        <description>CQ Change Supplier Escalation record owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>CQ_Supplier_Escalation_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>CQ Change Supp Escalation Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CQ_Supplier_Escalation_Set_Assignee_to_Queue_and_Send_Email</fullName>
        <actions>
            <name>CQ_Email_Notification_to_Queue_Member_When_Escalation_Record_is_Submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CQ_Change_Supp_Escalation_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>compliancequest__SQX_Supplier_Escalation__c.compliancequest__Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>compliancequest__SQX_Supplier_Escalation__c.compliancequest__Record_Stage__c</field>
            <operation>equals</operation>
            <value>Triage</value>
        </criteriaItems>
        <description>This rule change assign current owner to queue and send email notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>