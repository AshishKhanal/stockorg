<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SQX_Approval_Status_to_Approved</fullName>
        <description>Set Approval Status to Approved</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Approval_Status_to_Pending</fullName>
        <description>Set Approval Status to &apos; - &apos;</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>-</literalValue>
        <name>CQ Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Approval_Status_to_Rejected</fullName>
        <description>Set Approval Status to Rejected</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Status_to_Complete</fullName>
        <description>Set Status to Complete</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Complete</literalValue>
        <name>CQ Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Status_to_InApproval</fullName>
        <description>Set status to In Approval</description>
        <field>compliancequest__Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>CQ Status to In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Draft</fullName>
        <description>Set status to Draft</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Draft</literalValue>
        <name>CQ Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
