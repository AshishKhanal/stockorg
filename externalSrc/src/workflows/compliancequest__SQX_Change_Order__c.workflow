<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Change_Order_Implementation_Email</fullName>
        <description>CQ Approved Change Order Implementation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/Change_Order_Approved_Implementation_Email</template>
    </alerts>
    <alerts>
        <fullName>Rejected_Change_Order_Implementation_Email</fullName>
        <description>CQ Rejected Change Order Implementation Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/Change_Order_Rejected_Implementation_Email</template>
    </alerts>
    <alerts>
        <fullName>SQX_Approved_Change_Order_Plan_Email</fullName>
        <description>CQ Approved Change Order Plan Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Change_Order_Approved_Plan_Email</template>
    </alerts>
    <alerts>
        <fullName>SQX_Rejected_Change_Order_Plan_Email</fullName>
        <description>CQ Rejected Change Order Plan Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Change_Order_Rejected_Plan_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Increase_Current_Approval_Step_By_1</fullName>
        <description>Increase Current Approval Step by 1 to advance to next step after each step is approved in approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>compliancequest__Current_Approval_Step__c + 1</formula>
        <name>CQ Increase Current Approval Step By 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_In_Plan_Approval</fullName>
        <description>Set Approval Status to In Approval when submitted for approval.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Plan Approval</literalValue>
        <name>CQ Set Approval Status In Plan Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_Plan_Approved</fullName>
        <description>Set Approval Status to Plan Approved when approved at the final approval step.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Plan Approved</literalValue>
        <name>CQ Set Approval Status To Plan Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_Plan_Rejected</fullName>
        <description>Set Approval Status to Plan Rejected when rejected at the final approval step.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Plan Rejected</literalValue>
        <name>CQ Set Approval Status To Plan Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_Recalled</fullName>
        <description>Set Approval Status to Recalled when recalled at any approval step.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>CQ Set Approval Status To Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Current_Approval_Step_To_0</fullName>
        <description>Set Current Approval Step to 0 when recalled or rejected using approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>0</formula>
        <name>CQ Set Current Approval Step To 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Current_Approval_Step_To_1</fullName>
        <description>Set Current Approval Step to 1 as initial step when submitted for approval using approval matrix process.</description>
        <field>compliancequest__Current_Approval_Step__c</field>
        <formula>1</formula>
        <name>CQ Set Current Approval Step To 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Implementation_Approved_Status</fullName>
        <description>Sets Approval Status to &quot;Implementation Approved&quot;.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Implementation Approved</literalValue>
        <name>CQ Set Implementation Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_In_Implementation_Approval_Status</fullName>
        <description>Sets Approval Status to &quot;Implementation Approval&quot;.</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Implementation Approval</literalValue>
        <name>CQ Set In Implementation Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Approval_Status</fullName>
        <description>Copy Approval Status to Last Approval Status as initial step when submitted for approval using approval matrix process.</description>
        <field>compliancequest__Last_Approval_Status__c</field>
        <formula>TEXT(PRIORVALUE(compliancequest__Approval_Status__c))</formula>
        <name>CQ Set Last Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Status_To_Open</fullName>
        <description>Set Status to Open when approved at the final approval step.</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Open</literalValue>
        <name>CQ Set Status To Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CQ_Set_Stage_To_Complete</fullName>
        <description>Set stage to complete when approved at the final approval step.</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Complete</literalValue>
        <name>CQ Set Stage To Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Implementation</fullName>
        <description>Set stage to Implementation when approved plan at final approval step</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Implementation</literalValue>
        <name>CQ Set Stage To Implementation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_To_Impl</fullName>
        <description>Set Stage to complete when reject implementation at the final approval step.</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Implementation</literalValue>
        <name>CQ Set Stage To Impl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_stage_to_plan</fullName>
        <description>Set stage to plan when approval process is rejected</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Plan</literalValue>
        <name>CQ_Set stage to plan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
