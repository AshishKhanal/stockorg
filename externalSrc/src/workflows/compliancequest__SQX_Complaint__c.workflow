<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_sent_to_owner_on_Complaint_Rejection</fullName>
        <description>CQ Email sent to owner on Complaint Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_When_Complaint_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Check_the_approved_checkbox</fullName>
        <description>This rule is used to check the approved checkbox when the record is approved.</description>
        <field>compliancequest__Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>CQ Check the approved checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_The_Close_By</fullName>
        <description>This rule will reset the close by of the complaint.</description>
        <field>compliancequest__Closed_By__c</field>
        <name>CQ Reset The Close By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_The_Close_Date</fullName>
        <description>This rule will reset the close day for complaint</description>
        <field>compliancequest__Close_Date__c</field>
        <name>CQ Reset The Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Review_Date_To_Current_Date</fullName>
        <description>This rule will set the closure review date to today.</description>
        <field>compliancequest__Closure_Review_Date__c</field>
        <formula>Today()</formula>
        <name>CQ Set Review Date To Current Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Closed</fullName>
        <description>Set Stage to Closed</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Closed</literalValue>
        <name>Set Stage to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Closed</fullName>
        <description>This field update will set the status to closed</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Closed</literalValue>
        <name>CQ Set Status To Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Closure_Rejected</fullName>
        <description>This field update will set the status to closure rejected</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Closure Rejected</literalValue>
        <name>CQ Set Status to Closure Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Closure_Review</fullName>
        <description>This field update will set the status of the complaint to closure review</description>
        <field>compliancequest__Record_Stage__c</field>
        <literalValue>Closure Review</literalValue>
        <name>CQ Set Status to Closure Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Recalled</fullName>
        <description>Set is recalled field in complaint</description>
        <field>compliancequest__Is_Recalled__c</field>
        <literalValue>1</literalValue>
        <name>CQ Update Is Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
