<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_is_send_to_queue_member_when_nsi_record_is_submited</fullName>
        <description>Email notification is send to queue member when nsi record is submitted</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Queue_Members_When_NSI_record_is_submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_assign_owner_to_queue</fullName>
        <description>Change owner to queue</description>
        <field>OwnerId</field>
        <lookupValue>CQ_Supplier_Introduction_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change assign owner to queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CQ_Supplier_Introduction_Set_Assignee_To_Queue_And_Send_Email_Notification</fullName>
        <actions>
            <name>Email_notification_is_send_to_queue_member_when_nsi_record_is_submited</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_assign_owner_to_queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>compliancequest__SQX_New_Supplier_Introduction__c.compliancequest__Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>compliancequest__SQX_New_Supplier_Introduction__c.compliancequest__Record_Stage__c</field>
            <operation>equals</operation>
            <value>Triage</value>
        </criteriaItems>
        <description>This rule change assign current owner to queue  and send email notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
