<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Approval_Status_To_Recalled</fullName>
        <description>This update will set the status to recalled</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>CQ Set Approval Status To Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Complete</fullName>
        <description>This update will set the status to complete</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Completed</literalValue>
        <name>CQ Set Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_approved</fullName>
        <description>Set the approval status to approved</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CQ Set approval status to approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_pending</fullName>
        <description>Sets approval status to pending after submission of approval request</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>CQ Set approval status to pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_approval_status_to_rejected</fullName>
        <description>Set approval status to rejected</description>
        <field>compliancequest__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>CQ Set approval status to rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_completed</fullName>
        <description>Set status to completed</description>
        <field>compliancequest__Status__c</field>
        <literalValue>Completed</literalValue>
        <name>CQ Set status to completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_in_approval</fullName>
        <description>Set status to in approval</description>
        <field>compliancequest__Status__c</field>
        <literalValue>In Approval</literalValue>
        <name>CQ Set status to in approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
