![ComplianceQuest - Quality . Compliance . Collaboration . Communication](src/documents/Compliance_Quest/Compliance_Quest_Logo.png)
==

This repository contains the source code for the ComplianceQuest Managed and External Packages, along with build scripts. The code has been organized as follows

* __build__: This folder contains the submodule (another git repo) for building/packaging the source code.

* __externalSrc/__: This folder contains the external metadata/unmanaged metadata that will be sent along with CQ. This normally includes approval processes, queues etc. These kinds of metadata can't be included in main managed package.

    * __src__: This folder contains the metdata for external package.

* __src/__: This folder contains the actual SalesForce metadata for the CQ package

* __scripts/__: Contains scripts/anonymously executable source code for performing various tasks.

* __testRecords/__: These are records/metadata that need to be inserted for test purposes only and are added only in unmanaged environment. It should never be deployed to packaging environment (patch or  main).

* __unzippedResources/__: This folder contains all the resources an unzipped format the resources are minified, combined and cleared by build scripts. (Note: when the source code is completely moved to SFDX the unzipped resources will be within the src directory)

Other related repositories
---

* [Excel Plugin for checking in audit](https://bitbucket.org/ambarkaar/cq-audit-checkin-plugin)

* [SCORM Heroku Server](https://bitbucket.org/ambarkaar/cq-proxy)

* [Build Tools/Scripts Container](https://bitbucket.org/ambarkaar/sqx-ci)

References
---

* [Salesforce Coding Standards & Naming Convention](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/4259842/Salesforce+Coding+Standards+Naming+Convention)

* [Standardization of Field Length](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/3932161/Standardization+of+Field+Length)

* [How to work with Salesforce and GIT](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/983064/How+to+work+with+Salesforce+and+GIT)

* [Development Code Cycle](https://ambarkaar.atlassian.net/wiki/spaces/SQE/blog/2014/07/14/11075586/RFC+Re-engineering+Code+Development+Cycle+at+Ambarkaar)