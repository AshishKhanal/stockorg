# Compliance Quest - Scripts:

## Controlled Document Cleanup Script.txt:

Script to CLEANUP DATABASE for controlled document and personnel records and their related objects.

Deletes controlled documents with document reviews and its primary content, related documents, requirements, personnels with personnel job functions and document trainings, obsolete document trainings, rendition logs, document release information, job functions, change order implementations with document related tasks, equipments with event schedules, history records and related calibrations.

**Note:** Deletes personnel records to avoid trigger validation while deleting document trainings, then adds / imports personnel records related to SF user and their active personnel job functions.

**Reference:** [CD-430](https://ambarkaar.atlassian.net/browse/CD-430)