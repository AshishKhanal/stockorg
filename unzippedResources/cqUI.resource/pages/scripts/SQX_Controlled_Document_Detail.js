/*jshint browser: true */
/*global window document */

window.cq = (function (doc, cq) {
    "use strict";

    var jQuery = window.jQuery,
        PROCESS_INSTANCE_LINK_SELECTOR = "a.actionLink[href*='ProcessInstance']",
        PROCESS_INSTANCE_APPROVE_REJECT_LINK_SELECTOR = '.relatedProcessHistory tr.dataRow>td.actionColumn>a.actionLink[href*="apex/SQX_Controlled_Document_Approval"]',
        COLLABORATION_CMD_BUTTONS_SELECTOR = "input[name='compliancequest__collaborate'],input[name='compliancequest__view_collaboration_group']",
        APPROVER_SECTION_SELECTOR = "div.pbSubheader>img[name=Approvers]",
        COLLABORATION_START_CMD_BUTTON_SELECTOR = "input[name = 'compliancequest__collaborate']",
        COLLABORATION_VIEW_CMD_BUTTON_SELECTOR = "input[name = 'compliancequest__view_collaboration_group']",
        ERROR_DIV_SELECTOR = ".errorDiv",
        CQ_DOMAIN = cq.NS_PREFIX + ".",
        CQ_RELOAD_REQUEST = "reload",
        MESSAGE_EVENT = "message",
        DOC_STATUS_DRAFT = "Draft",
        DOC_STATUS_CURRENT = "Current",
        TRUE_STRING = "true",
        FALSE_STRING = "false",
        EMPTY_STRING = "",
        CQ_APPROVE_REJECT_CLASSES = 'approveResponse cqCommandButton k-button approve textBlackBold',
        SF_PAGE_BLOCK_SUB_SECTION_SELECTOR = "div.pbSubsection",
        SF_APPROVAL_SUBMIT_SELECTOR = '[name="piSubmit"]',
        SF_APPROVAL_RECALL_SELECTOR = '[name="piRemove"]',
        SF_APPROVAL_RECALL_BTN_SELECTOR = ".relatedProcessHistory .pbButton",
        SF_BTN_INLINE_EDIT_SELECTOR = "input[name = 'inlineEditSave']",
        SF_BTN_INLINE_EDIT_CANCEL_SELECTOR = "input[name = 'inlineEditCancel']",
        SF_SECTION_HEADER_CLASS = '.pageType',
        w = window;

    /**
    * function to confirm and recall controlled document from change approval
    * @since 6.1.0
    */
    cq.recallFromChangeApproval = function () {
        if (window.canRecallFromChangeApproval === TRUE_STRING) {
            if ((w.Modal.confirm && w.Modal.confirm(w.msg)) || (!w.Modal.confirm && window.confirm(w.msg))) {
                w.callRecallFromChangeApproval();
            }
        }
    };

    jQuery(doc).ready(function () {

        // If the document status is current then change the label of Submit for Approval button to 'Submit for Obsolescence Approval' [SQX-3639]
        var visibleApprovalButton = document.getElementById(w.visibleApprovalButtonId);
        visibleApprovalButton.value =  w.controlledDocStatus === DOC_STATUS_CURRENT ? w.obsolescenceButtonLabel : visibleApprovalButton.value;

        jQuery(PROCESS_INSTANCE_LINK_SELECTOR).each(function (i, a) {
            var approvalId, approvalLink;

            approvalId = cq.i_getQueryParam('id', jQuery(a).attr('href'));
            approvalLink = w.approvalLink.replace('approvalId', approvalId);

            jQuery(a).attr('href', approvalLink);
        });

        //adds css classes to make the approve/reject link like a button
        jQuery(PROCESS_INSTANCE_APPROVE_REJECT_LINK_SELECTOR).addClass(CQ_APPROVE_REJECT_CLASSES);

        // hide Approvers sections from layout with First,..., Fifth Approver fields
        var divApprovers = jQuery(APPROVER_SECTION_SELECTOR).parent();
        divApprovers.next(SF_PAGE_BLOCK_SUB_SECTION_SELECTOR).hide();
        divApprovers.hide();


        // overriding Submit For Approval to support immediate rendition
        var formSubmitSync = jQuery(document.getElementById(w.formSubmitSyncId)),
            btnPiSubmit = jQuery(SF_APPROVAL_SUBMIT_SELECTOR),
            btnPiRecall = jQuery(SF_APPROVAL_RECALL_SELECTOR),
            submitButton;

        // hide 'Submit for approval' button and override with 'Submit for approval'(sync version) since rendition will be required
        btnPiSubmit.parent().append(formSubmitSync);
        submitButton = formSubmitSync;
        btnPiSubmit.hide();

        // 'Submit for Approval' button needs to be hidden when document approval is pending or synchronization is pending        
        if (btnPiRecall.length > 0 || w.isDocInReleaseApproval === TRUE_STRING || w.isDocInObsolescenceApproval === TRUE_STRING) {
            submitButton.hide();
        } else if (w.isApprovalSubmissionPending === TRUE_STRING) {
            //  create a disabled button
            btnPiSubmit.parent().append(w.submitForApproval);
            submitButton.hide();
        }

        if (w.isDocInChangeApproval === TRUE_STRING) {

            // hide submit for approval button when in change approval
            submitButton.hide();

            if (w.canRecallFromChangeApproval === TRUE_STRING && btnPiRecall.length === 0) {
                // show recall from change approval button only when no approval process is active
                jQuery(SF_APPROVAL_RECALL_BTN_SELECTOR).append(w.recallFromChangeApproval);
            }
        }

        cq.conditionallyShowButtons(w.isDocInChangeApproval, w.isDocInReleaseApproval, submitButton);


        function subscribeToCancelEvent() {
            cq.conditionallyShowButtons(w.isDocInChangeApproval, w.isDocInReleaseApproval, submitButton); 
        }

        function subscribeToEditEvent() {
            /*
            * every 100 mill sec it check whether 'Save' button is visible or not, through this scenario collaborate/view collaboration buttons shows
            */
            var interval = window.setInterval(function () {
                if (jQuery(SF_BTN_INLINE_EDIT_SELECTOR).is(":visible") === false) {
                    if (jQuery(SF_BTN_INLINE_EDIT_SELECTOR).hasClass("btn")) {
                        cq.conditionallyShowButtons(w.isDocInChangeApproval, w.isDocInReleaseApproval, submitButton);
                        w.clearInterval(cq.i_interval);
                        jQuery(SF_BTN_INLINE_EDIT_SELECTOR).click(subscribeToEditEvent);
                        jQuery(SF_BTN_INLINE_EDIT_CANCEL_SELECTOR).click(subscribeToCancelEvent);
                    }
                }
            }, 100);

            cq.i_interval = interval;
        }



        jQuery(SF_BTN_INLINE_EDIT_CANCEL_SELECTOR).click(subscribeToCancelEvent);
        jQuery(SF_BTN_INLINE_EDIT_SELECTOR).click(subscribeToEditEvent);

        $(SF_SECTION_HEADER_CLASS).append("&nbsp;(" + document.getElementById(w.recordTypeId).textContent + ")");

        cq.hideLoading();
    });

    /**
    * hide collaborate and view colaboration group buttons
    * @since 7.0
    */
    cq.hideButtons = function () {
        jQuery(COLLABORATION_CMD_BUTTONS_SELECTOR).hide();
    }

    /**
    * collaborate/view collaboration buttons conditionaly show on page
    * @param {boolean} isDocInChangeApproval doc approval status is in change approval
    * @param {boolean} isDocInReleaseApproval doc approval status is in release approval
    * @param submitButton is innerHtml object of Submit to Approval button
    * @since 7.0
    */
    cq.conditionallyShowButtons = function (isDocInChangeApproval, isDocInReleaseApproval, submitButton) {

        cq.hideButtons();

        if (w.hasEditAccess === TRUE_STRING && w.controlledDocStatus === DOC_STATUS_DRAFT && w.isDocInChangeApproval === FALSE_STRING
                && isDocInReleaseApproval === FALSE_STRING) {
            if (w.controlledDocCheckout === FALSE_STRING && w.collaborationGrpId === EMPTY_STRING) {
                jQuery(COLLABORATION_START_CMD_BUTTON_SELECTOR).show();
            } else {
                if (w.collaborationGrpId !== EMPTY_STRING){
                    jQuery(COLLABORATION_VIEW_CMD_BUTTON_SELECTOR).show();
                }
                submitButton.hide();
            }
        }
    };

    /**
    * Callback for handling start of synchronization of content
    * @param message {string} Message to be displayed when starting synchronization
    * @since 7.0
    */
    cq.syncStart = function (message) {
        cq.showLoading(message);
    };

    /**
    * This is a callback for handling completion of synchronization phase
    * @since 7.0
    */
    cq.syncComplete = function () {
        var msg = jQuery(ERROR_DIV_SELECTOR)[0].innerText.trim();
        if (msg.length > 0){
            // implies that there are some page messages
            // displaying the message
            w.alert(msg);
            cq.hideLoading();
        } else {
            jQuery(document.getElementById(w.approvalButtonId)).click();
        }
    };

    /**
    * Callback for handling completion of submission for approval. Shows error message alert on error
    * @param message {string} Message to be displayed when submission for approval is complete
    * @since 7.0
    */
    cq.approvalSubmissionComplete = function (message) {
        var msg = jQuery(ERROR_DIV_SELECTOR)[0].innerText.trim();
        if (msg.length > 0) {
            // implies that there are some page messages
            // displaying the message
            w.alert(msg);
            cq.hideLoading();
        } else {
            cq.showLoading(message);
            window.location.reload();
        }
    };

    /**
    * Callback for handling start of submission for approval
    * @param message {string} Message to be displayed when starting submission for approval
    * @since 7.0
    */
    cq.approvalSubmissionStart = function (message) {
        cq.showLoading(message);
    };
    
    /**
    * reload page when controlled document checkin is completed because the inner iframe requested
    * a reload
    */
    function reloadWhenCheckin(evt) {
        var origin = (window.location.protocol + "//" + window.location.hostname) +
                (window.location.port ? ':' + window.location.port : '');

        //While not strictly enforced, the first thing a handler should do to lessen potential security
        //issues is verify the domain of the message sender with the evt.origin propert
        if (evt.data === CQ_RELOAD_REQUEST && (evt.origin === origin ||
                evt.origin === origin.replace(CQ_DOMAIN, EMPTY_STRING))) {
            window.location.reload();
        }
    }

    if (window.addEventListener) {
        //Event listener fires when a user clicks an checkin button in controlled doc content iframe window
        //In order to receive the events, the code in the other window (controlled doc content page) must
        //register a handler for 'message' events on its window:
        window.addEventListener(MESSAGE_EVENT, reloadWhenCheckin, false);
    }

    return cq;
}(document, window.cq || {}));