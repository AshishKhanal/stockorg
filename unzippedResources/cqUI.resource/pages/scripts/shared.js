/*jshint browser: true */
/*global window document */

/**
*  Extends cq namespace to add scripts helper methods for use in other pages
*/
window.cq = (function (doc, cq) {
    "use strict";

    var LOADING_DEFAULT_TEMPLATE =
            ("<div class='cq-loader-bkgnd' id='loading'>" +
                "<div class='cq-loader'>" +
                    "<div class='cq-img'></div>" +
                    "<div class='cq-msg'></div>" +
                "</div>" +
            "</div>"),
        CQ_HIDE_CLASS = "cq-hide",
        LOADER_SELECTOR = "#loading",
        LOADER_MESSAGE_SELECTOR = ".cq-msg",
        BODY_SELECTOR = "body",
        ERROR_DIV_SELECTOR = ".errorDiv",
        jQuery = window.jQuery,
        LIGHTNING_ALOHA_REDIRECT_MATCH = /#\/alohaRedirecthttps/,
        LIGHTNING_JS_MATCH = /javascript:\w+\((.+)\)/,
        HEIGHT = 'height',
        PX = 'px',
        PB_HEADER = '.pbHeader';

    if (jQuery) {
        jQuery(doc).ready(function () {
            cq.hideLoading();
        });
    } else {
        window.console.warn("jQuery is not defined.");
    }

    /**
     * Common variable for holding the namespace prefix of ComplianceQuest
     * @since 7.0
     */
    cq.NS_PREFIX = "compliancequest";

    /**
    * Shows a loader with a given message
    * @param message {string} The string message to display
    * @since 7.0
    */
    cq.showLoading = function (message) {
        var loading = jQuery(LOADER_SELECTOR);

        if (loading.length === 0) {
            loading = jQuery(LOADING_DEFAULT_TEMPLATE);
            loading.appendTo(jQuery(BODY_SELECTOR));
        }

        loading.find(LOADER_MESSAGE_SELECTOR).text(message || '');
        loading.removeClass(CQ_HIDE_CLASS);
    };

    /**
    * Hides the loader present in the screen
    * @since 7.0
    */
    cq.hideLoading = function () {
        jQuery(LOADER_SELECTOR).addClass(CQ_HIDE_CLASS);
    };

    /**
    * Reloads page displaying alert messages, if any
    * @since 7.0
    */
    cq.reloadPage = function () {
        var msg = jQuery(ERROR_DIV_SELECTOR)[0].innerText.trim();
        if (msg.length > 0) {
            // implies that there are some page messges
            // displaying the message
            window.alert(msg);
        }

        window.location.reload();
    };

    /**
     * Shows a given DOM element on the screen
     * @param element {Object} the DOM element that is to be hidden
     * @since 7.0
     */
    cq.showElement = function (element) {
        jQuery(element).removeClass(CQ_HIDE_CLASS);
    };

    /**
     * Hides a given DOM element on the screen
     * @param element {Object} the DOM element that is to be hidden
     * @since 7.0
     */
    cq.hideElement = function (element) {
        jQuery(element).addClass(CQ_HIDE_CLASS);
    };



    /**
     * Returns the value of query param provided in the url or current location
     * See http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
     * @param name {String} the name of the query param to search
     * @param url {String} optional param specifying the url to find the param in. If nothing
     *                     is passed matches with the location ref
     */
    function getUrlParamAloha(name, url) {
        if (!url) {
            url = window.location.href;
        }

        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url),
            match = null;

        if (results) {
            if (!results[2]) {
                match = '';
            } else {
                match = decodeURIComponent(results[2].replace(/\+/g, " "));
            }
        }

        return match;
    }

    /**
     * Returns the value of query param provided in the url or current location for lightning
     * @param name {String} the name of the query param to search
     * @param url {String} optional param specifying the url to find the param in. If nothing
     *                     is passed matches with the location ref
     */
    function getUrlParamLightning(name, url) {
        var tempUrl, match;

        url = url || window.location.href;

        tempUrl = LIGHTNING_JS_MATCH.exec(url);
        if (tempUrl) {
            url = decodeURIComponent(tempUrl[1]); //decode first group, since it is the actual encoded url
        } else {
            tempUrl = LIGHTNING_ALOHA_REDIRECT_MATCH.exec(url);
            if (tempUrl) {
                //Assumption: only https is the valid protocol no other is supported
                match = 'https' + url.substring(tempUrl.index + tempUrl[0].length);
                url = match ? decodeURIComponent(match) : url;
            }
        }

        return getUrlParamAloha(name, url);
    }

    /**
     * Navigates to given url
     * @param url {String} Url to navigate 
     * @param target {String} button target (_blank, _top, _self, _parent)
     */
    cq.navigateToUrl = function(url, target) {
        if (cq.i_isLightning()) {
            sforce.one.navigateToURL(url);
        } else {
            window.open(url, target);
        }
    }

    /**
     * Returns true or false based on whether current UI is being displayed in lightning mode.
     * i.e. Apex page in lightning experience.
     */
    cq.i_isLightning = function() {
        return typeof(window.sforce) === "object";
    }

    /**
     * Returns the value of query param provided in the url or current location.
     * @param name {String} the name of the query param to search
     * @param url {String} optional param specifying the url to find the param in. If nothing
     *                     is passed matches with the location ref
     */
    cq.i_getQueryParam = function(name, url) {
        // switch the method to use correct function
        // lazily once loaded
        if(cq.i_isLightning()) {
            cq.i_getQueryParam = getUrlParamLightning;
        }
        else {
            cq.i_getQueryParam = getUrlParamAloha;
        }

        return cq.i_getQueryParam.apply(null, arguments);
    }

    /**
     * Sets the height of related list
     * @param divs array of div inwhich the css to be applied
     * @param listHeightFactor integer value in pixel used for adjusting height of page block
     */
    cq.adjustListHeight = function(divs, listHeightFactor) {
        if (divs && divs.length > 0) { 
            var headerSize = jQuery(PB_HEADER).height() * divs.length,
                listHeight = parseInt(($(window).height() - headerSize) / divs.length) - (listHeightFactor ? listHeightFactor : 0);
            
            // apply css for each provided div
            for (var i in divs) {
                jQuery(divs[i]).css(HEIGHT, listHeight + PX);
            }
        }
    }

    /**
     * @description: remove isdtp from url(lightning mode).
     */
    cq.removeISDTPFromURL = function(url){
        return url.replace('isdtp=p1', '');
    }

    return cq;

}(document, window.cq || {}));
