$(document).ready(function() {
    const conn = new jsforce.Connection({ accessToken: sessionId });
    console.log(sessionId);
    const fieldTrackingLimit = 60;
    const greenHeader = 'slds-button_success';
    const redHeader = 'slds-button_destructive';
    const defaultArchiveAfterMonths = 18;
    const defaultArchiveRetentionYears = 10;
    const defaultGracePeriodDays = 1;
    const metadataAPIObjectLimit = 10;


    function updateMetadata(type, metadataForUpdate, callback) {
        conn.metadata.update(type, metadataForUpdate, function(err, results) {
            return callback(err, results);
        });
    }

    function readMetadata(type, fullNames, callback) {
        conn.metadata.read(type, fullNames, function(err, metadata) {
            return callback(err, metadata);
        });
    }

    $(document).on('click', '.select-object button', function(e){
        let element = this;
        $(element).parents('.slds-accordion__section').toggleClass('slds-is-open');
    });

    function generateFieldList(obj) {
        let objectName = obj.fullName;
        let fieldListItems = '';
        let fieldCounter = 0;
        let workableFields = [];
        let archiveAfterMonths = defaultArchiveAfterMonths;
        let archiveRetentionYears = defaultArchiveRetentionYears;
        let gracePeriodDays = defaultGracePeriodDays;

        if (obj.historyRetentionPolicy) {
            archiveAfterMonths = obj.historyRetentionPolicy.archiveAfterMonths;
            archiveRetentionYears = obj.historyRetentionPolicy.archiveRetentionYears;
            gracePeriodDays = obj.historyRetentionPolicy.gracePeriodDays;
        }
        if (obj.fields) {
            if (!Array.isArray(obj.fields)) obj.fields = [obj.fields];
            obj.fields.forEach(function(field) {
                let keysArray = Object.keys(field);
                if (field.hasOwnProperty('trackHistory') && !keysArray.includes('formula')) {
                    workableFields.push(field);
                    let historyIsTracked = '';
                    if (field.trackHistory === 'true') {
                        historyIsTracked = 'checked';
                    }
                    if (fieldCounter % 4 === 0) {
                        fieldListItems = fieldListItems + `<div class="slds-grid slds-wrap">`;
                    }
                    fieldListItems = fieldListItems + `<div class="slds-col slds-size_3-of-12">`;
                    fieldListItems = fieldListItems + `<input type="checkbox" class="field-checkbox" ${historyIsTracked} id="${objectName}-${field.fullName}"`;
                    keysArray.forEach(function(itm) {
                        if (typeof(field[itm]) === 'object') {
                            let stringifiedVal = JSON.stringify(field[itm]);
                            fieldListItems = fieldListItems + ` data-${itm}='${stringifiedVal}'`;
                        } else {
                            fieldListItems = fieldListItems + ` data-${itm}="${field[itm]}"`;
                        }
                    });
                    let keysString = keysArray.join(';'); {
                        fieldListItems = fieldListItems + ` data-keys="${keysString}"`;
                    }

                    fieldListItems = fieldListItems + ` value="${objectName}.${field.fullName}" /><span title="${field.fullName}">${field.label}</span>`;
                    if (fieldCounter % 4 === 3) {
                        fieldListItems = fieldListItems + `</div>`;
                    }
                    fieldListItems = fieldListItems + `</div>`;
                    fieldCounter++;
                }
            });
        }
        if (fieldListItems.length) {
            fieldListItems = `
                <div class="field-list">
                    <div class="historyRenentionPolicy">
                        <form class="hrp-form">
                            Archive After Months: <input class="archive-after-months" type="Number" max="18" min="1" value="${archiveAfterMonths}" name="archiveAfterMonths" />
                            Archive Retention Years: <input class="archive-retention-years" type="Number" max="10" min="0" value="${archiveRetentionYears}" name="archiveRetentionYears" />
                            Grace Period Days: <input class="grace-period-days" type="Number" max="10" min="0" value="${gracePeriodDays}" name="gracePeriodDays" />
                            <button type="button" class="slds-button_brand hrp-update-btn">Update Policy </button>
                        </form>
                    </div>
                    <form class="field-form">
                        <button type="button" class="slds-button_brand field-save-btn">Save </button>
                        <span class="saving-display slds-hide">Saving ...</span>
                        ${fieldListItems}
                    </form>
                </div>
            `;
        } else {
            fieldListItems = '<div class="field-list">No fields to edit</div>';
        }
        let checkedFieldCount = 0;
        workableFields.forEach((item) => {
            if (item.trackHistory === 'true') checkedFieldCount++;
        });
        let fieldListItemsObj = { fieldListItems, workableFields, checkedFieldCount };
        return fieldListItemsObj;
    }

    function generateAccordionView(objArray) {
        if (!Array.isArray(objArray)) objArray = [objArray];
        let accordionListItem = '';
        objArray.forEach(function(obj) {
            let objectName = obj.fullName;
            let objectLabel = obj.label;
            let objectPluralLabel = obj.pluralLabel;
            let objectNameField = {};
            if (obj.nameField) {
                objectNameField.label = obj.nameField.label;
                objectNameField.type = obj.nameField.type;
                objectNameField.trackHistory = obj.nameField.trackHistory;
                objectNameField.displayFormat = obj.nameField.displayFormat;
            }
            let objectNameFieldLabel = objectNameField.label || '';
            let fieldListItemsObj = generateFieldList(obj);
            let fieldListItems = fieldListItemsObj.fieldListItems;
            let objLevelTrackingEnabled = '';
            let totalFieldCount = fieldListItemsObj.workableFields.length;
            if (totalFieldCount) {
                let checkedField = fieldListItemsObj.workableFields.find((field) => field.trackHistory === "true" );
                if (checkedField) {
                    objLevelTrackingEnabled = 'checked';
                }
            }

            let objectCheckBoxOption = `
                <div class="slds-form-element">
                    <label class="slds-checkbox_toggle slds-grid">
                    <input class="object-checkbox" name="checkbox-toggle-14" ${objLevelTrackingEnabled} type="checkbox" aria-describedby="checkbox-toggle-14" value="checkbox-toggle-14" />
                    <span id="checkbox-toggle-14" class="slds-checkbox_faux_container" aria-live="assertive">
                        <span class="slds-checkbox_faux"></span>
                        <span class="slds-checkbox_on">Enabled</span>
                        <span class="slds-checkbox_off">Disabled</span>
                    </span>
                    </label>
                </div>
            `;

            let archiveAfterMonths = defaultArchiveAfterMonths;
            let archiveRetentionYears = defaultArchiveRetentionYears;
            let gracePeriodDays = defaultGracePeriodDays;

            if (obj.historyRetentionPolicy) {
                archiveAfterMonths = obj.historyRetentionPolicy.archiveAfterMonths;
                archiveRetentionYears = obj.historyRetentionPolicy.archiveRetentionYears;
                gracePeriodDays = obj.historyRetentionPolicy.gracePeriodDays;
            }

            let historyRetentionPolicy = `
                <span>(
                    Archive After Months: <span class="aam-read">${archiveAfterMonths}</span>
                    | Archive Retention Years: <span class="ary-read">${archiveRetentionYears}</span>
                    | Grace Period Days: <span class="gpd-read">${gracePeriodDays}</span>
                    )
                </span>
            `;
            let checkedFieldCount = fieldListItemsObj.checkedFieldCount;
            let header_style = '';
            if (totalFieldCount > fieldTrackingLimit) header_style = redHeader;
            if (totalFieldCount <= fieldTrackingLimit &&
                totalFieldCount === checkedFieldCount &&
                checkedFieldCount !== 0) header_style = greenHeader;
            accordionListItem = accordionListItem + `
                <li class="slds-accordion__list-item">
                    <section class="slds-accordion__section">
                        <!-- ObjectName holder only -->
                        <span title="${objectName}" class="object-API-name"></span>
                        <span title="${objectLabel}" class="object-API-label"></span>
                        <span title="${objectPluralLabel}" class="object-API-pluralLabel"></span>
                        <span title="${objectNameFieldLabel}" class="object-API-objectNameFieldLabel" data-trackhistory="${objectNameField.trackHistory}" data-type="${objectNameField.type}" data-displayformat="${objectNameField.displayFormat}"></span>
                        <span title="customObjRequirement" class="object-API-customObjRequirement" data-deploymentStatus="${obj.deploymentStatus}" data-sharingModel="${obj.sharingModel}" data-description="${obj.description}"></span>
                        <div class="slds-accordion__summary select-object">
                            <h3 class="slds-accordion__summary-heading ${header_style}">
                                <button aria-controls="accordion-details-${objectName}" aria-expanded="false" class="slds-button slds-button_reset slds-accordion__summary-action">
                                    <span class="slds-truncate" title="${objectLabel}">${objectLabel}</span>
                                </button>
                                 ${historyRetentionPolicy}
                                <span class="slds-float_right field-count">
                                    <span class="checked-fieldCount">${checkedFieldCount}</span>
                                    /
                                    <span class="total-fieldCount">${totalFieldCount}</span>
                                </span>
                            </h3>
                            ${objectCheckBoxOption}
                        </div>
                        <div aria-hidden="true" class="slds-accordion__content" id="accordion-details-${objectName}">
                        ${fieldListItems}
                        </div>
                    </section>
                </li>
            `;
        });
        $('#outer-wrapping-ul').append(accordionListItem);
    }

    conn.describeGlobal(function(err, res) {
        if (err) { return console.error(err); }
        let CQ_customRegex = new RegExp(/^(compliancequest__)[\w]+(__c)$/);
        let objectList = res.sobjects;
        let type = 'CustomObject';
        let filteredObjectList = objectList.filter(function(obj) {
            return CQ_customRegex.test(obj.name);
        });
        let totalObjectCount = filteredObjectList.length;
        let fetchedObjectCount = 0;
        $('#total-object-count')[0].innerHTML = totalObjectCount;

        let fullNames = [];
        filteredObjectList.forEach(function(obj) {
            fullNames.push(obj.name);
        });

        for (let i = 0; i < fullNames.length / metadataAPIObjectLimit; i++) {
                    let slicedFullNames = fullNames.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);

                    readMetadata(type, slicedFullNames, function(err, objMetadata) {
                        fetchedObjectCount = fetchedObjectCount + slicedFullNames.length;
                        $('#fetched-object-count')[0].innerHTML = fetchedObjectCount;
                        if (err) return console.error(err);
                        if (!Array.isArray(objMetadata)) objMetadata = [objMetadata];
                        let filteredMetadataList = objMetadata.filter(function(ele) {
                            if (ele.hasOwnProperty('actionOverrides')) {
                                return ele;
                            }
                        });
                        if (filteredMetadataList.length) {
                            filteredMetadataList.forEach(function(obj, idx) {
                                conn.describe(obj.fullName, function(err, metaResponse) {
                                    if (err) {
                                        return console.error(err);
                                    }
                                    if (obj.fields) {
                                        if (!Array.isArray(obj.fields)) obj.fields = [obj.fields];
                                        obj.fields.forEach(function(field, fieldIdx) {
                                            metaResponse.fields.find(function(namedField) {
                                                if (namedField.name === field.fullName) {
                                                    field.label = namedField.label;
                                                }
                                            });
                                            if (idx === filteredMetadataList.length - 1 && fieldIdx === obj.fields.length - 1) {
                                                setTimeout(function() {
                                                    generateAccordionView(filteredMetadataList);
                                                    $('#search-box input').removeAttr('disabled');
                                                }, 1000);
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    });
        }
    });

    function getMetadataResponseErrorArray (responseArray) {
        let errorArray = [];
        responseArray.forEach((item) => {
            if (item.success === false) {
                if (!Array.isArray(item.errors)) item.errors = [item.errors];
                item.errors.forEach((error) => {
                    errorArray.push(`<p> ${error.message} </p>`);
                });
            }
        });
        return errorArray.join('');
    }

    $(document).on('change', '.slds-accordion__summary input.object-checkbox', function() {
        let clickedElement = $(this);
        clickedElement.attr('disabled', 'disabled');
        let objectAPIName = clickedElement.parents('section').children('span.object-API-name').attr('title');
        let objectLabel = clickedElement.parents('section').children('span.object-API-label').attr('title');
        let isChecked = clickedElement.prop('checked');

        readMetadata('CustomObject', [objectAPIName], function(err, readResp) {
            if (err) {
                console.error(err);
                let toastMessage = err.message;
                $('#error-toast-message').html(toastMessage);
                clickedElement.removeAttr('disabled');
                return toastLaunch("Error");
            }
            let fetchedMetadata = readResp;
            fetchedMetadata.enableHistory = isChecked;
            if (!isChecked) {
                let fieldsArray = [];
                let fields = $(clickedElement).parents('section').find('.field-form input:checked');
                fields = Array.from(fields);
                fields.forEach(function(field) {
                    let data = $(field).data();
                    let keysArray = data.keys.split(';');
                    let fieldObj = {};
                    keysArray.forEach(function(key) {
                        fieldObj[key] = data[key.toLowerCase()];
                    });
                    fieldObj.fullName = field.value;
                    fieldObj.trackHistory = false;
                    if (fieldObj.fullName.slice(-3) !== '__c') {
                        delete fieldObj.label;
                    }

                    fieldsArray.push(fieldObj);
                });
                if (!fieldsArray.length) {
                    updateMetadata('CustomObject', [fetchedMetadata], function(err, resp) {
                        if (err) {
                            console.error(err);
                            let toastMessage = err.message;
                            $('#error-toast-message').html(toastMessage);
                            clickedElement.removeAttr('disabled');
                            return toastLaunch("Error");
                        }
                        if (!Array.isArray(resp)) resp = [resp];
                        let errorArray = getMetadataResponseErrorArray(resp);
                        readMetadata('CustomObject', [objectAPIName], function(err, response) {
                            if (err) {
                                console.error(err);
                                let toastMessage = err.message;
                                $('#error-toast-message').html(toastMessage);
                                clickedElement.removeAttr('disabled');
                                return toastLaunch("Error");
                            }
                            conn.describe(objectAPIName, function(err, metaResponse) {
                                if (err) {
                                    console.error(err);
                                    let toastMessage = err.message;
                                    $('#error-toast-message').html(toastMessage);
                                    clickedElement.removeAttr('disabled');
                                    return toastLaunch("Error");
                                }
                                if (!Array.isArray(response.fields)) response.fields = [response.fields];
                                response.fields.forEach(function(field, fieldIdx) {
                                    metaResponse.fields.find(function(namedField) {
                                        if (namedField.name === field.fullName) {
                                            field.label = namedField.label;
                                        }
                                    });
                                    if (fieldIdx === response.fields.length - 1) {
                                        let fieldListItemsObj = generateFieldList(response);
                                        let fieldListItems = fieldListItemsObj.fieldListItems;
                                        clickedElement.parents('section').find('.field-list').remove();
                                        clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                                        clickedElement.removeAttr('disabled');
                                        if (!errorArray.length) {
                                            let toastMessage = 'Tracking disabled for object ' + objectLabel;
                                            $('#success-toast-message').html(toastMessage);
                                            clickedElement.parents('.select-object').removeClass(greenHeader + ' ' + redHeader);
                                            console.log(clickedElement.parents('.select-object').find('.checked-fieldCount'));
                                            clickedElement.parents('.select-object').find('.checked-fieldCount')[0].innerHTML = fieldListItemsObj.checkedFieldCount;
                                            clickedElement.parents('.select-object').find('.total-fieldCount')[0].innerHTML = fieldListItemsObj.workableFields.length;
                                            clickedElement.parents('.select-object').find('h3').removeClass(greenHeader);
                                            return toastLaunch("Success");
                                        } else {
                                            let toastMessage = `Tracking disable incomplete for object ${objectLabel} <p>${errorArray}</p>`;
                                            $('#error-toast-message').html(toastMessage);
                                            return toastLaunch("Error");
                                        }
                                    }
                                });
                            });
                        });
                    });
                } else {
                    let errorArray = [];
                    let requestCount = 0;
                    let responseCount = 0;
                    for (let i = 0; i < fieldsArray.length / metadataAPIObjectLimit; i++) {
                        let slicedFieldArray = fieldsArray.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);
                        requestCount++;
                        updateMetadata('CustomField', slicedFieldArray, function(err, updateResp) {
                            if (err) {
                                console.error(err);
                                let toastMessage = err.message;
                                $('#error-toast-message').html(toastMessage);
                                clickedElement.removeAttr('disabled');
                                return toastLaunch("Error");
                            }
                            responseCount++;
                            if (!Array.isArray(updateResp)) updateResp = [updateResp];
                            errorArray = errorArray + getMetadataResponseErrorArray(updateResp);
                            if (requestCount === responseCount) {
                                if (!errorArray.length) {
                                    readMetadata('CustomObject', [objectAPIName], function(err, metaDataResponse) {
                                        if (err) {
                                            console.error(err);
                                            let toastMessage = err.message;
                                            $('#error-toast-message').html(toastMessage);
                                            clickedElement.removeAttr('disabled');
                                            return toastLaunch("Error");
                                        }
                                        metaDataResponse.enableHistory = isChecked;
                                        updateMetadata('CustomObject', [metaDataResponse], function(err, response) {
                                            if (err) {
                                                console.error(err);
                                                let toastMessage = err.message;
                                                $('#error-toast-message').html(toastMessage);
                                                clickedElement.removeAttr('disabled');
                                                return toastLaunch("Error");
                                            }
                                            if (!Array.isArray(response)) response = [response];
                                            errorArray = getMetadataResponseErrorArray(response);
                                            readMetadata('CustomObject', [objectAPIName], function(err, readResp) {
                                                if (err) {
                                                    console.error(err);
                                                    let toastMessage = err.message;
                                                    $('#error-toast-message').html(toastMessage);
                                                    clickedElement.removeAttr('disabled');
                                                    return toastLaunch("Error");
                                                }
                                                conn.describe(objectAPIName, function(err, metaResponse) {
                                                    if (err) {
                                                        console.error(err);
                                                        let toastMessage = err.message;
                                                        $('#error-toast-message').html(toastMessage);
                                                        clickedElement.removeAttr('disabled');
                                                        return toastLaunch("Error");
                                                    }
                                                    if (!Array.isArray(readResp.fields)) readResp.fields = [readResp.fields];
                                                    readResp.fields.forEach(function(field, fieldIdx) {
                                                        metaResponse.fields.find(function(namedField) {
                                                            if (namedField.name === field.fullName) {
                                                                field.label = namedField.label;
                                                            }
                                                        });
                                                        if (fieldIdx === readResp.fields.length - 1) {
                                                            let fieldListItemsObj = generateFieldList(readResp);
                                                            let fieldListItems = fieldListItemsObj.fieldListItems;
                                                            clickedElement.parents('section').find('.field-list').remove();
                                                            clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                                                            if (!errorArray.length) {
                                                                let toastMessage = 'Tracking disabled for object ' + objectLabel;
                                                                $('#success-toast-message').html(toastMessage);
                                                                clickedElement.removeAttr('disabled');
                                                                clickedElement.parents('.select-object').removeClass(greenHeader + ' ' + redHeader);
                                                                clickedElement.parents('.select-object').find('.checked-fieldCount')[0].innerHTML = fieldListItemsObj.checkedFieldCount;
                                                                clickedElement.parents('.select-object').find('.total-fieldCount')[0].innerHTML = fieldListItemsObj.workableFields.length;
                                                                clickedElement.parents('.select-object').find('h3').removeClass(greenHeader);
                                                                return toastLaunch("Success");
                                                            } else {
                                                                let toastMessage = `Tracking disable incomplete for object ${objectLabel} <p>${errorArray}</p>`;
                                                                $('#error-toast-message').html(toastMessage);
                                                                clickedElement.removeAttr('disabled');
                                                                return toastLaunch("Error");
                                                            }
                                                        }
                                                    });
                                                });
                                            });
                                        });
                                    });
                                } else {
                                    let toastMessage = `Tracking disable incomplete for object ${objectLabel} <p>${errorArray}</p>`;
                                    $('#error-toast-message').html(toastMessage);
                                    clickedElement.removeAttr('disabled');
                                    return toastLaunch("Error");
                                }
                            }
                        });
                    }
                }
            }
            if(isChecked) {
                updateMetadata('CustomObject', [fetchedMetadata], function(err, resp) {
                    if (err) {
                        console.error(err);
                        let toastMessage = err.message;
                        $('#error-toast-message').html(toastMessage);
                        clickedElement.removeAttr('disabled');
                        return toastLaunch("Error");
                    }
                    if (!Array.isArray(resp)) resp = [resp];
                    let errorArray = getMetadataResponseErrorArray(resp);
                    readMetadata('CustomObject', [objectAPIName], function(err, readResp) {
                        if (err) {
                            console.error(err);
                            let toastMessage = err.message;
                            $('#error-toast-message').html(toastMessage);
                            clickedElement.removeAttr('disabled');
                            return toastLaunch("Error");
                        }
                        let fieldListItemsObj = generateFieldList(readResp);
                        let fieldListItems = fieldListItemsObj.fieldListItems;
                        let workableFields = fieldListItemsObj.workableFields;
                        if (workableFields.length && workableFields.length <= fieldTrackingLimit) {
                            workableFields.forEach(function(field) {
                                field.fullName = objectAPIName + '.' + field.fullName;
                                field.trackHistory = true;
                            });
                            if (!errorArray.length) {
                                let requestCount = 0;
                                let responseCount = 0;
                                for (let i = 0; i < workableFields.length / metadataAPIObjectLimit; i++) {
                                    let slicedFieldArray = workableFields.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);
                                    requestCount++;
                                    updateMetadata('CustomField', slicedFieldArray, function(err, updateResp) {
                                        if (err) {
                                            console.error(err);
                                            let toastMessage = err.message;
                                            $('#error-toast-message').html(toastMessage);
                                            clickedElement.removeAttr('disabled');
                                            return toastLaunch("Error");
                                        }
                                        responseCount++;
                                        if (!Array.isArray(updateResp)) updateResp = [updateResp];
                                        errorArray = errorArray + getMetadataResponseErrorArray(updateResp);
                                        if (requestCount === responseCount) {
                                            readMetadata('CustomObject', [objectAPIName], function(err, readResponse) {
                                                if (err) {
                                                    console.error(err);
                                                    let toastMessage = err.message;
                                                    $('#error-toast-message').html(toastMessage);
                                                    clickedElement.removeAttr('disabled');
                                                    return toastLaunch("Error");
                                                }
                                                conn.describe(objectAPIName, function(err, metaResponse) {
                                                    if (err) {
                                                        console.error(err);
                                                        let toastMessage = err.message;
                                                        $('#error-toast-message').html(toastMessage);
                                                        clickedElement.removeAttr('disabled');
                                                        return toastLaunch("Error");
                                                    }
                                                    if (!Array.isArray(readResponse.fields)) readResponse.fields = [readResponse.fields];
                                                    readResponse.fields.forEach(function(field, fieldIdx) {
                                                        metaResponse.fields.find(function(namedField) {
                                                            if (namedField.name === field.fullName) {
                                                                field.label = namedField.label;
                                                            }
                                                        });
                                                        if (fieldIdx === readResponse.fields.length - 1) {
                                                            let fieldListItemsObj = generateFieldList(readResponse);
                                                            let fieldListItems = fieldListItemsObj.fieldListItems;
                                                            clickedElement.parents('section').find('.field-list').remove();
                                                            clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                                                            if (!errorArray.length) {
                                                                let toastMessage = 'Tracking enabled for object ' + objectLabel;
                                                                $('#success-toast-message').html(toastMessage);
                                                                clickedElement.removeAttr('disabled');
                                                                clickedElement.parents('.select-object').find('h3').addClass(greenHeader);
                                                                clickedElement.parents('.select-object').find('.checked-fieldCount')[0].innerHTML = fieldListItemsObj.checkedFieldCount;
                                                                clickedElement.parents('.select-object').find('.total-fieldCount')[0].innerHTML = fieldListItemsObj.workableFields.length;
                                                                return toastLaunch("Success");
                                                            } else {
                                                                let toastMessage = `Tracking enable incomplete for object ${objectLabel} <p>${errorArray}</p>`;
                                                                $('#error-toast-message').html(toastMessage);
                                                                clickedElement.removeAttr('disabled');
                                                                return toastLaunch("Error");
                                                            }
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    });

                                }
                            } else {
                                let toastMessage = `Tracking enable incomplete for object ${objectLabel} <p>${errorArray}</p>`;
                                $('#error-toast-message').html(toastMessage);
                                clickedElement.removeAttr('disabled');
                                return toastLaunch("Error");
                            }
                        } else if (workableFields.length && workableFields.length > fieldTrackingLimit) {
                            clickedElement.parents('section').find('.field-list').remove();
                            clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                            let toastMessage = 'Maximum fields for history tracking reached, Please select desired fields';
                            $('#error-toast-message').html(toastMessage);
                            clickedElement.removeAttr('disabled');
                            clickedElement.parents('.select-object h3').addClass(redHeader);
                            return toastLaunch("Error");
                        } else {
                            clickedElement.parents('section').find('.field-list').remove();
                            clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                            let toastMessage = 'No fields available for tracking';
                            $('#success-toast-message').html(toastMessage);
                            clickedElement.removeAttr('disabled');
                            clickedElement.prop('checked', false);
                            return toastLaunch("Success");
                        }
                    });
                });
            }
        });

    });
    function savingDisplay(element) {
        element.attr('disabled', 'disabled');
        element.siblings('.saving-display').removeClass('slds-hide');
    }
    function savingDisplayDone(element) {
        element.removeAttr('disabled');
        element.siblings('.saving-display').addClass('slds-hide');
    }

    $(document).on('click', '.field-save-btn', function() {
        let clickedObject = $(this);
        let parentObject = $(clickedObject).parents('section');
        savingDisplay(clickedObject);
        let objectAPIName = clickedObject.parents('section').children('span.object-API-name').attr('title');
        let objectLabel = clickedObject.parents('section').children('span.object-API-label').attr('title');
        let fields = clickedObject.siblings().children().children('input.field-checkbox');
        fields = Array.from(fields);
        let fieldCheckCount = 0;
        let changedFieldArray = [];
        fields.forEach(function(field) {
            if(field.checked) fieldCheckCount++;
            if(field.checked !== field.defaultChecked) {
                let data = $(field).data();
                let keysArray = data.keys.split(';');
                let changedObj = {};
                keysArray.forEach(function(key) {
                    if (typeof(data[key.toLowerCase()]) === 'string' && data[key.toLowerCase()].includes('&quot;')) {
                        let stringifiedObj = data[key.toLowerCase()];
                        stringifiedObj = stringifiedObj.replace(/&quot;/g, '"');
                        changedObj[key] = JSON.parse(stringifiedObj);
                    } else {
                        changedObj[key] = data[key.toLowerCase()];
                    }
                });
                changedObj.fullName = field.value;
                changedObj.trackHistory = field.checked;
                if (changedObj.fullName.slice(-3) !== '__c') {
                    delete changedObj.label;
                }

                changedFieldArray.push(changedObj);
            }
        });
        if (fieldCheckCount > fieldTrackingLimit) {
            savingDisplayDone(clickedObject);
            let toastMessage = 'More than allowed number of fields selected for tracking';
            $('#error-toast-message').html(toastMessage);
            return toastLaunch("Error");
        }
        if (!changedFieldArray.length) {
            savingDisplayDone(clickedObject);
            return;
        }

        let errorArray = [];
        let requestCount = 0;
        let responseCount = 0;
        for (let i = 0; i < changedFieldArray.length / metadataAPIObjectLimit; i++) {
            let slicedFieldArray = changedFieldArray.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);
            requestCount++;
            updateMetadata('CustomField', slicedFieldArray, function(err, updateResp) {
                if (err) {
                    console.error(err);
                    savingDisplayDone(clickedObject);
                    let toastMessage = err.message;
                    $('#error-toast-message').html(toastMessage);
                    return toastLaunch("Error");
                }
                responseCount++;
                if (!Array.isArray(updateResp)) updateResp = [updateResp];
                errorArray = errorArray + getMetadataResponseErrorArray(updateResp);
                if (requestCount === responseCount) {
                    readMetadata('CustomObject', [objectAPIName], function(err, resp) {
                        if (err) {
                            savingDisplayDone(clickedObject);
                            console.error(err);
                            let toastMessage = err.message;
                            $('#error-toast-message').html(toastMessage);
                            return toastLaunch("Error");
                        }
                        conn.describe(objectAPIName, function(err, metaResponse) {
                            if (err) {
                                return console.error(err);
                            }
                            if (!Array.isArray(resp.fields)) resp.fields = [resp.fields];
                            resp.fields.forEach(function(field, fieldIdx) {
                                metaResponse.fields.find(function(namedField) {
                                    if (namedField.name === field.fullName) {
                                        field.label = namedField.label;
                                    }
                                });
                                if (fieldIdx === resp.fields.length - 1) {
                                    setTimeout(function() {
                                        let parentElement = clickedObject.parents('.slds-accordion__content');
                                        clickedObject.parents('.slds-accordion__content').children('.field-list').remove();
                                        let fieldListItemsObj = generateFieldList(resp);
                                        let fieldListItems = fieldListItemsObj.fieldListItems;
                                        parentElement.append(fieldListItems);
                                        savingDisplayDone(clickedObject);
                                        if (!errorArray.length) {
                                            let toastMessage = 'Object '+ objectLabel + ' modified successfully';
                                            $('#success-toast-message').html(toastMessage);
                                            parentObject.find('.checked-fieldCount')[0].innerHTML = fieldListItemsObj.checkedFieldCount;
                                            parentObject.find('.total-fieldCount')[0].innerHTML = fieldListItemsObj.workableFields.length;
                                            if (fieldListItemsObj.checkedFieldCount > 0 &&
                                            fieldListItemsObj.checkedFieldCount === fieldListItemsObj.workableFields.length) {
                                                parentObject.find('.slds-accordion__summary-heading').addClass(greenHeader);
                                            } else {
                                                parentObject.find('.slds-accordion__summary-heading').removeClass(greenHeader);
                                            }
                                            return toastLaunch("Success");
                                        } else {
                                            let toastMessage = `Object ${objectLabel} modification was incomplete <p>${errorArray}</p>`;
                                            $('#error-toast-message').html(toastMessage);
                                            return toastLaunch("Error");
                                        }
                                    }, 1000);
                                }
                            });
                        });

                    });
                }
            });
        }
    });

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    $('#search-box input').keyup(delay(function(e) {
        let valueToMatch = e.target.value;
        let liArray = $('.slds-accordion__list-item');
        liArray = Array.from(liArray);
        if (!valueToMatch.length) {
            $('li').removeClass('slds-hide');
            let filterValue = $(document).find("input[name=filter-object]:checked").val();
            filterObjectList(filterValue);
        } else {
            valueToMatch = valueToMatch.toLowerCase();
            liArray.forEach(function(li) {
                let objLabel = $(li).find('.object-API-label').attr('title').toLowerCase();
                if (!objLabel.includes(valueToMatch)) {
                    $(li).addClass('slds-hide');
                } else {
                    $(li).removeClass('slds-hide');
                }
            });
        }
    }, 500));
    function enableObjectHistory(objectAPIName, section, callback) {
        let clickedElement = $(section).find('.slds-accordion__summary input.object-checkbox');
        readMetadata('CustomObject', [objectAPIName], function(err, objMetadata) {
            if (err) return callback(err);
            let fetchedMetadata = objMetadata;
            fetchedMetadata.enableHistory = true;
            updateMetadata('CustomObject', [fetchedMetadata], function(err, resp) {
                if (err) {
                    console.error(err);
                    return callback(err);
                }
                if (!Array.isArray(resp)) resp = [resp];
                let errorArray = getMetadataResponseErrorArray(resp);
                if (errorArray.length) return callback({ message: errorArray });
                readMetadata('CustomObject', [objectAPIName], function(err, readResp) {
                    if (err) {
                        console.error(err);
                        return callback(err);
                    }
                    let fieldListItemsObj = generateFieldList(readResp);
                    let fieldListItems = fieldListItemsObj.fieldListItems;
                    let workableFields = fieldListItemsObj.workableFields;
                    if (workableFields.length && workableFields.length <= fieldTrackingLimit) {
                        workableFields.forEach(function(field) {
                            field.fullName = objectAPIName + '.' + field.fullName;
                            field.trackHistory = true;
                        });
                        if (!errorArray.length) {
                            let requestCount = 0;
                            let responseCount = 0;
                            for (let i = 0; i < workableFields.length / metadataAPIObjectLimit; i++) {
                                let slicedFieldArray = workableFields.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);
                                requestCount++;
                                updateMetadata('CustomField', slicedFieldArray, function(err, updateResp) {
                                    if (err) {
                                        console.error(err);
                                        return callback(err);
                                    }
                                    responseCount++;
                                    if (!Array.isArray(updateResp)) updateResp = [updateResp];
                                    errorArray = errorArray + getMetadataResponseErrorArray(updateResp);
                                    if (requestCount === responseCount) {
                                        readMetadata('CustomObject', [objectAPIName], function(err, readResponse) {
                                            if (err) {
                                                console.error(err);
                                                return callback(err);
                                            }
                                            conn.describe(objectAPIName, function(err, metaResponse) {
                                                if (err) {
                                                    console.error(err);
                                                    return callback(err);
                                                }
                                                if (!Array.isArray(readResponse.fields)) readResponse.fields = [readResponse.fields];
                                                readResponse.fields.forEach(function(field, fieldIdx) {
                                                    metaResponse.fields.find(function(namedField) {
                                                        if (namedField.name === field.fullName) {
                                                            field.label = namedField.label;
                                                        }
                                                    });
                                                    if (fieldIdx === readResponse.fields.length - 1) {
                                                        let fieldListItemsObj = generateFieldList(readResponse);
                                                        let fieldListItems = fieldListItemsObj.fieldListItems;
                                                        clickedElement.parents('section').find('.field-list').remove();
                                                        clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                                                        if (!errorArray.length) {
                                                            clickedElement.parents('.select-object').find('h3').addClass(greenHeader);
                                                            clickedElement.parents('.select-object').find('.checked-fieldCount')[0].innerHTML = fieldListItemsObj.checkedFieldCount;
                                                            clickedElement.parents('.select-object').find('.total-fieldCount')[0].innerHTML = fieldListItemsObj.workableFields.length;
                                                            return callback(null);
                                                        } else {
                                                            console.error(errorArray);
                                                            return callback({message: errorArray});
                                                        }
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            }
                        } else {
                            console.error(err);
                            return callback(err);
                        }
                    } else if (workableFields.length && workableFields.length > fieldTrackingLimit) {
                        clickedElement.parents('section').find('.field-list').remove();
                        clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                        let toastMessage = 'Maximum fields for history tracking reached, Please select desired fields';
                        return callback({ message: toastMessage });
                    } else {
                        clickedElement.parents('section').find('.field-list').remove();
                        clickedElement.parents('section').find('.slds-accordion__content').append(fieldListItems);
                        clickedElement.prop('checked', false);
                        return callback(null);
                    }
                });
            });
        });
    }

    function enableAllObjects() {
        let allObjects = Array.from($(document).find('.slds-accordion__section'));
        let count = 0;
        $('#total-objects')[0].innerHTML = allObjects.length;
        allObjects.forEach(function(section, index) {
            let objectAPIName = $(section).find('span.object-API-name').attr('title');

            let isGreen = $(section).find('.slds-accordion__summary-heading').hasClass(greenHeader);
            if (!isGreen) {
                enableObjectHistory(objectAPIName, section, function(err, response) {
                    if (err) {
                        $(section).find('.select-object h3').addClass(redHeader);
                        $(section).append(`<p>${err.message}</p>`);
                    } else {
                        $(section).find('.object-checkbox').attr('checked', 'checked');
                    }
                    count++;
                    $('#processed-count')[0].innerHTML = count;
                    if (allObjects.length === count) {
                        $('#enable-all-btn').removeAttr('disabled');
                        toastClose("Waiting");
                    }
                });
            } else {
                count++;
                $('#processed-count')[0].innerHTML = count;
                if (allObjects.length === count) {
                    $('#enable-all-btn').removeAttr('disabled');
                    toastClose("Waiting");
                }
            }
        });
    }

    $(document).on('click', '#enable-all-btn', function(e) {
        $('#enable-all-btn').attr('disabled', 'disabled');
        $('#processed-count')[0].innerHTML = 0;
        toastLaunch("Waiting");
        enableAllObjects();
    });

    function filterObjectList(filterValue) {
        let totalObjectList = $('#outer-wrapping-ul').find('li');
        switch(filterValue) {
            case 'none':
                $('#search-box input')[0].value = ''
                totalObjectList.removeClass('slds-hide');
                break;
            case 'redHeader':
                $('#search-box input')[0].value = ''
                totalObjectList.addClass('slds-hide');
                totalObjectList.find('.slds-accordion__summary-heading.'+redHeader).parents('.slds-accordion__list-item').removeClass('slds-hide');
                break;
            case 'greenHeader':
                $('#search-box input')[0].value = ''
                totalObjectList.addClass('slds-hide');
                totalObjectList.find('.slds-accordion__summary-heading.'+greenHeader).parents('.slds-accordion__list-item').removeClass('slds-hide');
                break;
            default:
                $('#search-box input')[0].value = ''
                totalObjectList.removeClass('slds-hide');
                totalObjectList.find('.slds-accordion__summary-heading.'+redHeader).parents('.slds-accordion__list-item').addClass('slds-hide');
                totalObjectList.find('.slds-accordion__summary-heading.'+greenHeader).parents('.slds-accordion__list-item').addClass('slds-hide');
        }
    }

    $("input[name=filter-object]").on("change", function(e) {
        const filterValue = e.target.value;
        filterObjectList(filterValue);
    });

    $(document).on('click', '.hrp-update-btn', function(e) {
        let selectedObject = $(this);
        let historyRetentionPolicy = {};
        let objectAPIName = selectedObject.parents('section').children('span.object-API-name').attr('title');
        historyRetentionPolicy.archiveAfterMonths = parseInt(selectedObject.parents('.hrp-form').find('.archive-after-months').val());
        historyRetentionPolicy.archiveRetentionYears = parseInt(selectedObject.parents('.hrp-form').find('.archive-retention-years').val());
        historyRetentionPolicy.gracePeriodDays = parseInt(selectedObject.parents('.hrp-form').find('.grace-period-days').val());
        console.log(historyRetentionPolicy);
        selectedObject.attr('disabled', 'disabled');
        readMetadata('CustomObject', [objectAPIName], function(err, objMetadata) {
            if (err) {
                console.error(err);
                let toastMessage = err.message;
                $('#error-toast-message').html(toastMessage);
                selectedObject.removeAttr('disabled');
                return toastLaunch("Error");
            }
            objMetadata.historyRetentionPolicy = historyRetentionPolicy;
            updateMetadata('CustomObject', [objMetadata], function(err, response) {
                if (err) {
                    console.error(err);
                    let toastMessage = err.message;
                    $('#error-toast-message').html(toastMessage);
                    selectedObject.removeAttr('disabled');
                    return toastLaunch("Error");
                }
                if (!Array.isArray(response)) response = [response];
                let errors = getMetadataResponseErrorArray(response);
                if (errors.length) {
                    let toastMessage = errors;
                    $('#error-toast-message').html(toastMessage);
                    selectedObject.removeAttr('disabled');
                    return toastLaunch('Error');
                }
                selectedObject.parents('section').find('.aam-read')[0].innerHTML = historyRetentionPolicy.archiveAfterMonths;
                selectedObject.parents('section').find('.ary-read')[0].innerHTML = historyRetentionPolicy.archiveRetentionYears;
                selectedObject.parents('section').find('.gpd-read')[0].innerHTML = historyRetentionPolicy.gracePeriodDays;
                let toastMessage = 'History Retention Policy updated successfully.';
                $('#success-toast-message').html(toastMessage);
                selectedObject.removeAttr('disabled');
                return toastLaunch('Success');
            });
        });
    });

    function updateHRP(objectAPIName, historyRetentionPolicy, callback) {
        readMetadata('CustomObject', objectAPIName, function(err, objMetadata) {
            if (err) return callback(err);
            if (!Array.isArray(objMetadata)) objMetadata = [objMetadata];
            objMetadata.forEach(function(ob) {
                ob.historyRetentionPolicy = historyRetentionPolicy
            });
            updateMetadata('CustomObject', objMetadata, function(err, resp) {
                if (err) {
                    console.error(err);
                    return callback(err);
                }
                if (!Array.isArray(resp)) resp = [resp];
                return callback(null, resp);
            });
        });
    }
    function updateSectionDisplay(slicedSections, historyRetentionPolicy, errorObject) {
        slicedSections.forEach(function(sec) {
            if (!errorObject[sec.objectAPIName]) {
                $(sec.section).find('.aam-read')[0].innerHTML = historyRetentionPolicy.archiveAfterMonths;
                $(sec.section).find('.ary-read')[0].innerHTML = historyRetentionPolicy.archiveRetentionYears;
                $(sec.section).find('.gpd-read')[0].innerHTML = historyRetentionPolicy.gracePeriodDays;
            } else {
                $(sec.section).find('.select-object h3').addClass(redHeader);
                $(sec.section).append(errorObject[sec.objectAPIName].join(''));
            }
        });
    }

    function setHRPForAllObjects() {
        let allObjects = Array.from($(document).find('.slds-accordion__section'));
        let count = 0;
        $('#total-objects')[0].innerHTML = allObjects.length;
        let historyRetentionPolicy = {
            archiveAfterMonths: $(document).find('.archive-after-months-global').val(),
            archiveRetentionYears: $(document).find('.archive-retention-years-global').val(),
            gracePeriodDays: $(document).find('.grace-period-days-global').val()
        };
        let errors = '';
        let tempA = parseInt(historyRetentionPolicy.archiveAfterMonths);
        let tempB = parseInt(historyRetentionPolicy.archiveRetentionYears);
        let tempC = parseInt(historyRetentionPolicy.gracePeriodDays);
        if (tempA < 1 || tempA > 18) errors = '<p>Archive After Months should be between 1 and 18.</p>';
        if (tempB < 0 || tempB > 10) errors = errors + '<p>Archive Retention Years should be between 0 and 10.</p>';
        if (tempC < 0 || tempC > 10) errors = errors + '<p>Archive After Months should be between 0 and 10.</p>';
        if (errors.length) {
            let toastMessage = errors;
            $('#error-toast-message').html(toastMessage);
            $('#set-hrp-for-all-btn').removeAttr('disabled');
            toastClose("Waiting");
            return toastLaunch("Error");
        }
        let allSectionsArray = [];
        let errorObjectArray = [];
        allObjects.forEach(function(section, index) {
            let objectAPIName = $(section).find('span.object-API-name').attr('title');
            allSectionsArray.push({
                objectAPIName,
                section
            });
        });
        let updateHRP_requestCounter = 0;
        let updateHRP_responseCounter = 0;
        for (let i = 0; i < allSectionsArray.length / metadataAPIObjectLimit; i++) {
            let slicedSections = allSectionsArray.slice(i * metadataAPIObjectLimit, (i+1) * metadataAPIObjectLimit);
            let slicedFullNames = slicedSections.map(function(sec) {
                return sec.objectAPIName;
            });
            updateHRP_requestCounter++;
            updateHRP(slicedFullNames, historyRetentionPolicy, function(err, response) {
                updateHRP_responseCounter++;
                count = count + slicedFullNames.length;
                $('#processed-count')[0].innerHTML = count;
                if (err) {
                    errorObjectArray.push(err);
                } else {
                    let errorObject = {};
                    response.forEach((item) => {
                        if (item.success === false) {
                            if (!Array.isArray(item.errors)) item.errors = [item.errors];
                            let objectAPIName = item.fullName.split('.')[0];
                            item.errors.forEach((error) => {
                                if (!errorObject[objectAPIName]) {
                                    errorObject[objectAPIName] = [`<p>${error.message}</p>`];
                                } else {
                                    errorObject[objectAPIName].push(`<p>${error.message}</p>`);
                                }
                            });
                        }
                    });
                    updateSectionDisplay(slicedSections, historyRetentionPolicy, errorObject);
                }
                if (updateHRP_requestCounter === updateHRP_responseCounter) {
                    if (allObjects.length === count) {
                        $('#set-hrp-for-all-btn').removeAttr('disabled');
                        toastClose("Waiting");
                        if (errorObjectArray.length) {
                            let toastMessage = errorObjectArray[0].message;
                            $('#error-toast-message').html(toastMessage);
                            toastLaunch('Error');
                        }
                    }
                }
            });
        }
    }

    $(document).on('click', '#set-hrp-for-all-btn', function(e) {
        $('#set-hrp-for-all-btn').attr('disabled', 'disabled');
        $('#processed-count')[0].innerHTML = 0;
        toastLaunch("Waiting");
        setHRPForAllObjects();
    });
});
