/*jshint browser: true */
/*global window document $*/

/**
*  Extends cq namespace to add scripts related to the page
*/
window.cq = (function (doc, cq) {
    "use strict";

    var ERROR_DIV_SELECTOR = '.errorDiv',
        GROUP_DETAIL_SELECTOR = '#groupDetails',
        GROUP_CLOSED_SELECTOR = '#groupClosed';

    /**
    * Method to display loader with the given message on collaboration group close
    * @param message {String} the message to display in the loading screen
    * @since 7.0
    */
    cq.closeCollaborationStart = function (message) {
        cq.showLoading(message);
    };

    /**
    * Callback method after group is closed, it hides the loader and shows the screen
    * @param expectedMsg {String} the message that is present in the screen for information purpose
    *                             but denotes successfully completed task.
    * @since 7.0
    */
    cq.groupClosed = function (expectedMsg) {
        cq.hideLoading();

        var msg = $(ERROR_DIV_SELECTOR)[0].innerText.trim();
        if (msg.length === 0 || msg === expectedMsg) {
            cq.hideElement($(GROUP_DETAIL_SELECTOR));
            cq.showElement($(GROUP_CLOSED_SELECTOR));
        }
    };

    return cq;
}(document, window.cq || {}));