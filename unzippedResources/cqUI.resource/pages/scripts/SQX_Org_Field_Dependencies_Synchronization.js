/*global window document */

window.cq = ( function ( doc, cq ) {
    "use strict";
    
    var jQuery = window.jQuery,
        selectedObjectApiName = '',
        selectedObjectLabel = null,
        RESULT_SUCCESS = 'success',
        RESULT_ERROR = 'error',
        CUSTOM_OBJECT = 'CustomObject',
        CUSTOM_FIELD = 'CustomField',
        JSFORCE_MAX_BATCH_SIZE = 10,
        GLOBAL_VALUE_SET = 'GlobalValueSet',
        w = window,
        VAL_TRUE = 'true',
        VAL_FALSE = 'false',
        VAL_BLANK = '',
        ORG_PREFIX = 'compliancequest',
        RECORD_TYPE = 'RecordType',
        API_VERSION='39';

    var records = [], // will holds all custom objects being queried
        objectId = null, // id of the object whose dependency page needs to be opened 
        valueSetNameToValueSetMapForDependency = null, // holds dependent fields as key and its corresponding valueSet as value
        fieldFullNameToValueSetMapForRecordTypes = null, // holds dependency fields(controlling and dependent) as key and its corresponding valueSet as value
        fieldApiNameToLabelMap = null, // holds field's api name to its label for all objects
        processedFieldsApiNameToLabelMap = null, // map that holds all field's api name to its label that have been processed
        globalValueSets = null, // will be used to read all needed GlobalValueSet
        unSuccessFields = []; // will hold all field for which synchronization gets failed.

    jQuery(doc).ready(function ($) {
        // query for all the custom object of specified org
        var types = [{type: CUSTOM_OBJECT, folder: null}];
        cq.conn.metadata.list(types, API_VERSION, function(err, metadata) {
            if (err) { 
                // oops!! got error
                console.error(err);
                alert(err);
            } else {
                console.log('Custom object metadata info');
                console.log(metadata);
                records = metadata;
            }
        });
    });
    
    /**
     * This method sets the object id of selected object and construct required url for dependency page whenever object in the drop-down list is changed.
     */
    cq.setObjectId = function() {

        // hide the required msg section
        jQuery(cq.REQUIRED_ERR_SELECTOR).hide();

        // get the selected object's api name and label from the drop down list
        selectedObjectApiName = cq.selectList.options[cq.selectList.selectedIndex].value;
        selectedObjectLabel = cq.selectList.options[cq.selectList.selectedIndex].text;
        objectId = null;
        for (var i in records) {

            // get the id of the selected object
            // Note: in managed org object's full name(api name) comes with org prefix but in unmanaged it comes without org prefix.
            if (records[i].namespacePrefix == ORG_PREFIX && (records[i].fullName == selectedObjectApiName || records[i].fullName == selectedObjectApiName.replace(ORG_PREFIX + '__', ''))) {
                objectId = records[i].id.substring(0,15); // when using 18 characters id, existing dependencies are not being listed
                break;
            }
        }
    }

    /**
     * This method opens the required dependency page for the selected object.
     */
    cq.openDependencyPageForSelectedObject = function() { 
        if (objectId == null) {
            
            // show required error message
            jQuery(cq.REQUIRED_ERR_SELECTOR).show();
            return;
        }

        // open the url
       window.open(cq.getURL(objectId));
    }

    /**
     * Returns the url of dependency page for the given object id
     * @param objectId Id of the custom object whose dependency page is to be opened.
     */
    cq.getURL = function(objectId) {
        var dependencyURL = '';

        // create dependency url
        // hack : to open dependency page of object, there are two different url for lightning and for classic. To DO : find the better approach.
        if (cq.i_isLightning()) {
            var end_uri = '/setup/ui/dependencyList.jsp?tableEnumOrId=' + objectId;
            dependencyURL = cq.baseURL + '/one/one.app?source=aloha#/setup/ObjectManager/' + objectId + '/FieldsAndRelationships/page?address=' + encodeURIComponent(end_uri);
        } else {
            dependencyURL = cq.baseURL + '/setup/ui/dependencyList.jsp?tableEnumOrId=' + objectId + '&setupid=CustomObjects';
        }
        return dependencyURL;
    }

    /**
     * This method syncs all the object with provided field dependencies. It also sets the piclist valuesets for recordtypes.
     */
    cq.syncFieldDepencies = function () {
        processedFieldsApiNameToLabelMap = new Map();
        unSuccessFields = []; // clear initially
        if (selectedObjectApiName == VAL_BLANK) {
            jQuery(cq.REQUIRED_ERR_SELECTOR).show();
        } else {                                            
            cq.showLoading(cq.WAIT_MESSAGE);

            // read the metadata of the selected object to process dependency
            cq.conn.metadata.read( CUSTOM_OBJECT, [selectedObjectApiName], function(err, metadata) {
                if ( err ) {
                    console.error(err);
                    cq.showFinalResult( VAL_FALSE );
                }
                if ( metadata != null && !jQuery.isEmptyObject(metadata)) {
                    var isValueStored = cq.storeValueSet(metadata);

                    if (isValueStored === VAL_TRUE) {
                        cq.processData( function(result) {
                            if ( result == RESULT_SUCCESS ) {
                                cq.showFinalResult(VAL_TRUE, selectedObjectLabel);
                            } else {
                                cq.showFinalResult( VAL_FALSE );
                            }
                        });
                    }
                    
                } else {
                    cq.showFinalResult( VAL_FALSE );
                }
            });
        } 
    }

    /**
     * This method is used to show the final success or failure result and also set the picklist global valueset for record types too.
     * @param isSuccess(boolean) confirms whether the result is succeeded or failed
     * @param sourceObject (String) name of source object through which dependency is synchronized.
     */
    cq.showFinalResult = function (isSuccess, sourceObject) {
        var message = '',
            MessageElement = jQuery(cq.MESSAGE_SELECTOR);

        if(isSuccess === VAL_TRUE && sourceObject && processedFieldsApiNameToLabelMap.size > 0) {

            // if synchronization has failed for some fields then show error message
            if (unSuccessFields.length > 0) {
                message = 'Synchronization for some or all custom fields has failed. ' + (unSuccessFields.length > 10 ? 'First 10 failed custom fields along with their object name are : ' : 'Failed custom fields along with their object name are : ');
                MessageElement.text(message);

                // add line break
                var lineBreak = jQuery('<br/><br/>');
                lineBreak.appendTo(MessageElement);

                // show failed custom fields in tabular form with object name in one column and field name in another column
                var custom_table = jQuery('<table></table>').addClass('errTable');
                var row = jQuery('<tr></tr>');
                var rowHeader = jQuery('<th></th>').addClass('tableHeader').text('Object Name');
                row.append(rowHeader);
                rowHeader = jQuery('<th></th>').addClass('tableHeader').text('Field Name');
                row.append(rowHeader);
                custom_table.append(row);
                // construct the table data
                for (var i in unSuccessFields) {
                    if (i < 10) { // just show maximum of 10 error occurred fields, showing more fields may over lap the view.
                        var dataArray = unSuccessFields[i].split('.'); // eg. unSuccessFields[i] = compliancequest__SQX_Audit__c.compliancequest__Org_Site__c
                        row = jQuery('<tr></tr>');
                        var rowDataObject = jQuery('<td></td>').addClass('tableData').text(dataArray[0]); // object name
                        var rowDataField = jQuery('<td></td>').addClass('tableData').text(dataArray[1]); // field name
                        row.append(rowDataObject);
                        row.append(rowDataField);

                        custom_table.append(row);
                    }
                }
                custom_table.appendTo(MessageElement);

                // log the failed result
                console.log('All fields for which synchronization has failed : ', unSuccessFields);
                // hide the loading bar
                cq.hideLoading();

            } else {
                message = 'Fields dependencies have been successfully synchronized using ' + sourceObject + ' object  for following fields :';
                MessageElement.text(message); // clear old message if any and then append other required messages
                for (var [key, value] of processedFieldsApiNameToLabelMap) {
                    console.log('Processed fields:', value);
                    var elem = jQuery('<li class="boldText"></li>');
                    elem.text(value);
                    elem.appendTo(MessageElement);
                }

                // if dependency is set properly then set record types too
                cq.setPickListValueSetForAllRecordTypes(function(result) {
                    if (result == RESULT_ERROR) {
                        
                        jQuery(cq.MESSAGE_SELECTOR).append('But error occurred while setting picklist values for record types.');
                    } 
                    // hide the loading bar
                    cq.hideLoading();
                    
                });
            }
            
        } else {
            cq.hideLoading();
            message = 'Failed while synchronizing field dependencies.Dependency fields from source object are :'
            MessageElement.text(message); // clear old message if any and then append other required messages.
            
            valueSetNameToValueSetMapForDependency.forEach(function(value, key){
                console.log('Dependent fields from source object:', key);
                var elem = jQuery('<li class="boldText"></li>');
                elem.text(key);
                elem.appendTo(MessageElement);
            });
            
        }
    }

    /**
     * This method stores the valueSet of dependency fields i.e. both controlling and dependent fields (eg. div, bu, region, site, etc)
     * @param metadata metadata of selected object which has already configured field dependencies
     * note : valueset only exists for dependent pick list field not for controlling field
     */
    cq.storeValueSet = function(metadata) {

        valueSetNameToValueSetMapForDependency = new Map();
        fieldFullNameToValueSetMapForRecordTypes = new Map(); 
        globalValueSets = new Set();
        var allContrllingFields = [];

        for (var i in metadata.fields) {
            if (metadata.fields[i].valueSet && metadata.fields[i].valueSet.valueSetName && metadata.fields[i].valueSet.controllingField) {
                valueSetNameToValueSetMapForDependency.set(metadata.fields[i].valueSet.valueSetName, metadata.fields[i].valueSet);
                fieldFullNameToValueSetMapForRecordTypes.set(metadata.fields[i].fullName, metadata.fields[i].valueSet); // only holds valueset for dependent fields
                globalValueSets.add(metadata.fields[i].valueSet.valueSetName); // only holds valueSetName of dependent fields but not of controlling field 
                allContrllingFields.push(metadata.fields[i].valueSet.controllingField); // to search controlling field for record type, as we cannot get the valueSetName for controlling field here (e.g. Org_Division__c)                
            }
        }

        // again traverse in metadata.fields array and search for controlling field and store it's valueSet too.
        for (var i in metadata.fields) {
            for (var j in allContrllingFields) {
                if (allContrllingFields[j] == metadata.fields[i].fullName) {
                    globalValueSets.add(metadata.fields[i].valueSet.valueSetName); // add the valueSetName for controlling field too
                    fieldFullNameToValueSetMapForRecordTypes.set(metadata.fields[i].fullName, metadata.fields[i].valueSet); // Org_Division__c => valueSet
                    break;
                }
            }
        }

        // show error when the source object has no any dependencies defined already.
        if (valueSetNameToValueSetMapForDependency.getSize() == 0) {
            cq.hideLoading();
            jQuery(cq.MESSAGE_SELECTOR).text('The selected source object has no dependency. Please create dependency first and try again');
            return VAL_FALSE;
        }
        return VAL_TRUE;
    }

    /**
     * This method prcesses the data in maximum pull size of 10 as the maximum limit of data for any operations(read,update,etc) in jsforce is 10
     * @param callback callback function to get the operation status
     */
    cq.processData = function(callback) {
        var tempAllObj = cq.allAvailableObjectsToProcess.slice(); // copy original object's array to process further
        // remove the selected object from the arary as it does not need to be processed
        tempAllObj.forEach(function(item, index, object) {
            if (item.name === selectedObjectApiName) {
                object.splice(index, 1);
            }
        });

        // make array of all picklist fields for all other objects. 
        var fieldApiNames = [];
        fieldApiNameToLabelMap = new Map(); // instantiate map

        for ( var i in tempAllObj ) {
            
            // making map of api name to its label, this will be use full to show final result in the label form
            var fieldApiNameToLabel = JSON.parse(tempAllObj[i].fieldApiNameToLabel);
            for (var j in fieldApiNameToLabel) {
                fieldApiNameToLabelMap.set(fieldApiNameToLabel[j].fieldApiName, fieldApiNameToLabel[j].fieldLabel);
            }

            var tempFieldApiNames  = JSON.parse(tempAllObj[i].fieldApiNames); // need to parse since is json string
            for ( var j in tempFieldApiNames ) {
                fieldApiNames.push(tempFieldApiNames[j]);
            }
        }
        
        cq.readAndsetValueSet(fieldApiNames, function(result) {
            if ( result == RESULT_SUCCESS ) {
                callback(result);
            } else {
                callback(result);
            }
        });
    }

    /**
     * This method reads all metadata and set the valueSet for all remaining objects
     * Note : readAndSetValueset() method is being called recursively in batch of batch size 10 as jsforce's max limitation.
     * @param fieldApiNames array of custom field api name
     *        if true means synchronization is based on setup table else based on selected object
     * @param callback callback function to get the operation status
     */
    cq.readAndsetValueSet = function(fieldApiNames, callback) {
        var returnVal,
            fieldsToRead = fieldApiNames.slice(0,JSFORCE_MAX_BATCH_SIZE); // taking only specified size of data for processing
        cq.conn.metadata.read( CUSTOM_FIELD, fieldsToRead, function(err, metadata) {
            if (err) {
                console.error(err);
                callback(err);
            } 
            if ( metadata != null) {
                if ( Array.isArray(metadata) ) { // ie if the metadata is array of object
                    metadata.forEach(function(fieldObj) {
                        returnVal = cq.setValueSet(fieldObj);
                    });
                } else {
                    returnVal = cq.setValueSet(metadata);
                }

                if (returnVal) {
                    for (var i = 0; i < JSFORCE_MAX_BATCH_SIZE; i++) {
                        // after sending each array elements in batch, remove them each time so that no same record will get processed again for same transaction.
                        fieldApiNames.shift();
                    }
                    if (processedFieldsApiNameToLabelMap.size > 0) {
                        cq.updateSchema(metadata, function(result){
                            if(result = RESULT_SUCCESS) {
                                
                                if ( fieldApiNames.length > 0 ) {
                                    cq.readAndsetValueSet(fieldApiNames, callback ); // recursive call
                                } else {
                                    callback(RESULT_SUCCESS);
                                }
                            } else {
                                callback(result);
                            }
                       });
                    } else {
                        if ( fieldApiNames.length > 0 ) {
                            cq.readAndsetValueSet(fieldApiNames, callback ); // recursive call
                        } else {
                            callback(RESULT_ERROR);
                        }
                    }
                } else {
                    callback(RESULT_ERROR);
                }
                
            }
        });
    }

    /**
     * This method to set the valueSet for all remaining objects
     * @param fieldObj all Custom field's schema which need to be synchronized
     *        if true means synchronization is based on setup table else based on selected object
     * @return true or false based on success or failure to set valueSet
     */
    cq.setValueSet = function(fieldObj) {
        var returnVal;
        //note : in some case the returned metadata might not be null but it might be an empty object.
        if( jQuery.isEmptyObject(fieldObj) ) {
            console.error(fieldObj);
            returnVal = VAL_FALSE;
        } else {

            // take value set for other given fieldObj if the valueSetName of fieldObj matches the key in valueSetNameToValueSetMapForDependency
            if (fieldObj.valueSet && fieldObj.valueSet.valueSetName) {
                var valueSet = valueSetNameToValueSetMapForDependency.get(fieldObj.valueSet.valueSetName); // fieldObj.valueSet.valueSetName = 'compliancequest__Org_Division__c' for Org_Business_Unit__c field
                if (valueSet) {
                    if (fieldObj.valueSet.controllingField) { // only process those pick list fields that have initial dependency defined
                        fieldObj.valueSet.valueSettings = valueSet.valueSettings;
                        processedFieldsApiNameToLabelMap.set(fieldObj.fullName.split('.').pop().toLocaleLowerCase(), fieldObj.label); // this attempt will add dependent fields map
                        if (fieldObj.valueSet.controllingField) {
                            processedFieldsApiNameToLabelMap.set(fieldObj.valueSet.controllingField.toLocaleLowerCase(), fieldApiNameToLabelMap.get(fieldObj.valueSet.controllingField)); // this attempt will add controlling fields map
                        }   
                    } else {
                        // store all the fields which don't have initial dependency set (at least empty dependency)
                        unSuccessFields.push(fieldObj.fullName);
                    }         
                }
            }
            returnVal = VAL_TRUE;
        }
        return returnVal;
    }

    /**
     * This method updates the remaining objects with set field dependencies
     * @param fieldMetadata all field's schema which need to be synchronized
     * @param callback callback function to get the operation status
     */
    cq.updateSchema = function(fieldMetadata, callback){
        // update the objects
        cq.conn.metadata.update( CUSTOM_FIELD, fieldMetadata, function(err,results) {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                if ( Array.isArray( results ) && results.length > 0 ) {
                    for ( var i=0; i < results.length; i++ ) {
                        cq.showResultInConsole(results[i]);
                    }
                } else if (!jQuery.isEmptyObject(results)) {
                    cq.showResultInConsole(results);
                }
                callback(RESULT_SUCCESS);
            }
        });
    }

    /**
     * This method shows the result of field update in the console and also captures all the unsuccessful fields
     * @param result{object} result of field update
     */
    cq.showResultInConsole = function(result) {
        var fieldName = result.fullName.split('.').pop().toLocaleLowerCase(); // e.g. result.fullName = compliancequest__SQX_Audit__c.compliancequest__Org_Business_Unit__c
        if ( result.success) {
            if(processedFieldsApiNameToLabelMap.get(fieldName)) {
                console.log('success ? : ' + result.success);
                console.log('fullName : ' + result.fullName);
            }
        } else {
            if(processedFieldsApiNameToLabelMap.get(fieldName)) {
                unSuccessFields.push(result.fullName);
                console.log('success ? : ' + result.success);
                console.log('fullName : ' + result.fullName);
                console.log(result);
            }
        }
    }

    /**
     * This method sets picklist values for record types
     * @param callback callback function to get the operation status
     * 
     * Note internal structure of GlobalValueSet, eg. for SQX_Division GlobalValueSet : 
        {
            "fullName": "compliancequest__SQX_Division",
            "customValue": [{
                    "fullName": "Division 1",
                    "default": "false",
                    "label": "Division 1"
                }, {
                    "fullName": "Division 2",
                    "default": "false",
                    "label": "Division 2"
                }, {
                    "fullName": "Division 3",
                    "default": "false",
                    "label": "Division 3"
                }, {
                    "fullName": "Division 4",
                    "default": "false",
                    "label": "Division 4"
                },{
                    "fullName": "Division 5",
                    "default": "false",
                    "label": "Division 5"
                }, {
                    "fullName": "Sample Division",
                    "default": "false",
                    "isActive": "false",
                    "label": "Sample Division"
                }
            ],
            "description": "This picklist contains the list of divisions that are valid for the organization.",
            "masterLabel": "Division",
            "sorted": "false"
        }
     */
    cq.setPickListValueSetForAllRecordTypes = function(callback) {

        // valuesMap(<key ==> array[{key : value}]>) map that contains globalvaluest name to its corresponding customvalues array
        var valuesMap = new Map();
        
        cq.conn.metadata.read(GLOBAL_VALUE_SET, Array.from(globalValueSets), function( err, metadata) { // globalValueSets is set so making array
            if (err) {
                cq.showFinalResult( VAL_FALSE );
            } else {
                if ( metadata != null ) {
                    for(var idx in metadata ) {
                        var obj = metadata[idx];

                        if ( Array.isArray( obj.customValue ) ) {
                            var valuesArray = [];
                            for (var jdx in obj.customValue) {
                                if (obj.customValue[jdx].isActive == null) {
                                    valuesArray.push({fullName : obj.customValue[jdx].fullName, default : VAL_FALSE});
                                }
                            }
                            valuesMap.set(obj.fullName, valuesArray);

                        } else if ( !Array.isArray( obj.customValue ) && !jQuery.isEmptyObject( obj.customValue ) && ! obj.isActive == VAL_FALSE) {
                            
                            valuesMap.set(obj.fullName, {fullName : obj.customValue.fullName, default : VAL_FALSE});
                        }
                    }

                    // call method to update data 
                    cq.updateRecordTypes(cq.allAvailableObjectsToProcess.slice(), valuesMap, function(result) {
                        if (result == RESULT_SUCCESS) {
                            callback(RESULT_SUCCESS);
                        } else {
                            callback(RESULT_ERROR);
                        }
                    });
                }
            }
        });
    }

    /**
     * This method updates the remaining objects with set field dependencies
     * @param objectsToProcess all objects
     * @param valuesMap(<key ==> array[{key : value}]>) map that contains globalvaluest name to its corresponding customvalues array
     * @param callback callback function to get the operation status
     */
    cq.updateRecordTypes = function (objectsToProcess, valuesMap, callback) {
        var tempObjectsToProcess = objectsToProcess.slice(0,1); // taking one at time
    
        cq.conn.metadata.read(CUSTOM_OBJECT, tempObjectsToProcess[0].name, function( err, result) {
            if (err) {
                console.error(err);
                cq.showFinalResult( VAL_FALSE );
            } else {
                
                if (result != null && result.recordTypes != null) {
                    if ( Array.isArray( result.recordTypes ) ) {
                        for (var j in result.recordTypes) {

                            var recordTypes = result.recordTypes[j];
                            recordTypes.fullName = tempObjectsToProcess[0].name + '.' + recordTypes.fullName;

                            if (recordTypes.picklistValues && Array.isArray(recordTypes.picklistValues)) {

                                for (var k in recordTypes.picklistValues) {

                                    cq.setPicklistValuesForRecordType(recordTypes.picklistValues[k], valuesMap);
                                }

                            } else if (recordTypes.picklistValues && !Array.isArray(recordTypes.picklistValues)) {

                                cq.setPicklistValuesForRecordType(recordTypes.picklistValues, valuesMap);
                            }
                        }
                    } else if(!jQuery.isEmptyObject(result.recordTypes)) {

                        if (result.recordTypes.picklistValues && Array.isArray(result.recordTypes.picklistValues)) {

                            for (var k in result.recordTypes.picklistValues) {

                                cq.setPicklistValuesForRecordType(result.recordTypes.picklistValues[k], valuesMap);
                            }
                        } else if (result.recordTypes.picklistValues && !Array.isArray(result.recordTypes.picklistValues)) {
                            
                            cq.setPicklistValuesForRecordType(result.recordTypes.picklistValues, valuesMap);
                        }
                    }

                    // update the record types stored in result above
                    cq.conn.metadata.update(RECORD_TYPE, result.recordTypes, function(err, res) {
                        if (err) {
                            console.error(err);
                            callback(RESULT_ERROR);
                        } 
                        console.log('RecordType update result : ');
                        console.log(res);
                        objectsToProcess.shift(); // once the object gets processed remove it from array
                        if (objectsToProcess.length == 0) {
                            callback(RESULT_SUCCESS);
                        } else {
                            cq.updateRecordTypes(objectsToProcess, valuesMap, callback); // recursive call
                        }
                    });
                } else {
                    objectsToProcess.shift(); // once the object gets processed remove it from array
                    if (objectsToProcess.length == 0) {
                        callback(RESULT_SUCCESS);
                    } else {
                        cq.updateRecordTypes(objectsToProcess, valuesMap, callback); // recursive call
                    }
                }
            }
        });
    }

    /**
     * This method sets values of picklistValues of record types
     * @param picklistValues picklistValues for the given field of record types
     * @param valuesMap(<key ==> array[{key : value}]>) map that contains globalvaluest name to its corresponding customvalues array
     * 
     * Note : Internal structure of record types, for eg. RecordTypes of SQX_Finding__c object : 

            [{
                "fullName": "compliancequest__SQX_Finding__c.compliancequest__Audit_Finding",
                "active": "true",
                "description": "Audit finding means finding that is a result of an Audit.",
                "label": "Audit Finding",
                "picklistValues": [{
                        "picklist": "compliancequest__Org_Business_Unit__c",
                        "values": [{
                                "fullName": "Business Unit1",
                                "default": "false"
                            }, {
                                "fullName": "Business Unit2",
                                "default": "false"
                            }
                        ]
                    }, {
                        "picklist": "compliancequest__Org_Division__c",
                        "values": [{
                            "fullName": "Sample Division",
                            "default": "false"
                        }, {
                            "fullName": "Division1",
                            "default": "false"
                        }]
                    }, {
                        "picklist": "compliancequest__Org_Region__c",
                        "values": [{
                                "fullName": "Region1",
                                "default": "false"
                            }, {
                                "fullName": "Region2",
                                "default": "false"
                            }
                        ]
                    }, {
                        "picklist": "compliancequest__Org_Site__c",
                        "values": [{
                                "fullName": "Sample Site",
                                "default": "false"
                            }, {
                                "fullName": "Site1",
                                "default": "false"
                            }
                        ]
                    }
                ]
            },...]
     */
    cq.setPicklistValuesForRecordType = function(picklistValues, valuesMap) {

        var valueSet = fieldFullNameToValueSetMapForRecordTypes.get(picklistValues.picklist);
        if (valueSet) {
            var values = valuesMap.get(valueSet.valueSetName);
            if (values) {
                picklistValues.values = values;
            }
        }
    }

    return cq;
}( document, window.cq || {}));