/*jshint browser: true */
/*global window*/

window.cq = (function (cq, $) {
    "use strict";

    $('.cq-data-table').on('click', '.cq-requirement', function (e) {
        var sender = $(e.target),
            row;

        row = sender.closest('tr');
        row.find('.cq-refresher').prop('disabled', !sender.is(':checked'));
    });

    $('.cq-refresher.cq-initially-disabled').each(function (i, e) {
        $(e).prop('disabled', true);
    });


    return cq;
}(window.cq || {}, window.jQuery));