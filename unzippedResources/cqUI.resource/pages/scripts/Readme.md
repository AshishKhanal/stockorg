# JavaScript guidelines

This document provides basic guidelines when creating CQ related page level JavaScripts.

Note: Any page JavaScript is automatically combined with shared.js to form a *.min.js. Please consider adding common methods and functions to shared.js. Exchange of Id's and values of labels from VF page to JavaScript should be done through use of variables. Refer to current implementation(s) for how to exchange dynamic runtime values.

# Template

```
/*jshint browser: true */
/*global window document */

window.cq = (function (doc, cq) {
    "use strict";
    
    // all your methods must be in cq namespace and must be exported
    // using cq.xyz = function() {} type method assignment or
    // cq.Message = 'Hello World' type attribute assignment
    
    return cq;
}(document, window.cq || {}));
```