/*jshint browser: true */
/*global window document */

window.cq = (function (doc, cq) {
    "use strict";

    var APPROVER_INPUT_CONTAINER_SELECTOR = "cq-approver-input-container";

    /**
     * This method hides the input step field when job function is changed until
     * it is re-rendered by ajax call completion
     * @since 7.0
     * @story SQX-3218
     */
    cq.hideFields = function () {
        var usersLookup = doc.getElementById(APPROVER_INPUT_CONTAINER_SELECTOR);
        usersLookup.style.display = "none";
    };

    return cq;
}(document, window.cq || {}));
