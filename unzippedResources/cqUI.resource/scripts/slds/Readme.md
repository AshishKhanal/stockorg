SLDS Javascript
===

This folder contains both code and spec for JavaScript added for integrating SF's functionality such as lookup filter into kendo based pages.

In order to run the spec and verify the code please execute the following

```
$ npm install
$ npm test
```
