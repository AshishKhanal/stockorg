/**
 * Contains the logic for mocking or stubbing classes and functions for tests
 */
var MockDataView = function(){
    this.fields = [];
    this.addField = function(fieldName) {
        this.fields.push(fieldName);
    }
};
var jsdom = require('jsdom');
global.window = new jsdom.JSDOM("<html></html>").window;
var w = global.window;

w.UserContext = {language: 'eng'};
w.mainRecordType = 'Main Record';
w.mockServer = {};
w.mainRecord = {
    Id: 'mockrecord',
    Name: 'a',
    NonEditable: '123',
    CreatedDate: new Date(),
    attributes: {type: 'MockRecord'},
    fields: {
        Name: {editable: true},
        NonEditable: {editable: false},
        CreatedDate: {type: 'date', editable: true}
    },
    set: function(field, value) { global.window.mainRecord[field] = value;}
};
w.sqx = {
    common: {
        showLoading: function() {},
        hideLoading: function() {},
        SFConstants: {
            TYPE_REFERENCE: 'reference'
        },
        isSFID: function(id) {
            return id != null && id.length === 18;
        },
        Constants: {
            TYPE_DATE: 'date'
        }
    },
    remote: {
        createDataSourceForObject: function() {
            return new MockDataView();
        }
    },
    DataView: MockDataView
};
w.UIController = {
    invokeFlow: function(ns, name, jsonMainRecord, callback) {
        var mockServer,
            serverName = ns + '.' + name;
        if (ns && name && jsonMainRecord){
            mockServer = global.window.mockServer[serverName];
            if (!mockServer) {
                throw new Error("Mock server for invoke flow is not defined for " + serverName);
            }
            mockServer.call(null, ns, name, jsonMainRecord, callback);
        }
    }
};
w.SQXSchema = {
    'Main Record': {
        fields: {
            "AccountField": {
                "additionalInfo": {
                    "referenceTo": "Contact"
                }
            }
        }
    }
};
w.kendo = {
    bind: function() {},
    init: function() {}
};

w.jQuery = require('jquery');