require('./cqmocks.js');
var dateFormat = require('dateformat');

global.window.sqx.dataStore = {
    getChangeSet: function () {
        return {modified: [{Id: ''}]};
    }
};
global.window.kendo.toString = function(date, format) {
    return dateFormat(date, format);
}
var expect = require("chai").expect;
var invoke = require('../src/invoke.js');

describe("Invocable tests", function () {
    var w = global.window,
        serializedRecord = {},
        YEAR = 2010, MONTH = 1, DAY = 1;
    w.mockServer['cq.s'] = function(ns, name, mr, callback) {
        serializedRecord[ns + '.' + name] = JSON.parse(mr);
        callback(JSON.stringify({
            SObjValueJSON: JSON.stringify({
                'Name': 'b',
                'Id': '123'
            })
        }), {
            status: true
        });
    };
    w.mockServer['cq.date'] = function(ns, name, mr, callback) {
        serializedRecord[ns + '.' + name] = JSON.parse(mr);
        callback(JSON.stringify({
            SObjValueJSON: JSON.stringify({
                'Name': 'b',
                'Id': '123',
                'CreatedDate': new Date(YEAR, MONTH, DAY)
            })
        }), {
            status: true
        });
    };

    w.mockServer['cq.refSetter'] = function(ns, name, mr, callback) {
        serializedRecord[ns + '.' + name] = JSON.parse(mr);

        callback(JSON.stringify({
            SObjValueJSON: JSON.stringify({
                'Name': 'b',
                'Id': '123',
                'RefField1__c': "Id_For_RefField1",
                'RefField1__r': {
                    "Id": "Id_For_RefField1",
                    "Name": "RefField Name"
                }
            })
        }), {
            status: true
        });
    }

    it("Should invoke correctly", function () {
        w.cq.invokeFlow({values: {ns: 'cq', name: 's'}});
        expect(w.mainRecord.Name).to.be.eq('b');
        expect(w.mainRecord.Id).to.not.be.eq('123');
        expect(serializedRecord['cq.s'].CreatedDate).to.be.match(/\d{4}-\d{2}-\d{2}/);
    });
    
    it("Should shift the date correctly", function () {
        var ADDED_HOURS = 12
        w.cq.invokeFlow({values: {ns: 'cq', name: 'date'}});
        expect(w.mainRecord.CreatedDate.getTime()).to.be.eq(new Date(YEAR, MONTH, DAY, ADDED_HOURS, 00, 00).getTime());
    });

    it("Should set reference record in mainrecord if the referenced object's datasource is available", function() {

        var filterFn = function (filter) {};

        w.mainRecord.fields["RefField1__c"] = {
            "type": "reference",
            "editable": true
        };

        w.mainRecord["RefField1__c_Source"] = {
            "filter": filterFn
        };

        w.cq.invokeFlow({
            values: {
                ns: 'cq',
                name: 'refSetter'
            }
        });

        expect(w.mainRecord).to.include.keys("RefField1__r");
        expect(w.mainRecord.RefField1__r.Name).to.be.eq("RefField Name");
    });

    it("Should not set reference record in mainrecord if the referenced object's datasource is not available", function () {

        delete w.mainRecord["RefField1__r"];
        w.mainRecord["RefField1__c_Source"] = function() {};    // datasource that has not been read yet

        w.cq.invokeFlow({
            values: {
                ns: 'cq',
                name: 'refSetter'
            }
        });

        expect(w.mainRecord).to.not.include.keys("RefField1__r");
    });
});