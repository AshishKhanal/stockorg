require('./cqmocks.js');
var expect = require("chai").expect;
require('../src/layouttransformer.js');

describe("Layout transformation", function () {
    var w = global.window;

    it("number of layout sections should match with layout data", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": true,
                "editHeading": false,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": [
                        {
                            "behavior": null,
                            "emptySpace": true,
                            "field": null
                        }
                    ]},
                    {"layoutItems": [
                        {
                            "behavior": null,
                            "emptySpace": true,
                            "field": null
                        }
                    ]}
                ]
            }
        ];

        recordSchema = {
            fields: {}
        };
        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        expect(layoutContainer.layoutSections).to.have.lengthOf(1);
        expect(layoutContainer.layoutSections[0].layoutItems).to.have.lengthOf(2);
        expect(layoutContainer.layoutSections[0].headerLabel).to.be.eq('Information');
    });

    it("header should be invisible if edit heading flag is turned off", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": true,
                "editHeading": false,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": []}
                ]
            }
        ];

        recordSchema = {
            fields: {}
        };
        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        expect(layoutContainer.layoutSections[0].showHeader).to.be.eq(false);
    });

    it("header should be visible if both edit heading and detail heading flag is turned on", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": true,
                "editHeading": true,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": []}
                ]
            }
        ];

        recordSchema = {
            fields: {}
        };
        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        expect(layoutContainer.layoutSections[0].showHeader).to.be.eq(true);
    });

    it("layout section columns should be based on layout columns count", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema,
            idx;

        layoutContainerData = [
            [
                {
                    "customLabel": false,
                    "detailHeading": true,
                    "editHeading": true,
                    "label": "Information",
                    "layoutColumns": [
                        {"layoutItems": []}
                    ]
                }
            ],
            [
                {
                    "customLabel": false,
                    "detailHeading": true,
                    "editHeading": true,
                    "label": "Information",
                    "layoutColumns": [
                        {"layoutItems": []},
                        {"layoutItems": []},
                        {"layoutItems": []}
                    ]
                }
            ]
        ];

        recordSchema = {
            fields: {}
        };

        for (idx = 0; idx < layoutContainerData.length; idx++) {
            layoutContainer = new w.cq.LayoutContainer(layoutContainerData[idx], {language: 'es'});
            layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
            expect(layoutContainer.layoutSections[0].cols).to.be.eq(layoutContainerData[idx][0].layoutColumns.length);
        }
    });

    it("layout section item should be based on layout items", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema,
            idx;

        layoutContainerData = [
            [
                {
                    "customLabel": false,
                    "detailHeading": true,
                    "editHeading": true,
                    "label": "Information",
                    "layoutColumns": [
                        {
                            "layoutItems": [
                                {
                                    "behavior": "Required",
                                    "emptySpace": null,
                                    "field": "SQX_Supplier_Deviation__c"
                                },
                                {
                                    "behavior": "Readonly",
                                    "emptySpace": null,
                                    "field": "SupplierDeviationRead"
                                }
                            ]
                        }
                    ]
                }
            ]
        ];

        recordSchema = {
            fields: {
                "SQX_Supplier_Deviation__c": {"type": 'reference', additionalInfo: {referenceTo: 'Acct-2312312'}},
                "SupplierDeviationRead": {"type": "boolean", additionalInfo: {}}
            }
        };

        for (idx = 0; idx < layoutContainerData.length; idx++) {
            layoutContainer = new w.cq.LayoutContainer(layoutContainerData[idx], {language: 'es'});
            layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
            expect(layoutContainer.layoutSections[0].cols).to.be.eq(layoutContainerData[idx][0].layoutColumns.length);
        }

        // ensure that reference fields has remote setup
        expect(w.sqx.remote).to.have.ownPropertyDescriptor('Acct-2312312', 'remote should automatically have a lookup');
    });

    it("layout section collapses when clicked", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema,
            layoutSectionDiv;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": true,
                "editHeading": true,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": []}
                ]
            }
        ];

        recordSchema = {
            fields: {}
        };

        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        layoutSectionDiv = containerDiv.find('.slds-section').eq(0);
        expect(layoutSectionDiv.hasClass('slds-is-open')).to.be.eq(true);

        // click and ensure the section is hidden.
        layoutSectionDiv.find('.slds-button').click();
        expect(layoutSectionDiv.hasClass('slds-is-open')).to.be.eq(false);
    });

    it("layout section header supports translation", function() {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema,
            spanishHeader;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": true,
                "editHeading": true,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": []}
                ],
                "translations": {
                    "es": "Información"
                }
            }
        ];

        recordSchema = {
            fields: {}
        };

        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        spanishHeader = layoutContainerData[0].translations.es;
        expect(layoutContainer.layoutSections[0].headerLabel).to.be.eq(spanishHeader);
        expect(containerDiv.find('.slds-section .cq-title').text()).to.be.eq(spanishHeader);
    });

    it("layout section isn't rendered when both edit and detail is off", function () {
        var layoutContainerData,
            layoutContainer,
            containerDiv = w.jQuery("<div />"),
            recordSchema,
            layoutSectionDiv;

        layoutContainerData = [
            {
                "customLabel": false,
                "detailHeading": false,
                "editHeading": false,
                "label": "Information",
                "layoutColumns": [
                    {"layoutItems": []}
                ]
            }
        ];

        recordSchema = {
            fields: {}
        };

        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {language: 'es'});
        layoutContainer.initialize(containerDiv, w.mainRecord, recordSchema);
        layoutSectionDiv = containerDiv.find('.slds-section');
        expect(layoutSectionDiv).to.be.have.lengthOf(0);
    });

    it("layout container should tell me if a given field exists in layout or not", function () {
        var layoutContainerData,
            layoutContainer;

        layoutContainerData = [{
            "customLabel": false,
            "detailHeading": true,
            "editHeading": false,
            "label": "Information",
            "layoutColumns": [{
                "layoutItems": [{
                    "behavior": null,
                    "emptySpace": true,
                    "field": "FIELD_A"
                }]
            }]
        }];


        layoutContainer = new w.cq.LayoutContainer(layoutContainerData, {
            language: 'es'
        });

        expect(layoutContainer.containsField("FIELD_A")).to.be.eq(true);
        expect(layoutContainer.containsField("FIELD_B")).to.be.eq(false);

    });
});