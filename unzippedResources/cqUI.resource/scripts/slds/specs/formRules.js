require('./cqmocks.js');
var expect = require("chai").expect;
var formRules = require('../src/formRules.js');

describe("Flow rule conversion tests", function () {
    /*
        [
            {
                "field": "<field name>",
                "operator": "eq|neq|changed|isnull|notnull",
                "value": "<value with eq or neq>",
                "setRequired": [
                <list of fields>
                ],
                "hideSection": [
                <list of section names to hide>
                ],
                "transfer": {
                    "sourcefield": [ "target field" ... "target field"],
                    ...
                    "sourcefield": [ "target field" ... "target field"],
                },
                "invoke": { "ns": "<namespace>", "name": "<flow name>" }
            }
        ]
    */
    describe("Condition/Operator Transformations", function () {
        var w = global.window;
        it("Should work correctly for equals operator", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "eq",
                    value: 10
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField === 10');
        });

        it("Should work correctly for not equals operator", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "neq",
                    value: 10
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField !== 10');
        });

        it("Should work correctly for isnull operator", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "isnull",
                    value: 10 //should ignore value
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField == null');
        });

        it("Should work correctly for notnull operator", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "notnull",
                    value: 10 //should ignore value
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField != null');
        });

        it("Should work correctly for ischanged operator", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "changed",
                    value: 10 //should ignore value
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField != null');
        });

        it("Should work correctly for not equals operator with string value", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "neq",
                    value: "Test"
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField !== \"Test\"');
        });

        it("Should work correctly for equals operator with string value", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "eq",
                    value: "Test"
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField === \"Test\"');
        });
    });

    describe("Set Required/Hide Section/Transfer/Invoke Transformations", function(){
        var w = global.window;
        it("Should create toggle actions for each 'set required' field", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "eq",
                    value: 10,
                    setRequired: ["Random", "India"]
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules).to.have.lengthOf(1);
            expect(transformedRules[0].objectName).to.be.eq(w.mainRecordType);
            expect(transformedRules[0].rule).to.have.lengthOf(1);
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField === 10');
            expect(transformedRules[0].rule[0].field).to.be.eq(flowExpression[0].field);
            expect(transformedRules[0].rule[0].toggleActions).to.have.lengthOf(flowExpression[0].setRequired.length);
            transformedRules[0].rule[0].toggleActions.forEach(function (toggleAction, index) {
                expect(toggleAction.targetField).to.be.eq(flowExpression[0].setRequired[index]);
            });
        });

        it("Should create toggle actions for each 'hide section' field in format hideSection.<fieldname>", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "eq",
                    value: 10,
                    hideSection: ["Random", "India"]
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules).to.have.lengthOf(1);
            expect(transformedRules[0].objectName).to.be.eq(w.mainRecordType);
            expect(transformedRules[0].rule).to.have.lengthOf(1);
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField === 10');
            expect(transformedRules[0].rule[0].field).to.be.eq(flowExpression[0].field);
            expect(transformedRules[0].rule[0].toggleActions).to.have.lengthOf(flowExpression[0].hideSection.length);
            transformedRules[0].rule[0].toggleActions.forEach(function (toggleAction, index) {
                expect(toggleAction.targetField).to.be.eq('hideSection.' + flowExpression[0].hideSection[index]);
            });
        });

        it("Should create actions if met with invoke for each 'invoke'", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "notnull",
                    invoke: {ns: "cq", name: 'as'}
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules).to.have.lengthOf(1);
            expect(transformedRules[0].objectName).to.be.eq(w.mainRecordType);
            expect(transformedRules[0].rule).to.have.lengthOf(1);
            expect(transformedRules[0].rule[0].condition).to.be.eq('AccountField != null');
            expect(transformedRules[0].rule[0].field).to.be.eq(flowExpression[0].field);
            expect(transformedRules[0].rule[0].actionsIfMet).to.have.lengthOf(1);
            transformedRules[0].rule[0].actionsIfMet.forEach(function (toggleAction, index) {
                expect(toggleAction.values.ns).to.be.eq(flowExpression[0].invoke.ns);
                expect(toggleAction.values.name).to.be.eq(flowExpression[0].invoke.name);
            });
        });


        it("Should create actions if met with transfer for each 'transfer'", function () {
            var flowExpression = [{
                    field: "AccountField",
                    operator: "notnull",
                    transfer: {"source1": ["target1", "target2"], "source2": ["target2"]}
                }],
                rulesTransformer = new w.cq.FormRulesTransformer([JSON.stringify(flowExpression)]),
                transformedRules,
                flowTransfer,
                idx,
                expectations;

            transformedRules = rulesTransformer.transform();
            expect(transformedRules).to.have.lengthOf(1);
            expect(rulesTransformer.transferMap).to.haveOwnProperty(flowExpression[0].field);
            flowTransfer = rulesTransformer.transferMap[flowExpression[0].field];
            expect(flowTransfer).to.have.lengthOf(3);
            expectations = [];
            for(var field in flowExpression[0].transfer) {
                for(idx = 0; idx < flowExpression[0].transfer[field].length; idx++) {
                    expectations.push({source: field, target: flowExpression[0].transfer[field][idx]});
                }
            }
            expect(flowTransfer).to.have.deep.members(expectations);
        });

    });

});