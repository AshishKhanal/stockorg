require('./cqmocks.js');
var expect = require("chai").expect;
require('../src/lookupfilter.js');

describe("Lookup filter to kendo filter conversion tests", function () {
    "use strict";
    var w = global.window;

    it("Passed only literal", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            
            "filterItems": [
                {field: 'Account.Name', operation: 'equals', value: 'Sample'}
            ],
            "booleanFilter": "1"
        };

        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter).to.not.have.property('logic');
        expect(transformedFilter).to.have.property('value', 'Sample');
        expect(transformedFilter).to.have.property('field', 'Name');
    });

    it("Should transform binary expression (1 OR (2 AND 3))", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

            lookupFilter = {
                booleanFilter: "(1 OR (2 AND 3))",
                filterItems: [
                    {field: 'Account.Contact', operation: 'equals', valueField: '$Source.Contact'},
                    {field: 'Account.Type', operation: 'equals', valueField: '$Source.Type'},
                    {field: 'Account.Age', operation: 'equals', valueField: '$Source.Age'},
                ]
            };

            record = {
                Contact: 'SuppContact',
                Type: 'Supplier',
                Age: 30,
                get: function(fieldName) {
                    return this[fieldName];
                }
            };

        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);

        expect(transformedFilter.logic).to.be.eq('or');
        expect(transformedFilter.filters).to.include.deep.members([
            {field: 'Contact', operator: 'eq', value: record.Contact},
            {logic: 'and', filters: [
                {field: 'Type', operator: 'eq', value: record.Type},
                {field: 'Age', operator: 'eq', value: record.Age}
            ]}
        ]);
    });

    it("Should transform binary expression 1 AND 2 OR 3", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

            lookupFilter = {
                booleanFilter: "1 AND 2 OR 3",
                filterItems: [
                    {field: 'Account.Contact', operation: 'equals', valueField: '$Source.Contact'},
                    {field: 'Account.Type', operation: 'equals', valueField: '$Source.Type'},
                    {field: 'Account.Age', operation: 'equals', valueField: '$Source.Age'},
                ]
            };

            record = {
                Contact: 'SuppContact',
                Type: 'Supplier',
                Age: 30,
                get: function(fieldName) {
                    return this[fieldName];
                }
            };

        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);

        expect(transformedFilter.logic).to.be.eq('or');
        expect(transformedFilter.filters).to.include.deep.members([
            {field: 'Age', operator: 'eq', value: record.Age},
            {logic: 'and', filters: [
                {field: 'Contact', operator: 'eq', value: record.Contact},
                {field: 'Type', operator: 'eq', value: record.Type}
                
            ]}
        ]);
    });

    it("Should convert simple static filter", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            filterItems: [
                {field: 'Account.Status', operation: 'equals', value: 'Open'}
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(1);
        expect(transformedFilter.filters[0]).to.include({field: 'Status', operator: 'eq', value: 'Open'});
    });

    it("Should use AND logic filter if boolean filter is missing", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            filterItems: [
                {
                    field: 'Account.Status',
                    operation: 'equals',
                    value: 'Open'
                },
                {
                    field: 'Account.Stage',
                    operation: 'equals',
                    value: 'Triage'
                }
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(2);
    });

    it("Should change RecordTypeId field into RecordType reference field", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            filterItems: [{
                    field: 'Account.RecordType.DeveloperName',
                    operation: 'equals',
                    value: 'ACC_RT'
                }
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(1);
        expect(transformedFilter.filters[0]).to.include({
            field: 'RecordTypeId',
            usv_function : 'reference',
            usv_param: 'DeveloperName',
            operator: 'eq',
            value: 'ACC_RT'
        });
    });

    it("Should convert comma separated list into OR filter", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            filterItems: [
                {field: 'Account.Status', operation: 'equals', value: 'Open,Ready,Closed'}
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(1);
        expect(transformedFilter.filters[0].logic).to.be.eq('or');
        expect(transformedFilter.filters[0].filters).to.have.deep.members([
            {field: 'Status', operator: 'eq', value: 'Open'},
            {field: 'Status', operator: 'eq', value: 'Ready'},
            {field: 'Status', operator: 'eq', value: 'Closed'}
        ]);
    });

    it("Should convert complex filter", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            booleanFilter: "1 OR ( 2 AND 3)",
            filterItems: [
                {field: 'Account.Status', operation: 'equals', value: 'Open,Ready,Closed'},
                {field: 'Account.Name', operation: 'equals', value: 'Open'},
                {field: 'Account.Person', operation: 'equals', value: '123'},
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.filters[0].logic).to.be.eq('or');
        expect(transformedFilter.filters[0].filters).to.have.deep.members([
            {field: 'Status', operator: 'eq', value: 'Open'},
            {field: 'Status', operator: 'eq', value: 'Ready'},
            {field: 'Status', operator: 'eq', value: 'Closed'}
        ]);
        expect(transformedFilter.filters[1].logic).to.be.eq('and');
        expect(transformedFilter.filters[1].filters).to.have.deep.members([
            {field: 'Name', operator: 'eq', value: 'Open'},
            {field: 'Person', operator: 'eq', value: '123'}
        ]);
    });

    it("Should convert dynamic filter", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

        lookupFilter = {
            booleanFilter: "1 OR 2 AND 3",
            filterItems: [
                {field: 'Account.Status', operation: 'equals', valueField: '$Source.Name'},
                {field: 'Account.Name', operation: 'equals', valueField: '$Source.Title'},
                {field: 'Account.Age', operation: 'equals', valueField: '$Source.Age'},
            ]
        };
        record = {
            Name: 'hisuyP',
            Title: 'uedibS',
            Age: 10,
            get: function(fieldName) {
                return this[fieldName];
            }
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);

        expect(transformedFilter.logic).to.be.eq('or');
        expect(transformedFilter.filters).to.include.deep.members([
            {field: 'Status', operator: 'eq', value: record.Name},
            {logic: 'and', filters: [
                {field: 'Name', operator: 'eq', value: record.Title},
                {field: 'Age', operator: 'eq', value: record.Age}
            ]}
        ]);
    });

    it("Should convert dynamic filter with field property containing dynamic filter", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

        lookupFilter = {
            booleanFilter: "1",
            filterItems: [
                {field: '$Source.Name', operation: 'equals', valueField: 'Account.Status'}
            ]
        };
        record = {
            Name: 'hisuyP',
            Title: 'uedibS',
            Age: 10,
            get: function(fieldName) {
                return this[fieldName];
            }
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);

        expect(transformedFilter).to.have.property('value', record.Name);
        expect(transformedFilter).to.have.property('field', 'Status');
    });

    it("Should convert dynamic filter with field property containing dynamic filter involving constant values", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

        lookupFilter = 
        {
            "filterItems": [{
                "value": "",
                "operation": "equals",
                "field": "$Source.compliancequest__Linked_Account_Type__c"
            }, {
                "valueField": "$Source.compliancequest__Linked_Account_Type__c",
                "operation": "equals",
                "field": "Account.RecordType.DeveloperName"
            }],
            "booleanFilter": "1 OR 2"
        }

        record = {
            Name: 'raphatdna',
            Title: 'mar',
            compliancequest__Linked_Account_Type__c: null,
            get: function (fieldName) {
                return this[fieldName];
            }
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        var tFilter = filterTransformer.transform();
        for(var int = 0; int < 4; int++){
            transformedFilter = tFilter.exec(record);

            expect(transformedFilter.logic).to.be.eq('or');
            expect(transformedFilter.filters).to.include.deep.members([
                {
                    field: 'Id',
                    operator: record.compliancequest__Linked_Account_Type__c  ? 'eq' : 'neq',
                    value: null
                },
                {
                    field: 'RecordTypeId',
                    operator: 'eq',
                    value: record.compliancequest__Linked_Account_Type__c,
                    usv_function : 'reference',
                    usv_param : 'DeveloperName'
                },
            ]);
            record.compliancequest__Linked_Account_Type__c = 'Supplier';
        }
    });

    it("Should remove spaces from comma separated list if any", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter;

        lookupFilter = {
            filterItems: [
                {field: 'Account.Status', operation: 'equals', value: 'Open, Ready, Closed'}
            ]
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec({});

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(1);
        expect(transformedFilter.filters[0].logic).to.be.eq('or');
        expect(transformedFilter.filters[0].filters).to.have.deep.members([
            {field: 'Status', operator: 'eq', value: 'Open'},
            {field: 'Status', operator: 'eq', value: 'Ready'},
            {field: 'Status', operator: 'eq', value: 'Closed'}
        ]);
    });

    it("Should transform binary expressions with braces like 1 OR (2 AND 3)", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

            lookupFilter = 
            {
                "filterItems": [{
                    "value": "",
                    "operation": "equals",
                    "field": "$Source.compliancequest__Linked_Account_Type__c"
                }, {
                    "valueField": "$Source.compliancequest__Linked_Account_Type__c",
                    "operation": "equals",
                    "field": "Account.RecordType.DeveloperName"
                }, {
                    "valueField": "$Source.Name",
                    "operation": "equals",
                    "field": "Account.Name"
                }],
                "booleanFilter": "1 OR (2 AND 3)"
            };
        record = {
            Name: 'Test',
            compliancequest__Linked_Account_Type__c: 'Supplier',
            get: function (fieldName) {
                return this[fieldName];
            }
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);
        
        expect(transformedFilter.logic).to.be.eq('or');
        expect(transformedFilter.filters).to.have.lengthOf(2);
        expect(transformedFilter.filters[1].logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.deep.members([
            {field: 'Id', operator: record.compliancequest__Linked_Account_Type__c  ? 'eq' : 'neq', value: null},
            {logic: 'and', filters: [{field: 'RecordTypeId', operator: 'eq', value: 'Supplier', 
            usv_function: 'reference', usv_param: 'DeveloperName', value: 'Supplier'},
            {field: 'Name', operator: 'eq', value: 'Test'}]}
        ]);
    });

    it("Should transform binary expressions with braces like 1 AND (2 AND 3) OR (4 AND 5) OR (6 AND 7) OR (8 AND 9)", function () {
        var lookupFilter,
            filterTransformer,
            transformedFilter,
            record;

        lookupFilter = 
            {
                "filterItems": [{
                    "valueField": "$Source.SupplierName",
                    "operation": "equals",
                    "field": "Task.RecordType.DeveloperName"
                }, {
                    "value": "CQ Perform Audit",
                    "operation": "equals",
                    "field": "$Source.RecordType.DeveloperName"
                }, {
                    "value": "Perform Audit",
                    "operation": "equals",
                    "field": "Task.compliancequest__Task_Type__c"
                },
                {
                    "value": "CQ Task",
                    "operation": "equals",
                    "field": "$Source.RecordType.DeveloperName"
                },
                {
                    "value": "Task",
                    "operation": "equals",
                    "field": "Task.compliancequest__Task_Type__c"
                },
                {
                    "value": "CQ Approval",
                    "operation": "equals",
                    "field": "$Source.RecordType.DeveloperName"
                },
                {
                    "value": "Approval",
                    "operation": "equals",
                    "field": "Task.compliancequest__Task_Type__c"
                },
                {
                    "value": "CQ Document Request",
                    "operation": "equals",
                    "field": "$Source.RecordType.DeveloperName"
                },
                {
                    "value": "Document Request",
                    "operation": "equals",
                    "field": "Task.compliancequest__Task_Type__c"
                }
            ],
                "booleanFilter": "1 AND ((2 AND 3) OR (4 AND 5) OR (6 AND 7) OR (8 AND 9))"
        };
        record = {
            SupplierName: 'Supplier',
            get: function (fieldName) {
                if(fieldName == 'RecordType.DeveloperName') {
                    return 'CQ Perform Audit';
                }
                return this[fieldName];
            }
        };
        filterTransformer = new w.cq.LookupFilterTransformer(lookupFilter);
        transformedFilter = filterTransformer.transform().exec(record);

        expect(transformedFilter.logic).to.be.eq('and');
        expect(transformedFilter.filters).to.have.lengthOf(2);
        expect(transformedFilter.filters[1].logic).to.be.eq('or');
        expect(transformedFilter.filters[1].filters[0].filters[0].filters).to.have.deep.members([
            {logic: 'and', filters: [
                {field: 'Id', operator: 'neq', value: null},
                {field: "compliancequest__Task_Type__c", operator: "eq", value: "Perform Audit"}
                ]
            },
            {logic: 'and', filters: [
                {field: 'Id', operator: 'eq', value: null},
                {field: "compliancequest__Task_Type__c", operator: "eq", value: "Task"}
                ]
            }
        ]);
        
        
    });
});