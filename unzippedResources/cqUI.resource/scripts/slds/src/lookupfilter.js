/**
 * This file contains logic for transforming SF's lookup filter to kendo's
 * datasource filter.
 */

(function(w) {
    "use strict";
    var cq = w.cq || {},
        LookupFilterTransformer,
        LookupFilter,
        LookupFilterKey = 1,
        jsepModule = {},
        jsep,
        jsepLoaded;

/* Load JSEP library for parsing Boolean expressions */
/* jsep v0.3.4 (http://jsep.from.so/) */
jsepLoaded = !function(e){"use strict";var w="Compound",C="MemberExpression",O="Literal",U=function(e,r){var t=new Error(e+" at character "+r);throw t.index=r,t.description=e,t},k={"-":!0,"!":!0,"~":!0,"+":!0},j={"||":1,"&&":2,"|":3,"^":4,"&":5,"==":6,"!=":6,"===":6,"!==":6,"<":7,">":7,"<=":7,">=":7,"<<":8,">>":8,">>>":8,"+":9,"-":9,"*":10,"/":10,"%":10},r=function(e){var r,t=0;for(var n in e)(r=n.length)>t&&e.hasOwnProperty(n)&&(t=r);return t},A=r(k),P=r(j),S={true:!0,false:!1,null:null},L=function(e){return j[e]||0},B=function(e,r,t){return{type:"||"===e||"&&"===e?"LogicalExpression":"BinaryExpression",operator:e,left:r,right:t}},M=function(e){return 48<=e&&e<=57},q=function(e){return 36===e||95===e||65<=e&&e<=90||97<=e&&e<=122||128<=e&&!j[String.fromCharCode(e)]},J=function(e){return 36===e||95===e||65<=e&&e<=90||97<=e&&e<=122||48<=e&&e<=57||128<=e&&!j[String.fromCharCode(e)]},t=function(n){for(var e,r,s=0,t=n.charAt,o=n.charCodeAt,i=function(e){return t.call(n,e)},a=function(e){return o.call(n,e)},u=n.length,p=function(){for(var e=a(s);32===e||9===e||10===e||13===e;)e=a(++s)},f=function(){var e,r,t=l();return p(),63!==a(s)?t:(s++,(e=f())||U("Expected expression",s),p(),58===a(s)?(s++,(r=f())||U("Expected expression",s),{type:"ConditionalExpression",test:t,consequent:e,alternate:r}):void U("Expected :",s))},c=function(){p();for(var e=n.substr(s,P),r=e.length;0<r;){if(j.hasOwnProperty(e)&&(!q(a(s))||s+e.length<n.length&&!J(a(s+e.length))))return s+=r,e;e=e.substr(0,--r)}return!1},l=function(){var e,r,t,n,o,i,a,u;if(i=h(),!(r=c()))return i;for(o={value:r,prec:L(r)},(a=h())||U("Expected expression after "+r,s),n=[i,o,a];(r=c())&&0!==(t=L(r));){for(o={value:r,prec:t};2<n.length&&t<=n[n.length-2].prec;)a=n.pop(),r=n.pop().value,i=n.pop(),e=B(r,i,a),n.push(e);(e=h())||U("Expected expression after "+r,s),n.push(o,e)}for(e=n[u=n.length-1];1<u;)e=B(n[u-1].value,n[u-2],e),u-=2;return e},h=function(){var e,r,t;if(p(),e=a(s),M(e)||46===e)return d();if(39===e||34===e)return v();if(91===e)return b();for(t=(r=n.substr(s,A)).length;0<t;){if(k.hasOwnProperty(r)&&(!q(a(s))||s+r.length<n.length&&!J(a(s+r.length))))return s+=t,{type:"UnaryExpression",operator:r,argument:h(),prefix:!0};r=r.substr(0,--t)}return!(!q(e)&&40!==e)&&g()},d=function(){for(var e,r,t="";M(a(s));)t+=i(s++);if(46===a(s))for(t+=i(s++);M(a(s));)t+=i(s++);if("e"===(e=i(s))||"E"===e){for(t+=i(s++),"+"!==(e=i(s))&&"-"!==e||(t+=i(s++));M(a(s));)t+=i(s++);M(a(s-1))||U("Expected exponent ("+t+i(s)+")",s)}return r=a(s),q(r)?U("Variable names cannot start with a number ("+t+i(s)+")",s):46===r&&U("Unexpected period",s),{type:O,value:parseFloat(t),raw:t}},v=function(){for(var e,r="",t=i(s++),n=!1;s<u;){if((e=i(s++))===t){n=!0;break}if("\\"===e)switch(e=i(s++)){case"n":r+="\n";break;case"r":r+="\r";break;case"t":r+="\t";break;case"b":r+="\b";break;case"f":r+="\f";break;case"v":r+="\v";break;default:r+=e}else r+=e}return n||U('Unclosed quote after "'+r+'"',s),{type:O,value:r,raw:t+r+t}},x=function(){var e,r=a(s),t=s;for(q(r)?s++:U("Unexpected "+i(s),s);s<u&&(r=a(s),J(r));)s++;return e=n.slice(t,s),S.hasOwnProperty(e)?{type:O,value:S[e],raw:e}:"this"===e?{type:"ThisExpression"}:{type:"Identifier",name:e}},y=function(e){for(var r,t,n=[],o=!1;s<u;){if(p(),(r=a(s))===e){o=!0,s++;break}44===r?s++:((t=f())&&t.type!==w||U("Expected comma",s),n.push(t))}return o||U("Expected "+String.fromCharCode(e),s),n},g=function(){var e,r;for(r=40===(e=a(s))?m():x(),p(),e=a(s);46===e||91===e||40===e;)s++,46===e?(p(),r={type:C,computed:!1,object:r,property:x()}):91===e?(r={type:C,computed:!0,object:r,property:f()},p(),93!==(e=a(s))&&U("Unclosed [",s),s++):40===e&&(r={type:"CallExpression",arguments:y(41),callee:r}),p(),e=a(s);return r},m=function(){s++;var e=f();if(p(),41===a(s))return s++,e;U("Unclosed (",s)},b=function(){return s++,{type:"ArrayExpression",elements:y(93)}},E=[];s<u;)59===(e=a(s))||44===e?s++:(r=f())?E.push(r):s<u&&U('Unexpected "'+i(s)+'"',s);return 1===E.length?E[0]:{type:w,body:E}};if(t.version="0.3.4",t.toString=function(){return"JavaScript Expression Parser (JSEP) v"+t.version},t.addUnaryOp=function(e){return A=Math.max(e.length,A),k[e]=!0,this},t.addBinaryOp=function(e,r){return P=Math.max(e.length,P),j[e]=r,this},t.addLiteral=function(e,r){return S[e]=r,this},t.removeUnaryOp=function(e){return delete k[e],e.length===A&&(A=r(k)),this},t.removeAllUnaryOps=function(){return k={},A=0,this},t.removeBinaryOp=function(e){return delete j[e],e.length===P&&(P=r(j)),this},t.removeAllBinaryOps=function(){return j={},P=0,this},t.removeLiteral=function(e){return delete S[e],this},t.removeAllLiterals=function(){return S={},this},"undefined"==typeof exports){var n=e.jsep;(e.jsep=t).noConflict=function(){return e.jsep===t&&(e.jsep=n),t}}else"undefined"!=typeof module&&module.exports?exports=module.exports=t:exports.parse=t}(jsepModule);

    jsep = (typeof(module) !== "undefined" && module.exports) || jsepModule.jsep;

    jsep.addBinaryOp('OR', 1);
    jsep.addBinaryOp('AND', 2);
    /**
     * Converts the lookup filter operation value into Kendo equivalent filter
     * @param {String} operation SF operation value in lookup filter
     */
    function getOperator(operation) {
        var mapped = 'eq';

        switch (operation) {
            case 'equals': mapped = 'eq'; break;
            case 'not equal to': mapped = 'neq';  break;
            case 'contains': mapped = 'contains'; break;
            case 'starts with': mapped = 'startswith'; break;
            default: throw new Error('Unsupported operation found ' + operation);
        }

        return mapped;
    }


    /**
     * Transforms a given lookup filter's filter item into Kendo compatible format
     * Example: Resolves $Source.* as current records field and Object.Field into field of object being queried
     * @param {Object} lookupFilter The lookup filter item that is to be transformed
     */
    function transformLookupFilter(lookupFilter) {
        // TODO : Support for $User, which might never be used
        var DYNAMIC_FILTER = /^\$Source\.(\w+(?:\.\w+)*)$/,
            STATIC_FILTER = /^(?:\w+)((?:\.\w+)+)$/,
            SOURCE_RECORDTYPE_ID = '$Source.RecordTypeId',
            SOURCE_RECORDTYPE_NAME = '$Source.RecordType.Name',
            filter = {},
            match,
            temp,
            fields,
            commaSeparatedValues,
            idx,
            tmpFilter;

        match = lookupFilter.field.match(DYNAMIC_FILTER);
        if(match !== null) {
            // swap field with valueField because field needs to statically refer to referring objects field
            // SF likes jumbled order, we don't
            temp = lookupFilter.valueField;
            lookupFilter.valueField = lookupFilter.field;
            lookupFilter.field = temp;
        }

        match = (lookupFilter.field && lookupFilter.field.match(STATIC_FILTER)) || null;
        if(match !== null) {
            fields = match[1].split('.');
            filter.field = fields[1];
            if(fields.length > 3) {
                throw new Error('Unsupported filter, only single level relationship is supported');
            } else if(fields.length === 3) {
                filter.usv_function = 'reference';
                filter.usv_param = fields[2];

                // our dynamic query class expects 'field' value to be field name and not relationship name
                // hence, converting relationship names to field names
                if(filter.field.endsWith("__r")) {
                    filter.field = filter.field.replace("__r","__c");
                } else {
                    filter.field = filter.field.concat("Id");
                }

            }

        }

        filter.operator = getOperator(lookupFilter.operation);
        filter.value = lookupFilter.value;

        if(filter.value && filter.value.indexOf(',') > 0) {
            //multivalued filter
            commaSeparatedValues = filter.value.split(',');
            tmpFilter = JSON.parse(JSON.stringify(filter));
            delete filter.field;
            delete filter.operator;
            delete filter.value;
            filter.logic = 'or';
            filter.filters = [];
            for(idx = 0; idx < commaSeparatedValues.length; idx++) {
                filter.filters.push(JSON.parse(JSON.stringify(tmpFilter)));
                filter.filters[idx].value = (commaSeparatedValues[idx] || '').trim();
            }
        } else if(lookupFilter.valueField) {
                if(lookupFilter.valueField === SOURCE_RECORDTYPE_ID) {
                    lookupFilter.valueField = SOURCE_RECORDTYPE_NAME;
                }

            match = lookupFilter.valueField.match(DYNAMIC_FILTER);
            if(match) {
                filter.dynamicValue = match[1];
                filter.isDynamic = true;
            }
        }

        return filter;
    }

    /**
     * Represents the converted format of SF's filter in kendo terms. A filter must be executed to get the actual value
     * @param filterTree the boolean expression tree to represent the lookup filter
     * @param filterMap the filter map that contains all the filter tree elements by Numeric index
     * @param filterElements the list of transformed lookup filter elements
     */
    LookupFilter = function(filterTree, filterMap, filterElements) {
        var idx;
        this.filterTree = filterTree;
        this.filterElements = [];
        this.filterMap = filterMap;
        this.identifier = 'lookupFilter' + LookupFilterKey
        LookupFilterKey++;

        for(idx = 0; idx < filterElements.length; idx++) {
            this.filterElements.push(transformLookupFilter(filterElements[idx]));
        }
    };

    /**
     * Returns a unique identifier for the filter, the filter is cached in lookup store using the identifier
     */
    LookupFilter.prototype.getIdentifier = function() {
        return this.identifier;
    }

    /**
     * Sets static filter by evaluating the condition specified in the filter element
     * If the condition turns out to be true, static filter used will be 'Id neq null' else 'Id eq null' if false
     * @param filterElement transformed lookup filter
     * @param fieldValue value in database for the field specified in the filter element
     */
    function setStaticFilter(filterElement, fieldValue) {
        var evalResult = undefined,
            FIELD_ID = 'Id',
            value;

        value = filterElement.value || null;
        if(filterElement.operator === 'eq') {
            evalResult = value === fieldValue;
        } else if(filterElement.operator === 'neq') {
            evalResult = value !== fieldValue;
        }

        if(evalResult !== undefined) {
            filterElement.field = FIELD_ID;
            filterElement.operator = evalResult === true ? 'neq' : 'eq';
            filterElement.value = null;
        }
    }

    /**
     * Executes the transformed filter with model values and returns a Kendo filter.
     * @param {Object} model The object model containing the values which will be replaced in the compiled filter
     */
    LookupFilter.prototype.exec = function(model) {
        var idx = 0,
            filter,
            filterElement,
            fieldValue;

        for(idx = 0; idx < this.filterElements.length; idx++) {
            filter = this.filterMap[(idx + 1)];
            filterElement = Object.assign({}, this.filterElements[idx]);    // clone such that the original filter remains intact
            if(filterElement.isDynamic) {
                fieldValue = model.get(filterElement.dynamicValue) || null;
                if (typeof(filterElement.value) === 'string') {
                    // e.g. $Source.Account_Type__c != ''
                    // evaluate the condition right away and set the filter
                    setStaticFilter(filterElement, fieldValue);
                } else {
                    filterElement.value = fieldValue;
                }
            }
            filter.field = filterElement.field;
            filter.operator = filterElement.operator;
            filter.value = filterElement.value;
            filter.usv_function = filterElement.usv_function;
            filter.usv_param = filterElement.usv_param;
            filter.logic = filterElement.logic;
            filter.filters = filterElement.filters;
        }

        filter = JSON.parse(JSON.stringify(this.filterTree));

        // TODO: reduce filter i.e. simplify

        return filter;
    }

    /**
     * Transformer engine that returns the kendo lookup filter for the SF lookup filter
     * @param {Object} sfFilter the filter present in SF metadata
     */
    LookupFilterTransformer = function (sfFilter) {
        this.sfFilter = sfFilter;
    }

    /**
     * Transforms the Boolean filter expression into expression tree
     * Example 1 AND (2 OR 3) is transformed into tree structure
     * {
     *  logic: 'and',
     *  filters: [
     *    {1}
     *    {logic: 'or',
     *       filters: [
     *          {2},
     *          {3}
     *       ]
     *    }
     *  ]
     * }
     *
     * The actual parsing of the boolean logic is done by JSEP
     */
    function constructFilterTree(expTree, filterElementsMap) {
        var filter = {
                'logic': null,
                filters: []
            },
            idx,
            LITERAL = 'Literal',
            BINARYEXPRESSION = 'BinaryExpression',
            expression,
            filterPlaceHolder;

        for(idx = 0; idx < expTree.length; idx++) {
            expression = expTree[idx];
            if(expression.type === LITERAL) {
                filterPlaceHolder = { };
                filterElementsMap[expression.value] = filterPlaceHolder;
                filter = filterPlaceHolder; // return final node of filter
            } else if (expression.type === BINARYEXPRESSION) {
                filter.logic = expression.operator.toLowerCase();
                filter.filters.push(constructFilterTree([expression.left], filterElementsMap));
                filter.filters.push(constructFilterTree([expression.right], filterElementsMap));
            }
        }

        return filter;
    }

    /**
     * Parses the filter logic and returns the filter tree
     * @param {String} combinationLogic The combination logic present in salesforce
     * @param {Object} filterElementsMap the elements map that will be used to
     */
    function parseFilterLogic(combinationLogic, filterElementsMap) {
        var expTree = jsep(combinationLogic);

        var tree = constructFilterTree(expTree.body || [expTree], filterElementsMap);
        return tree;
    }

    /**
     * Actual method to convert the SF lookup filter into kendo filter
     */
    LookupFilterTransformer.prototype.transform = function() {
        var kendoFilter,
            filterElementsMap = {},
            filterElement,
            idx;

        if(this.sfFilter.booleanFilter) {
            kendoFilter = parseFilterLogic(this.sfFilter.booleanFilter, filterElementsMap);
        } else if(this.sfFilter.filterItems.length >= 1) {
            kendoFilter = {filters: [], logic: 'and'};
            for(idx = 0; idx < this.sfFilter.filterItems.length; idx++) {
                filterElement = {};
                filterElementsMap[idx+1] = filterElement;
                kendoFilter.filters.push(filterElement);
            }
        } else {
            return;
        }


        var lookupFilter = new LookupFilter(kendoFilter,filterElementsMap, this.sfFilter.filterItems);
        cq.LookupFilterStore = cq.LookupFilterStore || {};
        cq.LookupFilterStore[lookupFilter.getIdentifier()] = lookupFilter;

        return lookupFilter;
    }

    cq.LookupFilterTransformer = LookupFilterTransformer;
    w.cq = cq;
}(window));