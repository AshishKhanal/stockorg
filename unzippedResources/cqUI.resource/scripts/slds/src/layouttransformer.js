
(function (w, common, remotes, DataView, jQ, kendo) {
    "use strict";

    var cq = w.cq || {},
        LAYOUT_TEMPLATE = '<div data-role="cqcompositefield" class="cq-layout-item" data-inline-edit="false" />',
        BLANK_ITEM_TEMPLATE = '<div class="cq-layout-item slds-form-element"></div>',
        BUTTON_TEMPLATE = "<button aria-controls='expando-unique-id' aria-expanded='true' data class='slds-button slds-section__title-action' formnovalidate='true'>" +
                "<svg class='slds-section__title-action-icon slds-button__icon slds-button__icon_left' aria-hidden='true'>" +
                "<use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#switch' /></svg><span class='slds-truncate cq-title'></span></button>",
        DEFAULT_FIELDS = ['Id', 'Name'],
        LayoutSectionItem,
        LayoutSection,
        LayoutContainer;

    /**
     * Represents individual layout item for each layout
     */
    LayoutSectionItem = function (item) {
        this.item = item;
    };

    /**
     * Adds a remote reference type if not present in the remotes collection.
     */
    function createRemoteForReferenceFields(field, mainRecordSchema) {
        var fieldDescribe = mainRecordSchema.fields[field],
            referenceTo;

        referenceTo = fieldDescribe && fieldDescribe.additionalInfo.referenceTo;

        if (referenceTo && fieldDescribe.type === common.SFConstants.TYPE_REFERENCE && (remotes[referenceTo] === undefined || !(remotes[referenceTo] instanceof DataView))) {
            remotes[referenceTo] = remotes.createDataSourceForObject(referenceTo, DEFAULT_FIELDS, undefined, undefined, undefined, true);
        }
    }

    /**
     * Method to draw item in the container
     */
    LayoutSectionItem.prototype.render = function (container, identifier, recordSchema) {
        var a;

        if (this.item.field !== null) {
            a = jQ(LAYOUT_TEMPLATE);
            a.attr("data-container", "mainRecord." + this.item.field);
        } else {
            a = jQ(BLANK_ITEM_TEMPLATE);
        }
        a.attr('id', identifier);
        a.appendTo(container);
        this.component = a;

        createRemoteForReferenceFields(this.item.field, recordSchema);
    };

    /**
     * calls the method after rendering is completed. This method is used for updating
     * readonly property of section item correctly, based on layout detail
     */
    LayoutSectionItem.prototype.postRender = function (record) {
        var fieldNames = ['isReadonly', 'isRequired'],
            behaviorMap = {'isReadonly': 'Readonly', 'isRequired': 'Required'},
            idx,
            fieldName;
        for (idx = 0; idx < fieldNames.length; idx++) {
            fieldName = fieldNames[idx];
            if (!record[fieldName]) {
                record.set(fieldName, {});
            }

            record.set(fieldName + '.' + this.item.field, this.item.behavior === behaviorMap[fieldName]);
        }
    };

    /**
     * Represents individual section of layout
     */
    LayoutSection = function (sectionData, userContext) {
        var colIdx,
            cols = sectionData.layoutColumns,
            allColumns = [],
            BLANK = { blankspace: true},
            maxColIdx = Math.max(cols[0].layoutItems.length,
                                (cols[1] && cols[1].layoutItems.length) || 0),
            numberOfCols = cols.length,
            columnSelector;

        for (colIdx = 0; colIdx < maxColIdx; colIdx++) {
            for (columnSelector = 0; columnSelector < numberOfCols; columnSelector++) {
                allColumns.push(new LayoutSectionItem(cols[columnSelector].layoutItems[colIdx] || BLANK, userContext));
            }
        }

        this.layoutItems = allColumns;
        this.name = sectionData.label;
        this.headerLabel = (sectionData.translations && sectionData.translations[userContext.language]) || sectionData.label;
        this.showHeader = sectionData.editHeading && sectionData.detailHeading;
        this.hidden = !sectionData.editHeading && !sectionData.detailHeading;
        this.cols = sectionData.layoutColumns.length;
    };

    /**
     * Render a layout section with layout items inside it
     */
    LayoutSection.prototype.render = function (container, identifier, recordSchema) {
        var layoutName = btoa(this.name).replace(/=/g, '');
        if (!this.hidden) {
            var sectionContainer = jQ("<div id='" + identifier + "' class='slds-section slds-is-open slds-size_1-of-1 cqSectionContent section1col' data-bind='invisible: hideSection." + layoutName + "' />"),
                sectionTitle,
                colClass = 'slds-size_' + (12 / this.cols) + '-of-12',
                itemContainer;
            if (this.showHeader) {
                sectionTitle = jQ(BUTTON_TEMPLATE);
                sectionTitle.appendTo(sectionContainer);
                sectionTitle.find('.cq-title').text(this.headerLabel);
                sectionContainer.find('.slds-button').click(function (e) {
                    sectionContainer.toggleClass('slds-is-open');
                    e.preventDefault();
                });
            }

            itemContainer = jQ('<div class="slds-section__content slds-grid slds-wrap"></div>');
            itemContainer.appendTo(sectionContainer);
            this.layoutItems.forEach(function(item, index) {
                item.render(itemContainer, identifier + '_' + (index + 1), recordSchema);
                itemContainer.find('.cq-layout-item').addClass(colClass);
            });
            sectionContainer.appendTo(container);
        }
    };

    /**
     * Invokes post render method after layout section is rendered
     */
    LayoutSection.prototype.postRender = function(record) {
        this.layoutItems.forEach(function(items) {
            items.postRender(record);
        });
    };

    /**
     * Represents the complete layout based on the layout data provided.
     */
    LayoutContainer = function(layoutData, userContext) {
        var idx,
            layoutSections = [];

        for(idx = 0; idx < layoutData.length; idx++) {
            layoutSections.push(new LayoutSection(layoutData[idx], userContext));
        }

        this.layoutSections = layoutSections;
    };

    /**
     * Draws the layout on the given container
     */
    LayoutContainer.prototype.initialize = function(container, record, recordSchema) {
        this.layoutSections.forEach(function(section, index) {
            section.render(container, 'section_' + (index + 1), recordSchema);
        });
        kendo.bind(jQ(".cqSectionContent"), record);
        kendo.init(container[0]);

        this.layoutSections.forEach(function(section) {
            section.postRender(record);
        });
    };

    /**
     * Returns true if the given field is present in the layout
     * @param field name of the field
     */
    LayoutContainer.prototype.containsField = function (field) {
        var idx, idy;

        for(idx = 0; idx < this.layoutSections.length; idx++) {
            for(idy = 0; idy < this.layoutSections[idx].layoutItems.length; idy++) {
                if(this.layoutSections[idx].layoutItems[idy].item.field === field) {
                    return true;
                }
            }
        }

        return false;
    }

    cq.LayoutContainer = LayoutContainer;

    w.cq = cq;
}(window, window.sqx && window.sqx.common, window.sqx && window.sqx.remote, window.sqx && window.sqx.DataView, window.jQuery, window.kendo));