/**
 * This file contains the logic to transform any Lightning UI based form rules to
 * classic kendo based rules
 *
 * Details of new form based rule can be found in
 * (https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/345440260/Lightning+Layout+Design+Configuration+Support)
 *
 */
(function (w, remotes, schema, DataView) {
    "use strict";
    var OPERATOR_IS_NULL = 'isnull',
        OPERATOR_NOT_IS_NULL = 'notnull',
        OPERATOR_CHANGED = 'changed',
        OPERATOR_EQUALS = 'eq',
        OPERATOR_NOT_EQUALS = 'neq',
        FLOW_RULE_INVOKE = 'invoke',
        FLOW_RULE_TRANSFER = 'transfer',
        ACTION_SET_REQUIRED = 'setrequired',
        ACTION_HIDE_SECTION = 'hidesection',
        jsonStringify = JSON.stringify,
        cq = w.cq || {};

    /**
     * Form Rule Operator encapsulates the rule to convert any flow operator 
     * to form rules
     */
    function FormRuleOperator(field, operator, value) {
        this.operator = operator;
        this.field = field;
        this.value = value;
    }

    /**
     * Converts the invocable's operator passed from flow to kendo form rules condition.
     */
    FormRuleOperator.prototype.getCondition = function () {
        var condition = this.field;

        switch (this.operator) {
            case OPERATOR_IS_NULL:
                condition += ' == null';
                break;
            case OPERATOR_NOT_IS_NULL:
                condition += ' != null';
                break;
            case OPERATOR_CHANGED:
                condition += ' != null';
                break;
            case OPERATOR_EQUALS:
                condition += ' === ' + jsonStringify.call(null, this.value);
                break;
            case OPERATOR_NOT_EQUALS:
            condition += ' !== ' + jsonStringify.call(null, this.value);
                break;
            default:
                throw new Error('Unsupported error ');
        }

        return condition;
    };

    /**
     * FormRule Action converts the flow rules actions too.
     */
    function FormRuleAction(field, action, values) {
        this.field = field;
        this.action = action.toLowerCase();
        this.values = values;
    }

    /**
     * Internal method to convert a transfer UI rule to transfer map based logic.
     */
    function addToTransferMap(field, values, transferMap) {
        var map,
            sourceField,
            referenceTo,
            idx;

        map = transferMap[field] || [];
        referenceTo = schema.fields[field].additionalInfo.referenceTo;
        if (!(remotes[referenceTo] instanceof DataView)) {
            remotes[referenceTo] = remotes.createDataSourceForObject(referenceTo, ['Id', 'Name'], undefined, undefined, undefined, true);
        }
        for (var sourceField in values) {
            for (idx = 0; idx < values[sourceField].length; idx++) {
                map.push({ source: sourceField, target: values[sourceField][idx] });
                remotes[referenceTo].addField(sourceField);
            }
        }
        transferMap[field] = map;
    }

    /**
     * Returns the action equivalent for flow rules
     */
    FormRuleAction.prototype.getActions = function (transferMap) {
        var actions = [],
            idx,
            action;

        if ((this.action === FLOW_RULE_INVOKE || this.action === FLOW_RULE_TRANSFER) && this.values) {
            if (this.action === FLOW_RULE_INVOKE) {
                actions.push({
                    action: this.action,
                    values: this.values
                });
            } else {
                addToTransferMap(this.field, this.values, transferMap)
            }
        } else {
            for (idx = 0; this.values && idx < this.values.length; idx++) {
                action = { 'action': this.action, targetNode: "" };
                if (this.action === ACTION_SET_REQUIRED) {
                    action.targetField = this.values[idx];
                } else if (this.action === ACTION_HIDE_SECTION) {
                    action.action = 'setvalue';
                    action.targetField = 'hideSection.' + btoa(this.values[idx]).replace(/=/g, '');
                }
                actions.push(action);
            }
        }
        return actions;
    }

    /**
     * Class to represent individual form rule.
     */
    function FormRule(rule) {
        this.rule = rule;
    }

    /**
     * Converts a UI rule to kendo form rule and returns it
     */
    FormRule.prototype.getTransformedRule = function (transferMap) {
        var newRule = { objectName: w.mainRecordType, rule: [] },
            ACTIONS = ['setRequired', 'hideSection', 'transfer', 'invoke'],
            ruleElement,
            idx,
            ruleAction;

        ruleElement = {
            toggleActions: [],
            actionsIfMet: [],
            condition: new FormRuleOperator(this.rule.field, this.rule.operator, this.rule.value).getCondition(),
            field: this.rule.field
        };
        newRule.rule.push(ruleElement);
        for (idx = 0; idx < ACTIONS.length; idx++) {
            ruleAction = new FormRuleAction(this.rule.field, ACTIONS[idx], this.rule[ACTIONS[idx]]);
            if (ACTIONS[idx] === 'invoke' || ACTIONS[idx] === 'transfer') {
                ruleElement.actionsIfMet = ruleElement.actionsIfMet.concat(ruleAction.getActions(transferMap));
            } else {
                ruleElement.toggleActions = ruleElement.toggleActions.concat(ruleAction.getActions(transferMap));
            }
        }

        return newRule;
    }

    /**
     * Encapsulates the core logic for transforming UI based form rules string to kendo
     * rules
     */
    var FormRulesTransformer = function (formRulesStringList) {
        var idx,
            rules = [];

        if (formRulesStringList && formRulesStringList.length) {
            for (idx = 0; idx < formRulesStringList.length; idx++) {
                rules = rules.concat(JSON.parse(formRulesStringList[idx]));
            }
        }
        this.rules = rules;
        this.transferMap = {};
    }

    /**
     * This method performs the actual transformation of UI rule to kendo rule
     * @returns returns the transformed kendo rule
     */
    FormRulesTransformer.prototype.transform = function () {
        var idx, rule,
            transformedRule = [];

        for (idx = 0; idx < this.rules.length; idx++) {
            rule = this.rules[idx];
            transformedRule.push(new FormRule(rule).getTransformedRule(this.transferMap));
        }

        return transformedRule;
    }

    cq.FormRulesTransformer = FormRulesTransformer;

    w.cq = cq;
}(window, window.sqx && window.sqx.remote, window.mainRecordType && window.SQXSchema && window.SQXSchema[window.mainRecordType], window.sqx && window.sqx.DataView));
