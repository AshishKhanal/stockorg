
/**
 * This file contains the logic for invoking a particular flow on the server side and then returning the
 * value
 */
(function (w, dataStore, common, UIController, kendo) {
    "use strict";
    var flowLock = false,
        EMPTY_JSON = '{}',
        ID_FIELD = 'Id',
        cq = w.cq || {},
        REFERENCE = 'reference',
        SOURCE = '_Source';

    /**
     * Returns the main record present in the page
     */
    function getMainRecord() {
        return w.mainRecord;
    }

    /**
     * Returns true if the record's field is editable.
     * Note: Id field are considered non-editable since it is assumed that they won't need any change
     */
    function isEditable(record, field) {
        return record.fields[field] && record.fields[field].editable === true && field !== ID_FIELD;
    }

    /**
     * Returns the object containing all the field values for a given main record object.
     * It will remove any cyclic properties and create a basic object, that can be serialized.
     */
    function getMainRecordFields() {
        var mainRecord = getMainRecord(),
            fields = mainRecord.fields || {},
            field,
            record = {},
            SF_DATE_FORMAT = 'yyyy-MM-dd';

        for (var fieldName in fields) {
            field = fields[fieldName];
            if (field && (field.editable || field.creatable)) {
                if(field.type === common.Constants.TYPE_DATE) {
                    // date field isn't present in JSON and needs to be serialized in a special format
                    record[fieldName] = kendo.toString(new Date(), SF_DATE_FORMAT);
                } else {
                    record[fieldName] = mainRecord[fieldName];
                }
            }
        }

        record.attributes = JSON.parse(JSON.stringify(mainRecord.attributes));
        record.Id = common.isSFID(record.Id) ? record.Id : '';

        return record;
    }

    /**
     * Invokes the flow applicable in CQ
     * @param {Object} act Contains the name and namespace of the flow to be invoked
     */
    cq.invokeFlow = function (act) {
        common.showLoading();
        var jsonMainRecord;

        jsonMainRecord = JSON.stringify(getMainRecordFields());

        if (!flowLock && jsonMainRecord !== EMPTY_JSON) {

            UIController.invokeFlow(act.values.ns, act.values.name, jsonMainRecord, function (result, response) {
                var mainRecord = getMainRecord(),
                    resultRecord;
                if (response.status === true && result) {
                    //TODO: add support for showing success/error/warning messages
                    resultRecord = JSON.parse(JSON.parse(result).SObjValueJSON);
                    flowLock = true;
                    for (var field in resultRecord) {
                        //since we are ignoring id field
                        if (isEditable(mainRecord, field)) {
                            if (mainRecord.fields[field] && mainRecord.fields[field].type === common.Constants.TYPE_DATE) {
                                //set hour time of date to 12'0 clock.so as to resolve date conversion 
                                //that happens when converting date from UTC to GMT and vice versa.
                                if(resultRecord[field] != null) {
                                    var dt = new Date(resultRecord[field]);
                                    dt.setHours(12);
                                    mainRecord.set(field, dt);
                                } else {mainRecord.set(field, '');}
                            } else {
                                mainRecord.set(field, resultRecord[field]);
                            }
                        }

                        // setting reference type field
                        if (mainRecord.fields[field] && mainRecord.fields[field].type === REFERENCE) {
                            var relationName = field.replace('__c','__r');
                            var dsName = field + SOURCE;

                            // in some cases, _Source field might not have been read/fetched if it is not present in the UI
                            // in such cases, _Source will be a function and filter will not have been defined
                            // we can avoid setting relationship field
                            if(mainRecord[dsName].filter) {
                                mainRecord[dsName].filter({field: 'Id', operator: 'eq', value: resultRecord[field]});
                                mainRecord.set(relationName, resultRecord[relationName] === undefined ? null : resultRecord[relationName]);
                            }
                        }
                    }
                    flowLock = false;
                } else {
                    common.error('Error occurred while invoking flow', response);
                }
                common.hideLoading();
            }, { escape: false });
        }
    }

    w.cq = cq;
}(window, window.sqx && window.sqx.dataStore,window.sqx && window.sqx.common, window.UIController, window.kendo));
