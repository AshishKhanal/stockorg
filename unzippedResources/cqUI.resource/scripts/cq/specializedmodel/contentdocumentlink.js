/*jshint browser: true */
/*global window document this arguments*/

var cq = (function (cq, CQModel, dataStore, sqx) {
    "use strict";
    var model = cq.model || {},
        contentDocLinkModel,
        CONTENT_DOCUMENT = 'ContentDocument',
        CONTENT_DOCUMENT_LINK = 'ContentDocumentLink',
        FIELD_TITLE = 'Title',
        FIELD_DESCRIPTION = 'Description',
        logger = sqx.common;

    contentDocLinkModel = CQModel.define({
        init: function () {
            var that = this,
                RECURSION_LOCK = false;
            CQModel.fn.init.apply(that, arguments);

            that.bind('change', function (e) {
                if (!RECURSION_LOCK) {
                    try {
                        RECURSION_LOCK = true;
                        var contentDocumentLinks = dataStore.getObjectsWithType(CONTENT_DOCUMENT_LINK),
                            contentDoc,
                            idx,
                            dirtyItem = e.sender;

                        contentDoc = dataStore.getObjectWithIdFor(CONTENT_DOCUMENT, dirtyItem.ContentDocumentId);
                        if (contentDoc) {
                            contentDoc.set(FIELD_TITLE, dirtyItem.ContentDocument[FIELD_TITLE]);
                            contentDoc.set(FIELD_DESCRIPTION, dirtyItem.ContentDocument[FIELD_DESCRIPTION]);

                            //update other content document links related to the content
                            for (idx = 0; idx < contentDocumentLinks.length; idx++) {
                                if (contentDocumentLinks[idx].ContentDocumentId === dirtyItem.ContentDocumentId) {
                                    contentDocumentLinks[idx].set('ContentDocument.Title', dirtyItem.ContentDocument.Title);
                                    contentDocumentLinks[idx].set('ContentDocument.Description', dirtyItem.ContentDocument.Description);
                                }
                            }
                        }
                        else {
                            logger.error('Content Document with ' + dirtyItem.ContentDocumentId + ' couldnt be found. Is something being missed ?');
                        }
                    } finally {
                        RECURSION_LOCK = false;
                    }
                }
            });
        }
    });

    model[CONTENT_DOCUMENT_LINK] = contentDocLinkModel;

    cq.model = model;
    return cq;
}(window.cq || {}, window.CQModel, window.sqx && window.sqx.dataStore, window.sqx));