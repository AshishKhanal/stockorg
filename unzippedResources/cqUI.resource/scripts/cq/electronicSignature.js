 /**
* @author Sagar Shrestha
* @date 2014/11/14
* @description this custom widget a provides a composite control that renders username and passowrd field according to option specified
*/
 (function ($) {
    var kendo = window.kendo,
                ui = kendo.ui,
                Widget = ui.Widget;
 
    var CQElectronicSignature = Widget.extend({
        
        options: {
            name: "CQElectronicSignature",
            currentUserDetails: null, //pass current user details
            allowESig: false, //if set to true, then only render the password field
            requireUserName: false, //if set to true, ask for input to username 
            currentUserDetailsVar: null //the variable that is storing the current user details, this field is not used if current user details is present
        },
         
        init: function (element, options) {
            if (typeof (options) == "undefined") options = this.options;
            // base call to widget initialization
            Widget.fn.init.call(this, element, options);
            this._create();
            
        },

        //if esig is disabled, renders username in read-only format
        //if esig is enabled and requireUserName is false, then username is render read-only format, and password is rendered as input 
        //if esig is enabled, and requireUserName is true, then username and password are rendered as input
        _create: function () {
            // cache a reference to this
            var that = this;
            
            var usernamefieldElement;

            that._containerItemUsername = $("<div class='formItem'></div>");
            that.element.append(that._containerItemUsername);
            
            var usernameText, passwordText,
                usernameRequiredMsg, passwordRequiredMsg,
                translator = sqx.translation; 

            usernameText = translator.get('esig.username');
            passwordText = translator.get('esig.password');
            
            usernameRequiredMsg = translator.get('common.requiredmsg', {fieldName : usernameText});
            passwordRequiredMsg = translator.get('common.requiredmsg', {fieldName : passwordText});
            
            var usernamelabelElement = $(kendo.template(that._templates.usernamelabel)({ text: usernameText }));
            $(usernamelabelElement).appendTo(that._containerItemUsername);
            
            //if esig is enabled and requires a username, render a input field for username, else render the current username details
            if(that.options.allowESig && that.options.requireUserName ){
                usernamefieldElement= $(kendo.template(that._templates.usernameTemplateWO)({usernameRequiredMsg : usernameRequiredMsg}));
            }
            else{
                //added support for using javascript variable or direct user details
                var username = that.options.currentUserDetails 
                                || kendo.getter(that.options.currentUserDetailsVar)(window);
                usernamefieldElement= $(kendo.template(that._templates.usernameTemplateRO)({username: username}));
            }
                
            $(usernamefieldElement).appendTo(that._containerItemUsername);
        
            //render password only if ESig is required
            if(that.options.allowESig)
            {
                that._containerItemPassword = $("<div class='formItem'></div>");
                that.element.append(that._containerItemPassword);
                var passwordlabelElement = $(kendo.template(that._templates.passwordLabel)({text: passwordText}));
                $(passwordlabelElement).appendTo(that._containerItemPassword);
                var passwordInputElement= $(kendo.template(that._templates.passwordTemplateWO)({passwordRequiredMsg : passwordRequiredMsg}));
                $(passwordInputElement).appendTo(that._containerItemPassword);
            }

        },


        _templates : {

            usernamelabel : "<div class='formLabel'><label for='username'>#: text #</label></div>",
            passwordLabel : "<div class='formLabel'><label for='password'>#: text #</label></div>",
            

            usernameTemplateRO: "<div class='formField' id='username'><div class='username readonlyElement' ><input class='esigUsername' autocomplete = 'off' type='text' readonly='readonly' value='#: username #' name='Username' style='width:90%; border:none' tabindex='-1'/></div>",

            passwordTemplateWO: "<div class='formField'><div class='editableElement cq-field-required password'><input class='cq-default-input' id='password' autocomplete = 'off' type='password' required='required' name='Password' style='width:30%' data-required-msg='#: passwordRequiredMsg #' /></div>",
            usernameTemplateWO: "<div class='formField'><div class='editableElement cq-field-required username'><input class='cq-default-input' id='username' autocomplete = 'off' type='text' required='required' name='Username' style='width:60%' data-required-msg='#: usernameRequiredMsg #' /></div>"
            
        }
 
    });
    // add the widget to the ui namespace so it's available
    ui.plugin(CQElectronicSignature);
 
})(jQuery);