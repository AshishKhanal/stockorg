/**
* Class to initialize default language related localizations
*/
(function(sqx, labels){
    var translations, DEFAULT_LANGUAGE = 'en_us';

    function replaceValues(text, replacers){
        
        if(replacers){
            for(var key in replacers){
                text = text.replace('{' + key + '}', replacers[key]);
            }
        }

        return text;
    }


    translations = {};

    translations._getKey = function(key){
        var path = key.split('.'),
            text, i,
            currentKey = labels;


        for(i = 0; i < path.length; i++){
            
            if(currentKey != null){
                currentKey = currentKey[path[i]];
            }
            else{
                currentKey = null;
                break;
            }
        }

        text = currentKey;

        if(!text){
            sqx.common.debugLog('No language key found for ' + key);
            text = undefined;
        }

        return text;

    };

    translations.get = function(key, values){
        return replaceValues(this._getKey(key), values);
    }

    sqx.translation = translations;
})(window.sqx || {}, (window.cq && window.cq.labels) || {} );