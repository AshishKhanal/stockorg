function initGridDetail(e) {
    var detailRow,
        detailRowSourceName,
        detailRowSource,
        toolbarTemplate,
        tabControl, tabOptions = {},
        childrenGrid,
        columns, options,
        detailTemplateId,
        detailTemplate,
        detailInit, editable, selectable, saveCallback, editCallback, boundElems;

    detailRow = e.detailRow,
    tabControl = detailRow.find(".tabStrip").eq(0); //todo:handle multiple tabs?
    boundElems = detailRow.find(".boundElem");
    if(boundElems.length > 0){
        boundElems.each(function(number,belement){kendo.bind(belement, e.data);})
        
    }
    if (tabControl) tabControl = kendo.bind(tabControl, e.data);
    var parentModel = e.data;
    childrenGrid = detailRow.find(".subGrid"); // expand to find all container classes?
    for (var i = 0; i < childrenGrid.length; i++) {
        detailTemplateId = '';
        detailTemplate = null;
        detailInit = null;
        columns = null;
        editable = null;
        saveCallback = null;
        editCallback = null;
        options = {};
        selectable = false;
        toolbarTemplate = null;

        grid = $(childrenGrid[i]);
        detailRowSourceName = grid.attr("data-relatedList");
        if ((detailRowSourceName) && e.data.relatedList) {
            detailRowSource = e.data.relatedList(detailRowSourceName) || e.data[detailRowSourceName]();
            if (detailRowSource != null) {
                if (grid.attr("data-columns")) {
                    var dataColumns = {dataColumns: [] };
                    dataColumns = kendo.parseOptions(grid[0], dataColumns);
                    columns = dataColumns.dataColumns// JSON.parse(grid.attr("data-columns"));
                } else {
                    columns = [];
                }

                detailTemplateId = grid.attr("data-detailTemplate");
                if (detailTemplateId != undefined) {
                    detailTemplate = kendo.template($("#" + detailTemplateId).html());
                    detailInit = initGridDetail;
                };
                if (grid.attr("data-options")) {
                    options = JSON.parse(grid.attr("data-options"));
                };

                if (grid.attr("data-save")) {
                    saveCallback = kendo.getter(grid.attr("data-save"))(window);
                }

                if (grid.attr("data-edit")) {
                    editCallback = kendo.getter(grid.attr("data-edit"))(window);
                }

                if (grid.attr("data-toolbar")) {
                    toolbarTemplate = JSON.parse(grid.attr("data-toolbar"));
                }

                options = $.extend({}, {
                    dataSource: detailRowSource,
                    columns: columns,
                    autoBind: true,
                    detailInit: detailInit,
                    detailTemplate: detailTemplate,
                    toolbar: toolbarTemplate || ["create"],
                    save: saveCallback,
                    edit: editCallback,
                    parentModel: parentModel
                }, options);
                grid.kendoCQGrid(options);
            } else {
                grid.text("Child Data Source Not Found for " + detailRowSourceName + " please verify relatedList names");
            }
        } 
        else { grid.text("detailRowSource " + detailRowSourceName + " defined but no relatedList exists in model"); }
    }
    var childlistviews = detailRow.find(".subListView");
    for (var i = 0; i < childlistviews.length; i++){
        var options = {},
        listview = $(childlistviews[i]);
        detailRowSourceName = listview.attr("data-relatedList");
        if ((detailRowSourceName) && e.data.relatedList) {
            detailRowSource = e.data.relatedList(detailRowSourceName) || e.data[detailRowSourceName]();
            if (detailRowSource != null) {
                detailTemplateId = listview.attr("data-detailTemplate");
                if (detailTemplateId != undefined) {
                    detailTemplate = kendo.template($("#" + detailTemplateId).html());
                    detailInit = initGridDetail;
                };
                options = $.extend({}, {
                        dataSource: detailRowSource,
                        template: detailTemplate
                    }, options);
                    listview.kendoListView(options);
            }
            else {
                grid.text("Child Data Source Not Found for " + detailRowSourceName + " please verify relatedList names");
            }
        }
        else { grid.text("detailRowSource " + detailRowSourceName + " defined but no relatedList exists in model"); }
    }
}