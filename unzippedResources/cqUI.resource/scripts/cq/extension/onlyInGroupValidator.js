/**
* validates all belonging to a group only in the element if group id is provided
* @groupId the id of the group to validate, if groupId is null then default validation is done.
* @return true if valid, else false
*/
kendo.ui.Validator.fn.validateOnlyInGroup = function (groupId){
    var INPUTSELECTOR = ":input:not(:button,[type=submit],[type=reset],[disabled],[readonly])",
            elements,
            isValid = true,
            that = this;

    //if valid group id is passed validate elements belonging to the group only
    if(groupId){
        elements = this.element.find(".cq-val-" + groupId + ' ' + INPUTSELECTOR); //select items in the group
        
        elements.each(function(index, element){
            isValid = isValid && that.validateInput(element);
        });
    }
    else{
        //else validate everything
        isValid = this.validate();
    }

    return isValid;
};