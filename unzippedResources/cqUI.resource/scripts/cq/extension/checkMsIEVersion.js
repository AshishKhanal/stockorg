/*** HACK to make Rich text editor give up focus when it is being used in a popup/window  *****/
function msieversion() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
            return (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    
    // If another browser, return -1
    return -1;
} 

if(msieversion() != -1){
    //intercepting close on window
    //so that if a window has a Rich text editor, its focus is lost
    //refer to bug SQX-476 as to why this hack is in place.
    kendo.ui.Window.fn.iehack_close = kendo.ui.Window.fn.close; 
    kendo.ui.Window.fn.close = function(){
        var retVal = this.iehack_close(), //original function has no arguments check kendo.window.js close method
            range = document.createRange(),
            sel = window.getSelection();
            
            sel.addRange(range);

        return retVal; 
    }  
}