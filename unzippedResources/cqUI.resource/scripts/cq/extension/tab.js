/*custom tab to show record title/count from child grid, each tabitem should have data-grid-sobject attribute to associate the correct grid. may need to change this to specific grid id */
(function ($) {
    ui = kendo.ui,
    tab = ui.TabStrip,
    widget = ui.Widget;

    var BOUNDED_TEMPLATE = kendo.template('#: label #<label data-bind="text: #: value #" class="cq-tab-counter"></label>');
    function tabGridEventHandler(e) {
        var tab = $(e.delegateTarget).data("kendoCQTabStrip"),
            tabItem,tabTitle,tabCount="",matchingTabFound=false,
            src = e.sender.options.dataSource.options.schema.model.fn, srcLabel;
        if (tab && (tab.options.showGridTitle ) && src && src.sObjectName) {
            srcLabel = (src.sObjectPluralLabel ? src.sObjectPluralLabel : src.sObjectLabel ? src.sObjectLabel : src.sObjectName);
            tab.setGridCount(src.sObjectName, srcLabel,e.sender.options.dataSource.total(), e);
        }
   }
    /**
    * @description: bind header with model
    * @param: headerList : list of tab or panel headers
    * @param: headerModel : model of mainRecord and and objectCount
    */
    function bindHeaderWithModel(headerList, headerModel){

        $.each(headerList, function(i, item){
            var title, objType, header, headerItem;

            item = $(item);
            title = item.attr("data-title") || '';
            objType = item.attr("data-grid-sobject");

            // By default try to use data-grid-sobject's if no data-title has been provided
            if(title.length == 0 ){
                try{
                    title = window.SQXSchema[objType].sObjectLabel;
                }
                catch(ex){
                    title = '';
                }
            }

            // > 6.1 Upgrade: Customers will have data-grid-sobject="Note,Attachment"
            //                This will automatically use NotesAndAttachments aggregate count
            if(objType === 'Note,Attachment') {
                objType = 'NotesAndAttachments';
            } else if(objType === 'Note,Attachment,ContentDocumentLink') {
                objType = 'NotesAndAttachmentsAndDocLinks';
            }else if(objType ===  'Note,ContentDocumentLink') {
                objType='NotesAndDocLinks';
            }

            header = item.find('.k-link');
            headerItem = header.children('.item');

            if(headerItem.length !== 0) {
                header = header.children('.item').eq(0);
            }

            if(objType){
                header.html(BOUNDED_TEMPLATE({label : title, value: objType}));
            }

            kendo.bind(item, headerModel);
        });
     }

    /**
    * panelBar for print and mobile view  
    */
    var CQPanel = widget.extend({
        init: function(element, options) {
            var that = this, panelElement;

            if(options.transformTabDOMToPanel !== false) {
                that._transformTabDOMToPanel(element, 1);
            }

            widget.fn.init.call(that, element, options);

            panelElement = $(element).children('ul');
            that._panelElement = panelElement;

            panelElement.kendoPanelBar($.extend({
                activate: function(e) {
                    kendo.init($(e.item).find('.cq-content')[0]);
                }
            }, options));


            if(options.panelHeaderModel){
                //use aggregate count based on the objects
                that._updateModelPanelTitle(panelElement, options.panelHeaderModel);
            }

            if(options.transformTabDOMToPanel !== false) {
                that._transformTabDOMToPanel(element, 2);
            }

            if(options.expandAllPanels === true) {
                panelElement.data('kendoPanelBar').expand(panelElement.find('li'), false);
            }

            // activate first tab manually because activation is done
            kendo.init(panelElement.find('li:first>.cq-content'));
        },
        _updateModelPanelTitle: function(element, panelHeaderModel) {
            var that = this;

            bindHeaderWithModel(element.find('li'), panelHeaderModel);
        },
        /**
        * Transforms tab like DOM structure into Panel bar compliant DOM
        * <pre>
        *    <div>
        *        <ul>
        *           <li>Tab Title 1</li>
        *           <li>Tab Title 2</li> ...
        *           <li>Tab Title N</li>
        *        </ul>
        *        <div> Tab Content 1</div>
        *        <div> Tab Content 2</div> ...
        *        <div> Tab Content N</div>
        *    <div>
        * </pre>
        *
        * to
        *
        * <pre>
        *    <div>
        *        <ul>
        *           <li><span>Tab Title 1</span><div> Tab Content 1</div></li>
        *           <li><span>Tab Title 2</span><div> Tab Content 2</div></li> ...
        *           <li><span>Tab Title N</span><div> Tab Content N</div></li>
        *        </ul>
        *    <div>
        * </pre>
        */
        _transformTabDOMToPanel: function(element, step){
            var that = this,
                uiElement = $(element),
                tabHeaders = uiElement.children("ul").children("li"),
                divs = uiElement.children("div"),
                index = 0, length = 0, oldHtml, span;
    
            //only transform if we have tabheaders and divs and 
            //they are equal in number
            if(tabHeaders && divs && tabHeaders.length == divs.length){
                for(index = 0, length = tabHeaders.length; index < length; index++){
                    if (step === 1){
                        oldHtml = tabHeaders.eq(index).html();
                        span = $("<span></span>");
                        span.html('<span class="item">' + oldHtml + '</span>');
                        span.addClass("k-link");
                        span.addClass("k-state-selected");
                        tabHeaders.eq(index).html(span);
                        tabHeaders.eq(index).append("<div class='cq-content'><div style='display:none'>Content here</div></div>");
                    }
                    else {
                        tabHeaders.eq(index).children('.cq-content').append(divs.eq(index));
                    }

                }
            }
        },
        options : {
            name: "CQPanel",
            // transforms Tab panel style DOM structuring to Panel style structuring.
            transformTabDOMToPanel : true,
            // if set to true expands all panels by default, else opens only default panel
            expandAllPanels: false,
            //the model that will be used to set the panel header
            panelHeaderModel: null
        }
    });

    var CQTabStrip = tab.extend({
        init: function (element, options) {
            var that = this;
            tab.fn.init.call(that, element, options);

            if(options.tabHeaderModel){
                //use aggregate count based on the objects
                this._updateModelTabTitle();
            }
            else{
                that.options.initialCounts = options.initialCounts;
                that._setInitialCounts();
                if ((that.options.showGridTitle === true) || (that.options.showGridCount === true)) {
                    that.wrapper.on("customBind", ".subGrid", tabGridEventHandler);
                }
            }
            that._visibilityCheck();
        },
        /**
        * ensures that tabs that are visible conditionally are hidden automatically
        * Plus this method also ensures that last tab is properly selected from those visible
        */
        _visibilityCheck: function() {
            var i, that = this, items = that.items(), lastIndex = 0;
            
            // hide both invisible tab and 
            for (i = 0; i < items.length; i++) {
                if (!$(items[i]).is(':visible')) {
                    $(that.contentElement(i)).hide();
                }
                else {
                    lastIndex = i;
                }
            }
            // added k-last to disable the next button when last Tab/Index is reached
            $(items[lastIndex]).addClass('k-last');
        },
        options: {
            name: "CQTabStrip",
            animation: false,
            showGridTitle: false,
            showGridCount: false,
            collapsible: false,
            activate: function (e) {
                var that = this;
                that.initializeTab(e.contentElement);
                setActionButtons(e);
            },
            initialCounts:null,
            tabHeaderModel: null // the object containing details such as count and main record that is to be bound to tab header
        },
        _setInitialCounts: function () {
            var that = this;
            if (!that.options.initialCounts) return;
            $.each(Object.getOwnPropertyNames(this.options.initialCounts), function (i, srcName) {
                var ds = that.options.initialCounts[srcName];
                if (ds instanceof CQDataSource){
                    that.setGridCount(ds.options.schema.model.fn.sObjectName, ds.options.schema.model.fn.sObjectLabel, ds.total());
                }
            })
        },
        /*
        * initializes given tab and makes attributes accessible 
        */
        initializeTab: function(tab) {
            var that = this;
            tab = $(tab);
            if (!tab.attr('data-initialized')) {
                kendo.init(tab);

                if(!that.options.tabHeaderModel){
                    that.wrapper.off("customBind", ".subGrid");
                    that.wrapper.on("customBind", ".subGrid", tabGridEventHandler);
                }
                
                tab.attr('data-initialized', true);
            }
        },
        setGridCount: function (sObjectName, srcLabel,count, e) {
            this._gridCounts[sObjectName] = count;
            this._updateTabTitle(sObjectName,srcLabel,count, e)
        },
        getGridCount: function (sObjectName) {
            return this._gridCounts[sObjectName] || 0;
        },
        _gridCounts: {},

        /**
        * updates the tab count for objects using related list
        * @param {string} sObjectName - the name of the object whose count has changed and related tabs are to be updated
        * @param {string} srcLabel - the label related to the object which is used to format the count
        * @param {integer} count - the total count that is to be set for the tab
        * @param {object} e - the event that caused the title to be updated 
        */
        _updateTabTitle: function(sObjectName, srcLabel, count, e){
            var tabItem,matchingTabFound,that=this;
            $.each(that.items(), function (i, item) {
                tabItem = $(item);

                // if there is a event ensure that the tab that is being updated actually holds the target/sender of the event
                // this will prevent object of same type updating 
                if(e !== undefined && e.sender.element.closest('div[role="tabpanel"]')[0] !== that.contentElement(i)){
                    return;
                }

                tabTitle = tabItem.attr("data-title") || srcLabel;
                matchingTabFound = false;
                if (tabItem.attr("data-grid-sobject") && tabItem.attr("data-grid-sobject") === sObjectName) {
                    tabCount = ' [' + that.getGridCount(sObjectName) + ']';
                    matchingTabFound = true;
                } else {
                    //todo:revisit kind of hacky! added support to handle multiple grids inside a tab and want to show that content exist
                    if (tabItem.attr("data-grid-sobject") && tabItem.attr("data-grid-sobject").indexOf(sObjectName) >= 0 ) {
                        var allGridCount=0;
                        $.each(tabItem.attr("data-grid-sobject").split(","), function (counter, obj) { allGridCount = allGridCount+that.getGridCount(obj) });
                        tabCount = (allGridCount > 0 ? ' [1+]' : ' [0]');
                        matchingTabFound = true;
                    }
                }
                if (matchingTabFound == true && that.options.showGridCount == true) {
                    $('a', tabItem).text(tabTitle + tabCount);
                }
            })
        },
        _updateModelTabTitle: function(){
            var that = this;
            bindHeaderWithModel(that.items(), that.options.tabHeaderModel);
        },
        /**
        * Selects the next tab to the current tab
        */
        showNext: function () {
            var currentIndex = -1,
                nextIndex = -1,
                currTab = $(this.select())[0],
                i, items = this.items();

            for (var i = 0; i < items.length; i++) {
                if(currTab == items[i]) {
                    currentIndex = i;
                }
                else if (currentIndex != -1 && $(items[i]).is(':visible')) {
                    nextIndex = i;
                    break;
                 }
             }

            if (nextIndex != -1) { this.select(nextIndex); }
        },
        /**
        * Selects the previous tab to the current tab
        */
        showPrev: function () {
            var currentIndex = -1,
                prevIndex = -1,
                currTab = $(this.select())[0],
                i, items = this.items();

            for (var i = items.length - 1; i >= 0; i--) {
                if(currTab == items[i]) {
                    currentIndex = i;
                }
                else if (currentIndex != -1 && $(items[i]).is(':visible')) {
                    prevIndex = i;
                    break;
                 }
             }

            if (prevIndex != -1) { this.select(prevIndex); }
        }
    });
    function toggleSection(e) {
        var parentDiv, target = e.currentTarget;
        parentDiv = target; //find parent div starting with img
        while (parentDiv && parentDiv.tagName != 'DIV') { parentDiv = parentDiv.parentNode; }
        if (parentDiv) {
            target = $(target);
            target.toggleClass('hideListButton showListButton');
            target.attr('title', target.attr('title') == 'Show Section' ? 'Hide Section' : 'Show Section');
            var div = parentDiv.nextElementSibling;
            while ($(div).hasClass('cqSectionContent')) {//toggles all continous cqSectionContent
                $(div).toggle();
                div = div.nextElementSibling;
            }
        }
    }
    var CQSectionHeader = kendo.ui.Widget.extend({
        init: function (element, options) {
            var that = this;
            kendo.ui.Widget.fn.init.call(this, element, options);
            if (that.options.title == '') that.options.title = element.text;
            that._create(element);
        },
        options: {
            name: "CQSectionHeader",
            collapsible: true,
            initiallyCollapsed: false,
            visible: true,
            title: ''
        },
        _create: function (element) {
            var that = this;
            $(element).addClass('cqSectionHeader');
            if (!!that.options.initiallyCollapsed) {
                $(element.nextElementSibling).hide();
            }
            if (that.options.collapsible) {
                var img = $('<img src="/img/s.gif" alt="Show Section" style="cursor:pointer;" tabindex="0">');
                img.on('click', toggleSection);
                img.on('keypress', function () { console.log(event.which); if (event.which == '13') toggleSection(event) })
                //            onclick="toggleSection(this);" onkeypress="if (event.keyCode==\'13\')toggleSection(this);" 
                $(element).text(that.options.title);
                img.addClass(that.options.initiallyCollapsed ? 'showListButton' : 'hideListButton');
                img.attr('title', that.options.initiallyCollapsed ? 'Show Section' : 'Hide Section');
                $(element).prepend(img);
            }
        }
    });

    var CQLoading = kendo.ui.Widget.extend({
        init: function (element, options) {
            var that = this;
            kendo.ui.Widget.fn.init.call(this, element, options);
            that._create(element);
        },
        options: {
            name: "CQLoading"
        },
        _create: function (element) {
            var message = 'Please Wait...';
            if(sqx.translation){
                message = sqx.translation.get('common.waitmessage');
            }

            var sldsEl = '<div role="status" class="slds-spinner slds-spinner_medium"><span class="slds-assistive-text">#: message#</span><div class="slds-spinner__dot-a"></div><div class="slds-spinner__dot-b"></div>        </div>';
            var subEl = '<div class="loadingDiv"><img src="../img/loading32.gif" alt="loading" /><div>#: message #</div></div>';
            var template = subEl;

            if(sqx.common.isLightningScope()) {
                template = sldsEl;
            }

            subEl = $(kendo.template(template)({message: message}));
            subEl.appendTo(element);
        }
    });

    ui.plugin(CQTabStrip);
    ui.plugin(CQSectionHeader);
    ui.plugin(CQLoading);
    ui.plugin(CQPanel);
})(jQuery);