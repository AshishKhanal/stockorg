/**
 * Custom date time picker for displaying datetime values serialzied from
 * SF in kendo UI
 */
var CQDateTimePicker = kendo.ui.DateTimePicker.extend({
    init: function (element, options) {
        var that = this;
        kendo.ui.DateTimePicker.fn.init.call(that, element, options);
        that.bind("open", function (e) {
            var now = new Date();
            
            if (this.options.noFutureDate) {
                var maxVal = this.max();
                
                // set max limit whichever is lower between max date option and current time
                if (maxVal && maxVal instanceof Date && maxVal < now) {
                    this.max(maxVal);
                } else {
                    this.max(now);
                }
            }

            if (this.options.noPastDate) {
                var minVal = this.min();
                
                // set min limit whichever is greater between min date option and current time
                if (minVal && minVal instanceof Date && minVal > now ) {
                    this.min(minVal);
                } else {
                    this.min(now);
                }
            }
        });
    },
    options: {
        name: "cqDateTimePicker",
        noPastDate: null,
        noFutureDate: null
    },
    value : function(value) {
        if(value && !(value instanceof Date)) {
            // Salesforce serializes Datetime as string but we need date object to
            // be able to see it in the UI.
            value = new Date(value);
        }
        return kendo.ui.DateTimePicker.fn.value.call(this, value);
    }

});

kendo.ui.plugin(CQDateTimePicker);