/*custom tab to show record title/count from child grid, each tabitem should have data-grid-sobject attribute to associate the correct grid. may need to change this to specific grid id */
var CQDatePicker = kendo.ui.DatePicker.extend({
    init: function (element, options) {
        var that = this;
        kendo.ui.DatePicker.fn.init.call(that, element, options);
        that.bind("open", function (e) {
            var now = new Date();
            
            if (this.options.noFutureDate) {
                var maxVal = this.max();
                
                // set max limit whichever is lower between max date option and current time
                if (maxVal && maxVal instanceof Date && maxVal < now) {
                    this.max(maxVal);
                } else {
                    this.max(now);
                }
            }

            if (this.options.noPastDate) {
                var minVal = this.min();
                
                // set min limit whichever is greater between min date option and current time
                if (minVal && minVal instanceof Date && minVal > now ) {
                    this.min(minVal);
                } else {
                    this.min(now);
                }
            }
        });
    },
    options: {
        name: "cqDatePicker",
        noPastDate: null,
        noFutureDate: null
    }

});

kendo.ui.plugin(CQDatePicker);

