//////// SECURITY CORRECTION AND OTHER COMMON CODE IN PAGE START

var sqx = (function($, sqx){
    var schemaConfiguration = function(otherConfigs){
        if(otherConfigs){
            $.extend(this, otherConfigs);
        }
    };

    /**
    * Common schema correction function that adds Is<PickListValue> to the schema object.
    * Example: If Audit has status Draft, In Approval and Published
    * 3 functions are added to audit schema called
    *  isDraft(), isPlan_Approval(), isPublished()
    * [Note: the spaces in the status field has been replaced  by an underscore(_) character and dash(-) with a dollar($) character]
    * @param schema the schema where the functions are to be added
    * @param fieldName the fieldName whose valid values functions are to be added. ex Status__c
    * @param prefix the prefix to add after 'is' i.e. changes is<Status> to is<Prefix><Status>
    * @param isOrdered indicates whether the list of valid values is ordered, if it is then it also generates is...OrHigher function
    * @param validValues a user can pass a list of valid values if they don't want to use values defined in the schema 
    */
    schemaConfiguration.prototype._pickListValuesToFunction = function(schema, fieldName, prefix, isOrdered, validValues){

        prefix = prefix || ''; //default no prefix

        //this function will return the actual function that will match the objects field value with picklist value
        //any match will then return a TRUE value.
        function checkStatus(valueToMatch){
            return function(){
                return this[fieldName] == valueToMatch;
            }
        }

        function isHigher(statesToNumber, valueToCheck){
            return function(){
                var currentVal = statesToNumber[this[fieldName]],
                    val = statesToNumber[valueToCheck];

                return currentVal >= val;
            }
        }

        //get the list of valid values for the picklist item
        var status, funcName, statesToNumber = {};

        validValues = validValues || schema.fields[fieldName].additionalInfo.validValues;

        $.each(validValues, function(i,e){ statesToNumber[e.value]  = i; });

        //loop through all valid status values and create a function called is<****> that checks if the item has the status
        for (var i = 0, length = validValues.length; i < length; i++) {
            status = validValues[i].value || validValues[i]; //support for simple array of valid values
            funcName = 'is' + prefix + status.replace(/(\s)+/g, '_');
            funcName = funcName.replace('-', '$');
            schema[funcName] = checkStatus(status); //add the function to schema

            if(isOrdered)
                schema[funcName + 'OrHigher'] = isHigher(statesToNumber, status);
        };
    };


    /**
    * Default can edit method checks for object's update access, record's edit access and parent's lock status
    */
    schemaConfiguration.prototype._canEdit = function(){
        var hasCrudAccess = this.ObjectAccess.isUpdateable && this.hasRecordEditAccess(),
            parentModelIsLocked = (this.parentModel && this.parentModel.isLocked && this.parentModel.isLocked()) || false;

        return hasCrudAccess && (!this.isLocked || ( this.isLocked && !this.isLocked() ) ) && !parentModelIsLocked;
    };

    /**
    * Default can delete method checks for object's delete access, record's delete access and parent's lock status
    */
    schemaConfiguration.prototype._canDelete = function(){
        var hasCrudAccess = this.ObjectAccess.isDeletable && this.hasRecordDeleteAccess(),
            parentModelIsLocked = (this.parentModel && this.parentModel.isLocked && this.parentModel.isLocked()) || false;

        return hasCrudAccess && (!this.isLocked || ( this.isLocked && !this.isLocked() ) ) && !parentModelIsLocked;
    };

    sqx.SchemaConfiguration = schemaConfiguration;

    return sqx;
})(window.jQuery, window.sqx || {});


//////// SECURITY CORRECTION AND OTHER COMMON CODE IN PAGE END