//Combobox extended to workaround bug of clear selection not triggering model change
//additionally combobox has been extended to fetch the correct data item if not in the original datasource

/**
* copy of removeFiltersForField present in kendo.ui.combobox. The purpose of this function is to remove
* the relevant filters i.e. containing name, when using contains mode.
* So that new filter can be added without removing other non-affecting filters
*/
function cq_removeFiltersForField(expression, field) {
    if (expression.filters) {
      expression.filters = $.grep(expression.filters, function(filter) {
        cq_removeFiltersForField(filter, field);
        if (filter.filters) {
          return filter.filters.length;
        } else {
          return filter.field != field;
        }
      });
    }
}

var CQComboBox = kendo.ui.ComboBox.extend({
    init: function (element, options) {
        var that = this, fn;
        kendo.ui.ComboBox.fn.init.call(this, element, options);

        // SQX-2582 : only clear if user has changed the value i.e. added something to the text box or selected new item
        //            and the selected value becomes invalid
        //
        // _hasUserEntered is used to track if user has made any changes
        //      There are two ways user can make changes
        //          a. typing : Keypress will set the flag
        //          b. selecting an item: will set the flag
        //
        //  note : this ignores any value set by backend scripts as correct. So, it will not clear it
        //  Known Side-effect: if there is a network delay and no result is returned id is displayed. 
        that._hasUserEntered = false; 
        fn =  function(){
            that._hasUserEntered = true;
        };

        that.bind('select', fn);
        that.input.bind('keypress', fn);
    },
    options: {
        name: "cqComboBox",
        cqFilterFields: [] //array of fields used in the combobox to search record
    },

    /**
    * clears the selection
    */
    _clearSelection: function (parent, isFiltered) {
        kendo.ui.ComboBox.fn._clearSelection.call(this, parent, isFiltered);
        this.trigger('change');
    },

    /**
    * re init filter is a custom cq extension that sets the datasource of the combobox to contain the selected value
    * if it is not already present in the datasource. This can happen when undoing a recent change, because the new datasource
    * will contain only the values present in freshly searched item.
    */
    cq_ReInitFilter: function(oldDataId){
        var that = this;

        if(!that.dataSource.get(oldDataId)){
            that.dataSource.filter({field : 'Id', operator: 'equals', value: oldDataId});
            that.value(oldDataId);
        }
    },
    /**
    * override for kendo.ui.combobox's default search to allow searching in multiple fields
    */
    search: function(word){
        //we will resort to standard behaviour unless cq-filter-fields have been provided
        if(!this.options.cqFilterFields || !this.options.cqFilterFields.length || this.options.cqFilterFields.length == 0){
            kendo.ui.ComboBox.fn.search.call(this, word);
        }
        else{

            /*
            Note: We are duplicating the code from kendo need to watch out for future upgrades
            */
            word = typeof word === "string" ? word : this.text();
            var that = this,
                length = word.length,
                options = that.options,
                ignoreCase = options.ignoreCase,
                filter = options.filter,
                field = options.dataTextField,
                extFilter = [], index,
                filterBy = options.cqFilterFields,
                STATE_FILTER = "filter"; //hack brought constant value from kendo combobox, might be an issue in later version

            clearTimeout(that._typing);

            if (length >= options.minLength) {
                that._state = STATE_FILTER;
                if (filter === "none") {
                    that._filter(word);
                } else {
                    that._open = true;
                    for(index = 0; index < filterBy.length; index++){
                        extFilter.push({
                            value: ignoreCase ? word.toLowerCase() : word,
                            field: filterBy[index],
                            operator: filter,
                            ignoreCase: ignoreCase
                        });
                    }

                    that.dataSource.filter({logic: 'or', filters: extFilter}); // used for filtering only once as default filter used to filter multiple times
                }
            }
        }
    },
    
    /**
    * override for kendo.ui.combobox's default _fetchItems function.
    * _fetchItems function is designed to get the selected value from the datasource in kendo for the first load.
    * _fetchItems has been used by CQ to add a new filter to include the current item in the datasource's result.
    * This happens when default response is size limited and the current item is beyond that limit.
    * This forces the item to be returned.
    */
    _fetchItems : function (value) {

    var that = this, hasItems = that.ul[0].firstChild;
    if (that._request) {
      return true;
    }
    if (!that._fetch && !hasItems) {
      if (that.options.cascadeFrom) {
        return!hasItems;
      }
      that.dataSource.one("change", function() {
        that.value(value);
        that._fetch = false;
      });
      that._fetch = true;
      if(value)
          that.dataSource.filter({field: "Id", operator: "equals", value: value});
      else
          that.dataSource.fetch();
      return true;
    }
  },
  
  /**
  * this is kendo's default function to add a new filter to the datasource.
  * this function has been overidden because we want to clear the filters based on value field introduced by the _fetchItems
  */
  _filterSource : function (filter) {
    var that = this, options = that.options, dataSource = that.dataSource, expression = dataSource.filter() || {};
    cq_removeFiltersForField(expression, options.dataTextField);
    cq_removeFiltersForField(expression, options.dataValueField); //added a new rule to remove filters based on value field usually the id field, added by the override in _fetchItems
    if (filter) {
      expression = expression.filters || [];
      expression.push(filter);
    }
    dataSource.filter(expression);
  }
});
kendo.ui.plugin(CQComboBox);