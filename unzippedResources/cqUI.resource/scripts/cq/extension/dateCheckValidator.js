/*jshint browser: true */
/*global window document console*/
/** Adding default rules in kendo validator to validate min/max date and future & past date */

(function (kendo) {
    "use strict";

    var MIN_ATTR = 'min',
        MAX_ATTR = 'max',
        TYPE_DATE_ATTR = 'data-type',
        NO_FUTURE_ATTR = 'data-no-future-date',
        NO_PAST_ATTR = 'data-no-past-date',
        TYPE_DATE = 'date',
        TYPE_DATE_TIME = 'datetime',
        TRUE_STRING = 'true';

    /**
     * Returns the type of input element/format that is used for the input type
     * @param {String} inputType the type of input as set in data-type property
     */
    function getFormat(inputType) {
        return inputType === TYPE_DATE_TIME
            ? 'kendocqDateTimePicker'
            : 'kendocqDatePicker';
    }

    /**
     * Returns the value of current date or current date time based on the input type.
     * @param {String} inputType The type of input as set in data-type property
     */
    function getNow(inputType) {
        var now = new Date();

        return inputType === TYPE_DATE_TIME
            ? now
            : new Date(now.getFullYear(), now.getMonth(), now.getDate()); //today
    }

    //adding new rules so that error message is shown when value less than min value is entered
    //TODO: consider an alternative approach to completely blank the date value, by making changes in date picker
    //but this is better
    kendo.ui.Validator.fn.options.rules.datemincheck = function (input) {
        var inputType = input.attr(TYPE_DATE_ATTR);

        if ((inputType === TYPE_DATE || inputType === TYPE_DATE_TIME) && input.attr(MIN_ATTR) !== undefined) {
            var date = null,
                format = getFormat(inputType, input),
                inputWidgetFormat = input.data(format).options.format,
                minDate = kendo.parseDate(input.attr(MIN_ATTR));

            date = kendo.parseDate(input.val(), inputWidgetFormat); //this is because if format is not provided date may not be parsed correctly

            if (date !== null) {

                if (inputType === TYPE_DATE) {
                    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                    minDate = (minDate && new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate())) || null;
                }

                return date >= minDate;
            }

        }

        return true;
    };

    kendo.ui.Validator.fn.options.messages.datemincheck = 'Date can\'t be less than {1}';

    //max rule
    kendo.ui.Validator.fn.options.rules.datemaxcheck = function (input) {
        var inputType = input.attr(TYPE_DATE_ATTR);

        if ((inputType === TYPE_DATE || inputType === TYPE_DATE_TIME) && input.attr(MAX_ATTR) !== undefined) {
            var date = null,
                format = getFormat(inputType, input),
                inputWidgetFormat = input.data(format).options.format,
                maxDate = kendo.parseDate(input.attr(MAX_ATTR));

            date = kendo.parseDate(input.val(), inputWidgetFormat); //this is because if format is not provided date may not be parsed correctly

            if (date !== null) {

                if (inputType === TYPE_DATE) {
                    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                    maxDate = (maxDate && new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate())) || null;
                }

                return date <= maxDate;
            }

        }

        return true;
    };
    kendo.ui.Validator.fn.options.messages.datemaxcheck = 'Date can\'t be greater than {1}';


    kendo.ui.Validator.fn.options.rules.noFutureDateCheck = function (input) {
        var inputType = input.attr(TYPE_DATE_ATTR);
        if ((inputType === TYPE_DATE || inputType === TYPE_DATE_TIME) && input.attr(NO_FUTURE_ATTR) === TRUE_STRING) {

            var date = null,
                format = getFormat(inputType, input),
                inputWidgetFormat = input.data(format).options.format,
                maxDate = getNow(inputType);

            date = kendo.parseDate(input.val(), inputWidgetFormat);

            if (date !== null) {
                return (date <= maxDate);
            }

        }

        return true;
    };

    kendo.ui.Validator.fn.options.messages.noFutureDateCheck = 'Future date is not allowed';

    kendo.ui.Validator.fn.options.rules.noPastDateCheck = function (input) {
        var inputType = input.attr(TYPE_DATE_ATTR);
        if ((inputType === TYPE_DATE || inputType === TYPE_DATE_TIME) && input.attr(NO_PAST_ATTR) === TRUE_STRING) {
            var date = null,
                format = getFormat(inputType, input),
                inputWidgetFormat = input.data(format).options.format,
                minDate = getNow(inputType);

            date = kendo.parseDate(input.val(), inputWidgetFormat);

            if (date !== null) {
                return (date >= minDate);
            }

        }

        return true;
    };

    kendo.ui.Validator.fn.options.messages.noPastDateCheck = 'Past date is not allowed';

}(window.kendo));