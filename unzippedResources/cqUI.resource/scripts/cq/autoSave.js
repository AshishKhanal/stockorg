/*
* implements the auto save functionality for salesforce
*/
var sqx = (function(sqx){
    "use strict";
    
    sqx.autosave = sqx.autosave || {};
    var _autoSaveRPC = null;
    var _lastSavedVersion = -1;
    
    var _lock = false;
    var _autoSaveInterval = 2000; // 2 second by default
    var _autoSaveTimer = null; 
    var _mainRecordID = null;
    var _titleContainer = null;

    
    /**
    * initializes the auto save with the given version number
    */
    sqx.autosave.init = function(changeSet, mainRecordID, titleContainer, autoSaveRPC){
        sqx.common.assert(changeSet.version != null || 'Version number missing in passed change set object');
        _lastSavedVersion = changeSet.version;
        _mainRecordID = mainRecordID;
        _titleContainer = titleContainer || { Title__c : '' };

        _autoSaveRPC = autoSaveRPC;

        sqx.common.assert(_autoSaveRPC != null || 'Auto save RPC is not defined');
        
        sqx.common.log('Auto save initialized with version ' + _lastSavedVersion);
    }
    
    /**
    * called to synchronize the changeset in server with client, stated simply saves the changeset 
    * only one save request can be processed at any given time.
    */
    sqx.autosave.sync = function(changeSet){
        sqx.common.assert(changeSet.version != null || 'Version number missing in passed change set object');
        sqx.common.assert(_lastSavedVersion <= changeSet.version || 'Version number out of sequence');
        
        
        //if last saved version and current version are no different we return
        if(_lastSavedVersion == changeSet.version)
            return;
        
        
        
        if(!_lock){ //ensures that only single request is made per transaction.
            _lock = true;
            
            sqx.common.assert(changeSet.deleted instanceof Array || 'Expected deleted in changeset but was missing');
            sqx.common.assert(changeSet.modified instanceof Array || 'Expected modified in changeset but was missing');
            
            var versionNumber = changeSet.version; //this ensures that even if changeSet object is modified, we have a version number
                                                    // that initiated the request
            sqx.common.log('Saving version ' + versionNumber);

            if(!$('#autoSave').length){
                $('#mainTab').after('<span id="autoSave"></span>'); // Add an empty span to show save status in page
            }

            $('#autoSave').text(cq.labels.autoSave.saving);

            //send deleted as strings so that it can be deleted from server easily.
            var deleted = [], removedIds = {};
            for(var index = 0, length = changeSet.deleted.length; index < length; index++){
                deleted.push({ Id: changeSet.deleted[index].Id });
                removedIds[changeSet.deleted[index].Id] = true;
            }
            
            _autoSaveRPC.call(null, _mainRecordID, deleted, JSON.stringify({changeSet: changeSet.modified }), _titleContainer.Title__c, function(response, request){
                
                
                if(response == '+OK' ){
                    sqx.common.log('Saved version ' + versionNumber); 
                    
                    $('#autoSave').text(cq.labels.autoSave.saved +' '+ kendo.toString(new Date(), sqx.common.CQTimeFormat));

                    _lastSavedVersion = versionNumber;

                    // remove the deleted record from the changeset
                    var deletedChangeSet = changeSet.deleted, index;
                    for(index = deletedChangeSet.length - 1;index >= 0; index--){
                        
                        if(removedIds[deletedChangeSet[index].Id]  === true){
                            deletedChangeSet.splice(index, 1);
                        }
                    }

                }
                else{
                    sqx.common.error('Could not save the change set', request);
                }
                _lock = false;

            });
        }
    }
    
    /**
    * this method starts the auto save timer and takes the first param as source of change set and second param as the number of 
    * milliseconds before attempting to synchronize
    * @param changeSetQueryFn the function that returns the changeSet object
    * @param duration the number of seconds in which to query the change set source and sync it automatically
    */
    sqx.autosave.start = function(changeSetQueryFn, duration){

        duration = duration || _autoSaveInterval;

        sqx.common.log('Starting autosave with check duration set to ' + duration);

        _autoSaveTimer = setInterval(function(){
                                        sqx.autosave.sync(changeSetQueryFn(true)); //true prevents auto sync from firing
                                    }, duration);
    }
    
    
    /**
    * returns true if the change set has not changed from the last saved version
    */
    sqx.autosave.isInSync = function(changeSet){

        sqx.common.log('Last saved : ' + _lastSavedVersion + ', Current Version : ' + changeSet.version);
        return _lastSavedVersion == -1 || _lastSavedVersion == changeSet.version;
    }
    
    /**
    * if the auto save had been started using sqx.autosave.start, it will stop it
    */
    sqx.autosave.stop = function(){
        sqx.common.log('Stopping autosave');
        clearInterval(_autoSaveTimer);

        _lastSavedVersion = -1;
    }
    
    
    return sqx;
    
    
})(sqx || {} );