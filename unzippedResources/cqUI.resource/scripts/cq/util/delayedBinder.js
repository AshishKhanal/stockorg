/**
 * This is an internal method that is useful for binding columns in CQGridbase in a
 * delayed manner
 * @param data {Object} the model that is being bound
 * @param field {String} the name of the field/property of data we have to bind to
 * @param identifier {String} the identifier
 */
function cq_i_delayedBinder(data, field, identifier) {
    var elementSelector = '.cq-' + identifier;

    setTimeout(function(){
        $(elementSelector).attr('data-bind', 'file: ' + field);
        kendo.bind($(elementSelector), data);
    }, 50);
}