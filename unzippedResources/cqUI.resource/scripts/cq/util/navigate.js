/*
 * this function navigates to record based on provided object Id
 */
function navigateToSObject(objectId) {
    if (typeof (sforce) == "object") {
        sforce.one.navigateToSObject(objectId);
    } 
    else if (sqx.common.isSFID(objectId)) {
        window.location = "/" + objectId;
    }
    else {
        window.location = window.location.pathname + "?id=" + objectId;
    }
}

/*
 * this function navigates to provided URL
 */
function navigateToURL(url) {
    if (typeof (sforce) == "object") {
        sforce.one.navigateToURL(url);
    } else {
        window.location = url;
    }
}

function navigateTab(e) {
    var btnType = $(e.sender.wrapper).attr('id');
    if (btnType == 'btnNextSection') {
        mainTS.showNext();
    }
    if (btnType == 'btnPrevSection') {
        mainTS.showPrev();
    }
}