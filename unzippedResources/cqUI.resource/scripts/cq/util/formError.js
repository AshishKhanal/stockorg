/*
 * This function sets Form error
 */
function setFormError(message, contextWindow) {
    var DEFAULT_MSG = sqx.translation.get('common.errormsg'),
        msg = message || DEFAULT_MSG,
        errDiv = contextWindow == null ? $("#errorDiv") : $(contextWindow).find("div.errorDiv");

        errDiv.find('.msg').html(msg);
        if (errDiv.is(':visible')) {
            shakeElement(errDiv);
        };
        errDiv.show(600);
}

/*
 * This function animates the form error
 */
function shakeElement(elem, times) {
    if (times == null) times = 2;
    elem.animate({
        top: "-=5"
    }, 75, function () {
        elem.animate({ top: "+=5" }, 75, function () {
            if (times > 0) shakeElement(elem, --times);
        })
    })
}

/*
 * This function clears form error
 */
function clearFormError(contextWindow) {
    var errDiv =  contextWindow == null ? $("#errorDiv") : $(contextWindow).find("div.errorDiv");
    
    errDiv.hide(400);
    errDiv.find('.msg').empty();
}