/**
* @description: initilize esignature and add attributes for enabled and required username
* Esig_Enabled      Esig_Config_Required        Esig_Required
*       T                   T                       T
*       T                   F                       F
*       F                   T                       F
*       F                   F                       F
**/
function bindeSignature(nextAction){
    var needsEsig,
        action;
    if(nextAction){
        action = nextAction.toLowerCase();
        needsEsig = esigEnabled &&  (esigPolicies[action] ? esigPolicies[action].compliancequest__Required__c : true);
    }else{
        needsEsig = esigEnabled
    }            
    $( "div[data-role='cqelectronicsignature']" ).attr({
        'data-allow-e-sig': needsEsig,
        'data-require-user-name': requireUserName
    });
    kendo.bind($( "div[data-role='cqelectronicsignature']" ));
}

/*
 * @description: enable/disable popup window for any actions
 * Requires_Esig        Requires_Comment        Enable_Popup
 *      T                       T                   T
 *      T                       F                   T
 *      F                       T                   T
 *      F                       F                   F
**/
function requirePopupWindow(nextAction, defaultComment){
     var needsEsig,
        action, needsComment = false, actionSpecificPolicy;
     if(nextAction){
        action = nextAction.toLowerCase();
        actionSpecificPolicy = esigPolicies[action];
        needsEsig = esigEnabled &&  ( actionSpecificPolicy ? actionSpecificPolicy.compliancequest__Required__c : true);
        needsComment = actionSpecificPolicy && actionSpecificPolicy.compliancequest__Comment__c && !defaultComment; //set comment required if action specific policy is defined
     }else{
         needsEsig = esigEnabled;
     }  

     if(needsEsig || needsComment){
         return true;
     }

     return false;
}