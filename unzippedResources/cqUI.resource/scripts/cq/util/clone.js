/**
* clones a given object, based on the provide schema, additionally some fields may be omitted
* @param object object to be cloned
* @param objectSchema schema of the object to be cloned
* @param omitFields the fields that are not to be copied
*/
function cloneObject(object, objectSchema, omitFields){
    var clonedObject = {};

    for(var field in  objectSchema.fields){
        if(omitFields === undefined || omitFields[field] === undefined || omitFields[field] === false){
            clonedObject[field] = object[field]
        }
    }

    clonedObject.attributes = {type : objectSchema.sObjectName};


    return clonedObject;
}

function CloneConfig(){
    this._config = { followRelations: {}, omitFields: { Id : true }}; //never clone Id
    return this;
}

/**
* add to the list of relation to follow
*/
CloneConfig.prototype.followRelation = function(relation){
    this._config.followRelations[relation] = new CloneConfig();
    
    return this._config.followRelations[relation];
}

/**
* add to the list of omit field
*/
CloneConfig.prototype.omitField = function(field){
    this._config.omitFields[field] = true;
    
    return this;
}

/**
* returns true if the field is to be omitted
*/
CloneConfig.prototype.shouldOmitField = function(field){
    return this._config.omitFields[field] || false; //if the field is undefined returns false else returns whatever is set
}

/**
* returns the list of relations to follow
*/
CloneConfig.prototype.relationsToFollow = function(){
    return this._config.followRelations;
}


/**
* clones a given object following the instructions in config
*/
function cloneObject(object, dataStore, config){
    var newObject = dataStore.insert(0, {}),//insert an empty object
        fields,
        idx,
        length,
        relationsToFollow; 
    
    config = config === undefined ? new CloneConfig() : config; //default config don't follow relationship copy all fields
    
    //copy all fields except those which are to be ignored
    fields = newObject.fields;
    for(var field in fields ){
        if(config.shouldOmitField(field)){
            sqx.common.log('Omitting field ' + field);
            continue; //if this field is ignored then just move on
        }
        
        newObject.set(field, object.get(field)); //TODO: decide between direct assignment vs set
    }
    
    //copy related objects
    relationsToFollow = config.relationsToFollow()
    for(var relation in relationsToFollow){
        var relatedDataStore = object.relatedList(relation),
            relatedConfig = relationsToFollow[relation],
            allObjects = null,
            newDataStore = newObject.relatedList(relation);

        
        sqx.common.assert(relatedDataStore != null || ('No data store found for ' + relation));
        
        allObjects = relatedDataStore.view(); //iterate through the list of related objects and clone them
        for(idx = allObjects.length - 1; idx >= 0; idx--){ //why reverse because this ensures  the order is same
            
            cloneObject(allObjects[idx], newDataStore, relatedConfig); //clones the related object
            
        }
        
    }

    dataStore.sync();
    
        
    return newObject;
}