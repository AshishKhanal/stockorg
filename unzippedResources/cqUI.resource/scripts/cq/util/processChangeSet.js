function getSynchronizedChangeSet() {
    syncChanges();
    return getServerSubmissionSet(sqx.dataStore.getChangeSet().modified);
}

function syncChanges() {
    var synchronizeRelated = function () { var grid = $(this).data('kendoCQGrid'); if(grid) grid.dataSource.sync(); };

    sqx.dataStore[mainRecordType].sync();
    sqx.dataStore.ContentDocStore.sync();
    $('.cq-topGrid').each(synchronizeRelated);
    $('.subGrid.cq-incelledit').each(synchronizeRelated);
    if ($.isFunction(window.syncBeforeSave)) syncBeforeSave(); //all custom sync unique to each page
}

function processChangeSet(nextAction) {
    formValidator = formValidator || { validate: function () { return true } };
    if (formValidator.validate()) {
        try {
            $("#loadingBlock").show();
            var idtodelete = [];
            var changeSet = getSynchronizedChangeSet();
            var deletedSobjects = sqx.dataStore.getChangeSet().deleted.filter(function (e) {
                if (sqx.common.isSFID(e.Id)) {
                    idtodelete.push({ Id: e.Id });
                    return true;
                }
                return false;
            });
            showLoading();
            UIController.processChangeSetWithAction(mainRecord.Id, changeSet, idtodelete, nextAction, null , processChangeSetControllerCallBack);
        }
        catch (ex) {
            console.log(ex);
            alert('Sorry an error occured while saving');
            setFormError(ex.message);
            $("#loadingBlock").hide();
        }
    }
    else {
        setFormError();
    }
}
function processChangeSetControllerCallBack(e, s) {
    if (s.status) {
        commandRules.set('isDirty', false);
        navigateToSObject(e);
        return;
    }
    console.log(s);
    hideLoading();
    setFormError(s.message);
}