  
window.cq =(function(doc, cq) {
      "use strict";

       /*
        * This method is used to add parameters to give url
        * @param 'key' url querystring name
        * @param 'value' url querystring value
        * @param 'url' url
        */
        cq.insertParamtoURL = function(key, value, url) {
          key = encodeURI(key);
          value = encodeURI(value);

          var splitStr = url.split('&');
          var i=splitStr.length; 
          var x; 

          while(i--) 
          {
              x = splitStr[i].split('=');
              
              if (x[0]==key)
              {
                  x[1] = value;
                 splitStr[i] = x.join('=');
                  break;
              }
          }

          if(i<0) {
              splitStr[splitStr.length] = [key,value].join('=');
          }

          if(url.indexOf("?") != -1) {
              url = splitStr.join('&'); 
          } else {
              url = splitStr.join('?'); 
          }
          return url;
        }
      
      return cq;
  }(document, window.cq || {}));