$(function() {
    "use strict";

    /**
     * function for overriding default approval link.
     * Note: There are two different approaches in kendo pages and Controlled doc detail
     * page.
     * @param : callback : function to be executed when the approve/reject link is clicked.
     */
    sqx.overrideDefaultApprovalLinkFunction = function(callback){
        $('[name="piSubmit"]').hide();

        $('a.cqCommandButton.approveResponse').each(function(i, e) {
            e = $(e);
            $(e).attr('data-href', $(e).attr('href'));
            $(e).attr('href', '#');
        });
        $('a.cqCommandButton.approveResponse').click(callback);
    };

    //adds css classes to make the approve/reject link like a button
    $('tr.dataRow>td.actionColumn>a.actionLink[href*="ProcessI"]').addClass('approveResponse cqCommandButton k-button approve textBlackBold');

    sqx.commonActions = {
        _addLink: function(model, file, shareType){
            var linkStore, linkDef, isPresent, doc, fileId;

            if(!file) {
                return;
            }

            linkStore = model.relatedList('ContentDocumentLinks');
            isPresent = linkStore.view().filter(function(i) { return i.LinkedEntityId == model.Id && i.ContentDocumentId == file.Id; } ).length > 0;
            if(!isPresent) {
                link = linkStore.add({
                    attributes: {
                        type: 'ContentDocument' // Attribute has to be manually set because of autosync
                    },
                    ContentDocumentId: file.Id,
                    ContentDocument: {
                        Title : '',
                        Description : ''
                    },
                    LinkedEntityId: model.Id,
                    ShareType: shareType
                });
                linkStore.sync();
                //HACK: Double synchronization required because change wasn't being captured for doc link
                link.set('ContentDocument.Title', file.Title);
                link.set('ContentDocument.Description', file.Description);
                linkStore.sync();
            }
        },
        addLinkToFiles: function(model, file) {
            var that = this;
            that._addLink(model, file, 'V');
            if(model.Id !== mainRecord.Id) {
                that._addLink(mainRecord, file, 'I');
            }
        },
        addFiles : function(e) {
            var sender = $(e.target),
                grid = sender.parents('[data-role="cqgrid"]').data('kendoCQGrid'),
                dataSource = grid.dataSource,
                dataItem, rootRecordId,
                fileDialog = $("<div><input class='cq-file-uploader' type='file' /></div>"), otherOptions,
                fileDialogWindow;

                dataItemId = dataSource.options && dataSource.options.parentId || null;

                if (dataItemId === null) {
                    console.error('Configuration anomaly, dataitem id is null');
                    return;
                }

                fileDialog.kendoWindow({
                    modal: true,
                    width: "400px",
                    height: "200px",
                    title: sqx.translation.get('common.addfiles') || 'Add Files'
                });

                fileDialogWindow = fileDialog.data('kendoWindow');
                fileDialogWindow.center();
                fileDialogWindow.open();
                var triggerCallBack = function(arg){
                            // disable buttons when file is being uploaded

                            var buttons = $(".k-window .cqCommandButton");
                            var close_anchor = $(".k-window .k-link");
                            buttons.prop('disabled', arg == 'Upload Started');
                            if(arg == 'Upload Started')
                                close_anchor.hide();
                            else
                                close_anchor.show();
                        };

                otherOptions = {
                    multiple: true,
                    error : function(e) {  triggerCallBack('Upload Failed')},
                    cancel : function(e) { triggerCallBack('Upload Cancelled')},
                    start : function(e) { triggerCallBack('Upload Started')},

                    success: function(r) {
                        var files = r.files, idx, mainRecordlinkStore, childRecordlinkStore, link;

                        sqx.dataStore.addToOriginal('ContentDocument', {Id : r.response.id, Title: r.response.title, Description: r.response.description});
                        sqx.commonActions.addLinkToFiles(sqx.dataStore.getObjectWithId(dataItemId), {
                            Id : r.response.id,
                            Title: r.response.title,
                            Description: r.response.description
                        });


                        triggerCallBack('Upload Succeeded');


                    }
                };
                sqx.createFileUploader(fileDialog.find(".cq-file-uploader"), cq.SESSION_ID, '', otherOptions);
        }
    };

    // Default Event Handler for buttons, all the pages should use this event handler instead of redefining it every where
    sqx._defaultActionHdlr = function(e){
        var d = $("<div />"),
            needsEsigFn = null,
            needsEsig = true,
            options = JSON.parse(JSON.stringify(kendo.ui.CQESigWindow.fn.options)); //clone valid options from esig window to fn all permitted options are allowed.


        options.needsEsig = null;
        options.cqAction = null;
        options.showPopup = false;

        options = kendo.parseOptions(e.currentTarget, options);


        //validate the main form when the action is invoked
        if(window.formValidator){
            options.validateMainWindow = options.validateMainWindow === undefined ? true : options.validateMainWindow; //always validate by default
            if(options.validateMainWindow && !window.formValidator.validate()){
                setFormError();
                return;
            }
        }

        if(options.fnNs){
            options.open = kendo.getter((options.fnNs || 'window') + '.start')(window);
            options.close = kendo.getter((options.fnNs || 'window') + '.cleanUp')(window);
        }

        if(options.needsEsig){
            needsEsigFn = kendo.getter((options.fnNs || 'window') + '.' + options.needsEsig)(window);
            needsEsig = needsEsigFn.call(null, d);
        }

        //if actions is not defined needsEsig is true
        //A' => true
        needsEsig = true;
        var noPopupRequired = !requirePopupWindow(options.cqAction, options.defaultComment) && !options.showPopup;
        options.cqAction = options.cqAction && options.cqAction.toLowerCase() || '';
        if(options.cqAction && window.esigPolicies[options.cqAction] !== undefined){
            needsEsig = window.esigPolicies[options.cqAction].compliancequest__Required__c;
            options.requireComment = esigPolicies[options.cqAction].compliancequest__Comment__c;
        }


        options.purposeOfSig = options.purposeOfSig || (window.purposeOfSigs && window.purposeOfSigs[options.cqAction]);


        //remove the window from DOM once deactivated
        options.deactivate = function(){
            this.destroy();
        }



        options.width =  $(document).width() - 2 * window.popupPadding(); // set width with padding by default
        options.width = options.width + 'px';


        options.currentUserDetailsVar = options.currentUserDetailsVar || 'userDetails'; //set user details variable if not defined
        options.requireUserName = options.requireUserName || requireUserName; //require user name is being set after the script so we have to reinject it :-( globalRequireUserName is always set to true in the component. We need to figure a way to pass the param correctly.
        options.requireEsig = options.requireEsig || esigEnabled;

        options.requireEsig = options.requireEsig && needsEsig;

        options.needsCommentField = true; //always require comment by default


        options.title = options.title || options.purposeOfSig;

        d = d.kendoCQESigWindow(options).data('kendoCQESigWindow');
        //execute default action if no template defined and nopopuprequired condition
        if(options.templateId == null && noPopupRequired){
            //call the default savehandler immediately and save without any param
            d.executeDefaultAction();
            d.destroy();
        }
        else{
            d.open();
        }

    };
});