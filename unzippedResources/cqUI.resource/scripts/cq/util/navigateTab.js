function navigateTab(e) {
    var btnType = $(e.sender.wrapper).attr('id');
    if (btnType == 'btnNextSection') {
        mainTS.showNext();
    }
    if (btnType == 'btnPrevSection') {
        mainTS.showPrev();
    }
}
function setActionButtons(e) {
    var currTab = $(e.sender.select());
    commandRules.set('enablePrev',  !currTab.is(".k-first"));
    commandRules.set('enableNext',  !currTab.is(".k-last"));
}

function cancelChanges(){
    if(sqx.common.isSFID(mainRecord.Id))
    {
        navigateToSObject(mainRecord.Id);
    }
    else
    {
        if(queryString.retURL){
            navigateToURL(queryString.retURL);
        }
        else{
            if ((typeof sforce != 'undefined') && (sforce != null)) {
                sforce.one.back();
            }else{
                navigateToURL(window.location);
            }
        }
    }
}