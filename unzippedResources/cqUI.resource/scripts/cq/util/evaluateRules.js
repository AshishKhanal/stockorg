function evaluateRules(node, field, allRules) {
    /*
                        objectName: "SQX_Finding__c",
                    rule: [
                        {
                            field: "Response_Required__c",
                            condition: "Response_Required__c == true",
                            toggleActions: [{ action: "setrequired", targetNode: "", targetField: "Due_Date_Response__c" }]
                        },
                        {
                            field: "Containment_Required__c",
                            condition: "Containment_Required__c == true",
                            toggleActions: [{ action: "setrequired", targetNode: "pSQX_CAPA__r", targetField: "External_Reference_Number__c" }]
                        },
                            field: "Investigation_Required__c",
                            condition: "Investigation_Required__c == true",
                            toggleActions: [{ action: "setrequired", targetNode: "", targetField: "Due_Date_Investigation__c" }],
                            actionsIfMet:[{action: "setvalue", targetNode: "", targetField: "Corrective_Action_Required__c", value: true },
                            {action: "setdatasource", targetNode: "", targetField: "devcq__SQX_Account__c", value: true }],
                            actionsIfUnMet:[{action: "setvalue", targetNode: "", targetField: "Corrective_Action_Required__c", value: false },
                            {action: "setvalue", targetNode: "Finding", targetField: "Description",  valueField: 'Description__c' }]
                        }
                    ]

                    setdatasource is evaluated everytime actionsIfMet or actionsIfUnMet is evaluated to true. 
                    DOM actions occur all the time (required, show/hide), Data actions will occur only when field changed (value)

    */
    var formRule, rule, condition, conditionMet, action, target, source = node.sObjectName, nd, lastNodeName,fieldChanged = (!!field);
    for (var r = 0; r < formRules.length; r++) {
        formRule = formRules[r];
        if (formRule.objectName == source) {
            for (var i = 0; i < formRule.rule.length; i++) {
                rule = formRule.rule[i];
                if ((allRules === true) || !field || (rule.field === field)) {
                    conditionMet = kendo.getter(rule.condition)(node);
                    //process toggle actions
                    if (rule.toggleActions) {
                        for (var ta = 0; ta < rule.toggleActions.length; ta++) {
                            act = rule.toggleActions[ta];
                            if ((lastNodeName !== act.targetNode) || nd == null) { //minimize target node evals if all actions are referring to the same target node
                                nd = kendo.getter(act.targetNode)(node);
                                lastNodeName = act.targetNode;
                            };
                            if (act.action == 'setrequired') {
                                if (!nd.isRequired) { nd.set('isRequired', {}) }
                                nd.set("isRequired." + act.targetField, conditionMet);
                            }
                            if(act.action=='setHidden'){
                                if (!nd.isHidden) { nd.set('isHidden', {}) }
                                nd.set("isHidden." + act.targetField, conditionMet);
                            }
                            if (fieldChanged && act.action == 'setvalue') {
                                nd.set( act.targetField, conditionMet);
                            }
                            if (act.action == "setReadonly") {
                                if (!nd.isReadonly) { nd.set('isReadonly', {}) }
                                nd.set("isReadonly." + act.targetField, conditionMet);
                            }
                        }
                    }
                    //process met actions
                    if (rule.actionsIfMet && conditionMet) {
                        for (var ta = 0; ta < rule.actionsIfMet.length; ta++) {
                            act = rule.actionsIfMet[ta];
                            if ((lastNodeName !== act.targetNode) || nd == null) { //minimize target node evals if all actions are referring to the same target node
                                nd = kendo.getter(act.targetNode)(node);
                                lastNodeName = act.targetNode;
                            };
                            if (fieldChanged && act.action == 'setvalue') {
                                if(act.valueField){
                                    nd.set(act.targetField, node.get(act.valueField)); //added support for field propagation of current object to target node
                                }
                                else
                                    nd.set(act.targetField, act.value); //todo: support non primitive values/functions and path
                            }
                            else if((cq.initialLoad || fieldChanged) && act.action == 'setdatasource'){
                                //setdatasource also checks fieldchanged because default data source switching is being done by the 
                                //the initialize rule

                            	nd.set(act.targetField + '_Source', act.value);

                                //added data reload because that ensures the bounded component gets refreshed.
                                //if read is not called, the bounded component such as combobox, will have invalid items in their
                                //dropdown. It should be noted that these elements can't be clicked.
                                nd.get(act.targetField + '_Source').read();
                            } else if (fieldChanged && act.action === 'invoke' && cq.invokeFlow) {
                                cq.invokeFlow(act);
                            } else if (fieldChanged && act.action === 'transfer' && cq.transferFlow) {
                                cq.transferFlow(act);
                            }
                        }
                    }

                    //process unmet actions
                    //process met actions
                    if (rule.actionsIfUnMet && !conditionMet) {
                        for (var ta = 0; ta < rule.actionsIfUnMet.length; ta++) {
                            act = rule.actionsIfUnMet[ta];
                            if ((lastNodeName !== act.targetNode) || nd == null) { //minimize target node evals if all actions are referring to the same target node
                                nd = kendo.getter(act.targetNode)(node);
                                lastNodeName = act.targetNode;
                            };
                            if (fieldChanged && act.action == 'setvalue') {
                                 if(act.valueField){
                                    nd.set(act.targetField, node.get(act.valueField)); //added support for field propagation of current object to target node
                                }
                                else
                                    nd.set(act.targetField, act.value); //todo: support non primitive values/functions and path
                            }
                            else if ((cq.initialLoad || fieldChanged) && act.action == 'setdatasource'){
                                //refer to comments on ifMet's setdatasource for reasons on adding fieldchanged and read().
                            	nd.set(act.targetField + '_Source', act.value);
                                nd.get(act.targetField + '_Source').read();
                            }
                        }
                    }
                }
            }
        }
    }
}

function setupWIP() {
    UIController.initWipItem(function (e, s) { if (!(s.success)) { sqx.common.log('unable to create WIP store') } });
}

/**
* This function returns function based on security matrix rule
*/
function createSecurityEvaluationRule(rule, check){
    switch (check){
        case 'edit':
            return function(){
                var hasCrudAccess = rule.isUpdateable && this.hasRecordEditAccess();
                
                return hasCrudAccess && (!this.isLocked ||  !this.isLocked());
            };

        case 'delete':
            return function(){
                var hasCrudAccess = rule.isDeletable && this.hasRecordEditAccess();
                
                return hasCrudAccess && (!this.isLocked ||  !this.isLocked());
            };

        case 'create':
            return function(){
                return rule.isCreateable;
            };
    }

    return null;
}