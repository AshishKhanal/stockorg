/**
* initalizes query string variable with the name value pair in querystring
*/
(function initializeQueryString(w){
    var queryStringPortion = w.location.search,
		queryString = w.queryString || {},
        queryParams,
        count, length, paramNameArray;
    
    if(queryStringPortion.length > 0 && queryStringPortion[0] == '?'){
        queryParams = queryStringPortion.substr(1).split('&');
        for(count = 0, length = queryParams.length; count < length ; count++){
            paramNameArray = queryParams[count].split('=');
            
            queryString[paramNameArray[0]] = paramNameArray[1] && decodeURIComponent(paramNameArray[1]);
        }
    }
	
	w.queryString = queryString;
})(window);