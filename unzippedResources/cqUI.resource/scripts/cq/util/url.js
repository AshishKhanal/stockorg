/*jshint browser: true */
/*global window document console*/

/**
 * This module contains helpers that are being used for interaction with url and url parameters
 */
(function (w, jQuery, URL) {
    "use strict";

    var REGEX_URL_EXTRACTOR = /\(\'(https:\/\/.*)\'\);/,
        windowLocation = new URL(w.location.toString()),
        navigateToUrl = w.navigateToUrl;

    /**
    * Get value of param in current URL
    * @sParam : Parameter whose value is to be returned
    * @hreftarget : the element of which href is a property of
    **/
    function getUrlParameter(sParam, hrefTarget) {
        var urlStr = '',
            url;

        urlStr = (hrefTarget && (jQuery(hrefTarget).data('href') || hrefTarget.href));
        if (!urlStr) {
            url = windowLocation;
        } else {
            url = new URL(urlStr);
        }

        //if the page is in lightning mode and target can have javascript protocol, then get URL string by decoding the string
        if (url.protocol === 'javascript:') {

            url = new URL(decodeURIComponent(url.pathname).match(REGEX_URL_EXTRACTOR)[1]);
        }
        console.debug(sParam + ' : ' + url.searchParams.get(sParam) + ' in ' + url);
        return url.searchParams.get(sParam);
    }

    /**
    * adds param editMode=true to current url
    **/
    function renderPageToEditMode() {
        var url = new URL(w.location.toString());
        url.searchParams.append('editMode', true);
        navigateToUrl(url.toString());
    }

    w.renderPageToEditMode = renderPageToEditMode;
    w.getUrlParameter = getUrlParameter;
}(window, window.jQuery, window.URL));