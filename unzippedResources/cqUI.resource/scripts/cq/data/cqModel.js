var CQModel = kendo.data.Model.define({
    init: function (value) {
        var that = this, propName, chldRelations;
        kendo.data.Model.fn.init.call(that, value);
        that.options = { editable: true };
        chldRelations = that.childRelationships;
        if (chldRelations) {

            for (var i = 0; i < chldRelations.length; i++) {
                (function (obj, propName) {
                    Object.defineProperty(obj,
                      propName,
                      { get: function () { return sqx.dataStore._getRelatedView(this, propName) } })
                }
                )(that, chldRelations[i].relationshipName);
            }
        }
        evaluateRules(this);
    },
    relatedList: function (relName) { return sqx.dataStore._getRelatedDataSource(this, relName) },
    isNewObject: function () { return this[this.idField].length > 18; },
    /** Override kendo's isNew in model because id isn't blank for CQ Model. So it uses isNewObject instead */
    isNew: function() { return !sqx.dataStore.isCommitted(this.Id) && (this.isNewObject() || kendo.data.Model.fn.isNew.call(this));},
    isExistingObject: function () { return !this.isNewObject(); },
    canEdit: function () { return (this.parentModel ? this.parentModel.canEditChildren() : true) && (this.rowEditable ? this.rowEditable() : true) && this.options.editable;},
    canDelete: function () { return (this.parentModel ? this.parentModel.canDeleteChildren() : true) && (this.rowDeletable ? this.rowDeletable() : true) && this.options.editable;},
    canEditChildren: function () { return this.canEdit()},
    canAddChildren: function () { return this.canEdit() },
    canDeleteChildren: function(){return this.canEdit()},
    allowEdit: function (enableEdit) { this.options.editable = enableEdit; },

    set: function (field, value) {
        var that = this, newValue = value, originalFieldName = field,
            fieldDef;
            var timeZoneOffset = new Date().getTimezoneOffset();
            var hourOffset = Math.floor(timeZoneOffset / 60.0);
            var minuteOffset = timeZoneOffset - (hourOffset * 60.0);
            hourOffset = hourOffset > 0 ? 24 - hourOffset :  Math.abs(hourOffset);
            minuteOffset = minuteOffset > 0 ? 60 - minuteOffset : Math.abs(minuteOffset);
            fieldDef = this.fields && this.fields[field];

        if(fieldDef && sqx.commonActions && fieldDef.type === sqx.common.SFConstants.TYPE_FILE) {
            // add content document links to both model and main record
            sqx.commonActions.addLinkToFiles(this, sqx._fileUploadCache.getFile(value));
        }

        if (value instanceof Date && this.fields && this.fields[field] && this.fields[field].type == sqx.common.SFConstants.TYPE_DATE) {
            //always set the time portion of Date time to 12:00 mid-day, this will reduce date conversion issue
            // such as reduction of a day when converting to GMT
            // example: if time is before 5:45 NPT then GMT automatically becomes a day ahead.
            newValue = new Date(value.getFullYear(), value.getMonth(), value.getDate(), hourOffset, minuteOffset, 0);
            value = newValue; //update set value as well
        }

        if ((field.endsWith("__r"))) {
            originalFieldName = field.replace('__r', '__c');
        }

        //if value transfer map is present it is used
        var mapping = that.fields[originalFieldName] && that.fields[originalFieldName].additionalInfo && that.fields[originalFieldName].additionalInfo.map,
            fieldType = that.fields[field] && that.fields[field].type;

        if (mapping
            && ( fieldType === undefined || fieldType != sqx.common.SFConstants.TYPE_REFERENCE)) { //ignoring reference type because __r field will be setting a reference Id __c
                                                                                              // which then seems to cause a cycle if not ignored
            this._transferMappedValues(value, mapping);
        }

        kendo.data.Model.fn.set.call(that, field, newValue);

        if (that.fields[field]) { evaluateRules(that, field) }
    },
    _transferMappedValues: function (fromSource, mapping) {
        var that = this;
        $.each(mapping, function (index, map) {
            var valueToSet =  fromSource == null ? null : ( map.source === undefined ? fromSource : fromSource[map.source]),
            sourceDS, refField, dataBoundFunc;//copies self if no map source is provided, which might be the case if source is primitive

            //target can be related field too.
            try{
                //if map type is reference, map ID to the mapping field
                if(map.type == 'reference'){
                    refField = map.target.endsWith('__c') ? map.target.replace("__c", "__r") : map.target.replace('Id', '');
                    sourceDS = that[map.target + '_Source'];

                    if($.isFunction(sourceDS)){
                        sourceDS = sourceDS.call(map);
                    }

                    that.set(refField, null);

                    sourceDS.filter({field: 'Id', operator: 'eq', value: valueToSet});

                    dataBoundFunc = function(resp){
                        if(resp.response){
                            var item = resp.response.filter(function(e) { return e.Id == valueToSet} );
                            if(item.length == 1){
                                setTimeout(function() { that.set(refField, item[0]); }, 0);
                            }
                        }

                        sourceDS.unbind('requestEnd', dataBoundFunc);
                    }


                    sourceDS.bind('requestEnd', dataBoundFunc);

                }
                else if(map.type == 'eval' && $.isFunction(map.fn)){
                    that.set(map.target, map.fn.call(null, valueToSet, fromSource, that));
                }
                else{
                    if(valueToSet === undefined && sqx.common.isLightningScope()){
                        valueToSet = null;
                    }
                    that.set(map.target, valueToSet);
                }
            }
            catch(ex){
                sqx.common.error('Error occurred while mapping ' + map.target, ex);
            }

        })
    },
    editable: function (field) {
        field = (this.fields || {})[field];
        return field ? (kendo.isFunction(field.editable) ? field.editable(this) !== false : field.editable !== false) : true;
    }
});