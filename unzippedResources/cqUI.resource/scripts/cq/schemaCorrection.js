/**
* @author Pradhanta Bhandari
* @date 2014/3/14
* @description module for correcting the schema of the object
*/

var sqx = function (sqx) {
    "use strict";

    var SF_NAME_FIELD_MAX_LENGTH = 80,
        KENDO_GUID_LENGTH = 32,
        ALIAS = '_c',
        MAX_LENGTH = SF_NAME_FIELD_MAX_LENGTH - KENDO_GUID_LENGTH - 'compliancequest__'.length + 1; // extra 1 for spacing 

    sqx.schemaCorrection = {};
    sqx.schemaCorrection.correctSchema = function(schemaContainer){

        SQXSchema.Note.fields.ParentId.editable = true;

        SQXSchema.ContentDocumentLink.sObjectLabel = sqx.translation.get('common.file') || 'File';
        // added for translation support grid column using virtual field
        SQXSchema.ContentDocumentLink.fields.ContentTitle = {
            "name" : SQXSchema.ContentDocument.fields.Title.name,
            "editable": true,
            "additionalInfo" : { "trackChanges" : false }
        };
        SQXSchema.ContentDocumentLink.fields.ContentDescription = {
            "name" : SQXSchema.ContentDocument.fields.Description.name,
            "editable": true,
            "additionalInfo" : { "trackChanges" : false }
        };
        
        SQXSchema.ContentDocumentLink.fields.ContentDocumentId.editable = true;
        SQXSchema.ContentDocumentLink.fields.LinkedEntityId.editable = true;
        SQXSchema.ContentDocumentLink.Title = function() {
            return (this.ContentDocument && this.ContentDocument.Title) || '';
        }

        SQXSchema.ContentDocumentLink.Description = function() {
            return (this.ContentDocument && this.ContentDocument.Description) || '';
        }
        
        //add attachment
        SQXSchema.Attachment.fields.ParentId.editable = true;
        SQXSchema.Attachment.rowEditable = function(){ return this.canEdit;};
        SQXSchema.Attachment.rowDeletable = function(){ if(this.parentModel && this.parentModel.rowDeletable) return this.parentModel.rowDeletable(); return true;};
        SQXSchema.Attachment.dataLink = function() { return sqx.common.getFileDownloadUrl(this.Id); };

        SQXSchema.compliancequest__SQX_Containment__c.fields.compliancequest__Completion_Date__c.validation.required = true;
        SQXSchema.compliancequest__SQX_Investigation_Tool__c.fields.compliancequest__Investigation_Tool_Method_Used__c.validation.required = true;

        // this method returns the parent model of the note and attachment
        // since notes and attachment are shared across models the parentModel property may not behave properly
        SQXSchema.Note.parentEntity = 
        SQXSchema.Attachment.parentEntity = function(){
                  return sqx.dataStore.getObjectWithId(this.ParentId);
                };

        SQXSchema.ContentDocumentLink.parentEntity = function() {
            return sqx.dataStore.getObjectWithId(this.LinkedEntityId);
        }

        if (window.preAdjustSchema && $.isFunction(window.preAdjustSchema)){
            preAdjustSchema(schemaContainer);
        }  
        for(var objectType in schemaContainer){
            if(correction[objectType] !== undefined){
                correction[objectType](schemaContainer[objectType]);
            }                
            correction.commonCorrection(schemaContainer[objectType]);            
        }
        if (window.postAdjustSchema && $.isFunction(window.postAdjustSchema)){
            postAdjustSchema(schemaContainer);
        }  

        if(window.customPostSchemaCorrection){
            for (var objectType in schemaContainer) {
                if (customPostSchemaCorrection[objectType] !== undefined) {
                    customPostSchemaCorrection[objectType](schemaContainer[objectType]);
                }
            }
        }
    };
         

    function requiredObject(fieldDescriptor){
        return { message : fieldDescriptor.name + ' is required' };
    }
    

    //used to get name value for Id. Instead send all related object names initially
    function sfDataFor(relationName, references){
            
        return function(){
            var found = {data: sqx.dataStore.originalData[this[relationName]]};
                
                
            if(found.data === undefined){
                if(sqx.remote[references] !== undefined){
                    found.promise = sqx.remote[references].fetch();
                    found.remoteSource = sqx.remote[references];
                    found.objectId = this[relationName];
                }
                        
            }
                
            return found;
        };
    }
    
    /**
    * returns a  function that will return the data source for the given fieldtype and cache it in the objects fieldName
    */
    function getSourceFor(fieldName, fieldType, filter, useSearch) {
        return function(){
            var dataSource;
            if(useSearch) {
                if(!sqx.remote[fieldType + '_Search']) {
                    sqx.remote[fieldType + '_Search'] = sqx.remote.createDataSourceForObject(fieldType, ['Id', 'Name'], filter, undefined, undefined, true);
                }
                dataSource = sqx.remote[fieldType + '_Search'];
            }

            //check if a remote of the requested fieldType is present or not
            if (sqx.remote[fieldType] !== undefined || dataSource) {

                var sourceName = fieldName + '_Source';

                //if the object has its own property of the requested type then return the cached value
                if(this.hasOwnProperty(sourceName)){
                    return this[sourceName];
                }


                //if no such property is found, get the default(template) datasource from remote
                dataSource = dataSource || sqx.remote[fieldType];

                //if remote is a function evaluate it with the given filter
                if ($.isFunction(dataSource)) { dataSource = dataSource(filter) }

                //clone the template datasource
                dataSource = sqx.remote.cloneSource(dataSource);
                if(dataSource.options.transport){
                    dataSource.options.transport.relatedModel = this;
                    dataSource.options.transport.sourceIdentifier = fieldName;
                }

                //assign the data source as own property to cache, for further references from the object
                this[sourceName] = dataSource;

                return dataSource;
            }
            return null;
        };
    }

    function getPickListSourceFor(fieldName, picklistCategory){
        return function(){

            //check if a remote of the requested fieldType is present or not
            if (sqx.remote['pickListValues'] !== undefined) {
                
                var sourceName = fieldName + '_Source';

                //if the object has its own property of the requested type then return the cached value
                if(this.hasOwnProperty(sourceName)){
                    return this[sourceName];
                }


                //if no such property is found, get the default(template) datasource from remote
                var dataSource = sqx.remote.pickListValues(picklistCategory);

                //if remote is a function evaluate it with the given filter
                //if ($.isFunction(dataSource)) { dataSource = dataSource(filter) }

                //clone the template datasource
                dataSource = sqx.remote.cloneSource(dataSource);
                
                //assign the data source as own property to cache, for further references from the object
                this[sourceName] = dataSource;

                return dataSource;
            }
            return null;
        };
    }
    
    //common correction used to correct the schema when correction has to be applied accross the schema
    var correction = {
        
        commonCorrection : function(schema){
            
            schema.changeOwnerLink = function() { return '/' + this.Id + '/a?retURL=' + encodeURIComponent(window.location)} 
            schema.changeOwnerLinkHidden = function() { return !this.canEdit() || !sqx.common.isSFID(this.Id); }
            schema.toString = function () { return this.Name; };

            // Creates an alias for sobjectname if its length is greater than max permissible.
            if(schema.sObjectName.length > MAX_LENGTH){
                // ensure that namespace is found i.e. '__' is not of __c but of namespace prefix
                if(schema.sObjectName.indexOf('__') != schema.sObjectName.lastIndexOf('__')){
                    
                    schema.sObjectNameAlias = ALIAS + schema.sObjectName.substring(schema.sObjectName.indexOf('__'));

                    sqx.objAlias = sqx.objAlias || {};
                    sqx.objAlias[schema.sObjectNameAlias] = schema;
                
                }
            }

            // sets the setNullOnDelete to false on the child relationship so that attachments gets deleted when its parent is deleted
            var objectChildRelationship = schema.childRelationships || [];
            for(var index = 0; index < objectChildRelationship.length; index++){
                if(objectChildRelationship[index].childSObject == 'Attachment'){
                    objectChildRelationship[index].setNullOnDelete = false;
                    break;
                }
            }

            schema.fields.CreatedDate = schema.fields.CreatedDate || { type: "datetime", editable: true, name: cq.labels.common.createddate || 'Created Date'};

            schema.fields["CreatedById"]={
                "validation": {},
                "type": "reference",
                "nullable": null,
                "name": "Created By",
                "editable": false,
                "additionalInfo": {
                    "apiname": "CreatedById",
                    "referenceTo": "User"
                }
            };

            schema.fields["LastModifiedById"]={
                "validation": {},
                "type": "reference",
                "nullable": null,
                "name": "Modified By",
                "editable": false,
                "additionalInfo": {
                    "apiname": "LastModifiedById",
                    "referenceTo": "User"
                }
            };

            schema.fields.isRequired = {  additionalInfo : { trackChanges: false } };
            for(var field in schema.fields){
                var selectedField = schema.fields[field];
                if (field == 'Id'){
                    selectedField.editable = true;
                }

                if(field == 'LastModifiedDate'){
                    schema.fields.LastModifiedDate.editable = false;
                    schema.fields.LastModifiedDate.nullable = true;
                }

                //if the field is of reference type, add <FieldName>_Source field to the schema, 
                //which is the datasource to be used as the source for the list of object.
                if(selectedField.type ==  sqx.common.SFConstants.TYPE_REFERENCE){
                    //schema[relationName] = sfDataFor(relationName, selectedField.additionalInfo.referenceTo);    
                    var relationName = field.replace('__c', '__r');
                    schema[relationName] = null; //prop will be set by combobox, need it to be null in the beginning to not attempt any mapping
                    if(selectedField.additionalInfo.referenceTo !== undefined){
                        schema[field + '_Source'] = getSourceFor(field, selectedField.additionalInfo.referenceTo, selectedField.additionalInfo.filter, selectedField.additionalInfo.useSearch);
                         selectedField.additionalInfo.map = [
                            { "source": "Id", "target": selectedField.additionalInfo.apiname }
                         ]

                    }
                }
                else if(selectedField.type == sqx.common.SFConstants.TYPE_PICKLIST){
                    //if field is of picklist type add <FieldName>_ValidValues to the schema,
                    //which is used to create the dropdown.
                    if(selectedField.additionalInfo !== undefined &&
                       selectedField.additionalInfo.validValues !== undefined){
                        var newFieldName = field + '_ValidValues',
                            defaultSort = { field: 'text', dir: 'asc' },
                            sort = selectedField.additionalInfo.sort ? 
                                selectedField.additionalInfo.sort : defaultSort; //sort is added to alter picklist default sort by field text

                        //if valid values has been overriden then leave the datasource
                        if(!schema[newFieldName]){

                            schema[newFieldName] = new kendo.data.DataSource({
                                data: schema.fields[field].additionalInfo.validValues,
                                sort: sort
                            });

                            //review: revisit need to expose at top level instead of field
                            //field level prop required to make values available to grid column definition
                            selectedField.validValues = schema[newFieldName];                         
                            schema[newFieldName].fetch();
                        }
                    }
                }
                else if(selectedField.type == sqx.common.SFConstants.TYPE_PICKLIST_OBJECT){
                    var relationName = field.replace('__c', '__r');
                    schema[relationName] = null; //prop will be set by combobox, need it to be null in the beginning to not attempt any mapping
                    if(selectedField.additionalInfo.picklistCategory !== undefined){
                        schema[field + '_Source'] = getPickListSourceFor(field, selectedField.additionalInfo.picklistCategory);

                    }
                }
            }
            
            /**
            * returns user edit access on a record
            */
            schema.hasRecordEditAccess = function() {
                var ds = sqx.dataStore.userRecordAccess;
                return !(ds && ds[this.Id]) || ds[this.Id].HasEditAccess || "All" === ds[this.Id].MaxAccessLevel;
            }
            /**
            * returns user delete access on a record
            */
            schema.hasRecordDeleteAccess = function() {
                var ds = sqx.dataStore.userRecordAccess;
                return !(ds && ds[this.Id]) || ds[this.Id].HasDeleteAccess || "All" === ds[this.Id].MaxAccessLevel;
            }

        }
     
    };
    
    
    return sqx;
}(sqx || {});

