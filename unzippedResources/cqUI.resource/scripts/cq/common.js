/**
* @author Pradhanta Bhandari
* @date 2014/3/14
* @description module for common operations by SQX, such as logging, constant definition etc.
*/
var mainRecord = mainRecord || {}, formRules = formRules || {};

var sqx = (function (sqx) {
    "use strict";

    var lightingScopeCache = undefined;

    /**
     * Stores common methods that are to be used to perform cross-cutting actions such as
     * showing validation error, hiding/showing loading indicator etc.
     */
    sqx.common = {
        /**
         * Method to show loading indicator in the ui.
         */
        showLoading : function(){
            window.showLoading.apply(null, arguments);
        },
        /**
         * Method to hide loading indicator in the ui.
         */
        hideLoading : function() {
            window.hideLoading.apply(null, arguments);
        },
        /**
         * Method to show form error in kendo ui.
         */
        setFormError : function(){
            window.setFormError.apply(null, arguments);
        },
        /**
         * Returns whether or not the UI is being displayed in lightning scope
         */
        isLightningScope: function() {
            if(lightingScopeCache === undefined) {
                lightingScopeCache = $('.slds-scope').length > 0;
            }
            return lightingScopeCache;
        }
    };

    // Changed DOM element used for binding and triggering events, because Salesforce lightning
    // doesn't work correctly with document DOM element.
    var EVENT_DOM_ELEMENT = $('<div />');

    sqx.Events = {
        /**
        * POSTINIT : Actions to be performed after initializing the record
        * PREINIT : Actions to be performed before initializing the record
        * PREINITNEWRECORD : Actions to be performed before initializing the new record
        * POSTINITNEWRECORD : Actions to be performed after initializing the new record
        * POSTINITNEWRECORD : Actions to be performed before initializing the tabstrip of the record
        * COMPLETINGTABSTRIP : Actions to be performed after initializing the tabstrip of the record
        */
        POSTINIT : 'postInit',
        PREINIT : 'preInit',
        PREINITNEWRECORD : 'preInitNewRecord',
        POSTINITNEWRECORD : 'postInitNewRecord',
        INITIATINGTABSTRIP : 'intiatingTabStrip',
        COMPLETINGTABSTRIP : 'completingTabStrip',
        trigger : function(eventName, params){
            EVENT_DOM_ELEMENT.trigger(eventName, params);
        },

        on: function(eventName, callback){
            EVENT_DOM_ELEMENT.on(eventName, callback);
        }
    };

    $(function(){
        var dateFormat = UserContext.dateFormat;
        var userContextDateFormat = (dateFormat && (dateFormat.match(/y/g)).length == 1) ? dateFormat.replace('y', 'yyyy') : dateFormat; // replaced y with yyyy as kendo does not support y which is the year for the pakistan locale

        sqx.common.CQDateFormat = window.customDateFormat || userContextDateFormat || 'dd-MMM-yyyy';

        var timeFormat = window.customTimeFormat || UserContext.timeFormat || 'h:mm:ss tt';

        /*
        * replaced 'a' with 'tt' because for the AM/PM indication java uses 'a' whereas kendo uses 'tt'
        * Java doc: https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
        * Kendo doc: http://docs.telerik.com/kendo-ui/framework/globalization/dateformatting
        */
        sqx.common.CQTimeFormat = timeFormat.includes('a') ? timeFormat.replace('a', 'tt') : timeFormat;

        sqx.common.CQDateTimeFormat = sqx.common.CQDateFormat + ' ' + sqx.common.CQTimeFormat;

        if(window.kendo){
            // customNumberDecimalFormat: used for separating decimals in the numbers for eg. 100.00
            if(window.customNumberDecimalFormat){
                window.kendo.culture().numberFormat['.'] = window.customNumberDecimalFormat;
            }

            // customNumberGroupFormat: used for separating group in the numbers for eg. 100,000
            if(window.customNumberGroupFormat){
                window.kendo.culture().numberFormat[','] = window.customNumberGroupFormat;
            }
        }
    });

    /**
    * @description logs the list of messages (param), sent to this function
    */
    sqx.common.log = function (messages/*, messagen*/) {

        if(console !== undefined && console.log !== undefined){

            for(var count = 0, length = arguments.length; count < length; count++){
                    console.log(arguments[count]);
            }
        }
    };

    /**
    * @description debug logs the list of messages (param), has been separated from log, because on
    *               final deployment all references to debug log can be removed
    */
    sqx.common.debugLog = function (message /*, message n */ ) {
        if(console !== undefined && console.debug !== undefined){

            for(var count = 0, length = arguments.length; count < length; count++){
                    console.debug(arguments[count]);
            }
        }
    };

    /**
    * @description debug logs the list of messages (param), has been separated from log, because on
    *               final deployment all references to debug log can be removed
    */
    sqx.common.error = function (message /*, message n */ ) {
        if(console !== undefined && console.error !== undefined){

            for(var count = 0, length = arguments.length; count < length; count++){
                    console.error(arguments[count]);
            }
        }
    };

    /**
    * @description info logs the list of messages (param), has been separated from log, because on
    *               final deployment all references to info log can be removed
    */
    sqx.common.toDo = function (message /*, message n */ ) {
        if(console !== undefined && console.info !== undefined){

            for(var count = 0, length = arguments.length; count < length; count++){
                    console.info('To Do:' + arguments[count]);
            }
        }
    };

    /**
    * @description asserts and displays error on error
    * assertion must be done using the or's lazy evaluation
    * so that no evaluation of message is done prior to assertion failure
    * example sqx.common.assert( (condition) || 'error message')
    */
    sqx.common.assert = function (message /*, message n*/) {
        if(console !== undefined){
            for(var count = 0, length = arguments.length; count < length; count++){
                    if(arguments[count] !== true){
                        throw (message);
                    }
            }
        }

    };

    /**
    * @description returns true if it is valid sf id else returns false
    */
    sqx.common.isSFID = function(id){
        return id != null && id.length == 18;
    };

    /**
    * @description returns the url for the object in salesforce
    */
    sqx.common.getObjectUrl = function(id){
        return '../' + id;
    };

    /**
    * @description
    */
    sqx.common.getFileDownloadUrl = function(id){
        return '../servlet/servlet.FileDownload?file=' + id;
    };

    /**
    * @description returns document compare url if doc compare package is available
    * @param basedocid          id of the document to compare
    * @param comparewithdocid   id of the document to compare with
    * @return returns document compare url
    */
    sqx.common.getDocumentCompareUrl = function(basedocid, comparewithdocid) {
        var isAvailable = window.isDocComparePackagePageAvailable,
            url = window.sqxDocumentComparePage;
        
        if (isAvailable && url && sqx.common.isSFID(comparewithdocid) && (basedocid == null || sqx.common.isSFID(basedocid))) {
            url += "?comparewithdocid=" + comparewithdocid;
            if (basedocid) {
                url += "&basedocid=" + basedocid;
            }
            return url;
        }
        return "";
    };

    /*
    * @author: Anish Shrestha
    * @date: 2014/11/24
    * @description: check if the device is mobile or PC
    */
     sqx.common.isMobile = function(){
         var is_mobile = false;

        if( $('#checkMobile').css('display') === 'none' && isUserThemeMobile === 'true') {
            is_mobile = true;
        }

        return is_mobile;
     };

    sqx.common.SFConstants = {
        TYPE_STRING : 'string',
        TYPE_PICKLIST : 'values',
        TYPE_NUMBER : 'number',
        TYPE_DATE : 'date',
        TYPE_BOOLEAN :'boolean',
        TYPE_REFERENCE :'reference',
        TYPE_DATE_TIME : 'datetime',
        TYPE_PICKLIST_OBJECT : 'picklistobj', //picklist value from picklist object
        TYPE_FILE : 'file'
    };

    //TODO: Identify purpose of two different constants
    sqx.common.Constants =
    {
        TYPE_STRING : 'string',
        TYPE_PICKLIST : 'picklist',
        TYPE_NUMBER : 'number',
        TYPE_DATE : 'date',
        TYPE_DATE_TIME : 'datetime'
    };

    return sqx;
}(sqx || {}));

