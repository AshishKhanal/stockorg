/**
* excel sheet provider
*/

var sqx=(function(sqx, $){
    "use strict";

    var excel = function(controller, mainRecord){
        this.controller = controller;
        this.mainRecord = mainRecord;
    }

    /**
    * This method checks out a audit
    */
    excel.prototype.checkout = function(param, electronicSignatureData)
    {
        var that = this;
        return new Promise(function(resolve,reject){
            that.controller.checkOut(that.mainRecord.Id, '', [], param, electronicSignatureData, null,function(data,event){
                if(event.status) { 
                    resolve(data);
                }else {
                    reject(event)
                }
            });
        });
    }

    sqx.spreadsheet =  sqx.spreadsheet || {};
    sqx.spreadsheet.Excel = excel;

    return sqx;
}(sqx || {}, jQuery))