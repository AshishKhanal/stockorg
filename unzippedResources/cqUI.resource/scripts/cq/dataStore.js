/**
* @author Pradhanta Bhandari
* @date 2014/3/14
* @description module for data store operations by SQX, such as getting elements in the datastore, and getting changes
*/
/*define(["scripts/sqx/common.js",
        "scripts/kendo/kendo.web.js" / *,
        "scripts/kendo/kendo.data.js",
        "scripts/kendo/kendo.grid.js",
        "scripts/kendo/kendo.window.js"* / ], function () {
    "use strict";*/

    var sqx = (function (sqx, kendo) {
        "use strict";

        var CONTENT_DOCUMENT = 'ContentDocument',
            CONTENT_DOCUMENT_LINK = 'ContentDocumentLink',
            REL_CONTENT_DOCUMENT_LINK = 'ContentDocumentLinks',
            SOBJTYPE_NOTE = 'Note',
            SOBJTYPE_ATTACHMENT = 'Attachment',
            REL_NOTES = 'Notes',
            REL_NOTES_AND_ATTACHMENTS = 'NotesAndAttachments',
            REL_NOTES_ATTACHMENTS_LINKS = 'NotesAndAttachmentsAndDocLinks',
            REL_NOTES_LINKS='NotesAndDocLinks',
            REL_ATTACHMENTS = 'Attachments';

        sqx.dataStore = sqx.dataStore || {};

        sqx.dataStore.originalData = null;

        sqx.dataStore.userRecordAccess= null;

        var ken_do = kendo; //local reference of kendo
        var delta = { deleted: [], modified: [], version: 0 }; //stores the deleted objects and modified objects
        var deltaMap = {}; //used to hash the objects within
        var storeDelta = false;
        var logged = {};
        var dsList = {},
            prefixMatrix = {},
            logger = sqx.common;

        sqx.dataStore._delta = delta;
        sqx.dataStore._deltaMap = deltaMap;

        // this object maintains the count of
        var objectCount = new kendo.data.ObservableObject({}),
            compositeCounts = {};


        /**
        * Adds a type of composite count i.e. a count that depends on more than one object.
        * @param fieldName the name of composite count field
        * @param objectTypes the array of object that the count depends on
        * @param callback if the count is anything else than simple aggregate the callback returns the count
        * @param maxCount the maxCount for composite count after which N+ is returned
        */
        sqx.dataStore.createCompositeCount = function(fieldName, objectTypes, callback, maxCount){
            var countEntry = {  fieldName : fieldName,
                                maxCount : maxCount || 1, // default to 1+
                                callback : callback,
                                objectTypes : {}},
                idx = 0;
            // convert array to object for quicker search later on
            for(;idx < objectTypes.length; idx++){
                countEntry.objectTypes[objectTypes[idx]] = true;
            }

            compositeCounts[fieldName] = countEntry;

            _refreshTotal(countEntry);
        }

        /**
        * updates the total of the composite count field using either the callback or the aggregate count
        */
        function _refreshTotal(compositeCount){
            var total, objName;

            if(compositeCount.callback){
                total = compositeCount.callback.call();
            }
            else{
                total = 0;
                for(objName in compositeCount.objectTypes){
                    // NOTE: we are expecting the composite count to depend on simple object type count only
                    // depending on composite field might not return the correct count
                    total += objectCount.get(objName) || 0;
                }
            }

            if(compositeCount.maxCount != -1 && total > compositeCount.maxCount)
                total = compositeCount.maxCount + '+';

            objectCount.set(compositeCount.fieldName, total);
        }

        /**
        * Internal method that updates the count of composite fields
        */
        function _updateCompositeCounts(changedObjectType){
            var idx, cc;

            for(idx in compositeCounts){
                cc = compositeCounts[idx];
                if(cc.objectTypes[changedObjectType] === true){
                    _refreshTotal(cc);
                }
            }
        }

        /**
        * updates the count of individual objectType
        */
        function _updateCount(objType){
            var dsListEntry = dsList[objType],
                total;

            total = (dsListEntry && dsListEntry.source.length) || 0;

            objectCount.set(objType, total);
        }

        /**
         * Creates content document objects from content document links that have been serialized to the UI.
         * @param {*} links the list of content document links that need to be converted to content docs.
         */
        function addContentDocumentsFromContentDocLink(links) {
            var contents = [], contentIds = {}, link, content;
            for (var index = 0; index < links.length; index++) {
                link = links[index];
                if(contentIds[link.ContentDocumentId] === undefined) {
                    content = { Id: link.ContentDocument.Id,
                                Title: link.ContentDocument.Title,
                                Description: link.ContentDocument.Description,
                                attributes: {type: CONTENT_DOCUMENT}
                            };
                    contents.push(content);
                    contentIds[link.ContentDocumentId] = true;
                }
            }

            return contents;
        }

        sqx.dataStore.objectCount = objectCount;

        /**
         * Adds item to the original list so that it is used as baseline when comparing and generating the changeset.
         * @param objType {String} the sobject type of the object
         * @param record {Object} the object representing the sobject.
         */
        sqx.dataStore.addToOriginal = function(objType, record) {
            var dsListEntry = dsList[objType] && dsList[objType],
                recordIndex,
                clonedRecord,
                obs;

            if(dsListEntry) {
                var ds = createDataStore(dsListEntry.source, dsListEntry.model, {});
                obs = ds.add(record);

                this.originalData[record.Id] = { raw: JSON.parse(JSON.stringify(record)), "obs": obs};
                ds.sync();
            }
            else {
                logger.error('Invalid data source');
            }
        }

        /**
        * @author Pradhanta Bhandari
        * @date 2014/3/14
        * @description this function initializes the data store with the supplied data
        */
        sqx.dataStore.initialize = function (data, schemaContainer, options, deltaSet) {

            var that = this,
                objectType,
                dsListEntry,
                objectStore, newMainRecord;

            storeDelta = false; //don't store delta when initializing

            sqx.common.debugLog('Initializing data store');

            this.originalData = {};
            this.schemaContainer = schemaContainer || {};
            delta = {};

            delta.deleted = (deltaSet && deltaSet.deleted) || [];
            delta.modified = (deltaSet && deltaSet.modified) || [];
            delta.version = (deltaSet && deltaSet.version) || 0;
            sqx.schemaCorrection.correctSchema(SQXSchema);

            //update the deltaMap because it is used to quicken the search later on
            //if the map is not updated if a record is saved and later on when an update is done duplicate objects are created
            //because it can't be found in deltamap
            for(var dI = 0, dL = delta.modified.length; dI < dL; dI++){
                deltaMap[delta.modified[dI].Id] = delta.modified[dI]; //this is equivalent to constructing a hash map which will be used later on
            }
            //now load the data; ensuring that the data is not null.
            sqx.common.assert((data !== null) || 'An empty store can not be initialized.');

            //we expect record set bundles
            sqx.common.assert((data.RecordSetBundles !== null) || 'Record set bundle can not be null');

            sqx.dataStore.userRecordAccess = data.RecordAccess;

            //convert all data sent to observable arrat. this will be master datasource for each object. When refering through object navigation, a new data source is created for each child. This may cause perf issue on client side, need to watch out for it
            var recordSetBundles = data.RecordSetBundles.slice(0),
                recordSetBundle,
                objectType;
            while (recordSetBundles.length > 0) {
                recordSetBundle = recordSetBundles.splice(0,1)[0];
                objectType = recordSetBundle.ObjectType;

                if(objectType === CONTENT_DOCUMENT_LINK) {
                    var contentDocuments = addContentDocumentsFromContentDocLink(recordSetBundle.Records);
                    recordSetBundles.push({ ObjectType : CONTENT_DOCUMENT, Records : contentDocuments });
                }

                dsListEntry = {
                    source: new kendo.data.ObservableArray(recordSetBundle.Records),
                    model: schemaContainer[objectType]
                };

                //add an entry to prefix type, by getting the 1st 3 characters of SF ID, which is the prefix
                if(recordSetBundle.Records.length > 0){
                    sqx.common.assert(sqx.common.isSFID(recordSetBundle.Records[0].Id) || 'Could not find a valid SF ID while initializing the data store');
                    prefixMatrix[recordSetBundle.Records[0].Id.substr(0,3)] = objectType;
                }

                dsList[objectType] = dsListEntry;

                try {
                    //add all existing records to originalData store
                    for (var recordIndex = 0, recordLength = recordSetBundle.Records.length; recordIndex < recordLength; recordIndex++) {
                        var currentRecord = recordSetBundle.Records[recordIndex];

                        sqx.common.assert(currentRecord.Id !== undefined || 'Expected Id in object but found it missing in ' + $.getJSON(currentRecord));
                        //AR, store reference to both raw and observable obj in originalData, needed to simplyfy changeset merge
                        this.originalData[currentRecord.Id] = { raw: currentRecord, obs: dsListEntry.source[recordIndex] };
                    }
                }
                catch (ex) {
                    sqx.common.log(ex);
                }
            }


            //update all records using delta set, applying changes
            if (delta !== undefined && delta.modified !== undefined) {
                for (var mindex = 0, mlength = delta.modified.length; mindex < mlength; mindex++) {
                    var moddedObject = delta.modified[mindex];
                    /*
                    * Merging of changeset to persistent data uses the following algorithm
                    * -------------------
                    *
                    * IF the delta object's Id isn't Salesforce Id i.e. it hasn't been persisted in Salesforce
                    *       Add the object directly to the modified list (Case 1)
                    * ELSE
                    *       IF it is present in the main set returned by the server
                    *           find the corresponding object returned by the server and update the object with the changes (Case 2.1)
                    *       ELSE
                    *           Add the object directly to the modified list (Case 2.2)
                    *
                    *
                    * Case 1: is usual for objects that haven't been submitted in responses or haven't been persisted.
                    *         Example: Adding containment, investigation for the first time in response screen
                    *
                    * Case 2.1: is usual for objects that have been persisted but the changes haven't been submitted.
                    *         Example: Modifying implementation submitted as action plan in investigation.
                    *                  Or Modifying recalled containments, investigations etc.
                    *
                    * Case 2.2: is usual for unsubmitted attachments, which is always saved in Salesforce under TempStoreAttachments.
                    *           The attachments under temp store aren't returned by server in the main change set.
                    *
                    */
                    if (sqx.common.isSFID(moddedObject.Id) === false || this.originalData[moddedObject.Id] === undefined) {

                        // Case 1 and Case 2.2

                        var dsListEntry = _getDsListEntryFor(schemaContainer, moddedObject.attributes.type);

                        sqx.common.assert(dsListEntry != null || 'Expected ds list to be found for ' + moddedObject.attributes.type + ' but was missing.')
                        dsListEntry.source.push(moddedObject);

                        deltaMap[moddedObject.Id] = moddedObject;
                    }
                    else {
                        // Case 2.1
                        var mainObject = this.originalData[moddedObject.Id].obs;
                        $.extend(mainObject, moddedObject);
                    }
                }
            }


            //start recording the delta if asked
            storeDelta = (options && options.recordChanges) || false;
            //create datasource for main record, rest will be created on demand
            dsListEntry = dsList[mainRecordType];
            objectStore = createDataStore(dsListEntry.source, dsListEntry.model);
            this.ContentDocStore = createDataStore(this.getObjectsWithType(CONTENT_DOCUMENT), schemaContainer[CONTENT_DOCUMENT]);

            if ((objectStore.data().length == 0)) {
                newMainRecord = objectStore.add();
                if (window.initializeNewMainRecord && $.isFunction(initializeNewMainRecord) && newMainRecord) {

                    var events = sqx.Events;
                    events.trigger(events.PREINITNEWRECORD,[newMainRecord]);

                    initializeNewMainRecord(newMainRecord);

                    events.trigger(events.POSTINITNEWRECORD, [newMainRecord]);
                };
            }
            that[mainRecordType] = objectStore;
            //allow form to initialize data as needed if new record
            mainRecord = sqx.dataStore[mainRecordType].at(0); //(0)has to exist by now

            // Update count
            for(var objType in schemaContainer){
                _updateCount(objType);
            }



            sqx.common.debugLog('Data store initialized');

            /**
            * dispatches a call to datastoreinitalized if defined
            */
            if(window.dataStoreInitialized !== undefined){
                window.dataStoreInitialized();
            }

            if(mainRecord && compositeCounts[REL_NOTES_AND_ATTACHMENTS] === undefined){
                // add notes and attachments count by default
                sqx.dataStore.createCompositeCount(REL_NOTES_AND_ATTACHMENTS, [SOBJTYPE_NOTE,SOBJTYPE_ATTACHMENT], function(){
                    return mainRecord.relatedList(REL_NOTES).view().length + mainRecord.relatedList(REL_ATTACHMENTS).view().length;
                });

                sqx.dataStore.createCompositeCount(REL_NOTES_ATTACHMENTS_LINKS,  [SOBJTYPE_NOTE,SOBJTYPE_ATTACHMENT, CONTENT_DOCUMENT_LINK], function() {
                    return mainRecord.relatedList(REL_NOTES).view().length + mainRecord.relatedList(REL_ATTACHMENTS).view().length
                    + mainRecord.relatedList(REL_CONTENT_DOCUMENT_LINK).view().length;
                });

                sqx.dataStore.createCompositeCount(REL_NOTES_LINKS,  [SOBJTYPE_NOTE, CONTENT_DOCUMENT_LINK], function() {
                    return mainRecord.relatedList(REL_NOTES).view().length + mainRecord.relatedList(REL_CONTENT_DOCUMENT_LINK).view().length;
                });
                

            }


        };

        /*
            creates a new data store for a model with given options
        */
        function createDataStore(dataContainer, model, options) {

            var dataIndex, dataLength;
            if (!options) options = {};

            var dsOptions = {
                transport: {
                    /**
                    * returns a copy of data that has been persisted in the server and doesn't have cycle
                    * since we have reference to the source and source has reference back to model it causes a cycle
                    * Having cycles causes the system to fail when deep cloning or serializing the data
                    *
                    * The assumption here is that the fields are found in SQXSchema object.
                    */
                    _cleanData : function(data){
                        var cleanedUp = data, fields;


                        if(data.attributes.type && window.SQXSchema){
                            fields = window.SQXSchema[data.attributes.type] && window.SQXSchema[data.attributes.type].fields;

                            if(fields){
                                cleanedUp = {};

                                for(var key in data){
                                    if(fields[key]){
                                        cleanedUp[key] = data[key];
                                    }
                                }

                            }
                        }

                        return cleanedUp;
                    },
                    read: function (options) {
                        var that = this;
                        if (that.parentModel) {
                            options.success(dataContainer);
                            //use same base observable array otherwise there won't be a single observable array
                            //options.success($.grep(dataContainer, function (data, count) { return data[that.parentCol] === that.parentId }));
                        } else {
                            options.success(dataContainer);
                        }
                    },
                    update: function (options) {
                        sqx.common.debugLog('Updating ' + options.data.attributes.type);
                        sqx.dataStore._onDataChange('update', options.data);

                        options.success([this._cleanData(options.data)]);
                    },
                    create: function (options) {
                        sqx.common.debugLog('Creating ' + options.data.attributes.type);
                        sqx.dataStore._onDataChange('create', options.data);

                        options.success([this._cleanData(options.data)]);

                    },
                    destroy: function (options) {
                        sqx.common.debugLog('Destroying ' + options.data.attributes.type);
                        sqx.dataStore._onDataChange('delete', options.data);

                        options.success([this._cleanData(options.data)]);

                    }, parentModel: options.parentModel, parentCol: options.parentCol, parentId: options.parentId
                },
                schema: { model: model },
                batch: false,
                autoSync: options.autoSync || false,
                sync: function(e){
                    var objType = e.sender.options.schema.model.fn.sObjectName;

                    if(objType){
                        _updateCount(objType);
                        _updateCompositeCounts(objType);
                    }
                },
                change: function (e) {
                    var itemIndex = 0,
                        itemLength = e.items.length, changedField = null;

                    if(e.action == 'itemchange' && e.field != null ){

                        for(;itemIndex < itemLength; itemIndex++){

                            /*
                            * some objects don't have the impacted field in them, so ignoring the changes when
                            * such an event occurs.
                            * One know condition when this happens is when a datasource is a field of the object
                            * All the loaded items are sent as items to the event.
                            */
                            if(e.items[itemIndex].fields == null){
                                break;
                            }

                            changedField = e.items[itemIndex].fields[e.field];
                            if(changedField && changedField.additionalInfo){

                                var ignoreChanges = changedField.additionalInfo.trackChanges === false;
                                if(!ignoreChanges){
                                    $('body').trigger('change'); //for field change trigger change only if the field is being tracked for changes.
                                    break;
                                }
                            }
                        }
                    }
                    else if(e.action=='add' || e.action=='remove' || e.action=='itemchange'){
                        //for all other event types such as create destroy trigger change
                         $('body').trigger('change'); //let views react if there is any change to data E.g. set dirty
                    }


                },
                sort: { field: "CreatedDate", dir: "desc" }
            };

            // override the sort definition with custom sort specified in the schema
            dsOptions.sort = (model && model.sort) || dsOptions.sort;

            if (options) {
                $.extend(true, dsOptions, options);
            }

            var ds = new CQDataSource(dsOptions);
            ds.fetch();

            return ds;
        }
        sqx.dataStore.createStore = createDataStore;

        /**
        * gets the dataStore of the object
        * @param schemaContainer schema containing all the objects
        * @param objectType name of the object for which dataStore is to be created
        */
        sqx.dataStore.getStore = function(schemaContainer, objectType, options){
            var dsListEntry = _getDsListEntryFor(schemaContainer, objectType);
            return createDataStore(dsListEntry.source, dsListEntry.model, options);
        }

        /**
        * compares two objects and returns a new object containing only modified fields of the first one
        * along with the original values set to the data item.
        * @param sourceObject is the original value
        * @param targetObject is the changed object
        */
        function getOnlyModifiedFields(targetObject, sourceObject) {
            var deltaObject = {},
                sfPersistedObject = sqx.common.isSFID(targetObject.Id),
                somethingChanged = sourceObject ? false : true, //if there is no source object than always return the new deltaObject object
                objectType = targetObject.attributes.type,
                schemaOfObject = sqx.dataStore.schemaContainer[objectType];

            if (sourceObject) {
                deltaObject.originalValues = {};
            }

            for (var field in targetObject) {

                if (schemaOfObject.fields[field] === undefined || (schemaOfObject.fields[field].editable === false &&
                     (schemaOfObject.fields[field].additionalInfo !== undefined &&  schemaOfObject.fields[field].additionalInfo.IncludeInChangeSet !== true )))
                    continue; //ignore fields if it is not in schema or is not editable

                //if it is editable see if trackchanges is false
                if(schemaOfObject.fields[field].additionalInfo !== undefined
                   && schemaOfObject.fields[field].additionalInfo.trackChanges !== undefined
                   && schemaOfObject.fields[field].additionalInfo.trackChanges === false){
                    continue;
                }

                if (sourceObject) {
                    if (sourceObject[field] != targetObject[field]) {
                        //todo:remove after adding date parser
                        if (targetObject[field] instanceof Date) {
                            //date field requires special treatment
                            //because original will have string value, changed will have date value
                            var parsedDate = ken_do.parseDate(sourceObject[field]);
                            if (parsedDate != null && parsedDate.getTime() === targetObject[field].getTime())
                                continue;
                        }
                        deltaObject[field] = targetObject[field];
                        deltaObject.originalValues[field] = sourceObject[field] !== undefined ? sourceObject[field] : null; //stores null if no original value is found
                        somethingChanged = true;
                    }
                }
                else {
                    deltaObject[field] = targetObject[field];
                }
            }

            if(!sfPersistedObject) {
                //note: This is a workaround to support autosync enabled source. We will
                // need to add test support for datastore before refactoring
                delete deltaObject.originalValues;
            }

            if (somethingChanged === false)
                return null;


            deltaObject.attributes = {};
            $.extend(deltaObject.attributes, targetObject.attributes);

            //always copy the Id
            deltaObject.Id = targetObject.Id;




            return deltaObject;
        }

        /**
         * Checks whether or not a record has been locally saved i.e. captured in the change set.
         * @returns true if it has been persisted locally, else returns false
         */
        sqx.dataStore.isCommitted = function(id) {
            return deltaMap[id] !== undefined;
        }

        /**
         * Internal method to add existing item to changed data set.
         * @param {object} item - the item that has been modified
         * @param {object} existingItem - the item that exists in the changed data set.
         */
        sqx.dataStore._addExistingItemToMap = function(item, existingItem) {

            var itemIndex = 0, itemLength = 0, toDelete = null, changedObject;

            //if there is an existing item in salesforce things are complicated
            if (sqx.common.isSFID(item.Id) && !(item.ParentId && !sqx.common.isSFID(item.ParentId))) {
                //1. get the modified object
                changedObject = getOnlyModifiedFields(item, !this.originalData[item.Id] || this.originalData[item.Id].raw);

                //2. if the object is null meaning no changes were found
                if(changedObject == null){
                    //just remove the previously added object from change set and delta map
                    delta.modified.remove(existingItem);
                    delete deltaMap[item.Id];
                    return;
                }

                //3. check if some fields are that are in existing change set are removed (meaning there was no change i.e. undone)
                toDelete = [];
                for(var field in existingItem){
                    if(changedObject[field] === undefined){
                        toDelete.push(field);
                    }
                }

                //4. delete all undone fields, from both original val and current val
                for(itemIndex = 0, itemLength = toDelete.length; itemIndex < itemLength; itemIndex++){
                    delete existingItem[toDelete[itemIndex]];
                    delete existingItem.originalValues[toDelete[itemIndex]];
                }

                //5. copy all other values
                $.extend(existingItem, changedObject);
            }
            else {
                //if it is a temporary object getonlymodifiedfields returns everything, so it simply overwrites any such issues
                $.extend(existingItem, getOnlyModifiedFields(item));
            }
        }

        /*
        * actual function that records the delta of the changes, assumes single item
        */
        sqx.dataStore._onDataChange = function (eventtype, item, changedField) {
            var changedObject = null;
            var existingItem;

            switch (eventtype) {
                case 'create':
                    existingItem = deltaMap[item.Id];
                    // create can be triggered multiple times for same object, so we need to ensure
                    // no duplicates are being stored
                    if(existingItem) {
                        this._addExistingItemToMap(item);
                    }
                    else {
                        changedObject = getOnlyModifiedFields(item);
                        delta.modified.push(changedObject);
                        deltaMap[item.Id] = changedObject;
                    }
                    delta.version++;
                    break;

                case 'update':
                    existingItem = deltaMap[item.Id];
                    if (existingItem === undefined) {

                        //if no existing item exists, while updating create one
                        changedObject = getOnlyModifiedFields(item, !this.originalData[item.Id]|| this.originalData[item.Id].raw);
                        if(changedObject != null){
                            delta.modified.push(changedObject);
                            deltaMap[item.Id] = changedObject;
                        }
                    }
                    else {
                        this._addExistingItemToMap(item, existingItem);
                    }
                    delta.version++;
                    break;

                case 'delete':

                    if (sqx.common.isSFID(item.Id))
                        delta.deleted.push(item);

                    //remove from array; can't avoid loops here
                    for (itemIndex = 0, itemLength = delta.modified.length; itemIndex < itemLength; itemIndex++) {
                        if (delta.modified[itemIndex].Id == item.Id) {
                            delta.modified.splice(itemIndex, 1);
                            break;
                        }
                    }
                    delete deltaMap[item.Id];
                    delta.version++;
                    break;
            }
        };

        /*
        * getting the changeset will synchronize all sources by default, unless explicitly told
        */
        sqx.dataStore.getChangeSet = function (dontsynchronize) {

            if (dontsynchronize !== true) {
                //synchronize all data
                sqx.common.debugLog('Synchronizing all datasources');

                for (var storeName in this) {
                    var store = this[storeName];
                    try {
                        if (store instanceof CQDataSource ||
                          store instanceof kendo.data.DataSource) {
                            store.sync();
                        }
                    }
                    catch (ex) {
                        //ignoring error for now
                        sqx.common.toDo('Schema of ' + storeName + ' missing');
                    }
                }
            }

            return delta;
        };


        /*
        returns the view of related object only, shorthand used by getter property
        */
        sqx.dataStore._getRelatedView = function (model, relName) {
            return sqx.dataStore._getRelatedDataSource(model, relName).view();//view is filtered datasource  but it is also paged!
        };


        /*
        * returns the filtered data source for the related list
        */
        sqx.dataStore._getRelatedDataSource = function (model, relName) {
            if (model.childRelationships === undefined) return null; //no views exist
            var dsName = "ds" + relName; //relname prop will refer to view so use ds prefix to store the dataSource
            if (!model.hasOwnProperty(dsName)) {
                var childRel = null;
                for (var i = 0; i < model.childRelationships.length; i++) {
                    if (model.childRelationships[i].relationshipName == relName) {
                        childRel = model.childRelationships[i];
                        break;
                    }
                }

                if (childRel != null) {
                    var dsListEntry = _getDsListEntryFor(this.schemaContainer, childRel.childSObject);

                    var dataContainer = dsListEntry.source;
                    var dataModel = dsListEntry.model;
                    var options = {};
                    options.parentModel = model;
                    options.parentCol = childRel.field;
                    options.parentId = model.get(childRel.parentField || model.idField);
                    if(relName === REL_CONTENT_DOCUMENT_LINK) {
                        options.autoSync = true;
                    }
                    var ds = createDataStore(dataContainer, dataModel, options);
                    Object.defineProperty(model, dsName, { get: function () { return ds; } });
                }
                else {
                    return null;
                }
            }

            return model.get(dsName);
        };

        /**
        * returns the ds list entry for a particular sobject type if it exists, else creates one and returns it.
        */
        function _getDsListEntryFor(schemaContainer, sObjectType){
            var dsListEntry = dsList[sObjectType];

            if(dsListEntry == null){
                dsListEntry = {
                    source: new kendo.data.ObservableArray([]),
                    model: schemaContainer[sObjectType]
                };
                dsList[sObjectType] = dsListEntry;
            }
            return dsListEntry;
        }

        /**
        * returns the observable object with given uid for a particular type
        */
        sqx.dataStore.getObjectWithUidFor = function(sObjectType, uid){
            var allObjects = sqx.dataStore.getObjectsWithType(sObjectType);
            if(allObjects !== null){
                for(var index = 0, length = allObjects.length; index < length; index++){
                    if(allObjects[index].uid == uid)
                        return allObjects[index];
                }
            }

            return null;
        };

        /**
        * returns the observable object with given id for a particular type
        */
        sqx.dataStore.getObjectWithIdFor = function(sObjectType, id){
            var allObjects = sqx.dataStore.getObjectsWithType(sObjectType);
            if(allObjects !== null){
                for(var index = 0, length = allObjects.length; index < length; index++){
                    if(allObjects[index].Id == id)
                        return allObjects[index];
                }
            }

            return null;
        };

        /**
        * returns the observable objects for all the objects of a particular type
        */
        sqx.dataStore.getObjectsWithType = function(sObjectType){
            return _getDsListEntryFor(this.schemaContainer, sObjectType).source;
        };

        /**
        * enables or disable the recording based on the recording value passed
        * true starts recording, while false disables it
        */
        sqx.dataStore.enableRecording = function(recording){
            storeDelta = recording;
        }

        /**
        * This function returns the object with the given id
        * @param id the id of the object which is to be retrieved
        * @return return the matching object if found in data store else returns null.
        */
        sqx.dataStore.getObjectWithId = function(id){
            var prefix, objectType;
            if(sqx.common.isSFID(id)){
                prefix = id.substr(0, 3);

                objectType = prefixMatrix[prefix];
                sqx.common.assert(objectType != undefined || 'Undefined type prefix found for ' + id);
            }
            else{
                objectType = id.substring(0, id.indexOf('-'));
                if(sqx.objAlias && sqx.objAlias[objectType]){
                    objectType = sqx.objAlias[objectType].sObjectName;
                }
            }

            return sqx.dataStore.getObjectWithIdFor(objectType, id);
        }


        /**
        * This function retrieves the old value i.e. the original value that the object had when the page was loaded if it was changed.
        * @param id the id of the object whose old value is to be fetched
        * @param fieldName the field whose old value will be fetched
        * @return returns the original value if it has been changed, else returns <code>undefined</code>. Note: it will return <code>null</code> if original value was null and is now changed.
        */
        sqx.dataStore.getOldValue = function(id, fieldName){
            var changedVal = this._deltaMap[id];

            return (changedVal && changedVal.originalValues[fieldName] ) || undefined;
        }

        //TODO: add save and restore point facility to datastore

        /**
        * returns the observable object with given id by searching across all stores
        * this is the most expensive operation
        */
        /*sqx.dataStore.getObjectWithId = function(id){
            for(var objectType in dsList){
                var objectFound = sqx.datastore.getObjectWithIdFor(objectType, id);
                if(objectFound !== null)
                    return objectFound;
            }

            return null;
        };*/

        return sqx;

    }(sqx || {}, kendo));

    //review:in Schema set additional info props only if true, e.g. string richtext = false is not necessary, that is assumed

    function saveFormChanges() {
        $("#loadingBlock").show();
        //review: why sync only 1 data store? error handling is missing
        //                        sqx.dataStore.SQX_Finding__c.sync();
        var idtodelete = [];
        var deletedSobjects = sqx.dataStore.getChangeSet().deleted.filter(function (e) {
            if (sqx.common.isSFID(e.Id)) {
                idtodelete.push({ Id: e.Id });
                return true;
            }
            return false;
        });
        var changeSet = getServerSubmissionSet(sqx.dataStore.getChangeSet().modified);

        SQX_Extension_UI.processChangeSet(changeSet, idtodelete, function (e, s) {

            if (sqx.dataStore.getChangeSet().modified.length > 0) {
                //review: where is the error handler?
                if (s.status) {
                    if (sqx.common.isSFID(e)) {
                        var url = "/apex/SQX_Finding_Customer?id=" + e;
                        location.replace(url);
                        return;
                    }
                    else {
                        var url = "/apex/SQX_Finding_Customer?id=" + mainRecord.Id;
                        location.replace(url);
                        return;
                    }
                }
                console.log(s);
                $("#loadingBlock").hide();
                alert(s.message);
            }
            else {
                alert("Data not found. Please enter data before saving");
            }
        });
        $("#loadingBlock").hide();
    }


    function getServerSubmissionSet(modifiedObjects) {
        "use strict";
        return JSON.stringify({changeSet: modifiedObjects});
        /* not necessary because we are already using primitive
        var changeSet = { changeSet: [] };
        var idx, length;
        for (idx = 0, length = modifiedObjects.length; idx < length; idx++) {
            var moddedObject = modifiedObjects[idx];
            var changedObject = {};

            for (var field in moddedObject) {

                if (moddedObject[field] === undefined || moddedObject[field] === null)
                    continue;

                if (!(moddedObject[field] instanceof Date) && typeof (moddedObject[field]) === 'object' && field !== 'attributes' && field != 'originalValues') {
                    if (moddedObject[field].Id !== undefined) {
                        moddedObject[field] = moddedObject[field].Id;
                    }
                    else if (moddedObject[field].value !== undefined) {
                        moddedObject[field] = moddedObject[field].value;
                    }
                    else {
                        continue;
                    }
                }


                changedObject[field] =  moddedObject[field];

            }

            changeSet.changeSet.push(changedObject);
        }

        return JSON.stringify(changeSet);*/
    }