/**
 * Binder for adding and removing boolean checked and unchecked class
 */
kendo.data.binders.bool = kendo.data.Binder.extend({

    refresh: function () {
        var data = this.bindings["bool"].get();
        $(this.element).removeClass('cq-boolean-checked');
        $(this.element).removeClass('cq-boolean-unchecked');

        if (data) {
            $(this.element).addClass('cq-boolean-checked');
        }
        else{
            $(this.element).addClass('cq-boolean-unchecked');
        }
    }
});