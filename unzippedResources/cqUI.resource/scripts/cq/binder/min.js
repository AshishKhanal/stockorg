/*jshint browser: true */
/*global window document console*/

(function (kendo, $) {
    "use strict";

    var ATTR_MIN = 'min',
        ISO_FORMAT = 'yyyy-MM-ddTHH:mm:sszzz';

    /**
    * this binder helps bind the min value of a widget
    */
    kendo.data.binders.widget.min = kendo.data.Binder.extend({
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        },
        refresh: function () {
            var that = this,
                value = that.bindings[ATTR_MIN].get(), //get the value from the View-Model
                input = $(that.element),
                role = input.attr('data-' + kendo.ns + 'role'),
                roles = kendo.rolesFromNamespaces(kendo.ns);

            if (roles[role].prototype.min !== undefined) {
                $(that.element).data('kendo' + roles[role].prototype.options.name)
                    .min(kendo.parseDate(kendo.toString(value, ISO_FORMAT)));//parsed date before setting min to bring 12:00 AM for time, else 12:00 PM was the defalt time
                //update attribute
                if (value instanceof Date) {
                    //since date has both date-time we would like to ignore time part
                    $(that.element).attr(ATTR_MIN, value.toISOString());
                } else {
                    $(that.element).attr(ATTR_MIN, value);
                }
            }
        }
    });

}(window.kendo, window.jQuery));