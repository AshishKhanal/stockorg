(function (binders, Binder) {
    "use strict";


    var authorizationToken = window && window.cq.SESSION_ID || '',
        THUMBNAIL_SUCCESS_PATH = 'thumb120By90RenditionStatus',
        THUMBNAIL_PATH = 'renditionUrl240By180',
        NO_IMAGE_PATH = '#',
        MIME_TYPE_PATH = 'mimeType',
        DOWNLOAD_PATH = 'downloadUrl',
        NAME_PATH = 'name',
        DESCRIPTION_PATH = 'description',
        retriever,
        translator = sqx.translation,
        FileThumbnailRetriever,
        ID_FIELD = "id",
        TEMPLATE = kendo.template('<div class="cq-file-field">' +
                    '<div class="cq-image"><img src="#: thumbnailSrc #" title="#: sourceTitle #" data-original-src="#: downloadlink #" data-mime-type="#: mimeType #"/></div>' +
                    '<div class="cq-detail"><span class="cq-filename">#: fileName #</span>' +
                    '<span class="cq-mimetype">#: mimeType #</span></div>'+
                    '<div class="cq-commands">' +
                    '<a href="#: previewLink #" target="_blank" class="cq-preview-file">#: previewLinkText #</a>' +
                    '<a href="#: downloadlink #" target="_blank" class="cq-download-file">#: downloadlinkText #</a>' +
                    '</div>' +
                    '</div>');

    /**
     * This is an event handler that loads the popup containing the image, when thumbnail is clicked.
     * @param {Object} e the event that triggered the showing of the image
     */
    function loadImage(e) {
        var target = $(e.currentTarget);

        if(target != null && target.data('original-src')) {
            if(target.data('mime-type').indexOf('image') !== 0) {
                window.open(target.data('original-src'));
                return;
            }
            var previewWindow = $('.cq-image-preview');

            if(previewWindow.length === 0) {
                var win = $(window),
                    height = win.height(),
                    width = win.width(),
                    min = Math.min(width, height);

                previewWindow = $("<div class='cq-image-preview'><div class='cq-container'><img class='cq-img' /></div></div>");

                var i = previewWindow.find('cq-img').find('img');
                i.width(min);
                i.height(min);

                previewWindow.kendoWindow({
                    modal: true,
                    resizable: false
                });
                previewWindow.find('img').bind('load', function() {
                    hideLoading();
                    var imgEle = previewWindow.find('img')[0];

                    var imgWidth = imgEle.width;
                    if (imgWidth > min) {
                        $(imgEle).css('width', min + 'px');
                    }
                    var d = previewWindow.data('kendoWindow');

                    d.center();
                    d.open();
                });
            }
            showLoading();
            previewWindow.find('img').css('width','auto');
            previewWindow.find('img').attr('src', target.data('original-src'));
        }
    }

    /**
     * This object is used for retrieving thumbnail information from Salesforce for a given record Id.
     */
    FileThumbnailRetriever = function() {
        var that = this;
        that.i_queuedRequests = [];
        that.i_cached = {};
        that.i_processor = null;
    };

    /**
     * Add a warning if authorization token hasn't been set because, going forward without authorization
     * token is almost impossible.
     */
    if(authorizationToken.length === 0) {
        console.warn('Authorization token might not have been set');
    }

    /**
     * This method fetches the detail of file id from salesforce
     * @returns returns the promise that when resolved contains the details of file
     */
    FileThumbnailRetriever.prototype.getDetail = function(fileId) {
        var that = this;

        return new Promise(function(resolve, reject){
            if(that.i_cached[fileId]){
                console.info('Cache Hit');
                resolve(that.i_cached[fileId]);
            }
            else {
                that.getInfo(fileId, resolve, reject);
            }
        });
    };

    /**
     * This method starts the thumbnail retriver, it has supports request aggregation i.e. if multiple
     * resolution requests are being made at the same time it will wait for a fixed amount of time,
     * aggregate them and send them as a single request.
     */
    FileThumbnailRetriever.prototype.startProcessor = function() {
        var that = this,
            DEFAULT_INTERVAL = 500,
            GET_THUMBNAIL_BATCH_URL = '/services/data/v40.0/connect/batch',
            GET_THUMBNAIL_URL = '/services/data/v40.0/connect/files/',
            MAX_BATCH_SIZE = 25, id,
            successfn,
            errorfn;

            successfn = function(res, status) {
                var failed = {};

                if (status == 'success') {
                    // set thumbnail url or show error for each thumbnail request
                    for(i = 0; i < res.results.length; i++) {
                        id = res.results[i].result.id;
                        if (res.results[i].statusCode == 200) {
                            that.i_cached[id] = res.results[i].result;
                        }
                        else {
                            failed[id] = res.results[i].result;
                        }
                    }

                    for(i = that.i_queuedRequests.length - 1; i >= 0; i--){
                        if(that.i_queuedRequests[i].processing){
                            id = that.i_queuedRequests[i].id;
                            if(that.i_cached[id]) {
                                that.i_queuedRequests[i].onSuccess.call(null, that.i_cached[id]);
                                that.i_queuedRequests.splice(i,1);
                            }
                            else if(failed[id]){
                                that.i_queuedRequests[i].onFailure.call(null, failed[id]);
                                that.i_queuedRequests.splice(i,1);
                            }
                        }
                    }
                }
                else {
                    //identify failure
                }
            };

            errorfn = function(resp) {
                //need to find out what to do here
                //requeuing could be appropriate
            };

            console.info('Queuing Batch');
            /**
             * Actual queue processor that periodically picks up cached items and
             * requests Salesforce for data. (NIH Syndrome, could have identified a library)
             */
            setTimeout(function() {
                var i,
                    batchedRequests = [],
                    requestFor = {},
                    requests = that.i_queuedRequests,
                    request,
                    currentBatch,
                    batchRequest;

                for(i = 0; i < requests.length; i++){
                    request = requests[i];
                    if(!request.processing){
                        request.processing = true;
                        if(!requestFor[request.id]){
                            requestFor[request.id] = true;
                            batchedRequests.push({
                                "method" : "GET",
                                "url" : GET_THUMBNAIL_URL + request.id
                            });
                        }
                    }
                }

                // for each batch send out a request
                while(batchedRequests.length > 0) {
                    currentBatch = batchedRequests.splice(0, MAX_BATCH_SIZE);
                    batchRequest = {
                        "batchRequests" : currentBatch,
                        haltOnError : false
                    };

                    console.info('Batch started with requests : ' + currentBatch.length);
                    $.ajax({
                        type: "POST",
                        url: GET_THUMBNAIL_BATCH_URL,
                        headers: {
                            'Authorization' : 'OAuth ' + authorizationToken
                        },
                        contentType: 'application/json',
                        data: JSON.stringify(batchRequest),
                        dataType: 'json',
                        success: successfn,
                        error: errorfn
                    });
                }

            }, DEFAULT_INTERVAL);
    };

    /**
     * This method pushes the request to the queue and calls the resolve/reject function based on success/failure
     * @param fileId {String} the id of the file whose data is to be fetched
     * @param resolve {Function} the method that is to be called when the resolution is complete
     * @param reject {Function} the method that is to be called in case of failure 
     */
    FileThumbnailRetriever.prototype.getInfo = function(fileId, resolve, reject){
        var that = this;

        that.i_queuedRequests.push({id: fileId, onSuccess : resolve, onFailure: reject, processing: false});
        that.startProcessor();

    };

    retriever = new FileThumbnailRetriever();

    //initialize a single retriever object within cq optimizing request cachching and queueing.
    window.cq_retriever = retriever;

    /**
     * Binder definition for 'File' type binding
     */
    binders.file = Binder.extend({
        init: function(element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);
        },
        refresh: function() {
            var that = this,
                fileId,
                element,
                imgElement,
                LOADING_CLASS = 'cq-loading',
                data = { thumbnailSrc : '', sourceTitle: '', fileName : '' ,mimeType : '',previewLink : '#' ,previewLinkText : '', downloadlinkText: '', downloadlink: '' };

            fileId = that.bindings.file.get();
            element = $(that.element);
            if(fileId != null){
                element.find('.cq-file-field').remove();
                imgElement = $(TEMPLATE.call(null, data));
                imgElement.addClass(LOADING_CLASS);
                imgElement.appendTo(element);
                retriever.getDetail(fileId).then(function(d){
                    if(d !== null) {
                        data.thumbnailSrc = d[THUMBNAIL_PATH];
                        data.sourceTitle = d[DESCRIPTION_PATH] || '';
                        data.fileName = d[NAME_PATH];
                        data.mimeType = d[MIME_TYPE_PATH];
                        data.downloadlink = d[DOWNLOAD_PATH];
                        data.previewLink = "/" + d[ID_FIELD];

                        data.downloadlinkText = translator.get('binders.file.download') || 'Download';
                        data.previewLinkText = translator.get('binders.file.preview') || 'Preview';

                        imgElement.remove();
                        imgElement = $(TEMPLATE.call(null, data));
                        imgElement.appendTo(element);

                        $(imgElement).find('.cq-image img').click(loadImage);

                        if(d[THUMBNAIL_SUCCESS_PATH] !== "Success") {
                            imgElement.addClass("no-thumbnail");
                        }
                        else {
                            imgElement.removeClass("no-thumbnail");
                        }
                    }
                })["catch"](function(err) {
                    var errorElement = $("<pre></pre>"), errorMessage;
                    errorMessage = err.message + '\nId: ' + err.documentId;
                    errorElement.text(errorMessage);
                    imgElement.html(errorElement.html());
                });
            }
            else {
                element.text('');
            }
        }
    });

} (kendo.data.binders, kendo.data.Binder));