/**
 * This binder binds Read Only attribute to the element
 */
kendo.data.binders.widget.cqReadonly = kendo.data.Binder.extend({
    refresh: function () { var readonly = this.bindings.cqReadonly.get();
        if (readonly !== undefined) {
            this.element.setReadonly(readonly);
        };
    }
});