/**
 * Binder for hiding element
 */
kendo.data.binders.widget.cqHide = kendo.data.Binder.extend({
    refresh: function () { var hide = this.bindings.cqHide.get();
        if (hide !== undefined) {
            this.element.hideField(hide);
        };
    }
});

kendo.data.binders.cqHide = kendo.data.Binder.extend({
    refresh: function () { var hide = this.bindings.cqHide.get();
        if (hide) {
            $(this.element).hide();
        }
        else{
            $(this.element).show();
        }
    }
});