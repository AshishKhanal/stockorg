/*
 * Binder to bind reference object
 */
kendo.data.binders.reference = kendo.data.Binder.extend({
    refresh: function () {
        var referenceObj = this.bindings["reference"].get();
        var that = this;
        
        if (referenceObj) {

            $(this.element).attr('href', sqx.common.getObjectUrl(referenceObj));

            // var objectToRender = null;
            // objectToRender = { Id: referenceObj.Id, Name: referenceObj.Name }; //default to Id
            // this._renderObject(objectToRender);
             //below no longer necesasry with "Name" data either sent from server intially or applied when selecting from combobox
            //if(referenceObj.promise !== undefined){
            //    //the data is not currently available
            //    objectToRender = {Id : referenceObj.objectId, Name: referenceObj.objectId }; //default to Id
            //    this._renderObject(objectToRender);
                
            //    referenceObj.promise.done(function(){
            //        var objToRender = referenceObj.remoteSource.get(referenceObj.objectId);
            //        if(objToRender !== undefined)
            //            that._renderObject(objToRender);
            //    })
            //    .fail(function(e){
            //        sqx.common.error('Error occurred while binding to widget', e);
            //    });
            //}
            //else{
            //    objectToRender = referenceObj.data;
            //    this._renderObject(objectToRender);
            //}          
        }
        
    },
    
    _renderObject: function(objToRender){
        if(!!objToRender    ){

            $(this.element).attr('href', sqx.common.getObjectUrl(objToRender.Id));
        
            $(this.element).text(objToRender.Name);
        }
    }
});