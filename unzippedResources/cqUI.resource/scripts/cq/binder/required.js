/*
 * Binder to bind validation required to the element
 */
kendo.data.binders.widget.cqRequired = kendo.data.Binder.extend({
    refresh: function () {var required = this.bindings.cqRequired.get();
        if (required !== undefined) {
            this.element.setRequired(required);
            // if element is in inline edit mode then switch it to edit/view mode

            //If the binder changed as a result of DOMContentLoad don't switch edit mode
            //because data will have already been provided.
            //SQX-660 for the issue resolved.
            if (window.event && window.event.type != 'DOMContentLoaded' && this.element.options.inlineEdit == true) this.element.setEditMode(required);
        };
    }
});