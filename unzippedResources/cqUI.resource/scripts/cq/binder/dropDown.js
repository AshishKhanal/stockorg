/**
* useless binder to associate the source of picklist with static elements like span 
*/
kendo.data.binders.cqdropdownsource = kendo.data.Binder.extend({
    refresh: function () { 
    }
});

/**
* Binder to display the value of the field instead of text if source is available.
* Should only be used with cqdropdownsource
*/
kendo.data.binders.cqdropdown = kendo.data.Binder.extend({
    refresh: function () { 
        var dropdown = this.bindings.cqdropdown.get(),
            source = this.bindings.cqdropdownsource,
            element = $(this.element),
            sourceField = element.attr('data-value-field') || 'value',
            textField = element.attr('data-text-field') || 'text',
            matches, text = dropdown;

        if(source){
            source = source.get();
            try{
                matches = source.filter(function(e, i){
                    return e[sourceField] == dropdown;
                });

                if(matches.length == 1){
                    text = matches[0][textField];
                }
            }
            catch(ex){
                //ignore error
                sqx.common.log('Error occurred while initializing dropdown', ex);
            }
        }

        element.text(text);

    }
});