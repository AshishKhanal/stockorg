
/*jshint browser: true */
/*global window document console*/

(function (kendo) {
    "use strict";

    /**
     * Binder for showing date value in a properly formatted manner.
     */
    kendo.data.binders.date = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);

            this.dateformat = $(element).data("dateformat");
        },
        refresh: function () {
            var data = this.bindings["date"].get();
            if (data) {
                if (!(data instanceof Date)) {
                    data = kendo.parseDate(data);
                }
                var dateObj = new Date(data);
                var dateformat = this.dateformat;
                if (dateformat == null){
                    dateformat = sqx.common.CQDateFormat || 'MMMM dd, yyyy';
                }
                $(this.element).text(kendo.toString(dateObj, dateformat));
            }
            else {
                $(this.element).text('');
            }
        }
    });

    /**
     * Binder for showing date time value in a properly formatted manner.
     * They also convert text value serialized from Salesforce and present
     * them as date
     */
    kendo.data.binders.datetime = kendo.data.Binder.extend({
        init: function (element, bindings, options) {
            kendo.data.Binder.fn.init.call(this, element, bindings, options);

            this.dateformat = $(element).data("dateformat");
        },
        refresh: function () {
            var data = this.bindings["datetime"].get();

            if (data) {

                if (!(data instanceof Date)) {
                    if (typeof(data) === "string" && data.indexOf("+0000") !== -1) {
                        data = data.replace("+0000", "Z");
                    }
                    data = kendo.parseDate(data);
                }

                var dateObj = data;
                var dateformat = this.dateformat;
                if (dateformat == null){
                    dateformat = sqx.common.CQDateTimeFormat || 'MMMM dd, yyyy HH:mm';
                }
                $(this.element).text(kendo.toString(dateObj, dateformat));
            }
            else {
                $(this.element).text('');
            }
        }
    });
}(window.kendo));