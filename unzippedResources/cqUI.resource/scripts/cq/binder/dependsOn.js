/**
 * This binder establishes the dependencies between two picklist values for dropdown. This
 * binder should only be used with dropdown list.
 */
kendo.data.binders.widget.dependson = kendo.data.Binder.extend({
    init: function(widget, bindings, options){
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        $(widget.element[0]).data('counter', 0);

        // first load is used to prevent clearing of value that has been serialized from server without parent Id
        this.firstLoad = true;
    },

    refresh: function() {
        var that = this,
            values = that.bindings["dependson"].get(),
            role = $(that.element).attr('data-' + kendo.ns + 'role'),
            roles = kendo.rolesFromNamespaces(kendo.ns),
            uielement = $(that.element).data('kendo' + roles[role].prototype.options.name),
            originalDataSource = null,
            dataSource = null,
            currentValue = that.bindings["value"].get(),
            allowNullParent, counter;

        counter = $(that.element).data('counter');
        counter++; 
        $(that.element).data('counter', counter);

        if(values == null){
            allowNullParent = $(that.element).attr('data-allow-null-parent') === 'true';

            // this will apply all the bindings after a certain lag i.e. after this binding finishes.
            // we are releasing the flow from current thread of execution.
            setTimeout(function(){ that.applyBindingToTarget(uielement, values, currentValue, allowNullParent, counter)}, 10);

            //reset the value in ui if no value is not set.
            uielement.value(null);
            uielement.value(null);
        }
        else{
            if(uielement.options.dataSource == null && that.bindings["source"].get() != null){
                uielement.options.dataSource = that.bindings["source"].get();

                //HACK: delay the binding, because binding order can make a difference,
                //when source binding has not been applied.
                uielement.enable(false);
                setTimeout(function(){ that.applyBindingToTarget(uielement, values, currentValue, true, counter)}, 10);
            }
            else{
                setTimeout(function(){ that.applyBindingToTarget(uielement, values, currentValue, true, counter)}, 10);
            }
        }

    }

    ,
    applyBindingToTarget : function(uielement, values, currentValue, enableElement, counter){
        if(counter >= uielement.element.data('counter') ){
            var firstLoad = this.firstLoad,
                dataSource,
                originalDataSource,
                filter,
                dynamicField;

            this.firstLoad = false;

            uielement.enable(enableElement === undefined ? true : enableElement);

            if(uielement.options.dataSource.filter() === undefined){
                originalDataSource = uielement.options.dataSource;

                dataSource = new kendo.data.DataSource({

                    transport : originalDataSource.transport,
                    schema: originalDataSource.schema,
                    sort: { field: 'Name', dir: 'asc' },
                    serverFiltering: true,
                    serverPaging: true,
                    pageSize: 200
                });
            } else {
                dataSource = uielement.options.dataSource;
            }
            dynamicField = uielement.element.attr("data-depends-on-field") === "dynamicfields";
            filter = {}; //just invoke filter if dynamic field. It will be added by model based call back HACK :D
            if (uielement.element.data().valueField && currentValue) {
                if (dynamicField) {
                    filter = {field: "Id", value: currentValue[uielement.element.data().valueField], operator: 'eq'};
                } else {
                    filter = {
                        logic: 'and',
                        filters: [
                            {field: uielement.element.attr("data-depends-on-field"), value: values, operator: 'eq'},
                            {field: "Id", value: currentValue[uielement.element.data().valueField], operator: 'eq'}
                        ]};
                }
            } else if (!dynamicField) {
                filter = {field: uielement.element.attr("data-depends-on-field"), value: values, operator: 'eq'};
            }

            dataSource.filter(filter);

            uielement.setDataSource(dataSource);

            //if the prior value exists and is not dependent on the current value, clear the value from the ui.
            // Note: If this is first load parentId can be undefined especially because parent Id isn't being serialized
            // Additionally, we are assuming data in server is correct and properly validated.
            var parentId = currentValue && currentValue[uielement.element.attr("data-depends-on-field")];
            if(parentId !== values && !(firstLoad && parentId == undefined) ){
                uielement.value(null);
                uielement.text(null);
                uielement.trigger('change'); //added trigger because without sending a change event the model isn't updated
            }

        }


    
    }
});