/*jshint browser: true */
/*global window document console*/

/** Added support for max and min value to a widget for number and date fields ***/

(function (kendo, $) {
    "use strict";

    var ATTR_MAX = 'max',
        ISO_FORMAT = 'yyyy-MM-ddTHH:mm:sszzz';


    //original code found at http://jsbin.com/eqidex/2/edit from http://www.telerik.com/forums/min-max-bindings-for-datepicker-and-timepicker
    /**
    * this binder helps bind the max value of a widget
    */
    kendo.data.binders.widget.max = kendo.data.Binder.extend({
        init: function (widget, bindings, options) {
            //call the base constructor
            kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
        },
        refresh: function () {
            var that = this,
                value = that.bindings[ATTR_MAX].get(), //get the value from the View-Model
                input = $(that.element),
                role = input.attr('data-' + kendo.ns + 'role'),
                roles = kendo.rolesFromNamespaces(kendo.ns);

            //check if the component does support max function, if yes set the new max value
            if (roles[role].prototype.max !== undefined) {
                $(that.element).data('kendo' + roles[role].prototype.options.name).max(kendo.parseDate(kendo.toString(value, ISO_FORMAT)));//parsed date before setting max to bring 12:00 AM for time, else 12:00 PM was the defalt time
                //update attribute
                if (value instanceof Date) {
                    //since date has both date-time we would like to ignore time part
                    $(that.element).attr(ATTR_MAX, value.toISOString());
                } else {
                    $(that.element).attr(ATTR_MAX, value);
                }
            }
        }
    });
}(window.kendo, window.jQuery));