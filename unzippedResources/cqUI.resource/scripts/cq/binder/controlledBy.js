/**
 * This binder establishes the dependencies between two picklist values for dropdown. This
 * binder should only be used with dropdown list.
 */
kendo.data.binders.widget.controlledby = kendo.data.Binder.extend({
    init: function(widget, bindings, options) {
        //call the base constructor
        kendo.data.Binder.fn.init.call(this, widget.element[0], bindings, options);
    },
    refresh: function() {
        var that = this,
            value = that.bindings["controlledby"].get(),
            currentValue = that.bindings["value"].get(),
            dataSource, clear, element = $(that.element), dropDown = element.data().kendoDropDownList;

        dataSource = dropDown.dataSource;

        if(dataSource.data().length == 0){
            // Since the datasource isn't loaded for the first load, we have to wait before the filter is applied
            setTimeout(function() {
                var value = that.bindings["controlledby"].get();
                    
                dataSource.data().forEach(function(d,i) {d.isValid = value !== undefined && d.validFor && d.validFor.indexOf(value) > -1; });
                dataSource.filter({field : 'isValid' , operator: 'eq', value: true});
                dropDown.enable(dataSource.view().length > 0);
            },50);
        }
        else{
            dataSource.data().forEach(function(d,i) {d.isValid = value !== undefined && d.validFor && d.validFor.indexOf(value) > -1; });
            dataSource.filter({field : 'isValid' , operator: 'eq', value: true});

            dropDown.enable(dataSource.view().length > 0);
        }
    }
});