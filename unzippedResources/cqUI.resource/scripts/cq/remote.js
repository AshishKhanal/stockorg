/*
used to fetch values from remote source i.e. sf
*/
var sqx = (function (sqx, kendo, uicontroller, $) {
    "use strict";

    if(uicontroller == null)
        return;


    sqx.remote = sqx.remote || {};

    var mockAjax = function () { return false; }// return navigator.onLine === false || window.location.hostname == 'localhost'; }

    /**
    @description configure here to return data depending on remote action name
    */
    function mockJax(remoteActionName) {
        var data = [];
        var status = { status: true };
        var filter = arguments.length > 1 ? Array.prototype.pop.apply(arguments) : null;

        switch (remoteActionName) {
            case 'getAccounts':
                data = [{ "Name": "AAcme", "Id": "001f000000FRrhsAAD", Account_Type: 'Supplier' }, { "Name": "Random", "Id": "001f000000WZTYtAAP", Account_Type: 'Supplier'}, { Id: '001c000000XL2fdAAE', Name: 'Account 2', Account_Type: 'Supplier' },
                { Id: '001c100001XL2fdAAD', Name: 'Customer Account 1', Account_Type: 'Customer' }, { Id: '001x000002XL2fdAAE', Name: 'Customer Account 2', Account_Type: 'Customer' }];
                break;

            case 'getContacts':
                data = [{ Id: '003f000000W1TqIAAV', Name: 'Rajesh Khanna', AccountId: '001f000000FRrhsAAD' },
                        { Id: '003f000000W2TqIAAV', Name: 'Contact 2 - Account 1', AccountId: '001c000000XL2fdAAD' },
                        { Id: '003c000000RkjNEAAZ', Name: 'Contact 1 - Account 2', AccountId: '001c000000XL2fdAAE' },
                        { Id: '003c000000RjkNEAAZ', Name: 'Contact 2 - Account 2', AccountId: '001c000000XL2fdAAE' },
                        { Id: '003c000000RljNEAAZ', Name: 'Binod Khanna', AccountId: '001f000000FRrhsAAD' }];
                break;
            case 'getParts':
                data = [{ Id: '003c000000RyjNEAAZ', Name: 'part 1' },
                        { Id: '003c000000RajNEAAZ', Name: 'part 2' },
                        { Id: '003c000000RkjNEAAZ', Name: 'part 3' },
                        { Id: '003c000000RljNEAAZ', Name: 'part 4' }];
                break;
            case 'getServices':
                data = [{ Id: '003c000000RyjNEAAZ', Name: 'Contact 1 - Account 1', AccountId: '001c000000XL2fdAAD' },
                        { Id: '003c000000RajNEAAZ', Name: 'Contact 2 - Account 1', AccountId: '001c000000XL2fdAAD' },
                        { Id: '003c000000RkjNEAAZ', Name: 'Contact 1 - Account 2', AccountId: '001c000000XL2fdAAE' },
                        { Id: '003c000000RljNEAAZ', Name: 'Contact 2 - Account 2', AccountId: '001c000000XL2fdAAE' }];
                break;
            case 'getStandardServices':
                data = [{ Id: 1, Name: 'Service 1' },
                        { Id: 2, Name: 'Service 2' },
                        { Id: 3, Name: 'Service 3' },
                        { Id: 4, Name: 'Service 4' },
                        { Id: 5, Name: 'Service 5' }];
                break;
            case 'getDefectCodes':
                data = [{ Id: 1, Name: 'Defect 1' ,Defect_Category__c:'Cat A'},
                        { Id: 2, Name: 'Defect 2', Defect_Category__c: 'Cat A' },
                        { Id: 3, Name: 'Defect 3', Defect_Category__c: 'Cat B' },
                        { Id: 4, Name: 'Defect 4', Defect_Category__c: 'Cat B' },
                        { Id: 5, Name: 'Defect 5', Defect_Category__c: 'Cat C' }];
                break;
            case 'getUsers':
                data = [{ Id: '005i0000005JEbqAAG', Name: 'User One', User_Type__c: 'Regular' },
                        { Id: '005i0000004JEbqAAG', Name: 'User Two - Approver', User_Type__c: 'Approver' },
                        { Id: '005i0000003JEbqAAG', Name: 'User Three', User_Type__c: 'Regular' },
                        { Id: '005i0000002JEbqAAG', Name: 'User Four - Approver', User_Type__c: 'Approver' }];
                break;
            default:
                //do nothing, defaults set on top
        }
        //hack for simple filter check
        if (filter[1] && $.isArray(filter[1].filters)) {
            data = $.grep(data, function (item, counter) {
                return item[filter[1].filters[0].field] == filter[1].filters[0].value
            })
        }
        return [data, status];
    }

    /**
    * @description returns the function which will make the remote call, also used to create a mockjax interceptor for local request
    */
    function getRemoteActionMethod(remoteActionName) {
        var remoteCallMethod = null;
        if (mockAjax()==true) {
            sqx.common.debugLog('Switching to remote mock mode for ' + remoteActionName);
            remoteCallMethod = function (/* list of parameters last one being the callback*/) {

                var callback = Array.prototype.pop.apply(arguments); //last param is always callback


                var mockJaxResult = mockJax(remoteActionName, arguments);

                setTimeout(function () { callback.apply(window, mockJaxResult); }, 500); //500 ms delay
            };
        }
        else {
            remoteCallMethod = uicontroller[remoteActionName];
        }

        return remoteCallMethod;
    }


    /**
    * @description creates and returns a data source dynamically with its transport method configured.
    * @remoteActionName function name of remote action
    * @filter filter for the fetching data
    * @escape boolean value to control whether to escape special chars or not
    */
    function createDataSource(remoteActionName, filter, escape) {
        var remoteCallMethod = getRemoteActionMethod(remoteActionName);

        return new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    var filters = options.data.filter != null ? options.data.filter : null;
                    
                    remoteCallMethod(options.data.page, filters, function (result, success) {
                        if (success.status) {
                            options.success(result || []); //so that null values are not sent
                        }
                        else {
                            options.error('Error occurred while fetching data from remote for ' + remoteActionName);
                        }
                    }, {
                        //if sent true, set true , else always false
                        escape: escape || false //by default never escape
                    });
                }
            },
            schema: { model: { id: 'Id' } },
            sort: { field: 'Name', dir: 'asc' },
            filter: filter,
            serverFiltering: true,
            serverPaging: true,
            pageSize: 200
        });
    }


    /**
    * This function instantiates a data source for a passed object type.
    * @param objectName the name of the object whose objects are to be fetched
    * @param fields the list of fields that are to be fetched for the object, if nothing is passed then id and name is returned
    * @param filter the filter to apply to search for the object
    * @param remoteCallMethod the remote method that will return the objects, if null is passed then that method is used.
    * @param objectModel schema of the object
    * @param useSearch uses SOSL search instead of SOQL for querying
    * @return returns a datasource that will return the objects of desired type.
    */
    function createDataSourceForObject(objectName, fields, filter, remoteCallMethod, objectModel, useSearch){

        var methodParams = {
                            objectName: objectName,
                            fields: fields,
                            viewFilter : filter,
                            remoteCallMethod : remoteCallMethod || getRemoteActionMethod('getObjects')
                        };
        if(useSearch === true) {
            methodParams.useSearch = true;
            methodParams.remoteCallMethod = remoteCallMethod || getRemoteActionMethod('searchObjects');
        }

        if(objectModel){
            methodParams.schema = { model : objectModel };
        } 

        return new sqx.DataView(methodParams);
    }

    sqx.remote.createDataSourceForObject = createDataSourceForObject;


    /**
    * clones a given remote source
    * this returns a new kendo datasource with the exact same transport as defined in the original
    */
    sqx.remote.cloneSource = function(source, overrides){
        var options = {
            transport : source.options.transport,
            schema : source.options.schema,
            filter : source.options.filter,
            serverFiltering: source.options.serverFiltering,
            serverPaging: source.options.serverPaging,
            pageSize: source.options.pageSize,
            parentModel: source.options.parentModel,
            parentCol: source.options.parentCol,
            parentId: source.options.parentId,
            data: source.options.data
        };

        if(overrides){
            $.extend(options, overrides);
        }
        return new CQDataSource(options);
    }


    /***** list of remote actions ****/
    //sqx.remote.Account = function (filter) {if (!filter && !sqx.remote._Account) { sqx.remote._Account = createDataSource('getAccounts', filter); return sqx.remote._Account } else { return createDataSource('getAccounts') } };
    //sqx.remote.Contact = function (filter) {if (!filter && !sqx.remote._Contact) { sqx.remote._Contact = createDataSource('getContacts', filter); return sqx.remote._Contact } else { return createDataSource('getContacts') } };
    //sqx.remote.User = function (filter) { if (!filter && !sqx.remote._User) { sqx.remote._User = createDataSource('getUsers', filter); return sqx.remote._User } else { return createDataSource('getUsers') } };
    //sqx.remote.SQX_Part__c = function (filter) { if (!filter && !sqx.remote._Part) { sqx.remote._Part = createDataSource('getParts', filter); return sqx.remote._Part } else { return createDataSource('getParts') } };
    //sqx.remote.SQX_Service__c = function (filter) { if (!filter && !sqx.remote._Service) { sqx.remote._Service = createDataSource('getServices', filter); return sqx.remote._Service } else { return createDataSource('getServices') } };
    //sqx.remote.SQX_Standard_Service__c = function (filter) { if (!filter && !sqx.remote._StandardService) { sqx.remote._StandardService = createDataSource('getStandardServices', filter); return sqx.remote._StandardService } else { return createDataSource('getStandardServices') } };

    //supplier and customer are special type of remote that have inbuilt filter enabled
    //supplier = record of type Managed Supplier
    //customer is any account that is not of Managed Supplier record type 
    sqx.remote.Suppliers = createDataSource('getSuppliers');
    sqx.remote.Customers = createDataSource('getCustomers');


    //creating unescaped contact and account for two objects only
    //TODO: find out if unescaping introduces security vulnerability in other components
    sqx.remote.unescapedAccount = createDataSource('getAccounts', undefined, false);
    sqx.remote.unescapedComplaint = createDataSource('getComplaints', undefined, false);
    
    //create remote action for various types based on the object type
    //usage: sqx.remote.<Remote_Source_Name> = createDataSource('ExtensionMethodName');
    sqx.remote.Account = createDataSource('getAccounts');
    sqx.remote.Contact = createDataSource('getContacts');
    sqx.remote.User = createDataSource('getUsers');
    sqx.remote.compliancequest__SQX_Part__c = createDataSource('getParts');
    sqx.remote.compliancequest__SQX_Service__c = createDataSource('getServices');
    sqx.remote.compliancequest__SQX_Standard_Service__c = createDataSource('getStandardServices');
    
    sqx.remote.compliancequest__SQX_CAPA__c = createDataSource('getCAPAs');
    sqx.remote.compliancequest__SQX_Nonconformance__c = createDataSource('getNCs');
    sqx.remote.compliancequest__SQX_Department__c = createDataSource('getDepartments');
    sqx.remote.compliancequest__SQX_Division__c = createDataSource('getDivisions');
    sqx.remote.compliancequest__SQX_Business_Unit__c = createDataSource('getBusinessUnits');
    sqx.remote.compliancequest__SQX_Site__c = createDataSource('getSites');
    sqx.remote.compliancequest__SQX_Audit_Program__c = createDataSource('getAuditPrograms');
    sqx.remote.compliancequest__SQX_Audit__c = createDataSource('getAudits');

    sqx.remote.compliancequest__SQX_Complaint__c = createDataSource('getComplaints');
    sqx.remote.compliancequest__SQX_Answer_Option__c = createDataSource('getAnswerOptions');

    sqx.remote.compliancequest__SQX_Result_Type__c = createDataSourceForObject('compliancequest__SQX_Result_Type__c', null);
    sqx.remote.compliancequest__SQX_Result_Type_Value__c = createDataSourceForObject('compliancequest__SQX_Result_Type_Value__c',['Name', 'compliancequest__SQX_Result_Type__c']);

    
    sqx.remote.filteredComplaints = function (filter) { if (filter && !sqx.remote._complaintsFilter) { sqx.remote._complaintsFilter = createDataSource('getComplaints', filter); return sqx.remote._complaintsFilter } else { return createDataSource('getComplaints') } };

    /* call remote to filter by catogery field of Picklist values*/
    sqx.remote.pickListValues = function(category) {
        var functionName = getRemoteActionMethod('getPicklistValues');

        return new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    var filters = options.data.filter != null ? options.data.filter : null;
                    functionName(options.data.page, filters, category, function (result, success) {
                        if (success.status) {
                            options.success(result);
                        }
                        else {
                            options.error('Error occurred while fetching data from remote for ' + functionName);
                        }
                    }, {escape: false});
                }
            },
            schema: { model: { id: 'Id' } },
            sort: { field: 'Name', dir: 'asc' },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 10
        });
    };

    /* one exception to the rule, called with param */
    sqx.remote.CustomerAccounts = function () {
        var functionName = getRemoteActionMethod('customersByType');

        return new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    functionName('customer', options.data, function (result, success) {
                        if (success.status) {
                            options.success(result);
                        }
                        else {
                            options.error('Error occurred while fetching data from remote for ' + remoteActionName);
                        }
                    }, {escape: false});
                }
            },
            schema: { model: { id: 'Id' } },
            sort: { field: 'Name', dir: 'asc' },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 10
        });
    }();

    /* call remote to filter by type field of Defect Code

    This remote actions can be used as follows:
    in schema correction define a new remote action as:
            sqx.remote.conclusionCode = sqx.remote.defecCodeByType('Conclusion Code');
    then use this remote in the required field as
            schema.fields.field_name__c.additionalInfo.referenceTo  = 'conclusionCode';

    */
    sqx.remote.defectCodeByType = function (type) {
        var functionName = getRemoteActionMethod('getDefectCodes');

        return new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    var filters = options.data.filter != null ? options.data.filter : null;
                    functionName(options.data.page, filters, type, function (result, success) {
                        if (success.status) {
                            options.success(result);
                        }
                        else {
                            options.error('Error occurred while fetching data from remote for getDefectCodes');
                        }
                    }, {escape: false});
                }
            },
            schema: { model: { id: 'Id' } },
            sort: { field: 'Name', dir: 'asc' },
            serverFiltering: true,
            serverPaging: true,
            pageSize: 10
        });
    };

    //added to support CAPA and NC Pages
    sqx.remote.compliancequest__SQX_Defect_Code__c = sqx.remote.defectCodeByType('');
    
    /*
    * Initialize any datasource for the objects that have to be initialized. To initialize any datasource
    * the data source is set using the following structure
    * sqx.remote {
    *       sourcesToInit : {
    *           'datasourceType' : {
    *               fields : [.. List of fields to fetch ..],
    *               filter: { .. Kendo filter to pass ..}
    *            },
    *           'datasourceType2' : 1
    *       }
    * }
    */
    (function (remote, sourcesToInit){
        var currentObject, fieldsToFetch, filter, remoteActionName;

        for(var sourceName in sourcesToInit){
            currentObject = sourcesToInit[sourceName];

            fieldsToFetch = (currentObject && currentObject.fields) || null;
            filter = (currentObject && currentObject.filter) || undefined;
            remoteActionName = (currentObject && currentObject.remoteActionName) || undefined;

            remote[sourceName] = createDataSourceForObject(sourceName, fieldsToFetch, filter, remoteActionName);            
        }

    })(sqx.remote, sqx.remote.sourcesToInit || {});
    
    return sqx;
}(sqx || {}, kendo, window.UIController || null, jQuery ));
