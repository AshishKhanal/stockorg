(function($, k, sqx){

    var OP_LIKE = 'like',
        OP_NOT_IN = 'nin',
        OP_IN = 'in',
        OP_EQ = 'eq',
        OP_NEQ = 'ne',
        CMB_OR = 'or',
        CMB_AND = 'and';

    var _Transport = function(options){
        $.extend(this, options);
    }

    /**
     * Extracts search string from the given filter and returns the result
     * @param {*} filter the filter to inspect and return the search string
     * @return returns the search string and new filter without search string
     */
    function extractSearchStringFromFilter(filter) {
        var newFilter, searchString, idx, filterElement;
        searchString = '';
        newFilter = null;
        if(filter) {
            newFilter = JSON.parse(JSON.stringify(filter));

            // CAUTION: Using Contains in UI Filter and Search isn't recommended, because search expects contains
            // 8.0.0 change the filter in composite field so that we don't have to rely on contains to identify search
            // key word
            if(filter.operator === 'contains' || (filter.filters && filter.filters.length <= 2)) {
                if(filter.operator === 'contains' && filter.value.length > 2) {
                    searchString = filter.value;
                    newFilter = null;
                } else if(filter.filters) {
                    newFilter.filters = [];
                    for (idx = 0; idx < filter.filters.length; idx+= 1) {
                        filterElement = filter.filters[idx];
                        // Assumption if it is a contains filter all will have same length, so all will be excluded
                        if(filterElement.operator === 'contains' && filterElement.value.length > 2) {
                            searchString = filterElement.value;
                        } else {
                            newFilter.filters.push(filterElement);
                        }
                    }
                }
            }
        }
        return {'searchString' : searchString, 'filter': newFilter};
    }

    _Transport.prototype.read = function(options){
        var modelFilter;

        modelFilter = (this.relatedModel && this.relatedModel.modelFilterCallBack && this.relatedModel.modelFilterCallBack.call(this.relatedModel, this.sourceIdentifier)) || null;

        if(this.useSFRemoteObjects){
            this._readUsingRemoteObjects(options, modelFilter);
        }
        else{

            this._readUsingRemoting(options, modelFilter);
        }
    }

    /**
    * Merges the three filters (view filter, passed/Kendo UI filter and model filter) into one kendo filter.
    */
    _Transport.prototype._applyViewFilter = function(passedFilter, modelFilter){
        var combinedFilter = null,
            FILTER_AND_TEMPLATE = { logic : 'and', filters : []},
            filters = [this.viewFilter, passedFilter, modelFilter],
            index;

        combinedFilter = FILTER_AND_TEMPLATE;
        for(index = 0; index < filters.length; index++){
            if(this._isValidFilter(filters[index])){
                combinedFilter.filters.push(filters[index]);
            }
        }

        // if no valid filter was found set the filter to null, if the combined filters has single filter just send a single filter
        // else send combined filter
        combinedFilter = combinedFilter.filters.length == 0 ? null : combinedFilter.filters.length == 1 ? combinedFilter.filters[0] : combinedFilter;



        return combinedFilter;
    }

    /**
    * Transforms a kendo filter to apex remote objects filter
    * @filter filter to transform
    */
    _Transport.prototype._transformFilter = function(filter){
        var tF = {}, idx, combination, op,fieldCount,
            FILTER_MAP = {
                'contains' : OP_LIKE,
                'neq' : OP_NEQ
            };

        if(filter.logic){
            // is compound filter
            combination = {};

            if(filter.filters.length == 1){
                $.extend(combination, this._transformFilter(filter.filters[0]));
                tF = combination;
            }
            else{

                fieldCount = 0;

                for(idx = 0; idx < filter.filters.length; idx++){
                    fieldCount += this._extend(combination, this._transformFilter(filter.filters[idx]), filter.logic);
                }

                // SF doesn't like combination operator if only one child node is present.
                if(fieldCount == 1)
                    tF = combination;
                else
                    tF[filter.logic] = combination;
            }

        }
        else{
            // is simple filter
            combination = {};
            op = filter.operator;
            op = FILTER_MAP[op] || op;
            combination[op] = filter.value;
            tF[filter.field] = combination;

            if(op == OP_LIKE)
                combination[op] = '%' + combination[op] + '%';
        }

        return tF;
    }


    /**
    * Standard jquery extend can't be used because salesforce's filter expects single field(key) combined
    * with IN or NOT IN operator for multivalued filters.
    * This is in contrast to kendo's filter which can have same field(key). So, the purpose of this method is convert
    * from kendo EQ, NEQ for a single field at same level of filter to IN and NOT IN respectively.
    * @param destFilter the filter where result is to be stored
    * @param filterToMerge the kendo based filter
    * @param combinationOperator the OR or AND logic that is being used to combine
    */
    _Transport.prototype._extend = function(destFilter, filterToMerge, combinationOperator){

        /* ***********************************NOTE Starts*************************************
            TODO: There are cases where both filterToMerge and destFilter are complex filters

            To replicate the issue expose _Transport as _cqtransport and run the following script:

            var destFilter = {"and":{"IsLatest":{"eq":true},"cqdoc__Controlled_Document__c":{"eq":""}}},
                filterToMerge = {"and":{"ContentDocumentId":{"eq":"0691a000000a7btAAA"},"Title":{"like":"%asd%"}}},
                combinationOperator = 'and';

            var d = new _cqTransport();
            d.extend(destFilter, filterToMerge, combinationOperator);

            This will return an exception saying "Error while transforming the filter".

           ***********************************NOTE Ends************************************* */
        var targetValue, currentValue, error, arr, fieldCount = 0, hasError;


        for(field in filterToMerge){
            targetValue = filterToMerge[field];
            currentValue = destFilter[field];


            if(currentValue === undefined){
                // there is no conflict so simple extend like merge can be done
                destFilter[field] = targetValue;
                fieldCount++;
            }
            else{

                hasError = false;

                if(combinationOperator == CMB_OR){
                    // if we are combining using or operation and more than one values are provided in node
                    // combine using IN operator
                    if(targetValue[OP_EQ] !== undefined || targetValue[OP_IN] !== undefined){
                        arr = currentValue[OP_IN];

                        if(arr === undefined && currentValue[OP_EQ] !== undefined){
                            arr = [currentValue[OP_EQ]];
                            currentValue[OP_IN] = arr;
                            delete currentValue[OP_EQ];
                        }

                        if(arr !== undefined){
                            if(targetValue[OP_EQ] !== undefined){
                                arr.push(targetValue[OP_EQ]);
                            }
                            else{
                                arr.concat(targetValue[OP_IN]);
                            }
                        }
                        else{
                            hasError = true;
                        }
                    }
                    else{
                        hasError = true;
                    }
                }
                else if(combinationOperator == CMB_AND){
                    if(targetValue[OP_NEQ] !== undefined || targetValue[OP_NOT_IN] !== undefined){

                        arr = currentValue[OP_NOT_IN];

                        if(arr === undefined && currentValue[OP_NEQ] !== undefined){
                            arr = [currentValue[OP_NEQ]];
                            currentValue[OP_NOT_IN] = arr;
                            delete currentValue[OP_NEQ];
                        }

                        if(arr !== undefined){
                            if(targetValue[OP_NEQ] !== undefined){
                                arr.push(targetValue[OP_NEQ]);
                            }
                            else{
                                arr.concat(targetValue[OP_NOT_IN]);
                            }
                        }
                        else{
                            hasError = true;
                        }

                    }
                    else{
                        hasError = true;
                    }
                }

                if(hasError){
                    throw 'Error while transforming the filter';
                }
            }
        }

        return fieldCount;
    }

    /**
    * Checks whether the kendo filter is valid or not and returns a boolean accordingly
    */
    _Transport.prototype._isValidFilter = function(filter){
        var isValid = false;

        // AND a filter is valid only if it is not null
        //          OR  if it has simple filter one driven by field
        //              if it has complex filter with at least one filter
        isValid = filter && (filter.field || (filter.filters && filter.filters.length > 0));

        return isValid;
    }

    /**
    * This methdd uses Remote Objects based method to query sf objects
    */
    _Transport.prototype._readUsingRemoteObjects = function(options, modelFilter){
        var that = this, offset, sfFilter = {}, filter, idx, sort, os;

        // translate options to SF apex:remotes query
        sfFilter.limit = options.data.pageSize;
        offset = (options.data.page - 1) * options.data.pageSize;

        if(offset > 0)
            sfFilter.offset = offset;

        if(options.data.sort){
            sfFilter.orderby = [];
            for(idx = 0; idx < options.data.sort.length; idx++ ){
                sort = {},
                os = options.data.sort[idx];

                sort[os.field] = os.dir.toUpperCase();
                sfFilter.orderby.push(sort);
            }
        }


        filter = this._applyViewFilter(options.data.filter, modelFilter);

        if(filter){
            sfFilter.where =  that._transformFilter(filter);
        }

        // todo : support offset


        this.remoteCallMethod.retrieve(sfFilter, function(err, results){
            var formattedResults = [], idx, fields = [], obj, fidx, fname, currentRecord;

            if(err){
                options.error('Error occurred while retrieving data. Actual Error: ' + err);
                console.error(err);
            }
            else{

                for(idx = 0; idx < results.length; idx++){
                    currentRecord = results[idx];
                    obj = {};
                    for(fidx = 0; fidx < that.fields.length; fidx++){
                        fname = that.fields[fidx];
                        obj[fname] = currentRecord.get(fname);
                    }

                    formattedResults.push(obj);
                }

                options.success(formattedResults);
            }
        });
    };

    /**
    * Internal method that uses Apex Remoting to query objects
    */
    _Transport.prototype._readUsingRemoting = function(options, modelFilter){
        var additionalParamsToPass, modelBasedParamsToPass, callBackFunction, defaultParams, filterToPass, searchString, transformedParams;

        additionalParamsToPass = this.paramsToPass || [];

        modelBasedParamsToPass = (this.relatedModel && this.relatedModel.modelBasedParamCallBack && this.relatedModel.modelBasedParamCallBack.call(this.relatedModel, this.sourceIdentifier)) || [];


        callBackFunction = function (result, success) {
            if (success.status) {
                options.success(result || []); //so that null values are not sent
            }
            else {
                options.error('Error occurred while fetching data from remote');
            }
        };

        if(this.useSearch) {
            transformedParams = extractSearchStringFromFilter(options.data.filter);

            searchString = transformedParams.searchString;
            filterToPass = this._applyViewFilter(transformedParams.filter, modelFilter);

            defaultParams = [
                searchString,
                this.objectName,
                this.fields,
                filterToPass,
                options.data.page,
                this.orderby || ['Name'],
                this.sortAscending || true,
                callBackFunction,
                {escape: this.escape}
            ];
        } else {
            filterToPass = this._applyViewFilter(options.data.filter, modelFilter);
            defaultParams = [
                this.objectName,
                this.fields,
                options.data.page,
                filterToPass,
                callBackFunction,
                { escape: this.escape }
            ]
        }

        // TODO : find a way to not send objectName and fields when using static methods instead of getObjects
        //        for now user will have to ignore the two params
        params = additionalParamsToPass.concat(modelBasedParamsToPass).concat(defaultParams);
        this.remoteCallMethod.apply(null, params);

    }

    /**
    * DataView represents the class that always imposes a baseline filter on a kendo data source. This is unlike kendo.data.DataSource where
    * the base filter can be cleared. Additionally, dataview also provides the feature if associating a model to the data source.
    *
    * This model can be configured to send additional params to the server, along with default params. It can also be used to send additional
    * filters to the server.
    * Order of params is
    *           Params to pass [configured via options]
    *           Model based params [returned by callback]
    *           Default params
    */
    sqx.DataView = kendo.data.DataSource.extend({
        init : function(options){
            var that = this;

            options.escape = options.escape || false; // by default don't escape the data returned by sf, because kendo based UI elements escape the data

            options.transport = new _Transport({
                viewFilter : options.viewFilter,
                useSearch : options.useSearch,
                remoteCallMethod : options.remoteCallMethod,
                fields : options.fields || [],
                objectName : options.objectName,
                relatedModel : options.relatedModel,
                sourceIdentifier : options.sourceIdentifier,
                paramsToPass : options.paramsToPass,
                useSFRemoteObjects : options.useSFRemoteObjects,
                escape : options.escape
            });

            kendo.data.DataSource.fn.init.apply(this, arguments);
        },
        options: {
            viewFilter : null,                          // the default/baseline filter that is to be always applied to the view.
            remoteCallMethod : null,                    // the server extension method that will return the data
            pageSize: 200,                              // page size of the request
            serverPaging: true,                         // indicates whether server supports paging or not. If it doesn't client side paging will be used.
            serverFiltering: true,                      // indicates whether server supports filtering or not. If it doesn't client side filtering will be used
            serverSorting: false,                       // indicates whether server side sorting is supported
            sort: { field: 'Name', dir: 'asc'},         // indicates the sort order of the objects
            schema : {model : {id: 'Id'} },             // default schema for objects returned by the datasource
            escape : false,                             // indicates whether or not to escape the data
            relatedModel : null,                        // the model that is related to the data source, this model will provide model based filter and other params
            sourceIdentifier : null,                    // the id that is used to identify this view. This identifier is returned to the callback
            paramsToPass : null,                        // An array of additional params that are to be passed to the server if any.
            fields: null,								// List of fields to fetched using the source, used by remoting to dynamically get the fields and by Remote Objects to receive the list of fields that will be returned by server
            objectName: null,							// the api name of the object that is being queried

            useSFRemoteObjects : false					// boolean value that indicates whether VF remoting is being used or apex:remoteObjects,
        },
        /**
         * Adds new field to query from the datasource
         * @param fieldName the new field to add
         */
        addField: function(fieldName) {
            if(this.options.fields.indexOf(fieldName) === -1) {
                this.options.fields.push(fieldName);
            }
        }
    });
    sqx.DataView.prototype.extractSearchStringFromFilter = extractSearchStringFromFilter;
})(jQuery, kendo, sqx);