/*jshint browser: true */
/*global window document */
/**
 * This is a CQ custom widget that helps show file/content uploader in kendo ui.
 */
((function ($, kendo, sqx) {
    "use strict";
    var ui = kendo.ui,
        Widget = ui.Widget,
        DATABINDING = "dataBinding",
        DATABOUND = "dataBound",
        CHANGE = "change",
        BLUR = "blur",
        fileCache,
        fileCacheMgr,
        BLOCKER_TEMPLATE = kendo.template('<input type="text" name="file" data-required-msg="#: blockerMessage #" style="display:none;" />'),
        CANCEL_TEMPLATE = kendo.template('<a href="javascript:void(0)">#: cancelCommand #</a>'),
        DELETE_TEMPLATE = kendo.template('<a href="javascript:void(0)">#: deleteCommand #</a>'),
        logger = sqx.common;

    /**
     * The actual variable that stores the cached items.
     */
    fileCache = {};

    /**
     * Object responsible for managing the caching. Allows addition to and getting items
     * from cache
     */
    fileCacheMgr = {
        /**
         * adds a files result to cache to quicken any subsequent request
         */
        add: function (f) {
            fileCache[f.Id] = f;
        },
        /**
         * returns the result that was previously cached
         * @param fileId {String} the ID of the file whose cached record is to be fetched
         */
        getFile: function (fileId) {
            var file = fileCache[fileId];

            if (!file) {
                //search for already loaded contentdocumentlinks
                sqx.dataStore.getObjectsWithType('ContentDocumentLink').forEach(function (e) {
                    if (e.ContentDocumentId === fileId) {
                        file = e.ContentDocument;
                        fileCache[fileId] = file;
                    }
                });
            }
            return file;
        }
    };
    sqx._fileUploadCache = fileCacheMgr;

    /**
     * The actual class that is responsible for uploading the content to salesforce. It extends
     * the file uploader component provided by kendo
     */
    var contentUploader = Widget.extend({
        init: function (element, options) {
            var that = this;

            Widget.fn.init.call(that, element);
            that.createElements();
            that.createFileElement();
        },

        /**
         * Registered list of options that are supported by the content uploader.
         */
        options: {
            name: 'CQContentUploader',
            localization: {
                blockerMessage: 'File is being uploaded. Please wait...',
                cancelCommand: 'Cancel',
                deleteCommand: 'Delete'
            }
        },

        /**
         * An array of events supported by content uploader
         */
        events: [DATABINDING, DATABOUND, CHANGE],

        /**
         * Creates internal elements that make up the content uploader. A file uploader is composed of
         * a. file container: Contains the actual file uploader component
         * b. file commands: Contains the commands that are available
         */
        createElements: function () {
            var fileContainer = $('<div class="cq-file-container"></div>'),
                commandElement = $('<div class="cq-file-commands"></div>'),
                that = this,
                localization = that.options.localization || {},
                cancelCommand,
                deleteCommand,
                blocker;

            localization = $.extend(ui.CQContentUploader.fn.options.localization, localization);
            cancelCommand = $(CANCEL_TEMPLATE.call(null, localization));
            deleteCommand = $(DELETE_TEMPLATE.call(null, localization));
            blocker = $(BLOCKER_TEMPLATE.call(null, localization));

            fileContainer.appendTo(that.element);
            commandElement.appendTo(that.element);
            cancelCommand.appendTo(commandElement);
            deleteCommand.appendTo(commandElement);
            blocker.appendTo(that.element);
            deleteCommand.hide();

            that.localization = localization;
            that.cancelCommand = cancelCommand;
            that.deleteCommand = deleteCommand;
            that.blockerElement = blocker;
            cancelCommand.click(function () {
                that.element.trigger(BLUR);
            });

            deleteCommand.click(function () {
                logger.log('We are initaiting delete command ' + that.value());
                var linkStore,
                    toRemove;

                toRemove = that.value();
                linkStore = window.mainRecord.relatedList('ContentDocumentLinks');
                $.each(linkStore.data(), function (i, obj) {
                    if (obj && !sqx.common.isSFID(obj.Id) && obj.ContentDocumentId === toRemove) {
                        linkStore.remove(obj);
                        logger.log('Removed existing content doc links');
                    }
                });
                linkStore.sync();
                that.value(null);
                if (that.cancelCommand.is(':visible')) {
                    //toggle back to read mode
                    that.cancelCommand.click();
                }
                logger.log('Deletion completed');
            });
        },
        /**
         * This method creates the file element in the file container
         */
        createFileElement: function () {
            var that = this, win;

            if (that.fileElement) {
                that.fileElement.data().kendoUpload.wrapper.remove();
            }

            that.fileElement = $('<input type="file" accept="image/*" />');
            that.fileElement.appendTo(that.element.find('.cq-file-container'));
            var otherOptions = {
                success: function (r) {
                    // assumption: Single file can only be uploaded
                    if (r.files.length === 1) {
                        that.lastUploadedFileName = r.response.title;
                        fileCacheMgr.add( {
                            Id : r.response.id,
                            Title : r.response.title,
                            Description : r.response.description
                        });

                        sqx.dataStore.addToOriginal('ContentDocument', {Id : r.response.id, Title: r.response.title, Description: r.response.description});

                        that.value(r.response.id);

                        // TODO: Optimize the result because it has thumbnail and other related information
                        that.element.trigger(BLUR);
                    } else {
                        logger.error('CQContentUploader received more than one files ' + r.files);
                    }
                    that.enableContainer();
                },
                error: function () {
                    that.enableContainer();
                },
                start: function () {
                    that.disableContainer();
                },
                cancel: function () {
                    that.enableContainer();
                }
            }

            // always update the content by passing undefined. Instead of that.i_value
            sqx.createFileUploader(that.fileElement, cq.SESSION_ID, '', otherOptions);

            win = that.element.parents("[data-role='window']").data('kendoWindow');
            if (win) {
                //hide cancel in popup mode.
                that.cancelCommand.hide();
                if (!win.wrapper.data('cqContentWindowCloseEvent')){
                    win.wrapper.data('cqContentWindowCloseEvent', true);
                    win.bind('close', function () {
                        var that = this;

                        that.wrapper.find('.cq-wo[data-role="cqcontentuploader"]').each(function(i, e) {
                            var d = jQuery(e).find('.k-cancel');

                            d.click();
                        });
                    });
                }
            }
        },

        /**
         * This method sets the value in file element or returns the current value
         * @param value {Object} the value to be set in the content uploader, usually the file Id.
         *                       set <code>undefined</code> to return the current value
         */
        value: function (value) {
            var that = this,
                contentlinks,
                file, i, fileName;

            if (value === undefined) {
                return that.i_value;
            } else {
                that.i_value = value;
                that.createFileElement();
                if (that.i_value !== null) {
                    if (that.lastUploadedFileName === undefined) {
                        contentlinks = sqx.dataStore.getObjectsWithType('ContentDocumentLink');
                        file = null;
                        for (i = 0; i < contentlinks.length; i++){
                            if (contentlinks[i].ContentDocumentId === value) {
                                file = contentlinks[i];
                                break;
                            }
                        }
                        fileName = (file && file.ContentDocument.Title) || '';
                    } else {
                        fileName = that.lastUploadedFileName;
                    }
                    that.deleteCommand.text(that.localization.deleteCommand.replace('{filename}', fileName));
                    that.deleteCommand.show();
                } else {
                    that.deleteCommand.hide();
                }

                that.trigger(CHANGE);
            }
        },
        enableContainer: function () {
            var that = this;
            that.blockerElement.removeAttr('required');
        },
        disableContainer: function () {
            var that = this;
            that.blockerElement.attr('required', 'required');
        }
    });

    // add the widget to the ui namespace so it's available
    ui.plugin(contentUploader);
})(window.jQuery, window.kendo, window.sqx));