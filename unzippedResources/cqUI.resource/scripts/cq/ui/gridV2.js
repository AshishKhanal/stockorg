/*
 * @author: Anish Shrestha
 * @date: 2014/11/24
 * @description: check if device is mobile or PC 
                 if PC CAPA/NC page will have grid
                 or if it is mobile CAPA/NC page will have listview   
 */
(function ($) {

    var SFConstants = sqx.common.SFConstants;
    var kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget,
        TOOLBARTITLETMPL = '<span class="cq-toolBarTitle">@=title@</span>',
        TOOLBARCOUNTTMPL = '<span class="cq-toolbarCount">[@=dataSource.total()@]</span>',
        TOOLBARCOLLAPSE = '<img src="/img/s.gif" alt="Show Section" class="cq-gridCollapse hideListButton" tabindex="-1"/>',
        templateHashRegExp = /#/ig,              
        CLICK = "click",
        NS = ".CQGrid",
        isMobile = sqx.common.isMobile ;

    var gridOptions = {}, events = [], CQGridBaseOptions = kendo.ui.CQGridBase.prototype.options;

    $.extend(gridOptions, CQGridBaseOptions)
    gridOptions.name = "CQGrid";
    gridOptions.detailInit = null;

    events = kendo.ui.CQGridBase.prototype.events;

    var CQGrid = Widget.extend({

        options: gridOptions,
        events : events,

        _getDefaultGridTitle: function(){
            if(this.options.title != null && this.options.title.length > 0)
                return this.options.title;

            return (this.options.dataSource.options.schema.model.fn.sObjectLabel ? this.options.dataSource.options.schema.model.fn.sObjectLabel : this.options.dataSource.options.schema.model.fn.sObject)},


        init: function (element, options) {

            // base call to widget initialization
            Widget.fn.init.call(this, element, options);

            //this._createGrid();
            
            this.options.title = this._getDefaultGridTitle();
            this._create();
            //this._createListView();
        },


        _create: function () {
            var that = this;

            this.dataSource = this.options.dataSource;
            if(isMobile()){
                this._createListView();
            } else{
                this._createGrid();
            }
        },

        _createGrid: function () {
            var grid = $("<div></div>");

            this.element.append(grid);
            var newOptions = {};
            $.extend(newOptions, this.options);
            newOptions.name = CQGridBaseOptions.name;

            if(newOptions.editable && newOptions.editable.mode === 'incell'){
                newOptions.navigatable = true;
            }

            grid.kendoCQGridBase(newOptions);

            if(this.dataSource.view().length == 0 && this.options.hideIfEmpty)
                this.element.hide();
        },

        _createListView: function() {             

            if(this.options.hideIfEmpty && this.options.dataSource.view().length == 0){
                this.element.hide();
                return;
            }

            this.wrapper = this.element;

            this.element.append($('<div class="k-toolbar k-grid-toolbar"><span class="cq-grid-toolbar-command"></span></div>'));
            var listView = $('<div></div>');

            this.element.append(listView);

            var templateToUse = "";
            if(this.options.dataSource.view().length > 0)
                templateToUse = this._getTemplateFromColumn(this.options.columns, this.options.dataSource.options.schema.model.prototype);

            listView.kendoListView({
                    dataSource: this.options.dataSource,
                    template: templateToUse  });

            var that = this;
            this.element.find(".cq-list-detail-template").click(function(e){
                var currentElement = $(e.currentTarget);
                if(!currentElement.attr('data-detail-initialized')){
                    currentElement.attr('data-detail-initialized', true);

                    var uid = currentElement.parent('.listItem').data().uid,
                        data = that.options.dataSource.getByUid(uid),
                        detailDiv = $("<div></div>");

                    detailDiv.html(that.options.detailTemplate({}));
                    detailDiv.insertAfter(currentElement);

                    kendo.bind(detailDiv, data);

                    $(detailDiv).find(".subGrid").each(function(index, element){
                        
                        element = $(element);
                        //element.attr("data-role", "cqsmartgrid");
                        var elemOptions = kendo.parseOptions(element[0], gridOptions),
                            detailRowSourceName = element.attr("data-relatedlist");

                        if(element.attr("data-options")){
                            $.extend(elemOptions, $.parseJSON(element.attr("data-options")));
                        }

                        if(element.attr('data-detailTemplate')){
                            var detailTemplateId = element.attr("data-detailTemplate");

                            if (detailTemplateId != undefined) {
                                elemOptions.detailTemplate = kendo.template($("#" + detailTemplateId).html());
                            }
                        }

                        elemOptions.dataSource = data.relatedList(detailRowSourceName) || data[detailRowSourceName]();
                        elemOptions.dataSource.read();

                        element.kendoCQGrid(elemOptions);

                    });
                }
                else{
                    currentElement.next().toggle();
                }

            });

            this.customizeToolBar();
        },

        _getTemplateFromColumn : function(columns, schema){

            var that = this,
                dataSource = this.options.dataSource,
                dataSourceHasModel = schema;               
            var template = '<div class="listItem cqHeaderBlock">',
                length, index, column;
            //template += '<div class="k-toolbar k-grid-toolbar"><span class="cq-grid-custom-toolbar"></span></div>';
            //TODO: Add title to each record in list view
            //data.schema.fields[
            if(columns && columns.length > 0){
                for(index = 0, length = columns.length; index < length; index++){
                    column = columns[index];

                    if(column.hidden)
                        continue;

                    template += "<div class='listRow'>";
                    
                    if (!column.command && dataSourceHasModel) {
                        var colDef = dataSource.options.schema.model.fields[column.field];                        

                        if(!colDef){
                            template += '<div class="listDef">' + column.title + '</div>';
                            if(column.template){
                                if(kendo.isFunction(column.template)){
                                    var functionName;
                                    sqx.anon2NamedFuncs = sqx.anon2NamedFuncs || {};
                                    functionName = kendo.guid();
                                    sqx.anon2NamedFuncs[functionName] = column.template;

                                        functionName = "sqx.anon2NamedFuncs['" + functionName + "']";

                                    template += ('<div class="listValue">#=' + functionName + "(data) #</div>");
                                }
                                else
                                    template += ('<div class="listValue">' + column.template + "</div>");

                            }
                            else
                                template += ('<div class="listValue">#= ' + column.field + ' == null ? "" : ' + column.field + '  #</div>');
                        }

                        else{

                            if (colDef && !column.command && !column.title && colDef.name) {
                                column.title = colDef.name;
                            }

                            template += '<div class="listDef">' + column.title + '</div>';
                            template += that._createReadOnlyElement(colDef, column);                                    
                        }

                    }

                    else{

                        template += '<div class="listDef">' + column.title + '</div>';
                        template += ('<div class="listValue">#= ' + columns[index].field + ' || "" #</div>');                        
                    }
                    
                    template += "</div>";
                }
            }

            if(this.options.detailTemplate){
                template += "<div class='cq-list-detail-template'>Click to view detail</div>";
            }
            
            template += "</div>";
            
            return template;                    
        },

        //CQ:custom adds title, count
        customizeToolBar: function () {
            var html = "", toolBarWrapper, customToolBarWrapper, that = this;

            toolBarWrapper = that.wrapper.find(".k-grid-toolbar")[0];
            if (toolBarWrapper) {
                toolBarWrapper = $(toolBarWrapper);
                customToolBarWrapper = toolBarWrapper.find(".cq-grid-custom-toolbar");
                if (that.options.showCount && that.options.showTitle) { html = TOOLBARCOUNTTMPL + html; } //no count without title

                if (that.options.showTitle) { html = TOOLBARTITLETMPL + html; }
                else if (!that.options.showTitle){toolBarWrapper.addClass('k-grid-toolbar-without-header'); }
                $('.k-grid-toolbar.k-grid-toolbar-without-header').hide();

                html = "<span class=\"cq-grid-custom-toolbar\">" + html.replace(templateHashRegExp, "\\#").replace(/@/ig, "#") + "</span>";
                $(kendo.template(html)(that.options)).prependTo(toolBarWrapper.find(".cq-grid-toolbar-command"));

            }
        },

        _createReadOnlyElement: function(field, column){
            if(column.template){
                if(kendo.isFunction(column.template)){
                    var functionName;
                                    
                    sqx.anon2NamedFuncs = sqx.anon2NamedFuncs || {};
                    functionName = kendo.guid();
                    sqx.anon2NamedFuncs[functionName] = column.template;

                    functionName = "sqx.anon2NamedFuncs['" + functionName + "']";
                                         
                                    
                    return '#=' + functionName + "(data) #";
                }
                else
                    return column.template;
            }
            var templateObj = ('<div class="listValue">#= ' + column.field + ' || "" #</div>'); 
            if (field.type == SFConstants.TYPE_BOOLEAN)
                templateObj = ('<div class="#= ' + column.field + ' ? \"cq-boolean-checked\" : \"cq-boolean-unchecked\" #"> </div>');
            else if(field.type == SFConstants.TYPE_DATE){
                var defaultFormat = 'MMMM dd, yyyy', 
                    format = null;
                format = field.dateFormat == null ? defaultFormat : field.dateFormat;
                templateObj = ('<div>#= ' + column.field + ' != null ? kendo.toString('  + column.field  + ', "' + format + '") : "" #</div>');
            }

            return templateObj;               
        },

        rows : function(){
            if(isMobile()){
                return this.element.find(".listItem");
            }
            else{
                return this.element.find("tr[data-uid]");
            }
        },

        dataItem: function(row){
            if(isMobile()){
                return this.dataSource.getByUid($(row).attr('data-uid'));
            }
            else{
                return this.element.children('[data-role="cqgridbase"]').data('kendoCQGridBase').dataItem(row);
            }
        }

    });
    // add the widget to the ui namespace so it's available
    ui.plugin(CQGrid);

})(jQuery);

/*Extends CQ Grid to add expand record*/
kendo.ui.CQGrid.fn.expandRecord = function(recordId, expandChildGrids){
    if(sqx.common.isMobile()){

    }else{
        this.element.children('[data-role="cqgridbase"]').data('kendoCQGridBase').expandRecord(recordId, expandChildGrids);
    }
};

/**
* This method provides a way of routing the grid item to correct edit row method.
* Since, a grid can be either a grid view or list view, and CQGrid wraps around
* the creation logic of the two. CQGrid acts as a router for row edit.
*/
kendo.ui.CQGrid.fn.editRow = function(dataItem){
    if(sqx.common.isMobile()){
        //do nothing
        sqx.common.error('Adding/Editing not supported in mobile');
    }
    else{
        var grid = this.element.children('[data-role="cqgridbase"]'),
            row = grid.find("[data-uid='" +  dataItem.uid + "']");

        grid.data('kendoCQGridBase').editRow(row);
    }
};

/**
* this functions sets red marker indicating changed field in the grid columns
* @params: dataItem : record whose field is changed
*           columns : field whose value is changed
*/
kendo.ui.CQGrid.fn.setDirty = function(dataItem, columns){
    if(sqx.common.isMobile()){
        //do nothing
        sqx.common.error('Adding/Editing not supported in mobile');
    }
    else{
        var grid = this.element.children('[data-role="cqgridbase"]'),
            row = grid.find("tr[data-uid='" +  dataItem.uid + "']"),
            fields = grid.data().kendoCQGridBase.options.columns,
            cells =  row.find('td[role="gridcell"]'),
            cell;

        for(var i = 0; i < fields.length; i++){

            if(fields[i].field && columns[fields[i].field]){
                cell = cells.eq(i);
                cell.addClass("k-dirty-cell");
                $('<span class="k-dirty"/>').prependTo(cell);
            }
        }

    }
};
