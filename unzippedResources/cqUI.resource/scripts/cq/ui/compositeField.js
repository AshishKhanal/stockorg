/**
* @author Sagar Shrestha
* @date 2014/4/21
* @description this custom widget a provides a composite control that renders both label and value mapping SF fields to kendo fields.
* @modified Pradhanta Bhandari [2014/4/21] refactored
*
* NOTE: kendo.web.js relies on the name field of the output widget being same as the API name. Please do not change this behaviour
*       unless you plan to fix other behaviour. Yes, I too wish we had a unit test instead of a comment :)
* TODO: use inheritance to provide functionality, since composite field is getting complex.
*/
(function ($, translate) {
    "use strict";
    sqx.common.toDo('Add read picklist template, that shows text not value of a selected picklist');

    function clearSelection() {
        if ( document.selection ) {
            document.selection.empty();
        } else if ( window.getSelection ) {
            window.getSelection().removeAllRanges();
        }
    }

    /**
     * Checks whether or not the UI is in edit mode.
     * @returns true if the UI is in edit mode else returns false
     */
    function isUIInEditMode() {
        var editMode = (getUrlParameter('editMode') !== undefined && getUrlParameter('editMode')) || window.sqx.editMode;

        return editMode;
    }

    // Help: shorten references to variables. this is better for uglification
    var SFConstants = sqx.common.SFConstants,
        kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget,
        _isInitialized = false,DATABINDING = "dataBinding",
        DATABOUND = "dataBound",
        EXTRA_INFO_TEMPLATE_SELECTOR = ".extra-info-template",
        CHANGE = "change",
        PREFIX = 'cq-',
        template = kendo.template;


    var FormItemClear = template("<div class='clearFix'></div>"),
        LabelTemplate = template("<div class='formLabel slds-form-element__label'><label for='#= Id #'>#= LabelText #</label></div>"),
        HelpTemplate = template("<div class='helptext' title='#= additionalInfo.helptext #' data-role='tooltip' data-position='right' data-show-on='mouseenter' data-auto-hide='true' ></div"),
        FieldTemplate = template("<div class='formField slds-form-element__control'><div class='readonlyElement'></div><div class='editableElement'></div></div>"),

        /*Field Templates Read Only (RO)*/
        defaultTemplateRO = template("<div><span data-bind='text:#= options.path +additionalInfo.apiname# #if(options.dynamicStyle != ''){#,style:{#=options.dynamicStyle#}' #}#'></span></div>"),
        pickListTemplateRO = template("<div><span data-bind='cqdropdownsource: #= options.path + additionalInfo.apiname#_ValidValues.view(), cqdropdown:#= options.path +additionalInfo.apiname# #if(options.dynamicStyle != ''){#,style:{#=options.dynamicStyle#}' #}#'></span></div>"),
        dateTemplateRO = template("<div><span data-bind='date:#= additionalInfo.apiname #'></span></div>"),
        datetimeTemplateRO = template("<div><span data-bind='datetime:#= additionalInfo.apiname #'></span></div>"),
        booleanTemplateRO = template("<div><span data-bind='bool: #= additionalInfo.apiname #'></span></div><div class='extra-info-template'></div>"),
        booleanTemplateSLDSRO = template("<div class='slds-form-element'><label class='slds-checkbox_toggle slds-grid'><input class='cq-ro' type='checkbox' data-bind='checked:#= additionalInfo.apiname #' name='#= additionalInfo.apiname #' disabled='disabled' /><span class='slds-checkbox_faux_container' aria-live='assertive'><span class='slds-checkbox_faux'></span><span class='slds-checkbox_on'></span><span class='slds-checkbox_off'></span></span></label></div><div class='extra-info-template'></div>"),
        fileFieldRO = template("<div><span data-bind='file: #= options.path + additionalInfo.apiname #'></span></div>"),

        referenceTemplateRO = template("<div><a data-bind='text: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ? additionalInfo.apiname.replace(\"__c\",\"__r.\") + datatextfield : additionalInfo.apiname.replace(\"Id\",\".Name\") # , reference: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ?  additionalInfo.apiname.replace(\"__c\",\"__r.Id\") :  additionalInfo.apiname.replace(\"Id\",\".Id\")#'  target='_blank'></a></div><div class='extra-info-template'></div>"),

        //standardFieldReferenceTemplateRO: "<div><a data-bind='text: #= additionalInfo.apiname.replace(\"Id\",\".Name\")# , reference: #=  additionalInfo.apiname.replace(\"Id\",\".Id\")#}'  target='_blank'></a></div>"),
        richTextTemplateRO = template("<div><div data-bind='html: #= additionalInfo.apiname #'></div></div>"),
        starTemplateRO = template("<div><ul data-bind='value: #= additionalInfo.apiname #' data-role='rating' data-readonly='true' ><li data-value='1' title='One'></li><li data-value='2' title='Two'></li><li data-value='3' title='Three'></li><li data-value='4' title='Four'></li><li data-value='5' title='Five'></li></ul></div>"),
        changeOwnerTemplateRO = template("<div><a data-bind='text: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ? additionalInfo.apiname.replace(\"__c\",\"__r.Name\") : additionalInfo.apiname.replace(\"Id\",\".Name\") # , reference: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ?  additionalInfo.apiname.replace(\"__c\",\"__r.Id\") :  additionalInfo.apiname.replace(\"Id\",\".Id\")#'  target='_blank'></a><a data-bind='attr:{href : changeOwnerLink}, cqHide: changeOwnerLinkHidden' class='changeownerlink'>#: changeOwnerText #</a></div>"),
        changeOwnerWithoutLinkTemplateRO = template("<div><a data-bind='text: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ? additionalInfo.apiname.replace(\"__c\",\"__r.Name\") : additionalInfo.apiname.replace(\"Id\",\".Name\") # , reference: #= (additionalInfo.apiname.indexOf(\"__c\")!=-1) ?  additionalInfo.apiname.replace(\"__c\",\"__r.Id\") :  additionalInfo.apiname.replace(\"Id\",\".Id\")#'  target='_blank'></a></div>"),

        /*Field Templates Write (WO)*/
        defaultTemplateWO = template("<input class='cq-default-input cq-wo slds-input' data-bind='value: #= options.path+apiname #' id='#= Id #' name='#= apiname #' #= otherattributes #/>"),
        dateTemplateWO = template("<input class='cq-wo' data-type='date' data-bind='value:#= apiname ##= otherbindoptions #' id='#= Id #' name='#= apiname #' data-role='cqdatepicker' data-no-past-date='#= noPastDate #' data-no-future-date='#= noFutureDate #' #= otherattributes # />"),
        datetimeTemplateWO = template("<input class='cq-wo' data-type='datetime' data-bind='value:#= apiname ##= otherbindoptions #' id='#= Id #' name='#= apiname #' data-role='cqdatetimepicker' data-no-past-date='#= noPastDate #' data-no-future-date='#= noFutureDate #' #= otherattributes # />"),
        pickListTemplateWO = template("<input class='cq-wo' data-role='dropdownlist' data-text-field='text' data-value-field='value' data-value-primitive='true' data-bind='value:#= options.path+apiname#, source: #= options.path+apiname#_ValidValues.view()' data-option-label='#:labelString #' data-auto-bind= 'true' id='#= Id #' name='#= apiname #'/>"),
        controlledPickListTemplateWO = template("<input class='cq-wo' data-role='dropdownlist' data-text-field='text' data-value-field='value' data-value-primitive='true' data-bind='value:#= options.path+apiname#, source: #= options.path+apiname#_ValidValues.view(), controlledby: #: controlledBy #' data-option-label='#:labelString #' data-auto-bind= 'true' id='#= Id #' name='#= apiname #'/>"),
        booleanTemplateSLDSWO = template("<div class='slds-form-element'><label class='slds-checkbox_toggle slds-grid'><input class='cq-wo' type='checkbox' data-bind='checked:#= apiname #' id='#= Id #' name='#= apiname #' /><span class='slds-checkbox_faux_container' aria-live='assertive'><span class='slds-checkbox_faux'></span><span class='slds-checkbox_on'></span><span class='slds-checkbox_off'></span></span></label></div>"),
        booleanTemplateWO = template("<input class='cq-wo' type= 'checkbox' data-bind='checked:#= apiname #' id='#= Id #' name='#= apiname #' />"),
        //referenceTemplateWO: "<input data-role='combobox' data-placeholder='#= placeholder#' data-text-field='#= datatextfield #' data-value-field='#= datavaluefield #' data-value-primitive='false' data-bind='value: #= apiname #, source: #= sourcename #, enabled: true' name='#= apiname #' data-api-name='#= apiname #' id='#= Id #' data-cascade='_cqCompositeFieldComboChange'/>"),
        referenceTemplateWO = template("<input class='cq-wo' data-auto-bind='false' data-filter='contains'  data-role='cqcombobox' data-placeholder='#= placeholder#' data-text-field='#= datatextfield #' data-value-field='#= datavaluefield #' data-value-primitive='false' data-bind='value: #= apiname.replace('__c','__r') #, source: #= sourcename #' name='#= apiname #' data-api-name='#= apiname #' id='#= Id #' #= otherattributes # data-cascade='_cqCompositeFieldComboChange'/><div class='extra-info-template'></div>"),
        referenceTemplateForPicklistWO = template("<input class='cq-wo' data-auto-bind='false' data-filter='contains'  data-role='dropdownlist' data-placeholder='#= placeholder#' data-text-field='compliancequest__Value__c' data-value-field='Name' data-value-primitive='true' data-bind='value: #= apiname #, source: #= sourcename #' name='#= apiname #' data-api-name='#= apiname #' data-option-label='--None--' id='#= Id #' #= otherattributes # />"),
        fileFieldWO = template("<div class='cq-wo' data-bind='value: #= options.path+apiname #' data-role='cqcontentuploader'></div>"),

        // cascadeReferenceTemplateWO : used when data-casacade-from is specified
        cascadeReferenceTemplateWO = template("<input class='cq-wo' data-auto-bind='false' data-filter='contains' data-role='cqcombobox' data-placeholder='#= placeholder#' data-text-field='#= datatextfield #' data-value-field='#= datavaluefield #' data-value-primitive='false' data-bind='value: #= apiname.replace('__c','__r') #, source: #= sourcename #' data-api-name='#= apiname #' data-cascade-from='#= cascadeFrom #' data-cascade-from-field='#= cascadeField #' name='#= apiname #' id='#= Id #' data-cascade='_cqCompositeFieldComboChange'/>"),

        //dependentReferenceTemplateWO : is used by a field that uses depends on attribute from schema
        dependentReferenceTemplateWO = template("<input class='cq-wo' data-auto-bind='false' data-filter='contains' data-role='cqcombobox' data-placeholder='#= placeholder#' data-text-field='#= datatextfield #' data-value-field='#= datavaluefield #' data-value-primitive='false' data-bind='value: #= apiname.replace('__c','__r') #, source: #= sourcename # #= otherbindoptions #' #= otherattributes #  id='#= Id #' data-cascade='_cqCompositeFieldComboChange' name='#= apiname #' />"),



        numberTemplateWO = template("<input class='cq-wo' data-role='numerictextbox' data-bind='value: #= apiname #' id='#= Id #' name='#= apiname #' data-type='number' max='#= maxval#' min='#= minval #' data-decimals='#= decimals #' data-format='#= format #' #= otherattributes #/>"),
        multilineTextTemplateWO = template("<textarea rows='4' class='cq-wo slds-input cq-multi-line-textarea' data-bind='value: #= apiname #' id='#= Id #' name='#= apiname #' maxlength='#= maxlength #'></textarea>"),
        richtextTemplateWO = template("<textarea rows='4' class='cq-wo' id='#= Id #' name='#= apiname #' data-role='editor' data-tools=\"['bold', 'italic', 'underline', 'strikethrough', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'createTable', 'addColumnLeft', 'addColumnRight', 'addRowAbove', 'addRowBelow', 'deleteRow', 'deleteColumn']\" data-bind='value: #= apiname #'></textarea>"),
        starTemplateWO = template("<input class='cq-wo' type='text' data-bind='value: #= apiname #' name='#= apiname #' style='display:none;' /><ul data-bind='value: #= apiname #' data-role='rating' data-readonly='false'><li data-value='1' title='One'></li><li data-value='2' title='Two'></li><li data-value='3' title='Three'></li><li data-value='4' title='Four'></li><li data-value='5' title='Five'></li></ul>"),
        undoTemplate = template("<span class='undohelper'><span class='text'></span><button class='inlineundo' aria-label='Undo changes'>&nbsp;</button></span>");


    // Help: declare the custom input widget by extenting the very base kendo widget
    var cqCompositeField = Widget.extend({

        /**
        * @description init method is default initializer for all kendo widget.
        */
        init: function (element, options) {

            try {
                sqx.common.debugLog("Initializing composite field widget for " + (!!options ? options.container : element.container));

                // cache a reference to this
                var that = this;
                that.i_editstate = true;

                if (options.container && !options.field) {
                    options.field = options.container.slice(options.container.lastIndexOf(".") + 1)
                    options.container = options.container.slice(0, options.container.lastIndexOf("."));
                }
                //infer field name from schema field reference
                if (options.schemaObjectField && typeof (options.schemaObjectField) === 'string') {
                    options.field = options.schemaObjectField.slice(options.schemaObjectField.lastIndexOf(".") + 1);
                }

                if (options.readOnly !==true){
                    $(element).attr('data-bind', ($(element).attr('data-bind')?$(element).attr('data-bind')+',' :'') +" cqRequired:isRequired." + options.field +',' +" cqHide:isHidden." + options.field +',' +" cqReadonly:isReadonly." + options.field);
                } // make the base call to initialize this widget


                Widget.fn.init.call(that, element, options);

                if (options.editMode == true) {
                    //if initially in editmode then inline editing is off, for all fields except file type
                    that.options.inlineEdit = options.field.type === sqx.common.SFConstants.TYPE_FILE;
                }

                if (options.container) { that.options.container = kendo.getter(options.container)(window); }

                console.log(that.options);
                // actually create the UI elements. this is a private method
                // that is below
                that._create();

                //convert the validation group to class so that it can be used by kendo validator.
                if(this.options.validationGroup){
                    this.element.addClass('cq-val-' + this.options.validationGroup);
                }

            }
            catch (ex) {
                var initErr = $("<div class='cq-error slds-error'></div>");
                initErr.text('Error occurred while initializing ' + (!!options ? options.field : element.field) + '. Inner Error: ' + ex.message);
                initErr.prependTo(this.element);
                sqx.common.error(initErr.text(), ex);
            }
        },

        options: {
            name: "cqCompositeField",
            // schemaObjectField: the field for which this composite control is to be rendered
            schemaObjectField: null,
            // autocompleteSource: auto complete text field name
            autocompleteSource: null,
            // placeholder: placeholder text for field with auto complete
            placeholder: "",
            // autocompleteTextField: auto complete text field name
            autocompleteTextField: "",
            // autocompleteValueField: auto complete value field name
            autocompleteValueField: "Id",
            // cascadeFrom: cascade from field name
            cascadeFrom: null,
            // cascadeFromField: The field in the response data that is used to filter by parent info
            cascadeField: null,
            editMode: null,
            // _isInEditMode: internal flag to set whether the widget is in edit mode or not.
            _isInEditMode: false,
            container: null, //if binding to a specific row send this else send path from parent container along with schemaObjectField
            path: "",
            field: null,
            // _boundData: associates a data to the object
            _boundData: null,
            readOnly: false, //todo:moved from render
            // render: renders Label, HelpText, ReadOnly as set by default renders everything in non-readonly mode
            render: { Label: true, HelpText: true },
            // _readElement: stores the reference to DOM read Element
            _readElement: null,
            // _writeElement: stores the reference to DOM write Element
            _writeElement: null,
            _labelElement: null,
            // Id : id of this widget
            Id: null,
            // fieldScope: cascade field scoping by scope name
            fieldScope: null,
            dynamicStyle: "",
            //inlineEdit: enables disables the inline editing feature, default is true
            inlineEdit: true,
            headerTemplateId: null,
            templateId: null,
            // group of field to be validated before saving record
            validationGroup: null,
            // enables disables change owner link. default is true
            changeOwner : true,
            // template id to show along with the field
            helperTemplateId: null,
            // will ignore any bound data's edit rule i.e. even if edit is returning false, this will ignore it
            ignoreDataEditRules : false
        },
        events:
            [DATABINDING,DATABOUND,CHANGE]
        ,

        // this function creates each of the UI elements and appends them to the element
        // that was selected out of the DOM for this widget
        _create: function () {

            // cache a reference to this
            var that = this;

            // setup the label
            if (that.options.schemaObjectField !== null && typeof (this.options.schemaObjectField) === 'string') {
                that.options.schemaObjectField = kendo.getter(that.options.schemaObjectField)(window);

            }
            if (that.options.container && that.options.field) {
                that.options.schemaObjectField = Object.getPrototypeOf(that.options.container).fields[that.options.field];
                that.options._boundData = that.options.container;
            }

            if (!this.options.schemaObjectField) {
                sqx.common.debugLog('Please initialize field and data for the widget through dataContainer and field');
                return;
            }

            this.options.autobind = !!this.options.container; //if container was provided then autobind


            if(this.options.editMode === null){
                this.options.editMode = (that.options._boundData && that.options._boundData.isNewObject());
            }
            var editMode = isUIInEditMode();

            var switchToEditMode = (this.options.editMode || editMode) && !cq.printMode;

            if (switchToEditMode || cq.printMode) { this.options.inlineEdit = false;}

            var isAlreadyTemplated = that.element.attr("data-initialized");

            if (isAlreadyTemplated)
                return; //prevents duplication of the control for same object

            that.options.Id = PREFIX + kendo.guid();

            that.element.addClass('formItem slds-form-element');

            that._containerItem = $("<div></div>");

            that.element.append(that._containerItem);

            var field = that.options.schemaObjectField;

            if(field.type === SFConstants.TYPE_FILE) {
                that.options.inlineEdit = true;
            }

            that._field = field;
            if (that.options.render.Label) {
                that._createLabel(field, that._containerItem);
            }

            //always render non-edit template

            that._createFieldElement(field, that.options._boundData, that._containerItem);

            // append all elements to the DOM
            //            that.element.html(that._containerItem);


            if (this.options.autobind) {
                kendo.bind(that.element, that.options._boundData);
                //              kendo.init(that.element);
            }

            //review: not needed, clearing inside container that has only 1 formitem to start with
            //            $(that._templates.FormItemClear).appendTo(that._containerItem);

            this.element.addClass('readmode'); //by default we have read mode

            if(field.type === sqx.common.SFConstants.TYPE_FILE
                && that.options._boundData && that.options._boundData[field.additionalInfo.apiname]) {
                that.setEditMode(false);
            }
            else {
                switchToEditMode = switchToEditMode && that.i_editstate;
                that.setEditMode(switchToEditMode);
            }

            that.element.attr("data-initialized", "true");

        },
        //data-no-past-dates=#= noPastDates# data-no-future-dates=#= noFutureDates #

        /**
        * @description creates the label for the field.
        */
        _createLabel: function (field, container) {
            //todo: make conditional for Id

            var labelElement = $(LabelTemplate({ Id: this.options.Id, LabelText: field.name }));
            if (this.options.render.HelpText && field.additionalInfo.helptext !== undefined)
                this._createHelpText(field, labelElement);

            this.options._labelElement = labelElement;

            $(labelElement).appendTo(container);

            if (field.additionalInfo.multiline) {
                labelElement.addClass('tall'); //reaches formItem
            }
        },

        /**
        * @description creates a help text for the field if present
        */
        _createHelpText: function (field, container) {

            if (field.additionalInfo.helptext !== undefined) {
                //if there is help text
                var helpElement = $(HelpTemplate(field));
                helpElement.prependTo(container);

                kendo.bind(helpElement);
            }
        },

        /**
        * @description creates a field element to display the fields
        */
        _createFieldElement: function (field, data, container) {
            var fieldElement = $(FieldTemplate({})),
                isReadOnly = (!kendo.isFunction(field.editable) && !field.editable );

                if(data != null ){
                    isReadOnly = !data.editable(field.additionalInfo.apiname);
                }

                /*
                    i. Check if schema defines field is non-editable
                    ii. If data is defined, then data is editable
                    iii. If data is defined and editable then
                            data.editable(field) is true
                */

            this._createReadOnlyElement(field, data, fieldElement);

            fieldElement.appendTo(container);



            if (!this.options.readOnly && !isReadOnly) {
                this._createReadWriteElement(field, data, fieldElement);


                if(this.options.inlineEdit && this.options.editMode === false ){
                    this._createInlineEditor(field, data, container);
                }
            }
            else{
                fieldElement.find('.editableElement').hide();
            }

            this._createExtraInfoTemplate(field, data, fieldElement);
        },

        /**
         * @description: adds template to append extra info with the field
         * @param: field - field in which we have to add extra template
         * @param: data -  info about the field
         * @param: container - div which contains extra template
         * TODO: Currently it supports for only reference field type,
         *       Need to add support for other field types
         */
        _createExtraInfoTemplate: function(field, data, container){
            if(!this.options.helperTemplateId){
                //remove helper template
                container.find(EXTRA_INFO_TEMPLATE_SELECTOR).remove();
                return;
            }
            else{
                var extraInfoTemplateContainer = container.find(EXTRA_INFO_TEMPLATE_SELECTOR);
                if(extraInfoTemplateContainer.length == 0){
                    sqx.common.error('Helper container not defined for ' + field);
                }
                else{
                    this.element.addClass('compositeFieldLink');
                    extraInfoTemplateContainer.html($("#" + this.options.helperTemplateId).html());
                }
            }
        },


        /**
        * @description creates  read only element for the data
        */
        _createReadOnlyElement: function (field, data, container) {

            var templateObj = defaultTemplateRO;
            if (field.type == SFConstants.TYPE_PICKLIST){
                templateObj = pickListTemplateRO;
            }
            else if (field.type == SFConstants.TYPE_BOOLEAN){
                templateObj = sqx.common.isLightningScope() ? booleanTemplateSLDSRO : booleanTemplateRO;
            }
            else if (field.type == SFConstants.TYPE_DATE){
                templateObj = dateTemplateRO;
            }
            else if (field.type == SFConstants.TYPE_DATE_TIME){
                templateObj = datetimeTemplateRO;
            }
            else if (field.type == SFConstants.TYPE_REFERENCE){
                templateObj = referenceTemplateRO;
                field.datatextfield = this.options.autocompleteTextField || field.additionalInfo.autocompleteTextField || 'Name';
                field.datavaluefield = this.options.autocompleteValueField || field.additionalInfo.autocompleteValueField || 'Id';
            }
            else if (field.type == SFConstants.TYPE_STRING && field.additionalInfo.richtext){
                templateObj = richTextTemplateRO;
            }
            else if (field.type === SFConstants.TYPE_FILE){
                templateObj = fileFieldRO;
            }

            if (field.additionalInfo.uitype == 'Rating')
                templateObj = starTemplateRO;

            if(field.additionalInfo.apiname == 'OwnerId'){
                if(this.options.changeOwner){

                    if(window.sforce){
                        templateObj = changeOwnerWithoutLinkTemplateRO;
                    } else {
                        templateObj = changeOwnerTemplateRO;
                    }
                    field.changeOwnerText = 'Change Owner';
                    if(sqx.translation){
                        field.changeOwnerText = sqx.translation.get('common.changeowner');
                    }
                }
            }

            field.options = this.options;
            var readonlyElement = $(templateObj(field));
            readonlyElement.appendTo(container.find(".readonlyElement"));
            this.options._readElement = readonlyElement.eq(0); //use only the first div because second one is likely to be extra-info div

            //add tall tag to formItem if is multiline, will fatten the element
            if (field.additionalInfo.multiline) {
                container.addClass('tall'); //reaches formItem
            }

        },


        /**
        * @description creates read write element for the data
        */
        _createReadWriteElement: function (field, data, container) {
            var templateObj = defaultTemplateWO,
                editorField = field.additionalInfo.apiname,
                templateInitializer = { apiname: editorField, Id: this.options.Id, name: field.name, options: this.options },
                index, searchByFields;

            if (templateInitializer.options.path != "") templateInitializer.options.path = templateInitializer.options.path + ".";
            var maxTextLength = parseInt(field.validation.maxlength);
            if(maxTextLength){
                templateInitializer.otherattributes= " maxlength='" + maxTextLength + "' ";
                templateInitializer.maxlength = maxTextLength;
            }

            if(field.additionalInfo.inputType){
                templateInitializer.otherattributes += " type = '" + field.additionalInfo.inputType  + "' data-"+ (field.additionalInfo.inputType).toLowerCase() +"-msg='" + field.name +" is invalid'";
            }

            if (field.type === SFConstants.TYPE_FILE){
                templateObj = fileFieldWO;
            }
            else if (field.type == SFConstants.TYPE_BOOLEAN){
                templateObj = booleanTemplateWO;
                if(sqx.common.isLightningScope()) {
                    templateObj = booleanTemplateSLDSWO;
                }
            }
            else if (field.type == SFConstants.TYPE_DATE) {
                templateObj = dateTemplateWO;
                templateInitializer.noPastDate = field.additionalInfo.noPastDate || false;
                templateInitializer.noFutureDate = field.additionalInfo.noFutureDate || false;

                templateInitializer.otherbindoptions = '';
                templateInitializer.otherattributes = '';

                if(field.additionalInfo.min !== undefined){
                    templateInitializer.otherbindoptions += ', min : ' + field.additionalInfo.min;

                    if(field.additionalInfo.minMsg !== undefined){
                        templateInitializer.otherattributes = ' data-datemincheck-msg=\"' + field.additionalInfo.minMsg + '\" '
                    }
                }

                if(field.additionalInfo.max !== undefined){
                    templateInitializer.otherbindoptions += ', max : ' + field.additionalInfo.max;

                    if(field.additionalInfo.maxMsg !== undefined){
                        templateInitializer.otherattributes += ' data-datemaxcheck-msg=\"' + field.additionalInfo.maxMsg + '\" '
                    }
                }

                templateInitializer.otherattributes += (" data-format='" + sqx.common.CQDateFormat + "' ");
                var name = (field && field.name) || 'Field';
                var date_not_valid_msg = translate.get('cqcompositefield.invaliddate', { fieldName: name});
                templateInitializer.otherattributes += (" data-date-msg='" + date_not_valid_msg + "' ");
            }
            else if(field.type == SFConstants.TYPE_DATE_TIME){
                templateObj = datetimeTemplateWO;
                templateInitializer.noPastDate = field.additionalInfo.noPastDate || false;
                templateInitializer.noFutureDate = field.additionalInfo.noFutureDate || false;

                templateInitializer.otherbindoptions = '';
                templateInitializer.otherattributes = '';

                if(field.additionalInfo.min !== undefined){
                    templateInitializer.otherbindoptions += ', min : ' + field.additionalInfo.min;
                    
                    if(field.additionalInfo.minMsg !== undefined){
                        templateInitializer.otherattributes = ' data-datemincheck-msg=\"' + field.additionalInfo.minMsg + '\" '
                    }
                }

                if(field.additionalInfo.max !== undefined){
                    templateInitializer.otherbindoptions += ', max : ' + field.additionalInfo.max;

                    if(field.additionalInfo.maxMsg !== undefined){
                        templateInitializer.otherattributes += ' data-datemaxcheck-msg=\"' + field.additionalInfo.maxMsg + '\" '
                    }
                }

                templateInitializer.otherattributes += (" data-format='" + sqx.common.CQDateTimeFormat + "' ");
                var name = (field && field.name) || 'Field';
                var date_not_valid_msg = translate.get('cqcompositefield.invaliddate', { fieldName: name});
                templateInitializer.otherattributes += (" data-date-msg='" + date_not_valid_msg + "' ");
            }
            else if (field.type == SFConstants.TYPE_PICKLIST) {
                templateObj = pickListTemplateWO;
                templateInitializer.labelString = translate.get('cqcompositefield.dropdownlabel');

                if(field.additionalInfo.controllingField){
                    templateObj = controlledPickListTemplateWO;
                    templateInitializer.controlledBy = field.additionalInfo.controllingField;
                }
            }
            else if (field.type == SFConstants.TYPE_REFERENCE) {

                templateInitializer.placeholder = this.options.placeholder || translate.get('cqcompositefield.comboplaceholder', { fieldName : field.name });
                templateInitializer.datatextfield = this.options.autocompleteTextField || field.additionalInfo.autocompleteTextField ||  'Name';
                templateInitializer.datavaluefield = this.options.autocompleteValueField  || field.additionalInfo.autocompleteValueField ||  'Id';
                templateInitializer.sourcename = this.options.autocompleteSource || templateInitializer.apiname + '_Source';
                templateInitializer.otherattributes='';
                

                if(field.additionalInfo.dependsOn !== undefined){
                    templateObj = dependentReferenceTemplateWO;
                    templateInitializer.otherbindoptions = ", dependson: " + field.additionalInfo.dependsOn;
                    templateInitializer.otherattributes = "data-depends-on-field='" + field.additionalInfo.dependsOnField + "' " ;
                    if (field.additionalInfo.allowNullParent !== undefined) {
                        templateInitializer.otherattributes += "data-allow-null-parent='" + field.additionalInfo.allowNullParent + "' ";
                    }
                }
                else{

                    if (this.options.cascadeFrom !== null) {
                        templateObj = cascadeReferenceTemplateWO;

                        templateInitializer.cascadeFrom = $("[data-field-scope = '" + this.options.fieldScope + "']").find("input[data-api-name='" + this.options.cascadeFrom + "']").attr('id');

                        sqx.common.assert(templateInitializer.cascadeFrom !== undefined || 'Cascade from is undefined for ' + field);

                        templateInitializer.cascadeField = this.options.cascadeField;

                    }
                    else {
                        templateObj = referenceTemplateWO;
                    }
                }

                if(this.options.headerTemplateId){
                    console.log(this.options.headerTemplateId);
                    //var headerTemplateForPart= kendo.template($("#"+this.options.headerTemplateId).html());
                    templateInitializer.otherattributes +=" data-header-template='"+this.options.headerTemplateId+"'";
                }

                if(this.options.templateId || field.additionalInfo.templateId){
                    console.log(this.options.templateId);
                    templateInitializer.otherattributes +=" data-template='"+ (this.options.templateId || field.additionalInfo.templateId) + "'";
                }

                if(field.additionalInfo.useSearch) {
                    templateInitializer.otherattributes += " data-min-length='2' ";
                }

                searchByFields = field.additionalInfo.searchByFields;

                // list of fields used for search in the combobox
                if(searchByFields){
                    templateInitializer.otherattributes += " data-cq-filter-fields='[";
                    for(var index = 0; index < searchByFields.length; ){
                        templateInitializer.otherattributes += "\"" + searchByFields[index]  + "\"";
                        index++;
                        if(index < searchByFields.length)
                            templateInitializer.otherattributes += ',';
                    }

                    templateInitializer.otherattributes += " ]' ";
                }

            }
            else if (field.type == SFConstants.TYPE_PICKLIST_OBJECT) {
                templateInitializer.placeholder = this.options.placeholder || translate.get('cqcompositefield.comboplaceholder', { fieldName : field.name });
                templateInitializer.datatextfield = this.options.autocompleteTextField || 'Name';
                templateInitializer.datavaluefield = this.options.autocompleteValueField || 'Name';
                templateInitializer.sourcename = this.options.autocompleteSource || templateInitializer.apiname + '_Source';
                templateInitializer.otherattributes='';
                templateObj = referenceTemplateForPicklistWO;

            }
            else if (field.type == SFConstants.TYPE_NUMBER) {
                templateObj = numberTemplateWO;

                var maxLength = parseInt(field.validation.maxlength);
                var maxVal = Math.pow(10, maxLength - field.additionalInfo.scale) - 1;
                //var allowsNegatives = (!field.additionalInfo.onlyPositive) || true; //default true, allows negatives
                templateInitializer.maxval = maxVal;
                templateInitializer.minval = (field.additionalInfo.minVal!=undefined) ? field.additionalInfo.minVal: -maxVal//if minVal is defined, then define assign that min Value, else assign negative of maxVal
                templateInitializer.decimals = field.additionalInfo.scale;
                templateInitializer.format = field.additionalInfo.scale === 0 ? '#' : 'n';
                if(field.additionalInfo.minVal){
                    var minFieldName = (field && field.name) || 'Field';
                    var data_min_msg = minFieldName + ' should not be less than '+field.additionalInfo.minVal;
                    templateInitializer.otherattributes =' data-min-msg=\"' + data_min_msg + '\" ';
                }

            }
            else if (field.additionalInfo.multiline) {
                templateObj = multilineTextTemplateWO;

                if (field.additionalInfo.richtext) {
                    templateObj = richtextTemplateWO;
                }
            }

            if (field.additionalInfo.uitype == 'Rating'){
                templateObj = starTemplateWO;
            }


            var readwriteElement = $(templateObj(templateInitializer));


            //var mainWriteElement = $('<div></div>');
            //readwriteElement.appendTo(mainWriteElement);
            var mainWriteElement = readwriteElement;


            if (field.additionalInfo.richtext) {
                mainWriteElement.addClass('cq-richtext-edit-field');
            }


            var editableElement = container.find(".editableElement");
            mainWriteElement.appendTo(editableElement);
            this.options._writeElement = mainWriteElement;
            editableElement.addClass('cq-' + field.type);

            //add error span
            var errorContainer = "<div class='k-invalid-msg cq-invalid-msg' data-for='" + templateInitializer.apiname + "'></div>";


            $(errorContainer).appendTo(this.element);

            this.setRequired(field.validation.required);
        },

        /**
        * adds support for inline edit basically adds doubleclick, blur handlers
        */
        _createInlineEditor: function(field, data, container){

                //check if field is editable from schema
                var canEditFromSchema = this.options.schemaObjectField == null ||
                                        (this.options.schemaObjectField && this.options.schemaObjectField.editable ),
                    that = this,
                    canEditData,
                    undoElement,
                    originalVal,
                    originalValReference,
                    originalCascadeValue,
                    readOnlyElem;

                //inline edit only possible if we have data
                if(canEditFromSchema && data){

                    canEditData = data.canEdit && data.canEdit();

                    if(canEditData == false || this.options.schemaObjectField.additionalInfo.uitype == 'Rating' )
                        return;



                    undoElement = $(undoTemplate({}));
                    undoElement.appendTo(this.options._readElement);

                    //record the original value, which will be used to compare
                    originalVal = data[field.additionalInfo.apiname];
                    originalValReference = null;
                    originalCascadeValue = undefined;

                    if(that.options.schemaObjectField.type == sqx.common.SFConstants.TYPE_REFERENCE){
                        originalValReference = data[field.additionalInfo.apiname.replace('__c', '__r')];
                        if(that.options.cascadeFrom != null){

                            $("#" + that.options._writeElement.attr("data-cascade-from")).data("kendocqComboBox")
                                .bind("dataBound", function(){
                                    var cmb = this;
                                    if(originalCascadeValue === undefined){
                                        originalCascadeValue = cmb._selectedValue;
                                        // HACK: Using internal property because the value() function doesn't return the correct
                                        // value for the element until dataBound event completes.
                                    }
                            });
                        }
                    }

                    //bind to double click of readonly element
                    readOnlyElem = this.options._readElement.parents('.readonlyElement');
                    readOnlyElem.dblclick(function(event){
                        if (that.options.inlineEdit) {
                            // process when inline edit is available
                            //clear selection clears any selection
                            var fieldName = that.options.schemaObjectField.additionalInfo.apiname;
                            clearSelection();

                            //set the widget to edit mode and focus on input element
                            that.setEditMode(true);

                            /*
                            disabled because manually adding to the datasource of combobox is not required, kept here for future references only
                            if(that.options._writeElement){
                                var combo = that.options._writeElement.data('kendocqComboBox');

                                if(combo){
                                    var referenceValueField = fieldName.replace('__c', '__r');
                                    if(that.options._boundData[referenceValueField]){
                                        combo.dataSource.add(that.options._boundData[referenceValueField]);
                                        combo.value(that.options._boundData[referenceValueField].Id);
                                    }
                                }
                            } */

                            //special case for picklist/drop down list where we need to call focus of the kendo's widget
                            if(that.options.schemaObjectField.type == sqx.common.SFConstants.TYPE_PICKLIST){
                                that.options._writeElement.data('kendoDropDownList').focus();
                            }
                            else{
                                var kendoInput = that.options._writeElement.parents('.editableElement').find('.k-input');
                                if(kendoInput.length == 1)
                                    kendoInput.focus();
                                else
                                    that.options._writeElement.focus();
                            }
                        }
                    });

                    //add a class so as to display the pencil on hover
                    readOnlyElem.addClass('inlineEditable');


                    //bind to blur on write element, to revert back to read only once done
                    if(that.options._boundData){
                        that.options._boundData.bind('change', function(e){
                            if(e.field == that.options.field){
                                var changedVal = e.sender.get(e.field),
                                    hasChanged = false, ov;

                                if(that.options.schemaObjectField.type == sqx.common.SFConstants.TYPE_DATE){

                                    if(changedVal != null)
                                        changedVal = kendo.parseDate(changedVal, sqx.common.CQDateFormat);

                                    // date time comparisions can only be made by using get time else even
                                    // if two matching dates are compared JS will return false

                                    ov = (originalVal && originalVal.getTime && originalVal.getTime()) || null;
                                    changedVal = (changedVal && changedVal.getTime && changedVal.getTime()) || null;

                                    hasChanged = ov != changedVal;
                                }
                                else{
                                    hasChanged = changedVal != originalVal
                                }

                                undoElement.find('.text').text('');

                                //if original and new val are different
                                if(hasChanged){

                                    //add changed class to highlight the changed text and display undo button
                                    readOnlyElem.addClass('changed');
                                    if (that.options.inlineEdit) {
                                        // show undo button when inline edit is available
                                        readOnlyElem.find('.inlineundo').css('display', 'inline-block');
                                    }

                                    //that.trigger(CHANGE,that); no need, handled by data change event which raises body change event
                                    //add deleted text if the original value was not null and new one is.
                                    if(originalVal != null &&  (changedVal == null ||
                                                                (changedVal.length != undefined && changedVal.length == 0 )) ){
                                        undoElement.find('.text').text('Deleted');
                                    }
                                }
                                else{
                                    //remove changed class and hide undo if no changes are made.
                                    readOnlyElem.removeClass('changed');
                                    readOnlyElem.find('.inlineundo').hide();
                                }
                            }
                        });
                    }
                    this.options._writeElement.blur(function(event){

                        //if there is an error don't change the mode back.
                        if(window.formValidator && window.formValidator.validateInput && !window.formValidator.validateInput(that.options._writeElement)){
                            return;
                        }

                        //switch to readonly mode
                        that.setEditMode(false);

                    });


                    //bind to undo event to revert back to the original value
                    undoElement.find('.inlineundo').click(function(e){
                        e.preventDefault();
                        undoElement.find('.text').text('');
                        that.options._boundData.set(that.options.schemaObjectField.additionalInfo.apiname, originalVal);
                        if(that.options.schemaObjectField.type == sqx.common.SFConstants.TYPE_REFERENCE){

                            if(that.options.cascadeFrom != null){
                                var depedentFieldValue = $("#" + that.options._writeElement
                                                         .attr("data-cascade-from")).data("kendocqComboBox").value();

                                if(originalCascadeValue != depedentFieldValue){

                                    that.options._boundData.set(that.options.schemaObjectField.additionalInfo.apiname.replace('__c', '__r'), null);
                                    return;

                                }
                            }

                            that.options._boundData.set(that.options.schemaObjectField.additionalInfo.apiname.replace('__c', '__r'), originalValReference);

                            //this will ensure that if original value has been removed from combo-boxes data source it is refetched.
                            that.options._writeElement.data('kendocqComboBox').cq_ReInitFilter(originalVal);
                        }

                        that.options._readElement.find('.inlineundo').hide();
                        readOnlyElem.removeClass('changed');


                    });

                }
        },


        /**
        * @description set the mode of the widget to either read write or readonly
        * @param write <code>true</code> to set the widget to write mode
        *                  <code>false</code> to set the widget to read mode
        */
        setEditMode: function (edit) {
            //to switch to edit mode, widget cannot be readonly  and if  bound to data and data has to be editable
            var that = this,
                opts = that.options,
                soField = opts.schemaObjectField,
                canEditFromSchema = soField == null || (soField && (kendo.isFunction(soField.editable) ? soField.editable(opts.model) : soField.editable !== false)),
                canEdit = canEditFromSchema,
                canEditFromBoundData = (opts._boundData && opts._boundData.canEdit()) || !(opts._boundData);

            // if bound data rules are to be overriden then ignore data edit rules options can be used.
            // this will allow the user to conditionally edit some fields even when model is locked.
            // Example scenarios where this can be beneficial is:
            //      When a CAPA record is complete, all other fields should be locked, except for resolution field when showing up in close
            //      dialog. Previous implementations resorted to setting canEdit to true and then setting editable of all but some fields to
            //      return false.
            //
            //  This method provides a simpler solution where the edit rules need to be bypassed and provides more control.
            canEditFromBoundData = canEditFromBoundData || opts.ignoreDataEditRules || false;

            canEdit = canEditFromSchema && canEditFromBoundData;

            if (edit && (opts.readOnly !== true) && canEdit) {
                //if a compositefield is to be switched to edit mode and can be switched (not readonly and editable)
                if (opts._writeElement !== null) {

                    //show the write element so that data can be entered.
                    if(opts._writeElement.hasClass('editableElement'))
                        opts._writeElement.show();
                    else
                        opts._writeElement.parents('.editableElement').show();

                    //hide the read element when in edit mode.
                    opts._readElement.parent().hide();

                    //enable the write element so that proper validation can occur
                    //TODO: this is actually the desired behaviour whenever the composite field is in edit mode,
                    //the write element must be enabled, so that validation can kick in
                    //this has been disabled because inline edit's undo feature doesn't set the edit mode of dependent field
                    //even when the dependent field is required.
                    //this.options._writeElement.prop('disabled', false);

                    //remove the css classes
                    that.element.addClass('editmode');
                    that.element.removeClass('readmode');
                }

                //add the label's for attribute when label is in edit mode because clicking on label will set the focus on the control
                if (!!opts.render.Label) {
                    opts._labelElement.children().eq(0).attr('for', opts.Id);
                }
            }
            else {
                //switch the compositefield to read mode.
                opts._readElement.parent().show();

                if (opts._writeElement !== null){
                    //if write element is present hide it set the disabled property to true

                    //hide the write element
                    if(opts._writeElement.hasClass('editableElement'))
                        opts._writeElement.hide();
                    else
                        opts._writeElement.parents('.editableElement').hide();

                    //remove the css classes
                    that.element.removeClass('editmode');
                    that.element.addClass('readmode');

                    //setting disabled to true prevents validation from happening when not in edit mode and kendo validator or any other
                    //validator is being used.
                    //this.options._writeElement.prop('disabled', true);
                }

                //remove the for attribute from the label because we don't want clicking on label focusing on the write element
                if (!!opts.render.Label) {
                    opts._labelElement.children().eq(0).removeAttr('for');
                }
            }

            that.i_editstate = that.element.hasClass('editmode');
        },

        /**
        * @descripion sets the mode of the widget to readonly
        * @param readonly <code>true</code> to set the readonly field
        *                 <code>false</code> to remove the readonly field
        */
        setReadonly: function (readonly) {
            var that = this,
                opts = this.options,
                rdElmParent = opts._readElement.parent(),
                soField = opts.schemaObjectField,
                canEditFromSchema = soField == null || (soField && (kendo.isFunction(soField.editable) ? soField.editable(opts.model) : soField.editable !== false)),
                componentMode,
                uiMode;

            if (opts.readOnly !== true && canEditFromSchema) {
                // process when editable and not readonly field
                componentMode = that.i_editstate;

                if (readonly) {
                    if (componentMode) {
                        // show read element when in edit mode
                        that.setEditMode(false);
                    }
                    else {
                        // remove inline edit when not in edit mode
                        opts.inlineEdit = false;
                        rdElmParent.removeClass("inlineEditable");

                        if (rdElmParent.hasClass("changed")) {
                            // remove undo icon
                            rdElmParent.find(".inlineundo").hide();
                        }
                    }
                }
                else {
                    uiMode = opts.editMode || isUIInEditMode();

                    if (uiMode) {
                        // show write element when in edit mode
                        that.setEditMode(true);
                    }
                    else {
                        // set inline edit when not in edit mode
                        opts.inlineEdit = true;
                        rdElmParent.addClass("inlineEditable");

                        if (rdElmParent.hasClass("changed")) {
                            // set undo icon
                            rdElmParent.find('.inlineundo').css('display', 'inline-block');
                        }
                    }
                }
            }
        },

        /**
        * @descripion set/unset the required value for the widget
        * @param required <code>true</code> to set the required field indicator and attribute
        *                 <code>false</code> to remove the required field indicator and attribute
        */
        setRequired: function (required) {
            var element = this.options._writeElement,
                inputElement;

            /* Fix for SQX-751, when setRequired is fired by evaluate rules for elements which are readonly.
            This usually happens when the rules have conditional editable property i.e. functions that can't be evaluated immediately  */
            if(element == null)
                return;

            inputElement = element.hasClass('cq-wo') ? element : element.find('.cq-wo');

            if (this.options._writeElement !== null) {
                if (required) {
                    //add css classes for required
                    if(sqx.common.isLightningScope() && this.options._labelElement.find("abbr.slds-required").length === 0) {
                        $("<abbr class='slds-required cq-slds-required'>*</abbr>").prependTo(this.options._labelElement);
                    }
                    this.options._writeElement.parents('.editableElement').addClass('cq-field-required');
                    this.options._writeElement.parents('.editableElement').removeClass('cq-field-not-required');


                    //set the validation message to properly formatted message
                    var msg = (this.options.schemaObjectField.validation
                                && this.options.schemaObjectField.validation.required);


                    if(typeof(msg) != 'string'){
                        var name = (this._field && this._field.name) || 'Field';
                        msg = translate.get('common.requiredmsg', { fieldName : name });;
                    }

                    inputElement.attr('required', 'required');
                    inputElement.attr('data-required-msg', msg);
                    //this.options._writeElement.attr('required', 'required');
                }
                else {
                    //add the css class for not required
                    if(sqx.common.isLightningScope() && this.options._labelElement) {
                        this.options._labelElement.find('.slds-required').remove();
                    }
                    this.options._writeElement.parents('.editableElement').addClass('cq-field-not-required');
                    this.options._writeElement.parents('.editableElement').removeClass('cq-field-required');

                    //remove the validation messages
                    inputElement.removeAttr('required');
                    inputElement.removeAttr('data-required-msg');
                    //this.options._writeElement.find('textarea').removeAttr('required');
                }

            }
        },

        /** hides or unhides the cqcompositewidget
        * if no param is passed returns the current status
        * @param hide <code>true</code> to hide the widget's field <code>false</code> to unhide the widget <code>undefined</code> pass nothing to get the current status
        */
        hideField : function(hide){
            var that = this,
                compositewidget;
            compositewidget= that.element;
            if(hide)
            {
                compositewidget.addClass('hide');
            }
            else
            {
                compositewidget.removeClass('hide');
            }

        },

        /**
         * sets the given value or returns the value stored in the writeField
         */
        val : function(newValue) {
            if(newValue !== undefined) {
                this.options._writeElement.val(newValue);
                this.options._writeElement.trigger('change');
            }

            return this.options._writeElement.val();
        }


    });

    //this functions clears the text from the combobox if no matching item is found
    window._cqCompositeFieldComboChange = function(e){
        if (this.value() && this.selectedIndex == -1) {   //or use this.dataItem to see if it has an object
            if(this._hasUserEntered){
                this.text('');
                if(this.options.cqFilterFields && this.options.cqFilterFields.length > 0){
                    this.search(''); //reset the search filter too.
                }
            }
        }
    };

    // add the widget to the ui namespace so it's available
    ui.plugin(cqCompositeField);


})(jQuery, sqx.translation);

