/**
* CQESigWindow wraps the electronic signature to create a popup window widget that asks for e-signature
* When trying to submit the record.
*/
(function ($, kendo, cqHelper, globalEsigEnabled, globalRequireUserName, UIController) {
    "use strict";

    var ui = kendo.ui,
        Groupable = ui.Groupable,
        Widget = ui.Widget,
        keys = kendo.keys,
        isPlainObject = $.isPlainObject,
        extend = $.extend,
        map = $.map,
        grep = $.grep,
        isArray = $.isArray,
        inArray = $.inArray,
        proxy = $.proxy,
        isFunction = kendo.isFunction,
        isEmptyObject = $.isEmptyObject,
        eSigWindow = null,
        KWindow = ui.Window,
        //Templates
        ERROR_TEMPLATE = '<div><div class="errorStyle errorDiv"><span class="k-icon k-warning"></span><span class="msg"></span></div></div>',
        WINDOW_TEMPLATE = '<div><div class="cq-esig-tmpl"></div><div class="cq-esig-container section1col cqSectionContent"></div><div class="clearFix"></div><div class="commandSection" style="text-align:center;"></div></div>',
        CMD_TEMPLATE = '<button type="button" data-callback="#= callback #" data-validation-group="#= validationGroup #" data-next-action="#= nextAction #" class="cqCommandButton k-button #: cssClass #" data-role="button" role="button" aria-disabled="false" tabindex="#= index #">#= text #</button>',		
        PURPOSE_OF_SIG_TEMPLATE = '<div class="formItem"><div class="formLabel"><label for="meaningOfSignature">#: purposeOfSigLabel #</label></div><div class="formField" name="meaningOfSignature">#: purposeOfSig #</div></div>',
        COMMENT_TEMPLATE = '<div class="formItem editmode cq-comment"><div><div class="formLabel tall"><label for="#= CommentFieldId #">#: CommentFieldLabel #</label></div><div class="formField tall"><div class="editableElement #= required ? \'cq-field-required\' : \'\' # "><textarea rows="4" maxlength="255" class="cq-wo cq-multi-line-textarea" id="#= CommentFieldId #" name="Comment" #= required ? \'required="required"\' : \'\' # data-required-msg="#: CommentRequiredMsg #"></textarea></div></div></div></div>',
        COMMENT_BOUND_TEMPLATE = '<div data-container="#= commentFieldBoundTo #" data-role="cqcompositefield" data-edit-mode="true" class="cq-bounded-comment"></div>',
        ERR_MSG_TEMPLATE = '<span>Error: Invalid and Or Missing Data. <br /> Please Review all error indicators, correct and resubmit</span><ul>#for(var ei = 0; ei < errors.length; ei++){ # <li>#: errors[ei] #</li>  # } #</ul>',

        //Class Names
        eSigTmplSelector = '.cq-esig-tmpl',
        eSigContainerSelector = '.cq-esig-container',
        cmdSectionSelector = '.commandSection'
        SAVE_BTN = {name: 'save', text: 'Save', action: saveESigWindow},
        CANCEL_BTN = {name: 'cancel', text: 'Cancel', action: cancelESigWindow}
        DEFAULTBUTTONS = [
            SAVE_BTN,
            CANCEL_BTN
        ];



    /**
    * Default Save Event Handler for E-Sig Window. This submits a request to server with the e-sig,
    * next action and comment param
    */
    function saveESigWindow(e){
        var w = getEnclosingWindow(e),
            errorContext = w,
            validator = w.element.data('kendoValidator'),
            param = {}, nextAction,callback, validateMainWindow = w.options.validateMainWindow;




        // If the window is not visible then, that means that we are executing the default action directly
        // without showing the window in that case we are validating that the data outside window is consistent i.e. it is valid.
        //
        // This is because in CQ App we usually send the changeset for all the changes by default (And this is a default save action :D) 
        // So we must validate the main form too.
        if(!w.element.is(':visible') && window.formValidator){
            //TODO: Consider if validating the main window is really necessary
            //		Since, we expect the callback/event handler of the button that creates the esig window to have already performed the
            //		validation if necessary.
            //		So, the question is "Is this function redundant ?". If 'Yes', we need to remove this section
            validator = window.formValidator;
            errorContext = undefined;

            validateMainWindow = validateMainWindow === undefined ? true : validateMainWindow; //always validate main window by default
            
            if(validateMainWindow === false)
                validator = null; //setting this null will skip validation of main window when default action is executed immediately.

        }


        //don't validate if window is not visible. useful for direct action execution
        if(!validator || validator.validate()){
            cqHelper.debugLog('Processing change set');


            callback = $(e.currentTarget).attr('data-callback');
            if(callback.length > 0){
                callback = kendo.getter(callback)(window);
            }
            else{
                callback = undefined;
            }

            param = w._getDefaultParams(e.currentTarget);
            w.submitWindowWith(param, callback)
            
        }
        else{
            cqHelper.setFormError(w.options.formValidationErrorMessage, errorContext ? errorContext.element : errorContext);
            //cqHelper.setFormError(kendo.template(ERR_MSG_TEMPLATE)({errors: validator.errors()}), errorContext);
        }

    }

    /**
    * Cancel the electronic signature window's default handler, performs the closure
    */
    function cancelESigWindow(e){
        var w = getEnclosingWindow(e);
        w.close();

        //Note: Window should be destroyed once closed as a best practice, which is usually done in the close event.
    }

    /**
    * helper function to get the enclosing window of the button or any element contained within the esig window. 
    */
    function getEnclosingWindow(e){
        return $(e.currentTarget).parents('[data-role="cqesigwindow"]').data('kendoCQESigWindow');
    }

    /**
    * Adds the command section and creates related buttons in the UI for the window
    */
    function constructCmdSection(cmdSection){

        $(ERROR_TEMPLATE).appendTo(cmdSection);

        //create the buttons
        var cmdButtonToDisplay = this.options.cmdButtons || DEFAULTBUTTONS, callbackMethod, btnTemplate,
            index, currBtnData, currBtn, eventHandler;
        if(cmdButtonToDisplay){
            cqHelper.assert(isArray(cmdButtonToDisplay) || 'Command buttons options is not valid');
            
            btnTemplate = kendo.template(CMD_TEMPLATE);
            
            for(index = 0, length = cmdButtonToDisplay.length; index < length; index++){
                currBtnData = cmdButtonToDisplay[index];

                if(!isPlainObject(currBtnData)){
                    //standard commands string passed
                    switch(currBtnData){
                        case SAVE_BTN.name: 	currBtnData = SAVE_BTN; break;
                        case CANCEL_BTN.name: 	currBtnData = CANCEL_BTN; break;
                    }

                    if(sqx && sqx.translation){
                        currBtnData.text = sqx.translation.get('esig.' + currBtnData.name);
                    }
                }

                callbackMethod = '';
                if(currBtnData.callback)
                    callbackMethod = (this.options.fnNs || 'window') + '.' + currBtnData.callback;


                currBtn = $(btnTemplate({text : currBtnData.text || currBtnData.name, index : index, nextAction : currBtnData.nextAction || '', cssClass : currBtnData.cssClass || '', callback : callbackMethod, validationGroup : currBtnData.validationGroup || '' }));
                currBtn.appendTo(cmdSection);

                eventHandler = null;

                if(currBtnData.action){
                    if(isFunction(currBtnData.action)){
                        eventHandler = currBtnData.action;
                    }
                    else{
                        eventHandler = kendo.getter((this.options.fnNs || 'window') + '.' + currBtnData.action)(window);
                    }
                }
                else{
                    switch(currBtnData.name){
                        case SAVE_BTN.name: 	eventHandler = SAVE_BTN.action; break;
                        case CANCEL_BTN.name: 	eventHandler = CANCEL_BTN.action; break;
                        default: 				break;
                    }
                }

                if(currBtnData.defaultBtn === true){
                    this.defaultAction = currBtn;
                }

                if(eventHandler){
                    currBtn.click(eventHandler);
                }
            }
        }
    }

    /**
    * Adds the purpose of signature, comment section to the esig window
    */
    function constructESigSection(container){
        var commentField,
            defaultComment = this.options.defaultComment || '',
            esigItem;

        cqHelper.assert(kendo.ui.CQElectronicSignature != null || 'Error CQ Electronic Signature Widget is not initialized');

        esigItem = $("<div />");
        esigItem.kendoCQElectronicSignature({
            allowESig: this.options.requireEsig,
            requireUserName: this.options.requireUserName,
            currentUserDetailsVar: this.options.currentUserDetailsVar
        });

        //add purpose of esig if it is defined
        if(this.options.purposeOfSig !== null){
            this.options.purposeOfSigLabel = this.options.purposeOfSigLabel = sqx.translation.get('esig.purposeofsig');

            $(kendo.template(PURPOSE_OF_SIG_TEMPLATE)(this.options)).appendTo(container);
        }

        if(this.options.needsCommentField === true){
            if(this.options.commentFieldBoundTo){
                commentField = $(kendo.template(COMMENT_BOUND_TEMPLATE)(this.options));
                commentField.appendTo(container);
                kendo.init(commentField);

                if(this.options.requireComment)
                    commentField.data('kendocqCompositeField').setRequired(true);//comment is always required
                
                var currentComment = kendo.getter(this.options.commentFieldBoundTo)(window);
                commentField.data().kendocqCompositeField.val(currentComment || defaultComment);
            }
            else{
                var commentLabel, commentRequiredMsg;

                commentLabel = sqx.translation.get('esig.comment');
                commentRequiredMsg = sqx.translation.get('common.requiredmsg', {fieldName : commentLabel}); 
                commentField = $(kendo.template(COMMENT_TEMPLATE)({CommentRequiredMsg : commentRequiredMsg,CommentFieldId : 'cq-' + kendo.guid(), required : this.options.requireComment, CommentFieldLabel: commentLabel }));
                commentField.appendTo(container);
                commentField.find('[name="Comment"]').val(defaultComment);
            }
        }

        esigItem.appendTo(container);
    }

    /**
    * This is the actual method that creates the template for the window
    */
    function constructESigWindow(){
        var mainTemplate,
            esigTemplate,
            userContent,
            cmdSection,
            index, length, cmd, btnTemplate, currBtnData, currBtn, eventHandler,
            requiresEsig = this._requiresEsig,
            esigContainer, esigItem, commentField, validationOptions;

        cqHelper.debugLog('Constructing e-signature window [START]');
        mainTemplate = $("<div></div>");
        esigTemplate = $(WINDOW_TEMPLATE)

        if(!this.options.embedEsigIn){
            //add the basic window template if it isn't to be embedded inside user provided template
            esigTemplate.appendTo(mainTemplate);
        }

        //if standard content is undefined for the window 
        if(!this.options.content){

            if(this.options.templateId){
                userContent = $($("#" + this.options.templateId).html());

                if(this.options.autoBind){
                
                    if(this.options.data){
                        kendo.bind(userContent, this.options.data);	
                    }
                    else{
                        kendo.init(userContent[0]);
                    }
                }
            }
            else if(this.options.templateElement){
                userContent = this.options.templateElement;
            }

            if(this.options.embedEsigIn){
                userContent.appendTo(mainTemplate);

                //embed window template inside template
                esigTemplate.appendTo(userContent.find(this.options.embedEsigIn));
                esigTemplate.find(eSigTmplSelector).remove(); //remove the template container
            }
            else if(userContent){
                userContent.appendTo(esigTemplate.find(eSigTmplSelector));
            }

            
        }
        
        constructESigSection.call(this, esigTemplate.find(eSigContainerSelector));

        constructCmdSection.call(this, esigTemplate.find(cmdSectionSelector));
        
        validationOptions = {};

        //add custom validation rules to the window if required.
        if(this.options.validatorConfig){
            if(!isPlainObject(this.options.validatorConfig)){
                validationOptions = kendo.getter((this.options.fnNs || 'window') + '.' +this.options.validatorConfig)(window);
            }
            else{
                validationOptions = this.options.validatorConfig;
            }
        }

        $(this.element).kendoValidator(validationOptions);


        mainTemplate.appendTo(this.element);


        cqHelper.assert(esigTemplate.parent().length != 0 || 'Looks like esig template doesn\'t have a parent. If you are using embedEsigIn, please provide a valid selector');

        cqHelper.debugLog('Constructing e-signature window [END]');
    }

    /**
     * Parses the exception message and adds it to the UI in a formatted manner.
     * @param {*} message the message containing the error.
     */
    function parseCQUpsertException(message) {
        var parsedExceptions,
            msg = message;

        try {
            parsedExceptions = JSON.parse(message);
            if(parsedExceptions.UpsertExceptions) {
                msg = '';
                parsedExceptions.UpsertExceptions.forEach(function(exception) {
                    if(!exception.success) {
                        exception.errors.forEach(function(error) {
                            msg += '<p class="cq-error-item">' + error.message + "</p>";
                        });
                    }
                });
            }
        } catch(ex) {
            //ignore exception since default will fallback to message
            msg = message;
        }

        return msg;
    }

    eSigWindow = KWindow.extend({
        COMMENT_PARAM : 'comment',
        PURPOSE_OF_SIG_PARAM : 'purposeOfSignature',
        NEXT_ACTION_PARAM : 'nextAction',
        
        options : {
            name : "CQESigWindow",
            templateId: null, //the id of the template script to use to add to the template
            templateElement: null, //the dom element which is to be used while inserting the record
            data: null, //the data that is to be bound to the window
            cmdButtons: null, //the array of command buttons that are to be added to the window
            requireEsig: globalEsigEnabled, //function or true value that is used to decide whether esig component should be added or not.
            requireUserName: globalRequireUserName, //if set to true, ask for input to username 
            currentUserDetailsVar: null, //the variable that is storing the current user details, this field is not used if current user,
            validateOnSave: true, //validates the record when saving the record
            autoBind: true, //indicates whether or not the window should be automatically bound to the data
            modal: true, //indicates whether the window is modal or not
            occupyFullWidth: true, //indicates whether the window should take almost full screen i.e. width - 2*PAdding 
            purposeOfSig: null, //the purpose of signature to be displayed
            needsCommentField: false, //use full for passing the comment when signing of records
            commentFieldBoundTo: null, //the field in Data to which comment must be bound to.
            embedEsigIn: null, //the container where the esig template is to be placed when not using the standard form
            fnNs: null, //the namespace that contains the functions and validatorConfig for the window. The functions and validator config will be evaluated on that basis
            validatorConfig : null, //the validator config which is the configuration that will be passed to the validator for the window, use this to pass additinal rules and messages for the ui.
            requireComment : false, //enforces validation rule on comment field
            formValidationErrorMessage : 'Please fix the errors', //default form validation error messasge
            validateMainWindow: true,//If set, it is used to validate the main window by default action when it is executed directly.
            defaultComment: null //If not null default comment is copied to comment without showing any popup during save
        },
        /**
        * The entry point when constructing the window.
        */
        init: function(element, options){

            if(this.options.occupyFullWidth){
                var PADDING = (window.popupWidth && popupWidth()) || 10,
                    windowWidth = $(document).width() - PADDING * 2;

                this.options.width = windowWidth + 'px';
            }

            // base call to window initialization
            KWindow.fn.init.call(this, element, options);

            this.options.formValidationErrorMessage = sqx.translation.get('common.fixerrormsg');
            this._requiresEsig = isFunction(this.options.requireEsig) ? this.options.requireEsig.call() : this.options.requireEsig;

            cqHelper.assert(this.options.currentUserDetailsVar != null || 'Expected current user details var to be set but found it empty');

            constructESigWindow.call(this);

            this.center();

        },
        /**
        * returns the esig data in a properly formatted manner ready to be submitted to the server.
        */
        getEsigData : function(){
            var electronicSignatureData= null,
                parentElement,
                passwordElement,
                usernameElement;

            if(this._requiresEsig)
            {

                parentElement = $(this.element).find("[data-role='cqelectronicsignature']");
                if(parentElement.length > 0) {
                    usernameElement = parentElement.find('input[name="Username"]');
                    passwordElement = parentElement.find('input[name="Password"]');

                    var passwordtext = passwordElement.val();
                    var usernametext = '';
                    //if Ask Username is enabled, get username form textbox
                    if(this.options.requireUserName){
                        usernametext = $.trim(usernameElement.val())//get from username textbox
                    }
                    else{
                        usernametext = kendo.getter(this.options.currentUserDetailsVar)(window);
                        usernametext = usernametext.substr(0, usernametext.indexOf('/')); //Using HACK for now, need to refactor server code when username is not required.
                    }
                    electronicSignatureData={password: passwordtext, userName : usernametext };
                }
            }

            return electronicSignatureData;
        },
        /**
        * returns the value of comment present in the esig window.
        */
        getComment : function(){
            var comment = null;

            if(this.options.needsCommentField){
                if(this.options.commentFieldBoundTo){
                    comment = kendo.getter(this.options.commentFieldBoundTo)(window);
                }
                else{
                    comment = this.element.find('[name="Comment"]').val();
                }
                if(!comment && this.options.defaultComment){
                    comment = this.options.defaultComment;
                }
            }
            return comment;
        },
        /**
        * function to get default params of esig
        */
        _getDefaultParams : function(eventTarget){
            var param = {}, nextAction;

            if(this.options.needsCommentField){
                param[this.COMMENT_PARAM] = this.getComment();
            }

            if(this.options.purposeOfSig){
                param[this.PURPOSE_OF_SIG_PARAM] = this.options.purposeOfSig;
            }

            nextAction = $(eventTarget).attr('data-next-action');
            if(nextAction){
                param[this.NEXT_ACTION_PARAM] = nextAction;
            }

            return param;
        },
        /**
        * function to submit window with additional param
        */
        submitWindowWithAdditionalParam: function(evt, additionalParam, successCallback){
            var param = this._getDefaultParams(evt.currentTarget);
            $.extend(param, additionalParam);

            this.submitWindowWith(param, successCallback);
        },
        /**
        * Submits the SF window with provided param.
        */
        submitWindowWith : function(param, successCallback){
            var that = this, esigData = this.getEsigData(),
                deleted, changeSet, deletedObjs = [];
            cqHelper.showLoading();

            param = param  || {};

            //if additional param is specified add it to the params object to be submitted.
            if(this.additionalParam){
                $.extend(param, this.additionalParam);
            }



            //esigData = esigData != null ? JSON.stringify(esigData) : null; 

            changeSet = getSynchronizedChangeSet();
            deleted = [];
            deletedObjs = window.sqx.dataStore.getChangeSet().deleted;
            for(var i = 0; i < deletedObjs.length; i++){
                if (cqHelper.isSFID(deletedObjs[i].Id)) {
                    deleted.push({ Id: deletedObjs[i].Id });
                }
            }

            var params = [
                mainRecord.Id,
                changeSet,
                deleted,
                param,
                esigData
            ];

            if(sqx.common.isLightningScope()) {
                params.push(mainRecord.attributes.type);
            }

            params.push( function processChangeSetControllerCallBack(e, s) {
                var errMsg;
                if (s.status) {
                    commandRules.set('isDirty',false);
                    if(successCallback) {
                        successCallback.call(null, e, s);
                    }
                    else{
                        navigateToSObject(e);
                    }

                    return;
                } else {
                    errMsg = parseCQUpsertException(s.message) || s.message;
                    cqHelper.setFormError(errMsg, $(that.element).is(':visible') ? that.element : undefined);
                }
                cqHelper.debugLog(s);
                cqHelper.hideLoading();
            });


            //submit the data to salesforce using the UIController callback
            UIController.processChangeSetWithAction.apply(UIController, params);
        },

        /**
        * executes the default action i.e. command button if any for the window
        */
        executeDefaultAction: function(){
            if(this.defaultAction){
                this.defaultAction.click();
            }
        },

        /**
        * used to submit additional param that must be sent along when using default submission.
        */
        setAdditionalParam: function(additionalParam){
            this.additionalParam = additionalParam;
        },


        /**
        * validates if the window's field are filled correctly
        */
        validate : function(){
            var validator = $(this.element).data('kendoValidator'),
                result;

            result = validator.validate();
            if(!result){
                cqHelper.setFormError(this.options.formValidationErrorMessage,  $(this.element).is(':visible') ? this.element : undefined);
            }

            return result;
        },

        open : function(){
            //save isDirty flag on window open
            this._isDirty = window.commandRules && window.commandRules.isDirty;
            KWindow.fn.open.apply(this,arguments);
            this.bind('close', function(){
                if(this._isDirty !== undefined){
                    if(window.commandRules){
                        //reset isDirty flag
                        window.commandRules.set('isDirty', this._isDirty);
                    }
                    
                }
            });
        }

    });

    //register the plugin with kendo for use in the UI.
    ui.plugin(eSigWindow);

})(jQuery, kendo, window.sqx.common, window.esigEnabled || true, window.requireUserName || true, window.UIController || {});
