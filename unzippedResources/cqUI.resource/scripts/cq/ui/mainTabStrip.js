/**
 * @description: checks if device is mobile or printMode is active
                 if device is mobile page will have panelbar
                    if printMode is active all panels and tabs will be activated
                else pages will have tabstrip
*/
sqx.mainTabStrip = function(){  
    var mainTS;
    var headerModel = sqx.dataStore.objectCount;
    headerModel.set('mainRecord', mainRecord);
    var events = sqx.Events;
    events.trigger(events.INITIATINGTABSTRIP);
    if (sqx.common.isMobile() || cq.printMode) {
        $('#mainTab').kendoCQPanel({
            expandAllPanels: cq.printMode,
            panelHeaderModel: headerModel
        });

        mainTS = $('#mainTab').data("kendoCQPanel");

        //activate all panels and grids in printMode
        if(cq.printMode){
            sqx.print.expandAllGrids();
            sqx.print.transformInnerTabToPanel();
            sqx.print.hideActionColumns();

            if(!window.sqx._printPreviewBound){   
                // if the function hasn't been bound, bind it for one time                     
                window.sqx._printPreviewBound = true;
                $(window).load(function(){
                    // because background-image is not displayed during print when background graphics is disabled
                    sqx.print.transformCheckboxes();
                });
            }
        }

        //set the initialTab as passed in the query string or using someother method
        else{
            if (initialTab !== "") {
                var tabToSelect = mainTS.element.find($('#' + initialTab));
                if (tabToSelect.length == 1) {
                mainTS.expand(tabToSelect);
                }
            }
        }
    } 
    else {
        $('#mainTab').kendoCQTabStrip({
            showGridTitle: true,
            showGridCount: true,
            tabHeaderModel: headerModel
        });

        mainTS = $('#mainTab').data("kendoCQTabStrip");

        kendo.init($(mainTS.contentElement(0)));

        //set the initialTab as passed in the query string or using someother method
        if (initialTab !== "") {
            var tabToSelect = mainTS.tabGroup.find($('#' + initialTab)).eq(0);
            if (tabToSelect.length == 1) {
                mainTS.activateTab(tabToSelect);

                //expand the provided row in tab.
                if (expandRecord !== "") {

                    //locate the main tab
                    var contentIndex = tabToSelect.index(),
                        contentElem = null,
                        tsGrids = null,
                        tblIndex, tblLength,
                        matchingRecord = null,
                        tableFor;

                    if (contentIndex != -1) {
                        //if content index is found
                        contentElem = $(mainTS.contentElement(contentIndex));

                        //find all grids in tab strip
                        tsGrids = contentElem.find('[data-role="cqgrid"]');

                        for (tblIndex = 0, tblLength = tsGrids.length; tblIndex < tblLength; tblIndex++) {
                            tableFor = tsGrids.eq(tblIndex).data('kendoCQGrid').dataSource.options.schema.model.prototype.sObjectName;
                            if (tableFor === expandRecordType) {
                                tsGrids.eq(tblIndex).data('kendoCQGrid').expandRecord(expandRecord, true);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    window.mainTS = mainTS; //assign mainTS outside the scope so that it can be used in scripts controlling next previous button.
    
    kendo.bind($("#printPage"), mainRecord); //conditional visibility of print link, visible only once record is saved
    events.trigger(events.COMPLETINGTABSTRIP);
};