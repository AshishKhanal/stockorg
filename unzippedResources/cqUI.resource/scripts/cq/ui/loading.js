/*
 *  This method shows Loading block
 */
var _loadingInit = false;
function showLoading() {
    var loadingBlock = $("#loadingBlock");
    if(!_loadingInit){
        kendo.init(loadingBlock);
        _loadingInit = true;
    }

    loadingBlock.show();
}

/*
 *  This method hides Loading block
 */
function hideLoading() {
    $("#loadingBlock").hide();
}