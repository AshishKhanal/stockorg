/*
 * @description: get formatted date in format dd-MMM-yyyy eg. 01-Jan-2014
*/
function getFormattedDate(date){
        if(date)
            return kendo.toString(kendo.parseDate(date), sqx.common.CQDateFormat);
        else return '';
       }

/**
 * @description: get formatted date-time
**/
function convertToLocalDateTime(datetime) {
    if(datetime){
        if(typeof(datetime) === "string" && datetime.indexOf("+0000") !== -1) {
            datetime = datetime.replace("+0000", "Z");
        }

        var parsedDate= kendo.parseDate(datetime);
        return kendo.toString(parsedDate, sqx.common.CQDateTimeFormat);
    }
    else return '';

}