/**
* NOTE : We have assumed that the child record will always be linked with the main record
*           If it is linked with mainrecord then it will set the main record field
*           Else if we don't have the main record field in the child object it will not be set
*/


/* The following 'cq' class has been introduced in 7.3.0 to support customization to the values provided by the sheet */
var cq = (function (cq) {

    var overridable = { recordValidator: {} };

    overridable.preProcess = function (sheetJSON) {
        return sheetJSON;
    }

    overridable.postProcess = function (objectType, record, existingRecords) {
        return record;
    }

    overridable.recordValidator.isValid = function (objectType, record) {
        return true;
    }

    cq.overridable = overridable;

    return cq;

}(window.cq || {}));


var sqx = (function (sqx, $) {
    "use strict";


    var CHILD_TABLE_ORIENTATION = "VERTICAL",   // row-wise
        GRAND_CHILD_TABLE_ORIENTATION = "HORIZONTAL",   // column-wise
        VALIDATION_ENABLED = true,
        NAMEDRANGE_UPDATE_REQUIRED = false,
        USE_ID = true,
        NAMESPACE_PREFIX = "",
        BATCHUPDATE = "BATCH_UPDATE",
        VALUESUPDATE = "VALUES_UPDATE",
        DEFAULT_THICK_ROW_SIZE = 1;


    /**
    * Class to perform checkin/checkout of data(to and from google sheet)
    * @param controller the controller providing various methods to fetch/update google spreadsheet
    * @param mainRecord the main record containing data
    */
    var gsheet = function (controller, mainRecord) {
        this.controller = controller;
        this.mainRecord = mainRecord;
        this.aliases = {};
        this.updateFields = {};
        this.protectedFields = [];
        this._currentSheet = null;
        this._currentSheetValues = null;
        this._orderOfImportedValues = [];
        this._template = null;

        this._parentChildMap = {};      //  sorted mapping of child and dynamically growing grand-child ; sorted according to header-name
        this._sheetTablesHeaderRange = {};  // header name per sheet mapped with namedrange details
        this._orderOfGrandChildParent = {};

        this._headerCopyRequests = [];
        this._rowCopyRequests = [];
        this._cutPasteRequests = [];
        this._insertDimensionRequests = [];
        this._validationRequests = [];
        this._namedRangeUpdateRequests = [];
        this._hideRequests = [];
        this._childObjectBatchUpdateRequest = [];
        this._headerValuesBatchUpdateRequest = [];
        this.consolidatedList = [];
        this.mainRecordFields = {};

        this._tableDataMap = {};    // table,per sheet, mapped with data range set in the sheet(specified by 'tr_')

        this._validationFields = [];    // list of all fields which require validation to be set in the sheet
        this.validationValuesMap = {};  // fields(in whitelist) mapped with valid values list
        this._lookup_AllValuesMap = {}; // lookup fields(not necessarily in whitelist) mapped with valid values list; useful while checking in


        this.tablesMap = {};    // this object will hold table names (child and grand-child records) as key name mapped with their list of columns
        this.tableRowLengthMap = {};  // this object will hold master tables(per sheet) mapped with their thick-row range
        this.rangeMap = {};   // this object will hold table names (child records) as key name mapped with the range over which the data has to be appended inside the table
        this.tableMappedWithNamedRangeDetail = {}; // this object will hold a map of tablename and its corresponding namedrange name and id  
    }

    /**
    * sets the aliases for the spreadsheet
    * @param aliases the alias data for the spreadsheet
    */
    gsheet.prototype.setAliases = function (aliases) {
        this.aliases = aliases;
    }

    /**
    * sets the aliases for the spreadsheet
    * @param fields that are whitelisted or fields to be updated
    */
    gsheet.prototype.setUpdateFields = function (updateFields) {
        this.updateFields = updateFields;
    }

    /**
    * sets the protected fields for the spreadsheet to prevent from updating in sf
    * @param protectedFields that are protected to prevent form editing
    */
    gsheet.prototype.setProtectedFields = function (protectedFields) {
        this.protectedFields = protectedFields;
    }

    /**
    * This method checks out the main record data into a google spreadsheet
    * @return returns a jQuery deferred which completes on checkout
    */
    gsheet.prototype.checkout = function (param, electronicSignatureData) {
        var promise = this._getNamedRanges();
        var that = this;

        return promise.then(function (data) {

            return new Promise(function (resolve, reject) {

                return that._setValidationMap().then(function (data) {

                    var requestList = [];
                    /* Note:
                        The order in which the requests are pushed to requestList is not to be tampered with!
                        ORDERING MATTERS!!!
                    */
                    var values = that._createMainRecordRequests();
                    if (values.length > 0) {
                        var update =
                            {
                                "data": values,
                                "valueInputOption": "USER_ENTERED"
                            };
                        var requestMap = {};
                        requestMap[VALUESUPDATE] = JSON.stringify(update);
                        requestList.push(requestMap);
                    }

                    var validationRequest = that._validationRequests;
                    if (validationRequest.length > 0) {
                        var request =
                            {
                                "requests": validationRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    that._validationRequests = [];

                    if (USE_ID === true) {
                        NAMEDRANGE_UPDATE_REQUIRED = true;
                        that._addIdColumnToTables();
                    }

                    that._setDynamicHeader();
                    that._getRowData();

                    values = that._consolidateAllRequestLists();

                    var insertDimensionRequest = that._insertDimensionRequests;
                    if (insertDimensionRequest.length > 0) {
                        var request =
                            {
                                "requests": insertDimensionRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    var headerCopyRequest = that._headerCopyRequests;
                    if (headerCopyRequest.length > 0) {
                        var request =
                            {
                                "requests": headerCopyRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    var cutPasteRequest = that._cutPasteRequests;
                    if (cutPasteRequest.length > 0) {
                        var request =
                            {
                                "requests": cutPasteRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    var copyRequest = that._rowCopyRequests;
                    if (copyRequest.length > 0) {
                        var request =
                            {
                                "requests": copyRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    var hideRequest = that._hideRequests;
                    if (hideRequest.length > 0) {
                        var request =
                            {
                                "requests": hideRequest
                            }
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    if (values.length > 0) {
                        var update =
                            {
                                "data": values,
                                "valueInputOption": "USER_ENTERED"
                            };
                        var requestMap = {};
                        requestMap[VALUESUPDATE] = JSON.stringify(update);
                        requestList.push(requestMap);
                    }

                    validationRequest = that._validationRequests;
                    if (validationRequest.length > 0) {
                        var request =
                            {
                                "requests": validationRequest
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }

                    if (NAMEDRANGE_UPDATE_REQUIRED) {
                        var namedRangeUpdateBody = that._createUpdateNamedRangeRequest();
                        var request =
                            {
                                "requests": namedRangeUpdateBody
                            };
                        var requestMap = {};
                        requestMap[BATCHUPDATE] = JSON.stringify(request);
                        requestList.push(requestMap);
                    }
                    that.controller.checkOut(
                        that.mainRecord.Id, getSynchronizedChangeSet(), [], param, electronicSignatureData, requestList,
                        function (data, event) {
                            if (event.status) {
                                that.controller.addFileToSF(
                                    that.mainRecord.Id,
                                    function (result, event) {
                                        if (event.status) {
                                            resolve(result);
                                        }
                                        else
                                            reject(event);
                                    });
                            }
                            else
                                reject(event);
                        });
                });
            });
        });
    }

    /**
    * This method checks in the data from google spreadsheet.
    * @return returns the jQuery deferred which completes on checkin
    */
    gsheet.prototype.checkin = function (attachmentId) {
        var that = this;

        return new Promise(function (resolve, reject) {
            that._getSheetNamedRanges(attachmentId)
                .then(function (sheetId) {
                    that._setValidationMap()
                        .then(function (data) {
                            that._modifyRangeMap();
                            resolve(that._getSheetValues(sheetId));
                        })
                })
        });
    }

    /**
     * Method contains common code to add ranges to retrieve to the given array
     * @param {Array} arrayOfNamedRangesForSheetValues array to which range is to be added
     * @param {Object} fieldsToFetch contains information regarding range to fetch from which sheet
     */
    gsheet.prototype._addRangesToRetrieve = function (arrayOfNamedRangesForSheetValues, fieldsToFetch) {
        var that = this,
            keys = [],
            sheetObject,
            sheetName,
            range;

        // first order the table/field names by name
        for (var sheetObject in fieldsToFetch) {
            keys.push(sheetObject);
        }
        keys.sort();

        for (var i = 0; i < keys.length; i++) {
            sheetObject = keys[i];
            sheetName = fieldsToFetch[sheetObject];
            range = that.rangeMap[sheetName][sheetObject]["standardRange"];

            arrayOfNamedRangesForSheetValues.push(range);
            that._orderOfImportedValues.push({ 'sheetName': sheetName, 'objectName': sheetObject });
        }
    }

    /*
    * @description: retrives the values from the sheets
    */
    gsheet.prototype._getSheetValues = function (sheetId) {
        var that = this;
        return new Promise(function (resolve, reject) {
            if (that.controller.getSheetValues) {

                var mainRecordFieldsToFetch = {},
                    childTablesToFetch = {},
                    subTablesToFetch = {};

                for (var sheetName in that.rangeMap) {
                    for (var sheetObject in that.rangeMap[sheetName]) {

                        var objectDetails = that.rangeMap[sheetName][sheetObject];

                        // only fetch data from tables that aren't protected
                        // and have white-listed fields
                        if (!objectDetails["protected"] && that.updateFields[sheetObject]) {
                            if (sheetObject.endsWith("s__r")) {
                                var tableDetails = that.tablesMap[sheetName][sheetObject];

                                if (tableDetails.ORIENTATION === "VERTICAL") {
                                    childTablesToFetch[sheetObject] = sheetName;
                                }
                                else {
                                    subTablesToFetch[sheetObject] = sheetName;
                                }
                            }
                            else {
                                // main record field
                                mainRecordFieldsToFetch[sheetObject] = sheetName;
                            }
                        }
                    }
                }

                /*
                    We now have a list of alphabetically sorted(sorted by API name of fields and tables) lists of 
                    - Main Record Fields
                    - Child Tables
                    - Sub Tables
                */

                // constructing "ranges to retrieve" in order(based on above)
                var arrayOfNamedRangesForSheetValues = [];

                that._addRangesToRetrieve(arrayOfNamedRangesForSheetValues, mainRecordFieldsToFetch);
                that._addRangesToRetrieve(arrayOfNamedRangesForSheetValues, childTablesToFetch);
                that._addRangesToRetrieve(arrayOfNamedRangesForSheetValues, subTablesToFetch);

                that.controller.getSheetValues(sheetId, arrayOfNamedRangesForSheetValues, function (result, event) {
                    if (event.status) {
                        try {
                            var responseData = JSON.parse(result);
                            if (responseData.Code) {
                                reject(responseData);
                            } else {
                                that._currentSheetValues = responseData;
                                that._valueRanges = that._currentSheetValues.valueRanges;
                                resolve(that._processSheetValues());
                            }
                        } catch (ex) {
                            reject(ex);
                        }
                    } else {
                        reject('Error occurred while retrieving sheet values');
                    }
                }, { escape: false });
            }
        });
    }

    /*
    * @description: process the value retrived from the sheets to save in sf
    */
    gsheet.prototype._processSheetValues = function () {
        var that = this,
            itemsChanged = [],
            kendoGrids = $('[data-role="cqgrid"]');

        for (var i = 0; i < that._valueRanges.length; i++) {
            if (that._orderOfImportedValues[i]) {
                var objectsToBeUpdated = that._orderOfImportedValues[i]['objectName'],
                    sheetToBeUpdated = that._orderOfImportedValues[i]['sheetName'],
                    childValueRange = that._valueRanges[i].values; // values of the fields from spreadsheet;
                if (childValueRange) {
                    if (objectsToBeUpdated.endsWith('__r')) {
                        var childRecord = that.mainRecord[objectsToBeUpdated],                      // child records in the main object(>=1)
                            childRecordProperties = that.tablesMap[sheetToBeUpdated][objectsToBeUpdated],             // field name of the child record
                            childFieldsToBeUpdated = that.updateFields[objectsToBeUpdated].fields,  // list of fields to be updated
                            mainRecodObjectName = null,
                            changedGrid;

                        var childRecordsOfMainRecord = that.mainRecord.childRelationships;
                        for (var count = 0; count < childRecordsOfMainRecord.length; count++) {
                            if (childRecordsOfMainRecord[count].relationshipName == objectsToBeUpdated) {
                                mainRecodObjectName = childRecordsOfMainRecord[count].field;
                                break;
                            }
                        }

                        for (var x = 0; x < kendoGrids.length; x++) {
                            var gridDataSourceName = kendoGrids[x].getAttribute('data-source');
                            if (gridDataSourceName.indexOf(objectsToBeUpdated) > -1) {
                                changedGrid = $('#' + kendoGrids[x].parentNode['id']).find('[data-role="cqgrid"]').data('kendoCQGrid');
                                break;
                            }
                        }
                        if (childRecordProperties.ORIENTATION === 'VERTICAL') {
                            itemsChanged = that._processVerticalSheetValues(sheetToBeUpdated, objectsToBeUpdated, childValueRange, mainRecodObjectName, childRecord, childRecordProperties, childFieldsToBeUpdated);
                        }
                        else {
                            itemsChanged = that._processHorizontalSheetValues(sheetToBeUpdated, objectsToBeUpdated, childValueRange, mainRecodObjectName, childRecord, childRecordProperties, childFieldsToBeUpdated);
                        }

                        if (itemsChanged.length > 0) {
                            for (var y = itemsChanged.length - 1; y >= 0; y--) {
                                if (itemsChanged[y].chk !== undefined)
                                    changedGrid.setDirty(itemsChanged[y].chk, itemsChanged[y].fieldChanged);
                            }

                        }
                    }
                    else {
                        that._setValue(that.mainRecord, objectsToBeUpdated, childValueRange[0][0]);
                    }
                }
            }
        }
    };

    /**
    * @description : processes the sheet values which are aligned vertically
    * @params: objectToBeUpdated : object name which value is to be updated
    *           childValueRange: array of values extracted from sheets
    *           mainRecodObjectName: mainR record of the page
    *           childRecord: record which is to be updated
    *           childRecordProperties: properties of child record in the sheet
    *           childFieldToBeUpdated: fields of the object that are to be updated
    */
    gsheet.prototype._processVerticalSheetValues = function (sheetToBeUpdated, objectsToBeUpdated, childValueRange, mainRecodObjectName, childRecord, childRecordProperties, childFieldsToBeUpdated) {
        var that = this,
            itemsChanged = [],
            newRecordFields,
            numberOfRowsPerRecord = that.tableRowLengthMap[sheetToBeUpdated][childRecordProperties.TABLE],
            recordToBeProcessed,
            isNewRecord,
            singleRowChildValue,
            fieldChanged,
            childRecordToBeSynced,
            parentRecord;

        that._orderOfGrandChildParent[sheetToBeUpdated] = [];

        for (var j = 0; j < childValueRange.length; j += numberOfRowsPerRecord) {

            newRecordFields = undefined;
            recordToBeProcessed = undefined;
            childRecordToBeSynced = that.mainRecord.relatedList(objectsToBeUpdated);

            for (var z = 0; z < numberOfRowsPerRecord; z++) {

                singleRowChildValue = childValueRange[j + z];

                if (z === 0) { // new data row
                    if (singleRowChildValue[0]) { // has id
                        isNewRecord = false;
                        for (var record = 0; record < childRecord.length; record++) {
                            if (singleRowChildValue[0] === childRecord[record].Id) {
                                recordToBeProcessed = childRecord[record];
                                break;
                            }
                        }
                        if (!recordToBeProcessed) {
                            console.log("Warning! No record present with the given Id " + singleRowChildValue[0]);
                            break;
                        }
                    }
                    else {
                        isNewRecord = true;
                    }
                }

                if (isNewRecord && (singleRowChildValue.length === 0 ||
                    singleRowChildValue.filter(function (value) {
                        return value !== ''
                    }).length === 0)) {
                    continue;
                }else {
                    newRecordFields = newRecordFields || {};
                }

                for (var k = 0; k < singleRowChildValue.length; k++) {
                    var field = childRecordProperties[z][k];
                    if (isNewRecord) {
                        newRecordFields = that._processSheetNewRecord(objectsToBeUpdated, newRecordFields, field, singleRowChildValue[k]);

                        newRecordFields = cq.overridable.postProcess(objectsToBeUpdated, newRecordFields, childRecordToBeSynced.data());

                        if (newRecordFields.Id) {
                            recordToBeProcessed = newRecordFields;
                            if ($.inArray(field, childFieldsToBeUpdated) > -1) {
                                var updatedRecord = that._processSheetExistingRecord(objectsToBeUpdated, newRecordFields, field, singleRowChildValue[k], fieldChanged);
                                recordToBeProcessed = updatedRecord['recordToBeProcessed'];
                                fieldChanged = updatedRecord['fieldChanged'];
                            }
                            isNewRecord = false;
                        }

                    } else {
                        if ($.inArray(field, childFieldsToBeUpdated) > -1) {
                            var updatedRecord = that._processSheetExistingRecord(objectsToBeUpdated, recordToBeProcessed, field, singleRowChildValue[k], fieldChanged);
                            recordToBeProcessed = updatedRecord['recordToBeProcessed'];
                            fieldChanged = updatedRecord['fieldChanged'];
                        }
                    }

                    if (fieldChanged) {
                        itemsChanged.push({ chk: recordToBeProcessed, fieldChanged: fieldChanged });
                        fieldChanged = undefined; // reset
                    }
                }
            }

            parentRecord = undefined;
            if (isNewRecord) {
                if (newRecordFields !== undefined && cq.overridable.recordValidator.isValid(objectsToBeUpdated, newRecordFields)) {
                    var childModel = childRecordToBeSynced.add({});
                    if (mainRecodObjectName) {
                        newRecordFields[mainRecodObjectName] = that.mainRecord.Id;
                    }
                    for (var fieldToSet in newRecordFields) {
                        that._setValue(childModel, fieldToSet, newRecordFields[fieldToSet]);
                    }
                    childRecordToBeSynced.sync();
                    parentRecord = childModel;
                }
            }
            else if (cq.overridable.recordValidator.isValid(objectsToBeUpdated, recordToBeProcessed)) {
                parentRecord = recordToBeProcessed;
            }

            that._orderOfGrandChildParent[sheetToBeUpdated].push(parentRecord);
        }

        return itemsChanged;
    };


    /**
    * @description : processes the sheet values which are aligned horizontally
    * @params: objectToBeUpdated : object name which value is to be updated
    *           childValueRange: array of values extracted from sheets
    *           mainRecodObjectName: main Record of the page
    *           childRecord: record which is to be updated
    *           childRecordProperties: properties of child record in the sheet
    *           childFieldToBeUpdated: fields of the object that are to be updated
    */
    gsheet.prototype._processHorizontalSheetValues = function (sheetToBeUpdated, objectsToBeUpdated, childValueRange, mainRecodObjectName, childRecord, childRecordProperties, childFieldsToBeUpdated) {
        var that = this,
            itemsChanged = [],
            newChildRecords,
            newChildRecord,
            parentRecord,
            numberOfRecords = childRecordProperties.TOTAL_COLUMNS_WITHIN_TABLE / childRecordProperties.NO_COLUMNS_PER_RECORD,
            numberOfRowsPerRecord = that.tableRowLengthMap[sheetToBeUpdated][childRecordProperties.TABLE],
            isNewRecord,
            fieldChanged,
            singleRowChildValue,
            childColumnCount,
            childRecordToBeProcessed,
            childRecordToBeSynced,
            skipLoop;

        if (childRecordProperties.TOTAL_COLUMNS_WITHIN_TABLE % childRecordProperties.NO_COLUMNS_PER_RECORD !== 0) {
            console.error('Child columns not defined properly. Expected integer to be found but found ' +
                childRecordProperties.TOTAL_COLUMNS_WITHIN_TABLE / childRecordProperties.NO_COLUMNS_PER_RECORD +
                'grandchild to be found but has' +
                childRecordProperties.TOTAL_COLUMNS_WITHIN_TABLE +
                'total columns and ' +
                childRecordProperties.NO_COLUMNS_PER_RECORD +
                'columns per record');
            return;
        }

        var pRecordIndex = 0;
        for (var k = 0; k < childValueRange.length; k += numberOfRowsPerRecord) {

            parentRecord = that._orderOfGrandChildParent[sheetToBeUpdated] && that._orderOfGrandChildParent[sheetToBeUpdated][pRecordIndex++];

            if (!parentRecord) {
                continue;
            }

            childRecordToBeSynced = parentRecord.relatedList(objectsToBeUpdated);
            childColumnCount = 0;
            newChildRecords = [];

            for (var z = 0; z < numberOfRecords; z++) {

                newChildRecord = undefined;
                childRecordToBeProcessed = undefined;
                skipLoop = false;

                for (var q = 0; q < numberOfRowsPerRecord; q++) {

                    if (childValueRange[k + q].length === 0) {
                        skipLoop = true;
                        break;
                    }

                    singleRowChildValue = childValueRange[k + q].slice(childColumnCount, childColumnCount + childRecordProperties.NO_COLUMNS_PER_RECORD);

                    if (q === 0) {
                        if (singleRowChildValue[0]) {
                            isNewRecord = false;
                            var childRecords = parentRecord[objectsToBeUpdated];
                            for (var record = 0; record < childRecord.length; record++) {
                                if (singleRowChildValue[0] === childRecord[record].Id) {
                                    childRecordToBeProcessed = childRecord[record];
                                    break;
                                }
                            }
                            if (!childRecordToBeProcessed) {
                                console.log("Warning! No record present with the given Id " + singleRowChildValue[0]);
                                break;
                            }
                        }
                        else {
                            isNewRecord = true;
                        }
                    }

                    if (isNewRecord && (singleRowChildValue.length === 0 ||
                        singleRowChildValue.filter(function (value) {
                            return value !== ''
                        }).length === 0)) {
                        continue;
                    }
                    else {
                        newChildRecord = newChildRecord || {};
                    }

                    for (var j = 0; j < singleRowChildValue.length; j++) {
                        var field = childRecordProperties[q][j];

                        if (isNewRecord) {

                            newChildRecord = that._processSheetNewRecord(objectsToBeUpdated, newChildRecord, field, singleRowChildValue[j]);

                            newChildRecord = cq.overridable.postProcess(objectsToBeUpdated, newChildRecord, childRecordToBeSynced.data());

                            if (newChildRecord.Id) {
                                childRecordToBeProcessed = newChildRecord;
                                if ($.inArray(field, childFieldsToBeUpdated) > -1) {

                                    var updatedRecord = that._processSheetExistingRecord(objectsToBeUpdated, newChildRecord, field, singleRowChildValue[j], fieldChanged);
                                    childRecordToBeProcessed = updatedRecord['recordToBeProcessed'];
                                    fieldChanged = updatedRecord['fieldChanged'];
                                }

                                isNewRecord = false;
                            }

                        } else {
                            if ($.inArray(field, childFieldsToBeUpdated) > -1) {

                                var updatedRecord = that._processSheetExistingRecord(objectsToBeUpdated, childRecordToBeProcessed, field, singleRowChildValue[j], fieldChanged);
                                childRecordToBeProcessed = updatedRecord['recordToBeProcessed'];
                                fieldChanged = updatedRecord['fieldChanged'];
                            }
                        }

                        if (fieldChanged) {
                            itemsChanged.push({ chk: childRecordToBeProcessed, fieldChanged: fieldChanged });
                            fieldChanged = undefined;
                        }
                    }
                }

                if (skipLoop) {
                    break;
                }

                if (isNewRecord && newChildRecord !== undefined) {
                    newChildRecords.push(newChildRecord);
                }

                childColumnCount += childRecordProperties.NO_COLUMNS_PER_RECORD;
            }

            if (newChildRecords.length > 0) {
                for (var index = 0; index < newChildRecords.length; index++) {
                    if (cq.overridable.recordValidator.isValid(objectsToBeUpdated, newChildRecords[index])) {
                        var childModel = childRecordToBeSynced.add({});
                        if (mainRecodObjectName) {
                            newChildRecords[index][mainRecodObjectName] = that.mainRecord.Id;
                        }
                        for (var fieldToSet in newChildRecords[index]) {
                            that._setValue(childModel, fieldToSet, newChildRecords[index][fieldToSet]);
                        }
                        childRecordToBeSynced.sync();
                    }
                }
            }
        }

        return itemsChanged;
    };

    /*
    * @description: processes the new record extracted from the sheet
    * @params: objectsToBeUpdated: name of the object that is to be updated
    *           recordToBeAdded: record that is to be added in sf
    *           field: field name whose value is to be added
    *           sheetValue: value which is to be added
    */
    gsheet.prototype._processSheetNewRecord = function (objectsToBeUpdated, recordToBeAdded, field, sheetValue) {
        var that = this;
        if (field === undefined) {
            field = '';
        }
        if (field !== '') {
            if (field.endsWith("__r.Name")) {
                var rField = field.split('.')[0],
                    originalFieldName = rField.replace('__r', '__c'),
                    lookupFieldValues = that._lookup_AllValuesMap[objectsToBeUpdated][field],
                    lookupFieldValue = {};


                if (lookupFieldValues === undefined)
                    return;

                for (var r = 0; r < lookupFieldValues.length; r++) {
                    if (Object.keys(that._lookup_AllValuesMap[objectsToBeUpdated][field][r])[0] === sheetValue) {
                        lookupFieldValue = that._lookup_AllValuesMap[objectsToBeUpdated][field][r][sheetValue];
                        recordToBeAdded[originalFieldName] = lookupFieldValue.Id;
                        recordToBeAdded[rField] = lookupFieldValue;
                        break;
                    }
                }

            } else {
                recordToBeAdded[field] = sheetValue;
            }

        }

        return recordToBeAdded;
    };


    /*
    * @description: processes the new record extracted from the sheet
    * @params: objectsToBeUpdated: name of the object that is to be updated
    *           recordToBeAdded: record that is to be added in sf
    *           field: field name whose value is to be added
    *           sheetValue: value which is to be added
    *           fieldChanged: field whose value is changed
    */
    gsheet.prototype._processSheetExistingRecord = function (objectsToBeUpdated, recordToBeUpdated, field, sheetValue, fieldChanged) {
        var that = this,
            returnValue = {};
        if (field.endsWith("__r.Name")) {
            var resultValues = that.validationValuesMap[recordToBeUpdated.Id][field];
            for (var w = 0; w < resultValues.length; w++) {
                if (resultValues[w].Name === sheetValue) {
                    var rField = field.split('.')[0],
                        originalFieldName = rField.replace('__r', '__c');
                    if (recordToBeUpdated[originalFieldName] !== resultValues[w].Id) {
                        that._setValue(recordToBeUpdated, rField, resultValues[w]);
                        fieldChanged = fieldChanged || {};
                        fieldChanged[originalFieldName] = true;
                    }
                    break;
                }
            }
        } else {
            if (!(recordToBeUpdated[field] === undefined && sheetValue === "") &&
                recordToBeUpdated[field] !== sheetValue) {
                that._setValue(recordToBeUpdated, field, sheetValue);
                fieldChanged = fieldChanged || {};
                fieldChanged[field] = true;
            }
        }

        returnValue['recordToBeProcessed'] = recordToBeUpdated;
        returnValue['fieldChanged'] = fieldChanged;

        return returnValue;
    };


    /**
     * Method sets the given value for the given field in the given model
     * for picklist values, it sets the actual value of the field and not the text
     * @param {CQDatasource} model 
     * @param {String} field 
     * @param {String} value 
     */
    gsheet.prototype._setValue = function (model, field, value) {
        var that = this;

        if (model.fields[field] && model.fields[field].type === "values") {
            var validValue = model.fields[field].additionalInfo.validValues.find(function (s) { return s.text === value });
            if (validValue) {
                value = validValue.value;
            }
        }

        model.set(field, value);
    }



    /**
    * Internal method to get named ranges from template
    */
    gsheet.prototype._getNamedRanges = function () {
        var that = this;

        return new Promise(function (resolve, reject) {
            if (that.controller.getNamedRanges) {

                that.controller.getNamedRanges(function (result, ev) {
                    if (ev.status) {
                        var response = JSON.parse(result);

                        NAMESPACE_PREFIX = response.NAMESPACE_PREFIX;
                        var responseData = JSON.parse(response.RESPONSE);

                        responseData = cq.overridable.preProcess(responseData);

                        that._currentSheet = responseData;

                        var processResult = that._processNamedRanges();
                        if (processResult === null)
                            reject("No namedranges in template");
                        else
                            resolve(processResult);
                    }
                    else {
                        reject(ev);
                    }
                }, { escape: false });
            }
            else {
                reject('UIController doesn\'t have getNamedRanges method');
            }

        });
    }

    /**
    * Internal method to get named ranges from sheet
    */
    gsheet.prototype._getSheetNamedRanges = function (attachmentId) {
        var that = this;

        return new Promise(function (resolve, reject) {
            if (that.controller.getSheetNamedRanges) {

                that.controller.getSheetNamedRanges(that.mainRecord.Id, attachmentId, function (result, ev) {
                    if (ev.status) {
                        try {
                            var response = JSON.parse(result);

                            NAMESPACE_PREFIX = response.NAMESPACE_PREFIX;
                            var responseData = JSON.parse(response.RESPONSE);

                            responseData = cq.overridable.preProcess(responseData);

                            that._currentSheet = responseData;
                            that._processNamedRanges();
                            resolve(response.SHEET_ID);
                        }
                        catch (ex) {
                            reject(ex);
                        }
                    }
                    else {
                        reject('Error occurred while retrieving named ranges.');
                    }
                }, { escape: false });
            }
            else {
                reject('UIController doesn\'t have getNamedRanges method');
            }

        });
    }

    /**
    *   Extract all the namedranges in the sheet and process them to create several objects to capture the structure of the sheet using the details extracted
    *   return {String} null if no namedranges have been set in the sheet, "Done processing namedranges" otherwise
    */
    gsheet.prototype._processNamedRanges = function () {
        var namedRanges = this._currentSheet.namedRanges;
        var tabularFieldsList = [];

        if (namedRanges === undefined)
            return null;

        for (var i = 0; i < namedRanges.length; i++)  // scanning through all the named ranges
        {
            var namedRange = namedRanges[i];
            var name = namedRange.name; // name of the namedRange
            var range = namedRange.range;
            var sheetId = range.sheetId || 0;
            var sheetName = this._getSheetName(sheetId);

            if (name.startsWith("t_"))  // meaning its a child records which has a tabular form in the sheet
            {
                var tableName = name.replace("t_", "");
                var aliasName = this.aliases[tableName];
                if (aliasName !== undefined)
                    this._setNamedRangeDetailsOfThisTable(sheetName, tableName, aliasName, namedRange);
                else
                    console.log(name + " invalid!! Hence, ignored!!");
            }
            else if (name.startsWith("tr_")) // specifies the thick-row range for each record
            {
                var tableName = name.replace("tr_", "");
                var aliasName = this.aliases[tableName];
                if (aliasName !== undefined) {
                    // find the row length (per record)
                    var rowLength = range.endRowIndex - range.startRowIndex;
                    var temp_map = {};
                    temp_map[aliasName] = rowLength;
                    this.tableRowLengthMap[sheetName] = temp_map;
                }
                else
                    console.log(name + " invalid!! Hence, ignored!!");
            }
            else if (name.startsWith("th_")) // specifies the table header
            {
                var tableName = name.replace("th_", "");
                var aliasName = this.aliases[tableName];
                if (aliasName !== undefined)
                    this._processTableHeader(sheetName, aliasName, namedRange);
                else
                    console.log(name + " invalid!! Hence, ignored!!");
            }
            else if (name.startsWith("td_")) // one of the tabular fields(columns)
            {
                tabularFieldsList.push(namedRange);
            }
            else    // main record fields
            {
                var fieldName = this._getStandardFieldName(name, this.mainRecord.sObjectName);
                if (fieldName !== '')
                    this._processMainRecordFields(sheetName, fieldName, range);
            }
        }

        // check if all the tables in each sheet have thick-row defined
        // if not set it to a default value
        for (var sheetName in this.tableMappedWithNamedRangeDetail) {
            var tableDetail = this.tableMappedWithNamedRangeDetail[sheetName];
            for (var tableName in tableDetail)   // only one per sheet
            {
                if (!(sheetName in this.tableRowLengthMap)) {
                    var temp_map = {};
                    temp_map[tableName] = DEFAULT_THICK_ROW_SIZE;
                    this.tableRowLengthMap[sheetName] = temp_map;
                }
            }
        }

        var tableMappedWithFieldsList = {};
        if (tabularFieldsList.length > 0) {
            // mapping tabular fields with their corresponding table
            for (var i = 0; i < tabularFieldsList.length; i++) {
                var namedRange = tabularFieldsList[i];
                var name = namedRange.name;
                var sheetId = namedRange.range.sheetId || 0;
                var sheetName = this._getSheetName(sheetId);

                var tables = this.tableMappedWithNamedRangeDetail[sheetName];

                if (tables === undefined) {
                    console.log(name + " table header not set");
                    continue;
                }

                var trimmedName = "BLANK";
                for (var table in tables)    // note : there should only be one table(per sheet)
                {
                    var tableName = tables[table].aliasOf;
                    if (name.startsWith("td_" + tableName + "_")) {
                        var trimmedName = name.replace("td_" + tableName + "_", "");
                        var tablesMap = tableMappedWithFieldsList[sheetName] || {};
                        var tableFieldMap = tablesMap[table] || {};
                        tableFieldMap[trimmedName] = namedRange;
                        tablesMap[table] = tableFieldMap;
                        tableMappedWithFieldsList[sheetName] = tablesMap;
                        break;
                    }
                }
                if (trimmedName === "BLANK")
                    console.info(name + " invalid!! ignored");

            }
            this._processColumns(tableMappedWithFieldsList);
        }
        return "Done processing namedranges";
    }

    /**
     *  Get the sobject name of the field whose relationship name is specified
     *  @param {String} parent - sObjectName of the parent record
     *  @param {String} rName - relationship name of the field
     *  @returns {String} sObject name of the given field
     */
    gsheet.prototype._getSObjectNameFromRelationshipName = function (parent, rName) {
        var children = SQXSchema[parent].childRelationships;
        for (var k = 0; k < children.length; k++) {
            var child = children[k];
            if (child.relationshipName === rName)
                return child.childSObject;
        }
        return null;
    }

    /**
     *  Get the api name of the field 
     *  @param {String} name - name of the field specified in the sheet  
     *  @param {String} objectName - api name of the object of which the field is a part of
     *  @return {String} api name of the field specified in the sheet 
    */
    gsheet.prototype._getStandardFieldName = function (name, objectName) {
        var stdName = name;
        var keys;
        if (name.match(/__c$|__r\..*$/)) {
            if (name.match(/__r\..*$/)) {
                // lookup field
                keys = Object.keys(SQXSchema[objectName]);
                name = name.split(".")[0];
            }
            else if (name.match(/__c$/)) {
                keys = Object.keys(SQXSchema[objectName].fields);
            }
            if (keys.indexOf(name) === -1) {
                //add namespace prefix                               
                name = NAMESPACE_PREFIX + name;
                if (keys.indexOf(name) !== -1) {
                    stdName = NAMESPACE_PREFIX + stdName;
                }
                else
                    stdName = '';
            }
        }
        return stdName;
    }

    /**
    *   Store the namedrange details of the given table in the given sheet
    *   Apart from the range details, the namedrange id and name are also stored which is to be used to create update named range request
    *   @param {String} sheetName - name of the sheet
    *   @param {String} tableName - name of the table in the sheet
    *   @param {String} aliasName - alias name of the table
    *   @param {Object} namedRange - namedRange details of the given table
    */
    gsheet.prototype._setNamedRangeDetailsOfThisTable = function (sheetName, tableName, aliasName, namedRange) {
        var range = namedRange.range;
        var standardRange = this._createRangeFromIndex(range);

        var namedRangeMap =
            {
                "aliasOf": tableName,
                "name": namedRange.name,
                "id": namedRange.namedRangeId,
                "range": namedRange.range,
                "standardRange": standardRange
            };

        // NOTE: 
        // ONE SHEET CAN HAVE ONLY ONE TABLE        
        var tableMap = {};
        tableMap[aliasName] = namedRangeMap;
        this.tableMappedWithNamedRangeDetail[sheetName] = tableMap;
    }

    /**
    *   Verify if the given field in the given sheet is protected or not
    *   @param {String} sheetName - name of the sheet
    *   @param {String} field - name(api name) of the field
    *   @return {Boolean} True if the field has been set as protected, false otherwise
    */
    gsheet.prototype._isProtected = function (sheetName, field) {
        for (var k = 0; k < this.protectedFields.length; k++) {
            var protectedFieldsMap = this.protectedFields[k];
            if (protectedFieldsMap["sheet"] === sheetName)
                if (protectedFieldsMap["protected"].indexOf(field) !== -1)
                    return true;
        }
        return false;
    }

    /**
    *   For each table, per sheet, extract and store table-header details as well as store associated detail(range at which the data is to be added in the table)
    *   @param {String} sheetName - name of the sheet
    *   @param {String} aliasName - alias name of the table under consideration 
    *   @param {Object} namedRange - namedrange details of the header 
    */
    gsheet.prototype._processTableHeader = function (sheetName, aliasName, namedRange) {
        var range = $.extend({}, namedRange.range);  // making a shallow copy rather than a deep copy

        range.startRowIndex = range.endRowIndex;    // storing the header's range is insignificant; instead we are storing the range at which data is to be inserted
        range.endRowIndex = range.startRowIndex + 1;
        var standardRange = this._createRangeFromIndex(range);   // converting to A1 notation

        var rangeMap = this.rangeMap[sheetName] || {};
        var isProtected = this._isProtected(sheetName, aliasName);
        rangeMap[aliasName] =
            {
                "protected": isProtected,
                "standardRange": standardRange,
                "range": range
            };
        this.rangeMap[sheetName] = rangeMap;

        var headerMap = this._sheetTablesHeaderRange[sheetName] || {};
        headerMap[aliasName] = namedRange          // named-range details for headers are stored because they have to updated (as new dimensions are inserted)  }
        this._sheetTablesHeaderRange[sheetName] = headerMap;
    }

    /**
    *   Process main record (i.e. non-tabular) fields and store the details associated with each field
    *   @param {String} sheetName - name of the sheet
    *   @param {String} fieldName - name(api name) of the field 
    *   @param {Object} range - object with range details
    */
    gsheet.prototype._processMainRecordFields = function (sheetName, fieldName, range) {
        var standardRange = this._createRangeFromIndex(range);

        var rangeMap = this.rangeMap[sheetName] || {};
        var isProtected = this._isProtected(sheetName, fieldName);
        rangeMap[fieldName] =
            {
                "standardRange": standardRange,
                "protected": isProtected
            }

        this.rangeMap[sheetName] = rangeMap;

        var mainRecordList = this.mainRecordFields[sheetName] || [];
        mainRecordList.push(fieldName);
        this.mainRecordFields[sheetName] = mainRecordList;
    }

    /**
    *   Process all the tabular columns per sheet
    *   @param {Object} tableFieldsMap - contains a map of master table mapped with its corresponding child column details, per sheet
    */
    gsheet.prototype._processColumns = function (tableFieldsMap) {

        var MAX = 99999;

        for (var sheetName in tableFieldsMap) {
            var grandChild_startingColumnIndex = MAX;
            var childColumnsMap = {};
            var grandChildColumnsMap = {};

            var tableMap = tableFieldsMap[sheetName];
            for (var tableName in tableMap)          // there will only be one table
            {
                var fieldsMap = tableMap[tableName];
                var table_sObjectName = this._getSObjectNameFromRelationshipName(this.mainRecord.sObjectName, tableName);
                for (var field in fieldsMap) {
                    var namedRange = fieldsMap[field];
                    if (field.startsWith("t_")) {
                        var grandChild = field.replace("t_", "");            //  ex: ObjectiveEvidence_td_Name
                        var grandChildName = grandChild.slice(0, grandChild.indexOf("_"));   //  ex: ObjectiveEvidence
                        var grandChildAliasName = this.aliases[grandChildName];             //  ex: devcq06__Objective_Evidence__c

                        if (grandChildAliasName === undefined) {
                            console.info(namedRange.name + " ignored!!");
                            continue;
                        }

                        var fieldName = field.replace("t_" + grandChildName + "_", "");             // ex: Name or Description or h[Dynamic Header]
                        var fieldMapping = grandChildColumnsMap[grandChildAliasName] || {};
                        fieldMapping[fieldName] = namedRange;
                        grandChildColumnsMap[grandChildAliasName] = fieldMapping;
                    }
                    else if (field.startsWith("th_")) {
                        /*
                            There can be multiple grandchildren(ex: Finding and Objective Evidence for Checklist)
                            It is necessary to know the starting column of the first grandchild as we have to split child range from grandchild range
                            which is why the following logic has been placed
                        */
                        if (grandChild_startingColumnIndex > namedRange.range.startColumnIndex)
                            grandChild_startingColumnIndex = namedRange.range.startColumnIndex;

                        var grandChildTableName = field.replace("th_", "");
                        var aliasName = this.aliases[grandChildTableName];
                        if (aliasName !== undefined)
                            this._processTableHeader(sheetName, aliasName, namedRange);
                        else
                            console.log(namedRange.name + " invalid!! Ignored!!");
                    }
                    else    // not a grand-child field
                    {
                        var fieldName = this._getStandardFieldName(field, table_sObjectName);
                        childColumnsMap[fieldName] = namedRange;
                    }
                }
            }
            /* separating child and grand-child columns for further processing
            */
            var tables = this.tableMappedWithNamedRangeDetail[sheetName];
            if (tables !== undefined) {
                for (var table in tables)            // again, just one table in this
                {
                    var tableRangeDetails = tables[table];

                    if (Object.keys(childColumnsMap).length > 0) // processing child columns
                    {
                        var table_range = tableRangeDetails.range;
                        var childColumn_StartColumn = table_range.startColumnIndex;
                        var childColumn_EndColumn = (grandChild_startingColumnIndex < MAX) ? grandChild_startingColumnIndex : table_range.endColumnIndex;
                        this._processChildColumns(sheetName, table, childColumnsMap, childColumn_StartColumn, childColumn_EndColumn);
                    }
                    if (grandChild_startingColumnIndex < MAX && Object.keys(grandChildColumnsMap).length > 0)    // processing grandchildren columns
                    {
                        for (var key in grandChildColumnsMap) {
                            if (this.rangeMap[sheetName] === undefined)
                                break;
                            var rangeDetails = this.rangeMap[sheetName][key];
                            if (rangeDetails !== undefined) {
                                var range = rangeDetails.standardRange;
                                var rangeMap = this._getRangeMapFromStandardRange(range);

                                this._processGrandChildColumns(sheetName, table, key, grandChildColumnsMap[key], rangeMap.startColumnIndex, rangeMap.endColumnIndex);
                            }
                        }
                    }
                }
            }
            else {
                console.log("Given sheet " + sheetName + " doesn't have any tables!");
            }
        }
    }

    /**
    *   Process child tables to extract details such fields, total number of columns, parent table, etc per child table per sheet
    *   @param {String} sheetName - name of the sheet
    *   @param {String} tableName - name of the master table
    *   @param {Object} childColumnsMap - contains field and its corresponding details under the given table
    *   @param {Number} startColumn - column at which the given table starts in the sheet
    *   @param {Number} endColumn - column at which the given table ends in the sheet
    */
    gsheet.prototype._processChildColumns = function (sheetName, tableName, childColumnsMap, startColumn, endColumn) {
        var fieldsPositionMap = {}; // used to store values and their corresponding position in the sheet        
        var header = {};

        for (var fieldName in childColumnsMap) {
            var namedRange = childColumnsMap[fieldName];
            var range = namedRange.range;
            var startingColumn = range.startColumnIndex;
            var row = range.startRowIndex;

            if (fieldsPositionMap[row] === undefined)
                fieldsPositionMap[row] = {};
            fieldsPositionMap[row][startingColumn] = fieldName;
        }

        var totalColumns = endColumn - startColumn;
        this._storeChildren(sheetName, tableName, fieldsPositionMap, startColumn, endColumn, CHILD_TABLE_ORIENTATION, this.mainRecord, header, tableName, totalColumns);
    }

    /**
    *   Process grand-child tables to extract details such fields, total number of columns, parent table, etc per grand-child table per sheet
    *   @param {String} sheetName - name of the sheet
    *   @param {String} tableName - name of the master table
    *   @param {String} grandChildTable - name of the grand-child table(table under consideration)
    *   @param {Object} grandChildColumns - contains field and its corresponding namedrange details under the given table
    *   @param {Number} startColumn - column at which the given table starts in the sheet
    *   @param {Number} endColumn - column at which the given table ends in the sheet
    */
    gsheet.prototype._processGrandChildColumns = function (sheetName, tableName, grandChildTable, grandChildColumns, startColumn, endColumn) {
        var childRecord = this.mainRecord.relatedList(tableName),
            childRecord_sObjectName = this._getSObjectNameFromRelationshipName(this.mainRecord.sObjectName, tableName),
            grandChildTable_sObjectName = this._getSObjectNameFromRelationshipName(childRecord_sObjectName, grandChildTable),
            grandChildFieldsPositionMap = {}, // used to store values and their corresponding position in the sheet for grand-child records
            header = {},
            range = {},
            endColumnPerRecord = -1;

        for (var field in grandChildColumns) {
            var namedRange = grandChildColumns[field];
            if (field.startsWith("td_"))  // grand child field
            {
                var fieldName = field.replace("td_", "");
                var startingColumn = namedRange.range.startColumnIndex; // use this to sort the list of fields according to their position in the sheet
                var row = namedRange.range.startRowIndex;

                if (grandChildFieldsPositionMap[row] === undefined)
                    grandChildFieldsPositionMap[row] = {};

                fieldName = this._getStandardFieldName(fieldName, grandChildTable_sObjectName);
                grandChildFieldsPositionMap[row][startingColumn] = fieldName;
            }
            else if (field.match(/^h(_.+)?$/))      // dynamic header // for vertical orientation
            {
                var fieldName = field.replace("h", "");
                range = namedRange.range;

                var standardRange = this._createRangeFromIndex(range);

                if (fieldName.length > 0) {        // user-defined header
                    fieldName = fieldName.replace("_", "");
                    fieldName = this._getStandardFieldName(fieldName, grandChildTable_sObjectName);
                }
                else {
                    // EXTRACT SUB TABLE NAME                    
                    fieldName = SQXSchema[grandChildTable_sObjectName].sObjectLabel;
                }

                header =
                    {
                        "name": fieldName,
                        "range": standardRange
                    };
                endColumnPerRecord = range.endColumnIndex;
            }
            else {
                console.log(namedRange.name + " invalid!! Ignored!!");
            }
        }
        endColumn = endColumnPerRecord !== -1 ? endColumnPerRecord : endColumn;
        var totalColumns = endColumn - startColumn;
        this._storeChildren(sheetName, grandChildTable, grandChildFieldsPositionMap, startColumn, endColumn, GRAND_CHILD_TABLE_ORIENTATION, childRecord, header, tableName, totalColumns);
    }

    /**
    *   Store tabular details(e.g column names, total number of columns, parent table name, orientation, etc) per table per sheet
    *   The details are stored as objects in tablesMap
    *   @param {String} sheetName - name of the sheet 
    *   @param {String} tableName - name of the table(api name) whose details are to be stored
    *   @param {Object} fieldsPositionMap - 
    *   @param {Number} startColumn - column at which the given table starts in the sheet
    *   @param {Number} endColumn - column at which the given table ends in the sheet
    *   @param {String} table_orientaion - orientation of the table about which the data will be stored(e.g HORIZONTAL implies data is to be appended column wise)
    *   @param {Object} parentRecord - datastore object of parent record
    *   @param {Object} header - contains details of the header (undefined except for grand-child tables which grow dynamically)
    *   @param {String} parentTable - name(api name) of the master table
    *   @param {Number} totalColumns - total number of columns in the table
    */
    gsheet.prototype._storeChildren = function (sheetName, tableName, fieldsPositionMap, startColumn, endColumn, table_orientation, parentRecord, header, parentTable, totalColumns) {
        var fields = [];

        var temp = -1;
        // accounting for empty rows
        for (var row in fieldsPositionMap) {
            if (temp === -1)
                temp = parseInt(row);
            else if (parseInt(row) !== (temp + 1)) {
                var emptyMap = {};
                for (var i = startColumn; i < endColumn; i++)
                    emptyMap[i] = "";
                fieldsPositionMap[temp + 1] = emptyMap;

            }
            temp = parseInt(row);
        }

        //  accounting for empty columns
        for (var row in fieldsPositionMap) {
            var columnFieldMap = fieldsPositionMap[row];
            for (var i = startColumn; i < endColumn; i++) {
                if (!(i in columnFieldMap))
                    columnFieldMap[i] = "";   // use this value to identify if a column has to be left blank  
            }
            var sortedListOfFields = [];
            for (var key in columnFieldMap)
                sortedListOfFields.push(columnFieldMap[key]);
            fields.push(sortedListOfFields);
        }

        fields.ORIENTATION = table_orientation;
        fields.PARENT = parentRecord;
        fields.HEADER = Object.keys(header).length > 0 ? header : undefined;
        fields.TABLE = parentTable;
        fields.NO_COLUMNS_PER_RECORD = totalColumns;
        fields.TOTAL_COLUMNS_WITHIN_TABLE = totalColumns;

        // mapping childRecord Name with associated list of sorted columns of the child record
        var tableMap = this.tablesMap[sheetName] || {};
        tableMap[tableName] = fields;
        this.tablesMap[sheetName] = tableMap;
    }

    /**
    *   Additional columns are added to each master-tables(at the front) in each sheet for incorporating IDs of each record
    *   Hidden requests are also made alongside to hide these added columns in the sheet
    */
    gsheet.prototype._addIdColumnToTables = function () {
        for (var sheetName in this.tableMappedWithNamedRangeDetail) {
            var tables = this.tableMappedWithNamedRangeDetail[sheetName];
            for (var table in tables) {
                var tableDetails = tables[table];

                var rangeMap = tableDetails.range;
                var sheetId = rangeMap.sheetId || 0;
                var standardRange = tableDetails.standardRange;

                rangeMap.endColumnIndex += 1;   // COMPENSATION FOR THE FIRST 'ID' COLUMN
                standardRange = this._createRangeFromIndex(rangeMap)

                var insertDimensionRequest =
                    {
                        "insertDimension":
                            {
                                "inheritFromBefore": "false",
                                "range":
                                    {
                                        "startIndex": rangeMap.startColumnIndex,
                                        "endIndex": rangeMap.startColumnIndex + 1,
                                        "dimension": "COLUMNS",
                                        "sheetId": sheetId
                                    }
                            }
                    };
                var hideDimensionRequest =
                    {
                        "updateDimensionProperties":
                            {
                                "properties":
                                    {
                                        "hiddenByUser": true
                                    },
                                "fields": "*",
                                "range":
                                    {
                                        "dimension": "COLUMNS",
                                        "sheetId": sheetId,
                                        "startIndex": rangeMap.startColumnIndex,
                                        "endIndex": rangeMap.startColumnIndex + 1
                                    }
                            }
                    };

                this._hideRequests.push(hideDimensionRequest);
                this._insertDimensionRequests.push(insertDimensionRequest);

                this.tableMappedWithNamedRangeDetail[sheetName][table].range = rangeMap;   // unnecessary step {THERE IS SOMETHING CALLED POINTER , YOU FOOL}
                this.tableMappedWithNamedRangeDetail[sheetName][table].standardRange = standardRange;

                this._addIdColumnToChildTables(sheetName);
            }
        }
        this._modifyRangeMap();
    }

    /**
    *   Additional columns are added to grand-child tables for the given sheet
    *   @param {String} sheetName - name of the sheet whose grand-child tables have to be appended with new column at the beginning 
    */
    gsheet.prototype._addIdColumnToChildTables = function (sheetName) {
        var no_of_columns_added = 1;        // One column added at the beginning of the table
        var sortedTablesMap = this._getSortedTablesMap(sheetName);

        for (var key in sortedTablesMap) {
            var tablesMap = sortedTablesMap[key];

            for (var table in tablesMap) {
                var fieldList = tablesMap[table];
                for (var i = 0; i < fieldList.length; i++) {
                    var rowList = fieldList[i];
                    rowList.unshift("");
                }
                fieldList.TOTAL_COLUMNS_WITHIN_TABLE += 1;
                fieldList.NO_COLUMNS_PER_RECORD += 1;

                if (fieldList.ORIENTATION === "VERTICAL")
                    continue;

                if (this.rangeMap[sheetName] === undefined)
                    return;

                var rangeDetail = this.rangeMap[sheetName][table];

                if (rangeDetail === undefined)
                    return;

                var rangeMap = rangeDetail.range;
                var sheetId = rangeMap.sheetId || 0;

                var insertDimensionRequest =
                    {
                        "insertDimension":
                            {
                                "inheritFromBefore": "false",
                                "range":
                                    {
                                        "startIndex": rangeMap.startColumnIndex + no_of_columns_added,
                                        "endIndex": rangeMap.startColumnIndex + no_of_columns_added + 1,
                                        "dimension": "COLUMNS",
                                        "sheetId": sheetId
                                    }
                            }
                    };
                var hideDimensionRequest =
                    {
                        "updateDimensionProperties":
                            {
                                "properties":
                                    {
                                        "hiddenByUser": true
                                    },
                                "fields": "*",
                                "range":
                                    {
                                        "dimension": "COLUMNS",
                                        "sheetId": sheetId,
                                        "startIndex": rangeMap.startColumnIndex + no_of_columns_added,
                                        "endIndex": rangeMap.startColumnIndex + no_of_columns_added + 1
                                    }
                            }
                    };

                this._hideRequests.push(hideDimensionRequest);
                this._insertDimensionRequests.push(insertDimensionRequest);

                no_of_columns_added += 1;

                // shift header by '1'            
                var header = fieldList.HEADER;
                if (header !== undefined) {
                    var headerRange = header.range;
                    var sheetName = headerRange.split("!")[0]
                    var rangeMap = this._getRangeMapFromStandardRange(headerRange);

                    rangeMap.startColumnIndex += no_of_columns_added;
                    rangeMap.endColumnIndex += no_of_columns_added;
                    headerRange = this._createRangeFromIndex(rangeMap);
                    header.range = headerRange;
                }

                // TRAVERSING THE CHANGES TO THE PARENT TABLE

                var parentTableName = fieldList.TABLE;
                var rangeDetail = this.tableMappedWithNamedRangeDetail[sheetName][parentTableName];
                if (rangeDetail !== undefined) {
                    var rangeMap = rangeDetail.range;
                    var standardRange = rangeDetail.standardRange;
                    rangeMap.endColumnIndex += 1;
                    var sheetName = standardRange.split("!")[0];

                    var new_standardRange = this._createRangeFromIndex(rangeMap);
                    this.tableMappedWithNamedRangeDetail[sheetName][parentTableName].range = rangeMap;   // unnecessary step {THERE IS SOMETHING CALLED POINTER , YOU FOOL}
                    this.tableMappedWithNamedRangeDetail[sheetName][parentTableName].standardRange = new_standardRange;
                }
            }
        }
    }


    /**
    *   Creates a new map from tablesMap with an additional property(start column index) that orders the tablesMap in ascending order of their starting position in the sheet
    *   @param {String} sheetName - Name of the sheet whose tables are to be sorted
    *   @return {Object} sorted tables map
    */
    gsheet.prototype._getSortedTablesMap = function (sheetName) {
        var sortedTablesMap = {};
        var tableMap = this.tablesMap[sheetName];
        if (tableMap === undefined)
            return null;
        for (var key in tableMap) {
            var rangeDetail = this.rangeMap[sheetName][key];
            if (rangeDetail === undefined)
                return null;
            var range = rangeDetail.range;

            sortedTablesMap[range.startColumnIndex] = {};
            sortedTablesMap[range.startColumnIndex][key] = tableMap[key];
        }
        return sortedTablesMap;
    }

    /**
    *   Create an object that maps each tables(in each sheet) to its corresponding row map
    *   Child table and grand-child table are treated as two different tables
    */
    gsheet.prototype._getRowData = function () {
        for (var sheetName in this.tableMappedWithNamedRangeDetail) {
            var tables = this.tableMappedWithNamedRangeDetail[sheetName];
            for (var table in tables)    // only one table always
            {
                var tableDetails = tables[table];
                var tableRange = tableDetails.range;
                var endIndex = tableRange.endColumnIndex;

                var rowLength = this.tableRowLengthMap[sheetName][table];
                var dataRange = $.extend({}, this.rangeMap[sheetName][table].range);
                dataRange.endRowIndex = dataRange.startRowIndex + rowLength;
                dataRange.endColumnIndex = endIndex;
                this._tableDataMap[sheetName] = dataRange;
            }
        }
    }

    /**
    *   For every grandchild table whose header has been defined, 
    *   Dynamic header names are set, grand-child table replication request is created, a parentChildMap consisting of child records mapped with sorted list of grand-child records is created
    *   The sorting is based on table header name
    */
    gsheet.prototype._setDynamicHeader = function () {

        for (var sheetName in this.tablesMap) {
            var tableMap = this.tablesMap[sheetName];
            var headerList = [];
            var gridProperties = this._getGridSize(sheetName);
            var maxColumn = gridProperties.columnCount;     // grid's maximum column(visible)
            // have to insert columns if while adding dynamic headers, the column index exceeds max column
            for (var table in tableMap) {
                var tableDetail = tableMap[table];
                var no_of_grandChildFields = tableDetail[0].length;
                var header = tableDetail.HEADER;
                var parentTable = tableDetail.TABLE;

                if (header === undefined)
                    continue;

                var headerName = header.name;
                var headerRange = header.range;
                var headerRangeMap = this._getRangeMapFromStandardRange(headerRange);

                var rangeDetail = this.rangeMap[sheetName][table];
                var sourceRange = $.extend({}, rangeDetail.range);       // shallow copy : such that the changes do not propagate

                var sheetId = sourceRange.sheetId || 0;

                var parent = tableDetail.PARENT;
                var parentList = [];

                // note : parent.view will be undefined if the parent is the main-record
                // with that note, the line below will make some sense
                (parent.view === undefined) ? parentList.push(parent) : parentList = parent.view();

                for (var i = 0; i < parentList.length; i++) {
                    var parentRecord = parentList[i];
                    var childRecordList = parentRecord.relatedList(table).view();
                    var sortedListOfRecords = {};

                    for (var j = 0; j < childRecordList.length; j++) {
                        var childRecord = childRecordList[j];
                        var headerValue = childRecord[headerName];
                        if (headerValue === undefined)        // not an SF field 
                            headerValue = headerName + ' ' + (j + 1);     // custom name generation [Ex: Objective Evidence 1]

                        var index = headerList.indexOf(headerValue);
                        if (index === -1) {
                            //  ADD HEADER NAME TO SHEET
                            var header =
                                [
                                    [
                                        headerValue
                                    ]
                                ];

                            if (headerList.length >= 1) {
                                /* Setting header range for new dynamically added grandchild */
                                headerRangeMap.startColumnIndex = headerRangeMap.endColumnIndex + (USE_ID === true ? 1 : 0);
                                headerRangeMap.endColumnIndex += no_of_grandChildFields;

                                /* Creating a copy paste request for copying grand-child table header into an adjacent vacant spot */
                                var tableRangeDetail = this.tableMappedWithNamedRangeDetail[sheetName][parentTable];
                                var tableRange = tableRangeDetail.range;
                                sourceRange.startRowIndex = tableRange.startRowIndex;

                                var destinationRange = $.extend({}, sourceRange);
                                destinationRange.startColumnIndex = sourceRange.endColumnIndex + (headerList.length - 1) * no_of_grandChildFields;
                                destinationRange.endColumnIndex = headerRangeMap.endColumnIndex;

                                if (destinationRange.startColumnIndex >= maxColumn) {
                                    var insertDimensionRequest =
                                        {
                                            "insertDimension":
                                                {
                                                    "inheritFromBefore": "false",
                                                    "range":
                                                        {
                                                            "startIndex": maxColumn - 1,
                                                            "endIndex": destinationRange.endColumnIndex,
                                                            "dimension": "COLUMNS",
                                                            "sheetId": sheetId
                                                        }
                                                }
                                        };
                                    this._insertDimensionRequests.push(insertDimensionRequest);
                                    maxColumn = destinationRange.endColumnIndex;
                                }

                                var copyPasteRequest =
                                    {
                                        "copyPaste":
                                            {
                                                "source": sourceRange,
                                                "destination": destinationRange
                                            }
                                    };
                                this._headerCopyRequests.push(copyPasteRequest);

                                if (USE_ID) {
                                    var hideDimensionRequest =
                                        {
                                            "updateDimensionProperties":
                                                {
                                                    "properties":
                                                        {
                                                            "hiddenByUser": true
                                                        },
                                                    "fields": "*",
                                                    "range":
                                                        {
                                                            "dimension": "COLUMNS",
                                                            "sheetId": sheetId,
                                                            "startIndex": destinationRange.startColumnIndex,
                                                            "endIndex": destinationRange.startColumnIndex + 1
                                                        }
                                                }
                                        };
                                    this._hideRequests.push(hideDimensionRequest);
                                }

                                //  update master table's range(i.e end column index)
                                if (tableRangeDetail !== undefined) {
                                    tableRange.endColumnIndex = destinationRange.endColumnIndex;

                                    var standardRange = tableRangeDetail.standardRange;
                                    var new_standardRange = this._createRangeFromIndex(tableRange);

                                    this.tableMappedWithNamedRangeDetail[sheetName][parentTable].range = tableRange;    // MOOT 
                                    this.tableMappedWithNamedRangeDetail[sheetName][parentTable].standardRange = new_standardRange;
                                    NAMEDRANGE_UPDATE_REQUIRED = true;
                                }

                            }

                            var modifiedRange = this._createRangeFromIndex(headerRangeMap);
                            this._headerValuesBatchUpdateRequest.push(this._createUpdateValuesObject(modifiedRange, header));

                            sortedListOfRecords[headerList.length] = childRecord;
                            headerList.push(headerValue);
                        }
                        else {
                            sortedListOfRecords[index] = childRecord;
                        }
                    }

                    // add empty maps if records are missing in between the sorted-list
                    for (var k = 0; k < headerList.length; k++)
                        if (!(k in sortedListOfRecords))
                            sortedListOfRecords[k] = {};

                    var parentChildMap = this._parentChildMap[sheetName] || {};
                    parentChildMap[parentRecord] = sortedListOfRecords;
                    this._parentChildMap[sheetName] = parentChildMap;
                }
            }

        }
        // since master table's range is modified, we have to call modifyRangeMap to propagate the changes to child and grandchild tables
        this._modifyRangeMap();
    }

    /** 
    *   Modify rangemap(this.rangeMap) of child and grandchildren with respect to the parent table's range
    *   Called when master table's range is modified
    */
    gsheet.prototype._modifyRangeMap = function () {
        for (var sheetName in this.tablesMap) {
            var sortedTablesMap = this._getSortedTablesMap(sheetName);  // sorted map of tables based on their starting position in the sheet
            var endColumnOfPreviousTable = 0;   // will change per table that is scanned
            var keys = Object.keys(sortedTablesMap);

            for (var key in sortedTablesMap) {
                var tablesMap = sortedTablesMap[key];

                for (var table in tablesMap) {
                    var tableDetails = tablesMap[table];
                    var parentTable = tableDetails.TABLE;

                    var parentTableDetails = this.tableMappedWithNamedRangeDetail[sheetName][parentTable];
                    var lastRow = parentTableDetails.range.endRowIndex;

                    var rangeDetail = this.rangeMap[sheetName][table];

                    if (rangeDetail === undefined)
                        break;

                    var rangeMap = rangeDetail.range;
                    var numberOfColumns = tableDetails[0].length;

                    if (tableDetails.ORIENTATION === CHILD_TABLE_ORIENTATION) {
                        rangeMap.startColumnIndex = parentTableDetails.range.startColumnIndex;
                        rangeMap.endColumnIndex = rangeMap.startColumnIndex + numberOfColumns;
                    }
                    else if (tableDetails.ORIENTATION === GRAND_CHILD_TABLE_ORIENTATION) {
                        rangeMap.startColumnIndex = endColumnOfPreviousTable;
                        // if its the last grand-child table, the end column index will be that of the parent table's 
                        rangeMap.endColumnIndex = (keys.indexOf(key) === (keys.length - 1)) ? parentTableDetails.range.endColumnIndex : rangeMap.startColumnIndex + numberOfColumns;

                        tableDetails.TOTAL_COLUMNS_WITHIN_TABLE = rangeMap.endColumnIndex - rangeMap.startColumnIndex;
                        tableDetails.NO_COLUMNS_PER_RECORD = tableDetails[0].length;
                    }
                    rangeMap.endRowIndex = lastRow;
                    endColumnOfPreviousTable = rangeMap.endColumnIndex;

                    var newRange = this._createRangeFromIndex(rangeMap);
                    this.rangeMap[sheetName][table]["standardRange"] = newRange;
                }
            }
        }
    }



    /**
    *   Get the field value for the given record of the given fieldName 
    *   Fetch lookup values if the field is of lookup type
    *   If the value if of date type, format it to extract only Day,Month and Year
    *   @param {Object} record - datastore object
    *   @param {String} fieldName - SF field's api name
    *   @return {String/Date} value in SF of the given field
    */
    gsheet.prototype._getFormattedValue = function (record, fieldName) {
        var value = null;
        if (fieldName.includes(".")) {
            var splitName = fieldName.split(".");
            if (record[splitName[0]] !== undefined && record[splitName[0]] !== null)
                value = record[splitName[0]][splitName[1]] || null;
        }
        else if (record.fields && record.fields[fieldName] && record.fields[fieldName].type === "values") {
            value = translateValue(record, record[fieldName], fieldName);
        }
        else {
            value = record[fieldName];
        }

        if (value instanceof Date) {
            var date = value.getDate().toString();
            var month = (value.getMonth() + 1).toString();    // months are zero-indexed
            var year = value.getFullYear().toString();
            value = month + " " + date + " " + year;
        }
        return value;
    }

    /**
    *   For each sheet, create a main record fields' values update requests    
    */
    gsheet.prototype._createMainRecordRequests = function () {

        var mainObjectBatchUpdateRequest = [];

        for (var sheetName in this.mainRecordFields) {
            var fieldsList = this.mainRecordFields[sheetName];
            for (var i = 0; i < fieldsList.length; i++) {
                var fieldName = fieldsList[i];

                if (this.rangeMap[sheetName] === undefined) {
                    console.log(sheetName + "not defined in range-map");
                    break;
                }

                var rangeDetail = this.rangeMap[sheetName][fieldName];
                if (rangeDetail === undefined) {
                    console.log(fieldName + " not defined in rangeMap under " + sheetName);
                    break;
                }
                var range = rangeDetail.standardRange;
                if (this._validationFields.indexOf(fieldName) !== -1) {
                    this._setValidationRequestBody(this.mainRecord, fieldName, range);
                }

                var value = this._getFormattedValue(this.mainRecord, fieldName);

                var mainRecordValue =
                    {
                        majorDimension: "ROWS",
                        range: range,
                        values: [
                            [
                                value
                            ]
                        ]
                    };

                mainObjectBatchUpdateRequest.push(mainRecordValue);
            }
        }

        return mainObjectBatchUpdateRequest;
    }

    /**
    *   For each table in each sheet, create a update values requests for each field in the table
    */
    gsheet.prototype._createUpdateChildRowsRequest = function () {
        for (var sheetName in this.tablesMap) {
            var tableMap = this.tablesMap[sheetName];
            var childRecordsValues = [];

            var gridProperties = this._getGridSize(sheetName);
            var maxRows = gridProperties.rowCount;      // maximum number of visible rows in sheet
            // have to insert new rows it if the total rows of values to be updated exceeds the limit
            for (var childRecordName in tableMap) {
                var rowValues = []; // list of values per row

                var childRecord = tableMap[childRecordName];
                var tableName = childRecord.TABLE;
                var rowLength = this.tableRowLengthMap[sheetName][tableName]
                var rangeDetail = this.rangeMap[sheetName][childRecordName];
                var rangeMap = rangeDetail.range;
                var finalRange = rangeDetail.standardRange;
                var sheetId = rangeMap.sheetId || 0;

                var parent = [];
                parent.push(childRecord.PARENT);

                //  checking if parent is main record
                var parentRecords = [];
                parentRecords.push.apply(parentRecords, parent[0].view === undefined ? parent : parent[0].view());

                var row = 0, col = 0;
                var sourceRange = this._tableDataMap[sheetName];

                if (childRecord.HEADER === undefined) {
                    for (var i = 0; i < parentRecords.length; i++) {
                        var childRecordsList = parentRecords[i].relatedList(childRecordName).view();

                        if (childRecordsList.length === 0) {
                            var childRecordRow = [];
                            for (var j = 0; j < childRecord.length; j++) {
                                for (var k = 0; k < childRecord[j].length; k++)
                                    childRecordRow.push(null);
                                rowValues.push(childRecordRow);
                            }
                        }
                        if (childRecord.ORIENTATION === GRAND_CHILD_TABLE_ORIENTATION)    // Grand-child but having 1:1 relationship with child record according to sheet
                        {
                            childRecordsList.splice(1); // take only record
                        }
                        for (var k = 0; k < childRecordsList.length; k++) {
                            for (var j = 0; j < childRecord.length; j++) {
                                var rowRecord = childRecord[j];
                                childRecordRow = [];
                                col = 0;
                                for (var l = 0; l < rowRecord.length; l++) {
                                    if (j == 0 && l == 0 && USE_ID === true) {
                                        childRecordRow.push(childRecordsList[k]["Id"] || null);
                                        col++;
                                        continue;
                                    }
                                    var field = rowRecord[l];

                                    if (this._validationFields.indexOf(field) !== -1) {
                                        var startRowIndex = rangeMap.startRowIndex + row;
                                        var endRowIndex = startRowIndex + 1;
                                        var startColumnIndex = rangeMap.startColumnIndex + col;
                                        var endColumnIndex = startColumnIndex + 1;

                                        var objRange =
                                            {
                                                "startRowIndex": startRowIndex,
                                                "endRowIndex": endRowIndex,
                                                "startColumnIndex": startColumnIndex,
                                                "endColumnIndex": endColumnIndex,
                                                "sheetId": sheetId
                                            }
                                        var standardRange = this._createRangeFromIndex(objRange);

                                        this._setValidationRequestBody(childRecordsList[k], field, standardRange);
                                    }
                                    var value = this._getFormattedValue(childRecordsList[k], field);
                                    childRecordRow.push(value);
                                    col++;
                                }
                                rowValues.push(childRecordRow);
                                row++;
                            }
                            if (childRecord.length < rowLength) {
                                var no_of_columns = childRecord[0].length;
                                for (var z = 0; z < (rowLength - childRecord.length); z++) {
                                    var emptyList = [];
                                    for (var q = 0; q < no_of_columns; q++)
                                        emptyList.push(null);
                                    rowValues.push(emptyList);
                                }
                                row += (rowLength - childRecord.length);
                            }

                            if (k > 0) {
                                var destinationRangeMap = $.extend({}, sourceRange);
                                destinationRangeMap.startRowIndex = sourceRange.startRowIndex + (k * rowLength);
                                destinationRangeMap.endRowIndex = destinationRangeMap.startRowIndex + rowLength;

                                var copyPasteRequest =
                                    {
                                        "copyPaste":
                                            {
                                                "source": sourceRange,
                                                "destination": destinationRangeMap
                                            }
                                    };
                                this._rowCopyRequests.push(copyPasteRequest);
                            }
                        }
                    }
                }
                else if (childRecord.HEADER !== undefined) {
                    var parentChildMap = this._parentChildMap[sheetName];   // contains sorted list(based on header value) of grand-child records
                    if (parentChildMap === undefined)
                        continue;
                    for (var parent in parentChildMap) {
                        var sortedChildRecords = parentChildMap[parent];
                        var newRecord = true;
                        for (var j = 0; j < childRecord.length; j++) {
                            var rowRecord = childRecord[j];
                            var childRecordRow = [];
                            col = 0;

                            for (var key in sortedChildRecords) {
                                var sortedChildRecord = sortedChildRecords[key];
                                for (var l = 0; l < rowRecord.length; l++) {
                                    if (newRecord == true && l == 0 && USE_ID === true)   // insert ID if new Record
                                    {
                                        childRecordRow.push(sortedChildRecord["Id"] || null);
                                        col++;
                                        continue;
                                    }

                                    var field = rowRecord[l];
                                    if (this._validationFields.indexOf(field) !== -1) {
                                        var startRowIndex = rangeMap.startRowIndex + row;
                                        var endRowIndex = startRowIndex + 1;
                                        var startColumnIndex = rangeMap.startColumnIndex + col;
                                        var endColumnIndex = startColumnIndex + 1;

                                        var objRange =
                                            {
                                                "startRowIndex": startRowIndex,
                                                "endRowIndex": endRowIndex,
                                                "startColumnIndex": startColumnIndex,
                                                "endColumnIndex": endColumnIndex,
                                                "sheetId": sheetId
                                            }

                                        var standardRange = this._createRangeFromIndex(objRange);

                                        this._setValidationRequestBody(sortedChildRecord, field, standardRange);
                                    }
                                    var value = this._getFormattedValue(sortedChildRecord, field);
                                    childRecordRow.push(value);
                                    col++;
                                }
                            }
                            newRecord = false;
                            if (Object.keys(sortedChildRecords).length === 0)
                                for (var q = 0; q < rowRecord.length; q++)
                                    childRecordRow.push(null);

                            rowValues.push(childRecordRow);
                            row++;
                        }
                        if (childRecord.length < rowLength) {
                            var no_of_columns = childRecord[0].length * (Object.keys(sortedChildRecords).length === 0 ? 1 : Object.keys(sortedChildRecords).length);
                            for (var z = 0; z < (rowLength - childRecord.length); z++) {
                                var emptyList = [];
                                for (var q = 0; q < no_of_columns; q++)
                                    emptyList.push(null);
                                rowValues.push(emptyList);
                            }
                            row += (rowLength - childRecord.length);
                        }
                    }
                }

                if (rowValues.length > rangeMap.endRowIndex - rangeMap.startRowIndex)    // if exceeded initial expectation of number of rows per table
                {

                    var tableRangeDetail = this.tableMappedWithNamedRangeDetail[sheetName][tableName];
                    if (tableRangeDetail !== undefined) {
                        var tableRangeMap = tableRangeDetail.range;

                        var sourceRange =
                            {
                                "sheetId": tableRangeMap.sheetId || 0,
                                "endRowIndex": tableRangeMap.endRowIndex,
                                "startRowIndex": tableRangeMap.endRowIndex - 1,
                                "startColumnIndex": tableRangeMap.startColumnIndex,
                                "endColumnIndex": tableRangeMap.endColumnIndex
                            };

                        tableRangeMap.endRowIndex = rangeMap.startRowIndex + rowValues.length;

                        // insert rows if exceeds total rows count of the grid
                        if (tableRangeMap.endRowIndex >= maxRows) {
                            var insertDimensionRequest =
                                {
                                    "insertDimension":
                                        {
                                            "inheritFromBefore": "false",
                                            "range":
                                                {
                                                    "startIndex": maxRows - 1,
                                                    "endIndex": tableRangeMap.endRowIndex,
                                                    "dimension": "ROWS",
                                                    "sheetId": sheetId
                                                }
                                        }
                                };
                            this._insertDimensionRequests.push(insertDimensionRequest);
                        }

                        // cut paste border
                        var cutPasteRequest =
                            {
                                "cutPaste":
                                    {
                                        "source": sourceRange,
                                        "destination":
                                            {
                                                "sheetId": tableRangeMap.sheetId || 0,
                                                "columnIndex": tableRangeMap.startColumnIndex,
                                                "rowIndex": tableRangeMap.endRowIndex
                                            }

                                    }
                            };

                        this._cutPasteRequests.push(cutPasteRequest);

                        var standardRange = this._createRangeFromIndex(tableRangeMap);
                        this.tableMappedWithNamedRangeDetail[sheetName][tableName].range = tableRangeMap;
                        this.tableMappedWithNamedRangeDetail[sheetName][tableName].standardRange = standardRange;
                        this._modifyRangeMap();
                        finalRange = this.rangeMap[sheetName][childRecordName].standardRange;
                        NAMEDRANGE_UPDATE_REQUIRED = true;
                    }
                }
                childRecordsValues.push(this._createUpdateValuesObject(finalRange, rowValues));
            }
            this._childObjectBatchUpdateRequest.push.apply(this._childObjectBatchUpdateRequest, childRecordsValues);
        }
    }

    /**
    *   Create an update named range request for master table as well as grand-child table
    *   The master table's range will be updated both horizontally as well as vertically
    *   The grand-child's as well child's header range will also have to be updated 
    */
    gsheet.prototype._createUpdateNamedRangeRequest = function () {
        var namedRangeUpdateRequests = [];
        for (var sheetName in this.tableMappedWithNamedRangeDetail) {
            var tables = this.tableMappedWithNamedRangeDetail[sheetName];
            for (var table in tables) {
                var tableRangeDetail = tables[table];
                if (tableRangeDetail === undefined)
                    break;
                var range = tableRangeDetail.range;
                var requestBody =
                    {
                        "updateNamedRange":
                            {
                                "namedRange":
                                    {
                                        "name": tableRangeDetail.name,
                                        "namedRangeId": tableRangeDetail.id,
                                        "range": range
                                    },
                                "fields": "*"
                            }
                    };
                namedRangeUpdateRequests.push(requestBody);
            }

            var tableHeaderMap = this._sheetTablesHeaderRange[sheetName]
            if (tableHeaderMap === undefined)
                break;

            for (var table in tableHeaderMap) {
                var namedRangeDetail = tableHeaderMap[table];
                var modifiedRange = this.rangeMap[sheetName][table];
                if (modifiedRange !== undefined) {
                    var range = $.extend({}, modifiedRange.range);

                    range.startRowIndex = namedRangeDetail.range.startRowIndex;
                    range.endRowIndex = namedRangeDetail.range.endRowIndex;

                    var requestBody =
                        {
                            "updateNamedRange":
                                {
                                    "namedRange":
                                        {
                                            "name": namedRangeDetail.name,
                                            "namedRangeId": namedRangeDetail.namedRangeId,
                                            "range": range
                                        },
                                    "fields": "*"
                                }
                        };
                    namedRangeUpdateRequests.push(requestBody);
                }
            }
        }
        return namedRangeUpdateRequests;
    }

    /**
    *   Create an object of the format that matches Google Sheet's values.batchUpdate request body
    *   @param {range} - range(A1 notation) over which the data is to be updated
    *   @param {valueList} - list of values to be populated in the sheet
    *   @return {Object} values.batchUpdate request body
    */
    gsheet.prototype._createUpdateValuesObject = function (range, valueList) {
        var childRecordValue = {
            majorDimension: "ROWS",
            range: range,
            values: valueList
        };
        return childRecordValue;
    }

    /**
    *   Create a single list that contains child table's values update request as well as grand-child header values update request
    *   return {Array} single consolidated list of update requests
    */
    gsheet.prototype._consolidateAllRequestLists = function () {
        var consolidatedList = [];
        this._createUpdateChildRowsRequest();

        if (this._childObjectBatchUpdateRequest.length > 0)
            consolidatedList.push(this._childObjectBatchUpdateRequest);
        if (this._headerValuesBatchUpdateRequest.length > 0)
            consolidatedList.push(this._headerValuesBatchUpdateRequest);
        return consolidatedList;
    }



    /**
    *   Check to see if the given field under the given parent-table is in white-list
    *   @param {String} parent - name of the parent-table ; empty string("") for main record field 
    *   @param {String} field - field name (api name)
    *   @return {Boolean} True if field is in white-list, False otherwise
    */
    gsheet.prototype._isFieldInWhiteList = function (parent, field) {
        if (parent === '')    // main record field
        {
            if (field in this.updateFields)
                return true;
        }
        var detail = this.updateFields[parent];
        if (detail !== undefined) {
            var whiteListFields = detail.fields;
            if (whiteListFields.indexOf(field) !== -1)
                return true;
        }
        return false;
    }

    /**
    *   Set validation map for all the fields in the sheet
    *   @return {Promise} Promise to create a validation map for all the fields in the sheet
    */
    gsheet.prototype._setValidationMap = function () {
        /*
            We have to make an asynchronous remote call to SF to fetch the list of valid values for "reference" type fields
            Hence, 'promise' is to be used!!
        */
        var promiseList = [],
            mockRecordMap = {};

        if (!VALIDATION_ENABLED)
            return Promise.all(promiseList);

        /*  First processing main record fields
        */
        var parent = this.mainRecord;
        for (var sheetName in this.mainRecordFields) {
            var fieldsList = this.mainRecordFields[sheetName];
            for (var i = 0; i < fieldsList.length; i++) {
                var field = fieldsList[i];
                promiseList.push(this._getAndSetValidValues(this.mainRecord.Name, parent, (this.updateFields[field] === undefined) ? field : this.updateFields[field], field));
            }
        }

        /*  Processing tabular fields
        */
        for (var sheetName in this.tablesMap) {
            var tableMap = this.tablesMap[sheetName];
            for (var record in tableMap) {
                var mockParent = false;

                var recordDetails = tableMap[record];
                var recordParent = recordDetails.PARENT;

                var parentList = [];

                if (recordParent.view === undefined)      //  meaning single-parent, which implies parent is the main record
                    parentList.push(recordParent);
                else
                    parentList = recordParent.view();   //  all parent records for 'grand-child'
                if (parentList.length == 0) {
                    // add a mock record                                   
                    parentList.push(this._getMockRecord(this.mainRecord, recordDetails.TABLE, false, mockRecordMap));
                    mockParent = true;
                }

                for (var i = 0; i < parentList.length; i++) {
                    var parentRecord = parentList[i];
                    var childRecordsList = parentRecord.relatedList(record).view();

                    if (childRecordsList.length == 0) {
                        // add a mock record                                                                    
                        childRecordsList.push(this._getMockRecord(parentRecord, record, mockParent, mockRecordMap));
                    }
                    for (var s = 0; s < childRecordsList.length; s++) {
                        var childRecord = childRecordsList[s];
                        for (var j = 0; j < recordDetails.length; j++) {
                            var fieldList = recordDetails[j];
                            for (var k = 0; k < fieldList.length; k++) {
                                var field = fieldList[k];
                                if (field !== "")
                                    promiseList.push(this._getAndSetValidValues(record, childRecord, field, field));
                            }
                        }
                    }
                }
            }
        }

        return Promise.all(promiseList).then(function (d) {
            // remove mock records, if any
            if (Object.keys(mockRecordMap).length > 0) {

                // converting map to list
                var mockRecordsList = Object.keys(mockRecordMap).map(function (key) { return mockRecordMap[key]; }),
                    index = 0,
                    numberOfRecords = mockRecordsList.length;
                while (index < mockRecordsList.length) {
                    var mockRecords = mockRecordsList[index];
                    if (mockRecords.isMock && index < numberOfRecords - 1) {
                        // push the record to the end since its a parent mock record
                        var recordToBeShifted = mockRecordsList[index];
                        mockRecordsList.push(recordToBeShifted);
                    } else {
                        var parentRecord = mockRecords.Parent;
                        for (var s = 0; s < mockRecords.length; s++) {
                            var mockRecordDetails = mockRecords[s],
                                mockRecord = mockRecordDetails.Record,
                                mockRecordObject = mockRecordDetails.Object;
                            parentRecord.relatedList(mockRecordObject).remove(mockRecord);
                        }
                    }
                    index++;
                }
            }

        });
    }

    /**
    *   Returns a temporary record with required fields of the given object set to 'MOCK'
    *   @param {Object} parent - parent object 
    *   @param {Object} object - object whose mock instance is to be created
    *   @param {Boolean} isMock - specifies if the parent record is a mock record too
    *   @param {Object} mockRecordMap - store mock record details
    *   @return {Object} mock record
    */
    gsheet.prototype._getMockRecord = function (parent, object, isMock, mockRecordMap) {
        // create mock record
        var mockRecord = parent.relatedList(object).add();

        // get required fields of the object and add them to the mockrecord
        var fields = mockRecord.fields;
        for (var field in fields) {
            var fieldDetails = fields[field];
            var validationRule = fieldDetails.validation || {};
            if (Object.keys(validationRule).length > 0) {
                var requiredField = (validationRule.required !== undefined) ? validationRule.required : false;
                if (requiredField) {
                    mockRecord[requiredField] = 'MOCK';
                }
            }
        }

        // store the mock record details to remove it once validation is done                       
        var mockRecordList = mockRecordMap[parent.Id] || [],
            recordMap = {
                'Record': mockRecord,
                'Object': object
            };
        mockRecordList.push(recordMap);
        mockRecordList['isMock'] = isMock;
        mockRecordList['Parent'] = parent;

        mockRecordMap[parent.Id] = mockRecordList;

        return mockRecord;
    }

    /**
    *   Get a list of valid values for the given field and set into the validation map
    *   @param {String} parentName - name of the parent-table ; empty string("") for main record field 
    *   @param {Object} parent - datastore of parent record
    *   @param {String} field - alias name of field from the white-list
    *   @param {String} fieldName - actual field name
    *   @return {Promise} Promise to create a validation map for the given field
    */
    gsheet.prototype._getAndSetValidValues = function (parentName, parent, field, fieldName) {
        var that = this;
        return new Promise(function (resolve, reject) {

            var field_trimmed = that._trimReference(field);         //  Removing '.<fieldName>' and Replacing '__r' with '__c' if the field is of 'lookup' type
            var fieldInfo = parent.fields[field_trimmed];

            if (fieldInfo === undefined) {
                resolve("No such field!!");
                return;
            }

            var fieldType = fieldInfo.type;

            if (fieldType === "values")       //  picklist
            {
                var addInfo = fieldInfo.additionalInfo,
                    cf = addInfo.controllingField,
                    validValuesRecords = [];

                if (cf) {
                    var cfValue = parent[cf];
                    // list valid values for the given picklist field for given controlling field value
                    if (cfValue != null && addInfo.validValues) {
                        validValuesRecords = addInfo.validValues.filter(function (val) { return val.validFor && val.validFor.includes(cfValue); });
                    }
                }
                else {
                    // list all valid values for the given picklist field when no controlling field is present
                    validValuesRecords = addInfo.validValues;
                }
                validValuesRecords.TYPE = "values";

                var mapWithField = that.validationValuesMap[parent.Id] || {};
                mapWithField[fieldName] = validValuesRecords;

                that.validationValuesMap[parent.Id] = mapWithField;                 // mapping with parent id because, some field's values change per record

                //  add to validation fields list only if it's not already present there and it is in the white-list
                if (that._validationFields.indexOf(fieldName) === -1 && that._isFieldInWhiteList((parentName === that.mainRecord.Name ? '' : parentName), field))
                    that._validationFields.push(fieldName);

                resolve("DONE");
            }
            else if (fieldType === "reference")   // lookup
            {
                var referencedField_Name = fieldInfo.additionalInfo.dependsOn;
                var referencedField_Id = parent[referencedField_Name] || '';
                var sourceField = field_trimmed + "_Source";        // appending _Source to fetch the source details of the given reference field

                var ds = $.isFunction(parent[sourceField]) ? parent[sourceField]() : parent[sourceField];

                if (ds === null) {
                    resolve("DONE");
                    return;
                }

                // binder that is called if a change is made to the datastore 'ds'
                ds.bind('change', function (data) {

                    ds.unbind('change');
                    var values = data.items;

                    var listOfValues = [];
                    for (var i = 0; i < values.length; i++) {
                        var name = values[i].Name;
                        var mapping = {};
                        mapping[name] =
                            {
                                "Name": values[i].Name,
                                "Id": values[i].Id
                            };
                        if (referencedField_Name !== undefined)
                            mapping[name][referencedField_Name] = values[i][referencedField_Name] || '';
                        listOfValues.push(mapping);
                    }

                    var mapWithField = that._lookup_AllValuesMap[parentName] || {};
                    mapWithField[fieldName] = listOfValues;

                    if (parent !== that.mainRecord)   // no need to create a lookupvaluesmap for main record fields as they have only one parent
                    {
                        that._lookup_AllValuesMap[parentName] = mapWithField;
                    }

                    if (!that._isFieldInWhiteList((parentName === that.mainRecord.Name ? '' : parentName), field)) {
                        resolve("DONE");
                        return;
                    }

                    var validValues = data.items;

                    if (referencedField_Name !== undefined) {
                        // filtering validValues list to extract only dependent field specific values                                        
                        var dependsOn_Value = parent[referencedField_Name];
                        validValues = validValues.filter(function (a) { return a[fieldInfo.additionalInfo.dependsOnField] === dependsOn_Value });
                    }

                    var validValuesMapList = [];

                    for (var i = 0; i < validValues.length; i++) {
                        var v_map =
                            {
                                "Name": validValues[i].Name,
                                "Id": validValues[i].Id
                            };
                        if (referencedField_Name !== undefined)
                            v_map[referencedField_Name] = referencedField_Id;
                        validValuesMapList.push(v_map);
                    }
                    validValuesMapList.TYPE = "reference";

                    mapWithField = that.validationValuesMap[parent.Id] || {};
                    mapWithField[fieldName] = validValuesMapList;

                    that.validationValuesMap[parent.Id] = mapWithField;

                    if (that._validationFields.indexOf(fieldName) === -1)
                        that._validationFields.push(fieldName);

                    resolve("DONE");

                });

                ds.read();
            }
            else {
                resolve("DONE");
            }
        });
    }

    /**
    *   Converts reference type fieldname(ending with __r.<fieldname>) to non-reference type fieldname(ending with __c)
    *   @param {String} field - field name to be trimmed
    *   @return {String} trimmed name ending with '__c'
    */
    gsheet.prototype._trimReference = function (field) {
        if (field.match(/.*\..*$/)) {
            field = field.split(".")[0];
            field = field.replace("__r", "__c");
        }
        return field;
    }

    /**
    *   Add validation request for the given field at the given range
    *   @param {Object} parent - datastore of parent record
    *   @param {String} field - field name (api name)
    *   @param {String} range - Standard range in A1 notation
    */
    gsheet.prototype._setValidationRequestBody = function (parent, field, range) {
        var rangeMap = this._getRangeMapFromStandardRange(range);
        var valueList = [];
        var parentId = parent.Id;
        if (parentId in this.validationValuesMap) {
            if (this.validationValuesMap[parentId] === undefined)
                return;
            var validValuesList = this.validationValuesMap[parentId][field];

            if (validValuesList === undefined)
                return;

            var extractionField = "";

            if (validValuesList.TYPE === "values")
                extractionField = "text";
            else if (validValuesList.TYPE === "reference")
                extractionField = "Name";

            for (var i = 0; i < validValuesList.length; i++) {
                var value = validValuesList[i][extractionField];
                var jsonFormattedValue =
                    {
                        "userEnteredValue": value
                    }
                valueList.push(jsonFormattedValue);
            }
        }
        this._createValidationRequest(valueList, rangeMap);
    }

    /**
    *   Create a validation request object(object pattern matching with Google Sheet api) and add to validation request list
    *   @param {Array} values - list of values
    *   @param {Object} range - range map
    */
    gsheet.prototype._createValidationRequest = function (values, range) {
        var dataValidation =
            {
                "setDataValidation":
                    {
                        "range": range,
                        "rule":
                            {
                                "condition":
                                    {
                                        "type": "ONE_OF_LIST",
                                        "values": values
                                    },
                                "showCustomUi": true,
                                "strict": true
                            }
                    }
            }

        this._validationRequests.push(dataValidation);
    }


    // UTILITY FUNCTIONS BELOW

    /**
    *   Convert column index to column name (ex : 1 -> A, 6 -> F)
    *   @param {Number} index - column index
    *   @returns {String} corresponding column name  
    */
    gsheet.prototype._getColumnNameFromColumnIndex = function (index) {
        var name = "";

        while (index > 0) {
            var rem = (index - 1) % 26;
            name = String.fromCharCode("A".charCodeAt(0) + rem) + name;
            index = parseInt((index - rem) / 26);
        }

        return name;
    }

    /**
    *   Convert column name to column index (ex : A -> 0 , F -> 5)
    *   @param {String} columnName - name of the column
    *   @returns {Number} corresponding column index  
    */
    gsheet.prototype._getColumnIndexFromColumnName = function (columnName) {
        var alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        var index = 0;
        for (var i = 0, j = columnName.length - 1; i < columnName.length; i++ , j -= 1) {
            index += Math.pow(alphabets.length, j) * (alphabets.indexOf(columnName[i]) + 1);
        }
        return (index - 1);
    }

    /**
    * gives grid properties of the corresponding sheet
    * @params{String} sheetName - name of the sheet
    * @returns {Object} null if sheet not found else returns grid properties of the corresponding sheet
    */
    gsheet.prototype._getGridSize = function (sheetName) {
        var sheets = this._currentSheet.sheets;
        for (var i = 0; i < sheets.length; i++) {
            var properties = sheets[i].properties;
            var name = properties.title;

            if (sheetName === name)
                return properties.gridProperties
        }
        return null;
    }

    /**
    *   Get sheet name (from Google Sheet) of the given sheetId
    *   @param {Number} sheetId - id of the sheet
    *   @returns {String} corresponding sheet name  
    */
    gsheet.prototype._getSheetName = function (sheetId) {
        var sheets = this._currentSheet.sheets;

        for (var i = 0; i < sheets.length; i++) {
            var properties = sheets[i].properties;
            var id = properties.sheetId;

            if ((sheetId === 0 && id === undefined) || sheetId === id)
                return properties.title;
        }
        return null;
    }

    /**
    *   Get sheet id (from Google Sheet) of the given sheetName
    *   @param {String} sheetName - name of the sheet
    *   @returns {Number} corresponding sheet id 
    */
    gsheet.prototype._getSheetId = function (sheetName) {
        var sheets = this._currentSheet.sheets;

        for (var i = 0; i < sheets.length; i++) {
            var properties = sheets[i].properties;
            var name = properties.title;

            if (name === sheetName)
                return properties.sheetId;
        }
        return null;
    }

    /**
    *   Create range object(Google sheet format) from standard range(A1 notation)
    *   @param {String} standardRange - range in A1 notation
    *   @return {Object} range object corresponding to given standard range
    */
    gsheet.prototype._getRangeMapFromStandardRange = function (standardRange) {
        var sheetName = standardRange.split("!")[0];
        var sheetId = this._getSheetId(sheetName);

        standardRange = standardRange.split("!")[1];
        var startingRow = parseInt(standardRange.split(/^[A-Z]+|:/)[1]);
        var startingColumn = standardRange.split(/[0-9]+:/)[0];
        var endingRow = parseInt(standardRange.split(/.*:[A-Z]+/)[1]);
        var endingColumn = standardRange.split(/.*:|[0-9]+$/)[1];

        var startColumnIndex = this._getColumnIndexFromColumnName(startingColumn);
        var endColumnIndex = this._getColumnIndexFromColumnName(endingColumn);

        var rangeMap =
            {
                "sheetId": sheetId,
                "startRowIndex": startingRow - 1,      // -1 because row indexing starts with 0
                "endRowIndex": endingRow,              // no -1 here because endRowIndex is exclusive in the range
                "startColumnIndex": startColumnIndex,
                "endColumnIndex": endColumnIndex + 1   // +1 because column indexing starts with 0
            }
        return rangeMap;
    }

    /**
    *   Create a standard range (A1 notation) using range indices
    *   @param {Object} rangeMap - Google Sheet format based range object
    *   @return {String} standard range in A1 notation for the given range
    */
    gsheet.prototype._createRangeFromIndex = function (rangeMap) {
        var startColumnName = this._getColumnNameFromColumnIndex(rangeMap.startColumnIndex + 1);  // +1 because of 0 based indexing
        var endColumnName = this._getColumnNameFromColumnIndex(rangeMap.endColumnIndex);

        var startCell = startColumnName + (rangeMap.startRowIndex + 1);
        var endCell = endColumnName + rangeMap.endRowIndex;

        var sheetId = rangeMap.sheetId || 0;
        var sheetName = this._getSheetName(sheetId);
        var standardRange = sheetName + "!" + startCell + ":" + endCell;

        return standardRange;
    }

    sqx.spreadsheet = sqx.spreadsheet || {};
    sqx.spreadsheet.GSheet = gsheet;

    return sqx;
}(sqx || {}, jQuery));