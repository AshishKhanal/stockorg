(function($, k, sqx){
    /**
    * Creates a kendo based file uploader capable of uploading using Salesforce Chatter REST API
    * @param element the DOM element that must be wrapped with uploader
    * @param sessionId the session id of current session, this will be passed to the server to upload file
    * @param currentRef the value of current reference value. if this value is null it will allow insertion else it will update existing file
    * @param otherOptions other options that are to be passed the kendo uploader
    * @return returns the file uploader widget
    */
    sqx.createFileUploader = function(element, sessionId, currentRef, otherOptions)
    {
        var UPLOAD_PATH = '/services/data/v36.0/connect/files/users/me',
            UPDATE_DELETE_PATH = '/services/data/v36.0/connect/files/{FILEID}',
            options,
            otherOptionsStartCallBack = otherOptions.start || null;

        // upload must be used to send the session id, else upload won't work
        function onUpload(e) {
            var xhr = e.XMLHttpRequest;
            if (xhr) {
                xhr.addEventListener("readystatechange", function (e) {
                    if (xhr.readyState == 1 /* OPENED */) {
                        xhr.setRequestHeader("Authorization", "OAuth " + sessionId);

                        if(otherOptionsStartCallBack){
                            otherOptionsStartCallBack.call(null, e);
                        }
                    }
                });
            }
        }

        options = {
            async : {
                saveUrl : currentRef.length == 0 ? UPLOAD_PATH : UPDATE_DELETE_PATH.replace('{FILEID}', currentRef),
                // removeUrl : UPDATE_DELETE_PATH, //note: we aren't supporting delete for now, this behaviour might change someday
                // removeVerb: 'DELETE',
                saveField: 'fileData' //this is the name of rest api's file component's name
            },
            multiple: false,
            localization : {
                select : currentRef.length == 0 ? cq.labels.docUpload.uploadNewFile : cq.labels.docUpload.uploadNewVersion
            },
            upload : onUpload,
            select : function(e){
                var hasZeroSizeFile = false;

                for(var i = 0; i < e.files.length; i++){
                    hasZeroSizeFile = hasZeroSizeFile || e.files[i].size == 0 || (e.files[i].rawFile && e.files[i].rawFile.size == 0);
                }

                if(hasZeroSizeFile){
                    e.preventDefault();
                    alert(cq.labels.docUpload.emptyFileUpload)
                }
            }
        };

        $.extend(options, otherOptions || {});

        element.kendoUpload(options);

        var uploader = element.data('kendoUpload');

        uploader.bind('error', function(e){
            var msg = cq.labels.docUpload.errorInUploading + ' ' + e.files[0].name + '.';

            try{
                msg += cq.labels.docUpload.actualError + ' ' + JSON.parse(e.XMLHttpRequest.responseText)[0].message;
            }
            catch(ex){
                //ignore parsing error
            }

            alert(msg);

        });

        uploader.wrapper.removeClass('k-upload-empty');

        return uploader;
    }
})(jQuery, kendo, sqx);