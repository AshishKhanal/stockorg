/*
 * Removes attachment
 */
function removeAttachment(event) {
    $("#loadingBlock").show();
    var row = $(event.target).closest("tr");
    var grid = row.closest("[data-role=grid]").data("kendoGrid");
    var dataItem = grid.dataItem(row);

    SQX_Extension_UI.removeAttachment(changesRecorder.primaryId, dataItem.ParentId, dataItem.Id, function (e, s) {
        if (e == '+OK') {
            dataStore.getAttachments().remove(dataItem);
            changesRecorder.isChanged = true; //forces save
            changesRecorder.syncNow(true, $("#loadingBlock"));
        }
        else {
            $("#loadingBlock").hide();
        }
    });
}

/*
 * returns attachment template
 */
function getAttachmentTemplate(data) {

    return kendo.template($("#AttachmentEditTemplateChild").html())({}).replace(/\"/g, "\\\"").replace(/\n/g, " ");
}

function attachmentSaveCalled(event) {
    var frameId = 'Attachment' + event.model.Id;
    var frame = document.getElementById(frameId);


    if (frame != null) {
        //if files length is greater then 0 that means it has been uploaded
        if (frame.contentWindow.files.length == 0 || frame.contentWindow.fileSelected == true || frame.contentWindow.descriptionChanged == true) {
            if (!frame.contentWindow.validateContent()) {
                event.preventDefault();
            }
            else {

                //post back main frame
                event.preventDefault();
                frame.contentWindow.startUpload();
                //show loading

            }
        }
    }
}

function attachmentCallBack(event, recordid, data) {
    var attachment = sqx.dataStore.getObjectWithIdFor('Attachment', recordid);
    if (attachment == null) {
        sqx.common.error('Null attachment received ', recordid, event, data);
        return;
    }

    if (event == 'ItemChange') {

        for (var index = 0, length = data.length; index < length; index++) {
            attachment.set(data[index].field, data[index].value);
        }

    }
    else if (event == 'In Progress') {
        $("#loadingBlock").show()
        sqx.common.debugLog('Uploading attachment');
    }
    else if (event == 'Completed') {
        var frameId = '#Attachment' + attachment.Id;

        attachment.ParentId = data.ParentId;
        attachment.Id = data.Id;
        attachment.Name = data.Name;
        attachment.IsNewAttachment = true;

        $(frameId).parents('[data-role="window"]').find('.k-grid-update').trigger('click');
        attachment.trigger('change');
        $("#loadingBlock").hide()
    }
    else {
        //an error occurred 
        $("#loadingBlock").hide()
        sqx.common.error('Unknow error occurred')
    }
}

function buildAttachmentViewLink(dataItem) {
    return "<a href='" + sqx.common.getFileDownloadUrl(dataItem.Id) + "' target='_blank'>" + kendo.htmlEncode(dataItem.Name) + "</a>";
    
}