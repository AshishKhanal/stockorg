// JavaScript source code
var mainRecord = mainRecord || {}, formRules = formRules || {}, queryString = {};
var commandRules = kendo.observable({
    enablePrev: false, isDirty: false, enableNext: true,
    navigateTab: function (e) { navigateTab(e)},
    isNewObject: function () { return mainRecord.isNewObject() },
    isExistingObject: function () { return mainRecord.isExistingObject() },
    enableSave: function () { return mainRecord.canEdit() && (this.isNewObject() || this.get('isDirty') == true )},
    enableCancel: function () {return this.enableSave() || getUrlParameter('editMode')=='true'}//enable when save is activated or is in edit mode
});
String.prototype.endsWith = function (substr) { return this.length < substr.length ? false : (this.indexOf(substr) == this.length - substr.length) };

/**
* fix for not supporting 'startsWith' in IE
*/
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

/*
 * returns Parent's Id 
 */
function getParentID(parentModel, mainrecord) {
    //if (parentModel.isExistingObject())
    //    return parentModel.Id + '&attachtoparent';


    //if (mainRecord.isExistingObject())
        return mainrecord.Id;

    //return mainRecord.attributes.type + mainrecord.Id;
}

$(document).ready(function () {

    // allows form rules to be removed from the customer page customization
    var removeFormRules = window.removeFormRules || undefined;
        customFormRules = window.customFormRules || undefined;

    if(removeFormRules){
        for(j = 0; j < removeFormRules.length; j++){
            var formRulesToBeRemoved = removeFormRules[j];
            removeARuleFromFormRules(formRulesToBeRemoved.objectName, formRulesToBeRemoved.fieldName, formRulesToBeRemoved.conditionName);
        }
    }

    //allow injection of customer form rules if any this should be the last one because
    //customFormRules then take precedence over our formrules
    if(formRules && customFormRules){
      for(var i=0; i<customFormRules.length; i++)
        formRules.push(customFormRules[i]);
    }

    var cq = window.cq || {};
    try {
        cq.initialLoad = true;
        if($("#changeSetDataJSON").length > 0){
            var changeSet = $.parseJSON($("<div/>").html($("#changeSetDataJSON").text()).text());
            changeSet = changeSet.changeSet ? changeSet.changeSet : changeSet; //server understands { changeSet : [] }
            //client understands { modified : [] }
            //so there is an issue 
            sqx.dataStore.initialize($.parseJSON($("<div/>").html($("#completeDataJSON").text()).text()),
                                    SQXSchema,
                                    { recordChanges: true },
                                    { modified: changeSet });  

        }
    } finally {
        cq.initialLoad = false;
    }

    /**
     * function to remove the rule from the object of the formrules
     * @param objectName name of the object from which rule is to be deleted
     * @param fieldName name of the field  whose rule is to be deleted
     * @param conditionName condition which is to be deleted
     */
    function removeARuleFromFormRules(objectName, fieldName, conditionName){
        for(var i = 0; i < formRules.length; i++){
            var objectFormRule = formRules[i];
            if(objectFormRule.objectName == objectName){
                var objectRule = objectFormRule.rule;
                if(objectRule.length != 0){
                    for(var j = 0; j < objectRule.length; j++){
                        var ruleDetail = objectRule[j],
                            fieldToBeCompared = ruleDetail.field,
                            conditionToBeCompared = ruleDetail.condition;
                        if(fieldToBeCompared === fieldName && fieldName != undefined &&
                            conditionToBeCompared === conditionName && conditionName != undefined){
                            formRules[i].rule.splice(j,1);
                            break;
                        }
                    }
                }
            }
        }
    }

    if(window.initialize !== undefined){
        var events = sqx.Events;
        events.trigger(events.PREINIT);

        window.initialize();

        events.trigger(events.POSTINIT);
    }


    $("#loadingBlock").hide();
    $('#errorDiv').click(clearFormError)
    $("#mainbody").css('visibility', 'visible');
    $(".cqCommandButton[data-action]").click(commandButtonHandler);

    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if(commandRules){
            if (commandRules.isDirty==true ||(sqx.autosave && sqx.dataStore && !sqx.autosave.isInSync(sqx.dataStore.getChangeSet()))) {
                return 'You have made changes. Would you like to exit without saving ?';
            }
        }
    }
});

function commandButtonHandler(e) {
    var btn = $(e.currentTarget);
    processChangeSet({'nextAction' : btn.attr('data-action') });//making the current handler work with remote action changes
}

function setActionButtons(e) {
    var currTab = $(e.sender.select());
    commandRules.set('enablePrev',  !currTab.is(".k-first"));
    commandRules.set('enableNext',  !currTab.is(".k-last"));
}

function cancelChanges(){
    if(sqx.common.isSFID(mainRecord.Id))
    {
        navigateToSObject(mainRecord.Id);
    }
    else
    {
        if(queryString.retURL){
            navigateToURL(queryString.retURL);
        }
        else{
            if ((typeof sforce != 'undefined') && (sforce != null)) {
                sforce.one.back();
            }else{
                navigateToURL(window.location);
            }
        }
    }
}

// if the summary is present it will parse the text greater than 100 into the length of 100
// else if summary is absent it will return empty string
function chompedSummary(summary){
    if(summary){
        var summaryText= $($.parseHTML(summary)).text() || summary;
        return summaryText.length > 100 ? summaryText.substring(0, 100) : summaryText;
    }

    return '';
}

/*
 *@description: arrange the popup size checking if it is mobile or PC*/
function popupPadding(){
    var padding = 100;

    if(sqx.common.isMobile()){
        padding = 5;  
    } 

    return padding;           
}

/*
 * @description: get the data-item during approval*/

function getDataItemForCommand(event) {
    var data, dataId, row, grid, target;

    jqTarget = $(event.currentTarget);
    dataId = jqTarget.attr('data-item-id');

    if (dataId == null) {
        row = jqTarget.parents('tr');
        grid = jqTarget.parents('.cq-topGrid').data('kendoCQGrid');
        data = grid.dataItem(row);
    } else {
        data = sqx.dataStore.getObjectWithId(dataId);
    }
    return data;
}

/**
 * this function will return the checkbox icon 
 */
function getCheckBox(isChecked) {
    if (isChecked == undefined || isChecked == false)
        return '<div style="width:100%; text-align: center;"><img src="../img/checkbox_unchecked.gif" alt="Not Checked"/></div>';
    return '<div style="width:100%; text-align: center;"><img src="../img/checkbox_checked.gif" alt="Checked"/></div>';
}

/**
 * this function will evaluate the form rules and validate the form rules and if validation fails it throws error
 */
function evaluateFormRules(ruleBase, fieldName, validationMsgAttribute, input) {
    if (input.is("[name='" + fieldName + "']")) {
        var container = $(input.parents('.k-window-content')).eq(0);

        for (var rule in ruleBase[fieldName]) {
            var ruleResult = ruleBase[fieldName][rule](container, input);

            if (ruleResult.status === false) {
                input.attr(validationMsgAttribute, ruleResult.errormsg);
                return false;
            }
        }

        return true;
    }

    return true;
}
        
/**
 * this fucntion will enable the attachment if the record is not locked
 */
function enableAttachment(data){
    var parent= sqx.dataStore.getObjectWithId(data.ParentId);
    if(parent && parent.isLocked!=undefined && parent.isLocked())
        return false;
    return true;
}

/**
* Common schema correction function that adds Is<PickListValue> to the schema object.
* Example: If Audit has status Draft, In Approval and Published
* 3 functions are added to audit schema called
*  isDraft(), isPlan_Approval(), isPublished()
* [Note: the spaces in the status field has been replaced  by an underscore(_) character and dash(-) with a dollar($) character]
* @param schema the schema where the functions are to be added
* @param fieldName the fieldName whose valid values functions are to be added. ex Status__c
* @param prefix the prefix to add after 'is' i.e. changes is<Status> to is<Prefix><Status>
*
*/
function _pickListValuesToFunction(schema, fieldName, prefix){

    if(window.console && window.console.warn){
        window.console.warn('Use of depecrated function. Please use the newer configurations object sqx.SchemaConfiguration');
    }

    prefix = prefix || ''; //default no prefix

    //this function will return the actual function that will match the objects field value with picklist value
    //any match will then return a TRUE value.
    function checkStatus(valueToMatch){
        return function(){
            return this[fieldName] == valueToMatch;
        }
    }

    //get the list of valid values for the picklist item
    var validValues = schema.fields[fieldName].additionalInfo.validValues,
        status, funcName;

    //loop through all valid status values and create a function called is<****> that checks if the item has the status
    for (var i = 0, length = validValues.length; i < length; i++) {
        status = validValues[i].value;
        funcName = 'is' + prefix + status.replace(' ', '_');
        funcName = funcName.replace('-', '$');
        schema[funcName] = checkStatus(status); //add the function to schema
    };
}

/**
* This function sets the 'setNullOnDelete' in specified child relationships which prevents delete from cascading to objects
* where such enforcement is not desired
* @param childRelationships the array of relationships which is to be modified.
* @param inRelationships list of relations in the relationship where the 'setNullOnDelete' is to be set. 
*/
function cq_setNullOnDeletionFor(childRelationships, inRelationships){
    var setNullOn = {}, index, length;

    inRelationships = inRelationships || [];

    //since the relationships are unique we can create a map so that we can use the map to search easily instead of iterating over a list
    for(index = 0; index < inRelationships.length; index++){
        setNullOn[inRelationships[index]] = true;
    }

    childRelationships.forEach(function(relationship, relIndex){
        relationship.setNullOnDelete = setNullOn[relationship.relationshipName]; //sets null on delete to true if in the map, else to undefined which works as false.
    });
}

/**
* This function translate the picklist value in UI
* @param dataItem object data
* @param fieldValue picklist value
* @param fieldName which field to be translated
*/
function translateValue(dataItem,fieldValue,fieldName){
    var validValues= dataItem.fields[fieldName].additionalInfo.validValues;
    var translatedValue = fieldValue;
    for (var i = 0, length = validValues.length; i < length; i++) {
        if(validValues[i].value == fieldValue){
            translatedValue = validValues[i].text;
        }
    }
    return translatedValue || '';
}