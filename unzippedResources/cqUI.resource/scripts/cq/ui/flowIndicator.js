//custom widget for flow control 
(function($, kendo){
    "use strict";
    // Help: shorten references to variables. this is better for uglification 
    var ui = kendo.ui,
        Widget = ui.Widget,
        DATABINDING = "dataBinding",
        DATABOUND = "dataBound",
        CHANGE = "change",
        PREFIX = 'cq-';

    var flowIndicator = Widget.extend({
        init: function(element, options){
            
            Widget.fn.init.call(this, element);
            this._create(element, options);
        },

        options: {
            name: 'CQFlowIndicator',
            orderedList: [], //list of steps in the indicator
            showOnTop: {} //list of elements to be displayed on the top
        },

        events : [DATABINDING,DATABOUND,CHANGE],

        _create : function(element, options){

            var container = $("<table ></table>"),
                bottomRow = $("<tr></tr>"),
                topRow = $("<tr></tr>"),
                idx, length,
                paramCell,emptyCell,
                element = $(element),
                currentItem, colItem,
                size = 0,
                lastIndex = 0;


            topRow.appendTo(container);
            bottomRow.appendTo(container);
            container.appendTo(element);
            element.addClass('cqFlowIndicator');

            //convert list of show on top to map for quicker access
            if(options.showOnTop instanceof Array){
                var mappedOptions = {};
                for(idx = 0, length = options.showOnTop.length; idx < length; idx++){
                    mappedOptions[options.showOnTop[idx]] = true;
                }

                options.showOnTop = mappedOptions;
            }


            for(idx = 0, length = options.orderedList.length, lastIndex = length - 1; idx < length; idx++){


                currentItem = options.orderedList[idx];



                paramCell = $("<td></td>");

                //if is last item render as icon and text instead of just text
                if(idx == lastIndex){
                    paramCell.html('<div class="icon"></div><div class="text">' + currentItem.text || currentItem + '</div>');
                }
                else{
                    paramCell.text(currentItem.text || currentItem);
                }
                paramCell.attr('data-bind', 'attr: { class: state__' + (currentItem.name || currentItem) + ' }');

                emptyCell = $("<td></td>");
                emptyCell.addClass('cqFlowStep');
                emptyCell.addClass('empty');

                if(options.showOnTop[currentItem.name || currentItem]){
                    paramCell.appendTo(topRow);
                    emptyCell.appendTo(bottomRow); //empty column in the bottom row
                    emptyCell.addClass('bottom');
                }
                else{
                    paramCell.appendTo(bottomRow);
                    emptyCell.appendTo(topRow); //empty column in top row
                    emptyCell.addClass('top');
                }
            }
            //some maths
            //orderedList.length is the number of rows needed

            //max(showOnTop, showOnBottom) is the number of columns needed 
        }
    });

    // add the widget to the ui namespace so it's available
    ui.plugin(flowIndicator);
})(jQuery, kendo);