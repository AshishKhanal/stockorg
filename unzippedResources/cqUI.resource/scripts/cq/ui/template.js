//used by grid to find field editor
function getEditTemplate(container, options) {
    var field = options.model.fields[options.field];
    if (field.additionalInfo.editorField) field = options.model.fields[field.additionalInfo.editorField];//works with fields of same scope only!
    var editor = $('<div/>');
    editor.kendocqCompositeField($.extend(true, {}, {
        schemaObjectField: field, render: { Label: false }, editMode: true
    }, options));
    editor.appendTo(container);
}

window.cq = (function(cq, schemaContainer, translator) {
    "use strict";
    var CONTENT_TITLE = 'ContentTitle',
        CONTENT_DESCRIPTION = 'ContentDescription',
        CONTENT_DOCUMENT_LINK = 'ContentDocumentLink',
        FIELD_TITLE = 'Title',
        FIELD_DESCRIPTION = 'Description',
        CONTENT_SCHEMA = schemaContainer['ContentDocument'],
        CONTAINER_PARENT = 'tr[data-uid]';
    /**
     * Callback to create file title and description editor
     * @param {DOMNode} container to add new element
     * @param {Options} options the options returned from object
     */
    cq.getFileEditor = function(container, options) {
        if(options.field && (options.field === CONTENT_TITLE || options.field === CONTENT_DESCRIPTION) && container.parent(CONTAINER_PARENT).length !== 0) {
            var parent = container.parent(CONTAINER_PARENT),
                uid = parent.data().uid,
                model = sqx.dataStore.getObjectWithUidFor(CONTENT_DOCUMENT_LINK, uid),
                field = options.field === CONTENT_TITLE ? FIELD_TITLE : FIELD_DESCRIPTION;

            var elem;

            if(field === FIELD_TITLE) {
                elem = $('<input type="text" class="cq-default-input cq-wo cq-input-required" maxlength="' + CONTENT_SCHEMA.fields.Title.validation.maxlength + '" name="ContentDocument.Title" required="required"/>');
                elem.attr('data-required-msg', translator.get('common.requiredmsg', {fieldName: CONTENT_SCHEMA.fields.Title.name }));
            }
            else {
                elem = $('<textarea class="cq-wo cq-multi-line-textarea" maxlength="' + CONTENT_SCHEMA.fields.Description.validation.maxlength + '" name="ContentDocument.Description"></textarea>');
            }

            elem.appendTo(container);
        }
        else {
            $("<p>Incorrect configuration for file editor. Note it doesn't support popup editor</p>").appendTo(container);
        }
    }

    return cq;
})(window.cq || {}, window.SQXSchema, window.sqx.translation);

/**
* returns a template that can be used for reference field where name is to be displayed.
* @param : field : field for which template is to be generated
* @param : relatedField : Field which is to be displayed instead of field param
*/
function getReferenceDisplayTemplate(field, relatedField) {
    
    var relName = field.replace("__c", "__r");

    if(field.indexOf('Id') == field.length - 'Id'.length){
        //if it is standard field it will end with Id rather than __c
        relName = field.substr(0, field.length - 'Id'.length);
    }


    return kendo.template("#if(data." + relName + " == null){#  #}else{# #: data." + relName + "." + (relatedField || "Name") + " # #}#");
}

/**
* returns a template that can be used for reference field where link as a name is to be displayed.
*/
function getReferenceLinkDisplayTemplate(field, relatedField){

    var relName = 'data.' + field.replace("__c", "__r"),
        relField = relatedField || 'Name',
        REF_TMPL =    '# var refField = data.{field}, isValidId = sqx.common.isSFID(refField); #'
                    + '# if(refField) {#'
                    +      '# if(isValidId && {relName}) { #'
                    +          '<a target="_blank" href="#= sqx.common.getObjectUrl(refField) #" class="referenceLink">'
                    +              '#: {relName} && {relName}.{relField} #'
                    +          '</a>'
                    +      '# } else {# '
                    +          '#: {relName} && {relName}.{relField} #'
                    + '#} } #';

    if(field.indexOf('Id') == field.length - 'Id'.length){
        //if it is standard field it will end with Id rather than __c
        relName = field.substr(0, field.length - 'Id'.length);
    }
    return REF_TMPL.replace(/{field}/g, field).replace(/{relName}/g, relName).replace(/{relField}/g, relField);
}

var attachmentTemplate = kendo.template('<h3>File</h3><p><label>Name:<input type=file name="name" /></label></p><p><label>Desc: <textarea name="Description" /></label></p>');

/**
* This function returns an evaluated template defined by the templatedId. The data param is passed when evaluating the template this
* allows binding of template properties with data.
* @param templateId the DOM element id of the template that is to be used
* @param data the data/model that is to be used when evaluating the template.
* [Note: This method is a shorthand that can be used when generating dynamic templates for a kendo template. i.e. a template inside a template.
*        It reduces the necessity for escaping the use of '#' character in the template selection.]
*/
function getCellTemplate(templateId, data){
    return kendo.template($("#" + templateId).html())(data);
}