var notificationItem = null, centeredNotification = null;

/**
* displays a notification message for an object
*/
function showNotification(message, level, position){
    var msgDisplayer = null;
    if(position != 'center' ){ //doesn't not support anything else then center and bottom


        notificationItem = notificationItem || $("<div></div>").kendoNotification({
                        stacking: "up",
                        autoHideAfter: 0,
                        button: true,
                        position: {pinned: true, top: null, left: 10, right: 10, bottom: 10}
                    }).data("kendoNotification");

        msgDisplayer = notificationItem
    }
    else{

        centeredNotification = centeredNotification || $("<div></div>").kendoNotification({
                        stacking: "up",
                        button: true,
                        autoHideAfter: 0,
                        show: function onShow(e) { //this is a function that centers the notification message
                                    if (!$("." + e.sender._guid)[1]) {
                                        var element = e.element.parent(),
                                            eWidth = element.width(),
                                            eHeight = element.height(),
                                            wWidth = $(window).width(),
                                            wHeight = $(window).height(),
                                            newTop, newLeft;
                                        
                                        newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                                        newTop = Math.floor(wHeight / 2 - eHeight / 2);

                                        e.element.parent().css({top: newTop, left: newLeft});
                                    }
                                },
                        position: {pinned: true, top: null, left: 10, right: 10, bottom: null}
                    }).data("kendoNotification");

        msgDisplayer = centeredNotification
    }

    level = level || 'info'; //defaults to information

    msgDisplayer.show(message, level);

}