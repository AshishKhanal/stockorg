/* Salesforce VF Page helper classes. This class includes common javascripts functions for working in a VF page
* These include things like helping inline vf page redirect properly.
*/
(function(w,$,k){
	var sqx = {};

	/**
	* reloads inline vf pages for both lightning and standard ui.
	* @url the url of the record/iframe which is to be reloaded
	*/
	sqx.reloadInlineVFOnSuccess = function(url){
		if($(".errorDiv").find('img[title="ERROR"]').length == 0){
			sqx.reloadInlineVF(url);
        }
	}

	/**
	* reloads an inline vf pages unconditionally
	* @url the url of the record/iframe which is to be reloaded
	*/
	sqx.reloadInlineVF = function(url){
		if(window.sforce && window.sforce.one){
            window.location.reload(); // because the iframe has correct source we can reload the file without any issue
    	}
		else{
        	navigateToUrl(url); // because the iframe is being loaded by external script we need to navigate to the record for correct view
        }
	}

	/**
	* This method takes a Visualforce apex:selectList and then replaces it with the selected items text
	* This is useful for creating Read mode of select list which is being virtually tied to the content
	* Example: the library field stores the library Id, but we need to display the library name using select list 
	*/
	sqx.convertSelectToText = function(element){
		element = $(element);

		element.parent().text(element.find('option:selected').text());
	}

	/**
	* static counter for number of uploads being done, used by enableDisableButtonsOnUpload
	*/
	sqx._uploadCount = 0;

	/**
	* enables/disables command button present in page 
	*/
	sqx.enableDisableButtonsOnUpload = function(eId){
		var buttons = $(".cmdbtn");
		
        if(eId == 'Upload Started'){
            sqx._uploadCount++;
        }
        else if(eId == 'Upload Cancelled' || eId == 'Upload Failed' || eId == 'Upload Succeeded'){
            sqx._uploadCount--;
        }

        buttons.prop('disabled', sqx._uploadCount > 0);
	}
	
	/**
	* creates a file picker with the given element and returns the widget
	* @param element the DOM element which is to be wrapped as a file picker
	* @param template the template that is to be used to show the text in the picker
	* @param defaultDSFilter the default filter that must be set on the data source for the combobox
	* @param fields the list of fields that can be fetched by the server
	* @param returns the combobox widget
	*/
	sqx.createFilePicker = function(element, remoteCall, defaultDSFilter, fields, options){
		//element, template, source, defaultDSFilter, fields){

		options.dataSource = new sqx.DataView({
			viewFilter : defaultDSFilter,
			remoteCallMethod: remoteCall,
			sort: { field: 'Title', dir: 'asc'},
            schema : {model : {id: 'Id'} },
            fields: fields,
            useSFRemoteObjects : true,
            serverSorting: true
		});

		options.dataTextField = 'Title';
		options.dataValueField = 'ContentDocumentId';
		options.filter = 'contains';
		options.valuePrimitive = true;
		options.useSFRemoteObjects = true;
		options.minLength = 3;
		options.placeholder = cq.labels.docUpload.selectFileFromServer;

		element.kendoComboBox(options);

		return element.data('kendoComboBox');
	}
	w.sqx = sqx;

})(window, jQuery, kendo)
