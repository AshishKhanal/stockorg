sqx.print={};

/**
* @description: hides all action column in printMode
*/
sqx.print.hideActionColumns = function(){
    var index, grid, columns;
    var cqGrid = $('div[data-role="cqgridbase"]');
    if(cqGrid.length > 0){
        cqGrid.each(function(i, e){
            index = -1;
            grid = $(e).data('kendoCQGridBase'); 
            columns = grid.options.columns;
            if (columns.length > 0) {
                for (var i = 0; i < columns.length; i++) {
                    if (columns[i].title != null && columns[i].title == 'Actions') { 
                        index = i;
                    }
                }
            }
            if(index>=0){
                grid.hideColumn(index);
            }
        });
    }
}

/**
* @description: listens for the print action to perform print, and redirects the user to main page after print
*/
$(function() {
    var printLink = $('#printPage'), returnLink, linkText, mr;

    printLink.click(function(e){
        if(cq.printMode) {
            window.print();
            e.preventDefault();
        }
        else {
            kendo.init($("#loadingBlock"));
            $("#loadingBlock").show().delay(3000).fadeOut();
        }
    });

    if (cq.printMode) {
        $("body").addClass("cq-print-mode");
        returnLink = printLink.attr('href').replace('printMode=true', '');
        mr = window.mainRecord || {Name: 'No record found', 'Id': '#'};

        linkText = sqx.translation.get('common.printmodeheader') || 'Print Mode';
        linkText = linkText.replace('[PrintLinkStart]', '<a href="javascript:window.print()">');
        linkText = linkText.replace('[PrintLinkEnd]', '</a>');
        linkText = linkText.replace('[ReturnLinkStart]', '<a href="' + returnLink + '">');
        linkText = linkText.replace('[ReturnLinkEnd]', '</a>');

        $("<div class='cq-print-mode-header'>" + linkText  + "</div>").prependTo($("body"));
        printLink.attr('data-href', printLink.attr('href'));
        printLink.attr('href', 'javascript:void(0);')
    }
});

/**
* @description: transforms all inner tabs inside grids to panel
*/
sqx.print.transformInnerTabToPanel = function(){
    var innerTabs = $('.k-detail-row').find($('div[data-role="cqtabstrip"]'));
    if(innerTabs.length > 0){
        innerTabs.each(function(i, e){
            $(e).kendoCQPanel({
                expandAllPanels: cq.printMode,
                collapsible: false
             });
        });
    }
    $('li[role="menuitem"]').css('width', '100%');
}

/**
 * Transform checkboxes to displayable content because browsers can have background
 * graphics turned off
 */
sqx.print.transformCheckboxes = function() {
    $('span[class="cq-boolean-checked"]').removeClass('cq-boolean-checked').append('<img src="' + cq.resourceLocation + '/styles/Silver/checkbox_checked.png" alt="Checked">');
    $('span[class="cq-boolean-unchecked"]').removeClass('cq-boolean-unchecked').append('<img src="' + cq.resourceLocation +'/styles/Silver/checkbox_unchecked.png" alt="Not Checked">');

}

/**
* @description: expands all grids inside panels during print
*/
sqx.print.expandAllGrids = function(){
    var gridBase = $('div[data-role="cqgridbase"]');
    if(gridBase.length > 0){
        var gridCount = gridBase.length;
        var kendoGridBase = gridBase.data('kendoCQGridBase');
        kendoGridBase.expandRow($('.k-master-row'));

        if(gridBase.length > gridCount){
            sqx.print.expandAllGrids();
        }
    }
}

/**
 * @description : expand iframe to content window height and width
 * @param : sender : the element on which changes is to be done
 */
sqx.print.frameLoadComplete = function(sender){
    if(cq.printMode){
        $(sender).css('height', $($(sender)[0].contentWindow.document).height());
        $($(sender)[0].contentWindow.document).find('.pbButton, .actionColumn').hide();
        $(sender.contentDocument).find('.bRelatedList *').css({'border' : 'none', 'background-color' : 'white'});
        $(sender.contentDocument).find('.bRelatedList .pbTitle h3, .bPageBlock .pbTitle h2, .bPageBlock .pbTitle h3, .bSubBlock h3.lbHeader').css({ 'color': '#515967','font-weight': 'normal'});
        $(sender.contentDocument).find('.pbBody table.list tr.headerRow td, .pbBody table.list tr.headerRow th').css({ 'color': '#515967', 'font-weight': 'normal'});
    }
}
