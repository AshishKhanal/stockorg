/**
 * provides a common method create the list of homepage items, filters and sorting functions
 */
(function() {

    var list,
        fn,
        ORDER_BY_DUE_DATE = 1,
        ORDER_BY_CREATED_DATE = 2,
        DESC_ORDER = 1,
        ASC_ORDER = 2,
        EVT_VIEW_UPDATED = 'view_updated';

    var badgeType = {
        MODULE: 'module',
        ACTION: 'action'
    };

    var urgency = {
        CRITICAL: 'Critical',
        HIGH: 'High',
        LOW: 'Low'
    };

    list = function(items, icons) {
        var that = this;
        that.view = [];
        that.originalItems = items;
        that.orderField = ORDER_BY_DUE_DATE;
        that.order = DESC_ORDER;
        that.orderFunction = false;
        that.listeners = [];
        that.filter = [];
        that.applicableFilters = [];
        that.actionFilters = [];
        that.moduleFilters = [];
        that.icons = icons;
        that._computeFilters();
        that._resetView();
    }

    fn = list.prototype;

    /**
     * adds to list of change listeners
     */
    fn.addListener = function(listener) {
        var that = this;
        that.listeners.push(listener);
    }

    /**
     * removes from the list of change listeners
     */
    fn.removeListener = function(listener) {
        var that = this,
            location;

        location = that.listeners.indexOf(listener);

        if (location !== -1) {
            that.listeners.splice(location, 1);
        }

    }

    /**
     * adds active filter from the list of available filters
     */
    fn.addFilter = function(filter, type) {
        var that = this;
        if (type === badgeType.ACTION) {
            if (that.actionFilters.indexOf(filter) === -1) {
                that.actionFilters.push(filter);
            }
        } else {
            if (that.moduleFilters.indexOf(filter) === -1) {
                that.moduleFilters.push(filter);
            }
        }
        that._resetView();
    }

    /**
     * removes unactive filter from the list of active filters
     */
    fn.removeFilter = function(filter, type) {
        var that = this,
            filterLocation;

        if (type === badgeType.ACTION) {
            filterLocation = that.actionFilters.indexOf(filter);
            if (filterLocation !== -1) {
                that.actionFilters.splice(filterLocation, 1);
            }
        } else {
            filterLocation = that.moduleFilters.indexOf(filter);
            if (filterLocation !== -1) {
                that.moduleFilters.splice(filterLocation, 1);
            }
        }
        that._resetView();
    }

    /**
     * resets the items view
     */
    fn._resetView = function() {
        var that = this;
        var icons = this.icons;
        if ((that.actionFilters.length === 0) && (that.moduleFilters.length === 0)) {
            that.view = that.originalItems.slice(0);
        } else {
            that.view = that._filter() || [];
        }

        that.sort();
        //notify all listeners
        that._notifyAll();
    }

    /**
     * sorts with the provided sorting method
     */
    fn.sort = function() {
        var that = this,
            orderAlgorithm;
        orderAlgorithm = that.view.reverse;
        that.view = orderAlgorithm.call(that.view, function(e1, e2) {
            var comparisionResult = 0;

            if (that.orderFunction) {
                comparisionResult = that.orderFunction.call(null, e1, e2);
            }

            return comparisionResult;
        });
    }

    /**
     * notifies all listeners
     */
    fn._notifyAll = function() {
        var that = this,
            i, listeners;

        listeners = that.listeners;
        for (i = 0; i < listeners.length; i++) {
            listeners[i].call(null, EVT_VIEW_UPDATED);
        }
    }

    /**
     * creates filter items
     */
    fn._filter = function() {
        var that = this;
        return that.originalItems.filter(function(item, index) {
            var i, filters = that.filter;
            var filterbadges = [item.actionType, item.moduleType];
            if (that.actionFilters.length === 0 && that.moduleFilters.length == 0) {
                return true;
            } else if (filterbadges) {
                if (((that.actionFilters.indexOf(filterbadges[0]) !== -1) && (that.moduleFilters.length === 0)) ||
                    ((that.moduleFilters.indexOf(filterbadges[1]) !== -1) && (that.actionFilters.length === 0)) ||
                    ((that.actionFilters.indexOf(filterbadges[0]) !== -1) && (that.moduleFilters.indexOf(filterbadges[1]) !== -1))) {
                    return true;
                }
            }

            return false;
        });
    }

    /**
     * creates the filter
     */
    var filterType = function(text, listener) {
        var that = this;

        this.filter = text;
        this.count = 0;
        this.active = false;
        this.listener = listener;
        this.type = badgeType.MODULE;
        this.countRed = 0;
        this.countYellow = 0;
        this.countGray = 0;
        this.icon = '';
    }

    /**
     * toggles the active state of the filter
     */
    filterType.prototype.toggle = function() {
        this.active = !this.active;

        if (this.listener) {
            this.listener.call(null, this.filter, this.active, this.type);
        }
    }

    /**
     * adds/removes active filters
     */
    fn._computeFilters = function() {
        var that = this,
            i, filterMap = {},
            listener;
        listener = function(filter, active, type) {
            if (active) {
                that.addFilter(filter, type);
            } else {
                that.removeFilter(filter, type);
            }
        }

        that.originalItems.forEach(function(e, i) {
            var k, badg, count, text, badges;
            badges = [e.actionType, e.moduleType];
            for (k = 0; k < badges.length; k++) {
                text = badges[k];
                badg = filterMap[text] || new filterType(text, listener);
                badg.count++;
                if (k == 0) {
                    badg.type = badgeType.ACTION;
                } else {
                    if (e.urgency === urgency.CRITICAL) {
                        badg.countRed++;
                    } else if (e.urgency === urgency.HIGH) {
                        badg.countYellow++;
                    } else {
                        badg.countGray++;
                    }
                }
                e.aIconSrc = that.icons[e.actionType];
                e.mIconSrc = that.icons[e.moduleType];
                filterMap[text] = badg;
            }
        });
        for (var f in filterMap) {
            filterMap[f].icon = that.icons[f];
            that.applicableFilters.push(filterMap[f]);
        }
        //sorting filters alphabetically in ascending order
        that.applicableFilters.sort(function (a, b) { return a.filter >= b.filter ? 1 : -1; });
    }

    window.cq = window.cq || {};
    window.cq.list = list;
})();
