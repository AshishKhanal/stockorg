[
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-5"
        },
        "Name": "Course",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "CRS-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-6"
        },
        "Name": "Inspection Criteria",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "INS-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-6"
    }
]