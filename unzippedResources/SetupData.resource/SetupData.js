[
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-1"
        },
        "Name": "Customer",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Customer",
        "Id": "PickListValue-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-2"
        },
        "Name": "Europe",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "Europe",
        "Id": "PickListValue-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-3"
        },
        "Name": "Australia",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "Australia",
        "Id": "PickListValue-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-4"
        },
        "Name": "Japan",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "Japan",
        "Id": "PickListValue-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-5"
        },
        "Name": "Canada",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "Canada",
        "Id": "PickListValue-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-6"
        },
        "Name": "Brazil",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "Brazil",
        "Id": "PickListValue-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-7"
        },
        "Name": "Complainant",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Complainant",
        "Id": "PickListValue-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-8"
        },
        "Name": "Distributor",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Distributor",
        "Id": "PickListValue-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-9"
        },
        "Name": "USA",
        "compliancequest__Category__c": "Complaint Origin",
        "compliancequest__Value__c": "USA",
        "Id": "PickListValue-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-10"
        },
        "Name": "Sales Rep",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Sales Rep",
        "Id": "PickListValue-10"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-11"
        },
        "Name": "Supplier",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Supplier",
        "Id": "PickListValue-11"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-12"
        },
        "Name": "End User",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "End User",
        "Id": "PickListValue-12"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-13"
        },
        "Name": "Friend",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Friend",
        "Id": "PickListValue-13"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Picklist_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Picklist_Value__c/PickListValue-14"
        },
        "Name": "Family",
        "compliancequest__Category__c": "Complaint Contact Type",
        "compliancequest__Value__c": "Family",
        "Id": "PickListValue-14"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-1"
        },
        "Name": "Labeling Deficiency",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-2"
        },
        "Name": "Conclusion Not Yet Available; Evaluation in Progress",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-3"
        },
        "Name": "Failure to Follow Instructions",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-4"
        },
        "Name": "Use Error Caused or Contributed to Event",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-5"
        },
        "Name": "No Failure Detected, Device Out of specification",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-6"
        },
        "Name": "Device Difficult to Operate",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-7"
        },
        "Name": "Maintenance Deficiency",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-8"
        },
        "Name": "Operational Context Caused or Contributed to Event",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-9"
        },
        "Name": "Device Not Returned",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-10"
        },
        "Name": "Re-use of Single Use Device",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-10"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-11"
        },
        "Name": "Device Returned in Condition Making Evaluation Impossible",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-11"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-12"
        },
        "Name": "Device Incorrectly Prepared for Use or Modified",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-12"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-13"
        },
        "Name": "Device Failure Related to Patient Condition",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-13"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-14"
        },
        "Name": "Design Deficiency",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-14"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-15"
        },
        "Name": "Manufacturing Deficiency",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-15"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-16"
        },
        "Name": "Cannot Complete Investigation; Incomplete Device Returned",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-16"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-17"
        },
        "Name": "Device Not Manufactured by Reporting Firm",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-17"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-18"
        },
        "Name": "Infrastructure Failure",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-18"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-19"
        },
        "Name": "Device Repaired and Returned",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-19"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-20"
        },
        "Name": "Known Inherent Risk of Procedure",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-20"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-21"
        },
        "Name": "Training Deficiency",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-21"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-22"
        },
        "Name": "No Failure Detected, Device Operated within specification",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-22"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-23"
        },
        "Name": "Device Returned Unused in Original Package",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-23"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-24"
        },
        "Name": "Inconclusive Investigation In Progress",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-24"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-25"
        },
        "Name": "Off-Label, Unapproved, or Contra-indicated use",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-25"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Defect_Code__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Defect_Code__c/DefectCode-26"
        },
        "Name": "Device Discarded by User, Unable to Follow-up",
        "compliancequest__Defect_Category__c": "Complaint",
        "compliancequest__Type__c": "Complaint Conclusion",
        "Id": "DefectCode-26"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-1"
        },
        "Name": "Product History Review",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "Perform Product History Review",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "PHR",
        "Id": "TaskFlow-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-2"
        },
        "Name": "FDA Medwatch",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "FDA Medwatch Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-3"
        },
        "Name": "Request Samples",
        "compliancequest__Allowed_Days__c": 10,
        "compliancequest__Description__c": "Appropriate samples need to be requested",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Sample Request",
        "Id": "TaskFlow-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-4"
        },
        "Name": "Japan - ARR",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "Japan ARR Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-5"
        },
        "Name": "EMEA - Initial MDV",
        "compliancequest__Allowed_Days__c": 30,
        "compliancequest__Description__c": "EMEA - Initial MDV Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-6"
        },
        "Name": "Health Canada - Initial MDPR",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "Health Canada Initial MDPR Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-7"
        },
        "Name": "Australia - TGA",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "Australian Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-8"
        },
        "Name": "Health Canada - Final MDPR",
        "compliancequest__Allowed_Days__c": 30,
        "compliancequest__Description__c": "Health Canada Final MDPR Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-9"
        },
        "Name": "EMEA - Final MDV",
        "compliancequest__Allowed_Days__c": 30,
        "compliancequest__Description__c": "EMEA - Final MDV Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-10"
        },
        "Name": "Brazil",
        "compliancequest__Allowed_Days__c": 30,
        "compliancequest__Description__c": "Brazil Regulatory Submission Assessment Decision Tree",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Decision Tree",
        "Id": "TaskFlow-10"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task__c/TaskFlow-11"
        },
        "Name": "Investigation",
        "compliancequest__Allowed_Days__c": 5,
        "compliancequest__Description__c": "Perform detailed investigation",
        "compliancequest__Record_Type__c": "Complaint",
        "compliancequest__Task_Type__c": "Investigation",
        "Id": "TaskFlow-11"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-1"
        },
        "Name": "EMEA.06",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Has led or might lead to death or serious deterioration in the state of health?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-2"
        },
        "Name": "FDA.08",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does the information reasonably suggest that the device would be likely to cause or contribute to a death or serious injury if the malfunction were to reoccur?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-3"
        },
        "Name": "FDA.06",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does this information reasonably suggest that a death or serious injury has occurred?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-4"
        },
        "Name": "FDA.13a",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is the event or type of device involved reportable on an Alternative Summary Report (ASR)?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-5"
        },
        "Name": "FDA.14",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does this information reasonably suggest that the malfunction resulted in a death or serious injury?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-6"
        },
        "Name": "FDA.07",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does the information reasonably suggest a malfunction of the device has occurred?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-7"
        },
        "Name": "FDA.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is Company responsible for reporting this type of event or device involved (i.e. approved contract.)?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-8"
        },
        "Name": "FDA.05",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is the event or type of device involved reportable on an Alternative Summary Report (ASR)?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-9"
        },
        "Name": "FDA.03",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Was there an adverse event that was disassociated from the product?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-10"
        },
        "Name": "FDA.13",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did this event necessitate remedial action to prevent an unreasonable risk of substantial harm to the public health, or requested by FDA by written notice?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-10"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-11"
        },
        "Name": "3",
        "RecordTypeId": "SimpleQuestionRecordType",
        "compliancequest__QuestionText__c": "Were there any change orders placed in production?",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "compliancequest__SQX_Task__c": "TaskFlow-1",
        "Id": "TaskQuestion-11"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-12"
        },
        "Name": "2",
        "RecordTypeId": "SimpleQuestionRecordType",
        "compliancequest__QuestionText__c": "Were there any deviations, NCs at incoming, inprocess or final stages?",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "compliancequest__SQX_Task__c": "TaskFlow-1",
        "Id": "TaskQuestion-12"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-13"
        },
        "Name": "1",
        "RecordTypeId": "SimpleQuestionRecordType",
        "compliancequest__QuestionText__c": "Was there a high scrap rate in any part of the process?",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "compliancequest__SQX_Task__c": "TaskFlow-1",
        "Id": "TaskQuestion-13"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-14"
        },
        "Name": "FDA.04",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did this event necessitate remedial action to prevent an unreasonable risk of substantial harm to the public health, or requested by FDA by written notice?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-14"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-15"
        },
        "Name": "FDA.11",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does the information reasonably suggest a malfunction of the device has occurred?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-15"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-16"
        },
        "Name": "FDA.12",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is Company responsible for reporting this type of event or device involved (i.e. approved contract.)?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-16"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-17"
        },
        "Name": "FDA.15",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does the information reasonably suggest that the device would be likely to cause or contribute to a death or serious injury if the malfunction were to reoccur?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-17"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-18"
        },
        "Name": "FDA.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is this device sold and/or marketed in the US?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-18"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-19"
        },
        "Name": "FDA.10",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is this device similar to a device sold and/or marketed in the US?",
        "compliancequest__SQX_Task__c": "TaskFlow-2",
        "Id": "TaskQuestion-19"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-20"
        },
        "Name": "4",
        "RecordTypeId": "SimpleQuestionRecordType",
        "compliancequest__QuestionText__c": "were all incoming, inprocess and final inspections completed?",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "compliancequest__SQX_Task__c": "TaskFlow-1",
        "Id": "TaskQuestion-20"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-21"
        },
        "Name": "5",
        "RecordTypeId": "SimpleQuestionRecordType",
        "compliancequest__QuestionText__c": "were all inspections within specification limits?",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "compliancequest__SQX_Task__c": "TaskFlow-1",
        "Id": "TaskQuestion-21"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-22"
        },
        "Name": "HC.INIT.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is the device involved in this incident used in Canada?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-22"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-23"
        },
        "Name": "EMEA.14",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Death or unanticipated serious deterioration in state of health?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-23"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-24"
        },
        "Name": "EMEA.21",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Incident lead to an FSCA in EEA/CH?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-24"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-25"
        },
        "Name": "EMEA.31",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is there a PSR, if agreement with Coordinating CA?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-25"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-27"
        },
        "Name": "EMEA.03",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Incident described in already notified FSCA?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-27"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-28"
        },
        "Name": "EMEA.09",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Caused by patient conditions or is an expected and foreseeable side effects?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-28"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-29"
        },
        "Name": "HC.INIT.04",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Could the incident lead to the death or a serious deterioration in the state of health of a patient, user or other person if it were to recur?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-29"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-30"
        },
        "Name": "HC.INIT.03",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Has the incident led to the death or a serious deterioration in the state of health of a patient, user or other person?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-30"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-31"
        },
        "Name": "EMEA.05",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Abnormal use?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-31"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-32"
        },
        "Name": "EMEA.07",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Device suspected contributory cause?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-32"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-33"
        },
        "Name": "EMEA.04",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Agreement with affected CA for this type of incident?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-33"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-34"
        },
        "Name": "EMEA.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Pre-market clinical study device?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-34"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-35"
        },
        "Name": "HC.INIT.07",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Have we indicated to the regulatory agency of the incident country that we intend to take corrective action, or has that agency required us to take corrective action? (Corrective action refers to any action taken to prevent the recurrence of the problem.)",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-35"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-36"
        },
        "Name": "HC.INIT.05",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did the incident occur within Canada?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-36"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-37"
        },
        "Name": "HC.FIN.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Are all tasks complete and ready for final reporting?",
        "compliancequest__SQX_Task__c": "TaskFlow-8",
        "Id": "TaskQuestion-37"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-38"
        },
        "Name": "HC.INIT.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Does the incident relate to a failure of the device or a deterioration in its effectiveness, or any inadequacy in its labeling or in its directions for use?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-38"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-39"
        },
        "Name": "EMEA.08",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Serious public health threat?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-39"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-40"
        },
        "Name": "HC.INIT.06",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did the incident occur within Canada?",
        "compliancequest__SQX_Task__c": "TaskFlow-6",
        "Id": "TaskQuestion-40"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-41"
        },
        "Name": "EMEA.12",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Protection against fault functioned correctly or negligible likelihood or caused by use error?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-41"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-42"
        },
        "Name": "HC.FIN.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is the initial MDPR form submitted?",
        "compliancequest__SQX_Task__c": "TaskFlow-8",
        "Id": "TaskQuestion-42"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-43"
        },
        "Name": "EMEA.11",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Deficiency always detected prior to use?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-43"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-44"
        },
        "Name": "EMEA.13",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Death or Serious Injury?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-44"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-45"
        },
        "Name": "EMEA.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Event within EEA or CH?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-45"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-46"
        },
        "Name": "EMEA.10",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Caused by exceeded service life /shelf life?",
        "compliancequest__SQX_Task__c": "TaskFlow-5",
        "Id": "TaskQuestion-46"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-47"
        },
        "Name": "Japan.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is this a reportable incident?",
        "compliancequest__SQX_Task__c": "TaskFlow-4",
        "Id": "TaskQuestion-47"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-48"
        },
        "Name": "BR.04",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did the event cause the patient death?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-48"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-49"
        },
        "Name": "BR.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did the event occur in Brazil?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-49"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-50"
        },
        "Name": "Australia.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Could death or serious injury occur if the event were to recur?",
        "compliancequest__SQX_Task__c": "TaskFlow-7",
        "Id": "TaskQuestion-50"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-51"
        },
        "Name": "BR.06",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Would the event potentially lead to a serious injury, if it were to re-occur?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-51"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-52"
        },
        "Name": "Japan.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "What is the type of Report?",
        "compliancequest__SQX_Task__c": "TaskFlow-4",
        "Id": "TaskQuestion-52"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-53"
        },
        "Name": "BR.03",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Is the Adverse event caused by the user after the expiration date (shelf life) established by the manufacturer?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-53"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-54"
        },
        "Name": "BR.05",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did the event lead to a serious injury?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-54"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-55"
        },
        "Name": "EMEAF.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Are all tasks complete and ready for final reporting?",
        "compliancequest__SQX_Task__c": "TaskFlow-9",
        "Id": "TaskQuestion-55"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-56"
        },
        "Name": "BR.02",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Were the deficiencies of devices detected by the user before the use, and no serious injury has occured?",
        "compliancequest__SQX_Task__c": "TaskFlow-10",
        "Id": "TaskQuestion-56"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Task_Question__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Task_Question__c/TaskQuestion-57"
        },
        "Name": "Australia.01",
        "RecordTypeId": "DynamicQuestionRecordType",
        "compliancequest__QuestionText__c": "Did a serious injury or death occur?",
        "compliancequest__SQX_Task__c": "TaskFlow-7",
        "Id": "TaskQuestion-57"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-1"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 3,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-14",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "5 Day MDR",
        "Id": "AnswerOption-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-2"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-14",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-9",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-3"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-15",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-4"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-3",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-8",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-5"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-19",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-6"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-10",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-16",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-7"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-9",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-7",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-8"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-16",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-9"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-2",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day MDR",
        "Id": "AnswerOption-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-10"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-15",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-19",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-10"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-11"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-5",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-4",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-11"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-12"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-7",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-18",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-12"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-13"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-9",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-13"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-14"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-4",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Variance Report",
        "Id": "AnswerOption-14"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-15"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-19",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-18",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-15"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-16"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-4",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-10",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-16"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-17"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 3,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-10",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "5 Day MDR",
        "Id": "AnswerOption-17"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-18"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-2",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-6",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-18"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-19"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-3",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day MDR",
        "Id": "AnswerOption-19"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-20"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-5",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day MDR",
        "Id": "AnswerOption-20"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-21"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-7",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-21"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-22"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-6",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-22"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-23"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-8",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Variance Report",
        "Id": "AnswerOption-23"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-24"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-2",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-24"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-25"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-8",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-14",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-25"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-26"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-17",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day MDR",
        "Id": "AnswerOption-26"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-27"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-17",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-5",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-27"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-28"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-16",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-15",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-28"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-29"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-17",
        "compliancequest__RegulatoryBody__c": "US FDA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-29"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-31"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-44",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No report, trending",
        "Id": "AnswerOption-31"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-32"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-33",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-27",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-32"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-33"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-28",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No report, trending",
        "Id": "AnswerOption-33"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-34"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-43",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-46",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-34"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-35"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-24",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-45",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-35"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-36"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-38",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-22",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-36"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-37"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-38",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-37"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-38"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-37",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-38"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-39"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-40",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-29",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-39"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-40"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-35",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-40"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-41"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-35",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-36",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-41"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-42"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-42",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-42"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-43"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-40",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30-day MDPR",
        "Id": "AnswerOption-43"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-44"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-29",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-30",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-44"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-45"
        },
        "Name": "No",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-42",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Final MDPR",
        "Id": "AnswerOption-45"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-46"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-29",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-46"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-47"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-35",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30-day MDPR",
        "Id": "AnswerOption-47"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-48"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-35",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-40",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-48"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-49"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-36",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-30",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-49"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-50"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-22",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-50"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-51"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-30",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-38",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-51"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-52"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 7,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-36",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "10-day MDPR",
        "Id": "AnswerOption-52"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-53"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Next_Question__c": "TaskQuestion-42",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-37",
        "compliancequest__RegulatoryBody__c": "Health Canada",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Final MDPR",
        "Id": "AnswerOption-53"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-54"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-24",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-54"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-55"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-31",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-33",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-55"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-56"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-1",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-56"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-57"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-34",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Report under Vigilance",
        "Id": "AnswerOption-57"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-58"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 1,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-39",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Report. Max. 2 days after awareness (ASAP MDV)",
        "Id": "AnswerOption-58"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-59"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-32",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-1",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-59"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-60"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 7,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-57",
        "compliancequest__RegulatoryBody__c": "Australia TGA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "TGA - 10 Day",
        "Id": "AnswerOption-60"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-61"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-23",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-41",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-61"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-62"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-46",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-28",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-62"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-63"
        },
        "Name": "No",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-23",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Report immediately. Max. 30 days (30-Day MDV)",
        "Id": "AnswerOption-63"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-64"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 7,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-23",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Report immediately. Max. 10 days (10-Day MDV)",
        "Id": "AnswerOption-64"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-65"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-6",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-3",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-65"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-66"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-28",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-39",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-66"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-67"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-39",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-32",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-67"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-68"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-27",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-68"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-69"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-33",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "PSR, Trending",
        "Id": "AnswerOption-69"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-70"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-27",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-45",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-70"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-71"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-43",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No report, trending",
        "Id": "AnswerOption-71"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-72"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-44",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-41",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-72"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-73"
        },
        "Name": "15-Day-MDR Clinical",
        "compliancequest__Due_In_Days__c": 10,
        "compliancequest__Option_Value__c": "15-Day-MDR Clinical",
        "compliancequest__Question__c": "TaskQuestion-52",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "15-Day-MDR Clinical",
        "Id": "AnswerOption-73"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-74"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-41",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-43",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-74"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-75"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-24",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Report as FSCA",
        "Id": "AnswerOption-75"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-76"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-55",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Final MDV",
        "Id": "AnswerOption-76"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-77"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-51",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day Report",
        "Id": "AnswerOption-77"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-78"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-55",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-78"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-79"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-51",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-54",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-79"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-80"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-51",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Notification Needed",
        "Id": "AnswerOption-80"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-81"
        },
        "Name": "Annual-MDR Post",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Annual-MDR Post",
        "compliancequest__Question__c": "TaskQuestion-52",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "Annual-MDR Post",
        "Id": "AnswerOption-81"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-82"
        },
        "Name": "15-Day-MDR Post",
        "compliancequest__Due_In_Days__c": 10,
        "compliancequest__Option_Value__c": "15-Day-MDR Post",
        "compliancequest__Question__c": "TaskQuestion-52",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "15-Day-MDR Post",
        "Id": "AnswerOption-82"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-83"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-54",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30 Day Report",
        "Id": "AnswerOption-83"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-84"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-1",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-31",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-84"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-85"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-46",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No report, trending",
        "Id": "AnswerOption-85"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-86"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-27",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-34",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-86"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-87"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-53",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-56",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-87"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-88"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-48",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-49",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-88"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-89"
        },
        "Name": "7-Day-MDR Clinical",
        "compliancequest__Due_In_Days__c": 5,
        "compliancequest__Option_Value__c": "7-Day-MDR Clinical",
        "compliancequest__Question__c": "TaskQuestion-52",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "7-Day-MDR Clinical",
        "Id": "AnswerOption-89"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-90"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-32",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-90"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-91"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-23",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-44",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-91"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-92"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-50",
        "compliancequest__RegulatoryBody__c": "Australia TGA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-92"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-93"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-52",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-47",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-93"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-94"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-48",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-53",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-94"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-95"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-54",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-48",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-95"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-96"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-50",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-57",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-96"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-97"
        },
        "Name": "No",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-47",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-97"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-98"
        },
        "Name": "Yes",
        "compliancequest__Next_Question__c": "TaskQuestion-56",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-49",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-98"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-99"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-56",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Notification Needed",
        "Id": "AnswerOption-99"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-100"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 30,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-25",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "PSR, if agreement with Coordinating CA",
        "Id": "AnswerOption-100"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-101"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-31",
        "compliancequest__RegulatoryBody__c": "EMEA",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Reporting Required",
        "Id": "AnswerOption-101"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-102"
        },
        "Name": "No",
        "compliancequest__Next_Question__c": "TaskQuestion-31",
        "compliancequest__Option_Value__c": "No",
        "compliancequest__Question__c": "TaskQuestion-25",
        "compliancequest__Reportable__c": false,
        "Id": "AnswerOption-102"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-103"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-50",
        "compliancequest__RegulatoryBody__c": "Australia TGA",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "TGA - 30 Day",
        "Id": "AnswerOption-103"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-104"
        },
        "Name": "30-Day-MDR Post",
        "compliancequest__Due_In_Days__c": 25,
        "compliancequest__Option_Value__c": "30-Day-MDR Post",
        "compliancequest__Question__c": "TaskQuestion-52",
        "compliancequest__RegulatoryBody__c": "Japan MHW",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "30-Day-MDR Post",
        "Id": "AnswerOption-104"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-105"
        },
        "Name": "Yes",
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-53",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": false,
        "compliancequest__Report_Name__c": "No Notification Needed",
        "Id": "AnswerOption-105"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Answer_Option__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Answer_Option__c/AnswerOption-106"
        },
        "Name": "Yes",
        "compliancequest__Due_In_Days__c": 1,
        "compliancequest__Option_Value__c": "Yes",
        "compliancequest__Question__c": "TaskQuestion-48",
        "compliancequest__RegulatoryBody__c": "Brazil",
        "compliancequest__Reportable__c": true,
        "compliancequest__Report_Name__c": "48 Hour Report",
        "Id": "AnswerOption-106"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type__c/ResultType-1"
        },
        "Name": "Yes/No",
        "Id": "ResultType-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type__c/ResultType-2"
        },
        "Name": "Pass/Fail",
        "Id": "ResultType-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type__c/ResultType-3"
        },
        "Name": "Temperature Range",
        "Id": "ResultType-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type__c/ResultType-4"
        },
        "Name": "Sat/UnSat",
        "Id": "ResultType-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-1"
        },
        "Name": "Yes",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "Id": "ResultTypeValue-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-2"
        },
        "Name": "Fail",
        "compliancequest__SQX_Result_Type__c": "ResultType-2",
        "Id": "ResultTypeValue-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-3"
        },
        "Name": "Pass",
        "compliancequest__SQX_Result_Type__c": "ResultType-2",
        "Id": "ResultTypeValue-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-4"
        },
        "Name": "No",
        "compliancequest__SQX_Result_Type__c": "ResultType-1",
        "Id": "ResultTypeValue-4"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-5"
        },
        "Name": "38 Degrees",
        "compliancequest__SQX_Result_Type__c": "ResultType-3",
        "Id": "ResultTypeValue-5"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-6"
        },
        "Name": "36 Degrees",
        "compliancequest__SQX_Result_Type__c": "ResultType-3",
        "Id": "ResultTypeValue-6"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-7"
        },
        "Name": "Unsatisfactory",
        "compliancequest__SQX_Result_Type__c": "ResultType-4",
        "Id": "ResultTypeValue-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Result_Type_Value__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Result_Type_Value__c/ResultTypeValue-8"
        },
        "Name": "Satisfactory",
        "compliancequest__SQX_Result_Type__c": "ResultType-4",
        "Id": "ResultTypeValue-8"
    }
]