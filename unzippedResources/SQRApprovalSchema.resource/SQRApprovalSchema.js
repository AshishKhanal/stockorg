"use strict"; var SQXSchema = SQXSchema || {} ;
SQXSchema.compliancequest__SQX_SQR_Approval__c={
"sObjectName": "compliancequest__SQX_SQR_Approval__c",
"sObjectLabel": "SQR Approval",
"id": "Id",
"fields": {
"compliancequest__Approval_Status__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Approval Status",
"editable": false,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Approval_Status__c",
"validValues": [
{
"value": "In Approval",
"text": "In Approval"
},
{
"value": "In Hold",
"text": "In Hold"
},
{
"value": "Draft",
"text": "Draft"
},
{
"value": "Purged",
"text": "Purged"
},
{
"value": "Approved",
"text": "Approved"
},
{
"value": "Rework",
"text": "Rework"
},
{
"value": "Voided",
"text": "Voided"
},
{
"value": "Rejected",
"text": "Rejected"
}
]
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__Comments__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Comments",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Comments__c",
"multiline": true
}
},
"compliancequest__Due_Date__c": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Due Date",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Due_Date__c"
}
},
"compliancequest__Completion_Date__c": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Completion Date",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Completion_Date__c"
}
},
"compliancequest__Days_To_Complete__c": {
"validation": {
"maxlength": 10
},
"type": "number",
"nullable": null,
"name": "Days To Complete",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Days_To_Complete__c",
"scale": 0
}
},
"compliancequest__SQX_Policy_Task__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Policy Task",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Policy_Task__c",
"referenceTo": "compliancequest__SQX_Policy_Task__c"
}
},
"compliancequest__SQX_SQR_Form__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "SQR Form",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_SQR_Form__c",
"referenceTo": "compliancequest__SQX_SQR_Form__c"
}
},
"compliancequest__Supplier__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Supplier",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Supplier__c"
}
},
"compliancequest__SQX_Task__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Task",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Task__c",
"referenceTo": "compliancequest__SQX_Task__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "SQR Approval Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"compliancequest__Is_Rework__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Is_Rework",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Is_Rework__c"
}
},
"compliancequest__SQX_Linked_Approval__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Linked Approval",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Linked_Approval__c",
"referenceTo": "compliancequest__SQX_SQR_Approval__c"
}
},
"compliancequest__Level__c": {
"validation": {
"required": true,
"maxlength": 8
},
"type": "number",
"nullable": null,
"name": "Approval Level",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Level__c",
"scale": 0
}
},
"compliancequest__SQX_User__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Assigned User",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_User__c",
"referenceTo": "User"
}
},
"compliancequest__SQX_Actual_Approver__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Actual Approver",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Actual_Approver__c",
"referenceTo": "User"
}
},
"compliancequest__IsLocked__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "IsLocked",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__IsLocked__c"
}
},
"compliancequest__Owner__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Owner",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Owner__c",
"referenceTo": "User"
}
}
},
"childRelationships": [
{
"relationshipName": "compliancequest__Uploaded_Attachments__r",
"field": "compliancequest__SQX_SQR_Approval__c",
"childSObject": "compliancequest__SQX_SQR_Uploaded_Attachment__c"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "compliancequest__Answers__r",
"field": "compliancequest__SQX_SQR_Approval__c",
"childSObject": "compliancequest__SQX_SQR_Answer__c"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "compliancequest__SQR_Approvals__r",
"field": "compliancequest__SQX_Linked_Approval__c",
"childSObject": "compliancequest__SQX_SQR_Approval__c"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
}
]
}
SQXSchema.compliancequest__SQX_SQR_Form__c={
"sObjectName": "compliancequest__SQX_SQR_Form__c",
"sObjectLabel": "SQR Form",
"id": "Id",
"fields": {
"compliancequest__SQX_Existing_Account__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Existing Supplier",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Existing_Account__c",
"referenceTo": "Account"
}
},
"compliancequest__Supplier_Name__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Supplier Name",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Name__c",
"multiline": false
}
},
"compliancequest__SQR_Form_Status__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "SQR Form Status",
"editable": false,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__SQR_Form_Status__c",
"validValues": [
{
"value": "Draft",
"text": "Draft"
},
{
"value": "Completed",
"text": "Completed"
},
{
"value": "Voided",
"text": "Voided"
},
{
"value": "Rejected",
"text": "Rejected"
},
{
"value": "Submitted",
"text": "Submitted"
},
{
"value": "Initiated",
"text": "Initiated"
}
]
}
},
"compliancequest__Comments__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Comments",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Comments__c",
"multiline": true
}
},
"compliancequest__Type__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Type",
"editable": false,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Type__c",
"validValues": [
{
"value": "Service",
"text": "Service"
},
{
"value": "Part",
"text": "Part"
}
]
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__SQX_Part__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Part",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Part__c",
"referenceTo": "compliancequest__SQX_Part__c"
}
},
"compliancequest__IsNew_Approval__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "IsNew Approval",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__IsNew_Approval__c",
"validValues": [
{
"value": "false",
"text": "false"
},
{
"value": "true",
"text": "true"
}
]
}
},
"compliancequest__Supplier_Ref__c": {
"validation": {
"maxlength": 40
},
"type": "string",
"nullable": null,
"name": "Supplier Ref",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Ref__c",
"multiline": false
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "SQR Form Id",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"compliancequest__Next_Approver__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Next Approver",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__Next_Approver__c",
"referenceTo": "User"
}
},
"compliancequest__Is_Rework__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Is_Rework",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Is_Rework__c"
}
},
"compliancequest__Next_Step__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Next Step",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Next_Step__c",
"multiline": false
}
},
"compliancequest__Form_Type__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Form Type",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Form_Type__c",
"validValues": [
{
"value": "Existing Supplier",
"text": "Existing Supplier"
},
{
"value": "New Supplier",
"text": "New Supplier"
},
{
"value": "New Part",
"text": "New Part"
},
{
"value": "Existing Part",
"text": "Existing Part"
}
]
}
},
"compliancequest__Supplier_Website__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Supplier Website",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Website__c",
"multiline": false
}
},
"compliancequest__SQR_Form_Title__c": {
"validation": {
"required": true,
"maxlength": 160
},
"type": "string",
"nullable": null,
"name": "SQR Form Title",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__SQR_Form_Title__c",
"multiline": false
}
},
"compliancequest__Level_Check__c": {
"validation": {
"maxlength": 2
},
"type": "number",
"nullable": null,
"name": "Current Approval Level",
"editable": true,
"additionalInfo": {
"defaultvalue": "1",
"apiname": "compliancequest__Level_Check__c",
"scale": 0
}
},
"compliancequest__Approval_Status__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Approval Status",
"editable": false,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Approval_Status__c",
"validValues": [
{
"value": "In Approval",
"text": "In Approval"
},
{
"value": "Approved",
"text": "Approved"
},
{
"value": "None",
"text": "None"
},
{
"value": "Rejected",
"text": "Rejected"
}
]
}
},
"compliancequest__SQX_Service__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Service",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Service__c",
"referenceTo": "compliancequest__SQX_Standard_Service__c"
}
},
"compliancequest__Supplier_Email__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Supplier Email",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Email__c",
"multiline": false
}
},
"compliancequest__Supplier_Phone__c": {
"validation": {
"maxlength": 40
},
"type": "string",
"nullable": null,
"name": "Supplier Phone",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Phone__c",
"multiline": false
}
},
"compliancequest__Updated_Level__c": {
"validation": {
"maxlength": 2
},
"type": "number",
"nullable": null,
"name": "Approved Level",
"editable": true,
"additionalInfo": {
"defaultvalue": "0",
"apiname": "compliancequest__Updated_Level__c",
"scale": 0
}
},
"compliancequest__SQX_Existing_Supplier__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Existing Supplier",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Existing_Supplier__c",
"referenceTo": "compliancequest__Supplier_Profile__c"
}
},
"compliancequest__Ownership_IsTaken__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Ownership IsTaken",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Ownership_IsTaken__c"
}
},
"compliancequest__Owner_Check__c": {
"validation": {
"maxlength": 100
},
"type": "string",
"nullable": null,
"name": "Owner",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Owner_Check__c",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
},
"compliancequest__Supplier_Address__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Supplier Address",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Address__c",
"multiline": true
}
},
"compliancequest__Reason_For_Request__c": {
"validation": {
"required": true,
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Reason For Request",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Reason_For_Request__c",
"multiline": true
}
},
"compliancequest__Supplier_Type__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Supplier Type",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Supplier_Type__c",
"validValues": [
{
"value": "Existing Supplier",
"text": "Existing Supplier"
},
{
"value": "New Supplier",
"text": "New Supplier"
}
]
}
},
"OwnerId": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Owner ID",
"editable": true,
"additionalInfo": {
"apiname": "OwnerId"
}
},
"compliancequest__Last_Approved_Step__c": {
"validation": {
"maxlength": 2
},
"type": "number",
"nullable": null,
"name": "Last Approved Step",
"editable": false,
"additionalInfo": {
"defaultvalue": "1",
"apiname": "compliancequest__Last_Approved_Step__c",
"scale": 0
}
},
"RecordTypeId": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Record Type ID",
"editable": true,
"additionalInfo": {
"apiname": "RecordTypeId",
"referenceTo": "RecordType"
}
},
"compliancequest__Supplier_Description__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Supplier Description",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Description__c",
"multiline": true
}
},
"compliancequest__Select__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Select",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Select__c",
"validValues": [
{
"value": "Service",
"text": "Service"
},
{
"value": "Update Info",
"text": "Update Info"
},
{
"value": "Part",
"text": "Part"
}
]
}
},
"compliancequest__Contact_Person__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Contact Person",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Contact_Person__c",
"multiline": false
}
}
},
"childRelationships": [
{
"relationshipName": "compliancequest__Supplier_Profile_Parts__r",
"field": "compliancequest__SQR_Form__c",
"childSObject": "compliancequest__Supplier_Profile_Part__c"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "compliancequest__Supplier_Profile_Services__r",
"field": "compliancequest__SQR_Form__c",
"childSObject": "compliancequest__Supplier_Profile_Service__c"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "compliancequest__SQR_Approvals__r",
"field": "compliancequest__SQX_SQR_Form__c",
"childSObject": "compliancequest__SQX_SQR_Approval__c"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "compliancequest__SQR_Tasks__r",
"field": "compliancequest__SQX_SQR_Form__c",
"childSObject": "compliancequest__SQX_SQR_Task__c"
}
]
}
SQXSchema.compliancequest__Supplier_Profile_Part__c={
"sObjectName": "compliancequest__Supplier_Profile_Part__c",
"sObjectLabel": "Supplier Profile Part",
"id": "Id",
"fields": {
"compliancequest__Part__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Part",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Part__c",
"referenceTo": "compliancequest__SQX_Part__c"
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__Active__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Active",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Active__c"
}
},
"compliancequest__Approved__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Approved",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Approved__c"
}
},
"compliancequest__Supplier_Part_Reference_Number__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Supplier Part Reference Number",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Supplier_Part_Reference_Number__c",
"multiline": false
}
},
"compliancequest__SQR_Form__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "SQR Form",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQR_Form__c",
"referenceTo": "compliancequest__SQX_SQR_Form__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Supplier Profile Parts Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "compliancequest__Supplier_Profiles__r",
"field": "compliancequest__Supplier_Profile_Part__c",
"childSObject": "compliancequest__Supplier_Profile__c"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
}
]
}
SQXSchema.compliancequest__SQX_Policy_Task__c={
"sObjectName": "compliancequest__SQX_Policy_Task__c",
"sObjectLabel": "Policy Task",
"id": "Id",
"fields": {
"compliancequest__Level__c": {
"validation": {
"maxlength": 8
},
"type": "number",
"nullable": null,
"name": "Level",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Level__c",
"scale": 0
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__SQX_Policy__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Policy",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_Policy__c",
"referenceTo": "compliancequest__SQX_Policy__c"
}
},
"compliancequest__Supplier__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Supplier",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Supplier__c"
}
},
"compliancequest__SQX_User_Queue__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "User/Queue",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_User_Queue__c",
"referenceTo": "User"
}
},
"compliancequest__SQX_Task__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Task",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Task__c",
"referenceTo": "compliancequest__SQX_Task__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Policy Task Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "compliancequest__SQR_Approvals__r",
"field": "compliancequest__SQX_Policy_Task__c",
"childSObject": "compliancequest__SQX_SQR_Approval__c"
},
{
"relationshipName": "compliancequest__SQR_Tasks1__r",
"field": "compliancequest__SQX_Policy_With_Task__c",
"childSObject": "compliancequest__SQX_SQR_Task__c"
}
]
}
SQXSchema.compliancequest__SQX_Task__c={
"sObjectName": "compliancequest__SQX_Task__c",
"sObjectLabel": "Task",
"id": "Id",
"fields": {
"compliancequest__Completion_Date__c": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Completion Date",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__Completion_Date__c"
}
},
"compliancequest__Description__c": {
"validation": {
"maxlength": 32768
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": true
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"OwnerId": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Owner ID",
"editable": true,
"additionalInfo": {
"apiname": "OwnerId",
"referenceTo": "Group"
}
},
"compliancequest__Due_Date__c": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Due Date",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Due_Date__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Task Name",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "compliancequest__SQR_Approvals__r",
"field": "compliancequest__SQX_Task__c",
"childSObject": "compliancequest__SQX_SQR_Approval__c"
},
{
"relationshipName": "compliancequest__Policy_Tasks__r",
"field": "compliancequest__SQX_Task__c",
"childSObject": "compliancequest__SQX_Policy_Task__c"
},
{
"relationshipName": "compliancequest__Question_With_Tasks__r",
"field": "compliancequest__SQX_Task__c",
"childSObject": "compliancequest__SQX_Question_With_Task__c"
},
{
"relationshipName": "compliancequest__Attachment_With_Tasks__r",
"field": "compliancequest__SQX_Task__c",
"childSObject": "compliancequest__SQX_Attachment_With_Task__c"
}
]
}
SQXSchema.compliancequest__SQX_Attachment__c={
"sObjectName": "compliancequest__SQX_Attachment__c",
"sObjectLabel": "Attachment",
"id": "Id",
"fields": {
"compliancequest__Description__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": true
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__Type__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Type",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Type__c",
"validValues": [
{
"value": "Information",
"text": "Information"
},
{
"value": "Data",
"text": "Data"
},
{
"value": "Licence",
"text": "Licence"
},
{
"value": "Certificate",
"text": "Certificate"
}
]
}
},
"OwnerId": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Owner ID",
"editable": true,
"additionalInfo": {
"apiname": "OwnerId",
"referenceTo": "Group"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Attacment Name",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "compliancequest__Attachment_With_Tasks__r",
"field": "compliancequest__SQX_Attachment__c",
"childSObject": "compliancequest__SQX_Attachment_With_Task__c"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
}
]
}
SQXSchema.compliancequest__SQX_Question__c={
"sObjectName": "compliancequest__SQX_Question__c",
"sObjectLabel": "Question",
"id": "Id",
"fields": {
"compliancequest__Answer__c": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Answer",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Answer__c",
"multiline": false
}
},
"compliancequest__Description__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": true
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__Type__c": {
"validation": {},
"type": "values",
"nullable": null,
"name": "Type",
"editable": true,
"additionalInfo": {
"allowMultiSelect": false,
"apiname": "compliancequest__Type__c",
"validValues": [
{
"value": "Radio buttons",
"text": "Radio buttons"
},
{
"value": "Text based answers",
"text": "Text based answers"
},
{
"value": "Date field",
"text": "Date field"
},
{
"value": "Numeric field",
"text": "Numeric field"
},
{
"value": "Picklist values",
"text": "Picklist values"
},
{
"value": "Multiple Choice",
"text": "Multiple Choice"
}
]
}
},
"OwnerId": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Owner ID",
"editable": true,
"additionalInfo": {
"apiname": "OwnerId",
"referenceTo": "Group"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Question",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "compliancequest__Question_With_Tasks__r",
"field": "compliancequest__SQX_Question__c",
"childSObject": "compliancequest__SQX_Question_With_Task__c"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
}
]
}
SQXSchema.compliancequest__SQX_SQR_Answer__c={
"sObjectName": "compliancequest__SQX_SQR_Answer__c",
"sObjectLabel": "Answer",
"id": "Id",
"fields": {
"compliancequest__Answer__c": {
"validation": {
"required": true,
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Answer",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Answer__c",
"multiline": true
}
},
"compliancequest__SQX_Question_With_Task__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Question With Task",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Question_With_Task__c",
"referenceTo": "compliancequest__SQX_Question_With_Task__c"
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__User__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "User",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__User__c",
"referenceTo": "User"
}
},
"compliancequest__Completed_Date__c": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Completed Date",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Completed_Date__c"
}
},
"compliancequest__SQX_SQR_Approval__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "SQR Approval",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_SQR_Approval__c",
"referenceTo": "compliancequest__SQX_SQR_Approval__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Answer Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
}
]
}
SQXSchema.compliancequest__SQX_SQR_Uploaded_Attachment__c={
"sObjectName": "compliancequest__SQX_SQR_Uploaded_Attachment__c",
"sObjectLabel": "Upload Attachment",
"id": "Id",
"fields": {
"compliancequest__Description__c": {
"validation": {
"maxlength": 255
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": true,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": true
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__SQX_Attachment_With_Task__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Attachment With Task",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Attachment_With_Task__c",
"referenceTo": "compliancequest__SQX_Attachment_With_Task__c"
}
},
"compliancequest__SQX_SQR_Approval__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "SQR Approval",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_SQR_Approval__c",
"referenceTo": "compliancequest__SQX_SQR_Approval__c"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Upload Attachment Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
}
},
"childRelationships": [
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
}
]
}
SQXSchema.compliancequest__SQX_Question_With_Task__c={
"sObjectName": "compliancequest__SQX_Question_With_Task__c",
"sObjectLabel": "Question With Task",
"id": "Id",
"fields": {
"compliancequest__Description__c": {
"validation": {
"maxlength": 1300
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": false
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__SQX_Task__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Task",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_Task__c",
"referenceTo": "compliancequest__SQX_Task__c"
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Question With Task Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
},
"compliancequest__SQX_Question__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Question",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Question__c",
"referenceTo": "compliancequest__SQX_Question__c"
}
}
},
"childRelationships": [
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "compliancequest__Answers__r",
"field": "compliancequest__SQX_Question_With_Task__c",
"childSObject": "compliancequest__SQX_SQR_Answer__c"
}
]
}
SQXSchema.compliancequest__SQX_Attachment_With_Task__c={
"sObjectName": "compliancequest__SQX_Attachment_With_Task__c",
"sObjectLabel": "Attachment With Task",
"id": "Id",
"fields": {
"compliancequest__SQX_Attachment__c": {
"validation": {},
"type": "reference",
"nullable": null,
"name": "Attachment",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__SQX_Attachment__c",
"referenceTo": "compliancequest__SQX_Attachment__c"
}
},
"compliancequest__Description__c": {
"validation": {
"maxlength": 1300
},
"type": "string",
"nullable": null,
"name": "Description",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "compliancequest__Description__c",
"multiline": false
}
},
"Id": {
"validation": {
"required": true
},
"type": "string",
"nullable": null,
"name": "Record ID",
"editable": false,
"additionalInfo": {
"apiname": "Id"
}
},
"compliancequest__Required__c": {
"validation": {},
"type": "boolean",
"nullable": null,
"name": "Required",
"editable": true,
"additionalInfo": {
"apiname": "compliancequest__Required__c"
}
},
"compliancequest__SQX_Task__c": {
"validation": {
"required": true
},
"type": "reference",
"nullable": null,
"name": "Task",
"editable": false,
"additionalInfo": {
"apiname": "compliancequest__SQX_Task__c",
"referenceTo": "compliancequest__SQX_Task__c"
}
},
"LastActivityDate": {
"validation": {},
"type": "date",
"nullable": true,
"name": "Last Activity Date",
"editable": false,
"additionalInfo": {
"apiname": "LastActivityDate"
}
},
"Name": {
"validation": {
"maxlength": 80
},
"type": "string",
"nullable": null,
"name": "Attachment With Task Name",
"editable": false,
"additionalInfo": {
"richtext": false,
"apiname": "Name",
"multiline": false
}
}
},
"childRelationships": [
{
"relationshipName": "Notes",
"field": "ParentId",
"childSObject": "Note"
},
{
"relationshipName": "TopicAssignments",
"field": "EntityId",
"childSObject": "TopicAssignment"
},
{
"relationshipName": "Attachments",
"field": "ParentId",
"childSObject": "Attachment"
},
{
"relationshipName": "compliancequest__Upload_Attachments__r",
"field": "compliancequest__SQX_Attachment_With_Task__c",
"childSObject": "compliancequest__SQX_SQR_Uploaded_Attachment__c"
}
]
}