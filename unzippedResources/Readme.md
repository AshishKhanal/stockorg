Compliance Quest - Supplier Quality Exchange Code Repository
=============================================================

Static Resource How To
---------------

Static Resources have to be placed in the corresponding directory based on their name. Example
   if a static resource named cqui.resource exists 

   then a folder containing cqui.resource containing the unzipped resource

All changes should be placed there
If any pre-build compilations such as minification are to be performed a corresponding bat file must be created Eg. cqui.resource.bat 

The pre-build entity will be compiled then zipped if the .resource folder contains multiple objects else a single object .resource file is created