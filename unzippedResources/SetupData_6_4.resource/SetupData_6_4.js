[
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-1"
        },
        "Name": "Controlled Document",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "DOC-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-1"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-2"
        },
        "Name": "Template Document",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "DOT-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-2"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-3"
        },
        "Name": "Audit Criteria",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "CRT-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-3"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-4"
        },
        "Name": "Audit Program",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "APG-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-4"
    }
]