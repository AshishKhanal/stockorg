[
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-7"
        },
        "Name": "Supplier Introduction",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "SI-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-7"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-8"
        },
        "Name": "Supplier Deviation",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "SD-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-8"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-9"
        },
        "Name": "Supplier Document",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "SUD-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-9"
    },
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-10"
        },
        "Name": "Supplier Escalation",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "SE-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-10"
    },    
    {
        "attributes": {
            "type": "compliancequest__SQX_Auto_Number__c",
            "url": "/services/data/v36.0/sobjects/compliancequest__SQX_Auto_Number__c/AutoNumber-11"
        },
        "Name": "Supplier Interaction",
        "compliancequest__Next_Number__c": 0,
        "compliancequest__Number_Format__c": "ST-{1}",
        "compliancequest__Numeric_Format__c": "000000",
        "Id": "AutoNumber-11"
    }
]