/*jshint browser: true */
/*global window document */
/*eslint no-undef: "error"*/
/**
 * This module adds support for exporting and importing data to and from kendo spreadsheet
 * into CQ data source
 */

// TODO: Revisit the code and remove duplicate code as part of new release
var cq = (function (jQ, kendo, cq, translate) {
    "use strict";

    var spreadsheet = {},
        dependentMap = {}, //stores the list of fields that have dependency along with the name of the dependent field
        columnIndicesMap = {}, //stores the index of the column based on the spreadsheet column definition
        _columnMap = {}, // temporarily stores the index of the columns based on the key
        _indices = {}, // temporarily stores the header name and its index
        _fieldMap = {}, // temporarily stores the field and its valid values
        _mergedCells = [], // temporarily stores the mergedCells in sheet
        _requiredFields = {}, // temporarily stores the required fields
        existingRecords = {},
        _getUrl = window.location,
        _baseUrl = _getUrl.protocol + "//" + _getUrl.host + "/" + _getUrl.pathname.split('/')[0],
        NO_VALUE = "",
        REQUIRED_INDICATOR = "(*)",
        mainObjApiName,
        childObjApiName;

    /**
     * Returns the default configuration used by the spreadsheet to initialize itself.
     */
    function defaultConfig() {
        return { sheetsbar: false, sheets: [] };
    }

    /**
     * Handy method to add error message cell so that it is easier to debug
     * @param {String} errorDescription Error message to be added
     */
    function addError(errorDescription) {
        if (window.console) {
            window.console.error(errorDescription);
        }
    }

    /**
     * Gets the data source for the given parameters
     * @param {Object} sourceName the name of the source to identify
     * @param {Object} record the main record that contains the source
     * @return returns the data source for the sourcename in the record. Sets error if nothing is found
     */
    function getSource(sourceName, record) {
        var source = null;
        if (!sourceName || (!record[sourceName] && !record.relatedList)) {
            addError("Sheet doesn't have source configured. Source provided: " + sourceName);
        } else {
            source = (record.relatedList && record.relatedList(sourceName)) || record[sourceName];
            if (jQ.isFunction(source)) {
                source = source.call(record);
            }

            if (source === undefined || source === null) {
                addError("Sheet's source couldn't be found. Source provided: " + sourceName);
            }
        }

        return source;
    }

    /**
    * method to return child object name from parent schema
    * @param {object} schema parent schema
    * @param {object} relationshipName relationship name of the child
    * @return returns childSObject name of the object
    */
    function getChildObjectName(schema, relationshipName) {
        return schema.childRelationships.filter(function (e) {
            return e.relationshipName === relationshipName;
        })[0].childSObject;
    }

    /**
     * This method is used to check if field is required
     * @param {Object} schema  schema containing definition of the object
     * @param {String} field field name to check required
     * @return returns true if field is required else false
     */
    function isFieldRequired(schema, field) {
        return schema.fields[field] && schema.fields[field].validation &&
            schema.fields[field].validation.required === true;
    }
    /**
     * Builds the header row from provided configuration
     * @param {*} globalSchema the master schema containing definition of all the fields.
     * @param {*} recordSchema the schema of the record that is being manipulated
     * @param {*} cqSheet the configuration provided by the user to generate the schema
     * @return returns the header row with cells information in it
     */
    function buildHeaderRow(globalSchema, recordSchema, cqSheet) {
        var indices = _indices,
            sheetColumns = cqSheet.columns,
            column,
            i,
            j,
            field,
            fieldName,
            childColumn,
            validValues,
            childObjSchema,
            headerRows = [],
            cells = [],
            promiseList = [],
            fieldValue,
            requiredFields = _requiredFields;
        try {
            _columnMap.mainObjectApiName = recordSchema.sObjectName;
            mainObjApiName = _columnMap.mainObjectApiName;

            for ( i = 0; i < sheetColumns.length; i += 1) {
                column = sheetColumns[i];
                if (column.Id) {
                    field = column.Id;
                    _columnMap.sourceIdField = field;
                    fieldName = recordSchema.fields[field].name;
                    indices[field] = cells.length;
                    _columnMap.sourceIdIndex = cells.length;
                    cells.push({ value: fieldName });
                    columnIndicesMap[field] = cells.length - 1;
                }
                else if (column.field) {
                    field = column.field;
                    fieldValue = column.value;
                    fieldName = recordSchema.fields[field].name;
                    requiredFields[fieldValue] = isFieldRequired(recordSchema, field)
                        || (column.required !== undefined && column.required);
                    indices[fieldValue || field] = cells.length;
                    cells.push({ value: requiredFields[fieldValue] ? REQUIRED_INDICATOR + fieldName : fieldName});

                    if(recordSchema.fields[field].additionalInfo) {

                        if(recordSchema.fields[field].additionalInfo.dependsOn) {
                            dependentMap[recordSchema.sObjectName + '.' + column.value] = {
                                field : recordSchema.fields[field].additionalInfo.dependsOn,
                                linkingField: recordSchema.fields[field].additionalInfo.dependsOnField
                            };
                        } else if(recordSchema.fields[field].additionalInfo.controllingField) {
                            dependentMap[recordSchema.sObjectName + '.' + column.field] = {
                                controllingField : recordSchema.fields[field].additionalInfo.controllingField
                            };
                        }
                    }
                    promiseList.push(_getValidValues(recordSchema, fieldValue, indices[field], indices[field]));

                    columnIndicesMap[field] = cells.length - 1;

                } else if (column.relation) {

                    if (typeof _columnMap.relationStartIndex === 'undefined') {
                        _columnMap.relationStartIndex = cells.length;
                    }
                    childObjSchema = globalSchema[getChildObjectName(recordSchema, column.relation)];
                    _columnMap.relatedObjectApiName = childObjSchema.sObjectName;
                    childObjApiName = column.relation;
                    _columnMap.relationSourceName = column.relation;
                    for (j = 0; j < column.columns.length; j += 1) {
                        childColumn = column.columns[j];
                        if (childColumn.Id) {
                            field = childColumn.Id;
                            _columnMap.relationIdField = field;
                            fieldName = childObjSchema.fields[field].name;
                            indices[column.relation + '.' + field] = cells.length;
                            _columnMap.relationIdIndex = cells.length;
                            cells.push({ value: fieldName });
                            columnIndicesMap[field] = cells.length - 1;
                        }
                        else if (childColumn.field) {
                            requiredFields.relation = requiredFields.relation || {};
                            field = childColumn.field;
                            fieldValue = childColumn.value;
                            fieldName = childObjSchema.fields[field].name;
                            requiredFields.relation[fieldValue] = isFieldRequired(childObjSchema, field)
                                || (childColumn.required !== undefined && childColumn.required);
                            indices[column.relation + '.' + (fieldValue || field)] = cells.length;
                            cells.push({ value: requiredFields.relation[fieldValue] ? REQUIRED_INDICATOR + fieldName : fieldName });
                            promiseList.push(_getValidValues(childObjSchema, fieldValue, indices[column.relation + '.' + field], indices[column.relation + '.' + field]));

                            if(childObjSchema.fields[field].additionalInfo) {
                                if(childObjSchema.fields[field].additionalInfo.dependsOn) {
                                    dependentMap[childObjSchema.sObjectName + '.' + childColumn.value] = {
                                        field : column.relation + '.' + recordSchema.fields[field].additionalInfo.dependsOn,
                                        linkingField: childObjSchema.fields[field].additionalInfo.dependsOnField
                                    };
                                } else if(childObjSchema.fields[field].additionalInfo.controllingField) {
                                    dependentMap[childObjSchema.sObjectName + '.' + childColumn.field] = {
                                        controllingField : column.relation + '.' + childObjSchema.fields[field].additionalInfo.controllingField
                                    };
                                }
                            }
                            columnIndicesMap[column.relation + '.' + field] = cells.length - 1;
                        }
                    }
                    _columnMap.relationEndIndex = cells.length;
                }
            }
            if (cqSheet.pivotField) {
                requiredFields[cqSheet.pivotField] = isFieldRequired(recordSchema, _removeReferenceField(cqSheet.pivotField));
                requiredFields[cqSheet.pivotValue] = isFieldRequired(recordSchema, _removeReferenceField(cqSheet.pivotValue));
                _columnMap.pivotStartIndex = cells.length;
                _columnMap.defaultPivotValue = cqSheet.defaultPivotValue;
                validValues = recordSchema.fields[cqSheet.pivotField].additionalInfo.validValues;
                for (i = 0; i < validValues.length; i+= 1) {
                    indices[validValues[i].value] = cells.length;
                    cells.push({ value: validValues[i].text });
                }
                _columnMap.pivetEndIndex = cells.length;
                _columnMap.pivotField = cqSheet.pivotField;
                _columnMap.pivotValueField = cqSheet.pivotValue;
                promiseList.push(_getValidValues(recordSchema, _columnMap.pivotValueField, _columnMap.pivotStartIndex, _columnMap.pivotStartIndex + validValues.length - 1));
            }


            headerRows.push({ cells: cells });

            _columnMap.dataRowStartIndex = _columnMap.dataRowStartIndex || headerRows.length;

            return Promise.all(promiseList).then(function () {
                return headerRows;
            }, function (error) {
                addError(error);
            });
        }
        catch (e) {
            addError(e);
        }
    }

    /**
     * Creates data rows from datasource and related objects
     * @param {CQDataSource} source the data source that contains list of records
     * @param {Object} _columnMap the map containing the index of each column in the record
     * @param {Object} cqSheet actual configuration that must be used to guide the construction of data
     * @param {Object} colorPalette list of color codes
     * @return returns list of kendo rows i.e. cells to be used in spreadsheet
     */
    function createDataRows(source, objSchema, cqSheet, colorPalette) {
        var rows = [],
            data,
            cells,
            datum,
            column,
            dataIndex,
            columnIndex,
            pivotColumnIndex,
            pivotValue,
            pivotDatum,
            relationalColumn,
            relationalColumnIndex,
            relationalSource,
            relationalDatum,
            relationalData,
            relationalDataIndex,
            relationalCells,
            cellsString,
            relationalFieldIndex,
            relationStartIndex,
            relationalChunk,
            beforeChunk,
            afterChunk,
            mapPivotColorPalette = {},
            paletteIndex = 0,
            dataRowStartIndex = _columnMap.dataRowStartIndex,
            pivotFieldName,
            childSchema,
            indices = _indices,
            paletteValue,
            defaultPivotValue = _columnMap.defaultPivotValue,
            recordId,
            childRecordId,
            fieldMap = _fieldMap,
            picklistValue;

        try {
            _columnMap.pivotColumnMap = {};
            data = source.view();
            for (dataIndex = 0; dataIndex < data.length; dataIndex++) {
                cells = [];
                if (data[dataIndex][cqSheet.pivotField]) {
                    datum = data[dataIndex];

                    for (columnIndex = 0; columnIndex < cqSheet.columns.length; columnIndex += 1) {

                        picklistValue = undefined;

                        column = cqSheet.columns[columnIndex];
                        if (column.Id) {
                            recordId = datum.get(column.Id);
                            cells.push({ value: recordId });
                            existingRecords[recordId] = {};
                        } else if (column.field) {
                            if (_getFieldType(objSchema, column.field) === 'reference') {
                                if (datum.get(column.field)) {
                                    cells.push({ value : datum.get(column.value || column.field), link: _baseUrl+datum.get(column.field) });
                                } else {
                                    cells.push({ value: null });
                                }
                            } else if (_getFieldType(objSchema, column.field) === 'values') {
                                datum[column.field + "_ValidValues"]._view.filter(function (e, i) {
                                    if (e.value === datum.get(column.field) && !picklistValue) {
                                        picklistValue = e.text;
                                    }
                                });
                                if (picklistValue) {
                                    cells.push({ value: picklistValue });
                                } else {
                                    cells.push({ value: datum.get(column.value || column.field) });
                                }
                            } else {
                                cells.push({ value: datum.get(column.value || column.field) });
                            }
                            existingRecords[recordId][column.value || column.field] = datum.get(column.value || column.field);

                        } else if (column.relation) {
                            relationalColumn = column;
                            //adds empty place holder cells for relationship
                            for (relationalColumnIndex = 0; relationalColumnIndex < column.columns.length; relationalColumnIndex++) {
                                cells.push({});
                            }
                        }
                    }

                    if (cqSheet.pivotField) {
                        pivotValue = datum.get(cqSheet.pivotValue);
                        pivotDatum = datum[cqSheet.pivotField];
                        pivotColumnIndex = indices[pivotDatum];
                        pivotFieldName = _removeReferenceField(cqSheet.pivotValue);
                        paletteValue = datum.get(pivotFieldName);

                        existingRecords[recordId][cqSheet.pivotValue || pivotFieldName] = datum.get(cqSheet.pivotValue || pivotFieldName);
                        existingRecords[recordId][cqSheet.pivotField] = datum.get(cqSheet.pivotField);

                        if (datum.get(pivotFieldName) === undefined) {
                            _columnMap.pivotColumnMap[cells[_columnMap.sourceIdIndex].value] = pivotDatum;
                        }
                        else if (_getFieldType(objSchema, cqSheet.pivotValue) === 'reference') {
                            _columnMap.pivotColumnMap[cells[_columnMap.sourceIdIndex].value + '-' + pivotDatum] = datum.get(pivotFieldName);
                        } else {
                            _columnMap.pivotColumnMap[cells[_columnMap.sourceIdIndex].value + '-' + pivotDatum] = pivotValue;
                        }
                        if (pivotColumnIndex) {
                            //pad the cells to start index
                            while (cells.length < pivotColumnIndex) {
                                cells.push({});
                            }
                            if (paletteValue && pivotValue) {
                                if (colorPalette.length) {
                                    if (colorPalette[paletteIndex] === undefined) {
                                        paletteIndex = 0;
                                    }
                                    if (mapPivotColorPalette[paletteValue] !== undefined) {
                                        if (_getFieldType(objSchema, cqSheet.pivotValue) === 'reference') {
                                            cells.push({ value: pivotValue, background: mapPivotColorPalette[paletteValue], link: _baseUrl + datum.get(pivotFieldName) });
                                        } else {
                                            cells.push({ value: pivotValue, background: mapPivotColorPalette[paletteValue] });
                                        }
                                        dataRowStartIndex++;
                                    } else {
                                        mapPivotColorPalette[paletteValue] = colorPalette[paletteIndex];
                                        if (_getFieldType(objSchema, cqSheet.pivotValue) === 'reference') {
                                            cells.push({ value: pivotValue, background: mapPivotColorPalette[paletteValue], link: _baseUrl + datum.get(pivotFieldName) });
                                        } else {
                                            cells.push({ value: pivotValue, background: mapPivotColorPalette[paletteValue] });
                                        }
                                        paletteIndex++;
                                        dataRowStartIndex++;
                                    }
                                } else {
                                    cells.push({ value: pivotValue, link: _baseUrl + datum.get(pivotFieldName) });
                                    dataRowStartIndex++;
                                }
                            } else {
                                cells.push({ value: defaultPivotValue });
                                dataRowStartIndex++;
                            }
                        } else {
                            addError("Pivot column doesn't match with pivot data for " + JSON.stringify(datum) + " . Missing value: " + pivotDatum);
                        }
                    }

                    if (relationalColumn) {
                        relationalSource = getSource(relationalColumn.relation, datum);
                        relationalData = relationalSource.view();
                        cellsString = JSON.stringify(cells);
                        childSchema = relationalSource.options.schema.model.fn;
                        if (relationalData.length == 0) {
                            rows.push({ cells: cells });
                        } else {
                            existingRecords[recordId].relation = {};
                            for (relationalDataIndex = 0; relationalDataIndex < relationalData.length; relationalDataIndex++) {
                                relationalCells = JSON.parse(cellsString);
                                relationalDatum = relationalData[relationalDataIndex];
                                relationalChunk = [];
                                for (relationalFieldIndex = 0; relationalFieldIndex < relationalColumn.columns.length; relationalFieldIndex++) {
                                    column = relationalColumn.columns[relationalFieldIndex];
                                    if (column.Id !== undefined) {
                                        childRecordId = relationalDatum.get(column.Id);
                                        relationalChunk.push({ value: childRecordId });
                                        existingRecords[recordId]['relation'][childRecordId] = {};
                                    } else {
                                        if (_getFieldType(childSchema, column.field) == 'reference') {
                                            if(relationalDatum.get(column.field)) {
                                                relationalChunk.push({ value : relationalDatum.get(column.value || column.field), link: _baseUrl+relationalDatum.get(column.field) });
                                            } else {
                                                relationalChunk.push({ value: null });
                                            }
                                        } else {
                                            relationalChunk.push({ value: relationalDatum.get(column.value || column.field) });
                                        }
                                        existingRecords[recordId]['relation'][childRecordId][column.value || column.field] = relationalDatum.get(column.value || column.field);
                                    }
                                }
                                relationStartIndex = indices[relationalColumn.relation + '.' +
                                    (relationalColumn.columns[0].value || relationalColumn.columns[0].field || relationalColumn.columns[0].Id)];
                                beforeChunk = relationalCells.slice(0, relationStartIndex);
                                afterChunk = relationalCells.slice(relationStartIndex + relationalChunk.length);
                                relationalCells = beforeChunk.concat(relationalChunk).concat(afterChunk);

                                rows.push({ cells: relationalCells });
                            }
                        }
                    } else {
                        rows.push({ cells: cells });
                    }
                }
            }
            return rows;
        }
        catch (e) {
            addError(e);
        }
    }

    /**
     * Maps CQ's Sheet Configuration to Kendo's Sheet Configuration
     * @param {Object} cqSheet cq sheet configuration containing details about object and fields
     * @param {Object} record that should be used as the starting point to initialize things
     * @param {Object} globalSchema contains all the details (metadata) of objects
     * @param {Object} colorPalette color palette for coloring pivot cells
     * @param {Object} properties styling properties for cells and headers
     * @return returns kendo sheet
     */
    function convertFromCQToKendoSheet(cqSheet, record, globalSchema, colorPalette, properties) {
        // step 1: Identify the source
        var sheet = {},
            source = getSource(cqSheet.source, record),
            objSchema,
            rows = [],
            headerRows;
        
        if (cqSheet.sort){
            source.sort(cqSheet.sort);
            source.read();
        }
        if (source) {
            objSchema = source.options.schema.model.fn;

            //1. Naming the sheet correctly (From the model if it has schema, else set it same as source name)
            sheet.name = objSchema.sObjectLabel || cqSheet.source;
            sheet.columns = [];


            //2. Go through the columns and populate it
            return buildHeaderRow(globalSchema, objSchema, cqSheet).then(function (response) {

                headerRows = updateHeaderRow(response, globalSchema, properties, objSchema);

                rows = rows.concat(headerRows);

                rows = rows.concat(createDataRows(source, objSchema, cqSheet, colorPalette));

                //mergeSimilarCells(rows, properties);

                rows = setupCustomEditor(rows, properties);

                sheet.rows = rows;

                sheet.mergedCells = _mergedCells;

                sheet.frozenRows = _columnMap.dataRowStartIndex;

                return sheet;
            }, function (error) {
                addError(error);
            });
        }
    }

    /**
     * Method used to merge vertical cells which have similar values
     * @param {Object} rows data rows
     * @param {Object} properties sheet styling to be added
     */
    function mergeSimilarCells(rows, properties) {
        var rowIndex,
            cellIndex,
            row,
            cell,
            cellIndexMap = {},
            mergedCells = _mergedCells,
            ranges;

        for (rowIndex = _columnMap.dataRowStartIndex; rowIndex < rows.length; rowIndex++) {
            row = rows[rowIndex];
            for (cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
                cell = row.cells[cellIndex];
                if (cell.value !== undefined) {
                    jQ.each(properties.defaultCellProperties, function (key, value) {
                        cell[key] = value;
                    });
                    if (cellIndexMap[cellIndex + '-' + cell.value] !== undefined) {
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    } else {
                        cellIndexMap[cellIndex + '-' + cell.value] = [];
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    }
                } {
                    jQ.each(properties.defaultCellProperties, function (key, value) {
                        cell[key] = value;
                    });
                    if (cellIndexMap[cellIndex + '-' + cell.value] !== undefined) {
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    } else {
                        cellIndexMap[cellIndex + '-' + cell.value] = [];
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    }
                } {
                    jQ.each(properties.defaultCellProperties, function (key, value) {
                        cell[key] = value;
                    });
                    if (cellIndexMap[cellIndex + '-' + cell.value] !== undefined) {
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    } else {
                        cellIndexMap[cellIndex + '-' + cell.value] = [];
                        cellIndexMap[cellIndex + '-' + cell.value].push('R' + (rowIndex + 1) + 'C' + (cellIndex + 1));
                    }
                }
            }
        }
        jQ.each(cellIndexMap, function (key) {
            ranges = cellIndexMap[key];
            if (ranges.length > 1) {
                mergedCells.push(ranges[0] + ':' + ranges[ranges.length - 1]);
            }
        });
    }

    /**
     * Method to update header rows and merge relationship header
     * @param {Object} rows rows containing header cells and values
     * @param {Object} globalSchema the global schema containing every object description
     * @param {Object} properties styling for the cells and rows
     * @param {Object} objSchema schema of the parent schema
     * @return returns header row after configuration
     */
    function updateHeaderRow(rows, globalSchema, properties, objSchema) {
        var tempRows = [],
            tempCell = {},
            tempRow = {},
            mergedCells = _mergedCells,
            requiredFields = _requiredFields,
            range = '',
            mergedRow,
            relationName,
            rowsIndex,
            cellsIndex,
            headerTempRow = [];
        if (properties.titleCellProperties !== undefined) {
            jQ.each(properties.titleCellProperties, function (key, value) {
                tempCell[key] = value;
            });
        }

        if (properties.titleRowProperties !== undefined) {
            jQ.each(properties.titleRowProperties, function (key, value) {
                tempRow[key] = value;
            });
        }

        tempRow.cells = [];
        tempRow.cells.push(tempCell);
        tempRows.push(tempRow);
        mergedCells.push(setupRangeStructure(1, 1, _columnMap.pivetEndIndex, 1));
        _columnMap.dataRowStartIndex++;
        if (_columnMap.relationStartIndex == _columnMap.relationEndIndex) {
            mergedRow = _columnMap.dataRowStartIndex;
            _columnMap.dataRowStartIndex++;
            range = setupRangeStructure(1, mergedRow, _columnMap.pivotStartIndex, mergedRow);
            mergedCells.push(range);
            range = setupRangeStructure(_columnMap.pivotStartIndex + 1, mergedRow, _columnMap.pivetEndIndex, mergedRow);
            mergedCells.push(range);
            headerTempRow.push({
                cells: [
                    { index: 0, value: objSchema.sObjectLabel },
                    { index: _columnMap.pivotStartIndex, 
                        value: requiredFields[_columnMap.pivotField] ? 
                        REQUIRED_INDICATOR + objSchema.fields[_columnMap.pivotField].name : objSchema.fields[_columnMap.pivotField].name }
                ]
            });
        }else{
            mergedRow = _columnMap.dataRowStartIndex;
            _columnMap.dataRowStartIndex++;
            range = setupRangeStructure(1, mergedRow, _columnMap.relationStartIndex, mergedRow);
            mergedCells.push(range);
            range = setupRangeStructure(_columnMap.relationStartIndex + 1, mergedRow, _columnMap.relationEndIndex, mergedRow);
            mergedCells.push(range);
            range = setupRangeStructure(_columnMap.relationEndIndex + 1, mergedRow, _columnMap.pivetEndIndex, mergedRow);
            mergedCells.push(range);
            relationName = getChildObjectName(objSchema, _columnMap.relationSourceName);
            headerTempRow.push({
                cells: [
                    { index: 0, value: objSchema.sObjectLabel },
                    { index: _columnMap.relationStartIndex, value: globalSchema[relationName].sObjectLabel },
                    {
                        index: _columnMap.pivotStartIndex,
                        value: requiredFields[_columnMap.pivotField] ?
                            REQUIRED_INDICATOR + objSchema.fields[_columnMap.pivotField].name : objSchema.fields[_columnMap.pivotField].name
                    }
                ]
            });
        }
        headerTempRow = headerTempRow.concat(rows);

        if (properties.titleMenuProperties !== undefined) {
            for (rowsIndex = 0; rowsIndex < headerTempRow.length; rowsIndex++) {
                tempRow = headerTempRow[rowsIndex];
                for (cellsIndex = 0; cellsIndex < tempRow.cells.length; cellsIndex++) {
                    tempCell = tempRow.cells[cellsIndex];
                    jQ.each(properties.titleMenuProperties, function (key, value) {
                        tempCell[key] = value;
                    });
                }
            }
        }

        rows = tempRows.concat(headerTempRow);

        if (properties.headerProperties !== undefined) {
            for (rowsIndex = 0; rowsIndex < rows.length; rowsIndex++) {
                tempRow = rows[rowsIndex];
                for (cellsIndex = 0; cellsIndex < tempRow.cells.length; cellsIndex++) {
                    tempCell = tempRow.cells[cellsIndex];
                    jQ.each(properties.headerProperties, function (key, value) {
                        tempCell[key] = value;
                    });
                }
            }
        }
        return rows;
    }

    /**
     * Mthod used to create range in RC notation
     * @param {Number} col1 start column index
     * @param {Number} row1 start row index
     * @param {Number} col2 end column index
     * @param {Number} row2 end row index
     * @return returns RC structure range
     */
    function setupRangeStructure(col1, row1, col2, row2) {
        return 'R' + row1 + 'C' + col1 + ':' + 'R' + row2 + 'C' + col2;
    }

    /**
     * Setup validation for cells
     * @param {Object} rows current populated rows
     * @param {Object} cqSheet cq sheet configuration containing details about object and fields
     * @return returns rows with cells with validations
     */
    function setupCustomEditor(rows, properties) {
        var rowIndex,
            cellIndex,
            row,
            apiName = [],
            fieldName;

        for (var cKey in _indices) {
            apiName[_indices[cKey]] = cKey;
        }
        for (rowIndex = _columnMap.dataRowStartIndex; rowIndex < properties.defaultSheetProperties.rows; rowIndex++) {
            if (typeof rows[rowIndex] != "undefined") {
                row = rows[rowIndex];
            } else {
                rows.push({ cells: [] });
                row = rows[rowIndex];
            }
            for (cellIndex = 0; cellIndex < _columnMap.pivetEndIndex; cellIndex++) {
                if (cellIndex == _columnMap.sourceIdIndex || cellIndex == _columnMap.relationIdIndex) {
                    continue;
                }
                if (cellIndex >= _columnMap.pivotStartIndex) {
                    if (typeof row.cells[cellIndex] !== "undefined") {
                        jQ.extend(row.cells[cellIndex], { editor: _columnMap.mainObjectApiName + '.' + _columnMap.pivotValueField });
                    } else {
                        row.cells.push({ editor: _columnMap.mainObjectApiName + '.' + _columnMap.pivotValueField, index: cellIndex });
                    }
                } else if (cellIndex >= _columnMap.relationStartIndex && cellIndex < _columnMap.relationEndIndex) {
                    fieldName = apiName[cellIndex];
                    fieldName = fieldName.substr(fieldName.indexOf('.') + 1);

                    if(typeof row.cells[cellIndex] !== "undefined"){
                        jQ.extend(row.cells[cellIndex], {editor: _columnMap.relatedObjectApiName + '.' + fieldName});
                    }else{
                        row.cells.push({editor: _columnMap.relatedObjectApiName + '.' + fieldName, index: cellIndex});
                    }
                }else{
                    if(typeof row.cells[cellIndex] !== "undefined"){
                        jQ.extend(row.cells[cellIndex], {editor: _columnMap.mainObjectApiName + '.' + apiName[cellIndex]});
                    }else{
                        row.cells.push({editor: _columnMap.mainObjectApiName + '.' + apiName[cellIndex], index: cellIndex});
                    }
                }
            }
        }
        return rows;
    }

    /**
     * Takes a kendo worksheet and extracts information related to data(model)
     * @param {Object} worksheet the worksheet that needs to be processed
     * @param {Object} source the data source containing all objects
     * @param {Object} sheetConfig the configuration detail for the sheet
     * @param {Object} defaultValues default values for custom config to be set in records
     * @param {Object} globalSchema the global schema containing every object description
     */
    function sheetJSONToObjects(worksheet, source, sheetConfig, defaultValues, globalSchema) {
        var wsJSON,
            dataRows,
            dataMap = {},
            apiName = {},
            objSchema,
            parentData,
            childData,
            relatedSource,
            childSchema,
            errorMsgs = {},
            matchlength,
            matchValue,
            indices = _indices,
            rowsIndex = {},
            children = [],
            dataMap;

        try {
            objSchema = source.options.schema.model.fn;
            childSchema = globalSchema[getChildObjectName(objSchema, _columnMap.relationSourceName)];
            _columnMap.highestRowIndex = -1;
            parentData = source.view().slice(0); // audits from audit program
            wsJSON = worksheet.toJSON();

            dataRows = wsJSON.rows.filter(function (i) {
                return i.index && i.index >= _columnMap.dataRowStartIndex;
            });
            if (dataRows) {
                wsJSON = _duplicateMergedCellValues(wsJSON.mergedCells, worksheet);
                dataRows = wsJSON.rows.filter(function (i) {
                    return i.index && i.index >= _columnMap.dataRowStartIndex;
                });
            }

            for (var cKey in indices) {
                apiName[indices[cKey]] = cKey;
            }

            dataMap = _createRowDataMap(dataRows, apiName, objSchema, childSchema, rowsIndex);
            for (var key in dataMap) {
                var obj = dataMap[key],
                    existing,
                    relatedObjs,
                    existingRelation,
                    existingChild,
                    relatedObjIndex,
                    isSFId = new RegExp("^([a-zA-Z0-9]{18})$"),
                    requiredIndex = 0,
                    currentRow = rowsIndex[key],
                    rowIndex = 0,
                    i,
                    lowerCaseValue;

                _checkRequiredFieldValues(currentRow, obj, objSchema, childSchema, errorMsgs);

                if (!hasError(errorMsgs, currentRow[requiredIndex])) {
                    if (obj.filter) {
                        var ds = new kendo.data.DataSource({ data: parentData });
                        ds.filter(obj.filter);
                        existing = ds.view()[0];

                        if (existing) {
                            if (parentData.length) {
                                parentData.splice(parentData.indexOf(existing), 1); // remove records which are available in view
                            }
                            jQ.each(obj.data, function (key, value) {
                                if (value === NO_VALUE) {
                                    existing.set(_removeReferenceField(key), null);
                                }
                                else if (isSFId.test(value)) {
                                    key = _removeReferenceField(key);
                                    if (existing.get(key) !== value) {
                                        existing.set(key, value);
                                    }
                                }
                                else if (existing.get(key) !== value) {
                                    matchlength = 0;
                                    matchValue = '';
                                    key = _removeReferenceField(key);
                                    if (_fieldMap[key] !== undefined && key !== _columnMap.pivotField) {
                                        lowerCaseValue = typeof value == "string" ? value.toLowerCase() : value;
                                        if (Array.isArray(_fieldMap[key][lowerCaseValue])) {
                                            errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(objSchema, key)).replace('{value}', value));
                                        } else if (_fieldMap[key][lowerCaseValue] !== undefined) {
                                            existing.set(key, _fieldMap[key][lowerCaseValue]);
                                        } else if (lowerCaseValue.length >= 4) {
                                            jQ.each(_fieldMap[key], function (k, v) {
                                                if (k.includes(lowerCaseValue)) {
                                                    matchlength++;
                                                    matchValue = v;
                                                }
                                            });
                                            if (matchlength === 1) {
                                                if (Array.isArray(matchValue)) {
                                                    errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(objSchema, key)).replace('{value}', value));
                                                } else {
                                                    existing.set(key, matchValue);
                                                }
                                            } else if (matchlength > 1) {
                                                errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(objSchema, key)).replace('{value}', value));
                                            } else {
                                                errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(objSchema, key)));
                                            }
                                        } else {
                                            errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(objSchema, key)));
                                        }
                                    } else {
                                        existing.set(key, value);
                                    }
                                }
                            });

                            relatedSource = getSource(_columnMap.relationSourceName, existing);

                            childData = relatedSource.view().slice(0);
                            relatedObjs = obj.relation;

                            if (relatedObjs.length || childData.length) {
                                children.push(relatedSource);

                                for (relatedObjIndex = 0; relatedObjIndex < relatedObjs.length; relatedObjIndex++) {
                                    existingRelation = relatedObjs[relatedObjIndex];
                                    if (existingRelation.filter && !jQ.isEmptyObject(existingRelation.filter)) {
                                        var cds = new kendo.data.DataSource({ data: relatedSource.view() });
                                        cds.filter(existingRelation.filter);
                                        existingChild = cds.view()[0];
                                        if (existingChild) {
                                            if (childData.length) {
                                                childData.splice(childData.indexOf(existingChild), 1);
                                            }
                                            jQ.each(existingRelation.relData, function (key, value) {
                                                if (value === NO_VALUE) {
                                                    existingChild.set(_removeReferenceField(key), null);
                                                }
                                                else if (isSFId.test(value)) {
                                                    key = _removeReferenceField(key);
                                                    if (existingChild.get(key) !== value) {
                                                        existingChild.set(key, value);
                                                    }
                                                }
                                                else if (existingChild.get(key) !== value) {
                                                    matchlength = 0;
                                                    matchValue = '';
                                                    key = _removeReferenceField(key);
                                                    if (_fieldMap[key] !== undefined) {
                                                        lowerCaseValue = typeof value == "string" ? value.toLowerCase() : value;
                                                        errorMsgs[currentRow[rowIndex]] = errorMsgs[rowsIndex[rowIndex]] || [];
                                                        if (Array.isArray(_fieldMap[key][lowerCaseValue])) {
                                                            errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(childSchema, key)).replace('{value}', value));
                                                        } else if (_fieldMap[key][lowerCaseValue] !== undefined) {
                                                            existingChild.set(key, _fieldMap[key][lowerCaseValue]);
                                                        } else if (lowerCaseValue.length >= 4) {
                                                            jQ.each(_fieldMap[key], function (k, v) {
                                                                if (k.includes(lowerCaseValue)) {
                                                                    matchlength++;
                                                                    matchValue = v;
                                                                }
                                                            });
                                                            if (matchlength === 1) {
                                                                if (Array.isArray(matchValue)) {
                                                                    errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(childSchema, key)).replace('{value}', value));
                                                                } else {
                                                                    existingChild.set(key, matchValue);
                                                                }
                                                            } else if (matchlength > 1) {
                                                                errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(childSchema, key)).replace('{value}', value));
                                                            } else {
                                                                errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(childSchema, key)));
                                                            }
                                                        } else {
                                                            errorMsgs[currentRow[rowIndex]].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(childSchema, key)));
                                                        }
                                                    } else {
                                                        existingChild.set(key, value);
                                                    }
                                                }
                                            });
                                        } else {
                                            relatedSource.add(_setupNewRecord(existingRelation.relData, sheetConfig, defaultValues[childSchema.sObjectName], errorMsgs, currentRow[rowIndex], childSchema));
                                        }
                                    } else {
                                        relatedSource.add(_setupNewRecord(existingRelation.relData, sheetConfig, defaultValues[childSchema.sObjectName], errorMsgs, currentRow[rowIndex], childSchema));
                                    }
                                    rowIndex++;
                                }
                                if (!hasError(errorMsgs) && (childData.length || relatedSource.hasChanges())) {
                                    for (i = (childData.length - 1); i >= 0; i--) {
                                        relatedSource.remove(childData[i]);
                                    }
                                    relatedSource.sync();
                                } else {
                                    relatedSource.cancelChanges();
                                }
                            }
                        }
                        else {
                            _addNewParentAndChildRecordToSource(source, obj, sheetConfig, defaultValues, objSchema, childSchema, children, errorMsgs, currentRow[rowIndex]);
                        }
                    } else {
                        _addNewParentAndChildRecordToSource(source, obj, sheetConfig, defaultValues, objSchema, childSchema, children, errorMsgs, currentRow[rowIndex]);
                    }
                }
            }

            if (hasError(errorMsgs)) {
                throw errorMsgs;
            }

            if (!hasError(errorMsgs) && (parentData.length || source.hasChanges())) {
                while (parentData.length > 0) {
                    if (parentData[0][sheetConfig.pivotField]) {
                        source.remove(parentData[0]);
                    }
                    parentData.splice(0, 1);
                }
                source.sync();
            }

            return errorMsgs;
        }
        catch (e) {
            jQ.each(children, function (key, value) {
                if (value.hasChanges()) {
                    value.cancelChanges();
                }
            });
            if (source.hasChanges()) {
                source.cancelChanges();
            }
            addError(e);
            throw e;
        }
    }

    /**
     * Method to add record to parent and child source
     * @param {Object} source parent data source
     * @param {Object} obj contains record to be added
     * @param {Object} sheetConfig custum sheet configuration
     * @param {Object} defaultValues default values for record
     * @param {Object} objSchema data schema of parent
     * @param {Object} childSchema data schema of child
     * @param {Array} children array of child inserted to be reverted if any failure
     * @param {Object} errorMsgs collection of error messages
     * @param {Number} rowNumber row number for error row identification
     */
    function _addNewParentAndChildRecordToSource(source, obj, sheetConfig, defaultValues, objSchema, childSchema, children, errorMsgs, rowNumber) {
        var model,
            relatedObjs,
            relatedSource,
            childData,
            relatedObjIndex,
            newChild,
            rowIndex = 0,
            i;

        model = source.add(_setupNewRecord(obj.data, sheetConfig, defaultValues[objSchema.sObjectName], errorMsgs, rowNumber, objSchema));

        relatedObjs = obj.relation;
        if (relatedObjs.length) {
            relatedSource = getSource(_columnMap.relationSourceName, model);
            children.push(relatedSource);
            childData = relatedSource.view().slice(0);
            for (relatedObjIndex = 0; relatedObjIndex < relatedObjs.length; relatedObjIndex++) {
                rowIndex = rowNumber;
                newChild = relatedSource.add(_setupNewRecord(relatedObjs[relatedObjIndex].relData, sheetConfig, defaultValues[childSchema.sObjectName], errorMsgs, rowIndex, childSchema));
                childData.splice(childData.indexOf(newChild), 1);
                rowIndex++;
            }

            if (childData.length || relatedSource.hasChanges()) {
                for (i = (childData.length - 1); i >= 0; i--) {
                    relatedSource.remove(childData[i]);
                }
                relatedSource.sync();
            }
        }

        source.sync();
    }

    /**
     * Method used to check for required fields in row datum
     * @param {Object} currentRow object of row indices
     * @param {Object} obj object containing row data values
     * @param {Object} parentSchema schema of parent object
     * @param {Object} childSchema schema of child object
     * @param {Object} errorMsgs contains existing errors in rows
     * @return returns errorMsgs containing row errors
     */
    function _checkRequiredFieldValues(currentRow, obj, parentSchema, childSchema, errorMsgs) {
        var requiredIndex = 0;

        errorMsgs[currentRow[requiredIndex]] = errorMsgs[currentRow[requiredIndex]] || [];

        jQ.each(_requiredFields, function (key, value) {
            if (key !== 'relation') {
                if (value && !obj.data[key]) {
                    errorMsgs[currentRow[requiredIndex]].push(cq.labels.common.requiredmsg.replace('{fieldName}', _getFieldLabel(parentSchema, key)));
                }
            } else {
                if (obj.relation !== undefined && obj.relation.length) {
                    jQ.each(_requiredFields.relation, function (k, v) {
                        var reqIndex = requiredIndex;
                        if (v && !obj.relation[requiredIndex].relData[k]) {
                            errorMsgs[currentRow[reqIndex]] = errorMsgs[currentRow[reqIndex]] || [];
                            errorMsgs[currentRow[requiredIndex]].push(cq.labels.common.requiredmsg.replace('{fieldName}', _getFieldLabel(childSchema, k)));
                        }
                        reqIndex++;
                    });
                }
            }
        });
        return errorMsgs;
    }

    /**
     * Method to create dataMap from raw row data
     * @param {Object} dataRows raw row data map
     * @param {Object} apiName map of row index and their api name
     * @param {Object} objSchema parent data schema
     * @param {Object} childSchema child data schema
     * @param {Number} rowsIndex current row index
     * @return dataMap created from row raw data
     */
    function _createRowDataMap(dataRows, apiName, objSchema, childSchema, rowsIndex) {
        var dataIndex,
            cellIndex,
            dataRow,
            data,
            key,
            dataM = {},
            childFilter,
            rowId,
            sheetCellIndex,
            relData,
            cell,
            fieldName,
            filter,
            pivotIndex = 1,
            pivotKey,
            dataRanges = {},
            existing,
            dataMap = {},
            relIndex,
            existingRecord,
            existingChildRecords,
            existingChildRecord,
            relationId;

        for (dataIndex = 0; dataIndex < dataRows.length; dataIndex++) {
            dataRow = dataRows[dataIndex];
            data = {};
            relData = null;
            key = '';
            dataM = {};
            childFilter = {};
            rowId = null;
            pivotIndex = 1,
                relationId = null;

            for (cellIndex = 0; cellIndex < dataRow.cells.length; cellIndex++) {
                cell = dataRow.cells[cellIndex];
                if (cell.index !== undefined && apiName[cell.index] && cell.value !== undefined) {
                    sheetCellIndex = cell.index;
                    _columnMap.highestRowIndex = _columnMap.highestRowIndex < dataRow.index ? dataRow.index : _columnMap.highestRowIndex;

                    fieldName = apiName[sheetCellIndex];
                    if (sheetCellIndex < _columnMap.relationStartIndex || sheetCellIndex >= _columnMap.relationEndIndex) {
                        if (_columnMap.sourceIdIndex !== undefined) {
                            if (_columnMap.sourceIdIndex === sheetCellIndex) {
                                filter = _setupReferenceFilter(objSchema, _columnMap.sourceIdField, cell.value);
                                rowId = cell.value;
                                key = key + '-' + cell.value || '-';
                                existingRecord = existingRecords[rowId];
                            }
                            else if (sheetCellIndex >= _columnMap.pivotStartIndex) {

                                if (pivotIndex == 1) {
                                    pivotKey = key;
                                    if (cell.value !== _columnMap.defaultPivotValue) {
                                        if (cell.link !== undefined) {
                                            data[_columnMap.pivotValueField] = cell.link.replace(_baseUrl, "");
                                        } else {
                                            data[_columnMap.pivotValueField] = cell.value;
                                        }
                                    }
                                    data[_columnMap.pivotField] = fieldName;
                                    key = key + '-' + (cell.value || '-') + '-' + fieldName;
                                } else {
                                    if (cell.value === _columnMap.defaultPivotValue) {
                                        data[_columnMap.pivotValueField] = '';
                                    }
                                    else {
                                        if (cell.link !== undefined) {
                                            data[_columnMap.pivotValueField] = cell.link.replace(_baseUrl, "");
                                        } else {
                                            data[_columnMap.pivotValueField] = cell.value;
                                        }
                                    }
                                    data[_columnMap.pivotField] = fieldName;
                                    key = pivotKey + '-' + (cell.value || '-') + '-' + fieldName;
                                }
                                dataM[key] = JSON.parse(JSON.stringify(data));
                                pivotIndex++;
                            } else {
                                dataRanges[fieldName] = dataRanges[fieldName] || [];
                                if (cell.link !== undefined) {
                                    data[fieldName] = cell.link.replace(_baseUrl, "");
                                } else {
                                    data[fieldName] = cell.value;
                                }

                                dataRanges[fieldName].push('R' + (dataRow.index + 1) + 'C' + (sheetCellIndex + 1));
                                key = key + '-' + cell.value || '-';
                            }
                        } else {
                            filter = { logic: "and", filters: filter.filters || [] };
                            if (sheetCellIndex >= _columnMap.pivotStartIndex) {
                                data[_columnMap.pivotValueField] = cell.value;
                                data[_columnMap.pivotField] = fieldName;
                                key = key + '-' + (cell.value || '-') + '-' + fieldName;
                                filter.filters.push(_setupReferenceFilter(objSchema, _columnMap.pivotValueField, cell.value));
                                filter.filters.push(_setupReferenceFilter(objSchema, _columnMap.pivotField, fieldName));
                            }
                            else {
                                data[fieldName] = cell.value;
                                key = key + '-' + cell.value || '-';
                                filter.filters.push(_setupReferenceFilter(objSchema, fieldName, cell.value));
                            }
                        }
                    } else {
                        if (_columnMap.relationIdIndex !== undefined) {
                            if (_columnMap.relationIdIndex === sheetCellIndex) {
                                relationId = cell.value;
                                childFilter = _setupReferenceFilter(childSchema, _columnMap.relationIdField, relationId);
                            } else {
                                relData = relData || {};
                                fieldName = fieldName.substr(fieldName.indexOf('.') + 1);
                                dataRanges[fieldName] = dataRanges[fieldName] || [];
                                relData[fieldName] = cell.value;
                                if (cell.link !== undefined) {
                                    relData[fieldName] = cell.link.replace(_baseUrl, "");
                                } else {
                                    relData[fieldName] = cell.value;
                                }
                                dataRanges[fieldName].push('R' + (dataRow.index + 1) + 'C' + (sheetCellIndex + 1));

                            }
                        } else {
                            childFilter = { logic: "and", filters: childFilter.filters || [] };
                            childFilter.filters.push(_setupReferenceFilter(childSchema, fieldName, cell.value));
                            relData = relData || {};
                            relData[fieldName.substr(fieldName.indexOf('.') + 1)] = cell.value;
                        }
                    }
                }
            }

            if (key) {
                if (Object.keys(dataM).length > 1) {
                    jQ.each(dataM, function (key, value) {
                        existing = false;
                        if (_columnMap.pivotColumnMap[rowId + '-' + value[_columnMap.pivotField]] === value[_columnMap.pivotValueField]
                            || _columnMap.pivotColumnMap[rowId] === value[_columnMap.pivotField]) {
                            existing = true;
                        }

                        if (existing && rowId && existingRecords[rowId]) {
                            existingRecord = existingRecords[rowId];
                            jQ.each(existingRecord, function (k, v) {
                                if (k === 'relation') {
                                    existingChildRecords = v;
                                } else {
                                    if (v && value[k] === undefined) {
                                        value[_removeReferenceField(k)] = "";
                                    }
                                }
                            });
                        }

                        if (!dataMap[key]) {
                            dataMap[key] = { data: value };
                            dataMap[key].relation = [];
                        }

                        if (relData) {
                            if (existing && relationId && existingChildRecords[relationId]) {
                                existingChildRecord = existingChildRecords[relationId];
                                jQ.each(existingChildRecord, function (k, v) {
                                    if (v && relData[k] === undefined) {
                                        relData[k] = "";
                                    }
                                });
                            }
                            dataMap[key].relation.push({ relData: relData });
                            relIndex = dataMap[key].relation.length;
                        }
                        if (existing === true) {
                            dataMap[key].filter = filter || {};
                            if (dataMap[key].relation.length && relIndex) {
                                dataMap[key].relation[relIndex - 1].filter = childFilter || {};
                            }
                        }
                        if (rowsIndex[key]) {
                            rowsIndex[key].push(dataRow.index + 1);
                        } else {
                            rowsIndex[key] = [];
                            rowsIndex[key].push(dataRow.index + 1);
                        }
                    });
                } else {
                    data = dataM[key] || data;

                    if (existingRecords[rowId]) {
                        existingRecord = existingRecords[rowId] || {};
                        jQ.each(existingRecord, function (k, v) {
                            if (k === 'relation') {
                                existingChildRecords = v;
                            } else {
                                if (v && data[k] === undefined) {
                                    data[k] = "";
                                }
                            }
                        });
                    }

                    if (!dataMap[key]) {
                        dataMap[key] = { data: data };
                        dataMap[key].relation = [];
                    }
                    if (relData) {
                        if (relationId && existingChildRecords && existingChildRecords[relationId]) {
                            existingChildRecord = existingChildRecords[relationId] || {};
                            jQ.each(existingChildRecord, function (k, v) {
                                if (v && relData[k] === undefined) {
                                    relData[k] = "";
                                }
                            });
                        }
                        dataMap[key].relation.push({ relData: relData });
                        relIndex = dataMap[key].relation.length;
                    }

                    if (filter) {
                        dataMap[key].filter = filter || {};
                        if (dataMap[key].relation.length && relIndex && childFilter) {
                            dataMap[key].relation[relIndex - 1].filter = childFilter || {};
                        }
                    }

                    if (rowsIndex[key]) {
                        rowsIndex[key].push(dataRow.index + 1);
                    } else {
                        rowsIndex[key] = [];
                        rowsIndex[key].push(dataRow.index + 1);
                    }
                }
            }
        }
        return dataMap;
    }

    function hasError(errorMsgs, row) {
        var errorCount = 0;
        if (row !== undefined) {
            jQ.each(errorMsgs[row], function (k, v) {
                if (v) {
                    errorCount++;
                }
            });
        } else {
            jQ.each(errorMsgs, function (key, value) {
                jQ.each(value, function (k, v) {
                    if (v) {
                        errorCount++;
                    }
                });
            });
        }
        if (errorCount) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to duplicate merged range value to all cells
     * @param {Array} wMergedCells array of range of merged cells
     * @param {Object} worksheet current kendo sheet
     * @return returns JSON raw data of cells
     */
    function _duplicateMergedCellValues(wMergedCells, worksheet) {
        var wMergedIndex,
            cellValue;

        for (wMergedIndex = 0; wMergedIndex < wMergedCells.length; wMergedIndex++) {
            cellValue = null;
            worksheet.range(wMergedCells[wMergedIndex]).forEachCell(function (row, column, value) {
                if (value && value.value !== undefined) {
                    if (cellValue !== value.value) {
                        worksheet.range(wMergedCells[wMergedIndex]).value(value.value);
                        cellValue = value.value;
                        if (value.link) {
                            worksheet.range(wMergedCells[wMergedIndex]).link(value.link);
                        }
                    }
                }
            });
        }
        return worksheet.toJSON();
    }

    /**
     * Method to remove relation from field name
     * @param {String} fieldName name of the field
     * @param {Boolean} isReference is reference field required
     * @return returns field name after manipulation
     */
    function _removeReferenceField(fieldName, isReference) {
        var name = fieldName,
            dotIndex = fieldName.indexOf(".");

        isReference = isReference || false;

        if (dotIndex !== -1) {
            name = fieldName.slice(0, dotIndex);
        }
        if (isReference) {
            return name.endsWith('__c') ? name.replace('__c', '__r') : name;
        } else {
            return name.endsWith('__r') ? name.replace('__r', '__c') : name;
        }
    }

    /**
     * Relate field string value with its related id
     * @param {Object} data key value pair of field and their value
     * @param {Object} sheetConfig configuration for the sheet
     * @param {Object} defaultValues default values for the new records
     * @param {Object} errorMsgs errors in the sheet
     * @param {Object} rowNum current row number
     * @param {Object} schema schema of parent object
     * @return returns map with field and id pair
     */
    function _setupNewRecord(data, sheetConfig, defaultValues, errorMsgs, rowNum, schema) {
        var field,
            record = {},
            isSFId = new RegExp("^([a-zA-Z0-9]{18})$"),
            matchlength = 0,
            matchValue,
            lowerCaseValue;

        errorMsgs[rowNum] = errorMsgs[rowNum] || [];

        jQ.each(data, function (key, value) {
            matchlength = 0;
            matchValue = '';
            field = _removeReferenceField(key);
            if (value === NO_VALUE) {
                record[field] = null;
            }
            else if (isSFId.test(value)) {
                record[field] = value;
            }
            else if (_fieldMap[field] !== undefined && field !== _columnMap.pivotField) {
                lowerCaseValue = typeof value == "string" ? value.toLowerCase() : value;
                if (Array.isArray(_fieldMap[field][lowerCaseValue])) {
                    errorMsgs[rowNum].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(schema, field)).replace('{value}', value));
                } else if (_fieldMap[field][lowerCaseValue] !== undefined) {
                    record[field] = _fieldMap[field][lowerCaseValue];
                } else if (lowerCaseValue.length >= 4) {
                    jQ.each(_fieldMap[field], function (k, v) {
                        if (k.includes(lowerCaseValue)) {
                            matchlength++;
                            matchValue = v;
                        }
                    });
                    if (matchlength === 1) {
                        if (Array.isArray(matchValue)) {
                            errorMsgs[rowNum].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(schema, field)).replace('{value}', value));
                        } else {
                            record[field] = matchValue;
                        }
                    } else if (matchlength > 1) {
                        errorMsgs[rowNum].push(cq.labels.ksheet.multiValueErr.replace('{field}', _getFieldLabel(schema, field)).replace('{value}', value));
                    } else {
                        errorMsgs[rowNum].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(schema, key)));
                    }
                } else {
                    errorMsgs[rowNum].push(cq.labels.ksheet.validValueErr.replace('{value}', value).replace('{field}', _getFieldLabel(schema, key)));
                }
            } else {
                record[field] = value;
            }
        });

        jQ.each(defaultValues, function (key, value) {
            record[key] = value;
        });
        return record;
    }

    /**
     * Method to setup data source filter for fields
     * @param {Object} schema schema of the object
     * @param {String} field field to be used for reference
     * @param {String} value value of the field
     */
    function _setupReferenceFilter(schema, field, value) {
        var subFilter = {},
            refFieldName,
            refField;

        if (_getFieldType(schema, field) === 'reference') {

            refField = _removeReferenceField(field, true);
            if (field.indexOf('.') !== -1) {
                refFieldName = field;
            } else {
                addError(field + ' is not a valid reference field.');
                return;
            }

            subFilter.filters = [];

            subFilter.logic = 'and';
            subFilter.filters.push({ field: refField, operator: 'isnotnull' });
            subFilter.filters.push({ field: refFieldName, operator: 'isnotnull' });
            subFilter.filters.push({ field: refFieldName, operator: 'eq', value: value });

            return subFilter;
        } else {
            return { field: field, operator: 'eq', value: value };
        }

    }

    /**
     * Method to get valid values and cell validation for fields
     * @param {Object} parent Data source of the object
     * @param {String} field Field name of for valid values
     * @return _columnMap containing cell validationa and valid values
     */
    function _getValidValues(parent, field) {
        var fieldType,
            sourceField,
            ds,
            values,
            fieldMap = _fieldMap,
            fieldName,
            data = {},
            fieldLabel,
            refField,
            existingValue,
            dataValues,
            searchByFields,
            lowerCaseValue;

        return new Promise(function (resolve, reject) {
            fieldName = _removeReferenceField(field);

            fieldType = _getFieldType(parent, fieldName);
            fieldLabel = _getFieldLabel(parent, fieldName);

            if (fieldType === undefined) {
                reject(Error('Field Type is undefined for ' + field + ' of in ' + parent));
            }

            if (fieldType === "values") {
                sourceField = fieldName + "_ValidValues";
                ds = parent[sourceField];
                data.source = ds;
                data.fieldType = fieldType;
                data.otherattributes = '';
                createCustomEditor(parent.sObjectName + '.' + field, data);
                dataValues = ds.view();
                fieldMap[fieldName] = {};
                for (var i = 0; i < dataValues.length; i++) {
                    lowerCaseValue = typeof dataValues[i].text == "string" ? dataValues[i].text.toLowerCase() : dataValues[i].text;
                    fieldMap[fieldName][lowerCaseValue] = dataValues[i].value;
                }
                return resolve();
            }
            else if (fieldType === "reference") {
                refField = field.indexOf('.') !== -1 ? field.split('.')[1] : null;
                sourceField = fieldName + "_Source";
                ds = parent[sourceField];
                data.source = ds;
                data.fieldType = fieldType;
                data.searchByFields = parent.fields[fieldName].additionalInfo.searchByFields;
                data.templateId = parent.fields[fieldName].additionalInfo.templateId;
                data.placeholder = translate.get('cqcompositefield.comboplaceholder', { fieldName: fieldLabel });
                data.otherattributes = '';
                data.dataText = 'Name';
                data.dataValue = 'Id';

                if(parent.fields[fieldName].additionalInfo.searchByFields) {
                    searchByFields = parent.fields[fieldName].additionalInfo.searchByFields
                    data.otherattributes += " data-cq-filter-fields='[";
                    for(var index = 0; index < searchByFields.length; ){
                        data.otherattributes += "\"" + searchByFields[index]  + "\"";
                        index++;
                        if(index < searchByFields.length)
                            data.otherattributes += ',';
                    }
                    data.otherattributes += " ]' ";
                }

                if (parent.fields[fieldName].additionalInfo.templateId !== undefined) {
                    data.otherattributes += 'data-template=' + parent.fields[fieldName].additionalInfo.templateId;
                }
                if (parent.fields[fieldName].additionalInfo.autocompleteTextField !== undefined) {
                    data.dataText = parent.fields[fieldName].additionalInfo.autocompleteTextField;
                }
                createCustomEditor(parent.sObjectName + '.' +  field, data);
                if(refField){
                    ds = jQ.isFunction(ds) ? ds() : ds;
                    ds.read().then(function () {
                        fieldMap[fieldName] = {};
                        values = ds.view();
                        for (var i = 0; i < values.length; i++) {
                            lowerCaseValue = typeof values[i][refField] == "string" ? values[i][refField].toLowerCase() : values[i][refField];
                            if (fieldMap[fieldName][lowerCaseValue] !== undefined) {
                                if (Array.isArray(fieldMap[fieldName][lowerCaseValue])) {
                                    fieldMap[fieldName][lowerCaseValue].push(values[i].Id);
                                } else {
                                    existingValue = fieldMap[fieldName][lowerCaseValue];
                                    fieldMap[fieldName][lowerCaseValue] = [];
                                    fieldMap[fieldName][lowerCaseValue].push(existingValue);
                                }
                                fieldMap[fieldName][lowerCaseValue] = fieldMap[fieldName][lowerCaseValue] || [];
                                fieldMap[fieldName][lowerCaseValue].push(values[i].Id);
                            } else {
                                fieldMap[fieldName][lowerCaseValue] = values[i].Id;
                            }
                        }

                        return resolve();
                    }, function (error) {
                        return error;
                    });
                } else {
                    reject(Error('No reference field found for' + fieldLabel));
                }
            } else {
                return resolve();
            }
        });
    }

    /**
     * get field type of the field
     * @param {Object} parent Data source of object
     * @param {String} field Field name
     * @return returns field type of the field specified
     */
    function _getFieldType(parent, field) {
        var fieldInfo;

        fieldInfo = parent.fields[_removeReferenceField(field)];

        if (fieldInfo === undefined) {
            return;
        }

        return fieldInfo.type;
    }

    /**
     * get field label of the field
     * @param {Object} parent Data source of object
     * @param {String} field Field name
     * @return returns field label of the field specified
     */
    function _getFieldLabel(parent, field) {
        var fieldInfo;

        fieldInfo = parent.fields[_removeReferenceField(field)];

        if (fieldInfo === undefined) {
            return;
        }

        return fieldInfo.name;
    }

    /**
     * Exports individual worksheet to the datasource
     * @param {Object} worksheet the worksheet that is to be exported
     * @param {Object} sheetConfig configuration detailing how the sheet is built
     * @param {Object} record the record that contains the data source related to sheet's data
     * @param {Object} globalSchema the global schema containing every object description
     * @param {Object} defaultValues default values for the new records
     */
    function exportSheetToSource(worksheet, sheetConfig, record, globalSchema, defaultValues) {
        var source = getSource(sheetConfig.source, record),
            mainRecordSchema;

        if (source) {
            mainRecordSchema = source.options.schema.model.fn;
            return buildHeaderRow(globalSchema, mainRecordSchema, sheetConfig).then(function () {
                return sheetJSONToObjects(worksheet, source, sheetConfig, defaultValues, globalSchema);
            }, function (error) {
                throw error;
            });
        }
    }

    /**
     * Exports data from kendo spreadsheet to CQ Data Source
     * @param {Object} spreadsheetElement the DOM where kendo's spreadsheet was initialized
     * @param {Object} cqConfig CQ's configuration container
     * @param {Object} record that should be used as the starting point to initialize things
     * @param {Object} globalSchema global schema container, useful for identifying field type and information
     */
    function exportToCQDataSource(spreadsheetElement, cqConfig, record, globalSchema) {
        var sheets = cqConfig.sheets || [],
            defaultValues = cqConfig.defaultValues || {},
            i,
            kendoSheet = spreadsheetElement.data('kendoSpreadsheet'),
            worksheets,
            promiseList = [];

        if (kendoSheet) {
            worksheets = kendoSheet.sheets();
            for (i = 0; i < sheets.length; i++) {
                // TODO: Ensure that worksheet is same as sheet. Assumption is that no new sheet was added or removed
                promiseList.push(exportSheetToSource(worksheets[i], sheets[i], record, globalSchema, defaultValues));
            }

            return Promise.all(promiseList).then(function (msg) {
                return msg;
            }, function (error) {
                throw error;
            });
        }
    }

    /**
     * Maps CQ's Configuration to kendo's configuration
     * @param {Object} config kendo's configuration where mapped data is to be set
     * @param {Object} cqConfig CQ's configuration container
     * @param {Object} record that should be used as the starting point to initialize things
     * @param {Object} globalSchema global schema container, useful for identifying field type and information
     */
    function addSheetConfiguration(config, cqConfig, record, globalSchema) {
        var sheets = cqConfig.sheets,
            i,
            colorPalette = cqConfig.colorPalette || [],
            properties = cqConfig.kendo || {},
            promiseList = [];

        if (sheets) {
            for (i = 0; i < sheets.length; i++) {

                promiseList.push(
                    convertFromCQToKendoSheet(sheets[i], record, globalSchema, colorPalette, properties)
                );
            }
        } else {
            addError('No sheet configuration defined.');
        }

        return Promise.all(promiseList)
            .then(function (responses) { Array.prototype.push.apply(config.sheets, responses); });
    }

    /**
     * Function to create customer editor for the cells
     * @param {String} editorName editor name
     * @param {Object} data contains values for the editor configuration
     */
    function createCustomEditor(editorName, data) {
        kendo.spreadsheet.registerEditor(editorName, function () {
            var context, dlg, model,
                defaultTemplate = kendo.template(jQ("#customEditorTemplate").html());

            // Further delay the initialization of the UI until the `edit` method is
            // actually called, so here just return the object with the required API.

            return {
                edit: function (options) {
                    context = options;
                    open();
                },
                icon: "k-icon k-i-arrow-60-right"
            };

            /**
             * This method adds the dependency filter for the data source that is used to lookup
             */
            function filterSourceIfDependent() {
                if(dependentMap[editorName]) {
                    var cell = context.range.topLeft(),
                        kendoSpreadSheet = context.view.element.data().kendoSpreadsheet,
                        aSheet = kendoSpreadSheet.activeSheet(),
                        dependentFieldName = dependentMap[editorName].field || dependentMap[editorName].controllingField,
                        dependentCell = aSheet.range('R' + (cell.row + 1) + 'C' + (columnIndicesMap[dependentFieldName] + 1)), //assumption: dependent column is placed before
                        dependentFilter = {},
                        cellLink,
                        cellValue,
                        idx,
                        mCells = aSheet._mergedCells,
                        mCell,
                        cellRow,
                        cellCol,
                        validValues,
                        ds = $.isFunction(model.source) ? model.source() : model.source;

                    cellLink = dependentCell.link();
                    cellValue =  dependentCell.value();
                    cellRow = cell.row;
                    cellCol = columnIndicesMap[dependentFieldName]; // dependent column is placed before

                    if(cellValue === null) {
                        // if the cell doesn't have value check if it is part of merged cell and get value of merged cell
                        for(idx = 0; idx < mCells.length; idx++) {
                            mCell = mCells[idx];
                            if(cellRow >= mCell.topLeft.row && cellCol >= mCell.topLeft.col &&
                                cellRow <= mCell.bottomRight.row && cellCol <= mCell.bottomRight.col) {
                                mCell = aSheet.range('R' + (mCell.topLeft.row + 1) + 'C' + (mCell.topLeft.col + 1) + ':' + 'R' + (mCell.bottomRight.row + 1) + 'C' + (mCell.bottomRight.col + 1));
                                cellLink = mCell.link();
                                cellValue = mCell.value();
                                break;
                            }
                        }
                    }

                    if(dependentMap[editorName].controllingField) {
                        if(cellValue) {
                            validValues = model.source.options.data;

                            dependentFilter = {
                                field: 'text',
                                operator: function(items, filterValue) {
                                    if(!this._validValuesCache) {
                                        var cache = {};

                                        for(var idx = 0; idx < validValues.length; idx++) {
                                            if(!validValues[idx].validFor) {
                                                continue;
                                            }
                                            for (var ridx = 0; ridx < validValues[idx].validFor.length; ridx++) {
                                                if(!cache[validValues[idx].validFor[ridx]]) {
                                                    cache[validValues[idx].validFor[ridx]] = {};
                                                }

                                                cache[validValues[idx].validFor[ridx]][validValues[idx].text] = true;
                                            }
                                        }
                                        this._validValuesCache = cache;
                                    }

                                    return (this._validValuesCache[filterValue] && this._validValuesCache[filterValue][items]) || false;
                                },
                                value: cellValue
                            };
                        }
                    } else {
                        if(cellLink) {
                            dependentFilter = {
                                field: dependentMap[editorName].linkingField,
                                operator: "eq",
                                value: cellLink.replace(_baseUrl, "")
                            };
                        } else if(cellValue) {
                            dependentFilter = {
                                field: dependentMap[editorName].linkingField,
                                operator: 'startswith',
                                value: cellValue,
                                usv_param: 'Name',
                                usv_function: 'reference'
                            };
                        }
                    }

                    ds.filter(dependentFilter);
                }
            }
            /**
             * Transfers the value from one cell to another based on configuration specified in transfer map
             * @param {*} objName The object for which the values are to be transferred
             * @param {*} fieldName The field which is being currently set
             * @param {*} data The data that has been selected for the field
             * @param {*} context The context in which the transfer request is being made. Contains information about range, sheet etc.
             */
            function transferCellValues(objName, fieldName, data, context) {
                var fieldDef, idx, transferMap, targetName, spreadsheet, topRow, bottomRow, mCells, mCell, mCellIdx;
                // HACK: direct reference to SQXSchema for 7.4.3, need to improve in 8.0.0
                if(SQXSchema[objName] && SQXSchema[objName].fields[fieldName]) {
                    fieldDef = SQXSchema[objName].fields[fieldName];
                    transferMap = fieldDef.additionalInfo.map;

                    if(transferMap && transferMap.length > 1){
                        topRow = context.range.topLeft();
                        bottomRow = 1;

                        // identify the bottom row so that multiple rows are set for merged cells
                        mCells = context.range.sheet()._mergedCells;
                        for(idx = 0; idx < mCells.length; idx++) {
                            mCell = mCells[idx];
                            if(topRow.row >= mCell.topLeft.row && topRow.col >= mCell.topLeft.col &&
                                topRow.row <= mCell.bottomRight.row && topRow.col <= mCell.bottomRight.col) {
                                bottomRow = mCell.bottomRight.row - topRow.row + 1; // increasing by one because both rows are zero indexed and bottomRow (number of rows to fill) when setting value starts from 1.
                                break;
                            }
                        }
                        topRow = topRow.row;

                        for(idx = 1; idx < transferMap.length; idx++) {
                            if(mainObjApiName === objName) {
                                targetName = transferMap[idx].target;
                            } else {
                                targetName = childObjApiName + '.' + transferMap[idx].target;
                            }
                            if(columnIndicesMap[targetName]) {
                                context.range.sheet().range(topRow,columnIndicesMap[targetName],bottomRow,1).value(data[transferMap[idx].source] || '');
                            }
                        }
                    }
                }
            }

            // This function actually creates the UI if not already there, and
            // caches the dialog and the model.
            function create() {
                if (!dlg) {
                    model = kendo.observable({
                        value: "",
                        ok: function () {
                            var objName, fieldName;

                            // This is the result when OK is clicked. Invoke the
                            // callback with the value.
                            if(model.data){
                                //Hack:  for 7.4.3 need to find a better way
                                objName = editorName.match(/(\w+)\.(\w+)(\.\w+)*/);
                                fieldName = objName[2];
                                objName = objName[1];
                                fieldName = fieldName.indexOf('__r') ? fieldName.substr(0, fieldName.length - 1) + 'c' : fieldName;
                                transferCellValues(objName, fieldName, model.data, context);
                                if(model.data.Id) {
                                    context.range.link(_baseUrl + model.data.Id);
                                    context.callback(model.data[data.dataText]);
                                } else {
                                    context.callback(model.data.text);
                                }
                            }
                            dlg.close();
                        },
                        cancel: function () {
                            dlg.close();
                        },
                        source: data.source.options ? sqx.remote.cloneSource(data.source) : data.source // clone data source because filter creates issue with validation
                    });

                    filterSourceIfDependent();
                    var el = jQ(defaultTemplate(data));
                    kendo.bind(el, model);

                    // Cache the dialog.
                    dlg = el.getKendoPopup();
                }
            }

            function open() {
                create();
                dlg.setOptions({
                    anchor: jQ(".k-spreadsheet-editor-button")
                });
                dlg.open();

                var value = context.range.value(),
                    link = context.range.link(),
                    fieldName = _removeReferenceField(editorName),
                    lowerCaseValue;

                filterSourceIfDependent();
                model.set("data", null);
            }
        });
    }

    /**
     * Function to hide id fields in sheet
     * @param {Object} sheets current sheets
     */
    function hideIdColumns(sheets) {
        var sheetIndex;

        for (sheetIndex = 0; sheetIndex < sheets.length; sheetIndex++) {
            if (_columnMap.sourceIdIndex !== undefined) {
                sheets[sheetIndex].hideColumn(_columnMap.sourceIdIndex);
                if (_columnMap.relationIdIndex !== undefined) {
                    sheets[sheetIndex].hideColumn(_columnMap.relationIdIndex);
                }
            }
        }
    }

    /**
     * This method starts the initialization of kendo spreadsheet using the provided configuration
     * @param {Node} domElementToInitialize the DOM Node where spreadsheet is to be initialized
     * @param {Object} configuration the configuration that must be used to guide the construction of kendo spreadsheet
     * @param {Object} record that should be used as the starting point to initialize things
     * @param {Object} globalSchema the global schema containing all the details of the object
     */
    spreadsheet.initialize = function (domElementToInitialize, configuration, record, globalSchema) {
        var that = this,
            sheetConfig = jQ.extend(defaultConfig(), {});

        window.showLoading();

        return new Promise(function (resolve, reject) {

            addSheetConfiguration(sheetConfig, configuration, record, globalSchema)
            .then(function () {
                if (configuration.kendo !== undefined && configuration.kendo.defaultSheetProperties !== undefined) {
                    sheetConfig.columns = _columnMap.pivetEndIndex + 1;
                    jQ.each(configuration.kendo.defaultSheetProperties, function (key, value) {
                        sheetConfig[key] = value;
                    });
                }

                // sheet change events to show command button
                sheetConfig.change = window.commandRules.isSheetChanged;
                sheetConfig.deleteRow = window.commandRules.isSheetChanged;
                sheetConfig.excelImport = window.commandRules.isSheetChanged;
                
                sheetConfig.render = window.renderSpreadSheet;
                
                if (configuration.toolbar !== undefined) {
                    jQ.extend(sheetConfig, { toolbar: configuration.toolbar });
                }

                var sheets = jQ(domElementToInitialize).kendoSpreadsheet(sheetConfig).data('kendoSpreadsheet').sheets();
                jQ('#saveIcon').attr('title', cq.labels.esig.save);
                hideIdColumns(sheets);
            })
            .then(function () {
                resolve("done");
            })
            ['catch'](function (error) {
                reject(error);
            });
        });

    };

    spreadsheet.exportToCQDataSource = exportToCQDataSource;

    cq.spreadsheet = spreadsheet;

    return cq;

}(window.jQuery, window.kendo, window.cq || {}, window.sqx.translation || {}));