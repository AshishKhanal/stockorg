<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Internal CQ Setting used only for tests and not deployed in final packagin org</description>
    <label>TestCase1</label>
    <protected>false</protected>
    <values>
        <field>DateTime_Format__c</field>
        <value xsi:type="xsd:string">dd-MM hh:mm</value>
    </values>
    <values>
        <field>Date_Format__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default_Setting__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Enable_Stamping_When_Printed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Merge_Fields_Set_1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Merge_Fields_Set_2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Page_Namespace__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Print_Stamp_Coordinate__c</field>
        <value xsi:type="xsd:string">1,2,3,4</value>
    </values>
    <values>
        <field>Print_Stamp_DateTime_Format__c</field>
        <value xsi:type="xsd:string">dd-MM hh:mm</value>
    </values>
    <values>
        <field>Print_Stamp_Expiration__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Profile_Page_Inclusion_Type__c</field>
        <value xsi:type="xsd:string">Append</value>
    </values>
    <values>
        <field>Profile_Page_Parameter__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Profile_Page__c</field>
        <value xsi:type="xsd:string">SQX_Audit</value>
    </values>
    <values>
        <field>Stamping_PDF__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Text_To_Stamp_When_Printed__c</field>
        <value xsi:type="xsd:string">ABCDEF</value>
    </values>
    <values>
        <field>Watermark_Angle__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark_Color__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark_Horizontal_Position__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark_Text_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark_Vertical_Position__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark_Z_Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Watermark__c</field>
        <value xsi:type="xsd:string">{!Document_Number__c}::{!Effective_Date__c}::{!Description__c}::MOCK</value>
    </values>
</CustomMetadata>
