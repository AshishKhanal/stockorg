Compliance Quest - Supplier Quality Exchange Code Repository
=============================================================
New pickList values for Task type and Record Type fields under Custom object 'Task' are not deployed.
To fix this issue, we need to deploy that object as external package. 