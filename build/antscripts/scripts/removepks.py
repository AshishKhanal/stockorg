"""
This is a script to remove <packageVersions> ... </packageVersions> section from page and email template or any such files that could contain import

Note: This script is simple and assumes that no other tags come in between two packageVersions scripts and we have a clear packageVersions
"""
import os
import re

def remove_pkgs_from_file(filename):
    """
    Finds the package packageVersions section in a given filename and removes it if any such section is found
    """

    pkg_start_regex = r'<packageVersions>'
    pkg_end_regex = r'</packageVersions>'
    new_data = None
    
    # Check if an occurrence of package version can be found
    with open(filename, 'r') as filetomod:
        data = filetomod.read()
        match_start = re.search(pkg_start_regex, data)
        match_end = None
        
        for match in re.finditer(pkg_end_regex, data):
            match_end = match


        if match_end is not None and match_start is not None:
            new_data = data[:match_start.start()] + data[match_end.end():]

    # Save the file if new data is set i.e. package version section was found and removed
    if new_data is not None:
        print('Updating file ' + filename)
        with open(filename, 'w') as filetowrite:
            filetowrite.write(new_data)
    


# the list of directories where the meta data is to be searched
dirs_to_search = ['src/pages', 'src/email/Compliance_Quest']


# for each directory in the directory to search find matching meta.xml files and remove the packageVersions
for dir_to_search in dirs_to_search:
    for page_file in os.listdir(dir_to_search):
        if page_file.endswith('-meta.xml'):
            remove_pkgs_from_file(dir_to_search + '/'  + page_file)
