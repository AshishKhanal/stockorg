#!/usr/bin/python

import xml.etree.ElementTree as ET
import sys, getopt
import fileinput
import re

def main(argv):
    """
    This method copies the record type assignments specified in a source permission set
    to a destination permission set and applies provided namespace prefix
    """
    inputfile = ''
    outputfile = ''
    namespace_prefix = ''
    try:
        opts, _ = getopt.getopt(argv, "hi:o:p:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('externalPermissionsetBuilder.py -i <inputfile> -o <outputfile> -p <namespace prefix>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('externalPermissionsetBuilder.py -i <inputfile> -o <outputfile>  -p <namespace prefix>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-p", "--namespace"):
            namespace_prefix = arg

    if len(inputfile) == 0 or len(outputfile) == 0 or len(namespace_prefix) == 0:
        print('Invalid file name or namespace prefix. Please check help with -h option')
    else:
        PERMISSION_SET_NODE = 'PermissionSet'
        RECORD_TYPE_VISIBILITY_NODE = 'recordTypeVisibilities'
        source_tree = ET.parse(inputfile)
        src_root = source_tree.getroot()
        namespace = src_root.tag[:-len(PERMISSION_SET_NODE)]
        allrecord_types = ''
        for node in src_root.findall(namespace + RECORD_TYPE_VISIBILITY_NODE):
            allrecord_types = allrecord_types + ET.tostring(node)

        namespace = namespace.replace('{', '').replace('}', '')
        allrecord_types = allrecord_types.replace('ns0:', '').replace(' xmlns:ns0="'
                                                                      + namespace + '"', '')

        # Read in the file
        with open(outputfile, 'r') as file:
            filedata = file.read()

        # Add namespace prefix for custom record types of custom objects
        replacer = (r'>' + re.escape(namespace_prefix) + r'__\1.' + re.escape(namespace_prefix) + r'__\2<')
        allrecord_types = re.sub(r'>(\w+__c)\.(\w+)<', replacer, allrecord_types)

        # Add namespace prefix for custom record types of standard objects
        replacer = (r'>\1.' + re.escape(namespace_prefix) + r'__\2<')
        allrecord_types = re.sub(r'>(\w+(?<!__c))\.(\w+)<', replacer, allrecord_types)

        # Replace the target string
        text_to_replace = "###RecordType###"
        filedata = filedata.replace(text_to_replace, allrecord_types)

        # Write the file out again
        with open(outputfile, 'w') as file:
            file.write(filedata)

if __name__ == "__main__":
    main(sys.argv[1:])
