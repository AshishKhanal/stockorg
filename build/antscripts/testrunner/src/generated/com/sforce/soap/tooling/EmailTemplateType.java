
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailTemplateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailTemplateType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="text"/&gt;
 *     &lt;enumeration value="html"/&gt;
 *     &lt;enumeration value="custom"/&gt;
 *     &lt;enumeration value="visualforce"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EmailTemplateType")
@XmlEnum
public enum EmailTemplateType {

    @XmlEnumValue("text")
    TEXT("text"),
    @XmlEnumValue("html")
    HTML("html"),
    @XmlEnumValue("custom")
    CUSTOM("custom"),
    @XmlEnumValue("visualforce")
    VISUALFORCE("visualforce");
    private final String value;

    EmailTemplateType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailTemplateType fromValue(String v) {
        for (EmailTemplateType c: EmailTemplateType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
