
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScheduledProcessStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScheduledProcessStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PENDING"/&gt;
 *     &lt;enumeration value="FINISHED"/&gt;
 *     &lt;enumeration value="PROCESSING"/&gt;
 *     &lt;enumeration value="FAILED"/&gt;
 *     &lt;enumeration value="ACTIVATION_PENDING_CONFIRM"/&gt;
 *     &lt;enumeration value="ACTIVATION_CONFIRMED"/&gt;
 *     &lt;enumeration value="DEACTIVATION_CONFIRMED"/&gt;
 *     &lt;enumeration value="DEACTIVATION_FINISHED"/&gt;
 *     &lt;enumeration value="ACTIVATION_PROCESSING"/&gt;
 *     &lt;enumeration value="SUSPENDED"/&gt;
 *     &lt;enumeration value="OK"/&gt;
 *     &lt;enumeration value="NOT_ENABLED"/&gt;
 *     &lt;enumeration value="ALREADY_RUN"/&gt;
 *     &lt;enumeration value="DELETED"/&gt;
 *     &lt;enumeration value="PENDING_DELETE"/&gt;
 *     &lt;enumeration value="PENDING_DISCARD"/&gt;
 *     &lt;enumeration value="STOPPED"/&gt;
 *     &lt;enumeration value="PENDING_ORG_STATUS_UPDATE"/&gt;
 *     &lt;enumeration value="SET_TO_NULL"/&gt;
 *     &lt;enumeration value="NO_CHANGE"/&gt;
 *     &lt;enumeration value="PENDING_RESET"/&gt;
 *     &lt;enumeration value="PENDING_DELETE_PARTIAL"/&gt;
 *     &lt;enumeration value="SAMPLING"/&gt;
 *     &lt;enumeration value="PENDING_SAMPLING"/&gt;
 *     &lt;enumeration value="PENDING_REMOTE"/&gt;
 *     &lt;enumeration value="REMOTE_CREATED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ScheduledProcessStatus")
@XmlEnum
public enum ScheduledProcessStatus {

    PENDING,
    FINISHED,
    PROCESSING,
    FAILED,
    ACTIVATION_PENDING_CONFIRM,
    ACTIVATION_CONFIRMED,
    DEACTIVATION_CONFIRMED,
    DEACTIVATION_FINISHED,
    ACTIVATION_PROCESSING,
    SUSPENDED,
    OK,
    NOT_ENABLED,
    ALREADY_RUN,
    DELETED,
    PENDING_DELETE,
    PENDING_DISCARD,
    STOPPED,
    PENDING_ORG_STATUS_UPDATE,
    SET_TO_NULL,
    NO_CHANGE,
    PENDING_RESET,
    PENDING_DELETE_PARTIAL,
    SAMPLING,
    PENDING_SAMPLING,
    PENDING_REMOTE,
    REMOTE_CREATED;

    public String value() {
        return name();
    }

    public static ScheduledProcessStatus fromValue(String v) {
        return valueOf(v);
    }

}
