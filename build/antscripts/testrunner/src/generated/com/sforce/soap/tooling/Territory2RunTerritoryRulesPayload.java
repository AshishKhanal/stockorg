
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Territory2RunTerritoryRulesPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Territory2RunTerritoryRulesPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:tooling.soap.sforce.com}OperationPayload"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="keyPrefix" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="territoryId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="territoryModelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Territory2RunTerritoryRulesPayload", propOrder = {
    "keyPrefix",
    "territoryId",
    "territoryModelId"
})
public class Territory2RunTerritoryRulesPayload
    extends OperationPayload
{

    @XmlElement(required = true)
    protected String keyPrefix;
    @XmlElement(required = true)
    protected String territoryId;
    @XmlElement(required = true)
    protected String territoryModelId;

    /**
     * Gets the value of the keyPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyPrefix() {
        return keyPrefix;
    }

    /**
     * Sets the value of the keyPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyPrefix(String value) {
        this.keyPrefix = value;
    }

    /**
     * Gets the value of the territoryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerritoryId() {
        return territoryId;
    }

    /**
     * Sets the value of the territoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerritoryId(String value) {
        this.territoryId = value;
    }

    /**
     * Gets the value of the territoryModelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerritoryModelId() {
        return territoryModelId;
    }

    /**
     * Sets the value of the territoryModelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerritoryModelId(String value) {
        this.territoryModelId = value;
    }

}
