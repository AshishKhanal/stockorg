
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValueSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValueSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="controllingField" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="restricted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="valueSetDefinition" type="{urn:metadata.tooling.soap.sforce.com}ValueSetValuesDefinition" minOccurs="0"/&gt;
 *         &lt;element name="valueSetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="valueSettings" type="{urn:metadata.tooling.soap.sforce.com}ValueSettings" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValueSet", propOrder = {
    "controllingField",
    "restricted",
    "valueSetDefinition",
    "valueSetName",
    "valueSettings"
})
public class ValueSet {

    protected String controllingField;
    protected Boolean restricted;
    protected ValueSetValuesDefinition valueSetDefinition;
    protected String valueSetName;
    protected List<ValueSettings> valueSettings;

    /**
     * Gets the value of the controllingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControllingField() {
        return controllingField;
    }

    /**
     * Sets the value of the controllingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControllingField(String value) {
        this.controllingField = value;
    }

    /**
     * Gets the value of the restricted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestricted() {
        return restricted;
    }

    /**
     * Sets the value of the restricted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestricted(Boolean value) {
        this.restricted = value;
    }

    /**
     * Gets the value of the valueSetDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link ValueSetValuesDefinition }
     *     
     */
    public ValueSetValuesDefinition getValueSetDefinition() {
        return valueSetDefinition;
    }

    /**
     * Sets the value of the valueSetDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueSetValuesDefinition }
     *     
     */
    public void setValueSetDefinition(ValueSetValuesDefinition value) {
        this.valueSetDefinition = value;
    }

    /**
     * Gets the value of the valueSetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueSetName() {
        return valueSetName;
    }

    /**
     * Sets the value of the valueSetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueSetName(String value) {
        this.valueSetName = value;
    }

    /**
     * Gets the value of the valueSettings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueSettings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueSettings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueSettings }
     * 
     * 
     */
    public List<ValueSettings> getValueSettings() {
        if (valueSettings == null) {
            valueSettings = new ArrayList<ValueSettings>();
        }
        return this.valueSettings;
    }

}
