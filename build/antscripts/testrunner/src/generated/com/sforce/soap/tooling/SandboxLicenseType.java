
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SandboxLicenseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SandboxLicenseType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FULL"/&gt;
 *     &lt;enumeration value="PARTIAL"/&gt;
 *     &lt;enumeration value="DEVELOPER_PRO"/&gt;
 *     &lt;enumeration value="DEVELOPER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SandboxLicenseType")
@XmlEnum
public enum SandboxLicenseType {

    FULL,
    PARTIAL,
    DEVELOPER_PRO,
    DEVELOPER;

    public String value() {
        return name();
    }

    public static SandboxLicenseType fromValue(String v) {
        return valueOf(v);
    }

}
