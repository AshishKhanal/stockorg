
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlatformActionDeviceFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlatformActionDeviceFormat"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Phone"/&gt;
 *     &lt;enumeration value="Tablet"/&gt;
 *     &lt;enumeration value="Desktop"/&gt;
 *     &lt;enumeration value="Aloha"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PlatformActionDeviceFormat")
@XmlEnum
public enum PlatformActionDeviceFormat {

    @XmlEnumValue("Phone")
    PHONE("Phone"),
    @XmlEnumValue("Tablet")
    TABLET("Tablet"),
    @XmlEnumValue("Desktop")
    DESKTOP("Desktop"),
    @XmlEnumValue("Aloha")
    ALOHA("Aloha");
    private final String value;

    PlatformActionDeviceFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlatformActionDeviceFormat fromValue(String v) {
        for (PlatformActionDeviceFormat c: PlatformActionDeviceFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
