
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FieldType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FieldType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AutoNumber"/&gt;
 *     &lt;enumeration value="Lookup"/&gt;
 *     &lt;enumeration value="MasterDetail"/&gt;
 *     &lt;enumeration value="Checkbox"/&gt;
 *     &lt;enumeration value="Currency"/&gt;
 *     &lt;enumeration value="Date"/&gt;
 *     &lt;enumeration value="DateTime"/&gt;
 *     &lt;enumeration value="Email"/&gt;
 *     &lt;enumeration value="Number"/&gt;
 *     &lt;enumeration value="Percent"/&gt;
 *     &lt;enumeration value="Phone"/&gt;
 *     &lt;enumeration value="Picklist"/&gt;
 *     &lt;enumeration value="MultiselectPicklist"/&gt;
 *     &lt;enumeration value="Text"/&gt;
 *     &lt;enumeration value="TextArea"/&gt;
 *     &lt;enumeration value="LongTextArea"/&gt;
 *     &lt;enumeration value="Html"/&gt;
 *     &lt;enumeration value="Url"/&gt;
 *     &lt;enumeration value="EncryptedText"/&gt;
 *     &lt;enumeration value="Summary"/&gt;
 *     &lt;enumeration value="Hierarchy"/&gt;
 *     &lt;enumeration value="File"/&gt;
 *     &lt;enumeration value="MetadataRelationship"/&gt;
 *     &lt;enumeration value="Location"/&gt;
 *     &lt;enumeration value="ExternalLookup"/&gt;
 *     &lt;enumeration value="IndirectLookup"/&gt;
 *     &lt;enumeration value="CustomDataType"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FieldType")
@XmlEnum
public enum FieldType {

    @XmlEnumValue("AutoNumber")
    AUTO_NUMBER("AutoNumber"),
    @XmlEnumValue("Lookup")
    LOOKUP("Lookup"),
    @XmlEnumValue("MasterDetail")
    MASTER_DETAIL("MasterDetail"),
    @XmlEnumValue("Checkbox")
    CHECKBOX("Checkbox"),
    @XmlEnumValue("Currency")
    CURRENCY("Currency"),
    @XmlEnumValue("Date")
    DATE("Date"),
    @XmlEnumValue("DateTime")
    DATE_TIME("DateTime"),
    @XmlEnumValue("Email")
    EMAIL("Email"),
    @XmlEnumValue("Number")
    NUMBER("Number"),
    @XmlEnumValue("Percent")
    PERCENT("Percent"),
    @XmlEnumValue("Phone")
    PHONE("Phone"),
    @XmlEnumValue("Picklist")
    PICKLIST("Picklist"),
    @XmlEnumValue("MultiselectPicklist")
    MULTISELECT_PICKLIST("MultiselectPicklist"),
    @XmlEnumValue("Text")
    TEXT("Text"),
    @XmlEnumValue("TextArea")
    TEXT_AREA("TextArea"),
    @XmlEnumValue("LongTextArea")
    LONG_TEXT_AREA("LongTextArea"),
    @XmlEnumValue("Html")
    HTML("Html"),
    @XmlEnumValue("Url")
    URL("Url"),
    @XmlEnumValue("EncryptedText")
    ENCRYPTED_TEXT("EncryptedText"),
    @XmlEnumValue("Summary")
    SUMMARY("Summary"),
    @XmlEnumValue("Hierarchy")
    HIERARCHY("Hierarchy"),
    @XmlEnumValue("File")
    FILE("File"),
    @XmlEnumValue("MetadataRelationship")
    METADATA_RELATIONSHIP("MetadataRelationship"),
    @XmlEnumValue("Location")
    LOCATION("Location"),
    @XmlEnumValue("ExternalLookup")
    EXTERNAL_LOOKUP("ExternalLookup"),
    @XmlEnumValue("IndirectLookup")
    INDIRECT_LOOKUP("IndirectLookup"),
    @XmlEnumValue("CustomDataType")
    CUSTOM_DATA_TYPE("CustomDataType");
    private final String value;

    FieldType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FieldType fromValue(String v) {
        for (FieldType c: FieldType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
