
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SplitDataStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SplitDataStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Ready"/&gt;
 *     &lt;enumeration value="ToBeInserted"/&gt;
 *     &lt;enumeration value="ToBeDeleted"/&gt;
 *     &lt;enumeration value="InsertionFailed"/&gt;
 *     &lt;enumeration value="DeletionFailed"/&gt;
 *     &lt;enumeration value="ToBeInsertedAndActivated"/&gt;
 *     &lt;enumeration value="InsertionAndActivationFailed"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SplitDataStatus")
@XmlEnum
public enum SplitDataStatus {

    @XmlEnumValue("Ready")
    READY("Ready"),
    @XmlEnumValue("ToBeInserted")
    TO_BE_INSERTED("ToBeInserted"),
    @XmlEnumValue("ToBeDeleted")
    TO_BE_DELETED("ToBeDeleted"),
    @XmlEnumValue("InsertionFailed")
    INSERTION_FAILED("InsertionFailed"),
    @XmlEnumValue("DeletionFailed")
    DELETION_FAILED("DeletionFailed"),
    @XmlEnumValue("ToBeInsertedAndActivated")
    TO_BE_INSERTED_AND_ACTIVATED("ToBeInsertedAndActivated"),
    @XmlEnumValue("InsertionAndActivationFailed")
    INSERTION_AND_ACTIVATION_FAILED("InsertionAndActivationFailed");
    private final String value;

    SplitDataStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SplitDataStatus fromValue(String v) {
        for (SplitDataStatus c: SplitDataStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
