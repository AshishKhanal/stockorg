
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetadataForSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MetadataForSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataForSettings")
@XmlSeeAlso({
    AccountSettings.class,
    ActivitiesSettings.class,
    AddressSettings.class,
    BusinessHoursSettings.class,
    CaseSettings.class,
    ChatterAnswersSettings.class,
    CompanySettings.class,
    ContractSettings.class,
    EntitlementSettings.class,
    FileUploadAndDownloadSecuritySettings.class,
    ForecastingSettings.class,
    IdeasSettings.class,
    KnowledgeSettings.class,
    LiveAgentSettings.class,
    MacroSettings.class,
    MarketingActionSettings.class,
    MobileSettings.class,
    NameSettings.class,
    OpportunitySettings.class,
    OrderSettings.class,
    OrgPreferenceSettings.class,
    PathAssistantSettings.class,
    PersonListSettings.class,
    PersonalJourneySettings.class,
    ProductSettings.class,
    QuoteSettings.class,
    SearchSettings.class,
    SecuritySettings.class,
    Territory2Settings.class
})
public class MetadataForSettings
    extends Metadata
{


}
