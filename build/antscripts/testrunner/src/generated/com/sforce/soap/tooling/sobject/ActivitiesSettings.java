
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivitiesSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivitiesSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AllowUsersToRelateMultipleContactsToTasksAndEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsActivityRemindersEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsClickCreateEventsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsDragAndDropSchedulingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEmailTrackingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsGroupTasksEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsListViewSchedulingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsLogNoteEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsMultidayEventsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRecurringEventsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRecurringTasksEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSidebarCalendarShortcutEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSimpleTaskCreateUiEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsUnsTaskDelegatedToNotificationsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="MeetingRequestsLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}ActivitiesSettings" minOccurs="0"/&gt;
 *         &lt;element name="ShowCustomLogoMeetingRequests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowEventDetailsMultiUserCalendar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowHomePageHoverLinksForEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowMyTasksHoverLinks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowRequestedMeetingsOnHomePage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivitiesSettings", propOrder = {
    "allowUsersToRelateMultipleContactsToTasksAndEvents",
    "durableId",
    "fullName",
    "isActivityRemindersEnabled",
    "isClickCreateEventsEnabled",
    "isDragAndDropSchedulingEnabled",
    "isEmailTrackingEnabled",
    "isGroupTasksEnabled",
    "isListViewSchedulingEnabled",
    "isLogNoteEnabled",
    "isMultidayEventsEnabled",
    "isRecurringEventsEnabled",
    "isRecurringTasksEnabled",
    "isSidebarCalendarShortcutEnabled",
    "isSimpleTaskCreateUiEnabled",
    "isUnsTaskDelegatedToNotificationsEnabled",
    "meetingRequestsLogo",
    "metadata",
    "showCustomLogoMeetingRequests",
    "showEventDetailsMultiUserCalendar",
    "showHomePageHoverLinksForEvents",
    "showMyTasksHoverLinks",
    "showRequestedMeetingsOnHomePage"
})
public class ActivitiesSettings
    extends SObject
{

    @XmlElementRef(name = "AllowUsersToRelateMultipleContactsToTasksAndEvents", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> allowUsersToRelateMultipleContactsToTasksAndEvents;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsActivityRemindersEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isActivityRemindersEnabled;
    @XmlElementRef(name = "IsClickCreateEventsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isClickCreateEventsEnabled;
    @XmlElementRef(name = "IsDragAndDropSchedulingEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isDragAndDropSchedulingEnabled;
    @XmlElementRef(name = "IsEmailTrackingEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEmailTrackingEnabled;
    @XmlElementRef(name = "IsGroupTasksEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isGroupTasksEnabled;
    @XmlElementRef(name = "IsListViewSchedulingEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isListViewSchedulingEnabled;
    @XmlElementRef(name = "IsLogNoteEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isLogNoteEnabled;
    @XmlElementRef(name = "IsMultidayEventsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isMultidayEventsEnabled;
    @XmlElementRef(name = "IsRecurringEventsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRecurringEventsEnabled;
    @XmlElementRef(name = "IsRecurringTasksEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRecurringTasksEnabled;
    @XmlElementRef(name = "IsSidebarCalendarShortcutEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSidebarCalendarShortcutEnabled;
    @XmlElementRef(name = "IsSimpleTaskCreateUiEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSimpleTaskCreateUiEnabled;
    @XmlElementRef(name = "IsUnsTaskDelegatedToNotificationsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isUnsTaskDelegatedToNotificationsEnabled;
    @XmlElementRef(name = "MeetingRequestsLogo", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> meetingRequestsLogo;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.ActivitiesSettings> metadata;
    @XmlElementRef(name = "ShowCustomLogoMeetingRequests", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showCustomLogoMeetingRequests;
    @XmlElementRef(name = "ShowEventDetailsMultiUserCalendar", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showEventDetailsMultiUserCalendar;
    @XmlElementRef(name = "ShowHomePageHoverLinksForEvents", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showHomePageHoverLinksForEvents;
    @XmlElementRef(name = "ShowMyTasksHoverLinks", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showMyTasksHoverLinks;
    @XmlElementRef(name = "ShowRequestedMeetingsOnHomePage", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showRequestedMeetingsOnHomePage;

    /**
     * Gets the value of the allowUsersToRelateMultipleContactsToTasksAndEvents property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAllowUsersToRelateMultipleContactsToTasksAndEvents() {
        return allowUsersToRelateMultipleContactsToTasksAndEvents;
    }

    /**
     * Sets the value of the allowUsersToRelateMultipleContactsToTasksAndEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAllowUsersToRelateMultipleContactsToTasksAndEvents(JAXBElement<Boolean> value) {
        this.allowUsersToRelateMultipleContactsToTasksAndEvents = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isActivityRemindersEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsActivityRemindersEnabled() {
        return isActivityRemindersEnabled;
    }

    /**
     * Sets the value of the isActivityRemindersEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsActivityRemindersEnabled(JAXBElement<Boolean> value) {
        this.isActivityRemindersEnabled = value;
    }

    /**
     * Gets the value of the isClickCreateEventsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsClickCreateEventsEnabled() {
        return isClickCreateEventsEnabled;
    }

    /**
     * Sets the value of the isClickCreateEventsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsClickCreateEventsEnabled(JAXBElement<Boolean> value) {
        this.isClickCreateEventsEnabled = value;
    }

    /**
     * Gets the value of the isDragAndDropSchedulingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsDragAndDropSchedulingEnabled() {
        return isDragAndDropSchedulingEnabled;
    }

    /**
     * Sets the value of the isDragAndDropSchedulingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsDragAndDropSchedulingEnabled(JAXBElement<Boolean> value) {
        this.isDragAndDropSchedulingEnabled = value;
    }

    /**
     * Gets the value of the isEmailTrackingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEmailTrackingEnabled() {
        return isEmailTrackingEnabled;
    }

    /**
     * Sets the value of the isEmailTrackingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEmailTrackingEnabled(JAXBElement<Boolean> value) {
        this.isEmailTrackingEnabled = value;
    }

    /**
     * Gets the value of the isGroupTasksEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsGroupTasksEnabled() {
        return isGroupTasksEnabled;
    }

    /**
     * Sets the value of the isGroupTasksEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsGroupTasksEnabled(JAXBElement<Boolean> value) {
        this.isGroupTasksEnabled = value;
    }

    /**
     * Gets the value of the isListViewSchedulingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsListViewSchedulingEnabled() {
        return isListViewSchedulingEnabled;
    }

    /**
     * Sets the value of the isListViewSchedulingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsListViewSchedulingEnabled(JAXBElement<Boolean> value) {
        this.isListViewSchedulingEnabled = value;
    }

    /**
     * Gets the value of the isLogNoteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsLogNoteEnabled() {
        return isLogNoteEnabled;
    }

    /**
     * Sets the value of the isLogNoteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsLogNoteEnabled(JAXBElement<Boolean> value) {
        this.isLogNoteEnabled = value;
    }

    /**
     * Gets the value of the isMultidayEventsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsMultidayEventsEnabled() {
        return isMultidayEventsEnabled;
    }

    /**
     * Sets the value of the isMultidayEventsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsMultidayEventsEnabled(JAXBElement<Boolean> value) {
        this.isMultidayEventsEnabled = value;
    }

    /**
     * Gets the value of the isRecurringEventsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRecurringEventsEnabled() {
        return isRecurringEventsEnabled;
    }

    /**
     * Sets the value of the isRecurringEventsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRecurringEventsEnabled(JAXBElement<Boolean> value) {
        this.isRecurringEventsEnabled = value;
    }

    /**
     * Gets the value of the isRecurringTasksEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRecurringTasksEnabled() {
        return isRecurringTasksEnabled;
    }

    /**
     * Sets the value of the isRecurringTasksEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRecurringTasksEnabled(JAXBElement<Boolean> value) {
        this.isRecurringTasksEnabled = value;
    }

    /**
     * Gets the value of the isSidebarCalendarShortcutEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSidebarCalendarShortcutEnabled() {
        return isSidebarCalendarShortcutEnabled;
    }

    /**
     * Sets the value of the isSidebarCalendarShortcutEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSidebarCalendarShortcutEnabled(JAXBElement<Boolean> value) {
        this.isSidebarCalendarShortcutEnabled = value;
    }

    /**
     * Gets the value of the isSimpleTaskCreateUiEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSimpleTaskCreateUiEnabled() {
        return isSimpleTaskCreateUiEnabled;
    }

    /**
     * Sets the value of the isSimpleTaskCreateUiEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSimpleTaskCreateUiEnabled(JAXBElement<Boolean> value) {
        this.isSimpleTaskCreateUiEnabled = value;
    }

    /**
     * Gets the value of the isUnsTaskDelegatedToNotificationsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsUnsTaskDelegatedToNotificationsEnabled() {
        return isUnsTaskDelegatedToNotificationsEnabled;
    }

    /**
     * Sets the value of the isUnsTaskDelegatedToNotificationsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsUnsTaskDelegatedToNotificationsEnabled(JAXBElement<Boolean> value) {
        this.isUnsTaskDelegatedToNotificationsEnabled = value;
    }

    /**
     * Gets the value of the meetingRequestsLogo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMeetingRequestsLogo() {
        return meetingRequestsLogo;
    }

    /**
     * Sets the value of the meetingRequestsLogo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMeetingRequestsLogo(JAXBElement<String> value) {
        this.meetingRequestsLogo = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ActivitiesSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.ActivitiesSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ActivitiesSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.ActivitiesSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the showCustomLogoMeetingRequests property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowCustomLogoMeetingRequests() {
        return showCustomLogoMeetingRequests;
    }

    /**
     * Sets the value of the showCustomLogoMeetingRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowCustomLogoMeetingRequests(JAXBElement<Boolean> value) {
        this.showCustomLogoMeetingRequests = value;
    }

    /**
     * Gets the value of the showEventDetailsMultiUserCalendar property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowEventDetailsMultiUserCalendar() {
        return showEventDetailsMultiUserCalendar;
    }

    /**
     * Sets the value of the showEventDetailsMultiUserCalendar property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowEventDetailsMultiUserCalendar(JAXBElement<Boolean> value) {
        this.showEventDetailsMultiUserCalendar = value;
    }

    /**
     * Gets the value of the showHomePageHoverLinksForEvents property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowHomePageHoverLinksForEvents() {
        return showHomePageHoverLinksForEvents;
    }

    /**
     * Sets the value of the showHomePageHoverLinksForEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowHomePageHoverLinksForEvents(JAXBElement<Boolean> value) {
        this.showHomePageHoverLinksForEvents = value;
    }

    /**
     * Gets the value of the showMyTasksHoverLinks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowMyTasksHoverLinks() {
        return showMyTasksHoverLinks;
    }

    /**
     * Sets the value of the showMyTasksHoverLinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowMyTasksHoverLinks(JAXBElement<Boolean> value) {
        this.showMyTasksHoverLinks = value;
    }

    /**
     * Gets the value of the showRequestedMeetingsOnHomePage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowRequestedMeetingsOnHomePage() {
        return showRequestedMeetingsOnHomePage;
    }

    /**
     * Sets the value of the showRequestedMeetingsOnHomePage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowRequestedMeetingsOnHomePage(JAXBElement<Boolean> value) {
        this.showRequestedMeetingsOnHomePage = value;
    }

}
