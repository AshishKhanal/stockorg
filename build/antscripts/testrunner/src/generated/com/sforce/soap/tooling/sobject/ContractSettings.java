
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContractSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContractSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutoCalculateEndDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AutoExpirationDelay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AutoExpirationRecipient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AutoExpireContracts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsContractHistoryTrackingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}ContractSettings" minOccurs="0"/&gt;
 *         &lt;element name="NotifyOwnersOnContractExpiration" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContractSettings", propOrder = {
    "autoCalculateEndDate",
    "autoExpirationDelay",
    "autoExpirationRecipient",
    "autoExpireContracts",
    "durableId",
    "fullName",
    "isContractHistoryTrackingEnabled",
    "metadata",
    "notifyOwnersOnContractExpiration"
})
public class ContractSettings
    extends SObject
{

    @XmlElementRef(name = "AutoCalculateEndDate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> autoCalculateEndDate;
    @XmlElementRef(name = "AutoExpirationDelay", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> autoExpirationDelay;
    @XmlElementRef(name = "AutoExpirationRecipient", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> autoExpirationRecipient;
    @XmlElementRef(name = "AutoExpireContracts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> autoExpireContracts;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsContractHistoryTrackingEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isContractHistoryTrackingEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.ContractSettings> metadata;
    @XmlElementRef(name = "NotifyOwnersOnContractExpiration", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyOwnersOnContractExpiration;

    /**
     * Gets the value of the autoCalculateEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAutoCalculateEndDate() {
        return autoCalculateEndDate;
    }

    /**
     * Sets the value of the autoCalculateEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAutoCalculateEndDate(JAXBElement<Boolean> value) {
        this.autoCalculateEndDate = value;
    }

    /**
     * Gets the value of the autoExpirationDelay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAutoExpirationDelay() {
        return autoExpirationDelay;
    }

    /**
     * Sets the value of the autoExpirationDelay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAutoExpirationDelay(JAXBElement<String> value) {
        this.autoExpirationDelay = value;
    }

    /**
     * Gets the value of the autoExpirationRecipient property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAutoExpirationRecipient() {
        return autoExpirationRecipient;
    }

    /**
     * Sets the value of the autoExpirationRecipient property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAutoExpirationRecipient(JAXBElement<String> value) {
        this.autoExpirationRecipient = value;
    }

    /**
     * Gets the value of the autoExpireContracts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAutoExpireContracts() {
        return autoExpireContracts;
    }

    /**
     * Sets the value of the autoExpireContracts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAutoExpireContracts(JAXBElement<Boolean> value) {
        this.autoExpireContracts = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isContractHistoryTrackingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsContractHistoryTrackingEnabled() {
        return isContractHistoryTrackingEnabled;
    }

    /**
     * Sets the value of the isContractHistoryTrackingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsContractHistoryTrackingEnabled(JAXBElement<Boolean> value) {
        this.isContractHistoryTrackingEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ContractSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.ContractSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ContractSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.ContractSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the notifyOwnersOnContractExpiration property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyOwnersOnContractExpiration() {
        return notifyOwnersOnContractExpiration;
    }

    /**
     * Sets the value of the notifyOwnersOnContractExpiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyOwnersOnContractExpiration(JAXBElement<Boolean> value) {
        this.notifyOwnersOnContractExpiration = value;
    }

}
