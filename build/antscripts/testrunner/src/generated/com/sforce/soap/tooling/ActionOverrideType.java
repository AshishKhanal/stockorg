
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionOverrideType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionOverrideType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Default"/&gt;
 *     &lt;enumeration value="Standard"/&gt;
 *     &lt;enumeration value="Scontrol"/&gt;
 *     &lt;enumeration value="Visualforce"/&gt;
 *     &lt;enumeration value="Flexipage"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionOverrideType")
@XmlEnum
public enum ActionOverrideType {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("Scontrol")
    SCONTROL("Scontrol"),
    @XmlEnumValue("Visualforce")
    VISUALFORCE("Visualforce"),
    @XmlEnumValue("Flexipage")
    FLEXIPAGE("Flexipage");
    private final String value;

    ActionOverrideType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionOverrideType fromValue(String v) {
        for (ActionOverrideType c: ActionOverrideType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
