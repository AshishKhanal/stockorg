
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowComparisonOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlowComparisonOperator"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EqualTo"/&gt;
 *     &lt;enumeration value="NotEqualTo"/&gt;
 *     &lt;enumeration value="GreaterThan"/&gt;
 *     &lt;enumeration value="LessThan"/&gt;
 *     &lt;enumeration value="GreaterThanOrEqualTo"/&gt;
 *     &lt;enumeration value="LessThanOrEqualTo"/&gt;
 *     &lt;enumeration value="StartsWith"/&gt;
 *     &lt;enumeration value="EndsWith"/&gt;
 *     &lt;enumeration value="Contains"/&gt;
 *     &lt;enumeration value="IsNull"/&gt;
 *     &lt;enumeration value="WasSet"/&gt;
 *     &lt;enumeration value="WasSelected"/&gt;
 *     &lt;enumeration value="WasVisited"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlowComparisonOperator")
@XmlEnum
public enum FlowComparisonOperator {

    @XmlEnumValue("EqualTo")
    EQUAL_TO("EqualTo"),
    @XmlEnumValue("NotEqualTo")
    NOT_EQUAL_TO("NotEqualTo"),
    @XmlEnumValue("GreaterThan")
    GREATER_THAN("GreaterThan"),
    @XmlEnumValue("LessThan")
    LESS_THAN("LessThan"),
    @XmlEnumValue("GreaterThanOrEqualTo")
    GREATER_THAN_OR_EQUAL_TO("GreaterThanOrEqualTo"),
    @XmlEnumValue("LessThanOrEqualTo")
    LESS_THAN_OR_EQUAL_TO("LessThanOrEqualTo"),
    @XmlEnumValue("StartsWith")
    STARTS_WITH("StartsWith"),
    @XmlEnumValue("EndsWith")
    ENDS_WITH("EndsWith"),
    @XmlEnumValue("Contains")
    CONTAINS("Contains"),
    @XmlEnumValue("IsNull")
    IS_NULL("IsNull"),
    @XmlEnumValue("WasSet")
    WAS_SET("WasSet"),
    @XmlEnumValue("WasSelected")
    WAS_SELECTED("WasSelected"),
    @XmlEnumValue("WasVisited")
    WAS_VISITED("WasVisited");
    private final String value;

    FlowComparisonOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlowComparisonOperator fromValue(String v) {
        for (FlowComparisonOperator c: FlowComparisonOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
