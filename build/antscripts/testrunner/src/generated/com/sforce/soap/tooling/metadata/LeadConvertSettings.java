
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.VisibleOrRequired;


/**
 * <p>Java class for LeadConvertSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LeadConvertSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowOwnerChange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="opportunityCreationOptions" type="{urn:tooling.soap.sforce.com}VisibleOrRequired" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LeadConvertSettings", propOrder = {
    "allowOwnerChange",
    "opportunityCreationOptions"
})
public class LeadConvertSettings
    extends Metadata
{

    protected Boolean allowOwnerChange;
    @XmlSchemaType(name = "string")
    protected VisibleOrRequired opportunityCreationOptions;

    /**
     * Gets the value of the allowOwnerChange property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowOwnerChange() {
        return allowOwnerChange;
    }

    /**
     * Sets the value of the allowOwnerChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowOwnerChange(Boolean value) {
        this.allowOwnerChange = value;
    }

    /**
     * Gets the value of the opportunityCreationOptions property.
     * 
     * @return
     *     possible object is
     *     {@link VisibleOrRequired }
     *     
     */
    public VisibleOrRequired getOpportunityCreationOptions() {
        return opportunityCreationOptions;
    }

    /**
     * Sets the value of the opportunityCreationOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link VisibleOrRequired }
     *     
     */
    public void setOpportunityCreationOptions(VisibleOrRequired value) {
        this.opportunityCreationOptions = value;
    }

}
