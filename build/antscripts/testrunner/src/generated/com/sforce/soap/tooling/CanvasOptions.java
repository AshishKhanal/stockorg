
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CanvasOptions.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CanvasOptions"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HideShare"/&gt;
 *     &lt;enumeration value="HideHeader"/&gt;
 *     &lt;enumeration value="PersonalEnabled"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CanvasOptions")
@XmlEnum
public enum CanvasOptions {

    @XmlEnumValue("HideShare")
    HIDE_SHARE("HideShare"),
    @XmlEnumValue("HideHeader")
    HIDE_HEADER("HideHeader"),
    @XmlEnumValue("PersonalEnabled")
    PERSONAL_ENABLED("PersonalEnabled");
    private final String value;

    CanvasOptions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CanvasOptions fromValue(String v) {
        for (CanvasOptions c: CanvasOptions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
