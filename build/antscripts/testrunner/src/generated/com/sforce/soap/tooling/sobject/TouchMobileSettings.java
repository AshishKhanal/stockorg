
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TouchMobileSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TouchMobileSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTouchAppIpadEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsTouchAppIphoneEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsTouchBrowserIpadEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsTouchIosPhoneEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsVisualforceInTouchEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TouchMobileSettings", propOrder = {
    "durableId",
    "isTouchAppIpadEnabled",
    "isTouchAppIphoneEnabled",
    "isTouchBrowserIpadEnabled",
    "isTouchIosPhoneEnabled",
    "isVisualforceInTouchEnabled"
})
public class TouchMobileSettings
    extends SObject
{

    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "IsTouchAppIpadEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTouchAppIpadEnabled;
    @XmlElementRef(name = "IsTouchAppIphoneEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTouchAppIphoneEnabled;
    @XmlElementRef(name = "IsTouchBrowserIpadEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTouchBrowserIpadEnabled;
    @XmlElementRef(name = "IsTouchIosPhoneEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTouchIosPhoneEnabled;
    @XmlElementRef(name = "IsVisualforceInTouchEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isVisualforceInTouchEnabled;

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the isTouchAppIpadEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTouchAppIpadEnabled() {
        return isTouchAppIpadEnabled;
    }

    /**
     * Sets the value of the isTouchAppIpadEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTouchAppIpadEnabled(JAXBElement<Boolean> value) {
        this.isTouchAppIpadEnabled = value;
    }

    /**
     * Gets the value of the isTouchAppIphoneEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTouchAppIphoneEnabled() {
        return isTouchAppIphoneEnabled;
    }

    /**
     * Sets the value of the isTouchAppIphoneEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTouchAppIphoneEnabled(JAXBElement<Boolean> value) {
        this.isTouchAppIphoneEnabled = value;
    }

    /**
     * Gets the value of the isTouchBrowserIpadEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTouchBrowserIpadEnabled() {
        return isTouchBrowserIpadEnabled;
    }

    /**
     * Sets the value of the isTouchBrowserIpadEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTouchBrowserIpadEnabled(JAXBElement<Boolean> value) {
        this.isTouchBrowserIpadEnabled = value;
    }

    /**
     * Gets the value of the isTouchIosPhoneEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTouchIosPhoneEnabled() {
        return isTouchIosPhoneEnabled;
    }

    /**
     * Sets the value of the isTouchIosPhoneEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTouchIosPhoneEnabled(JAXBElement<Boolean> value) {
        this.isTouchIosPhoneEnabled = value;
    }

    /**
     * Gets the value of the isVisualforceInTouchEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsVisualforceInTouchEnabled() {
        return isVisualforceInTouchEnabled;
    }

    /**
     * Sets the value of the isVisualforceInTouchEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsVisualforceInTouchEnabled(JAXBElement<Boolean> value) {
        this.isVisualforceInTouchEnabled = value;
    }

}
