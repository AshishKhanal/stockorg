
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SamlSubjectType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SamlSubjectType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Username"/&gt;
 *     &lt;enumeration value="FederationId"/&gt;
 *     &lt;enumeration value="UserId"/&gt;
 *     &lt;enumeration value="SpokeId"/&gt;
 *     &lt;enumeration value="CustomAttribute"/&gt;
 *     &lt;enumeration value="PersistentId"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SamlSubjectType")
@XmlEnum
public enum SamlSubjectType {

    @XmlEnumValue("Username")
    USERNAME("Username"),
    @XmlEnumValue("FederationId")
    FEDERATION_ID("FederationId"),
    @XmlEnumValue("UserId")
    USER_ID("UserId"),
    @XmlEnumValue("SpokeId")
    SPOKE_ID("SpokeId"),
    @XmlEnumValue("CustomAttribute")
    CUSTOM_ATTRIBUTE("CustomAttribute"),
    @XmlEnumValue("PersistentId")
    PERSISTENT_ID("PersistentId");
    private final String value;

    SamlSubjectType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SamlSubjectType fromValue(String v) {
        for (SamlSubjectType c: SamlSubjectType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
