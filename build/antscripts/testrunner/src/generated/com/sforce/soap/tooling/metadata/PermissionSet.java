
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PermissionSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PermissionSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="applicationVisibilities" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetApplicationVisibility" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="classAccesses" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetApexClassAccess" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="customPermissions" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetCustomPermissions" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="externalDataSourceAccesses" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetExternalDataSourceAccess" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="fieldPermissions" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetFieldPermissions" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="hasActivationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="license" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="objectPermissions" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetObjectPermissions" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="pageAccesses" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetApexPageAccess" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="recordTypeVisibilities" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetRecordTypeVisibility" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tabSettings" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetTabSetting" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="userPermissions" type="{urn:metadata.tooling.soap.sforce.com}PermissionSetUserPermission" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PermissionSet", propOrder = {
    "applicationVisibilities",
    "classAccesses",
    "customPermissions",
    "description",
    "externalDataSourceAccesses",
    "fieldPermissions",
    "hasActivationRequired",
    "label",
    "license",
    "objectPermissions",
    "pageAccesses",
    "recordTypeVisibilities",
    "tabSettings",
    "userPermissions"
})
public class PermissionSet
    extends Metadata
{

    protected List<PermissionSetApplicationVisibility> applicationVisibilities;
    protected List<PermissionSetApexClassAccess> classAccesses;
    protected List<PermissionSetCustomPermissions> customPermissions;
    protected String description;
    protected List<PermissionSetExternalDataSourceAccess> externalDataSourceAccesses;
    protected List<PermissionSetFieldPermissions> fieldPermissions;
    protected Boolean hasActivationRequired;
    @XmlElement(required = true)
    protected String label;
    protected String license;
    protected List<PermissionSetObjectPermissions> objectPermissions;
    protected List<PermissionSetApexPageAccess> pageAccesses;
    protected List<PermissionSetRecordTypeVisibility> recordTypeVisibilities;
    protected List<PermissionSetTabSetting> tabSettings;
    protected List<PermissionSetUserPermission> userPermissions;

    /**
     * Gets the value of the applicationVisibilities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicationVisibilities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicationVisibilities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetApplicationVisibility }
     * 
     * 
     */
    public List<PermissionSetApplicationVisibility> getApplicationVisibilities() {
        if (applicationVisibilities == null) {
            applicationVisibilities = new ArrayList<PermissionSetApplicationVisibility>();
        }
        return this.applicationVisibilities;
    }

    /**
     * Gets the value of the classAccesses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classAccesses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassAccesses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetApexClassAccess }
     * 
     * 
     */
    public List<PermissionSetApexClassAccess> getClassAccesses() {
        if (classAccesses == null) {
            classAccesses = new ArrayList<PermissionSetApexClassAccess>();
        }
        return this.classAccesses;
    }

    /**
     * Gets the value of the customPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetCustomPermissions }
     * 
     * 
     */
    public List<PermissionSetCustomPermissions> getCustomPermissions() {
        if (customPermissions == null) {
            customPermissions = new ArrayList<PermissionSetCustomPermissions>();
        }
        return this.customPermissions;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the externalDataSourceAccesses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the externalDataSourceAccesses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExternalDataSourceAccesses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetExternalDataSourceAccess }
     * 
     * 
     */
    public List<PermissionSetExternalDataSourceAccess> getExternalDataSourceAccesses() {
        if (externalDataSourceAccesses == null) {
            externalDataSourceAccesses = new ArrayList<PermissionSetExternalDataSourceAccess>();
        }
        return this.externalDataSourceAccesses;
    }

    /**
     * Gets the value of the fieldPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetFieldPermissions }
     * 
     * 
     */
    public List<PermissionSetFieldPermissions> getFieldPermissions() {
        if (fieldPermissions == null) {
            fieldPermissions = new ArrayList<PermissionSetFieldPermissions>();
        }
        return this.fieldPermissions;
    }

    /**
     * Gets the value of the hasActivationRequired property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasActivationRequired() {
        return hasActivationRequired;
    }

    /**
     * Sets the value of the hasActivationRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasActivationRequired(Boolean value) {
        this.hasActivationRequired = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the license property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicense() {
        return license;
    }

    /**
     * Sets the value of the license property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicense(String value) {
        this.license = value;
    }

    /**
     * Gets the value of the objectPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the objectPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getObjectPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetObjectPermissions }
     * 
     * 
     */
    public List<PermissionSetObjectPermissions> getObjectPermissions() {
        if (objectPermissions == null) {
            objectPermissions = new ArrayList<PermissionSetObjectPermissions>();
        }
        return this.objectPermissions;
    }

    /**
     * Gets the value of the pageAccesses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pageAccesses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPageAccesses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetApexPageAccess }
     * 
     * 
     */
    public List<PermissionSetApexPageAccess> getPageAccesses() {
        if (pageAccesses == null) {
            pageAccesses = new ArrayList<PermissionSetApexPageAccess>();
        }
        return this.pageAccesses;
    }

    /**
     * Gets the value of the recordTypeVisibilities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recordTypeVisibilities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecordTypeVisibilities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetRecordTypeVisibility }
     * 
     * 
     */
    public List<PermissionSetRecordTypeVisibility> getRecordTypeVisibilities() {
        if (recordTypeVisibilities == null) {
            recordTypeVisibilities = new ArrayList<PermissionSetRecordTypeVisibility>();
        }
        return this.recordTypeVisibilities;
    }

    /**
     * Gets the value of the tabSettings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tabSettings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTabSettings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetTabSetting }
     * 
     * 
     */
    public List<PermissionSetTabSetting> getTabSettings() {
        if (tabSettings == null) {
            tabSettings = new ArrayList<PermissionSetTabSetting>();
        }
        return this.tabSettings;
    }

    /**
     * Gets the value of the userPermissions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userPermissions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserPermissions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermissionSetUserPermission }
     * 
     * 
     */
    public List<PermissionSetUserPermission> getUserPermissions() {
        if (userPermissions == null) {
            userPermissions = new ArrayList<PermissionSetUserPermission>();
        }
        return this.userPermissions;
    }

}
