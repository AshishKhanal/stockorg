
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DescribeValueTypeResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DescribeValueTypeResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="apiCreatable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="apiDeletable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="apiReadable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="apiUpdatable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="parentField" type="{urn:tooling.soap.sforce.com}ValueTypeField" minOccurs="0"/&gt;
 *         &lt;element name="valueTypeFields" type="{urn:tooling.soap.sforce.com}ValueTypeField" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DescribeValueTypeResult", propOrder = {
    "apiCreatable",
    "apiDeletable",
    "apiReadable",
    "apiUpdatable",
    "parentField",
    "valueTypeFields"
})
public class DescribeValueTypeResult {

    protected boolean apiCreatable;
    protected boolean apiDeletable;
    protected boolean apiReadable;
    protected boolean apiUpdatable;
    protected ValueTypeField parentField;
    protected List<ValueTypeField> valueTypeFields;

    /**
     * Gets the value of the apiCreatable property.
     * 
     */
    public boolean isApiCreatable() {
        return apiCreatable;
    }

    /**
     * Sets the value of the apiCreatable property.
     * 
     */
    public void setApiCreatable(boolean value) {
        this.apiCreatable = value;
    }

    /**
     * Gets the value of the apiDeletable property.
     * 
     */
    public boolean isApiDeletable() {
        return apiDeletable;
    }

    /**
     * Sets the value of the apiDeletable property.
     * 
     */
    public void setApiDeletable(boolean value) {
        this.apiDeletable = value;
    }

    /**
     * Gets the value of the apiReadable property.
     * 
     */
    public boolean isApiReadable() {
        return apiReadable;
    }

    /**
     * Sets the value of the apiReadable property.
     * 
     */
    public void setApiReadable(boolean value) {
        this.apiReadable = value;
    }

    /**
     * Gets the value of the apiUpdatable property.
     * 
     */
    public boolean isApiUpdatable() {
        return apiUpdatable;
    }

    /**
     * Sets the value of the apiUpdatable property.
     * 
     */
    public void setApiUpdatable(boolean value) {
        this.apiUpdatable = value;
    }

    /**
     * Gets the value of the parentField property.
     * 
     * @return
     *     possible object is
     *     {@link ValueTypeField }
     *     
     */
    public ValueTypeField getParentField() {
        return parentField;
    }

    /**
     * Sets the value of the parentField property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueTypeField }
     *     
     */
    public void setParentField(ValueTypeField value) {
        this.parentField = value;
    }

    /**
     * Gets the value of the valueTypeFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valueTypeFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValueTypeFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValueTypeField }
     * 
     * 
     */
    public List<ValueTypeField> getValueTypeFields() {
        if (valueTypeFields == null) {
            valueTypeFields = new ArrayList<ValueTypeField>();
        }
        return this.valueTypeFields;
    }

}
