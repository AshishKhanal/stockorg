
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Oauth2CannedScopes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Oauth2CannedScopes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="id"/&gt;
 *     &lt;enumeration value="api"/&gt;
 *     &lt;enumeration value="web"/&gt;
 *     &lt;enumeration value="full"/&gt;
 *     &lt;enumeration value="chatter_api"/&gt;
 *     &lt;enumeration value="visualforce"/&gt;
 *     &lt;enumeration value="refresh_token"/&gt;
 *     &lt;enumeration value="openid"/&gt;
 *     &lt;enumeration value="profile"/&gt;
 *     &lt;enumeration value="email"/&gt;
 *     &lt;enumeration value="address"/&gt;
 *     &lt;enumeration value="phone"/&gt;
 *     &lt;enumeration value="offline_access"/&gt;
 *     &lt;enumeration value="custom_permissions"/&gt;
 *     &lt;enumeration value="wave_api"/&gt;
 *     &lt;enumeration value="eclair_api"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Oauth2CannedScopes")
@XmlEnum
public enum Oauth2CannedScopes {

    @XmlEnumValue("id")
    ID("id"),
    @XmlEnumValue("api")
    API("api"),
    @XmlEnumValue("web")
    WEB("web"),
    @XmlEnumValue("full")
    FULL("full"),
    @XmlEnumValue("chatter_api")
    CHATTER_API("chatter_api"),
    @XmlEnumValue("visualforce")
    VISUALFORCE("visualforce"),
    @XmlEnumValue("refresh_token")
    REFRESH_TOKEN("refresh_token"),
    @XmlEnumValue("openid")
    OPENID("openid"),
    @XmlEnumValue("profile")
    PROFILE("profile"),
    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("address")
    ADDRESS("address"),
    @XmlEnumValue("phone")
    PHONE("phone"),
    @XmlEnumValue("offline_access")
    OFFLINE_ACCESS("offline_access"),
    @XmlEnumValue("custom_permissions")
    CUSTOM_PERMISSIONS("custom_permissions"),
    @XmlEnumValue("wave_api")
    WAVE_API("wave_api"),
    @XmlEnumValue("eclair_api")
    ECLAIR_API("eclair_api");
    private final String value;

    Oauth2CannedScopes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Oauth2CannedScopes fromValue(String v) {
        for (Oauth2CannedScopes c: Oauth2CannedScopes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
