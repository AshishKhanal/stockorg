
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BackgroundJobRunning.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BackgroundJobRunning"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NO_BACKGROUND_JOB_RUNNING"/&gt;
 *     &lt;enumeration value="ENCRYPTION_AT_REST_PENDING"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "BackgroundJobRunning")
@XmlEnum
public enum BackgroundJobRunning {

    NO_BACKGROUND_JOB_RUNNING,
    ENCRYPTION_AT_REST_PENDING;

    public String value() {
        return name();
    }

    public static BackgroundJobRunning fromValue(String v) {
        return valueOf(v);
    }

}
