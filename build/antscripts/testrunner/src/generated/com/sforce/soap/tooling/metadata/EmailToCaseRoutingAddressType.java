
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailToCaseRoutingAddressType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailToCaseRoutingAddressType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="EmailToCase"/&gt;
 *     &lt;enumeration value="Outlook"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EmailToCaseRoutingAddressType")
@XmlEnum
public enum EmailToCaseRoutingAddressType {

    @XmlEnumValue("EmailToCase")
    EMAIL_TO_CASE("EmailToCase"),
    @XmlEnumValue("Outlook")
    OUTLOOK("Outlook");
    private final String value;

    EmailToCaseRoutingAddressType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailToCaseRoutingAddressType fromValue(String v) {
        for (EmailToCaseRoutingAddressType c: EmailToCaseRoutingAddressType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
