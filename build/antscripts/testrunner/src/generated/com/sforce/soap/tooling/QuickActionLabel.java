
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuickActionLabel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QuickActionLabel"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="LogACall"/&gt;
 *     &lt;enumeration value="LogANote"/&gt;
 *     &lt;enumeration value="New"/&gt;
 *     &lt;enumeration value="NewRecordType"/&gt;
 *     &lt;enumeration value="Update"/&gt;
 *     &lt;enumeration value="NewChild"/&gt;
 *     &lt;enumeration value="NewChildRecordType"/&gt;
 *     &lt;enumeration value="CreateNew"/&gt;
 *     &lt;enumeration value="CreateNewRecordType"/&gt;
 *     &lt;enumeration value="SendEmail"/&gt;
 *     &lt;enumeration value="QuickRecordType"/&gt;
 *     &lt;enumeration value="Quick"/&gt;
 *     &lt;enumeration value="EditDescription"/&gt;
 *     &lt;enumeration value="Defer"/&gt;
 *     &lt;enumeration value="ChangeDueDate"/&gt;
 *     &lt;enumeration value="ChangePriority"/&gt;
 *     &lt;enumeration value="ChangeStatus"/&gt;
 *     &lt;enumeration value="SocialPost"/&gt;
 *     &lt;enumeration value="Escalate"/&gt;
 *     &lt;enumeration value="EscalateToRecord"/&gt;
 *     &lt;enumeration value="OfferFeedback"/&gt;
 *     &lt;enumeration value="RequestFeedback"/&gt;
 *     &lt;enumeration value="AddRecord"/&gt;
 *     &lt;enumeration value="AddMember"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "QuickActionLabel")
@XmlEnum
public enum QuickActionLabel {

    @XmlEnumValue("LogACall")
    LOG_A_CALL("LogACall"),
    @XmlEnumValue("LogANote")
    LOG_A_NOTE("LogANote"),
    @XmlEnumValue("New")
    NEW("New"),
    @XmlEnumValue("NewRecordType")
    NEW_RECORD_TYPE("NewRecordType"),
    @XmlEnumValue("Update")
    UPDATE("Update"),
    @XmlEnumValue("NewChild")
    NEW_CHILD("NewChild"),
    @XmlEnumValue("NewChildRecordType")
    NEW_CHILD_RECORD_TYPE("NewChildRecordType"),
    @XmlEnumValue("CreateNew")
    CREATE_NEW("CreateNew"),
    @XmlEnumValue("CreateNewRecordType")
    CREATE_NEW_RECORD_TYPE("CreateNewRecordType"),
    @XmlEnumValue("SendEmail")
    SEND_EMAIL("SendEmail"),
    @XmlEnumValue("QuickRecordType")
    QUICK_RECORD_TYPE("QuickRecordType"),
    @XmlEnumValue("Quick")
    QUICK("Quick"),
    @XmlEnumValue("EditDescription")
    EDIT_DESCRIPTION("EditDescription"),
    @XmlEnumValue("Defer")
    DEFER("Defer"),
    @XmlEnumValue("ChangeDueDate")
    CHANGE_DUE_DATE("ChangeDueDate"),
    @XmlEnumValue("ChangePriority")
    CHANGE_PRIORITY("ChangePriority"),
    @XmlEnumValue("ChangeStatus")
    CHANGE_STATUS("ChangeStatus"),
    @XmlEnumValue("SocialPost")
    SOCIAL_POST("SocialPost"),
    @XmlEnumValue("Escalate")
    ESCALATE("Escalate"),
    @XmlEnumValue("EscalateToRecord")
    ESCALATE_TO_RECORD("EscalateToRecord"),
    @XmlEnumValue("OfferFeedback")
    OFFER_FEEDBACK("OfferFeedback"),
    @XmlEnumValue("RequestFeedback")
    REQUEST_FEEDBACK("RequestFeedback"),
    @XmlEnumValue("AddRecord")
    ADD_RECORD("AddRecord"),
    @XmlEnumValue("AddMember")
    ADD_MEMBER("AddMember");
    private final String value;

    QuickActionLabel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QuickActionLabel fromValue(String v) {
        for (QuickActionLabel c: QuickActionLabel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
