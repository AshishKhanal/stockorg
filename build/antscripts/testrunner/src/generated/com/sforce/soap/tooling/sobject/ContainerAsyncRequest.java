
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.sforce.soap.tooling.DeployDetails;
import com.sforce.soap.tooling.QueryResult;


/**
 * <p>Java class for ContainerAsyncRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContainerAsyncRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApexClassMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="ApexComponentMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="ApexPageMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="ApexTriggerMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="AuraDefinitionChanges" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="CreatedBy" type="{urn:sobject.tooling.soap.sforce.com}User" minOccurs="0"/&gt;
 *         &lt;element name="CreatedById" type="{urn:tooling.soap.sforce.com}ID" minOccurs="0"/&gt;
 *         &lt;element name="CreatedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="CustomFieldMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="DeployDetails" type="{urn:tooling.soap.sforce.com}DeployDetails" minOccurs="0"/&gt;
 *         &lt;element name="ErrorMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCheckOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRunTests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedBy" type="{urn:sobject.tooling.soap.sforce.com}User" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedById" type="{urn:tooling.soap.sforce.com}ID" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="MetadataContainer" type="{urn:sobject.tooling.soap.sforce.com}MetadataContainer" minOccurs="0"/&gt;
 *         &lt;element name="MetadataContainerId" type="{urn:tooling.soap.sforce.com}ID" minOccurs="0"/&gt;
 *         &lt;element name="MetadataContainerMember" type="{urn:sobject.tooling.soap.sforce.com}sObject" minOccurs="0"/&gt;
 *         &lt;element name="MetadataContainerMemberId" type="{urn:tooling.soap.sforce.com}ID" minOccurs="0"/&gt;
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SystemModstamp" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="ValidationRuleMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowAlertMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowFieldUpdateMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowOutboundMessageMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowRuleMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowTaskMembers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContainerAsyncRequest", propOrder = {
    "apexClassMembers",
    "apexComponentMembers",
    "apexPageMembers",
    "apexTriggerMembers",
    "auraDefinitionChanges",
    "createdBy",
    "createdById",
    "createdDate",
    "customFieldMembers",
    "deployDetails",
    "errorMsg",
    "isCheckOnly",
    "isDeleted",
    "isRunTests",
    "lastModifiedBy",
    "lastModifiedById",
    "lastModifiedDate",
    "metadataContainer",
    "metadataContainerId",
    "metadataContainerMember",
    "metadataContainerMemberId",
    "state",
    "systemModstamp",
    "validationRuleMembers",
    "workflowAlertMembers",
    "workflowFieldUpdateMembers",
    "workflowOutboundMessageMembers",
    "workflowRuleMembers",
    "workflowTaskMembers"
})
public class ContainerAsyncRequest
    extends SObject
{

    @XmlElementRef(name = "ApexClassMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> apexClassMembers;
    @XmlElementRef(name = "ApexComponentMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> apexComponentMembers;
    @XmlElementRef(name = "ApexPageMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> apexPageMembers;
    @XmlElementRef(name = "ApexTriggerMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> apexTriggerMembers;
    @XmlElementRef(name = "AuraDefinitionChanges", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> auraDefinitionChanges;
    @XmlElementRef(name = "CreatedBy", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<User> createdBy;
    @XmlElementRef(name = "CreatedById", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> createdById;
    @XmlElementRef(name = "CreatedDate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> createdDate;
    @XmlElementRef(name = "CustomFieldMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> customFieldMembers;
    @XmlElementRef(name = "DeployDetails", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DeployDetails> deployDetails;
    @XmlElementRef(name = "ErrorMsg", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> errorMsg;
    @XmlElementRef(name = "IsCheckOnly", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCheckOnly;
    @XmlElementRef(name = "IsDeleted", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isDeleted;
    @XmlElementRef(name = "IsRunTests", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRunTests;
    @XmlElementRef(name = "LastModifiedBy", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<User> lastModifiedBy;
    @XmlElementRef(name = "LastModifiedById", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastModifiedById;
    @XmlElementRef(name = "LastModifiedDate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> lastModifiedDate;
    @XmlElementRef(name = "MetadataContainer", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<MetadataContainer> metadataContainer;
    @XmlElementRef(name = "MetadataContainerId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> metadataContainerId;
    @XmlElementRef(name = "MetadataContainerMember", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SObject> metadataContainerMember;
    @XmlElementRef(name = "MetadataContainerMemberId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> metadataContainerMemberId;
    @XmlElementRef(name = "State", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> state;
    @XmlElementRef(name = "SystemModstamp", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> systemModstamp;
    @XmlElementRef(name = "ValidationRuleMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> validationRuleMembers;
    @XmlElementRef(name = "WorkflowAlertMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowAlertMembers;
    @XmlElementRef(name = "WorkflowFieldUpdateMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowFieldUpdateMembers;
    @XmlElementRef(name = "WorkflowOutboundMessageMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowOutboundMessageMembers;
    @XmlElementRef(name = "WorkflowRuleMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowRuleMembers;
    @XmlElementRef(name = "WorkflowTaskMembers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowTaskMembers;

    /**
     * Gets the value of the apexClassMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getApexClassMembers() {
        return apexClassMembers;
    }

    /**
     * Sets the value of the apexClassMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setApexClassMembers(JAXBElement<QueryResult> value) {
        this.apexClassMembers = value;
    }

    /**
     * Gets the value of the apexComponentMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getApexComponentMembers() {
        return apexComponentMembers;
    }

    /**
     * Sets the value of the apexComponentMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setApexComponentMembers(JAXBElement<QueryResult> value) {
        this.apexComponentMembers = value;
    }

    /**
     * Gets the value of the apexPageMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getApexPageMembers() {
        return apexPageMembers;
    }

    /**
     * Sets the value of the apexPageMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setApexPageMembers(JAXBElement<QueryResult> value) {
        this.apexPageMembers = value;
    }

    /**
     * Gets the value of the apexTriggerMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getApexTriggerMembers() {
        return apexTriggerMembers;
    }

    /**
     * Sets the value of the apexTriggerMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setApexTriggerMembers(JAXBElement<QueryResult> value) {
        this.apexTriggerMembers = value;
    }

    /**
     * Gets the value of the auraDefinitionChanges property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getAuraDefinitionChanges() {
        return auraDefinitionChanges;
    }

    /**
     * Sets the value of the auraDefinitionChanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setAuraDefinitionChanges(JAXBElement<QueryResult> value) {
        this.auraDefinitionChanges = value;
    }

    /**
     * Gets the value of the createdBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public JAXBElement<User> getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the value of the createdBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public void setCreatedBy(JAXBElement<User> value) {
        this.createdBy = value;
    }

    /**
     * Gets the value of the createdById property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreatedById() {
        return createdById;
    }

    /**
     * Sets the value of the createdById property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreatedById(JAXBElement<String> value) {
        this.createdById = value;
    }

    /**
     * Gets the value of the createdDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getCreatedDate() {
        return createdDate;
    }

    /**
     * Sets the value of the createdDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setCreatedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.createdDate = value;
    }

    /**
     * Gets the value of the customFieldMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getCustomFieldMembers() {
        return customFieldMembers;
    }

    /**
     * Sets the value of the customFieldMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setCustomFieldMembers(JAXBElement<QueryResult> value) {
        this.customFieldMembers = value;
    }

    /**
     * Gets the value of the deployDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DeployDetails }{@code >}
     *     
     */
    public JAXBElement<DeployDetails> getDeployDetails() {
        return deployDetails;
    }

    /**
     * Sets the value of the deployDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DeployDetails }{@code >}
     *     
     */
    public void setDeployDetails(JAXBElement<DeployDetails> value) {
        this.deployDetails = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setErrorMsg(JAXBElement<String> value) {
        this.errorMsg = value;
    }

    /**
     * Gets the value of the isCheckOnly property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCheckOnly() {
        return isCheckOnly;
    }

    /**
     * Sets the value of the isCheckOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCheckOnly(JAXBElement<Boolean> value) {
        this.isCheckOnly = value;
    }

    /**
     * Gets the value of the isDeleted property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsDeleted() {
        return isDeleted;
    }

    /**
     * Sets the value of the isDeleted property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsDeleted(JAXBElement<Boolean> value) {
        this.isDeleted = value;
    }

    /**
     * Gets the value of the isRunTests property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRunTests() {
        return isRunTests;
    }

    /**
     * Sets the value of the isRunTests property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRunTests(JAXBElement<Boolean> value) {
        this.isRunTests = value;
    }

    /**
     * Gets the value of the lastModifiedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public JAXBElement<User> getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * Sets the value of the lastModifiedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public void setLastModifiedBy(JAXBElement<User> value) {
        this.lastModifiedBy = value;
    }

    /**
     * Gets the value of the lastModifiedById property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastModifiedById() {
        return lastModifiedById;
    }

    /**
     * Sets the value of the lastModifiedById property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastModifiedById(JAXBElement<String> value) {
        this.lastModifiedById = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastModifiedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the metadataContainer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MetadataContainer }{@code >}
     *     
     */
    public JAXBElement<MetadataContainer> getMetadataContainer() {
        return metadataContainer;
    }

    /**
     * Sets the value of the metadataContainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MetadataContainer }{@code >}
     *     
     */
    public void setMetadataContainer(JAXBElement<MetadataContainer> value) {
        this.metadataContainer = value;
    }

    /**
     * Gets the value of the metadataContainerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMetadataContainerId() {
        return metadataContainerId;
    }

    /**
     * Sets the value of the metadataContainerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMetadataContainerId(JAXBElement<String> value) {
        this.metadataContainerId = value;
    }

    /**
     * Gets the value of the metadataContainerMember property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SObject }{@code >}
     *     
     */
    public JAXBElement<SObject> getMetadataContainerMember() {
        return metadataContainerMember;
    }

    /**
     * Sets the value of the metadataContainerMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SObject }{@code >}
     *     
     */
    public void setMetadataContainerMember(JAXBElement<SObject> value) {
        this.metadataContainerMember = value;
    }

    /**
     * Gets the value of the metadataContainerMemberId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMetadataContainerMemberId() {
        return metadataContainerMemberId;
    }

    /**
     * Sets the value of the metadataContainerMemberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMetadataContainerMemberId(JAXBElement<String> value) {
        this.metadataContainerMemberId = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setState(JAXBElement<String> value) {
        this.state = value;
    }

    /**
     * Gets the value of the systemModstamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getSystemModstamp() {
        return systemModstamp;
    }

    /**
     * Sets the value of the systemModstamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setSystemModstamp(JAXBElement<XMLGregorianCalendar> value) {
        this.systemModstamp = value;
    }

    /**
     * Gets the value of the validationRuleMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getValidationRuleMembers() {
        return validationRuleMembers;
    }

    /**
     * Sets the value of the validationRuleMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setValidationRuleMembers(JAXBElement<QueryResult> value) {
        this.validationRuleMembers = value;
    }

    /**
     * Gets the value of the workflowAlertMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowAlertMembers() {
        return workflowAlertMembers;
    }

    /**
     * Sets the value of the workflowAlertMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowAlertMembers(JAXBElement<QueryResult> value) {
        this.workflowAlertMembers = value;
    }

    /**
     * Gets the value of the workflowFieldUpdateMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowFieldUpdateMembers() {
        return workflowFieldUpdateMembers;
    }

    /**
     * Sets the value of the workflowFieldUpdateMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowFieldUpdateMembers(JAXBElement<QueryResult> value) {
        this.workflowFieldUpdateMembers = value;
    }

    /**
     * Gets the value of the workflowOutboundMessageMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowOutboundMessageMembers() {
        return workflowOutboundMessageMembers;
    }

    /**
     * Sets the value of the workflowOutboundMessageMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowOutboundMessageMembers(JAXBElement<QueryResult> value) {
        this.workflowOutboundMessageMembers = value;
    }

    /**
     * Gets the value of the workflowRuleMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowRuleMembers() {
        return workflowRuleMembers;
    }

    /**
     * Sets the value of the workflowRuleMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowRuleMembers(JAXBElement<QueryResult> value) {
        this.workflowRuleMembers = value;
    }

    /**
     * Gets the value of the workflowTaskMembers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowTaskMembers() {
        return workflowTaskMembers;
    }

    /**
     * Sets the value of the workflowTaskMembers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowTaskMembers(JAXBElement<QueryResult> value) {
        this.workflowTaskMembers = value;
    }

}
