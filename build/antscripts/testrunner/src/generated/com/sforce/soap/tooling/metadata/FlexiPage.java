
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FlexiPageType;


/**
 * <p>Java class for FlexiPage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexiPage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="flexiPageRegions" type="{urn:metadata.tooling.soap.sforce.com}FlexiPageRegion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parentFlexiPage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quickActionList" type="{urn:metadata.tooling.soap.sforce.com}QuickActionList" minOccurs="0"/&gt;
 *         &lt;element name="sobjectType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="template" type="{urn:metadata.tooling.soap.sforce.com}FlexiPageTemplateInstance"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}FlexiPageType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexiPage", propOrder = {
    "description",
    "flexiPageRegions",
    "masterLabel",
    "parentFlexiPage",
    "quickActionList",
    "sobjectType",
    "template",
    "type"
})
public class FlexiPage
    extends Metadata
{

    protected String description;
    protected List<FlexiPageRegion> flexiPageRegions;
    @XmlElement(required = true)
    protected String masterLabel;
    protected String parentFlexiPage;
    protected QuickActionList quickActionList;
    protected String sobjectType;
    @XmlElement(required = true)
    protected FlexiPageTemplateInstance template;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FlexiPageType type;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the flexiPageRegions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flexiPageRegions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlexiPageRegions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlexiPageRegion }
     * 
     * 
     */
    public List<FlexiPageRegion> getFlexiPageRegions() {
        if (flexiPageRegions == null) {
            flexiPageRegions = new ArrayList<FlexiPageRegion>();
        }
        return this.flexiPageRegions;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the parentFlexiPage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentFlexiPage() {
        return parentFlexiPage;
    }

    /**
     * Sets the value of the parentFlexiPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentFlexiPage(String value) {
        this.parentFlexiPage = value;
    }

    /**
     * Gets the value of the quickActionList property.
     * 
     * @return
     *     possible object is
     *     {@link QuickActionList }
     *     
     */
    public QuickActionList getQuickActionList() {
        return quickActionList;
    }

    /**
     * Sets the value of the quickActionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuickActionList }
     *     
     */
    public void setQuickActionList(QuickActionList value) {
        this.quickActionList = value;
    }

    /**
     * Gets the value of the sobjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSobjectType() {
        return sobjectType;
    }

    /**
     * Sets the value of the sobjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSobjectType(String value) {
        this.sobjectType = value;
    }

    /**
     * Gets the value of the template property.
     * 
     * @return
     *     possible object is
     *     {@link FlexiPageTemplateInstance }
     *     
     */
    public FlexiPageTemplateInstance getTemplate() {
        return template;
    }

    /**
     * Sets the value of the template property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexiPageTemplateInstance }
     *     
     */
    public void setTemplate(FlexiPageTemplateInstance value) {
        this.template = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link FlexiPageType }
     *     
     */
    public FlexiPageType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexiPageType }
     *     
     */
    public void setType(FlexiPageType value) {
        this.type = value;
    }

}
