
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PolicyAction"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Block"/&gt;
 *     &lt;enumeration value="RaiseSessionLevel"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PolicyAction")
@XmlEnum
public enum PolicyAction {

    @XmlEnumValue("Block")
    BLOCK("Block"),
    @XmlEnumValue("RaiseSessionLevel")
    RAISE_SESSION_LEVEL("RaiseSessionLevel");
    private final String value;

    PolicyAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PolicyAction fromValue(String v) {
        for (PolicyAction c: PolicyAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
