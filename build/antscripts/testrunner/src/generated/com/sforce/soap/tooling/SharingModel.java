
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SharingModel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SharingModel"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Private"/&gt;
 *     &lt;enumeration value="Read"/&gt;
 *     &lt;enumeration value="ReadSelect"/&gt;
 *     &lt;enumeration value="ReadWrite"/&gt;
 *     &lt;enumeration value="ReadWriteTransfer"/&gt;
 *     &lt;enumeration value="FullAccess"/&gt;
 *     &lt;enumeration value="ControlledByParent"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SharingModel")
@XmlEnum
public enum SharingModel {

    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Read")
    READ("Read"),
    @XmlEnumValue("ReadSelect")
    READ_SELECT("ReadSelect"),
    @XmlEnumValue("ReadWrite")
    READ_WRITE("ReadWrite"),
    @XmlEnumValue("ReadWriteTransfer")
    READ_WRITE_TRANSFER("ReadWriteTransfer"),
    @XmlEnumValue("FullAccess")
    FULL_ACCESS("FullAccess"),
    @XmlEnumValue("ControlledByParent")
    CONTROLLED_BY_PARENT("ControlledByParent");
    private final String value;

    SharingModel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SharingModel fromValue(String v) {
        for (SharingModel c: SharingModel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
