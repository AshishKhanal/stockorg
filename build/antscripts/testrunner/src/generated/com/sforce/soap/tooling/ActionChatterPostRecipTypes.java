
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionChatterPostRecipTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionChatterPostRecipTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CollaborationGroup"/&gt;
 *     &lt;enumeration value="Group"/&gt;
 *     &lt;enumeration value="User"/&gt;
 *     &lt;enumeration value="OpportunityTeam"/&gt;
 *     &lt;enumeration value="AccountTeam"/&gt;
 *     &lt;enumeration value="Owner"/&gt;
 *     &lt;enumeration value="Creator"/&gt;
 *     &lt;enumeration value="PartnerUser"/&gt;
 *     &lt;enumeration value="AccountOwner"/&gt;
 *     &lt;enumeration value="CustomerSuccessUser"/&gt;
 *     &lt;enumeration value="ContactLookup"/&gt;
 *     &lt;enumeration value="UserLookup"/&gt;
 *     &lt;enumeration value="CaseTeam"/&gt;
 *     &lt;enumeration value="CampaignMemberDerivedOwner"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionChatterPostRecipTypes")
@XmlEnum
public enum ActionChatterPostRecipTypes {

    @XmlEnumValue("CollaborationGroup")
    COLLABORATION_GROUP("CollaborationGroup"),
    @XmlEnumValue("Group")
    GROUP("Group"),
    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("OpportunityTeam")
    OPPORTUNITY_TEAM("OpportunityTeam"),
    @XmlEnumValue("AccountTeam")
    ACCOUNT_TEAM("AccountTeam"),
    @XmlEnumValue("Owner")
    OWNER("Owner"),
    @XmlEnumValue("Creator")
    CREATOR("Creator"),
    @XmlEnumValue("PartnerUser")
    PARTNER_USER("PartnerUser"),
    @XmlEnumValue("AccountOwner")
    ACCOUNT_OWNER("AccountOwner"),
    @XmlEnumValue("CustomerSuccessUser")
    CUSTOMER_SUCCESS_USER("CustomerSuccessUser"),
    @XmlEnumValue("ContactLookup")
    CONTACT_LOOKUP("ContactLookup"),
    @XmlEnumValue("UserLookup")
    USER_LOOKUP("UserLookup"),
    @XmlEnumValue("CaseTeam")
    CASE_TEAM("CaseTeam"),
    @XmlEnumValue("CampaignMemberDerivedOwner")
    CAMPAIGN_MEMBER_DERIVED_OWNER("CampaignMemberDerivedOwner");
    private final String value;

    ActionChatterPostRecipTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionChatterPostRecipTypes fromValue(String v) {
        for (ActionChatterPostRecipTypes c: ActionChatterPostRecipTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
