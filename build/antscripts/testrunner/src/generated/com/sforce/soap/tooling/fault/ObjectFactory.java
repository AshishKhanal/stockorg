
package com.sforce.soap.tooling.fault;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sforce.soap.tooling.fault package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ApiFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "ApiFault");
    private final static QName _ApiQueryFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "ApiQueryFault");
    private final static QName _LoginFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "LoginFault");
    private final static QName _InvalidQueryLocatorFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "InvalidQueryLocatorFault");
    private final static QName _InvalidNewPasswordFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "InvalidNewPasswordFault");
    private final static QName _InvalidIdFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "InvalidIdFault");
    private final static QName _UnexpectedErrorFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "UnexpectedErrorFault");
    private final static QName _InvalidFieldFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "InvalidFieldFault");
    private final static QName _InvalidSObjectFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "InvalidSObjectFault");
    private final static QName _MalformedQueryFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "MalformedQueryFault");
    private final static QName _MalformedSearchFault_QNAME = new QName("urn:fault.tooling.soap.sforce.com", "MalformedSearchFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sforce.soap.tooling.fault
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ApiFault }
     * 
     */
    public ApiFault createApiFault() {
        return new ApiFault();
    }

    /**
     * Create an instance of {@link ApiQueryFault }
     * 
     */
    public ApiQueryFault createApiQueryFault() {
        return new ApiQueryFault();
    }

    /**
     * Create an instance of {@link LoginFault }
     * 
     */
    public LoginFault createLoginFault() {
        return new LoginFault();
    }

    /**
     * Create an instance of {@link InvalidQueryLocatorFault }
     * 
     */
    public InvalidQueryLocatorFault createInvalidQueryLocatorFault() {
        return new InvalidQueryLocatorFault();
    }

    /**
     * Create an instance of {@link InvalidNewPasswordFault }
     * 
     */
    public InvalidNewPasswordFault createInvalidNewPasswordFault() {
        return new InvalidNewPasswordFault();
    }

    /**
     * Create an instance of {@link InvalidIdFault }
     * 
     */
    public InvalidIdFault createInvalidIdFault() {
        return new InvalidIdFault();
    }

    /**
     * Create an instance of {@link UnexpectedErrorFault }
     * 
     */
    public UnexpectedErrorFault createUnexpectedErrorFault() {
        return new UnexpectedErrorFault();
    }

    /**
     * Create an instance of {@link InvalidFieldFault }
     * 
     */
    public InvalidFieldFault createInvalidFieldFault() {
        return new InvalidFieldFault();
    }

    /**
     * Create an instance of {@link InvalidSObjectFault }
     * 
     */
    public InvalidSObjectFault createInvalidSObjectFault() {
        return new InvalidSObjectFault();
    }

    /**
     * Create an instance of {@link MalformedQueryFault }
     * 
     */
    public MalformedQueryFault createMalformedQueryFault() {
        return new MalformedQueryFault();
    }

    /**
     * Create an instance of {@link MalformedSearchFault }
     * 
     */
    public MalformedSearchFault createMalformedSearchFault() {
        return new MalformedSearchFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApiFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "ApiFault")
    public JAXBElement<ApiFault> createApiFault(ApiFault value) {
        return new JAXBElement<ApiFault>(_ApiFault_QNAME, ApiFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApiQueryFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "ApiQueryFault")
    public JAXBElement<ApiQueryFault> createApiQueryFault(ApiQueryFault value) {
        return new JAXBElement<ApiQueryFault>(_ApiQueryFault_QNAME, ApiQueryFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "LoginFault")
    public JAXBElement<Object> createLoginFault(Object value) {
        return new JAXBElement<Object>(_LoginFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "InvalidQueryLocatorFault")
    public JAXBElement<Object> createInvalidQueryLocatorFault(Object value) {
        return new JAXBElement<Object>(_InvalidQueryLocatorFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "InvalidNewPasswordFault")
    public JAXBElement<Object> createInvalidNewPasswordFault(Object value) {
        return new JAXBElement<Object>(_InvalidNewPasswordFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "InvalidIdFault")
    public JAXBElement<Object> createInvalidIdFault(Object value) {
        return new JAXBElement<Object>(_InvalidIdFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "UnexpectedErrorFault")
    public JAXBElement<Object> createUnexpectedErrorFault(Object value) {
        return new JAXBElement<Object>(_UnexpectedErrorFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "InvalidFieldFault")
    public JAXBElement<Object> createInvalidFieldFault(Object value) {
        return new JAXBElement<Object>(_InvalidFieldFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "InvalidSObjectFault")
    public JAXBElement<Object> createInvalidSObjectFault(Object value) {
        return new JAXBElement<Object>(_InvalidSObjectFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "MalformedQueryFault")
    public JAXBElement<Object> createMalformedQueryFault(Object value) {
        return new JAXBElement<Object>(_MalformedQueryFault_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fault.tooling.soap.sforce.com", name = "MalformedSearchFault")
    public JAXBElement<Object> createMalformedSearchFault(Object value) {
        return new JAXBElement<Object>(_MalformedSearchFault_QNAME, Object.class, null, value);
    }

}
