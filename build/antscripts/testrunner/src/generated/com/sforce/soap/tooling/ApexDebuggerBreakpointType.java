
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerBreakpointType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerBreakpointType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Line"/&gt;
 *     &lt;enumeration value="Call"/&gt;
 *     &lt;enumeration value="Return"/&gt;
 *     &lt;enumeration value="Exception"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerBreakpointType")
@XmlEnum
public enum ApexDebuggerBreakpointType {

    @XmlEnumValue("Line")
    LINE("Line"),
    @XmlEnumValue("Call")
    CALL("Call"),
    @XmlEnumValue("Return")
    RETURN("Return"),
    @XmlEnumValue("Exception")
    EXCEPTION("Exception");
    private final String value;

    ApexDebuggerBreakpointType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerBreakpointType fromValue(String v) {
        for (ApexDebuggerBreakpointType c: ApexDebuggerBreakpointType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
