
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuestionRestriction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QuestionRestriction"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="DoesNotContainPassword"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "QuestionRestriction")
@XmlEnum
public enum QuestionRestriction {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("DoesNotContainPassword")
    DOES_NOT_CONTAIN_PASSWORD("DoesNotContainPassword");
    private final String value;

    QuestionRestriction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QuestionRestriction fromValue(String v) {
        for (QuestionRestriction c: QuestionRestriction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
