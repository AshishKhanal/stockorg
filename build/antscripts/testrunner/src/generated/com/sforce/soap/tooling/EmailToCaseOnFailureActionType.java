
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailToCaseOnFailureActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailToCaseOnFailureActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Bounce"/&gt;
 *     &lt;enumeration value="Discard"/&gt;
 *     &lt;enumeration value="Requeue"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EmailToCaseOnFailureActionType")
@XmlEnum
public enum EmailToCaseOnFailureActionType {

    @XmlEnumValue("Bounce")
    BOUNCE("Bounce"),
    @XmlEnumValue("Discard")
    DISCARD("Discard"),
    @XmlEnumValue("Requeue")
    REQUEUE("Requeue");
    private final String value;

    EmailToCaseOnFailureActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailToCaseOnFailureActionType fromValue(String v) {
        for (EmailToCaseOnFailureActionType c: EmailToCaseOnFailureActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
