
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FolderAccessTypes;
import com.sforce.soap.tooling.PublicFolderAccess;


/**
 * <p>Java class for Folder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Folder"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accessType" type="{urn:tooling.soap.sforce.com}FolderAccessTypes" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="publicFolderAccess" type="{urn:tooling.soap.sforce.com}PublicFolderAccess" minOccurs="0"/&gt;
 *         &lt;element name="sharedTo" type="{urn:metadata.tooling.soap.sforce.com}SharedTo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Folder", propOrder = {
    "accessType",
    "name",
    "publicFolderAccess",
    "sharedTo"
})
@XmlSeeAlso({
    DocumentFolder.class,
    EmailFolder.class
})
public class Folder
    extends Metadata
{

    @XmlSchemaType(name = "string")
    protected FolderAccessTypes accessType;
    @XmlElement(required = true)
    protected String name;
    @XmlSchemaType(name = "string")
    protected PublicFolderAccess publicFolderAccess;
    protected SharedTo sharedTo;

    /**
     * Gets the value of the accessType property.
     * 
     * @return
     *     possible object is
     *     {@link FolderAccessTypes }
     *     
     */
    public FolderAccessTypes getAccessType() {
        return accessType;
    }

    /**
     * Sets the value of the accessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FolderAccessTypes }
     *     
     */
    public void setAccessType(FolderAccessTypes value) {
        this.accessType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the publicFolderAccess property.
     * 
     * @return
     *     possible object is
     *     {@link PublicFolderAccess }
     *     
     */
    public PublicFolderAccess getPublicFolderAccess() {
        return publicFolderAccess;
    }

    /**
     * Sets the value of the publicFolderAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublicFolderAccess }
     *     
     */
    public void setPublicFolderAccess(PublicFolderAccess value) {
        this.publicFolderAccess = value;
    }

    /**
     * Gets the value of the sharedTo property.
     * 
     * @return
     *     possible object is
     *     {@link SharedTo }
     *     
     */
    public SharedTo getSharedTo() {
        return sharedTo;
    }

    /**
     * Sets the value of the sharedTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SharedTo }
     *     
     */
    public void setSharedTo(SharedTo value) {
        this.sharedTo = value;
    }

}
