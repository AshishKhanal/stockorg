
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FileType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *     &lt;enumeration value="PDF"/&gt;
 *     &lt;enumeration value="POWER_POINT"/&gt;
 *     &lt;enumeration value="POWER_POINT_X"/&gt;
 *     &lt;enumeration value="POWER_POINT_M"/&gt;
 *     &lt;enumeration value="POWER_POINT_T"/&gt;
 *     &lt;enumeration value="WORD"/&gt;
 *     &lt;enumeration value="WORD_X"/&gt;
 *     &lt;enumeration value="WORD_M"/&gt;
 *     &lt;enumeration value="WORD_T"/&gt;
 *     &lt;enumeration value="PPS"/&gt;
 *     &lt;enumeration value="PPSX"/&gt;
 *     &lt;enumeration value="EXCEL"/&gt;
 *     &lt;enumeration value="EXCEL_X"/&gt;
 *     &lt;enumeration value="EXCEL_M"/&gt;
 *     &lt;enumeration value="EXCEL_T"/&gt;
 *     &lt;enumeration value="GOOGLE_DOCUMENT"/&gt;
 *     &lt;enumeration value="GOOGLE_PRESENTATION"/&gt;
 *     &lt;enumeration value="GOOGLE_SPREADSHEET"/&gt;
 *     &lt;enumeration value="GOOGLE_DRAWING"/&gt;
 *     &lt;enumeration value="GOOGLE_FORM"/&gt;
 *     &lt;enumeration value="GOOGLE_SCRIPT"/&gt;
 *     &lt;enumeration value="LINK"/&gt;
 *     &lt;enumeration value="SLIDE"/&gt;
 *     &lt;enumeration value="AAC"/&gt;
 *     &lt;enumeration value="ACGI"/&gt;
 *     &lt;enumeration value="AI"/&gt;
 *     &lt;enumeration value="AVI"/&gt;
 *     &lt;enumeration value="BMP"/&gt;
 *     &lt;enumeration value="BOXNOTE"/&gt;
 *     &lt;enumeration value="CSV"/&gt;
 *     &lt;enumeration value="EPS"/&gt;
 *     &lt;enumeration value="EXE"/&gt;
 *     &lt;enumeration value="FLASH"/&gt;
 *     &lt;enumeration value="GIF"/&gt;
 *     &lt;enumeration value="GZIP"/&gt;
 *     &lt;enumeration value="HTM"/&gt;
 *     &lt;enumeration value="HTML"/&gt;
 *     &lt;enumeration value="HTX"/&gt;
 *     &lt;enumeration value="JPEG"/&gt;
 *     &lt;enumeration value="JPE"/&gt;
 *     &lt;enumeration value="PJP"/&gt;
 *     &lt;enumeration value="PJPEG"/&gt;
 *     &lt;enumeration value="JFIF"/&gt;
 *     &lt;enumeration value="JPG"/&gt;
 *     &lt;enumeration value="JS"/&gt;
 *     &lt;enumeration value="MHTM"/&gt;
 *     &lt;enumeration value="MHTML"/&gt;
 *     &lt;enumeration value="MP3"/&gt;
 *     &lt;enumeration value="M4A"/&gt;
 *     &lt;enumeration value="M4V"/&gt;
 *     &lt;enumeration value="MP4"/&gt;
 *     &lt;enumeration value="MPEG"/&gt;
 *     &lt;enumeration value="MPG"/&gt;
 *     &lt;enumeration value="MOV"/&gt;
 *     &lt;enumeration value="ODP"/&gt;
 *     &lt;enumeration value="ODS"/&gt;
 *     &lt;enumeration value="ODT"/&gt;
 *     &lt;enumeration value="OGV"/&gt;
 *     &lt;enumeration value="PNG"/&gt;
 *     &lt;enumeration value="PSD"/&gt;
 *     &lt;enumeration value="RTF"/&gt;
 *     &lt;enumeration value="SHTM"/&gt;
 *     &lt;enumeration value="SHTML"/&gt;
 *     &lt;enumeration value="SNOTE"/&gt;
 *     &lt;enumeration value="STYPI"/&gt;
 *     &lt;enumeration value="SVG"/&gt;
 *     &lt;enumeration value="SVGZ"/&gt;
 *     &lt;enumeration value="TEXT"/&gt;
 *     &lt;enumeration value="THTML"/&gt;
 *     &lt;enumeration value="VISIO"/&gt;
 *     &lt;enumeration value="WMV"/&gt;
 *     &lt;enumeration value="WRF"/&gt;
 *     &lt;enumeration value="XML"/&gt;
 *     &lt;enumeration value="ZIP"/&gt;
 *     &lt;enumeration value="XZIP"/&gt;
 *     &lt;enumeration value="WMA"/&gt;
 *     &lt;enumeration value="XSN"/&gt;
 *     &lt;enumeration value="TRTF"/&gt;
 *     &lt;enumeration value="TXML"/&gt;
 *     &lt;enumeration value="WEBVIEW"/&gt;
 *     &lt;enumeration value="RFC822"/&gt;
 *     &lt;enumeration value="ASF"/&gt;
 *     &lt;enumeration value="DWG"/&gt;
 *     &lt;enumeration value="JAR"/&gt;
 *     &lt;enumeration value="XJS"/&gt;
 *     &lt;enumeration value="OPX"/&gt;
 *     &lt;enumeration value="XPSD"/&gt;
 *     &lt;enumeration value="TIF"/&gt;
 *     &lt;enumeration value="TIFF"/&gt;
 *     &lt;enumeration value="WAV"/&gt;
 *     &lt;enumeration value="CSS"/&gt;
 *     &lt;enumeration value="THUMB720BY480"/&gt;
 *     &lt;enumeration value="THUMB240BY180"/&gt;
 *     &lt;enumeration value="THUMB120BY90"/&gt;
 *     &lt;enumeration value="ALLTHUMBS"/&gt;
 *     &lt;enumeration value="PAGED_FLASH"/&gt;
 *     &lt;enumeration value="PACK"/&gt;
 *     &lt;enumeration value="C"/&gt;
 *     &lt;enumeration value="CPP"/&gt;
 *     &lt;enumeration value="WORDT"/&gt;
 *     &lt;enumeration value="INI"/&gt;
 *     &lt;enumeration value="JAVA"/&gt;
 *     &lt;enumeration value="LOG"/&gt;
 *     &lt;enumeration value="POWER_POINTT"/&gt;
 *     &lt;enumeration value="SQL"/&gt;
 *     &lt;enumeration value="XHTML"/&gt;
 *     &lt;enumeration value="EXCELT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FileType")
@XmlEnum
public enum FileType {

    UNKNOWN("UNKNOWN"),
    PDF("PDF"),
    POWER_POINT("POWER_POINT"),
    POWER_POINT_X("POWER_POINT_X"),
    POWER_POINT_M("POWER_POINT_M"),
    POWER_POINT_T("POWER_POINT_T"),
    WORD("WORD"),
    WORD_X("WORD_X"),
    WORD_M("WORD_M"),
    WORD_T("WORD_T"),
    PPS("PPS"),
    PPSX("PPSX"),
    EXCEL("EXCEL"),
    EXCEL_X("EXCEL_X"),
    EXCEL_M("EXCEL_M"),
    EXCEL_T("EXCEL_T"),
    GOOGLE_DOCUMENT("GOOGLE_DOCUMENT"),
    GOOGLE_PRESENTATION("GOOGLE_PRESENTATION"),
    GOOGLE_SPREADSHEET("GOOGLE_SPREADSHEET"),
    GOOGLE_DRAWING("GOOGLE_DRAWING"),
    GOOGLE_FORM("GOOGLE_FORM"),
    GOOGLE_SCRIPT("GOOGLE_SCRIPT"),
    LINK("LINK"),
    SLIDE("SLIDE"),
    AAC("AAC"),
    ACGI("ACGI"),
    AI("AI"),
    AVI("AVI"),
    BMP("BMP"),
    BOXNOTE("BOXNOTE"),
    CSV("CSV"),
    EPS("EPS"),
    EXE("EXE"),
    FLASH("FLASH"),
    GIF("GIF"),
    GZIP("GZIP"),
    HTM("HTM"),
    HTML("HTML"),
    HTX("HTX"),
    JPEG("JPEG"),
    JPE("JPE"),
    PJP("PJP"),
    PJPEG("PJPEG"),
    JFIF("JFIF"),
    JPG("JPG"),
    JS("JS"),
    MHTM("MHTM"),
    MHTML("MHTML"),
    @XmlEnumValue("MP3")
    MP_3("MP3"),
    @XmlEnumValue("M4A")
    M_4_A("M4A"),
    @XmlEnumValue("M4V")
    M_4_V("M4V"),
    @XmlEnumValue("MP4")
    MP_4("MP4"),
    MPEG("MPEG"),
    MPG("MPG"),
    MOV("MOV"),
    ODP("ODP"),
    ODS("ODS"),
    ODT("ODT"),
    OGV("OGV"),
    PNG("PNG"),
    PSD("PSD"),
    RTF("RTF"),
    SHTM("SHTM"),
    SHTML("SHTML"),
    SNOTE("SNOTE"),
    STYPI("STYPI"),
    SVG("SVG"),
    SVGZ("SVGZ"),
    TEXT("TEXT"),
    THTML("THTML"),
    VISIO("VISIO"),
    WMV("WMV"),
    WRF("WRF"),
    XML("XML"),
    ZIP("ZIP"),
    XZIP("XZIP"),
    WMA("WMA"),
    XSN("XSN"),
    TRTF("TRTF"),
    TXML("TXML"),
    WEBVIEW("WEBVIEW"),
    @XmlEnumValue("RFC822")
    RFC_822("RFC822"),
    ASF("ASF"),
    DWG("DWG"),
    JAR("JAR"),
    XJS("XJS"),
    OPX("OPX"),
    XPSD("XPSD"),
    TIF("TIF"),
    TIFF("TIFF"),
    WAV("WAV"),
    CSS("CSS"),
    @XmlEnumValue("THUMB720BY480")
    THUMB_720_BY_480("THUMB720BY480"),
    @XmlEnumValue("THUMB240BY180")
    THUMB_240_BY_180("THUMB240BY180"),
    @XmlEnumValue("THUMB120BY90")
    THUMB_120_BY_90("THUMB120BY90"),
    ALLTHUMBS("ALLTHUMBS"),
    PAGED_FLASH("PAGED_FLASH"),
    PACK("PACK"),
    C("C"),
    CPP("CPP"),
    WORDT("WORDT"),
    INI("INI"),
    JAVA("JAVA"),
    LOG("LOG"),
    POWER_POINTT("POWER_POINTT"),
    SQL("SQL"),
    XHTML("XHTML"),
    EXCELT("EXCELT");
    private final String value;

    FileType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FileType fromValue(String v) {
        for (FileType c: FileType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
