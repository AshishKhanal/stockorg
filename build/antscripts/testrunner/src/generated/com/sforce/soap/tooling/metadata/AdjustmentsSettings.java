
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdjustmentsSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdjustmentsSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableAdjustments" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="enableOwnerAdjustments" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdjustmentsSettings", propOrder = {
    "enableAdjustments",
    "enableOwnerAdjustments"
})
public class AdjustmentsSettings {

    protected boolean enableAdjustments;
    protected boolean enableOwnerAdjustments;

    /**
     * Gets the value of the enableAdjustments property.
     * 
     */
    public boolean isEnableAdjustments() {
        return enableAdjustments;
    }

    /**
     * Sets the value of the enableAdjustments property.
     * 
     */
    public void setEnableAdjustments(boolean value) {
        this.enableAdjustments = value;
    }

    /**
     * Gets the value of the enableOwnerAdjustments property.
     * 
     */
    public boolean isEnableOwnerAdjustments() {
        return enableOwnerAdjustments;
    }

    /**
     * Sets the value of the enableOwnerAdjustments property.
     * 
     */
    public void setEnableOwnerAdjustments(boolean value) {
        this.enableOwnerAdjustments = value;
    }

}
