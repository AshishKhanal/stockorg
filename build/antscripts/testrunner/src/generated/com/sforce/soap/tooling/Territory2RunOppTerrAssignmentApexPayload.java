
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Territory2RunOppTerrAssignmentApexPayload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Territory2RunOppTerrAssignmentApexPayload"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:tooling.soap.sforce.com}OperationPayload"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="excludeClosedOpportunities" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="opportunityCloseDateFrom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="opportunityCloseDateTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="opportunityLastModifiedDateFrom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="opportunityLastModifiedDateTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="territoryModelId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Territory2RunOppTerrAssignmentApexPayload", propOrder = {
    "excludeClosedOpportunities",
    "opportunityCloseDateFrom",
    "opportunityCloseDateTo",
    "opportunityLastModifiedDateFrom",
    "opportunityLastModifiedDateTo",
    "territoryModelId"
})
public class Territory2RunOppTerrAssignmentApexPayload
    extends OperationPayload
{

    @XmlElement(required = true)
    protected String excludeClosedOpportunities;
    @XmlElement(required = true)
    protected String opportunityCloseDateFrom;
    @XmlElement(required = true)
    protected String opportunityCloseDateTo;
    @XmlElement(required = true)
    protected String opportunityLastModifiedDateFrom;
    @XmlElement(required = true)
    protected String opportunityLastModifiedDateTo;
    @XmlElement(required = true)
    protected String territoryModelId;

    /**
     * Gets the value of the excludeClosedOpportunities property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludeClosedOpportunities() {
        return excludeClosedOpportunities;
    }

    /**
     * Sets the value of the excludeClosedOpportunities property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludeClosedOpportunities(String value) {
        this.excludeClosedOpportunities = value;
    }

    /**
     * Gets the value of the opportunityCloseDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityCloseDateFrom() {
        return opportunityCloseDateFrom;
    }

    /**
     * Sets the value of the opportunityCloseDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityCloseDateFrom(String value) {
        this.opportunityCloseDateFrom = value;
    }

    /**
     * Gets the value of the opportunityCloseDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityCloseDateTo() {
        return opportunityCloseDateTo;
    }

    /**
     * Sets the value of the opportunityCloseDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityCloseDateTo(String value) {
        this.opportunityCloseDateTo = value;
    }

    /**
     * Gets the value of the opportunityLastModifiedDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityLastModifiedDateFrom() {
        return opportunityLastModifiedDateFrom;
    }

    /**
     * Sets the value of the opportunityLastModifiedDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityLastModifiedDateFrom(String value) {
        this.opportunityLastModifiedDateFrom = value;
    }

    /**
     * Gets the value of the opportunityLastModifiedDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpportunityLastModifiedDateTo() {
        return opportunityLastModifiedDateTo;
    }

    /**
     * Sets the value of the opportunityLastModifiedDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpportunityLastModifiedDateTo(String value) {
        this.opportunityLastModifiedDateTo = value;
    }

    /**
     * Gets the value of the territoryModelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerritoryModelId() {
        return territoryModelId;
    }

    /**
     * Sets the value of the territoryModelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerritoryModelId(String value) {
        this.territoryModelId = value;
    }

}
