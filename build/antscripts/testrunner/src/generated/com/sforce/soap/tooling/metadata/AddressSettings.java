
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddressSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="countriesAndStates" type="{urn:metadata.tooling.soap.sforce.com}CountriesAndStates"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressSettings", propOrder = {
    "countriesAndStates"
})
public class AddressSettings
    extends MetadataForSettings
{

    @XmlElement(required = true)
    protected CountriesAndStates countriesAndStates;

    /**
     * Gets the value of the countriesAndStates property.
     * 
     * @return
     *     possible object is
     *     {@link CountriesAndStates }
     *     
     */
    public CountriesAndStates getCountriesAndStates() {
        return countriesAndStates;
    }

    /**
     * Sets the value of the countriesAndStates property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountriesAndStates }
     *     
     */
    public void setCountriesAndStates(CountriesAndStates value) {
        this.countriesAndStates = value;
    }

}
