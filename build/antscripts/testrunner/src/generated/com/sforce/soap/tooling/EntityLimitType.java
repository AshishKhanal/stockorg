
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntityLimitType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntityLimitType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CustomFields"/&gt;
 *     &lt;enumeration value="RollupSummary"/&gt;
 *     &lt;enumeration value="CustomRelationship"/&gt;
 *     &lt;enumeration value="ActiveRules"/&gt;
 *     &lt;enumeration value="TotalRules"/&gt;
 *     &lt;enumeration value="ApprovalProcesses"/&gt;
 *     &lt;enumeration value="ActiveLookupFilters"/&gt;
 *     &lt;enumeration value="ActiveValidationRules"/&gt;
 *     &lt;enumeration value="VLookup"/&gt;
 *     &lt;enumeration value="CbsSharingRules"/&gt;
 *     &lt;enumeration value="SharingRules"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EntityLimitType")
@XmlEnum
public enum EntityLimitType {

    @XmlEnumValue("CustomFields")
    CUSTOM_FIELDS("CustomFields"),
    @XmlEnumValue("RollupSummary")
    ROLLUP_SUMMARY("RollupSummary"),
    @XmlEnumValue("CustomRelationship")
    CUSTOM_RELATIONSHIP("CustomRelationship"),
    @XmlEnumValue("ActiveRules")
    ACTIVE_RULES("ActiveRules"),
    @XmlEnumValue("TotalRules")
    TOTAL_RULES("TotalRules"),
    @XmlEnumValue("ApprovalProcesses")
    APPROVAL_PROCESSES("ApprovalProcesses"),
    @XmlEnumValue("ActiveLookupFilters")
    ACTIVE_LOOKUP_FILTERS("ActiveLookupFilters"),
    @XmlEnumValue("ActiveValidationRules")
    ACTIVE_VALIDATION_RULES("ActiveValidationRules"),
    @XmlEnumValue("VLookup")
    V_LOOKUP("VLookup"),
    @XmlEnumValue("CbsSharingRules")
    CBS_SHARING_RULES("CbsSharingRules"),
    @XmlEnumValue("SharingRules")
    SHARING_RULES("SharingRules");
    private final String value;

    EntityLimitType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EntityLimitType fromValue(String v) {
        for (EntityLimitType c: EntityLimitType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
