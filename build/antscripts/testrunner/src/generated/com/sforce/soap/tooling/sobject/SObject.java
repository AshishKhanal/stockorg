
package com.sforce.soap.tooling.sobject;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sObject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fieldsToNull" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Id" type="{urn:tooling.soap.sforce.com}ID"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sObject", propOrder = {
    "fieldsToNull",
    "id"
})
@XmlSeeAlso({
    AccountSettings.class,
    WorkflowAlert.class,
    WorkflowFieldUpdate.class,
    WorkflowOutboundMessage.class,
    ActionOverride.class,
    WorkflowTask.class,
    ActivitiesSettings.class,
    AddressSettings.class,
    AdjustmentsSettings.class,
    ApexClass.class,
    ApexClassMember.class,
    ApexCodeCoverage.class,
    ApexCodeCoverageAggregate.class,
    ApexComponent.class,
    ApexComponentMember.class,
    ApexEmailNotification.class,
    ApexExecutionOverlayAction.class,
    ApexExecutionOverlayResult.class,
    ApexLog.class,
    ApexOrgWideCoverage.class,
    ApexPage.class,
    ApexPageInfo.class,
    ApexPageMember.class,
    ApexTestQueueItem.class,
    ApexTestResult.class,
    ApexTestResultLimits.class,
    ApexTestRunResult.class,
    ApexTestSuite.class,
    ApexTrigger.class,
    ApexTriggerMember.class,
    AssignmentRule.class,
    AsyncApexJob.class,
    AuraDefinition.class,
    AuraDefinitionChange.class,
    AutoResponseRule.class,
    BusinessHoursEntry.class,
    BusinessHoursSettings.class,
    BusinessProcess.class,
    CaseSettings.class,
    Certificate.class,
    ChatterAnswersSettings.class,
    ChatterMobileSettings.class,
    CleanDataService.class,
    CleanRule.class,
    CompactLayout.class,
    CompactLayoutInfo.class,
    CompactLayoutItemInfo.class,
    CompanySettings.class,
    ContainerAsyncRequest.class,
    ContractSettings.class,
    CountriesAndStates.class,
    Country.class,
    CspTrustedSite.class,
    CustomObject.class,
    CustomField.class,
    CustomFieldMember.class,
    HomePageLayout.class,
    CustomTab.class,
    DashboardMobileSettings.class,
    DataDotComEntitySetting.class,
    DataSourceSettings.class,
    DataType.class,
    DebugLevel.class,
    Document.class,
    EmailTemplate.class,
    EmailToCaseRoutingAddress.class,
    EmailToCaseSettings.class,
    EmbeddedServiceBranding.class,
    EmbeddedServiceConfig.class,
    EntitlementSettings.class,
    EntityDefinition.class,
    EntityLimit.class,
    EntityParticle.class,
    ExternalServiceRegistration.class,
    ExternalString.class,
    FeedItemSettings.class,
    PostTemplate.class,
    FieldDefinition.class,
    FieldMapping.class,
    FieldMappingField.class,
    FieldMappingRow.class,
    FieldSet.class,
    FileTypeDispositionAssignmentBean.class,
    FileUploadAndDownloadSecuritySettings.class,
    FindSimilarOppFilter.class,
    FlexiPage.class,
    FlowDefinition.class,
    ForecastRangeSettings.class,
    ForecastingSettings.class,
    ForecastingTypeSettings.class,
    FormulaFunction.class,
    FormulaFunctionCategory.class,
    FormulaOperator.class,
    Group.class,
    IDEPerspective.class,
    IDEWorkspace.class,
    IdeasSettings.class,
    Flow.class,
    IpRange.class,
    KeywordList.class,
    KnowledgeAnswerSettings.class,
    KnowledgeCaseField.class,
    KnowledgeCaseFieldsSettings.class,
    KnowledgeCaseSettings.class,
    KnowledgeLanguage.class,
    KnowledgeLanguageSettings.class,
    KnowledgeSettings.class,
    KnowledgeSitesSettings.class,
    KnowledgeSuggestedArticlesSettings.class,
    KnowledgeWorkOrderField.class,
    KnowledgeWorkOrderFieldsSettings.class,
    KnowledgeWorkOrderLineItemField.class,
    KnowledgeWorkOrderLineItemFieldsSettings.class,
    Layout.class,
    LeadConvertSettings.class,
    LiveAgentSettings.class,
    MacroSettings.class,
    MenuItem.class,
    MetadataContainer.class,
    MetadataContainerMember.class,
    MobileSettings.class,
    ModerationRule.class,
    Name.class,
    NameSettings.class,
    LookupFilter.class,
    NetworkAccess.class,
    ObjectSearchSetting.class,
    OperationLog.class,
    OpportunityListFieldsSelectedSettings.class,
    OpportunityListFieldsUnselectedSettings.class,
    OpportunitySettings.class,
    OrderSettings.class,
    OrgPreferenceSettings.class,
    OrganizationSettingsDetail.class,
    OwnerChangeOptionInfo.class,
    HomePageComponent.class,
    PasswordPolicies.class,
    PathAssistant.class,
    PathAssistantSettings.class,
    PathAssistantStepInfo.class,
    PathAssistantStepItem.class,
    PermissionSet.class,
    PermissionSetTabSetting.class,
    PersonalJourneySettings.class,
    ProductSettings.class,
    Profile.class,
    ProfileActionOverride.class,
    ProfileLayout.class,
    Publisher.class,
    QuickActionDefinition.class,
    QuickActionList.class,
    QuickActionListItem.class,
    QuotasSettings.class,
    QuoteSettings.class,
    RecentlyViewed.class,
    RecordType.class,
    RelationshipDomain.class,
    RelationshipInfo.class,
    RemoteProxy.class,
    SFDCMobileSettings.class,
    Scontrol.class,
    SearchLayout.class,
    SearchSettings.class,
    SearchSettingsByObject.class,
    SecurityHealthCheck.class,
    SecurityHealthCheckRisks.class,
    SecuritySettings.class,
    SessionSettings.class,
    GlobalValueSet.class,
    SiteDetail.class,
    StandardAction.class,
    StandardValueSet.class,
    State.class,
    StaticResource.class,
    Territory2Settings.class,
    Territory2SettingsOpportunityFilter.class,
    TestSuiteMembership.class,
    TouchMobileSettings.class,
    TraceFlag.class,
    TransactionSecurityPolicy.class,
    User.class,
    UserCriteria.class,
    UserEntityAccess.class,
    UserFieldAccess.class,
    UserPreference.class,
    UserProvisioningConfig.class,
    UserRole.class,
    ValidationRule.class,
    ValidationRuleMember.class,
    WebLink.class,
    WebToCaseSettings.class,
    WorkflowAlertMember.class,
    WorkflowFieldUpdateMember.class,
    WorkflowOutboundMessageMember.class,
    WorkflowRule.class,
    WorkflowRuleMember.class,
    WorkflowTaskMember.class
})
public class SObject {

    @XmlElement(nillable = true)
    protected List<String> fieldsToNull;
    @XmlElement(name = "Id", required = true, nillable = true)
    protected String id;

    /**
     * Gets the value of the fieldsToNull property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldsToNull property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldsToNull().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFieldsToNull() {
        if (fieldsToNull == null) {
            fieldsToNull = new ArrayList<String>();
        }
        return this.fieldsToNull;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

}
