
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SetupObjectVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SetupObjectVisibility"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Protected"/&gt;
 *     &lt;enumeration value="Public"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SetupObjectVisibility")
@XmlEnum
public enum SetupObjectVisibility {

    @XmlEnumValue("Protected")
    PROTECTED("Protected"),
    @XmlEnumValue("Public")
    PUBLIC("Public");
    private final String value;

    SetupObjectVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SetupObjectVisibility fromValue(String v) {
        for (SetupObjectVisibility c: SetupObjectVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
