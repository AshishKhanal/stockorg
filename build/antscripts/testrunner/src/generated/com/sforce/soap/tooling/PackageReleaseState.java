
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PackageReleaseState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PackageReleaseState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Beta"/&gt;
 *     &lt;enumeration value="Released"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PackageReleaseState")
@XmlEnum
public enum PackageReleaseState {

    @XmlEnumValue("Beta")
    BETA("Beta"),
    @XmlEnumValue("Released")
    RELEASED("Released");
    private final String value;

    PackageReleaseState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PackageReleaseState fromValue(String v) {
        for (PackageReleaseState c: PackageReleaseState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
