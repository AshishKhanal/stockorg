
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexExecutionOverlayActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexExecutionOverlayActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="Apex"/&gt;
 *     &lt;enumeration value="SOQL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexExecutionOverlayActionType")
@XmlEnum
public enum ApexExecutionOverlayActionType {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Apex")
    APEX("Apex"),
    SOQL("SOQL");
    private final String value;

    ApexExecutionOverlayActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexExecutionOverlayActionType fromValue(String v) {
        for (ApexExecutionOverlayActionType c: ApexExecutionOverlayActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
