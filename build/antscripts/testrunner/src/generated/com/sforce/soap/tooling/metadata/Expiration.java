
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Expiration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Expiration"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ThirtyDays"/&gt;
 *     &lt;enumeration value="SixtyDays"/&gt;
 *     &lt;enumeration value="NinetyDays"/&gt;
 *     &lt;enumeration value="SixMonths"/&gt;
 *     &lt;enumeration value="OneYear"/&gt;
 *     &lt;enumeration value="Never"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Expiration")
@XmlEnum
public enum Expiration {

    @XmlEnumValue("ThirtyDays")
    THIRTY_DAYS("ThirtyDays"),
    @XmlEnumValue("SixtyDays")
    SIXTY_DAYS("SixtyDays"),
    @XmlEnumValue("NinetyDays")
    NINETY_DAYS("NinetyDays"),
    @XmlEnumValue("SixMonths")
    SIX_MONTHS("SixMonths"),
    @XmlEnumValue("OneYear")
    ONE_YEAR("OneYear"),
    @XmlEnumValue("Never")
    NEVER("Never");
    private final String value;

    Expiration(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Expiration fromValue(String v) {
        for (Expiration c: Expiration.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
