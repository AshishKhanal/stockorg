
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Regular"/&gt;
 *     &lt;enumeration value="Role"/&gt;
 *     &lt;enumeration value="RoleAndSubordinates"/&gt;
 *     &lt;enumeration value="RoleAndSubordinatesInternal"/&gt;
 *     &lt;enumeration value="Manager"/&gt;
 *     &lt;enumeration value="ManagerAndSubordinatesInternal"/&gt;
 *     &lt;enumeration value="Organization"/&gt;
 *     &lt;enumeration value="Queue"/&gt;
 *     &lt;enumeration value="Territory"/&gt;
 *     &lt;enumeration value="TerritoryAndSubordinates"/&gt;
 *     &lt;enumeration value="PRMOrganization"/&gt;
 *     &lt;enumeration value="AllCustomerPortal"/&gt;
 *     &lt;enumeration value="CollaborationGroup"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GroupType")
@XmlEnum
public enum GroupType {

    @XmlEnumValue("Regular")
    REGULAR("Regular"),
    @XmlEnumValue("Role")
    ROLE("Role"),
    @XmlEnumValue("RoleAndSubordinates")
    ROLE_AND_SUBORDINATES("RoleAndSubordinates"),
    @XmlEnumValue("RoleAndSubordinatesInternal")
    ROLE_AND_SUBORDINATES_INTERNAL("RoleAndSubordinatesInternal"),
    @XmlEnumValue("Manager")
    MANAGER("Manager"),
    @XmlEnumValue("ManagerAndSubordinatesInternal")
    MANAGER_AND_SUBORDINATES_INTERNAL("ManagerAndSubordinatesInternal"),
    @XmlEnumValue("Organization")
    ORGANIZATION("Organization"),
    @XmlEnumValue("Queue")
    QUEUE("Queue"),
    @XmlEnumValue("Territory")
    TERRITORY("Territory"),
    @XmlEnumValue("TerritoryAndSubordinates")
    TERRITORY_AND_SUBORDINATES("TerritoryAndSubordinates"),
    @XmlEnumValue("PRMOrganization")
    PRM_ORGANIZATION("PRMOrganization"),
    @XmlEnumValue("AllCustomerPortal")
    ALL_CUSTOMER_PORTAL("AllCustomerPortal"),
    @XmlEnumValue("CollaborationGroup")
    COLLABORATION_GROUP("CollaborationGroup");
    private final String value;

    GroupType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupType fromValue(String v) {
        for (GroupType c: GroupType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
