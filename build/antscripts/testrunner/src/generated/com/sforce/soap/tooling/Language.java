
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Language.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Language"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="en_US"/&gt;
 *     &lt;enumeration value="de"/&gt;
 *     &lt;enumeration value="es"/&gt;
 *     &lt;enumeration value="fr"/&gt;
 *     &lt;enumeration value="it"/&gt;
 *     &lt;enumeration value="ja"/&gt;
 *     &lt;enumeration value="sv"/&gt;
 *     &lt;enumeration value="ko"/&gt;
 *     &lt;enumeration value="zh_TW"/&gt;
 *     &lt;enumeration value="zh_CN"/&gt;
 *     &lt;enumeration value="pt_BR"/&gt;
 *     &lt;enumeration value="nl_NL"/&gt;
 *     &lt;enumeration value="da"/&gt;
 *     &lt;enumeration value="th"/&gt;
 *     &lt;enumeration value="fi"/&gt;
 *     &lt;enumeration value="ru"/&gt;
 *     &lt;enumeration value="es_MX"/&gt;
 *     &lt;enumeration value="no"/&gt;
 *     &lt;enumeration value="hu"/&gt;
 *     &lt;enumeration value="pl"/&gt;
 *     &lt;enumeration value="cs"/&gt;
 *     &lt;enumeration value="tr"/&gt;
 *     &lt;enumeration value="in"/&gt;
 *     &lt;enumeration value="ro"/&gt;
 *     &lt;enumeration value="vi"/&gt;
 *     &lt;enumeration value="uk"/&gt;
 *     &lt;enumeration value="iw"/&gt;
 *     &lt;enumeration value="el"/&gt;
 *     &lt;enumeration value="bg"/&gt;
 *     &lt;enumeration value="en_GB"/&gt;
 *     &lt;enumeration value="ar"/&gt;
 *     &lt;enumeration value="sk"/&gt;
 *     &lt;enumeration value="pt_PT"/&gt;
 *     &lt;enumeration value="hr"/&gt;
 *     &lt;enumeration value="sl"/&gt;
 *     &lt;enumeration value="fr_CA"/&gt;
 *     &lt;enumeration value="ka"/&gt;
 *     &lt;enumeration value="sr"/&gt;
 *     &lt;enumeration value="sh"/&gt;
 *     &lt;enumeration value="en_AU"/&gt;
 *     &lt;enumeration value="en_MY"/&gt;
 *     &lt;enumeration value="en_IN"/&gt;
 *     &lt;enumeration value="en_PH"/&gt;
 *     &lt;enumeration value="en_CA"/&gt;
 *     &lt;enumeration value="ro_MD"/&gt;
 *     &lt;enumeration value="bs"/&gt;
 *     &lt;enumeration value="mk"/&gt;
 *     &lt;enumeration value="lv"/&gt;
 *     &lt;enumeration value="lt"/&gt;
 *     &lt;enumeration value="et"/&gt;
 *     &lt;enumeration value="sq"/&gt;
 *     &lt;enumeration value="sh_ME"/&gt;
 *     &lt;enumeration value="mt"/&gt;
 *     &lt;enumeration value="ga"/&gt;
 *     &lt;enumeration value="eu"/&gt;
 *     &lt;enumeration value="cy"/&gt;
 *     &lt;enumeration value="is"/&gt;
 *     &lt;enumeration value="ms"/&gt;
 *     &lt;enumeration value="tl"/&gt;
 *     &lt;enumeration value="lb"/&gt;
 *     &lt;enumeration value="rm"/&gt;
 *     &lt;enumeration value="hy"/&gt;
 *     &lt;enumeration value="hi"/&gt;
 *     &lt;enumeration value="ur"/&gt;
 *     &lt;enumeration value="bn"/&gt;
 *     &lt;enumeration value="de_AT"/&gt;
 *     &lt;enumeration value="de_CH"/&gt;
 *     &lt;enumeration value="ta"/&gt;
 *     &lt;enumeration value="ar_DZ"/&gt;
 *     &lt;enumeration value="ar_BH"/&gt;
 *     &lt;enumeration value="ar_EG"/&gt;
 *     &lt;enumeration value="ar_IQ"/&gt;
 *     &lt;enumeration value="ar_JO"/&gt;
 *     &lt;enumeration value="ar_KW"/&gt;
 *     &lt;enumeration value="ar_LB"/&gt;
 *     &lt;enumeration value="ar_LY"/&gt;
 *     &lt;enumeration value="ar_MA"/&gt;
 *     &lt;enumeration value="ar_OM"/&gt;
 *     &lt;enumeration value="ar_QA"/&gt;
 *     &lt;enumeration value="ar_SA"/&gt;
 *     &lt;enumeration value="ar_SD"/&gt;
 *     &lt;enumeration value="ar_SY"/&gt;
 *     &lt;enumeration value="ar_TN"/&gt;
 *     &lt;enumeration value="ar_AE"/&gt;
 *     &lt;enumeration value="ar_YE"/&gt;
 *     &lt;enumeration value="zh_SG"/&gt;
 *     &lt;enumeration value="zh_HK"/&gt;
 *     &lt;enumeration value="en_HK"/&gt;
 *     &lt;enumeration value="en_IE"/&gt;
 *     &lt;enumeration value="en_SG"/&gt;
 *     &lt;enumeration value="en_ZA"/&gt;
 *     &lt;enumeration value="fr_BE"/&gt;
 *     &lt;enumeration value="fr_LU"/&gt;
 *     &lt;enumeration value="fr_CH"/&gt;
 *     &lt;enumeration value="de_LU"/&gt;
 *     &lt;enumeration value="it_CH"/&gt;
 *     &lt;enumeration value="es_AR"/&gt;
 *     &lt;enumeration value="es_BO"/&gt;
 *     &lt;enumeration value="es_CL"/&gt;
 *     &lt;enumeration value="es_CO"/&gt;
 *     &lt;enumeration value="es_CR"/&gt;
 *     &lt;enumeration value="es_DO"/&gt;
 *     &lt;enumeration value="es_EC"/&gt;
 *     &lt;enumeration value="es_SV"/&gt;
 *     &lt;enumeration value="es_GT"/&gt;
 *     &lt;enumeration value="es_HN"/&gt;
 *     &lt;enumeration value="es_NI"/&gt;
 *     &lt;enumeration value="es_PA"/&gt;
 *     &lt;enumeration value="es_PY"/&gt;
 *     &lt;enumeration value="es_PE"/&gt;
 *     &lt;enumeration value="es_PR"/&gt;
 *     &lt;enumeration value="es_US"/&gt;
 *     &lt;enumeration value="es_UY"/&gt;
 *     &lt;enumeration value="es_VE"/&gt;
 *     &lt;enumeration value="eo"/&gt;
 *     &lt;enumeration value="iw_EO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Language")
@XmlEnum
public enum Language {

    @XmlEnumValue("en_US")
    EN_US("en_US"),
    @XmlEnumValue("de")
    DE("de"),
    @XmlEnumValue("es")
    ES("es"),
    @XmlEnumValue("fr")
    FR("fr"),
    @XmlEnumValue("it")
    IT("it"),
    @XmlEnumValue("ja")
    JA("ja"),
    @XmlEnumValue("sv")
    SV("sv"),
    @XmlEnumValue("ko")
    KO("ko"),
    @XmlEnumValue("zh_TW")
    ZH_TW("zh_TW"),
    @XmlEnumValue("zh_CN")
    ZH_CN("zh_CN"),
    @XmlEnumValue("pt_BR")
    PT_BR("pt_BR"),
    @XmlEnumValue("nl_NL")
    NL_NL("nl_NL"),
    @XmlEnumValue("da")
    DA("da"),
    @XmlEnumValue("th")
    TH("th"),
    @XmlEnumValue("fi")
    FI("fi"),
    @XmlEnumValue("ru")
    RU("ru"),
    @XmlEnumValue("es_MX")
    ES_MX("es_MX"),
    @XmlEnumValue("no")
    NO("no"),
    @XmlEnumValue("hu")
    HU("hu"),
    @XmlEnumValue("pl")
    PL("pl"),
    @XmlEnumValue("cs")
    CS("cs"),
    @XmlEnumValue("tr")
    TR("tr"),
    @XmlEnumValue("in")
    IN("in"),
    @XmlEnumValue("ro")
    RO("ro"),
    @XmlEnumValue("vi")
    VI("vi"),
    @XmlEnumValue("uk")
    UK("uk"),
    @XmlEnumValue("iw")
    IW("iw"),
    @XmlEnumValue("el")
    EL("el"),
    @XmlEnumValue("bg")
    BG("bg"),
    @XmlEnumValue("en_GB")
    EN_GB("en_GB"),
    @XmlEnumValue("ar")
    AR("ar"),
    @XmlEnumValue("sk")
    SK("sk"),
    @XmlEnumValue("pt_PT")
    PT_PT("pt_PT"),
    @XmlEnumValue("hr")
    HR("hr"),
    @XmlEnumValue("sl")
    SL("sl"),
    @XmlEnumValue("fr_CA")
    FR_CA("fr_CA"),
    @XmlEnumValue("ka")
    KA("ka"),
    @XmlEnumValue("sr")
    SR("sr"),
    @XmlEnumValue("sh")
    SH("sh"),
    @XmlEnumValue("en_AU")
    EN_AU("en_AU"),
    @XmlEnumValue("en_MY")
    EN_MY("en_MY"),
    @XmlEnumValue("en_IN")
    EN_IN("en_IN"),
    @XmlEnumValue("en_PH")
    EN_PH("en_PH"),
    @XmlEnumValue("en_CA")
    EN_CA("en_CA"),
    @XmlEnumValue("ro_MD")
    RO_MD("ro_MD"),
    @XmlEnumValue("bs")
    BS("bs"),
    @XmlEnumValue("mk")
    MK("mk"),
    @XmlEnumValue("lv")
    LV("lv"),
    @XmlEnumValue("lt")
    LT("lt"),
    @XmlEnumValue("et")
    ET("et"),
    @XmlEnumValue("sq")
    SQ("sq"),
    @XmlEnumValue("sh_ME")
    SH_ME("sh_ME"),
    @XmlEnumValue("mt")
    MT("mt"),
    @XmlEnumValue("ga")
    GA("ga"),
    @XmlEnumValue("eu")
    EU("eu"),
    @XmlEnumValue("cy")
    CY("cy"),
    @XmlEnumValue("is")
    IS("is"),
    @XmlEnumValue("ms")
    MS("ms"),
    @XmlEnumValue("tl")
    TL("tl"),
    @XmlEnumValue("lb")
    LB("lb"),
    @XmlEnumValue("rm")
    RM("rm"),
    @XmlEnumValue("hy")
    HY("hy"),
    @XmlEnumValue("hi")
    HI("hi"),
    @XmlEnumValue("ur")
    UR("ur"),
    @XmlEnumValue("bn")
    BN("bn"),
    @XmlEnumValue("de_AT")
    DE_AT("de_AT"),
    @XmlEnumValue("de_CH")
    DE_CH("de_CH"),
    @XmlEnumValue("ta")
    TA("ta"),
    @XmlEnumValue("ar_DZ")
    AR_DZ("ar_DZ"),
    @XmlEnumValue("ar_BH")
    AR_BH("ar_BH"),
    @XmlEnumValue("ar_EG")
    AR_EG("ar_EG"),
    @XmlEnumValue("ar_IQ")
    AR_IQ("ar_IQ"),
    @XmlEnumValue("ar_JO")
    AR_JO("ar_JO"),
    @XmlEnumValue("ar_KW")
    AR_KW("ar_KW"),
    @XmlEnumValue("ar_LB")
    AR_LB("ar_LB"),
    @XmlEnumValue("ar_LY")
    AR_LY("ar_LY"),
    @XmlEnumValue("ar_MA")
    AR_MA("ar_MA"),
    @XmlEnumValue("ar_OM")
    AR_OM("ar_OM"),
    @XmlEnumValue("ar_QA")
    AR_QA("ar_QA"),
    @XmlEnumValue("ar_SA")
    AR_SA("ar_SA"),
    @XmlEnumValue("ar_SD")
    AR_SD("ar_SD"),
    @XmlEnumValue("ar_SY")
    AR_SY("ar_SY"),
    @XmlEnumValue("ar_TN")
    AR_TN("ar_TN"),
    @XmlEnumValue("ar_AE")
    AR_AE("ar_AE"),
    @XmlEnumValue("ar_YE")
    AR_YE("ar_YE"),
    @XmlEnumValue("zh_SG")
    ZH_SG("zh_SG"),
    @XmlEnumValue("zh_HK")
    ZH_HK("zh_HK"),
    @XmlEnumValue("en_HK")
    EN_HK("en_HK"),
    @XmlEnumValue("en_IE")
    EN_IE("en_IE"),
    @XmlEnumValue("en_SG")
    EN_SG("en_SG"),
    @XmlEnumValue("en_ZA")
    EN_ZA("en_ZA"),
    @XmlEnumValue("fr_BE")
    FR_BE("fr_BE"),
    @XmlEnumValue("fr_LU")
    FR_LU("fr_LU"),
    @XmlEnumValue("fr_CH")
    FR_CH("fr_CH"),
    @XmlEnumValue("de_LU")
    DE_LU("de_LU"),
    @XmlEnumValue("it_CH")
    IT_CH("it_CH"),
    @XmlEnumValue("es_AR")
    ES_AR("es_AR"),
    @XmlEnumValue("es_BO")
    ES_BO("es_BO"),
    @XmlEnumValue("es_CL")
    ES_CL("es_CL"),
    @XmlEnumValue("es_CO")
    ES_CO("es_CO"),
    @XmlEnumValue("es_CR")
    ES_CR("es_CR"),
    @XmlEnumValue("es_DO")
    ES_DO("es_DO"),
    @XmlEnumValue("es_EC")
    ES_EC("es_EC"),
    @XmlEnumValue("es_SV")
    ES_SV("es_SV"),
    @XmlEnumValue("es_GT")
    ES_GT("es_GT"),
    @XmlEnumValue("es_HN")
    ES_HN("es_HN"),
    @XmlEnumValue("es_NI")
    ES_NI("es_NI"),
    @XmlEnumValue("es_PA")
    ES_PA("es_PA"),
    @XmlEnumValue("es_PY")
    ES_PY("es_PY"),
    @XmlEnumValue("es_PE")
    ES_PE("es_PE"),
    @XmlEnumValue("es_PR")
    ES_PR("es_PR"),
    @XmlEnumValue("es_US")
    ES_US("es_US"),
    @XmlEnumValue("es_UY")
    ES_UY("es_UY"),
    @XmlEnumValue("es_VE")
    ES_VE("es_VE"),
    @XmlEnumValue("eo")
    EO("eo"),
    @XmlEnumValue("iw_EO")
    IW_EO("iw_EO");
    private final String value;

    Language(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Language fromValue(String v) {
        for (Language c: Language.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
