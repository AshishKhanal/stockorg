
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowConnector complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlowConnector"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}FlowBaseElement"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="targetReference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowConnector", propOrder = {
    "targetReference"
})
public class FlowConnector
    extends FlowBaseElement
{

    @XmlElement(required = true)
    protected String targetReference;

    /**
     * Gets the value of the targetReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetReference() {
        return targetReference;
    }

    /**
     * Sets the value of the targetReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetReference(String value) {
        this.targetReference = value;
    }

}
