
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeedItemDisplayFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FeedItemDisplayFormat"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Default"/&gt;
 *     &lt;enumeration value="HideBlankLines"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FeedItemDisplayFormat")
@XmlEnum
public enum FeedItemDisplayFormat {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("HideBlankLines")
    HIDE_BLANK_LINES("HideBlankLines");
    private final String value;

    FeedItemDisplayFormat(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FeedItemDisplayFormat fromValue(String v) {
        for (FeedItemDisplayFormat c: FeedItemDisplayFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
