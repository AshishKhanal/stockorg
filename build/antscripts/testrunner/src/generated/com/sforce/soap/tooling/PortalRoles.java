
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PortalRoles.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PortalRoles"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Executive"/&gt;
 *     &lt;enumeration value="Manager"/&gt;
 *     &lt;enumeration value="Worker"/&gt;
 *     &lt;enumeration value="PersonAccount"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PortalRoles")
@XmlEnum
public enum PortalRoles {

    @XmlEnumValue("Executive")
    EXECUTIVE("Executive"),
    @XmlEnumValue("Manager")
    MANAGER("Manager"),
    @XmlEnumValue("Worker")
    WORKER("Worker"),
    @XmlEnumValue("PersonAccount")
    PERSON_ACCOUNT("PersonAccount");
    private final String value;

    PortalRoles(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PortalRoles fromValue(String v) {
        for (PortalRoles c: PortalRoles.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
