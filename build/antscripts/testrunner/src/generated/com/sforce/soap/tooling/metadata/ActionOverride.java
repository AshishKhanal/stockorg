
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.ActionOverrideType;
import com.sforce.soap.tooling.FormFactor;


/**
 * <p>Java class for ActionOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActionOverride"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="formFactor" type="{urn:tooling.soap.sforce.com}FormFactor" minOccurs="0"/&gt;
 *         &lt;element name="skipRecordTypeSelect" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}ActionOverrideType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActionOverride", propOrder = {
    "actionName",
    "comment",
    "content",
    "formFactor",
    "skipRecordTypeSelect",
    "type"
})
@XmlSeeAlso({
    AppActionOverride.class
})
public class ActionOverride {

    protected String actionName;
    protected String comment;
    protected String content;
    @XmlSchemaType(name = "string")
    protected FormFactor formFactor;
    protected Boolean skipRecordTypeSelect;
    @XmlSchemaType(name = "string")
    protected ActionOverrideType type;

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the formFactor property.
     * 
     * @return
     *     possible object is
     *     {@link FormFactor }
     *     
     */
    public FormFactor getFormFactor() {
        return formFactor;
    }

    /**
     * Sets the value of the formFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormFactor }
     *     
     */
    public void setFormFactor(FormFactor value) {
        this.formFactor = value;
    }

    /**
     * Gets the value of the skipRecordTypeSelect property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSkipRecordTypeSelect() {
        return skipRecordTypeSelect;
    }

    /**
     * Sets the value of the skipRecordTypeSelect property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSkipRecordTypeSelect(Boolean value) {
        this.skipRecordTypeSelect = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ActionOverrideType }
     *     
     */
    public ActionOverrideType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionOverrideType }
     *     
     */
    public void setType(ActionOverrideType value) {
        this.type = value;
    }

}
