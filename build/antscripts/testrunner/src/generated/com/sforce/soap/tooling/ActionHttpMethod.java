
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionHttpMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionHttpMethod"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="GET"/&gt;
 *     &lt;enumeration value="PUT"/&gt;
 *     &lt;enumeration value="POST"/&gt;
 *     &lt;enumeration value="DELETE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionHttpMethod")
@XmlEnum
public enum ActionHttpMethod {

    GET,
    PUT,
    POST,
    DELETE;

    public String value() {
        return name();
    }

    public static ActionHttpMethod fromValue(String v) {
        return valueOf(v);
    }

}
