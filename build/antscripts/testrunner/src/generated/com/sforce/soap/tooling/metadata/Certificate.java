
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Certificate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Certificate"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="caSigned" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="encryptedWithPlatformEncryption" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="keySize" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="privateKeyExportable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Certificate", propOrder = {
    "caSigned",
    "encryptedWithPlatformEncryption",
    "expirationDate",
    "keySize",
    "masterLabel",
    "privateKeyExportable"
})
public class Certificate
    extends Metadata
{

    protected boolean caSigned;
    @XmlElementRef(name = "encryptedWithPlatformEncryption", namespace = "urn:metadata.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> encryptedWithPlatformEncryption;
    @XmlElementRef(name = "expirationDate", namespace = "urn:metadata.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> expirationDate;
    protected Integer keySize;
    @XmlElement(required = true)
    protected String masterLabel;
    @XmlElementRef(name = "privateKeyExportable", namespace = "urn:metadata.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> privateKeyExportable;

    /**
     * Gets the value of the caSigned property.
     * 
     */
    public boolean isCaSigned() {
        return caSigned;
    }

    /**
     * Sets the value of the caSigned property.
     * 
     */
    public void setCaSigned(boolean value) {
        this.caSigned = value;
    }

    /**
     * Gets the value of the encryptedWithPlatformEncryption property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEncryptedWithPlatformEncryption() {
        return encryptedWithPlatformEncryption;
    }

    /**
     * Sets the value of the encryptedWithPlatformEncryption property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEncryptedWithPlatformEncryption(JAXBElement<Boolean> value) {
        this.encryptedWithPlatformEncryption = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setExpirationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the keySize property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getKeySize() {
        return keySize;
    }

    /**
     * Sets the value of the keySize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setKeySize(Integer value) {
        this.keySize = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the privateKeyExportable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getPrivateKeyExportable() {
        return privateKeyExportable;
    }

    /**
     * Sets the value of the privateKeyExportable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setPrivateKeyExportable(JAXBElement<Boolean> value) {
        this.privateKeyExportable = value;
    }

}
