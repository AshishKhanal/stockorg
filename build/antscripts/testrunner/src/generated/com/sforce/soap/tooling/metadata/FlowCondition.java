
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FlowComparisonOperator;


/**
 * <p>Java class for FlowCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlowCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}FlowBaseElement"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="leftValueReference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="operator" type="{urn:tooling.soap.sforce.com}FlowComparisonOperator"/&gt;
 *         &lt;element name="rightValue" type="{urn:metadata.tooling.soap.sforce.com}FlowElementReferenceOrValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowCondition", propOrder = {
    "leftValueReference",
    "operator",
    "rightValue"
})
public class FlowCondition
    extends FlowBaseElement
{

    @XmlElement(required = true)
    protected String leftValueReference;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FlowComparisonOperator operator;
    protected FlowElementReferenceOrValue rightValue;

    /**
     * Gets the value of the leftValueReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeftValueReference() {
        return leftValueReference;
    }

    /**
     * Sets the value of the leftValueReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeftValueReference(String value) {
        this.leftValueReference = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link FlowComparisonOperator }
     *     
     */
    public FlowComparisonOperator getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlowComparisonOperator }
     *     
     */
    public void setOperator(FlowComparisonOperator value) {
        this.operator = value;
    }

    /**
     * Gets the value of the rightValue property.
     * 
     * @return
     *     possible object is
     *     {@link FlowElementReferenceOrValue }
     *     
     */
    public FlowElementReferenceOrValue getRightValue() {
        return rightValue;
    }

    /**
     * Sets the value of the rightValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlowElementReferenceOrValue }
     *     
     */
    public void setRightValue(FlowElementReferenceOrValue value) {
        this.rightValue = value;
    }

}
