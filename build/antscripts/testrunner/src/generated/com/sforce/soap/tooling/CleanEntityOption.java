
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CleanEntityOption.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CleanEntityOption"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Flag"/&gt;
 *     &lt;enumeration value="AutoFill"/&gt;
 *     &lt;enumeration value="Custom"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CleanEntityOption")
@XmlEnum
public enum CleanEntityOption {

    @XmlEnumValue("Flag")
    FLAG("Flag"),
    @XmlEnumValue("AutoFill")
    AUTO_FILL("AutoFill"),
    @XmlEnumValue("Custom")
    CUSTOM("Custom");
    private final String value;

    CleanEntityOption(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CleanEntityOption fromValue(String v) {
        for (CleanEntityOption c: CleanEntityOption.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
