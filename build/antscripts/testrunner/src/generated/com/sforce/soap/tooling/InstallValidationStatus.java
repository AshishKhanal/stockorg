
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstallValidationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InstallValidationStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NoErrorsDetected"/&gt;
 *     &lt;enumeration value="BetaInstallIntoProductionOrg"/&gt;
 *     &lt;enumeration value="CannotInstallEarlierVersion"/&gt;
 *     &lt;enumeration value="CannotUpgradeBeta"/&gt;
 *     &lt;enumeration value="CannotUpgradeUnmanaged"/&gt;
 *     &lt;enumeration value="DeprecatedInstallPackage"/&gt;
 *     &lt;enumeration value="ExtensionsOnLocalPackages"/&gt;
 *     &lt;enumeration value="PackageNotInstalled"/&gt;
 *     &lt;enumeration value="PackageHasInDevExtensions"/&gt;
 *     &lt;enumeration value="InstallIntoDevOrg"/&gt;
 *     &lt;enumeration value="NoAccess"/&gt;
 *     &lt;enumeration value="PackagingDisabled"/&gt;
 *     &lt;enumeration value="PackagingNoAccess"/&gt;
 *     &lt;enumeration value="PackageUnavailable"/&gt;
 *     &lt;enumeration value="UninstallInProgress"/&gt;
 *     &lt;enumeration value="UnknownError"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InstallValidationStatus")
@XmlEnum
public enum InstallValidationStatus {

    @XmlEnumValue("NoErrorsDetected")
    NO_ERRORS_DETECTED("NoErrorsDetected"),
    @XmlEnumValue("BetaInstallIntoProductionOrg")
    BETA_INSTALL_INTO_PRODUCTION_ORG("BetaInstallIntoProductionOrg"),
    @XmlEnumValue("CannotInstallEarlierVersion")
    CANNOT_INSTALL_EARLIER_VERSION("CannotInstallEarlierVersion"),
    @XmlEnumValue("CannotUpgradeBeta")
    CANNOT_UPGRADE_BETA("CannotUpgradeBeta"),
    @XmlEnumValue("CannotUpgradeUnmanaged")
    CANNOT_UPGRADE_UNMANAGED("CannotUpgradeUnmanaged"),
    @XmlEnumValue("DeprecatedInstallPackage")
    DEPRECATED_INSTALL_PACKAGE("DeprecatedInstallPackage"),
    @XmlEnumValue("ExtensionsOnLocalPackages")
    EXTENSIONS_ON_LOCAL_PACKAGES("ExtensionsOnLocalPackages"),
    @XmlEnumValue("PackageNotInstalled")
    PACKAGE_NOT_INSTALLED("PackageNotInstalled"),
    @XmlEnumValue("PackageHasInDevExtensions")
    PACKAGE_HAS_IN_DEV_EXTENSIONS("PackageHasInDevExtensions"),
    @XmlEnumValue("InstallIntoDevOrg")
    INSTALL_INTO_DEV_ORG("InstallIntoDevOrg"),
    @XmlEnumValue("NoAccess")
    NO_ACCESS("NoAccess"),
    @XmlEnumValue("PackagingDisabled")
    PACKAGING_DISABLED("PackagingDisabled"),
    @XmlEnumValue("PackagingNoAccess")
    PACKAGING_NO_ACCESS("PackagingNoAccess"),
    @XmlEnumValue("PackageUnavailable")
    PACKAGE_UNAVAILABLE("PackageUnavailable"),
    @XmlEnumValue("UninstallInProgress")
    UNINSTALL_IN_PROGRESS("UninstallInProgress"),
    @XmlEnumValue("UnknownError")
    UNKNOWN_ERROR("UnknownError");
    private final String value;

    InstallValidationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InstallValidationStatus fromValue(String v) {
        for (InstallValidationStatus c: InstallValidationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
