
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ObjectSearchSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObjectSearchSetting"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enhancedLookupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="lookupAutoCompleteEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resultsPerPageCount" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectSearchSetting", propOrder = {
    "enhancedLookupEnabled",
    "lookupAutoCompleteEnabled",
    "name",
    "resultsPerPageCount"
})
public class ObjectSearchSetting {

    protected boolean enhancedLookupEnabled;
    protected boolean lookupAutoCompleteEnabled;
    @XmlElement(required = true)
    protected String name;
    protected int resultsPerPageCount;

    /**
     * Gets the value of the enhancedLookupEnabled property.
     * 
     */
    public boolean isEnhancedLookupEnabled() {
        return enhancedLookupEnabled;
    }

    /**
     * Sets the value of the enhancedLookupEnabled property.
     * 
     */
    public void setEnhancedLookupEnabled(boolean value) {
        this.enhancedLookupEnabled = value;
    }

    /**
     * Gets the value of the lookupAutoCompleteEnabled property.
     * 
     */
    public boolean isLookupAutoCompleteEnabled() {
        return lookupAutoCompleteEnabled;
    }

    /**
     * Sets the value of the lookupAutoCompleteEnabled property.
     * 
     */
    public void setLookupAutoCompleteEnabled(boolean value) {
        this.lookupAutoCompleteEnabled = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the resultsPerPageCount property.
     * 
     */
    public int getResultsPerPageCount() {
        return resultsPerPageCount;
    }

    /**
     * Sets the value of the resultsPerPageCount property.
     * 
     */
    public void setResultsPerPageCount(int value) {
        this.resultsPerPageCount = value;
    }

}
