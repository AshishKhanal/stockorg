
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlatformCacheType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlatformCacheType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Session"/&gt;
 *     &lt;enumeration value="Organization"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PlatformCacheType")
@XmlEnum
public enum PlatformCacheType {

    @XmlEnumValue("Session")
    SESSION("Session"),
    @XmlEnumValue("Organization")
    ORGANIZATION("Organization");
    private final String value;

    PlatformCacheType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlatformCacheType fromValue(String v) {
        for (PlatformCacheType c: PlatformCacheType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
