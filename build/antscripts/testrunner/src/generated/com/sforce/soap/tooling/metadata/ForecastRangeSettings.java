
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.PeriodTypes;


/**
 * <p>Java class for ForecastRangeSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastRangeSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="beginning" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="displaying" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="periodType" type="{urn:tooling.soap.sforce.com}PeriodTypes"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastRangeSettings", propOrder = {
    "beginning",
    "displaying",
    "periodType"
})
public class ForecastRangeSettings {

    protected int beginning;
    protected int displaying;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PeriodTypes periodType;

    /**
     * Gets the value of the beginning property.
     * 
     */
    public int getBeginning() {
        return beginning;
    }

    /**
     * Sets the value of the beginning property.
     * 
     */
    public void setBeginning(int value) {
        this.beginning = value;
    }

    /**
     * Gets the value of the displaying property.
     * 
     */
    public int getDisplaying() {
        return displaying;
    }

    /**
     * Sets the value of the displaying property.
     * 
     */
    public void setDisplaying(int value) {
        this.displaying = value;
    }

    /**
     * Gets the value of the periodType property.
     * 
     * @return
     *     possible object is
     *     {@link PeriodTypes }
     *     
     */
    public PeriodTypes getPeriodType() {
        return periodType;
    }

    /**
     * Sets the value of the periodType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PeriodTypes }
     *     
     */
    public void setPeriodType(PeriodTypes value) {
        this.periodType = value;
    }

}
