
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AsyncApexJobStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AsyncApexJobStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Queued"/&gt;
 *     &lt;enumeration value="Processing"/&gt;
 *     &lt;enumeration value="Aborted"/&gt;
 *     &lt;enumeration value="Completed"/&gt;
 *     &lt;enumeration value="Failed"/&gt;
 *     &lt;enumeration value="Preparing"/&gt;
 *     &lt;enumeration value="Holding"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AsyncApexJobStatus")
@XmlEnum
public enum AsyncApexJobStatus {

    @XmlEnumValue("Queued")
    QUEUED("Queued"),
    @XmlEnumValue("Processing")
    PROCESSING("Processing"),
    @XmlEnumValue("Aborted")
    ABORTED("Aborted"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Preparing")
    PREPARING("Preparing"),
    @XmlEnumValue("Holding")
    HOLDING("Holding");
    private final String value;

    AsyncApexJobStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AsyncApexJobStatus fromValue(String v) {
        for (AsyncApexJobStatus c: AsyncApexJobStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
