
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SControlContentSource.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SControlContentSource"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HTML"/&gt;
 *     &lt;enumeration value="URL"/&gt;
 *     &lt;enumeration value="Snippet"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SControlContentSource")
@XmlEnum
public enum SControlContentSource {

    HTML("HTML"),
    URL("URL"),
    @XmlEnumValue("Snippet")
    SNIPPET("Snippet");
    private final String value;

    SControlContentSource(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SControlContentSource fromValue(String v) {
        for (SControlContentSource c: SControlContentSource.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
