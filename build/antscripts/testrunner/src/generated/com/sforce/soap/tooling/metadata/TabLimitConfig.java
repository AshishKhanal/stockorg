
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TabLimitConfig complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TabLimitConfig"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maxNumberOfPrimaryTabs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maxNumberOfSubTabs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TabLimitConfig", propOrder = {
    "maxNumberOfPrimaryTabs",
    "maxNumberOfSubTabs"
})
public class TabLimitConfig {

    protected String maxNumberOfPrimaryTabs;
    protected String maxNumberOfSubTabs;

    /**
     * Gets the value of the maxNumberOfPrimaryTabs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumberOfPrimaryTabs() {
        return maxNumberOfPrimaryTabs;
    }

    /**
     * Sets the value of the maxNumberOfPrimaryTabs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumberOfPrimaryTabs(String value) {
        this.maxNumberOfPrimaryTabs = value;
    }

    /**
     * Gets the value of the maxNumberOfSubTabs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumberOfSubTabs() {
        return maxNumberOfSubTabs;
    }

    /**
     * Sets the value of the maxNumberOfSubTabs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumberOfSubTabs(String value) {
        this.maxNumberOfSubTabs = value;
    }

}
