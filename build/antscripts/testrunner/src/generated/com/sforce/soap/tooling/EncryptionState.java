
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EncryptionState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EncryptionState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ENCRYPTION_AT_REST_DISABLED"/&gt;
 *     &lt;enumeration value="ENCRYPTION_AT_REST_ENABLED"/&gt;
 *     &lt;enumeration value="ENCRYPTION_AT_REST_ENABLED_IMPLICITLY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EncryptionState")
@XmlEnum
public enum EncryptionState {

    ENCRYPTION_AT_REST_DISABLED,
    ENCRYPTION_AT_REST_ENABLED,
    ENCRYPTION_AT_REST_ENABLED_IMPLICITLY;

    public String value() {
        return name();
    }

    public static EncryptionState fromValue(String v) {
        return valueOf(v);
    }

}
