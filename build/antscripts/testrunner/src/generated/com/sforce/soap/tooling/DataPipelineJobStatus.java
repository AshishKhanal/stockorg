
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DataPipelineJobStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DataPipelineJobStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Scheduled"/&gt;
 *     &lt;enumeration value="Running"/&gt;
 *     &lt;enumeration value="Success"/&gt;
 *     &lt;enumeration value="Failed"/&gt;
 *     &lt;enumeration value="Killed"/&gt;
 *     &lt;enumeration value="Load"/&gt;
 *     &lt;enumeration value="Process"/&gt;
 *     &lt;enumeration value="Store"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DataPipelineJobStatus")
@XmlEnum
public enum DataPipelineJobStatus {

    @XmlEnumValue("Scheduled")
    SCHEDULED("Scheduled"),
    @XmlEnumValue("Running")
    RUNNING("Running"),
    @XmlEnumValue("Success")
    SUCCESS("Success"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Killed")
    KILLED("Killed"),
    @XmlEnumValue("Load")
    LOAD("Load"),
    @XmlEnumValue("Process")
    PROCESS("Process"),
    @XmlEnumValue("Store")
    STORE("Store");
    private final String value;

    DataPipelineJobStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DataPipelineJobStatus fromValue(String v) {
        for (DataPipelineJobStatus c: DataPipelineJobStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
