
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpportunitySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpportunitySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="autoActivateNewReminders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableFindSimilarOpportunities" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableOpportunityTeam" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableUpdateReminders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="findSimilarOppFilter" type="{urn:metadata.tooling.soap.sforce.com}FindSimilarOppFilter" minOccurs="0"/&gt;
 *         &lt;element name="promptToAddProducts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpportunitySettings", propOrder = {
    "autoActivateNewReminders",
    "enableFindSimilarOpportunities",
    "enableOpportunityTeam",
    "enableUpdateReminders",
    "findSimilarOppFilter",
    "promptToAddProducts"
})
public class OpportunitySettings
    extends MetadataForSettings
{

    protected Boolean autoActivateNewReminders;
    protected Boolean enableFindSimilarOpportunities;
    protected Boolean enableOpportunityTeam;
    protected Boolean enableUpdateReminders;
    protected FindSimilarOppFilter findSimilarOppFilter;
    protected Boolean promptToAddProducts;

    /**
     * Gets the value of the autoActivateNewReminders property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoActivateNewReminders() {
        return autoActivateNewReminders;
    }

    /**
     * Sets the value of the autoActivateNewReminders property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoActivateNewReminders(Boolean value) {
        this.autoActivateNewReminders = value;
    }

    /**
     * Gets the value of the enableFindSimilarOpportunities property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableFindSimilarOpportunities() {
        return enableFindSimilarOpportunities;
    }

    /**
     * Sets the value of the enableFindSimilarOpportunities property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableFindSimilarOpportunities(Boolean value) {
        this.enableFindSimilarOpportunities = value;
    }

    /**
     * Gets the value of the enableOpportunityTeam property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOpportunityTeam() {
        return enableOpportunityTeam;
    }

    /**
     * Sets the value of the enableOpportunityTeam property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOpportunityTeam(Boolean value) {
        this.enableOpportunityTeam = value;
    }

    /**
     * Gets the value of the enableUpdateReminders property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableUpdateReminders() {
        return enableUpdateReminders;
    }

    /**
     * Sets the value of the enableUpdateReminders property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableUpdateReminders(Boolean value) {
        this.enableUpdateReminders = value;
    }

    /**
     * Gets the value of the findSimilarOppFilter property.
     * 
     * @return
     *     possible object is
     *     {@link FindSimilarOppFilter }
     *     
     */
    public FindSimilarOppFilter getFindSimilarOppFilter() {
        return findSimilarOppFilter;
    }

    /**
     * Sets the value of the findSimilarOppFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link FindSimilarOppFilter }
     *     
     */
    public void setFindSimilarOppFilter(FindSimilarOppFilter value) {
        this.findSimilarOppFilter = value;
    }

    /**
     * Gets the value of the promptToAddProducts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPromptToAddProducts() {
        return promptToAddProducts;
    }

    /**
     * Sets the value of the promptToAddProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPromptToAddProducts(Boolean value) {
        this.promptToAddProducts = value;
    }

}
