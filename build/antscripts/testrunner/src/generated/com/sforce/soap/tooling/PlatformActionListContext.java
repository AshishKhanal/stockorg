
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlatformActionListContext.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlatformActionListContext"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ListView"/&gt;
 *     &lt;enumeration value="RelatedList"/&gt;
 *     &lt;enumeration value="ListViewRecord"/&gt;
 *     &lt;enumeration value="RelatedListRecord"/&gt;
 *     &lt;enumeration value="Record"/&gt;
 *     &lt;enumeration value="FeedElement"/&gt;
 *     &lt;enumeration value="Chatter"/&gt;
 *     &lt;enumeration value="Global"/&gt;
 *     &lt;enumeration value="Flexipage"/&gt;
 *     &lt;enumeration value="MruList"/&gt;
 *     &lt;enumeration value="MruRow"/&gt;
 *     &lt;enumeration value="RecordEdit"/&gt;
 *     &lt;enumeration value="Photo"/&gt;
 *     &lt;enumeration value="BannerPhoto"/&gt;
 *     &lt;enumeration value="ObjectHomeChart"/&gt;
 *     &lt;enumeration value="ListViewDefinition"/&gt;
 *     &lt;enumeration value="Dockable"/&gt;
 *     &lt;enumeration value="Lookup"/&gt;
 *     &lt;enumeration value="Assistant"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PlatformActionListContext")
@XmlEnum
public enum PlatformActionListContext {

    @XmlEnumValue("ListView")
    LIST_VIEW("ListView"),
    @XmlEnumValue("RelatedList")
    RELATED_LIST("RelatedList"),
    @XmlEnumValue("ListViewRecord")
    LIST_VIEW_RECORD("ListViewRecord"),
    @XmlEnumValue("RelatedListRecord")
    RELATED_LIST_RECORD("RelatedListRecord"),
    @XmlEnumValue("Record")
    RECORD("Record"),
    @XmlEnumValue("FeedElement")
    FEED_ELEMENT("FeedElement"),
    @XmlEnumValue("Chatter")
    CHATTER("Chatter"),
    @XmlEnumValue("Global")
    GLOBAL("Global"),
    @XmlEnumValue("Flexipage")
    FLEXIPAGE("Flexipage"),
    @XmlEnumValue("MruList")
    MRU_LIST("MruList"),
    @XmlEnumValue("MruRow")
    MRU_ROW("MruRow"),
    @XmlEnumValue("RecordEdit")
    RECORD_EDIT("RecordEdit"),
    @XmlEnumValue("Photo")
    PHOTO("Photo"),
    @XmlEnumValue("BannerPhoto")
    BANNER_PHOTO("BannerPhoto"),
    @XmlEnumValue("ObjectHomeChart")
    OBJECT_HOME_CHART("ObjectHomeChart"),
    @XmlEnumValue("ListViewDefinition")
    LIST_VIEW_DEFINITION("ListViewDefinition"),
    @XmlEnumValue("Dockable")
    DOCKABLE("Dockable"),
    @XmlEnumValue("Lookup")
    LOOKUP("Lookup"),
    @XmlEnumValue("Assistant")
    ASSISTANT("Assistant");
    private final String value;

    PlatformActionListContext(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlatformActionListContext fromValue(String v) {
        for (PlatformActionListContext c: PlatformActionListContext.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
