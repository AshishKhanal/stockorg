
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReputationLevels complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReputationLevels"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="chatterAnswersReputationLevels" type="{urn:metadata.tooling.soap.sforce.com}ChatterAnswersReputationLevel" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ideaReputationLevels" type="{urn:metadata.tooling.soap.sforce.com}IdeaReputationLevel" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReputationLevels", propOrder = {
    "chatterAnswersReputationLevels",
    "ideaReputationLevels"
})
public class ReputationLevels {

    protected List<ChatterAnswersReputationLevel> chatterAnswersReputationLevels;
    protected List<IdeaReputationLevel> ideaReputationLevels;

    /**
     * Gets the value of the chatterAnswersReputationLevels property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chatterAnswersReputationLevels property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChatterAnswersReputationLevels().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChatterAnswersReputationLevel }
     * 
     * 
     */
    public List<ChatterAnswersReputationLevel> getChatterAnswersReputationLevels() {
        if (chatterAnswersReputationLevels == null) {
            chatterAnswersReputationLevels = new ArrayList<ChatterAnswersReputationLevel>();
        }
        return this.chatterAnswersReputationLevels;
    }

    /**
     * Gets the value of the ideaReputationLevels property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ideaReputationLevels property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdeaReputationLevels().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdeaReputationLevel }
     * 
     * 
     */
    public List<IdeaReputationLevel> getIdeaReputationLevels() {
        if (ideaReputationLevels == null) {
            ideaReputationLevels = new ArrayList<IdeaReputationLevel>();
        }
        return this.ideaReputationLevels;
    }

}
