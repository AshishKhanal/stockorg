
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomTabDefinitionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomTabDefinitionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="customObject"/&gt;
 *     &lt;enumeration value="url"/&gt;
 *     &lt;enumeration value="sControl"/&gt;
 *     &lt;enumeration value="apexPage"/&gt;
 *     &lt;enumeration value="flexiPage"/&gt;
 *     &lt;enumeration value="aura"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomTabDefinitionType")
@XmlEnum
public enum CustomTabDefinitionType {

    @XmlEnumValue("customObject")
    CUSTOM_OBJECT("customObject"),
    @XmlEnumValue("url")
    URL("url"),
    @XmlEnumValue("sControl")
    S_CONTROL("sControl"),
    @XmlEnumValue("apexPage")
    APEX_PAGE("apexPage"),
    @XmlEnumValue("flexiPage")
    FLEXI_PAGE("flexiPage"),
    @XmlEnumValue("aura")
    AURA("aura");
    private final String value;

    CustomTabDefinitionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomTabDefinitionType fromValue(String v) {
        for (CustomTabDefinitionType c: CustomTabDefinitionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
