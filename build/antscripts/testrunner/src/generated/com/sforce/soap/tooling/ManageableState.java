
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ManageableState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ManageableState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="released"/&gt;
 *     &lt;enumeration value="deleted"/&gt;
 *     &lt;enumeration value="deprecated"/&gt;
 *     &lt;enumeration value="installed"/&gt;
 *     &lt;enumeration value="beta"/&gt;
 *     &lt;enumeration value="unmanaged"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ManageableState")
@XmlEnum
public enum ManageableState {

    @XmlEnumValue("released")
    RELEASED("released"),
    @XmlEnumValue("deleted")
    DELETED("deleted"),
    @XmlEnumValue("deprecated")
    DEPRECATED("deprecated"),
    @XmlEnumValue("installed")
    INSTALLED("installed"),
    @XmlEnumValue("beta")
    BETA("beta"),
    @XmlEnumValue("unmanaged")
    UNMANAGED("unmanaged");
    private final String value;

    ManageableState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ManageableState fromValue(String v) {
        for (ManageableState c: ManageableState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
