
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContainerAsyncRequestState.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContainerAsyncRequestState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Queued"/&gt;
 *     &lt;enumeration value="Invalidated"/&gt;
 *     &lt;enumeration value="Completed"/&gt;
 *     &lt;enumeration value="Failed"/&gt;
 *     &lt;enumeration value="Error"/&gt;
 *     &lt;enumeration value="Aborted"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ContainerAsyncRequestState")
@XmlEnum
public enum ContainerAsyncRequestState {

    @XmlEnumValue("Queued")
    QUEUED("Queued"),
    @XmlEnumValue("Invalidated")
    INVALIDATED("Invalidated"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Error")
    ERROR("Error"),
    @XmlEnumValue("Aborted")
    ABORTED("Aborted");
    private final String value;

    ContainerAsyncRequestState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContainerAsyncRequestState fromValue(String v) {
        for (ContainerAsyncRequestState c: ContainerAsyncRequestState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
