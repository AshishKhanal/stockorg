
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerEventType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerEventType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Stopped"/&gt;
 *     &lt;enumeration value="Resumed"/&gt;
 *     &lt;enumeration value="RequestStarted"/&gt;
 *     &lt;enumeration value="RequestFinished"/&gt;
 *     &lt;enumeration value="SystemInfo"/&gt;
 *     &lt;enumeration value="SystemWarning"/&gt;
 *     &lt;enumeration value="SystemGack"/&gt;
 *     &lt;enumeration value="OrgChange"/&gt;
 *     &lt;enumeration value="SessionTerminated"/&gt;
 *     &lt;enumeration value="LogLine"/&gt;
 *     &lt;enumeration value="Debug"/&gt;
 *     &lt;enumeration value="HeartBeat"/&gt;
 *     &lt;enumeration value="ApexException"/&gt;
 *     &lt;enumeration value="Ready"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerEventType")
@XmlEnum
public enum ApexDebuggerEventType {

    @XmlEnumValue("Stopped")
    STOPPED("Stopped"),
    @XmlEnumValue("Resumed")
    RESUMED("Resumed"),
    @XmlEnumValue("RequestStarted")
    REQUEST_STARTED("RequestStarted"),
    @XmlEnumValue("RequestFinished")
    REQUEST_FINISHED("RequestFinished"),
    @XmlEnumValue("SystemInfo")
    SYSTEM_INFO("SystemInfo"),
    @XmlEnumValue("SystemWarning")
    SYSTEM_WARNING("SystemWarning"),
    @XmlEnumValue("SystemGack")
    SYSTEM_GACK("SystemGack"),
    @XmlEnumValue("OrgChange")
    ORG_CHANGE("OrgChange"),
    @XmlEnumValue("SessionTerminated")
    SESSION_TERMINATED("SessionTerminated"),
    @XmlEnumValue("LogLine")
    LOG_LINE("LogLine"),
    @XmlEnumValue("Debug")
    DEBUG("Debug"),
    @XmlEnumValue("HeartBeat")
    HEART_BEAT("HeartBeat"),
    @XmlEnumValue("ApexException")
    APEX_EXCEPTION("ApexException"),
    @XmlEnumValue("Ready")
    READY("Ready");
    private final String value;

    ApexDebuggerEventType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerEventType fromValue(String v) {
        for (ApexDebuggerEventType c: ApexDebuggerEventType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
