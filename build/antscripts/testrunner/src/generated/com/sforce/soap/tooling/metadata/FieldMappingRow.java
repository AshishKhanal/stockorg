
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.MappingOperation;


/**
 * <p>Java class for FieldMappingRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FieldMappingRow"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fieldMappingFields" type="{urn:metadata.tooling.soap.sforce.com}FieldMappingField" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="fieldName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mappingOperation" type="{urn:tooling.soap.sforce.com}MappingOperation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FieldMappingRow", propOrder = {
    "sObjectType",
    "fieldMappingFields",
    "fieldName",
    "mappingOperation"
})
public class FieldMappingRow {

    @XmlElement(name = "SObjectType", required = true)
    protected String sObjectType;
    protected List<FieldMappingField> fieldMappingFields;
    @XmlElement(required = true)
    protected String fieldName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected MappingOperation mappingOperation;

    /**
     * Gets the value of the sObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSObjectType() {
        return sObjectType;
    }

    /**
     * Sets the value of the sObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSObjectType(String value) {
        this.sObjectType = value;
    }

    /**
     * Gets the value of the fieldMappingFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldMappingFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldMappingFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldMappingField }
     * 
     * 
     */
    public List<FieldMappingField> getFieldMappingFields() {
        if (fieldMappingFields == null) {
            fieldMappingFields = new ArrayList<FieldMappingField>();
        }
        return this.fieldMappingFields;
    }

    /**
     * Gets the value of the fieldName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Sets the value of the fieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldName(String value) {
        this.fieldName = value;
    }

    /**
     * Gets the value of the mappingOperation property.
     * 
     * @return
     *     possible object is
     *     {@link MappingOperation }
     *     
     */
    public MappingOperation getMappingOperation() {
        return mappingOperation;
    }

    /**
     * Sets the value of the mappingOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link MappingOperation }
     *     
     */
    public void setMappingOperation(MappingOperation value) {
        this.mappingOperation = value;
    }

}
