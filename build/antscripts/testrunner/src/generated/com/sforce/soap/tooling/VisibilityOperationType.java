
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VisibilityOperationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VisibilityOperationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AddGroupMember"/&gt;
 *     &lt;enumeration value="AddTerritory"/&gt;
 *     &lt;enumeration value="AddUser"/&gt;
 *     &lt;enumeration value="AddUserRole"/&gt;
 *     &lt;enumeration value="AddUserTerritory"/&gt;
 *     &lt;enumeration value="DeleteGroup"/&gt;
 *     &lt;enumeration value="DeleteMemberFromGroup"/&gt;
 *     &lt;enumeration value="DeleteTerritory"/&gt;
 *     &lt;enumeration value="DeleteUserFromGroup"/&gt;
 *     &lt;enumeration value="DeleteUserRole"/&gt;
 *     &lt;enumeration value="DeleteUserTerritory"/&gt;
 *     &lt;enumeration value="DisableCommunityUser"/&gt;
 *     &lt;enumeration value="MergeAccounts"/&gt;
 *     &lt;enumeration value="PopulateGroup"/&gt;
 *     &lt;enumeration value="RecalculateGroupMembership"/&gt;
 *     &lt;enumeration value="RecalculateSharingRules"/&gt;
 *     &lt;enumeration value="ReenableTerritoryManagement"/&gt;
 *     &lt;enumeration value="TransferAccountOwnership"/&gt;
 *     &lt;enumeration value="TransferOpportunityOwnership"/&gt;
 *     &lt;enumeration value="TransferUserTerritory"/&gt;
 *     &lt;enumeration value="UpdateCommunityContact"/&gt;
 *     &lt;enumeration value="UpdateGroup"/&gt;
 *     &lt;enumeration value="UpdateGroupMembers"/&gt;
 *     &lt;enumeration value="UpdateRoleForUser"/&gt;
 *     &lt;enumeration value="UpdateTerritory"/&gt;
 *     &lt;enumeration value="UpdateUser"/&gt;
 *     &lt;enumeration value="UpdateUserRole"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VisibilityOperationType")
@XmlEnum
public enum VisibilityOperationType {

    @XmlEnumValue("AddGroupMember")
    ADD_GROUP_MEMBER("AddGroupMember"),
    @XmlEnumValue("AddTerritory")
    ADD_TERRITORY("AddTerritory"),
    @XmlEnumValue("AddUser")
    ADD_USER("AddUser"),
    @XmlEnumValue("AddUserRole")
    ADD_USER_ROLE("AddUserRole"),
    @XmlEnumValue("AddUserTerritory")
    ADD_USER_TERRITORY("AddUserTerritory"),
    @XmlEnumValue("DeleteGroup")
    DELETE_GROUP("DeleteGroup"),
    @XmlEnumValue("DeleteMemberFromGroup")
    DELETE_MEMBER_FROM_GROUP("DeleteMemberFromGroup"),
    @XmlEnumValue("DeleteTerritory")
    DELETE_TERRITORY("DeleteTerritory"),
    @XmlEnumValue("DeleteUserFromGroup")
    DELETE_USER_FROM_GROUP("DeleteUserFromGroup"),
    @XmlEnumValue("DeleteUserRole")
    DELETE_USER_ROLE("DeleteUserRole"),
    @XmlEnumValue("DeleteUserTerritory")
    DELETE_USER_TERRITORY("DeleteUserTerritory"),
    @XmlEnumValue("DisableCommunityUser")
    DISABLE_COMMUNITY_USER("DisableCommunityUser"),
    @XmlEnumValue("MergeAccounts")
    MERGE_ACCOUNTS("MergeAccounts"),
    @XmlEnumValue("PopulateGroup")
    POPULATE_GROUP("PopulateGroup"),
    @XmlEnumValue("RecalculateGroupMembership")
    RECALCULATE_GROUP_MEMBERSHIP("RecalculateGroupMembership"),
    @XmlEnumValue("RecalculateSharingRules")
    RECALCULATE_SHARING_RULES("RecalculateSharingRules"),
    @XmlEnumValue("ReenableTerritoryManagement")
    REENABLE_TERRITORY_MANAGEMENT("ReenableTerritoryManagement"),
    @XmlEnumValue("TransferAccountOwnership")
    TRANSFER_ACCOUNT_OWNERSHIP("TransferAccountOwnership"),
    @XmlEnumValue("TransferOpportunityOwnership")
    TRANSFER_OPPORTUNITY_OWNERSHIP("TransferOpportunityOwnership"),
    @XmlEnumValue("TransferUserTerritory")
    TRANSFER_USER_TERRITORY("TransferUserTerritory"),
    @XmlEnumValue("UpdateCommunityContact")
    UPDATE_COMMUNITY_CONTACT("UpdateCommunityContact"),
    @XmlEnumValue("UpdateGroup")
    UPDATE_GROUP("UpdateGroup"),
    @XmlEnumValue("UpdateGroupMembers")
    UPDATE_GROUP_MEMBERS("UpdateGroupMembers"),
    @XmlEnumValue("UpdateRoleForUser")
    UPDATE_ROLE_FOR_USER("UpdateRoleForUser"),
    @XmlEnumValue("UpdateTerritory")
    UPDATE_TERRITORY("UpdateTerritory"),
    @XmlEnumValue("UpdateUser")
    UPDATE_USER("UpdateUser"),
    @XmlEnumValue("UpdateUserRole")
    UPDATE_USER_ROLE("UpdateUserRole");
    private final String value;

    VisibilityOperationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VisibilityOperationType fromValue(String v) {
        for (VisibilityOperationType c: VisibilityOperationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
