
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CanvasLocationOptions.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CanvasLocationOptions"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="Chatter"/&gt;
 *     &lt;enumeration value="UserProfile"/&gt;
 *     &lt;enumeration value="Visualforce"/&gt;
 *     &lt;enumeration value="Aura"/&gt;
 *     &lt;enumeration value="Publisher"/&gt;
 *     &lt;enumeration value="ChatterFeed"/&gt;
 *     &lt;enumeration value="ServiceDesk"/&gt;
 *     &lt;enumeration value="OpenCTI"/&gt;
 *     &lt;enumeration value="AppLauncher"/&gt;
 *     &lt;enumeration value="MobileNav"/&gt;
 *     &lt;enumeration value="PageLayout"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CanvasLocationOptions")
@XmlEnum
public enum CanvasLocationOptions {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Chatter")
    CHATTER("Chatter"),
    @XmlEnumValue("UserProfile")
    USER_PROFILE("UserProfile"),
    @XmlEnumValue("Visualforce")
    VISUALFORCE("Visualforce"),
    @XmlEnumValue("Aura")
    AURA("Aura"),
    @XmlEnumValue("Publisher")
    PUBLISHER("Publisher"),
    @XmlEnumValue("ChatterFeed")
    CHATTER_FEED("ChatterFeed"),
    @XmlEnumValue("ServiceDesk")
    SERVICE_DESK("ServiceDesk"),
    @XmlEnumValue("OpenCTI")
    OPEN_CTI("OpenCTI"),
    @XmlEnumValue("AppLauncher")
    APP_LAUNCHER("AppLauncher"),
    @XmlEnumValue("MobileNav")
    MOBILE_NAV("MobileNav"),
    @XmlEnumValue("PageLayout")
    PAGE_LAYOUT("PageLayout");
    private final String value;

    CanvasLocationOptions(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CanvasLocationOptions fromValue(String v) {
        for (CanvasLocationOptions c: CanvasLocationOptions.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
