
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForecastingTypeSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastingTypeSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Active" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentsSettings" type="{urn:sobject.tooling.soap.sforce.com}AdjustmentsSettings" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentsSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DisplayedCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ForecastRangeSettings" type="{urn:sobject.tooling.soap.sforce.com}ForecastRangeSettings" minOccurs="0"/&gt;
 *         &lt;element name="ForecastRangeSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ForecastedCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManagerAdjustableCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsLabelMappings" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsSelectedSettings" type="{urn:sobject.tooling.soap.sforce.com}OpportunityListFieldsSelectedSettings" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsSelectedSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsUnselectedSettings" type="{urn:sobject.tooling.soap.sforce.com}OpportunityListFieldsUnselectedSettings" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsUnselectedSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OwnerAdjustableCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuotasSettings" type="{urn:sobject.tooling.soap.sforce.com}QuotasSettings" minOccurs="0"/&gt;
 *         &lt;element name="QuotasSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastingTypeSettings", propOrder = {
    "active",
    "adjustmentsSettings",
    "adjustmentsSettingsId",
    "displayedCategoryApiNames",
    "durableId",
    "forecastRangeSettings",
    "forecastRangeSettingsId",
    "forecastedCategoryApiNames",
    "managerAdjustableCategoryApiNames",
    "name",
    "opportunityListFieldsLabelMappings",
    "opportunityListFieldsSelectedSettings",
    "opportunityListFieldsSelectedSettingsId",
    "opportunityListFieldsUnselectedSettings",
    "opportunityListFieldsUnselectedSettingsId",
    "ownerAdjustableCategoryApiNames",
    "quotasSettings",
    "quotasSettingsId"
})
public class ForecastingTypeSettings
    extends SObject
{

    @XmlElementRef(name = "Active", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> active;
    @XmlElementRef(name = "AdjustmentsSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<AdjustmentsSettings> adjustmentsSettings;
    @XmlElementRef(name = "AdjustmentsSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> adjustmentsSettingsId;
    @XmlElementRef(name = "DisplayedCategoryApiNames", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> displayedCategoryApiNames;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "ForecastRangeSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<ForecastRangeSettings> forecastRangeSettings;
    @XmlElementRef(name = "ForecastRangeSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forecastRangeSettingsId;
    @XmlElementRef(name = "ForecastedCategoryApiNames", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forecastedCategoryApiNames;
    @XmlElementRef(name = "ManagerAdjustableCategoryApiNames", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> managerAdjustableCategoryApiNames;
    @XmlElementRef(name = "Name", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "OpportunityListFieldsLabelMappings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> opportunityListFieldsLabelMappings;
    @XmlElementRef(name = "OpportunityListFieldsSelectedSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<OpportunityListFieldsSelectedSettings> opportunityListFieldsSelectedSettings;
    @XmlElementRef(name = "OpportunityListFieldsSelectedSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> opportunityListFieldsSelectedSettingsId;
    @XmlElementRef(name = "OpportunityListFieldsUnselectedSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<OpportunityListFieldsUnselectedSettings> opportunityListFieldsUnselectedSettings;
    @XmlElementRef(name = "OpportunityListFieldsUnselectedSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> opportunityListFieldsUnselectedSettingsId;
    @XmlElementRef(name = "OwnerAdjustableCategoryApiNames", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ownerAdjustableCategoryApiNames;
    @XmlElementRef(name = "QuotasSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QuotasSettings> quotasSettings;
    @XmlElementRef(name = "QuotasSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quotasSettingsId;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setActive(JAXBElement<Boolean> value) {
        this.active = value;
    }

    /**
     * Gets the value of the adjustmentsSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AdjustmentsSettings }{@code >}
     *     
     */
    public JAXBElement<AdjustmentsSettings> getAdjustmentsSettings() {
        return adjustmentsSettings;
    }

    /**
     * Sets the value of the adjustmentsSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AdjustmentsSettings }{@code >}
     *     
     */
    public void setAdjustmentsSettings(JAXBElement<AdjustmentsSettings> value) {
        this.adjustmentsSettings = value;
    }

    /**
     * Gets the value of the adjustmentsSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAdjustmentsSettingsId() {
        return adjustmentsSettingsId;
    }

    /**
     * Sets the value of the adjustmentsSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAdjustmentsSettingsId(JAXBElement<String> value) {
        this.adjustmentsSettingsId = value;
    }

    /**
     * Gets the value of the displayedCategoryApiNames property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisplayedCategoryApiNames() {
        return displayedCategoryApiNames;
    }

    /**
     * Sets the value of the displayedCategoryApiNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisplayedCategoryApiNames(JAXBElement<String> value) {
        this.displayedCategoryApiNames = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the forecastRangeSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ForecastRangeSettings }{@code >}
     *     
     */
    public JAXBElement<ForecastRangeSettings> getForecastRangeSettings() {
        return forecastRangeSettings;
    }

    /**
     * Sets the value of the forecastRangeSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ForecastRangeSettings }{@code >}
     *     
     */
    public void setForecastRangeSettings(JAXBElement<ForecastRangeSettings> value) {
        this.forecastRangeSettings = value;
    }

    /**
     * Gets the value of the forecastRangeSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForecastRangeSettingsId() {
        return forecastRangeSettingsId;
    }

    /**
     * Sets the value of the forecastRangeSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForecastRangeSettingsId(JAXBElement<String> value) {
        this.forecastRangeSettingsId = value;
    }

    /**
     * Gets the value of the forecastedCategoryApiNames property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForecastedCategoryApiNames() {
        return forecastedCategoryApiNames;
    }

    /**
     * Sets the value of the forecastedCategoryApiNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForecastedCategoryApiNames(JAXBElement<String> value) {
        this.forecastedCategoryApiNames = value;
    }

    /**
     * Gets the value of the managerAdjustableCategoryApiNames property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getManagerAdjustableCategoryApiNames() {
        return managerAdjustableCategoryApiNames;
    }

    /**
     * Sets the value of the managerAdjustableCategoryApiNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setManagerAdjustableCategoryApiNames(JAXBElement<String> value) {
        this.managerAdjustableCategoryApiNames = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the opportunityListFieldsLabelMappings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpportunityListFieldsLabelMappings() {
        return opportunityListFieldsLabelMappings;
    }

    /**
     * Sets the value of the opportunityListFieldsLabelMappings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpportunityListFieldsLabelMappings(JAXBElement<String> value) {
        this.opportunityListFieldsLabelMappings = value;
    }

    /**
     * Gets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsSelectedSettings }{@code >}
     *     
     */
    public JAXBElement<OpportunityListFieldsSelectedSettings> getOpportunityListFieldsSelectedSettings() {
        return opportunityListFieldsSelectedSettings;
    }

    /**
     * Sets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsSelectedSettings }{@code >}
     *     
     */
    public void setOpportunityListFieldsSelectedSettings(JAXBElement<OpportunityListFieldsSelectedSettings> value) {
        this.opportunityListFieldsSelectedSettings = value;
    }

    /**
     * Gets the value of the opportunityListFieldsSelectedSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpportunityListFieldsSelectedSettingsId() {
        return opportunityListFieldsSelectedSettingsId;
    }

    /**
     * Sets the value of the opportunityListFieldsSelectedSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpportunityListFieldsSelectedSettingsId(JAXBElement<String> value) {
        this.opportunityListFieldsSelectedSettingsId = value;
    }

    /**
     * Gets the value of the opportunityListFieldsUnselectedSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsUnselectedSettings }{@code >}
     *     
     */
    public JAXBElement<OpportunityListFieldsUnselectedSettings> getOpportunityListFieldsUnselectedSettings() {
        return opportunityListFieldsUnselectedSettings;
    }

    /**
     * Sets the value of the opportunityListFieldsUnselectedSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsUnselectedSettings }{@code >}
     *     
     */
    public void setOpportunityListFieldsUnselectedSettings(JAXBElement<OpportunityListFieldsUnselectedSettings> value) {
        this.opportunityListFieldsUnselectedSettings = value;
    }

    /**
     * Gets the value of the opportunityListFieldsUnselectedSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpportunityListFieldsUnselectedSettingsId() {
        return opportunityListFieldsUnselectedSettingsId;
    }

    /**
     * Sets the value of the opportunityListFieldsUnselectedSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpportunityListFieldsUnselectedSettingsId(JAXBElement<String> value) {
        this.opportunityListFieldsUnselectedSettingsId = value;
    }

    /**
     * Gets the value of the ownerAdjustableCategoryApiNames property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOwnerAdjustableCategoryApiNames() {
        return ownerAdjustableCategoryApiNames;
    }

    /**
     * Sets the value of the ownerAdjustableCategoryApiNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOwnerAdjustableCategoryApiNames(JAXBElement<String> value) {
        this.ownerAdjustableCategoryApiNames = value;
    }

    /**
     * Gets the value of the quotasSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QuotasSettings }{@code >}
     *     
     */
    public JAXBElement<QuotasSettings> getQuotasSettings() {
        return quotasSettings;
    }

    /**
     * Sets the value of the quotasSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QuotasSettings }{@code >}
     *     
     */
    public void setQuotasSettings(JAXBElement<QuotasSettings> value) {
        this.quotasSettings = value;
    }

    /**
     * Gets the value of the quotasSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuotasSettingsId() {
        return quotasSettingsId;
    }

    /**
     * Sets the value of the quotasSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuotasSettingsId(JAXBElement<String> value) {
        this.quotasSettingsId = value;
    }

}
