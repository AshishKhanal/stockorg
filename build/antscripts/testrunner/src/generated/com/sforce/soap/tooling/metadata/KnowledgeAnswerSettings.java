
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeAnswerSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KnowledgeAnswerSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="assignTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultArticleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="enableArticleCreation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KnowledgeAnswerSettings", propOrder = {
    "assignTo",
    "defaultArticleType",
    "enableArticleCreation"
})
public class KnowledgeAnswerSettings {

    protected String assignTo;
    protected String defaultArticleType;
    protected Boolean enableArticleCreation;

    /**
     * Gets the value of the assignTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignTo() {
        return assignTo;
    }

    /**
     * Sets the value of the assignTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignTo(String value) {
        this.assignTo = value;
    }

    /**
     * Gets the value of the defaultArticleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultArticleType() {
        return defaultArticleType;
    }

    /**
     * Sets the value of the defaultArticleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultArticleType(String value) {
        this.defaultArticleType = value;
    }

    /**
     * Gets the value of the enableArticleCreation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableArticleCreation() {
        return enableArticleCreation;
    }

    /**
     * Sets the value of the enableArticleCreation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableArticleCreation(Boolean value) {
        this.enableArticleCreation = value;
    }

}
