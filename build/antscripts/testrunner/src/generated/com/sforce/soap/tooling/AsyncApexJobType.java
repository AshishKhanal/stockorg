
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AsyncApexJobType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AsyncApexJobType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Future"/&gt;
 *     &lt;enumeration value="SharingRecalculation"/&gt;
 *     &lt;enumeration value="ScheduledApex"/&gt;
 *     &lt;enumeration value="BatchApex"/&gt;
 *     &lt;enumeration value="BatchApexWorker"/&gt;
 *     &lt;enumeration value="TestRequest"/&gt;
 *     &lt;enumeration value="TestWorker"/&gt;
 *     &lt;enumeration value="ApexToken"/&gt;
 *     &lt;enumeration value="Queueable"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AsyncApexJobType")
@XmlEnum
public enum AsyncApexJobType {

    @XmlEnumValue("Future")
    FUTURE("Future"),
    @XmlEnumValue("SharingRecalculation")
    SHARING_RECALCULATION("SharingRecalculation"),
    @XmlEnumValue("ScheduledApex")
    SCHEDULED_APEX("ScheduledApex"),
    @XmlEnumValue("BatchApex")
    BATCH_APEX("BatchApex"),
    @XmlEnumValue("BatchApexWorker")
    BATCH_APEX_WORKER("BatchApexWorker"),
    @XmlEnumValue("TestRequest")
    TEST_REQUEST("TestRequest"),
    @XmlEnumValue("TestWorker")
    TEST_WORKER("TestWorker"),
    @XmlEnumValue("ApexToken")
    APEX_TOKEN("ApexToken"),
    @XmlEnumValue("Queueable")
    QUEUEABLE("Queueable");
    private final String value;

    AsyncApexJobType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AsyncApexJobType fromValue(String v) {
        for (AsyncApexJobType c: AsyncApexJobType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
