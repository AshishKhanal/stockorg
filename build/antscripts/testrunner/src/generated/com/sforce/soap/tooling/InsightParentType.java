
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsightParentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsightParentType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Opportunity"/&gt;
 *     &lt;enumeration value="Account"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InsightParentType")
@XmlEnum
public enum InsightParentType {

    @XmlEnumValue("Opportunity")
    OPPORTUNITY("Opportunity"),
    @XmlEnumValue("Account")
    ACCOUNT("Account");
    private final String value;

    InsightParentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsightParentType fromValue(String v) {
        for (InsightParentType c: InsightParentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
