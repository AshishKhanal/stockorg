
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sforce.soap.tooling package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sforce.soap.tooling
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link APIPerformanceInfo }
     * 
     */
    public APIPerformanceInfo createAPIPerformanceInfo() {
        return new APIPerformanceInfo();
    }

    /**
     * Create an instance of {@link NameValuePair }
     * 
     */
    public NameValuePair createNameValuePair() {
        return new NameValuePair();
    }

    /**
     * Create an instance of {@link AllOrNoneHeader }
     * 
     */
    public AllOrNoneHeader createAllOrNoneHeader() {
        return new AllOrNoneHeader();
    }

    /**
     * Create an instance of {@link AllowFieldTruncationHeader }
     * 
     */
    public AllowFieldTruncationHeader createAllowFieldTruncationHeader() {
        return new AllowFieldTruncationHeader();
    }

    /**
     * Create an instance of {@link CallOptions }
     * 
     */
    public CallOptions createCallOptions() {
        return new CallOptions();
    }

    /**
     * Create an instance of {@link DebuggingHeader }
     * 
     */
    public DebuggingHeader createDebuggingHeader() {
        return new DebuggingHeader();
    }

    /**
     * Create an instance of {@link LogInfo }
     * 
     */
    public LogInfo createLogInfo() {
        return new LogInfo();
    }

    /**
     * Create an instance of {@link DebuggingInfo }
     * 
     */
    public DebuggingInfo createDebuggingInfo() {
        return new DebuggingInfo();
    }

    /**
     * Create an instance of {@link DisableFeedTrackingHeader }
     * 
     */
    public DisableFeedTrackingHeader createDisableFeedTrackingHeader() {
        return new DisableFeedTrackingHeader();
    }

    /**
     * Create an instance of {@link MetadataVersionCheck }
     * 
     */
    public MetadataVersionCheck createMetadataVersionCheck() {
        return new MetadataVersionCheck();
    }

    /**
     * Create an instance of {@link Fact }
     * 
     */
    public Fact createFact() {
        return new Fact();
    }

    /**
     * Create an instance of {@link MetadataWarningsHeader }
     * 
     */
    public MetadataWarningsHeader createMetadataWarningsHeader() {
        return new MetadataWarningsHeader();
    }

    /**
     * Create an instance of {@link PackageVersionHeader }
     * 
     */
    public PackageVersionHeader createPackageVersionHeader() {
        return new PackageVersionHeader();
    }

    /**
     * Create an instance of {@link PackageVersion }
     * 
     */
    public PackageVersion createPackageVersion() {
        return new PackageVersion();
    }

    /**
     * Create an instance of {@link SessionHeader }
     * 
     */
    public SessionHeader createSessionHeader() {
        return new SessionHeader();
    }

    /**
     * Create an instance of {@link Create }
     * 
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link CreateResponse }
     * 
     */
    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    /**
     * Create an instance of {@link SaveResult }
     * 
     */
    public SaveResult createSaveResult() {
        return new SaveResult();
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link DeleteResult }
     * 
     */
    public DeleteResult createDeleteResult() {
        return new DeleteResult();
    }

    /**
     * Create an instance of {@link DescribeGlobal }
     * 
     */
    public DescribeGlobal createDescribeGlobal() {
        return new DescribeGlobal();
    }

    /**
     * Create an instance of {@link DescribeGlobalResponse }
     * 
     */
    public DescribeGlobalResponse createDescribeGlobalResponse() {
        return new DescribeGlobalResponse();
    }

    /**
     * Create an instance of {@link DescribeGlobalResult }
     * 
     */
    public DescribeGlobalResult createDescribeGlobalResult() {
        return new DescribeGlobalResult();
    }

    /**
     * Create an instance of {@link DescribeLayout }
     * 
     */
    public DescribeLayout createDescribeLayout() {
        return new DescribeLayout();
    }

    /**
     * Create an instance of {@link DescribeLayoutResponse }
     * 
     */
    public DescribeLayoutResponse createDescribeLayoutResponse() {
        return new DescribeLayoutResponse();
    }

    /**
     * Create an instance of {@link DescribeLayoutResult }
     * 
     */
    public DescribeLayoutResult createDescribeLayoutResult() {
        return new DescribeLayoutResult();
    }

    /**
     * Create an instance of {@link DescribeSObject }
     * 
     */
    public DescribeSObject createDescribeSObject() {
        return new DescribeSObject();
    }

    /**
     * Create an instance of {@link DescribeSObjectResponse }
     * 
     */
    public DescribeSObjectResponse createDescribeSObjectResponse() {
        return new DescribeSObjectResponse();
    }

    /**
     * Create an instance of {@link DescribeSObjectResult }
     * 
     */
    public DescribeSObjectResult createDescribeSObjectResult() {
        return new DescribeSObjectResult();
    }

    /**
     * Create an instance of {@link DescribeSObjects }
     * 
     */
    public DescribeSObjects createDescribeSObjects() {
        return new DescribeSObjects();
    }

    /**
     * Create an instance of {@link DescribeSObjectsResponse }
     * 
     */
    public DescribeSObjectsResponse createDescribeSObjectsResponse() {
        return new DescribeSObjectsResponse();
    }

    /**
     * Create an instance of {@link DescribeSoqlListViews }
     * 
     */
    public DescribeSoqlListViews createDescribeSoqlListViews() {
        return new DescribeSoqlListViews();
    }

    /**
     * Create an instance of {@link DescribeSoqlListViewsRequest }
     * 
     */
    public DescribeSoqlListViewsRequest createDescribeSoqlListViewsRequest() {
        return new DescribeSoqlListViewsRequest();
    }

    /**
     * Create an instance of {@link DescribeSoqlListViewsResponse }
     * 
     */
    public DescribeSoqlListViewsResponse createDescribeSoqlListViewsResponse() {
        return new DescribeSoqlListViewsResponse();
    }

    /**
     * Create an instance of {@link DescribeSoqlListViewResult }
     * 
     */
    public DescribeSoqlListViewResult createDescribeSoqlListViewResult() {
        return new DescribeSoqlListViewResult();
    }

    /**
     * Create an instance of {@link DescribeValueType }
     * 
     */
    public DescribeValueType createDescribeValueType() {
        return new DescribeValueType();
    }

    /**
     * Create an instance of {@link DescribeValueTypeResponse }
     * 
     */
    public DescribeValueTypeResponse createDescribeValueTypeResponse() {
        return new DescribeValueTypeResponse();
    }

    /**
     * Create an instance of {@link DescribeValueTypeResult }
     * 
     */
    public DescribeValueTypeResult createDescribeValueTypeResult() {
        return new DescribeValueTypeResult();
    }

    /**
     * Create an instance of {@link DescribeWorkitemActions }
     * 
     */
    public DescribeWorkitemActions createDescribeWorkitemActions() {
        return new DescribeWorkitemActions();
    }

    /**
     * Create an instance of {@link DescribeWorkitemActionsResponse }
     * 
     */
    public DescribeWorkitemActionsResponse createDescribeWorkitemActionsResponse() {
        return new DescribeWorkitemActionsResponse();
    }

    /**
     * Create an instance of {@link DescribeWorkitemActionResult }
     * 
     */
    public DescribeWorkitemActionResult createDescribeWorkitemActionResult() {
        return new DescribeWorkitemActionResult();
    }

    /**
     * Create an instance of {@link ExecuteAnonymous }
     * 
     */
    public ExecuteAnonymous createExecuteAnonymous() {
        return new ExecuteAnonymous();
    }

    /**
     * Create an instance of {@link ExecuteAnonymousResponse }
     * 
     */
    public ExecuteAnonymousResponse createExecuteAnonymousResponse() {
        return new ExecuteAnonymousResponse();
    }

    /**
     * Create an instance of {@link ExecuteAnonymousResult }
     * 
     */
    public ExecuteAnonymousResult createExecuteAnonymousResult() {
        return new ExecuteAnonymousResult();
    }

    /**
     * Create an instance of {@link GetDeleted }
     * 
     */
    public GetDeleted createGetDeleted() {
        return new GetDeleted();
    }

    /**
     * Create an instance of {@link GetDeletedResponse }
     * 
     */
    public GetDeletedResponse createGetDeletedResponse() {
        return new GetDeletedResponse();
    }

    /**
     * Create an instance of {@link GetDeletedResult }
     * 
     */
    public GetDeletedResult createGetDeletedResult() {
        return new GetDeletedResult();
    }

    /**
     * Create an instance of {@link GetServerTimestamp }
     * 
     */
    public GetServerTimestamp createGetServerTimestamp() {
        return new GetServerTimestamp();
    }

    /**
     * Create an instance of {@link GetServerTimestampResponse }
     * 
     */
    public GetServerTimestampResponse createGetServerTimestampResponse() {
        return new GetServerTimestampResponse();
    }

    /**
     * Create an instance of {@link GetServerTimestampResult }
     * 
     */
    public GetServerTimestampResult createGetServerTimestampResult() {
        return new GetServerTimestampResult();
    }

    /**
     * Create an instance of {@link GetUpdated }
     * 
     */
    public GetUpdated createGetUpdated() {
        return new GetUpdated();
    }

    /**
     * Create an instance of {@link GetUpdatedResponse }
     * 
     */
    public GetUpdatedResponse createGetUpdatedResponse() {
        return new GetUpdatedResponse();
    }

    /**
     * Create an instance of {@link GetUpdatedResult }
     * 
     */
    public GetUpdatedResult createGetUpdatedResult() {
        return new GetUpdatedResult();
    }

    /**
     * Create an instance of {@link GetUserInfo }
     * 
     */
    public GetUserInfo createGetUserInfo() {
        return new GetUserInfo();
    }

    /**
     * Create an instance of {@link GetUserInfoResponse }
     * 
     */
    public GetUserInfoResponse createGetUserInfoResponse() {
        return new GetUserInfoResponse();
    }

    /**
     * Create an instance of {@link GetUserInfoResult }
     * 
     */
    public GetUserInfoResult createGetUserInfoResult() {
        return new GetUserInfoResult();
    }

    /**
     * Create an instance of {@link InvalidateSessions }
     * 
     */
    public InvalidateSessions createInvalidateSessions() {
        return new InvalidateSessions();
    }

    /**
     * Create an instance of {@link InvalidateSessionsResponse }
     * 
     */
    public InvalidateSessionsResponse createInvalidateSessionsResponse() {
        return new InvalidateSessionsResponse();
    }

    /**
     * Create an instance of {@link InvalidateSessionsResult }
     * 
     */
    public InvalidateSessionsResult createInvalidateSessionsResult() {
        return new InvalidateSessionsResult();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link LoginResult }
     * 
     */
    public LoginResult createLoginResult() {
        return new LoginResult();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Query }
     * 
     */
    public Query createQuery() {
        return new Query();
    }

    /**
     * Create an instance of {@link QueryResponse }
     * 
     */
    public QueryResponse createQueryResponse() {
        return new QueryResponse();
    }

    /**
     * Create an instance of {@link QueryResult }
     * 
     */
    public QueryResult createQueryResult() {
        return new QueryResult();
    }

    /**
     * Create an instance of {@link QueryAll }
     * 
     */
    public QueryAll createQueryAll() {
        return new QueryAll();
    }

    /**
     * Create an instance of {@link QueryAllResponse }
     * 
     */
    public QueryAllResponse createQueryAllResponse() {
        return new QueryAllResponse();
    }

    /**
     * Create an instance of {@link QueryMore }
     * 
     */
    public QueryMore createQueryMore() {
        return new QueryMore();
    }

    /**
     * Create an instance of {@link QueryMoreResponse }
     * 
     */
    public QueryMoreResponse createQueryMoreResponse() {
        return new QueryMoreResponse();
    }

    /**
     * Create an instance of {@link Retrieve }
     * 
     */
    public Retrieve createRetrieve() {
        return new Retrieve();
    }

    /**
     * Create an instance of {@link RetrieveResponse }
     * 
     */
    public RetrieveResponse createRetrieveResponse() {
        return new RetrieveResponse();
    }

    /**
     * Create an instance of {@link RunTests }
     * 
     */
    public RunTests createRunTests() {
        return new RunTests();
    }

    /**
     * Create an instance of {@link RunTestsRequest }
     * 
     */
    public RunTestsRequest createRunTestsRequest() {
        return new RunTestsRequest();
    }

    /**
     * Create an instance of {@link RunTestsResponse }
     * 
     */
    public RunTestsResponse createRunTestsResponse() {
        return new RunTestsResponse();
    }

    /**
     * Create an instance of {@link RunTestsResult }
     * 
     */
    public RunTestsResult createRunTestsResult() {
        return new RunTestsResult();
    }

    /**
     * Create an instance of {@link RunTestsAsynchronous }
     * 
     */
    public RunTestsAsynchronous createRunTestsAsynchronous() {
        return new RunTestsAsynchronous();
    }

    /**
     * Create an instance of {@link RunTestsAsynchronousResponse }
     * 
     */
    public RunTestsAsynchronousResponse createRunTestsAsynchronousResponse() {
        return new RunTestsAsynchronousResponse();
    }

    /**
     * Create an instance of {@link Search }
     * 
     */
    public Search createSearch() {
        return new Search();
    }

    /**
     * Create an instance of {@link SearchResponse }
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link SearchResult }
     * 
     */
    public SearchResult createSearchResult() {
        return new SearchResult();
    }

    /**
     * Create an instance of {@link SetPassword }
     * 
     */
    public SetPassword createSetPassword() {
        return new SetPassword();
    }

    /**
     * Create an instance of {@link SetPasswordResponse }
     * 
     */
    public SetPasswordResponse createSetPasswordResponse() {
        return new SetPasswordResponse();
    }

    /**
     * Create an instance of {@link SetPasswordResult }
     * 
     */
    public SetPasswordResult createSetPasswordResult() {
        return new SetPasswordResult();
    }

    /**
     * Create an instance of {@link Update }
     * 
     */
    public Update createUpdate() {
        return new Update();
    }

    /**
     * Create an instance of {@link UpdateResponse }
     * 
     */
    public UpdateResponse createUpdateResponse() {
        return new UpdateResponse();
    }

    /**
     * Create an instance of {@link Upsert }
     * 
     */
    public Upsert createUpsert() {
        return new Upsert();
    }

    /**
     * Create an instance of {@link UpsertResponse }
     * 
     */
    public UpsertResponse createUpsertResponse() {
        return new UpsertResponse();
    }

    /**
     * Create an instance of {@link UpsertResult }
     * 
     */
    public UpsertResult createUpsertResult() {
        return new UpsertResult();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link ExtendedErrorDetails }
     * 
     */
    public ExtendedErrorDetails createExtendedErrorDetails() {
        return new ExtendedErrorDetails();
    }

    /**
     * Create an instance of {@link DescribeGlobalSObjectResult }
     * 
     */
    public DescribeGlobalSObjectResult createDescribeGlobalSObjectResult() {
        return new DescribeGlobalSObjectResult();
    }

    /**
     * Create an instance of {@link DescribeLayout2 }
     * 
     */
    public DescribeLayout2 createDescribeLayout2() {
        return new DescribeLayout2();
    }

    /**
     * Create an instance of {@link DescribeLayoutButtonSection }
     * 
     */
    public DescribeLayoutButtonSection createDescribeLayoutButtonSection() {
        return new DescribeLayoutButtonSection();
    }

    /**
     * Create an instance of {@link DescribeLayoutButton }
     * 
     */
    public DescribeLayoutButton createDescribeLayoutButton() {
        return new DescribeLayoutButton();
    }

    /**
     * Create an instance of {@link DescribeColorResult }
     * 
     */
    public DescribeColorResult createDescribeColorResult() {
        return new DescribeColorResult();
    }

    /**
     * Create an instance of {@link DescribeIconResult }
     * 
     */
    public DescribeIconResult createDescribeIconResult() {
        return new DescribeIconResult();
    }

    /**
     * Create an instance of {@link DescribeLayoutSection }
     * 
     */
    public DescribeLayoutSection createDescribeLayoutSection() {
        return new DescribeLayoutSection();
    }

    /**
     * Create an instance of {@link DescribeLayoutRow }
     * 
     */
    public DescribeLayoutRow createDescribeLayoutRow() {
        return new DescribeLayoutRow();
    }

    /**
     * Create an instance of {@link DescribeLayoutItem }
     * 
     */
    public DescribeLayoutItem createDescribeLayoutItem() {
        return new DescribeLayoutItem();
    }

    /**
     * Create an instance of {@link DescribeLayoutComponent }
     * 
     */
    public DescribeLayoutComponent createDescribeLayoutComponent() {
        return new DescribeLayoutComponent();
    }

    /**
     * Create an instance of {@link AnalyticsCloudComponent }
     * 
     */
    public AnalyticsCloudComponent createAnalyticsCloudComponent() {
        return new AnalyticsCloudComponent();
    }

    /**
     * Create an instance of {@link AuraComponent }
     * 
     */
    public AuraComponent createAuraComponent() {
        return new AuraComponent();
    }

    /**
     * Create an instance of {@link Canvas }
     * 
     */
    public Canvas createCanvas() {
        return new Canvas();
    }

    /**
     * Create an instance of {@link CustomLinkComponent }
     * 
     */
    public CustomLinkComponent createCustomLinkComponent() {
        return new CustomLinkComponent();
    }

    /**
     * Create an instance of {@link ExpandedLookup }
     * 
     */
    public ExpandedLookup createExpandedLookup() {
        return new ExpandedLookup();
    }

    /**
     * Create an instance of {@link FieldComponent }
     * 
     */
    public FieldComponent createFieldComponent() {
        return new FieldComponent();
    }

    /**
     * Create an instance of {@link FieldLayoutComponent }
     * 
     */
    public FieldLayoutComponent createFieldLayoutComponent() {
        return new FieldLayoutComponent();
    }

    /**
     * Create an instance of {@link ReportChartComponent }
     * 
     */
    public ReportChartComponent createReportChartComponent() {
        return new ReportChartComponent();
    }

    /**
     * Create an instance of {@link VisualforcePage }
     * 
     */
    public VisualforcePage createVisualforcePage() {
        return new VisualforcePage();
    }

    /**
     * Create an instance of {@link DescribeLayoutFeedView }
     * 
     */
    public DescribeLayoutFeedView createDescribeLayoutFeedView() {
        return new DescribeLayoutFeedView();
    }

    /**
     * Create an instance of {@link DescribeLayoutFeedFilter }
     * 
     */
    public DescribeLayoutFeedFilter createDescribeLayoutFeedFilter() {
        return new DescribeLayoutFeedFilter();
    }

    /**
     * Create an instance of {@link OfflineLink }
     * 
     */
    public OfflineLink createOfflineLink() {
        return new OfflineLink();
    }

    /**
     * Create an instance of {@link DescribeQuickActionListResult }
     * 
     */
    public DescribeQuickActionListResult createDescribeQuickActionListResult() {
        return new DescribeQuickActionListResult();
    }

    /**
     * Create an instance of {@link DescribeQuickActionListItemResult }
     * 
     */
    public DescribeQuickActionListItemResult createDescribeQuickActionListItemResult() {
        return new DescribeQuickActionListItemResult();
    }

    /**
     * Create an instance of {@link RelatedContent }
     * 
     */
    public RelatedContent createRelatedContent() {
        return new RelatedContent();
    }

    /**
     * Create an instance of {@link DescribeRelatedContentItem }
     * 
     */
    public DescribeRelatedContentItem createDescribeRelatedContentItem() {
        return new DescribeRelatedContentItem();
    }

    /**
     * Create an instance of {@link RelatedList }
     * 
     */
    public RelatedList createRelatedList() {
        return new RelatedList();
    }

    /**
     * Create an instance of {@link RelatedListColumn }
     * 
     */
    public RelatedListColumn createRelatedListColumn() {
        return new RelatedListColumn();
    }

    /**
     * Create an instance of {@link RelatedListSort }
     * 
     */
    public RelatedListSort createRelatedListSort() {
        return new RelatedListSort();
    }

    /**
     * Create an instance of {@link DescribeLayoutSaveOption }
     * 
     */
    public DescribeLayoutSaveOption createDescribeLayoutSaveOption() {
        return new DescribeLayoutSaveOption();
    }

    /**
     * Create an instance of {@link ActionOverride }
     * 
     */
    public ActionOverride createActionOverride() {
        return new ActionOverride();
    }

    /**
     * Create an instance of {@link ChildRelationship }
     * 
     */
    public ChildRelationship createChildRelationship() {
        return new ChildRelationship();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link FilteredLookupInfo }
     * 
     */
    public FilteredLookupInfo createFilteredLookupInfo() {
        return new FilteredLookupInfo();
    }

    /**
     * Create an instance of {@link PicklistEntry }
     * 
     */
    public PicklistEntry createPicklistEntry() {
        return new PicklistEntry();
    }

    /**
     * Create an instance of {@link NamedLayoutInfo }
     * 
     */
    public NamedLayoutInfo createNamedLayoutInfo() {
        return new NamedLayoutInfo();
    }

    /**
     * Create an instance of {@link RecordTypeInfo }
     * 
     */
    public RecordTypeInfo createRecordTypeInfo() {
        return new RecordTypeInfo();
    }

    /**
     * Create an instance of {@link ScopeInfo }
     * 
     */
    public ScopeInfo createScopeInfo() {
        return new ScopeInfo();
    }

    /**
     * Create an instance of {@link DescribeSoqlListViewParams }
     * 
     */
    public DescribeSoqlListViewParams createDescribeSoqlListViewParams() {
        return new DescribeSoqlListViewParams();
    }

    /**
     * Create an instance of {@link DescribeSoqlListView }
     * 
     */
    public DescribeSoqlListView createDescribeSoqlListView() {
        return new DescribeSoqlListView();
    }

    /**
     * Create an instance of {@link ListViewColumn }
     * 
     */
    public ListViewColumn createListViewColumn() {
        return new ListViewColumn();
    }

    /**
     * Create an instance of {@link ListViewOrderBy }
     * 
     */
    public ListViewOrderBy createListViewOrderBy() {
        return new ListViewOrderBy();
    }

    /**
     * Create an instance of {@link SoqlWhereCondition }
     * 
     */
    public SoqlWhereCondition createSoqlWhereCondition() {
        return new SoqlWhereCondition();
    }

    /**
     * Create an instance of {@link SoqlCondition }
     * 
     */
    public SoqlCondition createSoqlCondition() {
        return new SoqlCondition();
    }

    /**
     * Create an instance of {@link SoqlConditionGroup }
     * 
     */
    public SoqlConditionGroup createSoqlConditionGroup() {
        return new SoqlConditionGroup();
    }

    /**
     * Create an instance of {@link SoqlNotCondition }
     * 
     */
    public SoqlNotCondition createSoqlNotCondition() {
        return new SoqlNotCondition();
    }

    /**
     * Create an instance of {@link SoqlSubQueryCondition }
     * 
     */
    public SoqlSubQueryCondition createSoqlSubQueryCondition() {
        return new SoqlSubQueryCondition();
    }

    /**
     * Create an instance of {@link ValueTypeField }
     * 
     */
    public ValueTypeField createValueTypeField() {
        return new ValueTypeField();
    }

    /**
     * Create an instance of {@link AllowedWorkitemAction }
     * 
     */
    public AllowedWorkitemAction createAllowedWorkitemAction() {
        return new AllowedWorkitemAction();
    }

    /**
     * Create an instance of {@link DeletedRecord }
     * 
     */
    public DeletedRecord createDeletedRecord() {
        return new DeletedRecord();
    }

    /**
     * Create an instance of {@link CodeCoverageResult }
     * 
     */
    public CodeCoverageResult createCodeCoverageResult() {
        return new CodeCoverageResult();
    }

    /**
     * Create an instance of {@link CodeLocation }
     * 
     */
    public CodeLocation createCodeLocation() {
        return new CodeLocation();
    }

    /**
     * Create an instance of {@link CodeCoverageWarning }
     * 
     */
    public CodeCoverageWarning createCodeCoverageWarning() {
        return new CodeCoverageWarning();
    }

    /**
     * Create an instance of {@link RunTestFailure }
     * 
     */
    public RunTestFailure createRunTestFailure() {
        return new RunTestFailure();
    }

    /**
     * Create an instance of {@link RunTestSuccess }
     * 
     */
    public RunTestSuccess createRunTestSuccess() {
        return new RunTestSuccess();
    }

    /**
     * Create an instance of {@link SearchRecord }
     * 
     */
    public SearchRecord createSearchRecord() {
        return new SearchRecord();
    }

    /**
     * Create an instance of {@link SearchSnippet }
     * 
     */
    public SearchSnippet createSearchSnippet() {
        return new SearchSnippet();
    }

    /**
     * Create an instance of {@link SearchResultsMetadata }
     * 
     */
    public SearchResultsMetadata createSearchResultsMetadata() {
        return new SearchResultsMetadata();
    }

    /**
     * Create an instance of {@link LabelsSearchMetadata }
     * 
     */
    public LabelsSearchMetadata createLabelsSearchMetadata() {
        return new LabelsSearchMetadata();
    }

    /**
     * Create an instance of {@link EntitySearchMetadata }
     * 
     */
    public EntitySearchMetadata createEntitySearchMetadata() {
        return new EntitySearchMetadata();
    }

    /**
     * Create an instance of {@link FieldLevelSearchMetadata }
     * 
     */
    public FieldLevelSearchMetadata createFieldLevelSearchMetadata() {
        return new FieldLevelSearchMetadata();
    }

    /**
     * Create an instance of {@link SymbolTable }
     * 
     */
    public SymbolTable createSymbolTable() {
        return new SymbolTable();
    }

    /**
     * Create an instance of {@link Constructor }
     * 
     */
    public Constructor createConstructor() {
        return new Constructor();
    }

    /**
     * Create an instance of {@link VisibilitySymbol }
     * 
     */
    public VisibilitySymbol createVisibilitySymbol() {
        return new VisibilitySymbol();
    }

    /**
     * Create an instance of {@link Symbol }
     * 
     */
    public Symbol createSymbol() {
        return new Symbol();
    }

    /**
     * Create an instance of {@link Annotation }
     * 
     */
    public Annotation createAnnotation() {
        return new Annotation();
    }

    /**
     * Create an instance of {@link Position }
     * 
     */
    public Position createPosition() {
        return new Position();
    }

    /**
     * Create an instance of {@link Parameter }
     * 
     */
    public Parameter createParameter() {
        return new Parameter();
    }

    /**
     * Create an instance of {@link Method }
     * 
     */
    public Method createMethod() {
        return new Method();
    }

    /**
     * Create an instance of {@link ExternalReference }
     * 
     */
    public ExternalReference createExternalReference() {
        return new ExternalReference();
    }

    /**
     * Create an instance of {@link ExternalMethod }
     * 
     */
    public ExternalMethod createExternalMethod() {
        return new ExternalMethod();
    }

    /**
     * Create an instance of {@link ExternalConstructor }
     * 
     */
    public ExternalConstructor createExternalConstructor() {
        return new ExternalConstructor();
    }

    /**
     * Create an instance of {@link ExternalSymbol }
     * 
     */
    public ExternalSymbol createExternalSymbol() {
        return new ExternalSymbol();
    }

    /**
     * Create an instance of {@link Coverage }
     * 
     */
    public Coverage createCoverage() {
        return new Coverage();
    }

    /**
     * Create an instance of {@link HeapDump }
     * 
     */
    public HeapDump createHeapDump() {
        return new HeapDump();
    }

    /**
     * Create an instance of {@link TypeExtent }
     * 
     */
    public TypeExtent createTypeExtent() {
        return new TypeExtent();
    }

    /**
     * Create an instance of {@link AttributeDefinition }
     * 
     */
    public AttributeDefinition createAttributeDefinition() {
        return new AttributeDefinition();
    }

    /**
     * Create an instance of {@link HeapAddress }
     * 
     */
    public HeapAddress createHeapAddress() {
        return new HeapAddress();
    }

    /**
     * Create an instance of {@link StateValue }
     * 
     */
    public StateValue createStateValue() {
        return new StateValue();
    }

    /**
     * Create an instance of {@link BooleanValue }
     * 
     */
    public BooleanValue createBooleanValue() {
        return new BooleanValue();
    }

    /**
     * Create an instance of {@link ListValue }
     * 
     */
    public ListValue createListValue() {
        return new ListValue();
    }

    /**
     * Create an instance of {@link MapValue }
     * 
     */
    public MapValue createMapValue() {
        return new MapValue();
    }

    /**
     * Create an instance of {@link MapEntry }
     * 
     */
    public MapEntry createMapEntry() {
        return new MapEntry();
    }

    /**
     * Create an instance of {@link NumberValue }
     * 
     */
    public NumberValue createNumberValue() {
        return new NumberValue();
    }

    /**
     * Create an instance of {@link SetValue }
     * 
     */
    public SetValue createSetValue() {
        return new SetValue();
    }

    /**
     * Create an instance of {@link StringValue }
     * 
     */
    public StringValue createStringValue() {
        return new StringValue();
    }

    /**
     * Create an instance of {@link ApexResult }
     * 
     */
    public ApexResult createApexResult() {
        return new ApexResult();
    }

    /**
     * Create an instance of {@link SOQLResult }
     * 
     */
    public SOQLResult createSOQLResult() {
        return new SOQLResult();
    }

    /**
     * Create an instance of {@link QueryResultMetadata }
     * 
     */
    public QueryResultMetadata createQueryResultMetadata() {
        return new QueryResultMetadata();
    }

    /**
     * Create an instance of {@link QueryResultColumnMetadata }
     * 
     */
    public QueryResultColumnMetadata createQueryResultColumnMetadata() {
        return new QueryResultColumnMetadata();
    }

    /**
     * Create an instance of {@link AggregateExpressionResultColumnMetadata }
     * 
     */
    public AggregateExpressionResultColumnMetadata createAggregateExpressionResultColumnMetadata() {
        return new AggregateExpressionResultColumnMetadata();
    }

    /**
     * Create an instance of {@link ComplexQueryResultColumnMetadata }
     * 
     */
    public ComplexQueryResultColumnMetadata createComplexQueryResultColumnMetadata() {
        return new ComplexQueryResultColumnMetadata();
    }

    /**
     * Create an instance of {@link AggregateQueryResultColumnMetadata }
     * 
     */
    public AggregateQueryResultColumnMetadata createAggregateQueryResultColumnMetadata() {
        return new AggregateQueryResultColumnMetadata();
    }

    /**
     * Create an instance of {@link PrimitiveQueryResultColumnMetadata }
     * 
     */
    public PrimitiveQueryResultColumnMetadata createPrimitiveQueryResultColumnMetadata() {
        return new PrimitiveQueryResultColumnMetadata();
    }

    /**
     * Create an instance of {@link DeployDetails }
     * 
     */
    public DeployDetails createDeployDetails() {
        return new DeployDetails();
    }

    /**
     * Create an instance of {@link DeployMessage }
     * 
     */
    public DeployMessage createDeployMessage() {
        return new DeployMessage();
    }

    /**
     * Create an instance of {@link RecordTypesSupported }
     * 
     */
    public RecordTypesSupported createRecordTypesSupported() {
        return new RecordTypesSupported();
    }

    /**
     * Create an instance of {@link RelationshipReferenceTo }
     * 
     */
    public RelationshipReferenceTo createRelationshipReferenceTo() {
        return new RelationshipReferenceTo();
    }

    /**
     * Create an instance of {@link JunctionIdListNames }
     * 
     */
    public JunctionIdListNames createJunctionIdListNames() {
        return new JunctionIdListNames();
    }

    /**
     * Create an instance of {@link SearchLayoutButtonsDisplayed }
     * 
     */
    public SearchLayoutButtonsDisplayed createSearchLayoutButtonsDisplayed() {
        return new SearchLayoutButtonsDisplayed();
    }

    /**
     * Create an instance of {@link SearchLayoutButton }
     * 
     */
    public SearchLayoutButton createSearchLayoutButton() {
        return new SearchLayoutButton();
    }

    /**
     * Create an instance of {@link SearchLayoutFieldsDisplayed }
     * 
     */
    public SearchLayoutFieldsDisplayed createSearchLayoutFieldsDisplayed() {
        return new SearchLayoutFieldsDisplayed();
    }

    /**
     * Create an instance of {@link SearchLayoutField }
     * 
     */
    public SearchLayoutField createSearchLayoutField() {
        return new SearchLayoutField();
    }

    /**
     * Create an instance of {@link OperationPayload }
     * 
     */
    public OperationPayload createOperationPayload() {
        return new OperationPayload();
    }

    /**
     * Create an instance of {@link Territory2RunOppTerrAssignmentApexPayload }
     * 
     */
    public Territory2RunOppTerrAssignmentApexPayload createTerritory2RunOppTerrAssignmentApexPayload() {
        return new Territory2RunOppTerrAssignmentApexPayload();
    }

    /**
     * Create an instance of {@link Territory2RunTerritoryRulesPayload }
     * 
     */
    public Territory2RunTerritoryRulesPayload createTerritory2RunTerritoryRulesPayload() {
        return new Territory2RunTerritoryRulesPayload();
    }

    /**
     * Create an instance of {@link OperationParameters }
     * 
     */
    public OperationParameters createOperationParameters() {
        return new OperationParameters();
    }

}
