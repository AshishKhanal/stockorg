
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForecastingCategoryMapping complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastingCategoryMapping"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="forecastingItemCategoryApiName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="weightedSourceCategories" type="{urn:metadata.tooling.soap.sforce.com}WeightedSourceCategory" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastingCategoryMapping", propOrder = {
    "forecastingItemCategoryApiName",
    "weightedSourceCategories"
})
public class ForecastingCategoryMapping {

    @XmlElement(required = true)
    protected String forecastingItemCategoryApiName;
    protected List<WeightedSourceCategory> weightedSourceCategories;

    /**
     * Gets the value of the forecastingItemCategoryApiName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForecastingItemCategoryApiName() {
        return forecastingItemCategoryApiName;
    }

    /**
     * Sets the value of the forecastingItemCategoryApiName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForecastingItemCategoryApiName(String value) {
        this.forecastingItemCategoryApiName = value;
    }

    /**
     * Gets the value of the weightedSourceCategories property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the weightedSourceCategories property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeightedSourceCategories().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WeightedSourceCategory }
     * 
     * 
     */
    public List<WeightedSourceCategory> getWeightedSourceCategories() {
        if (weightedSourceCategories == null) {
            weightedSourceCategories = new ArrayList<WeightedSourceCategory>();
        }
        return this.weightedSourceCategories;
    }

}
