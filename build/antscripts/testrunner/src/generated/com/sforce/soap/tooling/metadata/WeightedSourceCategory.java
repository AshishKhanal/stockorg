
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WeightedSourceCategory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WeightedSourceCategory"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceCategoryApiName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeightedSourceCategory", propOrder = {
    "sourceCategoryApiName",
    "weight"
})
public class WeightedSourceCategory {

    @XmlElement(required = true)
    protected String sourceCategoryApiName;
    protected double weight;

    /**
     * Gets the value of the sourceCategoryApiName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceCategoryApiName() {
        return sourceCategoryApiName;
    }

    /**
     * Sets the value of the sourceCategoryApiName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceCategoryApiName(String value) {
        this.sourceCategoryApiName = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     */
    public void setWeight(double value) {
        this.weight = value;
    }

}
