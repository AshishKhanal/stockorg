
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.ActionOverrideType;
import com.sforce.soap.tooling.FormFactor;


/**
 * <p>Java class for ProfileActionOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileActionOverride"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actionName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="formFactor" type="{urn:tooling.soap.sforce.com}FormFactor"/&gt;
 *         &lt;element name="pageOrSobjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="recordType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}ActionOverrideType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileActionOverride", propOrder = {
    "actionName",
    "content",
    "formFactor",
    "pageOrSobjectType",
    "recordType",
    "type"
})
@XmlSeeAlso({
    AppProfileActionOverride.class
})
public class ProfileActionOverride {

    @XmlElement(required = true)
    protected String actionName;
    protected String content;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FormFactor formFactor;
    @XmlElement(required = true)
    protected String pageOrSobjectType;
    protected String recordType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ActionOverrideType type;

    /**
     * Gets the value of the actionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Sets the value of the actionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionName(String value) {
        this.actionName = value;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the formFactor property.
     * 
     * @return
     *     possible object is
     *     {@link FormFactor }
     *     
     */
    public FormFactor getFormFactor() {
        return formFactor;
    }

    /**
     * Sets the value of the formFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormFactor }
     *     
     */
    public void setFormFactor(FormFactor value) {
        this.formFactor = value;
    }

    /**
     * Gets the value of the pageOrSobjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageOrSobjectType() {
        return pageOrSobjectType;
    }

    /**
     * Sets the value of the pageOrSobjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageOrSobjectType(String value) {
        this.pageOrSobjectType = value;
    }

    /**
     * Gets the value of the recordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * Sets the value of the recordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordType(String value) {
        this.recordType = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ActionOverrideType }
     *     
     */
    public ActionOverrideType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionOverrideType }
     *     
     */
    public void setType(ActionOverrideType value) {
        this.type = value;
    }

}
