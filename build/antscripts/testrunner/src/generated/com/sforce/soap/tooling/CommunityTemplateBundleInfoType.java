
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunityTemplateBundleInfoType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunityTemplateBundleInfoType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Highlight"/&gt;
 *     &lt;enumeration value="PreviewImage"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CommunityTemplateBundleInfoType")
@XmlEnum
public enum CommunityTemplateBundleInfoType {

    @XmlEnumValue("Highlight")
    HIGHLIGHT("Highlight"),
    @XmlEnumValue("PreviewImage")
    PREVIEW_IMAGE("PreviewImage");
    private final String value;

    CommunityTemplateBundleInfoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunityTemplateBundleInfoType fromValue(String v) {
        for (CommunityTemplateBundleInfoType c: CommunityTemplateBundleInfoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
