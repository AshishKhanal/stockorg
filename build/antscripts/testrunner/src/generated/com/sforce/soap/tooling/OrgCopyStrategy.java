
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrgCopyStrategy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrgCopyStrategy"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="legacyTICopy"/&gt;
 *     &lt;enumeration value="unknown"/&gt;
 *     &lt;enumeration value="sandstormCopy"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrgCopyStrategy")
@XmlEnum
public enum OrgCopyStrategy {

    @XmlEnumValue("legacyTICopy")
    LEGACY_TI_COPY("legacyTICopy"),
    @XmlEnumValue("unknown")
    UNKNOWN("unknown"),
    @XmlEnumValue("sandstormCopy")
    SANDSTORM_COPY("sandstormCopy");
    private final String value;

    OrgCopyStrategy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrgCopyStrategy fromValue(String v) {
        for (OrgCopyStrategy c: OrgCopyStrategy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
