
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SamlEncryptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SamlEncryptionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AES_128"/&gt;
 *     &lt;enumeration value="AES_256"/&gt;
 *     &lt;enumeration value="Triple_Des"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SamlEncryptionType")
@XmlEnum
public enum SamlEncryptionType {

    AES_128("AES_128"),
    AES_256("AES_256"),
    @XmlEnumValue("Triple_Des")
    TRIPLE_DES("Triple_Des");
    private final String value;

    SamlEncryptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SamlEncryptionType fromValue(String v) {
        for (SamlEncryptionType c: SamlEncryptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
