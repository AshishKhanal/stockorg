
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableNegativeQuantity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableOrders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableReductionOrders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderSettings", propOrder = {
    "enableNegativeQuantity",
    "enableOrders",
    "enableReductionOrders"
})
public class OrderSettings
    extends MetadataForSettings
{

    protected Boolean enableNegativeQuantity;
    protected Boolean enableOrders;
    protected Boolean enableReductionOrders;

    /**
     * Gets the value of the enableNegativeQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableNegativeQuantity() {
        return enableNegativeQuantity;
    }

    /**
     * Sets the value of the enableNegativeQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableNegativeQuantity(Boolean value) {
        this.enableNegativeQuantity = value;
    }

    /**
     * Gets the value of the enableOrders property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOrders() {
        return enableOrders;
    }

    /**
     * Sets the value of the enableOrders property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOrders(Boolean value) {
        this.enableOrders = value;
    }

    /**
     * Gets the value of the enableReductionOrders property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableReductionOrders() {
        return enableReductionOrders;
    }

    /**
     * Sets the value of the enableReductionOrders property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableReductionOrders(Boolean value) {
        this.enableReductionOrders = value;
    }

}
