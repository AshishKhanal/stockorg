
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EscalationStartTimeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EscalationStartTimeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CaseCreation"/&gt;
 *     &lt;enumeration value="CaseLastModified"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EscalationStartTimeType")
@XmlEnum
public enum EscalationStartTimeType {

    @XmlEnumValue("CaseCreation")
    CASE_CREATION("CaseCreation"),
    @XmlEnumValue("CaseLastModified")
    CASE_LAST_MODIFIED("CaseLastModified");
    private final String value;

    EscalationStartTimeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EscalationStartTimeType fromValue(String v) {
        for (EscalationStartTimeType c: EscalationStartTimeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
