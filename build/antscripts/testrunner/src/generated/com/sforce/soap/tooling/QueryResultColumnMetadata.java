
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryResultColumnMetadata complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryResultColumnMetadata"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aggregate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="apexType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="booleanType" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="columnName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="custom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="foreignKeyName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="insertable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="joinColumns" type="{urn:tooling.soap.sforce.com}QueryResultColumnMetadata" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="numberType" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="textType" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="updatable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryResultColumnMetadata", propOrder = {
    "aggregate",
    "apexType",
    "booleanType",
    "columnName",
    "custom",
    "displayName",
    "foreignKeyName",
    "insertable",
    "joinColumns",
    "numberType",
    "textType",
    "updatable"
})
@XmlSeeAlso({
    AggregateExpressionResultColumnMetadata.class,
    ComplexQueryResultColumnMetadata.class,
    PrimitiveQueryResultColumnMetadata.class
})
public class QueryResultColumnMetadata {

    protected boolean aggregate;
    @XmlElement(required = true)
    protected String apexType;
    protected boolean booleanType;
    @XmlElement(required = true)
    protected String columnName;
    protected boolean custom;
    @XmlElement(required = true)
    protected String displayName;
    @XmlElement(required = true)
    protected String foreignKeyName;
    protected boolean insertable;
    protected List<QueryResultColumnMetadata> joinColumns;
    protected boolean numberType;
    protected boolean textType;
    protected boolean updatable;

    /**
     * Gets the value of the aggregate property.
     * 
     */
    public boolean isAggregate() {
        return aggregate;
    }

    /**
     * Sets the value of the aggregate property.
     * 
     */
    public void setAggregate(boolean value) {
        this.aggregate = value;
    }

    /**
     * Gets the value of the apexType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApexType() {
        return apexType;
    }

    /**
     * Sets the value of the apexType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApexType(String value) {
        this.apexType = value;
    }

    /**
     * Gets the value of the booleanType property.
     * 
     */
    public boolean isBooleanType() {
        return booleanType;
    }

    /**
     * Sets the value of the booleanType property.
     * 
     */
    public void setBooleanType(boolean value) {
        this.booleanType = value;
    }

    /**
     * Gets the value of the columnName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Sets the value of the columnName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setColumnName(String value) {
        this.columnName = value;
    }

    /**
     * Gets the value of the custom property.
     * 
     */
    public boolean isCustom() {
        return custom;
    }

    /**
     * Sets the value of the custom property.
     * 
     */
    public void setCustom(boolean value) {
        this.custom = value;
    }

    /**
     * Gets the value of the displayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the value of the displayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * Gets the value of the foreignKeyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignKeyName() {
        return foreignKeyName;
    }

    /**
     * Sets the value of the foreignKeyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignKeyName(String value) {
        this.foreignKeyName = value;
    }

    /**
     * Gets the value of the insertable property.
     * 
     */
    public boolean isInsertable() {
        return insertable;
    }

    /**
     * Sets the value of the insertable property.
     * 
     */
    public void setInsertable(boolean value) {
        this.insertable = value;
    }

    /**
     * Gets the value of the joinColumns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the joinColumns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJoinColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryResultColumnMetadata }
     * 
     * 
     */
    public List<QueryResultColumnMetadata> getJoinColumns() {
        if (joinColumns == null) {
            joinColumns = new ArrayList<QueryResultColumnMetadata>();
        }
        return this.joinColumns;
    }

    /**
     * Gets the value of the numberType property.
     * 
     */
    public boolean isNumberType() {
        return numberType;
    }

    /**
     * Sets the value of the numberType property.
     * 
     */
    public void setNumberType(boolean value) {
        this.numberType = value;
    }

    /**
     * Gets the value of the textType property.
     * 
     */
    public boolean isTextType() {
        return textType;
    }

    /**
     * Sets the value of the textType property.
     * 
     */
    public void setTextType(boolean value) {
        this.textType = value;
    }

    /**
     * Gets the value of the updatable property.
     * 
     */
    public boolean isUpdatable() {
        return updatable;
    }

    /**
     * Sets the value of the updatable property.
     * 
     */
    public void setUpdatable(boolean value) {
        this.updatable = value;
    }

}
