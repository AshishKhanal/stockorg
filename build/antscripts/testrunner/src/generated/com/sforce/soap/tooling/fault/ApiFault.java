
package com.sforce.soap.tooling.fault;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.ExtendedErrorDetails;


/**
 * <p>Java class for ApiFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApiFault"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="exceptionCode" type="{urn:fault.tooling.soap.sforce.com}ExceptionCode"/&gt;
 *         &lt;element name="exceptionMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="extendedErrorDetails" type="{urn:tooling.soap.sforce.com}ExtendedErrorDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="upgradeURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="upgradeMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApiFault", propOrder = {
    "exceptionCode",
    "exceptionMessage",
    "extendedErrorDetails",
    "upgradeURL",
    "upgradeMessage"
})
@XmlSeeAlso({
    LoginFault.class,
    InvalidQueryLocatorFault.class,
    InvalidNewPasswordFault.class,
    InvalidIdFault.class,
    UnexpectedErrorFault.class,
    ApiQueryFault.class
})
public class ApiFault {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ExceptionCode exceptionCode;
    protected String exceptionMessage;
    @XmlElement(nillable = true)
    protected List<ExtendedErrorDetails> extendedErrorDetails;
    protected String upgradeURL;
    protected String upgradeMessage;

    /**
     * Gets the value of the exceptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link ExceptionCode }
     *     
     */
    public ExceptionCode getExceptionCode() {
        return exceptionCode;
    }

    /**
     * Sets the value of the exceptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExceptionCode }
     *     
     */
    public void setExceptionCode(ExceptionCode value) {
        this.exceptionCode = value;
    }

    /**
     * Gets the value of the exceptionMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionMessage() {
        return exceptionMessage;
    }

    /**
     * Sets the value of the exceptionMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionMessage(String value) {
        this.exceptionMessage = value;
    }

    /**
     * Gets the value of the extendedErrorDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extendedErrorDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtendedErrorDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtendedErrorDetails }
     * 
     * 
     */
    public List<ExtendedErrorDetails> getExtendedErrorDetails() {
        if (extendedErrorDetails == null) {
            extendedErrorDetails = new ArrayList<ExtendedErrorDetails>();
        }
        return this.extendedErrorDetails;
    }

    /**
     * Gets the value of the upgradeURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpgradeURL() {
        return upgradeURL;
    }

    /**
     * Sets the value of the upgradeURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpgradeURL(String value) {
        this.upgradeURL = value;
    }

    /**
     * Gets the value of the upgradeMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpgradeMessage() {
        return upgradeMessage;
    }

    /**
     * Sets the value of the upgradeMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpgradeMessage(String value) {
        this.upgradeMessage = value;
    }

}
