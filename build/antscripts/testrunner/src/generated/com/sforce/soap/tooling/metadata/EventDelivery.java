
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.EventDeliveryType;


/**
 * <p>Java class for EventDelivery complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventDelivery"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eventParameters" type="{urn:metadata.tooling.soap.sforce.com}EventParameterMap" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="eventSubscription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="referenceData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}EventDeliveryType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventDelivery", propOrder = {
    "eventParameters",
    "eventSubscription",
    "referenceData",
    "type"
})
public class EventDelivery
    extends Metadata
{

    protected List<EventParameterMap> eventParameters;
    @XmlElement(required = true)
    protected String eventSubscription;
    protected String referenceData;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected EventDeliveryType type;

    /**
     * Gets the value of the eventParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventParameterMap }
     * 
     * 
     */
    public List<EventParameterMap> getEventParameters() {
        if (eventParameters == null) {
            eventParameters = new ArrayList<EventParameterMap>();
        }
        return this.eventParameters;
    }

    /**
     * Gets the value of the eventSubscription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventSubscription() {
        return eventSubscription;
    }

    /**
     * Sets the value of the eventSubscription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventSubscription(String value) {
        this.eventSubscription = value;
    }

    /**
     * Gets the value of the referenceData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceData() {
        return referenceData;
    }

    /**
     * Sets the value of the referenceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceData(String value) {
        this.referenceData = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link EventDeliveryType }
     *     
     */
    public EventDeliveryType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventDeliveryType }
     *     
     */
    public void setType(EventDeliveryType value) {
        this.type = value;
    }

}
