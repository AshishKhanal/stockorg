
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FlexiPageRegionMode;
import com.sforce.soap.tooling.FlexiPageRegionType;
import com.sforce.soap.tooling.RegionFlagStatus;


/**
 * <p>Java class for FlexiPageRegion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexiPageRegion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="appendable" type="{urn:tooling.soap.sforce.com}RegionFlagStatus" minOccurs="0"/&gt;
 *         &lt;element name="componentInstances" type="{urn:metadata.tooling.soap.sforce.com}ComponentInstance" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mode" type="{urn:tooling.soap.sforce.com}FlexiPageRegionMode" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prependable" type="{urn:tooling.soap.sforce.com}RegionFlagStatus" minOccurs="0"/&gt;
 *         &lt;element name="replaceable" type="{urn:tooling.soap.sforce.com}RegionFlagStatus" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}FlexiPageRegionType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexiPageRegion", propOrder = {
    "appendable",
    "componentInstances",
    "mode",
    "name",
    "prependable",
    "replaceable",
    "type"
})
public class FlexiPageRegion {

    @XmlSchemaType(name = "string")
    protected RegionFlagStatus appendable;
    protected List<ComponentInstance> componentInstances;
    @XmlSchemaType(name = "string")
    protected FlexiPageRegionMode mode;
    @XmlElement(required = true)
    protected String name;
    @XmlSchemaType(name = "string")
    protected RegionFlagStatus prependable;
    @XmlSchemaType(name = "string")
    protected RegionFlagStatus replaceable;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FlexiPageRegionType type;

    /**
     * Gets the value of the appendable property.
     * 
     * @return
     *     possible object is
     *     {@link RegionFlagStatus }
     *     
     */
    public RegionFlagStatus getAppendable() {
        return appendable;
    }

    /**
     * Sets the value of the appendable property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionFlagStatus }
     *     
     */
    public void setAppendable(RegionFlagStatus value) {
        this.appendable = value;
    }

    /**
     * Gets the value of the componentInstances property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the componentInstances property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComponentInstances().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComponentInstance }
     * 
     * 
     */
    public List<ComponentInstance> getComponentInstances() {
        if (componentInstances == null) {
            componentInstances = new ArrayList<ComponentInstance>();
        }
        return this.componentInstances;
    }

    /**
     * Gets the value of the mode property.
     * 
     * @return
     *     possible object is
     *     {@link FlexiPageRegionMode }
     *     
     */
    public FlexiPageRegionMode getMode() {
        return mode;
    }

    /**
     * Sets the value of the mode property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexiPageRegionMode }
     *     
     */
    public void setMode(FlexiPageRegionMode value) {
        this.mode = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the prependable property.
     * 
     * @return
     *     possible object is
     *     {@link RegionFlagStatus }
     *     
     */
    public RegionFlagStatus getPrependable() {
        return prependable;
    }

    /**
     * Sets the value of the prependable property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionFlagStatus }
     *     
     */
    public void setPrependable(RegionFlagStatus value) {
        this.prependable = value;
    }

    /**
     * Gets the value of the replaceable property.
     * 
     * @return
     *     possible object is
     *     {@link RegionFlagStatus }
     *     
     */
    public RegionFlagStatus getReplaceable() {
        return replaceable;
    }

    /**
     * Sets the value of the replaceable property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionFlagStatus }
     *     
     */
    public void setReplaceable(RegionFlagStatus value) {
        this.replaceable = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link FlexiPageRegionType }
     *     
     */
    public FlexiPageRegionType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexiPageRegionType }
     *     
     */
    public void setType(FlexiPageRegionType value) {
        this.type = value;
    }

}
