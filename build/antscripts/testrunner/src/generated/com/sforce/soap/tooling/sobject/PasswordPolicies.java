
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PasswordPolicies complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PasswordPolicies"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApiOnlyUserHomePageUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Complexity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Expiration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HistoryRestriction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LockoutInterval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxLoginAttempts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MinPasswordLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MinimumPasswordLifetime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ObscureSecretAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="PasswordAssistanceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PasswordAssistanceUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuestionRestriction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordPolicies", propOrder = {
    "apiOnlyUserHomePageUrl",
    "complexity",
    "durableId",
    "expiration",
    "historyRestriction",
    "lockoutInterval",
    "maxLoginAttempts",
    "minPasswordLength",
    "minimumPasswordLifetime",
    "obscureSecretAnswer",
    "passwordAssistanceMessage",
    "passwordAssistanceUrl",
    "questionRestriction"
})
public class PasswordPolicies
    extends SObject
{

    @XmlElementRef(name = "ApiOnlyUserHomePageUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> apiOnlyUserHomePageUrl;
    @XmlElementRef(name = "Complexity", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complexity;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "Expiration", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> expiration;
    @XmlElementRef(name = "HistoryRestriction", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> historyRestriction;
    @XmlElementRef(name = "LockoutInterval", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lockoutInterval;
    @XmlElementRef(name = "MaxLoginAttempts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maxLoginAttempts;
    @XmlElementRef(name = "MinPasswordLength", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> minPasswordLength;
    @XmlElementRef(name = "MinimumPasswordLifetime", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> minimumPasswordLifetime;
    @XmlElementRef(name = "ObscureSecretAnswer", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> obscureSecretAnswer;
    @XmlElementRef(name = "PasswordAssistanceMessage", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passwordAssistanceMessage;
    @XmlElementRef(name = "PasswordAssistanceUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passwordAssistanceUrl;
    @XmlElementRef(name = "QuestionRestriction", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> questionRestriction;

    /**
     * Gets the value of the apiOnlyUserHomePageUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApiOnlyUserHomePageUrl() {
        return apiOnlyUserHomePageUrl;
    }

    /**
     * Sets the value of the apiOnlyUserHomePageUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApiOnlyUserHomePageUrl(JAXBElement<String> value) {
        this.apiOnlyUserHomePageUrl = value;
    }

    /**
     * Gets the value of the complexity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplexity() {
        return complexity;
    }

    /**
     * Sets the value of the complexity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplexity(JAXBElement<String> value) {
        this.complexity = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the expiration property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExpiration() {
        return expiration;
    }

    /**
     * Sets the value of the expiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExpiration(JAXBElement<String> value) {
        this.expiration = value;
    }

    /**
     * Gets the value of the historyRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHistoryRestriction() {
        return historyRestriction;
    }

    /**
     * Sets the value of the historyRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHistoryRestriction(JAXBElement<String> value) {
        this.historyRestriction = value;
    }

    /**
     * Gets the value of the lockoutInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLockoutInterval() {
        return lockoutInterval;
    }

    /**
     * Sets the value of the lockoutInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLockoutInterval(JAXBElement<String> value) {
        this.lockoutInterval = value;
    }

    /**
     * Gets the value of the maxLoginAttempts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMaxLoginAttempts() {
        return maxLoginAttempts;
    }

    /**
     * Sets the value of the maxLoginAttempts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMaxLoginAttempts(JAXBElement<String> value) {
        this.maxLoginAttempts = value;
    }

    /**
     * Gets the value of the minPasswordLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMinPasswordLength() {
        return minPasswordLength;
    }

    /**
     * Sets the value of the minPasswordLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMinPasswordLength(JAXBElement<String> value) {
        this.minPasswordLength = value;
    }

    /**
     * Gets the value of the minimumPasswordLifetime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getMinimumPasswordLifetime() {
        return minimumPasswordLifetime;
    }

    /**
     * Sets the value of the minimumPasswordLifetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setMinimumPasswordLifetime(JAXBElement<Boolean> value) {
        this.minimumPasswordLifetime = value;
    }

    /**
     * Gets the value of the obscureSecretAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getObscureSecretAnswer() {
        return obscureSecretAnswer;
    }

    /**
     * Sets the value of the obscureSecretAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setObscureSecretAnswer(JAXBElement<Boolean> value) {
        this.obscureSecretAnswer = value;
    }

    /**
     * Gets the value of the passwordAssistanceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasswordAssistanceMessage() {
        return passwordAssistanceMessage;
    }

    /**
     * Sets the value of the passwordAssistanceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasswordAssistanceMessage(JAXBElement<String> value) {
        this.passwordAssistanceMessage = value;
    }

    /**
     * Gets the value of the passwordAssistanceUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasswordAssistanceUrl() {
        return passwordAssistanceUrl;
    }

    /**
     * Sets the value of the passwordAssistanceUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasswordAssistanceUrl(JAXBElement<String> value) {
        this.passwordAssistanceUrl = value;
    }

    /**
     * Gets the value of the questionRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuestionRestriction() {
        return questionRestriction;
    }

    /**
     * Sets the value of the questionRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuestionRestriction(JAXBElement<String> value) {
        this.questionRestriction = value;
    }

}
