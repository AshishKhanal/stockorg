
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="documentContentSearchEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="optimizeSearchForCJKEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="recentlyViewedUsersForBlankLookupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="searchSettingsByObject" type="{urn:metadata.tooling.soap.sforce.com}SearchSettingsByObject"/&gt;
 *         &lt;element name="sidebarAutoCompleteEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="sidebarDropDownListEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="sidebarLimitToItemsIOwnCheckboxEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="singleSearchResultShortcutEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="spellCorrectKnowledgeSearchEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchSettings", propOrder = {
    "documentContentSearchEnabled",
    "optimizeSearchForCJKEnabled",
    "recentlyViewedUsersForBlankLookupEnabled",
    "searchSettingsByObject",
    "sidebarAutoCompleteEnabled",
    "sidebarDropDownListEnabled",
    "sidebarLimitToItemsIOwnCheckboxEnabled",
    "singleSearchResultShortcutEnabled",
    "spellCorrectKnowledgeSearchEnabled"
})
public class SearchSettings
    extends MetadataForSettings
{

    protected boolean documentContentSearchEnabled;
    protected boolean optimizeSearchForCJKEnabled;
    protected boolean recentlyViewedUsersForBlankLookupEnabled;
    @XmlElement(required = true)
    protected SearchSettingsByObject searchSettingsByObject;
    protected boolean sidebarAutoCompleteEnabled;
    protected boolean sidebarDropDownListEnabled;
    protected boolean sidebarLimitToItemsIOwnCheckboxEnabled;
    protected boolean singleSearchResultShortcutEnabled;
    protected boolean spellCorrectKnowledgeSearchEnabled;

    /**
     * Gets the value of the documentContentSearchEnabled property.
     * 
     */
    public boolean isDocumentContentSearchEnabled() {
        return documentContentSearchEnabled;
    }

    /**
     * Sets the value of the documentContentSearchEnabled property.
     * 
     */
    public void setDocumentContentSearchEnabled(boolean value) {
        this.documentContentSearchEnabled = value;
    }

    /**
     * Gets the value of the optimizeSearchForCJKEnabled property.
     * 
     */
    public boolean isOptimizeSearchForCJKEnabled() {
        return optimizeSearchForCJKEnabled;
    }

    /**
     * Sets the value of the optimizeSearchForCJKEnabled property.
     * 
     */
    public void setOptimizeSearchForCJKEnabled(boolean value) {
        this.optimizeSearchForCJKEnabled = value;
    }

    /**
     * Gets the value of the recentlyViewedUsersForBlankLookupEnabled property.
     * 
     */
    public boolean isRecentlyViewedUsersForBlankLookupEnabled() {
        return recentlyViewedUsersForBlankLookupEnabled;
    }

    /**
     * Sets the value of the recentlyViewedUsersForBlankLookupEnabled property.
     * 
     */
    public void setRecentlyViewedUsersForBlankLookupEnabled(boolean value) {
        this.recentlyViewedUsersForBlankLookupEnabled = value;
    }

    /**
     * Gets the value of the searchSettingsByObject property.
     * 
     * @return
     *     possible object is
     *     {@link SearchSettingsByObject }
     *     
     */
    public SearchSettingsByObject getSearchSettingsByObject() {
        return searchSettingsByObject;
    }

    /**
     * Sets the value of the searchSettingsByObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchSettingsByObject }
     *     
     */
    public void setSearchSettingsByObject(SearchSettingsByObject value) {
        this.searchSettingsByObject = value;
    }

    /**
     * Gets the value of the sidebarAutoCompleteEnabled property.
     * 
     */
    public boolean isSidebarAutoCompleteEnabled() {
        return sidebarAutoCompleteEnabled;
    }

    /**
     * Sets the value of the sidebarAutoCompleteEnabled property.
     * 
     */
    public void setSidebarAutoCompleteEnabled(boolean value) {
        this.sidebarAutoCompleteEnabled = value;
    }

    /**
     * Gets the value of the sidebarDropDownListEnabled property.
     * 
     */
    public boolean isSidebarDropDownListEnabled() {
        return sidebarDropDownListEnabled;
    }

    /**
     * Sets the value of the sidebarDropDownListEnabled property.
     * 
     */
    public void setSidebarDropDownListEnabled(boolean value) {
        this.sidebarDropDownListEnabled = value;
    }

    /**
     * Gets the value of the sidebarLimitToItemsIOwnCheckboxEnabled property.
     * 
     */
    public boolean isSidebarLimitToItemsIOwnCheckboxEnabled() {
        return sidebarLimitToItemsIOwnCheckboxEnabled;
    }

    /**
     * Sets the value of the sidebarLimitToItemsIOwnCheckboxEnabled property.
     * 
     */
    public void setSidebarLimitToItemsIOwnCheckboxEnabled(boolean value) {
        this.sidebarLimitToItemsIOwnCheckboxEnabled = value;
    }

    /**
     * Gets the value of the singleSearchResultShortcutEnabled property.
     * 
     */
    public boolean isSingleSearchResultShortcutEnabled() {
        return singleSearchResultShortcutEnabled;
    }

    /**
     * Sets the value of the singleSearchResultShortcutEnabled property.
     * 
     */
    public void setSingleSearchResultShortcutEnabled(boolean value) {
        this.singleSearchResultShortcutEnabled = value;
    }

    /**
     * Gets the value of the spellCorrectKnowledgeSearchEnabled property.
     * 
     */
    public boolean isSpellCorrectKnowledgeSearchEnabled() {
        return spellCorrectKnowledgeSearchEnabled;
    }

    /**
     * Sets the value of the spellCorrectKnowledgeSearchEnabled property.
     * 
     */
    public void setSpellCorrectKnowledgeSearchEnabled(boolean value) {
        this.spellCorrectKnowledgeSearchEnabled = value;
    }

}
