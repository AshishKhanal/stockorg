
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SymbolVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SymbolVisibility"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Global"/&gt;
 *     &lt;enumeration value="Public"/&gt;
 *     &lt;enumeration value="Private"/&gt;
 *     &lt;enumeration value="Protected"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SymbolVisibility")
@XmlEnum
public enum SymbolVisibility {

    @XmlEnumValue("Global")
    GLOBAL("Global"),
    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Protected")
    PROTECTED("Protected");
    private final String value;

    SymbolVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SymbolVisibility fromValue(String v) {
        for (SymbolVisibility c: SymbolVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
