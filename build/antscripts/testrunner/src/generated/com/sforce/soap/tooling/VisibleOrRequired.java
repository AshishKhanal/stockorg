
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VisibleOrRequired.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VisibleOrRequired"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="VisibleOptional"/&gt;
 *     &lt;enumeration value="VisibleRequired"/&gt;
 *     &lt;enumeration value="NotVisible"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VisibleOrRequired")
@XmlEnum
public enum VisibleOrRequired {

    @XmlEnumValue("VisibleOptional")
    VISIBLE_OPTIONAL("VisibleOptional"),
    @XmlEnumValue("VisibleRequired")
    VISIBLE_REQUIRED("VisibleRequired"),
    @XmlEnumValue("NotVisible")
    NOT_VISIBLE("NotVisible");
    private final String value;

    VisibleOrRequired(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VisibleOrRequired fromValue(String v) {
        for (VisibleOrRequired c: VisibleOrRequired.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
