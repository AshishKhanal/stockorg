
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableCascadeActivateToRelatedPrices" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableQuantitySchedule" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableRevenueSchedule" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductSettings", propOrder = {
    "enableCascadeActivateToRelatedPrices",
    "enableQuantitySchedule",
    "enableRevenueSchedule"
})
public class ProductSettings
    extends MetadataForSettings
{

    protected Boolean enableCascadeActivateToRelatedPrices;
    protected Boolean enableQuantitySchedule;
    protected Boolean enableRevenueSchedule;

    /**
     * Gets the value of the enableCascadeActivateToRelatedPrices property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCascadeActivateToRelatedPrices() {
        return enableCascadeActivateToRelatedPrices;
    }

    /**
     * Sets the value of the enableCascadeActivateToRelatedPrices property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCascadeActivateToRelatedPrices(Boolean value) {
        this.enableCascadeActivateToRelatedPrices = value;
    }

    /**
     * Gets the value of the enableQuantitySchedule property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableQuantitySchedule() {
        return enableQuantitySchedule;
    }

    /**
     * Sets the value of the enableQuantitySchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableQuantitySchedule(Boolean value) {
        this.enableQuantitySchedule = value;
    }

    /**
     * Gets the value of the enableRevenueSchedule property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRevenueSchedule() {
        return enableRevenueSchedule;
    }

    /**
     * Sets the value of the enableRevenueSchedule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRevenueSchedule(Boolean value) {
        this.enableRevenueSchedule = value;
    }

}
