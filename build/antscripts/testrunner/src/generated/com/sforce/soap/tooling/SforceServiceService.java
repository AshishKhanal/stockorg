package com.sforce.soap.tooling;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.10
 * 2019-05-16T17:03:00.441+05:45
 * Generated source version: 3.1.10
 * 
 */
@WebServiceClient(name = "SforceServiceService", 
                  wsdlLocation = "file:/D:/sqx/build/antscripts/testrunner/src/resources/tooling.wsdl",
                  targetNamespace = "urn:tooling.soap.sforce.com") 
public class SforceServiceService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("urn:tooling.soap.sforce.com", "SforceServiceService");
    public final static QName SforceService = new QName("urn:tooling.soap.sforce.com", "SforceService");
    static {
        URL url = null;
        try {
            url = new URL("file:/D:/sqx/build/antscripts/testrunner/src/resources/tooling.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(SforceServiceService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/D:/sqx/build/antscripts/testrunner/src/resources/tooling.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public SforceServiceService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public SforceServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SforceServiceService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public SforceServiceService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public SforceServiceService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public SforceServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns SforceServicePortType
     */
    @WebEndpoint(name = "SforceService")
    public SforceServicePortType getSforceService() {
        return super.getPort(SforceService, SforceServicePortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SforceServicePortType
     */
    @WebEndpoint(name = "SforceService")
    public SforceServicePortType getSforceService(WebServiceFeature... features) {
        return super.getPort(SforceService, SforceServicePortType.class, features);
    }

}
