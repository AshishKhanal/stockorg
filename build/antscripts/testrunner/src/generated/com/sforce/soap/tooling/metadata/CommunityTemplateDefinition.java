
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.CommunityTemplateCategory;


/**
 * <p>Java class for CommunityTemplateDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunityTemplateDefinition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bundlesInfo" type="{urn:metadata.tooling.soap.sforce.com}CommunityTemplateBundleInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="category" type="{urn:tooling.soap.sforce.com}CommunityTemplateCategory"/&gt;
 *         &lt;element name="defaultThemeDefinition" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="enableExtendedCleanUpOnDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="pageSetting" type="{urn:metadata.tooling.soap.sforce.com}CommunityTemplatePageSetting" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunityTemplateDefinition", propOrder = {
    "bundlesInfo",
    "category",
    "defaultThemeDefinition",
    "description",
    "enableExtendedCleanUpOnDelete",
    "masterLabel",
    "pageSetting"
})
public class CommunityTemplateDefinition
    extends Metadata
{

    protected List<CommunityTemplateBundleInfo> bundlesInfo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected CommunityTemplateCategory category;
    @XmlElement(required = true)
    protected String defaultThemeDefinition;
    protected String description;
    protected Boolean enableExtendedCleanUpOnDelete;
    @XmlElement(required = true)
    protected String masterLabel;
    protected List<CommunityTemplatePageSetting> pageSetting;

    /**
     * Gets the value of the bundlesInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bundlesInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBundlesInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunityTemplateBundleInfo }
     * 
     * 
     */
    public List<CommunityTemplateBundleInfo> getBundlesInfo() {
        if (bundlesInfo == null) {
            bundlesInfo = new ArrayList<CommunityTemplateBundleInfo>();
        }
        return this.bundlesInfo;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link CommunityTemplateCategory }
     *     
     */
    public CommunityTemplateCategory getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunityTemplateCategory }
     *     
     */
    public void setCategory(CommunityTemplateCategory value) {
        this.category = value;
    }

    /**
     * Gets the value of the defaultThemeDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultThemeDefinition() {
        return defaultThemeDefinition;
    }

    /**
     * Sets the value of the defaultThemeDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultThemeDefinition(String value) {
        this.defaultThemeDefinition = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the enableExtendedCleanUpOnDelete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExtendedCleanUpOnDelete() {
        return enableExtendedCleanUpOnDelete;
    }

    /**
     * Sets the value of the enableExtendedCleanUpOnDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExtendedCleanUpOnDelete(Boolean value) {
        this.enableExtendedCleanUpOnDelete = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the pageSetting property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pageSetting property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPageSetting().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunityTemplatePageSetting }
     * 
     * 
     */
    public List<CommunityTemplatePageSetting> getPageSetting() {
        if (pageSetting == null) {
            pageSetting = new ArrayList<CommunityTemplatePageSetting>();
        }
        return this.pageSetting;
    }

}
