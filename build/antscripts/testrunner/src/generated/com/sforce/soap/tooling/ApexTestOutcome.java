
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexTestOutcome.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexTestOutcome"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Pass"/&gt;
 *     &lt;enumeration value="Fail"/&gt;
 *     &lt;enumeration value="CompileFail"/&gt;
 *     &lt;enumeration value="Skip"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexTestOutcome")
@XmlEnum
public enum ApexTestOutcome {

    @XmlEnumValue("Pass")
    PASS("Pass"),
    @XmlEnumValue("Fail")
    FAIL("Fail"),
    @XmlEnumValue("CompileFail")
    COMPILE_FAIL("CompileFail"),
    @XmlEnumValue("Skip")
    SKIP("Skip");
    private final String value;

    ApexTestOutcome(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexTestOutcome fromValue(String v) {
        for (ApexTestOutcome c: ApexTestOutcome.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
