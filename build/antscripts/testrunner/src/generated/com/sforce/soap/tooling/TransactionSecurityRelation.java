
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionSecurityRelation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionSecurityRelation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MatchAll"/&gt;
 *     &lt;enumeration value="MatchAny"/&gt;
 *     &lt;enumeration value="MatchNone"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionSecurityRelation")
@XmlEnum
public enum TransactionSecurityRelation {

    @XmlEnumValue("MatchAll")
    MATCH_ALL("MatchAll"),
    @XmlEnumValue("MatchAny")
    MATCH_ANY("MatchAny"),
    @XmlEnumValue("MatchNone")
    MATCH_NONE("MatchNone");
    private final String value;

    TransactionSecurityRelation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionSecurityRelation fromValue(String v) {
        for (TransactionSecurityRelation c: TransactionSecurityRelation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
