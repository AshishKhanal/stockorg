
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexLogLocation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexLogLocation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Monitoring"/&gt;
 *     &lt;enumeration value="Profiling"/&gt;
 *     &lt;enumeration value="SystemLog"/&gt;
 *     &lt;enumeration value="HeapDump"/&gt;
 *     &lt;enumeration value="Preserved"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexLogLocation")
@XmlEnum
public enum ApexLogLocation {

    @XmlEnumValue("Monitoring")
    MONITORING("Monitoring"),
    @XmlEnumValue("Profiling")
    PROFILING("Profiling"),
    @XmlEnumValue("SystemLog")
    SYSTEM_LOG("SystemLog"),
    @XmlEnumValue("HeapDump")
    HEAP_DUMP("HeapDump"),
    @XmlEnumValue("Preserved")
    PRESERVED("Preserved");
    private final String value;

    ApexLogLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexLogLocation fromValue(String v) {
        for (ApexLogLocation c: ApexLogLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
