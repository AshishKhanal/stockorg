
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MappingOperation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MappingOperation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Autofill"/&gt;
 *     &lt;enumeration value="Overwrite"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MappingOperation")
@XmlEnum
public enum MappingOperation {

    @XmlEnumValue("Autofill")
    AUTOFILL("Autofill"),
    @XmlEnumValue("Overwrite")
    OVERWRITE("Overwrite");
    private final String value;

    MappingOperation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MappingOperation fromValue(String v) {
        for (MappingOperation c: MappingOperation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
