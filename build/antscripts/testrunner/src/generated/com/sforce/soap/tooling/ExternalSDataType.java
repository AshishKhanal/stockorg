
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExternalSDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExternalSDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NumberType"/&gt;
 *     &lt;enumeration value="DoubleType"/&gt;
 *     &lt;enumeration value="StringType"/&gt;
 *     &lt;enumeration value="BooleanType"/&gt;
 *     &lt;enumeration value="DatetimeType"/&gt;
 *     &lt;enumeration value="ComplexType"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ExternalSDataType")
@XmlEnum
public enum ExternalSDataType {

    @XmlEnumValue("NumberType")
    NUMBER_TYPE("NumberType"),
    @XmlEnumValue("DoubleType")
    DOUBLE_TYPE("DoubleType"),
    @XmlEnumValue("StringType")
    STRING_TYPE("StringType"),
    @XmlEnumValue("BooleanType")
    BOOLEAN_TYPE("BooleanType"),
    @XmlEnumValue("DatetimeType")
    DATETIME_TYPE("DatetimeType"),
    @XmlEnumValue("ComplexType")
    COMPLEX_TYPE("ComplexType");
    private final String value;

    ExternalSDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExternalSDataType fromValue(String v) {
        for (ExternalSDataType c: ExternalSDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
