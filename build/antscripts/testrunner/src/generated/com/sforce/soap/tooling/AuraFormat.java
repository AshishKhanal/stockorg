
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuraFormat.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuraFormat"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="XML"/&gt;
 *     &lt;enumeration value="JS"/&gt;
 *     &lt;enumeration value="CSS"/&gt;
 *     &lt;enumeration value="TEMPLATE_CSS"/&gt;
 *     &lt;enumeration value="SVG"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AuraFormat")
@XmlEnum
public enum AuraFormat {

    XML,
    JS,
    CSS,
    TEMPLATE_CSS,
    SVG;

    public String value() {
        return name();
    }

    public static AuraFormat fromValue(String v) {
        return valueOf(v);
    }

}
