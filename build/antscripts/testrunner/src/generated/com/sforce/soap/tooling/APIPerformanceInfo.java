
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="encodedIntervalTimerTree" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="handlerMetrics" type="{urn:tooling.soap.sforce.com}NameValuePair" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "encodedIntervalTimerTree",
    "handlerMetrics"
})
@XmlRootElement(name = "APIPerformanceInfo")
public class APIPerformanceInfo {

    @XmlElement(required = true)
    protected String encodedIntervalTimerTree;
    protected List<NameValuePair> handlerMetrics;

    /**
     * Gets the value of the encodedIntervalTimerTree property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncodedIntervalTimerTree() {
        return encodedIntervalTimerTree;
    }

    /**
     * Sets the value of the encodedIntervalTimerTree property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncodedIntervalTimerTree(String value) {
        this.encodedIntervalTimerTree = value;
    }

    /**
     * Gets the value of the handlerMetrics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the handlerMetrics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHandlerMetrics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameValuePair }
     * 
     * 
     */
    public List<NameValuePair> getHandlerMetrics() {
        if (handlerMetrics == null) {
            handlerMetrics = new ArrayList<NameValuePair>();
        }
        return this.handlerMetrics;
    }

}
