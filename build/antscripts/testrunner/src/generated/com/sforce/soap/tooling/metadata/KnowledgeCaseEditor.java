
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeCaseEditor.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="KnowledgeCaseEditor"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="simple"/&gt;
 *     &lt;enumeration value="standard"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "KnowledgeCaseEditor")
@XmlEnum
public enum KnowledgeCaseEditor {

    @XmlEnumValue("simple")
    SIMPLE("simple"),
    @XmlEnumValue("standard")
    STANDARD("standard");
    private final String value;

    KnowledgeCaseEditor(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static KnowledgeCaseEditor fromValue(String v) {
        for (KnowledgeCaseEditor c: KnowledgeCaseEditor.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
