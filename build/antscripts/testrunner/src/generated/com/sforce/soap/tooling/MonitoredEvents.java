
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MonitoredEvents.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MonitoredEvents"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AuditTrail"/&gt;
 *     &lt;enumeration value="Login"/&gt;
 *     &lt;enumeration value="Entity"/&gt;
 *     &lt;enumeration value="DataExport"/&gt;
 *     &lt;enumeration value="AccessResource"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MonitoredEvents")
@XmlEnum
public enum MonitoredEvents {

    @XmlEnumValue("AuditTrail")
    AUDIT_TRAIL("AuditTrail"),
    @XmlEnumValue("Login")
    LOGIN("Login"),
    @XmlEnumValue("Entity")
    ENTITY("Entity"),
    @XmlEnumValue("DataExport")
    DATA_EXPORT("DataExport"),
    @XmlEnumValue("AccessResource")
    ACCESS_RESOURCE("AccessResource");
    private final String value;

    MonitoredEvents(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MonitoredEvents fromValue(String v) {
        for (MonitoredEvents c: MonitoredEvents.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
