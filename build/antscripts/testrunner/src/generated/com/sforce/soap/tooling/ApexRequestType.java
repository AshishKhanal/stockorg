
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexRequestType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BATCH_APEX"/&gt;
 *     &lt;enumeration value="FUTURE"/&gt;
 *     &lt;enumeration value="SCHEDULED"/&gt;
 *     &lt;enumeration value="SYNCHRONOUS"/&gt;
 *     &lt;enumeration value="RUN_TESTS_ASYNCHRONOUS"/&gt;
 *     &lt;enumeration value="RUN_TESTS_SYNCHRONOUS"/&gt;
 *     &lt;enumeration value="RUN_TESTS_DEPLOY"/&gt;
 *     &lt;enumeration value="VISUALFORCE"/&gt;
 *     &lt;enumeration value="QUEUEABLE"/&gt;
 *     &lt;enumeration value="REMOTE_ACTION"/&gt;
 *     &lt;enumeration value="LIGHTNING"/&gt;
 *     &lt;enumeration value="QUICK_ACTION"/&gt;
 *     &lt;enumeration value="SOAP"/&gt;
 *     &lt;enumeration value="REST"/&gt;
 *     &lt;enumeration value="INVOCABLE_ACTION"/&gt;
 *     &lt;enumeration value="EXECUTE_ANONYMOUS"/&gt;
 *     &lt;enumeration value="INBOUND_EMAIL_SERVICE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexRequestType")
@XmlEnum
public enum ApexRequestType {

    BATCH_APEX,
    FUTURE,
    SCHEDULED,
    SYNCHRONOUS,
    RUN_TESTS_ASYNCHRONOUS,
    RUN_TESTS_SYNCHRONOUS,
    RUN_TESTS_DEPLOY,
    VISUALFORCE,
    QUEUEABLE,
    REMOTE_ACTION,
    LIGHTNING,
    QUICK_ACTION,
    SOAP,
    REST,
    INVOCABLE_ACTION,
    EXECUTE_ANONYMOUS,
    INBOUND_EMAIL_SERVICE;

    public String value() {
        return name();
    }

    public static ApexRequestType fromValue(String v) {
        return valueOf(v);
    }

}
