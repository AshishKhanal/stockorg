
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkflowActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WorkflowActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="FieldUpdate"/&gt;
 *     &lt;enumeration value="KnowledgePublish"/&gt;
 *     &lt;enumeration value="Task"/&gt;
 *     &lt;enumeration value="Alert"/&gt;
 *     &lt;enumeration value="Send"/&gt;
 *     &lt;enumeration value="OutboundMessage"/&gt;
 *     &lt;enumeration value="FlowAction"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WorkflowActionType")
@XmlEnum
public enum WorkflowActionType {

    @XmlEnumValue("FieldUpdate")
    FIELD_UPDATE("FieldUpdate"),
    @XmlEnumValue("KnowledgePublish")
    KNOWLEDGE_PUBLISH("KnowledgePublish"),
    @XmlEnumValue("Task")
    TASK("Task"),
    @XmlEnumValue("Alert")
    ALERT("Alert"),
    @XmlEnumValue("Send")
    SEND("Send"),
    @XmlEnumValue("OutboundMessage")
    OUTBOUND_MESSAGE("OutboundMessage"),
    @XmlEnumValue("FlowAction")
    FLOW_ACTION("FlowAction");
    private final String value;

    WorkflowActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WorkflowActionType fromValue(String v) {
        for (WorkflowActionType c: WorkflowActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
