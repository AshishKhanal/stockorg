
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerKilledBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerKilledBy"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NoOne"/&gt;
 *     &lt;enumeration value="Debugger"/&gt;
 *     &lt;enumeration value="Metadata"/&gt;
 *     &lt;enumeration value="BT"/&gt;
 *     &lt;enumeration value="OrgAdmin"/&gt;
 *     &lt;enumeration value="Sweeper"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerKilledBy")
@XmlEnum
public enum ApexDebuggerKilledBy {

    @XmlEnumValue("NoOne")
    NO_ONE("NoOne"),
    @XmlEnumValue("Debugger")
    DEBUGGER("Debugger"),
    @XmlEnumValue("Metadata")
    METADATA("Metadata"),
    BT("BT"),
    @XmlEnumValue("OrgAdmin")
    ORG_ADMIN("OrgAdmin"),
    @XmlEnumValue("Sweeper")
    SWEEPER("Sweeper");
    private final String value;

    ApexDebuggerKilledBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerKilledBy fromValue(String v) {
        for (ApexDebuggerKilledBy c: ApexDebuggerKilledBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
