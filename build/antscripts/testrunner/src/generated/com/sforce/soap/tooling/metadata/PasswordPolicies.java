
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PasswordPolicies complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PasswordPolicies"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="apiOnlyUserHomePageURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="complexity" type="{urn:metadata.tooling.soap.sforce.com}Complexity" minOccurs="0"/&gt;
 *         &lt;element name="expiration" type="{urn:metadata.tooling.soap.sforce.com}Expiration" minOccurs="0"/&gt;
 *         &lt;element name="historyRestriction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lockoutInterval" type="{urn:metadata.tooling.soap.sforce.com}LockoutInterval" minOccurs="0"/&gt;
 *         &lt;element name="maxLoginAttempts" type="{urn:metadata.tooling.soap.sforce.com}MaxLoginAttempts" minOccurs="0"/&gt;
 *         &lt;element name="minimumPasswordLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="minimumPasswordLifetime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="obscureSecretAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="passwordAssistanceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="passwordAssistanceURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="questionRestriction" type="{urn:metadata.tooling.soap.sforce.com}QuestionRestriction" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PasswordPolicies", propOrder = {
    "apiOnlyUserHomePageURL",
    "complexity",
    "expiration",
    "historyRestriction",
    "lockoutInterval",
    "maxLoginAttempts",
    "minimumPasswordLength",
    "minimumPasswordLifetime",
    "obscureSecretAnswer",
    "passwordAssistanceMessage",
    "passwordAssistanceURL",
    "questionRestriction"
})
public class PasswordPolicies {

    protected String apiOnlyUserHomePageURL;
    @XmlSchemaType(name = "string")
    protected Complexity complexity;
    @XmlSchemaType(name = "string")
    protected Expiration expiration;
    protected String historyRestriction;
    @XmlSchemaType(name = "string")
    protected LockoutInterval lockoutInterval;
    @XmlSchemaType(name = "string")
    protected MaxLoginAttempts maxLoginAttempts;
    protected String minimumPasswordLength;
    protected Boolean minimumPasswordLifetime;
    protected Boolean obscureSecretAnswer;
    protected String passwordAssistanceMessage;
    protected String passwordAssistanceURL;
    @XmlSchemaType(name = "string")
    protected QuestionRestriction questionRestriction;

    /**
     * Gets the value of the apiOnlyUserHomePageURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApiOnlyUserHomePageURL() {
        return apiOnlyUserHomePageURL;
    }

    /**
     * Sets the value of the apiOnlyUserHomePageURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApiOnlyUserHomePageURL(String value) {
        this.apiOnlyUserHomePageURL = value;
    }

    /**
     * Gets the value of the complexity property.
     * 
     * @return
     *     possible object is
     *     {@link Complexity }
     *     
     */
    public Complexity getComplexity() {
        return complexity;
    }

    /**
     * Sets the value of the complexity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Complexity }
     *     
     */
    public void setComplexity(Complexity value) {
        this.complexity = value;
    }

    /**
     * Gets the value of the expiration property.
     * 
     * @return
     *     possible object is
     *     {@link Expiration }
     *     
     */
    public Expiration getExpiration() {
        return expiration;
    }

    /**
     * Sets the value of the expiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Expiration }
     *     
     */
    public void setExpiration(Expiration value) {
        this.expiration = value;
    }

    /**
     * Gets the value of the historyRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoryRestriction() {
        return historyRestriction;
    }

    /**
     * Sets the value of the historyRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoryRestriction(String value) {
        this.historyRestriction = value;
    }

    /**
     * Gets the value of the lockoutInterval property.
     * 
     * @return
     *     possible object is
     *     {@link LockoutInterval }
     *     
     */
    public LockoutInterval getLockoutInterval() {
        return lockoutInterval;
    }

    /**
     * Sets the value of the lockoutInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link LockoutInterval }
     *     
     */
    public void setLockoutInterval(LockoutInterval value) {
        this.lockoutInterval = value;
    }

    /**
     * Gets the value of the maxLoginAttempts property.
     * 
     * @return
     *     possible object is
     *     {@link MaxLoginAttempts }
     *     
     */
    public MaxLoginAttempts getMaxLoginAttempts() {
        return maxLoginAttempts;
    }

    /**
     * Sets the value of the maxLoginAttempts property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaxLoginAttempts }
     *     
     */
    public void setMaxLoginAttempts(MaxLoginAttempts value) {
        this.maxLoginAttempts = value;
    }

    /**
     * Gets the value of the minimumPasswordLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumPasswordLength() {
        return minimumPasswordLength;
    }

    /**
     * Sets the value of the minimumPasswordLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumPasswordLength(String value) {
        this.minimumPasswordLength = value;
    }

    /**
     * Gets the value of the minimumPasswordLifetime property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMinimumPasswordLifetime() {
        return minimumPasswordLifetime;
    }

    /**
     * Sets the value of the minimumPasswordLifetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMinimumPasswordLifetime(Boolean value) {
        this.minimumPasswordLifetime = value;
    }

    /**
     * Gets the value of the obscureSecretAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isObscureSecretAnswer() {
        return obscureSecretAnswer;
    }

    /**
     * Sets the value of the obscureSecretAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setObscureSecretAnswer(Boolean value) {
        this.obscureSecretAnswer = value;
    }

    /**
     * Gets the value of the passwordAssistanceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordAssistanceMessage() {
        return passwordAssistanceMessage;
    }

    /**
     * Sets the value of the passwordAssistanceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordAssistanceMessage(String value) {
        this.passwordAssistanceMessage = value;
    }

    /**
     * Gets the value of the passwordAssistanceURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPasswordAssistanceURL() {
        return passwordAssistanceURL;
    }

    /**
     * Sets the value of the passwordAssistanceURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPasswordAssistanceURL(String value) {
        this.passwordAssistanceURL = value;
    }

    /**
     * Gets the value of the questionRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link QuestionRestriction }
     *     
     */
    public QuestionRestriction getQuestionRestriction() {
        return questionRestriction;
    }

    /**
     * Sets the value of the questionRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionRestriction }
     *     
     */
    public void setQuestionRestriction(QuestionRestriction value) {
        this.questionRestriction = value;
    }

}
