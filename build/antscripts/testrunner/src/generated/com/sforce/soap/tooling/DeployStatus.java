
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeployStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DeployStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Pending"/&gt;
 *     &lt;enumeration value="InProgress"/&gt;
 *     &lt;enumeration value="Succeeded"/&gt;
 *     &lt;enumeration value="SucceededPartial"/&gt;
 *     &lt;enumeration value="Failed"/&gt;
 *     &lt;enumeration value="Canceling"/&gt;
 *     &lt;enumeration value="Canceled"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DeployStatus")
@XmlEnum
public enum DeployStatus {

    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("InProgress")
    IN_PROGRESS("InProgress"),
    @XmlEnumValue("Succeeded")
    SUCCEEDED("Succeeded"),
    @XmlEnumValue("SucceededPartial")
    SUCCEEDED_PARTIAL("SucceededPartial"),
    @XmlEnumValue("Failed")
    FAILED("Failed"),
    @XmlEnumValue("Canceling")
    CANCELING("Canceling"),
    @XmlEnumValue("Canceled")
    CANCELED("Canceled");
    private final String value;

    DeployStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeployStatus fromValue(String v) {
        for (DeployStatus c: DeployStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
