
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailTemplateStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailTemplateStyle"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="none"/&gt;
 *     &lt;enumeration value="freeForm"/&gt;
 *     &lt;enumeration value="formalLetter"/&gt;
 *     &lt;enumeration value="promotionRight"/&gt;
 *     &lt;enumeration value="promotionLeft"/&gt;
 *     &lt;enumeration value="newsletter"/&gt;
 *     &lt;enumeration value="products"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EmailTemplateStyle")
@XmlEnum
public enum EmailTemplateStyle {

    @XmlEnumValue("none")
    NONE("none"),
    @XmlEnumValue("freeForm")
    FREE_FORM("freeForm"),
    @XmlEnumValue("formalLetter")
    FORMAL_LETTER("formalLetter"),
    @XmlEnumValue("promotionRight")
    PROMOTION_RIGHT("promotionRight"),
    @XmlEnumValue("promotionLeft")
    PROMOTION_LEFT("promotionLeft"),
    @XmlEnumValue("newsletter")
    NEWSLETTER("newsletter"),
    @XmlEnumValue("products")
    PRODUCTS("products");
    private final String value;

    EmailTemplateStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailTemplateStyle fromValue(String v) {
        for (EmailTemplateStyle c: EmailTemplateStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
