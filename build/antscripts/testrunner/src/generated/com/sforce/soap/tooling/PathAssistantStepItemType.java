
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PathAssistantStepItemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PathAssistantStepItemType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Layout"/&gt;
 *     &lt;enumeration value="Information"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PathAssistantStepItemType")
@XmlEnum
public enum PathAssistantStepItemType {

    @XmlEnumValue("Layout")
    LAYOUT("Layout"),
    @XmlEnumValue("Information")
    INFORMATION("Information");
    private final String value;

    PathAssistantStepItemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PathAssistantStepItemType fromValue(String v) {
        for (PathAssistantStepItemType c: PathAssistantStepItemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
