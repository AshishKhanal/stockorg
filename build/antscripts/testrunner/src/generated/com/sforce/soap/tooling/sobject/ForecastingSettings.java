
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForecastingSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastingSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdjustmentsSettings" type="{urn:sobject.tooling.soap.sforce.com}AdjustmentsSettings" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentsSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DataSourceSettings" type="{urn:sobject.tooling.soap.sforce.com}DataSourceSettings" minOccurs="0"/&gt;
 *         &lt;element name="DataSourceSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DisplayCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ForecastRangeSettings" type="{urn:sobject.tooling.soap.sforce.com}ForecastRangeSettings" minOccurs="0"/&gt;
 *         &lt;element name="ForecastRangeSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ForecastingCategoryMappings" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsForecastsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}ForecastingSettings" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsSelectedSettings" type="{urn:sobject.tooling.soap.sforce.com}OpportunityListFieldsSelectedSettings" minOccurs="0"/&gt;
 *         &lt;element name="OpportunityListFieldsSelectedSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuotasSettings" type="{urn:sobject.tooling.soap.sforce.com}QuotasSettings" minOccurs="0"/&gt;
 *         &lt;element name="QuotasSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastingSettings", propOrder = {
    "adjustmentsSettings",
    "adjustmentsSettingsId",
    "dataSourceSettings",
    "dataSourceSettingsId",
    "displayCurrency",
    "durableId",
    "forecastRangeSettings",
    "forecastRangeSettingsId",
    "forecastingCategoryMappings",
    "fullName",
    "isForecastsEnabled",
    "metadata",
    "opportunityListFieldsSelectedSettings",
    "opportunityListFieldsSelectedSettingsId",
    "quotasSettings",
    "quotasSettingsId"
})
public class ForecastingSettings
    extends SObject
{

    @XmlElementRef(name = "AdjustmentsSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<AdjustmentsSettings> adjustmentsSettings;
    @XmlElementRef(name = "AdjustmentsSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> adjustmentsSettingsId;
    @XmlElementRef(name = "DataSourceSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DataSourceSettings> dataSourceSettings;
    @XmlElementRef(name = "DataSourceSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dataSourceSettingsId;
    @XmlElementRef(name = "DisplayCurrency", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> displayCurrency;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "ForecastRangeSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<ForecastRangeSettings> forecastRangeSettings;
    @XmlElementRef(name = "ForecastRangeSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forecastRangeSettingsId;
    @XmlElementRef(name = "ForecastingCategoryMappings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> forecastingCategoryMappings;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsForecastsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isForecastsEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.ForecastingSettings> metadata;
    @XmlElementRef(name = "OpportunityListFieldsSelectedSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<OpportunityListFieldsSelectedSettings> opportunityListFieldsSelectedSettings;
    @XmlElementRef(name = "OpportunityListFieldsSelectedSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> opportunityListFieldsSelectedSettingsId;
    @XmlElementRef(name = "QuotasSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QuotasSettings> quotasSettings;
    @XmlElementRef(name = "QuotasSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quotasSettingsId;

    /**
     * Gets the value of the adjustmentsSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AdjustmentsSettings }{@code >}
     *     
     */
    public JAXBElement<AdjustmentsSettings> getAdjustmentsSettings() {
        return adjustmentsSettings;
    }

    /**
     * Sets the value of the adjustmentsSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AdjustmentsSettings }{@code >}
     *     
     */
    public void setAdjustmentsSettings(JAXBElement<AdjustmentsSettings> value) {
        this.adjustmentsSettings = value;
    }

    /**
     * Gets the value of the adjustmentsSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAdjustmentsSettingsId() {
        return adjustmentsSettingsId;
    }

    /**
     * Sets the value of the adjustmentsSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAdjustmentsSettingsId(JAXBElement<String> value) {
        this.adjustmentsSettingsId = value;
    }

    /**
     * Gets the value of the dataSourceSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DataSourceSettings }{@code >}
     *     
     */
    public JAXBElement<DataSourceSettings> getDataSourceSettings() {
        return dataSourceSettings;
    }

    /**
     * Sets the value of the dataSourceSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DataSourceSettings }{@code >}
     *     
     */
    public void setDataSourceSettings(JAXBElement<DataSourceSettings> value) {
        this.dataSourceSettings = value;
    }

    /**
     * Gets the value of the dataSourceSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDataSourceSettingsId() {
        return dataSourceSettingsId;
    }

    /**
     * Sets the value of the dataSourceSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDataSourceSettingsId(JAXBElement<String> value) {
        this.dataSourceSettingsId = value;
    }

    /**
     * Gets the value of the displayCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisplayCurrency() {
        return displayCurrency;
    }

    /**
     * Sets the value of the displayCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisplayCurrency(JAXBElement<String> value) {
        this.displayCurrency = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the forecastRangeSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ForecastRangeSettings }{@code >}
     *     
     */
    public JAXBElement<ForecastRangeSettings> getForecastRangeSettings() {
        return forecastRangeSettings;
    }

    /**
     * Sets the value of the forecastRangeSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ForecastRangeSettings }{@code >}
     *     
     */
    public void setForecastRangeSettings(JAXBElement<ForecastRangeSettings> value) {
        this.forecastRangeSettings = value;
    }

    /**
     * Gets the value of the forecastRangeSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForecastRangeSettingsId() {
        return forecastRangeSettingsId;
    }

    /**
     * Sets the value of the forecastRangeSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForecastRangeSettingsId(JAXBElement<String> value) {
        this.forecastRangeSettingsId = value;
    }

    /**
     * Gets the value of the forecastingCategoryMappings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getForecastingCategoryMappings() {
        return forecastingCategoryMappings;
    }

    /**
     * Sets the value of the forecastingCategoryMappings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setForecastingCategoryMappings(JAXBElement<String> value) {
        this.forecastingCategoryMappings = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isForecastsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsForecastsEnabled() {
        return isForecastsEnabled;
    }

    /**
     * Sets the value of the isForecastsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsForecastsEnabled(JAXBElement<Boolean> value) {
        this.isForecastsEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ForecastingSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.ForecastingSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ForecastingSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.ForecastingSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsSelectedSettings }{@code >}
     *     
     */
    public JAXBElement<OpportunityListFieldsSelectedSettings> getOpportunityListFieldsSelectedSettings() {
        return opportunityListFieldsSelectedSettings;
    }

    /**
     * Sets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OpportunityListFieldsSelectedSettings }{@code >}
     *     
     */
    public void setOpportunityListFieldsSelectedSettings(JAXBElement<OpportunityListFieldsSelectedSettings> value) {
        this.opportunityListFieldsSelectedSettings = value;
    }

    /**
     * Gets the value of the opportunityListFieldsSelectedSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOpportunityListFieldsSelectedSettingsId() {
        return opportunityListFieldsSelectedSettingsId;
    }

    /**
     * Sets the value of the opportunityListFieldsSelectedSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOpportunityListFieldsSelectedSettingsId(JAXBElement<String> value) {
        this.opportunityListFieldsSelectedSettingsId = value;
    }

    /**
     * Gets the value of the quotasSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QuotasSettings }{@code >}
     *     
     */
    public JAXBElement<QuotasSettings> getQuotasSettings() {
        return quotasSettings;
    }

    /**
     * Sets the value of the quotasSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QuotasSettings }{@code >}
     *     
     */
    public void setQuotasSettings(JAXBElement<QuotasSettings> value) {
        this.quotasSettings = value;
    }

    /**
     * Gets the value of the quotasSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuotasSettingsId() {
        return quotasSettingsId;
    }

    /**
     * Sets the value of the quotasSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuotasSettingsId(JAXBElement<String> value) {
        this.quotasSettingsId = value;
    }

}
