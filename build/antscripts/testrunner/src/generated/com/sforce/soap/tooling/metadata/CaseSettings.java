
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="caseAssignNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caseCloseNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caseCommentNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caseCreateNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caseFeedItemSettings" type="{urn:metadata.tooling.soap.sforce.com}FeedItemSettings" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="closeCaseThroughStatusChange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="defaultCaseOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultCaseOwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultCaseUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailActionDefaultsHandlerClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailToCase" type="{urn:metadata.tooling.soap.sforce.com}EmailToCaseSettings" minOccurs="0"/&gt;
 *         &lt;element name="enableCaseFeed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableDraftEmails" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableEarlyEscalationRuleTriggers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableEmailActionDefaultsHandler" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSuggestedArticlesApplication" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSuggestedArticlesCustomerPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSuggestedArticlesPartnerPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSuggestedSolutions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="keepRecordTypeOnAssignmentRule" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="notifyContactOnCaseComment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="notifyDefaultCaseOwner" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="notifyOwnerOnCaseComment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="notifyOwnerOnCaseOwnerChange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showFewerCloseActions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="systemUserEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="useSystemEmailAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="useSystemUserAsDefaultCaseUser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="webToCase" type="{urn:metadata.tooling.soap.sforce.com}WebToCaseSettings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseSettings", propOrder = {
    "caseAssignNotificationTemplate",
    "caseCloseNotificationTemplate",
    "caseCommentNotificationTemplate",
    "caseCreateNotificationTemplate",
    "caseFeedItemSettings",
    "closeCaseThroughStatusChange",
    "defaultCaseOwner",
    "defaultCaseOwnerType",
    "defaultCaseUser",
    "emailActionDefaultsHandlerClass",
    "emailToCase",
    "enableCaseFeed",
    "enableDraftEmails",
    "enableEarlyEscalationRuleTriggers",
    "enableEmailActionDefaultsHandler",
    "enableSuggestedArticlesApplication",
    "enableSuggestedArticlesCustomerPortal",
    "enableSuggestedArticlesPartnerPortal",
    "enableSuggestedSolutions",
    "keepRecordTypeOnAssignmentRule",
    "notifyContactOnCaseComment",
    "notifyDefaultCaseOwner",
    "notifyOwnerOnCaseComment",
    "notifyOwnerOnCaseOwnerChange",
    "showFewerCloseActions",
    "systemUserEmail",
    "useSystemEmailAddress",
    "useSystemUserAsDefaultCaseUser",
    "webToCase"
})
public class CaseSettings
    extends MetadataForSettings
{

    protected String caseAssignNotificationTemplate;
    protected String caseCloseNotificationTemplate;
    protected String caseCommentNotificationTemplate;
    protected String caseCreateNotificationTemplate;
    protected List<FeedItemSettings> caseFeedItemSettings;
    protected Boolean closeCaseThroughStatusChange;
    protected String defaultCaseOwner;
    protected String defaultCaseOwnerType;
    protected String defaultCaseUser;
    protected String emailActionDefaultsHandlerClass;
    protected EmailToCaseSettings emailToCase;
    protected Boolean enableCaseFeed;
    protected Boolean enableDraftEmails;
    protected Boolean enableEarlyEscalationRuleTriggers;
    protected Boolean enableEmailActionDefaultsHandler;
    protected Boolean enableSuggestedArticlesApplication;
    protected Boolean enableSuggestedArticlesCustomerPortal;
    protected Boolean enableSuggestedArticlesPartnerPortal;
    protected Boolean enableSuggestedSolutions;
    protected Boolean keepRecordTypeOnAssignmentRule;
    protected Boolean notifyContactOnCaseComment;
    protected Boolean notifyDefaultCaseOwner;
    protected Boolean notifyOwnerOnCaseComment;
    protected Boolean notifyOwnerOnCaseOwnerChange;
    protected Boolean showFewerCloseActions;
    protected String systemUserEmail;
    protected Boolean useSystemEmailAddress;
    protected Boolean useSystemUserAsDefaultCaseUser;
    protected WebToCaseSettings webToCase;

    /**
     * Gets the value of the caseAssignNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseAssignNotificationTemplate() {
        return caseAssignNotificationTemplate;
    }

    /**
     * Sets the value of the caseAssignNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseAssignNotificationTemplate(String value) {
        this.caseAssignNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCloseNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseCloseNotificationTemplate() {
        return caseCloseNotificationTemplate;
    }

    /**
     * Sets the value of the caseCloseNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseCloseNotificationTemplate(String value) {
        this.caseCloseNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCommentNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseCommentNotificationTemplate() {
        return caseCommentNotificationTemplate;
    }

    /**
     * Sets the value of the caseCommentNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseCommentNotificationTemplate(String value) {
        this.caseCommentNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCreateNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseCreateNotificationTemplate() {
        return caseCreateNotificationTemplate;
    }

    /**
     * Sets the value of the caseCreateNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseCreateNotificationTemplate(String value) {
        this.caseCreateNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseFeedItemSettings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the caseFeedItemSettings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCaseFeedItemSettings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeedItemSettings }
     * 
     * 
     */
    public List<FeedItemSettings> getCaseFeedItemSettings() {
        if (caseFeedItemSettings == null) {
            caseFeedItemSettings = new ArrayList<FeedItemSettings>();
        }
        return this.caseFeedItemSettings;
    }

    /**
     * Gets the value of the closeCaseThroughStatusChange property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCloseCaseThroughStatusChange() {
        return closeCaseThroughStatusChange;
    }

    /**
     * Sets the value of the closeCaseThroughStatusChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCloseCaseThroughStatusChange(Boolean value) {
        this.closeCaseThroughStatusChange = value;
    }

    /**
     * Gets the value of the defaultCaseOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCaseOwner() {
        return defaultCaseOwner;
    }

    /**
     * Sets the value of the defaultCaseOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCaseOwner(String value) {
        this.defaultCaseOwner = value;
    }

    /**
     * Gets the value of the defaultCaseOwnerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCaseOwnerType() {
        return defaultCaseOwnerType;
    }

    /**
     * Sets the value of the defaultCaseOwnerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCaseOwnerType(String value) {
        this.defaultCaseOwnerType = value;
    }

    /**
     * Gets the value of the defaultCaseUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCaseUser() {
        return defaultCaseUser;
    }

    /**
     * Sets the value of the defaultCaseUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCaseUser(String value) {
        this.defaultCaseUser = value;
    }

    /**
     * Gets the value of the emailActionDefaultsHandlerClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailActionDefaultsHandlerClass() {
        return emailActionDefaultsHandlerClass;
    }

    /**
     * Sets the value of the emailActionDefaultsHandlerClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailActionDefaultsHandlerClass(String value) {
        this.emailActionDefaultsHandlerClass = value;
    }

    /**
     * Gets the value of the emailToCase property.
     * 
     * @return
     *     possible object is
     *     {@link EmailToCaseSettings }
     *     
     */
    public EmailToCaseSettings getEmailToCase() {
        return emailToCase;
    }

    /**
     * Sets the value of the emailToCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailToCaseSettings }
     *     
     */
    public void setEmailToCase(EmailToCaseSettings value) {
        this.emailToCase = value;
    }

    /**
     * Gets the value of the enableCaseFeed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCaseFeed() {
        return enableCaseFeed;
    }

    /**
     * Sets the value of the enableCaseFeed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCaseFeed(Boolean value) {
        this.enableCaseFeed = value;
    }

    /**
     * Gets the value of the enableDraftEmails property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDraftEmails() {
        return enableDraftEmails;
    }

    /**
     * Sets the value of the enableDraftEmails property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDraftEmails(Boolean value) {
        this.enableDraftEmails = value;
    }

    /**
     * Gets the value of the enableEarlyEscalationRuleTriggers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEarlyEscalationRuleTriggers() {
        return enableEarlyEscalationRuleTriggers;
    }

    /**
     * Sets the value of the enableEarlyEscalationRuleTriggers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEarlyEscalationRuleTriggers(Boolean value) {
        this.enableEarlyEscalationRuleTriggers = value;
    }

    /**
     * Gets the value of the enableEmailActionDefaultsHandler property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmailActionDefaultsHandler() {
        return enableEmailActionDefaultsHandler;
    }

    /**
     * Sets the value of the enableEmailActionDefaultsHandler property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmailActionDefaultsHandler(Boolean value) {
        this.enableEmailActionDefaultsHandler = value;
    }

    /**
     * Gets the value of the enableSuggestedArticlesApplication property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSuggestedArticlesApplication() {
        return enableSuggestedArticlesApplication;
    }

    /**
     * Sets the value of the enableSuggestedArticlesApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSuggestedArticlesApplication(Boolean value) {
        this.enableSuggestedArticlesApplication = value;
    }

    /**
     * Gets the value of the enableSuggestedArticlesCustomerPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSuggestedArticlesCustomerPortal() {
        return enableSuggestedArticlesCustomerPortal;
    }

    /**
     * Sets the value of the enableSuggestedArticlesCustomerPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSuggestedArticlesCustomerPortal(Boolean value) {
        this.enableSuggestedArticlesCustomerPortal = value;
    }

    /**
     * Gets the value of the enableSuggestedArticlesPartnerPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSuggestedArticlesPartnerPortal() {
        return enableSuggestedArticlesPartnerPortal;
    }

    /**
     * Sets the value of the enableSuggestedArticlesPartnerPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSuggestedArticlesPartnerPortal(Boolean value) {
        this.enableSuggestedArticlesPartnerPortal = value;
    }

    /**
     * Gets the value of the enableSuggestedSolutions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSuggestedSolutions() {
        return enableSuggestedSolutions;
    }

    /**
     * Sets the value of the enableSuggestedSolutions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSuggestedSolutions(Boolean value) {
        this.enableSuggestedSolutions = value;
    }

    /**
     * Gets the value of the keepRecordTypeOnAssignmentRule property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isKeepRecordTypeOnAssignmentRule() {
        return keepRecordTypeOnAssignmentRule;
    }

    /**
     * Sets the value of the keepRecordTypeOnAssignmentRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setKeepRecordTypeOnAssignmentRule(Boolean value) {
        this.keepRecordTypeOnAssignmentRule = value;
    }

    /**
     * Gets the value of the notifyContactOnCaseComment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyContactOnCaseComment() {
        return notifyContactOnCaseComment;
    }

    /**
     * Sets the value of the notifyContactOnCaseComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyContactOnCaseComment(Boolean value) {
        this.notifyContactOnCaseComment = value;
    }

    /**
     * Gets the value of the notifyDefaultCaseOwner property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyDefaultCaseOwner() {
        return notifyDefaultCaseOwner;
    }

    /**
     * Sets the value of the notifyDefaultCaseOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyDefaultCaseOwner(Boolean value) {
        this.notifyDefaultCaseOwner = value;
    }

    /**
     * Gets the value of the notifyOwnerOnCaseComment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyOwnerOnCaseComment() {
        return notifyOwnerOnCaseComment;
    }

    /**
     * Sets the value of the notifyOwnerOnCaseComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyOwnerOnCaseComment(Boolean value) {
        this.notifyOwnerOnCaseComment = value;
    }

    /**
     * Gets the value of the notifyOwnerOnCaseOwnerChange property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyOwnerOnCaseOwnerChange() {
        return notifyOwnerOnCaseOwnerChange;
    }

    /**
     * Sets the value of the notifyOwnerOnCaseOwnerChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyOwnerOnCaseOwnerChange(Boolean value) {
        this.notifyOwnerOnCaseOwnerChange = value;
    }

    /**
     * Gets the value of the showFewerCloseActions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowFewerCloseActions() {
        return showFewerCloseActions;
    }

    /**
     * Sets the value of the showFewerCloseActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowFewerCloseActions(Boolean value) {
        this.showFewerCloseActions = value;
    }

    /**
     * Gets the value of the systemUserEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemUserEmail() {
        return systemUserEmail;
    }

    /**
     * Sets the value of the systemUserEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemUserEmail(String value) {
        this.systemUserEmail = value;
    }

    /**
     * Gets the value of the useSystemEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemEmailAddress() {
        return useSystemEmailAddress;
    }

    /**
     * Sets the value of the useSystemEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemEmailAddress(Boolean value) {
        this.useSystemEmailAddress = value;
    }

    /**
     * Gets the value of the useSystemUserAsDefaultCaseUser property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSystemUserAsDefaultCaseUser() {
        return useSystemUserAsDefaultCaseUser;
    }

    /**
     * Sets the value of the useSystemUserAsDefaultCaseUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSystemUserAsDefaultCaseUser(Boolean value) {
        this.useSystemUserAsDefaultCaseUser = value;
    }

    /**
     * Gets the value of the webToCase property.
     * 
     * @return
     *     possible object is
     *     {@link WebToCaseSettings }
     *     
     */
    public WebToCaseSettings getWebToCase() {
        return webToCase;
    }

    /**
     * Sets the value of the webToCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebToCaseSettings }
     *     
     */
    public void setWebToCase(WebToCaseSettings value) {
        this.webToCase = value;
    }

}
