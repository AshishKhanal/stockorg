
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DevicePlatformType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DevicePlatformType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ios"/&gt;
 *     &lt;enumeration value="android"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DevicePlatformType")
@XmlEnum
public enum DevicePlatformType {

    @XmlEnumValue("ios")
    IOS("ios"),
    @XmlEnumValue("android")
    ANDROID("android");
    private final String value;

    DevicePlatformType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DevicePlatformType fromValue(String v) {
        for (DevicePlatformType c: DevicePlatformType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
