
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FeedItemType;
import com.sforce.soap.tooling.FeedLayoutFilterType;


/**
 * <p>Java class for FeedLayoutFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeedLayoutFilter"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="feedFilterName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="feedFilterType" type="{urn:tooling.soap.sforce.com}FeedLayoutFilterType"/&gt;
 *         &lt;element name="feedItemType" type="{urn:tooling.soap.sforce.com}FeedItemType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeedLayoutFilter", propOrder = {
    "feedFilterName",
    "feedFilterType",
    "feedItemType"
})
public class FeedLayoutFilter {

    protected String feedFilterName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FeedLayoutFilterType feedFilterType;
    @XmlSchemaType(name = "string")
    protected FeedItemType feedItemType;

    /**
     * Gets the value of the feedFilterName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeedFilterName() {
        return feedFilterName;
    }

    /**
     * Sets the value of the feedFilterName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeedFilterName(String value) {
        this.feedFilterName = value;
    }

    /**
     * Gets the value of the feedFilterType property.
     * 
     * @return
     *     possible object is
     *     {@link FeedLayoutFilterType }
     *     
     */
    public FeedLayoutFilterType getFeedFilterType() {
        return feedFilterType;
    }

    /**
     * Sets the value of the feedFilterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeedLayoutFilterType }
     *     
     */
    public void setFeedFilterType(FeedLayoutFilterType value) {
        this.feedFilterType = value;
    }

    /**
     * Gets the value of the feedItemType property.
     * 
     * @return
     *     possible object is
     *     {@link FeedItemType }
     *     
     */
    public FeedItemType getFeedItemType() {
        return feedItemType;
    }

    /**
     * Sets the value of the feedItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeedItemType }
     *     
     */
    public void setFeedItemType(FeedItemType value) {
        this.feedItemType = value;
    }

}
