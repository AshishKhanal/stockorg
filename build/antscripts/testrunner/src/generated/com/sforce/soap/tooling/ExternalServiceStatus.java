
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExternalServiceStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExternalServiceStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Incomplete"/&gt;
 *     &lt;enumeration value="Complete"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ExternalServiceStatus")
@XmlEnum
public enum ExternalServiceStatus {

    @XmlEnumValue("Incomplete")
    INCOMPLETE("Incomplete"),
    @XmlEnumValue("Complete")
    COMPLETE("Complete");
    private final String value;

    ExternalServiceStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ExternalServiceStatus fromValue(String v) {
        for (ExternalServiceStatus c: ExternalServiceStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
