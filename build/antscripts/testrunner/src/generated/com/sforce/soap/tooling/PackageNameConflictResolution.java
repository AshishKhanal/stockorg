
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PackageNameConflictResolution.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PackageNameConflictResolution"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="RenameAllForTest"/&gt;
 *     &lt;enumeration value="RenameMetadata"/&gt;
 *     &lt;enumeration value="Block"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PackageNameConflictResolution")
@XmlEnum
public enum PackageNameConflictResolution {

    @XmlEnumValue("RenameAllForTest")
    RENAME_ALL_FOR_TEST("RenameAllForTest"),
    @XmlEnumValue("RenameMetadata")
    RENAME_METADATA("RenameMetadata"),
    @XmlEnumValue("Block")
    BLOCK("Block");
    private final String value;

    PackageNameConflictResolution(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PackageNameConflictResolution fromValue(String v) {
        for (PackageNameConflictResolution c: PackageNameConflictResolution.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
