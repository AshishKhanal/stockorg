
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivitiesSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivitiesSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowUsersToRelateMultipleContactsToTasksAndEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableActivityReminders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableClickCreateEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableDragAndDropScheduling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableEmailTracking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableGroupTasks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableListViewScheduling" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableLogNote" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableMultidayEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableRecurringEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableRecurringTasks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSidebarCalendarShortcut" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSimpleTaskCreateUI" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableUNSTaskDelegatedToNotifications" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="meetingRequestsLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="showCustomLogoMeetingRequests" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showEventDetailsMultiUserCalendar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showHomePageHoverLinksForEvents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showMyTasksHoverLinks" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showRequestedMeetingsOnHomePage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivitiesSettings", propOrder = {
    "allowUsersToRelateMultipleContactsToTasksAndEvents",
    "enableActivityReminders",
    "enableClickCreateEvents",
    "enableDragAndDropScheduling",
    "enableEmailTracking",
    "enableGroupTasks",
    "enableListViewScheduling",
    "enableLogNote",
    "enableMultidayEvents",
    "enableRecurringEvents",
    "enableRecurringTasks",
    "enableSidebarCalendarShortcut",
    "enableSimpleTaskCreateUI",
    "enableUNSTaskDelegatedToNotifications",
    "meetingRequestsLogo",
    "showCustomLogoMeetingRequests",
    "showEventDetailsMultiUserCalendar",
    "showHomePageHoverLinksForEvents",
    "showMyTasksHoverLinks",
    "showRequestedMeetingsOnHomePage"
})
public class ActivitiesSettings
    extends MetadataForSettings
{

    protected Boolean allowUsersToRelateMultipleContactsToTasksAndEvents;
    protected Boolean enableActivityReminders;
    protected Boolean enableClickCreateEvents;
    protected Boolean enableDragAndDropScheduling;
    protected Boolean enableEmailTracking;
    protected Boolean enableGroupTasks;
    protected Boolean enableListViewScheduling;
    protected Boolean enableLogNote;
    protected Boolean enableMultidayEvents;
    protected Boolean enableRecurringEvents;
    protected Boolean enableRecurringTasks;
    protected Boolean enableSidebarCalendarShortcut;
    protected Boolean enableSimpleTaskCreateUI;
    protected Boolean enableUNSTaskDelegatedToNotifications;
    protected String meetingRequestsLogo;
    protected Boolean showCustomLogoMeetingRequests;
    protected Boolean showEventDetailsMultiUserCalendar;
    protected Boolean showHomePageHoverLinksForEvents;
    protected Boolean showMyTasksHoverLinks;
    protected Boolean showRequestedMeetingsOnHomePage;

    /**
     * Gets the value of the allowUsersToRelateMultipleContactsToTasksAndEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUsersToRelateMultipleContactsToTasksAndEvents() {
        return allowUsersToRelateMultipleContactsToTasksAndEvents;
    }

    /**
     * Sets the value of the allowUsersToRelateMultipleContactsToTasksAndEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUsersToRelateMultipleContactsToTasksAndEvents(Boolean value) {
        this.allowUsersToRelateMultipleContactsToTasksAndEvents = value;
    }

    /**
     * Gets the value of the enableActivityReminders property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableActivityReminders() {
        return enableActivityReminders;
    }

    /**
     * Sets the value of the enableActivityReminders property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableActivityReminders(Boolean value) {
        this.enableActivityReminders = value;
    }

    /**
     * Gets the value of the enableClickCreateEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableClickCreateEvents() {
        return enableClickCreateEvents;
    }

    /**
     * Sets the value of the enableClickCreateEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableClickCreateEvents(Boolean value) {
        this.enableClickCreateEvents = value;
    }

    /**
     * Gets the value of the enableDragAndDropScheduling property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableDragAndDropScheduling() {
        return enableDragAndDropScheduling;
    }

    /**
     * Sets the value of the enableDragAndDropScheduling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableDragAndDropScheduling(Boolean value) {
        this.enableDragAndDropScheduling = value;
    }

    /**
     * Gets the value of the enableEmailTracking property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmailTracking() {
        return enableEmailTracking;
    }

    /**
     * Sets the value of the enableEmailTracking property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmailTracking(Boolean value) {
        this.enableEmailTracking = value;
    }

    /**
     * Gets the value of the enableGroupTasks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableGroupTasks() {
        return enableGroupTasks;
    }

    /**
     * Sets the value of the enableGroupTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableGroupTasks(Boolean value) {
        this.enableGroupTasks = value;
    }

    /**
     * Gets the value of the enableListViewScheduling property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableListViewScheduling() {
        return enableListViewScheduling;
    }

    /**
     * Sets the value of the enableListViewScheduling property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableListViewScheduling(Boolean value) {
        this.enableListViewScheduling = value;
    }

    /**
     * Gets the value of the enableLogNote property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableLogNote() {
        return enableLogNote;
    }

    /**
     * Sets the value of the enableLogNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableLogNote(Boolean value) {
        this.enableLogNote = value;
    }

    /**
     * Gets the value of the enableMultidayEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMultidayEvents() {
        return enableMultidayEvents;
    }

    /**
     * Sets the value of the enableMultidayEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMultidayEvents(Boolean value) {
        this.enableMultidayEvents = value;
    }

    /**
     * Gets the value of the enableRecurringEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRecurringEvents() {
        return enableRecurringEvents;
    }

    /**
     * Sets the value of the enableRecurringEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRecurringEvents(Boolean value) {
        this.enableRecurringEvents = value;
    }

    /**
     * Gets the value of the enableRecurringTasks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableRecurringTasks() {
        return enableRecurringTasks;
    }

    /**
     * Sets the value of the enableRecurringTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableRecurringTasks(Boolean value) {
        this.enableRecurringTasks = value;
    }

    /**
     * Gets the value of the enableSidebarCalendarShortcut property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSidebarCalendarShortcut() {
        return enableSidebarCalendarShortcut;
    }

    /**
     * Sets the value of the enableSidebarCalendarShortcut property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSidebarCalendarShortcut(Boolean value) {
        this.enableSidebarCalendarShortcut = value;
    }

    /**
     * Gets the value of the enableSimpleTaskCreateUI property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSimpleTaskCreateUI() {
        return enableSimpleTaskCreateUI;
    }

    /**
     * Sets the value of the enableSimpleTaskCreateUI property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSimpleTaskCreateUI(Boolean value) {
        this.enableSimpleTaskCreateUI = value;
    }

    /**
     * Gets the value of the enableUNSTaskDelegatedToNotifications property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableUNSTaskDelegatedToNotifications() {
        return enableUNSTaskDelegatedToNotifications;
    }

    /**
     * Sets the value of the enableUNSTaskDelegatedToNotifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableUNSTaskDelegatedToNotifications(Boolean value) {
        this.enableUNSTaskDelegatedToNotifications = value;
    }

    /**
     * Gets the value of the meetingRequestsLogo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeetingRequestsLogo() {
        return meetingRequestsLogo;
    }

    /**
     * Sets the value of the meetingRequestsLogo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeetingRequestsLogo(String value) {
        this.meetingRequestsLogo = value;
    }

    /**
     * Gets the value of the showCustomLogoMeetingRequests property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowCustomLogoMeetingRequests() {
        return showCustomLogoMeetingRequests;
    }

    /**
     * Sets the value of the showCustomLogoMeetingRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowCustomLogoMeetingRequests(Boolean value) {
        this.showCustomLogoMeetingRequests = value;
    }

    /**
     * Gets the value of the showEventDetailsMultiUserCalendar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowEventDetailsMultiUserCalendar() {
        return showEventDetailsMultiUserCalendar;
    }

    /**
     * Sets the value of the showEventDetailsMultiUserCalendar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowEventDetailsMultiUserCalendar(Boolean value) {
        this.showEventDetailsMultiUserCalendar = value;
    }

    /**
     * Gets the value of the showHomePageHoverLinksForEvents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowHomePageHoverLinksForEvents() {
        return showHomePageHoverLinksForEvents;
    }

    /**
     * Sets the value of the showHomePageHoverLinksForEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowHomePageHoverLinksForEvents(Boolean value) {
        this.showHomePageHoverLinksForEvents = value;
    }

    /**
     * Gets the value of the showMyTasksHoverLinks property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowMyTasksHoverLinks() {
        return showMyTasksHoverLinks;
    }

    /**
     * Sets the value of the showMyTasksHoverLinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowMyTasksHoverLinks(Boolean value) {
        this.showMyTasksHoverLinks = value;
    }

    /**
     * Gets the value of the showRequestedMeetingsOnHomePage property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowRequestedMeetingsOnHomePage() {
        return showRequestedMeetingsOnHomePage;
    }

    /**
     * Sets the value of the showRequestedMeetingsOnHomePage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowRequestedMeetingsOnHomePage(Boolean value) {
        this.showRequestedMeetingsOnHomePage = value;
    }

}
