
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuraBundleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuraBundleType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Application"/&gt;
 *     &lt;enumeration value="Component"/&gt;
 *     &lt;enumeration value="Event"/&gt;
 *     &lt;enumeration value="Interface"/&gt;
 *     &lt;enumeration value="Tokens"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AuraBundleType")
@XmlEnum
public enum AuraBundleType {

    @XmlEnumValue("Application")
    APPLICATION("Application"),
    @XmlEnumValue("Component")
    COMPONENT("Component"),
    @XmlEnumValue("Event")
    EVENT("Event"),
    @XmlEnumValue("Interface")
    INTERFACE("Interface"),
    @XmlEnumValue("Tokens")
    TOKENS("Tokens");
    private final String value;

    AuraBundleType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AuraBundleType fromValue(String v) {
        for (AuraBundleType c: AuraBundleType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
