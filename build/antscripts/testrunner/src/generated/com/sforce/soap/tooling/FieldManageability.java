
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FieldManageability.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FieldManageability"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DeveloperControlled"/&gt;
 *     &lt;enumeration value="SubscriberControlled"/&gt;
 *     &lt;enumeration value="Locked"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FieldManageability")
@XmlEnum
public enum FieldManageability {

    @XmlEnumValue("DeveloperControlled")
    DEVELOPER_CONTROLLED("DeveloperControlled"),
    @XmlEnumValue("SubscriberControlled")
    SUBSCRIBER_CONTROLLED("SubscriberControlled"),
    @XmlEnumValue("Locked")
    LOCKED("Locked");
    private final String value;

    FieldManageability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FieldManageability fromValue(String v) {
        for (FieldManageability c: FieldManageability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
