
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.Encoding;
import com.sforce.soap.tooling.WebLinkAvailability;
import com.sforce.soap.tooling.WebLinkDisplayType;
import com.sforce.soap.tooling.WebLinkPosition;
import com.sforce.soap.tooling.WebLinkType;
import com.sforce.soap.tooling.WebLinkWindowType;


/**
 * <p>Java class for CustomPageWebLink complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomPageWebLink"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="availability" type="{urn:tooling.soap.sforce.com}WebLinkAvailability"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="displayType" type="{urn:tooling.soap.sforce.com}WebLinkDisplayType"/&gt;
 *         &lt;element name="encodingKey" type="{urn:tooling.soap.sforce.com}Encoding" minOccurs="0"/&gt;
 *         &lt;element name="hasMenubar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="hasScrollbars" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="hasToolbar" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="isResizable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="linkType" type="{urn:tooling.soap.sforce.com}WebLinkType"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="openType" type="{urn:tooling.soap.sforce.com}WebLinkWindowType"/&gt;
 *         &lt;element name="page" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="position" type="{urn:tooling.soap.sforce.com}WebLinkPosition" minOccurs="0"/&gt;
 *         &lt;element name="protected" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="requireRowSelection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="scontrol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="showsLocation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="showsStatus" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomPageWebLink", propOrder = {
    "availability",
    "description",
    "displayType",
    "encodingKey",
    "hasMenubar",
    "hasScrollbars",
    "hasToolbar",
    "height",
    "isResizable",
    "linkType",
    "masterLabel",
    "openType",
    "page",
    "position",
    "_protected",
    "requireRowSelection",
    "scontrol",
    "showsLocation",
    "showsStatus",
    "url",
    "width"
})
public class CustomPageWebLink
    extends Metadata
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected WebLinkAvailability availability;
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected WebLinkDisplayType displayType;
    @XmlSchemaType(name = "string")
    protected Encoding encodingKey;
    protected Boolean hasMenubar;
    protected Boolean hasScrollbars;
    protected Boolean hasToolbar;
    protected Integer height;
    protected Boolean isResizable;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected WebLinkType linkType;
    protected String masterLabel;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected WebLinkWindowType openType;
    protected String page;
    @XmlSchemaType(name = "string")
    protected WebLinkPosition position;
    @XmlElement(name = "protected")
    protected boolean _protected;
    protected Boolean requireRowSelection;
    protected String scontrol;
    protected Boolean showsLocation;
    protected Boolean showsStatus;
    protected String url;
    protected Integer width;

    /**
     * Gets the value of the availability property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkAvailability }
     *     
     */
    public WebLinkAvailability getAvailability() {
        return availability;
    }

    /**
     * Sets the value of the availability property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkAvailability }
     *     
     */
    public void setAvailability(WebLinkAvailability value) {
        this.availability = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the displayType property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkDisplayType }
     *     
     */
    public WebLinkDisplayType getDisplayType() {
        return displayType;
    }

    /**
     * Sets the value of the displayType property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkDisplayType }
     *     
     */
    public void setDisplayType(WebLinkDisplayType value) {
        this.displayType = value;
    }

    /**
     * Gets the value of the encodingKey property.
     * 
     * @return
     *     possible object is
     *     {@link Encoding }
     *     
     */
    public Encoding getEncodingKey() {
        return encodingKey;
    }

    /**
     * Sets the value of the encodingKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link Encoding }
     *     
     */
    public void setEncodingKey(Encoding value) {
        this.encodingKey = value;
    }

    /**
     * Gets the value of the hasMenubar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasMenubar() {
        return hasMenubar;
    }

    /**
     * Sets the value of the hasMenubar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasMenubar(Boolean value) {
        this.hasMenubar = value;
    }

    /**
     * Gets the value of the hasScrollbars property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasScrollbars() {
        return hasScrollbars;
    }

    /**
     * Sets the value of the hasScrollbars property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasScrollbars(Boolean value) {
        this.hasScrollbars = value;
    }

    /**
     * Gets the value of the hasToolbar property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasToolbar() {
        return hasToolbar;
    }

    /**
     * Sets the value of the hasToolbar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasToolbar(Boolean value) {
        this.hasToolbar = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHeight(Integer value) {
        this.height = value;
    }

    /**
     * Gets the value of the isResizable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsResizable() {
        return isResizable;
    }

    /**
     * Sets the value of the isResizable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsResizable(Boolean value) {
        this.isResizable = value;
    }

    /**
     * Gets the value of the linkType property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkType }
     *     
     */
    public WebLinkType getLinkType() {
        return linkType;
    }

    /**
     * Sets the value of the linkType property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkType }
     *     
     */
    public void setLinkType(WebLinkType value) {
        this.linkType = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the openType property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkWindowType }
     *     
     */
    public WebLinkWindowType getOpenType() {
        return openType;
    }

    /**
     * Sets the value of the openType property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkWindowType }
     *     
     */
    public void setOpenType(WebLinkWindowType value) {
        this.openType = value;
    }

    /**
     * Gets the value of the page property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPage(String value) {
        this.page = value;
    }

    /**
     * Gets the value of the position property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkPosition }
     *     
     */
    public WebLinkPosition getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkPosition }
     *     
     */
    public void setPosition(WebLinkPosition value) {
        this.position = value;
    }

    /**
     * Gets the value of the protected property.
     * 
     */
    public boolean isProtected() {
        return _protected;
    }

    /**
     * Sets the value of the protected property.
     * 
     */
    public void setProtected(boolean value) {
        this._protected = value;
    }

    /**
     * Gets the value of the requireRowSelection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequireRowSelection() {
        return requireRowSelection;
    }

    /**
     * Sets the value of the requireRowSelection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequireRowSelection(Boolean value) {
        this.requireRowSelection = value;
    }

    /**
     * Gets the value of the scontrol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScontrol() {
        return scontrol;
    }

    /**
     * Sets the value of the scontrol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScontrol(String value) {
        this.scontrol = value;
    }

    /**
     * Gets the value of the showsLocation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowsLocation() {
        return showsLocation;
    }

    /**
     * Sets the value of the showsLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowsLocation(Boolean value) {
        this.showsLocation = value;
    }

    /**
     * Gets the value of the showsStatus property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowsStatus() {
        return showsStatus;
    }

    /**
     * Sets the value of the showsStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowsStatus(Boolean value) {
        this.showsStatus = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidth(Integer value) {
        this.width = value;
    }

}
