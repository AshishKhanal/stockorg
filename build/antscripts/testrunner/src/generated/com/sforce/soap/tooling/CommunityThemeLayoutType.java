
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunityThemeLayoutType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunityThemeLayoutType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Login"/&gt;
 *     &lt;enumeration value="Home"/&gt;
 *     &lt;enumeration value="Inner"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CommunityThemeLayoutType")
@XmlEnum
public enum CommunityThemeLayoutType {

    @XmlEnumValue("Login")
    LOGIN("Login"),
    @XmlEnumValue("Home")
    HOME("Home"),
    @XmlEnumValue("Inner")
    INNER("Inner");
    private final String value;

    CommunityThemeLayoutType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunityThemeLayoutType fromValue(String v) {
        for (CommunityThemeLayoutType c: CommunityThemeLayoutType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
