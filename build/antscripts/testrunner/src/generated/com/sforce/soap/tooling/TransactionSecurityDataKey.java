
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionSecurityDataKey.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionSecurityDataKey"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Bowser"/&gt;
 *     &lt;enumeration value="NumberOfRecords"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionSecurityDataKey")
@XmlEnum
public enum TransactionSecurityDataKey {

    @XmlEnumValue("Bowser")
    BOWSER("Bowser"),
    @XmlEnumValue("NumberOfRecords")
    NUMBER_OF_RECORDS("NumberOfRecords");
    private final String value;

    TransactionSecurityDataKey(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionSecurityDataKey fromValue(String v) {
        for (TransactionSecurityDataKey c: TransactionSecurityDataKey.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
