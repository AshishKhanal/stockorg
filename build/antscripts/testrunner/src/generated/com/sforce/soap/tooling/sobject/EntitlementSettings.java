
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntitlementSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntitlementSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssetLookupLimitedToActiveEntitlementsOnAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AssetLookupLimitedToActiveEntitlementsOnContact" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AssetLookupLimitedToSameAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="AssetLookupLimitedToSameContact" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EntitlementLookupLimitedToActiveStatus" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EntitlementLookupLimitedToSameAccount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EntitlementLookupLimitedToSameAsset" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EntitlementLookupLimitedToSameContact" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsEntitlementVersioningEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEntitlementsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}EntitlementSettings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntitlementSettings", propOrder = {
    "assetLookupLimitedToActiveEntitlementsOnAccount",
    "assetLookupLimitedToActiveEntitlementsOnContact",
    "assetLookupLimitedToSameAccount",
    "assetLookupLimitedToSameContact",
    "durableId",
    "entitlementLookupLimitedToActiveStatus",
    "entitlementLookupLimitedToSameAccount",
    "entitlementLookupLimitedToSameAsset",
    "entitlementLookupLimitedToSameContact",
    "fullName",
    "isEntitlementVersioningEnabled",
    "isEntitlementsEnabled",
    "metadata"
})
public class EntitlementSettings
    extends SObject
{

    @XmlElementRef(name = "AssetLookupLimitedToActiveEntitlementsOnAccount", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> assetLookupLimitedToActiveEntitlementsOnAccount;
    @XmlElementRef(name = "AssetLookupLimitedToActiveEntitlementsOnContact", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> assetLookupLimitedToActiveEntitlementsOnContact;
    @XmlElementRef(name = "AssetLookupLimitedToSameAccount", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> assetLookupLimitedToSameAccount;
    @XmlElementRef(name = "AssetLookupLimitedToSameContact", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> assetLookupLimitedToSameContact;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "EntitlementLookupLimitedToActiveStatus", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> entitlementLookupLimitedToActiveStatus;
    @XmlElementRef(name = "EntitlementLookupLimitedToSameAccount", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> entitlementLookupLimitedToSameAccount;
    @XmlElementRef(name = "EntitlementLookupLimitedToSameAsset", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> entitlementLookupLimitedToSameAsset;
    @XmlElementRef(name = "EntitlementLookupLimitedToSameContact", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> entitlementLookupLimitedToSameContact;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsEntitlementVersioningEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEntitlementVersioningEnabled;
    @XmlElementRef(name = "IsEntitlementsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEntitlementsEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.EntitlementSettings> metadata;

    /**
     * Gets the value of the assetLookupLimitedToActiveEntitlementsOnAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAssetLookupLimitedToActiveEntitlementsOnAccount() {
        return assetLookupLimitedToActiveEntitlementsOnAccount;
    }

    /**
     * Sets the value of the assetLookupLimitedToActiveEntitlementsOnAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAssetLookupLimitedToActiveEntitlementsOnAccount(JAXBElement<Boolean> value) {
        this.assetLookupLimitedToActiveEntitlementsOnAccount = value;
    }

    /**
     * Gets the value of the assetLookupLimitedToActiveEntitlementsOnContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAssetLookupLimitedToActiveEntitlementsOnContact() {
        return assetLookupLimitedToActiveEntitlementsOnContact;
    }

    /**
     * Sets the value of the assetLookupLimitedToActiveEntitlementsOnContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAssetLookupLimitedToActiveEntitlementsOnContact(JAXBElement<Boolean> value) {
        this.assetLookupLimitedToActiveEntitlementsOnContact = value;
    }

    /**
     * Gets the value of the assetLookupLimitedToSameAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAssetLookupLimitedToSameAccount() {
        return assetLookupLimitedToSameAccount;
    }

    /**
     * Sets the value of the assetLookupLimitedToSameAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAssetLookupLimitedToSameAccount(JAXBElement<Boolean> value) {
        this.assetLookupLimitedToSameAccount = value;
    }

    /**
     * Gets the value of the assetLookupLimitedToSameContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAssetLookupLimitedToSameContact() {
        return assetLookupLimitedToSameContact;
    }

    /**
     * Sets the value of the assetLookupLimitedToSameContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAssetLookupLimitedToSameContact(JAXBElement<Boolean> value) {
        this.assetLookupLimitedToSameContact = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the entitlementLookupLimitedToActiveStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEntitlementLookupLimitedToActiveStatus() {
        return entitlementLookupLimitedToActiveStatus;
    }

    /**
     * Sets the value of the entitlementLookupLimitedToActiveStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEntitlementLookupLimitedToActiveStatus(JAXBElement<Boolean> value) {
        this.entitlementLookupLimitedToActiveStatus = value;
    }

    /**
     * Gets the value of the entitlementLookupLimitedToSameAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEntitlementLookupLimitedToSameAccount() {
        return entitlementLookupLimitedToSameAccount;
    }

    /**
     * Sets the value of the entitlementLookupLimitedToSameAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEntitlementLookupLimitedToSameAccount(JAXBElement<Boolean> value) {
        this.entitlementLookupLimitedToSameAccount = value;
    }

    /**
     * Gets the value of the entitlementLookupLimitedToSameAsset property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEntitlementLookupLimitedToSameAsset() {
        return entitlementLookupLimitedToSameAsset;
    }

    /**
     * Sets the value of the entitlementLookupLimitedToSameAsset property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEntitlementLookupLimitedToSameAsset(JAXBElement<Boolean> value) {
        this.entitlementLookupLimitedToSameAsset = value;
    }

    /**
     * Gets the value of the entitlementLookupLimitedToSameContact property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEntitlementLookupLimitedToSameContact() {
        return entitlementLookupLimitedToSameContact;
    }

    /**
     * Sets the value of the entitlementLookupLimitedToSameContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEntitlementLookupLimitedToSameContact(JAXBElement<Boolean> value) {
        this.entitlementLookupLimitedToSameContact = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isEntitlementVersioningEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEntitlementVersioningEnabled() {
        return isEntitlementVersioningEnabled;
    }

    /**
     * Sets the value of the isEntitlementVersioningEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEntitlementVersioningEnabled(JAXBElement<Boolean> value) {
        this.isEntitlementVersioningEnabled = value;
    }

    /**
     * Gets the value of the isEntitlementsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEntitlementsEnabled() {
        return isEntitlementsEnabled;
    }

    /**
     * Sets the value of the isEntitlementsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEntitlementsEnabled(JAXBElement<Boolean> value) {
        this.isEntitlementsEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.EntitlementSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.EntitlementSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.EntitlementSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.EntitlementSettings> value) {
        this.metadata = value;
    }

}
