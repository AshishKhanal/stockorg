
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketingActionSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketingActionSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableMarketingAction" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingActionSettings", propOrder = {
    "enableMarketingAction"
})
public class MarketingActionSettings
    extends MetadataForSettings
{

    protected boolean enableMarketingAction;

    /**
     * Gets the value of the enableMarketingAction property.
     * 
     */
    public boolean isEnableMarketingAction() {
        return enableMarketingAction;
    }

    /**
     * Sets the value of the enableMarketingAction property.
     * 
     */
    public void setEnableMarketingAction(boolean value) {
        this.enableMarketingAction = value;
    }

}
