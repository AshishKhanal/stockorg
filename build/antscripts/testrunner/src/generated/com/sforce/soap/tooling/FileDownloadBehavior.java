
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileDownloadBehavior.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FileDownloadBehavior"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DOWNLOAD"/&gt;
 *     &lt;enumeration value="EXECUTE_IN_BROWSER"/&gt;
 *     &lt;enumeration value="HYBRID"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FileDownloadBehavior")
@XmlEnum
public enum FileDownloadBehavior {

    DOWNLOAD,
    EXECUTE_IN_BROWSER,
    HYBRID;

    public String value() {
        return name();
    }

    public static FileDownloadBehavior fromValue(String v) {
        return valueOf(v);
    }

}
