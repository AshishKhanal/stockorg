
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Modifier.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Modifier"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TESTMETHOD"/&gt;
 *     &lt;enumeration value="GLOBAL"/&gt;
 *     &lt;enumeration value="WEBSERVICE"/&gt;
 *     &lt;enumeration value="PUBLIC"/&gt;
 *     &lt;enumeration value="PRIVATE"/&gt;
 *     &lt;enumeration value="PROTECTED"/&gt;
 *     &lt;enumeration value="WITHSHARING"/&gt;
 *     &lt;enumeration value="WITHOUTSHARING"/&gt;
 *     &lt;enumeration value="STATIC"/&gt;
 *     &lt;enumeration value="TRANSIENT"/&gt;
 *     &lt;enumeration value="ABSTRACT"/&gt;
 *     &lt;enumeration value="FINAL"/&gt;
 *     &lt;enumeration value="OVERRIDE"/&gt;
 *     &lt;enumeration value="VIRTUAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Modifier")
@XmlEnum
public enum Modifier {

    TESTMETHOD,
    GLOBAL,
    WEBSERVICE,
    PUBLIC,
    PRIVATE,
    PROTECTED,
    WITHSHARING,
    WITHOUTSHARING,
    STATIC,
    TRANSIENT,
    ABSTRACT,
    FINAL,
    OVERRIDE,
    VIRTUAL;

    public String value() {
        return name();
    }

    public static Modifier fromValue(String v) {
        return valueOf(v);
    }

}
