
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexiPageRegionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlexiPageRegionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Region"/&gt;
 *     &lt;enumeration value="Facet"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlexiPageRegionType")
@XmlEnum
public enum FlexiPageRegionType {

    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("Facet")
    FACET("Facet");
    private final String value;

    FlexiPageRegionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlexiPageRegionType fromValue(String v) {
        for (FlexiPageRegionType c: FlexiPageRegionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
