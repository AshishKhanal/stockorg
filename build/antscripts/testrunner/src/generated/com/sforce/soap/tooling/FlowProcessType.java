
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowProcessType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlowProcessType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AutoLaunchedFlow"/&gt;
 *     &lt;enumeration value="Flow"/&gt;
 *     &lt;enumeration value="Workflow"/&gt;
 *     &lt;enumeration value="CustomEvent"/&gt;
 *     &lt;enumeration value="InvocableProcess"/&gt;
 *     &lt;enumeration value="LoginFlow"/&gt;
 *     &lt;enumeration value="ActionPlan"/&gt;
 *     &lt;enumeration value="JourneyBuilderIntegration"/&gt;
 *     &lt;enumeration value="UserProvisioningFlow"/&gt;
 *     &lt;enumeration value="Survey"/&gt;
 *     &lt;enumeration value="FieldServiceMobile"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlowProcessType")
@XmlEnum
public enum FlowProcessType {

    @XmlEnumValue("AutoLaunchedFlow")
    AUTO_LAUNCHED_FLOW("AutoLaunchedFlow"),
    @XmlEnumValue("Flow")
    FLOW("Flow"),
    @XmlEnumValue("Workflow")
    WORKFLOW("Workflow"),
    @XmlEnumValue("CustomEvent")
    CUSTOM_EVENT("CustomEvent"),
    @XmlEnumValue("InvocableProcess")
    INVOCABLE_PROCESS("InvocableProcess"),
    @XmlEnumValue("LoginFlow")
    LOGIN_FLOW("LoginFlow"),
    @XmlEnumValue("ActionPlan")
    ACTION_PLAN("ActionPlan"),
    @XmlEnumValue("JourneyBuilderIntegration")
    JOURNEY_BUILDER_INTEGRATION("JourneyBuilderIntegration"),
    @XmlEnumValue("UserProvisioningFlow")
    USER_PROVISIONING_FLOW("UserProvisioningFlow"),
    @XmlEnumValue("Survey")
    SURVEY("Survey"),
    @XmlEnumValue("FieldServiceMobile")
    FIELD_SERVICE_MOBILE("FieldServiceMobile");
    private final String value;

    FlowProcessType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlowProcessType fromValue(String v) {
        for (FlowProcessType c: FlowProcessType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
