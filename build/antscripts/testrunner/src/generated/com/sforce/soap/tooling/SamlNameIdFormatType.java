
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SamlNameIdFormatType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SamlNameIdFormatType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unspecified"/&gt;
 *     &lt;enumeration value="EmailAddress"/&gt;
 *     &lt;enumeration value="Persistent"/&gt;
 *     &lt;enumeration value="Transient"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SamlNameIdFormatType")
@XmlEnum
public enum SamlNameIdFormatType {

    @XmlEnumValue("Unspecified")
    UNSPECIFIED("Unspecified"),
    @XmlEnumValue("EmailAddress")
    EMAIL_ADDRESS("EmailAddress"),
    @XmlEnumValue("Persistent")
    PERSISTENT("Persistent"),
    @XmlEnumValue("Transient")
    TRANSIENT("Transient");
    private final String value;

    SamlNameIdFormatType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SamlNameIdFormatType fromValue(String v) {
        for (SamlNameIdFormatType c: SamlNameIdFormatType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
