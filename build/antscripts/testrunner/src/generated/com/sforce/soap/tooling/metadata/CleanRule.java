
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.CleanRuleStatus;


/**
 * <p>Java class for CleanRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CleanRule"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="bypassTriggers" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="bypassWorkflow" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="developerName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fieldMappings" type="{urn:metadata.tooling.soap.sforce.com}FieldMapping" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="matchRule" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sourceSobjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status" type="{urn:tooling.soap.sforce.com}CleanRuleStatus"/&gt;
 *         &lt;element name="targetSobjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CleanRule", propOrder = {
    "bulkEnabled",
    "bypassTriggers",
    "bypassWorkflow",
    "description",
    "developerName",
    "fieldMappings",
    "masterLabel",
    "matchRule",
    "sourceSobjectType",
    "status",
    "targetSobjectType"
})
public class CleanRule {

    protected boolean bulkEnabled;
    protected boolean bypassTriggers;
    protected boolean bypassWorkflow;
    @XmlElement(required = true)
    protected String description;
    @XmlElement(required = true)
    protected String developerName;
    protected List<FieldMapping> fieldMappings;
    @XmlElement(required = true)
    protected String masterLabel;
    @XmlElement(required = true)
    protected String matchRule;
    @XmlElement(required = true)
    protected String sourceSobjectType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected CleanRuleStatus status;
    @XmlElement(required = true)
    protected String targetSobjectType;

    /**
     * Gets the value of the bulkEnabled property.
     * 
     */
    public boolean isBulkEnabled() {
        return bulkEnabled;
    }

    /**
     * Sets the value of the bulkEnabled property.
     * 
     */
    public void setBulkEnabled(boolean value) {
        this.bulkEnabled = value;
    }

    /**
     * Gets the value of the bypassTriggers property.
     * 
     */
    public boolean isBypassTriggers() {
        return bypassTriggers;
    }

    /**
     * Sets the value of the bypassTriggers property.
     * 
     */
    public void setBypassTriggers(boolean value) {
        this.bypassTriggers = value;
    }

    /**
     * Gets the value of the bypassWorkflow property.
     * 
     */
    public boolean isBypassWorkflow() {
        return bypassWorkflow;
    }

    /**
     * Sets the value of the bypassWorkflow property.
     * 
     */
    public void setBypassWorkflow(boolean value) {
        this.bypassWorkflow = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the developerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeveloperName() {
        return developerName;
    }

    /**
     * Sets the value of the developerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeveloperName(String value) {
        this.developerName = value;
    }

    /**
     * Gets the value of the fieldMappings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fieldMappings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFieldMappings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FieldMapping }
     * 
     * 
     */
    public List<FieldMapping> getFieldMappings() {
        if (fieldMappings == null) {
            fieldMappings = new ArrayList<FieldMapping>();
        }
        return this.fieldMappings;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the matchRule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchRule() {
        return matchRule;
    }

    /**
     * Sets the value of the matchRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchRule(String value) {
        this.matchRule = value;
    }

    /**
     * Gets the value of the sourceSobjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceSobjectType() {
        return sourceSobjectType;
    }

    /**
     * Sets the value of the sourceSobjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceSobjectType(String value) {
        this.sourceSobjectType = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link CleanRuleStatus }
     *     
     */
    public CleanRuleStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link CleanRuleStatus }
     *     
     */
    public void setStatus(CleanRuleStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the targetSobjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetSobjectType() {
        return targetSobjectType;
    }

    /**
     * Sets the value of the targetSobjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetSobjectType(String value) {
        this.targetSobjectType = value;
    }

}
