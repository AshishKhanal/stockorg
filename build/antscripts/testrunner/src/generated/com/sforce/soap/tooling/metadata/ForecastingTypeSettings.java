
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForecastingTypeSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastingTypeSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="adjustmentsSettings" type="{urn:metadata.tooling.soap.sforce.com}AdjustmentsSettings"/&gt;
 *         &lt;element name="displayedCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="forecastRangeSettings" type="{urn:metadata.tooling.soap.sforce.com}ForecastRangeSettings"/&gt;
 *         &lt;element name="forecastedCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="isAmount" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="isAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="isQuantity" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="managerAdjustableCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="opportunityListFieldsLabelMappings" type="{urn:metadata.tooling.soap.sforce.com}OpportunityListFieldsLabelMapping" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="opportunityListFieldsSelectedSettings" type="{urn:metadata.tooling.soap.sforce.com}OpportunityListFieldsSelectedSettings"/&gt;
 *         &lt;element name="opportunityListFieldsUnselectedSettings" type="{urn:metadata.tooling.soap.sforce.com}OpportunityListFieldsUnselectedSettings"/&gt;
 *         &lt;element name="ownerAdjustableCategoryApiNames" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="quotasSettings" type="{urn:metadata.tooling.soap.sforce.com}QuotasSettings"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastingTypeSettings", propOrder = {
    "active",
    "adjustmentsSettings",
    "displayedCategoryApiNames",
    "forecastRangeSettings",
    "forecastedCategoryApiNames",
    "isAmount",
    "isAvailable",
    "isQuantity",
    "managerAdjustableCategoryApiNames",
    "masterLabel",
    "name",
    "opportunityListFieldsLabelMappings",
    "opportunityListFieldsSelectedSettings",
    "opportunityListFieldsUnselectedSettings",
    "ownerAdjustableCategoryApiNames",
    "quotasSettings"
})
public class ForecastingTypeSettings {

    protected boolean active;
    @XmlElement(required = true)
    protected AdjustmentsSettings adjustmentsSettings;
    protected List<String> displayedCategoryApiNames;
    @XmlElement(required = true)
    protected ForecastRangeSettings forecastRangeSettings;
    protected List<String> forecastedCategoryApiNames;
    protected boolean isAmount;
    protected boolean isAvailable;
    protected boolean isQuantity;
    protected List<String> managerAdjustableCategoryApiNames;
    @XmlElement(required = true)
    protected String masterLabel;
    @XmlElement(required = true)
    protected String name;
    protected List<OpportunityListFieldsLabelMapping> opportunityListFieldsLabelMappings;
    @XmlElement(required = true)
    protected OpportunityListFieldsSelectedSettings opportunityListFieldsSelectedSettings;
    @XmlElement(required = true)
    protected OpportunityListFieldsUnselectedSettings opportunityListFieldsUnselectedSettings;
    protected List<String> ownerAdjustableCategoryApiNames;
    @XmlElement(required = true)
    protected QuotasSettings quotasSettings;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the adjustmentsSettings property.
     * 
     * @return
     *     possible object is
     *     {@link AdjustmentsSettings }
     *     
     */
    public AdjustmentsSettings getAdjustmentsSettings() {
        return adjustmentsSettings;
    }

    /**
     * Sets the value of the adjustmentsSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdjustmentsSettings }
     *     
     */
    public void setAdjustmentsSettings(AdjustmentsSettings value) {
        this.adjustmentsSettings = value;
    }

    /**
     * Gets the value of the displayedCategoryApiNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the displayedCategoryApiNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDisplayedCategoryApiNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDisplayedCategoryApiNames() {
        if (displayedCategoryApiNames == null) {
            displayedCategoryApiNames = new ArrayList<String>();
        }
        return this.displayedCategoryApiNames;
    }

    /**
     * Gets the value of the forecastRangeSettings property.
     * 
     * @return
     *     possible object is
     *     {@link ForecastRangeSettings }
     *     
     */
    public ForecastRangeSettings getForecastRangeSettings() {
        return forecastRangeSettings;
    }

    /**
     * Sets the value of the forecastRangeSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link ForecastRangeSettings }
     *     
     */
    public void setForecastRangeSettings(ForecastRangeSettings value) {
        this.forecastRangeSettings = value;
    }

    /**
     * Gets the value of the forecastedCategoryApiNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forecastedCategoryApiNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForecastedCategoryApiNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getForecastedCategoryApiNames() {
        if (forecastedCategoryApiNames == null) {
            forecastedCategoryApiNames = new ArrayList<String>();
        }
        return this.forecastedCategoryApiNames;
    }

    /**
     * Gets the value of the isAmount property.
     * 
     */
    public boolean isIsAmount() {
        return isAmount;
    }

    /**
     * Sets the value of the isAmount property.
     * 
     */
    public void setIsAmount(boolean value) {
        this.isAmount = value;
    }

    /**
     * Gets the value of the isAvailable property.
     * 
     */
    public boolean isIsAvailable() {
        return isAvailable;
    }

    /**
     * Sets the value of the isAvailable property.
     * 
     */
    public void setIsAvailable(boolean value) {
        this.isAvailable = value;
    }

    /**
     * Gets the value of the isQuantity property.
     * 
     */
    public boolean isIsQuantity() {
        return isQuantity;
    }

    /**
     * Sets the value of the isQuantity property.
     * 
     */
    public void setIsQuantity(boolean value) {
        this.isQuantity = value;
    }

    /**
     * Gets the value of the managerAdjustableCategoryApiNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the managerAdjustableCategoryApiNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManagerAdjustableCategoryApiNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getManagerAdjustableCategoryApiNames() {
        if (managerAdjustableCategoryApiNames == null) {
            managerAdjustableCategoryApiNames = new ArrayList<String>();
        }
        return this.managerAdjustableCategoryApiNames;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the opportunityListFieldsLabelMappings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the opportunityListFieldsLabelMappings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOpportunityListFieldsLabelMappings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OpportunityListFieldsLabelMapping }
     * 
     * 
     */
    public List<OpportunityListFieldsLabelMapping> getOpportunityListFieldsLabelMappings() {
        if (opportunityListFieldsLabelMappings == null) {
            opportunityListFieldsLabelMappings = new ArrayList<OpportunityListFieldsLabelMapping>();
        }
        return this.opportunityListFieldsLabelMappings;
    }

    /**
     * Gets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityListFieldsSelectedSettings }
     *     
     */
    public OpportunityListFieldsSelectedSettings getOpportunityListFieldsSelectedSettings() {
        return opportunityListFieldsSelectedSettings;
    }

    /**
     * Sets the value of the opportunityListFieldsSelectedSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityListFieldsSelectedSettings }
     *     
     */
    public void setOpportunityListFieldsSelectedSettings(OpportunityListFieldsSelectedSettings value) {
        this.opportunityListFieldsSelectedSettings = value;
    }

    /**
     * Gets the value of the opportunityListFieldsUnselectedSettings property.
     * 
     * @return
     *     possible object is
     *     {@link OpportunityListFieldsUnselectedSettings }
     *     
     */
    public OpportunityListFieldsUnselectedSettings getOpportunityListFieldsUnselectedSettings() {
        return opportunityListFieldsUnselectedSettings;
    }

    /**
     * Sets the value of the opportunityListFieldsUnselectedSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpportunityListFieldsUnselectedSettings }
     *     
     */
    public void setOpportunityListFieldsUnselectedSettings(OpportunityListFieldsUnselectedSettings value) {
        this.opportunityListFieldsUnselectedSettings = value;
    }

    /**
     * Gets the value of the ownerAdjustableCategoryApiNames property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ownerAdjustableCategoryApiNames property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOwnerAdjustableCategoryApiNames().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOwnerAdjustableCategoryApiNames() {
        if (ownerAdjustableCategoryApiNames == null) {
            ownerAdjustableCategoryApiNames = new ArrayList<String>();
        }
        return this.ownerAdjustableCategoryApiNames;
    }

    /**
     * Gets the value of the quotasSettings property.
     * 
     * @return
     *     possible object is
     *     {@link QuotasSettings }
     *     
     */
    public QuotasSettings getQuotasSettings() {
        return quotasSettings;
    }

    /**
     * Sets the value of the quotasSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuotasSettings }
     *     
     */
    public void setQuotasSettings(QuotasSettings value) {
        this.quotasSettings = value;
    }

}
