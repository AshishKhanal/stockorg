
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EventDeliveryType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EventDeliveryType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="StartFlow"/&gt;
 *     &lt;enumeration value="ResumeFlow"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "EventDeliveryType")
@XmlEnum
public enum EventDeliveryType {

    @XmlEnumValue("StartFlow")
    START_FLOW("StartFlow"),
    @XmlEnumValue("ResumeFlow")
    RESUME_FLOW("ResumeFlow");
    private final String value;

    EventDeliveryType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EventDeliveryType fromValue(String v) {
        for (EventDeliveryType c: EventDeliveryType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
