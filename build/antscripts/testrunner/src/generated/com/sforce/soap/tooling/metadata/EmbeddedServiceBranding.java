
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmbeddedServiceBranding complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmbeddedServiceBranding"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contrastPrimaryColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="embeddedServiceConfig" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="font" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="navBarColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="primaryColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="secondaryColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmbeddedServiceBranding", propOrder = {
    "contrastPrimaryColor",
    "embeddedServiceConfig",
    "font",
    "masterLabel",
    "navBarColor",
    "primaryColor",
    "secondaryColor"
})
public class EmbeddedServiceBranding
    extends Metadata
{

    protected String contrastPrimaryColor;
    @XmlElement(required = true)
    protected String embeddedServiceConfig;
    protected String font;
    @XmlElement(required = true)
    protected String masterLabel;
    protected String navBarColor;
    protected String primaryColor;
    protected String secondaryColor;

    /**
     * Gets the value of the contrastPrimaryColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrastPrimaryColor() {
        return contrastPrimaryColor;
    }

    /**
     * Sets the value of the contrastPrimaryColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrastPrimaryColor(String value) {
        this.contrastPrimaryColor = value;
    }

    /**
     * Gets the value of the embeddedServiceConfig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmbeddedServiceConfig() {
        return embeddedServiceConfig;
    }

    /**
     * Sets the value of the embeddedServiceConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmbeddedServiceConfig(String value) {
        this.embeddedServiceConfig = value;
    }

    /**
     * Gets the value of the font property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFont() {
        return font;
    }

    /**
     * Sets the value of the font property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFont(String value) {
        this.font = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the navBarColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNavBarColor() {
        return navBarColor;
    }

    /**
     * Sets the value of the navBarColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNavBarColor(String value) {
        this.navBarColor = value;
    }

    /**
     * Gets the value of the primaryColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryColor() {
        return primaryColor;
    }

    /**
     * Sets the value of the primaryColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryColor(String value) {
        this.primaryColor = value;
    }

    /**
     * Gets the value of the secondaryColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryColor() {
        return secondaryColor;
    }

    /**
     * Sets the value of the secondaryColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryColor(String value) {
        this.secondaryColor = value;
    }

}
