
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Complexity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Complexity"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NoRestriction"/&gt;
 *     &lt;enumeration value="AlphaNumeric"/&gt;
 *     &lt;enumeration value="SpecialCharacters"/&gt;
 *     &lt;enumeration value="UpperLowerCaseNumeric"/&gt;
 *     &lt;enumeration value="UpperLowerCaseNumericSpecialCharacters"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Complexity")
@XmlEnum
public enum Complexity {

    @XmlEnumValue("NoRestriction")
    NO_RESTRICTION("NoRestriction"),
    @XmlEnumValue("AlphaNumeric")
    ALPHA_NUMERIC("AlphaNumeric"),
    @XmlEnumValue("SpecialCharacters")
    SPECIAL_CHARACTERS("SpecialCharacters"),
    @XmlEnumValue("UpperLowerCaseNumeric")
    UPPER_LOWER_CASE_NUMERIC("UpperLowerCaseNumeric"),
    @XmlEnumValue("UpperLowerCaseNumericSpecialCharacters")
    UPPER_LOWER_CASE_NUMERIC_SPECIAL_CHARACTERS("UpperLowerCaseNumericSpecialCharacters");
    private final String value;

    Complexity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Complexity fromValue(String v) {
        for (Complexity c: Complexity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
