
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KnowledgeSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DefaultLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsChatterQuestionKbDeflectionEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCreateEditOnArticlesTabEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsExternalMediaContentEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsKnowledgeEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeSettings" minOccurs="0"/&gt;
 *         &lt;element name="ShowArticleSummariesCustomerPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowArticleSummariesInternalApp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowArticleSummariesPartnerPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowValidationStatusField" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KnowledgeSettings", propOrder = {
    "defaultLanguage",
    "durableId",
    "fullName",
    "isChatterQuestionKbDeflectionEnabled",
    "isCreateEditOnArticlesTabEnabled",
    "isExternalMediaContentEnabled",
    "isKnowledgeEnabled",
    "metadata",
    "showArticleSummariesCustomerPortal",
    "showArticleSummariesInternalApp",
    "showArticleSummariesPartnerPortal",
    "showValidationStatusField"
})
public class KnowledgeSettings
    extends SObject
{

    @XmlElementRef(name = "DefaultLanguage", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultLanguage;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsChatterQuestionKbDeflectionEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isChatterQuestionKbDeflectionEnabled;
    @XmlElementRef(name = "IsCreateEditOnArticlesTabEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCreateEditOnArticlesTabEnabled;
    @XmlElementRef(name = "IsExternalMediaContentEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isExternalMediaContentEnabled;
    @XmlElementRef(name = "IsKnowledgeEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isKnowledgeEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.KnowledgeSettings> metadata;
    @XmlElementRef(name = "ShowArticleSummariesCustomerPortal", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showArticleSummariesCustomerPortal;
    @XmlElementRef(name = "ShowArticleSummariesInternalApp", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showArticleSummariesInternalApp;
    @XmlElementRef(name = "ShowArticleSummariesPartnerPortal", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showArticleSummariesPartnerPortal;
    @XmlElementRef(name = "ShowValidationStatusField", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showValidationStatusField;

    /**
     * Gets the value of the defaultLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultLanguage() {
        return defaultLanguage;
    }

    /**
     * Sets the value of the defaultLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultLanguage(JAXBElement<String> value) {
        this.defaultLanguage = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isChatterQuestionKbDeflectionEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsChatterQuestionKbDeflectionEnabled() {
        return isChatterQuestionKbDeflectionEnabled;
    }

    /**
     * Sets the value of the isChatterQuestionKbDeflectionEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsChatterQuestionKbDeflectionEnabled(JAXBElement<Boolean> value) {
        this.isChatterQuestionKbDeflectionEnabled = value;
    }

    /**
     * Gets the value of the isCreateEditOnArticlesTabEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCreateEditOnArticlesTabEnabled() {
        return isCreateEditOnArticlesTabEnabled;
    }

    /**
     * Sets the value of the isCreateEditOnArticlesTabEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCreateEditOnArticlesTabEnabled(JAXBElement<Boolean> value) {
        this.isCreateEditOnArticlesTabEnabled = value;
    }

    /**
     * Gets the value of the isExternalMediaContentEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsExternalMediaContentEnabled() {
        return isExternalMediaContentEnabled;
    }

    /**
     * Sets the value of the isExternalMediaContentEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsExternalMediaContentEnabled(JAXBElement<Boolean> value) {
        this.isExternalMediaContentEnabled = value;
    }

    /**
     * Gets the value of the isKnowledgeEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsKnowledgeEnabled() {
        return isKnowledgeEnabled;
    }

    /**
     * Sets the value of the isKnowledgeEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsKnowledgeEnabled(JAXBElement<Boolean> value) {
        this.isKnowledgeEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.KnowledgeSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.KnowledgeSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.KnowledgeSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.KnowledgeSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the showArticleSummariesCustomerPortal property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowArticleSummariesCustomerPortal() {
        return showArticleSummariesCustomerPortal;
    }

    /**
     * Sets the value of the showArticleSummariesCustomerPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowArticleSummariesCustomerPortal(JAXBElement<Boolean> value) {
        this.showArticleSummariesCustomerPortal = value;
    }

    /**
     * Gets the value of the showArticleSummariesInternalApp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowArticleSummariesInternalApp() {
        return showArticleSummariesInternalApp;
    }

    /**
     * Sets the value of the showArticleSummariesInternalApp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowArticleSummariesInternalApp(JAXBElement<Boolean> value) {
        this.showArticleSummariesInternalApp = value;
    }

    /**
     * Gets the value of the showArticleSummariesPartnerPortal property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowArticleSummariesPartnerPortal() {
        return showArticleSummariesPartnerPortal;
    }

    /**
     * Sets the value of the showArticleSummariesPartnerPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowArticleSummariesPartnerPortal(JAXBElement<Boolean> value) {
        this.showArticleSummariesPartnerPortal = value;
    }

    /**
     * Gets the value of the showValidationStatusField property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowValidationStatusField() {
        return showValidationStatusField;
    }

    /**
     * Sets the value of the showValidationStatusField property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowValidationStatusField(JAXBElement<Boolean> value) {
        this.showValidationStatusField = value;
    }

}
