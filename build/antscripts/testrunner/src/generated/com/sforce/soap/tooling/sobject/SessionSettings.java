
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SessionSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SessionSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DisableTimeoutWarning" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EnforceIpRangesEveryRequest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ForceLogoutOnSessionTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ForceRelogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCacheAndAutocompleteEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsClickjackNonsetupSfdcEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsClickjackNonsetupUserEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsClickjackNonsetupUserHeaderlessEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsClickjackSetupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCspOnEmailEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCsrfOnGetEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCsrfOnPostEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsPostForSessionsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSmsIdentityEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LockSessionsToDomain" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LockSessionsToIp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="LogoutUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SessionTimeout" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SessionSettings", propOrder = {
    "disableTimeoutWarning",
    "durableId",
    "enforceIpRangesEveryRequest",
    "forceLogoutOnSessionTimeout",
    "forceRelogin",
    "isCacheAndAutocompleteEnabled",
    "isClickjackNonsetupSfdcEnabled",
    "isClickjackNonsetupUserEnabled",
    "isClickjackNonsetupUserHeaderlessEnabled",
    "isClickjackSetupEnabled",
    "isCspOnEmailEnabled",
    "isCsrfOnGetEnabled",
    "isCsrfOnPostEnabled",
    "isPostForSessionsEnabled",
    "isSmsIdentityEnabled",
    "lockSessionsToDomain",
    "lockSessionsToIp",
    "logoutUrl",
    "sessionTimeout"
})
public class SessionSettings
    extends SObject
{

    @XmlElementRef(name = "DisableTimeoutWarning", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> disableTimeoutWarning;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "EnforceIpRangesEveryRequest", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> enforceIpRangesEveryRequest;
    @XmlElementRef(name = "ForceLogoutOnSessionTimeout", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> forceLogoutOnSessionTimeout;
    @XmlElementRef(name = "ForceRelogin", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> forceRelogin;
    @XmlElementRef(name = "IsCacheAndAutocompleteEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCacheAndAutocompleteEnabled;
    @XmlElementRef(name = "IsClickjackNonsetupSfdcEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isClickjackNonsetupSfdcEnabled;
    @XmlElementRef(name = "IsClickjackNonsetupUserEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isClickjackNonsetupUserEnabled;
    @XmlElementRef(name = "IsClickjackNonsetupUserHeaderlessEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isClickjackNonsetupUserHeaderlessEnabled;
    @XmlElementRef(name = "IsClickjackSetupEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isClickjackSetupEnabled;
    @XmlElementRef(name = "IsCspOnEmailEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCspOnEmailEnabled;
    @XmlElementRef(name = "IsCsrfOnGetEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCsrfOnGetEnabled;
    @XmlElementRef(name = "IsCsrfOnPostEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCsrfOnPostEnabled;
    @XmlElementRef(name = "IsPostForSessionsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isPostForSessionsEnabled;
    @XmlElementRef(name = "IsSmsIdentityEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSmsIdentityEnabled;
    @XmlElementRef(name = "LockSessionsToDomain", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> lockSessionsToDomain;
    @XmlElementRef(name = "LockSessionsToIp", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> lockSessionsToIp;
    @XmlElementRef(name = "LogoutUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoutUrl;
    @XmlElementRef(name = "SessionTimeout", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sessionTimeout;

    /**
     * Gets the value of the disableTimeoutWarning property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDisableTimeoutWarning() {
        return disableTimeoutWarning;
    }

    /**
     * Sets the value of the disableTimeoutWarning property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDisableTimeoutWarning(JAXBElement<Boolean> value) {
        this.disableTimeoutWarning = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the enforceIpRangesEveryRequest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEnforceIpRangesEveryRequest() {
        return enforceIpRangesEveryRequest;
    }

    /**
     * Sets the value of the enforceIpRangesEveryRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEnforceIpRangesEveryRequest(JAXBElement<Boolean> value) {
        this.enforceIpRangesEveryRequest = value;
    }

    /**
     * Gets the value of the forceLogoutOnSessionTimeout property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getForceLogoutOnSessionTimeout() {
        return forceLogoutOnSessionTimeout;
    }

    /**
     * Sets the value of the forceLogoutOnSessionTimeout property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setForceLogoutOnSessionTimeout(JAXBElement<Boolean> value) {
        this.forceLogoutOnSessionTimeout = value;
    }

    /**
     * Gets the value of the forceRelogin property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getForceRelogin() {
        return forceRelogin;
    }

    /**
     * Sets the value of the forceRelogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setForceRelogin(JAXBElement<Boolean> value) {
        this.forceRelogin = value;
    }

    /**
     * Gets the value of the isCacheAndAutocompleteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCacheAndAutocompleteEnabled() {
        return isCacheAndAutocompleteEnabled;
    }

    /**
     * Sets the value of the isCacheAndAutocompleteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCacheAndAutocompleteEnabled(JAXBElement<Boolean> value) {
        this.isCacheAndAutocompleteEnabled = value;
    }

    /**
     * Gets the value of the isClickjackNonsetupSfdcEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsClickjackNonsetupSfdcEnabled() {
        return isClickjackNonsetupSfdcEnabled;
    }

    /**
     * Sets the value of the isClickjackNonsetupSfdcEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsClickjackNonsetupSfdcEnabled(JAXBElement<Boolean> value) {
        this.isClickjackNonsetupSfdcEnabled = value;
    }

    /**
     * Gets the value of the isClickjackNonsetupUserEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsClickjackNonsetupUserEnabled() {
        return isClickjackNonsetupUserEnabled;
    }

    /**
     * Sets the value of the isClickjackNonsetupUserEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsClickjackNonsetupUserEnabled(JAXBElement<Boolean> value) {
        this.isClickjackNonsetupUserEnabled = value;
    }

    /**
     * Gets the value of the isClickjackNonsetupUserHeaderlessEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsClickjackNonsetupUserHeaderlessEnabled() {
        return isClickjackNonsetupUserHeaderlessEnabled;
    }

    /**
     * Sets the value of the isClickjackNonsetupUserHeaderlessEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsClickjackNonsetupUserHeaderlessEnabled(JAXBElement<Boolean> value) {
        this.isClickjackNonsetupUserHeaderlessEnabled = value;
    }

    /**
     * Gets the value of the isClickjackSetupEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsClickjackSetupEnabled() {
        return isClickjackSetupEnabled;
    }

    /**
     * Sets the value of the isClickjackSetupEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsClickjackSetupEnabled(JAXBElement<Boolean> value) {
        this.isClickjackSetupEnabled = value;
    }

    /**
     * Gets the value of the isCspOnEmailEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCspOnEmailEnabled() {
        return isCspOnEmailEnabled;
    }

    /**
     * Sets the value of the isCspOnEmailEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCspOnEmailEnabled(JAXBElement<Boolean> value) {
        this.isCspOnEmailEnabled = value;
    }

    /**
     * Gets the value of the isCsrfOnGetEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCsrfOnGetEnabled() {
        return isCsrfOnGetEnabled;
    }

    /**
     * Sets the value of the isCsrfOnGetEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCsrfOnGetEnabled(JAXBElement<Boolean> value) {
        this.isCsrfOnGetEnabled = value;
    }

    /**
     * Gets the value of the isCsrfOnPostEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCsrfOnPostEnabled() {
        return isCsrfOnPostEnabled;
    }

    /**
     * Sets the value of the isCsrfOnPostEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCsrfOnPostEnabled(JAXBElement<Boolean> value) {
        this.isCsrfOnPostEnabled = value;
    }

    /**
     * Gets the value of the isPostForSessionsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsPostForSessionsEnabled() {
        return isPostForSessionsEnabled;
    }

    /**
     * Sets the value of the isPostForSessionsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsPostForSessionsEnabled(JAXBElement<Boolean> value) {
        this.isPostForSessionsEnabled = value;
    }

    /**
     * Gets the value of the isSmsIdentityEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSmsIdentityEnabled() {
        return isSmsIdentityEnabled;
    }

    /**
     * Sets the value of the isSmsIdentityEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSmsIdentityEnabled(JAXBElement<Boolean> value) {
        this.isSmsIdentityEnabled = value;
    }

    /**
     * Gets the value of the lockSessionsToDomain property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getLockSessionsToDomain() {
        return lockSessionsToDomain;
    }

    /**
     * Sets the value of the lockSessionsToDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setLockSessionsToDomain(JAXBElement<Boolean> value) {
        this.lockSessionsToDomain = value;
    }

    /**
     * Gets the value of the lockSessionsToIp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getLockSessionsToIp() {
        return lockSessionsToIp;
    }

    /**
     * Sets the value of the lockSessionsToIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setLockSessionsToIp(JAXBElement<Boolean> value) {
        this.lockSessionsToIp = value;
    }

    /**
     * Gets the value of the logoutUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoutUrl() {
        return logoutUrl;
    }

    /**
     * Sets the value of the logoutUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoutUrl(JAXBElement<String> value) {
        this.logoutUrl = value;
    }

    /**
     * Gets the value of the sessionTimeout property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSessionTimeout() {
        return sessionTimeout;
    }

    /**
     * Sets the value of the sessionTimeout property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSessionTimeout(JAXBElement<String> value) {
        this.sessionTimeout = value;
    }

}
