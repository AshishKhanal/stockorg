
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SessionTimeout.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SessionTimeout"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TwentyFourHours"/&gt;
 *     &lt;enumeration value="TwelveHours"/&gt;
 *     &lt;enumeration value="EightHours"/&gt;
 *     &lt;enumeration value="FourHours"/&gt;
 *     &lt;enumeration value="TwoHours"/&gt;
 *     &lt;enumeration value="SixtyMinutes"/&gt;
 *     &lt;enumeration value="ThirtyMinutes"/&gt;
 *     &lt;enumeration value="FifteenMinutes"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SessionTimeout")
@XmlEnum
public enum SessionTimeout {

    @XmlEnumValue("TwentyFourHours")
    TWENTY_FOUR_HOURS("TwentyFourHours"),
    @XmlEnumValue("TwelveHours")
    TWELVE_HOURS("TwelveHours"),
    @XmlEnumValue("EightHours")
    EIGHT_HOURS("EightHours"),
    @XmlEnumValue("FourHours")
    FOUR_HOURS("FourHours"),
    @XmlEnumValue("TwoHours")
    TWO_HOURS("TwoHours"),
    @XmlEnumValue("SixtyMinutes")
    SIXTY_MINUTES("SixtyMinutes"),
    @XmlEnumValue("ThirtyMinutes")
    THIRTY_MINUTES("ThirtyMinutes"),
    @XmlEnumValue("FifteenMinutes")
    FIFTEEN_MINUTES("FifteenMinutes");
    private final String value;

    SessionTimeout(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SessionTimeout fromValue(String v) {
        for (SessionTimeout c: SessionTimeout.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
