
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Role complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Role"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}RoleOrTerritory"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentRole" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Role", propOrder = {
    "parentRole"
})
public class Role
    extends RoleOrTerritory
{

    protected String parentRole;

    /**
     * Gets the value of the parentRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentRole() {
        return parentRole;
    }

    /**
     * Sets the value of the parentRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentRole(String value) {
        this.parentRole = value;
    }

}
