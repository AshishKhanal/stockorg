
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FormFactor;
import com.sforce.soap.tooling.NavType;
import com.sforce.soap.tooling.UiType;


/**
 * <p>Java class for CustomApplication complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomApplication"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actionOverrides" type="{urn:metadata.tooling.soap.sforce.com}AppActionOverride" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="brand" type="{urn:metadata.tooling.soap.sforce.com}AppBrand" minOccurs="0"/&gt;
 *         &lt;element name="defaultLandingTab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="detailPageRefreshMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="enableCustomizeMyTabs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableKeyboardShortcuts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableListViewHover" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableListViewReskin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableMultiMonitorComponents" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enablePinTabs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableTabHover" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableTabLimits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="footerColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="formFactors" type="{urn:tooling.soap.sforce.com}FormFactor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="headerColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isServiceCloudConsole" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="keyboardShortcuts" type="{urn:metadata.tooling.soap.sforce.com}KeyboardShortcuts" minOccurs="0"/&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="listPlacement" type="{urn:metadata.tooling.soap.sforce.com}ListPlacement" minOccurs="0"/&gt;
 *         &lt;element name="listRefreshMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="liveAgentConfig" type="{urn:metadata.tooling.soap.sforce.com}LiveAgentConfig" minOccurs="0"/&gt;
 *         &lt;element name="logo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="navType" type="{urn:tooling.soap.sforce.com}NavType" minOccurs="0"/&gt;
 *         &lt;element name="primaryTabColor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="profileActionOverrides" type="{urn:metadata.tooling.soap.sforce.com}AppProfileActionOverride" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="saveUserSessions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="setupExperience" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tab" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tabLimitConfig" type="{urn:metadata.tooling.soap.sforce.com}TabLimitConfig" minOccurs="0"/&gt;
 *         &lt;element name="uiType" type="{urn:tooling.soap.sforce.com}UiType" minOccurs="0"/&gt;
 *         &lt;element name="utilityBar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="workspaceMappings" type="{urn:metadata.tooling.soap.sforce.com}WorkspaceMappings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomApplication", propOrder = {
    "actionOverrides",
    "brand",
    "defaultLandingTab",
    "description",
    "detailPageRefreshMethod",
    "enableCustomizeMyTabs",
    "enableKeyboardShortcuts",
    "enableListViewHover",
    "enableListViewReskin",
    "enableMultiMonitorComponents",
    "enablePinTabs",
    "enableTabHover",
    "enableTabLimits",
    "footerColor",
    "formFactors",
    "headerColor",
    "isServiceCloudConsole",
    "keyboardShortcuts",
    "label",
    "listPlacement",
    "listRefreshMethod",
    "liveAgentConfig",
    "logo",
    "navType",
    "primaryTabColor",
    "profileActionOverrides",
    "saveUserSessions",
    "setupExperience",
    "tab",
    "tabLimitConfig",
    "uiType",
    "utilityBar",
    "workspaceMappings"
})
public class CustomApplication
    extends Metadata
{

    protected List<AppActionOverride> actionOverrides;
    protected AppBrand brand;
    protected String defaultLandingTab;
    protected String description;
    protected String detailPageRefreshMethod;
    protected Boolean enableCustomizeMyTabs;
    protected Boolean enableKeyboardShortcuts;
    protected Boolean enableListViewHover;
    protected Boolean enableListViewReskin;
    protected Boolean enableMultiMonitorComponents;
    protected Boolean enablePinTabs;
    protected Boolean enableTabHover;
    protected Boolean enableTabLimits;
    protected String footerColor;
    @XmlSchemaType(name = "string")
    protected List<FormFactor> formFactors;
    protected String headerColor;
    protected Boolean isServiceCloudConsole;
    protected KeyboardShortcuts keyboardShortcuts;
    protected String label;
    protected ListPlacement listPlacement;
    protected String listRefreshMethod;
    protected LiveAgentConfig liveAgentConfig;
    protected String logo;
    @XmlSchemaType(name = "string")
    protected NavType navType;
    protected String primaryTabColor;
    protected List<AppProfileActionOverride> profileActionOverrides;
    protected Boolean saveUserSessions;
    protected String setupExperience;
    protected List<String> tab;
    protected TabLimitConfig tabLimitConfig;
    @XmlSchemaType(name = "string")
    protected UiType uiType;
    protected String utilityBar;
    protected WorkspaceMappings workspaceMappings;

    /**
     * Gets the value of the actionOverrides property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actionOverrides property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActionOverrides().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppActionOverride }
     * 
     * 
     */
    public List<AppActionOverride> getActionOverrides() {
        if (actionOverrides == null) {
            actionOverrides = new ArrayList<AppActionOverride>();
        }
        return this.actionOverrides;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link AppBrand }
     *     
     */
    public AppBrand getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link AppBrand }
     *     
     */
    public void setBrand(AppBrand value) {
        this.brand = value;
    }

    /**
     * Gets the value of the defaultLandingTab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultLandingTab() {
        return defaultLandingTab;
    }

    /**
     * Sets the value of the defaultLandingTab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultLandingTab(String value) {
        this.defaultLandingTab = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailPageRefreshMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailPageRefreshMethod() {
        return detailPageRefreshMethod;
    }

    /**
     * Sets the value of the detailPageRefreshMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailPageRefreshMethod(String value) {
        this.detailPageRefreshMethod = value;
    }

    /**
     * Gets the value of the enableCustomizeMyTabs property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCustomizeMyTabs() {
        return enableCustomizeMyTabs;
    }

    /**
     * Sets the value of the enableCustomizeMyTabs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCustomizeMyTabs(Boolean value) {
        this.enableCustomizeMyTabs = value;
    }

    /**
     * Gets the value of the enableKeyboardShortcuts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableKeyboardShortcuts() {
        return enableKeyboardShortcuts;
    }

    /**
     * Sets the value of the enableKeyboardShortcuts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableKeyboardShortcuts(Boolean value) {
        this.enableKeyboardShortcuts = value;
    }

    /**
     * Gets the value of the enableListViewHover property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableListViewHover() {
        return enableListViewHover;
    }

    /**
     * Sets the value of the enableListViewHover property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableListViewHover(Boolean value) {
        this.enableListViewHover = value;
    }

    /**
     * Gets the value of the enableListViewReskin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableListViewReskin() {
        return enableListViewReskin;
    }

    /**
     * Sets the value of the enableListViewReskin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableListViewReskin(Boolean value) {
        this.enableListViewReskin = value;
    }

    /**
     * Gets the value of the enableMultiMonitorComponents property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMultiMonitorComponents() {
        return enableMultiMonitorComponents;
    }

    /**
     * Sets the value of the enableMultiMonitorComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMultiMonitorComponents(Boolean value) {
        this.enableMultiMonitorComponents = value;
    }

    /**
     * Gets the value of the enablePinTabs property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePinTabs() {
        return enablePinTabs;
    }

    /**
     * Sets the value of the enablePinTabs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePinTabs(Boolean value) {
        this.enablePinTabs = value;
    }

    /**
     * Gets the value of the enableTabHover property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTabHover() {
        return enableTabHover;
    }

    /**
     * Sets the value of the enableTabHover property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTabHover(Boolean value) {
        this.enableTabHover = value;
    }

    /**
     * Gets the value of the enableTabLimits property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableTabLimits() {
        return enableTabLimits;
    }

    /**
     * Sets the value of the enableTabLimits property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableTabLimits(Boolean value) {
        this.enableTabLimits = value;
    }

    /**
     * Gets the value of the footerColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFooterColor() {
        return footerColor;
    }

    /**
     * Sets the value of the footerColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFooterColor(String value) {
        this.footerColor = value;
    }

    /**
     * Gets the value of the formFactors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formFactors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormFactors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormFactor }
     * 
     * 
     */
    public List<FormFactor> getFormFactors() {
        if (formFactors == null) {
            formFactors = new ArrayList<FormFactor>();
        }
        return this.formFactors;
    }

    /**
     * Gets the value of the headerColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeaderColor() {
        return headerColor;
    }

    /**
     * Sets the value of the headerColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeaderColor(String value) {
        this.headerColor = value;
    }

    /**
     * Gets the value of the isServiceCloudConsole property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsServiceCloudConsole() {
        return isServiceCloudConsole;
    }

    /**
     * Sets the value of the isServiceCloudConsole property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsServiceCloudConsole(Boolean value) {
        this.isServiceCloudConsole = value;
    }

    /**
     * Gets the value of the keyboardShortcuts property.
     * 
     * @return
     *     possible object is
     *     {@link KeyboardShortcuts }
     *     
     */
    public KeyboardShortcuts getKeyboardShortcuts() {
        return keyboardShortcuts;
    }

    /**
     * Sets the value of the keyboardShortcuts property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeyboardShortcuts }
     *     
     */
    public void setKeyboardShortcuts(KeyboardShortcuts value) {
        this.keyboardShortcuts = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the listPlacement property.
     * 
     * @return
     *     possible object is
     *     {@link ListPlacement }
     *     
     */
    public ListPlacement getListPlacement() {
        return listPlacement;
    }

    /**
     * Sets the value of the listPlacement property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListPlacement }
     *     
     */
    public void setListPlacement(ListPlacement value) {
        this.listPlacement = value;
    }

    /**
     * Gets the value of the listRefreshMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListRefreshMethod() {
        return listRefreshMethod;
    }

    /**
     * Sets the value of the listRefreshMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListRefreshMethod(String value) {
        this.listRefreshMethod = value;
    }

    /**
     * Gets the value of the liveAgentConfig property.
     * 
     * @return
     *     possible object is
     *     {@link LiveAgentConfig }
     *     
     */
    public LiveAgentConfig getLiveAgentConfig() {
        return liveAgentConfig;
    }

    /**
     * Sets the value of the liveAgentConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link LiveAgentConfig }
     *     
     */
    public void setLiveAgentConfig(LiveAgentConfig value) {
        this.liveAgentConfig = value;
    }

    /**
     * Gets the value of the logo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogo() {
        return logo;
    }

    /**
     * Sets the value of the logo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogo(String value) {
        this.logo = value;
    }

    /**
     * Gets the value of the navType property.
     * 
     * @return
     *     possible object is
     *     {@link NavType }
     *     
     */
    public NavType getNavType() {
        return navType;
    }

    /**
     * Sets the value of the navType property.
     * 
     * @param value
     *     allowed object is
     *     {@link NavType }
     *     
     */
    public void setNavType(NavType value) {
        this.navType = value;
    }

    /**
     * Gets the value of the primaryTabColor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryTabColor() {
        return primaryTabColor;
    }

    /**
     * Sets the value of the primaryTabColor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryTabColor(String value) {
        this.primaryTabColor = value;
    }

    /**
     * Gets the value of the profileActionOverrides property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the profileActionOverrides property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProfileActionOverrides().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppProfileActionOverride }
     * 
     * 
     */
    public List<AppProfileActionOverride> getProfileActionOverrides() {
        if (profileActionOverrides == null) {
            profileActionOverrides = new ArrayList<AppProfileActionOverride>();
        }
        return this.profileActionOverrides;
    }

    /**
     * Gets the value of the saveUserSessions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSaveUserSessions() {
        return saveUserSessions;
    }

    /**
     * Sets the value of the saveUserSessions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSaveUserSessions(Boolean value) {
        this.saveUserSessions = value;
    }

    /**
     * Gets the value of the setupExperience property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSetupExperience() {
        return setupExperience;
    }

    /**
     * Sets the value of the setupExperience property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSetupExperience(String value) {
        this.setupExperience = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tab property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTab().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTab() {
        if (tab == null) {
            tab = new ArrayList<String>();
        }
        return this.tab;
    }

    /**
     * Gets the value of the tabLimitConfig property.
     * 
     * @return
     *     possible object is
     *     {@link TabLimitConfig }
     *     
     */
    public TabLimitConfig getTabLimitConfig() {
        return tabLimitConfig;
    }

    /**
     * Sets the value of the tabLimitConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link TabLimitConfig }
     *     
     */
    public void setTabLimitConfig(TabLimitConfig value) {
        this.tabLimitConfig = value;
    }

    /**
     * Gets the value of the uiType property.
     * 
     * @return
     *     possible object is
     *     {@link UiType }
     *     
     */
    public UiType getUiType() {
        return uiType;
    }

    /**
     * Sets the value of the uiType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UiType }
     *     
     */
    public void setUiType(UiType value) {
        this.uiType = value;
    }

    /**
     * Gets the value of the utilityBar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtilityBar() {
        return utilityBar;
    }

    /**
     * Sets the value of the utilityBar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtilityBar(String value) {
        this.utilityBar = value;
    }

    /**
     * Gets the value of the workspaceMappings property.
     * 
     * @return
     *     possible object is
     *     {@link WorkspaceMappings }
     *     
     */
    public WorkspaceMappings getWorkspaceMappings() {
        return workspaceMappings;
    }

    /**
     * Sets the value of the workspaceMappings property.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkspaceMappings }
     *     
     */
    public void setWorkspaceMappings(WorkspaceMappings value) {
        this.workspaceMappings = value;
    }

}
