
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TestLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TestLevel"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NoTestRun"/&gt;
 *     &lt;enumeration value="RunSpecifiedTests"/&gt;
 *     &lt;enumeration value="RunLocalTests"/&gt;
 *     &lt;enumeration value="RunAllTestsInOrg"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TestLevel")
@XmlEnum
public enum TestLevel {

    @XmlEnumValue("NoTestRun")
    NO_TEST_RUN("NoTestRun"),
    @XmlEnumValue("RunSpecifiedTests")
    RUN_SPECIFIED_TESTS("RunSpecifiedTests"),
    @XmlEnumValue("RunLocalTests")
    RUN_LOCAL_TESTS("RunLocalTests"),
    @XmlEnumValue("RunAllTestsInOrg")
    RUN_ALL_TESTS_IN_ORG("RunAllTestsInOrg");
    private final String value;

    TestLevel(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TestLevel fromValue(String v) {
        for (TestLevel c: TestLevel.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
