
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexiPageRegionMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlexiPageRegionMode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Append"/&gt;
 *     &lt;enumeration value="Prepend"/&gt;
 *     &lt;enumeration value="Replace"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlexiPageRegionMode")
@XmlEnum
public enum FlexiPageRegionMode {

    @XmlEnumValue("Append")
    APPEND("Append"),
    @XmlEnumValue("Prepend")
    PREPEND("Prepend"),
    @XmlEnumValue("Replace")
    REPLACE("Replace");
    private final String value;

    FlexiPageRegionMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlexiPageRegionMode fromValue(String v) {
        for (FlexiPageRegionMode c: FlexiPageRegionMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
