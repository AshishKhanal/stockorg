
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LayoutType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LayoutType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Standard"/&gt;
 *     &lt;enumeration value="ProcessDefinition"/&gt;
 *     &lt;enumeration value="GlobalQuickActionList"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LayoutType")
@XmlEnum
public enum LayoutType {

    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("ProcessDefinition")
    PROCESS_DEFINITION("ProcessDefinition"),
    @XmlEnumValue("GlobalQuickActionList")
    GLOBAL_QUICK_ACTION_LIST("GlobalQuickActionList");
    private final String value;

    LayoutType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LayoutType fromValue(String v) {
        for (LayoutType c: LayoutType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
