
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchResultsMetadata complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchResultsMetadata"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entityLabelMetadata" type="{urn:tooling.soap.sforce.com}LabelsSearchMetadata" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="entityMetadata" type="{urn:tooling.soap.sforce.com}EntitySearchMetadata" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResultsMetadata", propOrder = {
    "entityLabelMetadata",
    "entityMetadata"
})
public class SearchResultsMetadata {

    protected List<LabelsSearchMetadata> entityLabelMetadata;
    protected List<EntitySearchMetadata> entityMetadata;

    /**
     * Gets the value of the entityLabelMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entityLabelMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntityLabelMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LabelsSearchMetadata }
     * 
     * 
     */
    public List<LabelsSearchMetadata> getEntityLabelMetadata() {
        if (entityLabelMetadata == null) {
            entityLabelMetadata = new ArrayList<LabelsSearchMetadata>();
        }
        return this.entityLabelMetadata;
    }

    /**
     * Gets the value of the entityMetadata property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entityMetadata property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntityMetadata().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntitySearchMetadata }
     * 
     * 
     */
    public List<EntitySearchMetadata> getEntityMetadata() {
        if (entityMetadata == null) {
            entityMetadata = new ArrayList<EntitySearchMetadata>();
        }
        return this.entityMetadata;
    }

}
