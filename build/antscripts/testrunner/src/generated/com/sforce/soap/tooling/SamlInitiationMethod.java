
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SamlInitiationMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SamlInitiationMethod"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="IdpInitiated"/&gt;
 *     &lt;enumeration value="SpInitiated"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SamlInitiationMethod")
@XmlEnum
public enum SamlInitiationMethod {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("IdpInitiated")
    IDP_INITIATED("IdpInitiated"),
    @XmlEnumValue("SpInitiated")
    SP_INITIATED("SpInitiated");
    private final String value;

    SamlInitiationMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SamlInitiationMethod fromValue(String v) {
        for (SamlInitiationMethod c: SamlInitiationMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
