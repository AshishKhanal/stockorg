
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeCaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KnowledgeCaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="articlePDFCreationProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="articlePublicSharingCommunities" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeCommunitiesSettings" minOccurs="0"/&gt;
 *         &lt;element name="articlePublicSharingSites" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeSitesSettings" minOccurs="0"/&gt;
 *         &lt;element name="articlePublicSharingSitesChatterAnswers" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeSitesSettings" minOccurs="0"/&gt;
 *         &lt;element name="assignTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customizationClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultContributionArticleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="editor" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeCaseEditor" minOccurs="0"/&gt;
 *         &lt;element name="enableArticleCreation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableArticlePublicSharingSites" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="useProfileForPDFCreation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KnowledgeCaseSettings", propOrder = {
    "articlePDFCreationProfile",
    "articlePublicSharingCommunities",
    "articlePublicSharingSites",
    "articlePublicSharingSitesChatterAnswers",
    "assignTo",
    "customizationClass",
    "defaultContributionArticleType",
    "editor",
    "enableArticleCreation",
    "enableArticlePublicSharingSites",
    "useProfileForPDFCreation"
})
public class KnowledgeCaseSettings {

    protected String articlePDFCreationProfile;
    protected KnowledgeCommunitiesSettings articlePublicSharingCommunities;
    protected KnowledgeSitesSettings articlePublicSharingSites;
    protected KnowledgeSitesSettings articlePublicSharingSitesChatterAnswers;
    protected String assignTo;
    protected String customizationClass;
    protected String defaultContributionArticleType;
    @XmlSchemaType(name = "string")
    protected KnowledgeCaseEditor editor;
    protected Boolean enableArticleCreation;
    protected Boolean enableArticlePublicSharingSites;
    protected Boolean useProfileForPDFCreation;

    /**
     * Gets the value of the articlePDFCreationProfile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArticlePDFCreationProfile() {
        return articlePDFCreationProfile;
    }

    /**
     * Sets the value of the articlePDFCreationProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArticlePDFCreationProfile(String value) {
        this.articlePDFCreationProfile = value;
    }

    /**
     * Gets the value of the articlePublicSharingCommunities property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeCommunitiesSettings }
     *     
     */
    public KnowledgeCommunitiesSettings getArticlePublicSharingCommunities() {
        return articlePublicSharingCommunities;
    }

    /**
     * Sets the value of the articlePublicSharingCommunities property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeCommunitiesSettings }
     *     
     */
    public void setArticlePublicSharingCommunities(KnowledgeCommunitiesSettings value) {
        this.articlePublicSharingCommunities = value;
    }

    /**
     * Gets the value of the articlePublicSharingSites property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeSitesSettings }
     *     
     */
    public KnowledgeSitesSettings getArticlePublicSharingSites() {
        return articlePublicSharingSites;
    }

    /**
     * Sets the value of the articlePublicSharingSites property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeSitesSettings }
     *     
     */
    public void setArticlePublicSharingSites(KnowledgeSitesSettings value) {
        this.articlePublicSharingSites = value;
    }

    /**
     * Gets the value of the articlePublicSharingSitesChatterAnswers property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeSitesSettings }
     *     
     */
    public KnowledgeSitesSettings getArticlePublicSharingSitesChatterAnswers() {
        return articlePublicSharingSitesChatterAnswers;
    }

    /**
     * Sets the value of the articlePublicSharingSitesChatterAnswers property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeSitesSettings }
     *     
     */
    public void setArticlePublicSharingSitesChatterAnswers(KnowledgeSitesSettings value) {
        this.articlePublicSharingSitesChatterAnswers = value;
    }

    /**
     * Gets the value of the assignTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignTo() {
        return assignTo;
    }

    /**
     * Sets the value of the assignTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignTo(String value) {
        this.assignTo = value;
    }

    /**
     * Gets the value of the customizationClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomizationClass() {
        return customizationClass;
    }

    /**
     * Sets the value of the customizationClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomizationClass(String value) {
        this.customizationClass = value;
    }

    /**
     * Gets the value of the defaultContributionArticleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultContributionArticleType() {
        return defaultContributionArticleType;
    }

    /**
     * Sets the value of the defaultContributionArticleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultContributionArticleType(String value) {
        this.defaultContributionArticleType = value;
    }

    /**
     * Gets the value of the editor property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeCaseEditor }
     *     
     */
    public KnowledgeCaseEditor getEditor() {
        return editor;
    }

    /**
     * Sets the value of the editor property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeCaseEditor }
     *     
     */
    public void setEditor(KnowledgeCaseEditor value) {
        this.editor = value;
    }

    /**
     * Gets the value of the enableArticleCreation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableArticleCreation() {
        return enableArticleCreation;
    }

    /**
     * Sets the value of the enableArticleCreation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableArticleCreation(Boolean value) {
        this.enableArticleCreation = value;
    }

    /**
     * Gets the value of the enableArticlePublicSharingSites property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableArticlePublicSharingSites() {
        return enableArticlePublicSharingSites;
    }

    /**
     * Sets the value of the enableArticlePublicSharingSites property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableArticlePublicSharingSites(Boolean value) {
        this.enableArticlePublicSharingSites = value;
    }

    /**
     * Gets the value of the useProfileForPDFCreation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseProfileForPDFCreation() {
        return useProfileForPDFCreation;
    }

    /**
     * Sets the value of the useProfileForPDFCreation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseProfileForPDFCreation(Boolean value) {
        this.useProfileForPDFCreation = value;
    }

}
