
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlatformActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlatformActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="QuickAction"/&gt;
 *     &lt;enumeration value="StandardButton"/&gt;
 *     &lt;enumeration value="CustomButton"/&gt;
 *     &lt;enumeration value="ProductivityAction"/&gt;
 *     &lt;enumeration value="ActionLink"/&gt;
 *     &lt;enumeration value="InvocableAction"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PlatformActionType")
@XmlEnum
public enum PlatformActionType {

    @XmlEnumValue("QuickAction")
    QUICK_ACTION("QuickAction"),
    @XmlEnumValue("StandardButton")
    STANDARD_BUTTON("StandardButton"),
    @XmlEnumValue("CustomButton")
    CUSTOM_BUTTON("CustomButton"),
    @XmlEnumValue("ProductivityAction")
    PRODUCTIVITY_ACTION("ProductivityAction"),
    @XmlEnumValue("ActionLink")
    ACTION_LINK("ActionLink"),
    @XmlEnumValue("InvocableAction")
    INVOCABLE_ACTION("InvocableAction");
    private final String value;

    PlatformActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlatformActionType fromValue(String v) {
        for (PlatformActionType c: PlatformActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
