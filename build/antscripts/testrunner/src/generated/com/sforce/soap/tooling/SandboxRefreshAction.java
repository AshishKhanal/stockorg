
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SandboxRefreshAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SandboxRefreshAction"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ACTIVATE"/&gt;
 *     &lt;enumeration value="DISCARD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SandboxRefreshAction")
@XmlEnum
public enum SandboxRefreshAction {

    ACTIVATE,
    DISCARD;

    public String value() {
        return name();
    }

    public static SandboxRefreshAction fromValue(String v) {
        return valueOf(v);
    }

}
