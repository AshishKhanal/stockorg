
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CaseAssignNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CaseCloseNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CaseCommentNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CaseCreateNotificationTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CloseCaseThroughStatusChange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DefaultCaseOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DefaultCaseOwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DefaultCaseUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailActionDefaultsHandlerClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCaseFeedEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsDraftEmailsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEarlyEscalationRuleTriggersEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEmailActionDefaultsHandlerEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsNewEmailDefaultTemplateEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSuggestedArticlesApplicationEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSuggestedArticlesCustomerPortalEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSuggestedArticlesPartnerPortalEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSuggestedSolutionsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="KeepRecordTypeOnAssignmentRule" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}CaseSettings" minOccurs="0"/&gt;
 *         &lt;element name="NewEmailDefaultTemplateClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NotifyContactOnCaseComment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="NotifyDefaultCaseOwner" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="NotifyOwnerOnCaseComment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="NotifyOwnerOnCaseOwnerChange" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ShowFewerCloseActions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SystemUserEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UseSystemEmailAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="UseSystemUserAsDefaultCaseUser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CaseSettings", propOrder = {
    "caseAssignNotificationTemplate",
    "caseCloseNotificationTemplate",
    "caseCommentNotificationTemplate",
    "caseCreateNotificationTemplate",
    "closeCaseThroughStatusChange",
    "defaultCaseOwner",
    "defaultCaseOwnerType",
    "defaultCaseUser",
    "durableId",
    "emailActionDefaultsHandlerClass",
    "fullName",
    "isCaseFeedEnabled",
    "isDraftEmailsEnabled",
    "isEarlyEscalationRuleTriggersEnabled",
    "isEmailActionDefaultsHandlerEnabled",
    "isNewEmailDefaultTemplateEnabled",
    "isSuggestedArticlesApplicationEnabled",
    "isSuggestedArticlesCustomerPortalEnabled",
    "isSuggestedArticlesPartnerPortalEnabled",
    "isSuggestedSolutionsEnabled",
    "keepRecordTypeOnAssignmentRule",
    "metadata",
    "newEmailDefaultTemplateClass",
    "notifyContactOnCaseComment",
    "notifyDefaultCaseOwner",
    "notifyOwnerOnCaseComment",
    "notifyOwnerOnCaseOwnerChange",
    "showFewerCloseActions",
    "systemUserEmail",
    "useSystemEmailAddress",
    "useSystemUserAsDefaultCaseUser"
})
public class CaseSettings
    extends SObject
{

    @XmlElementRef(name = "CaseAssignNotificationTemplate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> caseAssignNotificationTemplate;
    @XmlElementRef(name = "CaseCloseNotificationTemplate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> caseCloseNotificationTemplate;
    @XmlElementRef(name = "CaseCommentNotificationTemplate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> caseCommentNotificationTemplate;
    @XmlElementRef(name = "CaseCreateNotificationTemplate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> caseCreateNotificationTemplate;
    @XmlElementRef(name = "CloseCaseThroughStatusChange", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> closeCaseThroughStatusChange;
    @XmlElementRef(name = "DefaultCaseOwner", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultCaseOwner;
    @XmlElementRef(name = "DefaultCaseOwnerType", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultCaseOwnerType;
    @XmlElementRef(name = "DefaultCaseUser", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultCaseUser;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "EmailActionDefaultsHandlerClass", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailActionDefaultsHandlerClass;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsCaseFeedEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCaseFeedEnabled;
    @XmlElementRef(name = "IsDraftEmailsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isDraftEmailsEnabled;
    @XmlElementRef(name = "IsEarlyEscalationRuleTriggersEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEarlyEscalationRuleTriggersEnabled;
    @XmlElementRef(name = "IsEmailActionDefaultsHandlerEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEmailActionDefaultsHandlerEnabled;
    @XmlElementRef(name = "IsNewEmailDefaultTemplateEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isNewEmailDefaultTemplateEnabled;
    @XmlElementRef(name = "IsSuggestedArticlesApplicationEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSuggestedArticlesApplicationEnabled;
    @XmlElementRef(name = "IsSuggestedArticlesCustomerPortalEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSuggestedArticlesCustomerPortalEnabled;
    @XmlElementRef(name = "IsSuggestedArticlesPartnerPortalEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSuggestedArticlesPartnerPortalEnabled;
    @XmlElementRef(name = "IsSuggestedSolutionsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSuggestedSolutionsEnabled;
    @XmlElementRef(name = "KeepRecordTypeOnAssignmentRule", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> keepRecordTypeOnAssignmentRule;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.CaseSettings> metadata;
    @XmlElementRef(name = "NewEmailDefaultTemplateClass", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newEmailDefaultTemplateClass;
    @XmlElementRef(name = "NotifyContactOnCaseComment", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyContactOnCaseComment;
    @XmlElementRef(name = "NotifyDefaultCaseOwner", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyDefaultCaseOwner;
    @XmlElementRef(name = "NotifyOwnerOnCaseComment", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyOwnerOnCaseComment;
    @XmlElementRef(name = "NotifyOwnerOnCaseOwnerChange", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyOwnerOnCaseOwnerChange;
    @XmlElementRef(name = "ShowFewerCloseActions", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showFewerCloseActions;
    @XmlElementRef(name = "SystemUserEmail", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> systemUserEmail;
    @XmlElementRef(name = "UseSystemEmailAddress", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useSystemEmailAddress;
    @XmlElementRef(name = "UseSystemUserAsDefaultCaseUser", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useSystemUserAsDefaultCaseUser;

    /**
     * Gets the value of the caseAssignNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCaseAssignNotificationTemplate() {
        return caseAssignNotificationTemplate;
    }

    /**
     * Sets the value of the caseAssignNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCaseAssignNotificationTemplate(JAXBElement<String> value) {
        this.caseAssignNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCloseNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCaseCloseNotificationTemplate() {
        return caseCloseNotificationTemplate;
    }

    /**
     * Sets the value of the caseCloseNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCaseCloseNotificationTemplate(JAXBElement<String> value) {
        this.caseCloseNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCommentNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCaseCommentNotificationTemplate() {
        return caseCommentNotificationTemplate;
    }

    /**
     * Sets the value of the caseCommentNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCaseCommentNotificationTemplate(JAXBElement<String> value) {
        this.caseCommentNotificationTemplate = value;
    }

    /**
     * Gets the value of the caseCreateNotificationTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCaseCreateNotificationTemplate() {
        return caseCreateNotificationTemplate;
    }

    /**
     * Sets the value of the caseCreateNotificationTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCaseCreateNotificationTemplate(JAXBElement<String> value) {
        this.caseCreateNotificationTemplate = value;
    }

    /**
     * Gets the value of the closeCaseThroughStatusChange property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getCloseCaseThroughStatusChange() {
        return closeCaseThroughStatusChange;
    }

    /**
     * Sets the value of the closeCaseThroughStatusChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setCloseCaseThroughStatusChange(JAXBElement<Boolean> value) {
        this.closeCaseThroughStatusChange = value;
    }

    /**
     * Gets the value of the defaultCaseOwner property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultCaseOwner() {
        return defaultCaseOwner;
    }

    /**
     * Sets the value of the defaultCaseOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultCaseOwner(JAXBElement<String> value) {
        this.defaultCaseOwner = value;
    }

    /**
     * Gets the value of the defaultCaseOwnerType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultCaseOwnerType() {
        return defaultCaseOwnerType;
    }

    /**
     * Sets the value of the defaultCaseOwnerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultCaseOwnerType(JAXBElement<String> value) {
        this.defaultCaseOwnerType = value;
    }

    /**
     * Gets the value of the defaultCaseUser property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultCaseUser() {
        return defaultCaseUser;
    }

    /**
     * Sets the value of the defaultCaseUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultCaseUser(JAXBElement<String> value) {
        this.defaultCaseUser = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the emailActionDefaultsHandlerClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailActionDefaultsHandlerClass() {
        return emailActionDefaultsHandlerClass;
    }

    /**
     * Sets the value of the emailActionDefaultsHandlerClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailActionDefaultsHandlerClass(JAXBElement<String> value) {
        this.emailActionDefaultsHandlerClass = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isCaseFeedEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCaseFeedEnabled() {
        return isCaseFeedEnabled;
    }

    /**
     * Sets the value of the isCaseFeedEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCaseFeedEnabled(JAXBElement<Boolean> value) {
        this.isCaseFeedEnabled = value;
    }

    /**
     * Gets the value of the isDraftEmailsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsDraftEmailsEnabled() {
        return isDraftEmailsEnabled;
    }

    /**
     * Sets the value of the isDraftEmailsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsDraftEmailsEnabled(JAXBElement<Boolean> value) {
        this.isDraftEmailsEnabled = value;
    }

    /**
     * Gets the value of the isEarlyEscalationRuleTriggersEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEarlyEscalationRuleTriggersEnabled() {
        return isEarlyEscalationRuleTriggersEnabled;
    }

    /**
     * Sets the value of the isEarlyEscalationRuleTriggersEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEarlyEscalationRuleTriggersEnabled(JAXBElement<Boolean> value) {
        this.isEarlyEscalationRuleTriggersEnabled = value;
    }

    /**
     * Gets the value of the isEmailActionDefaultsHandlerEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEmailActionDefaultsHandlerEnabled() {
        return isEmailActionDefaultsHandlerEnabled;
    }

    /**
     * Sets the value of the isEmailActionDefaultsHandlerEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEmailActionDefaultsHandlerEnabled(JAXBElement<Boolean> value) {
        this.isEmailActionDefaultsHandlerEnabled = value;
    }

    /**
     * Gets the value of the isNewEmailDefaultTemplateEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsNewEmailDefaultTemplateEnabled() {
        return isNewEmailDefaultTemplateEnabled;
    }

    /**
     * Sets the value of the isNewEmailDefaultTemplateEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsNewEmailDefaultTemplateEnabled(JAXBElement<Boolean> value) {
        this.isNewEmailDefaultTemplateEnabled = value;
    }

    /**
     * Gets the value of the isSuggestedArticlesApplicationEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSuggestedArticlesApplicationEnabled() {
        return isSuggestedArticlesApplicationEnabled;
    }

    /**
     * Sets the value of the isSuggestedArticlesApplicationEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSuggestedArticlesApplicationEnabled(JAXBElement<Boolean> value) {
        this.isSuggestedArticlesApplicationEnabled = value;
    }

    /**
     * Gets the value of the isSuggestedArticlesCustomerPortalEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSuggestedArticlesCustomerPortalEnabled() {
        return isSuggestedArticlesCustomerPortalEnabled;
    }

    /**
     * Sets the value of the isSuggestedArticlesCustomerPortalEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSuggestedArticlesCustomerPortalEnabled(JAXBElement<Boolean> value) {
        this.isSuggestedArticlesCustomerPortalEnabled = value;
    }

    /**
     * Gets the value of the isSuggestedArticlesPartnerPortalEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSuggestedArticlesPartnerPortalEnabled() {
        return isSuggestedArticlesPartnerPortalEnabled;
    }

    /**
     * Sets the value of the isSuggestedArticlesPartnerPortalEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSuggestedArticlesPartnerPortalEnabled(JAXBElement<Boolean> value) {
        this.isSuggestedArticlesPartnerPortalEnabled = value;
    }

    /**
     * Gets the value of the isSuggestedSolutionsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSuggestedSolutionsEnabled() {
        return isSuggestedSolutionsEnabled;
    }

    /**
     * Sets the value of the isSuggestedSolutionsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSuggestedSolutionsEnabled(JAXBElement<Boolean> value) {
        this.isSuggestedSolutionsEnabled = value;
    }

    /**
     * Gets the value of the keepRecordTypeOnAssignmentRule property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getKeepRecordTypeOnAssignmentRule() {
        return keepRecordTypeOnAssignmentRule;
    }

    /**
     * Sets the value of the keepRecordTypeOnAssignmentRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setKeepRecordTypeOnAssignmentRule(JAXBElement<Boolean> value) {
        this.keepRecordTypeOnAssignmentRule = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.CaseSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.CaseSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.CaseSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.CaseSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the newEmailDefaultTemplateClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewEmailDefaultTemplateClass() {
        return newEmailDefaultTemplateClass;
    }

    /**
     * Sets the value of the newEmailDefaultTemplateClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewEmailDefaultTemplateClass(JAXBElement<String> value) {
        this.newEmailDefaultTemplateClass = value;
    }

    /**
     * Gets the value of the notifyContactOnCaseComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyContactOnCaseComment() {
        return notifyContactOnCaseComment;
    }

    /**
     * Sets the value of the notifyContactOnCaseComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyContactOnCaseComment(JAXBElement<Boolean> value) {
        this.notifyContactOnCaseComment = value;
    }

    /**
     * Gets the value of the notifyDefaultCaseOwner property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyDefaultCaseOwner() {
        return notifyDefaultCaseOwner;
    }

    /**
     * Sets the value of the notifyDefaultCaseOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyDefaultCaseOwner(JAXBElement<Boolean> value) {
        this.notifyDefaultCaseOwner = value;
    }

    /**
     * Gets the value of the notifyOwnerOnCaseComment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyOwnerOnCaseComment() {
        return notifyOwnerOnCaseComment;
    }

    /**
     * Sets the value of the notifyOwnerOnCaseComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyOwnerOnCaseComment(JAXBElement<Boolean> value) {
        this.notifyOwnerOnCaseComment = value;
    }

    /**
     * Gets the value of the notifyOwnerOnCaseOwnerChange property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyOwnerOnCaseOwnerChange() {
        return notifyOwnerOnCaseOwnerChange;
    }

    /**
     * Sets the value of the notifyOwnerOnCaseOwnerChange property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyOwnerOnCaseOwnerChange(JAXBElement<Boolean> value) {
        this.notifyOwnerOnCaseOwnerChange = value;
    }

    /**
     * Gets the value of the showFewerCloseActions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowFewerCloseActions() {
        return showFewerCloseActions;
    }

    /**
     * Sets the value of the showFewerCloseActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowFewerCloseActions(JAXBElement<Boolean> value) {
        this.showFewerCloseActions = value;
    }

    /**
     * Gets the value of the systemUserEmail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSystemUserEmail() {
        return systemUserEmail;
    }

    /**
     * Sets the value of the systemUserEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSystemUserEmail(JAXBElement<String> value) {
        this.systemUserEmail = value;
    }

    /**
     * Gets the value of the useSystemEmailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseSystemEmailAddress() {
        return useSystemEmailAddress;
    }

    /**
     * Sets the value of the useSystemEmailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseSystemEmailAddress(JAXBElement<Boolean> value) {
        this.useSystemEmailAddress = value;
    }

    /**
     * Gets the value of the useSystemUserAsDefaultCaseUser property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseSystemUserAsDefaultCaseUser() {
        return useSystemUserAsDefaultCaseUser;
    }

    /**
     * Sets the value of the useSystemUserAsDefaultCaseUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseSystemUserAsDefaultCaseUser(JAXBElement<Boolean> value) {
        this.useSystemUserAsDefaultCaseUser = value;
    }

}
