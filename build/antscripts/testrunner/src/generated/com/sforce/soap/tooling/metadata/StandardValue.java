
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.ForecastCategories;


/**
 * <p>Java class for StandardValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}CustomValue"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="closed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="converted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="cssExposed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forecastCategory" type="{urn:tooling.soap.sforce.com}ForecastCategories" minOccurs="0"/&gt;
 *         &lt;element name="highPriority" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="probability" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="reverseRole" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reviewed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="won" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardValue", propOrder = {
    "allowEmail",
    "closed",
    "converted",
    "cssExposed",
    "forecastCategory",
    "highPriority",
    "probability",
    "reverseRole",
    "reviewed",
    "won"
})
public class StandardValue
    extends CustomValue
{

    protected Boolean allowEmail;
    protected Boolean closed;
    protected Boolean converted;
    protected Boolean cssExposed;
    @XmlSchemaType(name = "string")
    protected ForecastCategories forecastCategory;
    protected Boolean highPriority;
    protected Integer probability;
    protected String reverseRole;
    protected Boolean reviewed;
    protected Boolean won;

    /**
     * Gets the value of the allowEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowEmail() {
        return allowEmail;
    }

    /**
     * Sets the value of the allowEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowEmail(Boolean value) {
        this.allowEmail = value;
    }

    /**
     * Gets the value of the closed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClosed() {
        return closed;
    }

    /**
     * Sets the value of the closed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClosed(Boolean value) {
        this.closed = value;
    }

    /**
     * Gets the value of the converted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isConverted() {
        return converted;
    }

    /**
     * Sets the value of the converted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setConverted(Boolean value) {
        this.converted = value;
    }

    /**
     * Gets the value of the cssExposed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCssExposed() {
        return cssExposed;
    }

    /**
     * Sets the value of the cssExposed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCssExposed(Boolean value) {
        this.cssExposed = value;
    }

    /**
     * Gets the value of the forecastCategory property.
     * 
     * @return
     *     possible object is
     *     {@link ForecastCategories }
     *     
     */
    public ForecastCategories getForecastCategory() {
        return forecastCategory;
    }

    /**
     * Sets the value of the forecastCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ForecastCategories }
     *     
     */
    public void setForecastCategory(ForecastCategories value) {
        this.forecastCategory = value;
    }

    /**
     * Gets the value of the highPriority property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHighPriority() {
        return highPriority;
    }

    /**
     * Sets the value of the highPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHighPriority(Boolean value) {
        this.highPriority = value;
    }

    /**
     * Gets the value of the probability property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProbability() {
        return probability;
    }

    /**
     * Sets the value of the probability property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProbability(Integer value) {
        this.probability = value;
    }

    /**
     * Gets the value of the reverseRole property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReverseRole() {
        return reverseRole;
    }

    /**
     * Sets the value of the reverseRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReverseRole(String value) {
        this.reverseRole = value;
    }

    /**
     * Gets the value of the reviewed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReviewed() {
        return reviewed;
    }

    /**
     * Sets the value of the reviewed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReviewed(Boolean value) {
        this.reviewed = value;
    }

    /**
     * Gets the value of the won property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWon() {
        return won;
    }

    /**
     * Sets the value of the won property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWon(Boolean value) {
        this.won = value;
    }

}
