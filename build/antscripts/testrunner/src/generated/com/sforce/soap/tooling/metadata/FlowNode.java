
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowNode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlowNode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}FlowElement"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="locationX" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="locationY" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowNode", propOrder = {
    "label",
    "locationX",
    "locationY"
})
@XmlSeeAlso({
    FlowActionCall.class,
    FlowApexPluginCall.class,
    FlowAssignment.class,
    FlowDecision.class,
    FlowLoop.class,
    FlowRecordCreate.class,
    FlowRecordDelete.class,
    FlowRecordLookup.class,
    FlowRecordUpdate.class,
    FlowScreen.class,
    FlowStep.class,
    FlowSubflow.class,
    FlowWait.class
})
public class FlowNode
    extends FlowElement
{

    @XmlElement(required = true)
    protected String label;
    protected int locationX;
    protected int locationY;

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the locationX property.
     * 
     */
    public int getLocationX() {
        return locationX;
    }

    /**
     * Sets the value of the locationX property.
     * 
     */
    public void setLocationX(int value) {
        this.locationX = value;
    }

    /**
     * Gets the value of the locationY property.
     * 
     */
    public int getLocationY() {
        return locationY;
    }

    /**
     * Sets the value of the locationY property.
     * 
     */
    public void setLocationY(int value) {
        this.locationY = value;
    }

}
