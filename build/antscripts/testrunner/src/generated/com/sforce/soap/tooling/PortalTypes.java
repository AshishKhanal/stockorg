
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PortalTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PortalTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="CustomerPortal"/&gt;
 *     &lt;enumeration value="Partner"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PortalTypes")
@XmlEnum
public enum PortalTypes {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("CustomerPortal")
    CUSTOMER_PORTAL("CustomerPortal"),
    @XmlEnumValue("Partner")
    PARTNER("Partner");
    private final String value;

    PortalTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PortalTypes fromValue(String v) {
        for (PortalTypes c: PortalTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
