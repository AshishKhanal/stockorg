
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ModerationRuleAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ModerationRuleAction"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Block"/&gt;
 *     &lt;enumeration value="FreezeAndNotify"/&gt;
 *     &lt;enumeration value="Review"/&gt;
 *     &lt;enumeration value="Replace"/&gt;
 *     &lt;enumeration value="Flag"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ModerationRuleAction")
@XmlEnum
public enum ModerationRuleAction {

    @XmlEnumValue("Block")
    BLOCK("Block"),
    @XmlEnumValue("FreezeAndNotify")
    FREEZE_AND_NOTIFY("FreezeAndNotify"),
    @XmlEnumValue("Review")
    REVIEW("Review"),
    @XmlEnumValue("Replace")
    REPLACE("Replace"),
    @XmlEnumValue("Flag")
    FLAG("Flag");
    private final String value;

    ModerationRuleAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ModerationRuleAction fromValue(String v) {
        for (ModerationRuleAction c: ModerationRuleAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
