
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.CommunityThemeLayoutType;


/**
 * <p>Java class for CommunityThemeSetting complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunityThemeSetting"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customThemeLayoutType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="themeLayout" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="themeLayoutType" type="{urn:tooling.soap.sforce.com}CommunityThemeLayoutType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunityThemeSetting", propOrder = {
    "customThemeLayoutType",
    "themeLayout",
    "themeLayoutType"
})
public class CommunityThemeSetting {

    protected String customThemeLayoutType;
    @XmlElement(required = true)
    protected String themeLayout;
    @XmlSchemaType(name = "string")
    protected CommunityThemeLayoutType themeLayoutType;

    /**
     * Gets the value of the customThemeLayoutType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomThemeLayoutType() {
        return customThemeLayoutType;
    }

    /**
     * Sets the value of the customThemeLayoutType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomThemeLayoutType(String value) {
        this.customThemeLayoutType = value;
    }

    /**
     * Gets the value of the themeLayout property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThemeLayout() {
        return themeLayout;
    }

    /**
     * Sets the value of the themeLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThemeLayout(String value) {
        this.themeLayout = value;
    }

    /**
     * Gets the value of the themeLayoutType property.
     * 
     * @return
     *     possible object is
     *     {@link CommunityThemeLayoutType }
     *     
     */
    public CommunityThemeLayoutType getThemeLayoutType() {
        return themeLayoutType;
    }

    /**
     * Sets the value of the themeLayoutType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommunityThemeLayoutType }
     *     
     */
    public void setThemeLayoutType(CommunityThemeLayoutType value) {
        this.themeLayoutType = value;
    }

}
