
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChatterAnswersSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChatterAnswersSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailFollowersOnBestAnswer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EmailFollowersOnReply" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EmailOwnerOnPrivateReply" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="EmailOwnerOnReply" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="FacebookAuthProvider" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAnswerViaEmailEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsChatterAnswersEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsFacebookSsoEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsInlinePublisherEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsOptimizeQuestionFlowEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsReputationEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRichTextEditorEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}ChatterAnswersSettings" minOccurs="0"/&gt;
 *         &lt;element name="ShowInPortals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChatterAnswersSettings", propOrder = {
    "durableId",
    "emailFollowersOnBestAnswer",
    "emailFollowersOnReply",
    "emailOwnerOnPrivateReply",
    "emailOwnerOnReply",
    "facebookAuthProvider",
    "fullName",
    "isAnswerViaEmailEnabled",
    "isChatterAnswersEnabled",
    "isFacebookSsoEnabled",
    "isInlinePublisherEnabled",
    "isOptimizeQuestionFlowEnabled",
    "isReputationEnabled",
    "isRichTextEditorEnabled",
    "metadata",
    "showInPortals"
})
public class ChatterAnswersSettings
    extends SObject
{

    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "EmailFollowersOnBestAnswer", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> emailFollowersOnBestAnswer;
    @XmlElementRef(name = "EmailFollowersOnReply", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> emailFollowersOnReply;
    @XmlElementRef(name = "EmailOwnerOnPrivateReply", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> emailOwnerOnPrivateReply;
    @XmlElementRef(name = "EmailOwnerOnReply", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> emailOwnerOnReply;
    @XmlElementRef(name = "FacebookAuthProvider", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> facebookAuthProvider;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsAnswerViaEmailEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isAnswerViaEmailEnabled;
    @XmlElementRef(name = "IsChatterAnswersEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isChatterAnswersEnabled;
    @XmlElementRef(name = "IsFacebookSsoEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isFacebookSsoEnabled;
    @XmlElementRef(name = "IsInlinePublisherEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isInlinePublisherEnabled;
    @XmlElementRef(name = "IsOptimizeQuestionFlowEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOptimizeQuestionFlowEnabled;
    @XmlElementRef(name = "IsReputationEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isReputationEnabled;
    @XmlElementRef(name = "IsRichTextEditorEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRichTextEditorEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.ChatterAnswersSettings> metadata;
    @XmlElementRef(name = "ShowInPortals", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> showInPortals;

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the emailFollowersOnBestAnswer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEmailFollowersOnBestAnswer() {
        return emailFollowersOnBestAnswer;
    }

    /**
     * Sets the value of the emailFollowersOnBestAnswer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEmailFollowersOnBestAnswer(JAXBElement<Boolean> value) {
        this.emailFollowersOnBestAnswer = value;
    }

    /**
     * Gets the value of the emailFollowersOnReply property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEmailFollowersOnReply() {
        return emailFollowersOnReply;
    }

    /**
     * Sets the value of the emailFollowersOnReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEmailFollowersOnReply(JAXBElement<Boolean> value) {
        this.emailFollowersOnReply = value;
    }

    /**
     * Gets the value of the emailOwnerOnPrivateReply property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEmailOwnerOnPrivateReply() {
        return emailOwnerOnPrivateReply;
    }

    /**
     * Sets the value of the emailOwnerOnPrivateReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEmailOwnerOnPrivateReply(JAXBElement<Boolean> value) {
        this.emailOwnerOnPrivateReply = value;
    }

    /**
     * Gets the value of the emailOwnerOnReply property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getEmailOwnerOnReply() {
        return emailOwnerOnReply;
    }

    /**
     * Sets the value of the emailOwnerOnReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setEmailOwnerOnReply(JAXBElement<Boolean> value) {
        this.emailOwnerOnReply = value;
    }

    /**
     * Gets the value of the facebookAuthProvider property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFacebookAuthProvider() {
        return facebookAuthProvider;
    }

    /**
     * Sets the value of the facebookAuthProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFacebookAuthProvider(JAXBElement<String> value) {
        this.facebookAuthProvider = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isAnswerViaEmailEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsAnswerViaEmailEnabled() {
        return isAnswerViaEmailEnabled;
    }

    /**
     * Sets the value of the isAnswerViaEmailEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsAnswerViaEmailEnabled(JAXBElement<Boolean> value) {
        this.isAnswerViaEmailEnabled = value;
    }

    /**
     * Gets the value of the isChatterAnswersEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsChatterAnswersEnabled() {
        return isChatterAnswersEnabled;
    }

    /**
     * Sets the value of the isChatterAnswersEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsChatterAnswersEnabled(JAXBElement<Boolean> value) {
        this.isChatterAnswersEnabled = value;
    }

    /**
     * Gets the value of the isFacebookSsoEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsFacebookSsoEnabled() {
        return isFacebookSsoEnabled;
    }

    /**
     * Sets the value of the isFacebookSsoEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsFacebookSsoEnabled(JAXBElement<Boolean> value) {
        this.isFacebookSsoEnabled = value;
    }

    /**
     * Gets the value of the isInlinePublisherEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsInlinePublisherEnabled() {
        return isInlinePublisherEnabled;
    }

    /**
     * Sets the value of the isInlinePublisherEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsInlinePublisherEnabled(JAXBElement<Boolean> value) {
        this.isInlinePublisherEnabled = value;
    }

    /**
     * Gets the value of the isOptimizeQuestionFlowEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOptimizeQuestionFlowEnabled() {
        return isOptimizeQuestionFlowEnabled;
    }

    /**
     * Sets the value of the isOptimizeQuestionFlowEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOptimizeQuestionFlowEnabled(JAXBElement<Boolean> value) {
        this.isOptimizeQuestionFlowEnabled = value;
    }

    /**
     * Gets the value of the isReputationEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsReputationEnabled() {
        return isReputationEnabled;
    }

    /**
     * Sets the value of the isReputationEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsReputationEnabled(JAXBElement<Boolean> value) {
        this.isReputationEnabled = value;
    }

    /**
     * Gets the value of the isRichTextEditorEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRichTextEditorEnabled() {
        return isRichTextEditorEnabled;
    }

    /**
     * Sets the value of the isRichTextEditorEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRichTextEditorEnabled(JAXBElement<Boolean> value) {
        this.isRichTextEditorEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ChatterAnswersSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.ChatterAnswersSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.ChatterAnswersSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.ChatterAnswersSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the showInPortals property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getShowInPortals() {
        return showInPortals;
    }

    /**
     * Sets the value of the showInPortals property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setShowInPortals(JAXBElement<Boolean> value) {
        this.showInPortals = value;
    }

}
