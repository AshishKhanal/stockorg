
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExternalActionParameterType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ExternalActionParameterType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HEADER"/&gt;
 *     &lt;enumeration value="PAYLOAD"/&gt;
 *     &lt;enumeration value="URL"/&gt;
 *     &lt;enumeration value="RETURN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ExternalActionParameterType")
@XmlEnum
public enum ExternalActionParameterType {

    HEADER,
    PAYLOAD,
    URL,
    RETURN;

    public String value() {
        return name();
    }

    public static ExternalActionParameterType fromValue(String v) {
        return valueOf(v);
    }

}
