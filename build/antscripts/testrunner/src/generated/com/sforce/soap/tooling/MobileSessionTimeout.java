
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MobileSessionTimeout.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MobileSessionTimeout"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Never"/&gt;
 *     &lt;enumeration value="OneMinute"/&gt;
 *     &lt;enumeration value="FiveMinutes"/&gt;
 *     &lt;enumeration value="TenMinutes"/&gt;
 *     &lt;enumeration value="ThirtyMinutes"/&gt;
 *     &lt;enumeration value="SixtyMinutes"/&gt;
 *     &lt;enumeration value="OneTwentyMinutes"/&gt;
 *     &lt;enumeration value="OneEightyMinutes"/&gt;
 *     &lt;enumeration value="TwoFortyMinutes"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MobileSessionTimeout")
@XmlEnum
public enum MobileSessionTimeout {

    @XmlEnumValue("Never")
    NEVER("Never"),
    @XmlEnumValue("OneMinute")
    ONE_MINUTE("OneMinute"),
    @XmlEnumValue("FiveMinutes")
    FIVE_MINUTES("FiveMinutes"),
    @XmlEnumValue("TenMinutes")
    TEN_MINUTES("TenMinutes"),
    @XmlEnumValue("ThirtyMinutes")
    THIRTY_MINUTES("ThirtyMinutes"),
    @XmlEnumValue("SixtyMinutes")
    SIXTY_MINUTES("SixtyMinutes"),
    @XmlEnumValue("OneTwentyMinutes")
    ONE_TWENTY_MINUTES("OneTwentyMinutes"),
    @XmlEnumValue("OneEightyMinutes")
    ONE_EIGHTY_MINUTES("OneEightyMinutes"),
    @XmlEnumValue("TwoFortyMinutes")
    TWO_FORTY_MINUTES("TwoFortyMinutes");
    private final String value;

    MobileSessionTimeout(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MobileSessionTimeout fromValue(String v) {
        for (MobileSessionTimeout c: MobileSessionTimeout.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
