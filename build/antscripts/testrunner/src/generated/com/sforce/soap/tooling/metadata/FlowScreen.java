
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowScreen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlowScreen"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}FlowNode"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowBack" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowFinish" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowPause" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="connector" type="{urn:metadata.tooling.soap.sforce.com}FlowConnector" minOccurs="0"/&gt;
 *         &lt;element name="fields" type="{urn:metadata.tooling.soap.sforce.com}FlowScreenField" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="helpText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pausedText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="rules" type="{urn:metadata.tooling.soap.sforce.com}FlowScreenRule" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlowScreen", propOrder = {
    "allowBack",
    "allowFinish",
    "allowPause",
    "connector",
    "fields",
    "helpText",
    "pausedText",
    "rules"
})
public class FlowScreen
    extends FlowNode
{

    protected Boolean allowBack;
    protected Boolean allowFinish;
    protected Boolean allowPause;
    protected FlowConnector connector;
    protected List<FlowScreenField> fields;
    protected String helpText;
    protected String pausedText;
    protected List<FlowScreenRule> rules;

    /**
     * Gets the value of the allowBack property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowBack() {
        return allowBack;
    }

    /**
     * Sets the value of the allowBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowBack(Boolean value) {
        this.allowBack = value;
    }

    /**
     * Gets the value of the allowFinish property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowFinish() {
        return allowFinish;
    }

    /**
     * Sets the value of the allowFinish property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowFinish(Boolean value) {
        this.allowFinish = value;
    }

    /**
     * Gets the value of the allowPause property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPause() {
        return allowPause;
    }

    /**
     * Sets the value of the allowPause property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPause(Boolean value) {
        this.allowPause = value;
    }

    /**
     * Gets the value of the connector property.
     * 
     * @return
     *     possible object is
     *     {@link FlowConnector }
     *     
     */
    public FlowConnector getConnector() {
        return connector;
    }

    /**
     * Sets the value of the connector property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlowConnector }
     *     
     */
    public void setConnector(FlowConnector value) {
        this.connector = value;
    }

    /**
     * Gets the value of the fields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlowScreenField }
     * 
     * 
     */
    public List<FlowScreenField> getFields() {
        if (fields == null) {
            fields = new ArrayList<FlowScreenField>();
        }
        return this.fields;
    }

    /**
     * Gets the value of the helpText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpText() {
        return helpText;
    }

    /**
     * Sets the value of the helpText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpText(String value) {
        this.helpText = value;
    }

    /**
     * Gets the value of the pausedText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPausedText() {
        return pausedText;
    }

    /**
     * Sets the value of the pausedText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPausedText(String value) {
        this.pausedText = value;
    }

    /**
     * Gets the value of the rules property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rules property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRules().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FlowScreenRule }
     * 
     * 
     */
    public List<FlowScreenRule> getRules() {
        if (rules == null) {
            rules = new ArrayList<FlowScreenRule>();
        }
        return this.rules;
    }

}
