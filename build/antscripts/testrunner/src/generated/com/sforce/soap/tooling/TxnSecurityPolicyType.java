
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TxnSecurityPolicyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TxnSecurityPolicyType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CustomApexPolicy"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TxnSecurityPolicyType")
@XmlEnum
public enum TxnSecurityPolicyType {

    @XmlEnumValue("CustomApexPolicy")
    CUSTOM_APEX_POLICY("CustomApexPolicy");
    private final String value;

    TxnSecurityPolicyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TxnSecurityPolicyType fromValue(String v) {
        for (TxnSecurityPolicyType c: TxnSecurityPolicyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
