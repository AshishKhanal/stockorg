
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LmaApiSessionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LmaApiSessionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Debugger"/&gt;
 *     &lt;enumeration value="Profiler"/&gt;
 *     &lt;enumeration value="Logging"/&gt;
 *     &lt;enumeration value="Real"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LmaApiSessionType")
@XmlEnum
public enum LmaApiSessionType {

    @XmlEnumValue("Debugger")
    DEBUGGER("Debugger"),
    @XmlEnumValue("Profiler")
    PROFILER("Profiler"),
    @XmlEnumValue("Logging")
    LOGGING("Logging"),
    @XmlEnumValue("Real")
    REAL("Real");
    private final String value;

    LmaApiSessionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LmaApiSessionType fromValue(String v) {
        for (LmaApiSessionType c: LmaApiSessionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
