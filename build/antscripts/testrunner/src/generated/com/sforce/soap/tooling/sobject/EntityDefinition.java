
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.sforce.soap.tooling.QueryResult;
import com.sforce.soap.tooling.RecordTypesSupported;
import com.sforce.soap.tooling.metadata.CustomObject;


/**
 * <p>Java class for EntityDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EntityDefinition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApexTriggers" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="AssignmentRules" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="AutoResponseRules" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="BusinessProcesses" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="ChildRelationships" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="CompactLayouts" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="CustomFields" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="DefaultCompactLayout" type="{urn:sobject.tooling.soap.sforce.com}CompactLayoutInfo" minOccurs="0"/&gt;
 *         &lt;element name="DefaultCompactLayoutId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DetailUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeveloperName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EditDefinitionUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EditUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExternalSharingModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FieldSets" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="Fields" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HasSubtypes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="HelpSettingPageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HelpSettingPageUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InternalSharingModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsActivityTrackable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsApexTriggerable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCompactLayoutable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCustomSetting" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsCustomizable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsDeprecatedAndHidden" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEverCreatable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEverDeletable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsEverUpdatable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsFeedEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsFieldHistoryTracked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsFlsEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsIdEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsLayoutable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsMruEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsProcessEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsQueryable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsReplicateable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsReportingEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRetrieveable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSearchLayoutable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSearchable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsSubtype" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsTriggerable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsWorkflowEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="KeyPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedBy" type="{urn:sobject.tooling.soap.sforce.com}User" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedById" type="{urn:tooling.soap.sforce.com}ID" minOccurs="0"/&gt;
 *         &lt;element name="LastModifiedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="Layouts" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="Limits" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="LookupFilters" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="MasterLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}CustomObject" minOccurs="0"/&gt;
 *         &lt;element name="NamespacePrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NewUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OwnerChangeOptions" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="Particles" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="PluralLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PostTemplates" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="Publisher" type="{urn:sobject.tooling.soap.sforce.com}Publisher" minOccurs="0"/&gt;
 *         &lt;element name="PublisherId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QualifiedApiName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuickActionDefinitions" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="RecordTypes" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="RecordTypesSupported" type="{urn:tooling.soap.sforce.com}RecordTypesSupported" minOccurs="0"/&gt;
 *         &lt;element name="RelationshipDomains" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="RunningUserEntityAccess" type="{urn:sobject.tooling.soap.sforce.com}UserEntityAccess" minOccurs="0"/&gt;
 *         &lt;element name="RunningUserEntityAccessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SearchLayouts" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="StandardActions" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="ValidationRules" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WebLinks" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowAlerts" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowFieldUpdates" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowOutboundMessages" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowTasks" type="{urn:tooling.soap.sforce.com}QueryResult" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityDefinition", propOrder = {
    "apexTriggers",
    "assignmentRules",
    "autoResponseRules",
    "businessProcesses",
    "childRelationships",
    "compactLayouts",
    "customFields",
    "defaultCompactLayout",
    "defaultCompactLayoutId",
    "deploymentStatus",
    "description",
    "detailUrl",
    "developerName",
    "durableId",
    "editDefinitionUrl",
    "editUrl",
    "externalSharingModel",
    "fieldSets",
    "fields",
    "fullName",
    "hasSubtypes",
    "helpSettingPageName",
    "helpSettingPageUrl",
    "internalSharingModel",
    "isActivityTrackable",
    "isApexTriggerable",
    "isCompactLayoutable",
    "isCustomSetting",
    "isCustomizable",
    "isDeprecatedAndHidden",
    "isEverCreatable",
    "isEverDeletable",
    "isEverUpdatable",
    "isFeedEnabled",
    "isFieldHistoryTracked",
    "isFlsEnabled",
    "isIdEnabled",
    "isLayoutable",
    "isMruEnabled",
    "isProcessEnabled",
    "isQueryable",
    "isReplicateable",
    "isReportingEnabled",
    "isRetrieveable",
    "isSearchLayoutable",
    "isSearchable",
    "isSubtype",
    "isTriggerable",
    "isWorkflowEnabled",
    "keyPrefix",
    "label",
    "lastModifiedBy",
    "lastModifiedById",
    "lastModifiedDate",
    "layouts",
    "limits",
    "lookupFilters",
    "masterLabel",
    "metadata",
    "namespacePrefix",
    "newUrl",
    "ownerChangeOptions",
    "particles",
    "pluralLabel",
    "postTemplates",
    "publisher",
    "publisherId",
    "qualifiedApiName",
    "quickActionDefinitions",
    "recordTypes",
    "recordTypesSupported",
    "relationshipDomains",
    "runningUserEntityAccess",
    "runningUserEntityAccessId",
    "searchLayouts",
    "standardActions",
    "validationRules",
    "webLinks",
    "workflowAlerts",
    "workflowFieldUpdates",
    "workflowOutboundMessages",
    "workflowTasks"
})
public class EntityDefinition
    extends SObject
{

    @XmlElementRef(name = "ApexTriggers", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> apexTriggers;
    @XmlElementRef(name = "AssignmentRules", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> assignmentRules;
    @XmlElementRef(name = "AutoResponseRules", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> autoResponseRules;
    @XmlElementRef(name = "BusinessProcesses", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> businessProcesses;
    @XmlElementRef(name = "ChildRelationships", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> childRelationships;
    @XmlElementRef(name = "CompactLayouts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> compactLayouts;
    @XmlElementRef(name = "CustomFields", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> customFields;
    @XmlElementRef(name = "DefaultCompactLayout", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<CompactLayoutInfo> defaultCompactLayout;
    @XmlElementRef(name = "DefaultCompactLayoutId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultCompactLayoutId;
    @XmlElementRef(name = "DeploymentStatus", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deploymentStatus;
    @XmlElementRef(name = "Description", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "DetailUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> detailUrl;
    @XmlElementRef(name = "DeveloperName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> developerName;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "EditDefinitionUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> editDefinitionUrl;
    @XmlElementRef(name = "EditUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> editUrl;
    @XmlElementRef(name = "ExternalSharingModel", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> externalSharingModel;
    @XmlElementRef(name = "FieldSets", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> fieldSets;
    @XmlElementRef(name = "Fields", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> fields;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "HasSubtypes", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> hasSubtypes;
    @XmlElementRef(name = "HelpSettingPageName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> helpSettingPageName;
    @XmlElementRef(name = "HelpSettingPageUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> helpSettingPageUrl;
    @XmlElementRef(name = "InternalSharingModel", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> internalSharingModel;
    @XmlElementRef(name = "IsActivityTrackable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isActivityTrackable;
    @XmlElementRef(name = "IsApexTriggerable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isApexTriggerable;
    @XmlElementRef(name = "IsCompactLayoutable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCompactLayoutable;
    @XmlElementRef(name = "IsCustomSetting", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCustomSetting;
    @XmlElementRef(name = "IsCustomizable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCustomizable;
    @XmlElementRef(name = "IsDeprecatedAndHidden", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isDeprecatedAndHidden;
    @XmlElementRef(name = "IsEverCreatable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEverCreatable;
    @XmlElementRef(name = "IsEverDeletable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEverDeletable;
    @XmlElementRef(name = "IsEverUpdatable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEverUpdatable;
    @XmlElementRef(name = "IsFeedEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isFeedEnabled;
    @XmlElementRef(name = "IsFieldHistoryTracked", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isFieldHistoryTracked;
    @XmlElementRef(name = "IsFlsEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isFlsEnabled;
    @XmlElementRef(name = "IsIdEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isIdEnabled;
    @XmlElementRef(name = "IsLayoutable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isLayoutable;
    @XmlElementRef(name = "IsMruEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isMruEnabled;
    @XmlElementRef(name = "IsProcessEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isProcessEnabled;
    @XmlElementRef(name = "IsQueryable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isQueryable;
    @XmlElementRef(name = "IsReplicateable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isReplicateable;
    @XmlElementRef(name = "IsReportingEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isReportingEnabled;
    @XmlElementRef(name = "IsRetrieveable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRetrieveable;
    @XmlElementRef(name = "IsSearchLayoutable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSearchLayoutable;
    @XmlElementRef(name = "IsSearchable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSearchable;
    @XmlElementRef(name = "IsSubtype", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSubtype;
    @XmlElementRef(name = "IsTriggerable", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isTriggerable;
    @XmlElementRef(name = "IsWorkflowEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isWorkflowEnabled;
    @XmlElementRef(name = "KeyPrefix", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> keyPrefix;
    @XmlElementRef(name = "Label", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> label;
    @XmlElementRef(name = "LastModifiedBy", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<User> lastModifiedBy;
    @XmlElementRef(name = "LastModifiedById", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastModifiedById;
    @XmlElementRef(name = "LastModifiedDate", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> lastModifiedDate;
    @XmlElementRef(name = "Layouts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> layouts;
    @XmlElementRef(name = "Limits", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> limits;
    @XmlElementRef(name = "LookupFilters", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> lookupFilters;
    @XmlElementRef(name = "MasterLabel", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> masterLabel;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomObject> metadata;
    @XmlElementRef(name = "NamespacePrefix", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> namespacePrefix;
    @XmlElementRef(name = "NewUrl", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newUrl;
    @XmlElementRef(name = "OwnerChangeOptions", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> ownerChangeOptions;
    @XmlElementRef(name = "Particles", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> particles;
    @XmlElementRef(name = "PluralLabel", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pluralLabel;
    @XmlElementRef(name = "PostTemplates", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> postTemplates;
    @XmlElementRef(name = "Publisher", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Publisher> publisher;
    @XmlElementRef(name = "PublisherId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> publisherId;
    @XmlElementRef(name = "QualifiedApiName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> qualifiedApiName;
    @XmlElementRef(name = "QuickActionDefinitions", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> quickActionDefinitions;
    @XmlElementRef(name = "RecordTypes", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> recordTypes;
    @XmlElementRef(name = "RecordTypesSupported", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<RecordTypesSupported> recordTypesSupported;
    @XmlElementRef(name = "RelationshipDomains", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> relationshipDomains;
    @XmlElementRef(name = "RunningUserEntityAccess", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<UserEntityAccess> runningUserEntityAccess;
    @XmlElementRef(name = "RunningUserEntityAccessId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> runningUserEntityAccessId;
    @XmlElementRef(name = "SearchLayouts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> searchLayouts;
    @XmlElementRef(name = "StandardActions", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> standardActions;
    @XmlElementRef(name = "ValidationRules", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> validationRules;
    @XmlElementRef(name = "WebLinks", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> webLinks;
    @XmlElementRef(name = "WorkflowAlerts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowAlerts;
    @XmlElementRef(name = "WorkflowFieldUpdates", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowFieldUpdates;
    @XmlElementRef(name = "WorkflowOutboundMessages", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowOutboundMessages;
    @XmlElementRef(name = "WorkflowTasks", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<QueryResult> workflowTasks;

    /**
     * Gets the value of the apexTriggers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getApexTriggers() {
        return apexTriggers;
    }

    /**
     * Sets the value of the apexTriggers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setApexTriggers(JAXBElement<QueryResult> value) {
        this.apexTriggers = value;
    }

    /**
     * Gets the value of the assignmentRules property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getAssignmentRules() {
        return assignmentRules;
    }

    /**
     * Sets the value of the assignmentRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setAssignmentRules(JAXBElement<QueryResult> value) {
        this.assignmentRules = value;
    }

    /**
     * Gets the value of the autoResponseRules property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getAutoResponseRules() {
        return autoResponseRules;
    }

    /**
     * Sets the value of the autoResponseRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setAutoResponseRules(JAXBElement<QueryResult> value) {
        this.autoResponseRules = value;
    }

    /**
     * Gets the value of the businessProcesses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getBusinessProcesses() {
        return businessProcesses;
    }

    /**
     * Sets the value of the businessProcesses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setBusinessProcesses(JAXBElement<QueryResult> value) {
        this.businessProcesses = value;
    }

    /**
     * Gets the value of the childRelationships property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getChildRelationships() {
        return childRelationships;
    }

    /**
     * Sets the value of the childRelationships property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setChildRelationships(JAXBElement<QueryResult> value) {
        this.childRelationships = value;
    }

    /**
     * Gets the value of the compactLayouts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getCompactLayouts() {
        return compactLayouts;
    }

    /**
     * Sets the value of the compactLayouts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setCompactLayouts(JAXBElement<QueryResult> value) {
        this.compactLayouts = value;
    }

    /**
     * Gets the value of the customFields property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getCustomFields() {
        return customFields;
    }

    /**
     * Sets the value of the customFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setCustomFields(JAXBElement<QueryResult> value) {
        this.customFields = value;
    }

    /**
     * Gets the value of the defaultCompactLayout property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CompactLayoutInfo }{@code >}
     *     
     */
    public JAXBElement<CompactLayoutInfo> getDefaultCompactLayout() {
        return defaultCompactLayout;
    }

    /**
     * Sets the value of the defaultCompactLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CompactLayoutInfo }{@code >}
     *     
     */
    public void setDefaultCompactLayout(JAXBElement<CompactLayoutInfo> value) {
        this.defaultCompactLayout = value;
    }

    /**
     * Gets the value of the defaultCompactLayoutId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultCompactLayoutId() {
        return defaultCompactLayoutId;
    }

    /**
     * Sets the value of the defaultCompactLayoutId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultCompactLayoutId(JAXBElement<String> value) {
        this.defaultCompactLayoutId = value;
    }

    /**
     * Gets the value of the deploymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeploymentStatus() {
        return deploymentStatus;
    }

    /**
     * Sets the value of the deploymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeploymentStatus(JAXBElement<String> value) {
        this.deploymentStatus = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the detailUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDetailUrl() {
        return detailUrl;
    }

    /**
     * Sets the value of the detailUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDetailUrl(JAXBElement<String> value) {
        this.detailUrl = value;
    }

    /**
     * Gets the value of the developerName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeveloperName() {
        return developerName;
    }

    /**
     * Sets the value of the developerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeveloperName(JAXBElement<String> value) {
        this.developerName = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the editDefinitionUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEditDefinitionUrl() {
        return editDefinitionUrl;
    }

    /**
     * Sets the value of the editDefinitionUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEditDefinitionUrl(JAXBElement<String> value) {
        this.editDefinitionUrl = value;
    }

    /**
     * Gets the value of the editUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEditUrl() {
        return editUrl;
    }

    /**
     * Sets the value of the editUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEditUrl(JAXBElement<String> value) {
        this.editUrl = value;
    }

    /**
     * Gets the value of the externalSharingModel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExternalSharingModel() {
        return externalSharingModel;
    }

    /**
     * Sets the value of the externalSharingModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExternalSharingModel(JAXBElement<String> value) {
        this.externalSharingModel = value;
    }

    /**
     * Gets the value of the fieldSets property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getFieldSets() {
        return fieldSets;
    }

    /**
     * Sets the value of the fieldSets property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setFieldSets(JAXBElement<QueryResult> value) {
        this.fieldSets = value;
    }

    /**
     * Gets the value of the fields property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getFields() {
        return fields;
    }

    /**
     * Sets the value of the fields property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setFields(JAXBElement<QueryResult> value) {
        this.fields = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the hasSubtypes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getHasSubtypes() {
        return hasSubtypes;
    }

    /**
     * Sets the value of the hasSubtypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setHasSubtypes(JAXBElement<Boolean> value) {
        this.hasSubtypes = value;
    }

    /**
     * Gets the value of the helpSettingPageName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHelpSettingPageName() {
        return helpSettingPageName;
    }

    /**
     * Sets the value of the helpSettingPageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHelpSettingPageName(JAXBElement<String> value) {
        this.helpSettingPageName = value;
    }

    /**
     * Gets the value of the helpSettingPageUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHelpSettingPageUrl() {
        return helpSettingPageUrl;
    }

    /**
     * Sets the value of the helpSettingPageUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHelpSettingPageUrl(JAXBElement<String> value) {
        this.helpSettingPageUrl = value;
    }

    /**
     * Gets the value of the internalSharingModel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInternalSharingModel() {
        return internalSharingModel;
    }

    /**
     * Sets the value of the internalSharingModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInternalSharingModel(JAXBElement<String> value) {
        this.internalSharingModel = value;
    }

    /**
     * Gets the value of the isActivityTrackable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsActivityTrackable() {
        return isActivityTrackable;
    }

    /**
     * Sets the value of the isActivityTrackable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsActivityTrackable(JAXBElement<Boolean> value) {
        this.isActivityTrackable = value;
    }

    /**
     * Gets the value of the isApexTriggerable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsApexTriggerable() {
        return isApexTriggerable;
    }

    /**
     * Sets the value of the isApexTriggerable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsApexTriggerable(JAXBElement<Boolean> value) {
        this.isApexTriggerable = value;
    }

    /**
     * Gets the value of the isCompactLayoutable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCompactLayoutable() {
        return isCompactLayoutable;
    }

    /**
     * Sets the value of the isCompactLayoutable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCompactLayoutable(JAXBElement<Boolean> value) {
        this.isCompactLayoutable = value;
    }

    /**
     * Gets the value of the isCustomSetting property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCustomSetting() {
        return isCustomSetting;
    }

    /**
     * Sets the value of the isCustomSetting property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCustomSetting(JAXBElement<Boolean> value) {
        this.isCustomSetting = value;
    }

    /**
     * Gets the value of the isCustomizable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCustomizable() {
        return isCustomizable;
    }

    /**
     * Sets the value of the isCustomizable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCustomizable(JAXBElement<Boolean> value) {
        this.isCustomizable = value;
    }

    /**
     * Gets the value of the isDeprecatedAndHidden property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsDeprecatedAndHidden() {
        return isDeprecatedAndHidden;
    }

    /**
     * Sets the value of the isDeprecatedAndHidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsDeprecatedAndHidden(JAXBElement<Boolean> value) {
        this.isDeprecatedAndHidden = value;
    }

    /**
     * Gets the value of the isEverCreatable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEverCreatable() {
        return isEverCreatable;
    }

    /**
     * Sets the value of the isEverCreatable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEverCreatable(JAXBElement<Boolean> value) {
        this.isEverCreatable = value;
    }

    /**
     * Gets the value of the isEverDeletable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEverDeletable() {
        return isEverDeletable;
    }

    /**
     * Sets the value of the isEverDeletable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEverDeletable(JAXBElement<Boolean> value) {
        this.isEverDeletable = value;
    }

    /**
     * Gets the value of the isEverUpdatable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEverUpdatable() {
        return isEverUpdatable;
    }

    /**
     * Sets the value of the isEverUpdatable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEverUpdatable(JAXBElement<Boolean> value) {
        this.isEverUpdatable = value;
    }

    /**
     * Gets the value of the isFeedEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsFeedEnabled() {
        return isFeedEnabled;
    }

    /**
     * Sets the value of the isFeedEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsFeedEnabled(JAXBElement<Boolean> value) {
        this.isFeedEnabled = value;
    }

    /**
     * Gets the value of the isFieldHistoryTracked property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsFieldHistoryTracked() {
        return isFieldHistoryTracked;
    }

    /**
     * Sets the value of the isFieldHistoryTracked property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsFieldHistoryTracked(JAXBElement<Boolean> value) {
        this.isFieldHistoryTracked = value;
    }

    /**
     * Gets the value of the isFlsEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsFlsEnabled() {
        return isFlsEnabled;
    }

    /**
     * Sets the value of the isFlsEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsFlsEnabled(JAXBElement<Boolean> value) {
        this.isFlsEnabled = value;
    }

    /**
     * Gets the value of the isIdEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsIdEnabled() {
        return isIdEnabled;
    }

    /**
     * Sets the value of the isIdEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsIdEnabled(JAXBElement<Boolean> value) {
        this.isIdEnabled = value;
    }

    /**
     * Gets the value of the isLayoutable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsLayoutable() {
        return isLayoutable;
    }

    /**
     * Sets the value of the isLayoutable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsLayoutable(JAXBElement<Boolean> value) {
        this.isLayoutable = value;
    }

    /**
     * Gets the value of the isMruEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsMruEnabled() {
        return isMruEnabled;
    }

    /**
     * Sets the value of the isMruEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsMruEnabled(JAXBElement<Boolean> value) {
        this.isMruEnabled = value;
    }

    /**
     * Gets the value of the isProcessEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsProcessEnabled() {
        return isProcessEnabled;
    }

    /**
     * Sets the value of the isProcessEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsProcessEnabled(JAXBElement<Boolean> value) {
        this.isProcessEnabled = value;
    }

    /**
     * Gets the value of the isQueryable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsQueryable() {
        return isQueryable;
    }

    /**
     * Sets the value of the isQueryable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsQueryable(JAXBElement<Boolean> value) {
        this.isQueryable = value;
    }

    /**
     * Gets the value of the isReplicateable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsReplicateable() {
        return isReplicateable;
    }

    /**
     * Sets the value of the isReplicateable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsReplicateable(JAXBElement<Boolean> value) {
        this.isReplicateable = value;
    }

    /**
     * Gets the value of the isReportingEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsReportingEnabled() {
        return isReportingEnabled;
    }

    /**
     * Sets the value of the isReportingEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsReportingEnabled(JAXBElement<Boolean> value) {
        this.isReportingEnabled = value;
    }

    /**
     * Gets the value of the isRetrieveable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRetrieveable() {
        return isRetrieveable;
    }

    /**
     * Sets the value of the isRetrieveable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRetrieveable(JAXBElement<Boolean> value) {
        this.isRetrieveable = value;
    }

    /**
     * Gets the value of the isSearchLayoutable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSearchLayoutable() {
        return isSearchLayoutable;
    }

    /**
     * Sets the value of the isSearchLayoutable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSearchLayoutable(JAXBElement<Boolean> value) {
        this.isSearchLayoutable = value;
    }

    /**
     * Gets the value of the isSearchable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSearchable() {
        return isSearchable;
    }

    /**
     * Sets the value of the isSearchable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSearchable(JAXBElement<Boolean> value) {
        this.isSearchable = value;
    }

    /**
     * Gets the value of the isSubtype property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSubtype() {
        return isSubtype;
    }

    /**
     * Sets the value of the isSubtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSubtype(JAXBElement<Boolean> value) {
        this.isSubtype = value;
    }

    /**
     * Gets the value of the isTriggerable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsTriggerable() {
        return isTriggerable;
    }

    /**
     * Sets the value of the isTriggerable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsTriggerable(JAXBElement<Boolean> value) {
        this.isTriggerable = value;
    }

    /**
     * Gets the value of the isWorkflowEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsWorkflowEnabled() {
        return isWorkflowEnabled;
    }

    /**
     * Sets the value of the isWorkflowEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsWorkflowEnabled(JAXBElement<Boolean> value) {
        this.isWorkflowEnabled = value;
    }

    /**
     * Gets the value of the keyPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getKeyPrefix() {
        return keyPrefix;
    }

    /**
     * Sets the value of the keyPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setKeyPrefix(JAXBElement<String> value) {
        this.keyPrefix = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLabel(JAXBElement<String> value) {
        this.label = value;
    }

    /**
     * Gets the value of the lastModifiedBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public JAXBElement<User> getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * Sets the value of the lastModifiedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link User }{@code >}
     *     
     */
    public void setLastModifiedBy(JAXBElement<User> value) {
        this.lastModifiedBy = value;
    }

    /**
     * Gets the value of the lastModifiedById property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastModifiedById() {
        return lastModifiedById;
    }

    /**
     * Sets the value of the lastModifiedById property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastModifiedById(JAXBElement<String> value) {
        this.lastModifiedById = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setLastModifiedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the layouts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getLayouts() {
        return layouts;
    }

    /**
     * Sets the value of the layouts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setLayouts(JAXBElement<QueryResult> value) {
        this.layouts = value;
    }

    /**
     * Gets the value of the limits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getLimits() {
        return limits;
    }

    /**
     * Sets the value of the limits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setLimits(JAXBElement<QueryResult> value) {
        this.limits = value;
    }

    /**
     * Gets the value of the lookupFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getLookupFilters() {
        return lookupFilters;
    }

    /**
     * Sets the value of the lookupFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setLookupFilters(JAXBElement<QueryResult> value) {
        this.lookupFilters = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMasterLabel(JAXBElement<String> value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomObject }{@code >}
     *     
     */
    public JAXBElement<CustomObject> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomObject }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<CustomObject> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the namespacePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNamespacePrefix() {
        return namespacePrefix;
    }

    /**
     * Sets the value of the namespacePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNamespacePrefix(JAXBElement<String> value) {
        this.namespacePrefix = value;
    }

    /**
     * Gets the value of the newUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewUrl() {
        return newUrl;
    }

    /**
     * Sets the value of the newUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewUrl(JAXBElement<String> value) {
        this.newUrl = value;
    }

    /**
     * Gets the value of the ownerChangeOptions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getOwnerChangeOptions() {
        return ownerChangeOptions;
    }

    /**
     * Sets the value of the ownerChangeOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setOwnerChangeOptions(JAXBElement<QueryResult> value) {
        this.ownerChangeOptions = value;
    }

    /**
     * Gets the value of the particles property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getParticles() {
        return particles;
    }

    /**
     * Sets the value of the particles property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setParticles(JAXBElement<QueryResult> value) {
        this.particles = value;
    }

    /**
     * Gets the value of the pluralLabel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPluralLabel() {
        return pluralLabel;
    }

    /**
     * Sets the value of the pluralLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPluralLabel(JAXBElement<String> value) {
        this.pluralLabel = value;
    }

    /**
     * Gets the value of the postTemplates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getPostTemplates() {
        return postTemplates;
    }

    /**
     * Sets the value of the postTemplates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setPostTemplates(JAXBElement<QueryResult> value) {
        this.postTemplates = value;
    }

    /**
     * Gets the value of the publisher property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Publisher }{@code >}
     *     
     */
    public JAXBElement<Publisher> getPublisher() {
        return publisher;
    }

    /**
     * Sets the value of the publisher property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Publisher }{@code >}
     *     
     */
    public void setPublisher(JAXBElement<Publisher> value) {
        this.publisher = value;
    }

    /**
     * Gets the value of the publisherId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPublisherId() {
        return publisherId;
    }

    /**
     * Sets the value of the publisherId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPublisherId(JAXBElement<String> value) {
        this.publisherId = value;
    }

    /**
     * Gets the value of the qualifiedApiName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQualifiedApiName() {
        return qualifiedApiName;
    }

    /**
     * Sets the value of the qualifiedApiName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQualifiedApiName(JAXBElement<String> value) {
        this.qualifiedApiName = value;
    }

    /**
     * Gets the value of the quickActionDefinitions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getQuickActionDefinitions() {
        return quickActionDefinitions;
    }

    /**
     * Sets the value of the quickActionDefinitions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setQuickActionDefinitions(JAXBElement<QueryResult> value) {
        this.quickActionDefinitions = value;
    }

    /**
     * Gets the value of the recordTypes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getRecordTypes() {
        return recordTypes;
    }

    /**
     * Sets the value of the recordTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setRecordTypes(JAXBElement<QueryResult> value) {
        this.recordTypes = value;
    }

    /**
     * Gets the value of the recordTypesSupported property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RecordTypesSupported }{@code >}
     *     
     */
    public JAXBElement<RecordTypesSupported> getRecordTypesSupported() {
        return recordTypesSupported;
    }

    /**
     * Sets the value of the recordTypesSupported property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RecordTypesSupported }{@code >}
     *     
     */
    public void setRecordTypesSupported(JAXBElement<RecordTypesSupported> value) {
        this.recordTypesSupported = value;
    }

    /**
     * Gets the value of the relationshipDomains property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getRelationshipDomains() {
        return relationshipDomains;
    }

    /**
     * Sets the value of the relationshipDomains property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setRelationshipDomains(JAXBElement<QueryResult> value) {
        this.relationshipDomains = value;
    }

    /**
     * Gets the value of the runningUserEntityAccess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserEntityAccess }{@code >}
     *     
     */
    public JAXBElement<UserEntityAccess> getRunningUserEntityAccess() {
        return runningUserEntityAccess;
    }

    /**
     * Sets the value of the runningUserEntityAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserEntityAccess }{@code >}
     *     
     */
    public void setRunningUserEntityAccess(JAXBElement<UserEntityAccess> value) {
        this.runningUserEntityAccess = value;
    }

    /**
     * Gets the value of the runningUserEntityAccessId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRunningUserEntityAccessId() {
        return runningUserEntityAccessId;
    }

    /**
     * Sets the value of the runningUserEntityAccessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRunningUserEntityAccessId(JAXBElement<String> value) {
        this.runningUserEntityAccessId = value;
    }

    /**
     * Gets the value of the searchLayouts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getSearchLayouts() {
        return searchLayouts;
    }

    /**
     * Sets the value of the searchLayouts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setSearchLayouts(JAXBElement<QueryResult> value) {
        this.searchLayouts = value;
    }

    /**
     * Gets the value of the standardActions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getStandardActions() {
        return standardActions;
    }

    /**
     * Sets the value of the standardActions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setStandardActions(JAXBElement<QueryResult> value) {
        this.standardActions = value;
    }

    /**
     * Gets the value of the validationRules property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getValidationRules() {
        return validationRules;
    }

    /**
     * Sets the value of the validationRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setValidationRules(JAXBElement<QueryResult> value) {
        this.validationRules = value;
    }

    /**
     * Gets the value of the webLinks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWebLinks() {
        return webLinks;
    }

    /**
     * Sets the value of the webLinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWebLinks(JAXBElement<QueryResult> value) {
        this.webLinks = value;
    }

    /**
     * Gets the value of the workflowAlerts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowAlerts() {
        return workflowAlerts;
    }

    /**
     * Sets the value of the workflowAlerts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowAlerts(JAXBElement<QueryResult> value) {
        this.workflowAlerts = value;
    }

    /**
     * Gets the value of the workflowFieldUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowFieldUpdates() {
        return workflowFieldUpdates;
    }

    /**
     * Sets the value of the workflowFieldUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowFieldUpdates(JAXBElement<QueryResult> value) {
        this.workflowFieldUpdates = value;
    }

    /**
     * Gets the value of the workflowOutboundMessages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowOutboundMessages() {
        return workflowOutboundMessages;
    }

    /**
     * Sets the value of the workflowOutboundMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowOutboundMessages(JAXBElement<QueryResult> value) {
        this.workflowOutboundMessages = value;
    }

    /**
     * Gets the value of the workflowTasks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public JAXBElement<QueryResult> getWorkflowTasks() {
        return workflowTasks;
    }

    /**
     * Sets the value of the workflowTasks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link QueryResult }{@code >}
     *     
     */
    public void setWorkflowTasks(JAXBElement<QueryResult> value) {
        this.workflowTasks = value;
    }

}
