
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.DisplayCurrency;


/**
 * <p>Java class for ForecastingSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForecastingSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="displayCurrency" type="{urn:tooling.soap.sforce.com}DisplayCurrency" minOccurs="0"/&gt;
 *         &lt;element name="enableForecasts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forecastingCategoryMappings" type="{urn:metadata.tooling.soap.sforce.com}ForecastingCategoryMapping" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="forecastingTypeSettings" type="{urn:metadata.tooling.soap.sforce.com}ForecastingTypeSettings" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForecastingSettings", propOrder = {
    "displayCurrency",
    "enableForecasts",
    "forecastingCategoryMappings",
    "forecastingTypeSettings"
})
public class ForecastingSettings
    extends MetadataForSettings
{

    @XmlSchemaType(name = "string")
    protected DisplayCurrency displayCurrency;
    protected Boolean enableForecasts;
    protected List<ForecastingCategoryMapping> forecastingCategoryMappings;
    protected List<ForecastingTypeSettings> forecastingTypeSettings;

    /**
     * Gets the value of the displayCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link DisplayCurrency }
     *     
     */
    public DisplayCurrency getDisplayCurrency() {
        return displayCurrency;
    }

    /**
     * Sets the value of the displayCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisplayCurrency }
     *     
     */
    public void setDisplayCurrency(DisplayCurrency value) {
        this.displayCurrency = value;
    }

    /**
     * Gets the value of the enableForecasts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableForecasts() {
        return enableForecasts;
    }

    /**
     * Sets the value of the enableForecasts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableForecasts(Boolean value) {
        this.enableForecasts = value;
    }

    /**
     * Gets the value of the forecastingCategoryMappings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forecastingCategoryMappings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForecastingCategoryMappings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ForecastingCategoryMapping }
     * 
     * 
     */
    public List<ForecastingCategoryMapping> getForecastingCategoryMappings() {
        if (forecastingCategoryMappings == null) {
            forecastingCategoryMappings = new ArrayList<ForecastingCategoryMapping>();
        }
        return this.forecastingCategoryMappings;
    }

    /**
     * Gets the value of the forecastingTypeSettings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forecastingTypeSettings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForecastingTypeSettings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ForecastingTypeSettings }
     * 
     * 
     */
    public List<ForecastingTypeSettings> getForecastingTypeSettings() {
        if (forecastingTypeSettings == null) {
            forecastingTypeSettings = new ArrayList<ForecastingTypeSettings>();
        }
        return this.forecastingTypeSettings;
    }

}
