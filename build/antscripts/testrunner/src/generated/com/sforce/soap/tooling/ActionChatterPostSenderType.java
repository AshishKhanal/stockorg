
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionChatterPostSenderType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionChatterPostSenderType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CurrentUser"/&gt;
 *     &lt;enumeration value="DefaultChatterUser"/&gt;
 *     &lt;enumeration value="DefaultWorkflowUser"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionChatterPostSenderType")
@XmlEnum
public enum ActionChatterPostSenderType {

    @XmlEnumValue("CurrentUser")
    CURRENT_USER("CurrentUser"),
    @XmlEnumValue("DefaultChatterUser")
    DEFAULT_CHATTER_USER("DefaultChatterUser"),
    @XmlEnumValue("DefaultWorkflowUser")
    DEFAULT_WORKFLOW_USER("DefaultWorkflowUser");
    private final String value;

    ActionChatterPostSenderType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionChatterPostSenderType fromValue(String v) {
        for (ActionChatterPostSenderType c: ActionChatterPostSenderType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
