
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvocableActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InvocableActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="apex"/&gt;
 *     &lt;enumeration value="chatterPost"/&gt;
 *     &lt;enumeration value="contentWorkspaceEnableFolders"/&gt;
 *     &lt;enumeration value="emailAlert"/&gt;
 *     &lt;enumeration value="emailSimple"/&gt;
 *     &lt;enumeration value="flow"/&gt;
 *     &lt;enumeration value="metricRefresh"/&gt;
 *     &lt;enumeration value="quickAction"/&gt;
 *     &lt;enumeration value="submit"/&gt;
 *     &lt;enumeration value="thanks"/&gt;
 *     &lt;enumeration value="thunderResponse"/&gt;
 *     &lt;enumeration value="createServiceReport"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InvocableActionType")
@XmlEnum
public enum InvocableActionType {

    @XmlEnumValue("apex")
    APEX("apex"),
    @XmlEnumValue("chatterPost")
    CHATTER_POST("chatterPost"),
    @XmlEnumValue("contentWorkspaceEnableFolders")
    CONTENT_WORKSPACE_ENABLE_FOLDERS("contentWorkspaceEnableFolders"),
    @XmlEnumValue("emailAlert")
    EMAIL_ALERT("emailAlert"),
    @XmlEnumValue("emailSimple")
    EMAIL_SIMPLE("emailSimple"),
    @XmlEnumValue("flow")
    FLOW("flow"),
    @XmlEnumValue("metricRefresh")
    METRIC_REFRESH("metricRefresh"),
    @XmlEnumValue("quickAction")
    QUICK_ACTION("quickAction"),
    @XmlEnumValue("submit")
    SUBMIT("submit"),
    @XmlEnumValue("thanks")
    THANKS("thanks"),
    @XmlEnumValue("thunderResponse")
    THUNDER_RESPONSE("thunderResponse"),
    @XmlEnumValue("createServiceReport")
    CREATE_SERVICE_REPORT("createServiceReport");
    private final String value;

    InvocableActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvocableActionType fromValue(String v) {
        for (InvocableActionType c: InvocableActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
