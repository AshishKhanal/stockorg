
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpportunitySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpportunitySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutoActivateNewReminders" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FindSimilarOppFilter" type="{urn:sobject.tooling.soap.sforce.com}FindSimilarOppFilter" minOccurs="0"/&gt;
 *         &lt;element name="FindSimilarOppFilterId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFindSimilarOpportunitiesEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsOpportunityTeamEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsUpdateRemindersEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}OpportunitySettings" minOccurs="0"/&gt;
 *         &lt;element name="PromptToAddProducts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpportunitySettings", propOrder = {
    "autoActivateNewReminders",
    "durableId",
    "findSimilarOppFilter",
    "findSimilarOppFilterId",
    "fullName",
    "isFindSimilarOpportunitiesEnabled",
    "isOpportunityTeamEnabled",
    "isUpdateRemindersEnabled",
    "metadata",
    "promptToAddProducts"
})
public class OpportunitySettings
    extends SObject
{

    @XmlElementRef(name = "AutoActivateNewReminders", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> autoActivateNewReminders;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FindSimilarOppFilter", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<FindSimilarOppFilter> findSimilarOppFilter;
    @XmlElementRef(name = "FindSimilarOppFilterId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> findSimilarOppFilterId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "IsFindSimilarOpportunitiesEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isFindSimilarOpportunitiesEnabled;
    @XmlElementRef(name = "IsOpportunityTeamEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOpportunityTeamEnabled;
    @XmlElementRef(name = "IsUpdateRemindersEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isUpdateRemindersEnabled;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.OpportunitySettings> metadata;
    @XmlElementRef(name = "PromptToAddProducts", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> promptToAddProducts;

    /**
     * Gets the value of the autoActivateNewReminders property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getAutoActivateNewReminders() {
        return autoActivateNewReminders;
    }

    /**
     * Sets the value of the autoActivateNewReminders property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setAutoActivateNewReminders(JAXBElement<Boolean> value) {
        this.autoActivateNewReminders = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the findSimilarOppFilter property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FindSimilarOppFilter }{@code >}
     *     
     */
    public JAXBElement<FindSimilarOppFilter> getFindSimilarOppFilter() {
        return findSimilarOppFilter;
    }

    /**
     * Sets the value of the findSimilarOppFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FindSimilarOppFilter }{@code >}
     *     
     */
    public void setFindSimilarOppFilter(JAXBElement<FindSimilarOppFilter> value) {
        this.findSimilarOppFilter = value;
    }

    /**
     * Gets the value of the findSimilarOppFilterId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFindSimilarOppFilterId() {
        return findSimilarOppFilterId;
    }

    /**
     * Sets the value of the findSimilarOppFilterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFindSimilarOppFilterId(JAXBElement<String> value) {
        this.findSimilarOppFilterId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the isFindSimilarOpportunitiesEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsFindSimilarOpportunitiesEnabled() {
        return isFindSimilarOpportunitiesEnabled;
    }

    /**
     * Sets the value of the isFindSimilarOpportunitiesEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsFindSimilarOpportunitiesEnabled(JAXBElement<Boolean> value) {
        this.isFindSimilarOpportunitiesEnabled = value;
    }

    /**
     * Gets the value of the isOpportunityTeamEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOpportunityTeamEnabled() {
        return isOpportunityTeamEnabled;
    }

    /**
     * Sets the value of the isOpportunityTeamEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOpportunityTeamEnabled(JAXBElement<Boolean> value) {
        this.isOpportunityTeamEnabled = value;
    }

    /**
     * Gets the value of the isUpdateRemindersEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsUpdateRemindersEnabled() {
        return isUpdateRemindersEnabled;
    }

    /**
     * Sets the value of the isUpdateRemindersEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsUpdateRemindersEnabled(JAXBElement<Boolean> value) {
        this.isUpdateRemindersEnabled = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.OpportunitySettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.OpportunitySettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.OpportunitySettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.OpportunitySettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the promptToAddProducts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getPromptToAddProducts() {
        return promptToAddProducts;
    }

    /**
     * Sets the value of the promptToAddProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setPromptToAddProducts(JAXBElement<Boolean> value) {
        this.promptToAddProducts = value;
    }

}
