
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.PlatformActionListContext;


/**
 * <p>Java class for PlatformActionList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlatformActionList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="actionListContext" type="{urn:tooling.soap.sforce.com}PlatformActionListContext"/&gt;
 *         &lt;element name="platformActionListItems" type="{urn:metadata.tooling.soap.sforce.com}PlatformActionListItem" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="relatedSourceEntity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlatformActionList", propOrder = {
    "actionListContext",
    "platformActionListItems",
    "relatedSourceEntity"
})
public class PlatformActionList
    extends Metadata
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected PlatformActionListContext actionListContext;
    protected List<PlatformActionListItem> platformActionListItems;
    protected String relatedSourceEntity;

    /**
     * Gets the value of the actionListContext property.
     * 
     * @return
     *     possible object is
     *     {@link PlatformActionListContext }
     *     
     */
    public PlatformActionListContext getActionListContext() {
        return actionListContext;
    }

    /**
     * Sets the value of the actionListContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlatformActionListContext }
     *     
     */
    public void setActionListContext(PlatformActionListContext value) {
        this.actionListContext = value;
    }

    /**
     * Gets the value of the platformActionListItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the platformActionListItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlatformActionListItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlatformActionListItem }
     * 
     * 
     */
    public List<PlatformActionListItem> getPlatformActionListItems() {
        if (platformActionListItems == null) {
            platformActionListItems = new ArrayList<PlatformActionListItem>();
        }
        return this.platformActionListItems;
    }

    /**
     * Gets the value of the relatedSourceEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedSourceEntity() {
        return relatedSourceEntity;
    }

    /**
     * Sets the value of the relatedSourceEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedSourceEntity(String value) {
        this.relatedSourceEntity = value;
    }

}
