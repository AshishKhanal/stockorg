
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlinyPlatformIndexType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlinyPlatformIndexType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="PRIMARY"/&gt;
 *     &lt;enumeration value="SECONDARY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PlinyPlatformIndexType")
@XmlEnum
public enum PlinyPlatformIndexType {

    PRIMARY,
    SECONDARY;

    public String value() {
        return name();
    }

    public static PlinyPlatformIndexType fromValue(String v) {
        return valueOf(v);
    }

}
