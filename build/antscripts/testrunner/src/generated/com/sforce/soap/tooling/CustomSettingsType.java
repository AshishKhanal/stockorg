
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomSettingsType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomSettingsType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="List"/&gt;
 *     &lt;enumeration value="Hierarchy"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomSettingsType")
@XmlEnum
public enum CustomSettingsType {

    @XmlEnumValue("List")
    LIST("List"),
    @XmlEnumValue("Hierarchy")
    HIERARCHY("Hierarchy");
    private final String value;

    CustomSettingsType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomSettingsType fromValue(String v) {
        for (CustomSettingsType c: CustomSettingsType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
