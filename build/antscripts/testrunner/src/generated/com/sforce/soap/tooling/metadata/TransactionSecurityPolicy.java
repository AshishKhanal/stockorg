
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.MonitoredEvents;


/**
 * <p>Java class for TransactionSecurityPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionSecurityPolicy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="action" type="{urn:metadata.tooling.soap.sforce.com}TransactionSecurityAction"/&gt;
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="apexClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="developerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventType" type="{urn:tooling.soap.sforce.com}MonitoredEvents"/&gt;
 *         &lt;element name="executionUser" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="resourceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionSecurityPolicy", propOrder = {
    "action",
    "active",
    "apexClass",
    "description",
    "developerName",
    "eventType",
    "executionUser",
    "masterLabel",
    "resourceName"
})
public class TransactionSecurityPolicy
    extends Metadata
{

    @XmlElement(required = true)
    protected TransactionSecurityAction action;
    protected boolean active;
    @XmlElement(required = true)
    protected String apexClass;
    protected String description;
    protected String developerName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected MonitoredEvents eventType;
    @XmlElement(required = true)
    protected String executionUser;
    protected String masterLabel;
    @XmlElement(required = true)
    protected String resourceName;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionSecurityAction }
     *     
     */
    public TransactionSecurityAction getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionSecurityAction }
     *     
     */
    public void setAction(TransactionSecurityAction value) {
        this.action = value;
    }

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the apexClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApexClass() {
        return apexClass;
    }

    /**
     * Sets the value of the apexClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApexClass(String value) {
        this.apexClass = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the developerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeveloperName() {
        return developerName;
    }

    /**
     * Sets the value of the developerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeveloperName(String value) {
        this.developerName = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link MonitoredEvents }
     *     
     */
    public MonitoredEvents getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MonitoredEvents }
     *     
     */
    public void setEventType(MonitoredEvents value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the executionUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionUser() {
        return executionUser;
    }

    /**
     * Sets the value of the executionUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionUser(String value) {
        this.executionUser = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the resourceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * Sets the value of the resourceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceName(String value) {
        this.resourceName = value;
    }

}
