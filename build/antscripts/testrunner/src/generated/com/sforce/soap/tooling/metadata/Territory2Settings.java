
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Territory2Settings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Territory2Settings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="defaultAccountAccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultCaseAccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultContactAccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="defaultOpportunityAccessLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Territory2Settings", propOrder = {
    "defaultAccountAccessLevel",
    "defaultCaseAccessLevel",
    "defaultContactAccessLevel",
    "defaultOpportunityAccessLevel"
})
public class Territory2Settings
    extends MetadataForSettings
{

    protected String defaultAccountAccessLevel;
    protected String defaultCaseAccessLevel;
    protected String defaultContactAccessLevel;
    protected String defaultOpportunityAccessLevel;

    /**
     * Gets the value of the defaultAccountAccessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAccountAccessLevel() {
        return defaultAccountAccessLevel;
    }

    /**
     * Sets the value of the defaultAccountAccessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAccountAccessLevel(String value) {
        this.defaultAccountAccessLevel = value;
    }

    /**
     * Gets the value of the defaultCaseAccessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultCaseAccessLevel() {
        return defaultCaseAccessLevel;
    }

    /**
     * Sets the value of the defaultCaseAccessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultCaseAccessLevel(String value) {
        this.defaultCaseAccessLevel = value;
    }

    /**
     * Gets the value of the defaultContactAccessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultContactAccessLevel() {
        return defaultContactAccessLevel;
    }

    /**
     * Sets the value of the defaultContactAccessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultContactAccessLevel(String value) {
        this.defaultContactAccessLevel = value;
    }

    /**
     * Gets the value of the defaultOpportunityAccessLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultOpportunityAccessLevel() {
        return defaultOpportunityAccessLevel;
    }

    /**
     * Sets the value of the defaultOpportunityAccessLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultOpportunityAccessLevel(String value) {
        this.defaultOpportunityAccessLevel = value;
    }

}
