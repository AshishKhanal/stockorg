
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.UiBehavior;


/**
 * <p>Java class for QuickActionLayoutItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickActionLayoutItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emptySpace" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="field" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="uiBehavior" type="{urn:tooling.soap.sforce.com}UiBehavior" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickActionLayoutItem", propOrder = {
    "emptySpace",
    "field",
    "uiBehavior"
})
public class QuickActionLayoutItem {

    protected Boolean emptySpace;
    protected String field;
    @XmlSchemaType(name = "string")
    protected UiBehavior uiBehavior;

    /**
     * Gets the value of the emptySpace property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEmptySpace() {
        return emptySpace;
    }

    /**
     * Sets the value of the emptySpace property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmptySpace(Boolean value) {
        this.emptySpace = value;
    }

    /**
     * Gets the value of the field property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getField() {
        return field;
    }

    /**
     * Sets the value of the field property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setField(String value) {
        this.field = value;
    }

    /**
     * Gets the value of the uiBehavior property.
     * 
     * @return
     *     possible object is
     *     {@link UiBehavior }
     *     
     */
    public UiBehavior getUiBehavior() {
        return uiBehavior;
    }

    /**
     * Sets the value of the uiBehavior property.
     * 
     * @param value
     *     allowed object is
     *     {@link UiBehavior }
     *     
     */
    public void setUiBehavior(UiBehavior value) {
        this.uiBehavior = value;
    }

}
