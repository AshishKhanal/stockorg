
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlexiPageType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlexiPageType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="AppPage"/&gt;
 *     &lt;enumeration value="ObjectPage"/&gt;
 *     &lt;enumeration value="RecordPage"/&gt;
 *     &lt;enumeration value="HomePage"/&gt;
 *     &lt;enumeration value="MailAppAppPage"/&gt;
 *     &lt;enumeration value="CommAppPage"/&gt;
 *     &lt;enumeration value="CommForgotPasswordPage"/&gt;
 *     &lt;enumeration value="CommLoginPage"/&gt;
 *     &lt;enumeration value="CommObjectPage"/&gt;
 *     &lt;enumeration value="CommQuickActionCreatePage"/&gt;
 *     &lt;enumeration value="CommRecordPage"/&gt;
 *     &lt;enumeration value="CommRelatedListPage"/&gt;
 *     &lt;enumeration value="CommSearchResultPage"/&gt;
 *     &lt;enumeration value="CommSelfRegisterPage"/&gt;
 *     &lt;enumeration value="CommThemeLayoutPage"/&gt;
 *     &lt;enumeration value="UtilityBar"/&gt;
 *     &lt;enumeration value="RecordPreview"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlexiPageType")
@XmlEnum
public enum FlexiPageType {

    @XmlEnumValue("AppPage")
    APP_PAGE("AppPage"),
    @XmlEnumValue("ObjectPage")
    OBJECT_PAGE("ObjectPage"),
    @XmlEnumValue("RecordPage")
    RECORD_PAGE("RecordPage"),
    @XmlEnumValue("HomePage")
    HOME_PAGE("HomePage"),
    @XmlEnumValue("MailAppAppPage")
    MAIL_APP_APP_PAGE("MailAppAppPage"),
    @XmlEnumValue("CommAppPage")
    COMM_APP_PAGE("CommAppPage"),
    @XmlEnumValue("CommForgotPasswordPage")
    COMM_FORGOT_PASSWORD_PAGE("CommForgotPasswordPage"),
    @XmlEnumValue("CommLoginPage")
    COMM_LOGIN_PAGE("CommLoginPage"),
    @XmlEnumValue("CommObjectPage")
    COMM_OBJECT_PAGE("CommObjectPage"),
    @XmlEnumValue("CommQuickActionCreatePage")
    COMM_QUICK_ACTION_CREATE_PAGE("CommQuickActionCreatePage"),
    @XmlEnumValue("CommRecordPage")
    COMM_RECORD_PAGE("CommRecordPage"),
    @XmlEnumValue("CommRelatedListPage")
    COMM_RELATED_LIST_PAGE("CommRelatedListPage"),
    @XmlEnumValue("CommSearchResultPage")
    COMM_SEARCH_RESULT_PAGE("CommSearchResultPage"),
    @XmlEnumValue("CommSelfRegisterPage")
    COMM_SELF_REGISTER_PAGE("CommSelfRegisterPage"),
    @XmlEnumValue("CommThemeLayoutPage")
    COMM_THEME_LAYOUT_PAGE("CommThemeLayoutPage"),
    @XmlEnumValue("UtilityBar")
    UTILITY_BAR("UtilityBar"),
    @XmlEnumValue("RecordPreview")
    RECORD_PREVIEW("RecordPreview");
    private final String value;

    FlexiPageType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlexiPageType fromValue(String v) {
        for (FlexiPageType c: FlexiPageType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
