
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RiskType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RiskType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HIGH_RISK"/&gt;
 *     &lt;enumeration value="MEDIUM_RISK"/&gt;
 *     &lt;enumeration value="MEETS_STANDARD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RiskType")
@XmlEnum
public enum RiskType {

    HIGH_RISK,
    MEDIUM_RISK,
    MEETS_STANDARD;

    public String value() {
        return name();
    }

    public static RiskType fromValue(String v) {
        return valueOf(v);
    }

}
