
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeSuggestedArticlesSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KnowledgeSuggestedArticlesSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="caseFields" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeCaseFieldsSettings" minOccurs="0"/&gt;
 *         &lt;element name="useSuggestedArticlesForCase" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="workOrderFields" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeWorkOrderFieldsSettings" minOccurs="0"/&gt;
 *         &lt;element name="workOrderLineItemFields" type="{urn:metadata.tooling.soap.sforce.com}KnowledgeWorkOrderLineItemFieldsSettings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KnowledgeSuggestedArticlesSettings", propOrder = {
    "caseFields",
    "useSuggestedArticlesForCase",
    "workOrderFields",
    "workOrderLineItemFields"
})
public class KnowledgeSuggestedArticlesSettings {

    protected KnowledgeCaseFieldsSettings caseFields;
    protected Boolean useSuggestedArticlesForCase;
    protected KnowledgeWorkOrderFieldsSettings workOrderFields;
    protected KnowledgeWorkOrderLineItemFieldsSettings workOrderLineItemFields;

    /**
     * Gets the value of the caseFields property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeCaseFieldsSettings }
     *     
     */
    public KnowledgeCaseFieldsSettings getCaseFields() {
        return caseFields;
    }

    /**
     * Sets the value of the caseFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeCaseFieldsSettings }
     *     
     */
    public void setCaseFields(KnowledgeCaseFieldsSettings value) {
        this.caseFields = value;
    }

    /**
     * Gets the value of the useSuggestedArticlesForCase property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSuggestedArticlesForCase() {
        return useSuggestedArticlesForCase;
    }

    /**
     * Sets the value of the useSuggestedArticlesForCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSuggestedArticlesForCase(Boolean value) {
        this.useSuggestedArticlesForCase = value;
    }

    /**
     * Gets the value of the workOrderFields property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeWorkOrderFieldsSettings }
     *     
     */
    public KnowledgeWorkOrderFieldsSettings getWorkOrderFields() {
        return workOrderFields;
    }

    /**
     * Sets the value of the workOrderFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeWorkOrderFieldsSettings }
     *     
     */
    public void setWorkOrderFields(KnowledgeWorkOrderFieldsSettings value) {
        this.workOrderFields = value;
    }

    /**
     * Gets the value of the workOrderLineItemFields property.
     * 
     * @return
     *     possible object is
     *     {@link KnowledgeWorkOrderLineItemFieldsSettings }
     *     
     */
    public KnowledgeWorkOrderLineItemFieldsSettings getWorkOrderLineItemFields() {
        return workOrderLineItemFields;
    }

    /**
     * Sets the value of the workOrderLineItemFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link KnowledgeWorkOrderLineItemFieldsSettings }
     *     
     */
    public void setWorkOrderLineItemFields(KnowledgeWorkOrderLineItemFieldsSettings value) {
        this.workOrderLineItemFields = value;
    }

}
