
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AppleEnvironmentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AppleEnvironmentType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Sandbox"/&gt;
 *     &lt;enumeration value="Production"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AppleEnvironmentType")
@XmlEnum
public enum AppleEnvironmentType {

    @XmlEnumValue("Sandbox")
    SANDBOX("Sandbox"),
    @XmlEnumValue("Production")
    PRODUCTION("Production");
    private final String value;

    AppleEnvironmentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AppleEnvironmentType fromValue(String v) {
        for (AppleEnvironmentType c: AppleEnvironmentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
