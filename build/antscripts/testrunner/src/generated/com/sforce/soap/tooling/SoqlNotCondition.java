
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SoqlNotCondition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SoqlNotCondition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:tooling.soap.sforce.com}SoqlWhereCondition"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="condition" type="{urn:tooling.soap.sforce.com}SoqlWhereCondition"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SoqlNotCondition", propOrder = {
    "condition"
})
public class SoqlNotCondition
    extends SoqlWhereCondition
{

    @XmlElement(required = true)
    protected SoqlWhereCondition condition;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link SoqlWhereCondition }
     *     
     */
    public SoqlWhereCondition getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoqlWhereCondition }
     *     
     */
    public void setCondition(SoqlWhereCondition value) {
        this.condition = value;
    }

}
