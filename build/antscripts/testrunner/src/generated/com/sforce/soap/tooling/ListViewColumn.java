
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListViewColumn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListViewColumn"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ascendingLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="descendingLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fieldNameOrPath" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="hidden" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="selectListItem" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sortDirection" type="{urn:tooling.soap.sforce.com}orderByDirection"/&gt;
 *         &lt;element name="sortIndex" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sortable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListViewColumn", propOrder = {
    "ascendingLabel",
    "descendingLabel",
    "fieldNameOrPath",
    "hidden",
    "label",
    "selectListItem",
    "sortDirection",
    "sortIndex",
    "sortable",
    "type"
})
public class ListViewColumn {

    @XmlElement(required = true)
    protected String ascendingLabel;
    @XmlElement(required = true)
    protected String descendingLabel;
    @XmlElement(required = true)
    protected String fieldNameOrPath;
    protected boolean hidden;
    @XmlElement(required = true)
    protected String label;
    @XmlElement(required = true)
    protected String selectListItem;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected OrderByDirection sortDirection;
    protected int sortIndex;
    protected boolean sortable;
    @XmlElement(required = true)
    protected String type;

    /**
     * Gets the value of the ascendingLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAscendingLabel() {
        return ascendingLabel;
    }

    /**
     * Sets the value of the ascendingLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAscendingLabel(String value) {
        this.ascendingLabel = value;
    }

    /**
     * Gets the value of the descendingLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescendingLabel() {
        return descendingLabel;
    }

    /**
     * Sets the value of the descendingLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescendingLabel(String value) {
        this.descendingLabel = value;
    }

    /**
     * Gets the value of the fieldNameOrPath property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldNameOrPath() {
        return fieldNameOrPath;
    }

    /**
     * Sets the value of the fieldNameOrPath property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldNameOrPath(String value) {
        this.fieldNameOrPath = value;
    }

    /**
     * Gets the value of the hidden property.
     * 
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * Sets the value of the hidden property.
     * 
     */
    public void setHidden(boolean value) {
        this.hidden = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the selectListItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelectListItem() {
        return selectListItem;
    }

    /**
     * Sets the value of the selectListItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelectListItem(String value) {
        this.selectListItem = value;
    }

    /**
     * Gets the value of the sortDirection property.
     * 
     * @return
     *     possible object is
     *     {@link OrderByDirection }
     *     
     */
    public OrderByDirection getSortDirection() {
        return sortDirection;
    }

    /**
     * Sets the value of the sortDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderByDirection }
     *     
     */
    public void setSortDirection(OrderByDirection value) {
        this.sortDirection = value;
    }

    /**
     * Gets the value of the sortIndex property.
     * 
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * Sets the value of the sortIndex property.
     * 
     */
    public void setSortIndex(int value) {
        this.sortIndex = value;
    }

    /**
     * Gets the value of the sortable property.
     * 
     */
    public boolean isSortable() {
        return sortable;
    }

    /**
     * Sets the value of the sortable property.
     * 
     */
    public void setSortable(boolean value) {
        this.sortable = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
