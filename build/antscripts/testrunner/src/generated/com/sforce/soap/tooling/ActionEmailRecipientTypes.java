
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionEmailRecipientTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionEmailRecipientTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="group"/&gt;
 *     &lt;enumeration value="role"/&gt;
 *     &lt;enumeration value="user"/&gt;
 *     &lt;enumeration value="opportunityTeam"/&gt;
 *     &lt;enumeration value="accountTeam"/&gt;
 *     &lt;enumeration value="roleSubordinates"/&gt;
 *     &lt;enumeration value="owner"/&gt;
 *     &lt;enumeration value="creator"/&gt;
 *     &lt;enumeration value="partnerUser"/&gt;
 *     &lt;enumeration value="accountOwner"/&gt;
 *     &lt;enumeration value="customerPortalUser"/&gt;
 *     &lt;enumeration value="portalRole"/&gt;
 *     &lt;enumeration value="portalRoleSubordinates"/&gt;
 *     &lt;enumeration value="contactLookup"/&gt;
 *     &lt;enumeration value="userLookup"/&gt;
 *     &lt;enumeration value="roleSubordinatesInternal"/&gt;
 *     &lt;enumeration value="email"/&gt;
 *     &lt;enumeration value="caseTeam"/&gt;
 *     &lt;enumeration value="campaignMemberDerivedOwner"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionEmailRecipientTypes")
@XmlEnum
public enum ActionEmailRecipientTypes {

    @XmlEnumValue("group")
    GROUP("group"),
    @XmlEnumValue("role")
    ROLE("role"),
    @XmlEnumValue("user")
    USER("user"),
    @XmlEnumValue("opportunityTeam")
    OPPORTUNITY_TEAM("opportunityTeam"),
    @XmlEnumValue("accountTeam")
    ACCOUNT_TEAM("accountTeam"),
    @XmlEnumValue("roleSubordinates")
    ROLE_SUBORDINATES("roleSubordinates"),
    @XmlEnumValue("owner")
    OWNER("owner"),
    @XmlEnumValue("creator")
    CREATOR("creator"),
    @XmlEnumValue("partnerUser")
    PARTNER_USER("partnerUser"),
    @XmlEnumValue("accountOwner")
    ACCOUNT_OWNER("accountOwner"),
    @XmlEnumValue("customerPortalUser")
    CUSTOMER_PORTAL_USER("customerPortalUser"),
    @XmlEnumValue("portalRole")
    PORTAL_ROLE("portalRole"),
    @XmlEnumValue("portalRoleSubordinates")
    PORTAL_ROLE_SUBORDINATES("portalRoleSubordinates"),
    @XmlEnumValue("contactLookup")
    CONTACT_LOOKUP("contactLookup"),
    @XmlEnumValue("userLookup")
    USER_LOOKUP("userLookup"),
    @XmlEnumValue("roleSubordinatesInternal")
    ROLE_SUBORDINATES_INTERNAL("roleSubordinatesInternal"),
    @XmlEnumValue("email")
    EMAIL("email"),
    @XmlEnumValue("caseTeam")
    CASE_TEAM("caseTeam"),
    @XmlEnumValue("campaignMemberDerivedOwner")
    CAMPAIGN_MEMBER_DERIVED_OWNER("campaignMemberDerivedOwner");
    private final String value;

    ActionEmailRecipientTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionEmailRecipientTypes fromValue(String v) {
        for (ActionEmailRecipientTypes c: ActionEmailRecipientTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
