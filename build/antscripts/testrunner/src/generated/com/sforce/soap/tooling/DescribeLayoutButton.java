
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DescribeLayoutButton complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DescribeLayoutButton"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="behavior" type="{urn:tooling.soap.sforce.com}WebLinkWindowType" minOccurs="0"/&gt;
 *         &lt;element name="colors" type="{urn:tooling.soap.sforce.com}DescribeColorResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contentSource" type="{urn:tooling.soap.sforce.com}WebLinkType" minOccurs="0"/&gt;
 *         &lt;element name="custom" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="encoding" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="icons" type="{urn:tooling.soap.sforce.com}DescribeIconResult" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="menubar" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="overridden" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="resizeable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="scrollbars" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="showsLocation" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="showsStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="toolbar" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="url" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="windowPosition" type="{urn:tooling.soap.sforce.com}WebLinkPosition" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DescribeLayoutButton", propOrder = {
    "behavior",
    "colors",
    "content",
    "contentSource",
    "custom",
    "encoding",
    "height",
    "icons",
    "label",
    "menubar",
    "name",
    "overridden",
    "resizeable",
    "scrollbars",
    "showsLocation",
    "showsStatus",
    "toolbar",
    "url",
    "width",
    "windowPosition"
})
public class DescribeLayoutButton {

    @XmlSchemaType(name = "string")
    protected WebLinkWindowType behavior;
    protected List<DescribeColorResult> colors;
    protected String content;
    @XmlSchemaType(name = "string")
    protected WebLinkType contentSource;
    protected boolean custom;
    protected String encoding;
    protected Integer height;
    protected List<DescribeIconResult> icons;
    @XmlElement(required = true)
    protected String label;
    protected boolean menubar;
    @XmlElement(required = true)
    protected String name;
    protected boolean overridden;
    protected boolean resizeable;
    protected boolean scrollbars;
    protected boolean showsLocation;
    protected boolean showsStatus;
    protected boolean toolbar;
    protected String url;
    protected Integer width;
    @XmlSchemaType(name = "string")
    protected WebLinkPosition windowPosition;

    /**
     * Gets the value of the behavior property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkWindowType }
     *     
     */
    public WebLinkWindowType getBehavior() {
        return behavior;
    }

    /**
     * Sets the value of the behavior property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkWindowType }
     *     
     */
    public void setBehavior(WebLinkWindowType value) {
        this.behavior = value;
    }

    /**
     * Gets the value of the colors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the colors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getColors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeColorResult }
     * 
     * 
     */
    public List<DescribeColorResult> getColors() {
        if (colors == null) {
            colors = new ArrayList<DescribeColorResult>();
        }
        return this.colors;
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Gets the value of the contentSource property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkType }
     *     
     */
    public WebLinkType getContentSource() {
        return contentSource;
    }

    /**
     * Sets the value of the contentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkType }
     *     
     */
    public void setContentSource(WebLinkType value) {
        this.contentSource = value;
    }

    /**
     * Gets the value of the custom property.
     * 
     */
    public boolean isCustom() {
        return custom;
    }

    /**
     * Sets the value of the custom property.
     * 
     */
    public void setCustom(boolean value) {
        this.custom = value;
    }

    /**
     * Gets the value of the encoding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Sets the value of the encoding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncoding(String value) {
        this.encoding = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHeight(Integer value) {
        this.height = value;
    }

    /**
     * Gets the value of the icons property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the icons property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIcons().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeIconResult }
     * 
     * 
     */
    public List<DescribeIconResult> getIcons() {
        if (icons == null) {
            icons = new ArrayList<DescribeIconResult>();
        }
        return this.icons;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the menubar property.
     * 
     */
    public boolean isMenubar() {
        return menubar;
    }

    /**
     * Sets the value of the menubar property.
     * 
     */
    public void setMenubar(boolean value) {
        this.menubar = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the overridden property.
     * 
     */
    public boolean isOverridden() {
        return overridden;
    }

    /**
     * Sets the value of the overridden property.
     * 
     */
    public void setOverridden(boolean value) {
        this.overridden = value;
    }

    /**
     * Gets the value of the resizeable property.
     * 
     */
    public boolean isResizeable() {
        return resizeable;
    }

    /**
     * Sets the value of the resizeable property.
     * 
     */
    public void setResizeable(boolean value) {
        this.resizeable = value;
    }

    /**
     * Gets the value of the scrollbars property.
     * 
     */
    public boolean isScrollbars() {
        return scrollbars;
    }

    /**
     * Sets the value of the scrollbars property.
     * 
     */
    public void setScrollbars(boolean value) {
        this.scrollbars = value;
    }

    /**
     * Gets the value of the showsLocation property.
     * 
     */
    public boolean isShowsLocation() {
        return showsLocation;
    }

    /**
     * Sets the value of the showsLocation property.
     * 
     */
    public void setShowsLocation(boolean value) {
        this.showsLocation = value;
    }

    /**
     * Gets the value of the showsStatus property.
     * 
     */
    public boolean isShowsStatus() {
        return showsStatus;
    }

    /**
     * Sets the value of the showsStatus property.
     * 
     */
    public void setShowsStatus(boolean value) {
        this.showsStatus = value;
    }

    /**
     * Gets the value of the toolbar property.
     * 
     */
    public boolean isToolbar() {
        return toolbar;
    }

    /**
     * Sets the value of the toolbar property.
     * 
     */
    public void setToolbar(boolean value) {
        this.toolbar = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidth(Integer value) {
        this.width = value;
    }

    /**
     * Gets the value of the windowPosition property.
     * 
     * @return
     *     possible object is
     *     {@link WebLinkPosition }
     *     
     */
    public WebLinkPosition getWindowPosition() {
        return windowPosition;
    }

    /**
     * Sets the value of the windowPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebLinkPosition }
     *     
     */
    public void setWindowPosition(WebLinkPosition value) {
        this.windowPosition = value;
    }

}
