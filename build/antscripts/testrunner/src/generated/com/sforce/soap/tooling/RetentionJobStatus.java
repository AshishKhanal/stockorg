
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetentionJobStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RetentionJobStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CopyScheduled"/&gt;
 *     &lt;enumeration value="CopyRunning"/&gt;
 *     &lt;enumeration value="CopySucceeded"/&gt;
 *     &lt;enumeration value="CopyFailed"/&gt;
 *     &lt;enumeration value="CopyKilled"/&gt;
 *     &lt;enumeration value="NothingToArchive"/&gt;
 *     &lt;enumeration value="DeleteScheduled"/&gt;
 *     &lt;enumeration value="DeleteRunning"/&gt;
 *     &lt;enumeration value="DeleteSucceeded"/&gt;
 *     &lt;enumeration value="DeleteFailed"/&gt;
 *     &lt;enumeration value="DeleteKilled"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RetentionJobStatus")
@XmlEnum
public enum RetentionJobStatus {

    @XmlEnumValue("CopyScheduled")
    COPY_SCHEDULED("CopyScheduled"),
    @XmlEnumValue("CopyRunning")
    COPY_RUNNING("CopyRunning"),
    @XmlEnumValue("CopySucceeded")
    COPY_SUCCEEDED("CopySucceeded"),
    @XmlEnumValue("CopyFailed")
    COPY_FAILED("CopyFailed"),
    @XmlEnumValue("CopyKilled")
    COPY_KILLED("CopyKilled"),
    @XmlEnumValue("NothingToArchive")
    NOTHING_TO_ARCHIVE("NothingToArchive"),
    @XmlEnumValue("DeleteScheduled")
    DELETE_SCHEDULED("DeleteScheduled"),
    @XmlEnumValue("DeleteRunning")
    DELETE_RUNNING("DeleteRunning"),
    @XmlEnumValue("DeleteSucceeded")
    DELETE_SUCCEEDED("DeleteSucceeded"),
    @XmlEnumValue("DeleteFailed")
    DELETE_FAILED("DeleteFailed"),
    @XmlEnumValue("DeleteKilled")
    DELETE_KILLED("DeleteKilled");
    private final String value;

    RetentionJobStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RetentionJobStatus fromValue(String v) {
        for (RetentionJobStatus c: RetentionJobStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
