
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WebLinkDisplayType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WebLinkDisplayType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="link"/&gt;
 *     &lt;enumeration value="button"/&gt;
 *     &lt;enumeration value="massActionButton"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WebLinkDisplayType")
@XmlEnum
public enum WebLinkDisplayType {

    @XmlEnumValue("link")
    LINK("link"),
    @XmlEnumValue("button")
    BUTTON("button"),
    @XmlEnumValue("massActionButton")
    MASS_ACTION_BUTTON("massActionButton");
    private final String value;

    WebLinkDisplayType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WebLinkDisplayType fromValue(String v) {
        for (WebLinkDisplayType c: WebLinkDisplayType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
