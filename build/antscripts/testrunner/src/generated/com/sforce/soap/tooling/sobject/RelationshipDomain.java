
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.JunctionIdListNames;


/**
 * <p>Java class for RelationshipDomain complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelationshipDomain"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChildSobject" type="{urn:sobject.tooling.soap.sforce.com}EntityDefinition" minOccurs="0"/&gt;
 *         &lt;element name="ChildSobjectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Field" type="{urn:sobject.tooling.soap.sforce.com}FieldDefinition" minOccurs="0"/&gt;
 *         &lt;element name="FieldId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCascadeDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsDeprecatedAndHidden" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsRestrictedDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="JunctionIdListNames" type="{urn:tooling.soap.sforce.com}JunctionIdListNames" minOccurs="0"/&gt;
 *         &lt;element name="ParentSobject" type="{urn:sobject.tooling.soap.sforce.com}EntityDefinition" minOccurs="0"/&gt;
 *         &lt;element name="ParentSobjectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RelationshipInfo" type="{urn:sobject.tooling.soap.sforce.com}RelationshipInfo" minOccurs="0"/&gt;
 *         &lt;element name="RelationshipInfoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RelationshipName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelationshipDomain", propOrder = {
    "childSobject",
    "childSobjectId",
    "durableId",
    "field",
    "fieldId",
    "isCascadeDelete",
    "isDeprecatedAndHidden",
    "isRestrictedDelete",
    "junctionIdListNames",
    "parentSobject",
    "parentSobjectId",
    "relationshipInfo",
    "relationshipInfoId",
    "relationshipName"
})
public class RelationshipDomain
    extends SObject
{

    @XmlElementRef(name = "ChildSobject", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<EntityDefinition> childSobject;
    @XmlElementRef(name = "ChildSobjectId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> childSobjectId;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "Field", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<FieldDefinition> field;
    @XmlElementRef(name = "FieldId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fieldId;
    @XmlElementRef(name = "IsCascadeDelete", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isCascadeDelete;
    @XmlElementRef(name = "IsDeprecatedAndHidden", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isDeprecatedAndHidden;
    @XmlElementRef(name = "IsRestrictedDelete", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isRestrictedDelete;
    @XmlElementRef(name = "JunctionIdListNames", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<JunctionIdListNames> junctionIdListNames;
    @XmlElementRef(name = "ParentSobject", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<EntityDefinition> parentSobject;
    @XmlElementRef(name = "ParentSobjectId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> parentSobjectId;
    @XmlElementRef(name = "RelationshipInfo", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<RelationshipInfo> relationshipInfo;
    @XmlElementRef(name = "RelationshipInfoId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> relationshipInfoId;
    @XmlElementRef(name = "RelationshipName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> relationshipName;

    /**
     * Gets the value of the childSobject property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EntityDefinition }{@code >}
     *     
     */
    public JAXBElement<EntityDefinition> getChildSobject() {
        return childSobject;
    }

    /**
     * Sets the value of the childSobject property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EntityDefinition }{@code >}
     *     
     */
    public void setChildSobject(JAXBElement<EntityDefinition> value) {
        this.childSobject = value;
    }

    /**
     * Gets the value of the childSobjectId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChildSobjectId() {
        return childSobjectId;
    }

    /**
     * Sets the value of the childSobjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChildSobjectId(JAXBElement<String> value) {
        this.childSobjectId = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the field property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FieldDefinition }{@code >}
     *     
     */
    public JAXBElement<FieldDefinition> getField() {
        return field;
    }

    /**
     * Sets the value of the field property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FieldDefinition }{@code >}
     *     
     */
    public void setField(JAXBElement<FieldDefinition> value) {
        this.field = value;
    }

    /**
     * Gets the value of the fieldId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFieldId() {
        return fieldId;
    }

    /**
     * Sets the value of the fieldId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFieldId(JAXBElement<String> value) {
        this.fieldId = value;
    }

    /**
     * Gets the value of the isCascadeDelete property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsCascadeDelete() {
        return isCascadeDelete;
    }

    /**
     * Sets the value of the isCascadeDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsCascadeDelete(JAXBElement<Boolean> value) {
        this.isCascadeDelete = value;
    }

    /**
     * Gets the value of the isDeprecatedAndHidden property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsDeprecatedAndHidden() {
        return isDeprecatedAndHidden;
    }

    /**
     * Sets the value of the isDeprecatedAndHidden property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsDeprecatedAndHidden(JAXBElement<Boolean> value) {
        this.isDeprecatedAndHidden = value;
    }

    /**
     * Gets the value of the isRestrictedDelete property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsRestrictedDelete() {
        return isRestrictedDelete;
    }

    /**
     * Sets the value of the isRestrictedDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsRestrictedDelete(JAXBElement<Boolean> value) {
        this.isRestrictedDelete = value;
    }

    /**
     * Gets the value of the junctionIdListNames property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link JunctionIdListNames }{@code >}
     *     
     */
    public JAXBElement<JunctionIdListNames> getJunctionIdListNames() {
        return junctionIdListNames;
    }

    /**
     * Sets the value of the junctionIdListNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link JunctionIdListNames }{@code >}
     *     
     */
    public void setJunctionIdListNames(JAXBElement<JunctionIdListNames> value) {
        this.junctionIdListNames = value;
    }

    /**
     * Gets the value of the parentSobject property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EntityDefinition }{@code >}
     *     
     */
    public JAXBElement<EntityDefinition> getParentSobject() {
        return parentSobject;
    }

    /**
     * Sets the value of the parentSobject property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EntityDefinition }{@code >}
     *     
     */
    public void setParentSobject(JAXBElement<EntityDefinition> value) {
        this.parentSobject = value;
    }

    /**
     * Gets the value of the parentSobjectId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getParentSobjectId() {
        return parentSobjectId;
    }

    /**
     * Sets the value of the parentSobjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setParentSobjectId(JAXBElement<String> value) {
        this.parentSobjectId = value;
    }

    /**
     * Gets the value of the relationshipInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RelationshipInfo }{@code >}
     *     
     */
    public JAXBElement<RelationshipInfo> getRelationshipInfo() {
        return relationshipInfo;
    }

    /**
     * Sets the value of the relationshipInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RelationshipInfo }{@code >}
     *     
     */
    public void setRelationshipInfo(JAXBElement<RelationshipInfo> value) {
        this.relationshipInfo = value;
    }

    /**
     * Gets the value of the relationshipInfoId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRelationshipInfoId() {
        return relationshipInfoId;
    }

    /**
     * Sets the value of the relationshipInfoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRelationshipInfoId(JAXBElement<String> value) {
        this.relationshipInfoId = value;
    }

    /**
     * Gets the value of the relationshipName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRelationshipName() {
        return relationshipName;
    }

    /**
     * Sets the value of the relationshipName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRelationshipName(JAXBElement<String> value) {
        this.relationshipName = value;
    }

}
