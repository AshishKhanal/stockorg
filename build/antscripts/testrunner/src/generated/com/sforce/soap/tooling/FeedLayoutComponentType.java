
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeedLayoutComponentType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FeedLayoutComponentType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="HelpAndToolLinks"/&gt;
 *     &lt;enumeration value="CustomButtons"/&gt;
 *     &lt;enumeration value="Following"/&gt;
 *     &lt;enumeration value="Followers"/&gt;
 *     &lt;enumeration value="CustomLinks"/&gt;
 *     &lt;enumeration value="Milestones"/&gt;
 *     &lt;enumeration value="Topics"/&gt;
 *     &lt;enumeration value="CaseUnifiedFiles"/&gt;
 *     &lt;enumeration value="Visualforce"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FeedLayoutComponentType")
@XmlEnum
public enum FeedLayoutComponentType {

    @XmlEnumValue("HelpAndToolLinks")
    HELP_AND_TOOL_LINKS("HelpAndToolLinks"),
    @XmlEnumValue("CustomButtons")
    CUSTOM_BUTTONS("CustomButtons"),
    @XmlEnumValue("Following")
    FOLLOWING("Following"),
    @XmlEnumValue("Followers")
    FOLLOWERS("Followers"),
    @XmlEnumValue("CustomLinks")
    CUSTOM_LINKS("CustomLinks"),
    @XmlEnumValue("Milestones")
    MILESTONES("Milestones"),
    @XmlEnumValue("Topics")
    TOPICS("Topics"),
    @XmlEnumValue("CaseUnifiedFiles")
    CASE_UNIFIED_FILES("CaseUnifiedFiles"),
    @XmlEnumValue("Visualforce")
    VISUALFORCE("Visualforce");
    private final String value;

    FeedLayoutComponentType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FeedLayoutComponentType fromValue(String v) {
        for (FeedLayoutComponentType c: FeedLayoutComponentType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
