
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeedItemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FeedItemType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="TrackedChange"/&gt;
 *     &lt;enumeration value="UserStatus"/&gt;
 *     &lt;enumeration value="TextPost"/&gt;
 *     &lt;enumeration value="AdvancedTextPost"/&gt;
 *     &lt;enumeration value="LinkPost"/&gt;
 *     &lt;enumeration value="ContentPost"/&gt;
 *     &lt;enumeration value="PollPost"/&gt;
 *     &lt;enumeration value="RypplePost"/&gt;
 *     &lt;enumeration value="ProfileSkillPost"/&gt;
 *     &lt;enumeration value="DashboardComponentSnapshot"/&gt;
 *     &lt;enumeration value="ApprovalPost"/&gt;
 *     &lt;enumeration value="CaseCommentPost"/&gt;
 *     &lt;enumeration value="ReplyPost"/&gt;
 *     &lt;enumeration value="EmailMessageEvent"/&gt;
 *     &lt;enumeration value="CallLogPost"/&gt;
 *     &lt;enumeration value="ChangeStatusPost"/&gt;
 *     &lt;enumeration value="AttachArticleEvent"/&gt;
 *     &lt;enumeration value="MilestoneEvent"/&gt;
 *     &lt;enumeration value="ActivityEvent"/&gt;
 *     &lt;enumeration value="ChatTranscriptPost"/&gt;
 *     &lt;enumeration value="CollaborationGroupCreated"/&gt;
 *     &lt;enumeration value="CollaborationGroupUnarchived"/&gt;
 *     &lt;enumeration value="SocialPost"/&gt;
 *     &lt;enumeration value="QuestionPost"/&gt;
 *     &lt;enumeration value="FacebookPost"/&gt;
 *     &lt;enumeration value="BasicTemplateFeedItem"/&gt;
 *     &lt;enumeration value="CreateRecordEvent"/&gt;
 *     &lt;enumeration value="CanvasPost"/&gt;
 *     &lt;enumeration value="AnnouncementPost"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FeedItemType")
@XmlEnum
public enum FeedItemType {

    @XmlEnumValue("TrackedChange")
    TRACKED_CHANGE("TrackedChange"),
    @XmlEnumValue("UserStatus")
    USER_STATUS("UserStatus"),
    @XmlEnumValue("TextPost")
    TEXT_POST("TextPost"),
    @XmlEnumValue("AdvancedTextPost")
    ADVANCED_TEXT_POST("AdvancedTextPost"),
    @XmlEnumValue("LinkPost")
    LINK_POST("LinkPost"),
    @XmlEnumValue("ContentPost")
    CONTENT_POST("ContentPost"),
    @XmlEnumValue("PollPost")
    POLL_POST("PollPost"),
    @XmlEnumValue("RypplePost")
    RYPPLE_POST("RypplePost"),
    @XmlEnumValue("ProfileSkillPost")
    PROFILE_SKILL_POST("ProfileSkillPost"),
    @XmlEnumValue("DashboardComponentSnapshot")
    DASHBOARD_COMPONENT_SNAPSHOT("DashboardComponentSnapshot"),
    @XmlEnumValue("ApprovalPost")
    APPROVAL_POST("ApprovalPost"),
    @XmlEnumValue("CaseCommentPost")
    CASE_COMMENT_POST("CaseCommentPost"),
    @XmlEnumValue("ReplyPost")
    REPLY_POST("ReplyPost"),
    @XmlEnumValue("EmailMessageEvent")
    EMAIL_MESSAGE_EVENT("EmailMessageEvent"),
    @XmlEnumValue("CallLogPost")
    CALL_LOG_POST("CallLogPost"),
    @XmlEnumValue("ChangeStatusPost")
    CHANGE_STATUS_POST("ChangeStatusPost"),
    @XmlEnumValue("AttachArticleEvent")
    ATTACH_ARTICLE_EVENT("AttachArticleEvent"),
    @XmlEnumValue("MilestoneEvent")
    MILESTONE_EVENT("MilestoneEvent"),
    @XmlEnumValue("ActivityEvent")
    ACTIVITY_EVENT("ActivityEvent"),
    @XmlEnumValue("ChatTranscriptPost")
    CHAT_TRANSCRIPT_POST("ChatTranscriptPost"),
    @XmlEnumValue("CollaborationGroupCreated")
    COLLABORATION_GROUP_CREATED("CollaborationGroupCreated"),
    @XmlEnumValue("CollaborationGroupUnarchived")
    COLLABORATION_GROUP_UNARCHIVED("CollaborationGroupUnarchived"),
    @XmlEnumValue("SocialPost")
    SOCIAL_POST("SocialPost"),
    @XmlEnumValue("QuestionPost")
    QUESTION_POST("QuestionPost"),
    @XmlEnumValue("FacebookPost")
    FACEBOOK_POST("FacebookPost"),
    @XmlEnumValue("BasicTemplateFeedItem")
    BASIC_TEMPLATE_FEED_ITEM("BasicTemplateFeedItem"),
    @XmlEnumValue("CreateRecordEvent")
    CREATE_RECORD_EVENT("CreateRecordEvent"),
    @XmlEnumValue("CanvasPost")
    CANVAS_POST("CanvasPost"),
    @XmlEnumValue("AnnouncementPost")
    ANNOUNCEMENT_POST("AnnouncementPost");
    private final String value;

    FeedItemType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FeedItemType fromValue(String v) {
        for (FeedItemType c: FeedItemType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
