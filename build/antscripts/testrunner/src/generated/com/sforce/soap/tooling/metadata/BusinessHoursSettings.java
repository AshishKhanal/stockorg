
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessHoursSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessHoursSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="businessHours" type="{urn:metadata.tooling.soap.sforce.com}BusinessHoursEntry" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="holidays" type="{urn:metadata.tooling.soap.sforce.com}Holiday" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessHoursSettings", propOrder = {
    "businessHours",
    "holidays"
})
public class BusinessHoursSettings
    extends MetadataForSettings
{

    protected List<BusinessHoursEntry> businessHours;
    protected List<Holiday> holidays;

    /**
     * Gets the value of the businessHours property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessHours property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessHours().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusinessHoursEntry }
     * 
     * 
     */
    public List<BusinessHoursEntry> getBusinessHours() {
        if (businessHours == null) {
            businessHours = new ArrayList<BusinessHoursEntry>();
        }
        return this.businessHours;
    }

    /**
     * Gets the value of the holidays property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the holidays property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHolidays().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Holiday }
     * 
     * 
     */
    public List<Holiday> getHolidays() {
        if (holidays == null) {
            holidays = new ArrayList<Holiday>();
        }
        return this.holidays;
    }

}
