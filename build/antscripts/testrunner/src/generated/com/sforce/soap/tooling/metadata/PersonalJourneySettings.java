
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonalJourneySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonalJourneySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableExactTargetForSalesforceApps" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalJourneySettings", propOrder = {
    "enableExactTargetForSalesforceApps"
})
public class PersonalJourneySettings
    extends MetadataForSettings
{

    protected boolean enableExactTargetForSalesforceApps;

    /**
     * Gets the value of the enableExactTargetForSalesforceApps property.
     * 
     */
    public boolean isEnableExactTargetForSalesforceApps() {
        return enableExactTargetForSalesforceApps;
    }

    /**
     * Sets the value of the enableExactTargetForSalesforceApps property.
     * 
     */
    public void setEnableExactTargetForSalesforceApps(boolean value) {
        this.enableExactTargetForSalesforceApps = value;
    }

}
