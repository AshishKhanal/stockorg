
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecuritySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecuritySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="networkAccess" type="{urn:metadata.tooling.soap.sforce.com}NetworkAccess" minOccurs="0"/&gt;
 *         &lt;element name="passwordPolicies" type="{urn:metadata.tooling.soap.sforce.com}PasswordPolicies" minOccurs="0"/&gt;
 *         &lt;element name="sessionSettings" type="{urn:metadata.tooling.soap.sforce.com}SessionSettings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecuritySettings", propOrder = {
    "networkAccess",
    "passwordPolicies",
    "sessionSettings"
})
public class SecuritySettings
    extends MetadataForSettings
{

    protected NetworkAccess networkAccess;
    protected PasswordPolicies passwordPolicies;
    protected SessionSettings sessionSettings;

    /**
     * Gets the value of the networkAccess property.
     * 
     * @return
     *     possible object is
     *     {@link NetworkAccess }
     *     
     */
    public NetworkAccess getNetworkAccess() {
        return networkAccess;
    }

    /**
     * Sets the value of the networkAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link NetworkAccess }
     *     
     */
    public void setNetworkAccess(NetworkAccess value) {
        this.networkAccess = value;
    }

    /**
     * Gets the value of the passwordPolicies property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordPolicies }
     *     
     */
    public PasswordPolicies getPasswordPolicies() {
        return passwordPolicies;
    }

    /**
     * Sets the value of the passwordPolicies property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordPolicies }
     *     
     */
    public void setPasswordPolicies(PasswordPolicies value) {
        this.passwordPolicies = value;
    }

    /**
     * Gets the value of the sessionSettings property.
     * 
     * @return
     *     possible object is
     *     {@link SessionSettings }
     *     
     */
    public SessionSettings getSessionSettings() {
        return sessionSettings;
    }

    /**
     * Sets the value of the sessionSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionSettings }
     *     
     */
    public void setSessionSettings(SessionSettings value) {
        this.sessionSettings = value;
    }

}
