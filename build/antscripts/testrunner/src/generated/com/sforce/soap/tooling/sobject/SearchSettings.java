
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocumentContentSearchEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}SearchSettings" minOccurs="0"/&gt;
 *         &lt;element name="OptimizeSearchForCjkEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="RecentlyViewedUsersForBlankLookupEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SearchSettingsByObject" type="{urn:sobject.tooling.soap.sforce.com}SearchSettingsByObject" minOccurs="0"/&gt;
 *         &lt;element name="SearchSettingsByObjectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SidebarAutoCompleteEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SidebarDropDownListEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SidebarLimitToItemsIownCheckboxEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SingleSearchResultShortcutEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SpellCorrectKnowledgeSearchEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchSettings", propOrder = {
    "documentContentSearchEnabled",
    "durableId",
    "fullName",
    "metadata",
    "optimizeSearchForCjkEnabled",
    "recentlyViewedUsersForBlankLookupEnabled",
    "searchSettingsByObject",
    "searchSettingsByObjectId",
    "sidebarAutoCompleteEnabled",
    "sidebarDropDownListEnabled",
    "sidebarLimitToItemsIownCheckboxEnabled",
    "singleSearchResultShortcutEnabled",
    "spellCorrectKnowledgeSearchEnabled"
})
public class SearchSettings
    extends SObject
{

    @XmlElementRef(name = "DocumentContentSearchEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> documentContentSearchEnabled;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.SearchSettings> metadata;
    @XmlElementRef(name = "OptimizeSearchForCjkEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> optimizeSearchForCjkEnabled;
    @XmlElementRef(name = "RecentlyViewedUsersForBlankLookupEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> recentlyViewedUsersForBlankLookupEnabled;
    @XmlElementRef(name = "SearchSettingsByObject", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SearchSettingsByObject> searchSettingsByObject;
    @XmlElementRef(name = "SearchSettingsByObjectId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> searchSettingsByObjectId;
    @XmlElementRef(name = "SidebarAutoCompleteEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> sidebarAutoCompleteEnabled;
    @XmlElementRef(name = "SidebarDropDownListEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> sidebarDropDownListEnabled;
    @XmlElementRef(name = "SidebarLimitToItemsIownCheckboxEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> sidebarLimitToItemsIownCheckboxEnabled;
    @XmlElementRef(name = "SingleSearchResultShortcutEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> singleSearchResultShortcutEnabled;
    @XmlElementRef(name = "SpellCorrectKnowledgeSearchEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> spellCorrectKnowledgeSearchEnabled;

    /**
     * Gets the value of the documentContentSearchEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDocumentContentSearchEnabled() {
        return documentContentSearchEnabled;
    }

    /**
     * Sets the value of the documentContentSearchEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDocumentContentSearchEnabled(JAXBElement<Boolean> value) {
        this.documentContentSearchEnabled = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.SearchSettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.SearchSettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.SearchSettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.SearchSettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the optimizeSearchForCjkEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getOptimizeSearchForCjkEnabled() {
        return optimizeSearchForCjkEnabled;
    }

    /**
     * Sets the value of the optimizeSearchForCjkEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setOptimizeSearchForCjkEnabled(JAXBElement<Boolean> value) {
        this.optimizeSearchForCjkEnabled = value;
    }

    /**
     * Gets the value of the recentlyViewedUsersForBlankLookupEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getRecentlyViewedUsersForBlankLookupEnabled() {
        return recentlyViewedUsersForBlankLookupEnabled;
    }

    /**
     * Sets the value of the recentlyViewedUsersForBlankLookupEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setRecentlyViewedUsersForBlankLookupEnabled(JAXBElement<Boolean> value) {
        this.recentlyViewedUsersForBlankLookupEnabled = value;
    }

    /**
     * Gets the value of the searchSettingsByObject property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SearchSettingsByObject }{@code >}
     *     
     */
    public JAXBElement<SearchSettingsByObject> getSearchSettingsByObject() {
        return searchSettingsByObject;
    }

    /**
     * Sets the value of the searchSettingsByObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SearchSettingsByObject }{@code >}
     *     
     */
    public void setSearchSettingsByObject(JAXBElement<SearchSettingsByObject> value) {
        this.searchSettingsByObject = value;
    }

    /**
     * Gets the value of the searchSettingsByObjectId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSearchSettingsByObjectId() {
        return searchSettingsByObjectId;
    }

    /**
     * Sets the value of the searchSettingsByObjectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSearchSettingsByObjectId(JAXBElement<String> value) {
        this.searchSettingsByObjectId = value;
    }

    /**
     * Gets the value of the sidebarAutoCompleteEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSidebarAutoCompleteEnabled() {
        return sidebarAutoCompleteEnabled;
    }

    /**
     * Sets the value of the sidebarAutoCompleteEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSidebarAutoCompleteEnabled(JAXBElement<Boolean> value) {
        this.sidebarAutoCompleteEnabled = value;
    }

    /**
     * Gets the value of the sidebarDropDownListEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSidebarDropDownListEnabled() {
        return sidebarDropDownListEnabled;
    }

    /**
     * Sets the value of the sidebarDropDownListEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSidebarDropDownListEnabled(JAXBElement<Boolean> value) {
        this.sidebarDropDownListEnabled = value;
    }

    /**
     * Gets the value of the sidebarLimitToItemsIownCheckboxEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSidebarLimitToItemsIownCheckboxEnabled() {
        return sidebarLimitToItemsIownCheckboxEnabled;
    }

    /**
     * Sets the value of the sidebarLimitToItemsIownCheckboxEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSidebarLimitToItemsIownCheckboxEnabled(JAXBElement<Boolean> value) {
        this.sidebarLimitToItemsIownCheckboxEnabled = value;
    }

    /**
     * Gets the value of the singleSearchResultShortcutEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSingleSearchResultShortcutEnabled() {
        return singleSearchResultShortcutEnabled;
    }

    /**
     * Sets the value of the singleSearchResultShortcutEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSingleSearchResultShortcutEnabled(JAXBElement<Boolean> value) {
        this.singleSearchResultShortcutEnabled = value;
    }

    /**
     * Gets the value of the spellCorrectKnowledgeSearchEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getSpellCorrectKnowledgeSearchEnabled() {
        return spellCorrectKnowledgeSearchEnabled;
    }

    /**
     * Sets the value of the spellCorrectKnowledgeSearchEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setSpellCorrectKnowledgeSearchEnabled(JAXBElement<Boolean> value) {
        this.spellCorrectKnowledgeSearchEnabled = value;
    }

}
