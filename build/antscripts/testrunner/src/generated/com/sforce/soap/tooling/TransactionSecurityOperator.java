
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionSecurityOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionSecurityOperator"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Equal"/&gt;
 *     &lt;enumeration value="NotEqual"/&gt;
 *     &lt;enumeration value="LessThan"/&gt;
 *     &lt;enumeration value="GreaterThan"/&gt;
 *     &lt;enumeration value="LessThanOrEqualTo"/&gt;
 *     &lt;enumeration value="GreaterThanOrEqualTo"/&gt;
 *     &lt;enumeration value="Contain"/&gt;
 *     &lt;enumeration value="NotContain"/&gt;
 *     &lt;enumeration value="Range"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionSecurityOperator")
@XmlEnum
public enum TransactionSecurityOperator {

    @XmlEnumValue("Equal")
    EQUAL("Equal"),
    @XmlEnumValue("NotEqual")
    NOT_EQUAL("NotEqual"),
    @XmlEnumValue("LessThan")
    LESS_THAN("LessThan"),
    @XmlEnumValue("GreaterThan")
    GREATER_THAN("GreaterThan"),
    @XmlEnumValue("LessThanOrEqualTo")
    LESS_THAN_OR_EQUAL_TO("LessThanOrEqualTo"),
    @XmlEnumValue("GreaterThanOrEqualTo")
    GREATER_THAN_OR_EQUAL_TO("GreaterThanOrEqualTo"),
    @XmlEnumValue("Contain")
    CONTAIN("Contain"),
    @XmlEnumValue("NotContain")
    NOT_CONTAIN("NotContain"),
    @XmlEnumValue("Range")
    RANGE("Range");
    private final String value;

    TransactionSecurityOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionSecurityOperator fromValue(String v) {
        for (TransactionSecurityOperator c: TransactionSecurityOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
