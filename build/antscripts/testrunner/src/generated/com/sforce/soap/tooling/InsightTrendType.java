
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InsightTrendType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InsightTrendType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Positive"/&gt;
 *     &lt;enumeration value="Negative"/&gt;
 *     &lt;enumeration value="Informational"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InsightTrendType")
@XmlEnum
public enum InsightTrendType {

    @XmlEnumValue("Positive")
    POSITIVE("Positive"),
    @XmlEnumValue("Negative")
    NEGATIVE("Negative"),
    @XmlEnumValue("Informational")
    INFORMATIONAL("Informational");
    private final String value;

    InsightTrendType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InsightTrendType fromValue(String v) {
        for (InsightTrendType c: InsightTrendType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
