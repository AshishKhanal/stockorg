
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FeedItemDisplayFormat;
import com.sforce.soap.tooling.FeedItemType;


/**
 * <p>Java class for FeedItemSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeedItemSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="characterLimit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="collapseThread" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="displayFormat" type="{urn:tooling.soap.sforce.com}FeedItemDisplayFormat" minOccurs="0"/&gt;
 *         &lt;element name="feedItemType" type="{urn:tooling.soap.sforce.com}FeedItemType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeedItemSettings", propOrder = {
    "characterLimit",
    "collapseThread",
    "displayFormat",
    "feedItemType"
})
public class FeedItemSettings {

    protected Integer characterLimit;
    protected Boolean collapseThread;
    @XmlSchemaType(name = "string")
    protected FeedItemDisplayFormat displayFormat;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FeedItemType feedItemType;

    /**
     * Gets the value of the characterLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCharacterLimit() {
        return characterLimit;
    }

    /**
     * Sets the value of the characterLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCharacterLimit(Integer value) {
        this.characterLimit = value;
    }

    /**
     * Gets the value of the collapseThread property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCollapseThread() {
        return collapseThread;
    }

    /**
     * Sets the value of the collapseThread property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCollapseThread(Boolean value) {
        this.collapseThread = value;
    }

    /**
     * Gets the value of the displayFormat property.
     * 
     * @return
     *     possible object is
     *     {@link FeedItemDisplayFormat }
     *     
     */
    public FeedItemDisplayFormat getDisplayFormat() {
        return displayFormat;
    }

    /**
     * Sets the value of the displayFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeedItemDisplayFormat }
     *     
     */
    public void setDisplayFormat(FeedItemDisplayFormat value) {
        this.displayFormat = value;
    }

    /**
     * Gets the value of the feedItemType property.
     * 
     * @return
     *     possible object is
     *     {@link FeedItemType }
     *     
     */
    public FeedItemType getFeedItemType() {
        return feedItemType;
    }

    /**
     * Sets the value of the feedItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeedItemType }
     *     
     */
    public void setFeedItemType(FeedItemType value) {
        this.feedItemType = value;
    }

}
