
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowAssignmentOperator.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlowAssignmentOperator"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Assign"/&gt;
 *     &lt;enumeration value="Add"/&gt;
 *     &lt;enumeration value="Subtract"/&gt;
 *     &lt;enumeration value="AddItem"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlowAssignmentOperator")
@XmlEnum
public enum FlowAssignmentOperator {

    @XmlEnumValue("Assign")
    ASSIGN("Assign"),
    @XmlEnumValue("Add")
    ADD("Add"),
    @XmlEnumValue("Subtract")
    SUBTRACT("Subtract"),
    @XmlEnumValue("AddItem")
    ADD_ITEM("AddItem");
    private final String value;

    FlowAssignmentOperator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlowAssignmentOperator fromValue(String v) {
        for (FlowAssignmentOperator c: FlowAssignmentOperator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
