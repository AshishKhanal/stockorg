
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VisibilityOperationStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VisibilityOperationStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="New"/&gt;
 *     &lt;enumeration value="Completed"/&gt;
 *     &lt;enumeration value="InProgress"/&gt;
 *     &lt;enumeration value="Deferred"/&gt;
 *     &lt;enumeration value="Error"/&gt;
 *     &lt;enumeration value="Recovering"/&gt;
 *     &lt;enumeration value="Recovered"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VisibilityOperationStatus")
@XmlEnum
public enum VisibilityOperationStatus {

    @XmlEnumValue("New")
    NEW("New"),
    @XmlEnumValue("Completed")
    COMPLETED("Completed"),
    @XmlEnumValue("InProgress")
    IN_PROGRESS("InProgress"),
    @XmlEnumValue("Deferred")
    DEFERRED("Deferred"),
    @XmlEnumValue("Error")
    ERROR("Error"),
    @XmlEnumValue("Recovering")
    RECOVERING("Recovering"),
    @XmlEnumValue("Recovered")
    RECOVERED("Recovered");
    private final String value;

    VisibilityOperationStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VisibilityOperationStatus fromValue(String v) {
        for (VisibilityOperationStatus c: VisibilityOperationStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
