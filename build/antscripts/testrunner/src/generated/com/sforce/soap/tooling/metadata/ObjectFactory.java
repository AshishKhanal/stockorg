
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sforce.soap.tooling.metadata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FlowElementReferenceOrValueBooleanValue_QNAME = new QName("urn:metadata.tooling.soap.sforce.com", "booleanValue");
    private final static QName _FlowElementReferenceOrValueNumberValue_QNAME = new QName("urn:metadata.tooling.soap.sforce.com", "numberValue");
    private final static QName _CertificateEncryptedWithPlatformEncryption_QNAME = new QName("urn:metadata.tooling.soap.sforce.com", "encryptedWithPlatformEncryption");
    private final static QName _CertificateExpirationDate_QNAME = new QName("urn:metadata.tooling.soap.sforce.com", "expirationDate");
    private final static QName _CertificatePrivateKeyExportable_QNAME = new QName("urn:metadata.tooling.soap.sforce.com", "privateKeyExportable");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sforce.soap.tooling.metadata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccountSettings }
     * 
     */
    public AccountSettings createAccountSettings() {
        return new AccountSettings();
    }

    /**
     * Create an instance of {@link MetadataForSettings }
     * 
     */
    public MetadataForSettings createMetadataForSettings() {
        return new MetadataForSettings();
    }

    /**
     * Create an instance of {@link Metadata }
     * 
     */
    public Metadata createMetadata() {
        return new Metadata();
    }

    /**
     * Create an instance of {@link ApexClass }
     * 
     */
    public ApexClass createApexClass() {
        return new ApexClass();
    }

    /**
     * Create an instance of {@link PackageVersion }
     * 
     */
    public PackageVersion createPackageVersion() {
        return new PackageVersion();
    }

    /**
     * Create an instance of {@link ApexComponent }
     * 
     */
    public ApexComponent createApexComponent() {
        return new ApexComponent();
    }

    /**
     * Create an instance of {@link ApexPage }
     * 
     */
    public ApexPage createApexPage() {
        return new ApexPage();
    }

    /**
     * Create an instance of {@link ApexTestSuite }
     * 
     */
    public ApexTestSuite createApexTestSuite() {
        return new ApexTestSuite();
    }

    /**
     * Create an instance of {@link ApexTrigger }
     * 
     */
    public ApexTrigger createApexTrigger() {
        return new ApexTrigger();
    }

    /**
     * Create an instance of {@link AssignmentRule }
     * 
     */
    public AssignmentRule createAssignmentRule() {
        return new AssignmentRule();
    }

    /**
     * Create an instance of {@link RuleEntry }
     * 
     */
    public RuleEntry createRuleEntry() {
        return new RuleEntry();
    }

    /**
     * Create an instance of {@link FilterItem }
     * 
     */
    public FilterItem createFilterItem() {
        return new FilterItem();
    }

    /**
     * Create an instance of {@link EscalationAction }
     * 
     */
    public EscalationAction createEscalationAction() {
        return new EscalationAction();
    }

    /**
     * Create an instance of {@link AssignmentRules }
     * 
     */
    public AssignmentRules createAssignmentRules() {
        return new AssignmentRules();
    }

    /**
     * Create an instance of {@link AuraDefinitionBundle }
     * 
     */
    public AuraDefinitionBundle createAuraDefinitionBundle() {
        return new AuraDefinitionBundle();
    }

    /**
     * Create an instance of {@link AutoResponseRule }
     * 
     */
    public AutoResponseRule createAutoResponseRule() {
        return new AutoResponseRule();
    }

    /**
     * Create an instance of {@link AutoResponseRules }
     * 
     */
    public AutoResponseRules createAutoResponseRules() {
        return new AutoResponseRules();
    }

    /**
     * Create an instance of {@link BusinessHoursEntry }
     * 
     */
    public BusinessHoursEntry createBusinessHoursEntry() {
        return new BusinessHoursEntry();
    }

    /**
     * Create an instance of {@link BusinessProcess }
     * 
     */
    public BusinessProcess createBusinessProcess() {
        return new BusinessProcess();
    }

    /**
     * Create an instance of {@link PicklistValue }
     * 
     */
    public PicklistValue createPicklistValue() {
        return new PicklistValue();
    }

    /**
     * Create an instance of {@link GlobalPicklistValue }
     * 
     */
    public GlobalPicklistValue createGlobalPicklistValue() {
        return new GlobalPicklistValue();
    }

    /**
     * Create an instance of {@link Certificate }
     * 
     */
    public Certificate createCertificate() {
        return new Certificate();
    }

    /**
     * Create an instance of {@link CleanDataService }
     * 
     */
    public CleanDataService createCleanDataService() {
        return new CleanDataService();
    }

    /**
     * Create an instance of {@link CleanRule }
     * 
     */
    public CleanRule createCleanRule() {
        return new CleanRule();
    }

    /**
     * Create an instance of {@link FieldMapping }
     * 
     */
    public FieldMapping createFieldMapping() {
        return new FieldMapping();
    }

    /**
     * Create an instance of {@link FieldMappingRow }
     * 
     */
    public FieldMappingRow createFieldMappingRow() {
        return new FieldMappingRow();
    }

    /**
     * Create an instance of {@link FieldMappingField }
     * 
     */
    public FieldMappingField createFieldMappingField() {
        return new FieldMappingField();
    }

    /**
     * Create an instance of {@link Community }
     * 
     */
    public Community createCommunity() {
        return new Community();
    }

    /**
     * Create an instance of {@link ReputationLevels }
     * 
     */
    public ReputationLevels createReputationLevels() {
        return new ReputationLevels();
    }

    /**
     * Create an instance of {@link ChatterAnswersReputationLevel }
     * 
     */
    public ChatterAnswersReputationLevel createChatterAnswersReputationLevel() {
        return new ChatterAnswersReputationLevel();
    }

    /**
     * Create an instance of {@link IdeaReputationLevel }
     * 
     */
    public IdeaReputationLevel createIdeaReputationLevel() {
        return new IdeaReputationLevel();
    }

    /**
     * Create an instance of {@link CommunityTemplateDefinition }
     * 
     */
    public CommunityTemplateDefinition createCommunityTemplateDefinition() {
        return new CommunityTemplateDefinition();
    }

    /**
     * Create an instance of {@link CommunityTemplateBundleInfo }
     * 
     */
    public CommunityTemplateBundleInfo createCommunityTemplateBundleInfo() {
        return new CommunityTemplateBundleInfo();
    }

    /**
     * Create an instance of {@link CommunityTemplatePageSetting }
     * 
     */
    public CommunityTemplatePageSetting createCommunityTemplatePageSetting() {
        return new CommunityTemplatePageSetting();
    }

    /**
     * Create an instance of {@link CommunityThemeDefinition }
     * 
     */
    public CommunityThemeDefinition createCommunityThemeDefinition() {
        return new CommunityThemeDefinition();
    }

    /**
     * Create an instance of {@link CommunityCustomThemeLayoutType }
     * 
     */
    public CommunityCustomThemeLayoutType createCommunityCustomThemeLayoutType() {
        return new CommunityCustomThemeLayoutType();
    }

    /**
     * Create an instance of {@link CommunityThemeSetting }
     * 
     */
    public CommunityThemeSetting createCommunityThemeSetting() {
        return new CommunityThemeSetting();
    }

    /**
     * Create an instance of {@link CompactLayout }
     * 
     */
    public CompactLayout createCompactLayout() {
        return new CompactLayout();
    }

    /**
     * Create an instance of {@link CspTrustedSite }
     * 
     */
    public CspTrustedSite createCspTrustedSite() {
        return new CspTrustedSite();
    }

    /**
     * Create an instance of {@link CustomApplication }
     * 
     */
    public CustomApplication createCustomApplication() {
        return new CustomApplication();
    }

    /**
     * Create an instance of {@link AppActionOverride }
     * 
     */
    public AppActionOverride createAppActionOverride() {
        return new AppActionOverride();
    }

    /**
     * Create an instance of {@link ActionOverride }
     * 
     */
    public ActionOverride createActionOverride() {
        return new ActionOverride();
    }

    /**
     * Create an instance of {@link AppBrand }
     * 
     */
    public AppBrand createAppBrand() {
        return new AppBrand();
    }

    /**
     * Create an instance of {@link KeyboardShortcuts }
     * 
     */
    public KeyboardShortcuts createKeyboardShortcuts() {
        return new KeyboardShortcuts();
    }

    /**
     * Create an instance of {@link CustomShortcut }
     * 
     */
    public CustomShortcut createCustomShortcut() {
        return new CustomShortcut();
    }

    /**
     * Create an instance of {@link DefaultShortcut }
     * 
     */
    public DefaultShortcut createDefaultShortcut() {
        return new DefaultShortcut();
    }

    /**
     * Create an instance of {@link ListPlacement }
     * 
     */
    public ListPlacement createListPlacement() {
        return new ListPlacement();
    }

    /**
     * Create an instance of {@link LiveAgentConfig }
     * 
     */
    public LiveAgentConfig createLiveAgentConfig() {
        return new LiveAgentConfig();
    }

    /**
     * Create an instance of {@link AppProfileActionOverride }
     * 
     */
    public AppProfileActionOverride createAppProfileActionOverride() {
        return new AppProfileActionOverride();
    }

    /**
     * Create an instance of {@link ProfileActionOverride }
     * 
     */
    public ProfileActionOverride createProfileActionOverride() {
        return new ProfileActionOverride();
    }

    /**
     * Create an instance of {@link TabLimitConfig }
     * 
     */
    public TabLimitConfig createTabLimitConfig() {
        return new TabLimitConfig();
    }

    /**
     * Create an instance of {@link WorkspaceMappings }
     * 
     */
    public WorkspaceMappings createWorkspaceMappings() {
        return new WorkspaceMappings();
    }

    /**
     * Create an instance of {@link WorkspaceMapping }
     * 
     */
    public WorkspaceMapping createWorkspaceMapping() {
        return new WorkspaceMapping();
    }

    /**
     * Create an instance of {@link CustomField }
     * 
     */
    public CustomField createCustomField() {
        return new CustomField();
    }

    /**
     * Create an instance of {@link LookupFilter }
     * 
     */
    public LookupFilter createLookupFilter() {
        return new LookupFilter();
    }

    /**
     * Create an instance of {@link ValueSet }
     * 
     */
    public ValueSet createValueSet() {
        return new ValueSet();
    }

    /**
     * Create an instance of {@link ValueSetValuesDefinition }
     * 
     */
    public ValueSetValuesDefinition createValueSetValuesDefinition() {
        return new ValueSetValuesDefinition();
    }

    /**
     * Create an instance of {@link CustomValue }
     * 
     */
    public CustomValue createCustomValue() {
        return new CustomValue();
    }

    /**
     * Create an instance of {@link StandardValue }
     * 
     */
    public StandardValue createStandardValue() {
        return new StandardValue();
    }

    /**
     * Create an instance of {@link ValueSettings }
     * 
     */
    public ValueSettings createValueSettings() {
        return new ValueSettings();
    }

    /**
     * Create an instance of {@link CustomLabel }
     * 
     */
    public CustomLabel createCustomLabel() {
        return new CustomLabel();
    }

    /**
     * Create an instance of {@link CustomLabels }
     * 
     */
    public CustomLabels createCustomLabels() {
        return new CustomLabels();
    }

    /**
     * Create an instance of {@link CustomObject }
     * 
     */
    public CustomObject createCustomObject() {
        return new CustomObject();
    }

    /**
     * Create an instance of {@link SearchLayouts }
     * 
     */
    public SearchLayouts createSearchLayouts() {
        return new SearchLayouts();
    }

    /**
     * Create an instance of {@link CustomPageWebLink }
     * 
     */
    public CustomPageWebLink createCustomPageWebLink() {
        return new CustomPageWebLink();
    }

    /**
     * Create an instance of {@link CustomPermission }
     * 
     */
    public CustomPermission createCustomPermission() {
        return new CustomPermission();
    }

    /**
     * Create an instance of {@link CustomTab }
     * 
     */
    public CustomTab createCustomTab() {
        return new CustomTab();
    }

    /**
     * Create an instance of {@link DataPipeline }
     * 
     */
    public DataPipeline createDataPipeline() {
        return new DataPipeline();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link EmailTemplate }
     * 
     */
    public EmailTemplate createEmailTemplate() {
        return new EmailTemplate();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link EmbeddedServiceBranding }
     * 
     */
    public EmbeddedServiceBranding createEmbeddedServiceBranding() {
        return new EmbeddedServiceBranding();
    }

    /**
     * Create an instance of {@link EmbeddedServiceConfig }
     * 
     */
    public EmbeddedServiceConfig createEmbeddedServiceConfig() {
        return new EmbeddedServiceConfig();
    }

    /**
     * Create an instance of {@link EmbeddedServiceLiveAgent }
     * 
     */
    public EmbeddedServiceLiveAgent createEmbeddedServiceLiveAgent() {
        return new EmbeddedServiceLiveAgent();
    }

    /**
     * Create an instance of {@link EmbeddedServiceQuickAction }
     * 
     */
    public EmbeddedServiceQuickAction createEmbeddedServiceQuickAction() {
        return new EmbeddedServiceQuickAction();
    }

    /**
     * Create an instance of {@link EscalationRule }
     * 
     */
    public EscalationRule createEscalationRule() {
        return new EscalationRule();
    }

    /**
     * Create an instance of {@link EscalationRules }
     * 
     */
    public EscalationRules createEscalationRules() {
        return new EscalationRules();
    }

    /**
     * Create an instance of {@link EventDelivery }
     * 
     */
    public EventDelivery createEventDelivery() {
        return new EventDelivery();
    }

    /**
     * Create an instance of {@link EventParameterMap }
     * 
     */
    public EventParameterMap createEventParameterMap() {
        return new EventParameterMap();
    }

    /**
     * Create an instance of {@link EventSubscription }
     * 
     */
    public EventSubscription createEventSubscription() {
        return new EventSubscription();
    }

    /**
     * Create an instance of {@link EventType }
     * 
     */
    public EventType createEventType() {
        return new EventType();
    }

    /**
     * Create an instance of {@link EventTypeParameter }
     * 
     */
    public EventTypeParameter createEventTypeParameter() {
        return new EventTypeParameter();
    }

    /**
     * Create an instance of {@link FieldSet }
     * 
     */
    public FieldSet createFieldSet() {
        return new FieldSet();
    }

    /**
     * Create an instance of {@link FieldSetItem }
     * 
     */
    public FieldSetItem createFieldSetItem() {
        return new FieldSetItem();
    }

    /**
     * Create an instance of {@link FlexiPage }
     * 
     */
    public FlexiPage createFlexiPage() {
        return new FlexiPage();
    }

    /**
     * Create an instance of {@link FlexiPageRegion }
     * 
     */
    public FlexiPageRegion createFlexiPageRegion() {
        return new FlexiPageRegion();
    }

    /**
     * Create an instance of {@link ComponentInstance }
     * 
     */
    public ComponentInstance createComponentInstance() {
        return new ComponentInstance();
    }

    /**
     * Create an instance of {@link ComponentInstanceProperty }
     * 
     */
    public ComponentInstanceProperty createComponentInstanceProperty() {
        return new ComponentInstanceProperty();
    }

    /**
     * Create an instance of {@link QuickActionList }
     * 
     */
    public QuickActionList createQuickActionList() {
        return new QuickActionList();
    }

    /**
     * Create an instance of {@link QuickActionListItem }
     * 
     */
    public QuickActionListItem createQuickActionListItem() {
        return new QuickActionListItem();
    }

    /**
     * Create an instance of {@link FlexiPageTemplateInstance }
     * 
     */
    public FlexiPageTemplateInstance createFlexiPageTemplateInstance() {
        return new FlexiPageTemplateInstance();
    }

    /**
     * Create an instance of {@link Flow }
     * 
     */
    public Flow createFlow() {
        return new Flow();
    }

    /**
     * Create an instance of {@link FlowActionCall }
     * 
     */
    public FlowActionCall createFlowActionCall() {
        return new FlowActionCall();
    }

    /**
     * Create an instance of {@link FlowNode }
     * 
     */
    public FlowNode createFlowNode() {
        return new FlowNode();
    }

    /**
     * Create an instance of {@link FlowElement }
     * 
     */
    public FlowElement createFlowElement() {
        return new FlowElement();
    }

    /**
     * Create an instance of {@link FlowBaseElement }
     * 
     */
    public FlowBaseElement createFlowBaseElement() {
        return new FlowBaseElement();
    }

    /**
     * Create an instance of {@link FlowMetadataValue }
     * 
     */
    public FlowMetadataValue createFlowMetadataValue() {
        return new FlowMetadataValue();
    }

    /**
     * Create an instance of {@link FlowElementReferenceOrValue }
     * 
     */
    public FlowElementReferenceOrValue createFlowElementReferenceOrValue() {
        return new FlowElementReferenceOrValue();
    }

    /**
     * Create an instance of {@link FlowActionCallInputParameter }
     * 
     */
    public FlowActionCallInputParameter createFlowActionCallInputParameter() {
        return new FlowActionCallInputParameter();
    }

    /**
     * Create an instance of {@link FlowActionCallOutputParameter }
     * 
     */
    public FlowActionCallOutputParameter createFlowActionCallOutputParameter() {
        return new FlowActionCallOutputParameter();
    }

    /**
     * Create an instance of {@link FlowApexPluginCallInputParameter }
     * 
     */
    public FlowApexPluginCallInputParameter createFlowApexPluginCallInputParameter() {
        return new FlowApexPluginCallInputParameter();
    }

    /**
     * Create an instance of {@link FlowApexPluginCallOutputParameter }
     * 
     */
    public FlowApexPluginCallOutputParameter createFlowApexPluginCallOutputParameter() {
        return new FlowApexPluginCallOutputParameter();
    }

    /**
     * Create an instance of {@link FlowAssignmentItem }
     * 
     */
    public FlowAssignmentItem createFlowAssignmentItem() {
        return new FlowAssignmentItem();
    }

    /**
     * Create an instance of {@link FlowChoiceUserInput }
     * 
     */
    public FlowChoiceUserInput createFlowChoiceUserInput() {
        return new FlowChoiceUserInput();
    }

    /**
     * Create an instance of {@link FlowInputValidationRule }
     * 
     */
    public FlowInputValidationRule createFlowInputValidationRule() {
        return new FlowInputValidationRule();
    }

    /**
     * Create an instance of {@link FlowCondition }
     * 
     */
    public FlowCondition createFlowCondition() {
        return new FlowCondition();
    }

    /**
     * Create an instance of {@link FlowConnector }
     * 
     */
    public FlowConnector createFlowConnector() {
        return new FlowConnector();
    }

    /**
     * Create an instance of {@link FlowInputFieldAssignment }
     * 
     */
    public FlowInputFieldAssignment createFlowInputFieldAssignment() {
        return new FlowInputFieldAssignment();
    }

    /**
     * Create an instance of {@link FlowOutputFieldAssignment }
     * 
     */
    public FlowOutputFieldAssignment createFlowOutputFieldAssignment() {
        return new FlowOutputFieldAssignment();
    }

    /**
     * Create an instance of {@link FlowRecordFilter }
     * 
     */
    public FlowRecordFilter createFlowRecordFilter() {
        return new FlowRecordFilter();
    }

    /**
     * Create an instance of {@link FlowScreenRule }
     * 
     */
    public FlowScreenRule createFlowScreenRule() {
        return new FlowScreenRule();
    }

    /**
     * Create an instance of {@link FlowScreenRuleAction }
     * 
     */
    public FlowScreenRuleAction createFlowScreenRuleAction() {
        return new FlowScreenRuleAction();
    }

    /**
     * Create an instance of {@link FlowSubflowInputAssignment }
     * 
     */
    public FlowSubflowInputAssignment createFlowSubflowInputAssignment() {
        return new FlowSubflowInputAssignment();
    }

    /**
     * Create an instance of {@link FlowSubflowOutputAssignment }
     * 
     */
    public FlowSubflowOutputAssignment createFlowSubflowOutputAssignment() {
        return new FlowSubflowOutputAssignment();
    }

    /**
     * Create an instance of {@link FlowWaitEventInputParameter }
     * 
     */
    public FlowWaitEventInputParameter createFlowWaitEventInputParameter() {
        return new FlowWaitEventInputParameter();
    }

    /**
     * Create an instance of {@link FlowWaitEventOutputParameter }
     * 
     */
    public FlowWaitEventOutputParameter createFlowWaitEventOutputParameter() {
        return new FlowWaitEventOutputParameter();
    }

    /**
     * Create an instance of {@link FlowChoice }
     * 
     */
    public FlowChoice createFlowChoice() {
        return new FlowChoice();
    }

    /**
     * Create an instance of {@link FlowConstant }
     * 
     */
    public FlowConstant createFlowConstant() {
        return new FlowConstant();
    }

    /**
     * Create an instance of {@link FlowDynamicChoiceSet }
     * 
     */
    public FlowDynamicChoiceSet createFlowDynamicChoiceSet() {
        return new FlowDynamicChoiceSet();
    }

    /**
     * Create an instance of {@link FlowFormula }
     * 
     */
    public FlowFormula createFlowFormula() {
        return new FlowFormula();
    }

    /**
     * Create an instance of {@link FlowRule }
     * 
     */
    public FlowRule createFlowRule() {
        return new FlowRule();
    }

    /**
     * Create an instance of {@link FlowScreenField }
     * 
     */
    public FlowScreenField createFlowScreenField() {
        return new FlowScreenField();
    }

    /**
     * Create an instance of {@link FlowTextTemplate }
     * 
     */
    public FlowTextTemplate createFlowTextTemplate() {
        return new FlowTextTemplate();
    }

    /**
     * Create an instance of {@link FlowVariable }
     * 
     */
    public FlowVariable createFlowVariable() {
        return new FlowVariable();
    }

    /**
     * Create an instance of {@link FlowWaitEvent }
     * 
     */
    public FlowWaitEvent createFlowWaitEvent() {
        return new FlowWaitEvent();
    }

    /**
     * Create an instance of {@link FlowApexPluginCall }
     * 
     */
    public FlowApexPluginCall createFlowApexPluginCall() {
        return new FlowApexPluginCall();
    }

    /**
     * Create an instance of {@link FlowAssignment }
     * 
     */
    public FlowAssignment createFlowAssignment() {
        return new FlowAssignment();
    }

    /**
     * Create an instance of {@link FlowDecision }
     * 
     */
    public FlowDecision createFlowDecision() {
        return new FlowDecision();
    }

    /**
     * Create an instance of {@link FlowLoop }
     * 
     */
    public FlowLoop createFlowLoop() {
        return new FlowLoop();
    }

    /**
     * Create an instance of {@link FlowRecordCreate }
     * 
     */
    public FlowRecordCreate createFlowRecordCreate() {
        return new FlowRecordCreate();
    }

    /**
     * Create an instance of {@link FlowRecordDelete }
     * 
     */
    public FlowRecordDelete createFlowRecordDelete() {
        return new FlowRecordDelete();
    }

    /**
     * Create an instance of {@link FlowRecordLookup }
     * 
     */
    public FlowRecordLookup createFlowRecordLookup() {
        return new FlowRecordLookup();
    }

    /**
     * Create an instance of {@link FlowRecordUpdate }
     * 
     */
    public FlowRecordUpdate createFlowRecordUpdate() {
        return new FlowRecordUpdate();
    }

    /**
     * Create an instance of {@link FlowScreen }
     * 
     */
    public FlowScreen createFlowScreen() {
        return new FlowScreen();
    }

    /**
     * Create an instance of {@link FlowStep }
     * 
     */
    public FlowStep createFlowStep() {
        return new FlowStep();
    }

    /**
     * Create an instance of {@link FlowSubflow }
     * 
     */
    public FlowSubflow createFlowSubflow() {
        return new FlowSubflow();
    }

    /**
     * Create an instance of {@link FlowWait }
     * 
     */
    public FlowWait createFlowWait() {
        return new FlowWait();
    }

    /**
     * Create an instance of {@link FlowDefinition }
     * 
     */
    public FlowDefinition createFlowDefinition() {
        return new FlowDefinition();
    }

    /**
     * Create an instance of {@link Folder }
     * 
     */
    public Folder createFolder() {
        return new Folder();
    }

    /**
     * Create an instance of {@link SharedTo }
     * 
     */
    public SharedTo createSharedTo() {
        return new SharedTo();
    }

    /**
     * Create an instance of {@link DocumentFolder }
     * 
     */
    public DocumentFolder createDocumentFolder() {
        return new DocumentFolder();
    }

    /**
     * Create an instance of {@link EmailFolder }
     * 
     */
    public EmailFolder createEmailFolder() {
        return new EmailFolder();
    }

    /**
     * Create an instance of {@link GlobalValueSet }
     * 
     */
    public GlobalValueSet createGlobalValueSet() {
        return new GlobalValueSet();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link HomePageLayout }
     * 
     */
    public HomePageLayout createHomePageLayout() {
        return new HomePageLayout();
    }

    /**
     * Create an instance of {@link KeywordList }
     * 
     */
    public KeywordList createKeywordList() {
        return new KeywordList();
    }

    /**
     * Create an instance of {@link Keyword }
     * 
     */
    public Keyword createKeyword() {
        return new Keyword();
    }

    /**
     * Create an instance of {@link Layout }
     * 
     */
    public Layout createLayout() {
        return new Layout();
    }

    /**
     * Create an instance of {@link CustomConsoleComponents }
     * 
     */
    public CustomConsoleComponents createCustomConsoleComponents() {
        return new CustomConsoleComponents();
    }

    /**
     * Create an instance of {@link PrimaryTabComponents }
     * 
     */
    public PrimaryTabComponents createPrimaryTabComponents() {
        return new PrimaryTabComponents();
    }

    /**
     * Create an instance of {@link Container }
     * 
     */
    public Container createContainer() {
        return new Container();
    }

    /**
     * Create an instance of {@link SidebarComponent }
     * 
     */
    public SidebarComponent createSidebarComponent() {
        return new SidebarComponent();
    }

    /**
     * Create an instance of {@link RelatedList }
     * 
     */
    public RelatedList createRelatedList() {
        return new RelatedList();
    }

    /**
     * Create an instance of {@link SubtabComponents }
     * 
     */
    public SubtabComponents createSubtabComponents() {
        return new SubtabComponents();
    }

    /**
     * Create an instance of {@link FeedLayout }
     * 
     */
    public FeedLayout createFeedLayout() {
        return new FeedLayout();
    }

    /**
     * Create an instance of {@link FeedLayoutFilter }
     * 
     */
    public FeedLayoutFilter createFeedLayoutFilter() {
        return new FeedLayoutFilter();
    }

    /**
     * Create an instance of {@link FeedLayoutComponent }
     * 
     */
    public FeedLayoutComponent createFeedLayoutComponent() {
        return new FeedLayoutComponent();
    }

    /**
     * Create an instance of {@link LayoutSection }
     * 
     */
    public LayoutSection createLayoutSection() {
        return new LayoutSection();
    }

    /**
     * Create an instance of {@link LayoutColumn }
     * 
     */
    public LayoutColumn createLayoutColumn() {
        return new LayoutColumn();
    }

    /**
     * Create an instance of {@link LayoutItem }
     * 
     */
    public LayoutItem createLayoutItem() {
        return new LayoutItem();
    }

    /**
     * Create an instance of {@link AnalyticsCloudComponentLayoutItem }
     * 
     */
    public AnalyticsCloudComponentLayoutItem createAnalyticsCloudComponentLayoutItem() {
        return new AnalyticsCloudComponentLayoutItem();
    }

    /**
     * Create an instance of {@link ReportChartComponentLayoutItem }
     * 
     */
    public ReportChartComponentLayoutItem createReportChartComponentLayoutItem() {
        return new ReportChartComponentLayoutItem();
    }

    /**
     * Create an instance of {@link MiniLayout }
     * 
     */
    public MiniLayout createMiniLayout() {
        return new MiniLayout();
    }

    /**
     * Create an instance of {@link RelatedListItem }
     * 
     */
    public RelatedListItem createRelatedListItem() {
        return new RelatedListItem();
    }

    /**
     * Create an instance of {@link RelatedContent }
     * 
     */
    public RelatedContent createRelatedContent() {
        return new RelatedContent();
    }

    /**
     * Create an instance of {@link RelatedContentItem }
     * 
     */
    public RelatedContentItem createRelatedContentItem() {
        return new RelatedContentItem();
    }

    /**
     * Create an instance of {@link SummaryLayout }
     * 
     */
    public SummaryLayout createSummaryLayout() {
        return new SummaryLayout();
    }

    /**
     * Create an instance of {@link SummaryLayoutItem }
     * 
     */
    public SummaryLayoutItem createSummaryLayoutItem() {
        return new SummaryLayoutItem();
    }

    /**
     * Create an instance of {@link LeadConvertSettings }
     * 
     */
    public LeadConvertSettings createLeadConvertSettings() {
        return new LeadConvertSettings();
    }

    /**
     * Create an instance of {@link ListView }
     * 
     */
    public ListView createListView() {
        return new ListView();
    }

    /**
     * Create an instance of {@link ListViewFilter }
     * 
     */
    public ListViewFilter createListViewFilter() {
        return new ListViewFilter();
    }

    /**
     * Create an instance of {@link ModerationRule }
     * 
     */
    public ModerationRule createModerationRule() {
        return new ModerationRule();
    }

    /**
     * Create an instance of {@link ModeratedEntityField }
     * 
     */
    public ModeratedEntityField createModeratedEntityField() {
        return new ModeratedEntityField();
    }

    /**
     * Create an instance of {@link PathAssistant }
     * 
     */
    public PathAssistant createPathAssistant() {
        return new PathAssistant();
    }

    /**
     * Create an instance of {@link PermissionSet }
     * 
     */
    public PermissionSet createPermissionSet() {
        return new PermissionSet();
    }

    /**
     * Create an instance of {@link PermissionSetApplicationVisibility }
     * 
     */
    public PermissionSetApplicationVisibility createPermissionSetApplicationVisibility() {
        return new PermissionSetApplicationVisibility();
    }

    /**
     * Create an instance of {@link PermissionSetApexClassAccess }
     * 
     */
    public PermissionSetApexClassAccess createPermissionSetApexClassAccess() {
        return new PermissionSetApexClassAccess();
    }

    /**
     * Create an instance of {@link PermissionSetCustomPermissions }
     * 
     */
    public PermissionSetCustomPermissions createPermissionSetCustomPermissions() {
        return new PermissionSetCustomPermissions();
    }

    /**
     * Create an instance of {@link PermissionSetExternalDataSourceAccess }
     * 
     */
    public PermissionSetExternalDataSourceAccess createPermissionSetExternalDataSourceAccess() {
        return new PermissionSetExternalDataSourceAccess();
    }

    /**
     * Create an instance of {@link PermissionSetFieldPermissions }
     * 
     */
    public PermissionSetFieldPermissions createPermissionSetFieldPermissions() {
        return new PermissionSetFieldPermissions();
    }

    /**
     * Create an instance of {@link PermissionSetObjectPermissions }
     * 
     */
    public PermissionSetObjectPermissions createPermissionSetObjectPermissions() {
        return new PermissionSetObjectPermissions();
    }

    /**
     * Create an instance of {@link PermissionSetApexPageAccess }
     * 
     */
    public PermissionSetApexPageAccess createPermissionSetApexPageAccess() {
        return new PermissionSetApexPageAccess();
    }

    /**
     * Create an instance of {@link PermissionSetRecordTypeVisibility }
     * 
     */
    public PermissionSetRecordTypeVisibility createPermissionSetRecordTypeVisibility() {
        return new PermissionSetRecordTypeVisibility();
    }

    /**
     * Create an instance of {@link PermissionSetTabSetting }
     * 
     */
    public PermissionSetTabSetting createPermissionSetTabSetting() {
        return new PermissionSetTabSetting();
    }

    /**
     * Create an instance of {@link PermissionSetUserPermission }
     * 
     */
    public PermissionSetUserPermission createPermissionSetUserPermission() {
        return new PermissionSetUserPermission();
    }

    /**
     * Create an instance of {@link PlatformActionList }
     * 
     */
    public PlatformActionList createPlatformActionList() {
        return new PlatformActionList();
    }

    /**
     * Create an instance of {@link PlatformActionListItem }
     * 
     */
    public PlatformActionListItem createPlatformActionListItem() {
        return new PlatformActionListItem();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfileApplicationVisibility }
     * 
     */
    public ProfileApplicationVisibility createProfileApplicationVisibility() {
        return new ProfileApplicationVisibility();
    }

    /**
     * Create an instance of {@link ProfileApexClassAccess }
     * 
     */
    public ProfileApexClassAccess createProfileApexClassAccess() {
        return new ProfileApexClassAccess();
    }

    /**
     * Create an instance of {@link ProfileCustomPermissions }
     * 
     */
    public ProfileCustomPermissions createProfileCustomPermissions() {
        return new ProfileCustomPermissions();
    }

    /**
     * Create an instance of {@link ProfileExternalDataSourceAccess }
     * 
     */
    public ProfileExternalDataSourceAccess createProfileExternalDataSourceAccess() {
        return new ProfileExternalDataSourceAccess();
    }

    /**
     * Create an instance of {@link ProfileFieldLevelSecurity }
     * 
     */
    public ProfileFieldLevelSecurity createProfileFieldLevelSecurity() {
        return new ProfileFieldLevelSecurity();
    }

    /**
     * Create an instance of {@link ProfileLoginHours }
     * 
     */
    public ProfileLoginHours createProfileLoginHours() {
        return new ProfileLoginHours();
    }

    /**
     * Create an instance of {@link ProfileLoginIpRange }
     * 
     */
    public ProfileLoginIpRange createProfileLoginIpRange() {
        return new ProfileLoginIpRange();
    }

    /**
     * Create an instance of {@link ProfileObjectPermissions }
     * 
     */
    public ProfileObjectPermissions createProfileObjectPermissions() {
        return new ProfileObjectPermissions();
    }

    /**
     * Create an instance of {@link ProfileApexPageAccess }
     * 
     */
    public ProfileApexPageAccess createProfileApexPageAccess() {
        return new ProfileApexPageAccess();
    }

    /**
     * Create an instance of {@link ProfileRecordTypeVisibility }
     * 
     */
    public ProfileRecordTypeVisibility createProfileRecordTypeVisibility() {
        return new ProfileRecordTypeVisibility();
    }

    /**
     * Create an instance of {@link ProfileTabVisibility }
     * 
     */
    public ProfileTabVisibility createProfileTabVisibility() {
        return new ProfileTabVisibility();
    }

    /**
     * Create an instance of {@link ProfileUserPermission }
     * 
     */
    public ProfileUserPermission createProfileUserPermission() {
        return new ProfileUserPermission();
    }

    /**
     * Create an instance of {@link Queue }
     * 
     */
    public Queue createQueue() {
        return new Queue();
    }

    /**
     * Create an instance of {@link QueueSobject }
     * 
     */
    public QueueSobject createQueueSobject() {
        return new QueueSobject();
    }

    /**
     * Create an instance of {@link QuickAction }
     * 
     */
    public QuickAction createQuickAction() {
        return new QuickAction();
    }

    /**
     * Create an instance of {@link QuickActionLayout }
     * 
     */
    public QuickActionLayout createQuickActionLayout() {
        return new QuickActionLayout();
    }

    /**
     * Create an instance of {@link QuickActionLayoutColumn }
     * 
     */
    public QuickActionLayoutColumn createQuickActionLayoutColumn() {
        return new QuickActionLayoutColumn();
    }

    /**
     * Create an instance of {@link QuickActionLayoutItem }
     * 
     */
    public QuickActionLayoutItem createQuickActionLayoutItem() {
        return new QuickActionLayoutItem();
    }

    /**
     * Create an instance of {@link RecordType }
     * 
     */
    public RecordType createRecordType() {
        return new RecordType();
    }

    /**
     * Create an instance of {@link RecordTypePicklistValue }
     * 
     */
    public RecordTypePicklistValue createRecordTypePicklistValue() {
        return new RecordTypePicklistValue();
    }

    /**
     * Create an instance of {@link RemoteSiteSetting }
     * 
     */
    public RemoteSiteSetting createRemoteSiteSetting() {
        return new RemoteSiteSetting();
    }

    /**
     * Create an instance of {@link RoleOrTerritory }
     * 
     */
    public RoleOrTerritory createRoleOrTerritory() {
        return new RoleOrTerritory();
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link StandardValueSet }
     * 
     */
    public StandardValueSet createStandardValueSet() {
        return new StandardValueSet();
    }

    /**
     * Create an instance of {@link StaticResource }
     * 
     */
    public StaticResource createStaticResource() {
        return new StaticResource();
    }

    /**
     * Create an instance of {@link TransactionSecurityPolicy }
     * 
     */
    public TransactionSecurityPolicy createTransactionSecurityPolicy() {
        return new TransactionSecurityPolicy();
    }

    /**
     * Create an instance of {@link TransactionSecurityAction }
     * 
     */
    public TransactionSecurityAction createTransactionSecurityAction() {
        return new TransactionSecurityAction();
    }

    /**
     * Create an instance of {@link TransactionSecurityNotification }
     * 
     */
    public TransactionSecurityNotification createTransactionSecurityNotification() {
        return new TransactionSecurityNotification();
    }

    /**
     * Create an instance of {@link UserCriteria }
     * 
     */
    public UserCriteria createUserCriteria() {
        return new UserCriteria();
    }

    /**
     * Create an instance of {@link ValidationRule }
     * 
     */
    public ValidationRule createValidationRule() {
        return new ValidationRule();
    }

    /**
     * Create an instance of {@link WebLink }
     * 
     */
    public WebLink createWebLink() {
        return new WebLink();
    }

    /**
     * Create an instance of {@link Workflow }
     * 
     */
    public Workflow createWorkflow() {
        return new Workflow();
    }

    /**
     * Create an instance of {@link WorkflowAction }
     * 
     */
    public WorkflowAction createWorkflowAction() {
        return new WorkflowAction();
    }

    /**
     * Create an instance of {@link WorkflowAlert }
     * 
     */
    public WorkflowAlert createWorkflowAlert() {
        return new WorkflowAlert();
    }

    /**
     * Create an instance of {@link WorkflowEmailRecipient }
     * 
     */
    public WorkflowEmailRecipient createWorkflowEmailRecipient() {
        return new WorkflowEmailRecipient();
    }

    /**
     * Create an instance of {@link WorkflowFieldUpdate }
     * 
     */
    public WorkflowFieldUpdate createWorkflowFieldUpdate() {
        return new WorkflowFieldUpdate();
    }

    /**
     * Create an instance of {@link WorkflowFlowAction }
     * 
     */
    public WorkflowFlowAction createWorkflowFlowAction() {
        return new WorkflowFlowAction();
    }

    /**
     * Create an instance of {@link WorkflowFlowActionParameter }
     * 
     */
    public WorkflowFlowActionParameter createWorkflowFlowActionParameter() {
        return new WorkflowFlowActionParameter();
    }

    /**
     * Create an instance of {@link WorkflowKnowledgePublish }
     * 
     */
    public WorkflowKnowledgePublish createWorkflowKnowledgePublish() {
        return new WorkflowKnowledgePublish();
    }

    /**
     * Create an instance of {@link WorkflowOutboundMessage }
     * 
     */
    public WorkflowOutboundMessage createWorkflowOutboundMessage() {
        return new WorkflowOutboundMessage();
    }

    /**
     * Create an instance of {@link WorkflowSend }
     * 
     */
    public WorkflowSend createWorkflowSend() {
        return new WorkflowSend();
    }

    /**
     * Create an instance of {@link WorkflowTask }
     * 
     */
    public WorkflowTask createWorkflowTask() {
        return new WorkflowTask();
    }

    /**
     * Create an instance of {@link WorkflowRule }
     * 
     */
    public WorkflowRule createWorkflowRule() {
        return new WorkflowRule();
    }

    /**
     * Create an instance of {@link WorkflowActionReference }
     * 
     */
    public WorkflowActionReference createWorkflowActionReference() {
        return new WorkflowActionReference();
    }

    /**
     * Create an instance of {@link WorkflowTimeTrigger }
     * 
     */
    public WorkflowTimeTrigger createWorkflowTimeTrigger() {
        return new WorkflowTimeTrigger();
    }

    /**
     * Create an instance of {@link ActivitiesSettings }
     * 
     */
    public ActivitiesSettings createActivitiesSettings() {
        return new ActivitiesSettings();
    }

    /**
     * Create an instance of {@link AddressSettings }
     * 
     */
    public AddressSettings createAddressSettings() {
        return new AddressSettings();
    }

    /**
     * Create an instance of {@link CountriesAndStates }
     * 
     */
    public CountriesAndStates createCountriesAndStates() {
        return new CountriesAndStates();
    }

    /**
     * Create an instance of {@link Country }
     * 
     */
    public Country createCountry() {
        return new Country();
    }

    /**
     * Create an instance of {@link State }
     * 
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link BusinessHoursSettings }
     * 
     */
    public BusinessHoursSettings createBusinessHoursSettings() {
        return new BusinessHoursSettings();
    }

    /**
     * Create an instance of {@link Holiday }
     * 
     */
    public Holiday createHoliday() {
        return new Holiday();
    }

    /**
     * Create an instance of {@link CaseSettings }
     * 
     */
    public CaseSettings createCaseSettings() {
        return new CaseSettings();
    }

    /**
     * Create an instance of {@link FeedItemSettings }
     * 
     */
    public FeedItemSettings createFeedItemSettings() {
        return new FeedItemSettings();
    }

    /**
     * Create an instance of {@link EmailToCaseSettings }
     * 
     */
    public EmailToCaseSettings createEmailToCaseSettings() {
        return new EmailToCaseSettings();
    }

    /**
     * Create an instance of {@link EmailToCaseRoutingAddress }
     * 
     */
    public EmailToCaseRoutingAddress createEmailToCaseRoutingAddress() {
        return new EmailToCaseRoutingAddress();
    }

    /**
     * Create an instance of {@link WebToCaseSettings }
     * 
     */
    public WebToCaseSettings createWebToCaseSettings() {
        return new WebToCaseSettings();
    }

    /**
     * Create an instance of {@link ChatterAnswersSettings }
     * 
     */
    public ChatterAnswersSettings createChatterAnswersSettings() {
        return new ChatterAnswersSettings();
    }

    /**
     * Create an instance of {@link CompanySettings }
     * 
     */
    public CompanySettings createCompanySettings() {
        return new CompanySettings();
    }

    /**
     * Create an instance of {@link ContractSettings }
     * 
     */
    public ContractSettings createContractSettings() {
        return new ContractSettings();
    }

    /**
     * Create an instance of {@link EntitlementSettings }
     * 
     */
    public EntitlementSettings createEntitlementSettings() {
        return new EntitlementSettings();
    }

    /**
     * Create an instance of {@link FileUploadAndDownloadSecuritySettings }
     * 
     */
    public FileUploadAndDownloadSecuritySettings createFileUploadAndDownloadSecuritySettings() {
        return new FileUploadAndDownloadSecuritySettings();
    }

    /**
     * Create an instance of {@link FileTypeDispositionAssignmentBean }
     * 
     */
    public FileTypeDispositionAssignmentBean createFileTypeDispositionAssignmentBean() {
        return new FileTypeDispositionAssignmentBean();
    }

    /**
     * Create an instance of {@link ForecastingSettings }
     * 
     */
    public ForecastingSettings createForecastingSettings() {
        return new ForecastingSettings();
    }

    /**
     * Create an instance of {@link ForecastingCategoryMapping }
     * 
     */
    public ForecastingCategoryMapping createForecastingCategoryMapping() {
        return new ForecastingCategoryMapping();
    }

    /**
     * Create an instance of {@link WeightedSourceCategory }
     * 
     */
    public WeightedSourceCategory createWeightedSourceCategory() {
        return new WeightedSourceCategory();
    }

    /**
     * Create an instance of {@link ForecastingTypeSettings }
     * 
     */
    public ForecastingTypeSettings createForecastingTypeSettings() {
        return new ForecastingTypeSettings();
    }

    /**
     * Create an instance of {@link AdjustmentsSettings }
     * 
     */
    public AdjustmentsSettings createAdjustmentsSettings() {
        return new AdjustmentsSettings();
    }

    /**
     * Create an instance of {@link ForecastRangeSettings }
     * 
     */
    public ForecastRangeSettings createForecastRangeSettings() {
        return new ForecastRangeSettings();
    }

    /**
     * Create an instance of {@link OpportunityListFieldsLabelMapping }
     * 
     */
    public OpportunityListFieldsLabelMapping createOpportunityListFieldsLabelMapping() {
        return new OpportunityListFieldsLabelMapping();
    }

    /**
     * Create an instance of {@link OpportunityListFieldsSelectedSettings }
     * 
     */
    public OpportunityListFieldsSelectedSettings createOpportunityListFieldsSelectedSettings() {
        return new OpportunityListFieldsSelectedSettings();
    }

    /**
     * Create an instance of {@link OpportunityListFieldsUnselectedSettings }
     * 
     */
    public OpportunityListFieldsUnselectedSettings createOpportunityListFieldsUnselectedSettings() {
        return new OpportunityListFieldsUnselectedSettings();
    }

    /**
     * Create an instance of {@link QuotasSettings }
     * 
     */
    public QuotasSettings createQuotasSettings() {
        return new QuotasSettings();
    }

    /**
     * Create an instance of {@link IdeasSettings }
     * 
     */
    public IdeasSettings createIdeasSettings() {
        return new IdeasSettings();
    }

    /**
     * Create an instance of {@link KnowledgeSettings }
     * 
     */
    public KnowledgeSettings createKnowledgeSettings() {
        return new KnowledgeSettings();
    }

    /**
     * Create an instance of {@link KnowledgeAnswerSettings }
     * 
     */
    public KnowledgeAnswerSettings createKnowledgeAnswerSettings() {
        return new KnowledgeAnswerSettings();
    }

    /**
     * Create an instance of {@link KnowledgeCaseSettings }
     * 
     */
    public KnowledgeCaseSettings createKnowledgeCaseSettings() {
        return new KnowledgeCaseSettings();
    }

    /**
     * Create an instance of {@link KnowledgeCommunitiesSettings }
     * 
     */
    public KnowledgeCommunitiesSettings createKnowledgeCommunitiesSettings() {
        return new KnowledgeCommunitiesSettings();
    }

    /**
     * Create an instance of {@link KnowledgeSitesSettings }
     * 
     */
    public KnowledgeSitesSettings createKnowledgeSitesSettings() {
        return new KnowledgeSitesSettings();
    }

    /**
     * Create an instance of {@link KnowledgeLanguageSettings }
     * 
     */
    public KnowledgeLanguageSettings createKnowledgeLanguageSettings() {
        return new KnowledgeLanguageSettings();
    }

    /**
     * Create an instance of {@link KnowledgeLanguage }
     * 
     */
    public KnowledgeLanguage createKnowledgeLanguage() {
        return new KnowledgeLanguage();
    }

    /**
     * Create an instance of {@link KnowledgeSuggestedArticlesSettings }
     * 
     */
    public KnowledgeSuggestedArticlesSettings createKnowledgeSuggestedArticlesSettings() {
        return new KnowledgeSuggestedArticlesSettings();
    }

    /**
     * Create an instance of {@link KnowledgeCaseFieldsSettings }
     * 
     */
    public KnowledgeCaseFieldsSettings createKnowledgeCaseFieldsSettings() {
        return new KnowledgeCaseFieldsSettings();
    }

    /**
     * Create an instance of {@link KnowledgeCaseField }
     * 
     */
    public KnowledgeCaseField createKnowledgeCaseField() {
        return new KnowledgeCaseField();
    }

    /**
     * Create an instance of {@link KnowledgeWorkOrderFieldsSettings }
     * 
     */
    public KnowledgeWorkOrderFieldsSettings createKnowledgeWorkOrderFieldsSettings() {
        return new KnowledgeWorkOrderFieldsSettings();
    }

    /**
     * Create an instance of {@link KnowledgeWorkOrderField }
     * 
     */
    public KnowledgeWorkOrderField createKnowledgeWorkOrderField() {
        return new KnowledgeWorkOrderField();
    }

    /**
     * Create an instance of {@link KnowledgeWorkOrderLineItemFieldsSettings }
     * 
     */
    public KnowledgeWorkOrderLineItemFieldsSettings createKnowledgeWorkOrderLineItemFieldsSettings() {
        return new KnowledgeWorkOrderLineItemFieldsSettings();
    }

    /**
     * Create an instance of {@link KnowledgeWorkOrderLineItemField }
     * 
     */
    public KnowledgeWorkOrderLineItemField createKnowledgeWorkOrderLineItemField() {
        return new KnowledgeWorkOrderLineItemField();
    }

    /**
     * Create an instance of {@link LiveAgentSettings }
     * 
     */
    public LiveAgentSettings createLiveAgentSettings() {
        return new LiveAgentSettings();
    }

    /**
     * Create an instance of {@link MacroSettings }
     * 
     */
    public MacroSettings createMacroSettings() {
        return new MacroSettings();
    }

    /**
     * Create an instance of {@link MarketingActionSettings }
     * 
     */
    public MarketingActionSettings createMarketingActionSettings() {
        return new MarketingActionSettings();
    }

    /**
     * Create an instance of {@link MobileSettings }
     * 
     */
    public MobileSettings createMobileSettings() {
        return new MobileSettings();
    }

    /**
     * Create an instance of {@link ChatterMobileSettings }
     * 
     */
    public ChatterMobileSettings createChatterMobileSettings() {
        return new ChatterMobileSettings();
    }

    /**
     * Create an instance of {@link DashboardMobileSettings }
     * 
     */
    public DashboardMobileSettings createDashboardMobileSettings() {
        return new DashboardMobileSettings();
    }

    /**
     * Create an instance of {@link SFDCMobileSettings }
     * 
     */
    public SFDCMobileSettings createSFDCMobileSettings() {
        return new SFDCMobileSettings();
    }

    /**
     * Create an instance of {@link TouchMobileSettings }
     * 
     */
    public TouchMobileSettings createTouchMobileSettings() {
        return new TouchMobileSettings();
    }

    /**
     * Create an instance of {@link NameSettings }
     * 
     */
    public NameSettings createNameSettings() {
        return new NameSettings();
    }

    /**
     * Create an instance of {@link OpportunitySettings }
     * 
     */
    public OpportunitySettings createOpportunitySettings() {
        return new OpportunitySettings();
    }

    /**
     * Create an instance of {@link FindSimilarOppFilter }
     * 
     */
    public FindSimilarOppFilter createFindSimilarOppFilter() {
        return new FindSimilarOppFilter();
    }

    /**
     * Create an instance of {@link OrderSettings }
     * 
     */
    public OrderSettings createOrderSettings() {
        return new OrderSettings();
    }

    /**
     * Create an instance of {@link OrgPreferenceSettings }
     * 
     */
    public OrgPreferenceSettings createOrgPreferenceSettings() {
        return new OrgPreferenceSettings();
    }

    /**
     * Create an instance of {@link OrganizationSettingsDetail }
     * 
     */
    public OrganizationSettingsDetail createOrganizationSettingsDetail() {
        return new OrganizationSettingsDetail();
    }

    /**
     * Create an instance of {@link PathAssistantSettings }
     * 
     */
    public PathAssistantSettings createPathAssistantSettings() {
        return new PathAssistantSettings();
    }

    /**
     * Create an instance of {@link PersonListSettings }
     * 
     */
    public PersonListSettings createPersonListSettings() {
        return new PersonListSettings();
    }

    /**
     * Create an instance of {@link PersonalJourneySettings }
     * 
     */
    public PersonalJourneySettings createPersonalJourneySettings() {
        return new PersonalJourneySettings();
    }

    /**
     * Create an instance of {@link ProductSettings }
     * 
     */
    public ProductSettings createProductSettings() {
        return new ProductSettings();
    }

    /**
     * Create an instance of {@link QuoteSettings }
     * 
     */
    public QuoteSettings createQuoteSettings() {
        return new QuoteSettings();
    }

    /**
     * Create an instance of {@link SearchSettings }
     * 
     */
    public SearchSettings createSearchSettings() {
        return new SearchSettings();
    }

    /**
     * Create an instance of {@link SearchSettingsByObject }
     * 
     */
    public SearchSettingsByObject createSearchSettingsByObject() {
        return new SearchSettingsByObject();
    }

    /**
     * Create an instance of {@link ObjectSearchSetting }
     * 
     */
    public ObjectSearchSetting createObjectSearchSetting() {
        return new ObjectSearchSetting();
    }

    /**
     * Create an instance of {@link SecuritySettings }
     * 
     */
    public SecuritySettings createSecuritySettings() {
        return new SecuritySettings();
    }

    /**
     * Create an instance of {@link NetworkAccess }
     * 
     */
    public NetworkAccess createNetworkAccess() {
        return new NetworkAccess();
    }

    /**
     * Create an instance of {@link IpRange }
     * 
     */
    public IpRange createIpRange() {
        return new IpRange();
    }

    /**
     * Create an instance of {@link PasswordPolicies }
     * 
     */
    public PasswordPolicies createPasswordPolicies() {
        return new PasswordPolicies();
    }

    /**
     * Create an instance of {@link SessionSettings }
     * 
     */
    public SessionSettings createSessionSettings() {
        return new SessionSettings();
    }

    /**
     * Create an instance of {@link Territory2Settings }
     * 
     */
    public Territory2Settings createTerritory2Settings() {
        return new Territory2Settings();
    }

    /**
     * Create an instance of {@link ChannelLayoutItem }
     * 
     */
    public ChannelLayoutItem createChannelLayoutItem() {
        return new ChannelLayoutItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:metadata.tooling.soap.sforce.com", name = "booleanValue", scope = FlowElementReferenceOrValue.class)
    public JAXBElement<Boolean> createFlowElementReferenceOrValueBooleanValue(Boolean value) {
        return new JAXBElement<Boolean>(_FlowElementReferenceOrValueBooleanValue_QNAME, Boolean.class, FlowElementReferenceOrValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:metadata.tooling.soap.sforce.com", name = "numberValue", scope = FlowElementReferenceOrValue.class)
    public JAXBElement<Double> createFlowElementReferenceOrValueNumberValue(Double value) {
        return new JAXBElement<Double>(_FlowElementReferenceOrValueNumberValue_QNAME, Double.class, FlowElementReferenceOrValue.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:metadata.tooling.soap.sforce.com", name = "encryptedWithPlatformEncryption", scope = Certificate.class)
    public JAXBElement<Boolean> createCertificateEncryptedWithPlatformEncryption(Boolean value) {
        return new JAXBElement<Boolean>(_CertificateEncryptedWithPlatformEncryption_QNAME, Boolean.class, Certificate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:metadata.tooling.soap.sforce.com", name = "expirationDate", scope = Certificate.class)
    public JAXBElement<XMLGregorianCalendar> createCertificateExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CertificateExpirationDate_QNAME, XMLGregorianCalendar.class, Certificate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:metadata.tooling.soap.sforce.com", name = "privateKeyExportable", scope = Certificate.class)
    public JAXBElement<Boolean> createCertificatePrivateKeyExportable(Boolean value) {
        return new JAXBElement<Boolean>(_CertificatePrivateKeyExportable_QNAME, Boolean.class, Certificate.class, value);
    }

}
