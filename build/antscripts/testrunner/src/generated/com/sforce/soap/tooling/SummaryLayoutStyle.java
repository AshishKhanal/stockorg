
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SummaryLayoutStyle.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SummaryLayoutStyle"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Default"/&gt;
 *     &lt;enumeration value="QuoteTemplate"/&gt;
 *     &lt;enumeration value="DefaultQuoteTemplate"/&gt;
 *     &lt;enumeration value="ServiceReportTemplate"/&gt;
 *     &lt;enumeration value="ChildServiceReportTemplateStyle"/&gt;
 *     &lt;enumeration value="DefaultServiceReportTemplate"/&gt;
 *     &lt;enumeration value="CaseInteraction"/&gt;
 *     &lt;enumeration value="QuickActionLayoutLeftRight"/&gt;
 *     &lt;enumeration value="QuickActionLayoutTopDown"/&gt;
 *     &lt;enumeration value="PathAssistant"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SummaryLayoutStyle")
@XmlEnum
public enum SummaryLayoutStyle {

    @XmlEnumValue("Default")
    DEFAULT("Default"),
    @XmlEnumValue("QuoteTemplate")
    QUOTE_TEMPLATE("QuoteTemplate"),
    @XmlEnumValue("DefaultQuoteTemplate")
    DEFAULT_QUOTE_TEMPLATE("DefaultQuoteTemplate"),
    @XmlEnumValue("ServiceReportTemplate")
    SERVICE_REPORT_TEMPLATE("ServiceReportTemplate"),
    @XmlEnumValue("ChildServiceReportTemplateStyle")
    CHILD_SERVICE_REPORT_TEMPLATE_STYLE("ChildServiceReportTemplateStyle"),
    @XmlEnumValue("DefaultServiceReportTemplate")
    DEFAULT_SERVICE_REPORT_TEMPLATE("DefaultServiceReportTemplate"),
    @XmlEnumValue("CaseInteraction")
    CASE_INTERACTION("CaseInteraction"),
    @XmlEnumValue("QuickActionLayoutLeftRight")
    QUICK_ACTION_LAYOUT_LEFT_RIGHT("QuickActionLayoutLeftRight"),
    @XmlEnumValue("QuickActionLayoutTopDown")
    QUICK_ACTION_LAYOUT_TOP_DOWN("QuickActionLayoutTopDown"),
    @XmlEnumValue("PathAssistant")
    PATH_ASSISTANT("PathAssistant");
    private final String value;

    SummaryLayoutStyle(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SummaryLayoutStyle fromValue(String v) {
        for (SummaryLayoutStyle c: SummaryLayoutStyle.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
