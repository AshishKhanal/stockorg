
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateLimitTimePeriod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RateLimitTimePeriod"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Short"/&gt;
 *     &lt;enumeration value="Medium"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "RateLimitTimePeriod")
@XmlEnum
public enum RateLimitTimePeriod {

    @XmlEnumValue("Short")
    SHORT("Short"),
    @XmlEnumValue("Medium")
    MEDIUM("Medium");
    private final String value;

    RateLimitTimePeriod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RateLimitTimePeriod fromValue(String v) {
        for (RateLimitTimePeriod c: RateLimitTimePeriod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
