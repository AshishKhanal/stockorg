
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.AuraBundleType;


/**
 * <p>Java class for AuraDefinitionBundle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuraDefinitionBundle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SVGContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="apiVersion" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="controllerContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="designContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="documentationContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="helperContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="markup" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="modelContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="packageVersions" type="{urn:metadata.tooling.soap.sforce.com}PackageVersion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="rendererContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="styleContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="testsuiteContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}AuraBundleType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuraDefinitionBundle", propOrder = {
    "svgContent",
    "apiVersion",
    "controllerContent",
    "description",
    "designContent",
    "documentationContent",
    "helperContent",
    "markup",
    "modelContent",
    "packageVersions",
    "rendererContent",
    "styleContent",
    "testsuiteContent",
    "type"
})
public class AuraDefinitionBundle
    extends Metadata
{

    @XmlElement(name = "SVGContent")
    protected byte[] svgContent;
    protected Double apiVersion;
    protected byte[] controllerContent;
    protected String description;
    protected byte[] designContent;
    protected byte[] documentationContent;
    protected byte[] helperContent;
    protected byte[] markup;
    protected byte[] modelContent;
    protected List<PackageVersion> packageVersions;
    protected byte[] rendererContent;
    protected byte[] styleContent;
    protected byte[] testsuiteContent;
    @XmlSchemaType(name = "string")
    protected AuraBundleType type;

    /**
     * Gets the value of the svgContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSVGContent() {
        return svgContent;
    }

    /**
     * Sets the value of the svgContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSVGContent(byte[] value) {
        this.svgContent = value;
    }

    /**
     * Gets the value of the apiVersion property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getApiVersion() {
        return apiVersion;
    }

    /**
     * Sets the value of the apiVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setApiVersion(Double value) {
        this.apiVersion = value;
    }

    /**
     * Gets the value of the controllerContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getControllerContent() {
        return controllerContent;
    }

    /**
     * Sets the value of the controllerContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setControllerContent(byte[] value) {
        this.controllerContent = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the designContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDesignContent() {
        return designContent;
    }

    /**
     * Sets the value of the designContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDesignContent(byte[] value) {
        this.designContent = value;
    }

    /**
     * Gets the value of the documentationContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDocumentationContent() {
        return documentationContent;
    }

    /**
     * Sets the value of the documentationContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDocumentationContent(byte[] value) {
        this.documentationContent = value;
    }

    /**
     * Gets the value of the helperContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getHelperContent() {
        return helperContent;
    }

    /**
     * Sets the value of the helperContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setHelperContent(byte[] value) {
        this.helperContent = value;
    }

    /**
     * Gets the value of the markup property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMarkup() {
        return markup;
    }

    /**
     * Sets the value of the markup property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMarkup(byte[] value) {
        this.markup = value;
    }

    /**
     * Gets the value of the modelContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getModelContent() {
        return modelContent;
    }

    /**
     * Sets the value of the modelContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setModelContent(byte[] value) {
        this.modelContent = value;
    }

    /**
     * Gets the value of the packageVersions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packageVersions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackageVersions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackageVersion }
     * 
     * 
     */
    public List<PackageVersion> getPackageVersions() {
        if (packageVersions == null) {
            packageVersions = new ArrayList<PackageVersion>();
        }
        return this.packageVersions;
    }

    /**
     * Gets the value of the rendererContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getRendererContent() {
        return rendererContent;
    }

    /**
     * Sets the value of the rendererContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setRendererContent(byte[] value) {
        this.rendererContent = value;
    }

    /**
     * Gets the value of the styleContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getStyleContent() {
        return styleContent;
    }

    /**
     * Sets the value of the styleContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setStyleContent(byte[] value) {
        this.styleContent = value;
    }

    /**
     * Gets the value of the testsuiteContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getTestsuiteContent() {
        return testsuiteContent;
    }

    /**
     * Sets the value of the testsuiteContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setTestsuiteContent(byte[] value) {
        this.testsuiteContent = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link AuraBundleType }
     *     
     */
    public AuraBundleType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuraBundleType }
     *     
     */
    public void setType(AuraBundleType value) {
        this.type = value;
    }

}
