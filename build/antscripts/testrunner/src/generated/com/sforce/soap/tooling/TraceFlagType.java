
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TraceFlagType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TraceFlagType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="USER_DEBUG"/&gt;
 *     &lt;enumeration value="DEVELOPER_LOG"/&gt;
 *     &lt;enumeration value="CLASS_TRACING"/&gt;
 *     &lt;enumeration value="PROFILING"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TraceFlagType")
@XmlEnum
public enum TraceFlagType {

    USER_DEBUG,
    DEVELOPER_LOG,
    CLASS_TRACING,
    PROFILING;

    public String value() {
        return name();
    }

    public static TraceFlagType fromValue(String v) {
        return valueOf(v);
    }

}
