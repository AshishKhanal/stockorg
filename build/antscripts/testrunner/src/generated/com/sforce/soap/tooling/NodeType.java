
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="NodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Setup"/&gt;
 *     &lt;enumeration value="PersonalSettings"/&gt;
 *     &lt;enumeration value="ServiceSetup"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "NodeType")
@XmlEnum
public enum NodeType {

    @XmlEnumValue("Setup")
    SETUP("Setup"),
    @XmlEnumValue("PersonalSettings")
    PERSONAL_SETTINGS("PersonalSettings"),
    @XmlEnumValue("ServiceSetup")
    SERVICE_SETUP("ServiceSetup");
    private final String value;

    NodeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NodeType fromValue(String v) {
        for (NodeType c: NodeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
