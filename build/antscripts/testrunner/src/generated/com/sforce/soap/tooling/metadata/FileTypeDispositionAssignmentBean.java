
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.FileDownloadBehavior;
import com.sforce.soap.tooling.FileType;


/**
 * <p>Java class for FileTypeDispositionAssignmentBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FileTypeDispositionAssignmentBean"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="behavior" type="{urn:tooling.soap.sforce.com}FileDownloadBehavior"/&gt;
 *         &lt;element name="fileType" type="{urn:tooling.soap.sforce.com}FileType"/&gt;
 *         &lt;element name="securityRiskFileType" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileTypeDispositionAssignmentBean", propOrder = {
    "behavior",
    "fileType",
    "securityRiskFileType"
})
public class FileTypeDispositionAssignmentBean {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FileDownloadBehavior behavior;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected FileType fileType;
    protected boolean securityRiskFileType;

    /**
     * Gets the value of the behavior property.
     * 
     * @return
     *     possible object is
     *     {@link FileDownloadBehavior }
     *     
     */
    public FileDownloadBehavior getBehavior() {
        return behavior;
    }

    /**
     * Sets the value of the behavior property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileDownloadBehavior }
     *     
     */
    public void setBehavior(FileDownloadBehavior value) {
        this.behavior = value;
    }

    /**
     * Gets the value of the fileType property.
     * 
     * @return
     *     possible object is
     *     {@link FileType }
     *     
     */
    public FileType getFileType() {
        return fileType;
    }

    /**
     * Sets the value of the fileType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileType }
     *     
     */
    public void setFileType(FileType value) {
        this.fileType = value;
    }

    /**
     * Gets the value of the securityRiskFileType property.
     * 
     */
    public boolean isSecurityRiskFileType() {
        return securityRiskFileType;
    }

    /**
     * Sets the value of the securityRiskFileType property.
     * 
     */
    public void setSecurityRiskFileType(boolean value) {
        this.securityRiskFileType = value;
    }

}
