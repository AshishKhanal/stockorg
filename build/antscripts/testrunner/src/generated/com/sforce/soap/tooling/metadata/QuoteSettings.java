
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableQuote" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteSettings", propOrder = {
    "enableQuote"
})
public class QuoteSettings
    extends MetadataForSettings
{

    protected boolean enableQuote;

    /**
     * Gets the value of the enableQuote property.
     * 
     */
    public boolean isEnableQuote() {
        return enableQuote;
    }

    /**
     * Sets the value of the enableQuote property.
     * 
     */
    public void setEnableQuote(boolean value) {
        this.enableQuote = value;
    }

}
