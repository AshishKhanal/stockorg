
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerBreakpointHitCondition.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerBreakpointHitCondition"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="GreaterOrEquals"/&gt;
 *     &lt;enumeration value="Equals"/&gt;
 *     &lt;enumeration value="Modulo"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerBreakpointHitCondition")
@XmlEnum
public enum ApexDebuggerBreakpointHitCondition {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("GreaterOrEquals")
    GREATER_OR_EQUALS("GreaterOrEquals"),
    @XmlEnumValue("Equals")
    EQUALS("Equals"),
    @XmlEnumValue("Modulo")
    MODULO("Modulo");
    private final String value;

    ApexDebuggerBreakpointHitCondition(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerBreakpointHitCondition fromValue(String v) {
        for (ApexDebuggerBreakpointHitCondition c: ApexDebuggerBreakpointHitCondition.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
