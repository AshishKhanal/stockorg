
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuickActionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="QuickActionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Create"/&gt;
 *     &lt;enumeration value="VisualforcePage"/&gt;
 *     &lt;enumeration value="Post"/&gt;
 *     &lt;enumeration value="SendEmail"/&gt;
 *     &lt;enumeration value="LogACall"/&gt;
 *     &lt;enumeration value="SocialPost"/&gt;
 *     &lt;enumeration value="Canvas"/&gt;
 *     &lt;enumeration value="Update"/&gt;
 *     &lt;enumeration value="LightningComponent"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "QuickActionType")
@XmlEnum
public enum QuickActionType {

    @XmlEnumValue("Create")
    CREATE("Create"),
    @XmlEnumValue("VisualforcePage")
    VISUALFORCE_PAGE("VisualforcePage"),
    @XmlEnumValue("Post")
    POST("Post"),
    @XmlEnumValue("SendEmail")
    SEND_EMAIL("SendEmail"),
    @XmlEnumValue("LogACall")
    LOG_A_CALL("LogACall"),
    @XmlEnumValue("SocialPost")
    SOCIAL_POST("SocialPost"),
    @XmlEnumValue("Canvas")
    CANVAS("Canvas"),
    @XmlEnumValue("Update")
    UPDATE("Update"),
    @XmlEnumValue("LightningComponent")
    LIGHTNING_COMPONENT("LightningComponent");
    private final String value;

    QuickActionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static QuickActionType fromValue(String v) {
        for (QuickActionType c: QuickActionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
