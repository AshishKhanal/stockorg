
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Community complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Community"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="chatterAnswersFacebookSsoUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="communityFeedPage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dataCategoryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailFooterDocument" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailHeaderDocument" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailNotificationUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="enableChatterAnswers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enablePrivateQuestions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expertsGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="portal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reputationLevels" type="{urn:metadata.tooling.soap.sforce.com}ReputationLevels" minOccurs="0"/&gt;
 *         &lt;element name="showInPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="site" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Community", propOrder = {
    "active",
    "chatterAnswersFacebookSsoUrl",
    "communityFeedPage",
    "dataCategoryName",
    "description",
    "emailFooterDocument",
    "emailHeaderDocument",
    "emailNotificationUrl",
    "enableChatterAnswers",
    "enablePrivateQuestions",
    "expertsGroup",
    "portal",
    "reputationLevels",
    "showInPortal",
    "site"
})
public class Community
    extends Metadata
{

    protected Boolean active;
    protected String chatterAnswersFacebookSsoUrl;
    protected String communityFeedPage;
    protected String dataCategoryName;
    protected String description;
    protected String emailFooterDocument;
    protected String emailHeaderDocument;
    protected String emailNotificationUrl;
    protected Boolean enableChatterAnswers;
    protected Boolean enablePrivateQuestions;
    protected String expertsGroup;
    protected String portal;
    protected ReputationLevels reputationLevels;
    protected Boolean showInPortal;
    protected String site;

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActive(Boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the chatterAnswersFacebookSsoUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChatterAnswersFacebookSsoUrl() {
        return chatterAnswersFacebookSsoUrl;
    }

    /**
     * Sets the value of the chatterAnswersFacebookSsoUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChatterAnswersFacebookSsoUrl(String value) {
        this.chatterAnswersFacebookSsoUrl = value;
    }

    /**
     * Gets the value of the communityFeedPage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommunityFeedPage() {
        return communityFeedPage;
    }

    /**
     * Sets the value of the communityFeedPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommunityFeedPage(String value) {
        this.communityFeedPage = value;
    }

    /**
     * Gets the value of the dataCategoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCategoryName() {
        return dataCategoryName;
    }

    /**
     * Sets the value of the dataCategoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCategoryName(String value) {
        this.dataCategoryName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the emailFooterDocument property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailFooterDocument() {
        return emailFooterDocument;
    }

    /**
     * Sets the value of the emailFooterDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailFooterDocument(String value) {
        this.emailFooterDocument = value;
    }

    /**
     * Gets the value of the emailHeaderDocument property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailHeaderDocument() {
        return emailHeaderDocument;
    }

    /**
     * Sets the value of the emailHeaderDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailHeaderDocument(String value) {
        this.emailHeaderDocument = value;
    }

    /**
     * Gets the value of the emailNotificationUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailNotificationUrl() {
        return emailNotificationUrl;
    }

    /**
     * Sets the value of the emailNotificationUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailNotificationUrl(String value) {
        this.emailNotificationUrl = value;
    }

    /**
     * Gets the value of the enableChatterAnswers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableChatterAnswers() {
        return enableChatterAnswers;
    }

    /**
     * Sets the value of the enableChatterAnswers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableChatterAnswers(Boolean value) {
        this.enableChatterAnswers = value;
    }

    /**
     * Gets the value of the enablePrivateQuestions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePrivateQuestions() {
        return enablePrivateQuestions;
    }

    /**
     * Sets the value of the enablePrivateQuestions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePrivateQuestions(Boolean value) {
        this.enablePrivateQuestions = value;
    }

    /**
     * Gets the value of the expertsGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpertsGroup() {
        return expertsGroup;
    }

    /**
     * Sets the value of the expertsGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpertsGroup(String value) {
        this.expertsGroup = value;
    }

    /**
     * Gets the value of the portal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortal() {
        return portal;
    }

    /**
     * Sets the value of the portal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortal(String value) {
        this.portal = value;
    }

    /**
     * Gets the value of the reputationLevels property.
     * 
     * @return
     *     possible object is
     *     {@link ReputationLevels }
     *     
     */
    public ReputationLevels getReputationLevels() {
        return reputationLevels;
    }

    /**
     * Sets the value of the reputationLevels property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReputationLevels }
     *     
     */
    public void setReputationLevels(ReputationLevels value) {
        this.reputationLevels = value;
    }

    /**
     * Gets the value of the showInPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowInPortal() {
        return showInPortal;
    }

    /**
     * Sets the value of the showInPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowInPortal(Boolean value) {
        this.showInPortal = value;
    }

    /**
     * Gets the value of the site property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSite() {
        return site;
    }

    /**
     * Sets the value of the site property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSite(String value) {
        this.site = value;
    }

}
