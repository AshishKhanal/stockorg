
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.EmbeddedServiceScenario;


/**
 * <p>Java class for EmbeddedServiceLiveAgent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmbeddedServiceLiveAgent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="embeddedServiceConfig" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="embeddedServiceQuickActions" type="{urn:metadata.tooling.soap.sforce.com}EmbeddedServiceQuickAction" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="liveAgentChatUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="liveAgentContentUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="liveChatButton" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="liveChatDeployment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prechatEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="prechatJson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scenario" type="{urn:tooling.soap.sforce.com}EmbeddedServiceScenario"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmbeddedServiceLiveAgent", propOrder = {
    "embeddedServiceConfig",
    "embeddedServiceQuickActions",
    "liveAgentChatUrl",
    "liveAgentContentUrl",
    "liveChatButton",
    "liveChatDeployment",
    "masterLabel",
    "prechatEnabled",
    "prechatJson",
    "scenario"
})
public class EmbeddedServiceLiveAgent
    extends Metadata
{

    @XmlElement(required = true)
    protected String embeddedServiceConfig;
    protected List<EmbeddedServiceQuickAction> embeddedServiceQuickActions;
    protected String liveAgentChatUrl;
    protected String liveAgentContentUrl;
    @XmlElement(required = true)
    protected String liveChatButton;
    @XmlElement(required = true)
    protected String liveChatDeployment;
    @XmlElement(required = true)
    protected String masterLabel;
    protected boolean prechatEnabled;
    protected String prechatJson;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected EmbeddedServiceScenario scenario;

    /**
     * Gets the value of the embeddedServiceConfig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmbeddedServiceConfig() {
        return embeddedServiceConfig;
    }

    /**
     * Sets the value of the embeddedServiceConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmbeddedServiceConfig(String value) {
        this.embeddedServiceConfig = value;
    }

    /**
     * Gets the value of the embeddedServiceQuickActions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the embeddedServiceQuickActions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmbeddedServiceQuickActions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmbeddedServiceQuickAction }
     * 
     * 
     */
    public List<EmbeddedServiceQuickAction> getEmbeddedServiceQuickActions() {
        if (embeddedServiceQuickActions == null) {
            embeddedServiceQuickActions = new ArrayList<EmbeddedServiceQuickAction>();
        }
        return this.embeddedServiceQuickActions;
    }

    /**
     * Gets the value of the liveAgentChatUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiveAgentChatUrl() {
        return liveAgentChatUrl;
    }

    /**
     * Sets the value of the liveAgentChatUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiveAgentChatUrl(String value) {
        this.liveAgentChatUrl = value;
    }

    /**
     * Gets the value of the liveAgentContentUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiveAgentContentUrl() {
        return liveAgentContentUrl;
    }

    /**
     * Sets the value of the liveAgentContentUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiveAgentContentUrl(String value) {
        this.liveAgentContentUrl = value;
    }

    /**
     * Gets the value of the liveChatButton property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiveChatButton() {
        return liveChatButton;
    }

    /**
     * Sets the value of the liveChatButton property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiveChatButton(String value) {
        this.liveChatButton = value;
    }

    /**
     * Gets the value of the liveChatDeployment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiveChatDeployment() {
        return liveChatDeployment;
    }

    /**
     * Sets the value of the liveChatDeployment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiveChatDeployment(String value) {
        this.liveChatDeployment = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the prechatEnabled property.
     * 
     */
    public boolean isPrechatEnabled() {
        return prechatEnabled;
    }

    /**
     * Sets the value of the prechatEnabled property.
     * 
     */
    public void setPrechatEnabled(boolean value) {
        this.prechatEnabled = value;
    }

    /**
     * Gets the value of the prechatJson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrechatJson() {
        return prechatJson;
    }

    /**
     * Sets the value of the prechatJson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrechatJson(String value) {
        this.prechatJson = value;
    }

    /**
     * Gets the value of the scenario property.
     * 
     * @return
     *     possible object is
     *     {@link EmbeddedServiceScenario }
     *     
     */
    public EmbeddedServiceScenario getScenario() {
        return scenario;
    }

    /**
     * Sets the value of the scenario property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmbeddedServiceScenario }
     *     
     */
    public void setScenario(EmbeddedServiceScenario value) {
        this.scenario = value;
    }

}
