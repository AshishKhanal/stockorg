
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuraDefType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AuraDefType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="APPLICATION"/&gt;
 *     &lt;enumeration value="CONTROLLER"/&gt;
 *     &lt;enumeration value="COMPONENT"/&gt;
 *     &lt;enumeration value="EVENT"/&gt;
 *     &lt;enumeration value="HELPER"/&gt;
 *     &lt;enumeration value="INTERFACE"/&gt;
 *     &lt;enumeration value="RENDERER"/&gt;
 *     &lt;enumeration value="STYLE"/&gt;
 *     &lt;enumeration value="PROVIDER"/&gt;
 *     &lt;enumeration value="MODEL"/&gt;
 *     &lt;enumeration value="TESTSUITE"/&gt;
 *     &lt;enumeration value="DOCUMENTATION"/&gt;
 *     &lt;enumeration value="TOKENS"/&gt;
 *     &lt;enumeration value="DESIGN"/&gt;
 *     &lt;enumeration value="SVG"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AuraDefType")
@XmlEnum
public enum AuraDefType {

    APPLICATION,
    CONTROLLER,
    COMPONENT,
    EVENT,
    HELPER,
    INTERFACE,
    RENDERER,
    STYLE,
    PROVIDER,
    MODEL,
    TESTSUITE,
    DOCUMENTATION,
    TOKENS,
    DESIGN,
    SVG;

    public String value() {
        return name();
    }

    public static AuraDefType fromValue(String v) {
        return valueOf(v);
    }

}
