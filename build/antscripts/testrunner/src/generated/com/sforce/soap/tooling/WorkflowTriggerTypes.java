
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WorkflowTriggerTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WorkflowTriggerTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="onCreateOnly"/&gt;
 *     &lt;enumeration value="onCreateOrTriggeringUpdate"/&gt;
 *     &lt;enumeration value="onAllChanges"/&gt;
 *     &lt;enumeration value="OnRecursiveUpdate"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WorkflowTriggerTypes")
@XmlEnum
public enum WorkflowTriggerTypes {

    @XmlEnumValue("onCreateOnly")
    ON_CREATE_ONLY("onCreateOnly"),
    @XmlEnumValue("onCreateOrTriggeringUpdate")
    ON_CREATE_OR_TRIGGERING_UPDATE("onCreateOrTriggeringUpdate"),
    @XmlEnumValue("onAllChanges")
    ON_ALL_CHANGES("onAllChanges"),
    @XmlEnumValue("OnRecursiveUpdate")
    ON_RECURSIVE_UPDATE("OnRecursiveUpdate");
    private final String value;

    WorkflowTriggerTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WorkflowTriggerTypes fromValue(String v) {
        for (WorkflowTriggerTypes c: WorkflowTriggerTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
