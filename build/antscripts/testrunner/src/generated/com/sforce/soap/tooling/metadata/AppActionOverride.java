
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AppActionOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AppActionOverride"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}ActionOverride"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pageOrSobjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppActionOverride", propOrder = {
    "pageOrSobjectType"
})
public class AppActionOverride
    extends ActionOverride
{

    @XmlElement(required = true)
    protected String pageOrSobjectType;

    /**
     * Gets the value of the pageOrSobjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPageOrSobjectType() {
        return pageOrSobjectType;
    }

    /**
     * Sets the value of the pageOrSobjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPageOrSobjectType(String value) {
        this.pageOrSobjectType = value;
    }

}
