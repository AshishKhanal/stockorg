
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Metadata complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Metadata"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Metadata")
@XmlSeeAlso({
    ApexClass.class,
    ApexComponent.class,
    ApexPage.class,
    ApexTestSuite.class,
    ApexTrigger.class,
    AssignmentRule.class,
    AssignmentRules.class,
    AuraDefinitionBundle.class,
    AutoResponseRule.class,
    AutoResponseRules.class,
    BusinessHoursEntry.class,
    BusinessProcess.class,
    GlobalPicklistValue.class,
    Certificate.class,
    CleanDataService.class,
    Community.class,
    CommunityTemplateDefinition.class,
    CommunityThemeDefinition.class,
    CompactLayout.class,
    CspTrustedSite.class,
    CustomApplication.class,
    CustomField.class,
    CustomValue.class,
    CustomLabel.class,
    CustomLabels.class,
    CustomObject.class,
    CustomPageWebLink.class,
    CustomPermission.class,
    CustomTab.class,
    DataPipeline.class,
    Document.class,
    EmailTemplate.class,
    EmbeddedServiceBranding.class,
    EmbeddedServiceConfig.class,
    EmbeddedServiceLiveAgent.class,
    EscalationRule.class,
    EscalationRules.class,
    EventDelivery.class,
    EventSubscription.class,
    EventType.class,
    FieldSet.class,
    FlexiPage.class,
    Flow.class,
    FlowDefinition.class,
    Folder.class,
    GlobalValueSet.class,
    Group.class,
    HomePageLayout.class,
    KeywordList.class,
    Layout.class,
    LeadConvertSettings.class,
    ListView.class,
    ModerationRule.class,
    PathAssistant.class,
    PermissionSet.class,
    PlatformActionList.class,
    Profile.class,
    Queue.class,
    QuickAction.class,
    RecordType.class,
    RemoteSiteSetting.class,
    RoleOrTerritory.class,
    StandardValueSet.class,
    StaticResource.class,
    TransactionSecurityPolicy.class,
    UserCriteria.class,
    ValidationRule.class,
    WebLink.class,
    Workflow.class,
    WorkflowAction.class,
    WorkflowRule.class,
    MetadataForSettings.class
})
public class Metadata {


}
