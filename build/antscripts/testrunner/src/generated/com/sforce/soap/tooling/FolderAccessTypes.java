
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FolderAccessTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FolderAccessTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Shared"/&gt;
 *     &lt;enumeration value="Public"/&gt;
 *     &lt;enumeration value="Hidden"/&gt;
 *     &lt;enumeration value="PublicInternal"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FolderAccessTypes")
@XmlEnum
public enum FolderAccessTypes {

    @XmlEnumValue("Shared")
    SHARED("Shared"),
    @XmlEnumValue("Public")
    PUBLIC("Public"),
    @XmlEnumValue("Hidden")
    HIDDEN("Hidden"),
    @XmlEnumValue("PublicInternal")
    PUBLIC_INTERNAL("PublicInternal");
    private final String value;

    FolderAccessTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FolderAccessTypes fromValue(String v) {
        for (FolderAccessTypes c: FolderAccessTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
