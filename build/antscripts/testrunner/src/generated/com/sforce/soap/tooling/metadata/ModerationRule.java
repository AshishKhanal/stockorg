
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.ModerationRuleAction;
import com.sforce.soap.tooling.ModerationRuleType;
import com.sforce.soap.tooling.RateLimitTimePeriod;


/**
 * <p>Java class for ModerationRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ModerationRule"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="action" type="{urn:tooling.soap.sforce.com}ModerationRuleAction"/&gt;
 *         &lt;element name="actionLimit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="active" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitiesAndFields" type="{urn:metadata.tooling.soap.sforce.com}ModeratedEntityField" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="notifyLimit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="timePeriod" type="{urn:tooling.soap.sforce.com}RateLimitTimePeriod" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}ModerationRuleType" minOccurs="0"/&gt;
 *         &lt;element name="userCriteria" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="userMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModerationRule", propOrder = {
    "action",
    "actionLimit",
    "active",
    "description",
    "entitiesAndFields",
    "masterLabel",
    "notifyLimit",
    "timePeriod",
    "type",
    "userCriteria",
    "userMessage"
})
public class ModerationRule
    extends Metadata
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ModerationRuleAction action;
    protected Integer actionLimit;
    protected boolean active;
    protected String description;
    protected List<ModeratedEntityField> entitiesAndFields;
    @XmlElement(required = true)
    protected String masterLabel;
    protected Integer notifyLimit;
    @XmlSchemaType(name = "string")
    protected RateLimitTimePeriod timePeriod;
    @XmlSchemaType(name = "string")
    protected ModerationRuleType type;
    protected List<String> userCriteria;
    protected String userMessage;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link ModerationRuleAction }
     *     
     */
    public ModerationRuleAction getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModerationRuleAction }
     *     
     */
    public void setAction(ModerationRuleAction value) {
        this.action = value;
    }

    /**
     * Gets the value of the actionLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getActionLimit() {
        return actionLimit;
    }

    /**
     * Sets the value of the actionLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setActionLimit(Integer value) {
        this.actionLimit = value;
    }

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the entitiesAndFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entitiesAndFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntitiesAndFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ModeratedEntityField }
     * 
     * 
     */
    public List<ModeratedEntityField> getEntitiesAndFields() {
        if (entitiesAndFields == null) {
            entitiesAndFields = new ArrayList<ModeratedEntityField>();
        }
        return this.entitiesAndFields;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the notifyLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNotifyLimit() {
        return notifyLimit;
    }

    /**
     * Sets the value of the notifyLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNotifyLimit(Integer value) {
        this.notifyLimit = value;
    }

    /**
     * Gets the value of the timePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link RateLimitTimePeriod }
     *     
     */
    public RateLimitTimePeriod getTimePeriod() {
        return timePeriod;
    }

    /**
     * Sets the value of the timePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateLimitTimePeriod }
     *     
     */
    public void setTimePeriod(RateLimitTimePeriod value) {
        this.timePeriod = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link ModerationRuleType }
     *     
     */
    public ModerationRuleType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModerationRuleType }
     *     
     */
    public void setType(ModerationRuleType value) {
        this.type = value;
    }

    /**
     * Gets the value of the userCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUserCriteria() {
        if (userCriteria == null) {
            userCriteria = new ArrayList<String>();
        }
        return this.userCriteria;
    }

    /**
     * Gets the value of the userMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * Sets the value of the userMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserMessage(String value) {
        this.userMessage = value;
    }

}
