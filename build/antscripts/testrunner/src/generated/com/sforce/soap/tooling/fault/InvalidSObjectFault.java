
package com.sforce.soap.tooling.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvalidSObjectFault complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvalidSObjectFault"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:fault.tooling.soap.sforce.com}ApiQueryFault"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvalidSObjectFault")
public class InvalidSObjectFault
    extends ApiQueryFault
{


}
