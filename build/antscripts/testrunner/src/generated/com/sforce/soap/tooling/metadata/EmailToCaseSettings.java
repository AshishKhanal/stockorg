
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.EmailToCaseOnFailureActionType;


/**
 * <p>Java class for EmailToCaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmailToCaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableEmailToCase" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableHtmlEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableOnDemandEmailToCase" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableThreadIDInBody" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableThreadIDInSubject" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="notifyOwnerOnNewCaseEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="overEmailLimitAction" type="{urn:tooling.soap.sforce.com}EmailToCaseOnFailureActionType" minOccurs="0"/&gt;
 *         &lt;element name="preQuoteSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="routingAddresses" type="{urn:metadata.tooling.soap.sforce.com}EmailToCaseRoutingAddress" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="unauthorizedSenderAction" type="{urn:tooling.soap.sforce.com}EmailToCaseOnFailureActionType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailToCaseSettings", propOrder = {
    "enableEmailToCase",
    "enableHtmlEmail",
    "enableOnDemandEmailToCase",
    "enableThreadIDInBody",
    "enableThreadIDInSubject",
    "notifyOwnerOnNewCaseEmail",
    "overEmailLimitAction",
    "preQuoteSignature",
    "routingAddresses",
    "unauthorizedSenderAction"
})
public class EmailToCaseSettings {

    protected Boolean enableEmailToCase;
    protected Boolean enableHtmlEmail;
    protected Boolean enableOnDemandEmailToCase;
    protected Boolean enableThreadIDInBody;
    protected Boolean enableThreadIDInSubject;
    protected Boolean notifyOwnerOnNewCaseEmail;
    @XmlSchemaType(name = "string")
    protected EmailToCaseOnFailureActionType overEmailLimitAction;
    protected Boolean preQuoteSignature;
    protected List<EmailToCaseRoutingAddress> routingAddresses;
    @XmlSchemaType(name = "string")
    protected EmailToCaseOnFailureActionType unauthorizedSenderAction;

    /**
     * Gets the value of the enableEmailToCase property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableEmailToCase() {
        return enableEmailToCase;
    }

    /**
     * Sets the value of the enableEmailToCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableEmailToCase(Boolean value) {
        this.enableEmailToCase = value;
    }

    /**
     * Gets the value of the enableHtmlEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableHtmlEmail() {
        return enableHtmlEmail;
    }

    /**
     * Sets the value of the enableHtmlEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableHtmlEmail(Boolean value) {
        this.enableHtmlEmail = value;
    }

    /**
     * Gets the value of the enableOnDemandEmailToCase property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableOnDemandEmailToCase() {
        return enableOnDemandEmailToCase;
    }

    /**
     * Sets the value of the enableOnDemandEmailToCase property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableOnDemandEmailToCase(Boolean value) {
        this.enableOnDemandEmailToCase = value;
    }

    /**
     * Gets the value of the enableThreadIDInBody property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableThreadIDInBody() {
        return enableThreadIDInBody;
    }

    /**
     * Sets the value of the enableThreadIDInBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableThreadIDInBody(Boolean value) {
        this.enableThreadIDInBody = value;
    }

    /**
     * Gets the value of the enableThreadIDInSubject property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableThreadIDInSubject() {
        return enableThreadIDInSubject;
    }

    /**
     * Sets the value of the enableThreadIDInSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableThreadIDInSubject(Boolean value) {
        this.enableThreadIDInSubject = value;
    }

    /**
     * Gets the value of the notifyOwnerOnNewCaseEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyOwnerOnNewCaseEmail() {
        return notifyOwnerOnNewCaseEmail;
    }

    /**
     * Sets the value of the notifyOwnerOnNewCaseEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyOwnerOnNewCaseEmail(Boolean value) {
        this.notifyOwnerOnNewCaseEmail = value;
    }

    /**
     * Gets the value of the overEmailLimitAction property.
     * 
     * @return
     *     possible object is
     *     {@link EmailToCaseOnFailureActionType }
     *     
     */
    public EmailToCaseOnFailureActionType getOverEmailLimitAction() {
        return overEmailLimitAction;
    }

    /**
     * Sets the value of the overEmailLimitAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailToCaseOnFailureActionType }
     *     
     */
    public void setOverEmailLimitAction(EmailToCaseOnFailureActionType value) {
        this.overEmailLimitAction = value;
    }

    /**
     * Gets the value of the preQuoteSignature property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreQuoteSignature() {
        return preQuoteSignature;
    }

    /**
     * Sets the value of the preQuoteSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreQuoteSignature(Boolean value) {
        this.preQuoteSignature = value;
    }

    /**
     * Gets the value of the routingAddresses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routingAddresses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoutingAddresses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailToCaseRoutingAddress }
     * 
     * 
     */
    public List<EmailToCaseRoutingAddress> getRoutingAddresses() {
        if (routingAddresses == null) {
            routingAddresses = new ArrayList<EmailToCaseRoutingAddress>();
        }
        return this.routingAddresses;
    }

    /**
     * Gets the value of the unauthorizedSenderAction property.
     * 
     * @return
     *     possible object is
     *     {@link EmailToCaseOnFailureActionType }
     *     
     */
    public EmailToCaseOnFailureActionType getUnauthorizedSenderAction() {
        return unauthorizedSenderAction;
    }

    /**
     * Sets the value of the unauthorizedSenderAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailToCaseOnFailureActionType }
     *     
     */
    public void setUnauthorizedSenderAction(EmailToCaseOnFailureActionType value) {
        this.unauthorizedSenderAction = value;
    }

}
