
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MobileSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MobileSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="chatterMobile" type="{urn:metadata.tooling.soap.sforce.com}ChatterMobileSettings" minOccurs="0"/&gt;
 *         &lt;element name="dashboardMobile" type="{urn:metadata.tooling.soap.sforce.com}DashboardMobileSettings" minOccurs="0"/&gt;
 *         &lt;element name="salesforceMobile" type="{urn:metadata.tooling.soap.sforce.com}SFDCMobileSettings" minOccurs="0"/&gt;
 *         &lt;element name="touchMobile" type="{urn:metadata.tooling.soap.sforce.com}TouchMobileSettings" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MobileSettings", propOrder = {
    "chatterMobile",
    "dashboardMobile",
    "salesforceMobile",
    "touchMobile"
})
public class MobileSettings
    extends MetadataForSettings
{

    protected ChatterMobileSettings chatterMobile;
    protected DashboardMobileSettings dashboardMobile;
    protected SFDCMobileSettings salesforceMobile;
    protected TouchMobileSettings touchMobile;

    /**
     * Gets the value of the chatterMobile property.
     * 
     * @return
     *     possible object is
     *     {@link ChatterMobileSettings }
     *     
     */
    public ChatterMobileSettings getChatterMobile() {
        return chatterMobile;
    }

    /**
     * Sets the value of the chatterMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChatterMobileSettings }
     *     
     */
    public void setChatterMobile(ChatterMobileSettings value) {
        this.chatterMobile = value;
    }

    /**
     * Gets the value of the dashboardMobile property.
     * 
     * @return
     *     possible object is
     *     {@link DashboardMobileSettings }
     *     
     */
    public DashboardMobileSettings getDashboardMobile() {
        return dashboardMobile;
    }

    /**
     * Sets the value of the dashboardMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link DashboardMobileSettings }
     *     
     */
    public void setDashboardMobile(DashboardMobileSettings value) {
        this.dashboardMobile = value;
    }

    /**
     * Gets the value of the salesforceMobile property.
     * 
     * @return
     *     possible object is
     *     {@link SFDCMobileSettings }
     *     
     */
    public SFDCMobileSettings getSalesforceMobile() {
        return salesforceMobile;
    }

    /**
     * Sets the value of the salesforceMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link SFDCMobileSettings }
     *     
     */
    public void setSalesforceMobile(SFDCMobileSettings value) {
        this.salesforceMobile = value;
    }

    /**
     * Gets the value of the touchMobile property.
     * 
     * @return
     *     possible object is
     *     {@link TouchMobileSettings }
     *     
     */
    public TouchMobileSettings getTouchMobile() {
        return touchMobile;
    }

    /**
     * Sets the value of the touchMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link TouchMobileSettings }
     *     
     */
    public void setTouchMobile(TouchMobileSettings value) {
        this.touchMobile = value;
    }

}
