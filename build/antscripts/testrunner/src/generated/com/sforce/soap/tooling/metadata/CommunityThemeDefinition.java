
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunityThemeDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunityThemeDefinition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customThemeLayoutType" type="{urn:metadata.tooling.soap.sforce.com}CommunityCustomThemeLayoutType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="enableExtendedCleanUpOnDelete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="masterLabel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="themeSetting" type="{urn:metadata.tooling.soap.sforce.com}CommunityThemeSetting" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunityThemeDefinition", propOrder = {
    "customThemeLayoutType",
    "description",
    "enableExtendedCleanUpOnDelete",
    "masterLabel",
    "themeSetting"
})
public class CommunityThemeDefinition
    extends Metadata
{

    protected List<CommunityCustomThemeLayoutType> customThemeLayoutType;
    protected String description;
    protected Boolean enableExtendedCleanUpOnDelete;
    @XmlElement(required = true)
    protected String masterLabel;
    protected List<CommunityThemeSetting> themeSetting;

    /**
     * Gets the value of the customThemeLayoutType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customThemeLayoutType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomThemeLayoutType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunityCustomThemeLayoutType }
     * 
     * 
     */
    public List<CommunityCustomThemeLayoutType> getCustomThemeLayoutType() {
        if (customThemeLayoutType == null) {
            customThemeLayoutType = new ArrayList<CommunityCustomThemeLayoutType>();
        }
        return this.customThemeLayoutType;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the enableExtendedCleanUpOnDelete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableExtendedCleanUpOnDelete() {
        return enableExtendedCleanUpOnDelete;
    }

    /**
     * Sets the value of the enableExtendedCleanUpOnDelete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableExtendedCleanUpOnDelete(Boolean value) {
        this.enableExtendedCleanUpOnDelete = value;
    }

    /**
     * Gets the value of the masterLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterLabel() {
        return masterLabel;
    }

    /**
     * Sets the value of the masterLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterLabel(String value) {
        this.masterLabel = value;
    }

    /**
     * Gets the value of the themeSetting property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the themeSetting property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getThemeSetting().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunityThemeSetting }
     * 
     * 
     */
    public List<CommunityThemeSetting> getThemeSetting() {
        if (themeSetting == null) {
            themeSetting = new ArrayList<CommunityThemeSetting>();
        }
        return this.themeSetting;
    }

}
