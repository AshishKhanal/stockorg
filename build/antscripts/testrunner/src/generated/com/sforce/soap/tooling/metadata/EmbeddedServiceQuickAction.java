
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmbeddedServiceQuickAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmbeddedServiceQuickAction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="embeddedServiceLiveAgent" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="order" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="quickActionDefinition" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmbeddedServiceQuickAction", propOrder = {
    "embeddedServiceLiveAgent",
    "order",
    "quickActionDefinition"
})
public class EmbeddedServiceQuickAction {

    @XmlElement(required = true)
    protected String embeddedServiceLiveAgent;
    protected int order;
    @XmlElement(required = true)
    protected String quickActionDefinition;

    /**
     * Gets the value of the embeddedServiceLiveAgent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmbeddedServiceLiveAgent() {
        return embeddedServiceLiveAgent;
    }

    /**
     * Sets the value of the embeddedServiceLiveAgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmbeddedServiceLiveAgent(String value) {
        this.embeddedServiceLiveAgent = value;
    }

    /**
     * Gets the value of the order property.
     * 
     */
    public int getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     */
    public void setOrder(int value) {
        this.order = value;
    }

    /**
     * Gets the value of the quickActionDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickActionDefinition() {
        return quickActionDefinition;
    }

    /**
     * Sets the value of the quickActionDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickActionDefinition(String value) {
        this.quickActionDefinition = value;
    }

}
