
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KeyboardShortcuts complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KeyboardShortcuts"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customShortcut" type="{urn:metadata.tooling.soap.sforce.com}CustomShortcut" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="defaultShortcut" type="{urn:metadata.tooling.soap.sforce.com}DefaultShortcut" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyboardShortcuts", propOrder = {
    "customShortcut",
    "defaultShortcut"
})
public class KeyboardShortcuts {

    protected List<CustomShortcut> customShortcut;
    protected List<DefaultShortcut> defaultShortcut;

    /**
     * Gets the value of the customShortcut property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customShortcut property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomShortcut().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomShortcut }
     * 
     * 
     */
    public List<CustomShortcut> getCustomShortcut() {
        if (customShortcut == null) {
            customShortcut = new ArrayList<CustomShortcut>();
        }
        return this.customShortcut;
    }

    /**
     * Gets the value of the defaultShortcut property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the defaultShortcut property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDefaultShortcut().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DefaultShortcut }
     * 
     * 
     */
    public List<DefaultShortcut> getDefaultShortcut() {
        if (defaultShortcut == null) {
            defaultShortcut = new ArrayList<DefaultShortcut>();
        }
        return this.defaultShortcut;
    }

}
