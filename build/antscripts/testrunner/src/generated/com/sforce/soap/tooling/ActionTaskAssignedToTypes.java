
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionTaskAssignedToTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionTaskAssignedToTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="user"/&gt;
 *     &lt;enumeration value="role"/&gt;
 *     &lt;enumeration value="opportunityTeam"/&gt;
 *     &lt;enumeration value="accountTeam"/&gt;
 *     &lt;enumeration value="owner"/&gt;
 *     &lt;enumeration value="accountOwner"/&gt;
 *     &lt;enumeration value="creator"/&gt;
 *     &lt;enumeration value="accountCreator"/&gt;
 *     &lt;enumeration value="partnerUser"/&gt;
 *     &lt;enumeration value="portalRole"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActionTaskAssignedToTypes")
@XmlEnum
public enum ActionTaskAssignedToTypes {

    @XmlEnumValue("user")
    USER("user"),
    @XmlEnumValue("role")
    ROLE("role"),
    @XmlEnumValue("opportunityTeam")
    OPPORTUNITY_TEAM("opportunityTeam"),
    @XmlEnumValue("accountTeam")
    ACCOUNT_TEAM("accountTeam"),
    @XmlEnumValue("owner")
    OWNER("owner"),
    @XmlEnumValue("accountOwner")
    ACCOUNT_OWNER("accountOwner"),
    @XmlEnumValue("creator")
    CREATOR("creator"),
    @XmlEnumValue("accountCreator")
    ACCOUNT_CREATOR("accountCreator"),
    @XmlEnumValue("partnerUser")
    PARTNER_USER("partnerUser"),
    @XmlEnumValue("portalRole")
    PORTAL_ROLE("portalRole");
    private final String value;

    ActionTaskAssignedToTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActionTaskAssignedToTypes fromValue(String v) {
        for (ActionTaskAssignedToTypes c: ActionTaskAssignedToTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
