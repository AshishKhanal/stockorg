
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonListSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonListSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enablePersonList" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonListSettings", propOrder = {
    "enablePersonList"
})
public class PersonListSettings
    extends MetadataForSettings
{

    protected boolean enablePersonList;

    /**
     * Gets the value of the enablePersonList property.
     * 
     */
    public boolean isEnablePersonList() {
        return enablePersonList;
    }

    /**
     * Sets the value of the enablePersonList property.
     * 
     */
    public void setEnablePersonList(boolean value) {
        this.enablePersonList = value;
    }

}
