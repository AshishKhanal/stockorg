
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileUploadAndDownloadSecuritySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FileUploadAndDownloadSecuritySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}MetadataForSettings"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dispositions" type="{urn:metadata.tooling.soap.sforce.com}FileTypeDispositionAssignmentBean" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="noHtmlUploadAsAttachment" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileUploadAndDownloadSecuritySettings", propOrder = {
    "dispositions",
    "noHtmlUploadAsAttachment"
})
public class FileUploadAndDownloadSecuritySettings
    extends MetadataForSettings
{

    protected List<FileTypeDispositionAssignmentBean> dispositions;
    protected boolean noHtmlUploadAsAttachment;

    /**
     * Gets the value of the dispositions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dispositions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDispositions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FileTypeDispositionAssignmentBean }
     * 
     * 
     */
    public List<FileTypeDispositionAssignmentBean> getDispositions() {
        if (dispositions == null) {
            dispositions = new ArrayList<FileTypeDispositionAssignmentBean>();
        }
        return this.dispositions;
    }

    /**
     * Gets the value of the noHtmlUploadAsAttachment property.
     * 
     */
    public boolean isNoHtmlUploadAsAttachment() {
        return noHtmlUploadAsAttachment;
    }

    /**
     * Sets the value of the noHtmlUploadAsAttachment property.
     * 
     */
    public void setNoHtmlUploadAsAttachment(boolean value) {
        this.noHtmlUploadAsAttachment = value;
    }

}
