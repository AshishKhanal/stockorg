
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PermissionSetTabVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PermissionSetTabVisibility"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="None"/&gt;
 *     &lt;enumeration value="Available"/&gt;
 *     &lt;enumeration value="Visible"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PermissionSetTabVisibility")
@XmlEnum
public enum PermissionSetTabVisibility {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Available")
    AVAILABLE("Available"),
    @XmlEnumValue("Visible")
    VISIBLE("Visible");
    private final String value;

    PermissionSetTabVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PermissionSetTabVisibility fromValue(String v) {
        for (PermissionSetTabVisibility c: PermissionSetTabVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
