
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlobalVariableContext.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GlobalVariableContext"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Visualforce"/&gt;
 *     &lt;enumeration value="Flow"/&gt;
 *     &lt;enumeration value="Validation"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GlobalVariableContext")
@XmlEnum
public enum GlobalVariableContext {

    @XmlEnumValue("Visualforce")
    VISUALFORCE("Visualforce"),
    @XmlEnumValue("Flow")
    FLOW("Flow"),
    @XmlEnumValue("Validation")
    VALIDATION("Validation");
    private final String value;

    GlobalVariableContext(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GlobalVariableContext fromValue(String v) {
        for (GlobalVariableContext c: GlobalVariableContext.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
