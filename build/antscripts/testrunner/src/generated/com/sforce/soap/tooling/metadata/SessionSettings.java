
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SessionSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SessionSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="disableTimeoutWarning" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableCSPOnEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableCSRFOnGet" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableCSRFOnPost" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableCacheAndAutocomplete" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableClickjackNonsetupSFDC" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableClickjackNonsetupUser" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableClickjackNonsetupUserHeaderless" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableClickjackSetup" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableContentSniffingProtection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enablePostForSessions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableSMSIdentity" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableXssProtection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enforceIpRangesEveryRequest" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forceLogoutOnSessionTimeout" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forceRelogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="lockSessionsToDomain" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="lockSessionsToIp" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="logoutURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="securityCentralKillSession" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="sessionTimeout" type="{urn:metadata.tooling.soap.sforce.com}SessionTimeout" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SessionSettings", propOrder = {
    "disableTimeoutWarning",
    "enableCSPOnEmail",
    "enableCSRFOnGet",
    "enableCSRFOnPost",
    "enableCacheAndAutocomplete",
    "enableClickjackNonsetupSFDC",
    "enableClickjackNonsetupUser",
    "enableClickjackNonsetupUserHeaderless",
    "enableClickjackSetup",
    "enableContentSniffingProtection",
    "enablePostForSessions",
    "enableSMSIdentity",
    "enableXssProtection",
    "enforceIpRangesEveryRequest",
    "forceLogoutOnSessionTimeout",
    "forceRelogin",
    "lockSessionsToDomain",
    "lockSessionsToIp",
    "logoutURL",
    "securityCentralKillSession",
    "sessionTimeout"
})
public class SessionSettings {

    protected Boolean disableTimeoutWarning;
    protected Boolean enableCSPOnEmail;
    protected Boolean enableCSRFOnGet;
    protected Boolean enableCSRFOnPost;
    protected Boolean enableCacheAndAutocomplete;
    protected Boolean enableClickjackNonsetupSFDC;
    protected Boolean enableClickjackNonsetupUser;
    protected Boolean enableClickjackNonsetupUserHeaderless;
    protected Boolean enableClickjackSetup;
    protected Boolean enableContentSniffingProtection;
    protected Boolean enablePostForSessions;
    protected Boolean enableSMSIdentity;
    protected Boolean enableXssProtection;
    protected Boolean enforceIpRangesEveryRequest;
    protected Boolean forceLogoutOnSessionTimeout;
    protected Boolean forceRelogin;
    protected Boolean lockSessionsToDomain;
    protected Boolean lockSessionsToIp;
    protected String logoutURL;
    protected Boolean securityCentralKillSession;
    @XmlSchemaType(name = "string")
    protected SessionTimeout sessionTimeout;

    /**
     * Gets the value of the disableTimeoutWarning property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDisableTimeoutWarning() {
        return disableTimeoutWarning;
    }

    /**
     * Sets the value of the disableTimeoutWarning property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisableTimeoutWarning(Boolean value) {
        this.disableTimeoutWarning = value;
    }

    /**
     * Gets the value of the enableCSPOnEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCSPOnEmail() {
        return enableCSPOnEmail;
    }

    /**
     * Sets the value of the enableCSPOnEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCSPOnEmail(Boolean value) {
        this.enableCSPOnEmail = value;
    }

    /**
     * Gets the value of the enableCSRFOnGet property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCSRFOnGet() {
        return enableCSRFOnGet;
    }

    /**
     * Sets the value of the enableCSRFOnGet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCSRFOnGet(Boolean value) {
        this.enableCSRFOnGet = value;
    }

    /**
     * Gets the value of the enableCSRFOnPost property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCSRFOnPost() {
        return enableCSRFOnPost;
    }

    /**
     * Sets the value of the enableCSRFOnPost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCSRFOnPost(Boolean value) {
        this.enableCSRFOnPost = value;
    }

    /**
     * Gets the value of the enableCacheAndAutocomplete property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableCacheAndAutocomplete() {
        return enableCacheAndAutocomplete;
    }

    /**
     * Sets the value of the enableCacheAndAutocomplete property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableCacheAndAutocomplete(Boolean value) {
        this.enableCacheAndAutocomplete = value;
    }

    /**
     * Gets the value of the enableClickjackNonsetupSFDC property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableClickjackNonsetupSFDC() {
        return enableClickjackNonsetupSFDC;
    }

    /**
     * Sets the value of the enableClickjackNonsetupSFDC property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableClickjackNonsetupSFDC(Boolean value) {
        this.enableClickjackNonsetupSFDC = value;
    }

    /**
     * Gets the value of the enableClickjackNonsetupUser property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableClickjackNonsetupUser() {
        return enableClickjackNonsetupUser;
    }

    /**
     * Sets the value of the enableClickjackNonsetupUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableClickjackNonsetupUser(Boolean value) {
        this.enableClickjackNonsetupUser = value;
    }

    /**
     * Gets the value of the enableClickjackNonsetupUserHeaderless property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableClickjackNonsetupUserHeaderless() {
        return enableClickjackNonsetupUserHeaderless;
    }

    /**
     * Sets the value of the enableClickjackNonsetupUserHeaderless property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableClickjackNonsetupUserHeaderless(Boolean value) {
        this.enableClickjackNonsetupUserHeaderless = value;
    }

    /**
     * Gets the value of the enableClickjackSetup property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableClickjackSetup() {
        return enableClickjackSetup;
    }

    /**
     * Sets the value of the enableClickjackSetup property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableClickjackSetup(Boolean value) {
        this.enableClickjackSetup = value;
    }

    /**
     * Gets the value of the enableContentSniffingProtection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableContentSniffingProtection() {
        return enableContentSniffingProtection;
    }

    /**
     * Sets the value of the enableContentSniffingProtection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableContentSniffingProtection(Boolean value) {
        this.enableContentSniffingProtection = value;
    }

    /**
     * Gets the value of the enablePostForSessions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnablePostForSessions() {
        return enablePostForSessions;
    }

    /**
     * Sets the value of the enablePostForSessions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnablePostForSessions(Boolean value) {
        this.enablePostForSessions = value;
    }

    /**
     * Gets the value of the enableSMSIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableSMSIdentity() {
        return enableSMSIdentity;
    }

    /**
     * Sets the value of the enableSMSIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableSMSIdentity(Boolean value) {
        this.enableSMSIdentity = value;
    }

    /**
     * Gets the value of the enableXssProtection property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableXssProtection() {
        return enableXssProtection;
    }

    /**
     * Sets the value of the enableXssProtection property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableXssProtection(Boolean value) {
        this.enableXssProtection = value;
    }

    /**
     * Gets the value of the enforceIpRangesEveryRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnforceIpRangesEveryRequest() {
        return enforceIpRangesEveryRequest;
    }

    /**
     * Sets the value of the enforceIpRangesEveryRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnforceIpRangesEveryRequest(Boolean value) {
        this.enforceIpRangesEveryRequest = value;
    }

    /**
     * Gets the value of the forceLogoutOnSessionTimeout property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceLogoutOnSessionTimeout() {
        return forceLogoutOnSessionTimeout;
    }

    /**
     * Sets the value of the forceLogoutOnSessionTimeout property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceLogoutOnSessionTimeout(Boolean value) {
        this.forceLogoutOnSessionTimeout = value;
    }

    /**
     * Gets the value of the forceRelogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceRelogin() {
        return forceRelogin;
    }

    /**
     * Sets the value of the forceRelogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceRelogin(Boolean value) {
        this.forceRelogin = value;
    }

    /**
     * Gets the value of the lockSessionsToDomain property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLockSessionsToDomain() {
        return lockSessionsToDomain;
    }

    /**
     * Sets the value of the lockSessionsToDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockSessionsToDomain(Boolean value) {
        this.lockSessionsToDomain = value;
    }

    /**
     * Gets the value of the lockSessionsToIp property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLockSessionsToIp() {
        return lockSessionsToIp;
    }

    /**
     * Sets the value of the lockSessionsToIp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLockSessionsToIp(Boolean value) {
        this.lockSessionsToIp = value;
    }

    /**
     * Gets the value of the logoutURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogoutURL() {
        return logoutURL;
    }

    /**
     * Sets the value of the logoutURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogoutURL(String value) {
        this.logoutURL = value;
    }

    /**
     * Gets the value of the securityCentralKillSession property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSecurityCentralKillSession() {
        return securityCentralKillSession;
    }

    /**
     * Sets the value of the securityCentralKillSession property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSecurityCentralKillSession(Boolean value) {
        this.securityCentralKillSession = value;
    }

    /**
     * Gets the value of the sessionTimeout property.
     * 
     * @return
     *     possible object is
     *     {@link SessionTimeout }
     *     
     */
    public SessionTimeout getSessionTimeout() {
        return sessionTimeout;
    }

    /**
     * Sets the value of the sessionTimeout property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionTimeout }
     *     
     */
    public void setSessionTimeout(SessionTimeout value) {
        this.sessionTimeout = value;
    }

}
