
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CommunityTemplateCategory.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CommunityTemplateCategory"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="IT"/&gt;
 *     &lt;enumeration value="Marketing"/&gt;
 *     &lt;enumeration value="Sales"/&gt;
 *     &lt;enumeration value="Service"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CommunityTemplateCategory")
@XmlEnum
public enum CommunityTemplateCategory {

    IT("IT"),
    @XmlEnumValue("Marketing")
    MARKETING("Marketing"),
    @XmlEnumValue("Sales")
    SALES("Sales"),
    @XmlEnumValue("Service")
    SERVICE("Service");
    private final String value;

    CommunityTemplateCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CommunityTemplateCategory fromValue(String v) {
        for (CommunityTemplateCategory c: CommunityTemplateCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
