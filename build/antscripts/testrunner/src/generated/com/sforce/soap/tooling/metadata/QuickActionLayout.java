
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.LayoutSectionStyle;


/**
 * <p>Java class for QuickActionLayout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickActionLayout"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="layoutSectionStyle" type="{urn:tooling.soap.sforce.com}LayoutSectionStyle"/&gt;
 *         &lt;element name="quickActionLayoutColumns" type="{urn:metadata.tooling.soap.sforce.com}QuickActionLayoutColumn" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickActionLayout", propOrder = {
    "layoutSectionStyle",
    "quickActionLayoutColumns"
})
public class QuickActionLayout {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected LayoutSectionStyle layoutSectionStyle;
    protected List<QuickActionLayoutColumn> quickActionLayoutColumns;

    /**
     * Gets the value of the layoutSectionStyle property.
     * 
     * @return
     *     possible object is
     *     {@link LayoutSectionStyle }
     *     
     */
    public LayoutSectionStyle getLayoutSectionStyle() {
        return layoutSectionStyle;
    }

    /**
     * Sets the value of the layoutSectionStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link LayoutSectionStyle }
     *     
     */
    public void setLayoutSectionStyle(LayoutSectionStyle value) {
        this.layoutSectionStyle = value;
    }

    /**
     * Gets the value of the quickActionLayoutColumns property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quickActionLayoutColumns property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuickActionLayoutColumns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuickActionLayoutColumn }
     * 
     * 
     */
    public List<QuickActionLayoutColumn> getQuickActionLayoutColumns() {
        if (quickActionLayoutColumns == null) {
            quickActionLayoutColumns = new ArrayList<QuickActionLayoutColumn>();
        }
        return this.quickActionLayoutColumns;
    }

}
