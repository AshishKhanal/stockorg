
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProvInvocationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProvInvocationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Flow"/&gt;
 *     &lt;enumeration value="ApexHandler"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ProvInvocationType")
@XmlEnum
public enum ProvInvocationType {

    @XmlEnumValue("Flow")
    FLOW("Flow"),
    @XmlEnumValue("ApexHandler")
    APEX_HANDLER("ApexHandler");
    private final String value;

    ProvInvocationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ProvInvocationType fromValue(String v) {
        for (ProvInvocationType c: ProvInvocationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
