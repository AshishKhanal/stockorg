
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ContainerAsyncOperation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ContainerAsyncOperation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Deploy"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ContainerAsyncOperation")
@XmlEnum
public enum ContainerAsyncOperation {

    @XmlEnumValue("Deploy")
    DEPLOY("Deploy");
    private final String value;

    ContainerAsyncOperation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContainerAsyncOperation fromValue(String v) {
        for (ContainerAsyncOperation c: ContainerAsyncOperation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
