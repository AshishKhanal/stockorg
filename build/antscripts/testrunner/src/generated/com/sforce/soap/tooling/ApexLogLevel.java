
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexLogLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexLogLevel"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NONE"/&gt;
 *     &lt;enumeration value="INTERNAL"/&gt;
 *     &lt;enumeration value="FINEST"/&gt;
 *     &lt;enumeration value="FINER"/&gt;
 *     &lt;enumeration value="FINE"/&gt;
 *     &lt;enumeration value="DEBUG"/&gt;
 *     &lt;enumeration value="INFO"/&gt;
 *     &lt;enumeration value="WARN"/&gt;
 *     &lt;enumeration value="ERROR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexLogLevel")
@XmlEnum
public enum ApexLogLevel {

    NONE,
    INTERNAL,
    FINEST,
    FINER,
    FINE,
    DEBUG,
    INFO,
    WARN,
    ERROR;

    public String value() {
        return name();
    }

    public static ApexLogLevel fromValue(String v) {
        return valueOf(v);
    }

}
