
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KnowledgeCaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KnowledgeCaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ArticlePdfCreationProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AssignTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomizationClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DefaultContributionArticleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Editor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsArticleCreationEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsArticlePublicSharingSitesEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="UseProfileForPdfCreation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KnowledgeCaseSettings", propOrder = {
    "articlePdfCreationProfile",
    "assignTo",
    "customizationClass",
    "defaultContributionArticleType",
    "durableId",
    "editor",
    "isArticleCreationEnabled",
    "isArticlePublicSharingSitesEnabled",
    "useProfileForPdfCreation"
})
public class KnowledgeCaseSettings
    extends SObject
{

    @XmlElementRef(name = "ArticlePdfCreationProfile", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> articlePdfCreationProfile;
    @XmlElementRef(name = "AssignTo", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> assignTo;
    @XmlElementRef(name = "CustomizationClass", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customizationClass;
    @XmlElementRef(name = "DefaultContributionArticleType", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> defaultContributionArticleType;
    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "Editor", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> editor;
    @XmlElementRef(name = "IsArticleCreationEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isArticleCreationEnabled;
    @XmlElementRef(name = "IsArticlePublicSharingSitesEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isArticlePublicSharingSitesEnabled;
    @XmlElementRef(name = "UseProfileForPdfCreation", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useProfileForPdfCreation;

    /**
     * Gets the value of the articlePdfCreationProfile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getArticlePdfCreationProfile() {
        return articlePdfCreationProfile;
    }

    /**
     * Sets the value of the articlePdfCreationProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setArticlePdfCreationProfile(JAXBElement<String> value) {
        this.articlePdfCreationProfile = value;
    }

    /**
     * Gets the value of the assignTo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAssignTo() {
        return assignTo;
    }

    /**
     * Sets the value of the assignTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAssignTo(JAXBElement<String> value) {
        this.assignTo = value;
    }

    /**
     * Gets the value of the customizationClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomizationClass() {
        return customizationClass;
    }

    /**
     * Sets the value of the customizationClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomizationClass(JAXBElement<String> value) {
        this.customizationClass = value;
    }

    /**
     * Gets the value of the defaultContributionArticleType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDefaultContributionArticleType() {
        return defaultContributionArticleType;
    }

    /**
     * Sets the value of the defaultContributionArticleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDefaultContributionArticleType(JAXBElement<String> value) {
        this.defaultContributionArticleType = value;
    }

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the editor property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEditor() {
        return editor;
    }

    /**
     * Sets the value of the editor property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEditor(JAXBElement<String> value) {
        this.editor = value;
    }

    /**
     * Gets the value of the isArticleCreationEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsArticleCreationEnabled() {
        return isArticleCreationEnabled;
    }

    /**
     * Sets the value of the isArticleCreationEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsArticleCreationEnabled(JAXBElement<Boolean> value) {
        this.isArticleCreationEnabled = value;
    }

    /**
     * Gets the value of the isArticlePublicSharingSitesEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsArticlePublicSharingSitesEnabled() {
        return isArticlePublicSharingSitesEnabled;
    }

    /**
     * Sets the value of the isArticlePublicSharingSitesEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsArticlePublicSharingSitesEnabled(JAXBElement<Boolean> value) {
        this.isArticlePublicSharingSitesEnabled = value;
    }

    /**
     * Gets the value of the useProfileForPdfCreation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseProfileForPdfCreation() {
        return useProfileForPdfCreation;
    }

    /**
     * Sets the value of the useProfileForPdfCreation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseProfileForPdfCreation(JAXBElement<Boolean> value) {
        this.useProfileForPdfCreation = value;
    }

}
