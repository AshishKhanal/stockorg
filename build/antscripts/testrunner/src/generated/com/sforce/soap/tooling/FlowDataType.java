
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlowDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FlowDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Currency"/&gt;
 *     &lt;enumeration value="Date"/&gt;
 *     &lt;enumeration value="Number"/&gt;
 *     &lt;enumeration value="String"/&gt;
 *     &lt;enumeration value="Boolean"/&gt;
 *     &lt;enumeration value="SObject"/&gt;
 *     &lt;enumeration value="DateTime"/&gt;
 *     &lt;enumeration value="Picklist"/&gt;
 *     &lt;enumeration value="Multipicklist"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FlowDataType")
@XmlEnum
public enum FlowDataType {

    @XmlEnumValue("Currency")
    CURRENCY("Currency"),
    @XmlEnumValue("Date")
    DATE("Date"),
    @XmlEnumValue("Number")
    NUMBER("Number"),
    @XmlEnumValue("String")
    STRING("String"),
    @XmlEnumValue("Boolean")
    BOOLEAN("Boolean"),
    @XmlEnumValue("SObject")
    S_OBJECT("SObject"),
    @XmlEnumValue("DateTime")
    DATE_TIME("DateTime"),
    @XmlEnumValue("Picklist")
    PICKLIST("Picklist"),
    @XmlEnumValue("Multipicklist")
    MULTIPICKLIST("Multipicklist");
    private final String value;

    FlowDataType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FlowDataType fromValue(String v) {
        for (FlowDataType c: FlowDataType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
