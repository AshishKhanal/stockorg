
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionSecurityAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionSecurityAction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="block" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="endSession" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="notifications" type="{urn:metadata.tooling.soap.sforce.com}TransactionSecurityNotification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="twoFactorAuthentication" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionSecurityAction", propOrder = {
    "block",
    "endSession",
    "notifications",
    "twoFactorAuthentication"
})
public class TransactionSecurityAction {

    protected boolean block;
    protected boolean endSession;
    protected List<TransactionSecurityNotification> notifications;
    protected boolean twoFactorAuthentication;

    /**
     * Gets the value of the block property.
     * 
     */
    public boolean isBlock() {
        return block;
    }

    /**
     * Sets the value of the block property.
     * 
     */
    public void setBlock(boolean value) {
        this.block = value;
    }

    /**
     * Gets the value of the endSession property.
     * 
     */
    public boolean isEndSession() {
        return endSession;
    }

    /**
     * Sets the value of the endSession property.
     * 
     */
    public void setEndSession(boolean value) {
        this.endSession = value;
    }

    /**
     * Gets the value of the notifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionSecurityNotification }
     * 
     * 
     */
    public List<TransactionSecurityNotification> getNotifications() {
        if (notifications == null) {
            notifications = new ArrayList<TransactionSecurityNotification>();
        }
        return this.notifications;
    }

    /**
     * Gets the value of the twoFactorAuthentication property.
     * 
     */
    public boolean isTwoFactorAuthentication() {
        return twoFactorAuthentication;
    }

    /**
     * Sets the value of the twoFactorAuthentication property.
     * 
     */
    public void setTwoFactorAuthentication(boolean value) {
        this.twoFactorAuthentication = value;
    }

}
