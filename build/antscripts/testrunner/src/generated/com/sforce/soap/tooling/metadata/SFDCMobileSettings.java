
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SFDCMobileSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SFDCMobileSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="enableMobileLite" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="enableUserToDeviceLinking" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SFDCMobileSettings", propOrder = {
    "enableMobileLite",
    "enableUserToDeviceLinking"
})
public class SFDCMobileSettings {

    protected Boolean enableMobileLite;
    protected Boolean enableUserToDeviceLinking;

    /**
     * Gets the value of the enableMobileLite property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableMobileLite() {
        return enableMobileLite;
    }

    /**
     * Sets the value of the enableMobileLite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableMobileLite(Boolean value) {
        this.enableMobileLite = value;
    }

    /**
     * Gets the value of the enableUserToDeviceLinking property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEnableUserToDeviceLinking() {
        return enableUserToDeviceLinking;
    }

    /**
     * Sets the value of the enableUserToDeviceLinking property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEnableUserToDeviceLinking(Boolean value) {
        this.enableUserToDeviceLinking = value;
    }

}
