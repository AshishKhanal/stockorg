
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Pending"/&gt;
 *     &lt;enumeration value="Active"/&gt;
 *     &lt;enumeration value="Detach"/&gt;
 *     &lt;enumeration value="Kill"/&gt;
 *     &lt;enumeration value="KillAll"/&gt;
 *     &lt;enumeration value="Dead"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerStatus")
@XmlEnum
public enum ApexDebuggerStatus {

    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("Active")
    ACTIVE("Active"),
    @XmlEnumValue("Detach")
    DETACH("Detach"),
    @XmlEnumValue("Kill")
    KILL("Kill"),
    @XmlEnumValue("KillAll")
    KILL_ALL("KillAll"),
    @XmlEnumValue("Dead")
    DEAD("Dead");
    private final String value;

    ApexDebuggerStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerStatus fromValue(String v) {
        for (ApexDebuggerStatus c: ApexDebuggerStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
