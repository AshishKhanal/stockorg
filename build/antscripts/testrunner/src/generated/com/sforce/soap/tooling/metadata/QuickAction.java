
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.sforce.soap.tooling.QuickActionLabel;
import com.sforce.soap.tooling.QuickActionType;


/**
 * <p>Java class for QuickAction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuickAction"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="canvas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="height" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="icon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isProtected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lightningComponent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="optionsCreateFeedItem" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="page" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quickActionLayout" type="{urn:metadata.tooling.soap.sforce.com}QuickActionLayout" minOccurs="0"/&gt;
 *         &lt;element name="standardLabel" type="{urn:tooling.soap.sforce.com}QuickActionLabel" minOccurs="0"/&gt;
 *         &lt;element name="successMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="targetObject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="targetParentField" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="targetRecordType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{urn:tooling.soap.sforce.com}QuickActionType"/&gt;
 *         &lt;element name="width" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuickAction", propOrder = {
    "canvas",
    "description",
    "height",
    "icon",
    "isProtected",
    "label",
    "lightningComponent",
    "optionsCreateFeedItem",
    "page",
    "quickActionLayout",
    "standardLabel",
    "successMessage",
    "targetObject",
    "targetParentField",
    "targetRecordType",
    "type",
    "width"
})
public class QuickAction
    extends Metadata
{

    protected String canvas;
    protected String description;
    protected Integer height;
    protected String icon;
    protected Boolean isProtected;
    protected String label;
    protected String lightningComponent;
    protected boolean optionsCreateFeedItem;
    protected String page;
    protected QuickActionLayout quickActionLayout;
    @XmlSchemaType(name = "string")
    protected QuickActionLabel standardLabel;
    protected String successMessage;
    protected String targetObject;
    protected String targetParentField;
    protected String targetRecordType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected QuickActionType type;
    protected Integer width;

    /**
     * Gets the value of the canvas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanvas() {
        return canvas;
    }

    /**
     * Sets the value of the canvas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanvas(String value) {
        this.canvas = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHeight(Integer value) {
        this.height = value;
    }

    /**
     * Gets the value of the icon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Sets the value of the icon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIcon(String value) {
        this.icon = value;
    }

    /**
     * Gets the value of the isProtected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsProtected() {
        return isProtected;
    }

    /**
     * Sets the value of the isProtected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsProtected(Boolean value) {
        this.isProtected = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the lightningComponent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLightningComponent() {
        return lightningComponent;
    }

    /**
     * Sets the value of the lightningComponent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLightningComponent(String value) {
        this.lightningComponent = value;
    }

    /**
     * Gets the value of the optionsCreateFeedItem property.
     * 
     */
    public boolean isOptionsCreateFeedItem() {
        return optionsCreateFeedItem;
    }

    /**
     * Sets the value of the optionsCreateFeedItem property.
     * 
     */
    public void setOptionsCreateFeedItem(boolean value) {
        this.optionsCreateFeedItem = value;
    }

    /**
     * Gets the value of the page property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPage(String value) {
        this.page = value;
    }

    /**
     * Gets the value of the quickActionLayout property.
     * 
     * @return
     *     possible object is
     *     {@link QuickActionLayout }
     *     
     */
    public QuickActionLayout getQuickActionLayout() {
        return quickActionLayout;
    }

    /**
     * Sets the value of the quickActionLayout property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuickActionLayout }
     *     
     */
    public void setQuickActionLayout(QuickActionLayout value) {
        this.quickActionLayout = value;
    }

    /**
     * Gets the value of the standardLabel property.
     * 
     * @return
     *     possible object is
     *     {@link QuickActionLabel }
     *     
     */
    public QuickActionLabel getStandardLabel() {
        return standardLabel;
    }

    /**
     * Sets the value of the standardLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuickActionLabel }
     *     
     */
    public void setStandardLabel(QuickActionLabel value) {
        this.standardLabel = value;
    }

    /**
     * Gets the value of the successMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuccessMessage() {
        return successMessage;
    }

    /**
     * Sets the value of the successMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuccessMessage(String value) {
        this.successMessage = value;
    }

    /**
     * Gets the value of the targetObject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetObject() {
        return targetObject;
    }

    /**
     * Sets the value of the targetObject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetObject(String value) {
        this.targetObject = value;
    }

    /**
     * Gets the value of the targetParentField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetParentField() {
        return targetParentField;
    }

    /**
     * Sets the value of the targetParentField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetParentField(String value) {
        this.targetParentField = value;
    }

    /**
     * Gets the value of the targetRecordType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetRecordType() {
        return targetRecordType;
    }

    /**
     * Sets the value of the targetRecordType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetRecordType(String value) {
        this.targetRecordType = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link QuickActionType }
     *     
     */
    public QuickActionType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuickActionType }
     *     
     */
    public void setType(QuickActionType value) {
        this.type = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setWidth(Integer value) {
        this.width = value;
    }

}
