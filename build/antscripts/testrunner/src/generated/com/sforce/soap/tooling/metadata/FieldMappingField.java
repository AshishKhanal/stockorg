
package com.sforce.soap.tooling.metadata;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FieldMappingField complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FieldMappingField"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataServiceField" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="dataServiceObjectName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FieldMappingField", propOrder = {
    "dataServiceField",
    "dataServiceObjectName",
    "priority"
})
public class FieldMappingField {

    @XmlElement(required = true)
    protected String dataServiceField;
    @XmlElement(required = true)
    protected String dataServiceObjectName;
    protected int priority;

    /**
     * Gets the value of the dataServiceField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataServiceField() {
        return dataServiceField;
    }

    /**
     * Sets the value of the dataServiceField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataServiceField(String value) {
        this.dataServiceField = value;
    }

    /**
     * Gets the value of the dataServiceObjectName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataServiceObjectName() {
        return dataServiceObjectName;
    }

    /**
     * Sets the value of the dataServiceObjectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataServiceObjectName(String value) {
        this.dataServiceObjectName = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     */
    public void setPriority(int value) {
        this.priority = value;
    }

}
