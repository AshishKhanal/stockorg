
package com.sforce.soap.tooling;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DescribeLayout complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DescribeLayout"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="buttonLayoutSection" type="{urn:tooling.soap.sforce.com}DescribeLayoutButtonSection" minOccurs="0"/&gt;
 *         &lt;element name="detailLayoutSections" type="{urn:tooling.soap.sforce.com}DescribeLayoutSection" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="editLayoutSections" type="{urn:tooling.soap.sforce.com}DescribeLayoutSection" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="feedView" type="{urn:tooling.soap.sforce.com}DescribeLayoutFeedView" minOccurs="0"/&gt;
 *         &lt;element name="highlightsPanelLayoutSection" type="{urn:tooling.soap.sforce.com}DescribeLayoutSection" minOccurs="0"/&gt;
 *         &lt;element name="id" type="{urn:tooling.soap.sforce.com}ID"/&gt;
 *         &lt;element name="multirowEditLayoutSections" type="{urn:tooling.soap.sforce.com}DescribeLayoutSection" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="offlineLinks" type="{urn:tooling.soap.sforce.com}OfflineLink" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="quickActionList" type="{urn:tooling.soap.sforce.com}DescribeQuickActionListResult" minOccurs="0"/&gt;
 *         &lt;element name="relatedContent" type="{urn:tooling.soap.sforce.com}RelatedContent" minOccurs="0"/&gt;
 *         &lt;element name="relatedLists" type="{urn:tooling.soap.sforce.com}RelatedList" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="saveOptions" type="{urn:tooling.soap.sforce.com}DescribeLayoutSaveOption" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DescribeLayout", propOrder = {
    "buttonLayoutSection",
    "detailLayoutSections",
    "editLayoutSections",
    "feedView",
    "highlightsPanelLayoutSection",
    "id",
    "multirowEditLayoutSections",
    "offlineLinks",
    "quickActionList",
    "relatedContent",
    "relatedLists",
    "saveOptions"
})
public class DescribeLayout2 {

    protected DescribeLayoutButtonSection buttonLayoutSection;
    protected List<DescribeLayoutSection> detailLayoutSections;
    protected List<DescribeLayoutSection> editLayoutSections;
    protected DescribeLayoutFeedView feedView;
    protected DescribeLayoutSection highlightsPanelLayoutSection;
    @XmlElement(required = true)
    protected String id;
    protected List<DescribeLayoutSection> multirowEditLayoutSections;
    protected List<OfflineLink> offlineLinks;
    protected DescribeQuickActionListResult quickActionList;
    protected RelatedContent relatedContent;
    protected List<RelatedList> relatedLists;
    protected List<DescribeLayoutSaveOption> saveOptions;

    /**
     * Gets the value of the buttonLayoutSection property.
     * 
     * @return
     *     possible object is
     *     {@link DescribeLayoutButtonSection }
     *     
     */
    public DescribeLayoutButtonSection getButtonLayoutSection() {
        return buttonLayoutSection;
    }

    /**
     * Sets the value of the buttonLayoutSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescribeLayoutButtonSection }
     *     
     */
    public void setButtonLayoutSection(DescribeLayoutButtonSection value) {
        this.buttonLayoutSection = value;
    }

    /**
     * Gets the value of the detailLayoutSections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detailLayoutSections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetailLayoutSections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeLayoutSection }
     * 
     * 
     */
    public List<DescribeLayoutSection> getDetailLayoutSections() {
        if (detailLayoutSections == null) {
            detailLayoutSections = new ArrayList<DescribeLayoutSection>();
        }
        return this.detailLayoutSections;
    }

    /**
     * Gets the value of the editLayoutSections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the editLayoutSections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEditLayoutSections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeLayoutSection }
     * 
     * 
     */
    public List<DescribeLayoutSection> getEditLayoutSections() {
        if (editLayoutSections == null) {
            editLayoutSections = new ArrayList<DescribeLayoutSection>();
        }
        return this.editLayoutSections;
    }

    /**
     * Gets the value of the feedView property.
     * 
     * @return
     *     possible object is
     *     {@link DescribeLayoutFeedView }
     *     
     */
    public DescribeLayoutFeedView getFeedView() {
        return feedView;
    }

    /**
     * Sets the value of the feedView property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescribeLayoutFeedView }
     *     
     */
    public void setFeedView(DescribeLayoutFeedView value) {
        this.feedView = value;
    }

    /**
     * Gets the value of the highlightsPanelLayoutSection property.
     * 
     * @return
     *     possible object is
     *     {@link DescribeLayoutSection }
     *     
     */
    public DescribeLayoutSection getHighlightsPanelLayoutSection() {
        return highlightsPanelLayoutSection;
    }

    /**
     * Sets the value of the highlightsPanelLayoutSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescribeLayoutSection }
     *     
     */
    public void setHighlightsPanelLayoutSection(DescribeLayoutSection value) {
        this.highlightsPanelLayoutSection = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the multirowEditLayoutSections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the multirowEditLayoutSections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMultirowEditLayoutSections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeLayoutSection }
     * 
     * 
     */
    public List<DescribeLayoutSection> getMultirowEditLayoutSections() {
        if (multirowEditLayoutSections == null) {
            multirowEditLayoutSections = new ArrayList<DescribeLayoutSection>();
        }
        return this.multirowEditLayoutSections;
    }

    /**
     * Gets the value of the offlineLinks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offlineLinks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfflineLinks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfflineLink }
     * 
     * 
     */
    public List<OfflineLink> getOfflineLinks() {
        if (offlineLinks == null) {
            offlineLinks = new ArrayList<OfflineLink>();
        }
        return this.offlineLinks;
    }

    /**
     * Gets the value of the quickActionList property.
     * 
     * @return
     *     possible object is
     *     {@link DescribeQuickActionListResult }
     *     
     */
    public DescribeQuickActionListResult getQuickActionList() {
        return quickActionList;
    }

    /**
     * Sets the value of the quickActionList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescribeQuickActionListResult }
     *     
     */
    public void setQuickActionList(DescribeQuickActionListResult value) {
        this.quickActionList = value;
    }

    /**
     * Gets the value of the relatedContent property.
     * 
     * @return
     *     possible object is
     *     {@link RelatedContent }
     *     
     */
    public RelatedContent getRelatedContent() {
        return relatedContent;
    }

    /**
     * Sets the value of the relatedContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelatedContent }
     *     
     */
    public void setRelatedContent(RelatedContent value) {
        this.relatedContent = value;
    }

    /**
     * Gets the value of the relatedLists property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedLists property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedLists().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatedList }
     * 
     * 
     */
    public List<RelatedList> getRelatedLists() {
        if (relatedLists == null) {
            relatedLists = new ArrayList<RelatedList>();
        }
        return this.relatedLists;
    }

    /**
     * Gets the value of the saveOptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the saveOptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSaveOptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescribeLayoutSaveOption }
     * 
     * 
     */
    public List<DescribeLayoutSaveOption> getSaveOptions() {
        if (saveOptions == null) {
            saveOptions = new ArrayList<DescribeLayoutSaveOption>();
        }
        return this.saveOptions;
    }

}
