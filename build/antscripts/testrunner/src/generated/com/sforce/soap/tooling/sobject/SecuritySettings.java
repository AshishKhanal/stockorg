
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SecuritySettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SecuritySettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FullName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metadata" type="{urn:metadata.tooling.soap.sforce.com}SecuritySettings" minOccurs="0"/&gt;
 *         &lt;element name="NetworkAccess" type="{urn:sobject.tooling.soap.sforce.com}NetworkAccess" minOccurs="0"/&gt;
 *         &lt;element name="NetworkAccessId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PasswordPolicies" type="{urn:sobject.tooling.soap.sforce.com}PasswordPolicies" minOccurs="0"/&gt;
 *         &lt;element name="PasswordPoliciesId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SessionSettings" type="{urn:sobject.tooling.soap.sforce.com}SessionSettings" minOccurs="0"/&gt;
 *         &lt;element name="SessionSettingsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SecuritySettings", propOrder = {
    "durableId",
    "fullName",
    "metadata",
    "networkAccess",
    "networkAccessId",
    "passwordPolicies",
    "passwordPoliciesId",
    "sessionSettings",
    "sessionSettingsId"
})
public class SecuritySettings
    extends SObject
{

    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "FullName", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fullName;
    @XmlElementRef(name = "Metadata", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<com.sforce.soap.tooling.metadata.SecuritySettings> metadata;
    @XmlElementRef(name = "NetworkAccess", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<NetworkAccess> networkAccess;
    @XmlElementRef(name = "NetworkAccessId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkAccessId;
    @XmlElementRef(name = "PasswordPolicies", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<PasswordPolicies> passwordPolicies;
    @XmlElementRef(name = "PasswordPoliciesId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> passwordPoliciesId;
    @XmlElementRef(name = "SessionSettings", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SessionSettings> sessionSettings;
    @XmlElementRef(name = "SessionSettingsId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sessionSettingsId;

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFullName(JAXBElement<String> value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.SecuritySettings }{@code >}
     *     
     */
    public JAXBElement<com.sforce.soap.tooling.metadata.SecuritySettings> getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.sforce.soap.tooling.metadata.SecuritySettings }{@code >}
     *     
     */
    public void setMetadata(JAXBElement<com.sforce.soap.tooling.metadata.SecuritySettings> value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the networkAccess property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link NetworkAccess }{@code >}
     *     
     */
    public JAXBElement<NetworkAccess> getNetworkAccess() {
        return networkAccess;
    }

    /**
     * Sets the value of the networkAccess property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link NetworkAccess }{@code >}
     *     
     */
    public void setNetworkAccess(JAXBElement<NetworkAccess> value) {
        this.networkAccess = value;
    }

    /**
     * Gets the value of the networkAccessId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkAccessId() {
        return networkAccessId;
    }

    /**
     * Sets the value of the networkAccessId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkAccessId(JAXBElement<String> value) {
        this.networkAccessId = value;
    }

    /**
     * Gets the value of the passwordPolicies property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PasswordPolicies }{@code >}
     *     
     */
    public JAXBElement<PasswordPolicies> getPasswordPolicies() {
        return passwordPolicies;
    }

    /**
     * Sets the value of the passwordPolicies property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PasswordPolicies }{@code >}
     *     
     */
    public void setPasswordPolicies(JAXBElement<PasswordPolicies> value) {
        this.passwordPolicies = value;
    }

    /**
     * Gets the value of the passwordPoliciesId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPasswordPoliciesId() {
        return passwordPoliciesId;
    }

    /**
     * Sets the value of the passwordPoliciesId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPasswordPoliciesId(JAXBElement<String> value) {
        this.passwordPoliciesId = value;
    }

    /**
     * Gets the value of the sessionSettings property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SessionSettings }{@code >}
     *     
     */
    public JAXBElement<SessionSettings> getSessionSettings() {
        return sessionSettings;
    }

    /**
     * Sets the value of the sessionSettings property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SessionSettings }{@code >}
     *     
     */
    public void setSessionSettings(JAXBElement<SessionSettings> value) {
        this.sessionSettings = value;
    }

    /**
     * Gets the value of the sessionSettingsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSessionSettingsId() {
        return sessionSettingsId;
    }

    /**
     * Sets the value of the sessionSettingsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSessionSettingsId(JAXBElement<String> value) {
        this.sessionSettingsId = value;
    }

}
