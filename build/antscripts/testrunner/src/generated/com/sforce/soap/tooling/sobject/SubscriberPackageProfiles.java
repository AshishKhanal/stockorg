
package com.sforce.soap.tooling.sobject;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubscriberPackageProfiles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubscriberPackageProfiles"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="destinationProfiles" type="{urn:sobject.tooling.soap.sforce.com}SubscriberPackageDestinationProfile" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="sourceProfiles" type="{urn:sobject.tooling.soap.sforce.com}SubscriberPackageSourceProfile" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubscriberPackageProfiles", propOrder = {
    "destinationProfiles",
    "sourceProfiles"
})
public class SubscriberPackageProfiles {

    protected List<SubscriberPackageDestinationProfile> destinationProfiles;
    protected List<SubscriberPackageSourceProfile> sourceProfiles;

    /**
     * Gets the value of the destinationProfiles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinationProfiles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinationProfiles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriberPackageDestinationProfile }
     * 
     * 
     */
    public List<SubscriberPackageDestinationProfile> getDestinationProfiles() {
        if (destinationProfiles == null) {
            destinationProfiles = new ArrayList<SubscriberPackageDestinationProfile>();
        }
        return this.destinationProfiles;
    }

    /**
     * Gets the value of the sourceProfiles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sourceProfiles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSourceProfiles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubscriberPackageSourceProfile }
     * 
     * 
     */
    public List<SubscriberPackageSourceProfile> getSourceProfiles() {
        if (sourceProfiles == null) {
            sourceProfiles = new ArrayList<SubscriberPackageSourceProfile>();
        }
        return this.sourceProfiles;
    }

}
