
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomLinkComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomLinkComponent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:tooling.soap.sforce.com}DescribeLayoutComponent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customLink" type="{urn:tooling.soap.sforce.com}DescribeLayoutButton"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomLinkComponent", propOrder = {
    "customLink"
})
public class CustomLinkComponent
    extends DescribeLayoutComponent
{

    @XmlElement(required = true)
    protected DescribeLayoutButton customLink;

    /**
     * Gets the value of the customLink property.
     * 
     * @return
     *     possible object is
     *     {@link DescribeLayoutButton }
     *     
     */
    public DescribeLayoutButton getCustomLink() {
        return customLink;
    }

    /**
     * Sets the value of the customLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link DescribeLayoutButton }
     *     
     */
    public void setCustomLink(DescribeLayoutButton value) {
        this.customLink = value;
    }

}
