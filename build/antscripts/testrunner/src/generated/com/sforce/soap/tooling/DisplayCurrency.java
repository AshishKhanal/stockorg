
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisplayCurrency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DisplayCurrency"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CORPORATE"/&gt;
 *     &lt;enumeration value="PERSONAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DisplayCurrency")
@XmlEnum
public enum DisplayCurrency {

    CORPORATE,
    PERSONAL;

    public String value() {
        return name();
    }

    public static DisplayCurrency fromValue(String v) {
        return valueOf(v);
    }

}
