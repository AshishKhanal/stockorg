
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FieldUpdateOperation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FieldUpdateOperation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Formula"/&gt;
 *     &lt;enumeration value="Literal"/&gt;
 *     &lt;enumeration value="Null"/&gt;
 *     &lt;enumeration value="NextValue"/&gt;
 *     &lt;enumeration value="PreviousValue"/&gt;
 *     &lt;enumeration value="LookupValue"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FieldUpdateOperation")
@XmlEnum
public enum FieldUpdateOperation {

    @XmlEnumValue("Formula")
    FORMULA("Formula"),
    @XmlEnumValue("Literal")
    LITERAL("Literal"),
    @XmlEnumValue("Null")
    NULL("Null"),
    @XmlEnumValue("NextValue")
    NEXT_VALUE("NextValue"),
    @XmlEnumValue("PreviousValue")
    PREVIOUS_VALUE("PreviousValue"),
    @XmlEnumValue("LookupValue")
    LOOKUP_VALUE("LookupValue");
    private final String value;

    FieldUpdateOperation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FieldUpdateOperation fromValue(String v) {
        for (FieldUpdateOperation c: FieldUpdateOperation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
