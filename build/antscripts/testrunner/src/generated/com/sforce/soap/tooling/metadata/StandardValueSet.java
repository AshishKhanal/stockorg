
package com.sforce.soap.tooling.metadata;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardValueSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardValueSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:metadata.tooling.soap.sforce.com}Metadata"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sorted" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="standardValue" type="{urn:metadata.tooling.soap.sforce.com}StandardValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardValueSet", propOrder = {
    "sorted",
    "standardValue"
})
public class StandardValueSet
    extends Metadata
{

    protected boolean sorted;
    protected List<StandardValue> standardValue;

    /**
     * Gets the value of the sorted property.
     * 
     */
    public boolean isSorted() {
        return sorted;
    }

    /**
     * Sets the value of the sorted property.
     * 
     */
    public void setSorted(boolean value) {
        this.sorted = value;
    }

    /**
     * Gets the value of the standardValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the standardValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStandardValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StandardValue }
     * 
     * 
     */
    public List<StandardValue> getStandardValue() {
        if (standardValue == null) {
            standardValue = new ArrayList<StandardValue>();
        }
        return this.standardValue;
    }

}
