
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApexDebuggerLicenseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ApexDebuggerLicenseType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Self"/&gt;
 *     &lt;enumeration value="Sandbox"/&gt;
 *     &lt;enumeration value="LMO"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ApexDebuggerLicenseType")
@XmlEnum
public enum ApexDebuggerLicenseType {

    @XmlEnumValue("Self")
    SELF("Self"),
    @XmlEnumValue("Sandbox")
    SANDBOX("Sandbox"),
    LMO("LMO");
    private final String value;

    ApexDebuggerLicenseType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApexDebuggerLicenseType fromValue(String v) {
        for (ApexDebuggerLicenseType c: ApexDebuggerLicenseType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
