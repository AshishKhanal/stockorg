
package com.sforce.soap.tooling.sobject;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailToCaseSettings complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmailToCaseSettings"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:sobject.tooling.soap.sforce.com}sObject"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DurableId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsEmailToCaseEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsHtmlEmailEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsOnDemandEmailToCaseEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsThreadIdInBodyEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="IsThreadIdInSubjectEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="NotifyOwnerOnNewCaseEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="OverEmailLimitAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PreQuoteSignature" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="UnauthorizedSenderAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailToCaseSettings", propOrder = {
    "durableId",
    "isEmailToCaseEnabled",
    "isHtmlEmailEnabled",
    "isOnDemandEmailToCaseEnabled",
    "isThreadIdInBodyEnabled",
    "isThreadIdInSubjectEnabled",
    "notifyOwnerOnNewCaseEmail",
    "overEmailLimitAction",
    "preQuoteSignature",
    "unauthorizedSenderAction"
})
public class EmailToCaseSettings
    extends SObject
{

    @XmlElementRef(name = "DurableId", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> durableId;
    @XmlElementRef(name = "IsEmailToCaseEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isEmailToCaseEnabled;
    @XmlElementRef(name = "IsHtmlEmailEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isHtmlEmailEnabled;
    @XmlElementRef(name = "IsOnDemandEmailToCaseEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isOnDemandEmailToCaseEnabled;
    @XmlElementRef(name = "IsThreadIdInBodyEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isThreadIdInBodyEnabled;
    @XmlElementRef(name = "IsThreadIdInSubjectEnabled", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isThreadIdInSubjectEnabled;
    @XmlElementRef(name = "NotifyOwnerOnNewCaseEmail", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> notifyOwnerOnNewCaseEmail;
    @XmlElementRef(name = "OverEmailLimitAction", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> overEmailLimitAction;
    @XmlElementRef(name = "PreQuoteSignature", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> preQuoteSignature;
    @XmlElementRef(name = "UnauthorizedSenderAction", namespace = "urn:sobject.tooling.soap.sforce.com", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unauthorizedSenderAction;

    /**
     * Gets the value of the durableId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDurableId() {
        return durableId;
    }

    /**
     * Sets the value of the durableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDurableId(JAXBElement<String> value) {
        this.durableId = value;
    }

    /**
     * Gets the value of the isEmailToCaseEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsEmailToCaseEnabled() {
        return isEmailToCaseEnabled;
    }

    /**
     * Sets the value of the isEmailToCaseEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsEmailToCaseEnabled(JAXBElement<Boolean> value) {
        this.isEmailToCaseEnabled = value;
    }

    /**
     * Gets the value of the isHtmlEmailEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsHtmlEmailEnabled() {
        return isHtmlEmailEnabled;
    }

    /**
     * Sets the value of the isHtmlEmailEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsHtmlEmailEnabled(JAXBElement<Boolean> value) {
        this.isHtmlEmailEnabled = value;
    }

    /**
     * Gets the value of the isOnDemandEmailToCaseEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsOnDemandEmailToCaseEnabled() {
        return isOnDemandEmailToCaseEnabled;
    }

    /**
     * Sets the value of the isOnDemandEmailToCaseEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsOnDemandEmailToCaseEnabled(JAXBElement<Boolean> value) {
        this.isOnDemandEmailToCaseEnabled = value;
    }

    /**
     * Gets the value of the isThreadIdInBodyEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsThreadIdInBodyEnabled() {
        return isThreadIdInBodyEnabled;
    }

    /**
     * Sets the value of the isThreadIdInBodyEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsThreadIdInBodyEnabled(JAXBElement<Boolean> value) {
        this.isThreadIdInBodyEnabled = value;
    }

    /**
     * Gets the value of the isThreadIdInSubjectEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsThreadIdInSubjectEnabled() {
        return isThreadIdInSubjectEnabled;
    }

    /**
     * Sets the value of the isThreadIdInSubjectEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsThreadIdInSubjectEnabled(JAXBElement<Boolean> value) {
        this.isThreadIdInSubjectEnabled = value;
    }

    /**
     * Gets the value of the notifyOwnerOnNewCaseEmail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNotifyOwnerOnNewCaseEmail() {
        return notifyOwnerOnNewCaseEmail;
    }

    /**
     * Sets the value of the notifyOwnerOnNewCaseEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNotifyOwnerOnNewCaseEmail(JAXBElement<Boolean> value) {
        this.notifyOwnerOnNewCaseEmail = value;
    }

    /**
     * Gets the value of the overEmailLimitAction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOverEmailLimitAction() {
        return overEmailLimitAction;
    }

    /**
     * Sets the value of the overEmailLimitAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOverEmailLimitAction(JAXBElement<String> value) {
        this.overEmailLimitAction = value;
    }

    /**
     * Gets the value of the preQuoteSignature property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getPreQuoteSignature() {
        return preQuoteSignature;
    }

    /**
     * Sets the value of the preQuoteSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setPreQuoteSignature(JAXBElement<Boolean> value) {
        this.preQuoteSignature = value;
    }

    /**
     * Gets the value of the unauthorizedSenderAction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUnauthorizedSenderAction() {
        return unauthorizedSenderAction;
    }

    /**
     * Sets the value of the unauthorizedSenderAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUnauthorizedSenderAction(JAXBElement<String> value) {
        this.unauthorizedSenderAction = value;
    }

}
