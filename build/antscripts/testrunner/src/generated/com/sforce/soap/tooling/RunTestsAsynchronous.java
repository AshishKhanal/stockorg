
package com.sforce.soap.tooling;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="classids" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="suiteids" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="maxFailedTests" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="testLevel" type="{urn:tooling.soap.sforce.com}TestLevel"/&gt;
 *         &lt;element name="classNames" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="suiteNames" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "classids",
    "suiteids",
    "maxFailedTests",
    "testLevel",
    "classNames",
    "suiteNames"
})
@XmlRootElement(name = "runTestsAsynchronous")
public class RunTestsAsynchronous {

    @XmlElement(required = true)
    protected String classids;
    @XmlElement(required = true)
    protected String suiteids;
    protected int maxFailedTests;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TestLevel testLevel;
    @XmlElement(required = true)
    protected String classNames;
    @XmlElement(required = true)
    protected String suiteNames;

    /**
     * Gets the value of the classids property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassids() {
        return classids;
    }

    /**
     * Sets the value of the classids property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassids(String value) {
        this.classids = value;
    }

    /**
     * Gets the value of the suiteids property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuiteids() {
        return suiteids;
    }

    /**
     * Sets the value of the suiteids property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuiteids(String value) {
        this.suiteids = value;
    }

    /**
     * Gets the value of the maxFailedTests property.
     * 
     */
    public int getMaxFailedTests() {
        return maxFailedTests;
    }

    /**
     * Sets the value of the maxFailedTests property.
     * 
     */
    public void setMaxFailedTests(int value) {
        this.maxFailedTests = value;
    }

    /**
     * Gets the value of the testLevel property.
     * 
     * @return
     *     possible object is
     *     {@link TestLevel }
     *     
     */
    public TestLevel getTestLevel() {
        return testLevel;
    }

    /**
     * Sets the value of the testLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link TestLevel }
     *     
     */
    public void setTestLevel(TestLevel value) {
        this.testLevel = value;
    }

    /**
     * Gets the value of the classNames property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassNames() {
        return classNames;
    }

    /**
     * Sets the value of the classNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassNames(String value) {
        this.classNames = value;
    }

    /**
     * Gets the value of the suiteNames property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuiteNames() {
        return suiteNames;
    }

    /**
     * Sets the value of the suiteNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuiteNames(String value) {
        this.suiteNames = value;
    }

}
