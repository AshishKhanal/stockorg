package SFTests;


import com.sforce.soap.tooling.LoginResult;
import com.sforce.soap.tooling.SforceServicePortType;
import com.sforce.soap.tooling.SforceServiceService;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

import java.io.*;
import java.util.*;


/**
 * Main Engine for executing and fetching data from SF.
 *
 */
public class App 
{
    final static String ENDPOINT_URL = "https://" + System.getProperty("sf.domain") + "/services/data/v39.0/tooling";
    private static final String RESULT_FILE = System.getProperty("sf.resultpath");
    private static final Integer TESTS_PER_REQUEST = 1;

    public String sessionId;

    /**
     * logs into sf org using soap api to get the
     * @param username
     * @param password
     * @return
     */
    public String login(String username, String password){
        SforceServicePortType service = new SforceServiceService().getSforceService();


        LoginResult result = service.login(username, password);

        sessionId = result.getSessionId();
        return sessionId;
    }


    /**
     * queries the salesforce to get the map of test classes keyed by the id
     * @return
     */
    public Map<String, String> getClassNames(){

        Map<String, String> classIds = new HashMap<String, String>();

        try{
            // Note: SQX_Test is common prefix for all CQ related tests.
            // TODO: parse package.xml and identify the names of test instead of relying in convention.
            String resp = querySF("SELECT ID,Name,NamespacePrefix  FROM ApexClass WHERE Name Like 'SQX%Test%'");

            System.out.println("Got response" + resp.substring(0, resp.length() > 50 ? 50 : resp.length()));
            JSONObject response = new JSONObject(resp);
            JSONArray classes = response.getJSONArray("records");

            for (int index = 0; index < classes.length(); index++) {
                JSONObject klass = classes.getJSONObject(index);
                String prefix = "";

                    if(klass.get("NamespacePrefix") != null){
                        prefix = klass.getString("NamespacePrefix") + ".";
                    }

                classIds.put(klass.getString("Id"), prefix + klass.getString("Name"));
            }

            System.out.println("Got response for the query");
        }
        catch (Exception ex){
            System.out.println("Error occurred " + ex);
        }

        return classIds;
    }

    /**
     * queries the salesforce for a particular SOQL
     * @param query SOQL statement
     * @return JSON String returned from SF
     * @throws IOException
     */
    private String querySF(String query) throws IOException {
        System.out.println("Querying: " + query);
        return requestSF("/query", new NameValuePair[]{
                new NameValuePair("q", query)
        });
    }

    /**
     *
     * @param path
     * @param stringBody
     * @return
     * @throws IOException
     */
    private String postRequestSF(String path, String stringBody) throws IOException {
        HttpClient client = new HttpClient();

        PostMethod post = new PostMethod(ENDPOINT_URL + path);
        RequestEntity req = new StringRequestEntity(stringBody, "application/json", "UTF-8");
        post.setRequestEntity(req);

        post.setRequestHeader("Authorization", "Bearer " + this.sessionId);
        post.setRequestHeader("Content-Type", "application/json");

        client.executeMethod(post);

        return post.getResponseBodyAsString();
    }

    /**
     * Sends a http call to a particular sf path specified by the path param
     * @param path the path to call
     * @param queryParams the query parameters to send in the get call
     * @return
     * @throws IOException
     */
    private String requestSF(String path, NameValuePair[] queryParams) throws IOException{
        HttpClient client = new HttpClient();

        GetMethod get = new GetMethod(ENDPOINT_URL + path);

        get.setQueryString(queryParams);

        get.setRequestHeader("Authorization", "Bearer " + this.sessionId);
        get.setRequestHeader("Content-Type", "application/json");



        client.executeMethod(get);
        return get.getResponseBodyAsString();
    }

    /**
     * Executes the test
     * @param testsNamesByIds
     */
    public void executeTest(Map<String, String> testsNamesByIds) throws IOException {

        List<String> testIds = new ArrayList<String>(testsNamesByIds.keySet());

        lastTestResult = null;
        //submit five tests at a time, should be sufficiently
        int index = 0;
        while(index < testIds.size()){
            System.out.println("Running test " + testsNamesByIds.get(testIds.get(index)));
            int toIndex = index + TESTS_PER_REQUEST;
            if(toIndex > testIds.size())
                toIndex = testIds.size();

            List<String> subString = testIds.subList(index, toIndex);

	        //HACK: for some reason SF returns null randomly, so we run the tests again
            Integer retryCount = 0;
            final Integer MAX_RETRIES = 10;
            JSONObject subObject = null;
            do {
                try {

                    subObject = executeTestInSF(subString);
                } catch (JSONException ex) {
                    subObject = null;
                }

                retryCount++;

            }while(retryCount < MAX_RETRIES && subObject == null);


            if(lastTestResult == null)
                lastTestResult = subObject; //use the first result as the basis for combination
            else
                combineTestResult(subObject);



            index += TESTS_PER_REQUEST;
        }


        addDataOfUncoveredClasses();
        lastTestResult.put("runOn", new Date());

    }

    /**
     * Useful for combining sf results which we obtain in piece meal manner :(
     * @param subObject the object to copy
     */
    public void combineTestResult(JSONObject subObject) {
        JSONObject mainResult = lastTestResult;

        //combine successes section
        JSONArray mainSuccess = mainResult.getJSONArray("successes");
        JSONArray subSuccess = subObject.getJSONArray("successes");
        for (int index = 0; index < subSuccess.length(); index++) {
            mainSuccess.put(subSuccess.get(index));
        }

        //combine failure section
        JSONArray mainFailure = mainResult.getJSONArray("failures");
        JSONArray subFailure = subObject.getJSONArray("failures");
        for (int index = 0; index < subFailure.length(); index++) {
            mainFailure.put(subFailure.get(index));
        }

        double totalTimeMain = mainResult.getDouble("totalTime") + subObject.getDouble("totalTime");
        mainResult.put("totalTime", totalTimeMain);

        //combine code coverage information
        JSONArray mainCoverage = mainResult.getJSONArray("codeCoverage");
        JSONArray subCoverage = subObject.getJSONArray("codeCoverage");
        Map<String, JSONObject> coverageByClass = new HashMap<String, JSONObject>();
        //create hash map for quicker access later on, we might have to cache this because we will be
        //creating this map repeatedly for each iteration
        //this is the least of concern for this app
        for (int index = 0; index < mainCoverage.length(); index++) {
            JSONObject classCoverage = mainCoverage.getJSONObject(index);
            coverageByClass.put(classCoverage.getString("namespace") + "." + classCoverage.getString("name"),
                    classCoverage);
        }

        for (int index = 0; index < subCoverage.length(); index++) {
            JSONObject classCoverage = subCoverage.getJSONObject(index);
            String key = classCoverage.getString("namespace") + "." + classCoverage.getString("name");
            if(coverageByClass.containsKey(key)){
                //we might have to mix the two reports
                JSONObject mainCoverageData = coverageByClass.get(key);
                int newlyCoveredLocations = 0;

                Set<Integer> linesNotCoveredBySub = new HashSet<Integer>();
                JSONArray notCoveredBySub = classCoverage.getJSONArray("locationsNotCovered");

                //create a set of lines not covered by sub coverage.
                for(int row = 0; row < notCoveredBySub.length(); row++){
                    linesNotCoveredBySub.add(notCoveredBySub.getJSONObject(row).getInt("line"));
                }

                //remove any lines that have been covered by sub i.e. not in the linesNotCoveredBySub list
                JSONArray notCoveredByMain = mainCoverageData.getJSONArray("locationsNotCovered");
                for(int row = notCoveredByMain.length() - 1; row >= 0 ; row--){
                    JSONObject line = notCoveredByMain.getJSONObject(row);
                    int lineNum = line.getInt("line");
                    if(!linesNotCoveredBySub.contains(lineNum)){
                        //if the list of newly not covered lines doesn't contain the line we can remove this
                        newlyCoveredLocations++;
                        notCoveredByMain.remove(row);
                    }
                }



                //update numberOflocations not covered
                int numLocationsNotCovered = mainCoverageData.getInt("numLocationsNotCovered") - newlyCoveredLocations;
                mainCoverageData.put("numLocationsNotCovered", numLocationsNotCovered);
            }
            else{
                //life is a lot easier, just add the coverage data to the main object
                mainCoverage.put(classCoverage);
            }
        }


        //add num of tests run and num of failures
        int numTestsRun = mainResult.getInt("numTestsRun") + subObject.getInt("numTestsRun");
        mainResult.put("numTestsRun", numTestsRun);

        int numFailures = mainResult.getInt("numFailures") + subObject.getInt("numFailures");
        mainResult.put("numFailures", numFailures);


    }

    /**
     * Executing test in salesforce internally
     * @param testIds
     * @return
     */
    private JSONObject executeTestInSF(List<String> testIds){
        JSONObject testRequest = new JSONObject();
        //TODO: Decide if we want to fail immediately instead of running all tests in case of failure
        testRequest.put("maxFailedTests", -1);

        JSONArray testsToRun = new JSONArray();

        for(String testId : testIds){
            JSONObject obj = new JSONObject();
            obj.put("classId", testId);
            testsToRun.put(obj);
        }

        testRequest.put("tests", testsToRun);

        JSONObject obj = null;

        try {


            String response = postRequestSF("/runTestsSynchronous", testRequest.toString());


            System.out.println("Response for test poll " + response.substring(0, response.length() > 50 ? 50 :  response.length()));
            
	    obj  = new JSONObject(response);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return  obj;
    }

    public JSONObject lastTestResult;

    /**
     * Stores the generated coverage data returned from sf into a file
     */
    public void generateCoverageData() {
        try {
            PrintWriter writer = new PrintWriter(RESULT_FILE, "UTF-8");

            writer.print(lastTestResult.toString());
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method ensures that optimum code coverage is present in the code
     * @param log the log container where log will be appended
     * @return true if optimum coverage is found
     */
    public boolean ensureOptimumCodeCoverage(StringBuilder log) {
        final float OPTIMUM_COVERAGE_LEVEL = Float.parseFloat(System.getProperty("optimumCoverage"));
        float totalLines = 0, totalNotCovered = 0, percentCovered = 0;

        //TODO: find a way to run the test in CI environment as a whole instead of piece meal mode
        //running piece meal fashion always adds a coverage warning. So, this test will fail
        //JSONArray codeCoverageWarnings = lastTestResult.getJSONArray("codeCoverageWarnings");
        boolean result = true;

        //if(codeCoverageWarnings.length() > 0){
        //    result = false; //any warning means failure, might have to consider this
        //    log.append("Received warning:\n" + codeCoverageWarnings.toString(4));
        //}

        JSONArray coverage = lastTestResult.getJSONArray("codeCoverage");
        for (int i = 0; i < coverage.length(); i++) {
            JSONObject classCoverage = coverage.getJSONObject(i);
            totalLines += classCoverage.getInt("numLocations");
            totalNotCovered += classCoverage.getInt("numLocationsNotCovered");
        }

        percentCovered = (totalLines - totalNotCovered) / totalLines;
        result = result && (percentCovered > OPTIMUM_COVERAGE_LEVEL);

        System.out.println("Found " + totalNotCovered + " not covered out of " + totalLines + ". Coverage: " + percentCovered);
        log.append("Code coverage level is " + percentCovered);

        return result;
    }


    /**
     * Ensures that there was no test failures when running the test
     * @param log the log container where log will be appended
     * @return true if there are no failures
     */
    public boolean ensureNoFailures(StringBuilder log) {
        JSONArray failures = lastTestResult.getJSONArray("failures");
        int numTestsRun = lastTestResult.getInt("numTestsRun");
        int numFailures = lastTestResult.getInt("numFailures");
        boolean result = numFailures == 0;

        System.out.println("Number of tests run: " + numTestsRun + ", Total failures: " + numFailures);
        log.append("Following failures where found:\n" + failures.toString());

        return result;
    }


    /**
     * This method queries SF to get information about all the classes and adds them to the result if
     * they weren't covered by any tests. All uncovered objects have uncovered key set to true.
     * @throws IOException
     */
    public void addDataOfUncoveredClasses() throws IOException {
        JSONObject allClasses = new JSONObject(querySF("SELECT ApexClassOrTrigger.Name, NumLinesCovered, NumLinesUncovered FROM ApexCodeCoverageAggregate WHERE ApexClassOrTrigger.Name LIKE 'SQX%' ORDER BY ApexClassOrTrigger.Name ASC"));

        JSONArray classOrTriggers = allClasses.getJSONArray("records");

        Set<String> allPresentClasses = new HashSet<String>();
        JSONArray allClassesRun = lastTestResult.getJSONArray("codeCoverage");
        for (int index = 0; index < allClassesRun.length(); index++) {
            allPresentClasses.add(allClassesRun.getJSONObject(index).getString("name"));
        }

        for (int index = 0; index < classOrTriggers.length(); index++) {
            JSONObject obj = classOrTriggers.getJSONObject(index);
            JSONObject apexTrigger = obj.getJSONObject("ApexClassOrTrigger");
            JSONObject apexTriggerAttr = apexTrigger.getJSONObject("attributes");
            String type = apexTriggerAttr.getString("url");
            String name = apexTrigger.getString("Name");

            if(!type.contains("ApexTrigger")){
                //ignoring apex trigger for now we are only concerned with class coverage

                if(!allPresentClasses.contains(name)){
                    JSONObject uncovered = new JSONObject();
                    uncovered.put("name", name);
                    uncovered.put("namespace", "uncovered"); //HACK need to find the exact class
                    uncovered.put("locationsNotCovered", new JSONArray());
                    uncovered.put("uncovered", true);
                    int numLinesCovered = obj.getInt("NumLinesCovered");
                    int numLinesNotCovered = obj.getInt("NumLinesUncovered");

                    uncovered.put("numLocations", numLinesCovered + numLinesNotCovered );
                    uncovered.put("numLocationsNotCovered", numLinesNotCovered );
                    allClassesRun.put(uncovered);
                }
            }
        }
    }

}


/*
    public static void main( String[] args )
    {


        /*
        //TODO: use SOAP api so that the responses are strongly typed.
        SessionHeader s = new SessionHeader();
        s.setSessionId(result.getSessionId());

        BindingProvider bp = (BindingProvider)service;
        ArrayList<Header> headers = new ArrayList<Header>(1);

        SoapHeader header = new SoapHeader(QName.valueOf("SessionHeader"), s);

        headers.add(header);

        bp.getRequestContext().put(Header.HEADER_LIST, headers);


        System.out.println(service.getUserInfo().getUserFullName());

        */

/*
        if(classNames.length() > 0) {

            System.out.println("Testing classes " + classNames);

            get = new GetMethod(ENDPOINT_URL + "/runTestsSynchronous");

            queryParams = new NameValuePair[]{
                    new NameValuePair("classnames", classNames.toString())
            };
            get.setQueryString(queryParams);

            get.setRequestHeader("Authorization", "Bearer " + result.getSessionId());
            get.setRequestHeader("Content-Type", "application/json");

            try {
                System.out.print("Querying " + get.getURI().toString());

                int status = client.executeMethod(get);

                System.out.println(get.getResponseBodyAsString());
            } catch (Exception ex) {
                System.out.println("Following error occurred" + ex);
            }
        }
        else{
            System.out.println("Nothing to test");
        }
* /

}


public String startTest(Set<String> testClassIds) {
        StringBuilder params = new StringBuilder();
        for(String id : testClassIds){
        params.append(id);
        params.append(",");
        }

        try {
        String response = requestSF("/runTestsAsynchronous", new NameValuePair[]{
        new NameValuePair("classids", params.toString())
        });

        System.out.println("Question ID: " + response);
        return response.replace("\"", ""); //remove quotes from returned id.

        } catch (IOException e) {
        e.printStackTrace();
        }

        return null;
        }


public JSONArray getResultForTestRun(String testId, Map<String, String> classNames) throws TimeoutException, InterruptedException {
        JSONArray results = new JSONArray();
        waitForTestToComplete(testId);

        //get the result
        String query = "SELECT ApexClassId, Message,MethodName,Outcome,StackTrace FROM ApexTestResult WHERE AsyncApexJobId = '" + testId + "'";

        try {
        String testResult = querySF(query);
        System.out.println("Response for test poll " + testResult.substring(0, testResult.length() > 50 ? 50 :  testResult.length()));

        JSONObject response = new JSONObject(testResult);
        JSONArray array = response.getJSONArray("records");

        for(Integer index = 0; index < array.length(); index++){
        JSONObject obj = array.getJSONObject(index);
        if(!obj.getString("Outcome").equals("Pass")){
        obj.put("ClassName", classNames.get(obj.getString("ApexClassId")));
        results.put(obj);
        }
        }

        } catch (IOException e) {
        e.printStackTrace();
        }

        return results;
        }

private void waitForTestToComplete(String testId) throws TimeoutException, InterruptedException{
final int   MAX_POLLING = 50,  //maximum number of polling attempts before failure is signaled
        POLL_DURATION = 30; //poll duration in seconds
        // MAX_POLLING * POLL_DURATION is max wait time, so above case we wait for 25 minutes then fail
        int numberOfPolls = 0;
        Boolean testRunCompleted = false;
        String query = "SELECT ApexClassId,ExtendedStatus,Id,JobItemsProcessed,JobType,MethodName,NumberOfErrors,Status FROM AsyncApexJob WHERE Id = '" + testId + "'";
        do{
        try {
        String testResponse = querySF(query);

        System.out.println("Response for test poll " + testResponse.substring(0, testResponse.length() > 50 ? 50 :  testResponse.length()));
        JSONObject response = new JSONObject(testResponse);
        JSONObject queueItem = response.getJSONArray("records").getJSONObject(0);
        System.out.println(queueItem);
        testRunCompleted = queueItem.getString("Status").equals("Completed");
        System.out.println("Test Run Completed: " + testRunCompleted);

        if(!testRunCompleted)
        Thread.sleep(POLL_DURATION * 1000);


        } catch (IOException e) {
        e.printStackTrace();
        }

        numberOfPolls++;
        }while(!testRunCompleted && numberOfPolls < MAX_POLLING );

        if(!testRunCompleted)
        throw new TimeoutException("Time out occurred, it shouldn't take so long"); //if test didn't complete throw timeout exception
        }

 */
