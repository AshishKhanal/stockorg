package SFTests;

import org.junit.Assert;
import org.junit.Before;

import java.util.ArrayList;
import java.util.Map;


/**
 * Unit test for simple App.
 */
public class AppTest
{
    private static final String USERNAME = System.getProperty("sf.username"); //continuous.integrator@e1.ci-04-01.cq.com
    private static final String PASSWORD = System.getProperty("sf.password"); //ambarkaar1SXYAaYMUPMf68FK8l58YsjVwq

    @Before
    public void setup() {

        this.app = new App();
        app.login(USERNAME, PASSWORD);
        this.classes = app.getClassNames();

    }

    private App app;
    private Map<String, String> classes;

    @org.junit.Test
    public void testTheParameter() throws Exception{

        app.executeTest(this.classes);

        app.generateCoverageData();

        StringBuilder log = new StringBuilder();
        Boolean result = app.ensureNoFailures(log);
        Assert.assertTrue("Ensure that there was no test failure.\n" + log.toString(), result);

        app.ensureOptimumCodeCoverage(log);
        Assert.assertTrue("Optimum amount of code coverage not found.\n" + log.toString(), result);


    }
}
