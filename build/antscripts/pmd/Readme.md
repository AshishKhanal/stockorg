# PMD Integration

![ComplianceQuest Logo](../resources/Compliance_Quest_Logo.png "ComplianceQuest Logo")
![PMD Logo](https://pmd.github.io/img/pmd_logo.png "Logo Title Text 1")

This folder contains the library and rulesets for PMD analysis of apex, javascript and visualforce pages.

More details about the standard rules and library can be found in [PMD Github](https://pmd.github.io/)