<project name="SQX" default="startpackaging"  basedir="." xmlns:if="ant:if" xmlns:unless="ant:unless">

    <property environment="env"/>
    <property name="deployToSF" value="true" />
    <property name="configBaseDir" value="config" />
	<property name="srcbasedir" value="${env.WORKSPACE}" />
    <property name="pmdCacheDir" value="${user.home}/.pmdcache" />

    <taskdef resource="net/sf/antcontrib/antcontrib.properties"/>
    <import file="${basedir}/findDescription.build.xml"/>
    <path id="pmd.classpath">
        <pathelement location="./pmd/lib"/>
        <fileset dir="./pmd/lib">
            <include name="*.jar"/>
        </fileset>
    </path>

    <taskdef name="pmd" classname="net.sourceforge.pmd.ant.PMDTask" classpathref="pmd.classpath"/>

    <property name="buildscriptdir" value="${user.dir}"/>
    <condition property="isWindows">
        <os family="windows" />
    </condition>
    <condition property="isUnix">
        <os family="unix" />
    </condition>

    <target name="initializeproperties">
        <fail message="Property &quot;dep.sf.username&quot; needs to be set" unless="dep.sf.username"/>
        <!-- <fail message="Property &quot;dep.sf.password&quot; needs to be set" unless="dep.sf.password"/> -->

        <propertyregex property="configdir"
          input="${dep.sf.username}"
          regexp=".*@(.*)"
          select="\1" />

        <echo message="Loading project configurations from ${configBaseDir}/${configdir}/project.build.properties" />
        <loadproperties srcfile="${configBaseDir}/default/project.build.properties" />
        <loadproperties srcfile="${configBaseDir}/${configdir}/project.build.properties" />

        <!-- Dynamically setting CQ Regulatory Submission Export Service credentials
             We have different credentials for production and development/testing orgs -->
        <if>
            <equals arg1="${packagingOrg}" arg2="true" />
            <then>
                <property name="exportServiceEndpoint" value="${productionExportServiceEndpoint}" />
                <property name="exportServiceUsername" value="${productionExportServiceUsername}" />
                <property name="exportServicePassword" value="${productionExportServicePassword}" />
            </then>
            <else>
                <property name="exportServiceEndpoint" value="${stagingExportServiceEndpoint}" />
                <property name="exportServiceUsername" value="${stagingExportServiceUsername}" />
                <property name="exportServicePassword" value="${stagingExportServicePassword}"/>
            </else>
        </if>

    </target>

    <target name="pmdanalysis">
        <property name="outputDirectory" value="./bin" />
        <!-- Check if output directory is present; if not creating one (to store the result of analysis) -->
        <!-- When building the project, the output directory will most likely be present -->
        <!-- However, when calling this task before committing, the directory might not be present -->
        <if>
            <available file="${outputDirectory}" type="dir" />
            <then>
                <echo message="Writing reports to ${outputDirectory}" />
            </then>
            <else>
                <mkdir dir="${outputDirectory}" />
            </else>
        </if>

        <echo message="Running default rulesets" />
        <pmd shortFilenames="true"
             failOnRuleViolation="false"
             cacheLocation="${pmdCacheDir}/pmd-apex-warn.cache">
            <sourceLanguage name="apex" version=""/>
            <ruleset>rulesets/apex/ruleset.xml</ruleset>
            <formatter type="summaryhtml" toFile="${outputDirectory}/pmd_apex_warning_report.html" />
            <fileset dir="${srcDir}/classes/">
                <include name="*.cls"/>
            </fileset>
        </pmd>

        <pmd shortFilenames="true"
             failOnRuleViolation="false"
             cacheLocation="${pmdCacheDir}/pmd-vf.cache">
            <sourceLanguage name="vf" version=""/>
            <ruleset>rulesets/vf/security.xml</ruleset>
            <formatter type="summaryhtml" toFile="${outputDirectory}/pmd_vf.html" />
            <fileset dir="${srcDir}/pages/">
                <include name="*.page"/>
            </fileset>
        </pmd>

        <!-- The following analysis will cause build to fail on rule violation -->
        <echo message="Running custom rulesets" />
        <pmd shortFilenames="true"
            rulesetfiles="pmd/custom-rulesets/apex/ruleset.xml"
            failOnRuleViolation="true"
            cacheLocation="${pmdCacheDir}/pmd-apex-fail.cache">
            <sourceLanguage name="apex" version=""/>
            <formatter type="text" toConsole="true" />
            <fileset dir="${srcDir}/classes/">
                <include name="*.cls"/>
                <excludesfile name="pmd/pmd-analysis-exclusion-list.txt"/>
            </fileset>
        </pmd>

    </target>

    <target name="startbuild" depends="initializeproperties">
        <antcall target="FindDesc" />
        <antcall target="packageresource" />
        <antcall target="externalpackagebuild" />
        <antcall target="combinebothpackages" />
	    <property name="sfcoderootdir" value="${buildscriptdir}/bin/combined" />
        <antcall target="pmdanalysis" />
        <property name="sf.allowmissingfiles" value="false" />
        <antcall target="deployToSF" />
    </target>

    <target name="startpackaging" depends="initializeproperties">
        <property name="sf.allowmissingfiles" value="true" />
        <antcall target="packageresource" />
        <antcall target="removecomponents" />
	    <property name="sfcoderootdir" value="${buildscriptdir}/bin/combined" />
        <antcall target="pmdanalysis" />
        <antcall target="deployToSF" />
    </target>

    <target name="removeConnectedApp">
        <delete>
            <fileset dir="${srcbasedir}/src/connectedApps/" includes="*/**" />
        </delete>
    </target>

    <target name="removalScriptLinux" if="isUnix">
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'node ./build/index.js --username ${dep.sf.username} --password ${dep.sf.password} --instanceUrl ${dep.sf.instanceUrl} --accessToken ${dep.sf.accessToken} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/' --removeonly true" />
        </exec>
    </target>

    <target name="removalScriptWindows" if="isWindows">
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'node ./build/index.js --username ${dep.sf.username} --password ${dep.sf.password} --instanceUrl ${dep.sf.instanceUrl} --accessToken ${dep.sf.accessToken} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/' --removeonly true" />
        </exec>
    </target>

    <target name="runRemovalScript" depends="removalScriptLinux,removalScriptWindows">
        <echo message="Done removing" />
    </target>

    <target name="removecomponents">
        <antcall target="runRemovalScript" />
        <antcall target="removeConnectedApp" xmlns:if="${deleteConnectedApp}"/>
    </target>

    <target name="runCombineScript" depends="combineScriptLinux,combineScriptWindows">
        <echo message="Done combining" />
    </target>

    <target name="combineScriptWindows" if="isWindows">
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'node ./build/index.js --username ${dep.sf.username} --password ${dep.sf.password} --instanceUrl ${dep.sf.instanceUrl} --accessToken ${dep.sf.accessToken} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/' " />
        </exec>
    </target>

    <target name="combineScriptLinux" if="isUnix">
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'node ./build/index.js --username ${dep.sf.username} --password ${dep.sf.password} --instanceUrl ${dep.sf.instanceUrl} --accessToken ${dep.sf.accessToken} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/' " />
        </exec>
    </target>

    <target name="combinebothpackages">
        <antcall target="runCombineScript" />
    </target>


    <!-- todo make build file dynamic right now it is static -->

    <!-- delete existing source and override with new set of files -->
    <target name="prepareOutputDirectory">

        <!-- recreate build directory to store the output, this is the package that will be uploaded to sf-->
        <delete dir="${outputDirectory}" />
        <mkdir dir="${outputDirectory}" />

        <copy todir="${outputDirectory}/src" >
            <fileset dir="${srcDir}" includes="${filesToInclude}" />
        </copy>

    </target>


    <!-- replaces the source namespace prefix in the containing js to the  desired namespace prefix -->
    <target name="copySrcAndReplaceNameSpace">

        <replace token="${srcNamespacePrefix}" value="${destNamespacePrefix}" summary="true">
            <fileset file="${outputDirectory}/src/" includes="*/**" />
        </replace>

        <replace token="${srcNamespacePrefixOnly}" value="${destNamespacePrefixOnly}" summary="true">
            <fileset file="${outputDirectory}/src/customMetadata" includes="*/**" />
        </replace>

        <replace token="${srcNamespacePrefixOnly}." value="${destNamespacePrefixOnly}." summary="true">
            <fileset file="${outputDirectory}/src/pages" includes="*/**" />
            <fileset file="${outputDirectory}/src/aura" includes="**/*.cmp" />
        </replace>

        <replace token="${srcNamespacePrefixOnly}:" value="${destNamespacePrefixOnly}:" summary="true">
            <fileset file="${outputDirectory}/src/pages" includes="*/**" />
            <fileset file="${outputDirectory}/src/components" includes="*/**" />
            <fileset file="${outputDirectory}/src/flows" includes="*/**" />
        </replace>


        <replace summary="true">
            <replaceFilter token="#DRAFT_LIBRARY_ID#" value="${draftLibraryId}" />
            <replaceFilter token="#RELEASE_LIBRARY_ID#" value="${releaseLibraryId}" />
            <replaceFilter token="#DISTRIBUTION_LIBRARY_ID#" value="${distributionLibaryId}" />
            <replaceFilter token="CALENDAR_ID_FOR_WITHOUT_FULL_ACCESS" value="${calendarWithoutFullAccess}" />
            <replaceFilter token="CALENDAR_ID_FOR_THIS_ORG" value="${calendarId}" />
            <replaceFilter token="##EASYPDF_CLIENTID##" value="${easypdfClientId}" />
            <replaceFilter token="##EASYPDF_CLIENTSECRET##" value="${easypdfClientSecret}" />
            <replaceFilter token="##EASYPDF_WORKFLOWID##" value="${easypdfWorkflowId}" />
            <replaceFilter token="##SCORM_APP_ID##" value="${scormAppId}" />
            <replaceFilter token="##SCORM_SECRET_KEY##" value="${scormSecretKey}" />
            <replaceFilter token="##PROXY_SERVER_URL##" value="${proxyServerUrl}" />
            <replaceFilter token="##PROXY_SERVER_PASSWORD##" value="${proxyServerPassword}" />
            <replaceFilter token="##PROXY_SERVER_USERNAME##" value="${proxyServerUsername}" />
            <replaceFilter token="##EXPORT_SERVICE_ENDPOINT##" value="${exportServiceEndpoint}" />
            <replaceFilter token="##EXPORT_SERVICE_BASIC_AUTH_USERNAME##" value="${exportServiceUsername}" />
            <replaceFilter token="##EXPORT_SERVICE_BASIC_AUTH_PASSWORD##" value="${exportServicePassword}" />
            <replaceFilter token="##PROXY_SERVER_STAGING_URL##" value="${proxyServerUrlStaging}" />
            <replaceFilter token="##PROXY_SERVER_STAGING_PASSWORD##" value="${proxyServerPasswordStaging}" />
            <replaceFilter token="##PROXY_SERVER_STAGING_USERNAME##" value="${proxyServerUsernameStaging}" />
            <replaceFilter token="##EXPORT_SERVICE_STAGING_ENDPOINT##" value="${stagingExportServiceEndpoint}" />
            <replaceFilter token="##EXPORT_SERVICE_BASIC_AUTH_STAGING_PASSWORD##" value="${stagingExportServicePassword}" />
            <replaceFilter token="##EXPORT_SERVICE_BASIC_AUTH_STAGING_USERNAME##" value="${stagingExportServiceUsername}" />

            <fileset file="${outputDirectory}/src/classes" includes="*/**" />
        </replace>

        <!-- replace namespace in package.xml-->
        <replace token="&lt;namespacePrefix&gt;${srcNamespacePrefixOnly}&lt;/namespacePrefix&gt;" value="&lt;namespacePrefix&gt;${destNamespacePrefixOnly}&lt;/namespacePrefix&gt;" summary="true">
            <fileset file="${outputDirectory}/src/package.xml" />
        </replace>
    </target>

    <target name="externalpackagebuild">
        <ant antfile="external.package.build.xml" target="packageresourcewithoutZip"></ant>
    </target>

    <target name="packageresource">

        <antcall target="prepareOutputDirectory">
            <param name="filesToInclude" value="**" />
        </antcall>

        <antcall target="copySrcAndReplaceNameSpace" />

        <ant antfile="resource.build.xml" target="packageresource">
        </ant>

        <move todir="${outputDirectory}/src/staticresources">
            <fileset dir="${resourceDirectory}" includes="*.resource" />
        </move>

        <antcall target="addTestRecords">
        </antcall>

        <ant antfile="resource.build.xml" target="cleanupResources">
        </ant>

        <antcall target="cleanup">
        </antcall>

    </target>

    <target name="addTestRecords" if="${addTestRecords}">

        <copy todir="${outputDirectory}/src/customMetadata">
            <fileset dir="${srcbasedir}/testRecords/customMetadata" includes="*.*" />
        </copy>

        <property name="defaultMetaDataRecord" value="&lt;members&gt;Secondary_Format_Setting.CQ_Landscape&lt;/members&gt;" />

        <loadfile property="testMetaDataRecords" srcFile="${srcbasedir}/testRecords/package.xml"/>

        <replace token="${defaultMetaDataRecord}" value="${testMetaDataRecords}" summary="true">
            <fileset file="${outputDirectory}/src/package.xml" />
        </replace>

    </target>

    <target name="deployToSF" if="${deployToSF}">
        <property name="removeCalendarTestMethods" value="false" />
        
        <property name="sf.targetDir" value="${buildscriptdir}/bin/combined" />
        <if>
            <equals arg1="${removeCalendarTestMethods}" arg2="true" />
            <then>
                <echo message="Removing calendar test methods from SQX_Test_1354_AddAuditEventinCalendar.cls" />
                <replace file="${sf.targetDir}/classes/SQX_Test_1354_AddAuditEventinCalendar.cls" token="testmethod void" value="void" summary="true" />
            </then>
        </if>
        <echo>Deploying to SF...</echo>
        <ant antfile="sf.deploy.build.xml" target="deploy"></ant>
    </target>

    <!-- DOPS-7 : Delta change validation task -->
    <target name="validateChangeset" depends="createChangeset">

        <!-- let's analyze the code before starting validation -->
        <antcall target="pmdanalysis" />

        <!-- Initiate vaildation -->
        <echo>Validating Changeset...</echo>
        <property name="sf.allowmissingfiles" value="true" />
        <property name="sf.runalltests" value="false" />
        <property name="sf.targetDir" value="${buildscriptdir}/bin/src" />
        <ant antfile="sf.deploy.build.xml" target="validate"></ant>
    </target>

    <!-- Task to create delta package -->
    <target name="createChangeset" depends="initializeproperties">
        <echo>Creating changeset package...</echo>

        <!-- get all changed files from 'src' directory -->
        <exec executable="git" outputproperty="changedFilesList" dir="${buildscriptdir}">
            <arg value="diff" />
            <arg value="${newCommit}" />
            <arg value="${lastCommit}" />
            <arg value="--name-only" />
            <arg value="--" />
            <arg value="${srcDir}" />    <!-- only the 'src' folder is considered for diff -->
        </exec>

         <!-- getting comma-separated value of files, also excluding 'src' folder path -->
        <script language="javascript">
        <![CDATA[
            var changedFilesList = project.getProperty("changedFilesList"),
                splitFilesList = changedFilesList.split("\n"),
                changedFiles = splitFilesList.join().replace(/src\//g, '');
            project.setProperty("count", splitFilesList.length);
            project.setProperty("changedFiles", changedFiles);
        ]]>
        </script>

        <echo message="${count} changes detected."/>

        <!-- copy those changed files into the folder that will be deployed -->
        <antcall target="prepareOutputDirectory">
            <param name="filesToInclude" value="${changedFiles},package.xml" /> <!-- explicitly adding package.xml as it is required for creating a valid package -->
        </antcall>

        <antcall target="copySrcAndReplaceNameSpace" />

    </target>

    <target name="cleanup">

    </target>

    <!-- Check if configuration folder exists or not for provided org -->
    <target name="dir.check">
        <condition property="dir.exists">
            <available file="config/${org}" type="dir" />
        </condition>
    </target>

    <target name="purgeflows" depends="initializeproperties,purgeinit,purgeflowsWindows,purgeflowsLinux">
        <echo message="Done purging" />
    </target>

    <target name="purgeinit">
        <property name="deleteinterviews" value="false" />
    </target>

    <target name="purgeflowsWindows" if="isWindows">
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="cmd">
            <arg line="/c 'node ./build/destructive.js --username ${dep.sf.username} --password ${dep.sf.password} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/' --deleteinterviews ${deleteinterviews}" />
        </exec>
    </target>

    <target name="purgeflowsLinux" if="isUnix">
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm install' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'npm start' " />
        </exec>
        <exec dir="../merge/" executable="sh">
            <arg line="-c 'node ./build/destructive.js --username ${dep.sf.username} --password ${dep.sf.password} --loginurl ${dep.sf.url} --sourcedir ${buildscriptdir}/ --deleteinterviews ${deleteinterviews}' " />
        </exec>
    </target>

</project>
