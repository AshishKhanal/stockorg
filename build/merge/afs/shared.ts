import * as xml2js from 'xml2js'
import * as fse from 'fs-extra'

const parser : xml2js.Parser = new xml2js.Parser();
const builder : xml2js.Builder = new xml2js.Builder();

/**
 * Returns the object representation from an xml file
 * @param fileName the path of the file that contains the xml that is to be converted into object
 */
export async function getObjectFromXmlFile(fileName : string) : Promise<any> {
    const data : Buffer = await fse.readFile(fileName);
    return new Promise<any>((resolve, reject) => {
        try {
            parser.parseString(data, function(err, obj){
                if(!err) {
                    resolve(obj);
                }
                else {
                    console.log('Error occurred while parsing file '+ fileName);
                    console.log(err);
                    reject(err);
                }
            });
        } catch(ex) {
            console.log('Error occurred while parsing file ' + fileName);
            reject(ex);
        }
    });
}

/**
 * Writes the XML from given object into the file
 * @param obj The object whose xml representation is to be saved
 * @param fileName The destination file name where the file is to be saved
 */
export function writeObjectToXmlFile(obj : any, fileName: string) : Promise<void> {
    return new Promise((resolve, reject) => {
        let fs = fse.createWriteStream(fileName);
        fs.write(builder.buildObject(obj));
        fs.close();
        resolve();
    });
}