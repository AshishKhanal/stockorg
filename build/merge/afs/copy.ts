import * as fs from 'async-file';
import * as fse from 'fs-extra';
import { createWriteStream } from 'fs-extra';
import { mergeFiles } from './merge';
import * as rm from './remove';

/**
 * Copies each file in the given folder recursively. If there are duplicates
 * it merges them based on available merge-strategy, if no merge strategy is found
 * it gives an error saying no appropriate strategy is found.
 * @param fromDir the path/directory from which to copy all the files/folders
 * @param toDir the path/directory to which we should copy all the files/folders
 * @param emptyDir indicates whether or not to empty a given directory
 * @returns a promise that indicates the completion or failure of the copy
 */
export async function copyRecursive(fromDir : string, toDir: string, emptyDir : boolean) : Promise<boolean> {
    let retVal : boolean = false ;
    
    let stats = await fs.stat(fromDir);
    if(!stats.isDirectory()) {
        throw new Error('fromDir isn\'t a directory');
    }

    if(emptyDir) {
        await fs.delete(toDir);
    }

    await fs.createDirectory(toDir);

    let files = fs.readdir(fromDir);
    if(emptyDir) {
        await fse.copy(fromDir, toDir);    
    }
    else {
        //since we have a recursive merge strategy we will need to copy individual folders and merge them
        let dirContents = await fse.readdir(fromDir);
        copyDirItems(fromDir, dirContents, toDir);
    }

    return retVal;
}

function copyDirItems(basePath:string, dirContents : string[], toPath:string) {
    
    dirContents.forEach(async dirItem => {
        let destPath : string = toPath + '/' + dirItem;
        let srcPath : string = basePath + '/' + dirItem;

        if(dirItem.startsWith('.')) {
            //ignore/remove hidden folders
            await fse.remove(destPath);
            return;
        }

        if(await fs.exists(destPath)) {
            let stat = await fse.stat(basePath + '/' + dirItem);
            if(stat.isDirectory()) {
                copyDirItems(srcPath, await fse.readdir(srcPath), destPath);
            }
            else {
                // if file exists we need to merge them to get them complete source
                await mergeFiles(srcPath, destPath);
            }
        }
        else {
            //if it doesn't exist just copy it
            await fse.copy(srcPath, destPath);
        }
    });
}

/**
 * This method removes the component from the given directory and fixes the package.xml
 * @param fromDir the directory from where the items are to be removed
 * @param itemsToRemove the list of items to remove from the package
 * @param limitTo the scope/file to limit to example specific object.
 */
export function removeComponents(basePath: string, itemType: string, itemsToRemove: string[], limitTo?: string) : Promise<boolean>{
    return new Promise(async (resolve, reject) => {
        if(itemsToRemove && itemsToRemove.length > 0) {
            await rm.removeComponents(itemsToRemove, itemType, basePath, limitTo)
        }
        resolve(true);
    });
}

/**
 * This method copies the file to a destination path
 * @param srcFile The file to copy
 * @param destPath The path where to copy the file
 */
export function copyFile(srcFile: string, destPath: string) : Promise<void> {
    return new Promise(async (resolve, reject) => {
        fse.copyFile(srcFile, destPath, function(err) {
            if(err) {
                console.log(err);
                throw err;
            } else {
                resolve();
            }
        });
    });
}