import {getObjectFromXmlFile, writeObjectToXmlFile} from './shared';

/**
 * This module merges two files based on available merge strategy
 * @returns
 */
export function mergeFiles(file1: string, file2: string) : Promise<void> {
    
    for (let index = 0; index < availableStrategies.length; index++) {
        const strategy = availableStrategies[index];
        if(strategy.canMerge(file1, file2)){
            console.log('Merging files ' + file1 + ' and ' + file2);
            return strategy.merge(file1, file2);
        }    
    }

    throw new Error('Couldnt find appropriate merge strategy for ' + file1 + ' and ' + file2);
}

/**
 * Simple interface that need to be any class to become a merge strategy
 */
export interface MergeStrategy {
    /**
     * returns true/false based on the src and destination file. true if merge object can merge it, else false.
     */
    canMerge(srcFile : string, destFile : string) : boolean;

    /**
     * performs the actual merge of the code
     */
    merge(srcFile: string, destFile: string) : Promise<void>;
}


const availableStrategies : MergeStrategy[] = [];

/**
 * This method registers the provided strategy instance with the merge factory, which is later invoked when merging is required.
 * @param strategy Strategy to register with the factory
 */
export function registerStrategy(strategy : MergeStrategy){
    availableStrategies.push(strategy);
}

export const util = {
    'getObjectFromXmlFile': getObjectFromXmlFile,
    'writeObjectToXmlFile': writeObjectToXmlFile
};