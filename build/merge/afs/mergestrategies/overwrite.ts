/**
 * Simple strategy to simply overwrite files useful for overwriting pages and other files which
 * don't need to be merged
 */
import * as merge from '../merge';
import * as fse from 'fs-extra'

export class Merge implements merge.MergeStrategy {
    
    public canMerge(srcFile : string, destFile : string) : boolean {
        srcFile = srcFile.toLowerCase();
        destFile = destFile.toLowerCase();

        return (srcFile.endsWith('compliance_quest-meta.xml') &&
        destFile.endsWith('compliance_quest-meta.xml') && destFile.indexOf('email') !== -1) ||
        (srcFile.endsWith('.page') && destFile.endsWith('.page')) ||
        (srcFile.endsWith('.page-meta.xml') && destFile.endsWith('.page-meta.xml'));
    }

    public async merge(srcFile: string, destFile: string) : Promise<void> {
        await fse.copy(srcFile, destFile);
    }
}