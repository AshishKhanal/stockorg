/**
 * Simple strategy to merge two workflow rules
 */
import * as merge from '../merge';

export class Merge implements merge.MergeStrategy {
    
    public canMerge(srcFile : string, destFile : string) : boolean {
        srcFile = srcFile.toLowerCase();
        destFile = destFile.toLowerCase();

        return srcFile.endsWith('.workflow') &&
        destFile.endsWith('.workflow');
    }

    public async merge(srcFile: string, destFile: string) : Promise<void> {
        
        let srcObj = await merge.util.getObjectFromXmlFile(srcFile);
        let destObj = await merge.util.getObjectFromXmlFile(destFile);

        for (const key in srcObj.Workflow) {
            if(key === '$'){
                continue;
            }
            
            if (srcObj.Workflow.hasOwnProperty(key)) {
                const element = srcObj.Workflow[key];
                if(destObj.Workflow.hasOwnProperty(key)) {
                    const destElement = destObj.Workflow[key];
                    destObj.Workflow[key] = destElement.concat(element);
                } else {
                    destObj.Workflow[key] = element;
                }
            }
        }

        await merge.util.writeObjectToXmlFile(destObj, destFile);
        
    }
}