/**
 * This combines two package xml files based on the types that are in the xml. It should be noted that
 * all other nodes except types are ignored. Therefore, it is advised to copy package without name to
 * prevent errors such as having approval process in named package.
 */
import * as merge from '../merge';

export class Merge implements merge.MergeStrategy {
    
    public canMerge(srcFile : string, destFile : string) : boolean {
        return srcFile.toLowerCase().endsWith('package.xml') &&
        destFile.toLowerCase().endsWith('package.xml');
    }

    public async merge(srcFile: string, destFile: string) : Promise<void> {
        
        let srcObj : PackageXml = await merge.util.getObjectFromXmlFile(srcFile);
        let destObj : PackageXml = await merge.util.getObjectFromXmlFile(destFile);

        let destTypeMap = new Map<string, number>();
        for (let index = 0; index < destObj.Package.types.length; index++) {
            const element = destObj.Package.types[index];
            const name = element.name[0];
            destTypeMap[name] = index;
        }

        for(let index = 0; index < srcObj.Package.types.length; index++) {
            const element = srcObj.Package.types[index];
            const name = element.name[0];
            if(destTypeMap[name] !== undefined) {
                const types = destObj.Package.types[destTypeMap[name]];
                let members : Set<string> = new Set<string>();
                types.members.forEach(member => {
                    members.add(member);
                });
                element.members.forEach(member => {
                    members.add(member);
                });
                types.members = Array.from(members).sort();
            }
            else {
                destObj.Package.types.push(element);
            }
        }

        await merge.util.writeObjectToXmlFile(destObj, destFile);

        return null;
    }
}

/**
 * Internal interface for working with Package.xml structure
 */
interface PackageXml {
    Package: PackageXmlRoot;
}

interface PackageXmlRoot {
    types : PackageXmlType[];
}

interface PackageXmlType {
    members : string[];
    name: string[];
}
