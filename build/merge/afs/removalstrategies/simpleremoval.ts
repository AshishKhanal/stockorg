import * as remover from '../remove';
import * as fse from 'fs-extra';
import * as fs from 'async-file';
import * as xml2js from 'xml2js';
import * as merge from '../merge';
/**
 * Deletes a file and removes the corresponding entry from package.xml
 */

export class RemovalStrategy implements remover.Remover {
    i_packageXmlCache: Map<string, number> = null;

    canRemove(componentType: string): boolean {
        componentType = componentType.toLowerCase();
        return componentType === 'flow' || componentType === 'workflowrule' || componentType === 'matchingrule';
    }

    async remove(basePath: string, componentType: string, component: string, packageXmlObj: any, limitTo?: string): Promise<void> {
        componentType = componentType.toLowerCase();
        switch (componentType) {
            case "flow":
                await this.i_removeFlow(basePath, componentType, component, packageXmlObj, limitTo);
                break;
            case "workflowrule":
                await this.i_removeWorkflow(basePath, componentType, component, packageXmlObj, limitTo);
                break;
            case "matchingrule":
                await this.i_removeMatchingRule(basePath, componentType, component, packageXmlObj, limitTo);
                break;
        }
    }

    async i_removeWorkflow(basePath: string, componentType: string, component: string, packageXmlObj: any, limitTo?: string) {
        let filePath = basePath + '/workflows/' + limitTo + '.workflow';
        if (!await fs.exists(filePath)) {
            filePath = basePath + '/workflows/' + limitTo.substr(limitTo.indexOf('__') + 2) + '.workflow';
            if (!await fs.exists(filePath)) {
                return;
            }
        }
        console.log('Removing from file ' + filePath);
        let fileObjXml: any = await remover.util.getObjectFromXmlFile(filePath);
        if (fileObjXml.Workflow.rules) {
            let removed: boolean = false;
            for (let index = 0; index < fileObjXml.Workflow.rules.length; index++) {
                const rule = fileObjXml.Workflow.rules[index];
                if (rule.fullName[0] === component) {
                    if (rule.workflowTimeTriggers) {
                        console.log(rule.fullName[0] + ' has been removed');
                        removed = true;
                        fileObjXml.Workflow.rules.splice(index, 1);
                    }
                    break;
                }
            }

            if (removed) {
                // remove from package.xml
                this.i_removeMemberFromXml(componentType, limitTo + '-' + component, packageXmlObj);
                this.i_removeMemberFromXml(componentType, limitTo.substr(limitTo.indexOf('__') + 2) + '-' + component, packageXmlObj);
                await remover.util.writeObjectToXmlFile(fileObjXml, filePath);
            }
        }
    }

    async i_removeFlow(basePath: string, componentType: string, component: string, packageXmlObj: any, limitTo?: string) {
        let filePath: string = '';
        let flowDefinitionPath: string = '';
        if (componentType.toLowerCase() === 'flow') {
            filePath = basePath + '/flows/' + component + '.flow';
            let fullFlowName = component.split("-");
            let flowName = fullFlowName[0];
            let version = fullFlowName[1].trim();
            flowDefinitionPath = basePath + '/flowDefinitions/' + flowName + '.flowDefinition';
            if (await fs.exists(flowDefinitionPath)) {
                var flowDefinitionXml: FlowDefinitionXml = await merge.util.getObjectFromXmlFile(flowDefinitionPath);
                if (flowDefinitionXml.FlowDefinition.activeVersionNumber.toString() === version) {
                    console.log('Removing flow definition ' + flowDefinitionPath);
                    await fs.delete(flowDefinitionPath);
                    this.i_removeMemberFromXml('FlowDefinition', flowName, packageXmlObj);
                }
            }
        }
        if (await fs.exists(filePath)) {
            console.log('Removing file' + filePath);
            await fse.remove(filePath);
        }

        this.i_removeMemberFromXml(componentType, component, packageXmlObj);
    }
    /**
     * will remove matching rule 
     * Note : 
     */
    async i_removeMatchingRule(basePath: string, componentType: string, component: string, packageXmlObj: any, limitTo?: string) {
        let filePath = basePath + '/matchingRules/' + limitTo + '.matchingRule';
        if (await fs.exists(filePath)) {
            console.log('Examining file ' + filePath);
            try {
                let fileObjXml: any = await remover.util.getObjectFromXmlFile(filePath);
                if (fileObjXml.MatchingRules.matchingRules) {

                    // if only one entry exists then delete whole file else delete individual entry in the file
                    console.log(JSON.stringify(fileObjXml.MatchingRules.matchingRules));
                    if (fileObjXml.MatchingRules.matchingRules.length == 1 && fileObjXml.MatchingRules.matchingRules[0].fullName[0] === component) {
                        console.log('Removing from file ' + filePath);
                        await fse.remove(filePath);
                        // remove entry from package.xml file too.
                        this.i_removeMemberFromXml(componentType, limitTo + '.' + component, packageXmlObj);
                    } else {
                        let removed: boolean = false;
                        for (let index = 0; index < fileObjXml.MatchingRules.matchingRules.length; index++) {
                            const rule = fileObjXml.MatchingRules.matchingRules[index];
                            if (rule.fullName[0] === component) {
                                console.log('Removing matching rule ' + rule.fullName[0]);
                                removed = true;
                                fileObjXml.MatchingRules.matchingRules.splice(index, 1);
                                break;
                            }
                        }
                        if (removed) {
                            // remove from package.xml
                            this.i_removeMemberFromXml(componentType, limitTo + '.' + component, packageXmlObj);
                            await remover.util.writeObjectToXmlFile(fileObjXml, filePath);
                        }
                    }
                }
            } catch (ex) {
                console.log('Something was horribly wrong');
                console.log(ex);
                throw ex;
            }

            console.log('Removing file ' + filePath);
        }
    }

    i_removeMemberFromXml(componentType: string, component: string, packageXmlObj: any) {
        if (!this.i_packageXmlCache) {
            this.i_createCache(packageXmlObj);
        }

        componentType = componentType.toLowerCase();
        console.log("Removing " + componentType + " of " + component);
        if (this.i_packageXmlCache.has(componentType) && packageXmlObj.Package.types[this.i_packageXmlCache.get(componentType)]) {
            let types = packageXmlObj.Package.types[this.i_packageXmlCache.get(componentType)];
            if (types.members.length == 1) {
                // if only one member exists then delete entire types of that index
                /* eg.
                <types>
                    <members>SQX_Controlled_Document__c.CQ_Duplicate_Draft_Document_Matching_Rule</members>
                    <name>MatchingRule</name>
                </types>
                */
               
               packageXmlObj.Package.types.splice(this.i_packageXmlCache.get(componentType),1); // splice(index,1)
            } else {
                types.members = types.members.filter(function (e) {
                    return typeof (e) !== 'string' || e.indexOf(component) === -1;
                });
            }

            delete this.i_packageXmlCache; //invalidate cache since indices are now invalid
        }
    }

    i_createCache(packageXmlObj: any) {
        console.log('Building package.xml cache');
        this.i_packageXmlCache = new Map<string, number>();
        for (let index = 0; index < packageXmlObj.Package.types.length; index++) {
            const type = packageXmlObj.Package.types[index];
            this.i_packageXmlCache.set(type.name[0].toLowerCase(), index);
        }
    }
}

export class FlowDefinitionXml {
    FlowDefinition: FlowDefinitionObject;
}

export class FlowDefinitionObject {
    activeVersionNumber: String
}