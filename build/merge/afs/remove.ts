import {getObjectFromXmlFile, writeObjectToXmlFile} from './shared';

/**
 * This module is useful for removing any components from the combined source if it has already been deployed.
 * This is important because subsequent deployment can cause issue with SF.
 */

/**
 * The method to remove any components from the package.
 * @param components the list of component that are to be removed
 * @param basePath the 'src' path of the package.
 * @param componentType the type of component that this request is for example: Flow, WorkflowRule
 * @param limitTo the scope of the request, scope to a particular object etc.
 */
export async function removeComponents(components : string[], componentType: string, basePath: string, limitTo?: string) {
    const packageXmlObj = await getObjectFromXmlFile(basePath + '/package.xml');
    
    for (let index = 0; index < registeredRemovers.length; index++) {
        const remover = registeredRemovers[index];
        if(remover.canRemove(componentType)){
            await remove(remover, basePath, componentType, components, packageXmlObj, limitTo);
            await writeObjectToXmlFile(packageXmlObj, basePath + '/package.xml');
            return;
        }
    }
    throw new Error('Couldnt remove components of type ' + componentType);
}

/**
 * 
 * @param component the exact component to remove
 */
async function remove(remover: Remover, basePath: String, componentType: string, components : string[], packageXmlObj: any, limitTo?: string) {

    for (let index = 0; index < components.length; index++) {
        const element = components[index];
        await remover.remove(basePath, componentType, element, packageXmlObj, limitTo);
    }
}



export interface Remover {
    canRemove(componentType : String) : boolean;
    remove(basePath: String, componentType : String, component : String, packageXmlObj : any, limitTo?: string) : Promise<void>;
}

const registeredRemovers : Remover[] = [];
export function register(remover: Remover) {
    registeredRemovers.push(remover);
}

/**
 * Helpers for using getObjectToXmlFile and Similar
 */
export const util = {
    'getObjectFromXmlFile': getObjectFromXmlFile,
    'writeObjectToXmlFile': writeObjectToXmlFile
};