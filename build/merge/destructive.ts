/*jslint node: true, es6, strict:false */
import { config } from './config';
import * as boot from './boot'
import * as sf from './sfconnector';
import * as fse from 'fs-extra';
import { performance } from 'perf_hooks';

/**
 * This class will attempt to deactivate and delete any flow present in the org where it is being deployed.
 * It will only delete flows from unmanaged package.
 */
const managedSrc = config.source.location + '/bin/src';

boot.start();
let t0 = performance.now();
sf.purgeFlows(config.source.deleteinterviews);
let t1 = performance.now();
console.log("Call to purge flows took " + (t1 - t0) + " milliseconds.");
