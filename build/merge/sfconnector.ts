/**
 * Module to connect with salesforce and perform various actions in sf
 */
import { config } from './config';
import { FlowDefinitionObject } from './afs/removalstrategies/simpleremoval';
const jsforce = require('jsforce');
const async = require('async');


const conn = new jsforce.Connection({
    loginUrl: config.sf.loginUrl,
    accessToken: config.sf.accessToken,
    instanceUrl: config.sf.instanceUrl,
    version: '43.0'
});

/**
 * Fetches the list of metadata that are available in SF.
 * @param callback method callback for success and failures of async task
 * @param types the types of metadata to fetch. SF only supports 3 for a request.
 */
const getMetadataItem = function (callback, types) {
    conn.metadata.list(types, '41.0', function (err, metadata) {
        if (err) {
            console.error(err);
            callback(err, null);
        } else {
            callback(null, metadata);
        }
    });
};

/**
 * Returns the list of meta data of workflow and workflow rule type
 */
const getWorkflowRules = function (callback) {
    getMetadataItem(callback, [{type: 'WorkflowRule', folder: ''}, {type: 'Workflow', folder: null}]);
};

/**
 * Returns the list of meta data of Flow and Flow Defintion type
 */
const getFlowDefinitions = function (callback) {
    getMetadataItem(callback, [{type: 'Flow', folder: null}, {type: 'FlowDefinition', folder: null}]);
};

/**
 * Removes all flows that can be removed from Salesforce. It attempts deletion of all flows
 * and ignores any failures.
 */
export async function purgeFlows(deleteInterviews : Boolean) : Promise<void> {
    if(deleteInterviews) {
        await queryAndDeleteInterview();
        console.log('All interviews removed');
    }
    const WORKFLOW = 'Workflow';
    let flowObjects = await queryFlowDefinitions();
    let flowsToDeactivate = [];
    let flowsToDeleteByOrder = [];
    let flowDefinitionNames = [];
    let flowNamesByOrder = [];

    flowObjects[0].forEach((flowDefinition) => {
        if(flowDefinition.ActiveVersionId != null) {
            flowsToDeactivate.push(flowDefinition.Id);
            flowDefinitionNames.push(flowDefinition.DeveloperName);
        }
    });

    flowsToDeleteByOrder = [[],[]];
    flowNamesByOrder = [[],[]];

    flowObjects[1].forEach((flowDefinition) => {

        let index = 0;
        if(flowDefinition.ProcessType !== WORKFLOW) {
            index = 1;
        }

        flowsToDeleteByOrder[index].push(flowDefinition.Id);
        flowNamesByOrder[index].push(flowDefinition.MasterLabel);
    });

    if(flowsToDeleteByOrder[0].length > 0 || flowsToDeleteByOrder[1].length > 0 ) {
        await deactivateFlows(flowsToDeactivate, flowDefinitionNames);
        await deleteFlows(flowsToDeleteByOrder, flowNamesByOrder);
    } else {
        console.log('No flows to delete');
    }
}

/**
 * Deactivates flows using tooling api
 * @param flowsToDeactivate the list of flows to deactivate
 */
async function deactivateFlows(flowsToDeactivate : string[], flowDefintionNames: string[]) : Promise<void> {
    console.log('Deactivating ' + flowsToDeactivate.length);
    let deactivationRequests = [];
    flowsToDeactivate.forEach(flowId => {
        deactivationRequests.push({
            "method": "PATCH",
            "url": "v43.0/tooling/sobjects/FlowDefinition/" + flowId,
            "richInput": {
                "Metadata": {
                    "activeVersionNumber": null
                }
            }
        })
    });

    printResult(await batchRequest(deactivationRequests, 'Deactivating flows'), flowDefintionNames);
}

/**
 * Formats the result and prints it to the console
 * @param result the result returned by batch request
 * @param name the list of names to be displayed in log message and indexed in same order of result
 */
function printResult(result : any, names: string[]) {
    if(names.length > 0) {
        console.log('Has Errors:' + result.hasErrors);
        result.results.forEach((element, idx) => {
            console.log(names[idx] + ' : ' + element.statusCode + ' : ' + JSON.stringify(element.result));
        });
    } else {
        console.log('No Changes');
    }
}

/**
 * Removes all flows provided in the list. The workflows are removed first followed by
 * invocable flows. This will prevent dependency issues unless their exists one between
 * two invocable flows.
 * @param flowsToDelete The list of flows to delete in order of dependency.
 */
async function deleteFlows(flowsToDelete: string[][], names: string[][]) : Promise<void> {
    let idx = 0;
    let flowsToDel : string[];
    for(idx = 0; idx < flowsToDelete.length; idx++) {
        flowsToDel = flowsToDelete[idx];
        if(flowsToDel !== null && flowsToDel.length > 0) {
            console.log('Deleting ' + flowsToDel.length + ' level ' + (idx + 1));
            let requests = [];
            flowsToDel.forEach((flow) => {
                requests.push({
                    "method": "DELETE",
                    "url": "v43.0/tooling/sobjects/Flow/" + flow
                });
            })

            printResult(await batchRequest(requests, 'Deleting flows'), names[idx]);
        }
    }
}

/**
 * Deletes the flow interviews passed as parameter.
 * @param flowInterviewsToDelete the list of flow interview objects to delete. Contains the flowinterview object returned by SF query
 */
async function deleteFlowInterview(flowInterviewsToDelete: any[]) : Promise<void> {
    let deletionRequests = [];
    let deletionIds = [];
    flowInterviewsToDelete.forEach(flowInterview => {
        deletionRequests.push({
            "method": "DELETE",
            "url": "v43.0/sobjects/FlowInterview/" + flowInterview.Id
        });
        deletionIds.push(flowInterview.Id);
    });

    printResult(await batchRequest(deletionRequests, 'Deleting flow interviews'), deletionIds);
}

/**
 * Internal method to batch the deletion requests. It automatically chunks the request
 * based on the max request size
 * @param requests the list of requests to send
 * @param purpose the reason the batch is being carried out
 */
async function batchRequest(requests, purpose) : Promise<any> {
    const BATCH_REQUEST_MAX_SIZE = 25;
    let startSize = requests.length;
    let batchResults = {hasErrors: false, results: []};
    await loginToSF();
    while(requests !== null && requests.length > 0) {
        let requestBody = {
            "batchRequests": requests.splice(0, BATCH_REQUEST_MAX_SIZE)
        };
        console.log(purpose + ': ' + (startSize - requests.length) + '/' + startSize);

        let batchResult : any = await new Promise((resolve, reject) => {
            conn.requestPost("/services/data/v43.0/composite/batch", requestBody, null, function(err, result){
                if(err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
        batchResults.hasErrors = batchResults.hasErrors || batchResult.hasErrors;
        batchResults.results = batchResults.results.concat(batchResult.results);
    }

    return batchResults;
}

/**
 * Internal method to query all flows and flow defintions and return them
 * @param flowLabels the list of flows to match
 */
async function queryFlowDefinitions() : Promise<any[][]> {
    await loginToSF();
    let queryFlowDefs = 'SELECT Id,ActiveVersionId,LatestVersionId,DeveloperName,LatestVersion.ProcessType,LatestVersion.MasterLabel FROM FlowDefinition WHERE ManageableState=\'unmanaged\'';

    let flowDefs = await new Promise<any>((resolve, reject) => {
        conn.tooling.query(queryFlowDefs, function(err, result){
            if(err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });

    let flowMasterLabels = [];
    flowDefs.records.forEach(flowDef => {
        flowMasterLabels.push(flowDef.LatestVersion.MasterLabel);
    });

    let queryFlows = 'SELECT Id,ProcessType,MasterLabel FROM Flow WHERE ManageableState=\'unmanaged\'';

    let flows = await new Promise<any>((resolve, reject) => {
        conn.tooling.query(queryFlows, function(err, result){
            if(err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });

    return [flowDefs.records, flows.records];
}

/**
 * Queries the flow interviews and deletes them.
 * @param limit Used to prevent infinite recursion and counts the number of queries executed.
 */
async function queryAndDeleteInterview(limit : number = 0): Promise<void> {
    const MAX_DEPTH_LIMIT : number = 20; 
    if(limit === MAX_DEPTH_LIMIT) {
        throw Error('Failed to complete flow interview deletion after iterating ' + MAX_DEPTH_LIMIT + ' times');
    }
    await loginToSF();
    return new Promise<void>(async (resolve, reject) => {
        await conn.query('SELECT Id FROM FlowInterview LIMIT 2000', async function(err, result) {
            if(err) {
                reject(err);
            } else {
                if(result.records && result.records.length > 0) {
                    let newLimit = limit + 1;
                    await deleteFlowInterview(result.records);
                    console.log('Deleted ' + result.records.length + ' interviews. Attempt: ' + newLimit);
                    await queryAndDeleteInterview(newLimit); // recursion until everything is deleted
                }

                if(limit == 0)
                {
                    resolve();
                }
            }
        });
    });
}

/**
 * Queries all the flows present in the org and returns them
 */
export async function queryDeployedFlows() : Promise<any[]> {
    await loginToSF();
    return new Promise<any[]>((resolve, reject) => {
        conn.tooling.query('SELECT Id,Definition.DeveloperName,MasterLabel,VersionNumber,Status,ProcessType FROM Flow', function(err, result){
            if(err) {
                reject(err);
            } else {
                resolve(result.records);
            }
        });
    });
}

/**
 * Public method to get the list of flows by developer name and their version number
 */
export async function getDeployedFlows() : Promise<string[]> {
    return queryDeployedFlows().then((results) => {
        let records : string[] = [];
        for (let index = 0; index < results.length; index++) {
            const record = results[index];
            records.push(record.Definition.DeveloperName + '-' + record.VersionNumber);
        }

        return records;
    });
}

/**
 * Public method to get the list of matching rule by developer name
 */
export async function getDeployedMatchingRule() : Promise<string[]> {
    return queryDeployedMatchingRule().then((results) => {
        let records : any = [];
        for (let index = 0; index < results.length; index++) {
            const record = results[index];
            records.push({"developername" : record.DeveloperName, "sobjecttype" : record.SobjectType});
        }

        return records;
    });
}

/**
 * Gets the list of deployed workflow rules
 */
export async function getDeployedWorkflowRules() : Promise<any> {
    await loginToSF();
    console.log('querying for workflowrules');
    return new Promise<any>((resolve, reject) => {
        conn.tooling.query('SELECT Id,Name,TableEnumOrId FROM WorkflowRule', function(err, result){
            if(err) {
                throw new Error(err);
            } else {
                let typeMap = {};
                for (let index = 0; index < result.records.length; index++) {
                    const record = result.records[index];
                    if(!typeMap[record.TableEnumOrId]) {
                        typeMap[record.TableEnumOrId] = [];
                    }
                    typeMap[record.TableEnumOrId].push(record.Name);
                }
                resolve(typeMap);
            }
        });
    });
}

/**
 * Queries all the matching rules present in the org and returns them
 */
export async function queryDeployedMatchingRule() : Promise<any[]> {
    await loginToSF();
    return new Promise<any[]>((resolve, reject) => {
        conn.query("SELECT Id, SobjectType, DeveloperName, NamespacePrefix, MasterLabel, RuleStatus FROM MatchingRule WHERE RuleStatus = 'Active'", function(err, result){
            if(err) {
                reject(err);
            } else {
                resolve(result.records);
            }
        });
    });
}

/**
 * Login and establish a session with SF
 */
async function loginToSF() {
    if(!conn.instanceUrl && !conn.accessToken) {
        await new Promise((promResolve, promReject) => {
            conn.login(config.sf.username, config.sf.password, function (err, userInfo) {
                if (err) {
                    console.error("Failed while connecting to " + config.sf.username, err);
                    promReject(err);
                } else {
                    console.log("Successfully connected to : " + userInfo.id);
                    promResolve();
                }
            });
        });
    }
}