/**
 * This file stores the configurations appropriate for executing the module
 */
import { env } from "process"
import * as minimist from "minimist"

var argv = minimist(process.argv.slice(2));
console.log(argv);
export const config = {
    sf: {
        username: env.SF_USER_NAME || argv.username,
        password: env.SF_PASSWORD || argv.password,
        accessToken: env.SF_ACCESSTOKEN || (argv.accessToken !== "null" ? argv.accessToken : undefined),
        loginUrl: env.SF_LOGIN_URL || argv.loginurl,
        instanceUrl : env.SF_INSTANCE_URL || (argv.instanceUrl !== "null" ?  argv.instanceUrl : undefined)
    },
    source: {
        location: env.SF_SOURCE || argv.sourcedir,
        removeonly: env.SF_REMOVE_ONLY || (argv.removeonly === 'true'),
        deleteinterviews: (env.SF_DELETE === 'true' || argv.deleteinterviews === 'true')
    }
};

if(!config.sf.loginUrl || (!config.sf.accessToken && (!config.sf.password || !config.sf.username)) || !config.source.location) {
    throw new Error('Configuration is incomplete please set (SF_USER_NAME/ SF_PASSWORD) OR SF_ACCESSTOKEN, SF_LOGIN_URL, SF_SOURCE')
}