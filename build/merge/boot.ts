/**
 * This module performs all the necessary housekeeping for booting the copying system
 */

// load all merge and removal strategies (statically for now)
import * as packagexml from './afs/mergestrategies/packagexml';
import * as overwrite from './afs/mergestrategies/overwrite';
import * as workflow from './afs/mergestrategies/workflow';
import { registerStrategy, MergeStrategy } from './afs/merge';
import * as remover from './afs/remove';
import * as simpleremoval from './afs/removalstrategies/simpleremoval';

export function start() {
    const strategies : MergeStrategy[] = [new packagexml.Merge(), new overwrite.Merge(), new workflow.Merge()];
    for (let index = 0; index < strategies.length; index++) {
        const strategy = strategies[index];
        registerStrategy(strategy);
    }

    const removalStrategies : remover.Remover[] = [new simpleremoval.RemovalStrategy()];
    for (let index = 0; index < removalStrategies.length; index++) {
        const removalStrategy = removalStrategies[index];
        remover.register(removalStrategy);
    }
}
