/*jslint node: true, es6, strict:false */
import { config } from './config';
import * as boot from './boot'
import * as afscopy from './afs/copy'
import * as sf from './sfconnector';

const extPkg = config.source.location + '/bin/extPkg/src';
const managedSrc = config.source.location + '/bin/src';
const outSrc = config.source.location + '/bin/combined';

boot.start();

async function removeComponents(){
    //let flowsToRemove = await sf.getDeployedFlows();
    //await afscopy.removeComponents(outSrc, 'Flow', flowsToRemove);
    
    let matchingRulesToRemove : any = await sf.getDeployedMatchingRule();
    console.log(matchingRulesToRemove);
    for (const key in matchingRulesToRemove) {
        if (matchingRulesToRemove.hasOwnProperty(key)) {
            const ruleName = matchingRulesToRemove[key].developername;
            let sobjecttype = matchingRulesToRemove[key].sobjecttype;
            // if object type has prefix then remove it because in our source folder matching rule folder does not contain prefix
            var regex = /^[A-Za-z0-9]{0,15}__(\w+)/,
                match;
            match = sobjecttype.match(regex);
            if(match) {
                sobjecttype = match[1];
            }
            console.error('Removing ' + ruleName, sobjecttype);
            await afscopy.removeComponents(outSrc, 'MatchingRule', [ruleName], sobjecttype);
        }
    }
    
    /*
    Disabled because an error was causing the build to fail sometimes. Since, time triggers aren't being used. We are ignoring it until we require it in the future.

    let rulesToRemove : any = await sf.getDeployedWorkflowRules();
    for (const key in rulesToRemove) {
        if (rulesToRemove.hasOwnProperty(key)) {
            const objName = rulesToRemove[key];
            await afscopy.removeComponents(outSrc, 'WorkflowRule', objName, key);
        }
    } */
}

if(config.source.removeonly) {
    afscopy.copyRecursive(managedSrc, outSrc, true).then(async function(){
        await removeComponents();
        await afscopy.copyFile(managedSrc + '/package.xml', outSrc + '/package.xml');
        console.log('finished removing deployed components');
    });
} else {
    afscopy.copyRecursive(extPkg, outSrc, true).then(function(){
        afscopy.copyRecursive(managedSrc, outSrc, false);
    }).then(async function(){
        await  removeComponents();
        console.log('Completed combining packages.');
    }).catch(function(error) {
        console.error('Error occurred while copying' + error);
    });
}



