![ComplianceQuest - Quality . Compliance . Collaboration . Communication](resources/Compliance_Quest_Logo.png)
==

This repository contains the source code for the ComplianceQuest Managed and External Packages, along with build scripts. The code has been organized as follows

* __additionalPackages__: This folder containts additional configuration that have to be deployed in order to prepare the org. Example: field dependency as required for QC's testing.

* __antscripts__: This folder contains ant scripts for building main package, source and deploying the code (CURRENT technique)

* __merge/__: The TypeScript project containing the logic for merging two packages and removing the flows/workflows that have already been deployed

* __gulpscripts/__: Experimental script for use with SFDX (Future of SalesForce deployment)

* __resources/__: This folder images and other resources for readme

Other related repositories
---

* [Excel Plugin for checking in audit](https://bitbucket.org/ambarkaar/cq-audit-checkin-plugin)

* [SCORM Heroku Server](https://bitbucket.org/ambarkaar/cq-proxy)

* [CQ Main Source](https://bitbucket.org/ambarkaar/sqx)

References
---

* [Salesforce Coding Standards & Naming Convention](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/4259842/Salesforce+Coding+Standards+Naming+Convention)

* [Standardization of Field Length](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/3932161/Standardization+of+Field+Length)

* [How to work with Salesforce and GIT](https://ambarkaar.atlassian.net/wiki/spaces/SQE/pages/983064/How+to+work+with+Salesforce+and+GIT)

* [Development Code Cycle](https://ambarkaar.atlassian.net/wiki/spaces/SQE/blog/2014/07/14/11075586/RFC+Re-engineering+Code+Development+Cycle+at+Ambarkaar)