/* jslint node: true */
"use strict";

/* TODO : use 'pump' instead of 'pipe' for error propagation */

var all = require('gulp-all'),
    concat = require('gulp-concat'),
    foreach = require('gulp-foreach'),
    gulp = require('gulp'),
    merge = require('gulp-merge'),
    minimist = require("minimist"),
    path = require('path'),
    process = require("process"),
    replace = require('gulp-replace');

var config = {};

var minName = {
    CQ_MIN_JS: 'cq.min.js',
    KENDO_MIN_JS: 'kendo.web.min.js',
    VF_MINIMAL_MIN: 'cqminimal.min.js',
    CQ_MIN_CSS: 'cqui.min.css'
};

function loadResource(resource, options) {
    options = options || {};
    options.cwd = options.cwd || config.path.srcResources;
    options.base = options.base || config.path.srcResources;

    return gulp.src(resource, options);
}

function replaceNS(stream) {
    return stream.pipe(replace(config.replacement.token, config.replacement.value));
}

gulp.task('init', function () {

    // get configurable params from command line
    var argv = minimist(process.argv.slice(2));

    if (!argv.resourcesDir || !argv.hostingDir || !argv.srcNamespace || !argv.destNamespace) {
        throw new Error('Configuration is incomplete please provide srcDir, destDir, srcNamespace, destNamespace');
    }

    config = {
        path: {
            srcResources: argv.resourcesDir,
            hostingDir: argv.hostingDir
        },
        replacement: {
            token: argv.srcNamespace,
            value: argv.destNamespace
        },
        MINIMAL_VF_JS_FILES: ['../promise-7.0.4.js', '/vf/minimal.js', '/ui/fileUploader.js', 'dataView.js'],
        CQ_MIN_JS_FILES: ['./**/*.js', '../promise-7.0.4.js', '!./vf/minimal.js', '../slds/src/*.js'],
        CQ_UI_V2_JS_FILES: ['./**', '!./ui/gridBase.js', '!./ui/gridV2.js'],
        V1_STYLES_ASSETS: ['Default/**', 'Silver/**', 'textures/**'],
        V2_STYLES_ASSETS: ['fonts/**', 'Silver/**']
    };
});

gulp.task('moveOtherResources', ['init'], function () {

    var resourcesToMove = [
        'CAPASchema.resource/CAPASchema.js'
    ];

    return loadResource(resourcesToMove)
        .pipe(foreach(replaceNS))
        .pipe(gulp.dest(config.path.hostingDir));
});

gulp.task('mergecqUIScripts', ['init'], function () {

    return all(

        loadResource('cqUI.resource/scripts/kendo/**/*.js')
            .pipe(concat(minName.KENDO_MIN_JS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/scripts/kendo')),

        loadResource(config.CQ_MIN_JS_FILES, {cwd: config.path.srcResources + '/cqUI.resource/scripts/cq/'})
            .pipe(concat(minName.CQ_MIN_JS))
            .pipe(foreach(replaceNS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/scripts/cq')),

        loadResource(config.MINIMAL_VF_JS_FILES, {cwd: config.path.srcResources + '/cqUI.resource/scripts/cq/'})
            .pipe(concat(minName.VF_MINIMAL_MIN))
            .pipe(foreach(replaceNS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/scripts/cq'))

    );
});

gulp.task('mergecqUIv2Scripts', ['init'], function () {

    return all(

        loadResource('cqUIv2.resource/scripts/kendo/**/*.js')
            .pipe(concat(minName.KENDO_MIN_JS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUIv2.resource/scripts/kendo')),

        loadResource(['cqUIv2.resource/scripts/cq/**/*.js', 'cqUI.resource/scripts/cq/**/*.js'])
            .pipe(concat(minName.CQ_MIN_JS))
            .pipe(foreach(replaceNS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUIv2.resource/scripts/cq'))
    );
});

gulp.task('mergecqUIStyles', ['init'], function () {

    return all(

        loadResource(['cqUI.resource/styles/**/*.min.css', 'cqUI.resource/styles/**/*.css']) // gulp doesn't repeat files, so this is valid
            .pipe(concat(minName.CQ_MIN_CSS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/styles/')),

        // copy icons
        loadResource(config.V1_STYLES_ASSETS, {cwd: config.path.srcResources + '/cqUI.resource/styles'})
            .pipe(gulp.dest(config.path.hostingDir))
    );

});

gulp.task('mergecqUIv2Styles', ['init'], function () {

    return all(

        loadResource(['cqUIv2.resource/styles/**/*.min.css', 'cqUIv2.resource/styles/**/*.css'])
            .pipe(concat(minName.CQ_MIN_CSS))
            .pipe(gulp.dest(config.path.hostingDir + '/cqUIv2.resource/styles')),

        // copy icons and other assests
        loadResource(config.V2_STYLES_ASSETS, {cwd: config.path.srcResources + '/cqUIv2.resource/styles'})
            .pipe(gulp.dest(config.path.hostingDir))
    );

});

gulp.task('mergePageScripts', ['init'], function () {
    return loadResource('cqUI.resource/pages/scripts/*.js')
        .pipe(foreach(function (stream, file) {
            var name = path.basename(file.path, '.js');
            if (name !== 'shared') {
                stream = merge(stream, loadResource('cqUI.resource/pages/scripts/shared.js'));
            }
            return stream
                .pipe(concat(name + '.min.js'));
        }))
        .pipe(foreach(replaceNS))
        .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/pages/scripts'));
});

gulp.task('minifyPageCSS', ['init'], function () {
    return loadResource('cqUI.resource/pages/styles/*.css')
        .pipe(foreach(function (stream, file) {
            var name = path.basename(file.path, '.css');
            if (name !== 'shared') {
                stream = merge(stream, loadResource('cqUI.resource/pages/styles/shared.css'));
            }
            return stream
                .pipe(concat(name + '.min.css'));
        }))
        .pipe(gulp.dest(config.path.hostingDir + '/cqUI.resource/pages/styles'));
});


gulp.task('hostcqUI', ['init', 'mergecqUIScripts', 'mergecqUIStyles', 'mergePageScripts', 'minifyPageCSS']);


gulp.task('hostcqUIv2', ['init', 'mergecqUIv2Scripts', 'mergecqUIv2Styles']);


gulp.task('build', ['init', 'hostcqUI', 'hostcqUIv2', 'moveOtherResources']);