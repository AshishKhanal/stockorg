var path = {
    SRC_DIR: ['../../SQX/src/**',
                '../../SQX/Helpers/Approval Process + Profile + And All Post Dep/src/**',
                '!../../SQX/src/package.xml',
                '!../../SQX/Helpers/Approval Process + Profile + And All Post Dep/src/package.xml'],
    DEST_DIR: './bin/src/',
    STATIC_RESOURCE_DIR: '../../SQX/Static Resources -- unzipped/**',
    RESOURCE_TEMP_DIR: './tmp/',
    MINIFY_JSFILES: ['../promise-7.0.4.js', 'common.js', 'fileUploader.js', 'dataView.js',
                         'translations.js', 'cq.ui.util.js', 'cqAutoSave.js', 'cqBinders.js', 
                         'cqCompositeField.js', 'CQElectronicSignature.js', 'cqExtensions.js', 
                         'cqFlowIndicator.js', 'CQGridBase.js', 'CQGridV2.js', 'dataStore.js', 
                         'kendoNotification.js', 'remote.js', 'schemaCorrection.js', 'CQESigWindow.js', 
                         'CQSpreadsheet.js', 'CQCommonPrint.js'],
    MINIMAL_VF_JSFILES: ['../promise-7.0.4.js', 'cqminimal.js', 'fileUploader.js', 'dataView.js'],
    MINIFY_NEWKENDO_JSFILES: ['common.js', 'fileUploader.js', 'dataView.js', 'translations.js', 
                                'cq.ui.util.js', 'cqAutoSave.js', 'cqBinders.js', 'cqCompositeField.js', 
                                'CQElectronicSignature.js', 'cqExtensions.js', 'cqFlowIndicator.js', 'dataStore.js', 
                                'kendoNotification.js', 'remote.js', 'schemaCorrection.js', 'CQESigWindow.js', 
                                'CQSpreadsheet.js', 'CQExcelSheet.js', 'CQCommonPrint.js', 'CQContentUploader.js', 
                                'addParamtoURL.js', 'CQKendoSpreadsheet.js']
};

var fs = require('fs'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    replace = require('gulp-replace'),
    zip = require('gulp-zip'),
    all = require('gulp-all'),
    sourcemaps = require('gulp-sourcemaps'),
    util = require('gulp-util'),
    foreach = require('gulp-foreach'),
    gpath = require('path'),
    debug = require('gulp-debug'),
    merge = require('gulp-merge'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    logger = require('gulp-logger');


gulp.task('removeExistingSrcAndTmp', function(){
    return gulp.src([path.DEST_DIR, path.RESOURCE_TEMP_DIR])
        .pipe(clean());
});

gulp.task('mergeAndCopySource', ['removeExistingSrcAndTmp'], function(){
    return gulp.src(path.SRC_DIR)
        //.pipe(logger({ showChange: true }))
        .pipe(gulp.dest(path.DEST_DIR));
});

gulp.task('copyResourceToTmp', ['mergeAndCopySource'], function(){
    return gulp.src(path.STATIC_RESOURCE_DIR)
        //.pipe(logger({ showChange: true }))
        .pipe(gulp.dest(path.RESOURCE_TEMP_DIR));
});

var buildExtendedPermissionSet = function(srcFile, destFile) {
    var binPath = path.DEST_DIR + 'permissionsets/', // folder containing all permission set files
        RECORD_TYPE_PLACEHOLDER = '###RecordType###',
        reRTV = /<recordTypeVisibilities>[\s\S]*?<\/recordTypeVisibilities>/g, // expression to match record type visibility xml elements
        gulpRet = gulp.src(binPath + destFile), // init gulp src task using extended permission set file as we are building it
        srcXmlString,
        matches;
    
    // get source file text
    srcXmlString = fs.readFileSync(binPath + srcFile, 'utf8');
    // get all matched strings
    matches = srcXmlString.match(reRTV);
    
    if (matches != null && matches.length > 0) {
        // join all matches with new line and indentation into on text
        var visibilities = matches.join('\r\n    '); 
        
        // replace obtained record type visibilities into placeholder and save the extended permission set file
        gulpRet.pipe(replace(RECORD_TYPE_PLACEHOLDER, visibilities))
            .pipe(gulp.dest(binPath));
    }
    
    return gulpRet;
};

gulp.task('buildCQExtendedPermissionSets', ['mergeAndCopySource'], function() {
    return all(
        buildExtendedPermissionSet('CQ_System_Administrator.permissionset', 'CQ_Admin_Extended_Permissions.permissionset'),
        buildExtendedPermissionSet('CQ_Standard_User.permissionset', 'CQ_Standard_Extended_Permissions.permissionset')
    );
});

gulp.task('minifyCAPASchema', ['copyResourceToTmp'], function(){
    return gulp.src(path.RESOURCE_TEMP_DIR + 'CAPASchema.resource/CAPASchema.js')
        //.pipe(logger({ showChange: true }))
        .pipe(minify({ noSource: true}))
        .pipe(rename('CAPASchema.js'))
        .pipe(zip('CAPASchema.resource'))
        .pipe(gulp.dest(path.DEST_DIR+'staticresources'));
});

gulp.task('moveSetupData', ['copyResourceToTmp'], function(){
    return all(
        gulp.src(path.RESOURCE_TEMP_DIR + 'SetupData.resource/SetupData.js')
            //.pipe(logger({ showChange: true }))
            .pipe(zip('SetupData.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR + 'SetupData_6_4.resource/SetupData_6_4.js')
            //.pipe(logger({ showChange: true }))
            .pipe(zip('SetupData_6_4.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR + 'SetupData_7_0.resource/SetupData_7_0.js')
            //.pipe(logger({ showChange: true }))
            .pipe(zip('SetupData_7_0.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR + 'SetupData_8_0.resource/SetupData_8_0.js')
            //.pipe(logger({ showChange: true }))
            .pipe(zip('SetupData_8_0.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources'))
    );
});

gulp.task('minifycqUIKendoScripts', ['copyResourceToTmp'], function(){
    return all(
        gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/kendo/**')
            //.pipe(logger({ showChange: true }))
            .pipe(concat('kendo.web.min.js'))
            .pipe(minify({ noSource: true, ext:{min: '.js'}}))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR +'cqUI.resource/scripts/kendo/')),
        gulp.src(path.MINIFY_JSFILES, { cwd: path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/sqx/' })
            //.pipe(logger({ showChange: true }))
            .pipe(concat('cq.min.js'))
            .pipe(sourcemaps.init())
            .pipe(minify({ noSource: true, ext: { min: '.js' }}))
            .pipe(sourcemaps.write('.', { includeContent: false }))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/sqx/')),
        gulp.src(path.MINIMAL_VF_JSFILES, { cwd: path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/sqx/' })
            //.pipe(logger({ showChange: true }))
            .pipe(concat('minimal.min.js'))
            .pipe(sourcemaps.init())
            .pipe(minify({ noSource: true, ext: { min: '.js' } }))
            .pipe(sourcemaps.write('.', { includeContent: false }))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/sqx/'))
    );
});

gulp.task('minifycqUIv2KendoScripts', ['copyResourceToTmp'], function () {
    return all(
        gulp.src(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/scripts/kendo/**')
            //.pipe(logger({ showChange: true }))
            .pipe(concat('kendo.web.min.js'))
            .pipe(minify({ noSource: true, ext: { min: '.js' } }))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/scripts/kendo/')),
        gulp.src(path.MINIFY_NEWKENDO_JSFILES, { cwd: path.RESOURCE_TEMP_DIR + 'cqUI.resource/scripts/sqx/' })
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/scripts/sqx/')),
        gulp.src(path.MINIFY_NEWKENDO_JSFILES, { cwd: path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/scripts/sqx/' })
            //.pipe(logger({ showChange: true }))
            .pipe(concat('cq.min.js'))
            .pipe(sourcemaps.init())
            .pipe(minify({ noSource: true, ext: { min: '.js' } }))
            .pipe(sourcemaps.write('.', { includeContent: false }))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/scripts/sqx/'))
    );
});

gulp.task('minifyPageScripts', ['copyResourceToTmp'], function(){
    return gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/scripts/*.js')
        //.pipe(logger({ showChange: true }))
        .pipe(foreach(function(stream, file){
            var name = gpath.basename(file.path, '.js');
            if(name === 'shared'){
                return stream
                    .pipe(concat(name + '.min.js'))
                    .pipe(sourcemaps.init())
                    .pipe(minify({ noSource: true, ext: { min: '.js' } }))
                    .pipe(sourcemaps.write('.', { includeContent: false }));
            } else {
                return merge(stream, gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/scripts/shared.js'))
                    .pipe(concat(name + '.min.js'))
                    .pipe(sourcemaps.init())
                    .pipe(minify({ noSource: true, ext: { min: '.js' } }))
                    .pipe(sourcemaps.write('.', { includeContent: false }));
            }
        }))
        .pipe(gulp.dest(path.RESOURCE_TEMP_DIR +'cqUI.resource/pages/scripts/'));
});

gulp.task('minifycqUIKendoCSS', ['copyResourceToTmp'], function(){
    return merge(gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/CQStyle.silver.css')
        //.pipe(logger({ showChange: true }))
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' })), gulp.src([path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/*.min.css', '!' + path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/CQStyle.silver.min.css']))
        .pipe(concat('cqui.min.css'))
        .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/'));
});

gulp.task('minifycqUIv2KendoCSS', ['copyResourceToTmp'], function () {
    return merge(gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/CQStyle.silver.css')
        //.pipe(logger({ showChange: true }))
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' })), gulp.src([path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/*.min.css', '!' + path.RESOURCE_TEMP_DIR + 'cqUI.resource/styles/CQStyle.silver.min.css']))
        .pipe(concat('cqui.min.css'))
        .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/styles/'));
});

gulp.task('combineAndMinifyPageCSS', ['copyResourceToTmp'], function(){
    return all(
        gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/shared.css')
            //.pipe(logger({ showChange: true }))
            .pipe(cleanCSS())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/')),
        gulp.src([path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/*.css',
            '!' + path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/shared.css'])
            .pipe(foreach(function (stream, file) {
                var name = gpath.basename(file.path, '.css');
                return merge(stream, gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/shared.css'))
                    .pipe(concat(name + '.min.css'))
                    .pipe(cleanCSS());
            })
            )
            .pipe(gulp.dest(path.RESOURCE_TEMP_DIR + 'cqUI.resource/pages/styles/'))
    );
});

gulp.task('zipcqUIResources', ['copyResourceToTmp', 'minifycqUIKendoScripts', 'minifycqUIKendoCSS', 'minifyPageScripts', 'combineAndMinifyPageCSS'], function(){
    return gulp.src(path.RESOURCE_TEMP_DIR + 'cqUI.resource/**')
        //.pipe(logger({ showChange: true }))
        .pipe(zip('cqUI.resource'))
        .pipe(gulp.dest(path.DEST_DIR + 'staticresources'));
});

gulp.task('zipcqUIv2Resources', ['copyResourceToTmp', 'minifycqUIv2KendoScripts', 'minifycqUIv2KendoCSS'], function(){
    return gulp.src(path.RESOURCE_TEMP_DIR + 'cqUIv2.resource/**')
        //.pipe(logger({ showChange: true }))
        .pipe(zip('cqUIv2.resource'))
        .pipe(gulp.dest(path.DEST_DIR + 'staticresources'));
});

gulp.task('zipOtherResource', ['copyResourceToTmp'], function(){
    return all(
        gulp.src(path.RESOURCE_TEMP_DIR +'LightningDesignStyle.resource/**')
            .pipe(zip('LightningDesignStyle.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR +'Secondary_Format_Setting_Stamping_PDF_Landscape.resource/**')
            .pipe(zip('Secondary_Format_Setting_Stamping_PDF_Landscape.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR +'Secondary_Format_Setting_Stamping_PDF_Portrait.resource/**')
            .pipe(zip('Secondary_Format_Setting_Stamping_PDF_Portrait.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR +'Secondary_Format_Setting_Workflow.resource/**')
            .pipe(zip('Secondary_Format_Setting_Workflow.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources')),
        gulp.src(path.RESOURCE_TEMP_DIR + 'CQAuditSpreadsheetTemplate.resource/**')
            .pipe(zip('CQAuditSpreadsheetTemplate.resource'))
            .pipe(gulp.dest(path.DEST_DIR + 'staticresources'))
    );
});

gulp.task('buildSFDXSrc', ['removeExistingSrcAndTmp', 'mergeAndCopySource', 'copyResourceToTmp', 'buildCQExtendedPermissionSets', 'minifyCAPASchema', 
    'moveSetupData', 'zipcqUIResources', 'zipcqUIv2Resources', 'zipOtherResource'], function(){
                                return gulp.src(path.RESOURCE_TEMP_DIR)
                                    //.pipe(logger({ showChange: true }))
                                    .pipe(clean());
                                });


