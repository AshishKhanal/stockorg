# Scratch Org Project

This project contains required setup and CQ source files with components for creating scratch org mainly for CQ validation purpose.

Notes:

* To create scratch org for CQ validation purpose, use "val.dev.cq.com" domain and they can be used in jenkins project.
    e.g. target username for new scratch org valorg5.admin@val.dev.cq.com
* To create scratch org for dev purposes i.e. other than CQ validation, use your desired names.
* Do not add anything in the CQ permission sets files that are not in git or next deployment package since they will be overriden by the next package.

## About CQ Validation Purpose

CQ Validation purpose is to validate full CQ source in an empty sf org with running all unit tests.

Note: CQ source cannot be deployed to empty org due dependency on few settings, SF or CQ objects and their fields with their permissions.

## Steps to create and initialize scratch org for CQ project

Note: Execute following commands from _scratch-org-project_ folder.

### 1.  Create scratch org with 30 days duration:
```
    sfdx force:org:create -f config/project-scratch-def.json -d 30 username="<targetUsername>" adminEmail="<ambarkaarEmail>" orgName="CQ Package Validation" [-a <orgAlias>]
```
    
### 2. Initialize created scratch org using following sfdx commads:
```
    sfdx force:source:push -u <targetUsername/orgAlias>
```
    
### 3. For CQ validation, run following sfdx commands to setup user role and permission set assignments:
```
    sfdx force:apex:execute -f val-org-setup-scripts/user-setup-scripts.apex -u <targetUsername/orgAlias>
```

### 4. Generate password for the targetUsername:
```
    sfdx force:user:password:generate -u <targetUsername/orgAlias>
    
    Note: Use "sfdx force:user:display -u <targetUsername/orgAlias>" to view generated passowrd if needed again.
```

### 5. Open scratch org using following command and reset security token from "My Settings":
```
    sfdx force:org:open -u <targetUsername/orgAlias>
```
Note: Steps 4 and 5 will be used for deploying CQ source using ant scripts.

## Description of Files and Directories

* __config__ folder: sfdx project config folder containing project-scratch-def.json file.
* __force-app__ folder: sfdx project folder containing source files. Currently, it contains files that are needed for executing CQ validation purpose, which can be used for other purpose i.e. deploying full CQ source using ant scripts into the desired scratch org for development.
* __val-org-setup-scripts__ folder: contains scripts such as apex scripts that helps in setting up scratch org for CQ validation purpose.
* __val-org-setup-scripts/user-setup-scripts.apex__ file: contains apex scripts to assign "Admin" user role and assigns CQ permission sets to current user (i.e. targeted user) needed for CQ validation.
* __.forceignore__ file: sfdx source management ignore list.
* __README.md__ file: This file.
* __sfdx-project.json__ file: sfdx project info file.
