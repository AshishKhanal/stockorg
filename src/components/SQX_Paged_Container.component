<apex:component >
    <apex:attribute type="ApexPages.StandardSetController" name="setController" description="The standard set controller that is to be used to iterate over the records" required="true"/>
    <apex:attribute type="String" name="rerenderId" description="The name/id of the containing div. Useful for asynchronous rendering of the component" required="true" />
    <apex:attribute type="String" name="cssclass" description="The name of the css class that can be used to identify the panel" />
    <apex:attribute type="String" name="var" description="The name of the variable passed to component body" />

    <div style="width: 100%; overflow-y: scroll; overflow-x:auto;" class="{!cssclass}" >
        <apex:outputPanel id="pagedTable" rendered="{!setController.ResultSize > 0}">
            <apex:componentBody id="pagedArea">
                <apex:variable var="{!var}" value="{!setController.Records}"/>
            </apex:componentBody>
            <div style="width:80%; float:left;">
                <apex:commandLink action="{!setController.previous}" immediate="true" value="{!$Label.compliancequest__cq_ui_previous}" rendered="{!setController.hasPrevious}" reRender="{!RerenderId}" status="loading" />
                &nbsp;<apex:commandLink action="{!setController.next}" immediate="true" value="{!$Label.compliancequest__cq_ui_next}" rendered="{!setController.hasNext}" reRender="{!rerenderId}" status="loading" />
            </div>
            <div style="width:20%; float:right; clear:right; text-align:right;">
                <apex:actionStatus startText="{!$Label.compliancequest__cq_ui_loading}" stopText="" id="loading" onstop="if(window.resizePage){ window.resizePage(); }"></apex:actionStatus>
                <apex:outputText value="{!SUBSTITUTE(SUBSTITUTE('{pageNum} / {totalPage}', '{totalPage}', TEXT(CEILING(setController.ResultSize / setController.PageSize))), '{pageNum}', TEXT(setController.pageNumber) ) }"></apex:outputText>
            </div>
        </apex:outputPanel>
        <apex:pageBlockSection rendered="{!setController.ResultSize == 0}" columns="1">
            <p style="padding: 5px 0px; border-width: 1px 0; border-color: #d4dadc; border-style:solid; margin: 0px;">{!$Label.compliancequest__cq_ui_no_records_to_display}</p>
        </apex:pageBlockSection>
    </div>
</apex:component>