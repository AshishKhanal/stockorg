<apex:component layout="none">
    <apex:attribute type="SQX_Extension_UI" 
                    description="Extension of the page"
                    name="extension"
                    required="true" />
    
    <apex:attribute type="Boolean" 
                    description="New resource required"
                    name="newJS"
                    default="false"/>
                    
    <!-- StyleSheet needed for running kendo vf page -->
    <apex:stylesheet value="{!IF(newJS, URLFOR($Resource.cqUIv2, 'styles/cqui.min.css'), URLFOR($Resource.cqUI, 'styles/cqui.min.css'))}" />
    <script type="text/javascript" src="{!URLFOR($Resource.cqUI, 'scripts/jquery.2.1.3.min.js')}"></script>

    <script type="text/javascript">
        $(function(){
            var userLocale = UserContext.locale;
                if(userLocale.length > 0) {
                    userLocale = userLocale.substring(0,2);
                    switch(userLocale) {
                        case 'ja' : 
                            kendo.culture('ja-JP');
                            break;
                        case 'pl' :
                            kendo.culture('pl-PL');
                            break;
                        case 'zh' : 
                            kendo.culture('zh-CN');
                            break;
                        case 'pt' : 
                            kendo.culture('pt-PT');
                            break;
                        case 'de' :
                            kendo.culture('de-DE');
                            break;
                        case 'nl' :
                            kendo.culture('nl-Nl');
                            break;
                        case 'es' : 
                            kendo.culture('es-ES');
                            break;
                        case 'fr' : 
                            kendo.culture('fr-FR');
                            break;
                        default : 
                            kendo.culture('en-US');
                            break;
                    }
                }

            // this condition only for lightning experience mode
            if ((typeof sforce != 'undefined') && (sforce != null)) {
               $(".commandSection").css("padding-bottom","10px");
            }

            // add class in the body for changing UI in lighting
            if({!$User.UIThemeDisplayed == 'Theme4d' }){
            	$("body").addClass('cq-lightning');
            }
        });
    </script>

    <script type="text/javascript">
        var primaryController = '{!JSEncode(extension.ControllerName)}', //used to pass controller's name to remote.js
            UIController,

            //used to initialize the main record for use in the page
            mainRecordType = '{!JSEncode(extension.MainRecordType)}',  
            mainRecord,
            //the value of current user and user details used by the e-signature component
            currentUser = {'Id':'{!$User.Id}','Name':'{!JSENCODE($User.FirstName)} {!JSENCODE($User.LastName)}'},
            userDetails = '{!JSENCODE(extension.UserDetailsForRecordActivity)}',
 
            // personnels related the current user
            personnel = JSON.parse('{!JSENCODE(extension.Personnel)}'),
            
            //the value of the tab that is to be selected when the main page is selected
            initialTab = '{!JSENCODE($CurrentPage.parameters.initialTab)}',

            //the record to expand and the type of record that is to be expanded
            expandRecord='{!JSENCODE($CurrentPage.parameters.expandRecord)}',
            expandRecordType = '{!JSENCODE($CurrentPage.parameters.expandRecordType)}',
            
            // document compare values
            isDocComparePackagePageAvailable = {! extension.IsDocComparePackagePageAvailable },
            sqxDocumentComparePage = "{!JSENCODE(URLFOR($Page.SQX_Document_Compare))}",

            //the matrix that defines the various access in the SF Org for the SF records included in the dataset
            securityMatrix = JSON.parse('{!JSENCODE(extension.SecurityMatrix)}'),

            recordAccess,
            commandRules = {}, //control the visibility of the command section
            esigPolicies = JSON.parse('{!JSENCODE(extension.esigPoliciesJSON)}'),
            purposeOfSigs = JSON.parse('{!JSENCODE(extension.PurposeOfSigJSON)}'),
            customDateFormat = '{!JSENCODE($Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Date_Format__c)}',
            customTimeFormat = '{!JSENCODE($Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Time_Format__c)}',
            customNumberDecimalFormat = '{!JSENCODE($Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Decimal_Separator__c)}',
            customNumberGroupFormat = '{!JSENCODE($Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Group_Separator__c)}',
            isUserThemeMobile = "{!$User.UIThemeDisplayed == 'Theme4t'}",
            esigEnabled = {!JSEncode(IF($Setup.compliancequest__SQX_CQ_Electronic_Signature__c.compliancequest__Enabled__c, "true", "false" ) )},
            requireUserName = {!JSEncode(IF($Setup.compliancequest__SQX_CQ_Electronic_Signature__c.compliancequest__Ask_UserName__c, "true", "false" ) )};

            try {
                UIController = window.{!JSEncode(extension.ControllerName)};
            }
            catch(err) {
                //when error occured freezes the window, to replicate this issue reload a page
                setTimeout(function() {
                    $("#loadingBlock").text('There was some error while loading the page. Please wait while we reload');
                    navigateToUrl(window.location.href);
                }, 1000);
            }
    </script>


    <script type="text/javascript" src="{!HTMLENCODE(extension.ResourcePath)}"></script>
    <script type="text/javascript" src="{!IF(newJS, URLFOR($Resource.cqUIv2, 'scripts/kendo/kendo.web.min.js'), URLFOR($Resource.cqUI, 'scripts/kendo/kendo.web.min.js'))}" ></script>
    
    <script type="text/javascript">
        var cq = window.cq || {};
        cq.SESSION_ID = '{!JSENCODE($Api.Session_Id)}';
        cq.printMode = {!$CurrentPage.parameters.printMode == 'true'};
        cq.resourceLocation = '{!JSENCODE(URLFOR($Resource.cqUI))}';
        // added in 7.4 for approval configuration
        cq.apis = {
            submitForApproval : false
        };
        cq.labels = {
            common : {
                waitmessage: '{!JSENCODE($Label.CQ_UI_Please_Wait)}',
                ownerfield : '{!JSENCODE($Label.CQ_UI_Owner)}',
                changeowner : '{!JSENCODE($Label.CQ_UI_Owner_Change)}',
                requiredmsg : '{!JSENCODE($Label.CQ_UI_Field_Is_Required)}',
                errormsg : '{!JSENCODE($Label.CQ_UI_ERROR_MESSAGE)}',
                fixerrormsg : '{!JSENCODE($Label.CQ_UI_Fix_Error)}',
                file : '{!JSENCODE($Label.CQ_UI_File)}',
                addfiles: '{!JSENCODE($Label.CQ_UI_Add_Files)}',
                printmodeheader: '{!JSENCODE($Label.CQ_UI_Print_Preview_Message)}',
                createddate : '{!JSENCODE($ObjectType.SQX_Audit_Program_Record_Activity__c.fields.CreatedDate.Label)}'
            },
            cqcompositefield : {
                comboplaceholder : '{!JSENCODE($Label.CQ_UI_Dropdown_Placeholder)}',
                dropdownlabel : '{!JSENCODE($Label.CQ_UI_Dropdown_None)}',
                invaliddate : '{!JSENCODE($Label.CQ_UI_Invalid_Date)}'
            },
            cqgrid : {
                commands : {
                    create : '{!JSENCODE($Label.CQ_UI_Grid_Add)}',
                    update : '{!JSENCODE($Label.CQ_UI_Grid_Update)}',
                    cancel : '{!JSENCODE($Label.CQ_UI_Cancel)}',
                    edit   : '{!JSENCODE($Label.CQ_UI_Grid_Edit)}',
                    destroy: '{!JSENCODE($Label.CQ_UI_Grid_Delete)}',
                    canceledit : '{!JSENCODE($Label.CQ_UI_Cancel)}',
                    saveandnew : '{!JSENCODE($Label.CQ_UI_Grid_Save_And_New)}',
                    preview : '{!JSENCODE($Label.CQ_UI_Preview)}'
                },
                popuptitle: '{!JSENCODE($Label.CQ_UI_Grid_PopupTitle)}',
                deleteConfirmation: '{!JSENCODE($Label.CQ_UI_Grid_Delete_Confirmation)}',
                hidesection: '{!JSENCODE($Label.CQ_UI_Hide_Section)}',
                showsection: '{!JSENCODE($Label.CQ_UI_Show_Section)}'
            },
            esig : {
                username: '{!JSENCODE($Label.CQ_UI_Logged_In_As)}',
                password: '{!JSENCODE($Label.CQ_UI_Password)}',
                comment: '{!JSENCODE($Label.CQ_UI_Comments)}',
                purposeofsig: '{!JSENCODE($Label.CQ_UI_Purpose_Of_Sig)}',
                save : '{!JSENCODE($Label.CQ_UI_Save)}',
                cancel : '{!JSENCODE($Label.CQ_UI_Cancel)}'
            },
            docUpload : {
                selectFileFromServer : '{!JSENCODE($Label.CQ_UI_Select_file_from_Server)}',
                uploadNewFile : '{!JSENCODE($Label.CQ_UI_Click_to_upload_new_file)}',
                uploadNewVersion : '{!JSENCODE($Label.CQ_UI_Upload_New_Version)}',
                emptyFileUpload : '{!JSENCODE($Label.CQ_UI_Empty_Files_Cannot_Be_Uploaded)}',
                errorInUploading : '{!JSENCODE($Label.CQ_UI_Error_occurred_while_uploading)}',
                actualError : '{!JSENCODE($Label.CQ_UI_Actual_Error)}'
            },
            autoSave : {
                saving : '{!JSENCODE($Label.CQ_UI_Saving)}',
                saved : '{!JSENCODE($Label.CQ_UI_SAVED)}'
            },
            binders: {
                file: {
                    download: '{!JSENCODE($Label.CQ_UI_Page_Title_Download)}',
                    preview: '{!JSENCODE($Label.CQ_UI_Controlled_Document_Preview)}'
                }
            },
            ksheet: {
            	multiValueErr: '{!JSENCODE($Label.CQ_ERR_Multiple_Match_Found)}',
                validValueErr: '{!JSENCODE($Label.CQ_ERR_Value_Not_Valid)}',
                requiredFieldErr: '{!JSENCODE($Label.SQX_ERR_MSG_FIELD_IS_REQUIRED)}'
            }
        };

        window.kendo.ui.Upload.fn.options.localization = {
            select : '{!JSENCODE($Label.CQ_UI_Select_files)}',
            cancel : '{!JSENCODE($Label.CQ_UI_Cancel)}',
            retry : '{!JSENCODE($Label.CQ_UI_Retry)}',
            remove : '{!JSENCODE($Label.CQ_UI_Remove)}',
            uploadSelectedFiles : '{!JSENCODE($Label.CQ_UI_Upload_files)}',
            dropFilesHere : '{!JSENCODE($Label.CQ_UI_drop_files_here_to_upload)}',
            statusUploading : '{!JSENCODE($Label.CQ_UI_Uploading)}',
            statusUploaded : '{!JSENCODE($Label.CQ_UI_Uploaded)}',
            statusWarning : '{!JSENCODE($Label.CQ_UI_Warning)}',
            statusFailed : '{!JSENCODE($Label.CQ_UI_Failed)}',
            headerStatusUploading : '{!JSENCODE($Label.CQ_UI_File_Uploading)}',
            headerStatusUploaded : '{!JSENCODE($Label.CQ_UI_Done)}'
        }
    </script>
    
    
    <script type="text/javascript" src="{!IF(newJS, URLFOR($Resource.cqUIv2, 'scripts/cq.min.js'), URLFOR($Resource.cqUI, 'scripts/cq.min.js'))}"></script>

    <script type="text/javascript">
        window.kendo.ui.CQContentUploader.prototype.options.localization = {
            blockerMessage: '{!JSENCODE($Label.CQ_UI_Uploading)}',
            cancelCommand: '{!JSENCODE($Label.CQ_UI_Grid_Cancel)}',
            deleteCommand: '{!JSENCODE($Label.CQ_UI_Delete_Uploaded_Attachment)}'
        };

        (function(events) {
            events.on(events.POSTINITNEWRECORD, function(e, newRow){
                newRow.set('compliancequest__Org_Division__c', personnel.compliancequest__Org_Division__c);
                newRow.set('compliancequest__Org_Business_Unit__c', personnel.compliancequest__Org_Business_Unit__c);
                newRow.set('compliancequest__Org_Site__c', personnel.compliancequest__Org_Site__c);
                newRow.set('compliancequest__Org_Region__c', personnel.compliancequest__Org_Region__c);
            });
        })(window.sqx.Events);

    </script>
</apex:component>