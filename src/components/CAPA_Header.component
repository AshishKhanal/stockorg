<apex:component >

    <script type="text/javascript">
    function setupFlowIndicator() {
                    //********** Flow indicator section  ***************

                    var validStates = {
                        orderedList: ['close', 'initiate', 'ack', 'contain', 'investigate', 'approveInvestigation', 'implement', 'approveImplementation', 'verifyEffectiveness'],
                        showOnTop: ['close', 'initiate', 'approveInvestigation', 'approveImplementation', 'verifyEffectiveness'],
                        showOnBottom: ['ack', 'contain', 'investigate', 'implement'],
                        lastState:['close'],
                        stateEvaluator: {

                            /*  complete: capa-Status is closed
                                NotRequired: not applicable
                                Active: IF capa status is complete and Effectiveness Review not required
                                TBD: capa-status is not closed 
                            */
                            close: function (capaRecord, status, Finding) {
                                return (capaRecord.get('compliancequest__Status__c') == 'Closed') ? status.complete
                                        :(capaRecord.get('compliancequest__Status__c') == 'Complete' && !Finding.get('compliancequest__Effectiveness_Review_Required__c') && allEffReviewComplete(capaRecord)) ? status.active
                                        :status.TBD;
                            },

                            /*  Complete: capa-status is not draft
                                NotRequired: not appplicable
                                Active: capa status is draft
                                TBD: not applicable
                            */
                            initiate: function (capaRecord, status, Finding) {
                                return capaRecord.get('compliancequest__Status__c') == 'Draft' ? status.active 
                                        : status.complete;
                            },

                            /*  Complete: Initiate is complete and Finding has_Response = true 
                                NotRequired: Initiate is complete and Finding Response is not required
                                Active: Initiate is complete
                                TBD: above all are false
                            */
                            ack: function (capaRecord, status, Finding) {
                                return  (capaRecord.states.initiate == status.complete && Finding.get('compliancequest__Has_Response__c')) ? status.complete
                                        :(capaRecord.states.initiate == status.complete && !Finding.get('compliancequest__Response_Required__c')) ? status.notRequired
                                        :(capaRecord.states.close == status.complete) ? ''
                                        :(capaRecord.states.initiate == status.complete) ? status.active 
                                        : status.TBD;
                            },

                            /*  Complete:  finding has containment 
                                NotRequired: Initiate is Complete and Finding Containment is not required
                                Active: (Ack is Complete or NotRequired)
                                TBD: above all are false
                            */
                            contain: function (capaRecord, status, Finding) {
                                return  (Finding.get('compliancequest__Has_Containment__c')) ? status.complete
                                        :((capaRecord.states.ack == status.complete || capaRecord.states.ack == status.notRequired) && !Finding.get('compliancequest__Containment_Required__c')) ? status.notRequired
                                        :(capaRecord.states.close == status.complete) ? ''
                                        :(capaRecord.states.ack == status.complete || capaRecord.states.ack == status.notRequired) ? status.active 
                                        : status.TBD;
                            },

                            /*  Complete: Finding has_investigation==true
                                NotRequired: Initiate is Complete and Investigation is not required 
                                Active: Containment is Complete or NotRequired
                                TBD: above all are false
                            */
                            investigate: function (capaRecord, status, Finding) {
                                return  ((capaRecord.states.contain == status.complete || capaRecord.states.contain == status.notRequired) && approveInvestigationComplete(Finding)) ? status.complete
                                        :((capaRecord.states.contain == status.complete || capaRecord.states.contain == status.notRequired) && !Finding.get('compliancequest__Investigation_Required__c')) ? status.notRequired
                                        :(capaRecord.states.close == status.complete) ? ''
                                        :(capaRecord.states.contain == status.complete || capaRecord.states.contain == status.notRequired) ? status.active 
                                        : status.TBD;
                            },

                            /*  NotRequired: finding investigation approval is not required 
                                Complete:  (If (investigation is required) then has_investigation = true)
                                            && (If (CA is required) then has_Correction_Action= true) 
                                            && (IF (PA is required) then has_Preventive_Action=true)
                                                //Note: hasApprovedInvestigation cannot be used because it is flagged true even when one of the 
                                                //many required investigation is approved
                                Active: If any Response is InApproval and #of Investigation > 0
                                TBD: above all are false
                            */
                            approveInvestigation: function (capaRecord, status, Finding) {
                                return  ((capaRecord.states.initiate == status.complete) && numberOfInvestigationsInApproval(Finding)>0) ? status.active
                                        :((capaRecord.states.contain == status.complete || capaRecord.states.contain == status.notRequired) && !Finding.get('compliancequest__Investigation_Approval__c')) ? status.notRequired
                                        :(approveInvestigationComplete(Finding) && numberOfInvestigationsInApproval(Finding)==0) ? status.complete
                                        :(capaRecord.states.close == status.complete) ? ''
                                        : status.TBD;
                            },

                            /*  Complete: capa status is complete
                                NotRequired: (Approve Investigation is complete or NotRequired) and investigation is NotRequired and has no approved action plans
                                Active: (Approve Investigation Complete or NotRequired ) and has approved action plans
                                TBD: above all are false
                            */
                            implement: function (capaRecord, status, Finding) {
                                return  ((capaRecord.states.investigate == status.complete || capaRecord.states.investigate == status.notRequired) && !Finding.get('compliancequest__Investigation_Required__c') && approvedActionPlansCount(Finding)==0 ) ? status.notRequired
                                        :(capaRecord.get('compliancequest__Status__c') == 'Complete'||(capaRecord.get('compliancequest__Status__c') == 'Closed') && capaRecord.get('compliancequest__Is_CAPA_Ready_For_Completion__c')) ? status.complete
                                        :(capaRecord.states.close == status.complete) ? ''
                                        :((capaRecord.states.investigate == status.complete || capaRecord.states.investigate == status.notRequired) && approvedActionPlansCount(Finding)>0 )? status.active 
                                        : status.TBD;
                            },

                            /*  NotRequired: (Approve Investigation is complete or NotRequired) and {if (Pre and Post approval are not required) or ((if Pre or Post approval are required) and if number of actions =0)}
                                Complete: (When Capa Status is Complete) or (capa status is closed and Is_CAPA_Ready_For_Completion__c is flagged true)
                                Active:  If any Response is InApproval and #of Actions > 0 
                                TBD: above all are false
                            */
                            approveImplementation: function (capaRecord, status, Finding) {
                                return  ((capaRecord.states.approveInvestigation == status.complete || capaRecord.states.approveInvestigation == status.notRequired) && numberOfActionsInApproval(Finding)>0) ? status.active 
                                        :((capaRecord.states.approveInvestigation == status.complete || capaRecord.states.approveInvestigation == status.notRequired || capaRecord.states.approveInvestigation == "") && ((!capaRecord.get('compliancequest__Pre_Approval_Required__c') && !capaRecord.get('compliancequest__Post_Approval_Required__c')) || ((capaRecord.get('compliancequest__Pre_Approval_Required__c') || capaRecord.get('compliancequest__Post_Approval_Required__c')) && capaRecord.relatedList('compliancequest__SQX_Actions__r').view().length==0)))?status.notRequired
                                        :(capaRecord.get('compliancequest__Status__c') == 'Complete'||(capaRecord.get('compliancequest__Status__c') == 'Closed') && capaRecord.get('compliancequest__Is_CAPA_Ready_For_Completion__c')) ? status.complete
                                        :(capaRecord.states.close == status.complete) ? ''                         
                                        : status.TBD;
                            },

                            /*  Complete: capa status is closed and finding has a effectiveness review
                                Active: (capa status complete and eff review required) or ( capa status complete and finding has incomplete eff review)
                                NotRequired: needs eff review is false and finding has all eff review complete
                                TBD: above all are false 
                            */
                            verifyEffectiveness: function (capaRecord, status, Finding) {
                                return  (capaRecord.get('compliancequest__Status__c') == 'Closed' && capaRecord.relatedList('compliancequest__SQX_Effectiveness_Review__r').view().length>0) ? status.complete
                                        :((capaRecord.get('compliancequest__Status__c') == 'Complete') && !Finding.get('compliancequest__Effectiveness_Review_Required__c') && allEffReviewComplete(capaRecord))?status.notRequired
                                        :(capaRecord.states.close == status.complete) ? ''
                                        :((capaRecord.get('compliancequest__Status__c') == 'Complete' && Finding.get('compliancequest__Effectiveness_Review_Required__c') )|| (capaRecord.get('compliancequest__Status__c') == 'Complete' && !allEffReviewComplete(capaRecord))) ? status.active 
                                        : status.TBD;
                            }

                        }
                    };

                    function initializeStates(capaRecord, validStates) {

                        if (capaRecord.states === undefined) {
                            capaRecord.states = {};

                            var status = {};
                            status.active = 'active';
                            status.complete = 'complete';
                            status.TBD = 'TBD';
                            status.notRequired = 'notRequired';

                            var Finding = capaRecord.relatedList('compliancequest__SQX_Findings__r').at(0);

                            for (var i = 0; i < validStates.orderedList.length; i++) {
                                capaRecord.states[validStates.orderedList[i]] = validStates.stateEvaluator[validStates.orderedList[i]](capaRecord, status, Finding);
                            }

                        }
                    }

                    function numberOfActionsInApproval(Finding)
                    {
                        var numberOfActionsInApproval= 0;
                        var responses= Finding.relatedList('compliancequest__SQX_Responses__r').view();
                        for(var i=0; i<responses.length; i++)
                        {
                            var currentResponse=responses[i];
                            if(currentResponse.compliancequest__Approval_Status__c=='Pending')
                            {
                                numberOfActionsInApproval+=currentResponse.compliancequest__Number_of_Actions__c;
                            }
                        }
                        return numberOfActionsInApproval;
                    }

                    function numberOfInvestigationsInApproval(Finding)
                    {
                        var numberOfInvestigationsInApproval= 0;
                        var responses= Finding.relatedList('compliancequest__SQX_Responses__r').view();
                        for(var i=0; i<responses.length; i++)
                        {
                            var currentResponse=responses[i];
                            if(currentResponse.compliancequest__Approval_Status__c=='Pending')
                            {
                                numberOfInvestigationsInApproval+=currentResponse.compliancequest__Number_of_Investigations__c;
                            }
                        }

                        return numberOfInvestigationsInApproval;
                    }

                    function approveInvestigationComplete(Finding)
                    {
                        complete= true;
                        if(Finding.get('compliancequest__Investigation_Required__c'))
                        {
                            if(!Finding.get('compliancequest__Has_Investigation__c'))
                                complete=false;
                        }
                        if(Finding.get('compliancequest__Corrective_Action_Required__c'))
                        {
                            if(!Finding.get('compliancequest__Has_Corrective_Action__c'))
                                complete=false;
                        }
                        if(Finding.get('compliancequest__Preventive_Action_Required__c'))
                        {
                            if(!Finding.get('compliancequest__Has_Preventive_Action__c'))
                                complete=false;
                        }
                        if(!Finding.get('compliancequest__Investigation_Required__c') && !Finding.get('compliancequest__Corrective_Action_Required__c') && !Finding.get('compliancequest__Corrective_Action_Required__c'))
                        {
                            if(!Finding.get('compliancequest__Has_Investigation__c'))
                                complete=false;
                        }
                        return complete;
                    }

                    SQXSchema.compliancequest__SQX_CAPA__c.validStates = [];

                    for (var i = 0; i <= validStates.orderedList.length; i++) {
                        var currentValidState = validStates.orderedList[i];
                        var showOnTop = validStates.showOnTop.indexOf(currentValidState, 0);
                        var lastState=validStates.lastState.indexOf(currentValidState, 0);

                        SQXSchema.compliancequest__SQX_CAPA__c['state__' + validStates.orderedList[i]] = function (currentValidState, showOnTop, lastState) {
                            return function () {

                                var className = '';
                                if (showOnTop != -1)
                                    className = 'cqFlowStep top ';
                                else
                                    className = 'cqFlowStep bottom ';
                                initializeStates(this, validStates);

                                className += this.states[currentValidState];

                                if(lastState!=-1)
                                    className += ' last';
                                return className;
                            }
                        }(currentValidState, showOnTop, lastState);
                    }

                    function approvedActionPlansCount(Finding)
                    {
                       var numberOfApprovedActionPlans=0;
                       var investigations= Finding.relatedList('compliancequest__SQX_Investigations__r').view();

                       for (var i=0; i<investigations.length; i++)
                       {
                            var currentInvestigation= investigations[i];
                            if(currentInvestigation.compliancequest__Approval_Status__c=='Approved' || currentInvestigation.compliancequest__Approval_Status__c=='-')// should check condn in which approval is not required, in such cases approval status is blank
                            {
                                numberOfApprovedActionPlans+=currentInvestigation.compliancequest__Total_Number_Of_Corrective_Action__c;
                                numberOfApprovedActionPlans+=currentInvestigation.compliancequest__Total_Number_Of_Preventive_Action__c;
                            }
                       }
                       return numberOfApprovedActionPlans;
                    }

                    function allActionsComplete(capaRecord) {
                        var allActionComplete = true;
                        if (capaRecord.compliancequest__SQX_Actions__r.length == 0) {
                            if (!capaRecord.compliancequest__SQX_Findings__r[0].compliancequest__Corrective_Action_Required__c && !capaRecord.compliancequest__SQX_Findings__r[0].compliancequest__Corrective_Action_Required__c)
                                allActionComplete = false;
                        }
                        for (var i = 0; i < capaRecord.compliancequest__SQX_Actions__r.length; i++) {
                            var action = capaRecord.compliancequest__SQX_Actions__r[i];
                            if (action.compliancequest__Status__c != 'Complete') {
                                allActionComplete = false;
                                break;
                            }
                        }
                        return allActionComplete;
                    }

                    function allEffReviewComplete(capaRecord)
                    {
                        var allEffReviewComplete=true;
                        for (var i=0; i<capaRecord.relatedList('compliancequest__SQX_Effectiveness_Review__r').view().length; i++)
                        {
                            var currentEffReview= capaRecord.relatedList('compliancequest__SQX_Effectiveness_Review__r').view()[i];
                            if(currentEffReview.compliancequest__Status__c!='Complete')
                            {
                                allEffReviewComplete= false;
                                break;
                            }
                        }
                        return allEffReviewComplete;
                    }

                    //**********-------------*************
                }
    setupFlowIndicator();
    </script>

    <div id="headerSection" class="cqHeaderBlock">
        <div class="cqRecordIdentifier">
            <span>CAPA</span>
            <h1 data-bind="text:Name"></h1>
        </div>
        <div class="cqFlowIndicator">
            <table>
                <colgroup>
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="*" />
                </colgroup>
                <tr>
                    <td data-bind="attr: { class: state__initiate }">Initiate</td>
                    <td class="cqFlowStepTop top empty">&nbsp;</td>
                    <td class="cqFlowStepTop top empty">&nbsp;</td>
                    <td class="cqFlowStepTop top empty">&nbsp;</td>
                    <td data-bind="attr:{class: state__approveInvestigation }">Approve Investigation</td>
                    <td class="cqFlowStep  top empty">&nbsp;</td>
                    <td data-bind="attr:{class: state__approveImplementation }">Approve Implementation</td>
                    <td data-bind="attr:{class: state__verifyEffectiveness }">Verify Effectiveness</td>
                    <td data-bind="attr:{class: state__close" class="last"><div class="icon"></div><div class="text">Close</div></td>
                </tr>
                <tr>
                    <td class="cqFlowStep bottom empty">&nbsp;</td>
                    <td data-bind="attr:{class: state__ack }">Acknowledge</td>
                    <td data-bind="attr:{class: state__contain }">Contain</td>
                    <td data-bind="attr:{class: state__investigate }">Investigate</td>
                    <td class="cqFlowStep bottom empty">&nbsp;</td>
                    <td data-bind="attr:{class: state__implement }" colspan="4">Implement</td>
                </tr>
            </table>
        </div>
        <div class="clearFix"></div>
        <div id="headerRecordInfo" class="cqRecordHeader" data-bind="visible:isExistingObject()">
            <div class="section3col">
                <div data-container="Capa.compliancequest__CAPA_Type__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__Issued_Date__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__Status__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__External_Reference_Number__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__Target_Due_Date__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__Resolution__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__SQX_Account__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__SQX_External_Contact__c" data-read-only="true" data-role='cqcompositefield'></div>
                <div data-container="Capa.compliancequest__Rating__c" data-read-only="true" data-role="cqcompositefield"></div>
            </div>
        </div>
        <div class="clearFix"></div>
    </div>

    <script type="text/x-kendo-template" id="attachmentOnlyTemplate">
        <div data-role="cqtabstrip" data-collapsible="true" data-show-grid-title="true" data-show-grid-count="true" class="tabStrip">
            <ul>
                <li class="k-state-active" data-grid-sobject="Attachment">Attachments</li>
            </ul>
            <div>
                <div class="Attachment">
                    <div class="subGrid"
                         data-relatedlist="Attachments"
                         data-toolbar='[{"name" : "create"}]'
                         data-save="attachmentSaveCalled"
                         data-options='{"editable":{"mode":"popup","update":#= data.rowEditable ? data.rowEditable() : true#,"create":#= data.rowEditable ? data.rowEditable() : true #,"destroy":#= data.rowEditable ? data.rowEditable() : true #, "template": "#= getAttachmentTemplate(data) #" },"showTitle":false,"collapsible":false}'
                         data-columns='[
                                            {"title": "Actions", "command" : [{ "name": "edit", "text" : { "update": "Save"} } , "destroy" ], "width": "100px"},
                                            {"field" : "Name", "template": "\\#= buildAttachmentViewLink(data) \\#"},
                                            {"field" : "Description"}
                                            ]'>
                    </div>
                </div>
            </div>
        </div>
    </script>



</apex:component>