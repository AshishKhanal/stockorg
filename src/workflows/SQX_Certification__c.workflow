<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CQ_Send_Email_To_CQ_Account_Owner_When_Supplier_Document_Has_Expired</fullName>
        <description>CQ Send Email To CQ Account Owner When Supplier Document Has Expired</description>
        <protected>false</protected>
        <recipients>
            <field>CQ_Account_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/CQ_SupplierDocument_Expired_Notification</template>
    </alerts>
    <alerts>
        <fullName>CQ_Send_Renewal_Email_To_CQ_Account_Owner_When_Document_Is_Due_For_Expiration</fullName>
        <description>CQ Send Renewal Email To CQ Account Owner When Document Is Due For Expiration</description>
        <protected>false</protected>
        <recipients>
            <field>CQ_Account_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/CQ_SupplierDocument_Renewal_Notification</template>
    </alerts>
</Workflow>
