<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_actual_disposition_to_total_disposed</fullName>
        <description>Set the actual disposed to total disposed.</description>
        <field>Actual_Quantity_Disposed__c</field>
        <formula>Disposition_Quantity__c</formula>
        <name>Set actual disposition to total disposed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set actual disposed to total disposed</fullName>
        <actions>
            <name>Set_actual_disposition_to_total_disposed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Disposition__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>this rule copies the total disposed qty to actual disposed qty.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
