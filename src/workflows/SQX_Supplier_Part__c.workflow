<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_SuppliePartRiskLevel_on_Create</fullName>
         <description>Updates the supplier part risk level from part, for use in rollup in account</description>
        <field>Supplier_Part_Risk_Level__c</field>
        <formula>Part__r.Part_Risk_Level__c</formula>
        <name>Update SuppliePartRiskLevel on Create</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assist RollUp on Risk Level</fullName>
        <actions>
            <name>Update_SuppliePartRiskLevel_on_Create</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
         <description>Updates the part risk level on supplier part based on part selection</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
