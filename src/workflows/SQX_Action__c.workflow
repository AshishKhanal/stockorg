<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_email_to_Action_Assignee</fullName>
        <description>Notification email to Action Assignee.</description>
        <protected>true</protected>
        <recipients>
            <field>SQX_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/SQX_Email_to_Action_Assignee</template>
    </alerts>
    <rules>
        <fullName>Email Notification to Action Assignee</fullName>
        <actions>
            <name>Notification_email_to_Action_Assignee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send an email notification to the Assignee when an Action is assigned to them.</description>
        <formula>/*
If the action is open and it is newly saved or assignee is changed.
*/
AND( ISPICKVAL(Status__c, &apos;Open&apos;),
     OR ( 
           ISNEW(),
           ISCHANGED(SQX_User__c),
           ISCHANGED(Status__c)
        )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
