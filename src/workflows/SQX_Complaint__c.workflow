<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>This_will_send_the_email_to_user_to_take_the_ownership_of_the_complaint</fullName>
        <description>This will send the email to user to take the ownership of the complaint.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/Email_When_Complaint_Is_Created</template>
    </alerts>
    <rules>
        <fullName>Email_When_Complaint_is_Changed_From_Draft_To_Triage</fullName>
        <actions>
            <name>This_will_send_the_email_to_user_to_take_the_ownership_of_the_complaint</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Complaint__c.Record_Stage__c</field>
            <operation>equals</operation>
            <value>Triage</value>
        </criteriaItems>
        <description>This workflow is used to send the email when the complaint is send for ownership.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
