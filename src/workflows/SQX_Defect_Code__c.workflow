<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Level_to_Match_Parent</fullName>
        <description>Updates level to match parent defect code</description>
        <field>Level__c</field>
        <formula>if (isblank( SQX_Parent_Code__c),0, SQX_Parent_Code__r.Level__c +1)</formula>
        <name>Update Level to Match Parent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CQ Set Hierarchy Level</fullName>
        <actions>
            <name>Update_Level_to_Match_Parent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Hierarchy Level in defect code taking reference from parent code</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
