<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_Send_email_to_Auditeee_Contact_when_Audit_is_Completed</fullName>
        <description>Send email to Auditeee Contact when Audit is Completed</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Auditee_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/SQX_Send_email_when_audit_is_completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Copy_Checklist_Flag</fullName>
        <description>Field update to set copy checklist flag.</description>
        <field>Copy_Checklist__c</field>
        <literalValue>1</literalValue>
        <name>Set Copy Checklist Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Set_Copy_Audit_Checklist_Flag</fullName>
        <actions>
            <name>Set_Copy_Checklist_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>SQX_Audit__c.Stage__c</field>
            <operation>equals</operation>
            <value>Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Audit__c.Stage__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>This rule sets the copy audit checklist flag when the audit is set to required status.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SQX_Send_Email_When_Audit_is_Completed</fullName>
        <actions>
            <name>SQX_Send_email_to_Auditeee_Contact_when_Audit_is_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Audit__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Send email to auditee contact when audit is completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
