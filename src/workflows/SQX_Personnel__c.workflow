<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Unique_User_Constraint</fullName>
        <description>Sets unique value in the field Unique_User_Constraint.</description>
        <field>Unique_User_Constraint__c</field>
        <formula>IF( ISBLANK( SQX_User__c ),
     Identification_Number__c,
     CASESAFEID(SQX_User__c)
)</formula>
        <name>Set Unique User Constraint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set_Unique_User_Constraint</fullName>
        <actions>
            <name>Set_Unique_User_Constraint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ensures duplicate record is not created for same salesforce user.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
