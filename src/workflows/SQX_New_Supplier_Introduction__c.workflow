<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_NSI_Record_status_is_complete_Email_Alert</fullName>
        <description>SQX NSI Record status is complete Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Owner_When_NSI_status_is_complete</template>
    </alerts>
    <rules>
        <fullName>CQ_Supplier_Introduction_Send_Email_When_NSI_Status_Is_Complete</fullName>
        <actions>
            <name>SQX_NSI_Record_status_is_complete_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_New_Supplier_Introduction__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Send email to owner when NSI status is complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
