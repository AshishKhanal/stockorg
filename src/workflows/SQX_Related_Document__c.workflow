<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_the_uniqueness_constraint</fullName>
        <description>Set the value of Uniqueness Constraint to combination of Controlled and Referring doc.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID(Controlled_Document__c) +  CASESAFEID(Referenced_Document__c)</formula>
        <name>Set the uniqueness constraint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SQX Set Uniqueness Constraint</fullName>
        <actions>
            <name>Set_the_uniqueness_constraint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule sets the value of uniqueness constraint to the combination of controlled document and referring document.</description>
        <formula>OR (ISNEW(), ISCHANGED( Controlled_Document__c ), ISCHANGED( Referenced_Document__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
