<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Defect_Code_To_Master_Obj</fullName>
        <description>Set defect code to the name of defect code object's name</description>
        <field>Defect_Code__c</field>
        <formula>SQX_Defect_Code__r.Name</formula>
        <name>Set Defect Code To Master Obj</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_defect_category_to_master_objects</fullName>
        <description>copies the defect category from lookup if present</description>
        <field>Defect_Category__c</field>
        <formula>SQX_Defect_Code__r.Defect_Category__c</formula>
        <name>Set defect category to master objects</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Defect Code And Category From Master Table</fullName>
        <actions>
            <name>Set_Defect_Code_To_Master_Obj</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_defect_category_to_master_objects</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>copies the defect code and category from master table if lookup is selected.</description>
        <formula>NOT(ISBLANK(SQX_Defect_Code__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
