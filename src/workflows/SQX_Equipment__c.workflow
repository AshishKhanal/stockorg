<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_email_to_Equipment_owner_when_next_calibration_is_past_due_date</fullName>
        <description>CQ Send an email to Equipment owner when next calibration is past due date.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_eqp_is_past_calibration_date</template>
    </alerts>
</Workflow>
