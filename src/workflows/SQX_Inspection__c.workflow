<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_For_Closure_Reviewer_Of_Completed_Inspection</fullName>
        <description>Email Alert for closure reviewer when inspection is complete</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Closure_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Inspection_Review_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Draft_Inspection_To_Owner</fullName>
        <description>Email Alert for inspection to owner when inspection is in draft</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Draft_Inspection_Email</template>
    </alerts>
    <rules>
        <fullName>Send Email to Inspection Owner When Inspection Is In Draft</fullName>
        <actions>
            <name>Email_Alert_For_Draft_Inspection_To_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Inspection__c.Status__c</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>Sends an email to inspection record owner when the record moves to Draft status</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to Reviewer When Inspection Is Complete</fullName>
        <actions>
            <name>Email_Alert_For_Closure_Reviewer_Of_Completed_Inspection</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email to inspection reviewer when the inspection record having review requirement is complete</description>
        <formula>AND(
    ISPICKVAL(Status__c,&apos;Complete&apos;),
    Review_Required__c,
    OR
    (
        AND(
            ISCHANGED(Approval_Status__c),
            ISPICKVAL(Approval_Status__c, &apos;In Review&apos;)
        ),
        ISCHANGED(SQX_Closure_Reviewer__c)
    ) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
