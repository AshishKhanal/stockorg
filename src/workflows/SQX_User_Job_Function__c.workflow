<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Uniqueness_Constraint_Value</fullName>
        <description>Update/set the Uniqueness Constraint value with combination of Job Function, User and Deactivation Date.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID( SQX_User__c )
+ CASESAFEID( SQX_Job_Function__c )
+ IF( Active__c = false &amp;&amp; NOT ISNULL( Activation_Date__c ), Name, &apos;&apos;)</formula>
        <name>Set_Uniqueness_Constraint_Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_User_Job_Function_Activation_Date</fullName>
        <description>When user activates a User Job Function, populate activation date with today&apos;s date.</description>
        <field>Activation_Date__c</field>
        <formula>Today()</formula>
        <name>Set User Job Function Activation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_User_Job_Function_Deactivation_Date</fullName>
        <description>When user deactivates a User Job Function, populate deactivation date with today&apos;s date.</description>
        <field>Deactivation_Date__c</field>
        <formula>Today()</formula>
        <name>Set User Job Function Deactivation Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set_Uniqueness_Constraint_Value</fullName>
        <actions>
            <name>Set_Uniqueness_Constraint_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.SQX_User__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the value of Uniqueness Constraint with Job Function, User and Deactivation Date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set_User_Job_Function_Activation_Date</fullName>
        <actions>
            <name>Set_User_Job_Function_Activation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.Activation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When user activates a User Job Function, populate activation date with today&apos;s date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set_User_Job_Function_Deactivation_Date</fullName>
        <actions>
            <name>Set_User_Job_Function_Deactivation_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.Activation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_User_Job_Function__c.Deactivation_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When user deactivates a User Job Function, populate deactivation date with today&apos;s date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
