<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SQX_Clear_the_next_review_date</fullName>
        <description>Clear the next review date</description>
        <field>Next_Review_Date__c</field>
        <name>Clear the next review date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <targetObject>Controlled_Document__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Target_Review_Date_From_Document</fullName>
        <description>Copies Next Review Date of the related Controlled Document.</description>
        <field>Target_Review_Date__c</field>
        <formula>Controlled_Document__r.Next_Review_Date__c</formula>
        <name>Set Target Review Date From Document</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_controlled_docs_last_review_date</fullName>
        <description>This update sets the controlled documents last review date to review performed date.</description>
        <field>Last_Review_Date__c</field>
        <formula>Performed_Date__c</formula>
        <name>Set controlled docs last review date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
        <targetObject>Controlled_Document__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_doc_status_to_obsolete</fullName>
        <description>Sets the controlled document&apos;s status to obsolete.</description>
        <field>Document_Status__c</field>
        <literalValue>Obsolete</literalValue>
        <name>Set doc status to obsolete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Controlled_Document__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_next_review_date_from_review</fullName>
        <description>This update copies the next review date from review to controlled document. If the review decision is to retire clears the next review date.</description>
        <field>Next_Review_Date__c</field>
        <formula>Next_Review_Date__c</formula>
        <name>Set next review date from review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
        <targetObject>Controlled_Document__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unset_the_queue_review_task</fullName>
        <description>Unset the queue review task to set a workflow time trigger.</description>
        <field>Queue_Review_Task__c</field>
        <literalValue>0</literalValue>
        <name>Unset the queue review task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Controlled_Document__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>SQX_Update_Controlled_Doc_When_Retire</fullName>
        <actions>
            <name>SQX_Clear_the_next_review_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_doc_status_to_obsolete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SQX_Document_Review__c.Review_Decision__c</field>
            <operation>equals</operation>
            <value>Retire</value>
        </criteriaItems>
        <description>Performs field updates on controlled document when a review with a review decision &apos;Retire&apos; is added.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When a document review is created push next review</fullName>
        <actions>
            <name>Set_Target_Review_Date_From_Document</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_controlled_docs_last_review_date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_next_review_date_from_review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Unset_the_queue_review_task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Target Review Date of Document Review from Controlled Document and push next review date to Controlled Document when document review is created.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
