<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Unique_Combo_of_FindingReference</fullName>
        <description>Set Unique Combination of Finding and its reference</description>
        <field>Unique_Finding_Reference__c</field>
        <formula>IF( Resolves_by_CAPA__c == true, ( CASESAFEID(SQX_Finding__c) + &apos;true&apos;) , ( CASESAFEID(SQX_Finding__c) +  CASESAFEID(SQX_CAPA__c) + &apos;false&apos;))</formula>
        <name>Set Unique Combo of FindingReference</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Uniqueness Constraint of Finding CAPA Refrence</fullName>
        <actions>
            <name>Set_Unique_Combo_of_FindingReference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Unique Combination of Finding and its reference</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
