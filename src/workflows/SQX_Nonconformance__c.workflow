<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_email_to_NCs_new_owner_when_NC_is_submitted_for_review</fullName>
        <description>Send an email to NCs new owner when NC is submitted for review</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_When_NC_is_submitted</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_close_date_to_current_date</fullName>
        <description>This rule sets the NC&apos;s close date to the current date when NC is closed.</description>
        <field>Close_Date__c</field>
        <formula>Today()</formula>
        <name>Set close date to current date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_open_date_to_current_date</fullName>
        <description>this field update sets the open date of the NC to current date, when NC is opened.</description>
        <field>Open_Date__c</field>
        <formula>Today()</formula>
        <name>Set open date to current date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SQX_Copy_department_value</fullName>
        <description>Copy department values from SQX_Department__c(new department field) to Department__c(old department field). This will make old report usable without changing fields</description>
        <field>Department__c</field>
        <formula>SQX_Department__r.Name</formula>
        <name>Copy department value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fill in date when NC is opened</fullName>
        <actions>
            <name>Set_open_date_to_current_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Nonconformance__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>this rule fills in Opened date when NC is opened.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email when non-conformance is submitted</fullName>
        <actions>
            <name>Send_an_email_to_NCs_new_owner_when_NC_is_submitted_for_review</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow rule sends an email to the new owner of a Nonconformance, when it is submitted. So, if the owner hasn&apos;t changed no email is send.</description>
        <formula>AND( ISPICKVAL( Status__c , &apos;Triage&apos;),ISCHANGED(Status__c ),  ISCHANGED( OwnerId ) ) /* only send out an email if status has changed to Triage i.e has been submitted to the user and Owner has changed i.e. it is being assigned to a queue or a new user.*/</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set close date to current date</fullName>
        <actions>
            <name>Set_close_date_to_current_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Nonconformance__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This rule sets the close date of an NC to current date when it is closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SQX_Copy_department_value</fullName>
        <actions>
            <name>SQX_Copy_department_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copy department values from SQX_Department__c(new department field) to Department__c(old department field). This will make old report usable without changing fields</description>
        <formula>OR(ISNEW(), ISCHANGED( SQX_Department__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
