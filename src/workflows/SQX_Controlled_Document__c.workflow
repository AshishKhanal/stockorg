<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_email_to_Controlled_Document_owner</fullName>
        <description>Notification email to Controlled Document owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Controlled_Document_Owner</template>
    </alerts>
    <fieldUpdates>
        <fullName>Expire_Date</fullName>
        <description>When Document is Obsolete and Expire date is not specified, then Expire Date Should be provided</description>
        <field>Expiration_Date__c</field>
        <formula>TODAY()</formula>
        <name>Expire Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Effective_Date</fullName>
        <description>This field update will set the effective date of docs</description>
        <field>Effective_Date__c</field>
        <formula>IF( ISNULL( Effective_Date__c ),  Today() + IF(ISBLANK(Duration__c), 0 , Duration__c), 
Effective_Date__c )</formula>
        <name>Set Effective Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Issued_Date_To_Today</fullName>
        <description>This field update will set the issued date to today</description>
        <field>Date_Issued__c</field>
        <formula>Today()</formula>
        <name>Set Issued Date To Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Review_Date</fullName>
        <description>This update will set the next review date of the doc</description>
        <field>Next_Review_Date__c</field>
        <formula>IF(OR(!ISBLANK(Next_Review_Date__c),Review_Interval__c&lt;=0 ) ,  Next_Review_Date__c , Today() +Review_Interval__c  )</formula>
        <name>Set Next Review Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_To_Current</fullName>
        <description>This field update will set the status to pre-release if it meets the criteria. Previously it set the status to current</description>
        <field>Document_Status__c</field>
        <literalValue>Pre-Release</literalValue>
        <name>Set Status To Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sync_Stauts_Out_Of_Sync</fullName>
        <description>Sets Synchronization Status field to Out of Sync</description>
        <field>Synchronization_Status__c</field>
        <literalValue>Out of Sync</literalValue>
        <name>Set Sync Stauts Out Of Sync</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Batch_Job_Sts_to_Pre_releas_Queue</fullName>
        <description>updates batch job status to pre-release queued</description>
        <field>Batch_Job_Status__c</field>
        <literalValue>Pre-Release Queued</literalValue>
        <name>Update Batch Job Sts to Pre-releas Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change_Batch_Job_Status_To_Pre-Release_Queued</fullName>
        <actions>
            <name>Update_Batch_Job_Sts_to_Pre_releas_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field change the batch job status  to release queued if the status is approved and auto release is on.</description>
        <formula>AND( Auto_Release__c  = true, ISPICKVAL( Document_Status__c , &apos;Approved&apos;) , $Setup.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c = true)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change_Status_To_Current</fullName>
        <actions>
            <name>Set_Status_To_Current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field change the status to current if the status is approved and auto release is on.</description>
        <formula>AND( Auto_Release__c  = true, ISPICKVAL( Document_Status__c , &apos;Approved&apos;) , $Setup.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c = false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email_Notification_to_Controlled_Document_Owner</fullName>
        <actions>
            <name>Notification_email_to_Controlled_Document_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Send_Notification_Email_On_Sync__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Synchronization_Status__c</field>
            <operation>equals</operation>
            <value>In Sync</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Synchronization_Status__c</field>
            <operation>equals</operation>
            <value>Not Supported</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Synchronization_Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <description>Send an email to owner when document rendition is complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expire_Date_Should_Be_Updated</fullName>
        <actions>
            <name>Expire_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Expiration_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Document_Status__c</field>
            <operation>equals</operation>
            <value>Obsolete</value>
        </criteriaItems>
        <description>The Expiration date should be updated when the document is Obsolete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set_Date_When_Status_Is_Current</fullName>
        <actions>
            <name>Set_Effective_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Issued_Date_To_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Next_Review_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SQX_Controlled_Document__c.Document_Status__c</field>
            <operation>equals</operation>
            <value>Pre-Release</value>
        </criteriaItems>
        <description>This rule will set the issue date, effective date and next review date when the doc is released.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set_Sync_Status_Out_Of_Sync_On_Field_Change</fullName>
        <actions>
            <name>Set_Sync_Stauts_Out_Of_Sync</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule sets synchronization status to Out of Sync if white listed fields(Secondary_Format_Setting , Document_Status, Document_Number__c, Revision__c, Effective_Date__c, Expiration_Date__c, 
OwnerId, SQX_Business_Unit__c, SQX_Site__c, Title__c) change.</description>
        <formula>AND(

    ISPICKVAL( Secondary_Content__c, &apos;Auto&apos; ),
    NOT(Is_Scorm_Content__c),
    OR(

        ISCHANGED( Secondary_Format_Setting__c ) ,

        TEXT( PRIORVALUE( Document_Status__c ) ) &lt;&gt; TEXT( Document_Status__c ),
        TEXT( PRIORVALUE( Effective_Date__c ) ) &lt;&gt; TEXT( Effective_Date__c ), 
        TEXT( PRIORVALUE( Expiration_Date__c ) ) &lt;&gt; TEXT( Expiration_Date__c ),
        PRIORVALUE( Document_Number__c ) &lt;&gt; Document_Number__c, 
        PRIORVALUE( Revision__c ) &lt;&gt; Revision__c, 
        PRIORVALUE( OwnerId ) &lt;&gt; OwnerId, 
        PRIORVALUE( SQX_Business_Unit__c ) &lt;&gt; SQX_Business_Unit__c, 
        PRIORVALUE( SQX_Site__c ) &lt;&gt; SQX_Site__c, 
        PRIORVALUE( SQX_Division__c ) &lt;&gt; SQX_Division__c, 
        PRIORVALUE( SQX_Department__c ) &lt;&gt; SQX_Department__c, 
        PRIORVALUE( Title__c ) &lt;&gt; Title__c
    )

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
