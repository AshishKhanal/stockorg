<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_Send_email_when_audit_response_get_completed_directly</fullName>
        <description>SQX Send email when audit response get completed directly</description>
        <protected>false</protected>
        <recipients>
            <field>Response_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_when_audit_response_published</template>
    </alerts>
    <rules>
        <fullName>SQX-Send out a message when audit response gets published directly</fullName>
        <actions>
            <name>SQX_Send_email_when_audit_response_get_completed_directly</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email when audit response get submitted directly without approval</description>
        <formula>AND(ISPICKVAL(Status__c, &apos;Completed&apos;), ISPICKVAL(PRIORVALUE( Status__c ) , &apos;Draft&apos;)) 
/* Only those responses that are completed directly should trigger an email, because those that go for approval do send out an email.*/</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
