<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_SD_Record_is_Complete_Email_Alert</fullName>
        <description>SQX SD Record is Complete Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_To_Owner_When_SD_In_Complete_Status</template>
    </alerts>
    <alerts>
        <fullName>SQX_SD_Rejection_Email_Alert_to_Record_Creator</fullName>
        <description>SQX SD Rejection Email Alert to Record Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SD_Rejection_Email_to_Record_Creator</template>
    </alerts>
        <alerts>
        <fullName>SQX_Sd_Rejection_Email_Alert_to_Record_Owner</fullName>
        <description>SQX Sd Rejection Email Alert to Record Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SD_Rejection_Email_to_Record_Owner</template>
    </alerts>
    <rules>
        <fullName>CQ_Supplier_Deviation_Send_Email_Notification_When_Sd_Result_Is_Set_Rejected_To_Record_Creator</fullName>
        <actions>
            <name>SQX_SD_Rejection_Email_Alert_to_Record_Creator</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This Workflow rule sends email notification to Supplier Contact  or Record Creator  when supplier deviation is rejected.</description>
        <formula>AND(ISPICKVAL(Result__c  , &apos;Rejected&apos;) , OR(
                IF( CreatedBy.Id !=  Owner:User.Id , true, false),IF(CreatedBy.Id ==  Owner:User.Id, true, false)) )
        </formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CQ_Supplier_Deviation_Send_SD_Record_Rejection_Email_to_Record_Owner</fullName>
        <actions>
            <name>SQX_Sd_Rejection_Email_Alert_to_Record_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send email notification to record owner when sd result is rejected</description>
        <formula>AND(ISPICKVAL(Result__c , &apos;Rejected&apos;) , IF(CreatedBy.Id != Owner:User.Id, true, false))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CQ_Supplier_Deviation_Send_Email_To_Record_Owner_When_Sd_Record_Is_Complete</fullName>
        <actions>
            <name>SQX_SD_Record_is_Complete_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Supplier_Deviation__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_Supplier_Deviation__c.Record_Stage__c</field>
            <operation>equals</operation>
            <value>Verification</value>
        </criteriaItems>
        <description>This rule is used to send an email notification to record owner when the status is complete and the stage is in verification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
