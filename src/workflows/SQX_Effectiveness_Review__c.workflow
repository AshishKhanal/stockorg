<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Lock_record</fullName>
        <description>Update locked to true</description>
        <field>Locked__c</field>
        <literalValue>1</literalValue>
        <name>Lock record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Create_New_Revision_To_True</fullName>
        <description>Flag to set new revision of CAPA to true</description>
        <field>Create_New_Revision__c</field>
        <literalValue>1</literalValue>
        <name>Set Create New Revision To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>SQX_CAPA__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Lock Effectiveness Review</fullName>
        <actions>
            <name>Lock_record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Effectiveness_Review__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete,Void</value>
        </criteriaItems>
        <description>Lock Effectiveness Review on completion</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check New Revision Required</fullName>
        <actions>
            <name>Set_Create_New_Revision_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SQX_Effectiveness_Review__c.Create_New_Revision__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set New Revision required for CAPA. Note: Moved to trigger.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
