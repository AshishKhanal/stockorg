<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_an_email_to_CAPA_owner</fullName>
        <description>Send an email to CAPA owner</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Response_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Email_when_response_is_published</template>
    </alerts>
    <rules>
        <fullName>SQX-Send out a message when response gets published directly</fullName>
        <actions>
            <name>Send_an_email_to_CAPA_owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends out a message when a response gets completed without going for an approval.</description>
        <formula>AND(ISPICKVAL(Status__c, &apos;Completed&apos;), ISPICKVAL(PRIORVALUE( Status__c ) , &apos;Draft&apos;), 
ISBLANK(SQX_Audit_Response__c )  )  /* Only those responses that are completed directly should trigger an email, because those that go for approval do send out an email. Also do not send email when the response is related to Audit Response */</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
