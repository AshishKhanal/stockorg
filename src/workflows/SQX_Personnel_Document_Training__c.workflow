<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_personnel_when_personnel_document_training_needs_user_sign_off</fullName>
        <description>Send email to personnel when personnel document training needs user sign-off</description>
        <protected>false</protected>
        <recipients>
            <field>Personnel_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/SQX_PDT_Personnel_User_Sign_Off_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_trainer_when_personnel_document_training_needs_approval_sign_off</fullName>
        <description>Send email to trainer when personnel document training needs approval sign-off</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Trainer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/SQX_PDT_Trainer_Approval_Sign_Off_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Document_Number</fullName>
        <description>Sets document number from the related controlled document.</description>
        <field>Document_Number__c</field>
        <formula>SQX_Controlled_Document__r.Document_Number__c</formula>
        <name>Set Document Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Document_Revision</fullName>
        <description>Sets document revision from the related controlled document.</description>
        <field>Document_Revision__c</field>
        <formula>SQX_Controlled_Document__r.Revision__c</formula>
        <name>Set Document Revision</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Document_Title</fullName>
        <description>Sets document title from the related controlled document.</description>
        <field>Document_Title__c</field>
        <formula>SQX_Controlled_Document__r.Title__c</formula>
        <name>Set Document Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Personnel_Email</fullName>
        <description>Sets value of personnel email field in document training using linked user email of the related personnel field if exists, else using email address field in personnel.</description>
        <field>Personnel_Email__c</field>
        <formula>IF(
  ISBLANK( SQX_Personnel__r.SQX_User__c ),
  SQX_Personnel__r.Email_Address__c,
  SQX_Personnel__r.SQX_User__r.Email
)</formula>
        <name>Set_Personnel_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Uniqueness_Constraint</fullName>
        <description>Set the value of Uniqueness Constraint with Controlled Document and User for Document Training.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID( SQX_Personnel__c ) 
+ CASESAFEID( SQX_Controlled_Document__c )
+ BLANKVALUE( CASESAFEID( SQX_Assessment__c ), &apos;&apos;)
+ IF( ISPICKVAL( Status__c, &apos;Pending&apos; ), &apos;&apos;, Name )</formula>
        <name>Set_Uniqueness_Constraint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Send_email_to_personnel_when_new_personnel_document_training_needs_user_sign_off</fullName>
        <actions>
            <name>Send_email_to_personnel_when_personnel_document_training_needs_user_sign_off</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule send email to linked user of the related personnel when new personnel document training needs user sign-off.</description>
        <formula>AND (
  ISPICKVAL( Status__c, &apos;Pending&apos; ),
  NOT ISBLANK( SQX_Personnel__r.SQX_User__c ),
  ISBLANK( Completion_Date__c ),
  OR (
    IsMigrated__c != true,
    Is_Retrain__c == true
  )
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send_email_to_trainer_when_personnel_document_training_needs_approval_sign_off</fullName>
        <actions>
            <name>Send_email_to_trainer_when_personnel_document_training_needs_approval_sign_off</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule send email to approver / trainer associated to a personnel document training when the document training needs trainer sign-off.</description>
        <formula>ISCHANGED( Status__c )
&amp;&amp;
ISPICKVAL( Status__c, &apos;Trainer Approval Pending&apos; )
&amp;&amp;
NOT ISBLANK( SQX_Personnel__r.SQX_User__c )
&amp;&amp;
ISBLANK( Completion_Date__c )
&amp;&amp;
( IsMigrated__c != true || NOT ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set_Fields_From_Controlled_Document</fullName>
        <actions>
            <name>Set_Document_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Document_Revision</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Document_Title</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Copies document number, revision and title from the controlled document at the time of creation and completion.</description>
        <formula>NOT ISBLANK( SQX_Controlled_Document__c )
&amp;&amp;
(
  ISNEW() || ISPICKVAL( Status__c, &apos;Complete&apos; )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set_Fields_From_Personnel</fullName>
        <actions>
            <name>Set_Personnel_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>It sets fields of document training from the related personnel when created.</description>
        <formula>NOT ISBLANK( SQX_Personnel__c )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set_Uniqueness_Constraint</fullName>
        <actions>
            <name>Set_Uniqueness_Constraint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the value of Uniqueness Constraint with Controlled Document and Personnel for Personel Document Training.</description>
        <formula>NOT(
  AND(
    ISNEW(),
    IsMigrated__c,
    NOT ISPICKVAL( Status__c, &apos;Pending&apos;)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
