<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_unique_name_field</fullName>
        <description>This update copies the audit criteria(document) and program number to uniqueness constraint field.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID(SQX_Audit_Criteria_Document__c )  + 
CASESAFEID( SQX_Audit_Program__c )</formula>
        <name>Set unique name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>Ensure that audit program and criteria are unqiue</fullName>
        <actions>
            <name>Set_unique_name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule copies the audit program criteria(document) and program to the unique field, this will prevent any duplicates from occuring.</description>
        <formula>OR(ISNEW(), ISCHANGED( SQX_Audit_Criteria_Document__c ), ISCHANGED( SQX_Audit_Program__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
