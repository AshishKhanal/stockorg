<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_unique_name_field_to_copy_of_name</fullName>
        <description>Copy the Name field to unique field</description>
        <field>Unique_Name__c</field>
        <formula>Name</formula>
        <name>Set unique name field to copy of name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <rules>
        <fullName>SQX Copy Name to Unique Name Field</fullName>
        <actions>
            <name>Set_unique_name_field_to_copy_of_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule copies the name field to unique name field thus enforcing uniqueness constraint on the name value.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
