<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email_to_trainer_when_document_training_needs_approval</fullName>
        <description>Send email to trainer when document training needs approval</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Trainer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/Send_email_when_DocT_is_signed_off</template>
    </alerts>
    <alerts>
        <fullName>Send_email_to_user_when_document_training_is_created</fullName>
        <description>Send email to user when document training is created</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Compliance_Quest/Send_email_to_user_when_DTN_is_created</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_DocTraining_Uniqueness_Constraint</fullName>
        <description>Set the value of Uniqueness Constraint with Controlled Document and User for Document Training.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID( SQX_User__c ) 
+ CASESAFEID( SQX_Controlled_Document__c )
+ IF( ISNULL( User_SignOff_Date__c ), &apos;&apos;, Name )</formula>
        <name>Set_DocTraining_Uniqueness_Constraint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Send_email_To_User_When_Document_Training_Is_Created</fullName>
        <actions>
            <name>Send_email_to_user_when_document_training_is_created</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SQX_Document_Training__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <description>This rule send email to user associated to a document training when the document training is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send_email_to_approver_when_document_training_is_signed_off</fullName>
        <actions>
            <name>Send_email_to_trainer_when_document_training_needs_approval</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SQX_Document_Training__c.Status__c</field>
            <operation>equals</operation>
            <value>Trainer Approval Pending</value>
        </criteriaItems>
        <description>This rule send email to approver associated to a document training when the document training is is signed off if approver is needed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set_DocTraining_Uniqueness_Constraint</fullName>
        <actions>
            <name>Set_DocTraining_Uniqueness_Constraint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the value of Uniqueness Constraint with Controlled Document and User for Document Training.</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
