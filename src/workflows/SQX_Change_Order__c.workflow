<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_New_Change_Order_Owner</fullName>
        <description>Send Email to new Change Order Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/Change_Order_Submission_Email</template>
    </alerts>
    <alerts>
        <fullName>Change_Order_Complete_Email_Alert</fullName>
        <description>Change Order Complete Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Change_Order_Completion_Email</template>
    </alerts>
    <rules>
        <fullName>SQX Send Email when CO is submitted</fullName>
        <actions>
            <name>Send_Email_to_New_Change_Order_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends out an email to the new change owner to take the ownership and triage the change order, when a CO is submitted.</description>
        <formula>/*
Only when the user submitting the change order is different from the one who will be triaging.
*/
AND(
    OwnerId !=  $User.Id,
    ISPICKVAL(Status__c, &apos;Draft&apos;),
    ISPICKVAL(Record_Stage__c, &apos;Triage&apos;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>When Change Order is completed send an email</fullName>
        <actions>
            <name>Change_Order_Complete_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Change_Order__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>This rule sends out an email to the Change Order Owner/Coordinator when change order is completed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
