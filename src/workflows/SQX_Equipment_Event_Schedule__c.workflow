<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_Email_Equipment_Event_Due_For_Next_Maintenance</fullName>
        <description>CQ Email Equipment Event Due For Next Maintenance</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_EquipmentEvent_DueForNextMaintenance</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Uniqueness_Constraint_Value</fullName>
        <description>Sets Uniqueness Constraint with Event Name and Equipment values.</description>
        <field>Uniqueness_Constraint__c</field>
        <formula>CASESAFEID( SQX_Equipment__c ) + Name</formula>
        <name>Set Uniqueness Constraint Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set_Uniqueness_Constraint_Field</fullName>
        <actions>
            <name>Set_Uniqueness_Constraint_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Equipment_Event_Schedule__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets value for Uniqueness Constraint with combination of Event Name and Equipment.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
