<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Associated_Item_Name_as_Part_Family</fullName>
        <description>Updates associated item name as part family</description>
        <field>Name</field>
        <formula>SQX_Part_Family__r.Name</formula>
        <name>Set Associated Item Name as Part Family</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CQ Set Complaint Associated Item Name</fullName>
        <actions>
            <name>Set_Associated_Item_Name_as_Part_Family</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule to set complaint associated name from part family</description>
        <formula>OR(ISNEW(),  ISCHANGED( SQX_Part_Family__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
