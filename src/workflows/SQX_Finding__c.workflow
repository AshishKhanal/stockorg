<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SQX_Send_Email_Notification_to_Audit_Finding_Owner_When_Initiated</fullName>
        <description>SQX Send Email Notification to Audit Finding Owner When Initiated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_Initiated_Audit_Finding_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_investigation_approver_that_CAPA_Finding_is_closed</fullName>
        <description>Send an email to investigation approver that CAPA Finding is closed.</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Investigation_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Closed_Email_Investigation_Approver</template>
    </alerts>
    <fieldUpdates>
        <fullName>Remove_Complete_Date</fullName>
        <description>Remove complete date when finding is reopened</description>
        <field>Complete_Date__c</field>
        <name>Remove Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Audit_Criterion_For_Finding</fullName>
        <description>Copies audit criterion from audit checklist standard if the audit  criterion is blank.</description>
        <field>Audit_Criterion__c</field>
        <formula>IF(ISBLANK(  Audit_Criterion__c ),
SQX_Audit_Checklist__r.Standard__c  ,
Audit_Criterion__c )</formula>
        <name>Set Audit Criterion For Finding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Date</fullName>
        <description>Set closed date when finding is closed</description>
        <field>Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Complete_Date</fullName>
        <description>Set complete date when finding is complete</description>
        <field>Complete_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Containment_Completion_Date</fullName>
        <description>Set the containment completion date to today</description>
        <field>Completion_Date_Containment__c</field>
        <formula>Today()</formula>
        <name>Set Containment Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>true</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Criterion_Section_For_Finding</fullName>
        <description>Copies criterion section from audit checklist section if the criterion section is blank.</description>
        <field>Criterion_Section__c</field>
        <formula>IF(ISBLANK( Criterion_Section__c ),
SQX_Audit_Checklist__r.Section__c ,
Criterion_Section__c )</formula>
        <name>Set Criterion Section For Finding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Open_Date</fullName>
        <description>Set open date when finding is open</description>
        <field>Open_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Open Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reopen_Date</fullName>
        <description>Set reopen date when finding is reopened</description>
        <field>Reopen_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Reopen Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Criterion_Sub_Section_For_Finding</fullName>
        <description>Copies criterion sub section from audit checklist sub section if the criterion section is blank.</description>
        <field>Criterion_Sub_Section__c</field>
        <formula>IF(ISBLANK(Criterion_Sub_Section__c ),
SQX_Audit_Checklist__r.Sub_Section__c  ,
Criterion_Sub_Section__c )</formula>
        <name>Set Criterion Sub Section For Finding</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_investigation_completion_date</fullName>
        <description>Set the investigation completion date to today</description>
        <field>Completion_Date_Investigation__c</field>
        <formula>Today()</formula>
        <name>Set investigation completion date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_response_completion_date</fullName>
        <description>Set the response completion date to today</description>
        <field>Completion_Date_Response__c</field>
        <formula>Today()</formula>
        <name>Set response completion date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto fill containment completion date</fullName>
        <actions>
            <name>Set_Containment_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Finding__c.Has_Containment__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Automatically fills in containment completion date, when a containment is submitted and approved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto fill investigation completion date</fullName>
        <actions>
            <name>Set_investigation_completion_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Finding__c.Has_Investigation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto fills investigation completion date when investigation is submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto fill response completion date</fullName>
        <actions>
            <name>Set_response_completion_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Finding__c.Has_Response__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Auto fills response completion date upon submission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SQX Send Initiated Audit Finding Email To Assignee</fullName>
        <actions>
            <name>SQX_Send_Email_Notification_to_Audit_Finding_Owner_When_Initiated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends email to audit finding assignee other than auditee contact when finding is initiated</description>
        <formula>AND(
  ISPICKVAL( Status__c, &apos;Open&apos; ),
  OR(
    ISNEW(),
    ISPICKVAL( PRIORVALUE( Status__c ), &apos;Draft&apos; )
  ),
  NOT ISBLANK( SQX_Audit__c ),
  NOT ISBLANK( SQX_Assignee__c ),
  SQX_Assignee__c != SQX_Audit__r.SQX_Auditee_Contact__c 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SQX-Send email when CAPA-Finding gets closed to investigation approver</fullName>
        <actions>
            <name>Send_an_email_to_investigation_approver_that_CAPA_Finding_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule sends out an email to investigation approver when CAPA finding is closed. If Investigation approver differs from CAPA Owner, Implementation reviewer and Effectiveness Reviewer.</description>
        <formula>/*Pretty complex rule  Send out an email for all CAPA Finding with CAPA defined (because Audit finding won&apos;t have this)  */ AND(     /*i. It is a CAPA finding */  NOT(ISNULL(SQX_CAPA__c)),         /*ii. It is closed*/ ISPICKVAL(Status__c, &apos;Closed&apos;),         /*iii. It is a primary finding of that CAPA */  SQX_CAPA__r.SQX_Finding__c = Id,         /*iv. Investigation Approver is not the same as CAPA Owner, Effectiveness Reviewer, Action Approver */             AND(    SQX_Investigation_Approver__c &lt;&gt;  SQX_CAPA__r.SQX_Effectiveness_Reviewer__c ,                     SQX_Investigation_Approver__c &lt;&gt;  SQX_CAPA__r.SQX_Action_Approver__c ,                     SQX_Investigation_Approver__c &lt;&gt;  SQX_CAPA__r.OwnerId ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set closed date when finding is closed</fullName>
        <actions>
            <name>Set_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Finding__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Workflow rule to set closed date when status is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set complete date when finding is compete</fullName>
        <actions>
            <name>Set_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Finding__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Workflow rule to set complete date when status is complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set open date when finding is open</fullName>
        <actions>
            <name>Set_Open_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set open date when status is changed from draft to open</description>
        <formula>AND( ISCHANGED(Status__c),   ISPICKVAL(PRIORVALUE(Status__c), &apos;Draft&apos;),  ISPICKVAL(Status__c, &apos;Open&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set reopen date when finding is reopened</fullName>
        <actions>
            <name>Remove_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Reopen_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set reopen date when finding is reopened</description>
        <formula>AND(  ISPICKVAL(PRIORVALUE(Status__c), &apos;Complete&apos;),  ISPICKVAL(Status__c, &apos;Open&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update_Finding_Fields_When_Checklist_Is_Selected</fullName>
        <actions>
            <name>Set_Audit_Criterion_For_Finding</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Criterion_Section_For_Finding</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Criterion_Sub_Section_For_Finding</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule copies the field in finding such as criterion section, criterion sub section from the audit checklist when the audit checklist is selected in finding.</description>
        <formula>NOT(ISBLANK( SQX_Audit_Checklist__c ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
