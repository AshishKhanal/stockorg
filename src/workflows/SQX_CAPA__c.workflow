<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_CAPA_has_been_closed</fullName>
        <description>A CAPA has been closed</description>
        <protected>true</protected>
        <recipients>
            <field>SQX_External_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SCAR_Closed_Email</template>
    </alerts>
    <alerts>
        <fullName>A_CAPA_has_been_closed_By_CAPA_Coordinator</fullName>
        <description>A CAPA has been closed By CAPA Coordinator</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_CAPA_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Closed_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_CAPA_owner_and_approver_when_CAPA_is_closed</fullName>
        <description>Send an email to CAPA owner and approver when CAPA is closed.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SQX_Action_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SQX_Effectiveness_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Closed_Email_Approvers_And_Owners</template>
    </alerts>
    <alerts>
        <fullName>CQ_Send_email_to_Coordinator_regarding_open_CAPA_If_Coordinator_is_External_User</fullName>
        <description>Send an email to Coordinator User regarding open CAPA When Coordinator is External User.</description>
        <protected>true</protected>
        <recipients>
            <field>SQX_CAPA_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/CQ_Send_email_to_CAPA_Coordinator_User_If_Coordinator_is_External_User</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_Effectiveness_Reviewer_When_Capa_Is_Complete</fullName>
        <description>Send email to effectiveness reviewer when capa is complete.</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_Effectiveness_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Effectiveness_Review_Email</template>
    </alerts>
    <alerts>
        <fullName>CQ_Send_email_to_External_Contact_regarding_open_CAPA</fullName>
        <description>Send an email to External Contact User regarding open CAPA When Contact user and Coordinator are different</description>
        <protected>true</protected>
        <recipients>
            <field>SQX_External_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SCAR_Open_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_To_Supplier_When_The_Partner_Account_is_Disabled</fullName>
        <description>Send Email To Supplier When The Partner Account is Disabled</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_External_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Open_Email_To_Supplier</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_Coordinator_regarding_open_CAPA</fullName>
        <description>Send an email to Coordinator regarding open CAPA</description>
        <protected>false</protected>
        <recipients>
            <field>SQX_CAPA_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_CAPA_Open_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_SCAR_owner_and_approver_when_CAPA_is_closed</fullName>
        <description>Send an email to SCAR owner and approver when CAPA is closed.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>SQX_Action_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>SQX_Effectiveness_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SCAR_Closed_Email_Approvers_And_Owners</template>
    </alerts>
    <alerts>
        <fullName>Send_an_email_to_supplier_regarding_open_capa</fullName>
        <description>Send an email to supplier regarding open CAPA</description>
        <protected>true</protected>
        <recipients>
            <field>SQX_CAPA_Coordinator__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/SQX_SCAR_Open_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Close_Date_to_Current_Date_Time</fullName>
        <description>Fills the close date to the current system date and time</description>
        <field>Closed_Date__c</field>
        <formula>Now()</formula>
        <name>Set Close Date to Current Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_date_opened_to_current_date</fullName>
        <description>Sets the date opened for a CAPA to current date</description>
        <field>Date_Opened__c</field>
        <formula>Today()</formula>
        <name>Set date opened to current date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Fill in close date when capa is closed</fullName>
        <actions>
            <name>Set_Close_Date_to_Current_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_CAPA__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>This workflow fills in the close date when CAPA is closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Fill in date when CAPA is opened</fullName>
        <actions>
            <name>Set_date_opened_to_current_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule fills in the date when a CAPA is opened i.e. it transitions from &apos;Draft&apos; to &apos;Open&apos; state.</description>
        <formula>AND(ISPICKVAL(PRIORVALUE(Status__c), &apos;Draft&apos;), ISPICKVAL(Status__c, &apos;Open&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email To CAPA Coordinator When CAPA gets Open</fullName>
        <actions>
            <name>Send_Email_To_Supplier_When_The_Partner_Account_is_Disabled</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_an_email_to_Coordinator_regarding_open_CAPA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>((1 AND 2) OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>SQX_CAPA__c.CAPA_Type__c</field>
            <operation>equals</operation>
            <value>Supplier</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_CAPA__c.Partner_enabled__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_CAPA__c.CAPA_Type__c</field>
            <operation>notEqual</operation>
            <value>Supplier</value>
        </criteriaItems>
        <criteriaItems>
            <field>SQX_CAPA__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Sends an email out to the coordinator when a CAPA is opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email To CAPA Coordinator When CAPA gets closed</fullName>
        <actions>
            <name>A_CAPA_has_been_closed_By_CAPA_Coordinator</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_an_email_to_CAPA_owner_and_approver_when_CAPA_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is flow sends out email, when a CAPA is closed to the related supplier -capa coordinator (Primary Contact).</description>
        <formula>AND ( 
                        AND(
                            ISPICKVAL(Status__c, &apos;Closed&apos;), 
                            NOT(ISPICKVAL(PRIORVALUE(Status__c), &apos;Draft&apos;)),
                            NOT(ISPICKVAL(PRIORVALUE(Status__c), &apos;Closed&apos;))
                        ) ,
                        OR(
                            NOT(ISPICKVAL(CAPA_Type__c , &apos;Supplier&apos;)),
                            AND(
                                ISPICKVAL(CAPA_Type__c , &apos;Supplier&apos;),
                                NOT(Partner_enabled__c)
                            )
                        )
                    )
        </formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email To Capa Effectiveness Reviewer</fullName>
        <actions>
            <name>Send_Email_To_Effectiveness_Reviewer_When_Capa_Is_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_CAPA__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Send an email to effectiveness reviewer regarding effectiveness review of capa.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CQ Send Email to Coordinator and External Contact When CAPA gets opened and if Coordinator is an External User</fullName>
        <actions>
            <name>CQ_Send_email_to_Coordinator_regarding_open_CAPA_If_Coordinator_is_External_User</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CQ_Send_email_to_External_Contact_regarding_open_CAPA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to CAPA Coordinator and External Contact if Coordinator User is an External User and both User are different.</description>
        <formula>
            AND( 
                ISPICKVAL(CAPA_Type__c,&apos;Supplier&apos;), 
                Partner_enabled__c, 
                ISPICKVAL(Status__c, &apos;Open&apos;), 
                CASESAFEID(SQX_CAPA_Coordinator__r.ContactId) &lt;&gt; CASESAFEID(SQX_External_Contact__c),
                ISPICKVAL(SQX_CAPA_Coordinator__r.UserType, &apos;PowerPartner&apos;) 
            )
        </formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CQ Send Email to Coordinator and External Contact When CAPA gets opened and if Coordinator is an Internal User</fullName>
        <actions>
            <name>Send_an_email_to_Coordinator_regarding_open_CAPA</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CQ_Send_email_to_External_Contact_regarding_open_CAPA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send Email to CAPA Coordinator and External Contact if Coordinator User is an Internal User.</description>
        <formula>
            AND( 
                ISPICKVAL(CAPA_Type__c,&apos;Supplier&apos;), 
                Partner_enabled__c, 
                ISPICKVAL(Status__c, &apos;Open&apos;), 
                CASESAFEID(SQX_CAPA_Coordinator__r.ContactId) &lt;&gt; CASESAFEID(SQX_External_Contact__c),
                ISPICKVAL(SQX_CAPA_Coordinator__r.UserType, &apos;Standard&apos;) 
            )
        </formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email when CAPA gets closed</fullName>
        <actions>
            <name>A_CAPA_has_been_closed</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Send_an_email_to_SCAR_owner_and_approver_when_CAPA_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is flow sends out email, when a CAPA is closed to the related supplier -external contact(Primary Contact).</description>
        <formula>AND ( 
                        AND(
                            ISPICKVAL(Status__c, &apos;Closed&apos;), 
                            NOT(ISPICKVAL(PRIORVALUE(Status__c), &apos;Draft&apos;)),
                            NOT(ISPICKVAL(PRIORVALUE(Status__c), &apos;Closed&apos;))
                        ) ,
                        AND(
                            ISPICKVAL(CAPA_Type__c , &apos;Supplier&apos;),
                            Partner_enabled__c
                        )
                    )
        </formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send email when CAPA gets opened</fullName>
        <actions>
            <name>Send_an_email_to_supplier_regarding_open_capa</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Sends an email out to the supplier when a CAPA is opened</description>
        <formula>
            AND( 
                ISPICKVAL(CAPA_Type__c,&apos;Supplier&apos;), 
                Partner_enabled__c,
                ISPICKVAL(Status__c, &apos;Open&apos;),
                CASESAFEID(SQX_CAPA_Coordinator__r.ContactId) == CASESAFEID(SQX_External_Contact__c)
            )
        </formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
