<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CQ_Supplier_Escalation_Record_status_is_complete_Email_Alert</fullName>
        <description>SQX Supplier Escalation Record status is complete Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Compliance_Quest/CQ_Email_to_Owner_When_Supplier_Escalation_status_is_complete</template>
    </alerts>
    <rules>
        <fullName>CQ_Supplier_Escalation_Send_Email_When_Supplier_Escalation_Status_Is_Complete</fullName>
        <actions>
            <name>CQ_Supplier_Escalation_Record_status_is_complete_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SQX_Supplier_Escalation__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>This workflow is used to send email to owner when Supplier Escalation status is complete.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>