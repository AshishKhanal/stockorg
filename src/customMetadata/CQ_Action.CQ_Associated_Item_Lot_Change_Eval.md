<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Configuration for layout initialization for creating new Associated Item.</description>
    <label>CQ Associated Item Lot Change Eval</label>
    <protected>false</protected>
    <values>
        <field>CQ_Action_Type__c</field>
        <value xsi:type="xsd:string">Layout Initializer</value>
    </values>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">CQ_Associated_Item_Lot_Change_Eval</value>
    </values>
    <values>
        <field>Component_Type__c</field>
        <value xsi:type="xsd:string">Flow</value>
    </values>
    <values>
        <field>Main_Record_API_Name__c</field>
        <value xsi:type="xsd:string">compliancequest__SQX_Complaint_Associated_Item__c</value>
    </values>
    <values>
        <field>Namespace__c</field>
        <value xsi:type="xsd:string">compliancequest</value>
    </values>
</CustomMetadata>
