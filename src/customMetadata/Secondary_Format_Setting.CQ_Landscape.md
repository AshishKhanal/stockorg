<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>CQ Setting for PDF generation of Landscape documents</description>
    <label>CQ Landscape</label>
    <protected>false</protected>
    <values>
        <field>DateTime_Format__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Date_Format__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default_Setting__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Enable_Stamping_When_Printed__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Merge_Fields_Set_1__c</field>
        <value xsi:type="xsd:string">Document_Number__c,
Revision__c,
Effective_Date__c,
Expiration_Date__c, 
OwnerId,
SQX_Business_Unit__c,
SQX_Site__c,
SQX_Division__c,
SQX_Department__c,
Title__c</value>
    </values>
    <values>
        <field>Merge_Fields_Set_2__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Page_Namespace__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Print_Stamp_Coordinate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Print_Stamp_DateTime_Format__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Print_Stamp_Expiration__c</field>
        <value xsi:type="xsd:double">24.0</value>
    </values>
    <values>
        <field>Profile_Page_Inclusion_Type__c</field>
        <value xsi:type="xsd:string">Exclude</value>
    </values>
    <values>
        <field>Profile_Page_Parameter__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Profile_Page__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Stamping_PDF__c</field>
        <value xsi:type="xsd:string">Secondary_Format_Setting_Stamping_PDF_Landscape</value>
    </values>
    <values>
        <field>Text_To_Stamp_When_Printed__c</field>
        <value xsi:type="xsd:string">Printed on {0}, Expires on {1}</value>
    </values>
    <values>
        <field>Watermark_Angle__c</field>
        <value xsi:type="xsd:double">45.0</value>
    </values>
    <values>
        <field>Watermark_Color__c</field>
        <value xsi:type="xsd:string">255,0,0,1</value>
    </values>
    <values>
        <field>Watermark_Horizontal_Position__c</field>
        <value xsi:type="xsd:string">Center</value>
    </values>
    <values>
        <field>Watermark_Text_Size__c</field>
        <value xsi:type="xsd:double">72.0</value>
    </values>
    <values>
        <field>Watermark_Vertical_Position__c</field>
        <value xsi:type="xsd:string">Center</value>
    </values>
    <values>
        <field>Watermark_Z_Order__c</field>
        <value xsi:type="xsd:string">Bring to front</value>
    </values>
    <values>
        <field>Watermark__c</field>
        <value xsi:type="xsd:string">{!Document_Status__c}</value>
    </values>
</CustomMetadata>
