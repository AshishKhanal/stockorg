<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Settings for completing Change Order actions from Standard task UI and related list (activities)</description>
    <label>CQ CO Action Impl Complete</label>
    <protected>false</protected>
    <values>
        <field>CQ_Action_Type__c</field>
        <value xsi:type="xsd:string">Action</value>
    </values>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">SQX_ChangeOrder_Impl_Complete_Task</value>
    </values>
    <values>
        <field>Component_Type__c</field>
        <value xsi:type="xsd:string">VF Page</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">Complete Task</value>
    </values>
    <values>
        <field>Main_Record_API_Name__c</field>
        <value xsi:type="xsd:string">compliancequest__SQX_Implementation__c</value>
    </values>
    <values>
        <field>Namespace__c</field>
        <value xsi:type="xsd:string">compliancequest</value>
    </values>
    <values>
        <field>Purpose__c</field>
        <value xsi:type="xsd:string">Change order Action Implementation</value>
    </values>
</CustomMetadata>
