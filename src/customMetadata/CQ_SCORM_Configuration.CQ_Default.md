<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>[Deprecated] CQ SCORM configuration for setting callback site url in v.7.2.0</description>
    <label>CQ Default</label>
    <protected>false</protected>
    <values>
        <field>App_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Default__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Postback_Site_Url__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Secret_Key__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
