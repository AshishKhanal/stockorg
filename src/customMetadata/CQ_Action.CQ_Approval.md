<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>This metadata for approval history loader</description>
    <label>CQ Approval</label>
    <protected>false</protected>
    <values>
        <field>CQ_Action_Type__c</field>
        <value xsi:type="xsd:string">Action</value>
    </values>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">SQX_Approval_Document_Information</value>
    </values>
    <values>
        <field>Component_Type__c</field>
        <value xsi:type="xsd:string">Lightning Component</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:type="xsd:string">approval</value>
    </values>
    <values>
        <field>Main_Record_API_Name__c</field>
        <value xsi:type="xsd:string">compliancequest__SQX_Controlled_Document__c</value>
    </values>
    <values>
        <field>Namespace__c</field>
        <value xsi:type="xsd:string">compliancequest</value>
    </values>
    <values>
        <field>Purpose__c</field>
        <value xsi:type="xsd:string">approval</value>
    </values>
</CustomMetadata>
