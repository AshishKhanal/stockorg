<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Configuration for EMA - Final MDV</description>
    <label>EMA - Final MDV</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Form_Name__c</field>
        <value xsi:type="xsd:string">EU:Final MDV</value>
    </values>
    <values>
        <field>Form_Type__c</field>
        <value xsi:type="xsd:string">EU-Incident-v2.26</value>
    </values>
    <values>
        <field>Mode_of_Submission__c</field>
        <value xsi:type="xsd:string">Email</value>
    </values>
    <values>
        <field>Regulatory_Body__c</field>
        <value xsi:type="xsd:string">EMA</value>
    </values>
    <values>
        <field>Report_Type__c</field>
        <value xsi:type="xsd:string">Meddev</value>
    </values>
</CustomMetadata>
