<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Configuration for FDA - Summary</description>
    <label>FDA - Summary</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Form_Name__c</field>
        <value xsi:type="xsd:string">US:Summary</value>
    </values>
    <values>
        <field>Form_Type__c</field>
        <value xsi:type="xsd:string">FDA-3500A</value>
    </values>
    <values>
        <field>Mode_of_Submission__c</field>
        <value xsi:type="xsd:string">External System</value>
    </values>
    <values>
        <field>Regulatory_Body__c</field>
        <value xsi:type="xsd:string">FDA</value>
    </values>
    <values>
        <field>Report_Type__c</field>
        <value xsi:type="xsd:string">Medwatch</value>
    </values>
</CustomMetadata>
