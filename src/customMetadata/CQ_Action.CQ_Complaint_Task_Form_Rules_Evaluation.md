<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>Configuration to call flow for layout initialization and on change event support</description>
    <label>CQ Complaint Task Form Rules Evaluation</label>
    <protected>false</protected>
    <values>
        <field>CQ_Action_Type__c</field>
        <value xsi:type="xsd:string">Layout Initializer</value>
    </values>
    <values>
        <field>Component_Name__c</field>
        <value xsi:type="xsd:string">CQ_Complaint_Task_Form_Rules_Evaluation</value>
    </values>
    <values>
        <field>Component_Type__c</field>
        <value xsi:type="xsd:string">Flow</value>
    </values>
    <values>
        <field>Label__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Main_Record_API_Name__c</field>
        <value xsi:type="xsd:string">compliancequest__SQX_Complaint_Task__c</value>
    </values>
    <values>
        <field>Namespace__c</field>
        <value xsi:type="xsd:string">compliancequest</value>
    </values>
    <values>
        <field>Purpose__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
