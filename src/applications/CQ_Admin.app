<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>ComplianceQuest Admin App to manage setup objects</description>
    <label>CQ Admin</label>
    <logo>Compliance_Quest/Compliance_Quest_Logo.png</logo>
    <tabs>SQX_ES_Setting</tabs>
    <tabs>SQX_Field_Audit_Trail</tabs>
    <tabs>SQX_Org_Field_Dependency_Synchronization</tabs>
    <tabs>SQX_Part_Family__c</tabs>
    <tabs>SQX_Part__c</tabs>
    <tabs>SQX_Lot_Info__c</tabs>
    <tabs>SQX_Standard_Service__c</tabs>
    <tabs>SQX_Defect_Code__c</tabs>
    <tabs>SQX_Root_Cause_Code__c</tabs>
    <tabs>SQX_Investigation_Tool_Name__c</tabs>
    <tabs>SQX_Department__c</tabs>
    <tabs>SQX_Division__c</tabs>
    <tabs>SQX_Business_Unit__c</tabs>
    <tabs>SQX_Site__c</tabs>
    <tabs>SQX_Job_Function__c</tabs>
    <tabs>SQX_Result_Type__c</tabs>
    <tabs>SQX_Picklist_Value__c</tabs>
    <tabs>SQX_Task__c</tabs>
    <tabs>SQX_Approval_Matrix__c</tabs>
    <tabs>SQX_Auto_Number__c</tabs>
    <tabs>SQX_Reporting_Default__c</tabs>
</CustomApplication>
