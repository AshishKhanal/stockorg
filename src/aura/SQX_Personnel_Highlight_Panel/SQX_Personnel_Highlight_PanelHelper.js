({
    
    /**
     * fetches the personnel wrapper record which includes job functions list, certificates list and corresponding user records
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    getPersonnelWrapper : function(component, event) {
        var that = this;
        var action = component.get('c.getPersonnelData');
        action.setParams({
            "personnelId": component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS') {
                component.set("v.personnelWrapper", response.getReturnValue());
            } else {
                that.displayErrorInToast(response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
        
    /**
     * shows error message in toast
     * @param {message} error messages to be shown.
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    displayErrorInToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.c.CQ_UI_Error"),
            "type": "error",
            "message": message,
            "mode": "sticky"
        });
        toastEvent.fire();
    }
})