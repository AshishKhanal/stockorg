({
    /**
     * Initializes the component with proper data.
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    doInit : function(component, event, helper) {
        helper.getPersonnelWrapper(component, event);
    }
})