({
    onInit : function(component, event, helper) {
        component.get("v.sObjectName")  == 'compliancequest__SQX_Complaint__c' ? component.set('v.purposeActions', 'approverejectrequest') : component.set('v.purposeActions', 'approvereject');
        helper.invokeAction(component.get('c.getCurrentApprovalProcessStepName'), {
            docId: component.get("v.recordId"),
            workItemId:component.get("v.workitemId")
        }).then($A.getCallback(function(data) {
            var dataArray = data.split("/");
            component.set('v.currentApprovalProcessStepName', dataArray[0]);
            component.set('v.workitemId',dataArray[1]);
        })).catch(function(err) {
            helper.displayError(err, component);
        });
    },

    /**
    * This method is used to refresh the state of components
    */
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },

    /**
     * process the pending approval record and approves the doc if no error messages
     */
    approveRejectDoc: function(component,event, helper) {

        component.set('v.displaymode', 'quickaction');
        var esigComponent=component.find('esig');
        var successMsg = event.getSource().get("v.value") === 'true' ? $A.get("$Label.c.CQ_UI_Record_Approved_Success_Message") : $A.get("$Label.c.CQ_UI_Record_Reject_Message");
        esigComponent.validateEsig(esigComponent.get('v.esigPurpose')).then($A.getCallback(function() {
            helper.showLoading(component);
            helper.invokeAction(component.get('c.approveRejectDocument'), {
                documentId : component.get('v.recordId'),
                workItemId : component.get('v.workitemId'),
                userName :  esigComponent.get('v.username'),
                password :  esigComponent.get('v.password'),
                approveRejectFlag : event.getSource().get("v.value"),
                comment:esigComponent.get('v.comment')
            }).then(function(result) {
                component.set('v.displaymode', '');

                if(result.infoMessages) {
                    helper.showMessage(result.infoMessages.join('.'), 'info', '', 'dismissible');
                }
                if(result.errorMessages) {
                    helper.displayError(result.errorMessages.join('.'),component);
                }
                if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                    helper.showMessage(successMsg, 'success', '', 'dismissible');
                    helper.navigateToUrl('/' + component.get('v.recordId'));
                }
            })['catch']($A.getCallback(function(error) {
                helper.displayError(error, component);
            })).finally($A.getCallback(function() {
                helper.hideLoading(component);
            }));

        })).catch($A.getCallback(function(err) {
            helper.displayError(err, component);
        }))

    },
    /**
     * leaves the pending approval record as it is
     */
    cancelApproval: function(component,event, helper) {
        helper.navigateToUrl('/' + component.get('v.recordId'));
    }
})