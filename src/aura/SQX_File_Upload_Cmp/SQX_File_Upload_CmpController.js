({
    /**
     * function to fetch list of content documents and show list to screen
     */
    showFileSelectScreen : function(component, event, helper) {
        
        component.set('v.columns', [
            {label: $A.get("$Label.compliancequest.CQ_UI_Files"), 
             fieldName: 'ShowRecord', 
             type: 'url', 
             typeAttributes: {
                 label: { fieldName: 'Title' }
             }}
        ]);
        
        helper.invokeAction(component.get('c.getDocList'), {
            recordId: component.get("v.recordId")
        }).then($A.getCallback(function(data) {
            component.set('v.data',data);
        })).catch(function(err){
            helper.displayError(err, component);
        });

        component.set('v.isOpen', true);
    },
    
    /**
     * function to close popup
     */
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    /**
     * function to link files to regulatory record
     */
    addFiles : function(component, event, helper) {
        
        component.set("v.isProcessing", true);
        var contentDocList = component.get("v.contentDocIds");
        if(contentDocList.length > 0){
                
            helper.invokeAction(component.get('c.saveContentDocsToReport'), {
                recordId: component.get("v.recordId"),
                contentDocIds : contentDocList
            }).then($A.getCallback(function(data) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.CQ_UI_SUCCESS"),
                    "message":  $A.get("$Label.c.CQ_UI_File_Uploaded")
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            })).catch(function(err){
                helper.displayError(err, component);
            });
        }
        component.set("v.isProcessing", false);
        component.set("v.isOpen", false);
    },

    /**
     * function to selected content document to list
     */
    addContentDocumentToList: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        
        var contentDocList = [];
        for(var i = 0; i < selectedRows.length; i++){
            contentDocList.push(selectedRows[i].Id);
        }
        component.set('v.contentDocIds', contentDocList);
    }
})