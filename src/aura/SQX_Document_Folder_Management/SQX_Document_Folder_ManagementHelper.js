({
    /**
     * sets the icons in the returned documents
    */
    setIcon : function(d){
        if(d && d.length > 0){
            
            var EXTENSIONS = {
                'csv': 'csv',
                'doc' : 'word',
                'docx': 'word',
                'mp4': 'mp4',
                'pdf': 'pdf',
                'ppt': 'ppt',
                'psd': 'psd',
                'txt': 'txt',
                'vsd': 'visio',
                'vss': 'visio',
                'vst': 'visio',
                'xlsx': 'excel',
                'xls': 'excel',
                'zip': 'zip'
            };
            
            for(var i = 0; i < d.length; i++ ){
                var ext = EXTENSIONS[d[i].extension] || 'unknown';
                d[i].icon = 'doctype:' + ext;
            }
        }
    },
    /**
     * this method display selected binder document details
     * @param cmp is a app component
     * @param docListCmp is a binder document details component
     * @param node is a selected node item object
     * @param globalTextSearchValue is a global text search value in all fields
     * @param docDetailTextSearchValue selected nodeItem document detail search value
     * 'getMatchingDocs' is a controller method.
     */ 
    getBinderDetails: function(cmp,docListCmp, node,globalTextSearchValue,docDetailTextSearchValue){
        var cmpSpinner= cmp.find('divLoader');
        var action = cmp.get('c.getMatchingDocs');
        var pageNumber = cmp.get('v.pageNumber') || 1;        
        var hideLoading = false;
        var layoutConfig = cmp.get('v.layout');
        if(globalTextSearchValue == undefined){
            hideLoading = true;
            if(docDetailTextSearchValue == undefined){
                if(node){
                    var effFilter = node.getEffectiveFilter();
                    action.setParams({ 
                        'searchText':'',
                        'globalSearchText' : '',
                        'folderFilterString' : JSON.stringify(effFilter.filters.ff),
                        'topicFilterString' : JSON.stringify(effFilter.filters.tf),
                        'documentFilterString' : JSON.stringify(effFilter.filters.df),
                        'pageSize' : node.getBinder().pageSize ? node.getBinder().pageSize.toString() : null,
                        'pageNumber' : pageNumber.toString(),
                        'sortBy' : layoutConfig.sort.fields,
                        'sortOrder': layoutConfig.sort.sortOrder
                    });
                }
            } else{
                if(node){
                    var effFilter = node.getEffectiveFilter();
                    action.setParams({ 
                        'searchText':docDetailTextSearchValue,
                        'globalSearchText' : '',
                        'folderFilterString' : JSON.stringify(effFilter.filters.ff),
                        'topicFilterString' : JSON.stringify(effFilter.filters.tf),
                        'documentFilterString' : JSON.stringify(effFilter.filters.df),
                        'pageSize' : node.getBinder().pageSize ? node.getBinder().pageSize.toString() : null,
                        'pageNumber' : pageNumber.toString(),
                        'sortBy' : layoutConfig.sort.fields,
                        'sortOrder': layoutConfig.sort.sortOrder
                    });
                }
            }
        }
        else{
            hideLoading = true;
            action.setParams({ 
                'searchText':'',
                'globalSearchText' : globalTextSearchValue,
                'folderFilterString' : '{}',
                'topicFilterString' : '{}',
                'documentFilterString' : '{}',
                'pageNumber' : pageNumber.toString()
            });
        }

        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS') {
                var resp = response.getReturnValue() || {};
                
                var result = resp.documents || [];
    
                this.setIcon(result);
    
                /* Toggle 'show more' icon's visibility */
                docListCmp.set("v.hasRecords", resp.hasMoreDocuments);
    
                var remainingText = '';
                if(resp.hasMoreDocuments){
                    if(resp.remainingDocuments){
                        var label = cmp.get('v.LABEL_NO_MORE_RECORDS');
                        remainingText = '(' + label.replace('{0}', resp.remainingDocuments) + ')';
                    }
                }
    
                docListCmp.set("v.nRemaining", remainingText);
    
                /* 
                    if 'pageNumber' is greater than 1, it means user wishes to see more records in which case 
                    new result has to be appended with the old result
                */
                if(pageNumber > 1){
                    var oldResults = docListCmp.get("v.documents");
                    result = oldResults.concat(result);
                }
                
                // increment page number
                cmp.set('v.pageNumber', pageNumber + 1);
    
                docListCmp.set("v.documents", result);
    
                this.getField(docListCmp,cmp);
                if(hideLoading){
                    $A.util.addClass(cmpSpinner, 'slds-hide');
                }
            } else if (response.getState() === 'ERROR') {
                this.handleError(response.getError()[0], cmp);
                $A.util.addClass(cmpSpinner, 'slds-hide');
            }
        });
        
        $A.enqueueAction(action); 
    },

    getBinderClass: function() {
        if(!window.Binder) {
            (function (){

                /**
                * provides a common method to add sub folder to folder/binder
                * @param name the name of the new folder
                * @param parent the parent entity that contains the folder
                * @return returns the newly added sub-folder
                */
                function addSubFolder(name, parent){
                    var newFolder = new Folder();
                    newFolder.name = name;
                    newFolder.parentFolder = parent;

                    parent.folders.push(newFolder);
                    parent.folders.sort(sortFunction);

                    return newFolder;
                }



                /**
                * Removes a folder from the given folder array.
                * @folderArray the list of folder from which to remove the object
                * @folder the folder that is to be removed
                * @return returns true if the folder has been removed else false
                */
                function removeFolder(folderArray, folder){
                    var indexOfFolder = this.indexOf(folder);

                    if(indexOfFolder > -1){
                        folderArray.splice(indexOfFolder, 1);
                    }

                    return indexOfFolder > -1;
                }

                /**
                * Converts folders FolderObject
                * @param folder the folder/binder whose folders are to be converted to Folder object
                */
                function convertSubFoldersToObject(folders, parent){
                    parent.folders = [];

                    if(folders){
                        for(var i = 0; i < folders.length; i++){
                            parent.folders.push(new Folder(folders[i], parent));
                        }
                        parent.folders.sort(sortFunction);
                    }

                }


                /**
                * Default function to sort the folders array using either the name property of the folder or id property of folder based on value of Sort Folder By field of binder object.
                */
                function sortFunction(a,b) {
                    if( a.rootFolder.sortFolderBy === "Id") {
                        var n1 = parseInt(a.id) || -1,
                            n2 = parseInt(b.id) || -1;
                        if((n1 === -1 || n2 === -1) && window.console) {
                            window.console.error("Invalid id in " + a + "," + b);
                        }
                    } else {
                        var n1 = a.name, n2 = b.name;
                    }

                    return n1 > n2 ? 1 : n1 == n2 ? 0 : -1;
                }

                /**
                * This object represents the Folder object that will contain data at individual node level
                * @param folder the JSON object to convert to folder object
                */
                var Folder = function(folder, parent){
                    if(folder){
                        this.id=folder.id;
                        this.name = folder.name;
                        this.selected = false;
                        this.filter = new Filter(folder.filters);
                        this.rootFolder = parent.rootFolder || parent;
                        convertSubFoldersToObject(folder.folders, this);
                        this.parentFolder = parent;
                    }
                    else{
                        this.folders = [];
                        this.id=null;
                        this.filter = new Filter();
                        this.name = '';
                        this.selected = false;
                        this.rootFolder = parent.rootFolder || parent;
                        this.parentFolder = parent;
                    }
                };

                /**
                * adds a sub folder to a given folder with a specifc name
                */
                Folder.prototype.addSubFolder = function(name){
                    return addSubFolder(name, this);
                }

                /**
                * Checks whether or not object is empty i.e. has its own/inherited fields
                * @return returns <code>true</code> if it doesn't have any field else returns <code>false</code>
                */
                function isEmptyObject(obj){

                    return !(obj && JSON.stringify(obj).length > 2); //Hack: {} for checking empty
                }


                /**
                * Returns the effective filter that is to be applied to the folder
                */
                Folder.prototype.getEffectiveFilter = function(){
                    var combinedFilter = new Filter(),
                        currentFolder = this;

                    var FILTER_KEYS = ['ff', 'df', 'tf'];

                    while(currentFolder){
                        for(var i = 0; i < FILTER_KEYS.length; i++){
                            var key = FILTER_KEYS[i];
                            combinedFilter.combineFilter(key, currentFolder.filter.filters[key]);
                        }

                        currentFolder = currentFolder.parentFolder;
                    }

                    return combinedFilter;
                }


                /**
                * Represents the filter object at each node
                */
                var Filter = function(filters){
                    if(filters){
                        this.filters = filters;
                    }
                    else{
                        this.filters = {
                            'ff' : {},
                            'tf' : {},
                            'df' : {}
                        }
                    }
                };

                /**
                * Combines a given filter value for a given filter type /entity type
                * @fType the filter type to combine to
                * @filterValue the value of the filter to combine
                */
                Filter.prototype.combineFilter = function(fType, filterValue){
                    var cF = this.filters[fType];
                    if(!isEmptyObject(filterValue)){
                        if(isEmptyObject(cF)){
                            this.filters[fType] = JSON.parse(JSON.stringify(filterValue));
                        }
                        else{
                            this.filters[fType] = { logic : 'and', filters: []};
                            this.filters[fType].filters.push(cF);
                            this.filters[fType].filters.push(JSON.parse(JSON.stringify(filterValue)));
                        }
                    }
                }



                /**
                * internal method used to deserialze sub folder json to JavaScript object.
                */
                function getFolderArray(subFolderJSON, parent){
                    var folders = [];
                    try{
                        var foldersJSON = JSON.parse(subFolderJSON);
                        for(var i = 0; i < foldersJSON.length; i++){
                            var folder = foldersJSON[i];
                            folders.push(new Folder(folder, parent));
                        }

                        folders.sort(sortFunction);
                    }
                    catch(ex){
                        console.log(ex);
                    }

                    return folders;
                }

                /**
                * This object represents the Binder element that will hold all the folder information
                * and provide common methods to work with the SalesForce and JS.
                */
                var Binder = function(binderObject){
                    this.id = binderObject.Id;
                    this.name = binderObject.Name;
                    this.pageSize = binderObject.compliancequest__Page_Size__c;
                    this.selected = false;
                    this.sortFolderBy = binderObject.compliancequest__Sort_Folder_By__c;
                    this.folders = getFolderArray(binderObject.compliancequest__SubFolders__c, this);
                    if(binderObject.compliancequest__Filter__c){
                        this.filter = new Filter(JSON.parse(binderObject.compliancequest__Filter__c));
                    }
                    else{
                        this.filter = new Filter();
                    }
                };

                /**
                * Returns the filter that is represented by the binder
                */
                Binder.prototype.getEffectiveFilter = function(){
                    return this.filter;
                }

                /**
                 * Selects a particular node for the binder identified by the node id
                 * @param nodePath is a string concatenation uniquelly identifying the path to the node.
                 */
                Binder.prototype.selectNode = function(nodePath){
                    var path = nodePath.split('.'),
                        selectedNode = this, id, i, inPath, retNode;

                    while(selectedNode && path.length > 0){
                        // get id for current level
                        id = path.splice(0, 1);
                        if(id.length == 1){
                            inPath = false;
                            // for each folder in current level check if there is match in path
                            for(i = 0; i < selectedNode.folders.length; i++){
                                if(selectedNode.folders[i].id == id[0]){
                                    // proceed to next level
                                    selectedNode = selectedNode.folders[i];
                                    inPath = true;
                                    break;
                                }
                            }

                            if(!inPath){
                                selectedNode = null;
                            }
                        }
                    }

                    if(path.length == 0 && selectedNode != null){
                        // we have a match
                        retNode = selectedNode;

                        while(selectedNode != null){
                            selectedNode = selectedNode.parentFolder;
                            if(selectedNode){
                                selectedNode.expanded = true;
                            }
                        }

                        this.select(retNode);
                    }

                    return retNode;
                }

                /**
                 * Selects a binder and its node
                 */
                Binder.prototype.select = function(node){

                    if(this._selectedNode){
                        this._selectedNode.selected = false;
                    }

                    this._selectedNode = node || this;
                    this._selectedNode.selected = true;

                }


                /**
                * adds a sub folder to the binder
                * @param name the name of folder to add
                */
                Binder.prototype.addSubFolder = function(name){
                    return addSubFolder(name, this);
                }


                /**
                * Used by stringify to ignore property parent folder else a cyclic loop would be introduced
                * @param k key of property
                * @param v value of property
                */
                function ignoreParentFolderKey(k, v){
                    if(k === 'parentFolder'){
                        return undefined;
                    }

                    return v;
                }

                /**
                * Get path comnination of dot ie 2.1.1
                */
                Folder.prototype.getPath = function(){
                    return getFolderId(this);
                }
                /**
                * Get path comnination of dot ie 2.1.1
                */
                Binder.prototype.getPath = function(){
                    return getFolderId(this);
                }
                /**
                * Get binder id
                */
                Folder.prototype.getBinder = function(){
                    var parent = this;
                    while(parent.parentFolder){
                    parent = parent.parentFolder;
                    }
                    return parent;
                }
                /**
                */
                Binder.prototype.getBinder = function(){
                    return this;
                }
                /**
                *get folder ids
                */
                function getFolderId(selectedNodeObj){
                    var folderIds = '';
                    while(selectedNodeObj.parentFolder){
                    folderIds = selectedNodeObj.id + '.' + folderIds;
                    selectedNodeObj = selectedNodeObj.parentFolder;
                    }
                    folderIds = folderIds.substring(0, folderIds.length - 1);
                    return folderIds;
                }

                /**
                * Returns a salesforce object that can be stored using controller.
                */
                Binder.prototype.getSFObject = function(){
                    return {
                        sObjectType : 'compliancequest__SQX_Binder__c',
                        Id : this.id,
                        Name : this.name,
                        compliancequest__SubFolders__c : JSON.stringify(this.folders, ignoreParentFolderKey),
                        compliancequest__Filter__c : JSON.stringify(this.filter)
                    };
                }

                window.Binder = Binder;
                window.Folder = Folder;
            })();
        }

        return window.Binder;
    },
    
    /*
     * this method bind treeview hierarchy
     * @param cmpBinderList is a binderList component
     * @param binderId ddl selected binder ID.
     * @param appComponent is a app component.
     * 'getBinderTreeView' is a controller method.
     */ 
    getBinderTreeView:function(cmpBinderList,binderId,appComponent,defaultFolderIds,docListCmp){
        var cmpSpinner= appComponent.find('divLoader');
        var action = cmpBinderList.get("c.getBinder");
        //Set up the callback
        var self = this;
        action.setParams({
            "binderId" : binderId
        });
        var Binder = this.getBinderClass();
        action.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                var obj=response.getReturnValue();
                var currentTreeViewObj=new Binder(obj);
                //set default first level treeview expand
                currentTreeViewObj.expanded = true;
                var location=[];
                if(defaultFolderIds!=null){
                    var selectedNodeObj = currentTreeViewObj.selectNode(defaultFolderIds);
                    if(selectedNodeObj){
                        appComponent.set('v.currentSelectedItemObj',selectedNodeObj);
                        this.getBinderDetails(appComponent,docListCmp,selectedNodeObj ,undefined,undefined);
                        
                        //this logic for create dynamic breadCrumb, push selected nodeItem chain on Array.
                        while(selectedNodeObj.parentFolder){
                            location.push(selectedNodeObj.name);
                            selectedNodeObj = selectedNodeObj.parentFolder;
                            if(!selectedNodeObj.hasOwnProperty('parentFolder')){
                                location.push(selectedNodeObj.name);
                                break;
                            }
                        }
                        location.reverse();
                    }else if(selectedNodeObj==undefined) {
                        //this logic for when select binder as default
                        appComponent.set('v.currentSelectedItemObj', currentTreeViewObj);
                        this.getBinderDetails(appComponent,docListCmp,currentTreeViewObj ,undefined,undefined); 
                        location.push(currentTreeViewObj.name);
                    }  
                    docListCmp.set('v.currentLocation', location);
                    //this for search case change 'AllDocs' option to 'ThisFolder' option
                    //set item click path on temp
                    docListCmp.set('v.tempCurrentLocation', location);
                }
                cmpBinderList.set('v.binder', currentTreeViewObj);
                $A.util.addClass(cmpSpinner, 'slds-hide');
            } else if(response.getState() === 'ERROR'){
                this.handleError(response.getError()[0], appComponent);
                $A.util.addClass(cmpSpinner, 'slds-hide');
            }
        });
        $A.enqueueAction(action); 
    },
    
    /*
     * this method display binder list on ddl
     * @param cmpBinderList is a binderList component
     * @param appComponent is a app component.
     * 'getBinderList' is a controller method.
     */
    getBinderList:function(cmpBinderList,appComponent,defaultbinderId,defaultFolderName,docListCmp){
        var actionlist = cmpBinderList.get("c.getBinderList");
        //Set up the callback
        var self = this;
        actionlist.setParams({
            pageSize: 200,
            pageNumber: 0
        })
        actionlist.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                cmpBinderList.set("v.options", response.getReturnValue());
                var options=cmpBinderList.get('v.options');
                //default selected binder value id take first one
                var binderId='';
                if(options.length != 0){
                    if(defaultbinderId == null){
                        binderId=options[0].Id;
                    }else{
                        for(var i=0;i<options.length;i++){
                            if(options[i].Id == defaultbinderId){
                                binderId = defaultbinderId;
                                break;
                            }
                        }
                        //set defaultFolderName null when no default binder found
                        if(binderId == ''){
                            binderId = options[0].Id; 
                            docListCmp.set('v.currentLocation',options[0].Name);
                            defaultFolderName = null;
                        }
                    }
                    this.getBinderTreeView(cmpBinderList,binderId,appComponent,defaultFolderName,docListCmp);
                }else{
                    this.handleError({message : "No Binders found"}, appComponent);
                }
            } else if(response.getState() === 'ERROR'){
                this.handleError(response.getError()[0], appComponent);
            }
        });
        $A.enqueueAction(actionlist);
    },
  
    
    /**
    * Fetches the related documents related to a given controlled document.
    * @param {Object} component the container component that is being used
    * @param {Object} docListCmp the component that has the document list modal
    * @param {String} docId the id of the document that is to be shown
    * @param {String} docName the name of the document whose related list is being shown
    */
    getRelatedDocuments:function(component,docListCmp,docId, docName){
        var cmpSpinner= component.find('divLoader');
        var divmodal=docListCmp.find('divModel');
        var modalBackDrop = docListCmp.find('modalbackdrop');
        var actionShow = component.get("c.getRelatedDocuments");
        var layoutConfig = component.get('v.layout');
        //Set up the callback
        var self = this;
        actionShow.setParams({
            "documentId" : docId,
            "sortBy" : layoutConfig.sort.fields,
            "sortOrder": layoutConfig.sort.sortOrder
        });
        actionShow.setCallback(this, function(response) {
            if(response.getState() === 'SUCCESS') {
                var result = response.getReturnValue();
                this.setIcon(result);

                docListCmp.set("v.relatedDocuments", { name : docName, related:  result });
                $A.util.addClass(cmpSpinner, 'slds-hide');
                $A.util.addClass(divmodal, 'slds-fade-in-open slds-modal-backdrop');
                $A.util.addClass(modalBackDrop, 'slds-backdrop_open');
            } else if(response.getState() === 'ERROR'){
                this.handleError(response.getError()[0], component);
                $A.util.addClass(cmpSpinner, 'slds-hide');
            }
        });
        $A.enqueueAction(actionShow); 
    },
    /**
     * User should be able to make default folder
     * @param id is a binderId
     * @param folderId selected folder id
     * @param cmpBinderList component
     */ 
    makeDefault:function(component,id,folderId,cmpBinderList){
        var cmpSpinner= component.find('divLoader');
        var actionMakeDefult = component.get("c.setDefaultFolder");
        //Set up the callback
        actionMakeDefult.setParams({
            binderId: id,
            defaultFolder: folderId.toString()
        })
        actionMakeDefult.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                var divMakeDefault=cmpBinderList.find('divmakedefault');
                $A.util.addClass(divMakeDefault,'slds-hide');
                $A.util.addClass(cmpSpinner, 'slds-hide');
            }
            else if (response.getState() === "ERROR") {
                this.handleError(response.getError()[0], component);
                $A.util.addClass(cmpSpinner, 'slds-hide');
            }
            
        });
        $A.enqueueAction(actionMakeDefult);
    },
    /**
     * get default folder information
     * @param component is a main component
     * @param binderList is a binder drop down list component
     * @param docListCmp is a document details component
     */ 
    getDefaultFolderDetails:function(component,binderList,docListCmp){
        var cmpSpinner = component.find('divLoader');
        var actionDefaultInfo = component.get('c.getDefaultFolderInfo');
        actionDefaultInfo.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var objInfo=response.getReturnValue();
                if(objInfo!=null){
                    this.getBinderList(binderList,component,objInfo.compliancequest__BinderId__c,objInfo.compliancequest__DefaultFolder__c,docListCmp)
                }else{
                    this.getBinderDetails(component,docListCmp);
                    this.getBinderList(binderList,component,null,null,docListCmp)
                }
            } else if(response.getState() === 'ERROR'){
                this.handleError(response.getError()[0], component);
            }
            $A.util.addClass(cmpSpinner, 'slds-hide');
        });

        $A.enqueueAction(actionDefaultInfo); 
    },
    /**
     * get default folder information
     * @param component is a main component
     * @param docListCmp is a document details component
     */ 
    getField: function(docListCmp,component) {
        var lbl = component.get("c.getFieldLabels");
        lbl.setCallback(this, function(a) {
            docListCmp.set("v.Labels", a.getReturnValue());    
        });
        $A.enqueueAction(lbl);
    },

    /*
    * method to reset paging related attributes
    * @param component - parent component 
    */
    resetPaging: function(component){
        component.set('v.pageNumber', 1);
        component.set('v.folderSearchText', '');
        component.set('v.globalSearchText', '');
    },
    /**
     * Sets the document title given a location
     */
    setTitle: function(location) {
        var title = '';
        for (var i = 0; i < location.length; i++) {
            title = title + location[i] + ' | ';
        }
        document.title = title.slice(0, -3); //remove the outanding pipe and white spaces
    },
    
    handleError: function(error, component){
        if(error && component) {
            component.set('v.errorMsg', error.message);
        }
    }
})