({
    /**
     * this method load binderlist,binderDetails and binderTreeview on components.
     */
    initializeComponent: function(component, event, helper) {
        var docListCmp = component.find('docList');
        var binderList = component.find("binderList");
        // helper.getBinderDetails(component,docListCmp);
        //helper.getBinderList(binderList,component);
        var cmpSpinner = component.find('divLoader');
        $A.util.removeClass(cmpSpinner, 'slds-hide');
        helper.getDefaultFolderDetails(component, binderList, docListCmp);
    },

    /**
     * this method handle component events i.e on component nodeItem onClick and binder DDL onchange will fire this method.
     */
    handleComponentEvent: function(component, event, helper) {
        var docListCmp = component.find('docList');
        var binderList = component.find('binderList');

        // reset paging
        helper.resetPaging(component);
        
        //click treeview node case
        var itemObj = event.getParam('nodeObject');
        var selectedNodeObj = itemObj;
       
        var binder = binderList.get('v.binder');
        binder.select(itemObj);
        binderList.set('v.binder', binder);
        var headerTitle = (itemObj && itemObj.name) || event.getParam('headerTitle');
        component.set('v.currentSelectedItemObj', selectedNodeObj);
        var cmpSpinner = component.find('divLoader');
        var location = [];
        if (itemObj != undefined) {
            $A.util.removeClass(cmpSpinner, 'slds-hide');
            //this condition check for bind default location on breadcrumb
            if (!itemObj.hasOwnProperty('parentFolder')) {
                location.push(itemObj.name);
            } else {
                //this logic for create dynamic breadCrumb, push selected nodeItem chain on Array.
                while (itemObj.parentFolder) {
                    location.push(itemObj.name);
                    itemObj = itemObj.parentFolder;
                }
                var defaultLocation = docListCmp.get('v.currentLocation');
                location.push(defaultLocation[0]);
                location.reverse();
            }

            docListCmp.set('v.currentLocation', location);
            //this for search case change 'AllDocs' option to 'ThisFolder' option
            //set item click path on temp
            docListCmp.set('v.tempCurrentLocation', location);
            var divMakeDefault = binderList.find('divmakedefault');
            $A.util.removeClass(divMakeDefault, 'slds-hide');
        } else {
            docListCmp.set('v.currentLocation', location);
        }
        // when selected binder was change get binder Id
        var onChangeBinderId = event.getParam('selectedBinderId');

        if (onChangeBinderId != undefined) {
            location.push(headerTitle);
            $A.util.removeClass(cmpSpinner, 'slds-hide');
            docListCmp.set('v.currentLocation', location);
            helper.getBinderTreeView(binderList, onChangeBinderId, component);
        }
        helper.setTitle(location);

        helper.getBinderDetails(component, docListCmp, selectedNodeObj, undefined, undefined);
    },

    /**
     * this method handle when search for selected node item documents on document details
     */ 
    handleDocSearchTextEvent:function(component, event,helper){
        // reset attributes
        helper.resetPaging(component);

        var docSearchTextValue=event.getParam('docSearchTextValue');
        var searchSeparator=event.getParam('searchSeparatorValue');
        var selectedItemObj=component.get('v.currentSelectedItemObj');        
        var docListCmp = component.find('docList');
        var cmpSpinner = component.find('divLoader');
        $A.util.removeClass(cmpSpinner, 'slds-hide');

        if(searchSeparator!="AllDocs"){
            component.set('v.folderSearchText', docSearchTextValue);
            helper.getBinderDetails(component,docListCmp, selectedItemObj,undefined,docSearchTextValue);
            helper.setTitle(docListCmp.get('v.currentLocation'));
        }else{
            component.set('v.globalSearchText', docSearchTextValue);
            helper.getBinderDetails(component,docListCmp, null,docSearchTextValue,undefined);
            document.title = $A.get("$Label.compliancequest.CQ_UI_All_Docs");
        }
    },

    /**
     * This method will load the related documents for the selected document
     */
    showRelatedDocuments: function(component, event, helper) {
        var docListCmp = component.find('docList');
        var documentId = event.getParam('documentId');
        var docName = event.getParam('headerTitle');
        var cmpSpinner = component.find('divLoader');
        $A.util.removeClass(cmpSpinner, 'slds-hide');
        helper.getRelatedDocuments(component, docListCmp, documentId, docName);
    },

    /**
     *User should be able to search documents globally
     */
    docGlobalSearchFilter: function(component, event, helper) {
        var textCmp = component.find('txtSearchText');
        var textSearchValue = textCmp.get('v.value');
        var docListCmp = component.find('docList');
        var cmpSpinner = component.find('divLoader');
        $A.util.removeClass(cmpSpinner, 'slds-hide');
        helper.getBinderDetails(component, docListCmp, null, textSearchValue, undefined);
    },
    toggleSideBar: function(component, event, helper) {
        component.set('v.showsidebar', !component.get('v.showsidebar'));
    },
    /**
     * User should be able to make default folder
     */
    makeDefaultFolder: function(component, event, helper) {
        var binderList = component.find('binderList');
        var selectedNodeObj = component.get('v.currentSelectedItemObj');
        var selectPath = selectedNodeObj.getPath();
        var binderId = selectedNodeObj.getBinder().id;
        var cmpSpinner = component.find('divLoader');
        $A.util.removeClass(cmpSpinner, 'slds-hide');
        helper.makeDefault(component, binderId, selectPath==''?binderId:selectPath, binderList);
    },
    
    
    /**
    *   method requests for more records from the server for the current location(folder)
    */
    getMoreRecords : function(component, event, helper){

        var cmpSpinner= component.find('divLoader');        
        $A.util.removeClass(cmpSpinner, 'slds-hide');

        var docListCmp = component.find('docList');
        var selectedNodeObj =  component.get('v.currentSelectedItemObj');

        if(component.get('v.folderSearchText') !== ''){
            helper.getBinderDetails(component, docListCmp, selectedNodeObj, undefined, component.get('v.folderSearchText'));
        }else if(component.get('v.globalSearchText') !== ''){
            helper.getBinderDetails(component, docListCmp, null, component.get('v.globalSearchText'), undefined);  
        }
        else{
            helper.getBinderDetails(component, docListCmp, selectedNodeObj, undefined, undefined);
        }
    }
})