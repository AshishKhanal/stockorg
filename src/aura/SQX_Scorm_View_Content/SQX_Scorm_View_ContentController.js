({
    /**
     * This method is used to redirect scorm content preview
     */
    scormContentView: function(component, event, helper){
        var url = "/apex/compliancequest__SQX_View_Controlled_Document?id=" + component.get('v.recordId') + "";
        helper.navigateToUrl(url);
    }
})