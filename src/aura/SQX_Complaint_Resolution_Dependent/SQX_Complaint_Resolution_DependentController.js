({
    /**
     * validate resolution value
     */
    init: function(component, event, helper) {
        component.set('v.validate', function() {
            var val = component.get('v.resolutionVal');
            var isValid = val !== null && val.length > 0;
            return {'isValid':isValid,'errorMessage': $A.get("$Label.c.CQ_UI_Resolution_Is_Required")};
        });
    },
    /**
     * show required indicator
     */
    showRequiredFields: function(component, event, helper){
        $A.util.removeClass(component.find("compliancequest__Resolution__c"), "slds-hide");
    },
})