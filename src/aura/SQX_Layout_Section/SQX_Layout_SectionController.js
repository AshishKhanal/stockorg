({
    /**
     * Initializes the section UI based on the columns passed by the layout.
     */
    doInit: function (component, event, helper) {
        var section = component.get('v.layoutsection'),
            mainRecord = component.get('v.mainrecord'),
            col,
            colIdx,
            columnIndices = {},
            layoutItem,
            layoutIdx,
            maxColIdx,
            dbRequiredFields = component.get('v.dbrequiredfields'),
            dbReadFields = component.get('v.dbreadfields'),
            cols = section.layoutColumns,
            allColumns = [],
            BLANKITEM = { emptySpace : true };

        /**
         * Internally used to add layout item to the newly processed list for use in the layout
         */
        function addItemToLayout(layoutItem) {
            if(layoutItem) {
                if(layoutItem.field) {
                    columnIndices[layoutItem.field] = allColumns.length;
                    layoutItem.dbrequired = dbRequiredFields[layoutItem.field.toLowerCase()] && true;
                    layoutItem.dbreadfield = dbReadFields[layoutItem.field.toLowerCase()] && true;
                }
                allColumns.push(layoutItem);
                if(mainRecord[layoutItem.field]){
                    layoutItem.cqitemvalue = mainRecord[layoutItem.field];
                }
            } else {
                allColumns.push(BLANKITEM);
            }
        }
        if(mainRecord) {
            var maxColIdx = Math.max(cols[0].layoutItems.length,
                (cols[1] && cols[1].layoutItems.length) || 0),
                numberOfCols = cols.length,
                columnSelector;
            for(colIdx = 0; colIdx < maxColIdx; colIdx++) {
                for(columnSelector = 0; columnSelector < numberOfCols; columnSelector++) {
                    addItemToLayout(cols[columnSelector].layoutItems[colIdx]);
                }
            }

            // computes the size of columns dynamically
            // SF layouts are either 1 or 2 column which maps to 6 or 12 columns respectively in lightning
            component.set('v.columnSize', 12 / cols.length);
            component.set('v.columns', allColumns);
            component.set('v.columnIndices', columnIndices);
        }
    },
    /**
     * Toggle the section visibility i.e. hides/unhides a section when section header is clicked
     */
    toggleSection: function(component, event, helper) {
        $A.util.toggleClass(component.find('sectionContainer'), 'slds-is-open');
    },
    /**
     * Method that will be invoked by parent to set readonly in fields in layout section item
     */
    setReadonlyFields: function(component, event, helper) {
        var params = event.getParam('arguments'),
            fieldNames = params.fieldNames,
            readonly = params.setreadonly,
            colIndices = component.get('v.columnIndices'),
            columns = component.get('v.columns'),
            colIndex;

        for(var idx = 0; idx < fieldNames.length; idx++) {
            colIndex = colIndices[fieldNames[idx]];
            if(colIndex !== undefined) {
                //TODO: ensure we revert to original condition (initial layout's state) instead of blank
                columns[colIndex].behavior = readonly ? 'Readonly' : '';
            }
        }
        component.set('v.columns', columns);
    },
    /**
     * Method that will be invoked by parent to set required fields in layout section item
     */
    setRequiredFields: function(component, event, helper) {
        var params = event.getParam('arguments'),
            fieldNames = params.fieldNames,
            required = params.setrequired,
            colIndices = component.get('v.columnIndices'),
            columns = component.get('v.columns'),
            colIndex;

        for(var idx = 0; idx < fieldNames.length; idx++) {
            colIndex = colIndices[fieldNames[idx]];
            if(colIndex !== undefined) {
                //TODO: ensure we revert to original condition (initial layout's state) instead of blank
                columns[colIndex].behavior = required ? 'Required' : '';
            }
        }
        component.set('v.columns', columns);
    }
})