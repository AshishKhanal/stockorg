({
    /**
     * method the set field values when page loads  
     */
    handleLoad :function(component, event, helper) {
        component.find("compliancequest__SQX_Complaint").set("v.value", component.get("v.complaintId"));
        if(component.get("v.itemId")){
            component.find("compliancequest__SQX_Associated_Item").set("v.value", component.get("v.itemId"));
        }
        if(component.get("v.partId")){
            component.find("compliancequest__SQX_Part").set("v.value", component.get("v.partId"));
        }
        if(component.get("v.lotSerial")){
            component.find("compliancequest__Lot").set("v.value", component.get("v.lotSerial"));
        }
        component.find("compliancequest__Activity").set("v.value", "Receive");
        component.find("compliancequest__Performed_By").set("v.value",component.get("v.userName"));
        var today = $A.localizationService.formatDate(new Date(),"yyyy-MM-ddTHH:mm:ss");
        component.find("compliancequest__Performed_Date").set("v.value",today);        
    },

    /**
     * method the show success message when record is created
     */
    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.compliancequest.CQ_UI_SUCCESS"),
            "message": $A.get("$Label.compliancequest.CQ_UI_ACTIVITY_LOGGED_SUCCESSFULLY"),
            "type": "success"
        });
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();        
        var navigate = component.get("v.navigateFlow");
        navigate("FINISH");
        
    },
    /**
     * method to show spinner when submit the form
     */
    handleSubmit : function(component, event, helper) {
        var activitySpinner = component.find('logActivitySpinner');
        $A.util.removeClass(activitySpinner, 'slds-hide');
        $A.util.addClass(activitySpinner, 'slds-show');
    },
    /**
     * method to hide spinner when error occurs
     */
    handleError : function(component, event, helper) {
        var activitySpinner = component.find('logActivitySpinner');
        $A.util.removeClass(activitySpinner, 'slds-show');
        $A.util.addClass(activitySpinner, 'slds-hide');
    }
})