({
    /**
     * method to handle the module filter click event
     */
    clickedFilterItem: function(component, event, helper) {
        var selectedFilter = component.find('liModuleFilter');
        var applyFilter = component.getEvent('onApplyFilter');
        applyFilter.fire();
        $A.util.toggleClass(selectedFilter, 'slds-is-selected slds-is-active');
    },
})
