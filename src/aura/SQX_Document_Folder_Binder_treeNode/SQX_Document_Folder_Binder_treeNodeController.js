({
    /**
     * this method bind treeView child hierarchy.
     */ 
    getChildren : function(cmp, event, helper) {
        var ret = helper.getChildren(cmp);
        event.getParam('callback')(ret);
    },
    
    /**
     * this method will fire when treeview item on click
     */ 
    fireEvent : function(cmp,event) {
        var cmpEvent = cmp.getEvent("folderChangeEvent");
        var itemObj=cmp.get('v.folder');
        cmpEvent.setParams({
            "nodeObject" : itemObj 
        });
        cmpEvent.fire();
    },
    
    /**
    * this method will expand/collapse the tree node
    */
    toggleNode : function(cmp, event){
        var isExpanded = cmp.get('v.folder.expanded');
        cmp.set('v.folder.expanded', !isExpanded);
        event.stopPropagation();
    }
})