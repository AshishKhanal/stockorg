({
    
    /**
     * this method used to close pop-up
     */
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    /**
     * this method is used to recall or reassign approval request
     */ 
    approvalRequest: function(component, event, helper){
        var param ={
            recordId: component.get('v.mainRecordId'),
            comment: component.find('txtComment').get('v.value')
        }
        if(component.get('v.typeOfApproval') == 'changeOrderApproval') {
            helper.invokeAction(component.get('c.recallChangeOrderApproval'), {
                documentId : component.get('v.mainRecordId')
            }).then(function(result) {
                if(result.infoMessages) {
                    helper.showMessage(result.infoMessages.join('.'), 'info', '', 'dismissible');
                }
                if(result.errorMessages) {
                    helper.displayError(result.errorMessages.join('.'),component);
                }
                if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                    helper.showMessage($A.get("$Label.c.CQ_UI_Document_Recalled_Toast_Msg"), 'success', '', 'dismissible');
                    component.set("v.isOpen", false);
                    helper.navigateToUrl('/' + component.get('v.mainRecordId'));	
                }
            })['catch']($A.getCallback(function(error) {
                helper.displayError(error, component);
            }));
        
        } else {
            helper.invokeAction(component.get('c.approvalReq'), param).then(function(response) {
                helper.showMessage($A.get("$Label.c.CQ_UI_Recalled_Approval_Request"), 'success', '', 'dismissible');
                component.set("v.isOpen", false);
                $A.get('e.force:refreshView').fire();
            })['catch'](function (err) {
                helper.displayError(err, component);
            });
        }
    }
})