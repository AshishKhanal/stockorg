({

    /**
     * Method to submit asset generation and submission record creation request
     */
    submitRecord : function(component, event, helper) {

        try {

            helper.showLoading(component);

            component.set('v.columns', [{
                label: $A.get('$Label.c.CQ_UI_Error'),
                fieldName: 'message',
                type: 'text'
            }]);

            var recordId = component.get('v.recordId');

            var REMOTE_METHODS = {
                prepareRecord : component.get('c.prepareRecordForSubmission'),
                submit : component.get('c.submit'),
                poll : component.get('c.pollSubmission')
            };

            helper
            .invokeAction(REMOTE_METHODS.prepareRecord, { 'recordId': recordId })	// set submission number (in case of Medwatch)
            .then($A.getCallback(function (errors) {

                if(errors && errors.length > 0) {
                    throw errors;
                }

                return helper
                .invokeAction(REMOTE_METHODS.submit, { 'recordId' : recordId }); // call to export function
            }))
            .then(function (jobId) {
                return new Promise($A.getCallback(function (resolve, reject) { // poll job status
                    var interval = setInterval(
                        $A.getCallback(function () {
                            helper
                            .invokeAction(REMOTE_METHODS.poll, { 'jobId': jobId, 'recordId': recordId })
                            .then(function (pollInfoString) {
                                var pollInfo = JSON.parse(pollInfoString);
                                if (pollInfo.status !== 'Pending') {
                                    clearInterval(interval);
                                    resolve(pollInfo);
                                }
                            })
                            ['catch'](function (error) {
                                clearInterval(interval);
                                reject(error);
                            });
                        }), 5000);

                }));
            })
            .then(function (result) {

                if(result.submissionRecordId) {
                    // [TODO] : Use lightning:navigation instead

                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result.submissionRecordId,
                        "slideDevName": "related"
                    });
                    navEvt.fire();

                } else {
                    // there must have been an error
                    throw result.response;
                }

            })['catch']($A.getCallback(function (error) {
                helper.displayError(component, error);
                helper.hideLoading(component);
            }))['finally'](function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            helper.displayError(component, error);
            helper.hideLoading(component);
        }

    }
})