({

    /**
     * Reads contents of the given file
     * Borrowed from : https://stackoverflow.com/a/33542499
     * @param file file to be read
     */
    downloadFile : function (content, fileName, mimeType) {

        return new Promise ($A.getCallback(function (resolve, reject) {

            var blob = new Blob([content], {type: mimeType});
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, fileName);
            }
            else{
                var elem = window.document.createElement('a');
                elem.href = window.URL.createObjectURL(blob);
                elem.download = fileName;
                document.body.appendChild(elem);
                elem.click();
                document.body.removeChild(elem);
                window.URL.revokeObjectURL(elem.href);

                resolve();
            }

        }));
    }
})