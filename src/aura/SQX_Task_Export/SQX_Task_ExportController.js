({
    exportRecord : function (component, event, helper) {

        $A.util.addClass(component.getSuper().find("cq-spinner"), "slds-is-static"); // this will ensure the spinner doesn't go outside popup

        helper.showLoading(component);

        if (!component.get('v.recordError')) {
            var taskId = component.get('v.recordId'),
                taskName = component.get('v.simpleRecord.Name');

            var fileName = taskName + '.json';

            try {
                helper
                .invokeAction(component.get('c.export'), {
                    'recordIds' : [taskId],
                    'relationshipsToFollow' : [
                        'compliancequest__SQX_Task_Question__c.compliancequest__SQX_Task__c',
                        'compliancequest__SQX_Answer_Option__c.compliancequest__Question__c',
                        'compliancequest__SQX_Answer_Option_Attribute__c.compliancequest__SQX_Answer_Option__c'
                    ]
                })
                .then(function (jsonResult) {
                    return helper.downloadFile(jsonResult, fileName, 'text/plain');
                })
                .then(function() {
                    $A.get("e.force:closeQuickAction").fire();
                })
                ['catch'](function (err) {
                    helper.displayError(err);
                })
                ['finally'](function () {
                    helper.hideLoading(component);
                })

            } catch (error) {
                helper.displayError(error);
                helper.hideLoading(component);
            }

        } else {
            helper.displayError(component.get('v.recordError'));
            helper.hideLoading(component);
        }

    }
})