({
    /**
     * Common helper method to invoke any action on the server side and return a promise. This does have an
     * issue when response must be preprocessed prior to the returning the result. In such case individual helper
     * method should be used to provide the preprocessing and then return the result
     * @param {Object} methodAction the action object that can be enqueued in lighting to be performed
     * @param {Object} methodParams the parameters to be sent with the request
     */
    invokeAction : function(methodAction, methodParams) {
        var that = this;
        if(methodParams) {
            methodAction.setParams(methodParams);
        }
        var promise = new Promise($A.getCallback(function(resolve, reject){
            methodAction.setCallback(that, $A.getCallback(function(response) {
                if(response.getState() === 'SUCCESS') {
                    resolve(response.getReturnValue());
                } else {
                    reject(response.getError());
                }
            }));
        }));

        $A.enqueueAction(methodAction);

        return promise;
    },
    /**
     * Parses the default values sent in the URL in base 64 encoded form and sets it in the default value
     */
    loadDefaultValuesFromUrl: function(component, event) {
        var cqdefaultParam = window.location.search.match(/cqdefault=([a-zA-Z0-9+\/]+[%23BD]{0,3})/i),
            defaultval,
            regexp,
            match;
        if(cqdefaultParam !== null && cqdefaultParam.length == 2) {
            try {
                defaultval = decodeURIComponent(escape(atob(decodeURIComponent(cqdefaultParam[1]))));
                defaultval = JSON.parse(defaultval);
            } catch (ex) {
                this.displayError('Invalid default values passed');
                defaultval = null;
            }
        }

        regexp = /cq\_([a-z\_0-9]+)=([a-zA-Z0-9+\/]+[%23BD]{0,3})/ig;
        while((match = regexp.exec(window.location.search)) !== null) {
            defaultval = defaultval || {};
            defaultval[match[1]] = match[2];
        }

        return defaultval;
    },
    /**
     * Helper method to show any type of lightning ui's toast message
     * @param {string} message - the message to display in toast
     * @param {string} type - One of 'error', 'info', 'warning' to display the message in particular theme
     * @param {string} title - the title for the message
     * @param {string} mode - Lightning UI toast's mode.
     */
    showMessage: function(message, type, title, mode, toastDuration) {
        var TYPE_ERROR = 'error',
            toastEvent = $A.get('e.force:showToast');

        if (!title) {
            switch (type) {
                case TYPE_ERROR:
                    title = $A.get('$Label.compliancequest.CQ_UI_Error');
                    break;
                default:
                    title = '';
            }
        }

        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message,
            "mode": mode || 'sticky',
            "duration" : toastDuration
        });
        toastEvent.fire();
    },
    /**
     * Helper method to display error in Lightning toast
     */
    displayError : function(errors, component) {
        var idx,
            message,
            QUICK_ACTION = 'quickaction';

        message = errors;
        console.log(errors);

        if (errors.length && typeof(errors) !== 'string') {
            message = '';
            for (idx = 0; idx < errors.length; idx += 1) {
                message += errors[idx].message + '\n';
            }
        } else if(errors.message) {
            message = errors.message;
        }

        if (component && component.get('v.displaymode') === QUICK_ACTION) {
            component.set('v.errorMsg', message);
        } else {
            this.showMessage(message, 'error', '' , 'pester', 10000);
        }
    },
    /**
     * Performs a XMLHttpRequest to get JSON data from the given url
     */
    loadJSONFromUrl: function(url) {
        var httpRequest, retPromise,
            XHR_DONE = 4;
        // Old compatibility code, no longer needed.
        if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
            httpRequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) { // IE 6 and older
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }

        retPromise = new Promise($A.getCallback(function(resolve, reject) {
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === XHR_DONE) {
                    if (httpRequest.status === 200) {
                        try {
                            resolve(JSON.parse(httpRequest.responseText));
                        }
                        catch (ex) {
                            reject('Error occurred while parsing response from ' + url);
                        }
                    } else {
                        reject('An error occurred while connecting to ' + url);
                    }
                }
            };
        }));

        httpRequest.open('GET', url, true);
        httpRequest.send();

        return retPromise;
    },
    /**
     *  Navigates to a given reference using SF's Navigation Service.
     */
    navigateUsingService: function(navService, reference) {
        navService.generateUrl(reference)
        .then($A.getCallback(function(url) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url
            });
            urlEvent.fire();
        }), $A.getCallback(function(error) {
            helper.displayError(error);
        }));
    },

    /**
     * Helper method to navigate given url
     * @param 'url' navigate url link
     */
    navigateToUrl : function(url){
        var navigateToUrl = $A.get("e.force:navigateToURL");
        navigateToUrl.setParams({
            "url": url
        });
        navigateToUrl.fire();
    },

    /**
     * Redirects to the given reference using navigation service
     */
    navigateToUrlForReference: function(navService, reference) {
        navService.generateUrl(reference)
            .then($A.getCallback(function(url) {
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": url
                });
                urlEvent.fire();
            }), $A.getCallback(function(error) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": error.message,
                    "mode": "sticky"
                });
                toastEvent.fire();
            }));
    },

    /**
     * Renders spinner in UI
     * @param {*} component 
     */
    showLoading: function (component) {
        component.set('v.isProcessing', true);
    },

    /**
     * Hides spinner from UI
     * @param {*} component 
     */
    hideLoading: function (component) {
        component.set('v.isProcessing', false);
    }
})