({

    /**
     * This method handles functionality of first button i.e. it sets the current page number to 1
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    firstPage: function(component, event, helper) {
        component.set("v.currentPageNumber", 1);
    },

    /**
     * This method handles functionality of previous button i.e. it decreases the current page number by 1
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    prevPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber")-1, 1));
    },

    /**
     * This method handles functionality of next button and i.e. it increases the current page number by 1
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    nextPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber")+1, component.get("v.maxPageNumber")));
    },

    /**
     * This method handles functionality of last button i.e. it sets the current page number to maximum page number (last page no)
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    lastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
    }
})