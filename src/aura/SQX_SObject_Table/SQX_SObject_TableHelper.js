({
    START_PAGE: 1,
    /**
     * Used internally to prefix reference fields so that name fields are set with the prefix
     */
    REFERENCE_PREFIX: 'cq__i__',

    /**
     * List of configuration properties that can be passed to the component
     */
    CONFIG_PROPERTIES: ['hideCheckbox', 'mode','limitto', 'relationalField', 'relationshipName', 'objectImage', 'objectIcon', 'relatedObject', 'title', 'fields', 'filterJSON', 'orderByFields', 'pageSize', 'sortOrder', {source: 'headerActions', target: 'actions', json : true}, 'altFilter', 'showEditDeleteLink', 'rowEditable', {source: 'altFilterLabels', target: 'inactiveRecordsLabel', json:true}, 'relationalField',{source: 'rowActionsJSON', target:'rowActions', json:true}],

    /**
     * Default configuration for any type of configuration selected
     */
    DEFAULT_CONFIG: {
        'pageSize' : 25,
        'sortOrder' : true,
        'headerActions' : [],
        'showEditDeleteLink': false,
        'rowEditable': false,
        'altFilterLabels': '',
        'objectIcon': 'custom:custom9'
    },

    /**
     * Predefined configuration set for the elements
     */
    PREDEFINED_CONFIG: {
        'Doc Revision History' : {
            'relatedObject': 'compliancequest__SQX_Controlled_Document__c',
            'fields': 'compliancequest__Revision__c,compliancequest__Title__c,compliancequest__Date_Issued__c',
            'filterJSON': '{"logic":"and","filters":[{"field":"compliancequest__Document_Number__c","operator":"eq","usv_function":"currentRecord", "usv_param":"compliancequest__Document_Number__c"},{"field":"Id","operator":"neq","usv_function":"currentRecord", "usv_param":"Id"}]}',
            'orderByFields': 'compliancequest__Date_Issued__c',
            'altFilter': '{"field":"compliancequest__Date_Issued__c","operator":"lt","usv_function":"currentRecord", "usv_param":"compliancequest__Date_Issued__c"}'
        },
        'Approval': {
            'relatedObject': 'ProcessInstanceWorkItem',
            'objectIcon': 'standard:approval'
        },
        'Doc Training' : {
            'relatedObject': 'compliancequest__SQX_Personnel_Document_Training__c',
            'fields': 'Name,compliancequest__Status__c',
            'filterJSON': '{"field":"compliancequest__SQX_Controlled_Document__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}',
            'orderByFields': 'Name',
            'altFilter': '{"field":"compliancequest__Status__c","operator":"eq","s_value":"Pending"}',
            'showEditDeleteLink': true,
            'relationalField': 'compliancequest__SQX_Controlled_Document__c'
        },
        'Personnel Job Function' : {
            'relatedObject': 'compliancequest__SQX_Personnel_Job_Function__c',
            'fields': 'Name,compliancequest__SQX_Job_Function__c,compliancequest__Active__c,compliancequest__Training_Status__c,compliancequest__Current_Training_Program_Step__c,compliancequest__Activation_Date__c',
            'filterJSON': '{"field":"compliancequest__SQX_Personnel__c","operator":"eq","usv_function":"currentRecord","usv_param":"Id"}',
            'orderByFields': 'compliancequest__Activation_Date__c',
            'altFilter': '{"field":"compliancequest__Active__c","operator":"eq","b_value":true}',
            'showEditDeleteLink': true,
            'mode': 'fixedMode',
            'relationalField': 'compliancequest__SQX_Personnel__c',
            'hideCheckbox': true 
        },
        'Personnel Training' : {
            'relatedObject': 'compliancequest__SQX_Personnel_Document_Training__c',
            'fields': 'Name,compliancequest__SQX_Controlled_Document__c,compliancequest__SQX_Assessment__c,compliancequest__Status__c,compliancequest__SQX_Trainer__c,compliancequest__Due_Date__c',
            'filterJSON': '{"field":"compliancequest__SQX_Personnel__c","operator":"eq","usv_function":"currentRecord","usv_param":"Id"}',
            'orderByFields': 'Name',
            'altFilter': '{"logic":"or","filters":[{"field":"compliancequest__Status__c","operator":"eq","s_value":"Pending"},{"field":"compliancequest__Status__c","operator":"eq","s_value":"Trainer Approval Pending"}]}',
            'showEditDeleteLink': true,
            'mode': 'fixedMode',
            'relationalField': 'compliancequest__SQX_Personnel__c',
            'hideCheckbox': true 
        },
        'Complete Personnel Trainings' : {
           'relatedObject': 'compliancequest__SQX_Personnel_Document_Training__c',
            'fields': 'Name,compliancequest__SQX_Controlled_Document__c,compliancequest__SQX_Assessment__c,compliancequest__Status__c,compliancequest__SQX_Trainer__c,compliancequest__Due_Date__c',
            'filterJSON': '{"field":"compliancequest__SQX_Personnel__c","operator":"eq","usv_function":"currentRecord","usv_param":"Id"}',
            'orderByFields': 'Name',
            'altFilter': '{"logic":"or","filters":[{"field":"compliancequest__Status__c","operator":"eq","s_value":"Complete"},{"field":"compliancequest__Status__c","operator":"eq","s_value":"Trainer Approval Pending"}]}',
            'showEditDeleteLink': false,
            'mode': 'fixedMode',
            'relationalField': 'compliancequest__SQX_Personnel__c',
            'hideCheckbox': true 
        },
        'Personnel Assessment' : {
            'relatedObject': 'compliancequest__SQX_Personnel_Assessment__c',
            'fields': 'Name,compliancequest__SQX_Assessment__c,compliancequest__SQX_Last_Assessment_Attempt__c,compliancequest__Result__c',
            'filterJSON': '{"field":"compliancequest__SQX_Personnel__c","operator":"eq","usv_function":"currentRecord","usv_param":"Id"}',
            'orderByFields': 'compliancequest__SQX_Last_Assessment_Attempt__c',
            'altFilter': '{"field":"compliancequest__Result__c","operator":"neq","s_value":""}',
            'showEditDeleteLink': false,
            'mode': 'fixedMode',
            'relationalField': 'compliancequest__SQX_Personnel__c',
            'hideCheckbox': true 
        },
        'Sample Tracking' : {
            'relatedObject': 'compliancequest__SQX_Sample_Tracking__c',
            'fields': 'compliancequest__Part_Lot__c,compliancequest__Activity__c,compliancequest__Performed_By_Date__c,compliancequest__From_To_Location__c',
            'filterJSON': '{"field":"compliancequest__SQX_Complaint__c","operator":"eq","usv_function":"currentRecord","usv_param":"Id"}',
            'orderByFields': 'compliancequest__Performed_Date__c',
            'showEditDeleteLink': false,
            'relationalField': 'compliancequest__SQX_Complaint__c',
            'sortOrder':'desc',
            'mode': 'relatedlist',
            'hideCheckbox': true 
        },
        'Product History Answers' : {
            'relationshipName': 'compliancequest__SQX_Product_History_Answers__r',
            'relatedObject': 'compliancequest__SQX_Product_History_Answer__c',
            'fields': 'compliancequest__Question__c, compliancequest__Answer__c, compliancequest__Notes__c',
            'filterJSON': '{"field":"compliancequest__SQX_Product_History_Review__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}',
            'orderByFields': 'compliancequest__Question_Number__c',
            'relationalField': 'compliancequest__SQX_Product_History_Review__c',
            'rowEditable': true,
            'mode': 'relatedlist',
            'pageSize': 5
        },
        'Supplier Steps': {
            'fields': 'compliancequest__Name_Formula__c,compliancequest__SQX_User__c,compliancequest__Start_Date__c,compliancequest__Completion_Date__c',
            'filterJSON': '{"field":"compliancequest__SQX_Parent__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}',
            'orderByFields': 'compliancequest__Name_Formula__c',
            'showEditDeleteLink': true,
            'relationalField': 'compliancequest__SQX_Parent__c',
            'mode': 'relatedlist',
            'relationshipName': 'compliancequest__SQX_Steps__r',
            'pageSize': 5
        },     
        'Complaint Task Steps': {
            'relatedObject': 'compliancequest__SQX_Complaint_Task__c',
            'fields': 'Name,compliancequest__SQX_User__c,compliancequest__Start_Date__c,compliancequest__Completion_Date__c',
            'filterJSON': '{"field":"compliancequest__SQX_Complaint__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}',
            'orderByFields': 'Name',
            'showEditDeleteLink': true,
            'relationalField': 'compliancequest__SQX_Complaint__c',
            'mode': 'relatedlist',
            'relationshipName': 'compliancequest__SQX_Complaint_Tasks__r',
            'pageSize': 5
        },       
        'Change order Plan Implementation' : {
           'relatedObject': 'compliancequest__SQX_Implementation__c',
            'fields': 'compliancequest__Title__c,compliancequest__Type__c,compliancequest__SQX_User__c,compliancequest__Due_Date__c,compliancequest__Status__c',
            'filterJSON': '{"logic": "and", "filters": [ {"field":"compliancequest__SQX_Change_Order__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}, {"field":"RecordTypeId","operator":"eq","s_value":"Plan","usv_function":"reference", "usv_param": "DeveloperName"}]}',
            'orderByFields': 'Name',
            'showEditDeleteLink': true,
            'relationalField': 'compliancequest__SQX_Change_Order__c',
            'relationshipName': 'compliancequest__SQX_Implementations__r',
            'objectIcon': 'standard:maintenance_plan',
            'pageSize': 5,
            'limitto': 'Plan',
            'hideCheckbox': true 
        },        
        'Change order Action Implementation' : {
           'relatedObject': 'compliancequest__SQX_Implementation__c',
            'fields': 'compliancequest__Title__c,compliancequest__Type__c,compliancequest__SQX_User__c,compliancequest__Due_Date__c,compliancequest__Status__c',
            'filterJSON': '{"logic": "and", "filters": [ {"field":"compliancequest__SQX_Change_Order__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}, {"field":"RecordTypeId","operator":"eq","s_value":"Action","usv_function":"reference", "usv_param": "DeveloperName"}]}',
            'orderByFields': 'Name',
            'showEditDeleteLink': false,
            'relationalField': 'compliancequest__SQX_Change_Order__c',
            'relationshipName': 'compliancequest__SQX_Implementations__r',
            'objectIcon': 'standard:task',
            'pageSize': 5,
            'hideCheckbox': true 
        },        
        'Change order Impact' : {
            'relatedObject': 'compliancequest__SQX_Impact__c',
            'fields': 'Name,compliancequest__SQX_Change_Order__c,compliancequest__Org_Site__c,compliancequest__SQX_Job_Function__c',
            'filterJSON': '{"field":"compliancequest__SQX_Change_Order__c","operator":"eq","usv_function":"currentRecord", "usv_param":"Id"}',
            'orderByFields': 'Name',
            'showEditDeleteLink': true,
            'relationalField': 'compliancequest__SQX_Change_Order__c',
            'relationshipName': 'compliancequest__SQX_Impacts__r',
            'pageSize': 5,
            'mode': 'relatedlist',
            'hideCheckbox': true 
        },
        'Decision Tree': {
            'relatedObject': 'compliancequest__SQX_Decision_Tree__c',
            'fields': 'Name,compliancequest__SQX_User__c,compliancequest__Run_Date__c,compliancequest__Reportable__c',
            'filterJSON': '{"field" : "compliancequest__SQX_Complaint__c", "operator" : "eq", "usv_function" : "currentRecord", "usv_param" : "Id"}',
            'orderByFields': 'Name',
            'showEditDeleteLink': false,
            'objectIcon' : 'custom:custom64',
            'relationalField': 'compliancequest__SQX_Complaint__c',
            'pageSize': 5,
            'hideCheckbox': true 
        },
        'Decision Tree Answer': {
            'relatedObject': 'compliancequest__SQX_DT_Answer__c',
            'fields': 'Name,compliancequest__Question_Text__c,compliancequest__Answer_Value__c,compliancequest__Comment__c',
            'filterJSON': '{"field" : "compliancequest__SQX_Decision_Tree__c", "operator" : "eq", "usv_function" : "currentRecord", "usv_param" : "Id"}',
            'orderByFields': 'compliancequest__Answer_Sequence__c',
            'objectIcon' : 'custom:custom64'
        },
        'Decision Tree History': {
            'relatedObject': 'compliancequest__SQX_Decision_Tree__c',
            'fields': 'Name,compliancequest__SQX_User__c,compliancequest__Run_Date__c,compliancequest__Comment__c,compliancequest__Reportable__c',
            'filterJSON': '{"logic" : "and" , "filters" : [{"field" : "compliancequest__SQX_Complaint__c", "operator" : "eq", "usv_function" : "currentRecord" , "usv_param" : "compliancequest__SQX_Complaint__c"}, {"field" : "compliancequest__SQX_Task__c", "operator" : "eq", "usv_function" : "currentRecord", "usv_param" : "compliancequest__SQX_Task__c"}, {"field" : "compliancequest__Is_Archived__c" , "operator" : "eq" , "b_value" : true}]}',
            'orderByFields': 'Name',
            'objectIcon': 'custom:custom64'
        }
    },
    /**
     * loads the configuration and sets appropriate attributes of the passed component
     */
    initializeConfiguration: function(config, cmp) {
        var helper = this,
            defaults = helper.DEFAULT_CONFIG,
            propKeys = helper.CONFIG_PROPERTIES,
            propSource, propTarget, targetVal,
            idx, prop, val, specificConfig;
        if(!defaults.altFilterLabels) {
            defaults.altFilterLabels = {
                on: $A.get('$Label.compliancequest.CQ_UI_Inactive_Records_Hiding'),
                off: $A.get('$Label.compliancequest.CQ_UI_Inactive_Records_Showing'),
                toggle: $A.get('$Label.compliancequest.CQ_UI_Inactive_Records_Hide')
            };

            helper.PREDEFINED_CONFIG['Doc Revision History'].objectIcon = 'standard:channel_program_history';
            helper.PREDEFINED_CONFIG['Doc Training'].objectImage = $A.get("$Resource.compliancequest__cqUI ") + '/styles/logos/training.png';

            helper.PREDEFINED_CONFIG['Doc Training'].altFilterLabels = {
                on: 'Showing Pending Records',
                off: 'Showing All Records',
                toggle: 'Show All Records'
            };

           // set the alternate toggle filter label
            helper.PREDEFINED_CONFIG['Personnel Job Function'].altFilterLabels = {
                on: $A.get('$Label.compliancequest.CQ_UI_Showing_Active_Records'),
                off: $A.get('$Label.c.CQ_UI_Show_Active_Records'),
                toggle: $A.get('$Label.c.CQ_UI_Show_All_Records')
            };

            helper.PREDEFINED_CONFIG['Personnel Training'].altFilterLabels = {
                on: $A.get('$Label.c.CQ_UI_Showing_Pending_Records'),
                off: $A.get('$Label.c.CQ_UI_Show_Pending_Records'),
                toggle: $A.get('$Label.c.CQ_UI_Show_All_Records')
            };

            helper.PREDEFINED_CONFIG['Personnel Assessment'].altFilterLabels = {
                on: $A.get('$Label.c.CQ_UI_Showing_Completed_Records'),
                off: $A.get('$Label.c.CQ_UI_Show_Completed_Records'),
                toggle: $A.get('$Label.c.CQ_UI_Show_All_Records')
            };

            helper.PREDEFINED_CONFIG['Doc Revision History'].title = $A.get('$Label.compliancequest.CQ_UI_Controlled_Document_Revision_History');

            helper.PREDEFINED_CONFIG['Change order Plan Implementation'].title = $A.get('$Label.compliancequest.CQ_UI_Change_Plan_Type');
            helper.PREDEFINED_CONFIG['Change order Action Implementation'].title = $A.get('$Label.compliancequest.CQ_UI_Change_Implementation_Type');
            helper.PREDEFINED_CONFIG['Change order Impact'].title = $A.get('$Label.compliancequest.CQ_UI_Impacts');
            helper.PREDEFINED_CONFIG['Doc Revision History'].title = $A.get('$Label.c.CQ_UI_Controlled_Document_Revision_History');
            helper.PREDEFINED_CONFIG['Doc Revision History'].altFilterLabels = {
                on: $A.get('$Label.c.CQ_UI_Showing_Previous_Revisions'),
                off: $A.get('$Label.c.CQ_UI_Hide_Inactive_Revisions'),
                toggle: $A.get('$Label.c.CQ_UI_Show_All_Revisions')
            };
            
            helper.PREDEFINED_CONFIG['Complete Personnel Trainings'].title = $A.get('$Label.compliancequest.CQ_UI_Completed_Document_Trainings');
            helper.PREDEFINED_CONFIG['Decision Tree'].title = $A.get('$Label.c.CQ_UI_Decision_Trees');

            helper.PREDEFINED_CONFIG['Doc Training'].headerActions = JSON.stringify([
                {
                    id : "user_signoff",
                    label: $A.get('$Label.compliancequest.CQ_UI_Homepage_Action_User_Signoff'),
                    url: "/apex/compliancequest__SQX_PersonnelDocTraining_User_SignOff?dtnId="
                },
                {
                    id: "trainer_signoff",
                    label: $A.get('$Label.compliancequest.CQ_UI_Homepage_Action_Trainer_Signoff'),
                    url: "/apex/compliancequest__SQX_PersonnelDocTraining_Trainer_SignOff?dtnId="
                },
            ]);

            helper.PREDEFINED_CONFIG['Decision Tree History'].title = $A.get('$Label.c.CQ_UI_History');


            // set the title and row action attribute
            helper.PREDEFINED_CONFIG['Personnel Job Function'].rowActionsJSON = JSON.stringify([
                {
                    "label":$A.get('$Label.c.CQ_UI_Retrain'),
                    "name":"pjfRetrain",
                    "actionUrl":"/apex/compliancequest__SQX_Retrain_PersonnelJobFunction",
                    "urlParams":{
                                    "id":"Id"
                                },
                    "relatedListObject":"compliancequest__SQX_Personnel_Job_Function__c"
                }
            ]);

            helper.PREDEFINED_CONFIG['Personnel Training'].title = $A.get('$Label.c.CQ_UI_Pending_Document_Trainings');
            helper.PREDEFINED_CONFIG['Personnel Training'].rowActionsJSON = JSON.stringify([
                {
                    "label":$A.get('$Label.c.CQ_UI_Retrain'),
                    "name":"pdtRetrain",
                    "actionUrl":"/apex/compliancequest__SQX_Retrain_DocumentTraining",
                    "urlParams":{
                                    "id":"Id"
                                },
                    "relatedListObject":"compliancequest__SQX_Personnel_Document_Training__c"
                }
            ]);

            helper.PREDEFINED_CONFIG['Personnel Assessment'].title = $A.get('$Label.c.CQ_UI_Personnel_Assessments');
            helper.PREDEFINED_CONFIG['Personnel Assessment'].rowActionsJSON = JSON.stringify([
                {
                    "label":$A.get('$Label.c.CQ_UI_Take_Assessment'),
                    "name":"takeAssessment",
                    "actionUrl":"/apex/compliancequest__SQX_Assessment_Start",
                    "urlParams":{
                                    "astId":"compliancequest__SQX_Assessment__c",
                                    "psnId":"mainRecordId"
                                },
                    "relatedListObject" :"compliancequest__SQX_Personnel_Assessment__c"
                },
                {
                    "label":$A.get('$Label.c.CQ_UI_Retake_Assessment'),
                    "name":"retakeAssessment",
                    "actionUrl":"/apex/compliancequest__SQX_Assessment_Start",
                    "urlParams":{
                                    "astId":"compliancequest__SQX_Assessment__c",
                                    "psnId":"mainRecordId"
                                },
                    "relatedListObject" :"compliancequest__SQX_Personnel_Assessment__c"
                }
            ]);
            
            helper.PREDEFINED_CONFIG['Sample Tracking'].title = $A.get('$Label.c.CQ_UI_Sample_Tracking');
        }

        specificConfig = helper.PREDEFINED_CONFIG[config];
        for(idx = 0; idx < propKeys.length; idx++) {
            prop = propKeys[idx];
            propSource = prop['source'] || prop;
            propTarget = prop['target'] || prop;


            val = cmp.get('v.' + propSource);
            targetVal = val;
            if(val === undefined || val === null || (val === false && typeof(val) === "boolean")) {
                targetVal = (specificConfig && specificConfig[propSource]) || defaults[propSource];
            }

            try {
                if(typeof(targetVal) === "string" && prop['json']) {
                    targetVal = JSON.parse(targetVal);
                }
            } catch(ex) {
                //ignore json error
            }
            cmp.set('v.' + propTarget, targetVal || null);
        }
    },

    /**
     * Deletes the record for given record Id
     */
    deleteRecord: function(deletor, recordId) {
        deletor.setParams({
            recordToDelete : recordId
        });

        var promise = new Promise($A.getCallback(function(resolve, reject){
            deletor.setCallback(this, function(response) {

                if(response.getState() === 'SUCCESS') {
                    resolve();
                }
                else {
                    reject(response.getError()[0]);
                }
            });
        }));

        $A.enqueueAction(deletor);

        return promise;
    },
    /**
     * Alters the data returned to server to match with Datatable format
     */
    modifyToDataToSObjectTableData: function (returnVal, config, showEditDeleteLink, rowActions, navService, isRowEditable) {
        var helper = this;
        var promise = new Promise($A.getCallback(function(resolve, reject){
            var idx, fields, actions = [], pageReference,
                promises = [];

            if(config === 'Approval') {
                returnVal.fields = [
                    {type: "url", sortable: false, fieldName: "ApproveReject", label: 'Action', fixedWidth: 136 , typeAttributes: {label: $A.get("$Label.compliancequest.CQ_UI_Approve_Reject")}},
                    {type: "text", sortable: false, fieldName: "NodeName", label: $A.get("$Label.compliancequest.CQ_UI_Name")},
                    {type: "date", sortable: false, fieldName: "Date", label: $A.get("$Label.compliancequest.CQ_UI_Date")},
                    {type: "text", sortable: false, fieldName: "Status", label: $A.get("$Label.compliancequest.CQ_UI_Status")},
                    {type: "text", sortable: false, fieldName: "AssignedToName", label: $A.get("$Label.compliancequest.CQ_UI_Assigned_To")},
                    {type: "text", sortable: false, fieldName: "ActualApproverName", label: $A.get("$Label.compliancequest.CQ_UI_Actual_Approver")},
                    {type: "text", sortable: false, fieldName: "Comments", label: $A.get("$Label.compliancequest.CQ_UI_Comments")}
                ];
                var data = [], pi,widx, wi;
                for(idx = 0; idx < returnVal.sobjects.length; idx++) {
                    pi = returnVal.sobjects[idx];
                    if(pi.StepsAndWorkitems){
                        for(widx = 0; widx < pi.StepsAndWorkitems.length; widx++) {
                            wi = pi.StepsAndWorkitems[widx];
                            wi.Date = wi.CreatedDate;
                            wi.Status = wi.StepStatus;
                            wi.AssignedToName = wi.Actor.Name;
                            wi.ActualApproverName = wi.OriginalActor.Name;
                            wi.NodeName = wi.ProcessNode && wi.ProcessNode.Name;
                            if(wi.IsPending) {
                                pageReference = {
                                    type:'standard__component',
                                    attributes: {
                                    "componentName": "compliancequest__SQX_Approval_Document_Information"
                                    },
                                    state: {
                                        "compliancequest__recordId":pi.TargetObjectId,
                                        "compliancequest__origin": wi.Id
                                    }
                                };

                                var defaultURL = "#";

                                promises.push(helper.resolvePageReference(navService, pageReference, wi));
                            }
                            data.push(wi);
                        }
                    }
                }

                returnVal.sobjects = data;

                Promise.all(promises).then(function() { 
                    resolve(returnVal);
                });

                return;
            }

            fields = returnVal.fields;
            for(idx = 0; idx < fields.length; idx++) {
                fields[idx].type = fields[idx].fieldType;
                delete fields[idx].fieldType;
                if(fields[idx].fieldName.indexOf('.') !== -1) {
                    fields[idx].fieldName = helper.remapField(returnVal.sobjects, fields[idx].fieldName);
                    fields[idx].sortable = false;
                    fields[idx].type = 'url';
                    fields[idx].typeAttributes = { label : {fieldName: helper.remapTypeAttributeField(returnVal.sobjects, fields[idx].fieldName) } };
                }

                if (!isRowEditable) {
                    delete fields[idx].editable;
                }
            }
            fields[0].typeAttributes = { label : {fieldName: fields[0].fieldName } };

            fields[0].fieldName = helper.REFERENCE_PREFIX + fields[0].fieldName;
            fields[0].type = 'url';

            actions = actions.concat(rowActions || []);

            if(returnVal.actions && returnVal.actions.length) {
                returnVal.actions.forEach(function(rowAction) {
                    actions.push({label: rowAction.compliancequest__Label__c, name: rowAction.compliancequest__Label__c, config: rowAction})
                });
            }

            if(showEditDeleteLink || actions.length > 0) {
                actions = helper.getRowActions.bind(helper, showEditDeleteLink || false, actions || []);
                fields.push({type: 'action', typeAttributes: {'rowActions': actions}});
            }

            for(idx = 0; idx < returnVal.sobjects.length; idx++) {
                returnVal.sobjects[idx][fields[0].fieldName] = '/' + returnVal.sobjects[idx].Id;
                var sobjectKeys = Object.keys(returnVal.sobjects[idx]);
                var referenceKeys = sobjectKeys.filter(a => a.includes("__r"));
                for (var i=0; i<referenceKeys.length; i++) {
                    var field = Object.values(referenceKeys)[i];
                    if(!field.startsWith(helper.REFERENCE_PREFIX,0) && field.endsWith("__r")) {
                        var retValFieldLookup = returnVal.fields.find(a => a.fieldName.includes(field));
                        var index = returnVal.fields.indexOf(retValFieldLookup);
                        returnVal.sobjects[idx][fields[index].fieldName] = '/' + returnVal.sobjects[idx][field].Id;
                    }
            }
            }
            resolve(returnVal);

        }));

        return promise
    },
    /**
     * This method is used to resolve pagereference
     */
    resolvePageReference: function(navService, pageReference, wi) {
        return navService.generateUrl(pageReference)
                                      .then($A.getCallback(function(url){
                                          wi.ApproveReject = url;
                                      }))['catch']($A.getCallback(function(error) {
                                          wi.ApproveReject = defaultURL;
                                      }))  
      },
    /**
     * Dynamically returns row actions based on field values
     * @param addDefaultActions boolean value that determines if default actions(edit, delete) have to be added to rows
     * @param customActions dynamic row actions provided by user
     * @param row active row under consideration
     * @param callback function to be called upon task completion
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    getRowActions: function (addDefaultActions, customActions, row, callback) {

        var actions = [];

        if (addDefaultActions) {
            actions.push({ label: $A.get('$Label.c.CQ_UI_Grid_Edit'), name: 'edit' });
            actions.push({ label: $A.get('$Label.c.CQ_UI_Grid_Delete'), name: 'delete' });
        }

        if (customActions) {
            for (var i = 0; i < customActions.length; i++) {
                // pushing row level action for pjf object
                if (customActions[i].relatedListObject === 'compliancequest__SQX_Personnel_Job_Function__c'
                    && row['compliancequest__Active__c'] === true) {
                        actions.push(JSON.parse(JSON.stringify(customActions[i])));
                } else if ( customActions[i].relatedListObject === 'compliancequest__SQX_Personnel_Document_Training__c'
                            && row['compliancequest__Status__c']
                            && row['compliancequest__Status__c'] !== 'Pending' ) {
                    // pushing row level action for pdt object
                    actions.push(JSON.parse(JSON.stringify(customActions[i])));
                } else if ( customActions[i].relatedListObject === 'compliancequest__SQX_Personnel_Assessment__c'
                            && row['compliancequest__SQX_Assessment__c'] ) {
                    if (!row['compliancequest__Result__c'] && customActions[i].name === 'takeAssessment') {
                        // pushing 'take assessment' row level action for personnel assessment object
                        actions.push(JSON.parse(JSON.stringify(customActions[i])));
                    } else if (row['compliancequest__Result__c'] === 'Unsatisfactory' && customActions[i].name === 'retakeAssessment') {
                        // pushing 'retake assessment' row level action for personnel assessment object
                        actions.push(JSON.parse(JSON.stringify(customActions[i])));
                    }
                } else if(customActions[i].config) {
                    actions.push(JSON.parse(JSON.stringify(customActions[i])));
                }
            }
        }

        if (actions.length === 0) {
            actions.push({ label: $A.get('$Label.compliancequest.CQ_UI_NA_Msg'), name: 'N/A' });
        }

        // simulate a trip to the server
        setTimeout($A.getCallback(function () {
            callback(actions);
        }), 100);
    },

    /**
     * Method fires standard SF edit action for the given row record
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    fireEditAction: function (row) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": row.Id
        });
        editRecordEvent.fire();
    },

    /**
     * Method fires delete action for the given row record
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    fireDeleteAction: function (row, cmp) {

        var rows = cmp.get('v.resultRecords'), that=this;

        if (confirm($A.get('$Label.c.CQ_UI_Alert_Confirmation'))) {
            $A.util.removeClass(cmp.find('loadingIndicator'), 'slds-hide');
            that.deleteRecord(cmp.get('c.deleteRecord'), row.Id).then(function () {
                var rowIndex = rows.indexOf(row);
                rows.splice(rowIndex, 1);
                cmp.set('v.resultRecords', rows);
            })['catch'](function (error) {
                that.displayError(error);
            }).finally(function () {
                $A.util.addClass(cmp.find('loadingIndicator'), 'slds-hide');
            });
        }
    },

    /**
     * Method to fire actions associated with the custom row actions
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    fireCustomAction: function (row, action, cmp) {
        var rowActions = cmp.get('v.rowActions') || [],
            rowAction,
            urlEvent = $A.get("e.force:navigateToURL"),
            params = [],
            url,
            val,
            navService,
            pageReference,
            actionName = action.name,
            i = 0,
            LIGHTNING_COMPONENT = 'Lightning Component',
            VF_PAGE = 'VF Page';

        if(action.config) {
            if(action.config.compliancequest__Component_Type__c === LIGHTNING_COMPONENT) {
                navService = cmp.find('navService');
                pageReference = {
                    type: 'standard__component',
                    attributes: {
                        componentName: action.config.compliancequest__Namespace__c + '__' + action.config.compliancequest__Component_Name__c
                    },
                    state: {
                        "compliancequest__recordId": row.Id,
                        "compliancequest__origin": cmp.get('v.recordId')
                    }
                };
                navService.navigate(pageReference);
            } else if (action.config.compliancequest__Component_Type__c === VF_PAGE) {
                pageReference = '/apex/' + action.config.compliancequest__Namespace__c + '__' + action.config.compliancequest__Component_Name__c;
                pageReference += ('?recordId=' + encodeURIComponent(row.Id));
                pageReference += ('&origin=' + encodeURIComponent(cmp.get('v.recordId')));
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": pageReference
                });
                urlEvent.fire();
            }
        } else {
            for (; i < rowActions.length; i++) {
                if (rowActions[i].name === actionName) {
                    rowAction = rowActions[i];
                    break;
                }
            }

            if (rowAction) {
                url = rowAction.actionUrl;
                if (rowAction.urlParams) {
                    for (var key in rowAction.urlParams) {
                        if (rowAction.urlParams[key] === 'mainRecordId') {
                            val = cmp.get('v.recordId');
                        }
                        else {
                            val = row[rowAction.urlParams[key]];
                        }

                        params.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
                    }
                    url = url + '?' + params.join('&');
                }

                if (url) {
                    urlEvent.setParams({
                        "url": url
                    });
                    urlEvent.fire();
                }
            }
        }
    },

    /**
     * Used to map lookup fields to reference fields for displaying in the UI.
     */
    remapField: function(sobjects, fieldName) {
        var newName = this.REFERENCE_PREFIX + fieldName.replace('.', '$$'),
            path = fieldName.split('.'),
            idx,
            helper = this;

        for(idx = 0; idx < sobjects.length; idx++) {
            sobjects[idx][newName] = helper.getValue(sobjects[idx], path);
        }

        return newName;
    },
    remapTypeAttributeField: function(sobjects, fieldName) {
        var newName = this.REFERENCE_PREFIX + fieldName.replace('.', '%%'),
            path = fieldName.split('.'),
            idx,
            helper = this;
        for(idx = 0; idx < sobjects.length; idx++) {
            sobjects[idx][newName] = helper.getValue(sobjects[idx], path);
        }

        return newName;
    },

    /**
     * internal method to get a value of specific location in the json. This method can traverse dotted notation
     */
    getValue: function(sobj, path) {
        var retVal = undefined,
            helper = this;

        if(sobj[path[0]]) {
            retVal = path.length === 1 ? sobj[path[0]] : helper.getValue(sobj[path[0]], path.slice(1));
        }

        return retVal;
    }
})