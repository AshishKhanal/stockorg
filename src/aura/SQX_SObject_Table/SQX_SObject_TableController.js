/**
 * Controller to contain logic to render/display items in a tabular/related list format
 */
({
    /**
     * refreshes the table when SF's view is refreshed i.e. handles refresh view event.
     */
    refreshTable: function(component, event, helper) {
        component.set('v.pageNumber', helper.START_PAGE);
        $A.enqueueAction(component.get('c.loadData'));
    },
    /**
     * Initializes the component with proper data table, header action and data.
     */
    onInit: function(component, event, helper) {
        let fields = ['Id'];
        if(component.get("v.sObjectName") == 'compliancequest__SQX_Controlled_Document__c') {
            fields.push('compliancequest__Approval_Status__c');
        }
        component.set("v.recordFields", fields);
        
        var config = component.get('v.configuration');
        if(config != 'Approval'){
            helper.initializeConfiguration(config, component);
            $A.enqueueAction(component.get('c.loadData'));
        }
    },
    /**
     * Handler for toggling the alternate filter on/off
     */
    toggleAltFilter: function(component, event, helper) {
        var useAltFilter = component.get('v.useAltFilter');
        component.set('v.pageNumber', helper.START_PAGE);
        component.set('v.useAltFilter', !useAltFilter);
        if(component.get('v.configuration') !== 'Approval') {
             component.set('v.enableInfiniteLoading',  true);
        }
        
        $A.enqueueAction(component.get('c.loadData'));
    },
    /**
     * Fetches the data from the helper and sets it the component
     */
    loadData: function(component, event, helper) {
        var useAltFilter = component.get('v.useAltFilter'),
            altFilter,
            pageSize = component.get('v.pageSize'),
            mode = component.get('v.mode');

        if (mode === 'relatedlist') {
            pageSize++; //add one more column to show it as plus 1 in the header
        }

        if (useAltFilter) {
            altFilter = component.get('v.altFilter');
        }
        if(component.get('v.relatedObject') != undefined){
            $A.util.removeClass(component.find('loadingIndicator'), 'slds-hide');
            helper.invokeAction(component.get('c.getObjects'), {
                mainRecordId: component.get('v.recordId'),
                objectName: component.get('v.relatedObject'),
                fieldList: component.get('v.fields'),
                whereClause: component.get('v.filterJSON'),
                orderbyFields: component.get('v.orderByFields').replace(helper.REFERENCE_PREFIX, ''),
                'altFilter': altFilter,
                pageSize: pageSize,
                pageNumber: component.get('v.pageNumber'),
                sortAscending: component.get('v.sortOrder'),
                showEditDeleteLink: component.get('v.showEditDeleteLink'),
                purpose: component.get('v.configuration')
            }).then(function(data) {
                return helper.modifyToDataToSObjectTableData(data, component.get('v.configuration'), component.get('v.showEditDeleteLink'), component.get('v.rowActions'), component.find('navService'), component.get('v.rowEditable'));
            }).then($A.getCallback(function(data) {
                if (mode === 'relatedlist') {
                    //happens only in related list where plus on item is fetched
                    component.set('v.recordCount', data.sobjects.length);
                }
                component.set('v.resultRecords', data.sobjects.slice(0, component.get('v.pageSize')));
                component.set('v.resultColumns', data.fields);
                component.set('v.objectName', data.mainRecordName);
                // APPROVAL and relatedlist doesn't have any more pages, since we are returning everything
                if(component.get('v.configuration') === 'Approval') {
                    component.set('v.enableInfiniteLoading',  false);
                }
                var pageNum =  component.get('v.pageNumber');
                if(pageNum == 1) {
                    component.set('v.recordTypes', data.recordTypes);
                }
                component.set('v.pageNumber', pageNum + 1);
            }))['catch'](function (error) {
                helper.displayError(error, component);
            }).finally(function () {
                $A.util.addClass(component.find('loadingIndicator'), 'slds-hide');
            });
        }
    },
    /**
     * Handler for sorting records when using data table
     */
    sortRecords: function (component, event, helper) {
        component.set('v.pageNumber', helper.START_PAGE);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // assign the latest attribute with the sorted column fieldName and sorted direction
        component.set("v.sortOrder", sortDirection === 'asc');
        component.set("v.orderByFields", fieldName);
        
        if(component.get('v.configuration') !== 'Approval') {
             component.set('v.enableInfiniteLoading',  true);
        }

        $A.enqueueAction(component.get('c.loadData'));
    },
    /**
     * Header button action handler for component
     */
    onHeaderActionClicked: function(component, event, helper) {
        var btnId = event.target.getAttribute('data-btn-id'),
            actions = component.get('v.actions'),
            action,
            paramName,
            selectedItems,
            idx,
            sidx,
            selectedRows = component.get('v.selectedRows');

        if(selectedRows === "null") {
            alert('Nothing selected');
            return;
        }

        for(idx = 0; idx < actions.length; idx++) {
            action = actions[idx];
            if(action.id == btnId) {
                paramName = action.param || 'Id';
                selectedItems = '';
                for(sidx = 0; sidx < selectedRows.length; sidx++) {
                    selectedItems = encodeURIComponent(selectedRows[sidx][paramName]) + ",";
                }
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": action.url + selectedItems
                });
                urlEvent.fire();
                break;
            }
        }
    },
    /**
     * Handles saving from new drop down if multiple record types are present
     */
    handleNewMenu: function(cmp, event, helper) {
        var selectedMenuItemValue = event.getParam("value"),
            relatedObject = cmp.get('v.relatedObject'),
            relationalField = 'cq_' + cmp.get('v.relationalField'),
            id = cmp.get('v.recordId'),
            recordid = selectedMenuItemValue,
            navService = cmp.find("navService"),
            pageReference = {
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: relatedObject,
                    actionName: 'new'
                },
                state: {
                    recordTypeId: recordid,
                    additionalParams: relationalField + '=' + id
                }
            };

        helper.navigateToUrlForReference(navService, pageReference);
    },
    /**
     * Creates new record of given type
     */
    createNew: function(cmp, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var param = {
            "entityApiName": cmp.get('v.relatedObject'),
            "defaultFieldValues": {}
        };
        param.defaultFieldValues[cmp.get('v.relationalField')] = cmp.get('v.recordId');
        createRecordEvent.setParams(param);
        createRecordEvent.fire();
    },
    /**
     * Handles Row action for data table
     */
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action'),
            row = event.getParam('row');

        switch (action.name) {
            case 'edit':
                helper.fireEditAction(row);
                break;
            case 'delete':
                helper.fireDeleteAction(row, cmp);
                break;
            default:
                helper.fireCustomAction(row, action, cmp);
                break;
        }
    },
    /**
     * called when datatable items are selected rows on the data table component
     */
    setSelectedRows: function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');

        component.set('v.selectedRows', selectedRows);
    },
    /**
     * handler for managing pagination of the data
     */
    loadMoreData: function (component, event, helper) {
        var useAltFilter = component.get('v.useAltFilter'),
            altFilter;
        if(useAltFilter) {
            altFilter = component.get('v.altFilter');
        }

        //Display a spinner to signal that data is being loaded
        event.getSource().set("v.isLoading", true);
        var mRId = component.get('v.recordId');
        helper.invokeAction(component.get('c.getObjects'), {
            mainRecordId: mRId,
            objectName: component.get('v.relatedObject'),
            fieldList: component.get('v.fields'),
            whereClause: component.get('v.filterJSON'),
            orderbyFields: component.get('v.orderByFields').replace(helper.REFERENCE_PREFIX, ''),
            'altFilter': altFilter,
            pageSize: component.get('v.pageSize'),
            pageNumber: component.get('v.pageNumber'),
            sortAscending: component.get('v.sortOrder'),
            purpose: component.get('v.configuration')
        }).then(function(data) {
            return helper.modifyToDataToSObjectTableData(data, component.get('v.configuration'), component.get('v.showEditDeleteLink'), component.get('v.rowActions'), component.find('navService'), component.get('v.rowEditable'));
        }).then(function(data) {
            var oldData = component.get('v.resultRecords') || [];
            oldData = oldData.concat(data.sobjects);
            component.set('v.resultRecords', oldData);
            var pageNum = component.get('v.pageNumber');
            var pageSize = component.get('v.pageSize');
            component.set('v.enableInfiniteLoading',  data.sobjects.length === pageSize);
            component.set('v.pageNumber', pageNum+1);
        })['catch'](function(error) {
             helper.displayError(error, component);
        }).finally(function(){
            event.getSource().set("v.isLoading", false);
        });
    },
    /**
     * Handles event when view all button is clicked on the ui. It redirects to standard related list in lighting mode.
     */
    onViewAllClick: function (component, event, helper) {
        var recordId = component.get('v.recordId'),
            relatedField = component.get('v.relationshipName'),
            navService = component.find("navService"),
            pageReference;

        pageReference = {
            "type": "standard__recordRelationshipPage",
            "attributes": {
                "recordId": recordId,
                "objectApiName": component.get('v.objectName'),
                "relationshipApiName": relatedField,
                "actionName": "view"
            }
        };

        helper.navigateToUrlForReference(navService, pageReference);
    },

    /**
     * Method invoked when save action is requested from the data table
     * Saves updated records to db
     */
    saveRows: function(component, event, helper) {

        event.getSource().set('v.isLoading', true);

        helper
        .invokeAction(component.get('c.saveRecords'), { recordsList : event.getParam('draftValues') })
        .then(function () {
            var refreshEvent = $A.get("e.force:refreshView");
            if(refreshEvent){
                refreshEvent.fire();
            }
        })
        ['catch'](function (errors) {
            errors = (errors && errors.map(function (error) { return error.message })) || []; // just show error messages

            component.set('v.errors', {
                table: {
                    title: $A.get('$Label.CQ_UI_ERR_MSG_SAVE_FAILED'),
                    messages: JSON.stringify(errors)
                }
            });
        })
        ['finally'](function () {
            event.getSource().set('v.isLoading', false);
        });
    }
})