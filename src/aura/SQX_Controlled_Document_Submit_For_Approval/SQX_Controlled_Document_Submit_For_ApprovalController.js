({
    /**
     * This method is used to submit control doc record for approval process.
     */
    submitForApproval: function(component, event, helper) {
        component.set('v.spinnerText',$A.get("$Label.compliancequest.CQ_UI_Submit_For_Synchronization_Started"));
        helper.invokeAction(component.get('c.syncDoc'), {
            documentId: component.get('v.recordId')
        }).then(function(result) {
            if(result.infoMessages) {
                helper.showMessage(result.infoMessages.join('.'), 'info', '', 'dismissible');    
            }
            
            if(result.errorMessages) {
                helper.displayError(result.errorMessages.join('.'),component);    
            }
            if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                component.set('v.spinnerText',$A.get("$Label.compliancequest.CQ_UI_Submit_For_Approval_Started"));
                return helper.invokeAction(component.get('c.submitForapprovalProcess'), {
                    documentId: component.get('v.recordId')
                }).then($A.getCallback(function(data) {
                    helper.showMessage($A.get("$Label.c.CQ_UI_Document_Submitted_For_Approval"), 'success', '', 'dismissible');
                    $A.get('e.force:refreshView').fire();
                }))['catch']($A.getCallback(function(error){
                    helper.displayError(error, component);
                }));
            }
        })['catch']($A.getCallback(function(error){
            helper.displayError(error, component);
        })).finally($A.getCallback(function(){
            $A.get("e.force:closeQuickAction").fire();
        }));
    }
})