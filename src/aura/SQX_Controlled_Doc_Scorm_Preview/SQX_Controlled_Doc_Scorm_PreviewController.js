({
    /**
     * this method is use to get content information
     */
    onInit: function(component, event, helper) {
        var docId = component.get('v.recordId');
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        helper.invokeAction(component.get('c.getPreviewContent'), { 'docId': docId} ).then(function(data) {
            component.set('v.previewResult', data);
        })['catch'](function (err) {
            var errors = err.message || 'Unexpected error. Please try reloading';
            helper.displayError(errors, component);
        }).finally($A.getCallback(function() {
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        }));
    },
    
    /**
     * Handle to update scorm content value
     */
    handleToggleEvent: function(component, event, helper){
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        var scormValue = event.getParam("scormContent");
        helper.invokeAction(component.get('c.updateScormValue'), {
            scormVal : scormValue,
            recordId : component.get('v.recordId')
        }).then(function(result) {
            $A.get('e.force:refreshView').fire();
        })['catch']($A.getCallback(function(error) {
            helper.displayError(error, component);
        })).finally($A.getCallback(function() {
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        }));
    }
})