({
    onInit : function(component, event, helper) {
        component.set('v.recordId', component.get("v.pageReference").state.compliancequest__recordId);
        component.set('v.origin', component.get("v.pageReference").state.compliancequest__origin);

        var pageReference;
        if(component.get("v.pageReference").state.compliancequest__returnTo == 'homePage') {
            pageReference = {
                type: 'standard__namedPage',
                attributes: {
                    pageName: 'home'
                }
            };
        } else {
            pageReference = {
                'type': 'standard__recordPage',
                'attributes': {
                    'recordId': component.get('v.recordId'),
                    'objectApiName': 'compliancequest__SQX_Controlled_Document__c',
                    'actionName': 'view'
                }
            };
        }

        component.set('v.returnUrl', pageReference);
        helper.invokeAction(component.get('c.getCurrentApprovalProcessStepName'), {
            docId: component.get("v.pageReference").state.compliancequest__recordId,
            workItemId:component.get("v.pageReference").state.compliancequest__origin
        }).then($A.getCallback(function(data) {
            var dataArray = data.split("/");
            component.set('v.currentApprovalProcessStepName', dataArray[0]);
            component.set('v.origin',dataArray[1]);
        })).catch(function(err){
            helper.displayError(err, component);
        });
    },
    /**
    * This method is used to refresh the state of components
    */
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    /**
     * process the pending approval record and approves the doc if no error messages
     */
    approveRejectDoc: function(component,event, helper) {
	    var navService = component.find("navService");
            var esigComponent=component.find('esig');
            var successMsg = event.getSource().get("v.value") === 'true' ? $A.get("$Label.c.CQ_UI_Document_Approved_Toast_Msg") : $A.get("$Label.c.CQ_UI_Document_Rejected_Toast_Msg");
            esigComponent.validateEsig(esigComponent.get('v.esigPurpose')).then($A.getCallback(function(){
                helper.invokeAction(component.get('c.approveRejectDocument'), {
                    documentId : component.get('v.recordId'),
                    workItemId : component.get('v.origin'),
                    userName :  esigComponent.get('v.username'),
                    password :  esigComponent.get('v.password'),
                    approveRejectFlag : event.getSource().get("v.value"),
                    comment:esigComponent.get('v.comment')
                }).then(function(result) {
                    if(result.infoMessages) {
                        helper.showMessage(result.infoMessages.join('.'), 'info');
                    }
                    if(result.errorMessages) {
                        helper.displayError(result.errorMessages.join('.'),component);
                    }
                    if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                        helper.showMessage(successMsg, 'success');
                        navService.navigate(component.get('v.returnUrl'));
                    }
                })['catch']($A.getCallback(function(error) {
                    helper.displayError(error, component);
                }));
            })).catch($A.getCallback(function(err) {
                helper.displayError(err, component);
            }));

    },
    /**
     * leaves the pending approval record as it is
     */
    cancelApproval: function(component,event, helper) {
        var navService = component.find('navService');
        navService.navigate(component.get('v.returnUrl'));
    }
})