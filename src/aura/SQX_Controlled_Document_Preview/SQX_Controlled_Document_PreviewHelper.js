({
	getContentDetails : function(contentRetreiver, documentId) {
        contentRetreiver.setParams({
            docId : documentId
        });

        var promise = new Promise($A.getCallback(function(resolve, reject) {
             contentRetreiver.setCallback(this, function(response) {
                 if(response.getState() === 'SUCCESS') {
                     resolve(response.getReturnValue());
                 }
                 else {
                     reject(response.getError()[0]);
                 }
             });
        }));

        $A.enqueueAction(contentRetreiver);

        return promise;
	},
    regenerateSecondary : function(regeneration, documentId, moveLibrary) {
        regeneration.setParams({
            'documentId': documentId,
            'moveLibrary': moveLibrary
        });

        var promise = new Promise($A.getCallback(function(resolve, reject) {
             regeneration.setCallback(this, function(response) {
                 if(response.getState() === 'SUCCESS') {
                     resolve(response.getReturnValue());
                 }
                 else {
                     reject(response.getError()[0]);
                 }
             });
        }));

        $A.enqueueAction(regeneration);

        return promise;
    },
    /**
     * This method is used to upload the secondary content when secondary format is in manual
    */
    uploadSecondaryContent: function(addSecondaryContent,secondaryContentId,docId){
        addSecondaryContent.setParams({
            'secondaryContentId': secondaryContentId,
            'recordId':docId
            
        });
         var promise = new Promise($A.getCallback(function(resolve, reject) {
             addSecondaryContent.setCallback(this, function(response) {
                 if(response.getState() === 'SUCCESS') {
                      resolve({data: response.getReturnValue()})
                 }
                 else {
                    reject({ data: response.getError()[0].message});
                 }
             });
        }));

        $A.enqueueAction(addSecondaryContent);
        return promise;
    }
})