({
    onInit: function(component, event, helper) {
        var docId = component.get('v.recordId');
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        helper.getContentDetails(component.get('c.getPreviewContent'), docId).then(function(data) {
            component.set('v.previewResult', data);
            component.set('v.secondaryFormatSettings', data.secondaryFormatSettingOptions);
            if(component.get('v.previewResult.isPrimaryContentPrivate') == true) {
                component.set('v.showRegenerateOrMoveToDraftLibLabel', $A.get("$Label.c.CQ_UI_Controlled_Document_Move_To_Public_Draft"));
                component.set('v.moveToPublicDraftFlag', true);
            }
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        })['catch'](function (err) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": err.message || 'Unexpected error. Please try reloading',
                "type": "error",
                "mode": "sticky"
            });
            toastEvent.fire();
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        });
    },
    secondaryContentUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");

    },
    showSecondary : function(component, event, helper) {
        var secondaryContentId = component.get('v.simpleRecord.compliancequest__Secondary_Content_Reference__c');
        $A.get('e.lightning:openFiles').fire({
            recordIds: [secondaryContentId]
        });
    },
    navigateToCheckOut : function(component, event, helper) {
        var navigateToUrl = $A.get("e.force:navigateToURL");
        navigateToUrl.setParams({
            "url": "/apex/compliancequest__SQX_Controlled_Doc_Checkout?id=" + component.get('v.simpleRecord.Id') + "&mode=out"
        });

        navigateToUrl.fire();
    },
    /**
     * This method is used to regenerate secondary content.
     */
    regenerateOrMoveToPublicDraft : function(component, event, helper){
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        var successMsg = event.getSource().get("v.value") == true ? $A.get("$Label.compliancequest.SQX_MSG_MOVED_TO_PUBLIC_DRAFT") : $A.get("$Label.compliancequest.SQX_MSG_SECONDARY_CONTENT_GENERATED");
        helper.invokeAction(component.get('c.syncContentAndMoveToPublicLibrary'), {
            documentId : component.get('v.recordId'),
            moveLibrary : event.getSource().get("v.value")
        }).then($A.getCallback(function(result) {
            if(result.infoMessages) {
                helper.showMessage(result.infoMessages.join('.'), 'info', '', 'dismissible');
                $A.get('e.force:refreshView').fire();
            }
            if(result.errorMessages) {
                helper.displayError(result.errorMessages.join('.'),component);
            }
            if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                helper.showMessage(successMsg, 'success', '', 'dismissible');
                $A.get('e.force:refreshView').fire();
            }
        }))['catch']($A.getCallback(function(error) {
            helper.displayError(error, component);
        })).finally($A.getCallback(function() {
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        }));
        
    },
    menuSelected : function(component, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        switch(selectedMenuItemValue) {
            case 'secondaryMode':
                var navigateToUrl = $A.get("e.force:navigateToURL");
                navigateToUrl.setParams({
                    "url": "/apex/compliancequest__SQX_Controlled_Doc_Content?id=" + component.get('v.simpleRecord.Id') + "&mode=checkin"
                });

                navigateToUrl.fire();
                break;

            case 'regenerateSecondary':
                $A.util.removeClass(component.find('previewloading'), 'slds-hide');
                helper.regenerateSecondary(component.get('c.syncContentAndMoveToPublicLibrary'), component.get('v.recordId'), false).finally(function(){
                    $A.util.addClass(component.find('previewloading'), 'slds-hide');
                    $A.get('e.force:refreshView').fire();

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Content Generated Successfully",
                        "message": 'Successfully, generated content',
                        "type": "success"
                    });
                    toastEvent.fire();
                })['catch'](function (err) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": err.message || 'Unexpected error. Please try reloading',
                        "type": "error",
                        "mode": "sticky"
                    });
                    toastEvent.fire();
                    $A.util.addClass(component.find('previewloading'), 'slds-hide');
                });
                break;

            case 'checkout':
                var navigateToUrl = $A.get("e.force:navigateToURL");
                navigateToUrl.setParams({
                    "url": "/apex/compliancequest__SQX_Controlled_Doc_Checkout?mode=out&id=" + component.get('v.simpleRecord.Id')
                });

                navigateToUrl.fire();
                break;

            case 'checkin':
                var navigateToUrl = $A.get("e.force:navigateToURL");
                navigateToUrl.setParams({
                    "url": "/apex/compliancequest__SQX_Controlled_Doc_Content?mode=checkin&id=" + component.get('v.simpleRecord.Id')
                });

                navigateToUrl.fire();
                break;

            case 'undocheckout':
                var navigateToUrl = $A.get("e.force:navigateToURL");
                navigateToUrl.setParams({
                    "url": "/apex/compliancequest__SQX_Controlled_Doc_Checkout?mode=undo&id=" + component.get('v.simpleRecord.Id')
                });

                navigateToUrl.fire();
                break;
        }
    },
    uploadSecondaryContent : function(component, event, helper) {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        var secondaryContentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var docId = component.get('v.recordId');
        helper.uploadSecondaryContent(component.get('c.addControlledDocumentSecondaryContentReference'), secondaryContentId,docId).then(function(response){
                  
                    component.set('v.simpleRecord.compliancequest__Secondary_Content_Reference__c', response.data);
                    $A.get('e.force:refreshView').fire();

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "message":  $A.get("$Label.compliancequest.CQ_UI_Secondary_Content_Upload_Success_Message"),
                        "type": "success"
                    });
                    toastEvent.fire();
                }).catch(function(errorMsg){
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title":  $A.get("$Label.c.CQ_UI_Error"),
                                "message": errorMsg.data,
                                type:"error"
                            });
                            toastEvent.fire();
                            
                });
       
       
    },
    /**
     * This method is used to set secondary format after record creation
     */
    toggleSecondaryFormat:function(component, event, helper){
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        var btnId = event.getSource().getLocalId();
        var secondaryFormatValue = event.getParam("value");
        helper.invokeAction(component.get('c.updateSecondaryFormat'), {
            secondaryFormat : btnId == "secondaryFormat" ? secondaryFormatValue : '',
            recordId : component.get('v.recordId'),
            secondaryFormatSetting: btnId == "secondaryFormatSetting" ? secondaryFormatValue : ''
        }).then(function(result) {
            $A.get('e.force:refreshView').fire();
        })['catch']($A.getCallback(function(error) {
            helper.displayError(error, component);
        })).finally($A.getCallback(function() {
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        }));
    },

    /**
     * Handle to update scorm content value
     */
    handleToggleEvent: function(component, event, helper){
        $A.util.removeClass(component.find('previewloading'), 'slds-hide');
        var scormValue = event.getParam("scormContent");
        helper.invokeAction(component.get('c.updateScormValue'), {
            scormVal : scormValue,
            recordId : component.get('v.recordId')
        }).then(function(result) {
            $A.get('e.force:refreshView').fire();
        })['catch']($A.getCallback(function(error) {
            helper.displayError(error, component);
        })).finally($A.getCallback(function() {
            $A.util.addClass(component.find('previewloading'), 'slds-hide');
        }));
    }
})