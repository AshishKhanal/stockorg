({
    /**
     * Method to convert the errMsg to a suitable form
     */
    handleError: function (component, errMsg) {

        try {
            // try parsing the error up front
            // sometimes we get stringified error messages
            errMsg = JSON.parse(errMsg);
        } catch(e) {

        }

        if(errMsg.component) { // thrown from $A.getCallback

            // hacky code below
            // Backtracking Aura framework error wrapper code(thankfully open source), we can find that
            // they add a particular static msg - Error in $A.getCallback() before all error messages
            // in addition, if the error is a list of strings, they convert it to string(comma separated)
            //
            // the following code reverses that and extracts the actual list of errors
            var match = errMsg.message.match(/\[(.*)\]/);
            if(match && match.length > 1 ) {
                errMsg = match[1].split(",");
            } else{
                errMsg = errMsg.message;
            }
        } else {
            errMsg = errMsg.message || errMsg;
        }

        if (typeof (errMsg) === 'string') {
            errMsg = [{
                "message": errMsg
            }];
        } else if(errMsg.length) {
            errMsg = errMsg.map(function(msg) {
                if(msg.message) {
                   return msg;
                }
                return { "message" : msg };
            });
        }

        errMsg = errMsg.map(function(e) {  return e; });	// to avoid proxy objects

        this.invokeCallback(component, errMsg);
    },

    /**
     * Method to invoke callback function specified by the enclosing VF page
     */
    invokeCallback : function (component, errors) {
        var callbackFn = component.get('v.callback');
        if(callbackFn) {
            callbackFn(errors);
        }
    }
})