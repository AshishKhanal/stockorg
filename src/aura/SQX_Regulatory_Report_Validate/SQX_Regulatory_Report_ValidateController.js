({

    /**
     * Method to send request to validate current regulatory record
     */
    validateRecord : function(component, event, helper) {
        try {
            var recordId = component.get('v.recordId');

            var REMOTE_METHODS = {
                validateInSF: component.get('c.validateInSF'),
                validate: component.get('c.validate'),
                poll: component.get('c.poll')
            };

            helper.showLoading(component);

            helper
            .invokeAction(REMOTE_METHODS.validateInSF, { 'recordId' : recordId })
            .then($A.getCallback(function (validationResult) {

                if(validationResult.errors && validationResult.errors.length > 0) {
                    throw validationResult.errors;
                }

                if(validationResult.supportsXMLValidation) {
                    return helper
                    .invokeAction(REMOTE_METHODS.validate, { 'recordId' : recordId })
                    .then(function (jobId) {
                        return new Promise($A.getCallback(function (resolve, reject) { // poll job status
                            var interval = setInterval(
                                $A.getCallback(function () {
                                    helper
                                    .invokeAction(REMOTE_METHODS.poll, { 'jobId': jobId, 'recordId': recordId })
                                    .then(function (pollInfoString) {
                                        var pollInfo = JSON.parse(pollInfoString);
                                        if (pollInfo.status !== 'Pending') {
                                            clearInterval(interval);
                                            resolve(pollInfo);
                                        }
                                    })
                                    ['catch'](function (error) {
                                        clearInterval(interval);
                                        reject(error);
                                    });
                                }), 5000);
                        }));
                    })
                    .then(function (result) {
                        if (result.status == 'Error') {
                            throw result.response;
                        }
                    });
                }

                return Promise.resolve();
            }))
            .then($A.getCallback(function() {
                 helper.invokeCallback(component, null);
            }))
            ['catch']($A.getCallback(function (error) {
                helper.handleError(component, error);
                helper.hideLoading(component);
            }))['finally'](function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            helper.handleError(component, error);
            helper.hideLoading(component);
        }

    }
})