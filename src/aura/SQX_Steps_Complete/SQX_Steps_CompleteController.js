({
    doInit: function(component, event, helper) {
        helper.invokeAction(component.get('c.resolveObjectName'), {
            recordId : component.get("v.recordId") 
        }).then(function(response) {
           
            component.set("v.stepMap", response);
            if(component.get('v.stepMap.componentType')==='VF Page'){
                
                var pageReference =  component.get('v.stepMap.returnUrl');
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": pageReference
                });
                urlEvent.fire();
                
            }else{
                component.set("v.record.compliancequest__Status__c", 'Complete');
            }
            
        })['catch']($A.getCallback(function(error) {
            helper.displayError(error, component);
        }));
        
    },
    
    redirectToTaskRecord : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get("e.force:refreshView").fire();
    }
})