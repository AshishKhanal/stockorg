({
    /**
     * Handles the field changed event from lightning input and caches the changed value for subsequent use.
     */
    onFieldChanged: function(component, event, helper) {
        var value = event.getParam('value');
        component.set('v.fieldvalue', value);
        var cmpEvent = component.getEvent("LayoutFieldChanged");
        cmpEvent.setParams({
            "field": component.get('v.fieldname'),
            "fieldvalue": value
        });
        cmpEvent.fire();

        component.set('v.haserror', component.get('v.required') && (value === null || value === '' || (value.trim && value.trim() === '')));
    },
    /**
     * Handler the change of field "required" policy, so that the error message is removed if a field
     * is changed to not required
     */
    onRequiredChange: function(component, event, helper) {
        var value = component.get('v.fieldvalue'),
            hasError = component.get('v.haserror');
        if(hasError && !component.get('v.required') && (value === null || value === '' || (value.trim && value.trim() === ''))) {
            component.set('v.haserror', false);
        }
    }
})