({

    /**
     * Reads contents of the given file
     * @param file file to be read
     */
    readFile : function (file) {
        var reader = new FileReader();

        return new Promise ($A.getCallback(function (resolve, reject) {

            reader.onload = function () {
                resolve(reader.result);
            };

            reader.onerror = function () {
                reader.abort();
                reject(reader.error);
            };

            reader.readAsText(file);

        }));
    }
})