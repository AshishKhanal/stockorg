({

    /**
     * Gets file information(except the actual content) from upload
     * Gets fired when a file is uploaded
     */
    handleFileUpload : function (component, event, helper) {
        var file = event.getSource().get('v.files') && event.getSource().get('v.files')[0];
        component.set('v.file', file);
        component.set('v.fileName', file.name);
    },


    /**
     * Performs import operation using the data provided from the file
     */
    importBtnClicked : function (component, event, helper) {

        helper.showLoading(component);

        try {
            var file = component.get('v.file');

            helper
            .readFile(file)
            .then(function (jsonData){
                return helper
                .invokeAction(component.get('c.import'), { 'jsonData' : jsonData })
                .then(function (result) {
                    component.set('v.importResult', result);
                });
            })
            .catch(function (err) {
                if ($A.get('e.force:showToast')) {
                    helper.displayError(err);
                } else {
                    component.set('v.errorMsg', JSON.stringify(err));
                }
            })
            .finally(function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            // TODO : move error setting part to helper
            // for some reason, when moved to helper, extended helper class method(displayError) is not invoked
            if ($A.get('e.force:showToast')) {
                helper.displayError(error);
            } else {
                component.set('v.errorMsg', JSON.stringify(error));
            }

            helper.hideLoading(component);
        }

    }

})