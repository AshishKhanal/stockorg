({
    /**
     * Start the spinner and calls helper method that fetches items through callback.
     */
    getAllItems: function(component, event, helper) {
        var itemsComp = component.find('itemsList');
        var cmpSpinner = itemsComp.find('listSpinner');
        $A.util.removeClass(cmpSpinner, 'slds-hide');
        helper.getItems(component, event, helper);
    },

    /**
     * this method is triggered if any homeitem is selected and marks the checkbox as checked.
     */
    addToBulkActions: function(component, event, helper) {
        helper.checkItems(component);
    },

    /**
     * this selects all the bulk action supporting items
     */
    selectAllBulkActionItems: function(component, event, helper) {
        try {
            var evt = component.getEvent('onBulkActionAdd');
            var itemListCmp = component.find('itemsList');
            var bulkActCmp = component.find('homeHeader');
            var bulkSupportItems = itemListCmp.find('SQXBulkActionItem');
            if (bulkSupportItems && bulkSupportItems.length > 0) {
                for (var i = 0; i < bulkSupportItems.length; ++i) {
                    if (bulkActCmp.get('v.allSelected')) {
                        bulkSupportItems[i].set('v.value', true);
                    } else {
                        bulkSupportItems[i].set('v.value', false);
                    }
                }
                helper.selectAllBulkItems(component, evt, helper);
            }
        } catch (err) {
            console.error(err);
        }
    },

    /**
     * the method is called if individual bulk supporting item is selected.
     */
    selectBulkItems: function(component, event, helper) {
        helper.selectAllBulkItems(component, event, helper);
    },

    /**
     * the method shows the spinner when the items are being loaded
     */
    applyFilter: function(component, event, helper) {
        // hide/empty the bulk actions panel
        var bulkActCmp = component.find('homeHeader');
        bulkActCmp.set('v.bulkActions', []);

        //Apply the filter
        setTimeout(
            $A.getCallback(function() {
                var cmp = event.getSource();
                if (cmp.isValid()) {
                    var f = cmp.get("v.filter");
                    f.toggle();
                    cmp.set('v.filter', f);
                }
            }), 10);

        helper.toggleSpinner(component);
    },

    /**
     * this method refreshes the view
     */
    refreshView: function(component, event, helper) {
        var refreshView = $A.get('e.force:refreshView');
        if (refreshView) {
            refreshView.fire();
        } else {
            window.location.reload();
        }
    },

    /**
     * method to sort the homepage items based on sort order
     */
    sortItems: function (component, event, helper) {
        helper.sortItems(component, event, helper);
    },


    /**
     * handler method to handle more items request from user
     */
    showMoreItems: function (component, event, helper) {
        var itemsComp = event.getSource();
        if (itemsComp.isValid()) {

            var pageNumber = itemsComp.get("v.pageNumber");
            itemsComp.set("v.pageNumber", pageNumber + 1);

            helper.setVisibleItems(itemsComp);
        }
    }
})
