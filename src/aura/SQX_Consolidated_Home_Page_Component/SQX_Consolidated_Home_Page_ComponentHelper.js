({
    /**
     * this method adds the items selected to the bulk action list.
     */
    checkItems: function(component) {
        var itemListCmp = component.find('itemsList');
        var bulkActCmp = component.find('homeHeader');
        bulkActCmp.set('v.bulkActions', itemListCmp.get('v.selectedActionItem'));
    },

    /**
     * this method compiles the list of bulk action items and sets on the bulk action list
     * with the proper action items and urls.
     */
    selectAllBulkItems: function(component, event, helper) {
        try {
            var evt = component.getEvent('onBulkActionAdd');
            var selectedItems = [];
            var itemListCmp = component.find('itemsList');
            var bulkSupportItems = itemListCmp.find('SQXBulkActionItem') || [];
            for (var i = 0; i < bulkSupportItems.length; ++i) {
                if (bulkSupportItems[i].get('v.value') && bulkSupportItems[i].get('v.name')) {
                    selectedItems.push(bulkSupportItems[i].get('v.name'));
                }
            }
            var homeItems = itemListCmp.get('v.items');
            var bulkList = [];
            for (var i = 0; i < selectedItems.length; i++) {
                for (var j = 0; j < homeItems.length; j++) {
                    if (selectedItems[i] == homeItems[j].itemId) {
                        bulkList.push(homeItems[j]);
                    }
                }
            }

            var actionItemsList = [],
                actionItem,
                bulkActionItem,
                itemBulkUrl,
                actionItemsByName = {};

            for (var i = 0; i < bulkList.length; i++) {

                for (var act = 0; act < bulkList[i].actions.length; act++) {
                    if (bulkList[i].actions[act].supportsBulk) {
                        if (actionItemsByName[bulkList[i].actions[act].name]) {
                            actionItem = actionItemsByName[bulkList[i].actions[act].name];
                            if (actionItem.recordIds.indexOf(bulkList[i].itemId) === -1) {
                                if(!actionItem.actionIsLightning) {
                                    itemBulkUrl = actionItem.actionUrl;
                                    actionItem.actionUrl = itemBulkUrl.concat("," + bulkList[i].itemId);
                                }
                                actionItem.recordIds.push(bulkList[i].itemId);
                                actionItem.itemsCount++;
                            }
                        } else {
                            bulkActionItem = {
                                'actionItem': bulkList[i].itemId,
                                recordIds: [bulkList[i].itemId],
                                actionId: bulkList[i].actions[act].actionId,
                                actionName: bulkList[i].actions[act].name,
                                actionDisplayName: bulkList[i].actions[act].name,
                                actionUrl: bulkList[i].actions[act].actionUrl,
                                actionIsLightning: bulkList[i].actions[act].isLightningComponent,
                                itemsCount: 1
                            };
                            actionItemsList.push(bulkActionItem);
                            actionItemsByName[bulkActionItem.actionName] = bulkActionItem;
                        }
                    }

                }
            }
            itemListCmp.set('v.selectedActionItem', actionItemsList);
            evt.fire();
        } catch (err) {
            helper.handleError(err, component);
        }
    },

    /**
     * fetches the items from the serverside controller
     * and sets the filters and items list to respective component variables.
     */
    getItems: function(component, event, helper) {
        var itemsComp = component.find('itemsList');
        var headerComp = component.find('homeHeader');
        var modulesComp = component.find('modulesList');
        var filtersComp = component.find('filtersList');

        var action = component.get('c.getHomePageItems');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue(),
                    parsedResult = JSON.parse(result),
                    allAvailableItems = parsedResult.items,
                    iconsMap = parsedResult.iconsMap || {};

                var itemList = new cq.list(allAvailableItems, iconsMap);

                itemList.addListener(function() {
                    itemsComp.set("v.items", itemList.view);

                    // reset page number
                    itemsComp.set("v.pageNumber", 1);

                    helper.sortItems(component, event, helper);

                    helper.toggleSpinner(component);
                });

                filtersComp.set("v.filters", itemList.applicableFilters);
                modulesComp.set("v.filters", itemList.applicableFilters);
                itemsComp.set("v.items", itemList.view);

                // sort by due date [DEFAULT]
                headerComp.set("v.sortOrder", "due_date");
                helper.sortItems(component, event, helper);

                if (parsedResult.errors.length > 0) {
                    helper.handleError(new Error(parsedResult.errors.join()), component);
                }
            } else {
                helper.handleError(response.getError()[0], component);
            }
            helper.toggleSpinner(component);
        });

        $A.enqueueAction(action);
    },


    /**
     * Method to set visible items on the UI based on the page size
     */
    setVisibleItems: function (component) {

        var allItems = component.get("v.items");

        var pageSize = component.get("v.pageSize"),
            pageNumber = component.get("v.pageNumber"),
            visibleItems = component.get("v.visibleItems");

        visibleItems = allItems.slice(0, pageNumber * pageSize);

        component.set("v.visibleItems", []);    //clearing out visible items before rendering new items[ fix for SQX-4481 ]
        component.set("v.visibleItems", visibleItems);
    },

    /**
     * Toggles the visibility of the spinner
     */
    toggleSpinner: function(component, event) {
        var itemsComp = component.find('itemsList');
        var spinner = itemsComp.find('listSpinner');
        $A.util.toggleClass(spinner, 'slds-hide');
    },

    /**
     * sorts the items based on sort order
     */
    sortItems: function (component, event, helper) {
        var cmp = component.find('homeHeader');
        if (cmp.isValid()) {
            var sortOrder = cmp.get("{!v.sortOrder}");
            if (sortOrder == "due_date") {
                helper.dueDateSort(component, event, helper);
            } else {
                helper.createdDateSort(component, event, helper);
            }
        }
    },

    /**
     * sorts with most recent item first
     */
    createdDateSort: function(component, event, helper) {
        helper.toggleSpinner(component);
        var itemsComp = component.find('itemsList');
        setTimeout(
            $A.getCallback(function() {
                if (itemsComp.isValid()) {
                    var itemList = itemsComp.get('v.items');
                    itemsComp.set('v.items', itemList.sort(
                        function(e1, e2) {
                            return e1.itemAge > e2.itemAge ? 1 :
                                    e1.itemAge < e2.itemAge ? -1 :
                                    e1.moduleType > e2.moduleType ? 1 : -1;
                        }));
                    helper.setVisibleItems(itemsComp);
                    helper.toggleSpinner(component);
                }
            }), 10);
    },

    /**
     * sorts with highest overdue item first
     */
    dueDateSort: function(component, event, helper) {
        helper.toggleSpinner(component);
        var itemsComp = component.find('itemsList');
        setTimeout(
            $A.getCallback(function() {
                if (itemsComp.isValid()) {
                    var itemList = itemsComp.get('v.items');
                    itemsComp.set('v.items', itemList.sort(
                        /*
                            function sorts the items based on number of overdue days(desc order)
                            if the items do not have overdue days, they are sorted in the order of most aged item
                            if the items have been created on the same day, module name(alphabetical order) takes precedence(asc order)
                        */
                        function (e1, e2) {
                            var sortOrder = 0;
                            if (e1.dueDate) {
                                if (e2.dueDate) {
                                    sortOrder = e1.dueDate >= e2.dueDate ? 1 : -1;
                                } else {
                                    sortOrder = -1;
                                }
                            } else if (e2.dueDate) {
                                sortOrder = 1;
                            }
                            else {
                                // sort by most aged item
                                // if the age is same, sort by module name
                                sortOrder = e1.itemAge > e2.itemAge ? -1 :
                                            e1.itemAge < e2.itemAge ? 1 :
                                            e1.moduleType > e2.moduleType ? 1 : -1;	// sort by module name(asc)
                            }
                            return sortOrder;
                        }));
                    helper.setVisibleItems(itemsComp);
                    helper.toggleSpinner(component);
                }
            }), 10);
    },

    /*
     *   method to handle page errors
     *   displays errors as 'Toast' when run in lightning mode
     */
    handleError: function (error, component) {

        if (error) {
            //show error message in console
            if(error.message.search('is not accessible.') !== -1){
                if(window.console){
                    window.console.log(error.message);
                }
            }
            else{
                // Configure error toast
                var toastParams = {
                    mode: "sticky",
                    title: $A.get("$Label.compliancequest.CQ_UI_Error"),
                    message: error.message,
                    type: "error"
                };
        
                // Fire error toast
                var toastEvent = $A.get("e.force:showToast");
        
                if (toastEvent) {
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                } 
                else {
                    if (component) {
                        component.set('v.errorMsg', toastParams.message);
                    }
                }
            }
        }
    }

})