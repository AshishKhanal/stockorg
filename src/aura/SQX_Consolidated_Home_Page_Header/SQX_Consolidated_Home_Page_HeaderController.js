({
    /**
     * this method toggles the bulk select action
     */
    selectAllItems: function(component, event, helper) {
        var evt = component.getEvent('onAllSelect');
        if (component.get('v.allSelected')) {
            component.set('v.allSelected', false);
        } else {
            component.set('v.allSelected', true);
        }
        evt.fire();
    },

    /**
     * Server side refresh the whole view
     */
    refreshView: function(component, event, helper) {
        var evt = component.getEvent('onRefreshView');
        evt.fire();
    },

    /*
     * Toggles the bulk action container
     */
    toggleBulkPopup: function(component, event, helper) {
        var popup = component.find('bulkActionContainer');
        //$A.util.toggleClass(component,'slds-hide');
    },

    /*
     * Due Date sort
     */
    dueDateSort: function (component, event, helper) {
        component.set("v.sortOrder", "due_date");
        var evt = component.getEvent('onSortOrderChanged');
        evt.fire();
    },

    /*
     * Created Date sort
     */
    createdDateSort: function (component, event, helper) {
        component.set("v.sortOrder", "created_date");
        var evt = component.getEvent('onSortOrderChanged');
        evt.fire();
    },

    /**
    * This method is used to sign off bulk PDTs
    */
    bulkSignOff: function(component, event, helper){
        var actionId = event.target.getAttribute("data-action-id"),
            bulkActions = component.get('v.bulkActions'),
            idx,
            bulkAction,
            navService;
        for(idx = 0; idx < bulkActions.length; idx += 1) {
            if (bulkActions[idx].actionId === actionId) {
                bulkAction = bulkActions[idx];
                break;
            }
        }

        if(bulkAction.actionIsLightning) {
            // raise component event
            var pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: bulkAction.actionUrl
                },
                state: {
                    "compliancequest__recordId": bulkAction.recordIds,
                    "compliancequest__origin": actionId
                }
            };
            component.set("v.pageReference", pageReference);
            navService = component.find("navService");
            pageReference = component.get("v.pageReference");
            event.preventDefault();
            navService.navigate(pageReference);
        } else {
            console.error('Classic mode should use url instead of lightning component');
        }
    }
})