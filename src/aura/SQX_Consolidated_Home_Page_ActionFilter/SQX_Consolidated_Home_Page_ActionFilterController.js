({
    /**
     * Applies selected filter in the list of homepage items
     */
    clickedFilterItem: function(component, event, helper) {
        var selectedFilter = component.find('liActionFilter');
        var applyFilter = component.getEvent('onApplyFilter');
        applyFilter.fire();
        $A.util.toggleClass(selectedFilter, 'slds-is-selected slds-is-active');
    }
})
