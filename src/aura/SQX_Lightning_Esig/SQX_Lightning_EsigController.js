({

    /**
    * This method is used to get the esig seting and its sobject type  action purpose
    */
    doInit:function(component, event, helper) {
        component.set('v.isRelative', false);

        component.set('v.validate', function() {
            if(component.get('v.isValid') === true) {
                return {isValid: true};
            }

            var errMsg = component.get('v.errorMessage');

            return {
                isValid: false,
                errorMessage: errMsg
            };
        });

        helper.showLoading(component);
        helper.invokeAction(component.get('c.retriveEsigSetting'), {
            objId: typeof component.get('v.objType') == 'string' ? component.get('v.objType') : component.get('v.objType')[0], // Bulk sign-off case passing index 0 because we load esig-settings.
            action :component.get('v.purposeActions')
        }).then($A.getCallback(function(data) {
            component.set("v.esigSetting", data);
            component.set("v.esigPurpose",data.purposeOfSignature);
            if(!data.username){
                component.set("v.hideLoginInAs", true);
                component.set("v.loginInAs",data.loggedInAs);
            }
        })).catch(function(err){
            helper.displayError(err, component);
        }).finally(function() {
            helper.hideLoading(component);
        });


    },
    /**
    * This method is used to validate the esig value provider
    */
    validateEsigSetting: function(component, event, helper){
        var validationPromise = new Promise(function (resolve, reject) {
            if(helper.validateEsigParams(component, event, helper)) {
                helper.validateEsig(component, event, helper).then(function() {
                    resolve();
                }).catch(function() {
                    reject(component.get('v.errorMessage'));
                });
            } else {
                reject('');
            }
        }).finally($A.getCallback(function() {
            helper.hideLoading(component);
        }));

        return validationPromise;
    },
    /**
    * This method is used to catch finish event from flow and process the validation of attribute used in esig component
    */
    handleNavigationEvent: function(component,event,helper){
        console.log('at handle navigation event');
        var actionClicked = event.getParam('action');
        var okToNavigate = false;
        if (actionClicked=='NEXT'){
             okToNavigate = helper.validateEsigParams(component,event,helper) == true;

        } else if (actionClicked=='BACK'){
            var navigate = component.get('v.navigateFlow');
            navigate(actionClicked);
            
        } else if (actionClicked == 'CANCEL') {
             $A.get("e.force:closeQuickAction").fire();
        }
        if (okToNavigate){
            helper.validateEsig(component, event, helper).finally($A.getCallback(function() {
                var navigate = component.get('v.navigateFlow');
                navigate(actionClicked);
                helper.hideLoading(component);
                helper.showMessage($A.get("$Label.c.CQ_UI_Custom_Complete_Message").replace('{0}',component.get('v.esigPurpose')), 'success', '', 'pester');
            }));
        } else {
            //do nothing. error should be shown by dynamic component
        }

    }
})