({
    validateEsig: function(component, event, helper) {
        helper.showLoading(component);
        return helper.invokeAction(component.get('c.validateEsig'), {
            objId: component.get('v.objType'),
            action: component.get('v.purposeActions'),
            username: component.get("v.username"),
            password: component.get("v.password"),
            comments: component.get("v.comment")
        }).then(function(validationToken) {
            component.set('v.isValid', true);
            component.set('v.lastValidated', new Date());
            component.set('v.validationToken', validationToken);
        }).catch($A.getCallback(function(err) {
            var msg = '',
                idx;
            if(err.message) {
                msg = err.message
            } else {
                for (idx = 0; idx < err.length; idx++) {
                    msg += err[idx].message + "\n";
                }
            }
            console.error(err);
            component.set('v.errorMessage', '<span style="color: rgb(194, 57, 52);">' + msg +'</span>');
        }));
    },
    /**
    * This method is used to validate the esig value provider
    */
    validateEsigParams: function(component, event, helper){

        var params = event.getParam('arguments'),
            purposeField = component.find("purpose"),
            usernameField = component.find("username"),
            passwordField = component.find("password"),
            commentField = component.find("comment"),
            purpose     = component.get('v.esigSetting.purposeOfSignature'),
            username = component.get('v.username'),
            password    = component.get('v.password'),
            comment     = component.get('v.comment'),
            isValid     = true,
            requiredField = $A.get("$Label.c.CQ_UI_Field_Is_Required");
        // if  esig is enable than only it  validate the esig policies combination 
        if(component.get('v.esigSetting.showComponent') ){

            if($A.util.isEmpty(purpose) || $A.util.isUndefined(purpose)){
                
                purposeField.set("v.errors", [{message:requiredField.replace('{fieldName}', $A.get("$Label.c.CQ_UI_Purpose_Of_Sig"))}]);
                isValid=false;
            }else{
                purposeField.set("v.errors", []);
                isValid;
            }
            
            if(component.get('v.esigSetting.username')){
                if($A.util.isEmpty(username) || $A.util.isUndefined(username)){
                    usernameField.set("v.errors", [{message:requiredField.replace('{fieldName}', $A.get("$Label.c.CQ_UI_User_Name"))}]);
                    isValid=false;
                }else{
                    usernameField.set("v.errors", []);
                    isValid;
                }
            }
            
            if(component.get('v.esigSetting.enabled') && component.get('v.esigSetting.required')){
                if($A.util.isEmpty(password) || $A.util.isUndefined(password)){
                    passwordField.set("v.errors", [{message:requiredField.replace('{fieldName}', $A.get("$Label.c.CQ_UI_Password"))}]);
                    isValid=false;
                }else{
                    passwordField.set("v.errors", []);
                    isValid;
                }
            }
            if(component.get('v.esigSetting.comment')){
                if($A.util.isEmpty(comment) || $A.util.isUndefined(comment)){
                    commentField.set("v.errors", [{message:requiredField.replace('{fieldName}', $A.get("$Label.c.CQ_UI_Comment"))}]);
                    isValid=false;
                }else{
                    commentField.set("v.errors", []);
                    isValid;
                }
            }
        }
        return isValid;
    }
})