({
    /**
     * Loads the settings using apex to get the details regarding record type and other details
     */
    onInit : function(component, event, helper) {
        component.set('v.step', 'Initializing');
        component.set('v.isProcessing', true);

        //get default variable
        var urlDefault = helper.loadDefaultValuesFromUrl(),
            mainRecord = urlDefault || {},
            recordTypeId;


        var attributeDefault = component.get('v.record');
        if(attributeDefault) {
            for (var key in attributeDefault) {
                mainRecord[key] = attributeDefault[key];
                if(key === 'RecordTypeId') {
                    recordTypeId = attributeDefault[key]
                }
            }
        }
        
        component.set('v.mainRecord', mainRecord);


        helper.invokeAction(component.get('c.getDetails'), {'objectType' : component.get('v.objectType'), 'uiDefaults' : JSON.stringify(urlDefault), 'action': component.get('v.action'), recordId: component.get('v.recordId')})
        .then($A.getCallback(function(data) {
            var recordTypeComponent, idx,
                fields = {
                    DBRequiredFields : {},
                    DBReadFields : {}
                }, fieldProperties;

            if(data.Error) {
                throw new Error(data.Error);
            }

            if(!component.get('v.recordId')) {
                document.title = $A.get('$Label.compliancequest.CQ_UI_New_SObject').replace('{0}', data.ObjectLabel);
                component.set('v.title', document.title);
            } else {
                var pageTitle = component.get('v.title');
                if(pageTitle === ''){
                    component.set('v.title', $A.get('$Label.compliancequest.CQ_UI_Edit_SObject').replace('{0}', data.ObjectLabel));
                } else {
                    component.set('v.title', pageTitle);
                }
            }

            helper.resetEvalExpression();
            for(idx = 0; idx < data.EvalRules.length; idx++) {
                helper.addEvalExpression(JSON.parse(data.EvalRules[idx]));
            }

            for(var field in fields) {
                fieldProperties = {};
                for(idx = 0; data[field] && idx < data[field].length; idx++) {
                    fieldProperties[data[field][idx]] = true;
                }
                component.set('v.' + field.toLowerCase(), fieldProperties);
            }

            component.set('v.isProcessing', false);

            if(component.get('v.step') === 'Initializing') {
                recordTypeComponent = component.find('recordTypeSelector');
                recordTypeId = recordTypeId || recordTypeComponent.getRecordType();
                if(!recordTypeId && data.HasRecordTypes && !component.get('v.recordId')) {
                    component.set('v.step', 'Recordtype Selection');
                    recordTypeComponent.set('v.recordTypes', data.RecordTypes);
                } else {
                    helper.invokeAction(component.get('c.nextStep'));
                }

                component.set('v.layoutData', data);
            }
        }))['catch']($A.getCallback(function(error){
            helper.displayError(error, component);
        }));
    },
    /**
     * Moves the UI to next step after record type is selected
     */
    nextStep: function(component, event, helper) {
        var recordTypeComponent = component.find('recordTypeSelector'),
            recordId = component.get('v.recordId'),
            recordTypeId,
            recordTypes,
            recordType,
            layoutData,
            resourcePath,
            mainRecord = component.get('v.mainRecord') || {};

        layoutData = component.get('v.layoutData');
        recordTypeId = mainRecord.RecordTypeId;
        if(recordId) {
            recordTypeId = layoutData.RecordTypes[0].RecordTypeId; //incase of record id only one record type will be returned by server
        }
        if(!layoutData.HasRecordTypes || recordTypeId || recordTypeComponent.validate()) {
            recordTypes = layoutData.RecordTypes;

            var defRecordType = layoutData.HasRecordTypes ? recordTypeComponent.getRecordType() : 'Master',
                recordTypeDeveloperName = 'Master',
                recordTypeLabel = '';

            for(var idx = 0; idx < recordTypes.length; idx++) {
                recordType = recordTypes[idx];
                if((recordTypeId && recordTypeId === recordType.RecordTypeId) || (defRecordType && recordType.RecordTypeId && recordType.RecordTypeId.indexOf(defRecordType) === 0)){
                    recordTypeId = recordType.RecordTypeId;
                    recordTypeDeveloperName = recordType.DeveloperName;
                    recordTypeLabel = recordType.Name;
                    break;
                }
            }

            if(recordTypeDeveloperName !== 'Master') {
                var pageTitle = component.get('v.title');
                if(pageTitle == ''){
                    if(recordId) {
                        component.set('v.title', $A.get('$Label.compliancequest.CQ_UI_Edit_SObject').replace('{0}', layoutData.ObjectLabel + ' - ' + recordTypeLabel));
                    } else {
                        component.set('v.title', $A.get('$Label.compliancequest.CQ_UI_New_SObject').replace('{0}', layoutData.ObjectLabel + ' - ' + recordTypeLabel));
                    }
                } else {
                    component.set('v.title', pageTitle);
                }
            }
            component.set('v.isProcessing', true);
            component.set('v.step', 'Record Creation');
            component.set('v.recordTypeId', recordTypeId);
            var dj = JSON.parse(recordType.DefaultValueJSON);

            for(var k in dj) {
                if(dj[k].value) {
                    mainRecord[k] = dj[k].value;
                } else {
                    mainRecord[k] = dj[k];
                }
            }
            var overrides = component.get('v.override');
            if(overrides) {
                for (var key in overrides) {
                    mainRecord[key] = overrides[key];
                }
            }
            
            component.set('v.mainRecord', mainRecord);
            //Add static reference to both namespace and non-namespace to allow resolution at runtime
            resourcePath = '/layout/' + component.get('v.objectType') + '/' + component.get('v.action') + '/' + recordTypeDeveloperName + '.json'
            helper.loadJSONFromUrl($A.get('$Resource.' + layoutData.LayoutName) + resourcePath)
            .then($A.getCallback(function(layoutSections) {
                var language = $A.get('$Locale.langLocale'),
                    idx;
                // assign labels to section based on current user's language
                for(idx = 0; idx < layoutSections.length; idx++) {
                    if(layoutSections[idx].translations && layoutSections[idx].translations[language]) {
                        layoutSections[idx].label = layoutSections[idx].translations[language];
                    }
                }
                component.set('v.layoutSections', layoutSections);
                // reevaluate for each field in main record in layout section.
                for(var k in dj) {
                    helper.evaluateExpression(component, k, mainRecord[k], mainRecord, true);
                }
                component.set('v.layoutSections', layoutSections);
            }))['catch']($A.getCallback(function(errors) {
                helper.displayError(errors, component);
            }));

        }
    },
    /**
     * Action handler for cancel action, which usually takes back to last history
     */
    cancel: function(component, event) {
        var QUICK_ACTION = 'quickaction';
        if(component.get('v.displaymode') === QUICK_ACTION) {
            $A.get("e.force:closeQuickAction").fire();
        } else {
            history.back();
        }
    },
    /**
     * Event handler for executing save and new action. This will internally set save and new in progress attribute
     * and redirect accordingly
     */
    saveAndNew: function(component, event, helper) {
        component.set('v.saveandnewinprogress', true);
        helper.invokeAction(component.get('c.saveRecord'));
    },
    /**
     * Event handler for executing save only action
     */
    saveOnly: function(component, event, helper) {
        component.set('v.saveandnewinprogress', false);
        helper.invokeAction(component.get('c.saveRecord'));
    },
    /**
     * Validates the record and saves the record if it is okay
     */
    saveRecord: function(component, event, helper) {
        component.set('v.isProcessing', true);
        component.set('v.errorMsg', '');
        try {
            var isValid = true,
                idx,
                fidx,
                eidx,
                fieldIsValid,
                mainRecord = component.get('v.mainRecord'),
                layoutSections = component.get('v.layoutSections'),
                layoutSection,
                layoutColumn,
                layoutItem,
                value;

            for(idx = 0; idx < layoutSections.length; idx++) {
                layoutSection = layoutSections[idx];
                if(layoutSection.detailHeading || layoutSection.editHeading) {

                    for(fidx = 0; fidx < layoutSection.layoutColumns.length; fidx++) {
                        layoutColumn = layoutSection.layoutColumns[fidx];

                        for(eidx = 0; eidx < layoutColumn.layoutItems.length; eidx++) {

                            layoutItem = layoutColumn.layoutItems[eidx];

                            if(layoutItem.field && layoutItem.behavior === 'Required') {
                                value = mainRecord[layoutItem.field];
                                fieldIsValid = value != null && (typeof(value) !== 'string' ||  value.trim().length != 0);
                                if(!fieldIsValid) {
                                    console.log('Invalid field' + layoutItem.field);
                                    layoutItem.haserror = true;
                                }
                                isValid = isValid && fieldIsValid;
                            }
                        }
                    }
                }
            }

            if(isValid) {
                var strMainRecord = JSON.stringify(mainRecord);
                strMainRecord = strMainRecord.replace(/[.*[\]\\]/g, ''); // remove '[' and ']'
                helper.invokeAction(component.get('c.getCompleteTask'), { 'objDetails' : strMainRecord } ).then(function(result) {
                    $A.get('e.force:refreshView').fire();
                    $A.get('e.force:closeQuickAction').fire();
                })
                ['catch']($A.getCallback(function(errors){
                    helper.displayError(errors, component);
                })).finally($A.getCallback(function() {
                    component.set('v.isProcessing', false);
                }));
            } else {
                //set error on fields
                component.set('v.layoutSections', JSON.parse(JSON.stringify(layoutSections)));
                throw new Error($A.get('$Label.compliancequest.CQ_UI_ERROR_MESSAGE_Lightning'));
            }
        } catch(ex) {
            component.set('v.isProcessing', false);
            helper.displayError(ex.message, component);
        }
    },
    /**
     * Callback for edit form submission start
     */
    editFormOnSubmit: function(component, event) {
        component.set('v.isProcessing', true);
    },
    /**
     * Callback for edit form error
     */
    editFormErrorOccurred: function(component, event) {
        component.set('v.isProcessing', false);
    },
    /**
     * Callback for edit form when it is saved completely. It redirects to created/updated sobject
     */
    editFormSaved: function(component, event, helper) {
        if(component.get('v.saveandnewinprogress')) {
            helper.showMessage('Record created', 'success', '', 'pester');
            helper.invokeAction(component.get('c.onInit'));
        } else {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": event.getParam("response").id
            });
            navEvt.fire();
        }
    },
    /**
     * Callback when edit form is loaded completely
     */
    editFormLoaded: function(component, event) {
        component.set('v.isProcessing', false);
    },
    /**
     * Handles field change event for each layout field item and then evaluates the expression accordingly
     */
    fieldChanged: function(component, event, helper) {
        var field = event.getParam('field'),
            newValue = event.getParam('fieldvalue'),
            model = component.get('v.mainRecord');
        model[field] = newValue;
        helper.evaluateExpression(component, field, newValue, model);
    }
})