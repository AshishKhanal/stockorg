<aura:component controller="SQX_Lightning_Helper" extends="compliancequest:SQX_Lightning_Base_Component">
    <!-- Inbound attributes -->
    <aura:attribute name="objectType" type="String" description="The api name of the object that is to be saved (created)" required="true" />
    <aura:attribute name="action" type="String" description="The action for which the record is being created. The value drives the layout be used" required="true" />
    <aura:attribute name="title" type="String" description="Internal attribute to load the title" default="" />
    <aura:attribute name="record" type="Map" access="public" description="Used to pass record with default values if necessary using JS" />
    <aura:attribute name="override" type="Map" access="public" description="Used to pass record with override values for sobject" />
    <aura:attribute name="recordId" type="Id" access="public" description="Used to pass record Id" />
    <aura:attribute name="onclick" type="Aura.Action" access="public" default="{!c.editFormSaved}" description="Used to override action when saving record"/>
    <aura:attribute name="enableSaveAndNew" type="Boolean" access="public" description="Shows save and new button in the UI" default="false"/>
    <aura:attribute name="recordEditFormUpdate" type="Boolean" access="public" description="Used to update record edit form using record Id" />

    <!-- Private attributes -->
    <!-- Record type selection step and has record type selection -->
    <aura:attribute name="step" type="String" description="Internal attribute to track the step and show the ui accordingly" access="private"/>
    <aura:attribute name="hasRecordType" type="Boolean" description="Internal attribute to show record type selection, if it hasn't been selected" access="private" />
    <aura:attribute name="saveandnewinprogress" type="Boolean" description="Indicates whether save was initiated by clicking save and new or save" default="false" />

    <aura:attribute name="isProcessing" type="Boolean" description="Internal attribute to show/hide loading indicator" default="false" access="private" />
    <aura:attribute name="recordTypeId" type="String" description="Internal attribute to load the title" default="" access="private"/>

    <aura:attribute name="mainRecord" type="Map" access="private" />
    <aura:attribute name="layoutSections" type="Map" access="private"/>
    <aura:attribute name="layoutData" type="Map" access="private"/>
    <aura:attribute name="dbrequiredfields" type="Map" access="private" description="Fields that are required from database level"/>
    <aura:attribute name="dbreadfields" type="Map" access="private" description="Fields that are readonly fields from schema/database level"/>

    <aura:handler name="init" value="{!this}" action="{!c.onInit}" />
    <aura:handler name="LayoutFieldChanged" event="compliancequest:SQX_Layout_Event" action="{!c.fieldChanged}" />

    <lightning:card>
        <aura:set attribute="title">
            <div class="slds-media">
                <div class="slds-media__figure">
                    <aura:if isTrue="{!v.layoutData.IconUrl}">
	                    <img src="{!v.layoutData.IconUrl}" style="{!'background-color:#' + v.layoutData.BackgroundColor}" class="slds-icon slds-icon_container" />
                        <aura:set attribute="else">
                            <lightning:icon iconName="custom:custom9" />
                        </aura:set>
                    </aura:if>
                </div>
                <div class="slds-media__body">
                    <h1 class="slds-page-header__title slds-truncate slds-align-middle" title="{!v.title}">{!v.title}</h1>
                </div>

            </div>

        </aura:set>
        <lightning:spinner variant="brand" size="large" aura:id="loadingIndicator" class="{!v.isProcessing ? '' : 'slds-hide'}" />
        <aura:renderIf isTrue="{!v.errorMsg != ''}">
            <div class="slds-notify_container slds-is-relative">
                <div class="slds-notify slds-notify_toast slds-theme_error" role="alert">
                    <ui:outputText value="{!v.errorMsg}" />
                </div>
            </div>
        </aura:renderIf>

        <div class="cq-form">
            <div class="{! (v.step == 'Recordtype Selection' ? '' : 'slds-hide ') + 'slds-p-around_medium' }">
                <compliancequest:SQX_Record_Type_Selection aura:id="recordTypeSelector" />
            </div>
            <aura:if isTrue="{!v.step == 'Record Creation' &amp;&amp; v.layoutSections != null}">
                <lightning:recordEditForm objectApiName="{!v.objectType}"
                                          recordTypeId="{!v.recordTypeId}"
                                          recordId="{!v.recordEditFormUpdate == false ? null : v.recordId}"
                                          aura:id="formEdit"
                                          onload="{!c.editFormLoaded}"
                                          onerror="{!c.editFormErrorOccurred}"
                                          onsuccess="{!v.onclick}"
                                          onsubmit="{!c.editFormOnSubmit}">
                    <lightning:messages />
                    <aura:iteration items="{!v.layoutSections}" var="section">
                        <compliancequest:SQX_Layout_Section aura:id="layoutSection" layoutsection="{!section}" mainrecord="{!v.mainRecord}"
                                                    dbrequiredfields="{!v.dbrequiredfields}" hideSection="{!section.cqhidesection}"
                                                    dbreadfields="{!v.dbreadfields}"/>
                    </aura:iteration>
                </lightning:recordEditForm>
            </aura:if>
        </div>
        <aura:set attribute="actions">
            <aura:if isTrue="{!v.step != 'Initializing'}">
                <aura:if isTrue="{!v.step == 'Recordtype Selection'}">
                    <lightning:button label="{!$Label.compliancequest.CQ_UI_Next}" variant="brand" onclick="{!c.nextStep}"/>
                </aura:if>
                <aura:if isTrue="{!v.step == 'Record Creation'}">
                    <lightning:button label="{!$Label.compliancequest.CQ_UI_Button_Save}" variant="brand" onclick="{!c.saveOnly}"/>
                    <aura:if isTrue="{! v.recordId == null &amp;&amp; v.enableSaveAndNew == true}">
                        <lightning:button label="{!$Label.compliancequest.CQ_UI_Grid_Save_And_New}" variant="brand" onclick="{!c.saveAndNew}"/>
                    </aura:if>
                </aura:if>
                <lightning:button label="{!$Label.compliancequest.CQ_UI_Cancel}" onclick="{!c.cancel}"/>
            </aura:if>
        </aura:set>
    </lightning:card>
</aura:component>