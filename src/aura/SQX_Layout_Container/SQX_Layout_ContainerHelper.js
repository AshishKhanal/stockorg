({
    /**
     * Used for ensuring that multiple callbacks don't set multiple values and only last change sets the value
     */
    transferLock: {},
    /**
     * Stores the evaluation expression based on the object
     */
    evalExpression: {},
    /**
     * Resets the eval expression cache i.e. clears it. Used when initalizing the component.
     */
    resetEvalExpression: function() {
        this.evalExpression = {};
    },
    /**
     * Adds a given expression to eval expression list
     */
    addEvalExpression: function(evalExpressions) {
        var idx, expr, newExpr;
        for(idx = 0; idx < evalExpressions.length; idx++) {
            expr = evalExpressions[idx];
            newExpr = this.evalExpression[expr.field] || [];
            newExpr.push(expr);
            this.evalExpression[expr.field] = newExpr;
        }
    },
    /**
     * Evaluates all available expressions to check if the recent field change matches any criteria and applies them.
     * @param component {Object} the component that contains layout sections
     * @param field {Object} the field that was changed
     * @param value {Object} the new value of the field
     * @param model {Object} the main record the model
     * @param applyUiRulesOnly {Boolean} true if ui rules are to be applied too.
     */
    evaluateExpression: function(component, field, value, model, applyUiRulesOnly) {
        var idx = 0,
            expr,
            exprs,
            NOT_IS_NULL = 'notnull',
            IS_NULL = 'isnull',
            CHANGED = 'changed',
            EQUALS = 'eq',
            NOT_EQUALS = 'neq';

        if(this.evalExpression[field]) {
            exprs = this.evalExpression[field];
            for(; idx < exprs.length; idx++) {
                expr = exprs[idx];

                if(expr.operator === CHANGED || (expr.operator === EQUALS && expr.value == value)
                    || (expr.operator === NOT_EQUALS && expr.value != value) || (expr.operator === IS_NULL && value == null)
                   || (expr.operator === NOT_IS_NULL && value != null) ) {
                    this.applyRule(component, expr, true, value, model, applyUiRulesOnly);
                } else if ((expr.operator === EQUALS && expr.value != value)
                           || (expr.operator === NOT_EQUALS && expr.value == value) || (expr.operator === IS_NULL && value != null)
                          || (expr.operator === NOT_IS_NULL && value == null)) {
                    this.applyRule(component, expr, false, value, model, applyUiRulesOnly);
                }
            }
        }
    },
    /**
     * Internal method to apply the rules on successful/unsuccessful match. Successful match enables the feature
     * Unsuccessful disables it.
     */
    applyRule: function(component, expr, match, value, model, applyUiRulesOnly) {
        var idx,j, helper, allFields,
            layoutComponents = component.find('layoutSection');
        this.evaluateRequired(component, expr, match, value, model);
        this.evaluateReadonly(component, expr, match, value, model);
        this.evaluateHideSection(component, expr, match, value, model);
        if(applyUiRulesOnly !== true && value !== null) {
            this.evaluateTransfer(component, expr, match, value, model);
            this.evaluateInvoke(component, expr, match, value, model);
        }
    },
    /**
     * Set values in fields in the component based on the transfer fields or values returned by remote invocation
     */
    setValues: function(component, value, fieldsToSet) {
        if(value != null) {
            var mainRecord = component.get('v.mainRecord'),
                layoutSections = component.get('v.layoutSections'),
                fidx,
                fieldName;

            for(fidx = 0; fidx < fieldsToSet.length; fidx++) {
                fieldName = fieldsToSet[fidx];
                mainRecord[fieldName] = value[fieldName] || ' ';
                this.evaluateExpression(component, fieldName, mainRecord[fieldName], mainRecord);
            }

            component.set('v.mainRecord', JSON.parse(JSON.stringify(mainRecord)));
            component.set('v.layoutSections', JSON.parse(JSON.stringify(layoutSections)));
        }
    },
    /**
     * Evaluates the invoke expression i.e. flow invocation
     */
    evaluateInvoke: function(component, expr, match, value, model) {
        var helper = this;
        if(expr.invoke && match) {
            var modelWithAttr = JSON.parse(JSON.stringify(model)),
                helper = this;
            modelWithAttr.attributes = { type : component.get('v.objectType') };
            component.set('v.isProcessing', true);
            this.invokeAction(component.get('c.invokeFlow'), {
                namespace: expr.invoke.ns,
                flowName: expr.invoke.flow,
                obj: JSON.stringify(modelWithAttr)
            }).then($A.getCallback(function(data) {
                if(data.SObjValueJSON) {
                    var values = JSON.parse(data.SObjValueJSON),
                        impactedFields = data.ImpactedFields || [];

                    helper.setValues(component, values, impactedFields);

                    if(data.InfoMessage) {
                        helper.showMessage(data.InfoMessage, 'info');
                    }
                    if(data.ErrorMessage) {
                        helper.showMessage(data.ErrorMessage, 'error');
                    }
                    if(data.SuccessMessage) {
                        helper.showMessage(data.SuccessMessage, 'success');
                    }

                }
                component.set('v.isProcessing', false);
            }))['catch']($A.getCallback(function(e) {
                helper.displayError(e, component);
                component.set('v.isProcessing', false);
            }));
        }
    },
    /**
     * Evaluates the hide section expression and sets the appropriate value
     */
    evaluateHideSection: function(component, expr, match, value, model) {
        if(expr.hideSection) {
            var layoutSections = component.get('v.layoutSections');
            for(var idx = 0; idx < layoutSections.length; idx++) {
                layoutSections[idx].cqhidesection = (expr.hideSection.indexOf(layoutSections[idx].label) !== -1) && match ? true : false;
            }
            component.set('v.layoutSections', layoutSections);
        }
    },
    /**
     * Evaluates the readonly expression and sets the appropriate value on the field item
     */
    evaluateReadonly: function(component, expr, match, value, model) {
        if(expr.setReadonly) {
            this.callChildMethod(component, 'setReadonlys', [expr.setReadonly, match]);
        }
    },
    /**
     * Evaluates the required expression and sets the appropriate value on the field item
     */
    evaluateRequired: function(component, expr, match, value, model) {
        if(expr.setRequired) {
            this.callChildMethod(component, 'setRequireds', [expr.setRequired, match]);
        }
    },
    /**
     * Evaluates the transfer expression and sets the appropriate value based on return value
     */
    evaluateTransfer: function(component, expr, match, value, model) {
        var allFields, helper;
        if(expr.transfer) {
            //TODO: switch from object.keys because it isn't supported in all browsers
            allFields = Object.keys(expr.transfer);

            this.transferLock[expr.field] = value;
            helper = this;
            component.set('v.isProcessing', true);
            this.getObject(component, value, allFields).then($A.getCallback(function(data){
                var values = {},
                    targetFields,
                    idx,
                    impactedFields = [];

                // if multiple request were triggered ensures only last request's response is used
                if(helper.transferLock[expr.field] === value) {
                    delete helper.transferLock[expr.field];

                    //invoke set field
                    for(var sourceField in expr.transfer) {
                        targetFields = expr.transfer[sourceField];
                        for(idx = 0; idx < targetFields.length; idx++) {
                            values[targetFields[idx]] = data[sourceField];
                            impactedFields.push(targetFields[idx]);
                        }
                    }

                    helper.setValues(component, values, impactedFields);
                }
                component.set('v.isProcessing', false);
            }))['catch']($A.getCallback(function() {
                component.set('v.isProcessing', false);
            }));;
        }
    },
    /**
     * Calls child methods in each section. Used in setting readonly and required values
     */
    callChildMethod: function(component, methodName, params) {
        var layoutSections = component.find('layoutSection'),
            idx;
        for(idx = 0; idx < layoutSections.length; idx++) {
            layoutSections[idx][methodName].apply(layoutSections[idx], params);
        }
    },
    /**
     * Helper method for invoking the get object action to fetch fields of an object with a particular id.
     */
    getObject: function(component, objectId, fields) {
        return this.invokeAction(component.get('c.getObjectWithId'), {
            recordId: objectId,
            'fields': fields
        });
    }
})