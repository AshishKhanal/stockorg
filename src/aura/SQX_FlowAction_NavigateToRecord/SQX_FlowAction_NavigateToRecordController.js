({
    /**
     * function to navigate to record
     */
    invoke : function(component, event, helper) {
        var mode = component.get('v.mode');
        var targetObj = component.get("v.sObjId");
        var navPreference = component.get("v.navigationPreference");

        // Finish the flow which automatically refreshes the current view.
        if(navPreference == 'refresh'){
           var navigate = component.get("v.navigateFlow");
           navigate("FINISH");
        }else{
            // redirect
            if(mode == 'edit') {
                helper.navigateUsingService(component.find('navService'),{
                    type: 'standard__recordPage',
                    attributes: {
                        recordId: targetObj,
                        actionName: 'edit'
                    }
                });
                
            } else {
                
                var navEvt = $A.get("e.force:navigateToSObject");
                if(navEvt){

                    navEvt.setParams({
                        "recordId": targetObj,
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }

                // refresh the view as above navigation does not refresh view.
                
                var refreshEvt = $A.get("e.force:refreshView");
                if(refreshEvt){
                    refreshEvt.fire();
                }
            }
        }
    }
})