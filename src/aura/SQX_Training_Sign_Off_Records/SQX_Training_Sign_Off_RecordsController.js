({
    /**
    * this method is used to display list of personnel document trainings
    */
    onInit : function(component, event, helper) {

        component.set('v.isLoading', true);
        var myPageRef = component.get('v.pageReference');
        var ids = JSON.parse(JSON.stringify(myPageRef.state.compliancequest__recordId));
        var origin = myPageRef.state.compliancequest__origin;
        ids = ids.toString().includes(',') ? ids.split(',') : ids;
        var recordIds =[];
        if (typeof ids === 'string' || ids instanceof String){
            recordIds.push(ids);
        }else{
            for(var id in ids){
                recordIds.push(ids[id]);
            }
        }
        component.set('v.recordId',recordIds);

        if(origin === 'usersignoff') {
            component.set('v.layoutMode', 'full');
            component.set('v.viewFields', ['compliancequest__SQX_Controlled_Document__c','compliancequest__Level_of_Competency__c', 'compliancequest__Trainer_Approval_Needed__c','compliancequest__Due_Date__c','compliancequest__Status__c']);
            component.set('v.editFields', ['compliancequest__SQX_Trainer__c']);
            component.set('v.title', $A.get('$Label.c.cq_ui_doc_training_user_sign_off'));
            component.set('v.sectionTitle', $A.get('$Label.c.CQ_UI_Doc_Training_Require_User_Sign_Off'));
        } else {
            component.set('v.layoutMode', 'compact');
            component.set('v.viewFields', ['compliancequest__SQX_Controlled_Document__c','compliancequest__Level_of_Competency__c', 'compliancequest__Trainer_Approval_Needed__c','compliancequest__Due_Date__c','compliancequest__Status__c','compliancequest__SQX_Personnel__c', 'compliancequest__Personnel_Name__c', 'compliancequest__SQX_User_Signed_Off_By__c','compliancequest__User_SignOff_Date__c']);
            component.set('v.editFields', []);
            component.set('v.title', $A.get('$Label.c.cq_ui_doc_training_trainer_sign_off'));
            component.set('v.sectionTitle', $A.get('$Label.c.cq_ui_doc_training_requiring_trainer_sign_off'));
        }

        helper.invokeAction(component.get('c.getPersonnelDocumentTrainings'), { 'recordIds' : recordIds, 'purpose': origin  } ).then(
            $A.getCallback(function(data) {
                data.trainingsToSignOff.forEach(function(t){
                    t.model = { };
                });
                component.set('v.personnelDocumentTrainings', data.trainingsToSignOff);
                component.set('v.trainingsThatCantBeSignedOff', data.trainingsThatCantBeSignedOff);
                component.set('v.trainingsNeedingAssessment', data.trainingsNeedingAssessment);
            })
        )['catch']($A.getCallback(function(errors){
            helper.displayError(errors, component);
        })).finally($A.getCallback(function() {
            component.set('v.isLoading', false);
        }));
    },

    /**
    * this method is used to refresh the state of components
    */
    reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    
    /**
    * This method is used to sign off personnel document trainings
    */
    signOff: function(component, event, helper){

        //get child esig component
        var esigComponent=component.find('esig');
        esigComponent.validateEsig(esigComponent.get('v.esigPurpose')).then($A.getCallback(function() {
            var pageRef = component.get('v.pageReference'),
                recordIds = [],
                idx,
                training,
                trainings = component.get('v.personnelDocumentTrainings'),
                modifiedTrainings = [],
                modifiedTraining;
            
            component.set('v.isLoading', true);
            for(idx = 0; idx < trainings.length; idx++) {
                training = trainings[idx];
                recordIds.push(training.Id);
                if(Object.keys(training.model).length > 0) {
                    modifiedTraining = JSON.parse(JSON.stringify(training.model));
                    modifiedTraining.Id = training.Id;
                    modifiedTrainings.push(modifiedTraining);
                }
            }
            
            helper.invokeAction(component.get('c.performSignOff'), {
                'recordIds': recordIds,
                purpose: pageRef.state.compliancequest__origin,
                username :  esigComponent.get('v.username')!=undefined?esigComponent.get('v.username'):'',
                password :  esigComponent.get('v.password')!=undefined?esigComponent.get('v.password'):'',
                trainingsWithModifications: modifiedTrainings
            }).then($A.getCallback(function(result){

                if(result.infoMessages) {
                    helper.showMessage(result.infoMessages.join('.'), 'info', '', 'dismissible');
                }
                if(result.errorMessages) {
                    helper.displayError(result.errorMessages.join('.'),component);
                }
                if(result.infoMessages && result.infoMessages.length == 0 && result.errorMessages && result.errorMessages.length == 0 ) {
                    helper.showMessage( $A.get('$Label.c.CQ_UI_Trainings_signed_off_Success_Message'),'success', '', 'dismissible');
                    $A.enqueueAction(component.get('c.cancel'));
                }
                
            }))['catch']($A.getCallback(function(errors) {
                helper.displayError(errors, component)
            }))['finally']($A.getCallback(function(){
                component.set('v.isLoading', false);
            }));
        })).catch($A.getCallback(function(err) {
            helper.displayError(err, component);
        }))
    },
    /**
    * this method is used to redirect previous page
    */
    cancel: function(component, event, helper){
        var navService = component.find('navService');
        var pageReference = {
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            }
        };
        navService.navigate(pageReference);
    }
})