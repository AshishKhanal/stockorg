({    
    /**
     * Starts the search when enter key is pressed on the input text
     */
    textEntered: function(component, event, helper) {
        if(event.getParams().keyCode == 13){
            helper.fireEventForSearch(component, event, helper);
        }
    },
    /**
     * this method fire when user search on document details
     */ 
    fireTextSearchEvent : function(component, event, helper) {
        helper.fireEventForSearch(component, event, helper);
    },
    /**
     * Raise the event to toggle the sidebar. 
     */
    toggleSideBar:function(component,event,helper){
        var toggleEvt = component.getEvent('toggleSidebarEvent');
        component.set('v.expanded', !component.get('v.expanded') );
        toggleEvt.fire();
    },
    onSelectChange:function(component,event,helper){
        var selected = component.find("ddlSearch").get("v.value");
        var toggleEvt = component.getEvent('toggleSidebarEvent');
        
        if(selected=="AllDocs"){
            var location=[];
            location.push($A.get("$Label.compliancequest.CQ_UI_All_Documents"));
            component.set('v.currentLocation',location)
        }else{
            var currentLoc=component.get('v.tempCurrentLocation');
            component.set('v.currentLocation',currentLoc.length==0?"All Docs":currentLoc);
        }
        var isExpand=selected=="AllDocs"?true:false;
        component.set('v.expanded', isExpand );
        helper.fireEventForSearch(component, event, helper);
        toggleEvt.fire();
    },
    /**
    * this method hides the modal dialog that is being shown in the UI
    */
    hideModal:function(component,event,helper){
        var divModal = component.find('divModel');
        var modalBackDrop = component.find('modalbackdrop');
        $A.util.removeClass(divModal, 'slds-fade-in-open slds-modal-backdrop');
        $A.util.removeClass(modalBackDrop, 'slds-backdrop_open');
    },
    
    
    /**
    *  method fires an event to the main component to fetch more records for the current folder
    */
    getMoreRecords:function(component, event, helper){

        var moreRecordsEvt = component.getEvent("moreRecordsEvent");

        moreRecordsEvt.fire();
    }
})