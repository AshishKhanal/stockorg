({
    /**
    *this method fire on search click event
    */
    fireEventForSearch : function(component, event, helper) {
        var cmpEvent = component.getEvent("searchTextEvent");
        var searchSeparator = component.find("ddlSearch").get("v.value");
        var textboxCmp=component.find('txtSearchText');
        var searchText = textboxCmp.get('v.value');
        if(searchText != null && searchText.length > 2){
            cmpEvent.setParams({
                "docSearchTextValue" : textboxCmp.get('v.value'),
                "searchSeparatorValue":searchSeparator
            });
            cmpEvent.fire();
        }
    }
})