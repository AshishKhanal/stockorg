({
    cancelAction: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    performAction: function(component, event, helper) {
        component.set('v.displaymode', 'quickaction');
        var esigComponent = component.find('esigComponent');
        esigComponent.validateEsig({ purposeOfAction: component.find('v.purposeActions') }).then(function() {
            helper.showLoading(component);
            helper.invokeAction(component.get('c.performActionOnObject'),  {
                objId: component.get('v.recordId'),
                action: component.get('v.purposeActions'),
                username: esigComponent.get('v.username'),
                password: esigComponent.get('v.password'),
                comments: esigComponent.get('v.comment'),
                token: esigComponent.get('v.validationToken')
            }).then(function() {
                $A.get("e.force:closeQuickAction").fire();
                $A.get("e.force:refreshView").fire();
                component.set('v.displaymode', '');
                helper.showMessage($A.get("$Label.c.CQ_UI_Custom_Complete_Message").replace('{0}',component.get('v.esigPurpose')), 'success', '', 'pester');
            }).catch($A.getCallback(function(err){
                helper.displayError(err, component);
            })).finally($A.getCallback(function() {
                helper.hideLoading(component);
            }));
        }).catch($A.getCallback(function(err) {
            helper.displayError(err, component);
        }));
    }
})