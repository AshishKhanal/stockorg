({
    /**
    * Handle to change scorm content value
    */
    onCheck: function(component, event, helper) {
        var scormVal = !event.getSource().get('v.value');
        var componentEvent = component.getEvent("scormToggle");
        componentEvent.setParams({"scormContent": scormVal});
        componentEvent.fire();
    }
})