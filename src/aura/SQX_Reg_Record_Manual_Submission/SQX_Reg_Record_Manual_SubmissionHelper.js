({
    /**
     * Method to render error as datatable in UI
     */
    displayError: function (component, errMsg) {
       
        errMsg = errMsg.message || errMsg;

        if (typeof (errMsg) === 'string') {
            errMsg = [{
                "message": errMsg
            }];
        }

        errMsg = errMsg.map(function(e) { return e; });	// to avoid proxy objects

        component.set('v.errorJson', errMsg);
    }
})