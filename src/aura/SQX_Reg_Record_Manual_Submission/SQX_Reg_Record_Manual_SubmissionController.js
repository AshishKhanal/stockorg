({
    
    /**
     * This method invokes submission creation method
     */
    createSubmission : function(component, event, helper) {
    
        component.set('v.columns', [{
            label: $A.get('$Label.c.CQ_UI_Error'),
            fieldName: 'message',
            type: 'text'
        }]);
        
        var recordId = component.get('v.recordId');
        
        var remoteMethodToInvoke = component.get('c.createSubmissionRecord');
        
        helper.showLoading(component);
        
        try {
        
            helper
            .invokeAction(remoteMethodToInvoke, { 'recordId' : recordId })
            .then($A.getCallback(function(subRecordId) {
                component.set('v.newSubmissionRecordId', subRecordId);
            }))
            ['catch']($A.getCallback(function (error) {
                helper.displayError(component, error);
                helper.hideLoading(component);
            }))['finally'](function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            helper.displayError(component, error);
            helper.hideLoading(component);
        }
        
    },
    
    /**
     * Method is invoked when file upload is complete
     * Navigates user to new submission record
     */
    handleUploadFinished: function (component, event, helper) {
        try {

            var documentId = event.getParam('files')[0].documentId;

            var remoteMethodToInvoke = component.get('c.setRecordTypeForRegulatoryReport');

            helper
                .invokeAction(remoteMethodToInvoke, { 'contentDocumentId': documentId })	// set record type to the newly uploaded content
                .then($A.getCallback(function (subRecordId) {
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": component.get('v.newSubmissionRecordId'),
                        "slideDevName": "related"
                    });
                    navEvt.fire();
                }))
            ['catch']($A.getCallback(function (error) {
                helper.displayError(component, error);
                helper.hideLoading(component);
            }))['finally'](function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            helper.displayError(component, error);
            helper.hideLoading(component);
        }
    }
})