({
    /**
     *  this method get user permissions
     */
    onInit: function(component, event, helper){
        helper.invokeAction(component.get('c.getPermissions'), {'docId': component.get('v.recordId') }).then(function(data) {
            component.set('v.isUserCheckOutUser', data.isUserCheckOutUser);
            component.set('v.isSupervisorUser', data.isSupervisorUser);
            component.set('v.hasEditAccess', data.hasEditAccess);
        })['catch'](function (err) {
            helper.displayError(err, component);
        });
    },
    
    /**
     * this method used to open checkout mode
     */
    openCheckoutMode : function(component, event, helper) {
        var url ="/apex/compliancequest__SQX_Controlled_Doc_Checkout?id=" + component.get('v.recordId')+"&mode=out";
        helper.navigateToUrl(url);
    },

    /**
     * this method used to open checkin mode
     */
    openCheckinMode: function(component, event, helper){
        var url ="/apex/compliancequest__SQX_Controlled_Doc_Content?id=" + component.get('v.recordId')+"&mode=in";
        helper.navigateToUrl(url);
    },
    
    /**
     * this method used to open undo-checkout mode
     */
    openUndoCheckoutMode : function(component, event, helper) {
        var url ="/apex/compliancequest__SQX_Controlled_Doc_Checkout?id=" + component.get('v.recordId')+"&mode=undo";
        helper.navigateToUrl(url);
    },
    
    /**
     * this method used to collaboration document
     */
    collaboration: function(component, event, helper){
        var url ="/apex/compliancequest__SQX_Controlled_Doc_Collaboration?id=" + component.get('v.recordId');
        helper.navigateToUrl(url);
    },
    
    /**
     * this method used to view collaboration document
     */
    viewCollaboration: function(component, event, helper){
        var url ="/"+component.get('v.collaborationGroupId');
        helper.navigateToUrl(url);
    }
})