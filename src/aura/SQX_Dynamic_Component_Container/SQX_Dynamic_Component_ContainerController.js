({
	onInit : function(component, event, helper) {
        helper.invokeAction(component.get('c.getActionsFor'), {
            recordType: component.get('v.objectType'),
            purpose: component.get('v.purpose')
        }).then($A.getCallback(function(data) {
            component.set('v.components', data);
        }))['catch'](function(errors){
            helper.displayError(errors, component);
        });
	}
})