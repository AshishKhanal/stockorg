({
    /**
     * Generic method to fetch personnel related items from apex controller
     * @param contentRetriever controller method to be called
     * @param psnId id of the personnel
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    fetch: function (contentRetreiver) {
        var promise = new Promise($A.getCallback(function (resolve, reject) {
            contentRetreiver.setCallback(this, function (response) {
                if (response.getState() === 'SUCCESS') {
                    var result = response.getReturnValue(),
                        parsedResult = JSON.parse(result),
                        allAvailableItems = parsedResult.items;
                    if (parsedResult.errors.length == 0) {
                        resolve(allAvailableItems);
                    } else {
                        reject(new Error(parsedResult.errors.join()));
                    }
                } else {
                    reject(response.getError()[0]);
                }
            });
        }));

        $A.enqueueAction(contentRetreiver);

        return promise;
    },

    /**
     * Sets the actual current records to component and re-render the view
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    renderPage: function(component) {
        var records = component.get("v.allFeedItems"),
            pageNumber = component.get("v.pageNumber"),
            pageSize = component.get('v.pageSize'),
            pageRecords = records.slice((pageNumber - 1) * pageSize, pageNumber * pageSize);
        component.set("v.feedItems", pageRecords);
    }
})