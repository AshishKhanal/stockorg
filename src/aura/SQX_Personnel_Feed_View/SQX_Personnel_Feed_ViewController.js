({
    /**
     * Initializer method that retrieves pending DTs from SF and binds with view
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    onInit: function (component, event, helper) {

        var pageSize = component.get('v.pageSize');

        $A.util.removeClass(component.find('loadingIndicator'), 'slds-hide');

        helper.fetch(component.get('c.getPendingItems'))
            .then(function (data) {
                // bind the whole retrieves records to view and also set the maximum page that can be formed in the pagination bar
                component.set("v.allFeedItems", data);
                component.set("v.maxPage", Math.floor((data.length + (pageSize - 1)) / pageSize));
                helper.renderPage(component);
            }).catch(function (err) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": $A.get("$Label.c.CQ_UI_Error"),
                    "message": err.message,
                    "type": "error",
                    "mode": "sticky"
                });
                toastEvent.fire();
            })
            .finally(function () {
                $A.util.addClass(component.find('loadingIndicator'), 'slds-hide');
            });
    },

    /**
     * This method initializes the visibility of training records section. If the current use is personnel user then records section will be visible else not.
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    setPendingRecordSectionVisibility : function(component, event, helper) {
        // get current user id
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        // get personnel user id
        var personnelUserId = component.get('v.personnelRecord.compliancequest__SQX_User__c');
        
        /* 
         * Note : personnelUserId returned from record data is 18 character whereas currentUserId returned from sobjecttype is 15 characters.
         * check if current user is personnel user
         */
        if (personnelUserId && personnelUserId.startsWith(currentUserId)) {
            component.set('v.showSection',true);
        }
    },
    
    /**
     * Controller method which call helper method to set the actual current records to component and re-render the view
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    renderPage: function(component, event, helper) {
        helper.renderPage(component);
    },
    
    /**
     * This method is used to sign off if isLightningComponent is true
     * @author : Manish Joshi
     * @story  : [SQX-5316],[SQX-7241]
    */
    personnelTraningSignOff: function(component, event, helper){
        var target = event.target,  
            actionId = target.getAttribute("data-action-id"),
            actionUrl = target.getAttribute("data-action-url"),
            recordId = target.getAttribute("data-action-record-id"),
            navService;

        // raise component event
        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: actionUrl
            },
            state: {
                "compliancequest__recordId": recordId,
                "compliancequest__origin": actionId
            }
        };
        component.set("v.pageReference", pageReference);
        navService = component.find("navService");
        pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    }
    
})