({
    /**
     *  Function to submit PDF generation request
     *  The method also waits for the request to be processed and finally opens up the PDF
     */
    createPDF : function(component, event, helper) {
        var recordId = component.get('v.recordId');

        component.set('v.columns', [{
            label: $A.get('$Label.c.CQ_UI_Error'),
            fieldName: 'message',
            type: 'text'
        }]);

        var REMOTE_METHODS = {
            submitRequest : component.get('c.submitPDFGenerationRequest'),
            poll : component.get('c.poll')
        };

        helper.showLoading(component);

        try {

            helper
            .invokeAction(REMOTE_METHODS.submitRequest, { 'recordId' : recordId })
            .then($A.getCallback(function(jobId) {
                return new Promise($A.getCallback(function (resolve, reject) { // poll job status
                    var interval = setInterval(
                        $A.getCallback(function () {
                            helper
                            .invokeAction(REMOTE_METHODS.poll, { 'jobId': jobId, 'recordId' : recordId })
                            .then(function (pollInfoString) {
                                var pollInfo = JSON.parse(pollInfoString);
                                if (pollInfo.status !== 'Pending') {
                                    clearInterval(interval);
                                    resolve(pollInfo);
                                }
                            })
                            ['catch'](function (error) {
                                clearInterval(interval);
                                reject(error);
                            });

                        }), 5000);

                }));
            }))
            .then(function (result) {

                if (result.status == 'Error') {
                    throw new Error(result.response);
                }

                helper.navigateToUrl(result.response);

                $A.get("e.force:closeQuickAction").fire();	// close PopUp
            })
            ['catch']($A.getCallback(function (error) {
                helper.displayError(component, error);
                helper.hideLoading(component);
            }))['finally'](function () {
                helper.hideLoading(component);
            });

        } catch (error) {
            helper.displayError(component, error);
            helper.hideLoading(component);
        }
    }
})