({
    /**
     * this method will fire when binder ddl onchange
     */ 
    onChangeType : function(component, event, helper) {
        var divMakeDefault=component.find('divmakedefault');
        $A.util.addClass(divMakeDefault,'slds-hide');
        var typeSelect = component.find("binderList");
        var binderId = typeSelect.get("v.value");
        var optionList=component.get('v.options');
        var binderName="";
        for(var value in optionList){
            if(optionList[value].Id==binderId){
                binderName=optionList[value].Name;
            }
        }
        var cmpEvent = component.getEvent("folderChangeEvent");
        
        cmpEvent.setParams({
            "selectedBinderId" :binderId,
            "headerTitle":binderName
        });
        cmpEvent.fire();
    },
    
    /**
     * recursive function to append treeview hierarchy
     */ 
    getChildren : function(cmp, event) {
        var body = cmp.get("v.body") || [];
        var treeNodes = [];
        for (var n = 0; n < body.length; n++) {
            var child = body[n];
            
            if (child.isInstanceOf("compliancequest:treeNode")) {
                treeNodes.push(child);
            }
        }
        
        // Grab the model nodes from the tree.
        var modelNodes = cmp.find("modelNode");
        if ($A.util.isUndefinedOrNull(modelNodes)) {
            modelNodes = [];
        } else if (!$A.util.isArray(modelNodes)) {
            modelNodes = [modelNodes];
        }
        
        // Return them to the caller
        event.getParam("callback")(modelNodes.concat(treeNodes));
    },
    
    /**
     * this method will fire when select treeview node item .
     */ 
    fireEvent : function(cmp,event) {
        var cmpEvent = cmp.getEvent("folderChangeEvent");
        var itemObj=cmp.get('v.binder');
        //set parameter document header title, nodeItem object
        //'event.target.id' in this attribute set node item name.
        cmpEvent.setParams({
            "headerTitle" : event.target.id,
            "nodeObject" : itemObj 
        });
        cmpEvent.fire();
    },
    /**
     * make default folder
     */
    makeDefault:function(component,event,helper){
        var defaultEvent=component.getEvent('makeDefaultFolderEvent');
        defaultEvent.fire();
    }
})