({
    /**
     * Initialization method which creates component dynamically based on component name and also initializes required components.
     */
    doInit : function(component, event, helper) {
       $A.createComponent(
           component.get('v.componentName'),
           {
               "aura:id": "dynamicCompId",
               "resultValue": component.get('v.resultValue'),
               "comment": component.get('v.comment'),
               "caseId": component.get('v.caseId'),
               "complaintId": component.get('v.complaintId')
           },
           function(newComp, status, errorMessage) {
               //Add component to body
               if (status === "SUCCESS") {
                   var body = component.get("v.body");
                   body.push(newComp);
                   component.set("v.body", body);
                   component.set('v.validate',function(){
                       var dynCmp = component.find('dynamicCompId');
                       var valResult =  dynCmp.get('v.validate')();
                       if(valResult.isValid == true){
                           component.set('v.resultValue',dynCmp.get('v.resultValue'));
                           component.set('v.comment',dynCmp.get('v.comment'));
                       }
                      return valResult;
                   });
               } else if (status === "INCOMPLETE") {
                    var error = $A.get("$Label.c.CQ_UI_No_Response_From_Server");
                    console.log(error);
                    helper.displayError(error, component);

               } else if (status === "ERROR") {
                   console.log("Error: " + errorMessage);
                   helper.displayError(errorMessage, component);
               }
           }
       );
   },

    /**
     * Sets comment and result from lightning event.
     */
    dynamicHandler : function(component,event){
        var result = event.getParam("result");
        var comment = event.getParam("comment");
        component.set('v.resultValue',result);
        component.set('v.comment',comment);        
    },

    /**
     * Handles button navigation.
     */
    handleNavigationEvent: function(component,event){
        var actionClicked = event.getParam('action');
        var okToNavigate = false;
        var cmpResult;
        if (actionClicked == 'NEXT' || actionClicked=='FINISH'){
            var dynCmp = component.find('dynamicCompId');
            cmpResult = dynCmp.getResult();//todo implement interface to mandate method support
            if (cmpResult.status == 'OK') {
                component.set('v.resultValue',cmpResult.returnValue);
                component.set('v.comment',cmpResult.comment);        
                okToNavigate = true;
            }
        } else {
            okToNavigate = true;
        }

        if (okToNavigate == true){
            var navigate = component.get('v.navigateFlow');
            navigate(actionClicked);
        } else {
            //do nothing. error should be shown by dynamic component
        }
    }    
})