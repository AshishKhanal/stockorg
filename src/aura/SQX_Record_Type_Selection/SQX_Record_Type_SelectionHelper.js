({
    /**
     * Returns the record type Id set in the component or parses the recordtypeid parameter in the url
     * @return returns the record type id from the component or URL
     */ 
	getRecordType : function(component) {
		var recordTypeId = component.get('v.recordType'),
            recordTypeIdFromUrl; 
        if(recordTypeId == null ||  recordTypeId.length == 0) {
            // get record type id from url
            recordTypeIdFromUrl = window.location.search.match(/recordtypeid=(\w{15,18})/i);
            if(recordTypeIdFromUrl !== null && recordTypeIdFromUrl.length == 2) {
            	recordTypeId = recordTypeIdFromUrl[1];
        	}
        }
        return recordTypeId;
	}
})