({
    /**
     * Returns the record type id that has been selected in the UI or has been set in the component.
     */ 
    getRecordType: function(component, event, helper) {
        return helper.getRecordType(component);
    },
    /**
     * Validates that a record type has been selected in the component.
     */ 
    validate: function(component, event, helper) {
        var isValid = !!helper.getRecordType(component);
        if(!isValid) {
            component.find('selectRecordType').showHelpMessageIfInvalid();
        }
        
        return isValid;
    }
})