({
    /**
     * handler action method for the Bulk select event
     */
    addToBulkActions: function(component, event, helper) {
        var Evt = component.getEvent('onBulkSelect');
        Evt.fire();
    },

    /**
     * handler method to forward more items request from user to main component
     */
    showMoreItems: function (component, event, helper) {
        var evt = component.getEvent('onMoreItemsRequest');
        evt.fire();
    },

    /**
    * this method is used to sign off PDT
    */
   performAction: function(component, event, helper){
        var actionId = event.target.getAttribute("data-action-id"),
            recordId = event.target.getAttribute("data-record-id"),
            actionUrl = event.target.getAttribute("data-action-url"),
            pageReference,
            navService,
            isLightning = event.target.getAttribute("data-action-is-lightning");

        if(isLightning === "true") {
            pageReference = {
                type: 'standard__component',
                attributes: {
                    componentName: actionUrl,
                },
                state: {
                    "compliancequest__recordId": recordId,
                    "compliancequest__origin": actionId,
                    "compliancequest__returnTo": "homePage"
                }
            };
            component.set("v.pageReference", pageReference);
            navService = component.find("navService");
            pageReference = component.get("v.pageReference");
            event.preventDefault();
            navService.navigate(pageReference);
        }
    }
})