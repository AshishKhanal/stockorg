({
    /**
     * Initializes the component and sets the event handler for receiving the layout configuration
     * from the page and also get the configuration url  
     */
	doInit : function(component, event, helper) {
        // call get the cq vf page url for the component
        helper.getCQURL(component);

        // set title for the lightning page
        document.title = $A.get('$Label.compliancequest.CQ_UI_Doc_Folder_Title');

        // callback to receive the layout configuration and set it
        var funcCallback = $A.getCallback(function(event) {
            if (event.origin !== component.get('v.baseurl')) {
                // Not the expected origin: Reject the message!
                return;
            }
            if (event.data && event.data.folderLayout) {
                component.set('v.layout', JSON.parse(event.data.folderLayout));
            }
        });

        window.addEventListener("message", funcCallback, false);
    },
    /**
     * Handles folder event raised by the component and redirects a user based on the configuration
     * It takes to doc object when header is clicked and opens files when icon is clicked
     */
    handleFolderEvent: function(component, event, helper) {
        var eventType = event.getParam('eventType'),
            doc = event.getParam('document'),
            config = component.get('v.layout'),
            selectedConfig;

        selectedConfig = eventType === 'headerclicked' ? config.header.headerClick : config.header.iconClick;

        if (selectedConfig === 'OpenProfile') {
            var urlEvent = $A.get("e.force:navigateToSObject");
            urlEvent.setParams({
                "recordId": doc.Id
            });

            urlEvent.fire();
        }
        else if (selectedConfig === 'OpenContent') {
            var openFile = $A.get('e.lightning:openFiles');
            openFile.fire({
                recordIds: [doc.compliancequest__Valid_Content_Reference__c],
                selectedRecordId: doc.compliancequest__Valid_Content_Reference__c
            });
        }
        else {
            //disabled do nothing
        }

        event.stopPropagation();
    }
})