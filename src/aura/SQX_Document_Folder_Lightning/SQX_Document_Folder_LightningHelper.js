({
    /**
     * Returns the compliancequest domain so that iframes can be correctly included in the page
     */ 
	getCQURL : function(cmp) {
        var action = cmp.get("c.getCQVFUrl");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.baseurl", response.getReturnValue());
            }
            else if (state === "INCOMPLETE" || state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        action.setStorable();
        $A.enqueueAction(action);
	}
})