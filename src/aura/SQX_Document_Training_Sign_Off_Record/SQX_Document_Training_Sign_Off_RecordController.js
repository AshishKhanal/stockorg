({
    /**
     * this method is used to set layout size
     */
    onInit : function(component, event, helper) {
        var itemObj = [];
        var allEditFields = [];
        var documentFields =["compliancequest__Changes__c"];
        var mainRecord = component.get('v.personnelDocumentTraining');
        var pdtRecord = JSON.parse(JSON.stringify(mainRecord));
        component.set('v.documentFields',documentFields);
        if (component.get('v.editFields').length == 0) {
            component.set('v.isLoading', false);
        }else{
            var editFields = component.get('v.editFields');
            editFields.forEach(function(item){
                itemObj.field = item;
                itemObj.fieldValue = pdtRecord[item];
                allEditFields.push(itemObj);
            });
            component.set('v.pdtEditFields', allEditFields);
        }
    },

    /**
     * this method is used to stop the spinner, when all the details loaded
     */
    editFormLoaded: function(component, event, helper) {
        component.set('v.isLoading', false);
    },
    /**
     * Tracks field changes and updates the model information in the component, which will be used later when signing off.
     */
    onFieldChanged: function(component, event, helper) {
        var value = event.getParam('value'),
        	fieldName = event.getSource().get('v.fieldName'),
            pdt;
        if (value instanceof Array) {
            value = value[0];
        }
        pdt = component.get('v.personnelDocumentTraining');
        pdt.model[fieldName] = value;
        component.set('v.personnelDocumentTraining', pdt);
    },
    /**
     * Method to take assessment
     */
    takeAssessment: function(component, event, value) {
        var url = '/apex/compliancequest__sqx_assessment_start?astId=assessmentId&psnId=personnelId';
        var assessmentId = component.get('v.personnelDocumentTraining.compliancequest__SQX_Assessment__c');
        var personnelId = component.get('v.personnelDocumentTraining.compliancequest__SQX_Personnel__r.Id');
        url = url.replace('assessmentId', assessmentId).replace('personnelId',personnelId);
        var navigateToUrl = $A.get("e.force:navigateToURL");
        navigateToUrl.setParams({
            "url": url
        });
        navigateToUrl.fire();
    },
    /**
     * Method to view assessment and sign off
     */
    viewAssessmentAndSignOff: function(component, event, value) {
        var url = '/apex/compliancequest__SQX_Take_Assessment?astId=assessmentId&dtId=docTrainingId&psnId=personnelId&mode=signoff';
        var assessmentId = component.get('v.personnelDocumentTraining.compliancequest__SQX_Assessment__c');
        var personnelId = component.get('v.personnelDocumentTraining.compliancequest__SQX_Personnel__r.Id');
        var docTrainingId = component.get('v.personnelDocumentTraining.Id');
        url = url.replace('assessmentId', assessmentId).replace('personnelId',personnelId).replace('docTrainingId', docTrainingId);
        var navigateToUrl = $A.get("e.force:navigateToURL");
        navigateToUrl.setParams({
            "url": url
        });
        navigateToUrl.fire();
    }
})