({
	onInit : function(component, event, helper) {
        var namespace = component.get('v.namespace'),
            componentName = component.get('v.componentname'),
            id = component.get('v.recordId');
        $A.createComponent(
            namespace + ":" + componentName,
            {
                recordId: id,
                origin: component.get('v.origin'),
                purpose: component.get('v.purpose')
            },
            function(newCmp, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(newCmp);
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);

                }
            }
        );

	}
})