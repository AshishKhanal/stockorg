({
    /**
    * This method will permit modification of html that is being generated for the component
    * @param {Object} component the component that is being rendered
    * @param {Object} helper the javascript object providing common methods
    */
    render: function(component, helper) {
        var ret = this.superRender();
        
        //grab attributes from the component markup
        var classname = component.get("v.class"),
            xlinkhref = component.get("v.xlinkHref"),
            ariaHidden = component.get("v.ariaHidden"), svg, use;

        //create an svg element w/ the attributes
        svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        use = document.createElementNS("http://www.w3.org/2000/svg", "use");

        svg.setAttribute('class', classname);
        svg.setAttribute('aria-hidden', ariaHidden);
        use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', xlinkhref);
        svg.appendChild(use);
        
        var container = component.find('container').getElement();
        container.appendChild(svg);
        
        
        return ret;
    }
})