({
    /**
     * Initialization method which starts flow and also sets necessary variables.
     */
    init : function(component, event, helper) {
        helper.invokeAction(component.get('c.getFieldSchemaForObject'), {
            objectName: 'compliancequest__SQX_Script_Response__c',
            fieldList: component.get('v.fields')            
        }).then(function(data) {
            // setting field type
            data.fields.forEach(function(field){
                field.type = field.fieldType;
            });
            component.set('v.columns', data.fields);
        })['catch'](function (error) {
            helper.displayError(error, component);
        });
    
        var flow = component.find('dtHost');
        var params = [];
        if (component.get('v.executionType') == 'Decision Tree') {
            // if the execution type is Decision Tree then set only vComplaintId input variable to flow because decision tree execution flow will have only input variable.
            params.push({ name : "vComplaintId", type : "String", value: component.get("v.complaintId")});
        } else {
            // else means execution type is Script so putting all required input variables to flow.
            params.push({ name : "vCaseId", type : "String", value: component.get("v.caseId")},
                          { name : "vComplaintId", type : "String", value: component.get("v.complaintId")},
                          { name : "vComplaintStepId", type : "String", value: component.get("v.complaintStepId")},
                          { name : "taskName", type : "String", value: component.get("v.taskName")});
            
        }
        flow.startFlow(component.get("v.flowName"),params);
    },

    /**
     * Handles the status change from flow.
     */
    handleStatusChange : function (component, event) {
        if(event.getParam("status") === "STARTED" || event.getParam("status") === "FINISHED") {
            // Get the output variables and iterate over them
            var outputVariables = event.getParam("outputVariables");
            var outputVar;         
            if (outputVariables){
                for(var i = 0; i < outputVariables.length; i++) {
                    outputVar = outputVariables[i];
                    // Pass the values to the component's attributes
                    if(outputVar.name === "vObjResponses" && outputVar.value) {
                        // sorting array in reverse order of Response_Sequence__c
                        outputVar.value.reverse(function(a, b){
                            return a.cqcomplaint__Response_Sequence__c - b.cqcomplaint__Response_Sequence__c;
                        });
                        component.set("v.responses", outputVar.value);
                    } 
                }
            }
        }
    },

    /**
     * Handles button navigations.
     */
    handleNavigate:function(cmp,event,helper) {
        var pckLst = cmp.find('picklistControl');
        var action = event.getParam("action");
        if (pckLst && (action=='NEXT') && pckLst.get('v.validity').valueMissing) {
            pckLst.focus();
            pckLst.showHelpMessageIfInvalid();
            return;
        }
       // Figure out which action was called       
       var actionClicked = event.getSource().getLocalId();
       // Call that action
       var navEvent = cmp.getEvent("navigateFlowEvent");
       navEvent.setParam("action", actionClicked);
       navEvent.fire(); 
    }
})