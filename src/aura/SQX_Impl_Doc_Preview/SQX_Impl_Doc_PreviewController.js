({
    onInit : function(component, event, helper) {

        helper.invokeAction(component.get('c.getImplementationDocFile'), {
            recordId: component.get('v.recordId')
        }).then($A.getCallback(function(data) {
           component.set('v.objectList', data);
        })).catch(function(err){
            helper.displayError(err, component);
        });
    }
})