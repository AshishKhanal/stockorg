({
    /**
     * initializer module
     */
    init: function (component) {
        // collapse modules list on phone
        var ddDiv = component.find('divActionFiltersContainer');
        var isPhone = $A.get("$Browser.isPhone");
        if (isPhone) {
            $A.util.addClass(ddDiv, 'slds-hide');
        }
    },

    /**
     * method to toggle the visibility of the action filters list container. First load has it open
     */
    toggleVisibility: function (component, event, helper) {
        var ddDiv = component.find('divActionFiltersContainer');
        $A.util.toggleClass(ddDiv, 'slds-hide');
    }
})