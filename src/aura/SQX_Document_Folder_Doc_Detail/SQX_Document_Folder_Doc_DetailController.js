({
    /**
     * This event handler sets the title of the document when document is changed
     */
    docChanged : function(component, event, helper) {
        var doc = component.get('v.doc').doc,
            layout = component.get('v.layout'),
            headerText = '',
            headerField,
            fields = [],
            field,
            fieldDetail,
            labelfieldName,
            i, subsequent = false;
        if(layout && doc) {
            for(var i = 0; i < layout.header.headerFields.length; i++) {
                headerField = layout.header.headerFields[i];
                if(doc[headerField]) {
                    headerText = headerText + (subsequent ? layout.header.headerSeparator : '') + doc[headerField];
                    subsequent = true;
                }
            }

            for(var i = 0; i < layout.detail.fieldList.length; i++) {
                field = layout.detail.fieldList[i];
                fieldDetail = {};
                if(field.type == 'reference') {
                    fieldDetail.value = doc[field.relation][field.field];
                }
                else {
                    labelfieldName = field.field.replace('__c', '') + '__label';
                    fieldDetail.value = doc[labelfieldName] || doc[field.field];
                }

                fieldDetail.label = field.label;
                fieldDetail.type = field.type;
                fields.push(fieldDetail);
            }
        }

        component.set('v.headerText', headerText);
        component.set('v.fields', fields);
	},
    /**
    * this method will show the related documents for the selected controlled document
    */
    showRelatedDocuments:function(component,event,helper){
        var cmpEvent = component.getEvent("relatedDocEvent");
        var docId=event.target.id;
        cmpEvent.setParams({
            "documentId" : docId,
            "headerTitle" : component.get('v.headerText')
        });
        cmpEvent.fire();
    },
    /**
     * raises an application level event when the icon is clicked
     */
    iconClicked: function(component, event, helper) {
        var appEvent = $A.get("e.compliancequest:SQX_Document_Folder_Event");
        var doc = component.get('v.doc').doc;
        appEvent.setParam("eventType", "iconclicked");
        appEvent.setParam("document", doc);
        appEvent.fire();
    },
    /**
     * raises an application level event when the header text is clicked
     */
    headerClicked: function(component, event, helper) {
        var appEvent = $A.get("e.compliancequest:SQX_Document_Folder_Event");
        var doc = component.get('v.doc').doc;
        appEvent.setParam("eventType", "headerclicked");
        appEvent.setParam("document", doc);
        appEvent.fire();
    }
})