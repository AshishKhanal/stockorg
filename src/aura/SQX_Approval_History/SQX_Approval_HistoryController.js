({
    /**
     * this method used to fetch approval history, row action buttons and columns
     */ 
    onInit : function(component, event, helper) {
        if(component.get("v.sObjectName") == 'compliancequest__SQX_Controlled_Document__c') {
            if(component.get("v.simpleRecord.compliancequest__Approval_Status__c") == 'In Change Approval') {
                component.set('v.typeOfApproval', 'changeOrderApproval');
            }
        }        
        // callback binding
        var actions = helper.getRowActions.bind(this, component);
        component.set('v.columns', [
            { label: $A.get("$Label.compliancequest.CQ_UI_Name"), fieldName: 'stepName', type: 'text' },
            { label: $A.get("$Label.compliancequest.CQ_UI_Date"), fieldName: 'date', type: 'date' },
            { label: $A.get("$Label.compliancequest.CQ_UI_Status"), fieldName: 'stepStatus', type: 'text' },
            { label: $A.get("$Label.compliancequest.CQ_UI_Assigned_To"), fieldName: 'assignedName', type: 'text' },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
        helper.invokeAction(component.get('c.getObjects'), 
                            {
                                'mainRecordId': component.get('v.recordId'),
                                'objectName': 'ProcessInstanceWorkItem',
                                'purpose': 'approval'
                            }
                           ).then(function(result) {
            var displayData=[], pi;
            var idx;
            var currentUserId = result.currentUser.Id;
            var showBtns = false;
            var isInApproval = false;
            for(idx = 0; idx < result.sobjects.length; idx++) {
                pi = result.sobjects[idx];
                if(pi.StepsAndWorkitems){
                    pi.StepsAndWorkitems.forEach(function(item){
                        var fetchData = {
                            id: item.Id,
                            stepName: item.ProcessNode == undefined ? '-' : item.ProcessNode.Name,
                            date: item.CreatedDate,
                            stepStatus: item.StepStatusLabel,
                            assignedName: item.Actor.Name
                        }
                        displayData.push(fetchData); 
                        if(showBtns == false){
                            if(currentUserId == item.Actor.Id && item.IsPending){
                                showBtns = item.IsPending;
                                isInApproval = item.IsPending;
                            }
                            component.set('v.workitemId', item.Id);
                        }

                        isInApproval = isInApproval || item.IsPending;
                    });
                }
            }
            component.set('v.showButtons', showBtns);
            component.set('v.data', displayData);
            component.set('v.mainRecordLabel', result.mainRecordName);
            component.set('v.isInApproval', isInApproval);
            if(result.actions && result.actions.length > 0) {
                component.set('v.componentName', result.actions[0].compliancequest__Namespace__c+"__"+result.actions[0].compliancequest__Component_Name__c);
            }
        })['catch'](function (err) {
            helper.displayError(err, component);
        });
    },
    
    /**
     * this method used to fire row action event(s)
     */
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var workItemId = event.getParam('row').id;
        switch (action.name) {
            case 'approveReject':
                helper.approveReject(component, event, workItemId);
                break;
        }
        
    },
    
    /**
     * handle to approve/reject
     */ 
    approveReject: function(component, event, helper){
        helper.approveReject(component, event, component.get('v.workitemId'));
    },
    
    /**
     * handle to open recall layout
     */ 
    handleSelect: function (component, event, helper) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var innerCmp = component.find("cmpRecallApproval");
        var selectedMenuItemValue = event.getParam("value");
        innerCmp.set("v.mainRecordId", component.get('v.recordId'));
        if(selectedMenuItemValue === 'Recall'){
            innerCmp.set('v.title', $A.get('$Label.compliancequest.CQ_UI_Recall_Approval_Request'));
            innerCmp.set("v.isOpen", true);
            innerCmp.set('v.typeOfApproval', component.get('v.typeOfApproval'));
        }
    }
})