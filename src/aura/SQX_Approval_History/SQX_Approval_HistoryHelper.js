({
    /**
     * this method get row actions
     */
    getRowActions: function(component, row, callback) {
        var actions = [];
        actions.push(
            {
                label: $A.get("$Label.c.CQ_UI_Homepage_Action_Approve_Reject"),
                name: "approveReject",
                disabled: row.stepStatus != 'Pending'
            }
        );
        callback(actions);
    },

    /**
     * this method used to navigate approve/reject component
     */
    approveReject: function(component, event, workitemId){

        if(component.get('v.componentName') == null) {
            component.set('v.workitemId', workitemId);
            component.set('v.showapprovalpopup', true);
        } else {
            var  pageReference = {
                type:'standard__component',
                attributes: {
                    "componentName": component.get('v.componentName')
                },
                state: {
                    "compliancequest__recordId":component.get('v.recordId'),
                    "compliancequest__origin": workitemId
                }
            };
            var navService = component.find('navService');
            navService.navigate(pageReference);
        }
    }
})