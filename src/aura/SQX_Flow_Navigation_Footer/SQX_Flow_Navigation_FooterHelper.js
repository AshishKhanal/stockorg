({
    /**
     * Handles the navigation
     */
    handleNavigateAction: function(cmp, event, helper) {           
        // Figure out which action was called
        var actionClicked = event.getSource().getLocalId();
        var navigate = cmp.get('v.navigateFlow');//available for flow screen makes this possible
        navigate(actionClicked);
    },

    /**
     * Handles the button navigation and raised an app event.
     */
    startNavigateAction: function(cmp, event, helper) {
        // Figure out which action was called  and raise app event which dynamic host container
        // will handle, validate and then propagate to flow     
        var actionClicked = event.getSource().getLocalId();
        var navEvent = $A.get("e.c:SQX_Flow_Navigation_Event");
        navEvent.setParam("action", actionClicked);
        navEvent.fire();
    }
})