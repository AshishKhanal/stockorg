({

    /**
     * Initializes necessary variables.
     */
    init : function(cmp, event, helper) {
        // Figure out which buttons to display
        var availableActions = cmp.get('v.availableActions');
        for (var i = 0; i < availableActions.length; i++) {
            if (availableActions[i] == "PAUSE") {
                cmp.set("v.canPause", true);
            } else if (availableActions[i] == "BACK") {
                cmp.set("v.canBack", true);
            } else if (availableActions[i] == "NEXT") {
                cmp.set("v.canNext", true);
            } else if (availableActions[i] == "CANCEL") {
                cmp.set("v.canCancel", true);
             } else if (availableActions[i] == "FINISH") {
                cmp.set("v.canFinish", true);
                cmp.set("v.canCancel", true);
                cmp.set("v.canNext", false); 
            }
        }
   },

   /**
     * Performs action based on which button is clicked.
     */
   onButtonPressed:function(cmp,event,helper){
        //set validateNavigation if flow screen has dynamic component or others that have to 
        //validate date before allowing next
        if (cmp.get('v.validateNavigation')==true){
            helper.startNavigateAction(cmp,event);
        } else {
            helper.handleNavigateAction(cmp,event,helper);
       }
    }
})