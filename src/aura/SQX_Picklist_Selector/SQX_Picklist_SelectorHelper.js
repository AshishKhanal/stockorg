({
    /**
     * Validates and returns the final result.
     */

    /**
     * Validates input fields which are required.
     */
    validateRequiredField:function(cmp) {
        var pckLst = cmp.find('picklistControl');
        var isValid = true;
        var message = 'Response is required';

        if (pckLst.get('v.validity').valueMissing){
            pckLst.focus();
            pckLst.showHelpMessageIfInvalid();
            isValid=false;
        }
        return {'isValid':isValid,'errorMessage':message};
    }
})