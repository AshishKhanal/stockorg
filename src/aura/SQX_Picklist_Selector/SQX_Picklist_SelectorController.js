({

    /**
     * Initializes necessary variables.
     */
    init : function(cmp, event, helper) {
        var action = cmp.get("c.getPicklistItems");
        cmp.set('v.validate', function() {
            return helper.validateRequiredField(cmp);
        });
        action.setParams({category : cmp.get('v.category')},{parentFilter:cmp.get('v.parentValue')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.options", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    /**
     * Sets selected option value.
     */
    setSelected:function(cmp,event,helper) {
        cmp.set('v.selectedOption',event.getSource().get('v.value'));
    },

    /**
     * Handles the button navigation
     */
    handleNextAction:function(cmp,event,helper){
        var actionClicked = event.getParam('action');
        var isValid = true;
        if (actionClicked == 'NEXT' || actionClicked=='FINISH') {
            var result = helper.validateRequiredField(cmp);
            isValid = result.isValid;
        }
        if (isValid) {
            var navigate = cmp.get('v.navigateFlow');
            navigate(actionClicked);
        }
    },

    /**
     * Collects final result.
     */
    getResult:function(cmp,event) {
        //this is currently not needed, experimental model to see what happens if this component is 
        //dynamically loaded as well
        helper.validateAndReturnResult();
    }
})