({
    /**
     * this method get record id and redirect to their record
     */
    init : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId") == undefined ? "": cmp.get("v.recordId");
        if(recordId !=""){
            var navEvent = $A.get("e.force:navigateToSObject");
            navEvent.setParams({
                recordId: recordId
            });
            navEvent.fire(); 
        }
    },
    
     /**
     * this method fired button click event
     */
    onButtonPressed: function(cmp, event, helper) {
        // Figure out which action was called
        var actionClicked = event.getSource().getLocalId();
        var navigate = cmp.get('v.navigateFlow');
        navigate(actionClicked);
    }
})