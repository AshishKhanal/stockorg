<apex:page standardController="SQX_Controlled_Document__c" extensions="SQX_Extension_Controlled_Document" lightningStylesheets="true">
    <apex:includeScript value="{!URLFOR($Resource.cqUI, '/scripts/jquery.2.1.3.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.cqUI, '/pages/scripts/SQX_Controlled_Document_Detail.min.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.cqUI, 'styles/cqui.min.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.cqUI, 'pages/styles/shared.min.css')}" />
    <apex:outputText rendered="false"  id="mainDocName" value="{!SQX_Controlled_Document__c.name}"></apex:outputText>
    <apex:outputText rendered="false"  id="Document_Type" value="{!SQX_Controlled_Document__c.Document_Type__c}"></apex:outputText>    
    <apex:outputText rendered="false"  id="IssuedDate" value="{!SQX_Controlled_Document__c.Date_Issued__c}"></apex:outputText>
    <apex:variable var="docUrl" value="{!URLFOR($Action.SQX_Controlled_Document__c.View, SQX_Controlled_Document__c.Id )}"/>
    <apex:variable var="IsAuditCriteria" value="{! BEGINS(RecordTypes['Audit_Criteria'], SQX_Controlled_Document__c.RecordTypeId) }" />
    <apex:variable var="IsInspectionCriteria" value="{! BEGINS(RecordTypes['Inspection_Criteria'], SQX_Controlled_Document__c.RecordTypeId) }" />
    <apex:variable var="IsCourse" value="{! BEGINS(RecordTypes['Course'], SQX_Controlled_Document__c.RecordTypeId) }" />

    <!-- JavaScript variables -->
    <apex:variable var="approvalStatus" value="{!SQX_Controlled_Document__c.Approval_Status__c}" />
    <apex:variable var="synchronizationStatus" value="{!SQX_Controlled_Document__c.Synchronization_Status__c}" />
    <apex:variable var="autoSubmitForApproval" value="{!SQX_Controlled_Document__c.Auto_Submit_For_Approval__c}" />
    <apex:variable var="checkedOut" value="{!SQX_Controlled_Document__c.Checked_Out__c}" />
    <apex:variable var="collaborationGroupId" value="{!JSENCODE(SQX_Controlled_Document__c.Collaboration_Group_Id__c)}" />
    <apex:variable var="docStatus" value="{!JSENCODE(SQX_Controlled_Document__c.Document_Status__c)}" />
    
    <div class='cq-hide'>
        <!-- hidden initially as we do not need to show it here. we are using its value in the section header by script -->
        <apex:outputField value="{!SQX_Controlled_Document__c.RecordTypeId}" id="recordType"/>
    </div>
    
    <style>
        .errorDiv {max-height: 90px; overflow: auto;}
    </style>
    
    <script type="text/javascript">
        var msg = "{!JSENCODE($Label.compliancequest__CQ_UI_Approval_Recall_Confirmation)}",
            obsolescenceButtonLabel = "{!JSENCODE($Label.compliancequest__CQ_UI_Submit_For_Obsolescence_Approval)}",
            approvalLink = "{!URLFOR($Page.SQX_Controlled_Document_Approval, null, [id = SQX_Controlled_Document__c.Id, reqId='approvalId', retURL = docUrl])}",
            isDocInChangeApproval = "{!IF(approvalStatus == 'In Change Approval', 'true', 'false')}",
            isDocInReleaseApproval = "{!IF(approvalStatus == 'Release Approval', 'true', 'false')}",
            isDocInObsolescenceApproval = "{!IF(approvalStatus == 'Obsolescence Approval', 'true', 'false')}",
            canRecallFromChangeApproval = "{!If(canRecallFromChangeApproval, 'true', 'false')}",
            isApprovalSubmissionPending = "{!If(AND(synchronizationStatus == 'Pending', autoSubmitForApproval == 'Hold'), 'true', 'false')}",
            formSubmitSyncId = "{!$Component.frmSubmitForApprovalSync}",
            approvalButtonId = "{!$Component.frmSubmitForApprovalSync.submitForApprovalSync2Btn}",
            visibleApprovalButtonId = "{!$Component.frmSubmitForApprovalSync.submitForApprovalSyncBtn}",
            hasEditAccess = "{!RequestItemPermission.HasEditAccess}",
            controlledDocCheckout = "{!checkedOut}",
            collaborationGrpId = "{!collaborationGroupId}",
            controlledDocStatus = "{!docStatus}", 
            submitForApproval= '<input type="button" class="btn" disabled="true" value="{!JSENCODE($Label.compliancequest__CQ_UI_Submit_For_Approval)}" title="{!$Label.CQ_UI_Approval_With_Secondary_Content}" />',
            recallFromChangeApproval='<input type="button" class="btn" value="{!JSENCODE($Label.compliancequest__CQ_UI_Recall_From_Change_Approval)}" onclick="cq.recallFromChangeApproval()" />',
            recordTypeId = "{!$Component.recordType}";

    </script>


    <div class='errorDiv'>
        <apex:pageMessages id="pageMessages"/>
    </div>
    <apex:detail showChatter="true" relatedList="false" inlineEdit="true"/>

    <div class="cq-audit-scopes">
        <apex:relatedList list="compliancequest__SQX_Audit_Scopes__r" rendered="{!IsAuditCriteria}" />
    </div>

    <div class="cq-specifications">
        <apex:relatedList list="compliancequest__SQX_Specifications__r" rendered="{!IsInspectionCriteria}" />
    </div>

    <div class="cq-associations">
        <apex:relatedList list="compliancequest__SQX_Associations__r" rendered="{!IsInspectionCriteria}" />
    </div>
    
    <apex:form id="docAdditionalInfoForm" >
        <apex:pageBlock Title="{!$Label.CQ_UI_Approvers}" rendered="{! $Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Use_Approval_Matrix_For_Document__c == false}">   
            <apex:pageBlockSection columns="1">
                <apex:OutputField value="{!SQX_Controlled_Document__c.First_Approver__c}" />
                <apex:OutputField value="{!SQX_Controlled_Document__c.Second_Approver__c}" />
                <apex:OutputField value="{!SQX_Controlled_Document__c.Third_Approver__c}" />
                <apex:OutputField value="{!SQX_Controlled_Document__c.Fourth_Approver__c}" />
                <apex:OutputField value="{!SQX_Controlled_Document__c.Fifth_Approver__c}" />
            </apex:pageBlockSection>
        </apex:pageBlock>

        <apex:pageBlock Title="{!$Label.CQ_UI_Controlled_Document_Approval_Steps}" rendered="{!$Setup.SQX_Custom_Settings_Public__c.Use_Approval_Matrix_For_Document__c}">
            <apex:pageBlockButtons location="top" >
                <apex:outputLink value="{!URLFOR($Page.SQX_CDoc_AppStep, null, [docId = SQX_Controlled_Document__c.Id ])}" target="_parent"
                rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.createable}" 
                styleClass="btn"
                style="text-decoration:none;padding:4px;">{!$Label.CQ_UI_New_Approval_Step}</apex:outputLink>
            </apex:pageBlockButtons>            
            <apex:pageBlockSection columns="1">
                <apex:OutputField value="{!SQX_Controlled_Document__c.SQX_Approval_Matrix__c}" />
            </apex:pageBlockSection>      
            <div style="width: 100%; height: 145px; overflow-y: scroll; overflow-x:auto;" id="docApprovalStep">     
                <apex:pageBlockTable var="docStep" value="{!ApprovalDocumentSteps}" >
                    <apex:column width="10%">
                        <apex:facet name="header">{!$Label.CQ_UI_Action}</apex:facet>

                        <apex:outputLink target="_parent" rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.updateable}"
                        value="{!URLFOR($Page.SQX_CDoc_AppStep, null,  [id = docStep.Id])}"
                        >{!$Label.CQ_UI_Edit}</apex:outputLink> &nbsp; | &nbsp;  

                        <apex:commandLink action="{!deleteApprovalStep}"
                        target="_parent"
                        value="{!$Label.compliancequest__CQ_UI_Del}"
                        rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.deletable}" 
                        onclick="if (!confirm('{!JSENCODE($Label.compliancequest__CQ_UI_Alert_Confirmation)}')) return false;"
                        oncomplete="navigateToUrl('{!URLFOR($Page.SQX_Controlled_Document_Detail, null, [id = SQX_Controlled_Document__c.Id])}')" >
                        <apex:param name="stepId" value="{!docStep.id}"/>
                        </apex:commandLink>
                    </apex:column>

                    <apex:column value="{!docStep.Step__c}"/>

                    <apex:column value="{!docStep.SQX_Job_Function__c}"/> 

                    <apex:column value="{!docStep.SQX_User__c}"/>

                    <apex:column value="{!docStep.Is_Active__c}"/>

                </apex:pageBlockTable>
                <apex:outputPanel rendered="{!ApprovalDocumentSteps.size == 0}">
                    <p class="emptyList">{!$Label.CQ_UI_Doc_With_No_Approver}</p>
                </apex:outputPanel>
            </div>  
        </apex:pageBlock>
        
        <apex:actionFunction action="{!recallFromChangeApproval}" name="callRecallFromChangeApproval" rendered="{!canRecallFromChangeApproval}" />
    </apex:form>

    <apex:form id="frmSubmitForApprovalSync">
        <apex:variable var="msgOnApprovalSubmission" value="{!IF(SQX_Controlled_Document__c.Secondary_Content__c == 'Auto',
                                                                    $Label.cq_ui_submit_for_synchronization_started,
                                                                    $Label.cq_ui_submit_for_approval_started)}" />
        
        <apex:commandButton value="{!$Label.cq_ui_submit_for_approval}" id="submitForApprovalSyncBtn" action="{!syncDocumentForSubmission}" styleClass="btn" rerender="pageMessages" 
        onclick="cq.syncStart('{!msgOnApprovalSubmission}')"
        oncomplete="cq.syncComplete()" />

        <!-- Second button is for actual submission for approval of the content -->
        <apex:commandButton value="{!$Label.cq_ui_submit_for_approval}" id="submitForApprovalSync2Btn" action="{!submitDocForApproval}" styleclass="cq-hide" rerender="pageMessages" oncomplete="cq.approvalSubmissionComplete('{!$Label.cq_ui_submit_for_approval_completed}')" 
                            onclick="cq.approvalSubmissionStart('{!JSENCODE($Label.compliancequest__cq_ui_submit_for_approval_started)}')"
                            />
    </apex:form>
    <apex:relatedList list="ProcessSteps" pageSize="100" />

    <compliancequest:SQX_Controlled_Document_Revision_History currentDocument="{! compliancequest__SQX_Controlled_Document__c }" />

    <div class="cq-referenced-by-documents">
        <apex:pageBlock Title="{!$Label.CQ_UI_Controlled_Document_Referenced_By_Documents}">
            <div style="width: 100%; overflow-y: scroll; overflow-x:auto;" id="referringDocFrame">     
                <apex:pageBlockTable var="refByDoc" value="{!ReferringDocuments}"  rendered="{!ReferringDocuments.size>0}" >
                    <apex:column title="!$ObjectType.compliancequest__SQX_Controlled_Document__c.Label}">
                        <apex:facet name="header">{!$ObjectType.compliancequest__SQX_Controlled_Document__c.Label}</apex:facet>
                        <apex:outputLink value="{!URLFor('/'+refByDoc.Controlled_Document__c)}" target="top">
                        {!HTMLENCODE(refByDoc.Controlled_Document__r.Name)}
                        </apex:outputLink>
                    </apex:column>
                    <apex:column value="{!refByDoc.Controlled_Document_Number__c}"/>
                    <apex:column value="{!refByDoc.Controlled_Document_Rev__c}"/> 
                    <apex:column value="{!refByDoc.Controlled_Document_Title__c}"/> 
                </apex:pageBlockTable>
                <apex:outputPanel rendered="{!ReferringDocuments.size == 0}">
                    <p class="emptyList">{!$Label.CQ_UI_Controlled_Document_No_document_make_reference_to_this_document}</p>
                </apex:outputPanel>
            </div>
        </apex:pageBlock>
    </div>

    <div class="cq-qualified-trainers">
        <apex:relatedList list="compliancequest__SQX_Qualified_Trainers__r" rendered="{!IsCourse}" />
    </div>

    <div class="cq-related-documents">
        <apex:relatedList list="compliancequest__Related_Documents__r" />
    </div>

    <div class="cq-document-reviews">
        <apex:relatedList list="compliancequest__Document_Reviews__r"/>
    </div>

    <div class="cq-requirements">
        <apex:relatedList list="compliancequest__SQX_Requirements__r"/>
    </div>

    <div class="cq-personnel-document-trainings">     
        <apex:relatedList list="compliancequest__SQX_PersonnelDocumentTrainings__r" rendered="{!OR(IsStandardDoc, IsAuditCriteria, IsInspectionCriteria, IsCourse)}"/> 
    </div>

    <div class="cq-distribution-tracking">     
        <apex:relatedList list="compliancequest__SQX_Distribution_Trackings__r" rendered="{!IsStandardDoc}"/> 
    </div>

    <div class="cq-combined-attachments">    
        <apex:relatedList list="CombinedAttachments" rendered="{! NOT( $User.UITheme == 'Theme4d' ) }" /> 
        <apex:relatedList list="AttachedContentDocuments" rendered="{! $User.UITheme == 'Theme4d' }" />
    </div>
    
    <div class="cq-record-activities">     
        <apex:relatedList rendered="{!NOT(OR(IsCourse, IsAuditCriteria, IsInspectionCriteria))}" list="compliancequest__SQX_Controlled_Doc_Record_Activities__r"/>
    </div>
    

</apex:page>