<apex:page standardController="SQX_Audit__c" extensions="SQX_Extension_Audit" doctype="html-5.0"
    tabStyle="SQX_Audit_Calendar_View__tab"
    title="{!$ObjectType.SQX_Audit__c.label} {!$Label.compliancequest__cq_ui_calendar_view}">

    <compliancequest:SQX_Common_Scripts extension="{!Self}" newJS="true"/>
    <script type="text/javascript">
        (function(){

            /**
             * Method to retrieve record using visul for remote object and
             * @param {Object} record : instance of remote object model
             * @param {Object} recordList : list to store record list
             * @param {Object} filter : criteria for fetching record
             * @param {Number} offset : number of record to ignore during retrieval
             */
            function retrieveRecords(record, recordList, filter, offset){
                var criteria = { limit: SF_RETRIEVAL_LIMIT };
                if(filter !== undefined && !jQuery.isEmptyObject(filter)){
                    criteria.where = filter;
                }
                if(offset > 0){
                    criteria.offset = offset
                }

                return new Promise(function(resolve, reject){
                    record.retrieve(
                        criteria,
                        function(err, records, event) {
                            if (err) {
                                return reject(err);
                            } else {
                                recordList = recordList.concat(records);
                                return resolve(event);
                            }
                        });
                }).then(function(response){
                    // recurse the function if more records are found else return records
                    return response.result.size < SF_RETRIEVAL_LIMIT || recordList.length == SF_MAX_RETRIEVAL_LIMIT ? recordList : retrieveRecords(record, recordList, filter, offset+response.result.size);
                })['catch'](function(err) {
                    if(window.console){
                        window.console.error(err);
                    }
                    setFormError(err.message);
                    hideLoading();
                });
            }

            /**
             * Returns the records by querying salesforce. Uses promise pattern
             * @since 7.4.0
             */
            cq.apis.retrieveRecords = retrieveRecords;

        }());
        cq.apis.callback = cq.apis.callback || {};

         /**
         * @argument {auditsWithoutPlanPeriod} audit without plan period to be displayed in panelbar
         * @description show warning panelbar in top of spreadsheet
         */ 
        function showWarningNotification(auditsWithoutPlanPeriod){
 
            var pb = $('<ul id="panelbar"></ul>');
            var template = kendo.template($("#auditsWithoutPlanPeriodTemplate").html());            
            var result = template(auditsWithoutPlanPeriod);
            pb.html(result); //Append the result
            pb.prependTo($('#spreadsheet').parent());
            $("#panelbar").kendoPanelBar({});             
        }
    </script>

    <apex:define name="customerScriptBlock">
        <apex:composition template="{!CustomerScriptPageLayout}">
        </apex:composition>
    </apex:define>

    <apex:define name="detailBlock">
        <apex:composition template="{!DetailPageLayout}">
        </apex:composition>
    </apex:define>

    <apex:insert name="customerScriptBlock" />
    <br />
    <apex:insert name="detailBlock" />

    <script>
        var SF_MAX_RETRIEVAL_LIMIT = 2000; // maximum limit for remote object retrieval in multiple translation
        var SF_RETRIEVAL_LIMIT = 100; // limit of remote object retrieval in single translation

        $(document).ready(function() {
            var retrieveRecords = cq.apis.retrieveRecords;
            var auditProgram = new SObjectModel.compliancequest__SQX_Audit_Program__c();
            var multiSelect;
            var mergedCells = [];
            var auditProgramList = [];
            var columnMap = {headerColumnIndex: {}};
            var programsList = [];
            var palette = [
                            '#BBE5ED','#BFBCCB','#B399A2','#BCAB79','#C6E0FF','#EEF4D4','#DAEFB3','#EA9E8D','#D64550','#A1E8AF',
                            '#94C595','#747C92','#CF5C36', '#EFC88B', '#F4E3B2', '#D3D5D7', '#6B9080','#A4C3B2','#CCE3DE','#EAF4F4',
                            '#9AADBF','#6D98BA','#D3B99F','#C17767', '#FCDE9C','#C4D6B0','#FFA552','#97EAD2', '#8CC7A1','#E8F1F2',
                            '#1B98E0','#BA2C73', '#B0A084','#E9E6FF','#4281A4','#48A9A6', '#E4DFDA','#D4B483','#C1666B',
                            '#ACECA1','#96BE8C', '#C9F2C7', '#629460', '#BB0A21', '#4B88A2', '#574D68'
                        ];
            var _getUrl = window.location;
            window._baseUrl = _getUrl.protocol + "//" + _getUrl.host + "/" + _getUrl.pathname.split('/')[0];

            window.recordMap = {};
            window.criteriaMap = {};
            window.leadAuditorMap = {};

            /**
             * Creates a kendo multi select element for the given UI element
             * @param {*} element the UI/dom element that is to be initialized
             * @param {*} properties the applicable properties to set for the component
             */ 
            function createMultiSelect(element, properties) {
                var uitemplateProperties = JSON.parse(JSON.stringify(kendo.ui.MultiSelect.prototype.options));
                uitemplateProperties.templateIds = { header : '', footer: '', item: '', tag: '' };
                uitemplateProperties.sourceName = '';

                uitemplateProperties = kendo.parseOptions(element[0], uitemplateProperties);
                if(uitemplateProperties.templateIds) {
                    for(var tkey in uitemplateProperties.templateIds) {
                        uitemplateProperties[tkey + 'Template'] = $('#' + uitemplateProperties.templateIds[tkey]).html();
                    }
                }
                if(uitemplateProperties.sourceName) {
                    uitemplateProperties.dataSource = kendo.getter(uitemplateProperties.sourceName)(window)
                    if(!uitemplateProperties.dataSource) {
                        console.error('No source was loaded from template');
                    }
                }
                $.extend(properties, uitemplateProperties);

                element.kendoMultiSelect(properties);
            }

            createMultiSelect($("#division"), {
                dataTextField: "text",
                dataValueField: "value",
                dataSource: SQXSchema.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Division__c.additionalInfo.validValues,
                filter: "contains",
                placeholder: sqx.translation.get('cqcompositefield.comboplaceholder', { fieldName : '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Division__c.Label)}' })
            });

            createMultiSelect($("#businessunit"), {
                dataTextField: "text",
                dataValueField: "value",
                dataSource: SQXSchema.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Business_Unit__c.additionalInfo.validValues,
                filter: "contains",
                placeholder: sqx.translation.get('cqcompositefield.comboplaceholder', { fieldName : '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Business_Unit__c.Label)}' })
            });

            createMultiSelect($("#site"), {
                dataTextField: "text",
                dataValueField: "value",
                dataSource: SQXSchema.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Site__c.additionalInfo.validValues,
                filter: "contains",
                placeholder: sqx.translation.get('cqcompositefield.comboplaceholder', { fieldName : '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Site__c.Label)}' })
            });

            createMultiSelect($("#region"), {
                dataTextField: "text",
                dataValueField: "value",
                dataSource: SQXSchema.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Region__c.additionalInfo.validValues,
                filter: "contains",
                placeholder: sqx.translation.get('cqcompositefield.comboplaceholder', { fieldName : '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Org_Region__c.Label)}' })
            });

            var datepicker = $("#datepicker").kendoDatePicker({
            	format: sqx.common.CQDateFormat
            }).data("kendoDatePicker");
            if(datepicker.value() === null){
                datepicker.value(kendo.toString(kendo.parseDate(new Date()), sqx.common.CQDateFormat));
            }
            var enddatepicker = $("#enddatepicker").kendoDatePicker({
            	format: sqx.common.CQDateFormat
            }).data("kendoDatePicker");
            if(enddatepicker.value() === null){
                enddatepicker.value(kendo.toString(kendo.parseDate(new Date(new Date().setFullYear(new Date().getFullYear() + 1))), sqx.common.CQDateFormat));
            }
            var division = $("#division").data("kendoMultiSelect");
            var businessUnit = $("#businessunit").data("kendoMultiSelect");
            var site = $("#site").data("kendoMultiSelect");
            var region = $("#region").data("kendoMultiSelect");
            var startDateValidator = $("#startDate").kendoValidator({
                rules: {
                    dateValidation: function (e) {
                        var currentDate = kendo.parseDate($(e).val(), sqx.common.CQDateFormat);
                        if (!currentDate) {
                            return false;
                        }
                        return true;
                    }
                },
                messages: {
                    required: "{!JSENCODE($Label.compliancequest__sqx_err_msg_field_is_required)}".replace('{field}', '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Start_Date__c.Label)}'),
                    dateValidation: function(e){
                        return sqx.translation.get('cqcompositefield.invaliddate', { fieldName: $(e).val()});
                    }
                }
            }).data("kendoValidator");

            var endDateValidator = $("#endDate").kendoValidator({
                rules: {
                    dateValidation: function (e) {
                        var currentDate = kendo.parseDate($(e).val(), sqx.common.CQDateFormat);
                        //Check if Date parse is successful
                        if (!currentDate) {
                            return false;
                        }
                        return true;
                    },
                    endDateValidation: function (e) {
                        var startDate = datepicker.value();
                        var endDate = enddatepicker.value();
                        if (endDate < startDate) {
                            return false;
                        }
                        return true;
                    }
                },
                messages: {
                    //Define your custom validation massages
                    required: "{!JSENCODE($Label.compliancequest__sqx_err_msg_field_is_required)}".replace('{field}', '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__End_Date__c.Label)}'),
                    dateValidation: function(e){
                        return sqx.translation.get('cqcompositefield.invaliddate', { fieldName: $(e).val()});
                    },
                    endDateValidation: "{!JSENCODE($Label.compliancequest__cq_ui_end_date_error)}"
                }
            }).data("kendoValidator");

            
            createMultiSelect($("#pgms"), {
                dataTextField: "Name",
                dataValueField: "Id",
                filter: 'contains',
                dataSource: sqx.remote.createDataSourceForObject('compliancequest__SQX_Audit_Program__c', ['Id', 'Name']),
                placeholder: sqx.translation.get('cqcompositefield.comboplaceholder', { fieldName : SQXSchema.compliancequest__SQX_Audit_Program__c.sObjectLabel})
            });   
            
            multiSelect = $("#pgms").data("kendoMultiSelect");

            $("#scheduler").kendoScheduler({
                editable: false,
                date: new Date(),
                views: [
                    { type: "month", selected: true },
                    "day"
                ],
                eventTemplate: $("#event-template").html(),
                allDayEventTemplate: $("#day-event-template").html(),
                timezone: "Etc/UTC",
                dataSource: {
                    data: [],
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { type: "string" },
                                name: { type: "string" },
                                title: { type: "string" },
                                start: { type: "date" },
                                end: { type: "date" }
                            }
                        }
                    }
                },
                resources: [
                    {
                        field: "id",
                        dataSource: []
                    }
                ]
            });
            var scheduler = $("#scheduler").data("kendoScheduler");

            $("#getValue").click(function() {
                if (startDateValidator.validate() && endDateValidator.validate()) {
                    generateView(true);
                }
            });

            $('#getSpreadsheetValue').click(function() {
                if (startDateValidator.validate() && endDateValidator.validate()) {
                    generateView(false);
                }
            });

            function generateView(viewType) {
                showLoading();
                clearFormError();
                var audit = new SObjectModel.compliancequest__SQX_Audit__c();
                var criteria = new SObjectModel.compliancequest__SQX_Controlled_Document__c();
                var leadAuditor = new SObjectModel.User();
                var selectedAudits = [];
                var criterionList = new Set();
                var leadAuditorList = new Set();
                var resourceDs = [];
                var audits = [];
                var criterias = [];
                var leadAuditors = [];
                var promiseList = [], p1, p2;

                var auditFilter = {};
                if( multiSelect.value().length > 0){
                    auditFilter.compliancequest__SQX_Audit_Program__c = { in: multiSelect.value() };
                }
                if( division.value().length > 0){
                    auditFilter.compliancequest__Org_Division__c = { in: division.value() };
                }
                if( businessUnit.value().length > 0){
                    auditFilter.compliancequest__Org_Business_Unit__c = { in: businessUnit.value() };
                }
                if( site.value().length > 0){
                    auditFilter.compliancequest__Org_Site__c = { in: site.value() };
                }
                if( region.value().length > 0){
                    auditFilter.compliancequest__Org_Region__c = { in: region.value() };
                }

                auditFilter = $.extend(auditFilter, {
                    or: {
                        or : {
                            or: {
                                and: {
                                    compliancequest__End_Date__c : { gt: datepicker.value() },
                                    and: {
                                        compliancequest__End_Date__c: { lt: enddatepicker.value()},
                                        Id : {ne: ''}
                                    }
                                },
                                Id : {eq: ''}
                            },
                            and: {
                                compliancequest__Start_Date__c : { gt: datepicker.value() },
                                and: {
                                    compliancequest__Start_Date__c: { lt: enddatepicker.value()},
                                    Id : {ne: ''}
                                }
                            }
                        },
                        and: {
                            compliancequest__Start_Date__c : { lte: datepicker.value() },
                            compliancequest__End_Date__c : { gte: enddatepicker.value() }
                        }
                    }
                });

                if(window.cq && window.cq.apis && window.cq.apis.callback && window.cq.apis.callback.modifyFilter) {
                    // call back modify filter if it is defined
                    auditFilter = window.cq.apis.callback.modifyFilter(auditFilter)
                }

                retrieveRecords(audit, audits, auditFilter, 0).then(function(records){

                    // if records length reaches maximum limit of 2000 then show the page message
                    if (records.length >= SF_MAX_RETRIEVAL_LIMIT) {
                        $(".sf-max-limit-info").show();
                    } else {
                        $(".sf-max-limit-info").hide();
                    }
                    // Add the results to the page
                    var paletteNumber = 0;
                    $.each(records, function() {
                        if(!recordMap[this.get('Id')]){
                            recordMap[this.get('Id')] = this;
                        }
                        if(palette[paletteNumber] === undefined){
                            paletteNumber = 0
                        }
                        selectedAudits.push({
                            'id': this.get('Id'),
                            'title': this.get('compliancequest__Title__c'),
                            'name': this.get('Name'),
                            'start': new Date(this.get('compliancequest__Start_Date__c')),
                            'end': new Date(this.get('compliancequest__End_Date__c')),
                            'color' : palette[paletteNumber]
                        });
                        resourceDs.push({
                            'value': this.get('Id'),
                            'color': palette[paletteNumber]
                        })
                        paletteNumber = paletteNumber + 1;

                        if(this.get('compliancequest__SQX_Audit_Criteria_Document__c') !== undefined) criterionList.add(this.get('compliancequest__SQX_Audit_Criteria_Document__c'));
                        if(this.get('compliancequest__SQX_Lead_Auditor__c') !== undefined) leadAuditorList.add(this.get('compliancequest__SQX_Lead_Auditor__c'));
                    });


                    if(criterionList.size){
                        var criteriaFilter = {},
                            criterionArray = [];
                        criterionList.forEach(function(e){criterionArray.push(e)});
                        criteriaFilter.Id = { in: criterionArray };

                        p1 = retrieveRecords(criteria, criterias, criteriaFilter, 0);
                        promiseList.push(p1);
                    }

                    if(leadAuditorList.size){
                        var leadAuditorFilter = {},
                            leadAuditorArray = [];
                        leadAuditorList.forEach(function(e){leadAuditorArray.push(e)});
                        leadAuditorFilter.Id = { in: leadAuditorArray };

                        p2 = retrieveRecords(leadAuditor, leadAuditors, leadAuditorFilter, 0);
                        promiseList.push(p2);
                    }

                    Promise.all(promiseList).then(function(res){
                        if(p1 !== undefined){
                            p1.then(function(records){
                                        $.each(records, function(){
                                            if(!criteriaMap[this.get('Id')]){
                                            criteriaMap[this.get('Id')] = this;
                                        }
                                        });
                            });
                        }
                        if(p2 !== undefined){
                            p2.then(function(records){
                                        $.each(records, function(){
                                            if(!leadAuditorMap[this.get('Id')]){
                                                leadAuditorMap[this.get('Id')] = this;
                                            }
                                });
                            });
                        }
                    });

                    if (viewType) {
                        $("#spreadsheet").hide();
                        $("#scheduler").show();
                        var dataSource = new kendo.data.SchedulerDataSource({
                            data: selectedAudits,
                            schema: {
                                model: {
                                    id: "id",
                                    fields: {
                                        id: { type: "string" },
                                        name: { type: "string" },
                                        title: { type: "string" },
                                        start: { type: "date" },
                                        end: { type: "date" }
                                    }
                                }
                            }
                        });
                        scheduler.setDataSource(dataSource);
                        scheduler.resources[0].dataSource.data(resourceDs);
                        scheduler.refresh();

                        $("#scheduler").kendoTooltip({
                            filter: ".pgm-template a",
                            content: kendo.template($("#popupTemplate").html()),
                            position: "top"
                        });
                    }
                    else {
                            $("#scheduler").hide();
                            $("#spreadsheet").show();
                            $("#spreadsheet").empty();

                            $("#spreadsheet").kendoSpreadsheet({
                                toolbar: false,
                                sheetsbar: false,
                                sheets: [{
                                    name: "{!JSENCODE($ObjectType.SQX_Audit__c.label)}",
                                    rows: getAuditRowsLayout(records),
                                    columnWidth: 100,
                                    frozenRows: columnMap.headerRowIndex,
                                    mergedCells: mergedCells
                                }]
                            });

                            $("#spreadsheet").kendoTooltip({
                                filter: ".k-spreadsheet-cell a",
                                position: "right",
                                content: kendo.template($("#popupTemplate").html())
                            }).data("kendoTooltip");

                        }
                    hideLoading();
                })
            }

            function getAuditRowsLayout(audits){
                var rows = [];
                    auditSchema = SQXSchema['compliancequest__SQX_Audit__c'],
                    pivotFieldValues = auditSchema.fields["compliancequest__Plan_Period__c"].additionalInfo.validValues;

                rows = rows.concat(buildHeaderRow(pivotFieldValues, '{!JSENCODE($ObjectType.compliancequest__SQX_Audit__c.fields.compliancequest__Plan_Period__c.label)}'));
                rows = rows.concat(createDataRows(audits, auditSchema));

                return rows;
            }

            function buildHeaderRow(pivotFieldValues, titleLabel){
                var fieldIndex,
                    cells = [],
                    rows = [],
                    value,
                    headerColumnIndex = columnMap.headerColumnIndex;

                rows.push({cells: [{value: titleLabel, background: "rgb(96,181,255)", color: "white", fontSize: 25, bold: true, enable: false, textAlign: "center"}], height: 40});

                for(fieldIndex = 0; fieldIndex < pivotFieldValues.length; fieldIndex++){
                    value = pivotFieldValues[fieldIndex].text || pivotFieldValues[fieldIndex].value;
                    cells.push({value: value, textAlign: 'center', enable: false, verticalAlign: 'center', background: "rgb(167,214,255)", color: "rgb(0,62,117)", fontSize: 15, bold: true});
                    headerColumnIndex[pivotFieldValues[fieldIndex].value] = fieldIndex;

                }
                rows.push({cells: cells, height: 30});

                columnMap.headerRowIndex = rows.length;

                mergedCells.push('R1C1:R1C'+Object.keys(headerColumnIndex).length);

                return rows;
            }

            function createDataRows(audits, auditSchema){
                var dataIndex,
                    columnIndex,
                    rowIndex = columnMap.headerRowIndex,
                    rows = [],
                    cells = [],
                    headerColumnIndex = columnMap.headerColumnIndex,
                    periodRowIndexMap = {},
                    periodMap = {},
                    key = '',
                    audit,
                    period,
                    recordIndex,
                    colorPaletteIndex = 0,
                    criteriaPaletteMap = {},
                    color = '',
                    newColumn,
                    auditsWithoutPlanPeriod = [];

                for(dataIndex=0; dataIndex < audits.length; dataIndex++){
                    audit = audits[dataIndex];
                    period = audit.get('compliancequest__Plan_Period__c');
                    if(period !== undefined){
                        if(periodMap[period]){
                            periodMap[period].push(audit);
                        }else{
                            periodMap[period] = [];
                            periodMap[period].push(audit);
                        }
                    } else {
                        auditsWithoutPlanPeriod.push(audit);
                    }
                }
                if (auditsWithoutPlanPeriod.length > 0) {
                    $('#panelbar').remove();
                    showWarningNotification(auditsWithoutPlanPeriod);
                }
                $.each(periodMap, function(key, value){
                    for(recordIndex=0; recordIndex<value.length; recordIndex++){
                        audit = value[recordIndex];

                        if(palette[colorPaletteIndex] === undefined){
                            colorPaletteIndex = 0
                        }
                        if(audit.get('compliancequest__SQX_Audit_Criteria_Document__c') !== undefined){
                            if(criteriaPaletteMap[audit.get('compliancequest__SQX_Audit_Criteria_Document__c')]){
                                color = criteriaPaletteMap[audit.get('compliancequest__SQX_Audit_Criteria_Document__c')];
                            }else{
                                criteriaPaletteMap[audit.get('compliancequest__SQX_Audit_Criteria_Document__c')] = palette[colorPaletteIndex];
                                color = palette[colorPaletteIndex]
                                colorPaletteIndex++;
                            }
                            rows.push({cells: [{value: audit.get('compliancequest__Title__c'), background: color, index: headerColumnIndex[key], link: _baseUrl+audit.get('Id'), color: "rgb(0,0,0)"}], index: recordIndex+rowIndex})
                        }else{
                            rows.push({cells: [{value: audit.get('compliancequest__Title__c'), index: headerColumnIndex[key], link: _baseUrl+audit.get('Id'), color: "rgb(0,0,0)"}], index: recordIndex+rowIndex})
                        }
                    }
                });

                return rows;
            }
        });
    </script>

    <style>
        .pgm-template a {
            color: #333;
            font-weight: bold;
            text-decoration: none;
        }
    
        .cq-buttons button {
            margin: 20px !important;
            text-align: center !important;
        }
    
        .k-content {
            margin-top: 1em;
        }
    
        .k-content h4 {
            margin-left: 2px;
        }
    
        .headerMargin {
            margin-top: 0.5em;
        }
    
        .sf-max-limit-info {
            background-color: #ffc;
            border-style: solid;
            border-width: 1px;
            color: #000;
            margin: 4px 0px;
            border-color: #39f;
            border-radius: 4px;
            padding: 0.95em;
            text-align: center;
        }
    
        #panelbar .k-header {
            background-color: #fff4c9;
            color: #263248;
        }
    </style>
</apex:page>