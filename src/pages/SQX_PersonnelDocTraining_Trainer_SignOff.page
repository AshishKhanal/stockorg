<apex:page action="{!recordSignOff}" standardController="SQX_Personnel_Document_Training__c" extensions="SQX_Extension_PDT_Trainer_SignOff" recordSetVar="documentTrainings" 
    lightningStylesheets="true" title="{!$Label.cq_ui_doc_training_trainer_sign_off}">
    <apex:variable var="homePageRetUrl" value="{!IF($User.UIThemeDisplayed == 'Theme4d', '/lightning/page/home', '/home/home.jsp')}" />
    <apex:pageMessages ></apex:pageMessages>
    <style>
        .errormsg {
            color: Red; 
            font-size:13px; 
            font-weight:bold; 
            margin:5px; 
            padding:10px; 
            padding-top:2px;
        }
        .centerAligned {
            text-align: center;
        }
        
        .requiredLine {
            display: inline;
            border: 1px solid #c00;
            padding-right: 1px;
            background-color: #c00;
        }
    </style>
    <apex:form >
        <apex:pageBlock mode="edit" Title="{!$Label.cq_ui_doc_training_trainer_sign_off}">
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton value="{!$Label.cq_ui_sign_off}" action="{!signOff}" rendered="{!documentTrainingsToSignOff.size > 0}" />
                <apex:commandButton value="{!$Label.cq_ui_button_cancel}" action="{!cancel}" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection columns = "1" title="{!$Label.cq_ui_doc_training_requiring_trainer_sign_off}">
                 <apex:outputPanel rendered="{!listItems.size = 0}">
                    <div class="errormsg">
                        {!$Label.cq_ui_no_document_training_selected_to_sign_off}
                    </div>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!listItems.size > 0 && documentTrainingsToSignOff.size = 0}">
                    <div class="errormsg">
                        {!$Label.cq_ui_no_document_training_requiring_sign_off}
                    </div>
                </apex:outputPanel>
                <apex:pageBlockTable value="{!documentTrainingsToSignOff}" var="dt" rendered="{!documentTrainingsToSignOff.size > 0}">
                    <apex:column styleClass="actionCol" HeaderValue="Action">
                        <apex:variable var="hasScormContent" value="{!dt.SQX_Controlled_Document__r.Is_Scorm_Content__c}" />
                        <apex:outputPanel rendered="{!NOT(hasScormContent)}">
                            <apex:outputLink target="_blank" value="{!URLFOR($Page.SQX_View_Controlled_Document, null, [id=dt.SQX_Controlled_Document__c,retURL=homePageRetUrl])}">{!$Label.cq_ui_view_content}</apex:outputLink>
                            <apex:outputText value=" | " rendered="{!dt.Can_View_And_Sign_Off__c}"/>
                            <apex:outputLink target="_parent" rendered="{!dt.Can_View_And_Sign_Off__c}"
                                             value="{!URLFOR($Page.SQX_Take_Assessment, null, [astId=dt.SQX_Assessment__c,dtId=dt.Id,psnId=dt.SQX_Personnel__c,mode='signoff',retURL=homePageRetUrl])}">
                                {!$Label.CQ_UI_View_Assessment_And_Sign_Off}
                            </apex:outputLink>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!hasScormContent}">
                            <apex:outputLink target="_blank" value="{!URLFOR($Page.SQX_View_Controlled_Document, null, [id=dt.SQX_Controlled_Document__c,retURL=homePageRetUrl])}">{!$Label.cq_ui_view_content}</apex:outputLink>
                        </apex:outputPanel>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Controlled_Document__c}" />
                    <apex:column value="{!dt.Title__c}" />
                    <apex:column headerValue="{!$ObjectType.SQX_Personnel_Document_Training__c.fields.SQX_Personnel__c.label}">
                        <apex:outputLink value="{!URLFOR($Action.SQX_Personnel__c.View, dt.SQX_Personnel__c, [retURL=''])}" target="_blank">
                            <apex:outputText value="{!dt.Personnel_Name__c}" />
                        </apex:outputLink>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Trainer__c}" />
                    <apex:column value="{!dt.Level_of_Competency__c}" style="width:50px" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Trainer_Approval_Needed__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Due_Date__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Status__c}" />
                    <apex:column value="{!dt.SQX_User_Signed_Off_By__c}" />
                    <apex:column value="{!dt.User_SignOff_Date__c}" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns = "1" title="{!$Label.CQ_UI_Document_Needing_Assessment}" rendered="{!DocumentTrrainingsRequiringAssessmentToPass.size > 0}">
                <apex:pageBlockTable value="{!DocumentTrrainingsRequiringAssessmentToPass}" var="dt">
                    <apex:column styleClass="actionCol" HeaderValue="{!$Label.cq_ui_action}" width="16%">
                        <apex:outputLink target="_parent" rendered="{!dt.Can_Take_Assessment__c}"
                                         value="{!URLFOR($Page.SQX_Assessment_Start, null, [astId=dt.SQX_Assessment__c,psnId=dt.SQX_Personnel__c,retURL=homePageRetUrl])}">
                            {!$Label.cq_ui_take_assessment}
                        </apex:outputLink>
                        <apex:outputText value=" | " rendered="{!dt.Can_Take_Assessment__c}"/>
                        <apex:outputLink target="_blank" value="{!URLFOR($Page.SQX_View_Controlled_Document, null, [id=dt.SQX_Controlled_Document__c,retURL=homePageRetUrl])}">{!$Label.cq_ui_view_content}</apex:outputLink>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Controlled_Document__c}" />
                    <apex:column headerValue="{!$ObjectType.SQX_Personnel_Document_Training__c.fields.SQX_Personnel__c.label}">
                        <apex:outputLink value="{!URLFOR($Action.SQX_Personnel__c.View, dt.SQX_Personnel__c, [retURL=''])}" target="_blank">
                            <apex:outputText value="{!dt.Personnel_Name__c}" />
                        </apex:outputLink>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Trainer__c}" />
                    <apex:column value="{!dt.Level_of_Competency__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Trainer_Approval_Needed__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Due_Date__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Status__c}" />
                    <apex:column value="{!dt.SQX_User_Signed_Off_By__c}" />
                    <apex:column value="{!dt.User_SignOff_Date__c}" />
                    <apex:column value="{!dt.SQX_Training_Approved_By__c}" />
                    <apex:column value="{!dt.Trainer_SignOff_Date__c}" />
            </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="1" title="{!$Label.cq_ui_doc_training_that_can_not_be_signed_off}" rendered="{!documentTrainingsNotRequiringSignOff.size > 0}">
                <apex:pageBlockTable value="{!documentTrainingsNotRequiringSignOff}" var="dt" style="margin-top: 15px;">
                    <apex:column styleClass="actionCol" HeaderValue="{!$Label.cq_ui_action}" width="16%">
                        <apex:outputLink target="_blank" value="{!URLFOR($Page.SQX_View_Controlled_Document, null, [id=dt.SQX_Controlled_Document__c,retURL=homePageRetUrl])}">{!$Label.cq_ui_view_content}</apex:outputLink>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Controlled_Document__c}" />
                    <apex:column value="{!dt.Title__c}" />
                    <apex:column headerValue="{!$ObjectType.SQX_Personnel_Document_Training__c.fields.SQX_Personnel__c.label}">
                        <apex:outputLink value="{!URLFOR($Action.SQX_Personnel__c.View, dt.SQX_Personnel__c, [retURL=''])}" target="_blank">
                            <apex:outputText value="{!dt.Personnel_Name__c}" />
                        </apex:outputLink>
                    </apex:column>
                    <apex:column value="{!dt.SQX_Trainer__c}" />
                    <apex:column value="{!dt.Level_of_Competency__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Trainer_Approval_Needed__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Due_Date__c}" />
                    <apex:column styleClass="centerAligned" headerClass="centerAligned" value="{!dt.Status__c}" />
                    <apex:column value="{!dt.SQX_User_Signed_Off_By__c}" />
                    <apex:column value="{!dt.User_SignOff_Date__c}" />
                    <apex:column value="{!dt.SQX_Training_Approved_By__c}" />
                    <apex:column value="{!dt.Trainer_SignOff_Date__c}" />
                </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="1" title="Sign Off Signature" rendered="{!documentTrainingsToSignOff.size > 0}">
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{!$Label.cq_ui_purpose_of_signature}" />
                    <apex:outputText value="{!$Label.sqx_ps_pdt_signing_off_document_training}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{!$Label.cq_ui_comment}" />
                    <apex:inputTextarea value="{!signingOffComment}" rows="4" style="width:70%"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="{!$Label.cq_ui_logged_in_as}" />
                    <apex:outputPanel >
                        <apex:outputText value="{!currentUserSignatureData}" label="{!$Label.cq_ui_user_name}" rendered="{!NOT(CQ_Electronic_Signature_Ask_Username)}" />
                        <apex:outputPanel rendered="{!CQ_Electronic_Signature_Ask_Username}">
                            <div class="requiredLine" />
                            <span>&nbsp;</span>
                            <apex:inputText value="{!signingOffUsername}" label="{!$Label.cq_ui_user_name}" required="{!CQ_Electronic_Signature_Ask_Username}" rendered="{!CQ_Electronic_Signature_Ask_Username}" />
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!CQ_Electronic_Signature_Enabled}" >
                    <apex:outputText value="{!$Label.cq_ui_password}" />
                    <apex:outputPanel >
                        <div class="requiredLine" />
                        <span>&nbsp;</span>
                        <apex:inputSecret value="{!signingOffPassword}" label="{!$Label.cq_ui_password}" required="true" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>