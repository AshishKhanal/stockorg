<apex:page standardController="SQX_Assessment__c" extensions="SQX_Extension_Assessment" lightningStylesheets="true">

    <apex:form id="fm">
    
        <apex:inputHidden value="{!SQX_Assessment__c.Scorm_Content_Reference__c}" id="ContentRef"/>

        <div class='errorDiv'>
            <apex:pageMessages id="pageMessages"/>
        </div>
        
        <apex:pageBlock id="pb">            
            
            <div class="cq-body">

               <div class="infoSection" >
                    <apex:pageBlockSection id="pbs1" columns="1" showHeader="false">
                        
                        <apex:pageBlockSectionItem id="pbsi1" dataStyle="max-width: 10px">
                            
                            <apex:outputLabel value="{!$ObjectType.SQX_Assessment__c.Fields.Scorm_Content_Reference__c.Label}" />
                            <apex:outputLink target="_blank" value="{!URLFOR($Action.ContentDocument.View, SQX_Assessment__c.Scorm_Content_Reference__c)}">{!AssessmentTitle}</apex:outputLink>
                            
                        </apex:pageBlockSectionItem>
                        
                        <apex:pageBlockSectionItem>
                            <apex:outputText></apex:outputText>
                            <apex:commandButton value="{!$Label.CQ_UI_Update_Content}" onclick="javascript:openUpdateMode();return false;" rendered="{!AND(HasEditAccess, NOT(SQX_Assessment__c.Is_Locked__c))}" />
                        </apex:pageBlockSectionItem>

                   </apex:pageBlockSection>
                </div>

                <div class="viewSection">
                    <apex:pageBlockSection id="pbs2" columns="1" showHeader="false">

                        <apex:pageBlockSectionItem>
                            <apex:outputPanel>
                                <apex:outputLink value="{!URLFOR($Page.SQX_eContent, null, [id=Id])}" target="_top" styleClass="btn commandLink">{!$Label.CQ_UI_Controlled_Document_View_Content}</apex:outputLink>
                            </apex:outputPanel>
                        </apex:pageBlockSectionItem>

                    </apex:pageBlockSection>
                </div>

            </div>

            
            <div class="uploadSection" style="display:none; width:100%">
               
                <compliancequest:SQX_File_Upload_Component Record="{!SQX_Assessment__c}"
                                             ContentRef="{!$Component.fm.ContentRef}"
                                             JSCallBack="sqx.enableDisableButtonsOnUpload"
                                             SupportedExtensions="zip"
                                             CustomFilter="{'filters': [{'field' : 'compliancequest__Assessment__c', 'operator' : 'eq', 'value' : ''}]}"
                                             />
                <div class="pbs-button" >
                    <apex:commandButton value="{!$Label.CQ_UI_button_save}" reRender="pageMessages" oncomplete="reloadPage()" />
                    <apex:commandButton value="{!$Label.CQ_UI_close}" oncomplete="reloadPage()" />
                </div>
            </div>
            
        </apex:pageBlock>
    
    </apex:form>
    
    
    <!-- Remote Objects to fetch content version related to the content and content document for the file picker-->
    <apex:remoteObjects >
        <apex:remoteObjectModel name="ContentVersion" fields="Id,Title,Description,FileType,FileExtension,PathOnClient,RecordTypeId,IsLatest,ContentDocumentId,compliancequest__Assessment__c"></apex:remoteObjectModel>
    </apex:remoteObjects>
    
    <script type="text/javascript">
        
    	/**
        * Switches the UI to update mode
        */
        function openUpdateMode(){
            $(".uploadSection").show();
            $(".cq-body").hide();
            $(".errorDiv").hide();
        }

        /**
        *	Switches the UI to preview mode
        */
        function openPreviewMode(){
            $(".uploadSection").hide();
            $(".cq-body").show();
        }
    
    
    	/**
        * Reloads the page unconditionally.
        */
        function reloadPage(){
            sqx.reloadInlineVF('{!JSENCODE(URLFOR($Page.SQX_Assessment_Content, null, [id = SQX_Assessment__c.Id, inline = 1]))}');
        }
    
    </script>
    
    
    <style type="text/css">

        .infoSection { float:left; width:30%; }
        .viewSection { float:left; width:70%; }

        .infoSection a{
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .errorDiv { max-height: 70px; overflow: auto; }
        
        .loaderBackground {background-color: #fbfbfb;height:100%;opacity:0.8;width:100%;position:absolute;z-index:99999;}
        .loader {position: fixed; top: 50%; left: 50%;}
        
        .btn.commandLink {padding: 4px; text-decoration: none; margin-top: 5px; display:inline-block; }
        .btn.commandLink:hover {color: auto; text-decoration: none;}

        .pbBody { max-height: 232px; overflow-y:auto; }
        .pbs-button { margin:auto; width:50%; padding:10px}
    </style>
</apex:page>