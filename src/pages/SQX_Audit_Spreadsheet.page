<apex:page standardController="SQX_Audit__c" 
           extensions="SQX_Extension_Audit" 
           showHeader="true" 
           sidebar="true"   
           standardStylesheets="true"   
           docType="html-5.0">
    
    <apex:pageMessages />

    
    
    <!-- Script block -->
    <script type="text/javascript">
    //this is a javascript object passed as a parameter to set the default value when the main record is initialized -->
        var defaultValueMap = '{!JSENCODE($CurrentPage.parameters.defaultVal)}',
        
        //the title of the page that is to be set by default
        title = '{!JSENCODE(SQX_Audit__c.Name)}',
        
        //JS to make the page work
        isNewRecord=false, //used to record whether this page is for a new record or old.
        formValidator, //the form validator that is to be used to validate the form when button is pressed.
        formRules = [], //the form rules that are to be evaluated to provide the dynamic behaviour of the form. Check the related blog to find instructions on how to configure the formRules
        
        //Miscellaneous settings could include recordtype and others
        AuditRecordTypeId = '{!JSENCODE(RecordTypes['compliancequest__SQX_Finding__c']['Audit_Finding'])}',
        
        //Instantiation of the main object in the page
        mainRecordId = '{!JSENCODE(SQX_Audit__c.Id)}',
        Audit,
        auditUpdateAccess={!UpdateAccess},
        isProviderExcel = {!$Setup.compliancequest__SQX_Custom_Settings_Public__c.compliancequest__Audit_Offline_Provider__c == "Microsoft Excel" },
        sqx = {};
    
    </script>
    
    <compliancequest:SQX_Common_Scripts extension="{!Self}" />

    <!-- Script block to dump data to the page -->
    <script type="text/x-json-data" id="completeDataJSON">{!HTMLEncode(CompleteDataJSON)}</script>
    <script type="text/x-json-data" id="changeSetDataJSON">[]</script>
    
    
    <!--
        /**
* InitializeNewMainRecord function is called by the platform, it is used to initialize the new main record.
*/
        -->
        <script type="text/javascript">
            
            function initializeNewMainRecord(newRow){
            
        }
        </script>
    
    <!-- Initialization script to initialize all the sections of the UI -->
    <script type="text/javascript">
    var initializationScript = function(jQuery, kendo, sqx, initScript){
        
        initScript.procs = [];
        
        initScript.initialize = function(){
            for(var idx = 0, length = this.procs.length; idx < length; idx++){
                try{
                    this.procs[idx].call(this);
                }
                catch(e){
                    sqx.common.error(e);
                }
            }
        }
        
        /**
        * Function to setup main tab strip
        */
        function initializeMainTabStrip() {
            var mainTS;
            
            /*
             * @description: checks if device is mobile or PC
                             if device is mobile CAPA or NC page will have panelbar
                             else CAPA or NC page will have tabstrip
            */
            if (sqx.common.isMobile()) {
                transformTabToPanel($("#mainTab"));
                $('#mainTab').children("ul").kendoPanelBar({
                    activate: function(e) {
                        kendo.init(e.item);
                    }
                });
                
                mainTS = $('#mainTab').children("ul").data("kendoPanelBar");
                
                //add conditional logic to hide/unhide tabs based on the state of the main record or any other
                //desired condition
                
                
                //set the initialTab as passed in the query string or using someother method
                if (initialTab !== "") {
                    var tabToSelect = mainTS.element.find($('#' + initialTab));
                    if (tabToSelect.length == 1) {
                        mainTS.expand(tabToSelect);
                    }
                }
                
            } else {
                $('#mainTab').kendoCQTabStrip({
                    showGridTitle: true,
                    showGridCount: true,
                    countModel: sqx.dataStore.objectCount
                });
                
                mainTS = $('#mainTab').data("kendoCQTabStrip");
                
                //add conditional logic to hide/unhide tabs based on the state of the main record or any other
                //desired condition
                
                kendo.init($(mainTS.contentElement(0)));
                
                //set the initialTab as passed in the query string or using someother method
                if (initialTab !== "") {
                    var tabToSelect = mainTS.tabGroup.find($('#' + initialTab)).eq(0);
                    if (tabToSelect.length == 1) {
                        mainTS.activateTab(tabToSelect);
                        
                        //expand the provided row in tab.
                        if (expandRecord !== "") {
                            
                            //locate the main tab
                            var contentIndex = tabToSelect.index(),
                                contentElem = null,
                                tsGrids = null,
                                tblIndex, tblLength,
                                matchingRecord = null,
                                tableFor;
                            
                            if (contentIndex != -1) {
                                //if content index is found
                                contentElem = $(mainTS.contentElement(contentIndex));
                                
                                //find all grids in tab strip
                                tsGrids = contentElem.find('[data-role="cqgrid"]');
                                
                                for (tblIndex = 0, tblLength = tsGrids.length; tblIndex < tblLength; tblIndex++) {
                                    tableFor = tsGrids.eq(tblIndex).data('kendoCQGrid').dataSource.options.schema.model.prototype.sObjectName;
                                    if (tableFor === expandRecordType) {
                                        tsGrids.eq(tblIndex).data('kendoCQGrid').expandRecord(expandRecord, true);
                                        break;
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
            window.mainTS = mainTS; //assign mainTS outside the scope so that it can be used in scripts controlling next previous button.
        }
        
        
        /**
        * function to initalize the kendo validator and other validation logic into the main tab strip
        */
        function initializeValidatorInTabs(){
            formValidator = $("#mainTab").kendoValidator({}).data('kendoValidator');
        }
        
        
        /**
        * Function to initialize the command section
        */
        function initializeCommandSection() {

            commandRules.enableMSExcelCheckinSave =function() {
                // SQX-5529: always show SAVE/CHECKIN BUTTON when audit is checked out using MS EXCEL
                return auditUpdateAccess && Audit.compliancequest__Audit_Checked_Out__c && isProviderExcel;
            }
            commandRules.enableSave = function() {
                // SQX-5529: always show SAVE/CHECKIN BUTTON when audit is checked out using GOOGLE SHEETS (i.e. not ms excel)
                return auditUpdateAccess && Audit.compliancequest__Audit_Checked_Out__c && !isProviderExcel;
            }
            commandRules.enableRapidAuditSave = function() {
                // SQX-5529: always show SAVE RECORD BUTTON when audit is NOT CHECKED OUT and is in progress stage
                return auditUpdateAccess && !Audit.compliancequest__Audit_Checked_Out__c && Audit.isStageIn_Progress();
            }
            commandRules.enableCancel = function() {return this.enableMSExcelCheckinSave() || this.enableSave() || this.enableRapidAuditSave();}
            
            kendo.bind($('.commandSection'), commandRules);
            
            $('body').on('change', function (e) {commandRules.set('isDirty',true)});
        }
        
        /**
        * Function to initialize the header section of the page
        */
        function initializeHeaderSection(){
            setupFlowStates();//initialize states for change's flow indicator
            kendo.bind($("#headerSection"), mainRecord);
        }
        
        //push the functions as required to initialize the main tab strip
        initScript.procs.push(initializeMainTabStrip); //Initialize main tab strip
        initScript.procs.push(initializeCommandSection);
        initScript.procs.push(initializeHeaderSection);
        initScript.procs.push(initializeValidatorInTabs);
        
        return initScript;
        
    }(jQuery, kendo, sqx, initializationScript || {} );
    </script>
    
    <!--
    /**
    * Initialize function is called by the platform, when the basic environment setup such as datastore has been
    * initialized successfully and now the UI needs to be initialized.
    */
    -->
    <script type="text/javascript">
    function initialize(){
        Audit = mainRecord;
        recordAccess = sqx.dataStore.userRecordAccess;
        setupWIP(currentUser.Id);
        //initialize everything
        if (initializationScript) {
            initializationScript.initialize();
        }
    }

    function enableAttachment(data) {
        var parent= sqx.dataStore.getObjectWithId(data.ParentId);
        if(parent && parent.isLocked!=undefined && parent.isLocked())
            return false;
        return true;
    }
    
    var securityCorrections = new sqx.SchemaConfiguration({
        compliancequest__SQX_Audit__c : function(schema) {
            
            schema.canDelete = function(){
                return securityCorrections._canDelete.call(this) && Audit.isStageIn_Progress();
            }
            
            schema.canEdit = function(){
                return securityCorrections._canEdit.call(this) && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }

            schema.canAddChildren = function(childName){
                return securityCorrections._canEdit.call(this) && (!Audit.isClosedOrHigher() && !Audit.isComplete());
            }
        
            schema.IsLocked = function () {
                return !schema.canEdit();
            };
        },
        compliancequest__SQX_Audit_Checklist__c : function(schema) {
            schema.canDelete = function(){
                return securityCorrections._canDelete.call(this) && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.canEdit = function(){
                return securityCorrections._canEdit.call(this)  && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.IsLocked = function () {
                return !schema.canEdit();
            };

            schema.sort = [
                { "field": "compliancequest__Section__c", "dir": "desc" }
            ];
        },
        compliancequest__SQX_Finding__c : function(schema) {
            schema.canDelete = function(){
                return securityCorrections._canDelete.call(this) && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.canEdit = function(){
                return securityCorrections._canEdit.call(this)  && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.IsLocked = function () {
                return !schema.canEdit();
            };
        },
        Attachment : function(schema) {
            schema.canDelete = function(){
                return securityCorrections._canDelete.call(this) && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.canEdit = function(){
                return securityCorrections._canEdit.call(this)  && (Audit.isStageIn_Progress() || Audit.isStageScheduled() || Audit.isStageConfirmed());
            }
            
            schema.IsLocked = function () {
                return !schema.canEdit();
            };
        }
    }),
        schemaCorrections = new sqx.SchemaConfiguration({
            compliancequest__SQX_Audit__c  : function(schema){
                
                this._pickListValuesToFunction(schema, 'compliancequest__Status__c', '', true);
                this._pickListValuesToFunction(schema, 'compliancequest__Stage__c', 'Stage', true);

                schema.fields['Remove_Spreadsheet__c']={
                          "validation": {
                          },
                          "type": "boolean",
                          "nullable": false,
                          "name": "Remove Spreadsheet",
                          "editable": true,
                          "additionalInfo": {
                              "apiname": "Remove_Spreadsheet__c",
                              trackChanges: false
                          }
                      };
                    
                schema.fields.compliancequest__Event_Id__c.editable = false;
                schema.fields.compliancequest__Audit_Checked_Out__c.editable = false;
                schema.fields.compliancequest__Audit_Program_Calendar_Id__c.editable = false;
                schema.fields.compliancequest__Audit_Program_Status__c.editable = false;
                schema.fields.compliancequest__InApprovalReportCount__c.editable = false;
            },
            
            compliancequest__SQX_Audit_Checklist__c : function(schema){
                
                schema.fields.compliancequest__Objective__c.editable = true;
                schema.fields.compliancequest__Topic__c.editable = true;
                schema.fields.compliancequest__SQX_Audit__c.editable = true;
                schema.fields.compliancequest__Result_Type__c.nullable = true;
                schema.fields.compliancequest__Result_Type__c.validation.required = true;
                schema.fields.compliancequest__Result__c.nullable = true;
                schema.fields.compliancequest__SQX_Result_Value__c.additionalInfo.dependsOn = 'compliancequest__SQX_Result_Type__c';
                schema.fields.compliancequest__SQX_Result_Value__c.additionalInfo.dependsOnField = 'compliancequest__SQX_Result_Type__c';
            },
            compliancequest__SQX_Finding__c : function(schema) {
                schema.fields.RecordTypeId.defaultValue = AuditRecordTypeId;
                schema.fields.compliancequest__SQX_Audit__c.editable = true;
                schema.fields.compliancequest__Title__c.validation.required = true;
                schema.fields.compliancequest__Response_Required__c.defaultValue = false;
                schema.fields.compliancequest__Response_Required__c.nullable = false;
                schema.fields.compliancequest__Investigation_Required__c.defaultValue = false;
                schema.fields.compliancequest__Investigation_Required__c.nullable = false;
                schema.fields.compliancequest__Containment_Required__c.defaultValue = false;
                schema.fields.compliancequest__Containment_Required__c.nullable = false;

                schema.fields.compliancequest__CAPA_Coordinator_Name__c.editable = false;
                schema.fields.compliancequest__Effectiveness_Review_Required__c.editable = false;
                schema.fields.compliancequest__Is_Primary_Finding__c.editable = false;
                schema.fields.compliancequest__Overdue_Investigation_Days__c.editable = false;
                schema.fields.compliancequest__Overdue_Response_Days__c.editable = false;
                schema.fields.compliancequest__Risk_Level__c.editable = false;
                schema.fields.compliancequest__SQX_Part_Name__c.editable = false;
                schema.fields.compliancequest__SQX_Service_Name__c.editable = false;
                schema.fields.compliancequest__Total_Impacted_Lot_Quantity__c.editable = false;
                schema.fields.compliancequest__Total_Quantity_Disposed__c.editable = false;
                schema.fields.compliancequest__always_1__c.editable = false;
                schema.fields.compliancequest__Done_Responding__c.defaultValue = true;
            },
            compliancequest__SQX_Objective_Evidence__c : function(schema) {
                schema.fields.Name.validation.required = true;
                schema.fields.compliancequest__SQX_Audit__c.editable = true;
                schema.fields.compliancequest__SQX_Audit_Checklist__c.editable = true; //ensures that objective evidence can be created.
            },
            compliancequest__SQX_Checklist_Target__c: function(schema) {

                schema.fields.compliancequest__SQX_Audit_Checklist__c.editable = true;

                schema.concatenatedAuditTarget= function(){
                    var retVal = '';
                    
                    var auditTarget = sqx.dataStore.getObjectWithId(this.compliancequest__SQX_Audit_Target__c);
                    

                    if(auditTarget.compliancequest__Sample__c){
                        retVal += (auditTarget.compliancequest__Sample__c + ' > ');
                    }

                    if(auditTarget.compliancequest__SQX_Department__r){
                        retVal += (auditTarget.compliancequest__SQX_Department__r.Name + ' > ');
                    }

                    if(auditTarget.compliancequest__SQX_Product__r){
                        retVal += (auditTarget.compliancequest__SQX_Product__r.Name + ' > ');
                    }

                    if(auditTarget.compliancequest__Org_Division__c){
                        retVal += (auditTarget.compliancequest__Org_Division__c + ' > ');
                    }

                    if(auditTarget.compliancequest__Org_Business_Unit__c){
                        retVal += (auditTarget.compliancequest__Org_Business_Unit__c + ' > ');
                    }

                    if(auditTarget.compliancequest__Org_Region__c){
                        retVal += (auditTarget.compliancequest__Org_Region__c + ' > ');
                    }

                    if(auditTarget.compliancequest__Org_Site__c){
                        retVal += (auditTarget.compliancequest__Org_Site__c + ' > ');
                    }

                    if(auditTarget.compliancequest__Org_Site__c){
                        retVal += (auditTarget.compliancequest__Org_Site__c + ' > ');
                    }

                    if(auditTarget.compliancequest__SQX_Process__r){
                        retVal += (auditTarget.compliancequest__SQX_Process__r.Name + ' > ');
                    }
                    
                    retVal = retVal.slice(0, -2);
                    return retVal;
                }
            }
        }),
        postSchemaCorrections = new sqx.SchemaConfiguration({
        });
    </script>
    
    <!-- Adjust the SQXSchema to correct the schema and add features that are required to make the form functional -->
    <script type="text/javascript">
    function preAdjustSchema(){
        
        //functions to ensure that edit and delete access on record are available in the Salesforce org.
        var hasRecordEditAccess = function (){
            var hasAccess = true,
                rac = recordAccess && recordAccess[this.Id];
            
            if(rac){
                hasAccess = rac.HasEditAccess || rac.MaxAccessLevel == 'All';
            }
            
            return hasAccess
        },
            hasRecordDeleteAccess = function(){
                var hasAccess = true,
                    rac = recordAccess && recordAccess[this.Id];
                
                if(rac){
                    hasAccess = rac.HasDeleteAccess || rac.MaxAccessLevel == 'All';
                }
                
                return hasAccess
            },
            currentObject = null,
            customerCorrections = window.customerCorrections || {};
        
        for(var object in SQXSchema){
            currentObject = SQXSchema[object];
            
            if(securityMatrix[object]){
                currentObject.ObjectAccess = securityMatrix[object];
            }
            
            
            if(schemaCorrections[object]){
                try{
                    schemaCorrections[object].call(schemaCorrections,currentObject);
                }
                catch(ex){
                    sqx.common.error('Error occurred while initializing object ' + object, ex);
                }
            }
            
            //add security access check on all functions
            currentObject.hasRecordEditAccess = hasRecordEditAccess;
            currentObject.hasRecordDeleteAccess = hasRecordDeleteAccess;
            
            //set can delete to common delete and edit function
            currentObject.canDelete = securityCorrections._canDelete;
            currentObject.canEdit = securityCorrections._canEdit;
            
            //add security rules this should come after common edit because it allows user to override the basic
            //rules.
            if(securityCorrections[object]){
                try{
                    securityCorrections[object].call(securityCorrections, currentObject);
                }
                catch(ex){
                    sqx.common.error('Error occurred while add security rules to object ' + object, ex);
                }
            }
            //add customer rules overrides for the object
            //INJECTION POINT: for customer defined security rules.
            if(customerCorrections[object]){
                try{
                    customerCorrections[object].call(null, currentObject);
                }
                catch(ex){
                    sqx.common.error('Error occurred while adding customer rules to object ' + object, ex);
                }
            }
        }
        
        //custom security rule injection
        if (window.customSecurityRules && $.isFunction(window.customSecurityRules)){
            customSecurityRules();
        }
    }
    
    /**
    * allows schema to be adjusted after common schema correction is done. This allows the user to add
    *
    */
    function postAdjustSchema(schema){
        for(var object in SQXSchema){
            currentObject = SQXSchema[object];
            
            if(postSchemaCorrections[object]){
                postSchemaCorrections[object].call(null, currentObject);
            }
        }
    }
    
    function redirectToAudit(e){
        var url =  '{!JSENCODE(URLFOR($Page.SQX_Audit))}';
        url = cq.insertParamtoURL('id', mainRecord.Id, url);
        navigateToURL(url);
    }
    
    </script>
    
    <apex:composition template="SQX_Audit_Default_Template">  
        <apex:define name="customerScriptBlock">
            <apex:composition template="{!CustomerScriptPageLayout}">
            </apex:composition>
        </apex:define>
        
        <apex:define name="headerBlock">
            <apex:composition template="{!HeaderPageLayout}">
            </apex:composition>
        </apex:define>
        
        <apex:define name="detailBlock">
            <apex:composition template="{!DetailPageLayout}">
            </apex:composition>
        </apex:define>
        
        <apex:define name="commandBlock">
            <apex:composition template="{!CommandPageLayout}">
            </apex:composition>
        </apex:define>
    </apex:composition>
    
    
    <apex:insert name="customerScriptBlock" />
    <br />
    <apex:insert name="headerBlock" />
    <br />
    <apex:insert name="detailBlock" />
    <br />
    <apex:insert name="commandBlock" />
    
    
    <!--  Page Specific code for various event go here -->
    <script type="text/javascript">
    (function(){
        var evtHdlr = {
            audit : {
                saveNeedsEsig : function(e){
                    return true;
                },
                _deleteSpreadsheet : function(mainRecordId, electronicSignatureData, params){
                    UIController.deleteOfflineDoc(mainRecordId, electronicSignatureData, params, function(e,s){
                        if(s.status){
                            commandRules.set('isDirty',false);
                            var url = '{!JSENCODE(URLFOR($Page.SQX_Audit))}';
                            url = cq.insertParamtoURL('id', mainRecord.Id, url);
                            navigateToURL(url);
                            return;
                        }else{
                            hideLoading();
                            setFormError(s.message, closeWindow.element);
                            closeWindow.close();

                            var resubmitConfirm = confirm('{!JSENCODE($Label.compliancequest__CQ_UI_Spreadsheet_Deletion_unsuccessful_due_to_errors)}: \n'+ s.message + '\n' + '{!JSENCODE($Label.compliancequest__CQ_UI_Do_you_want_to_retry)}');
                            if(resubmitConfirm){
                                sqx.evtHdlr.audit._deleteSpreadsheet(mainRecord.Id, electronicSignatureData, params);
                            }else{
                                commandRules.set('isDirty',false);
                                var url = '{!JSENCODE(URLFOR($Page.SQX_Audit))}';
                                url = cq.insertParamtoURL('id', mainRecord.Id, url);
                                navigateToURL(url);
                                return;
                            }
                        }
                    });

                },
                saveAuditSpreadsheet : function(e){
                    var sender = $(e.currentTarget),
                        closeWindow = sender.parents('[data-role="cqesigwindow"]').data('kendoCQESigWindow'),
                        electronicSignatureData = null,
                        params;

                    if(closeWindow.validate()){
                        showLoading();

                        electronicSignatureData = closeWindow.getEsigData();
                        params = closeWindow._getDefaultParams(e.currentTarget);
                        params.jsonAttachmentId = getUrlParameter('jsonAttachmentId','');
                        
                        closeWindow.submitWindowWith(
                            params, 
                            function(){
                                    if(Audit.Remove_Spreadsheet__c === true){
                                        sqx.evtHdlr.audit._deleteSpreadsheet(mainRecord.Id, electronicSignatureData, params);
                                    } else {
                                        commandRules.set('isDirty',false);
                                        var url = '{!JSENCODE(URLFOR($Page.SQX_Audit))}';
                                        url = cq.insertParamtoURL('id', mainRecord.Id, url);
                                        navigateToURL(url);
                                        return;
                                    }
                            }
                        );
                    }
                },
                auditAttachment : function (e){
                    var editLink = $(e.target),
                        grid = editLink.parents('[data-role="cqgridbase"]').data('kendoCQGridBase'),
                        row = editLink.parents('tr[data-uid]'),
                        filesGrid,
                        rowModel;

                    rowModel = sqx.dataStore.getObjectWithUidFor(grid.dataSource.options.schema.model.fn.sObjectName, row.data('uid'));
                    if(rowModel) {
                        grid.expandRow(row);
                        filesGrid = row.next().find('[data-relatedlist="ContentDocumentLinks"]');
                        if(filesGrid.length > 0 && filesGrid.eq(0).data('kendoCQGrid').dataSource.options.parentModel.Id === rowModel.Id){
                            filesGrid.eq(0).find('.addFiles').click();
                        }
                        else {
                            row.next().find('[data-role="cqgridbase"]').eq(0).data('kendoCQGridBase').addRow();
                        }

                        e.preventDefault();
                    }
                }
                
            },
            syncObjectiveEvidence:{
                withAudit : function(e){
                    //set the audit's id in Objective Evidence
                    var model= e.model;
                    
                    model.set('compliancequest__SQX_Audit__c',Audit.Id);
                    model.set('compliancequest__SQX_Audit_Checklist__r', {Id: e.sender.options.parentModel.Id, Name: e.sender.options.parentModel.Name});
                    Audit.relatedList('compliancequest__SQX_Objective_Evidences__r').read();
                }
            },
            syncFinding:{
                withAudit : function(e){
                    //set the audit's id in Objective Evidence
                    var model= e.model;
                    
                    model.set('compliancequest__SQX_Audit__c',Audit.Id);
                    model.set('compliancequest__SQX_Audit_Checklist__r', {Id: e.sender.options.parentModel.Id, Name: e.sender.options.parentModel.Name});
                    Audit.relatedList('compliancequest__SQX_Findings__r').read();
                }
            },
            finding: {
                
                // Initialize Finding with default values from Audit 
                initializeRecord:function(e){
                    var model= e.model;
                    if(model.isNew()){
                        // Assign Auditee Contact as default assignee
                        if(Audit.compliancequest__SQX_Auditee_Contact__c != null){
                            model.set('compliancequest__SQX_Assignee__r', Audit.compliancequest__SQX_Auditee_Contact__r);
                        }

                        // Set default values (Division, Business Unit, Site and Region) from Audit 
                        if(Audit.compliancequest__Org_Division__c != null){
                            model.set('compliancequest__Org_Division__c', Audit.compliancequest__Org_Division__c);
                        }
                        if(Audit.compliancequest__Org_Business_Unit__c != null){
                            model.set('compliancequest__Org_Business_Unit__c', Audit.compliancequest__Org_Business_Unit__c);
                        }
                        if(Audit.compliancequest__Org_Site__c != null){
                            model.set('compliancequest__Org_Site__c', Audit.compliancequest__Org_Site__c);
                        }
                        if(Audit.compliancequest__Org_Region__c != null){
                            model.set('compliancequest__Org_Region__c', Audit.compliancequest__Org_Region__c);
                        }
                    }
                }
            }
            
        };
            
            sqx.evtHdlr = evtHdlr;
            
            
        })();
        
        /**
        * this function setsup the states of flow indicator
        */
        function setupFlowStates(){
            
        }
        
        $(function(){
            /** Wire the event handlers for the buttons */
            $(document).on('click', '.cqActionButton', sqx._defaultActionHdlr);  
            
        });
        
        </script>
    
    <script type="text/javascript">
    $(document).ready(function() {
        
        if(Audit.compliancequest__Audit_Checked_Out__c){
            showLoading();
            var attachmentId = '{!JSENCODE($CurrentPage.parameters.jsonAttachmentId)}';
            var auditCheckin = new sqx.spreadsheet.GSheet(UIController, mainRecord)

            auditCheckin.setAliases(checkInAliases);

            auditCheckin.setUpdateFields(checkInWhiteList);

            auditCheckin.setProtectedFields(checkInProtectedFields);
            
            auditCheckin.checkin(attachmentId)
                        .then(function(data) { hideLoading(); })
                        ['catch'](function(error) { console.error(error); hideLoading() });
        }
    });
    </script>
    
    
</apex:page>