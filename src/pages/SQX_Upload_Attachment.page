<apex:page sidebar="false" showHeader="false" standardStylesheets="false"
    applyHtmlTag="false" applyBodyTag="false" controller="SQX_Controller_Upload_Attachment" >
    <html>
    
    <head>
        <title>Async upload form</title>
        <style type="text/css">
            .k-filename {max-width:12.5em}
            
            <!-- override default styles -->
            .section1col .formLabel { width: 80px;}
            html {overflow-y : auto; }
            body { background-color: white; font-size: 0.75em;}
            .formItem:hover { background-color: rgba(255, 255, 255, 0) !important; }
        </style>
    </head>
    <body onload="loaded()">
        <apex:stylesheet value="{!URLFOR($Resource.cqUI, 'styles/cqui.min.css')}" />

        <apex:includescript value="{!URLFOR($Resource.cqUI, 'scripts/jquery-1.9.1.min.js')}" />
        <apex:includescript value="{!URLFOR($Resource.cqUI, 'scripts/kendo/kendo.web.min.js')}" />
        
        <script type="text/javascript" src="{!URLFOR($Resource.cqUI, 'scripts/sqx/cq.min.js')}"></script>
        
         <apex:pageMessages ></apex:pageMessages>
         
        <apex:form id="myForm" style="display:none">
            <div class="section1col">
                <div class="formItem">
                    <div class="formLabel">
                        <label for="{!$Component.myForm.myDescription}">{!$Label.CQ_UI_Description}</label>
                    </div>
                    <div class="formField">
                        <apex:inputTextarea id="myDescription" rows="4" style="width:550px;" value="{!attachment.description}"/> 
                    </div>
                </div>
                <div class="formItem">
                    <div class="formLabel">
                        <label for="{!$Component.myForm.myUploader}">{!$Label.CQ_UI_File}</label>
                        <br/>
                        <!-- move style out-->
                        <p style="font-weight:bold; font-size:0.9em;">{!$Label.CQ_UI_File_Max_size}</p>
                    </div>
                    <div class="formField cq-field-required" style="width:250px;">
                        <apex:inputFile id="myUploader" value="{!attachment.body}" filename="{!attachment.name}"></apex:inputFile>
                        <span class="k-invalid-msg" id="fileError"></span>
                    </div>
                </div>
                <div class="clearFix">
                </div>
            </div>
            <div style="text-align:center">
                <apex:commandButton action="{!upload}" id="Submit" value="Upload" onclick="uploadBegan()"/>
            </div>
            
            <input type="hidden" id="parentID" value="" name="parentID" />
        </apex:form>
        <input type="file" id="tempFile" style="display:none;" />
        
        
        <script type="text/javascript">
            var cq = window.cq || {};
            var fileSelected = false;
            var descriptionChanged = false;
            var files = {!lastUpload};
            var recordid = '{!JSEncode($CurrentPage.parameters.recordid)}';
            var parentid = '{!JSEncode($CurrentPage.parameters.parentid)}';
            var uuid = '{!JSEncode($CurrentPage.parameters.uid)}';
            var attachtoparent = '{!JSEncode($CurrentPage.parameters.attachtoparent)}';

            
            var SQXSchema = {
                Attachment: {
                    id : 'Id',
                    fields: {
                        Description: {editable: true},
                        Id: {editable: false}
                    }
                }
            };
            
            $(document).ready(function(){
                var uploadControl = $(document.getElementById("{!$Component.myForm.myUploader}"));
                $(document.getElementById("{!$Component.myForm.Submit}")).hide();
                $("#fileError").attr('data-for', uploadControl.attr('name'));
                
                //uploadControl.attr('required','required');
                uploadControl.attr('data-required-msg','{!JSEncode($Label.CQ_UI_File_is_required)}');
                uploadControl.kendoUpload({
                    multiple: false,
                    localization: {
                        select: '{!JSEncode($Label.CQ_UI_Select_a_file)}'
                    },
                    select : function(e){
                        var fileName = e.files[0].name
                        callBackParent('ItemChange', [{field: 'Name', value: fileName }]);
                        
                        
                        fileSelected = true;
                        ignoreValidation = false;
                    },
                    remove : function(e){
                        fileSelected = false;
                    },
                    //files: initialFiles //doesn't work for async false
                });
                

                
                $(document).kendoValidator({
                    rules: {
                        fileIsRequired: function(input){
                            
                            if(input.attr('id') == '{!$Component.myForm.myUploader}'){
                                
                                return files.length == 1 || fileSelected;
                            }
                        
                            return true;
                        }
                    },
                    messages: {
                        fileIsRequired : '{!JSEncode($Label.CQ_UI_File_is_required)}'
                    },
                    validateOnBlur : false
                });
                

                //always show the background
                $(".k-widget").removeClass("k-upload-empty");
                
                $(document.getElementById("{!$Component.myForm.myDescription}")).bind('change', notifyDescriptionChange);
                
                $(document.getElementById("{!$Component.myForm}")).show();
            });
            
            function validateContent(){
                return $(document).data('kendoValidator').validate();
            }
            
            function startUpload(){
                if($(document).data('kendoValidator').validate()){
                    document.getElementById("{!$Component.myForm.Submit}").click();
                }
            }

            
            function startUploadAsync(fileInput, fileDescription){
                var fileUploadNode = document.getElementById("{!$Component.myForm.myUploader}");
                fileUploadNode.removeAttribute('id');
                fileInput.setAttribute('id', "{!$Component.myForm.myUploader}");
                fileInput.setAttribute('name', fileUploadNode.getAttribute('name'));
                fileUploadNode.parentElement.removeChild(fileUploadNode);
                
                document.getElementById("{!$Component.myForm}").appendChild(fileInput);
                document.getElementById("{!$Component.myForm.myDescription}").value = fileDescription;
                
                document.getElementById("{!$Component.myForm.Submit}").click();
            }
            
            function uploadBegan(){
                callBackParent('In Progress');

            }
            
            function loaded(){
            
                var attachmentID = '{!JSENCODE(AttachmentID)}';
                if(attachmentID != ''){
                    var parentId = attachtoparent.length > 0 ? parentid : uuid;
                    var data = { Id: attachmentID, Name: '{!JSENCODE(lastUploadFileName)}', Size: '{!lastUploadFileSize}', ParentId : parentId};
                    callBackParent('Completed', data);
                }
                
                var error = '{!JSENCODE(ErrorMessage)}';
                if(error != ''){
                    callBackParent('Error Occurred', error);
                }
            }
            
            function callBackParent(status, data){
                var callBack = '{!JSEncode($CurrentPage.parameters.callback)}';
                if(this.parent != null && this.parent.window[callBack] != undefined){
                    
                    this.parent.window[callBack](status,recordid, data);
                }
            }
            
            function notifyDescriptionChange(sender){
                descriptionChanged = true;
                callBackParent('ItemChange', [{field: 'Description', value: $(sender.target).val() }]);
                
            }
            

        </script>
        
        
    </body>
    </html>
</apex:page>