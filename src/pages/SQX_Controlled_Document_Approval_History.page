<apex:page standardController="SQX_Controlled_Document__c" extensions="SQX_Extension_Controlled_Document" lightningStylesheets="true">
    <apex:includeScript value="{!URLFOR($Resource.cqUI, '/scripts/jquery-1.9.1.min.js')}"/>
    <apex:variable var="recallItemId" value="{!pendingApprovalItemIdToRecall}" />
    <apex:variable var="docUrl" value="{!URLFOR($Action.SQX_Controlled_Document__c.View, SQX_Controlled_Document__c.Id )}"/>
    <apex:pageBlock title="{!$Label.CQ_UI_Controlled_Document_Approval_History}">
        <apex:pageBlockButtons location="top">
            <apex:outputPanel rendered="{!recallItemId == null}">
                <input type="button" class="btn" value="{!$Label.CQ_UI_Submit_For_Approval}" onclick="submitForApproval();"></input>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!recallItemId != null}">
                <input type="button" class="btn" value="{!$Label.CQ_UI_Recall_Approval_Request}" onclick="redirectToURL('/{!recallItemId}/e?et=REMOVE&retURL={!SQX_Controlled_Document__c.Id}');"></input>
            </apex:outputPanel>
        </apex:pageBlockButtons>
        <div style="width: 100%; overflow-y: scroll; overflow-x:auto;" id="docApprovalHistory">
            <apex:pageBlockTable var="item" value="{!ApprovalHistoryItems}"  rendered="{!ApprovalHistoryItems.size > 0}" >
                <apex:column headerValue="Action" >
                    <apex:outputPanel rendered="{!item.isPending && ApprovalHistoryItemsPermission[item.Id].HasEditAccess}">
                        <apex:outputLink value="/{!item.Id}/e?et=REASSIGN&retURL={!SQX_Controlled_Document__c.Id}" target="_parent">{!$Label.CQ_UI_Reassign}</apex:outputLink>
                        <apex:outputText value=" | " />
                        <apex:outputLink value="{!URLFOR($Page.SQX_Controlled_Document_Approval, null, [id = SQX_Controlled_Document__c.Id, reqId = item.Id, retURL = docUrl])}" target="_parent">{!$Label.CQ_UI_Approve_Reject}</apex:outputLink>
                    </apex:outputPanel>
                </apex:column>
                <apex:column value="{!item.CreatedDate}" headerValue="Date" />
                <apex:column value="{!item.StepStatus}" />
                <apex:column headerValue="Assigned To">
                    <apex:outputLink value="/{!item.OriginalActorId}" target="_parent">{!item.OriginalActor.Name}</apex:outputLink>
                </apex:column>
                <apex:column headerValue="Actual Approver">
                    <apex:outputLink value="/{!item.ActorId}" target="_parent">{!item.Actor.Name}</apex:outputLink>
                </apex:column>
                <apex:column value="{!item.Comments}" />
            </apex:pageBlockTable>
            <apex:pageBlockSection rendered="{!ApprovalHistoryItems.size == 0}" columns="1">
                <p style="padding: 5px 0px; border-width: 1px 0; border-top-color: #4a4a56; border-bottom-color:#d4dadc; border-style:solid;">{!$Label.CQ_UI_No_records_to_display}</p>
            </apex:pageBlockSection>
        </div>
    </apex:pageBlock>    

    <apex:form rendered="{!SQX_Controlled_Document__c.SQX_Approval_Matrix__c != null || ApprovalDocumentSteps.size > 0 || IsStandardDoc}">
        <apex:pageBlock Title="{!$Label.CQ_UI_Controlled_Document_Approval_Steps}">

            <apex:pageBlockButtons location="top" >
                <apex:outputLink value="{!URLFOR($Page.SQX_CDoc_AppStep, null, [docId = SQX_Controlled_Document__c.Id ])}" target="_parent"
                                 rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.createable}" 
                                 styleClass="btn"
                                 style="text-decoration:none;padding:4px;">{!$Label.CQ_UI_New_Controlled_Document_Approval}</apex:outputLink>
            </apex:pageBlockButtons>
            
            <apex:pageMessages ></apex:pageMessages>

            <div style="width: 100%; overflow-y: scroll; overflow-x:auto;" id="docApprovalStep">     
                <apex:pageBlockTable var="docStep" value="{!ApprovalDocumentSteps}" rendered="{!ApprovalDocumentSteps.size>0}">
                    <apex:column width="10%">
                        <apex:facet name="header">{!$Label.CQ_UI_Action}</apex:facet>

                            <apex:outputLink target="_parent" rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.updateable}"
                                             value="{!URLFOR($Page.SQX_CDoc_AppStep, null,  [id = docStep.Id])}"
                                             >{!$Label.CQ_UI_Edit}</apex:outputLink> &nbsp; | &nbsp;  
                        
                            <apex:commandLink action="{!deleteApprovalStep}"
                                              target="_parent"
                                                value="{!$Label.CQ_UI_Del}"
                                                rendered="{!$ObjectType.SQX_Controlled_Document_Approval__c.deletable}" 
                                                onclick="if (!confirm('Are you sure?')) return false;" >
                            <apex:param name="stepId" value="{!docStep.id}"/>
                        </apex:commandLink>
                    </apex:column>

                    <apex:column value="{!docStep.Step__c}"/>

                    <apex:column value="{!docStep.SQX_Job_Function__c}"/> 

                    <apex:column value="{!docStep.SQX_User__c}"/>

                </apex:pageBlockTable>
                <apex:pageBlockSection rendered="{!ApprovalDocumentSteps.size == 0}" columns="1">
                    <p style="padding: 5px 0px; border-width: 1px 0; border-top-color: #4a4a56; border-bottom-color:#d4dadc; border-style:solid;">{!$Label.CQ_UI_No_Controlled_Document_Approval_to_this_document}</p>
                </apex:pageBlockSection>
            </div>
        </apex:pageBlock>
    </apex:form>
        
    <script type="text/javascript">
        var headerSize = $(".pbHeader").height(),
            gridHeight = $(window).height() - headerSize - 20;
        
        if({!SQX_Controlled_Document__c.SQX_Approval_Matrix__c != null || ApprovalDocumentSteps.size > 0 || IsStandardDoc}){
            gridHeight = gridHeight / 2 - 40;
        }
        
        if({!ApprovalHistoryItems.size != 0}){
            $("#docApprovalHistory").css('height', gridHeight + 'px');
        }

        if({!ApprovalDocumentSteps.size != 0}){
            $("#docApprovalStep").css('height', gridHeight + 'px');
        }
        
        function submitForApproval() {
            var confirmMsg = '{!$Label.CQ_UI_Aproval_Confirmation}';
            if ((Modal.confirm && Modal.confirm(confirmMsg)) || (!Modal.confirm && window.confirm(confirmMsg))) {
                redirectToURL("{!IF(ISNULL(Id), '', URLFOR($Action.SQX_Controlled_Document__c.Submit, Id, [retURL = SQX_Controlled_Document__c.Id]))}");
            }
        }
        
        function redirectToURL(url) {
            window.open(url, '_parent');
        }
    </script>  
</apex:page>