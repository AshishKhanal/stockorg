<apex:page standardController="SQX_Finding__c">

    <apex:variable var="PageSize" value="{!IF((JSENCODE($CurrentPage.parameters.printMode))=='true', 200, 5)}"/>
            
    <div id="checkMobile"></div>

    <div style="text-align:right;">
        <a href= "{!URLFOR($Page.SQX_Finding, null, [id=SQX_Finding__c.Id, printMode='true'])}" id="printPage" data-bind="invisible: isNewObject">{!$Label.compliancequest__CQ_UI_Print}</a>
    </div>
    
    <chatter:feedwithfollowers entityid="{!Id}" showheader="true"/>
    
    <div id="loadingBlock" data-role="cqloading"></div> 

    <div id="mainTab" class="cqMainTab">
        <ul>
            <li class="k-state-active" id="findingTab" data-title="{!$Label.compliancequest__CQ_UI_Issue}">{!$Label.compliancequest__CQ_UI_Issue}</li>
            <li id="policyTab" data-title="{!$Label.compliancequest__CQ_UI_Policy}">{!$Label.compliancequest__CQ_UI_Policy}</li>
            <li id="noteAndAttachmentTab" data-grid-sobject="NotesAndAttachments"
                data-title="{!$Label.compliancequest__CQ_UI_Notes}">{!$Label.compliancequest__CQ_UI_Notes}</li>
            <li id="referenceTab" data-grid-sobject="RelatedCAPAsEscalationsAndSupplierDeviations" data-title="{!$Label.compliancequest__CQ_UI_Reference}">{!$Label.compliancequest__CQ_UI_Reference}</li>
            <li id="responseHistoryTab" data-grid-sobject="compliancequest__SQX_Finding_Response__c" data-bind="invisible: mainRecord.isDraft">{!$ObjectType.compliancequest__SQX_Finding_Response__c.Label}</li>
            <li id="findingRecordActivityTab" data-grid-sobject="compliancequest__SQX_Finding_Record_Activity__c"
                data-title="{!$Label.compliancequest__CQ_UI_Record_Activity}">{!$Label.compliancequest__CQ_UI_Record_Activity}</li>
            <li id="workflowTab" data-title="{!$Label.compliancequest__CQ_UI_Workflow}" data-bind="invisible: mainRecord.isNew">{!$Label.compliancequest__CQ_UI_Workflow}</li>
        </ul>
        
        <div id="detailSection" class="cqFormSection">
            <div class="cqSectionContent section1col">

                <div data-container="Finding.compliancequest__Finding_Type__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Title__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Description__c" data-role="cqcompositefield"></div>
            </div>
            <div class="cqSectionContent section3col">

                <div data-container="Finding.compliancequest__Org_Division__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Org_Business_Unit__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Org_Region__c" data-role="cqcompositefield"></div>
                
                <div data-container="Finding.compliancequest__Issue_Date__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Due_Date__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Org_Site__c" data-role="cqcompositefield"></div>
                
                <div data-container="Finding.compliancequest__SQX_Department__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__SQX_Assignee__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.OwnerId" data-role="cqcompositefield" data-read-only="true"></div>
                
                <div data-container="Finding.compliancequest__QMS_Reference_Number__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Unit_of_Measure__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Quantity_Affected__c" data-role="cqcompositefield"></div>

                <div data-container="Finding.compliancequest__SQX_Audit__c" data-role="cqcompositefield" data-read-only="true"></div>
                <div data-container="Finding.compliancequest__SQX_CAPA__c" data-role="cqcompositefield" data-read-only="true"></div>
                <div class="clearFix"></div>

            </div>
            <div data-role='cqsectionheader' data-title='{!$Label.compliancequest__CQ_UI_Closure}' 
              data-initially-collapsed="{!IF($CurrentPage.parameters.printMode=='true', false, true)}">{!$Label.compliancequest__CQ_UI_Closure}</div>

            <div class="cqSectionContent section1col">
                <div data-container="Finding.compliancequest__Closure_Comment__c" data-role='cqcompositefield' data-read-only="true"></div>
            </div>
            
            <div class="clearFix"></div>
            
            <div data-role="cqgrid" id="filesGrid" class="cq-topGrid subGrid" data-source="Finding.relatedList('ContentDocumentLinks')"
                 data-collapsible="true" 
                 data-editable='{"mode": "incell", "create":true, "update":true, "destroy":true}'
                 data-toolbar="[{name: 'custom', text: '{!$Label.compliancequest__CQ_UI_Add_Files}', className: 'addFiles', enable: 'Finding.canAddChildren(\'ContentDocumentLink\')', action: 'sqx.commonActions.addFiles'}]"
                 data-columns="[{title: '{!$Label.compliancequest__CQ_UI_Actions}', command: ['preview', 'destroy'], width: '110px'},
                               {field: 'ContentTitle', template: '#: data.Title() #', editor: cq.getFileEditor}, 
                               {field: 'ContentDescription', template: '#: data.Description() #', editor: cq.getFileEditor}]">
            </div>
            <div class="clearFix"></div>
        </div>
        
        <div id="policySection" class="cqFormSection">
            <div data-role="cqsectionheader" data-title="{!$Label.compliancequest__CQ_UI_Risk}">{!$Label.compliancequest__CQ_UI_Risk}</div>
            <div class="cqSectionContent section3col">
                <div data-container="Finding.compliancequest__Severity__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Probability__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Risk_Level__c" data-role="cqcompositefield" data-read-only="true" data-dynamic-style="backgroundColor:riskLevelColor"></div>
            </div>
            <div class="cqSectionContent  section1col">
                <div data-container="Finding.compliancequest__Recommended_Action__c" data-role="cqcompositefield"></div>  
            </div>
            
            <div data-role="cqsectionheader" data-title="{!$Label.compliancequest__CQ_UI_Policy}">{!$Label.compliancequest__CQ_UI_Policy}</div>
            <div class="cqSectionContent section3col">

                <div data-container="Finding.compliancequest__CAPA_Required__c" data-role="cqcompositefield"></div>
                <div class="formItem"></div>
                <div class="formItem"></div>
                
                <div data-container="Finding.compliancequest__Response_Required__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Containment_Required__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Investigation_Required__c" data-role="cqcompositefield"></div>
                
                <div data-container="Finding.compliancequest__Due_Date_Response__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Due_Date_Containment__c" data-role="cqcompositefield"></div>
                <div data-container="Finding.compliancequest__Due_Date_Investigation__c" data-role="cqcompositefield"></div>
                
                <div class="formItem"></div>
                <div class="formItem"></div>
                <div data-container="Finding.compliancequest__Corrective_Action_Required__c" data-role="cqcompositefield"></div>
                
                <div class="formItem"></div>
                <div class="formItem"></div>
                <div data-container="Finding.compliancequest__Preventive_Action_Required__c" data-role="cqcompositefield"></div>
                
                <div class="formItem"></div>
                <div class="formItem"></div>
                <div data-container="Finding.compliancequest__Investigation_Approval__c" data-role="cqcompositefield"></div>
                
                <div class="formItem"></div>
                <div class="formItem"></div>
                <div data-container="Finding.compliancequest__SQX_Investigation_Approver__c" data-role="cqcompositefield"></div>
            </div>
            <div class="clearFix"></div>
        </div>
        
        <div id="noteAndAttachmentSection" class="cqFormSection">
            <div data-role="cqgrid" class="cq-topGrid subGrid" data-source="Finding.relatedList('Notes')"
                 data-editable='{ "mode": "popup", "update": true, "create": true, "destroy": true}'
                 data-columns='[
                    {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{ "name": "edit", "text": { "update": "{!$Label.compliancequest__CQ_UI_Save}"} }, "destroy"], "width": "100px"},
                    {"field": "IsPrivate", "hidden": true},
                    {"field": "Title"},
                    {"field": "Body"}
                 ]'>
            </div>
            <div class="clearFix"></div> 
        </div>
        <div id="referenceSection" class="cqFormSection">
            <div data-role="cqgrid" class="cq-topGrid subGrid"
                 data-title = "{!$Label.compliancequest__CQ_UI_Related_CAPA}"
                 data-source= "Finding.relatedList('compliancequest__SQX_Finding_CAPAs__r')"
                 data-editable='{ "mode": "popup", update:false, create:false, destroy:false}'
                 data-columns="[
                    {field: 'compliancequest__SQX_CAPA__c'},
                    {field: 'compliancequest__CAPA_Title__c', hideOnPopupEdit: true},
                    {field: 'compliancequest__CAPA_Status__c', hideOnPopupEdit: true},
                    {field: 'compliancequest__Resolves_by_CAPA__c' }
                 ]">
            </div>
            <div data-role="cqgrid" class="cq-topGrid subGrid"
                 data-title = "{!$Label.compliancequest__CQ_UI_Related_Supplier_Deviations}"
                 data-unique-fields = "compliancequest__SQX_Supplier_Deviation__c, compliancequest__SQX_Finding__c"
                 data-show-uniqueness-error-on= "compliancequest__SQX_Supplier_Deviation__c" 
                 data-uniqueness-validation-msg ="{!$Label.compliancequest__SQX_ERR_MSG_Supplier_Deviation_Already_Referred}"
                 data-source= "Finding.relatedList('compliancequest__SQX_Finding_Supplier_Deviations__r')"
                 data-editable='{ "mode": "popup", update:false, create:false, destroy:false }'
                 data-columns="[
                               {field: 'compliancequest__SQX_Supplier_Deviation__c'},
                               {field: 'compliancequest__Supplier_Deviation_Effective_Date__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Supplier_Deviation_Expiration_Date__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Supplier_Deviation_Status__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Comment__c'}
                               ]">
            </div>
            <div data-role="cqgrid" class="cq-topGrid subGrid"
                 data-title = "{!$Label.compliancequest__CQ_UI_Related_Supplier_Escalations}"
                 data-unique-fields = "compliancequest__SQX_Supplier_Escalation__c,compliancequest__SQX_Related_Finding__c"
                 data-show-uniqueness-error-on= "compliancequest__SQX_Supplier_Escalation__c" 
                 data-uniqueness-validation-msg ="{!$Label.compliancequest__SQX_ERR_MSG_Supplier_Escalation_Already_Referred}"
                 data-source= "Finding.relatedList('compliancequest__SQX_Escalation_References__r')"
                 data-editable='{ "mode": "popup", update:false, create:false, destroy:false }'
                 data-columns="[
                               {field: 'compliancequest__SQX_Supplier_Escalation__c'},
                               {field: 'compliancequest__Supplier_Escalation_Subject__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Supplier_Escalation_Level__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Supplier_Escalation_Status__c', hideOnPopupEdit: true},
                               {field: 'compliancequest__Comment__c'}
                               ]">
            </div>
            <div class="clearFix"></div>
        </div>
        <div id="responseHistorySection" class="cqFormDetail">
            <div data-role="cqgrid" class="cq-topGrid subGrid" data-source="Finding.relatedList('compliancequest__SQX_Responses__r')"
                 data-editable='{"mode": "popup", update:false, create:false, destroy:false}'
                 data-detail-init="initGridDetail"
                 data-detail-template="responseHistoryTemplate"
                 data-show-title="false"
                 data-collapsible="false"
                 data-columns="[
                    {title: '{!$Label.compliancequest__CQ_UI_Response_Summary}', template: '#: chompedSummary(responseSummary()) #'},
                    {field: 'compliancequest__Submitted_On__c'},
                    {field: 'compliancequest__Submitted_By__c'},
                    {field: 'hasContainments()', title: '{!$ObjectType.compliancequest__SQX_Containment__c.Label}', template: '#= getCheckBox(hasContainments()) #', width: '100px'},
                    {field: 'hasInvestigations()', title: '{!$ObjectType.compliancequest__SQX_Investigation__c.Label}', template: '#= getCheckBox(hasInvestigations()) #', width: '100px'},
                    {field: 'compliancequest__Approval_Status__c', template: function(dataItem){ return responseApprovalColumnTemplate(dataItem) }, width: '310px'},
                    {title: '{!$Label.compliancequest__CQ_UI_Approved_On}', template: '#: ApprovedOn() #'}
                 ]"
                 data-initially-collapsed="false">
            </div>
            <div class="clearFix"></div>
            
            <div class="cqSectionContent cqFormDetail" id="responseSection">
                <div id="findingContainmentGrid" data-role="cqgrid" class="cq-topGrid" data-source="Finding.relatedList('compliancequest__SQX_Containments__r')"
                     data-detail-init="initGridDetail"
                     data-detail-template="findingContainmentDetailTemplate"
                     data-editable='{"mode": "popup", update:true, create:true, destroy:true}'
                     data-columns='[
                        {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{ name: "edit", text: { update: "{!$Label.compliancequest__CQ_UI_Save}"} }, "destroy"], "width": "100px"},
                        {field: "compliancequest__Containment_Summary__c" },
                        {field: "compliancequest__Completion_Date__c", width: "200px", "template" : "\\#: getFormattedDate(compliancequest__Completion_Date__c)\\#" },
                        {field: "compliancequest__Status__c", width: "200px" }
                     ]'>
                </div>
                
                <div id="findingInvestigationGrid" data-role="cqgrid" class="cq-topGrid" data-source="Finding.relatedList('compliancequest__SQX_Investigations__r')"
                     data-detail-init="initGridDetail"
                     data-detail-template="findingInvestigationDetailTemplate"
                     data-editable="{mode: 'popup', update:true, create:true, destroy:true}"
                     data-columns="[
                        {title: '{!$Label.compliancequest__CQ_UI_Actions}', command: [{ name: 'edit', text: { update: '{!$Label.compliancequest__CQ_UI_Save}'} }, 'destroy', {name: 'cloneinvestigation', text: '{!$Label.compliancequest__CQ_UI_Clone}', enable: 'data.canClone()', click: cloneInvestigation }], 'width': '115px'},
                        {field: 'compliancequest__Investigation_Summary__c', template: '#: chompedSummary(compliancequest__Investigation_Summary__c) #'},
                        {field: 'compliancequest__Status__c', width: '200px'},
                        {field: 'compliancequest__Approval_Status__c', width: '200px'}
                     ]">
                </div>
            </div>
            <div class="clearFix"></div>
        </div>
        
        <div id="findingRecordActivitySection" class="cqFormSection">
            <div data-role="cqgrid" class="cq-topGrid subGrid" data-source="Finding.relatedList('compliancequest__SQX_Finding_Record_Activities__r')"
                 data-columns="[
                    {field: 'compliancequest__Activity__c'},
                    {field: 'compliancequest__Comment__c'},
                    {field: 'compliancequest__Modified_By__c'},
                    {field: 'CreatedDate'}
                 ]">
            </div>
            <div class="clearFix"></div>
        </div>
        
        <!-- code added for Workflow(Task) -->
        <div id="workflowSection" class="cqFormSection">
            <div class="cqSectionContent section1col">
              <apex:relatedList list="OpenActivities" pageSize="{!PageSize}"/>
            </div>

            <div class="cqSectionContent section1col">
               <apex:relatedList list="ActivityHistories" pageSize="{!PageSize}"/>
            </div>
            <div class="clearFix"></div>
        </div>
    </div>
    
    <!-- KENDO TEMPLATES -->
    <script type="text/x-kendo-template" id="CAPADropDownTemplate">
        <div>
            <p>#: data.Name # (#: data.compliancequest__Status__c #)</p>
            <em>#: data.compliancequest__Title__c #</em>
        </div>
    </script>

    <script type="text/x-kendo-template" id="containmentDetailTemplate">
        <div><b>{!$Label.compliancequest__CQ_UI_Summary}: </b><span class="boundElem containmentSummary" data-bind="html: compliancequest__SQX_Containment__r.compliancequest__Containment_Summary__c"></span></div>
        <div class="cq-topGrid subGrid"
             data-options='{"title":"{!$Label.compliancequest__CQ_UI_Attachments}","hideIfEmpty":true}'
             data-relatedlist="compliancequest__containmentAttachments__r"
             data-columns='[
                       {"field" : "Name", "template": "\\#= buildAttachmentViewLink(data) \\#"},
                        {"field" : "Description"}]'>
        </div>
    </script>
    
    <script type="text/x-kendo-template" id="invDetailTemplate">
        <div><b>{!$Label.compliancequest__CQ_UI_Summary}: </b><span class="boundElem investigationSummary" data-bind="html: compliancequest__SQX_Investigation__r.compliancequest__Investigation_Summary__c"></span></div>
        <div class="cq-topGrid subGrid" data-relatedlist="compliancequest__rootCauses__r"
             data-options='{"title": "{!$ObjectType.compliancequest__SQX_Root_Cause__c.LabelPlural}", "hideIfEmpty": true}'
             data-columns='[
                {"field": "compliancequest__SQX_Root_Cause_Code__r.Name","title":"{!$ObjectType.compliancequest__SQX_Root_Cause_Code__c.Label}"},
                {"field": "compliancequest__Description__c"},
                {"field": "compliancequest__Sub_Supplier_Name__c"},
                {"field": "compliancequest__Category__c"},
                {"field": "compliancequest__Failure_Component__c"},
                {"field": "compliancequest__Supplier_Issue__c","template": "\\#= getCheckBox(compliancequest__Supplier_Issue__c) \\#"}
             ]'>
        </div>
        <div class="cq-topGrid subGrid" data-relatedlist="compliancequest__investigationTools__r"
             data-options='{"title": "{!$ObjectType.compliancequest__SQX_Investigation_Tool__c.LabelPlural}", "hideIfEmpty": true}'
             data-columns='[
                {"field": "compliancequest__Investigation_Tool_Method_Used__c"},
                {"field": "compliancequest__Description__c"}
             ]'>
        </div>
        <div class="cq-topGrid subGrid" data-relatedlist="compliancequest__actionPlans__r"
             data-options='{"title": "{!$ObjectType.compliancequest__SQX_Action_Plan__c.LabelPlural}", "hideIfEmpty": true}'
             data-columns='[
                {"field": "compliancequest__Plan_Number__c"},
                {"field": "compliancequest__Plan_Type__c"},
                {"field": "compliancequest__Description__c"},
                {"field": "compliancequest__SQX_User__c", "template": "\\#= getReferenceDisplayTemplate(\"compliancequest__SQX_User__c\")(data) \\#"},
                {"field": "compliancequest__Due_Date__c", "template": "\\#: getFormattedDate(compliancequest__Due_Date__c)\\#"},
                {"field": "compliancequest__Days_Required__c"},
                {"field": "compliancequest__Completed__c", "template": "\\#= getCheckBox(compliancequest__Completed__c) \\#"},
                {"field": "compliancequest__Completion_Date__c", "template" : "\\#: getFormattedDate(compliancequest__Completion_Date__c)\\#"}
             ]'>
        </div>
        <div class="cq-topGrid subGrid"
             data-options='{"title":"{!$Label.compliancequest__CQ_UI_Attachments}","hideIfEmpty":true}'
             data-relatedlist="compliancequest__investigationAttachments__r"
             data-columns='[
                       {"field" : "Name", "template": "\\#= buildAttachmentViewLink(data) \\#"},
                        {"field" : "Description"}]'>
        </div>
    </script>
    
    <script type="text/x-kendo-template" id="findingInvestigationDetailTemplate">
        <div data-role="cqtabstrip" data-collapsible="true" data-show-grid-title="true" data-show-grid-count="true" class="tabStrip">
            <ul>
                <li class="k-state-active">{!$Label.compliancequest__CQ_UI_Investigation_Summary}</li>
                <li data-grid-sobject="compliancequest__SQX_Investigation_Tool__c">{!$ObjectType.compliancequest__SQX_Investigation_Tool__c.LabelPlural}</li>
                <li data-grid-sobject="compliancequest__SQX_Root_Cause__c">{!$ObjectType.compliancequest__SQX_Root_Cause__c.LabelPlural}</li>
                <li data-grid-sobject="compliancequest__SQX_Action_Plan__c">{!$ObjectType.compliancequest__SQX_Action_Plan__c.LabelPlural}</li>
                <li data-grid-sobject="Attachment">{!$Label.compliancequest__CQ_UI_Attachments}</li>
            </ul>
            
            <div>
                <div class="investigationSummary" data-bind="html: compliancequest__Investigation_Summary__c"></div>
            </div>
            <div>
                <div class="investigationTool">
                    <div class="subGrid" data-relatedlist="compliancequest__SQX_Investigation_Tools__r"
                         data-options='{"editable": {"mode": "popup", "update": true, "create": true, "destroy": true}, "showTitle": false, "showCount": false, "collapsible": false, "initialRows": 0, "expandOnSave": false}'
                         data-columns='[
                            {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{"name": "edit", "text": {"update": "{!$Label.compliancequest__CQ_UI_Save}"}}, "destroy"], "width": "100px"},
                            {"field": "compliancequest__Investigation_Tool_Method_Used__c"},
                            {"field": "compliancequest__Description__c"}
                         ]'>
                    </div>
                </div>
            </div>
            <div>
                <div class="rootCause">
                    <div class="subGrid" data-relatedlist="compliancequest__SQX_Root_Causes__r"
                         data-options='{"editable": {"mode": "popup", "update": true, "create": true, "destroy": true}, "showTitle": false, "showCount": false, "collapsible": false, "initialRows": 0, "expandOnSave": false}'
                         data-columns='[
                            {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{"name": "edit", "text": {"update": "{!$Label.compliancequest__CQ_UI_Save}"}}, "destroy"], "width": "100px"},
                            {"field": "compliancequest__SQX_Root_Cause_Code__c"},
                            {"field": "compliancequest__Description__c"},
                            {"field": "compliancequest__Sub_Supplier_Name__c"},
                            {"field": "compliancequest__Category__c"},
                            {"field": "compliancequest__Failure_Component__c"},
                            {"field": "compliancequest__Supplier_Issue__c", "template": "\\#= getCheckBox(data.compliancequest__Supplier_Issue__c)\\#"}
                         ]'>
                    </div>
                </div>
            </div>
            <div>
                <div class="actionPlan">
                    <div class="subGrid" data-relatedlist="compliancequest__SQX_Action_Plans__r"
                         data-options='{"editable": {"mode": "popup", "update": true, "create": true, "destroy": true}, "showTitle": false, "showCount": false, "collapsible": false, "initialRows": 0, "expandOnSave": false}'
                         data-columns='[
                            {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{"name": "edit", "text": {"update": "{!$Label.compliancequest__CQ_UI_Save}"}}, "destroy"], "width": "100px"},
                            {"field": "compliancequest__Plan_Number__c"},
                            {"field": "compliancequest__Plan_Type__c"},
                            {"field": "compliancequest__Description__c"},
                            {"field": "compliancequest__SQX_User__c", "template": "\\#= getReferenceDisplayTemplate(\"compliancequest__SQX_User__c\")(data) \\#"},
                            {"field": "compliancequest__Due_Date__c", "template": "\\#: getFormattedDate(compliancequest__Due_Date__c)\\#"},
                            {"field": "compliancequest__Completed__c", "template": "\\#= getCheckBox(data.compliancequest__Completed__c)\\#"},
                            {"field": "compliancequest__Completion_Date__c", "template" : "\\#: getFormattedDate(compliancequest__Completion_Date__c)\\#"}
                         ]'>
                    </div>
                </div>
            </div>
            <div>
                <div class="Attachment">
                    <div class="subGrid"
                         data-relatedlist="Attachments"
                         data-save="attachmentSaveCalled"
                         data-options='{"editable":{"mode":"popup","update":true ,"create": true ,"destroy": true, "template": "#= getAttachmentTemplate(data) #" },"showTitle":false,"collapsible":false}'
                         data-columns='[
                                        {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{ "name": "edit", "text" : { "update": "{!$Label.compliancequest__CQ_UI_Save}"}, "enable": "enableAttachment(data)" } , {"name":"destroy", "enable": "enableAttachment(data)"} ], "width": "100px"},
                                        {"field" : "Name", "template": "\\#= buildAttachmentViewLink(data) \\#"},
                                        {"field" : "Description"}
                                        ]'>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-kendo-template" id="findingContainmentDetailTemplate">
        <div data-role="cqtabstrip" data-collapsible="true" data-show-grid-title="true" data-show-grid-count="true" class="tabStrip">
            <ul>
                <li class="k-state-active">{!$Label.compliancequest__CQ_UI_Containment_Summary}</li>
                <li data-grid-sobject="Attachment">{!$Label.compliancequest__CQ_UI_Attachments}</li>
            </ul>
            <div>
                <div class="containmentSummary" data-bind="html: compliancequest__Containment_Summary__c"></div>
            </div>
            <div>
                <div class="Attachment">
                    <div class="subGrid"
                         data-relatedlist="Attachments"
                         data-save="attachmentSaveCalled"
                         data-options='{"editable":{"mode":"popup","update":true ,"create": true ,"destroy": true, "template": "#= getAttachmentTemplate(data) #" },"showTitle":false,"collapsible":false}'
                         data-columns='[
                                        {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{ "name": "edit", "text" : { "update": "{!$Label.compliancequest__CQ_UI_Save}"}, "enable": "enableAttachment(data)" } , {"name":"destroy", "enable": "enableAttachment(data)"} ], "width": "100px"},
                                        {"field" : "Name", "template": "\\#= buildAttachmentViewLink(data) \\#"},
                                        {"field" : "Description"}
                                        ]'>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type="text/x-jquery-tmpl" id="approveRejectResponseTemplate">
        <div id="approveRejectResponse">
            <div style="padding: 10px 20px 10px 10px;">
                <div class="cq-topGrid subGrid" data-relatedlist="containments"
                     data-options='{"title": "{!$ObjectType.compliancequest__SQX_Containment__c.Label}", "hideIfEmpty": true}'
                     data-columns='[
                        {"field": "compliancequest__SQX_Containment__r.compliancequest__Containment_Summary__c", "title": "{!$Label.compliancequest__CQ_UI_Summary}", "template": "\#: chompedSummary(compliancequest__SQX_Containment__r.compliancequest__Containment_Summary__c)\#"},
                        {"field": "compliancequest__SQX_Containment__r.compliancequest__Completion_Date__c", "title": "{!$ObjectType.compliancequest__SQX_Containment__c.fields.compliancequest__Completion_Date__c.Label}", "template" : "\\#: getFormattedDate(compliancequest__SQX_Containment__r.compliancequest__Completion_Date__c)\\#"},
                        {"field": "Remark", "title": "{!$Label.compliancequest__CQ_UI_Remark}", "template": "&lt;textarea type=\"text\" class=\"approvalRemarkItem\" data-bind=\"value: compliancequest__Approval_Remark__c \" &gt;&lt;/textarea&gt;", "width": "300px"}
                     ]'>
                </div>
                
                <div class="cq-topGrid subGrid" data-relatedlist="investigations"
                     data-options='{"title": "{!$ObjectType.compliancequest__SQX_Investigation__c.Label}", "hideIfEmpty": true}'
                     data-columns='[
                        {"field": "compliancequest__SQX_Investigation__r.compliancequest__Investigation_Summary__c", "title": "{!$Label.compliancequest__CQ_UI_Summary}", "template": "\#: chompedSummary(compliancequest__SQX_Investigation__r.compliancequest__Investigation_Summary__c)\#"},
                        {"field": "compliancequest__SQX_Investigation__r.compliancequest__Status__c", "title": "{!$ObjectType.compliancequest__SQX_Investigation__c.fields.compliancequest__Status__c.Label}", "template": "\\#: translateValue(data.compliancequest__SQX_Investigation__r, compliancequest__SQX_Investigation__r.compliancequest__Status__c, \"compliancequest__Status__c\")\\#"},
                        {"field": "Reject", "title": "{!$Label.compliancequest__CQ_UI_Reject}", "template": "&lt;input type=\"checkbox\" onchange=\"rejectCheckboxOnChange()\" class=\"rejectItem\" /&gt;", "width": "50px"},
                        {"field": "Remark", "title": "{!$Label.compliancequest__CQ_UI_Remark}", "template": "&lt;textarea class=\"approvalRemarkItem\" data-bind=\"value: compliancequest__Approval_Remark__c \" &gt;&lt;/textarea&gt;", "width": "300px"}
                     ]'>
                </div>
                
                <div class="section1col">
                    <!-- Meaning of signature -->
                    <div class="formItem">
                        <div class="formLabel"><label for="respondMeaningOfSignature">{!$Label.compliancequest__CQ_UI_Purpose_Of_Signature}</label></div>
                        <div class="formField" name="respondMeaningOfSignature"><span></span></div>
                    </div>
                    <!-- ****** -->
                    <div class="formItem">
                        <div class="formLabel"><label for="approvalRemark">{!$Label.compliancequest__CQ_UI_Comment}</label></div>
                        <div class="formField">
                            <div class="editableElement">
                                <textarea id="approvalRemark" class="cq-multi-line-textarea" name="Comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <div data-role="cqelectronicsignature" data-current-user-details-var="userDetails"></div>
                </div>
                <div class="clearFix"></div>
                
                <div class="commandSection" style="text-align:center;margin-top:10px">
                    <div>
                        <div class="errorStyle errorDiv">
                            <span class="k-icon k-warning"></span> <span class="msg"></span>
                        </div>
                    </div>
                    
                    <button type="button" id="btnApproveReject" onclick="approveRejectResponse(event)" class="cqCommandButton k-button" data-role="button" role="button" aria-disabled="false" tabindex="0">{!$Label.compliancequest__CQ_UI_Approve}</button>
                    <button type="button" id="btnCancelApproval" onclick="cancelResponseApproval(event)" class="cqCommandButton k-button" data-role="button" role="button" aria-disabled="false" tabindex="0">{!$Label.compliancequest__CQ_UI_Cancel}</button>
                </div>
            </div>
        </div>
    </script>
         
    <script type="text/x-kendo-template" id="escalateToCapaTemplate">
        <div class="cqSectionContent section1col">
            <!-- Meaning of signature -->
            <div data-container="Finding.compliancequest__Create_New_CAPA__c" data-role="cqcompositefield" data-edit-mode="true"></div>      
            <div data-container="Finding.compliancequest__SQX_CAPA__c" data-role="cqcompositefield" data-edit-mode="true"></div>      
        </div>
        <div class="clearFix"></div>
    </script>

    <script type="text/x-kendo-template" id="AttachmentEditTemplate">
        <iframe class="attachmentFrame" id="Attachment#= data.Id #" src="{!$Page.SQX_Upload_Attachment}?callback=attachmentCallBac{!$Label.compliancequest__CQ_UI_Nothing_Yet}k&parentId=#= currentUser.Id #&uid=#= data.parentEntity().Id #&recordid=#= data.Id #" style="height: 145px;border: none;width: 100%;">
            
        </iframe>
    </script>

    <script type="text/x-kendo-template" id="AttachmentEditTemplateChild">
        <iframe class="attachmentFrame" id="Attachment\\#= data.Id \\#" src="{!$Page.SQX_Upload_Attachment}?callback=attachmentCallBack&parentId=\\#= currentUser.Id \\#&uid=\\#= data.parentEntity().Id \\#&recordid=\\#= data.Id \\#" style="height: 145px;border: none;width: 100%;">
            {!$Label.compliancequest__CQ_UI_Nothing_Yet}
        </iframe>
    </script>
    <script type="text/x-kendo-template" id="responseHistoryTemplate">
        <div><b>{!$Label.compliancequest__CQ_UI_Summary}: </b><span class="boundElem responseSummary" data-bind="html: responseSummary()"></span></div>

        #if (data.compliancequest__Approval_Status__c != 'Recalled'){ #
            <div class="cq-topGrid subGrid" data-relatedlist="containments"
                 data-options='{"title": "{!$ObjectType.compliancequest__SQX_Containment__c.Label}", "hideIfEmpty": true}'
                 data-columns='[
                    {"field": "compliancequest__SQX_Containment__r.compliancequest__Containment_Summary__c", "title": "{!$Label.compliancequest__CQ_UI_Summary}", "template": "\\#: chompedSummary(compliancequest__SQX_Containment__r.compliancequest__Containment_Summary__c)\\#"},
                    {"field": "compliancequest__SQX_Containment__r.compliancequest__Completion_Date__c", "title": "{!$ObjectType.compliancequest__SQX_Containment__c.fields.compliancequest__Completion_Date__c.label}", "template" : "\\#: getFormattedDate(compliancequest__SQX_Containment__r.compliancequest__Completion_Date__c)\\#"},
                    {"title": "{!$Label.compliancequest__CQ_UI_Approval_Remark}", "template":"\\\#= ApprovalRemark() \\\#"}
                 ]'>
            </div>
            
            <div class="cq-topGrid subGrid" data-relatedlist="investigations"
                 data-detailtemplate="invDetailTemplate"
                 data-options='{"title": "{!$ObjectType.compliancequest__SQX_Investigation__c.Label}", "hideIfEmpty": true}'
                 data-columns='[
                    {"field": "compliancequest__SQX_Investigation__r.compliancequest__Investigation_Summary__c", "title": "{!$Label.compliancequest__CQ_UI_Summary}", "template": "\\#: chompedSummary(compliancequest__SQX_Investigation__r.compliancequest__Investigation_Summary__c)\\#"},
                    {"title": "{!$ObjectType.compliancequest__SQX_Investigation__c.fields.compliancequest__Approval_Status__c.label}", template: "\\\#: ApprovalStatus() \\\#"},
                    {"title": "{!$Label.compliancequest__CQ_UI_Approval_Remark}", template:"\\\#= ApprovalRemark() \\\#"}
                 ]'>
            </div>
            
            <div class="cq-topGrid subGrid" data-relatedlist="activeProcessSteps"
                 data-options='{"title": "{!$Label.compliancequest__CQ_UI_Controlled_Document_Approval_History}", "hideIfEmpty": true}'
                 data-columns='[
                    {"title": "{!$Label.compliancequest__CQ_UI_Action}", "width": "120px", "command": [{"name": "{!$Label.compliancequest__CQ_UI_Approve}", "enable": "data.canApprove()", "click": approveResponse}, {"name": "{!$Label.compliancequest__CQ_UI_Reject}", "enable": "data.canReject()", "click": rejectResponse}]},
                    {"title": "{!$Label.compliancequest__CQ_UI_Status}", "template": "\\\#: ComputedStatus() \\\#"},
                    {"title": "{!$Label.compliancequest__CQ_UI_Approved_On}", "template": "\\\#: ApprovedOn() \\\#"},
                    {"field": "OriginalActor.Name", "title": "{!$Label.compliancequest__CQ_UI_Assigned_To}"},
                    {"field": "Actor.Name", "title": "{!$Label.compliancequest__CQ_UI_Actual_Approver}"},
                    {"field": "Comments"}
                 ]'>
            </div>
        # } #
    </script>
</apex:page>
