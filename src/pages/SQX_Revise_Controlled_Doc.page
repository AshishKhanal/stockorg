<apex:page standardController="SQX_Controlled_Document__c" extensions="SQX_Extension_Controlled_Document" title="{!$Label.CQ_UI_Controlled_Doc_Edit_New_Revision}" lightningStylesheets="true">
    <apex:sectionHeader title="{!$Label.CQ_UI_Controlled_Doc_Edit}" subtitle="{!$Label.CQ_UI_New_Doc_Revision}" />
    <apex:variable var="docType" value="{!$ObjectType.SQX_Controlled_Document__c.fields.Document_Type__c.Name}"/>
    <apex:variable var="changeOrder" value="{!$ObjectType.SQX_Controlled_Document__c.fields.SQX_Change_Order__c.Name}"/>
    <apex:pageMessages escape="false"/>
    <apex:pageMessage severity="WARNING" strength="2" escape="false" rendered="{! HasLargePrimaryContent }"
                      summary="<h4>{!$Label.compliancequest__cq_ui_warning_heading}</h4> {!$Label.compliancequest__cq_ui_controlled_document_clone_or_revise_large_content_warning}" />
    <apex:form >
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Document_Number__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Document_Category__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Description__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Review_Interval__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Auto_Release__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Content_Reference__c}"/>
        <apex:inputHidden value="{!SQX_Controlled_Document__c.Primary_Content_Check_In_Method__c}"/>

        <apex:pageBlock title="{!$Label.CQ_UI_New_Doc_Revision}"> 
            <apex:pageBlockSection columns="1" collapsible="false" >               
                
                <!-- This field set contains read only field for document -->
                <apex:repeat value="{!$ObjectType.SQX_Controlled_Document__c.FieldSets.Revise_Document_Read_Only_Fields}" var="f">
                        <apex:outputField value="{!SQX_Controlled_Document__c[f]}" rendered="{!IF(f==changeOrder, NOT(ISBLANK(SQX_Controlled_Document__c.SQX_Change_Order__c)), True)}"/>
                </apex:repeat>
                <!-- This field set contains editable field for document. It is specially used to support client side configuration -->
                <apex:repeat value="{!$ObjectType.SQX_Controlled_Document__c.FieldSets.Revise_Document_Edit_Fields}" var="f">
                        <apex:inputField html-data-sqx-field="{!$ObjectType.SQX_Controlled_Document__c.Name}.{!f}" value="{!revisionDoc[f]}" rendered="{!IF(f==docType, isStandardDoc, True)}" required="{!OR(f.Required, f.DbRequired)}"/>
                </apex:repeat>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.CQ_UI_New_Revision}" for="Revision__c"></apex:outputLabel>
                    <apex:inputText id="Revision__c" value="{!newRev}"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!SQX_Controlled_Document__c.Document_Type__c}" rendered="{!isStandardDoc}"
                    html-data-sqx-field="{!$ObjectType.SQX_Controlled_Document__c.Name}.{!$ObjectType.SQX_Controlled_Document__c.fields.Document_Type__c.Name}" />
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.CQ_UI_Changes}" for="Changes__c" ></apex:outputLabel>
                    <apex:inputTextarea id="Changes__c" cols="50" rows="4" value="{!changes}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.CQ_UI_Change_Scope}" for="Change_Scope__c" ></apex:outputLabel>
                 <apex:inputField html-data-sqx-field="{!$ObjectType.SQX_Controlled_Document__c.Name}.{!$ObjectType.SQX_Controlled_Document__c.fields.Change_Scope__c.Name}" id="Change_Scope__c" value="{!SQX_Controlled_Document__c.Change_Scope__c}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.CQ_UI_File}" for="file"></apex:outputLabel>
                    <apex:inputFile id="file" value="{!file}" fileName="{!fileName}"></apex:inputFile>                
                </apex:pageBlockSectionItem>
               
            </apex:pageBlockSection>
            <apex:pageBlockButtons >
                <apex:commandButton value="{!$Label.CQ_UI_Button_Save}" action="{!Revise}"/>
                <apex:commandButton value="{!$Label.CQ_UI_Button_Cancel}" action="{!Cancel}"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
</apex:page>