<apex:page standardController="SQX_Change_Order__c">

    <apex:variable var="PageSize" value="{!IF((JSENCODE($CurrentPage.parameters.printMode))=='true', 200, 5)}"/>

    <div style="text-align:right;">
        <a href= "{!URLFOR($Page.SQX_Change, null, [id=SQX_Change_Order__c.Id, printMode='true'])}" id="printPage" data-bind="invisible: isNewObject">Print</a>
    </div>
    
	<chatter:feedwithfollowers entityid="{!Id}" showheader="true" />
	<div id="checkMobile"></div>
	<div id="loadingBlock" data-role="cqloading"></div>

    <div data-role="cqtabstrip" id="mainTab" data-collapsible="true"
        data-show-grid-title="true" data-show-grid-count="true"
        class="cqMainTab extraMargin" data-activate="setActionButtons">
        <ul>
            <li id="detailTab" class="k-state-active">{!$ObjectType.compliancequest__SQX_Change_Order__c.Label}</li>
            <li id="actionTab" data-grid-sobject="compliancequest__SQX_Implementation__c" data-title="{!$Label.compliancequest__CQ_UI_Changes}">{!$Label.compliancequest__CQ_UI_Changes}</li>
            <li id="noteAndAttachmentTab" data-grid-sobject="Note,ContentDocumentLink"
                data-title="{!$Label.compliancequest__CQ_UI_Notes_And_Attachments}">{!$Label.compliancequest__CQ_UI_Notes_And_Attachments}</li>
            <li id="impactTab" data-grid-sobject="compliancequest__SQX_Impact__c"
                data-title="{!$Label.compliancequest__CQ_UI_Impacts}">{!$Label.compliancequest__CQ_UI_Impacts}</li>
            <li id="changeRecordActivityTab"
                data-grid-sobject="compliancequest__SQX_Change_Order_Record_Activity__c"
                data-title="{!$Label.compliancequest__CQ_UI_Record_Activity}">{!$Label.compliancequest__CQ_UI_Record_Activity}</li>
            <li id="workflowTab" data-title="{!$Label.compliancequest__CQ_UI_Workflow}" data-bind="invisible: mainRecord.isNewObject">{!$Label.compliancequest__CQ_UI_Workflow}</li>
        </ul>
        <div id="detailSection" class="cqFormSection">
            <div class="cqSectionContent section1col">
                <div data-container="CqChange.compliancequest__Title__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Description__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Justification__c"
                    data-role='cqcompositefield'></div>
                <div class="clearFix"></div>
            </div>
            
            <div class="cqSectionContent section3col">
                <div data-container="CqChange.compliancequest__Change_Category__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Priority__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Target_Completion_Date__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__SQX_Submitted_By__c"
                    data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Submitted_Date__c"
                    data-role='cqcompositefield'></div>
                <div class="clearFix"></div>
            </div>

            <div data-role='cqsectionheader' data-title='{!$Label.compliancequest__CQ_UI_Ownership}'>{!$Label.compliancequest__CQ_UI_Ownership}</div>
            <div class="cqSectionContent section3col">
                <div data-container="CqChange.compliancequest__Org_Division__c" data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Org_Business_Unit__c" data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Org_Region__c" data-role='cqcompositefield'></div>
                
                <div data-container="CqChange.compliancequest__SQX_Product__c" data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__SQX_Process__c" data-role='cqcompositefield'></div>
                <div data-container="CqChange.compliancequest__Org_Site__c" data-role='cqcompositefield'></div>

                <div data-container="CqChange.OwnerId" data-role='cqcompositefield' data-read-only="true"></div>
                <div data-container="CqChange.compliancequest__SQX_Department__c" data-role='cqcompositefield'></div>
                
                <div class="clearFix"></div>
            </div>
            
            <div data-role='cqsectionheader' data-title='{!$Label.compliancequest__CQ_UI_Policy}'>{!$Label.compliancequest__CQ_UI_Policy}</div>
            <div class='cqSectionContent section3col'>
                <div data-container="CqChange.compliancequest__SQX_Approval_Matrix__c"
                    data-role='cqcompositefield'></div>
                <div class="clearFix"></div>
            </div>
        </div>

        <div id="actionSection" class="cqFormSection">
            <div data-role="cqgrid" class="cq-topGrid subGrid" id="planGrid"
                data-source="CqChange.plans()"
                data-toolbar="[{name: 'custom', text: '{!$Label.compliancequest__CQ_UI_Add_Task}', className: 'genericPlan', enable: 'CqChange.canEdit() && CqChange.canPlan()', action: 'sqx.evtHdlr.addPlan'},
                                {name: 'custom', text: '{!$Label.compliancequest__CQ_UI_New_Document}', className: 'newDocPlan', enable: 'CqChange.canEdit() && CqChange.canPlan()', action: 'sqx.evtHdlr.addPlan'},
                                {name: 'custom', text: '{!$Label.compliancequest__CQ_UI_Revise_Document}', className: 'reviseDocPlan', enable: 'CqChange.canEdit() && CqChange.canPlan()', action: 'sqx.evtHdlr.addPlan'},
                                {name: 'custom', text: '{!$Label.compliancequest__CQ_UI_Obsolete_Document}', className: 'obsoleteDocPlan', enable: 'CqChange.canEdit() && CqChange.canPlan()', action: 'sqx.evtHdlr.addPlan'}]"
                data-editable='{ "mode": "popup", "update":true, "create":true, "destroy":true}'
                data-detail-template="actionGridDetailTemplate"
                data-save-and-new = "false"
                data-detail-init="initGridDetail"
                data-columns='[{"title": "{!$Label.compliancequest__CQ_UI_Actions}","command": [{ "name": "edit", "text": { "update": "Save"} }, "destroy"], "width": "100px"},
                                {"template": $("#actionDetailTemplate").html(), "title" : "{!$Label.compliancequest__CQ_UI_Details}"},
                                {"field": "compliancequest__SQX_Controlled_Document__c", "hideOnPopupEdit" : sqx.evtHdlr.plan.hideCtrlDoc, hidden: true },
                                {"field": "compliancequest__Title__c"},
                                {"field": "compliancequest__Description__c"},
                                {"field": "compliancequest__Due_Date__c", "width" : "100px"},
                                {"field": "compliancequest__SQX_User__c", "width" : "100px"}]'>
            </div>

            <div data-role="cqgrid" class="cq-topGrid subGrid" id="actionGrid"
                data-source="CqChange.actions()"
                data-hide-if-empty="true"
                data-detail-template="actionGridDetailTemplate"
                data-detail-init="initGridDetail"
                data-editable='{ "mode": "popup", "update":true, "create":false, "destroy":false}'
                data-columns='[{"title": "{!$Label.compliancequest__CQ_UI_Actions}","command": [{ "name": "edit", "text": { "update": "Save"} }], "width": "100px"},
                                {"template": $("#actionDetailTemplate").html(), "title" : "{!$Label.compliancequest__CQ_UI_Details}"},
                                {"field": "compliancequest__Title__c"},
                                {"field": "compliancequest__Description__c"},
                                {"field": "compliancequest__SQX_User__c" , "width" : "100px"},
                                {"field": "compliancequest__Due_Date__c" , "width" : "100px"},
                                {"field": "compliancequest__Status__c" , "width" : "75px"},
                                {"field": "compliancequest__SQX_Controlled_Document_New__c", "hideOnPopupEdit": sqx.evtHdlr.action.hideNewDoc, hidden: true},
                                {"template": $("#signOffDocumentTemplate").html(), "title" : "{!$Label.compliancequest__CQ_UI_Sign_Off}", "hideOnPopupEdit": true},
                                {"field": "compliancequest__Completion_Date__c" , "width" : "100px", hidden: true},
                                {"field": "compliancequest__Completed_By__c" , "width" : "100px", hidden: true},
                                {"field": "compliancequest__Remark__c", "width": "150px", hidden: true}]'>
            </div>
            
            <div class="clearFix"></div>
            
            <div data-role="cqsectionheader" data-title="{!$Label.compliancequest__CQ_UI_New_Revised_Document_Task}" id="docThumbnailsSectionHeader" style="display: none;"></div>
            
            <div class="cqSectionContent section1col" id="docThumbnailsSection" style="display: none;">
                <div data-role="listview" style="border-width: 0px;"
                    data-source="CqChange.newOrRevisedDocuments()"
                    data-bound="docThumbnailsListViewDataBound"
                    data-template="newOrRevisedDocumentThumbnailsTemplate">
                </div>
                
                <div class="clearFix"></div>
            </div>

            <div class="clearFix"></div>
            
            <div class="cqSectionContent section1col processStepInApproval" id="inApprovalPlan" data-bind="{visible:isCOInApproval}"></div>

            <div class="clearFix"></div>
        </div>

        <div id="noteAndAttachmentSection" class="cqFormSection">
            <div data-role="cqgrid" class="cq-topGrid subGrid"
                data-source="CqChange.relatedList('Notes')"
                data-editable='{ "mode": "popup", "update":true, "create":true, "destroy":true}'
                data-columns='[{"title": "{!$Label.compliancequest__CQ_UI_Actions}","command": [{ "name": "edit", "text": { "update": "Save"} }, "destroy"], "width": "100px"},
           {"field": "IsPrivate","hidden": true},
           {"field": "Title"}, {"field": "Body"}]'>
            </div>
             <div data-role="cqgrid" id="filesGrid" class="cq-topGrid subGrid"
                 data-source="CqChange.relatedList('ContentDocumentLinks')"
                 data-collapsible="true"
                 data-editable='{"mode": "incell", "create":true, "update":true, "destroy":true}'
                 data-toolbar="[{name: 'custom', text: '{!$Label.compliancequest__CQ_UI_Add_Files}', className: 'addFiles', enable: 'CqChange.canEdit()', action: 'sqx.commonActions.addFiles'}]"
                 data-columns="[{title: '{!$Label.compliancequest__CQ_UI_Actions}', command: ['preview', 'destroy'], width: '110px'},
                               {field: 'ContentTitle', template: '#: data.Title() #', editor: cq.getFileEditor},
                               {field: 'ContentDescription', template: '#: data.Description() #', editor: cq.getFileEditor}]">
            </div>
			<div class="clearFix"></div>
        </div>

        <div id="impactSection" class = "cqFormSection">
            <div data-role = "cqgrid"
                class="cq-topGrid subGrid"
                data-source = "CqChange.jobFunction()"
                data-toolbar='[{"name" : "create", "text" : "{!$Label.compliancequest__CQ_UI_Add_Impacted_Job_Function}"}]'
                data-title = '{!$Label.compliancequest__CQ_UI_Impacted_Job_Functions}'
                data-collapsible="true"
                data-editable = '{"mode" : "popup", "update" : true, "create" : true,"destroy" : true}'
                data-columns = '[{"title" : "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{"name" : "edit", "text" : {"update" : "Save"} }, "destroy"], "width" : "100px"},
                {"field" : "compliancequest__SQX_Job_Function__c",
                template : getReferenceDisplayTemplate("compliancequest__SQX_Job_Function__c")},
                {"field" : "compliancequest__Description_of_Impact__c"}]'>
            </div>
            <div data-role = "cqgrid"
                class="cq-topGrid subGrid"
                data-source = "CqChange.metric()"
                data-toolbar='[{"name" : "create", "text" : "{!$Label.compliancequest__CQ_UI_Add_Impacted_Metric}"}]'
                data-title = '{!$Label.compliancequest__CQ_UI_Impacted_Metrics}'
                data-collapsible="true"
                data-editable = '{"mode" : "popup", "update" : true, "create" : true,"destroy" : true}'
                data-columns = '[{"title" : "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{"name" : "edit", "text" : {"update" : "Save"} }, "destroy"], "width" : "100px"},
                {"field" : "compliancequest__Metric_Name__c"},
                {"field" : "compliancequest__Expected_Metric_Change__c"},
                {"field" : "compliancequest__Description_of_Impact__c"}]'>
            </div>
            <div data-role = "cqgrid"
                class="cq-topGrid subGrid"
                data-source = "CqChange.internal()"
                data-toolbar='[{"name" : "create", "text" : "{!$Label.compliancequest__CQ_UI_Add_Internal_Impact}"}]'
                data-title = '{!$Label.compliancequest__CQ_UI_Internal_Impacts}'
                data-collapsible="true"
                data-editable = '{"mode" : "popup", "update" : true, "create" : true,"destroy" : true}'
                data-columns = '[{"title" : "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{"name" : "edit", "text" : {"update" : "Save"} }, "destroy"], "width" : "100px"},
                {"field" : "compliancequest__Org_Division__c"},
                {"field" : "compliancequest__Org_Business_Unit__c"},
                {"field" : "compliancequest__Org_Region__c"},
                {"field" : "compliancequest__Org_Site__c"},
                {"field" : "compliancequest__Description_of_Impact__c"} ]'>
            </div>
            <div data-role = "cqgrid"
                class="cq-topGrid subGrid"
                data-source = "CqChange.external()"
                data-toolbar='[{"name" : "create", "text" : "{!$Label.compliancequest__CQ_UI_Add_External_Impact}"}]'
                data-title = '{!$Label.compliancequest__CQ_UI_External_Impacts}'
                data-collapsible="true"
                data-editable = '{"mode" : "popup", "update" : true, "create" : true,"destroy" : true}'
                data-columns = '[{"title" : "{!$Label.compliancequest__CQ_UI_Actions}", "command" : [{"name" : "edit", "text" : {"update" : "Save"} }, "destroy"], "width" : "100px"},
                {"field" : "compliancequest__External_Identifier__c"},
                {"field" : "compliancequest__Description_of_Impact__c"}]'>
            </div>
            <div class="clearFix"></div>
        </div>

        <div id="changeRecordActivitySection" class="cqFormSection">

            <div data-role="cqgrid" class="cq-topGrid subGrid"
                data-source="CqChange.relatedList('compliancequest__SQX_Change_Order_Record_Activities__r')"
                data-columns="[
                                  {field: 'compliancequest__Activity__c'},
                                  {field: 'compliancequest__Comment__c'},
                                  {field: 'compliancequest__Modified_By__c'},
                                  {field: 'CreatedDate', template: '#: convertToLocalDateTime(CreatedDate) #', title: '{!$ObjectType.SQX_Change_Order__c.fields.CreatedDate.label}'}]">
            </div>

            <div class="clearFix"></div>
        </div>

        <div id="workflowSection" class="cqFormSection">
            <div class="cqSectionContent section1col">
                <div data-role="cqgrid" class="cq-topGrid subGrid"
                    data-source="CqChange.relatedList('compliancequest__SQX_Change_Order_Approvals__r')"
                    data-editable='{"mode": "popup","create":true, "update":true, "destroy":true}'
                    data-columns='[ {"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": [{ "name": "edit", "text": { "update": "Save"} },"destroy"], "width": "100px"},
                                    {field: "compliancequest__Step__c"},
                                    {field: "compliancequest__SQX_Job_Function__c"},
                                    {field: "compliancequest__SQX_User__c"}]'>
                </div>
            </div>

            <div class="cqSectionContent section1col" id ="processStep">
                <apex:relatedList list="ProcessSteps" pageSize="{!IF((JSENCODE($CurrentPage.parameters.printMode))=='true', PageSize, 100)}" ></apex:relatedList>
            </div>

            <div class="cqSectionContent section1col">
                <apex:relatedList list="OpenActivities" pageSize="{!PageSize}"/>
            </div>

            <div class="cqSectionContent section1col">
                <apex:relatedList list="ActivityHistories" pageSize="{!PageSize}"/>
            </div>
			<div class="clearFix"></div>
		</div>
	</div>

	<!-- Templates Beyond this line -->

	<script type="text/x-kendo-template" id="AttachmentEditTemplate">
        <iframe class="attachmentFrame" id="Attachment#= data.Id #" src="{!$Page.SQX_Upload_Attachment}?callback=attachmentCallBack&parentId=#= currentUser.Id #&uid=#= data.parentEntity().Id #&recordid=#= data.Id #" style="height: 145px;border: none;width: 100%;">
            Nothing Yet
        </iframe>
    </script>

	<script type="text/x-kendo-template" id="AttachmentEditTemplateChild">
        <iframe class="attachmentFrame" id="Attachment\\#= data.Id \\#" src="{!$Page.SQX_Upload_Attachment}?callback=attachmentCallBack&parentId=\\#= currentUser.Id \\#&uid=\\#= data.parentEntity().Id \\#&recordid=\\#= data.Id \\#" style="height: 145px;border: none;width: 100%;">
            Nothing Yet
        </iframe>
    </script>

	<script type="text/x-kendo-template" id="voidChangeTemplate">
		<div>
			<div class="cqSectionContent section1col">
				<div data-container="CqChange.compliancequest__Resolution_Code__c" data-role='cqcompositefield'  data-ignore-data-edit-rules="true" data-edit-mode="true"></div>
			</div>
		</div>
	</script>

	<script type="text/x-kendo-template" id="actionDetailTemplate">
        <div>
            # var templateName = 'actionGenericTemplate', prefix = data.isPlanRecordType() ? 'plan' : 'action';
            
            templateName = data.isTypeNew_Document() ? prefix + "NewDocumentTemplate" :
                           (data.isTypeRevise_Document() ? prefix + "ReviseDocumentTemplate" : 
                            (data.isTypeObsolete_Document() ? prefix + "ObsoleteDocumentTemplate" : templateName));
            #
            #= kendo.template($(document.getElementById(templateName)).html())(data) #
        </div>
    </script>
    <script type="text/x-kendo-template" id="actionGenericTemplate">
        <div><p>{!$Label.compliancequest__CQ_UI_Generic_Task}</p></div>
    </script>

    <script type="text/x-kendo-template" id="planNewDocumentTemplate">
        <div><p>{!$Label.compliancequest__CQ_UI_New_Document_Task}</p></div>
    </script>


    <script type="text/x-kendo-template" id="planReviseDocumentTemplate">
        <div>
            <p>{!$Label.compliancequest__CQ_UI_Revise_Document_Task}</p>
            # if(data.compliancequest__SQX_Controlled_Document__r) { #
                <p>{!$Label.compliancequest__CQ_UI_Document_To_Revise} <a href='#: sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document__c) #' target="_blank">#: data.compliancequest__SQX_Controlled_Document__r.Name #</a>
            # } #
        </div>
    </script>

    <script type="text/x-kendo-template" id="planObsoleteDocumentTemplate">
        <div>
            <p>{!$Label.compliancequest__CQ_UI_Obsolete_Document_Task}</p>
            # if(data.compliancequest__SQX_Controlled_Document__r) { #
                <p>{!$Label.compliancequest__CQ_UI_Document_To_Obsolete} <a href='#:sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document__c) #' target="_blank">#: data.get('compliancequest__SQX_Controlled_Document__r.Name') #</a> </p>
            # } #
        </div>
    </script>

    <script type="text/x-kendo-template" id="actionNewDocumentTemplate">
        <div><p>{!$Label.compliancequest__CQ_UI_New_Document_Task}</p></div>
        # if(data.get('compliancequest__SQX_Controlled_Document_New__c') == null && (data.isOpen() || data.HasChanged('compliancequest__Status__c'))) { #
            <a href='#= data.urlForAction() #'>{!$Label.compliancequest__CQ_UI_Create_New_Document}</a>
        # } else if(data.compliancequest__SQX_Controlled_Document_New__r){ #
            <p>{!$Label.compliancequest__CQ_UI_New_Document}: <a href='#:sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document_New__c) #' target="_blank">#: data.get('compliancequest__SQX_Controlled_Document_New__r.Name') #</a></p>

            <p>{!SUBSTITUTE($Label.compliancequest__CQ_UI_Remaining_Trainee, '{NumberOfTrainees}', '(#: data.get(\"compliancequest__Number_of_Trainees_Remaining__c\") #)')}</p>
        # } #
    </script>


    <script type="text/x-kendo-template" id="actionReviseDocumentTemplate">
        <div>
            <p>{!$Label.compliancequest__CQ_UI_Revise_Document_Task}</p>
            # if(data.compliancequest__SQX_Controlled_Document__r) { #
                <p>{!$Label.compliancequest__CQ_UI_Document_To_Revise}: <a href='#:sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document__c) #' target="_blank">#: data.get('compliancequest__SQX_Controlled_Document__r.Name') #</a></p>
            # } #
            # if(data.get('compliancequest__SQX_Controlled_Document_New__c') == null && (data.isOpen() || data.HasChanged('compliancequest__Status__c'))) { #
                <a href='#= data.urlForAction() #'>{!$Label.compliancequest__CQ_UI_Click_To_Revise}</a>
            # } else if(data.compliancequest__SQX_Controlled_Document_New__r) { #
                <p>{!$Label.compliancequest__CQ_UI_Document_Revised_To}: <a href='#:sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document_New__c) #' target="_blank">#: data.get('compliancequest__SQX_Controlled_Document_New__r.Name') #</a></p>
                <p>{!SUBSTITUTE($Label.compliancequest__CQ_UI_Remaining_Trainee, '{NumberOfTrainees}', '(#: data.get(\"compliancequest__Number_of_Trainees_Remaining__c\") #)')}</p>
                # if (data.urlForDocCompare()) { #
                    <p><a href="#= data.urlForDocCompare() #" target="_blank">{! $Label.compliancequest__CQ_UI_Click_To_Compare }</a></p>
                # } #
            # } #
        </div>
    </script>
    
    <script type="text/x-kendo-template" id="actionObsoleteDocumentTemplate">
        <div>
            <p>{!$Label.compliancequest__CQ_UI_Obsolete_Document_Task}</p>
            # if(data.compliancequest__SQX_Controlled_Document__r) { #
                <p>{!$Label.compliancequest__CQ_UI_Document_To_Obsolete} <a href='#:sqx.common.getObjectUrl(data.compliancequest__SQX_Controlled_Document__c) #' target="_blank">#: data.get('compliancequest__SQX_Controlled_Document__r.Name') #</a> </p>
            # } #
            # if(!data.relatedDocIsObsolete() && data.isOpen()){ #
                <a href='#= data.urlForAction() #'>{!$Label.compliancequest__CQ_UI_Click_To_Obsolete}</a>
            # } #
        </div>
    </script>

    <script type="text/x-kendo-template" id="signOffDocumentTemplate">
            # if(data.compliancequest__Status__c == 'Complete') { #
                <p>{!SUBSTITUTE($Label.compliancequest__CQ_UI_Completed_By, '{CompletedBy}', '#: data.get(\"compliancequest__Completed_By__c\")  ?  data.get(\"compliancequest__Completed_By__c\") : \"\" #')}</p>
                <p>{!SUBSTITUTE($Label.compliancequest__CQ_UI_Completed_On, '{CompletedOn}', '#:  getFormattedDate(data.get(\"compliancequest__Completion_Date__c\") )#')}</p>
                <p>#: data.get('compliancequest__Remark__c') ?  data.get('compliancequest__Remark__c') : ''#</p>
            # } #
    </script>

    <script type="text/x-kendo-template" id="actionGridDetailTemplate">
        <div data-role="cqtabstrip" data-collapsible="true" data-show-grid-title="true" data-show-grid-count="true" class="tabStrip">
            <ul>
                <li class="k-state-active">{!$Label.compliancequest__CQ_UI_SF_Tasks}</li>
                <li data-grid-sobject="ContentDocumentLink">{!$Label.compliancequest__CQ_UI_Files}</li>
            </ul>
            # if(sqx.common.isSFID(data.Id)) { #
            <div class="loading-iframe">
                <iframe src="{!$Page.SQX_Implementation_Attachment}?id=#=data.Id#" style="border: none; width: 100%; height: 200px; overflow-y: scroll;" 
                    onload="sqx.print.frameLoadComplete(this);"></iframe>
            </div>
            # } else { #
            <div>
                <p>{!$Label.compliancequest__CQ_UI_Save_Changes_To_Add_Plan}</p>
            </div>
            # } #
            <div>
                <div class="subGrid"
                         data-relatedlist="ContentDocumentLinks"
                         data-toolbar='[{"name": "custom", "text": "{!$Label.compliancequest__CQ_UI_Add_Files}", "className": "addFiles", "action": "sqx.commonActions.addFiles", "enable": "#: data.canAddChildren(SQXSchema.ContentDocumentLink.sObjectName) #"}]'
                         data-options='{"editable":{"mode":"incell","update":#= data.rowEditable ? data.rowEditable() : true #,"create":#= data.rowEditable ? data.rowEditable() : true #,"destroy":#= data.rowEditable ? data.rowEditable() : true # },"showTitle":false,"collapsible":false}'
                         data-columns='[{"title": "{!$Label.compliancequest__CQ_UI_Actions}", "command": ["preview", "destroy"], "width": "110px"},
                                        {"field": "ContentTitle", "template": "\\#: data.Title() \\#", "editor": cq.getFileEditor},
                                        {"field": "ContentDescription", "template": "\\#: data.Description() \\#", "editor": cq.getFileEditor}]'>
                 </div>
            </div>
        </div>
    </script>

    <script type="text/x-kendo-template" id="releaseChangeTemplate">
        <div>
            <div class="cqSectionContent section1col">
                <div data-container="CqChange.compliancequest__Resolution_Code__c" data-role='cqcompositefield' data-edit-mode="true" data-ignore-data-edit-rules="true"></div>
                <div data-container="CqChange.compliancequest__Effective_Date__c" data-role='cqcompositefield' data-edit-mode="true" data-ignore-data-edit-rules="true" class="effectiveDate"></div>
            </div>
        </div>
    </script>


    <script type="text/x-kendo-template" id="ControlledDocDropDownTemplate">
        <div>
            <p>#: data.Name # (#: data.compliancequest__Document_Status__c #)</p>
            <em>#: data.compliancequest__Title__c #</em>
        </div>
    </script>
    
    <script id="approveRejectChangeOrderTemplate" type="text/x-jquery-tmpl">
        <div id="approveRejectChangeOrder">
            <div style="padding:10px 20px 10px 10px;">
                <div class="section1col">
                    <!-- Meaning of signature -->
                    <div class="formItem">
                        <div class="formLabel"><label for="respondMeaningOfSignature">{!$Label.compliancequest__CQ_UI_Purpose_Of_Signature}</label></div>
                        <div class='formField' name='respondMeaningOfSignature'><span></span></div>
                    </div>
                    <!-- ****** -->
                    <div class="formItem">
                        <div class="formLabel"><label for="approvalRemark">{!$Label.compliancequest__CQ_UI_Comment}</label></div>
                        <div class="formField">
                            <div class="editableElement">
                                <textarea id="approvalRemark" class="cq-multi-line-textarea" name="Comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <div  data-current-user-details-var="userDetails"  data-role='cqelectronicsignature'></div>
                </div>
                <div class="clearFix"></div>
                <div class="commandSection" style="text-align:center;margin-top:10px">
                    <div>
                        <div class="errorStyle errorDiv">
                            <span class="k-icon k-warning"> </span> <span class="msg"></span>
                        </div>
                    </div>
                    <button type="button" id="btnApproveApproval" onclick="approveRejectChangeOrder(event, 'approveapproval', '#= data.workItemId #')" class="cqCommandButton k-button approve" data-role="button" role="button" aria-disabled="false" tabindex="0">{!$Label.compliancequest__CQ_UI_Approve}</button>
                    <button type="button" id="btnRejectApproval" onclick="approveRejectChangeOrder(event, 'rejectapproval', '#= data.workItemId #')" class="cqCommandButton k-button reject" data-role="button" role="button" aria-disabled="false" tabindex="0">{!$Label.compliancequest__CQ_UI_Reject}</button>
                    <button type="button" id="btnCancelApproval" onclick="cancelChangeOrderApproval(event)" class="cqCommandButton k-button" data-role="button" role="button" aria-disabled="false" tabindex="0">{!$Label.compliancequest__CQ_UI_Cancel}</button>
                </div>
            </div>
        </div>
    </script>
    <script id="PartDropdownTemplate" type="text/x-kendo-template">
        <div>
            <p>
                <strong>Part Number:</strong>
                #: compliancequest__Part_Number__c #
            </p>
            <p>
                <strong>Name:</strong>
                #: Name #
            </p>
        </div>
    </script>
    
    <style>
        #docThumbnailsSectionHeader {
            margin-left: 0px;
            margin-right: 0px;
            padding-top: 2px;
        }
        #docThumbnailsSection {
            border: 1px solid #ececec;
            margin-bottom: 3px;
        }
        .thbContainer {
            float: left;
            padding: 5px 0px;
            text-align: center;
        }
        .thbContainer a:focus {
            border: none;
        }
        .thbError {
            border: solid 1px black;
            display: table;
            height: 180px;
            margin: 0px 5px;
            width: 127px;
        }
        .thbError span{
            display: table-cell;
            vertical-align: middle;
        }
        .thbImage {
            border: solid 1px black;
            margin: 0px 5px;
            max-height: 180px;
        }
        .thbTitle {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
    
    <script type="text/x-kendo-template" id="newOrRevisedDocumentThumbnailsTemplate">
        <div class="thbContainer">
            <center>
                <a href="#: cq.insertParamtoURL('id', data.compliancequest__SQX_Controlled_Document_New__c, sqx.urls.docViewContent) #" target="_blank">
                    <img class="thbImage" src="{!URLFOR($Resource.cqui, '/styles/Silver/loading_2x.gif')}" />
                    <div class="thbError" style="display: none;"><span>{!$Label.compliancequest__CQ_UI_Thumbnail_Not_Available}<span></div>
                    <div class="thbTitle">#: data.get('compliancequest__SQX_Controlled_Document_New__r.Name') #</div>
                </a>
                # if (data.urlForDocCompare()) { #
                    <p><a href="#= data.urlForDocCompare() #" target="_blank">{! $Label.compliancequest__CQ_UI_Compare }</a></p>
                # } #
            </center>
        </div>
    </script>
    
</apex:page>