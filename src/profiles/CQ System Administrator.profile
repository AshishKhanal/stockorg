<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <applicationVisibilities>
        <application>CQ</application>
        <default>false</default>
        <visible>true</visible>
    </applicationVisibilities>
    <applicationVisibilities>
        <application>CQ_Admin</application>
        <default>true</default>
        <visible>true</visible>
    </applicationVisibilities>
    <custom>true</custom>
    <description>CQ System Administrator profile. Use permissionset instead of profile</description>
    <layoutAssignments>
        <layout>SQX_Assessment__c-Assessment</layout>
        <recordType>SQX_Assessment__c.CQ_Assessment</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Assessment__c-SCORM Layout</layout>
        <recordType>SQX_Assessment__c.SCORM_Assessment</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-CQ Content</layout>
        <recordType>ContentVersion.Controlled_Content</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Controlled_Document__c-Controlled Document</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Controlled_Document__c-Controlled Document</layout>
        <recordType>SQX_Controlled_Document__c.Controlled_Document</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Controlled_Document__c-Template Document</layout>
        <recordType>SQX_Controlled_Document__c.Template_Document</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Finding__c-Audit Layout</layout>
        <recordType>SQX_Finding__c.Audit_Finding</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Finding__c-Capa Layout</layout>
        <recordType>SQX_Finding__c.CAPA_Finding</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Finding__c-Finding Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SQX_Finding__c-NC Layout</layout>
        <recordType>SQX_Finding__c.NC_Finding</recordType>
    </layoutAssignments>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>SQX_Assessment__c.CQ_Assessment</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>SQX_Assessment__c.SCORM_Assessment</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>ContentVersion.Controlled_Content</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>ContentVersion.Controlled_Document</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>ContentVersion.SCORM_Content</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>ContentVersion.Template_Document</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>SQX_Controlled_Document__c.Controlled_Document</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>SQX_Controlled_Document__c.Template_Document</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>SQX_Finding__c.Audit_Finding</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>SQX_Finding__c.CAPA_Finding</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>SQX_Finding__c.NC_Finding</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <userLicense>Salesforce</userLicense>
</Profile>
