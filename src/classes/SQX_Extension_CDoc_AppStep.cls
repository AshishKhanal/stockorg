/**
* extension for the controlled document approval step
*/ 
public with sharing class SQX_Extension_CDoc_AppStep {
    
    ApexPages.StandardController controller;
    SQX_Controlled_Document_Approval__c rec;
    
    public SQX_Extension_CDoc_AppStep (ApexPages.StandardController sc) {
        controller = sc;
        rec = (SQX_Controlled_Document_Approval__c) sc.getRecord();

        // if doc id is not null, sets the id of the parent doc in the document approval
        try{
            Id docId = (Id)ApexPages.currentPage().getParameters().get('docId');
            if (docId != null)
                rec.SQX_Controlled_Document__c = docId;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error occurred while setting Controlled Document. Actual Error: '+ ex.getMessage()));
        }
    }
    
    /**
    * get the users which belongs to the selected job function
    */ 
    public List<SelectOption> getJobFunctionUsers() {
        
        List<SelectOption> users = new List<SelectOption>();
        users.add(new SelectOption('', '--None--'));
        
        List<SQX_Job_Function.SQX_JobFunctionUser_Wrapper> activeUsers = SQX_Job_Function.getActiveUsers(rec.SQX_Job_Function__c, null);
        
        boolean isValidUser = false;
        
        for (SQX_Job_Function.SQX_JobFunctionUser_Wrapper uw : activeUsers) {
            if(uw.Id == rec.SQX_User__c){
                isValidUser = true;
            }
            users.add(new SelectOption(uw.Id, uw.Name));
        }
        
        if(!isValidUser){
            rec.SQX_User__c = null;
        }
        
        return users;
    }
    
    /**
    * returns to controlled doc when the cancel button is clicked
    */ 
    public PageReference returnMainRecord() {
        PageReference redirectApprovalStepEditPage;
        redirectApprovalStepEditPage = new ApexPages.StandardController(new SQX_Controlled_Document__c(Id = rec.SQX_Controlled_Document__c)).view();
        redirectApprovalStepEditPage.setRedirect(true);
        return redirectApprovalStepEditPage;
    }
    
    /**
    * saves the record if the id is null else updates the record
    */ 
    public void saveRecord(){
        if(rec.Id == null){
            new SQX_DB().op_insert(new List<SQX_Controlled_Document_Approval__c>{rec}, new List<SObjectField>{});   
        }else{
            new SQX_DB().op_update(new List<SQX_Controlled_Document_Approval__c>{rec}, new List<SObjectField>{SQX_Controlled_Document_Approval__c.Step__c,
                SQX_Controlled_Document_Approval__c.SQX_Job_Function__c,
                SQX_Controlled_Document_Approval__c.SQX_User__c});                
        }
        
    }
    
    /**
    * save the document approval step and redirects to the main controlled doc
    */ 
    public PageReference saveOnly(){
        PageReference saveOnlyPage = null;
        try{
            saveRecord(); 
            saveOnlyPage = new ApexPages.StandardController(new SQX_Controlled_Document__c(Id = rec.SQX_Controlled_Document__c)).view();
            saveOnlyPage.setRedirect(true);
        }catch (Exception ex) {
            // display unknown exceptions
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return saveOnlyPage;
    }
    
    
    /**
    * saves the document aproval step and redirects to the new document approval create screen
    */ 
    public PageReference saveAndNew(){
        PageReference saveAndNewPage = null;
        try{
            saveRecord();
            saveAndNewPage = Page.SQX_CDoc_AppStep;
            saveAndNewPage.getParameters().put('docId', rec.SQX_Controlled_Document__c);
            saveAndNewPage.setRedirect(true);
        }catch (Exception ex) {
            // display unknown exceptions
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return saveAndNewPage;
    }
}