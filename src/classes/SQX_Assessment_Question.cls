/**
* This class will contain the common methods and static strings required for the Assessment Question object 
*/
public with sharing class SQX_Assessment_Question {
    public static final String QUESTION_NAVIGATION_END = 'End',
                               QUESTION_NAVIGATION_NEXT = 'Next';
    
    /**
     * Assessment Question Trigger Handler
     */                          
    public with sharing class Bulkified extends SQX_BulkifiedBase{
                
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newAssessment equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Assessment_Question__c> newAssessmentQuestion, Map<Id, SQX_Assessment_Question__c> oldMap){
            super(newAssessmentQuestion, oldMap);
        }
    }
}