/**
* This test class checks for the following functionality in the document folder management.
*/
@isTest
public class SQX_Test_2481_Doc_Folder_Management {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * Given : given controlled document effective date and User Object filter
    * When  : when we search and click the folder structure
    * Then  : matching documents are returned
    */ 
    public testmethod static void givenFilter_WhenSearchandClick_ReturnMatchingDocuments(){
        final String EMPTY_FILTER = '{}', EMPTY_TEXT = '';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standUser= allUsers.get('standardUser');
        System.runAs(adminUser){
            // Arrange: Create four documents
            List<SQX_Test_Controlled_Document> docList = new List<SQX_Test_Controlled_Document>();
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document();
            SQX_Test_Controlled_Document doc3 = new SQX_Test_Controlled_Document();
            SQX_Test_Controlled_Document doc4 = new SQX_Test_Controlled_Document();
            
            doc1.doc.Effective_Date__c=Date.today();
            doc1.doc.OwnerId=adminUser.Id;
            doc1.save();
            
            doc2.doc.Effective_Date__c=Date.today().addDays(-40);
            doc2.doc.OwnerId=standUser.Id;
            doc2.save();
            
            doc3.doc.Effective_Date__c=Date.today().addDays(30);
            doc3.doc.OwnerId=adminUser.Id;
            doc3.save();
            
            doc4.doc.Effective_Date__c=Date.today().addDays(-35);
            doc4.doc.OwnerId=adminUser.Id;
            doc4.save();
            
            Test.startTest();

            // Act: Query using document filter by Effective_Date  greater than or equal to current date
            String documentFilterString = '{"field":"compliancequest__Effective_Date__c","operator":"gte","usv_function":"addDays","usv_param":"0"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString,null, null, null).documents;
            
            // Assert: Ensure that two are four documents
            System.assertEquals(2, matchingDocuments.size());
            
            // Act1: Query using document filter by Effective_Date  greater than or equal to Today - 30days
            String documentFilterString1 = '{"field":"compliancequest__Effective_Date__c","operator":"gte","usv_function":"addDays","usv_param":"-30"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments1 = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString1,null, null, null).documents;
            
            // Assert1: Ensure that two are four documents
            System.assertEquals(2, matchingDocuments1.size());
            
            // Act2: Query using document filter by Effective_Date  greater than or equal to Today + 30days
            String documentFilterString2 = '{"field":"compliancequest__Effective_Date__c","operator":"gte","usv_function":"addDays","usv_param":"30"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments2 = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString2,null, null, null).documents;
            
            // Assert2: Ensure that one are four documents
            System.assertEquals(1, matchingDocuments2.size());
            
            // Act3: Query using document filter by user object
            String documentFilterString3 = '{"field":"OwnerId","operator":"eq","usv_function":"getUserInfo","usv_param":"Id"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments3 = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString3,null, null, null).documents;
            
            // Assert3: Ensure that three are four documents because doc2 OwnerId is Standerd User
            System.assertEquals(3, matchingDocuments3.size());
            
            // Act4: Query using document filter by personnel object
            String documentFilterString4 = '{"field":"OwnerId","operator":"eq","usv_function":"getPersonnelInfo","usv_param":"compliancequest__SQX_User__c"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments4 = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString4,null, null, null).documents;
            
            // Assert4: Ensure that zero documents
            System.assertEquals(0, matchingDocuments4.size());
            
            Test.stopTest();
        }
    }
}