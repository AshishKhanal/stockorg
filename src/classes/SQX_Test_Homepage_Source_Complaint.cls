/**
 *   This class contains test cases for ensuring that NC source is providing homepage items with desired values
 *   @author - Anuj Bhandari
 *   @date - 12-04-2017
 */

@isTest
public class SQX_Test_Homepage_Source_Complaint {

    /*
        Complaint source currently returns items of type :
        1. Open/Draft/Triage Complaint Items

    */

    @testSetup
    /*
     * Creates the complaint items of different vaild statuses
     */
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser) {

            /* Creating a Draft Complaint */
            SQX_Test_Complaint itmDraft = new SQX_Test_Complaint(adminUser);
            itmDraft.setStatus(SQX_Complaint.STATUS_DRAFT);
            itmDraft.complaint.Complaint_Title__c = 'Test Complaint';
            itmDraft.save();

            /* Creating an open Complaint */
            SQX_Test_Complaint itmOpen = new SQX_Test_Complaint(adminUser);
            itmOpen.setStatus(SQX_Complaint.STATUS_OPEN);
            itmOpen.save();

            /* Creating a triage Complaint */
            SQX_Test_Complaint itmTriage = new SQX_Test_Complaint(adminUser);
            itmTriage.setStage(SQX_Complaint.STAGE_TRIAGE);
            itmTriage.save();

        }
    }


    /**
     *   Given : A set of Complaint items (open, draft and triage) and a homepage component expecting records from Complaint Source
     *   When :  Complaint Record is fetched and processed
     *   Then :  All complaint record types are added to the homepage with desired values
     *   @story -3168
     */
    testmethod static void givenComplaintItemsWithDifferentStatus_WhenRecordsAreFetchedAndProcessed_ThenCorrepondingHomepageItemsShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange1: Fetch Complaint Items
            List <SQX_Complaint__c> itms = [SELECT
                                                    Id,
                                                    Name,
                                                    Complaint_Title__c,
                                                    Record_Stage__c,
                                                    Status__c,
                                                    CreatedDate
                                                    FROM SQX_Complaint__c
                                                ];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Complaint_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Complaint Items have been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size()>= 3, 'Expected at-least three items');

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_COMPLAINT, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                SQX_Homepage_Item_Action act = hpItem.actions.get(0);

                System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

                System.assertEquals(false, act.supportsBulk);

                for (SQX_Complaint__c itm: itms) {
                    if (itm.id == hpItem.itemId) {
                        if (itm.Status__c == SQX_Complaint.STATUS_DRAFT) {
                            if(itm.Record_Stage__c == SQX_Complaint.STAGE_DRAFT){
                                System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);
                            } else if(itm.Record_Stage__c ==  SQX_Complaint.STAGE_TRIAGE){
                                System.assertEquals(SQX_Homepage_Constants.ACTION_INITIATE, act.name);
                            }

                            validateDraftItem(itm, hpItem);
                            break;

                        } else if (itm.Status__c == SQX_Complaint.STATUS_OPEN) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

                            validateOpenItem(itm, hpItem);
                            break;

                        } else if (itm.Record_Stage__c == SQX_Complaint.STAGE_TRIAGE) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_INITIATE, act.name);

                            validateTriageItem(itm, hpItem);
                            break;

                        } else {

                            System.assert(false, 'Unknown record : ' + hpItem);

                        }
                    }
                }
            }

        }

    }


    /**
     *   Method validates the homepage item of type Complaint-Draft
     */
    private static void validateDraftItem(SQX_Complaint__c itm, SQX_Homepage_Item hpItem) {
        
        if(itm.Record_Stage__c == SQX_Complaint.STAGE_TRIAGE){
            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);
            String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, new String[] {
                itm.Name, String.isBlank(itm.Complaint_Title__c) ? '' : itm.Complaint_Title__c
            });

            System.assertEquals(expectedFeedText, hpItem.feedText);
        } else {
            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);
            String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, new String[] {
                itm.Name, String.isBlank(itm.Complaint_Title__c) ? '' : itm.Complaint_Title__c
            });

            System.assertEquals(expectedFeedText, hpItem.feedText);
        }


    }


    /**
     *   Method validates the homepage item of type Complaint-Open
     */
    private static void validateOpenItem(SQX_Complaint__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS, hpItem.actionType);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, new String[] {
            itm.Name, String.isBlank(itm.Complaint_Title__c) ? '' : itm.Complaint_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item of type Complaint-Triage
     */
    private static void validateTriageItem(SQX_Complaint__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, new String[] {
            itm.Name, String.isBlank(itm.Complaint_Title__c) ? '' : itm.Complaint_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

        System.assertEquals(false, hpItem.supportsBulk);
    }
}
