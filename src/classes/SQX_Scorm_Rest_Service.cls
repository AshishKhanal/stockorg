/**
* Rest resource to handle scorm responses
* Note : 'with sharing' is omitted as this service will be requested by Site Guest User who may not have the necessary sharing permissions
* DEPRECATED since 7.4.0
*/
@RestResource(urlMapping='/scorm')
global class SQX_Scorm_Rest_Service {
    @HttpPost
    global static void Post(){}
}