/**
 * @story SQX-847
 * @author Sagar Shrestha
 * @date 2014/12/08
 */
@isTest
public class SQX_Test_847_ESSetting{

    public static testmethod void givenESsetting_shouldBeSaved(){
        SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();
        SQX_CQ_Electronic_Signature__c esSetting= estest.esSettings;
        esSetting.Consumer_Secret__c='abcd';
        esSetting.Consumer_Key__c='bcf';
        System.PageReference saveResult= estest.saveElectronicSignatureSetting();
        System.assertEquals(saveResult.getUrl(), new PageReference('/apex/SQX_ES_Settings').getUrl());//should be SQX_ES_Settings
        SQX_CQ_Electronic_Signature__c esSettingSaved= SQX_CQ_Electronic_Signature__c.getInstance();
        System.assertEquals(esSettingSaved.Consumer_Secret__c, esSetting.Consumer_Secret__c);
        System.assertEquals(esSettingSaved.Consumer_Key__c, esSetting.Consumer_Key__c);
    }
}