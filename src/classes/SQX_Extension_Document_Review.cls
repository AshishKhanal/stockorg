global with sharing class SQX_Extension_Document_Review {

    private SQX_Document_Review__c mainRecord = null;
    private SQX_Controlled_Document__c controlledDoc = null;

    private ApexPages.StandardController controller = null;
    private Id stdDocRecTypeId;
    
    global String NewRevision {get; set;}
    
    global SQX_Controlled_Document__c getControlledDocument() {
        return controlledDoc;
    }
    
    global boolean getIsStandardDoc() {
        return (controlledDoc.RecordTypeId == null ? false : controlledDoc.RecordTypeId == stdDocRecTypeId);
    }
    
    // Holds record for doc fields referenced by field set (SQX-3664)
    global SQX_Controlled_Document__c revisionDoc{ get; set; }

    /**
    * controller
    */
    global SQX_Extension_Document_Review(ApexPages.StandardController controller){

        // overrided to copy fieldset field value to the revised document[SQX-3664]
        this( controller, Schema.SObjectType.SQX_Controlled_Document__c.fieldSets.Revise_Document_Edit_Fields.getFields() );
    }

    /**
     * This constructor is being used to copy fieldset field value to the revised document (SQX-3664) along with other required operations.
     */
    public SQX_Extension_Document_Review ( ApexPages.StandardController controller, List<Schema.FieldSetMember> fieldSetMembers ) {
        mainRecord = (SQX_Document_Review__c)controller.getRecord();
        this.controller = controller;
        stdDocRecTypeId = SQX_Controlled_Document.getControlledDocTypeId();

        if(controlledDoc == null){
            String docId = ApexPages.currentPage().getParameters().get('controlleddocid');
            SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
            filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter.field = 'Id';
            filter.value = docId;
            List<SQX_Controlled_Document__c> controlledDocs = (List<SQX_Controlled_Document__c>) SQX_DynamicQuery.getAllFields(SQX_Controlled_Document__c.SObjectType.getDescribe(), new Set<Schema.SObjectField>{}, filter,
                                                                                    new List<Schema.SObjectField>{ SQX_Controlled_Document__c.Document_Number__c }, true);

            if (controlledDocs.size() == 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Controlled document needs to be specified for review.'));
            }
            else{
                controlledDoc = controlledDocs.get(0);
                NewRevision = SQX_Utilities.getNextRev(SQX_Controlled_Document.getLastRevisionNumber(controlledDoc.Document_Number__c));
                mainRecord.Controlled_Document__c = controlledDoc.Id;
                
                // copy only those fields that are ony available in fieldset for use.
                revisionDoc = new SQX_Controlled_Document__c();
                for ( FieldSetMember m :  fieldSetMembers) {
                    revisionDoc.put(m.getFieldPath(), controlledDoc.get(m.getFieldPath()));    
                }
            }
        }
        
        if (ApexPages.currentPage().getParameters().containsKey(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION) || ApexPages.currentPage().getParameters().containsKey(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId)) {
            try {
                // get implementation id from url
                Id implementationId = Id.valueOf(ApexPages.currentPage().getParameters().get(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION));
                SQX_Implementation__c implementation = [SELECT SQX_Change_Order__c, CQ_Aware_Type__c,Type__c,Title__c, Description__c
                                                        FROM SQX_Implementation__c WHERE Id = :implementationId];
                
                // set change order of the passed implementation
                mainRecord.SQX_Obsolete_Document_Change_Order__c = implementation.SQX_Change_Order__c;
                
                // check retire decision value before setting as this has been renamed / customized for some clients eg: Stryker
                Set<String> reviewDecisionPicklistValues = SQX_Utilities.getPicklistValues(SQX.DocumentReview, SQX.getNSNameFor('Review_Decision__c'));
                if (reviewDecisionPicklistValues.contains(SQX_Document_Review.REVIEW_DECISION_RETIRE)) {
                    mainRecord.Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE;
                }
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,
                    '<em>Related Task</em><br/><span><strong>Task Type:</strong> ' + implementation.Type__c
                    + '</span><br/><span><strong>Title:</strong> ' + implementation.Title__c + '</span>'));
            }
            catch (Exception ex) {
                // ignore the error since this shouldn't stop the page from working
            }
        }
    }
    
    /**
     * This method calculates and sets the default value of next review date as Performed_Date__c + Review_Interval__c if the review decision is Continue Use.
     * It also sets today's date as the default value for Performed_Date__c if it is null.
     */
    global void setDefaultNextReviewDate() {
        mainRecord.Performed_Date__c = mainRecord.Performed_Date__c != null ? mainRecord.Performed_Date__c : System.today(); 
        mainRecord.Next_Review_Date__c = mainRecord.Review_Decision__c == SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE ? mainRecord.Performed_Date__c.addDays(Integer.valueOf(controlledDoc.Review_Interval__c)) : null;
    }
    
    /**
    * returns true if the current review is of revise type
    */
    global Boolean getIsRevise(){
        return mainRecord.Review_Decision__c == SQX_Document_Review.REVIEW_DECISION_REVISE;
    }
    
    /**
    * returns true if the current review is of Continue to Use type
    */
    global Boolean getIsContinue(){
        return mainRecord.Review_Decision__c == SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE;
    }
    
    /**
    * returns true if the current review is of Retire type
    */
    global Boolean getIsRetire(){
        return mainRecord.Review_Decision__c == SQX_Document_Review.REVIEW_DECISION_RETIRE;
    }
    
    /**
    * Checks if primary content data of the controlled document is large or not
    * @return <code>true</code> if primary content size is large
    */
    global Boolean getHasLargePrimaryContent() {
        return SQX_Controlled_Document.hasLargePrimaryContentData(controlledDoc);
    }


    private Boolean canReviseFetched = false;
    private Boolean canRevise = false;

    /**
    * returns true if a document can be revised else returns false.
    */
    global Boolean getCanRevise(){
        if(!canReviseFetched){
            if(controlledDoc == null){
            	return false;
            }

            canRevise = SQX_Controlled_Document.canReviseDocument(controlledDoc.Id, UserInfo.getUserId());

            if(!canRevise){
                if(controlledDoc.Document_Status__c != SQX_Controlled_Document.STATUS_CURRENT)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Controlled Document must be current to add a review.'));
                else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Insufficient access to review the document.'));
            }

            canReviseFetched = true;
        }

        return canRevise;
    }
    
    /**
    * returns valid change orders with retire document action related to the controlled document of the document review
    */ 
    global List<SelectOption> getChangeOrdersToRetireDocument() {
        List<SelectOption> changeOrders = new List<SelectOption>();
        changeOrders.add(new SelectOption('', '--None--'));
        
        Set<String> openCOStatuses = SQX_Document_Review.getChangeOrderStatusesRetiringDocument();
        
        // get open change orders with retire document action for the related document
        List<SQX_Change_Order__c> coList = [SELECT Id, Name
                                            FROM SQX_Change_Order__c
                                            WHERE Id IN (SELECT SQX_Change_Order__c
                                                         FROM SQX_Implementation__c
                                                         WHERE (CQ_Aware_Type__c = :SQX_Implementation.AWARE_DOCUMENT_OBSOLETE
                                                            OR compliancequest__Type__c=:SQX_Implementation.AWARE_DOCUMENT_OBSOLETE) 
                                                            AND SQX_Controlled_Document__c = :mainRecord.Controlled_Document__c)
                                                AND Status__c IN :openCOStatuses];
        
        // populate obtained change orders
        for (SQX_Change_Order__c co : coList) {
            changeOrders.add(new SelectOption(co.Id, co.Name));
        }
        
        return changeOrders;
    }

    /**
    * Saves the review associated with the document. 
    * If the review is type of 'Revise' then a new controlled document with the provided revision number is used.
    */
    global PageReference SubmitReview(){
        PageReference returnUrl = null;
        SavePoint sp = null;

        try{

            /**
            * Ensure that person trying to submit the review has at least read/write rights on Sharing setting
            * We are using without sharing() because Controlled Doc will have been locked by approval process
            * So any non-admin user will be unable to add any review.
            * We don't want to unlock the record to just insert review, because we will then have to manage a lot
            * of other issues
            * Instead we can allow the user to submit review using this vf page route only.
            */

            UserRecordAccess controlledDocAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE
                                                    RecordId = : mainRecord.Controlled_Document__c AND
                                                    UserId = : UserInfo.getUserId() ];

            
            if(controlledDocAccess.HasEditAccess){
                Id redirectToId = mainRecord.Controlled_Document__c;

                //set savepoint
                sp = Database.setSavePoint();

                Database.SaveResult saveResult = new SQX_DB().continueOnError().withoutSharing().op_insert(new List<SQX_Document_Review__c>{mainRecord}, new List<Schema.SObjectField>{

                }).get(0);

                //if there was no issue while saving the doc
                if(saveResult.isSuccess()){

                    //if the document review is Revise type, then revise the document 
                    if(getIsRevise()){
                        if(mainRecord.Controlled_Document__c != null){
                            //revise the document

                            SQX_Controlled_Document_Builder builder = new SQX_Controlled_Document_Builder(controlledDoc).revise(NewRevision, null, null, controlledDoc.Document_Type__c, null, revisionDoc);
                            
                            Id newDocID = builder.save();
                            

                            saveResult = builder.lastSaveResult;
                            
                            //set the redirection to newly created document.
                            redirectToId  = newDocId != null ? newDocId : redirectToId;
                        }
                    }

                    returnUrl = new apexPages.StandardController(redirectToId.getSObjectType().newSObject(redirectToId)).view();
                    returnUrl.setRedirect(true);
                }


                //add errors
                if(!saveResult.isSuccess()){
                    //add the errors that occurred while save the doc.
                    for(Database.Error error : saveResult.getErrors()){
                        String msg = error.getMessage();

                        if(msg.contains('Document_And_Rev__c')){

                            SQX_Controlled_Document__c dupDoc = [SELECT Id, Name FROM SQX_Controlled_Document__c WHERE 
                                                            Document_Number__c = : controlledDoc.Document_Number__c 
                                                            AND Revision__c =: NewRevision];
                            PageReference viewRef = new ApexPages.StandardController(dupDoc).view();

                            msg = 'Duplicate value on record: <a target="_blank" href="' + viewRef.getUrl() + '">'+ dupDoc.Name + '</a> (Related field: Document And Rev)';
                        }
                        
                        SQX_Utilities.addErrorMessageInPage(msg);
                    }

                    Database.rollback(sp);

                    returnUrl = null;
                    mainrecord.Id = null;
                }
            
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Insufficient privilege on the parent Controlled Document'));
            }

        }
        catch(Exception ex){
            //ignore error cause these will have been added by the dml exceptions
            SQX_Utilities.addErrorMessageInPage(ex.getMessage());


            if(sp != null)
                Database.rollback(sp); //rollback on error

            mainRecord.Id = null; //set the mainrecord id to null, not doing so will result in an error in the subsequent calls
            returnUrl = null;
        }

        return returnUrl;
    }
}