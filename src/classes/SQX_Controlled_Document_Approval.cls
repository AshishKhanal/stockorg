/**
* Extension class for Controlled Document Approval object
*/
public with sharing class SQX_Controlled_Document_Approval {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        /**
        * default constructor for Bulkified
        */
        public Bulkified(List<SQX_Controlled_Document_Approval__c> newList, Map<Id, SQX_Controlled_Document_Approval__c> oldMap){
            super(newList, oldMap);
        }
        
        /**
        * copies step number as text
        */
        public Bulkified setStepAsTextValue() {
            this.resetView();
            
            if (this.view.size() > 0) {
                for (SQX_Controlled_Document_Approval__c controlledDocApproval : (List<SQX_Controlled_Document_Approval__c>)this.view) {
                    controlledDocApproval.Step_As_Text__c = String.valueOf(controlledDocApproval.Step__c);
                }
            }
            
            return this;
        }
        
        /**
        * if the user selected does not belong to the job function doc approval save throws an error
        */
        public Bulkified checkIfUserHasValidUserFromJobFunction() {
            final String ACTION_NAME = 'checkIfUserHasValidUserFromJobFunction';
            
            // filter records submitted for approval process
            Rule filterRule = new Rule();
            filterRule.addRule(Schema.SQX_Controlled_Document_Approval__c.SQX_Job_Function__c, RuleOperator.NotEquals, null);
            filterRule.addRule(Schema.SQX_Controlled_Document_Approval__c.SQX_User__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.ControlledDoc, ACTION_NAME)
                .applyFilter(filterRule, RuleCheckMethod.OnCreateAndEveryEdit);
            if (this.view.size() > 0) {
                Set<Id> jobFunctionIds = getIdsForField(SQX_Controlled_Document_Approval__c.SQX_Job_Function__c);
                Map<Id, List<User>> controlledDocJobFunction = SQX_Job_Function.getActiveUsersForJobFunctions(jobFunctionIds, null, null);

                for(SQX_Controlled_Document_Approval__c controlledDocApproval : (List<SQX_Controlled_Document_Approval__c>) this.view){
                    List<User> controlledDocUsers = controlledDocJobFunction.get(controlledDocApproval.SQX_Job_Function__c);
                    Boolean containsJobFunctionUser = false;
                    for(User controlledDocUser : controlledDocUsers){
                        if(controlledDocApproval.SQX_User__c == controlledDocUser.Id){
                            containsJobFunctionUser = true;
                            break;
                        }
                    }
                    if(!containsJobFunctionUser){
                        controlledDocApproval.addError(Label.SQX_ERR_CONTROLLED_DOCUMENT_APPROVALS_USER_CANNOT_BE_SELECTED);
                    }
                }
            }
            
            return this;
        }
        
        /**
        * prevent create/ edit/ delete of doc approval once the parent doc is in approval or approved
        */
        public Bulkified preventApprovalCUDOnceItIsSentForApprovalOrApproved() {
            
            // filter records submitted for approval process

            Boolean trueObj = true;
            Rule filterRule = new Rule();
            filterRule.addRule(Schema.SQX_Controlled_Document_Approval__c.Is_Locked__c, RuleOperator.Equals, (Object) trueObj);
            
            this.resetView()
                .applyFilter(filterRule, RuleCheckMethod.OnCreateAndEveryEdit);

            if (filterRule.evaluationResult.size() > 0) {
                for(SQX_Controlled_Document_Approval__c controlledDocApproval : (List<SQX_Controlled_Document_Approval__c>) filterRule.evaluationResult){
                    controlledDocApproval.addError(Label.SQX_ERR_MSG_CONTROLLED_DOCUMENT_APPROVALS_MODIFY_AFTER_SENT_FOR_APPROVALS);
                }
            }
            
            return this;
        }
    }
}