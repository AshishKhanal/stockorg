/**
* Batch processor for generating personnel document job functions from active personnel job functions and requirements
* Added in version  : 7.0
* Used for versions : less than 7.0
* @author  Sajal Joshi
* @since   2017-03-27
*
* WITHOUT SHARING has been used 
* ---------------------------------
* Without sharing has been used because the running user may not have access on all personnels 
*/
global without sharing class SQX_7_0_PdjfGeneration implements Database.Batchable<sObject>{
    
    global SQX_7_0_PdjfGeneration() { }
    
    /**
     * Gets invoked when the batch job starts and selects all active personnel job functions which are not already selected
     * @param bc reference to the Database.BatchableContext object
     * @return Database.QueryLocator object that contains the pjfs passed to the job
     */ 
    global database.QueryLocator start(Database.BatchableContext bc){
        // return personnels and job functions of all active pjfs  
        return Database.getQueryLocator([SELECT 
                                             SQX_Personnel__c,
                                             SQX_Job_Function__c
                                         FROM SQX_Personnel_Job_Function__c
                                         WHERE Active__c = true 
                                         AND Is_PDJF_Generated__c = false
                                        ]);
    }
    
    /** 
     * Execute the method to generate pdjfs from the available active pjfs
     * @param bc reference to the Database.BatchableContext object
     * @param scope list of sObjects containing active pjfs
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        // List to store new pdjf to be inserted
        List<SQX_Personnel_Document_Job_Function__c> newPdjfs = new List<SQX_Personnel_Document_Job_Function__c>();
        // List to store personnel job function to be updated
        List<SQX_Personnel_Job_Function__c> pjfsToUpdate = new List<SQX_Personnel_Job_Function__c>();
        // List to store personnel document trainings to be updated
        Map<Id, SQX_Personnel_Document_Training__c> pdtsToUpdate = new Map<Id, SQX_Personnel_Document_Training__c>();
        // Map to store jfs with list of active pjfs
        Map<Id, List<SQX_Personnel_Job_Function__c>> jfPjfMap = new Map<Id, List<SQX_Personnel_Job_Function__c>>();
        
        for(SQX_Personnel_Job_Function__c pjf: (List<SQX_Personnel_Job_Function__c>)scope){
            if(jfPjfMap.containsKey(pjf.SQX_Job_Function__c)){
                jfPjfMap.get(pjf.SQX_Job_Function__c).add(pjf);
            }
            else{
                jfPjfMap.put(pjf.SQX_Job_Function__c, new List<SQX_Personnel_Job_Function__c>{pjf});
            }
            pjf.Is_PDJF_Generated__c = true;
            pjfsToUpdate.add(pjf);
        }
        // Map to store Personnel+Document with List of Pdfjs
        Map<String, List<SQX_Personnel_Document_Job_Function__c>> psnDocPdjfMap = new Map<String, List<SQX_Personnel_Document_Job_Function__c>>();
        Set<Id> allPsnIds = new Set<Id>();
        Set<Id> allDocIds = new Set<Id>();
        // iterate through active requirements and jfs to set in pdjfs
        for(SQX_Requirement__c req: [SELECT SQX_Controlled_Document__c, SQX_Job_Function__c, Uniqueness_Constraint__c,Optional__c, Level_of_Competency__c,
                                        (SELECT SQX_Personnel_Job_Function__c FROM SQX_Personnel_Document_Job_Functions__r WHERE SQX_Personnel_Job_Function__c IN: scope)
                                     FROM SQX_Requirement__c 
                                     WHERE Active__c =: true 
                                     AND SQX_Job_Function__c IN: jfPjfMap.keySet() 
                                     ORDER BY Level_of_Competency__c]){
                                         Set<id> pjfidstoskip = new Set<id>();
                                         for(SQX_Personnel_Document_Job_Function__c pdjf : req.SQX_Personnel_Document_Job_Functions__r) {
                                            pjfidstoskip.add(pdjf.SQX_Personnel_Job_Function__c);
                                         }
                                         for(SQX_Personnel_Job_Function__c pjf : jfPjfMap.get(req.SQX_Job_Function__c)){
                                             if (!pjfidstoskip.contains(pjf.Id)) {
                                                SQX_Personnel_Document_Job_Function__c pdjf = new SQX_Personnel_Document_Job_Function__c();  
                                                allDocIds.add(req.SQX_Controlled_Document__c); // used to query document training
                                                allPsnIds.add(pjf.SQX_Personnel__c); // used to query document training
                                                pdjf.SQX_Requirement__c = req.id;
                                                pdjf.SQX_Requirement__r = req; // used later to get level of competency
                                                pdjf.SQX_Personnel_Job_Function__c = pjf.id;
                                                pdjf.Training_Type__c = SQX_Personnel_Document_Job_Function.TRAINING_TYPE_INITIAL; // set training type as initial for all generated pdjfs
                                                String psnDocKey = String.valueOf(pjf.SQX_Personnel__c) + String.valueOf(req.SQX_Controlled_Document__c);
                                                if(psnDocPdjfMap.containsKey(psnDocKey)){
                                                    psnDocPdjfMap.get(psnDocKey).add(pdjf);
                                                }else{
                                                    psnDocPdjfMap.put(psnDocKey, new List<SQX_Personnel_Document_Job_Function__c>{pdjf});
                                                }
                                             }
                                         }
                                     }
   
        // nested map to store Personnel+Document as key with with Status and pdt map
        Map<String, Map<String, SQX_Personnel_Document_Training__c>>  psnDocStatusPdtMap = new Map<String, Map<String, SQX_Personnel_Document_Training__c>>();
        
        // iterate through pdt using the personnel and document set to get pdt for the pdjfs
        for(SQX_Personnel_Document_Training__c pdt: [SELECT Status__c, SQX_Personnel__c, SQX_Controlled_Document__c, Level_of_Competency__c, 
                                                     Overall_Competency__c, Overall_Optional__c
                                                     FROM SQX_Personnel_Document_Training__c 
                                                     WHERE SQX_Personnel__c IN: allPsnIds 
                                                     AND SQX_Controlled_Document__c IN: allDocIds 
                                                     AND Status__c !=: SQX_Personnel_Document_Training.STATUS_OBSOLETE 
                                                     ORDER BY Status__c, Level_of_Competency__c, User_SignOff_Date__c DESC, Completion_Date__c DESC]){
                                                         String psnDocKey = String.valueOf(pdt.SQX_Personnel__c) + String.valueOf(pdt.SQX_Controlled_Document__c);
                                                         if(!psnDocStatusPdtMap.containsKey(psnDocKey)){
                                                             psnDocStatusPdtMap.put(psnDocKey, new Map<String, SQX_Personnel_Document_Training__c>());
                                                         }
                                                         if(!psnDocStatusPdtMap.get(psnDocKey).containsKey(pdt.Status__c)){
                                                             psnDocStatusPdtMap.get(psnDocKey).put(pdt.Status__c, pdt);
                                                         }
                                                     }
        
        // Instantiate SQX_Requirement.SQX_LevelOfCompetencyMapper class to use later for level of competency comparision
        SQX_Requirement.SQX_LevelOfCompetencyMapper competencyMapper = new SQX_Requirement.SQX_LevelOfCompetencyMapper();
        
        // Iterate through the psnDocPdjfMap to get highest level of competency pdt for pdjf
        // [psnDocStatusPdtMap formation loop]
        for(String psnDocKey: psnDocStatusPdtMap.keySet()){

            if(!psnDocPdjfMap.containsKey(psnDocKey)) {
                /*
                * Fixes SQX-3893:
                * When a key is incorrectly formed in psnDocStatusPdtMap formation loop, null exception can result.
                *
                * Details:
                * Since all non-obsolete personnel document training forms the key in psnDocStatusPdtMap formation loop
                * there can be adhoc trainings or trainings added by inactive requirements. These requirements won't
                * have any requirement which is being used to form the psnDocPdjfMap. This missing values results in a
                * null exception.
                */
                continue;
            }

            for(SQX_Personnel_Document_Job_Function__c pdjf: psnDocPdjfMap.get(psnDocKey)){ 

                // If pending training exist add it is instantly
                if(psnDocStatusPdtMap.get(psnDocKey).containsKey(SQX_Personnel_Document_Training.STATUS_PENDING)){
                    SQX_Personnel_Document_Training__c pdt = psnDocStatusPdtMap.get(psnDocKey).get(SQX_Personnel_Document_Training.STATUS_PENDING);
                    pdjf.SQX_Personnel_Document_Training__c = pdt.Id;
                    
                    // set overall policies for pending trainings since they can be changed in this status only
                    if (String.isBlank(pdt.Overall_Competency__c)) {
                        // set policies using first pdjf
                        pdt.Overall_Competency__c = pdjf.SQX_Requirement__r.Level_of_Competency__c;
                        pdt.Overall_Optional__c = pdjf.SQX_Requirement__r.Optional__c;
                    }
                    else {
                        // compare and set policies for multiple pdjfs
                        if (competencyMapper.compare(pdt.Overall_Competency__c, pdjf.SQX_Requirement__r.Level_of_Competency__c) < 0) {
                            pdt.Overall_Competency__c = pdjf.SQX_Requirement__r.Level_of_Competency__c;
                        }
                        if (pdjf.SQX_Requirement__r.Optional__c == false) {
                            pdt.Overall_Optional__c = false;
                        }
                    }
                    
                    pdtsToUpdate.put(pdt.Id, pdt);
                    
                }else { // else compare level of competency and add the highest competent to pdjf
                    SQX_Personnel_Document_Training__c trainerPendingDT = psnDocStatusPdtMap.get(psnDocKey).get(SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING);
                    if(trainerPendingDT != null && competencyMapper.compare(trainerPendingDT.Level_of_Competency__c, pdjf.SQX_Requirement__r.Level_of_Competency__c)>=0){
                        pdjf.SQX_Personnel_Document_Training__c = trainerPendingDT.Id;
                    }
                    if (pdjf.SQX_Personnel_Document_Training__c == null ) {
                        pdjf.SQX_Personnel_Document_Training__c = psnDocStatusPdtMap.get(psnDocKey).get(SQX_Personnel_Document_Training.STATUS_COMPLETE).Id;                   
                    }
                }
                
            // add filtered pdjfs to new list
            newPdjfs.add(pdjf);
            }
        }
        
        SQX_DB db = new SQX_DB();
        
        // Insert all the pdjfs which are generated
        if(!newPdjfs.isEmpty()){
            db.op_insert(newPdjfs, new List<Schema.SObjectField>{
                SQX_Personnel_Document_Job_Function__c.SQX_Requirement__c,
                    SQX_Personnel_Document_Job_Function__c.SQX_Personnel_Job_Function__c,
                    SQX_Personnel_Document_Job_Function__c.SQX_Personnel_Document_Training__c,
                    SQX_Personnel_Document_Job_Function__c.Training_Type__c});
        }
        
        // Update pdts with new fields
        if(!pdtsToUpdate.isEmpty()){
            db.op_update(pdtsToUpdate.values(), new List<Schema.SObjectField>{
                    SQX_Personnel_Document_Training__c.Overall_Competency__c,
                    SQX_Personnel_Document_Training__c.Overall_Optional__c});
        }
        
        // Mark the pjfs after it is selected
        if(!pjfsToUpdate.isEmpty()){
            db.op_update(pjfsToUpdate, new List<Schema.SObjectField>{
                SQX_Personnel_Job_Function__c.Is_PDJF_Generated__c});
        }
    }
    
    /** 
     * Used to execute post-processing operations and is called once after all batches are processed
     * @param bc reference to the Database.BatchableContext object
     */
    global void finish(Database.BatchableContext bc){ }
}