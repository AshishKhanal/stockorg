/**
* unit tests for onboarding task trigger 
*/
@isTest
public class SQX_Test_5885_On_Boarding_Step_Trigger {

    // common error message when modifying locked record.
    static final String EXPECTED_VALIDATION_ERROR_MSG = 'Record status or Step status does not support the action performed.';
    /**
     * policy task should be copied to workflow policy
     * workflow policy should be open once parent record is initiated
     */

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, adminUser});
    }
    
    /**
     * Given:Any On Boarding Step with open status
     * When: User tries to delete On Boarding Step record
     * Then: Error is thrown
     * 
     * @Story: SQX-5885
     */
    @isTest
    public static void givenOnBoardingStepWithOpenStatus_whenUserDeletesRecord_ThenRecordErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
           // submit and initiate the NSI record
            nsi.submit();
            nsi.initiate();

            //Act: Try deleting On Boarding Step
            SQX_Onboarding_Step__c wfPolicy = [SELECT Id, SQX_Parent__c,Status__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id limit 1];                                                                   
            Database.DeleteResult result = Database.delete(wfPolicy, false);
            
            //Assert: Deleting Open Onboarding Step should throw an error message
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Delete_Once_Step_Is_In_Open_Status,result.getErrors()[0].getMessage(), 'Open Onboarding step can not be deleted');
            
            //Arrange : Complete the open onboarding task 
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id LIMIT 1];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            SQX_Onboarding_Step__c completeOnboardingStep = [SELECT Id, SQX_Parent__c, SQX_Parent__r.Workflow_Status__c, Status__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_COMPLETE LIMIT 1];   
            
            //Act: Try deleting On Boarding task which is in complete status
            Database.DeleteResult completeResult = Database.delete(completeOnboardingStep, false);

            //Assert: Deleting Completed Onboarding Step should throw error message.
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, completeResult.getErrors()[0].getMessage(), 'Completed On-boarding Step cannot be deleted');
        }
    }
    
    /** Given : Any On Boarding Step with open Complete
     * When: User tries to delete/edit On Boarding Step record
     * Then: Error msg is thrown.
     *
     * When : User tries to redo
     * Then : Redo is successfull
     * @Story: SQX-6104, SQX-6288
     */
    @isTest
    public static void givenCompletedOnboardingStep_WhenTryingToEditOrDelete_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange : Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
            // submit and initiate the NSI record
            nsi.submit();
            nsi.initiate();
                        
            // Arrange : Complete the open onboarding task 
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id LIMIT 1];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            SQX_Onboarding_Step__c completeOnboardingStep = [SELECT Id, SQX_Parent__c, SQX_Parent__r.Workflow_Status__c, Status__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_COMPLETE LIMIT 1];   
            
            //Act: Try deleting On Boarding task which is in complete status
            Database.DeleteResult completeResult = Database.delete(completeOnboardingStep, false);

            //Assert: Deleting Act should throw an error message
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, completeResult.getErrors()[0].getMessage(), 'Completed On-boarding Step cannot be deleted');

            // Act: try to edit On-boarding Step
            completeOnboardingStep.Description__c = 'Modified Description';
            Database.saveResult completeResult1 = Database.update(completeOnboardingStep, false);
            
            // Assert : Ensure error is thrown
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, completeResult1.getErrors()[0].getMessage(), 'Completed On-boarding Step cannot be modified');
        
            //Act : Try to redo Onboarding Step
            nsi.redoOnboardingStep(completeOnboardingStep);

            // Assert : Ensure that onboardingStep is redone
            SQX_Onboarding_Step__c redoneOnboardingStep = [SELECT Id, SQX_Parent__c, Status__c, Description__c FROM SQX_Onboarding_Step__c where SQX_Parent__r.id = :nsi.nsi.id AND Redo_Policy__c = true AND Archived__c = true];
            System.assert(redoneOnboardingStep.Id == completeOnboardingStep.Id, 'Expected redo should be successful');
        
            SQX_Onboarding_Step__c newOnboardingStep = [SELECT Id, SQX_Parent__c, Status__c, Description__c FROM SQX_Onboarding_Step__c where SQX_Parent__r.id = :nsi.nsi.id AND Redo_Policy__c = false AND Archived__c = false AND SQX_Parent_Step__c =: redoneOnboardingStep.Id];
            System.assert(newOnboardingStep.Status__c == SQX_Steps_Trigger_Handler.STATUS_DRAFT, 'Expected new draft OnBoarding Step record to be created after redo to complete Onboarding Step.');
        }
    }
    
    /**
     * Given : Voided Nsi Record the record
     * When : User tries to delete or edit On Boarding Step record
     * Then : Error message is thrown
     * @Story: SQX-6104, SQX-6288
     */
    @isTest
    public static void givenVoidedNSI_whenUserDeletesOrEditOnBoardingStepRecord_ThenRecordErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            // Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
            // Submitting the NSI record
            nsi.submit();

            //void the nsi record
            nsi.void();
            
            SQX_Onboarding_Step__c onboardingStep = [SELECT Id, SQX_Parent__c, Status__c, Is_Locked__c, Is_Parent_Locked__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c = :nsi.nsi.id LIMIT 1];                                                                   
            
            // Act: Try deleting On Boarding Step
            Database.DeleteResult deleteVoidResult = Database.delete(onboardingStep, false);
            
            // Assert: Deleting Act should throw an error message
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, deleteVoidResult.getErrors()[0].getMessage(), 'On-boarding Step cannot be deleted once NSI is voided');
        
            // Act: try to update the onboarding step
            onboardingStep.Description__c='update desc';
            Database.SaveResult editVoidRecord = Database.update(onboardingStep, false);

            // Assert: when edit user should get error message
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, editVoidRecord.getErrors().get(0).getMessage(), 'Onboarding step cannot be changed once nsi is closed');
        }
    }

    /**
     * Given : Closed Nsi Record
     * When : User tries to delete or edit On Boarding Step record
     * Then : Error message is thrown 
     *
     * @Story: SQX-6104, SQX-6288
     */
    @isTest
    public static void givenClosedNSI_whenUserDeletesOrEditsOnBoardingStepRecord_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
            // Submitting the NSI record
            nsi.submit();

            // initiate the nsi record
            nsi.initiate();

            //close  the nsi record
            nsi.close();

            SQX_Onboarding_Step__c onboardingStep = [SELECT Id, SQX_Parent__c, Status__c, Description__c FROM SQX_Onboarding_Step__c where SQX_Parent__r.id=:nsi.nsi.id limit 1];
            
            //Act: Try deleting On Boarding Step
            Database.DeleteResult deleteVoidResult = Database.delete(onboardingStep, false);
            
            //Assert: Deleting Act should throw an error message
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, deleteVoidResult.getErrors()[0].getMessage(), 'On-boarding Step cannot be deleted once NSI is closed');

            //Act: try to update the onboarding step
            onboardingStep.Description__c='update desc';
            List<Database.SaveResult> editVoidRecord = new SQX_DB().continueOnError().op_update(
                                                        new List<SQX_Onboarding_Step__c> {onboardingStep}, 
                                                        new List<Schema.SObjectField>{
                                                            SQX_Onboarding_Step__c.Description__c
                                                        });

            //Assert : when edit user should get error message
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, editVoidRecord[0].getErrors().get(0).getMessage(), 'Onboarding step cannot be edited once nsi is closed');
        }
    }
    
    /**
     * Given : Closed NSI and its Onboarding Steps
     * When : File is added to NSI or Onboarding Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenClosedNSIAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
            // Submitting the NSI record
            nsi.submit();

            // initiate the nsi record
            nsi.initiate();

            //close  the nsi record
            nsi.close();

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(nsi.nsi.Id);
        }
    }

    /**
     * Given : Void NSI and its Onboarding Steps
     * When : File is added to NSI or Onboarding Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenVoidNSIAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a NSI Record 
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
           
            // Submitting the NSI record
            nsi.submit();

            // void  the nsi record
            nsi.void();

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(nsi.nsi.Id);
            
        }
    }

    /**
     * This common internal private method is used to validate common error message while adding files to locked NSI/step records.
     */
    private static void validateErrorWhileAddingFilesToLockedRecords(Id nsiId) {
        // Act : add file
        ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
        insert cv;
        
        // query contentdocumentid
        Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
        // insert ContentDocumentLink for nsi
        Database.SaveResult result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = nsiId, ShareType = 'V'), false);
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for locked nsi record.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());

        // Act : add files (i.e. ContentDocumentLink) for Onboarding Step
        SQX_Onboarding_Step__c onboardingStep = [SELECT Id, Is_Locked__c, Is_Parent_Locked__c FROM SQX_Onboarding_Step__c where SQX_Parent__c = :nsiId LIMIT 1];
        result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId, LinkedEntityId = onboardingStep.Id, ShareType = 'V'), false);
        
        // Assert : Ensure that error is thrown.
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for OnBoarding Step whose NSI is locked.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());
    }

    /**
     *  GIVEN : Draft & Open On boarding steps
     *  WHEN : User tries to redo those incomplete steps
     *  THEN : Validation error message is thrown
     *  @story : SQX-6401
     */
    testmethod static void givenDraftAndOpenOnBoardingSteps_WhenUserTriesToRedoThoseSteps_ThenEXPECTED_VALIDATION_ERROR_MSGIsThrown() {

        final String EXPECTED_VALIDATION_ERROR_MSG_LOCAL = 'Cannot redo step which has not been completed.';

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User assigneeUser = allUsers.get('assigneeUser');

        SQX_Test_NSI nsi;
        List<SQX_Onboarding_Step__c> steps;
        Database.SaveResult result;

        System.runAs(adminUser) {
            // rrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 1);

            // Arrange :Initiate a NSI Record that opens the on-boarding step
            nsi = new SQX_Test_NSI().save();
            nsi.submit();

            steps = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_DRAFT];

            System.assertEquals(1, steps.size(), 'Expected draft onboarding step for NSI');

            // Act : Redo Draft Step
            result = Database.update(new SQX_Onboarding_Step__c(Id = steps.get(0).Id, Redo_Policy__c = true, Archived__c = true), false);

            // Assert : Error message is thrown when we redo Draft step
            System.assertEquals(1, result.getErrors().size(), 'Expected error to be thrown when user tries to redo Draft OnBoarding Step. Actual error : ' + result.getErrors());
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG_LOCAL, result.getErrors()[0].getMessage(), 'Unexpected error message encountered when user tried to redo draft on-boarding step');

            nsi.initiate();

            steps = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];

            System.assertEquals(1, steps.size(), 'Expected open onboarding step for NSI');

            // Act : Redo Open step
            result = Database.update(new SQX_Onboarding_Step__c(Id = steps.get(0).Id, Redo_Policy__c = true, Archived__c = true), false);

            // Assert : Error message is thrown when we redo to Completed step
            System.assertEquals(1, result.getErrors().size(), 'Expected error to be thrown when user tries to redo Open OnBoarding Step');
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG_LOCAL, result.getErrors()[0].getMessage(), 'Unexpected error message encountered when user tried to redo open on-boarding step');
        }

        // completing the step
        System.runAs(assigneeUser) {

            Task t = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
        }

        System.runAs(adminUser) {

            steps = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE];

            System.assertEquals(1, steps.size(), 'Expected completed onboarding step for NSI');

            // Act : Redo Completed Step
            result = Database.update(new SQX_Onboarding_Step__c(Id = steps.get(0).Id, Redo_Policy__c = true, Archived__c = true), false);

            // Assert : Redo is successful
            System.assertEquals(0, result.getErrors().size(), 'Expected user to be able to redo completed step but got error ' + result.getErrors());

            // Act : Redo step which has already been redone
            result = Database.update(new SQX_Onboarding_Step__c(Id = steps.get(0).Id, Redo_Policy__c = true, Archived__c = true), false);

            // Assert : Error message to be thrown
            System.assertEquals(1, result.getErrors().size(), 'Expected error to be thrown when user tries to redo an already archived Open OnBoarding Step');
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, result.getErrors()[0].getMessage(), 'Unexpected error message encountered when user tried to redo archived on-boarding step');
        }
    }

    /**
     * Given : NSI record, File(ContentDocumentLink) is added to NSI and is locked (voided or closed)
     * When : ContentDocumentLink is tried to delete
     * Then : Error is thrown
     */
    @isTest
    public static void givenLockedNSIAndOnboardingWithFile_WhenContentDocumentLinkIsDelted_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_NSI.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange : Create a NSI Record 
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            // add file
            ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
            insert cv;

            // query contentdocumentid
            Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
            // insert ContentDocumentLink for NSI
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = nsi.nsi.Id, ShareType = 'V');
            Database.SaveResult result = Database.insert(documentLink, false);
            
            System.assert(result.isSuccess() && documentLink.Id != null, 'Expected file/ContentDocumentLink to be added in NSI. Actual Error : ' + result.getErrors());

            // Submitting the NSI record
            nsi.submit();

             // add files to Onboarding Step too
            SQX_Onboarding_Step__c onboarding = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c = :nsi.nsi.Id LIMIT 1];
            ContentDocumentLink documentLink_onboarding = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = onboarding.Id, ShareType = 'V');
            result = Database.insert(documentLink_onboarding, false);
            // Ensure ContentDocumentLink is added
            System.assert(result.isSuccess() && documentLink_onboarding.Id != null, 'Expected file/ContentDocumentLink to be added in Onboarding Step. Actual Error : ' + result.getErrors());

            // void  the NSI record
            nsi.void();

            // Act : Try to delete ContentDocumentLink for NSI by calling common method
            validateDeletionOfContentDocumentLink(documentLink, 'NSI');

            // Act : Try to delete ContentDocumentLink for Onboarding Step by calling common method
            validateDeletionOfContentDocumentLink(documentLink_onboarding, 'Onboarding Step');
        }
    }

    /**
     * Common method to ensure that the deletion of ContentDocumentLink locked NSI and Onboarding Step is being prevented
     */
    public static void validateDeletionOfContentDocumentLink(ContentDocumentLink cdLink, String objName) {
        
        // Act : try to delete ContentDocumentLink
        Database.DeleteResult deleteResult = Database.delete(cdLink, false);
        
        // Assert : Ensure ContentDocumentLink is not deleted and system throws expected validation error.
        System.assert(deleteResult.isSuccess() == false, 'Expected ContentDocumentLink not to be deleted from' + objName + ' but is deleted.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, deleteResult.getErrors().get(0).getMessage());
    }
}