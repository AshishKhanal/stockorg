/**
 *  Interface that represents eContent provider
*/
global interface SQX_eContent_Provider {

    /**
     *  Method to upload content to Cloud
     *  @param contentId - identifier for the content in the cloud
     *  @param contentDocumentId - file to be uploaded
    */
    void uploadContent(String contentId, String contentDocumentId);


    /**
     *  Method returns the url for previewing the content
     *  @param contentId - identifier for content
     *  @param redirectUrl - url to redirect to after preview completion
    */
    String getPreviewUrl(String contentId, String redirectUrl);


    /**
     *  Method to check if the given registration exists
     *  @param regId - registration id to be searched
    */
    Boolean registrationExists(String regId);


    /**
     *  Method to register user with the given id for the given content
     *  @param userId - identification for user being registered
     *  @param contentId - content identifier
     *  @param regId - id to be associated with the registration
    */
    void registerUser(String userId, String contentId, String regId);

    /**
	 *	Method to reset the given registration
	 *	@param regId - identifier for the registration
	*/
    void resetRegistration(String regId);

    /**
     *  Method returns the url for launching the specified registration
     *  @param regId - identifier for registration
     *  @param redirectUrl - url to specify the browser where to return upon completion/closure of launching process
    */
    String getLaunchUrl(String regId, String redirectUrl);


    /**
     *  Method returns the result for the given registration
     *  @param regId - identifier for registration
    */
    SQX_eContent_Response getRegistrationResult(String regId);

}