/**
 * Test class to ensure all required fields are set when complaint is initiated
 * @author: Sanjay Maharjan
 * @date: 2019-01-21
 * @story: [SQX-7691] 
 */
@isTest
public class SQX_Test_7691_Complaint_Validation {
    
    static Database.SaveResult result;
    
    // List of error messages
    static Map<String, String> errorMessages = new Map<String, String>{
                                                                    'Title' => 'Title is required',
                                                                    'Department' => 'Department is required',
                                                                    'Report-Date' => 'Report Date is required',
                                                                    'Occurrence-Date' => 'Occurrence Date is required',
                                                                    'Aware-Date' => 'Aware Date is required',
                                                                    'Outcome' => 'Outcome is required',
                                                                    'Requested-Action' => 'Requested Action is required',
                                                                    'Part' => 'Part is required',
                                                                    'Complaint-Quantity' =>'Complaint Quantity is required',
                                                                    'Description' => 'Description is required',
                                                                    'Description-As-Reported' => 'Description As Reported is required'
                                                                    };
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * GIVEN : A complaint is created and initiated
     * WHEN  : Required fields are not empty
     * THEN  : Complaint is successfully initiated without any error
     */
    public testMethod static void givenAComplaintAndInitiated_WhenRequiredFieldsNotEmpty_ThenErrorIsNotThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Get complaint with all required fields set
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();

            // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is submitted and no errors are thrown
            System.assertEquals(0, result.getErrors().size(), 'No error messages should be thrown');
            
        }
        
    }
    
    /**
     * GIVEN : A complaint with empty title
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithTitleEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with title empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Complaint_Title__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Complaint_Title__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Title')));
            
        }
        
    }
	
    /**
     * GIVEN : A complaint with empty department
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithDepartmentEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with department empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Department__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.SQX_Department__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Department')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty Reported Date
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithReportedDateEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with reported date empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Reported_Date__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Reported_Date__c})[0];
            
             // ACT : Submit complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Report-Date')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty occurrence date
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithOccurrenceDateEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with occurrence date empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Occurrence_Date__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Occurrence_Date__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Occurrence-Date')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty aware date
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithAwareDateEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with aware date empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Aware_Date__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Aware_Date__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Aware-Date')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty outcome
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithOutcomeEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with outcome empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Outcome__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Outcome__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Outcome')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty requested action
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithRequestedActionEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with requested action empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Requested_Action__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Requested_Action__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Requested-Action')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty part
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithPartEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with part empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Part__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.SQX_Part__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Part')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty complaint quanitity
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithComplaintQuantityEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with complaint quantity empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Complaint_Quantity__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Complaint_Quantity__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Complaint-Quantity')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty description
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithDescriptionEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with description empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Description__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Description__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Description')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty description as reported
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate error is thrown
     */
    public testMethod static void givenAComplaintWithDescriptionAsReportedEmpty_WhenComplaintIsInitated_ThenAppropriateErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with description as reported empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Description_As_Reported__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Description_As_Reported__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate error is thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(1, result.getErrors().size(), 'Number of error message should be 1');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Description-As-Reported')));
            
        }
        
    }

    /**
     * GIVEN : A complaint with empty multiple required fields
     * WHEN  : Complaint is initiated
     * THEN  : Appropriate errors are thrown
     */
    public testMethod static void givenAComplaintWithRequiredFieldsEmpty_WhenComplaintIsInitiated_ThenAppropriateErrorsAreThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with multiple required fields empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Complaint_Title__c = '';
            complaint.complaint.SQX_Department__c = null;
            complaint.complaint.Description__c = '';
            complaint.complaint.Description_As_Reported__c = '';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint},
                                                              new List<SObjectField>{SQX_Complaint__c.Complaint_Title__c,
                                                                                     SQX_Complaint__c.SQX_Department__c,
                                                                                     SQX_Complaint__c.Description__c,
                                                                                     SQX_Complaint__c.Description_As_Reported__c})[0];
            
             // ACT : Initiate complaint
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is initiated and appropriate errors are thrown
            System.assertEquals(false, result.isSuccess(), 'Complaint should not be initiated');
            System.assertEquals(4, result.getErrors().size(), 'Number of error messages should be 4');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Title')));
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Department')));
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Description')));
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMessages.get('Description-As-Reported')));
            
        }
        
    }
    
    /**
     * GIVEN : A complaint with empty multiple required fields
     * WHEN  : Complaint is voided
     * THEN  : Complaint is voided without throwing any errors
     */
    public testMethod static void givenAComplaintWithRequiredFieldsEmpty_WhenComplaintIsVoided_NoErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with multiple required fields empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Complaint_Title__c = '';
            complaint.complaint.SQX_Department__c = null;
            complaint.complaint.Description__c = '';
            complaint.complaint.Description_As_Reported__c = '';
            complaint.complaint.Reported_Date__c = null;
            complaint.complaint.Occurrence_Date__c = null;
            complaint.complaint.Aware_Date__c = null;
            complaint.complaint.Outcome__c = '';
            complaint.complaint.Requested_Action__c = '';
            complaint.complaint.SQX_Part__c = null;
            complaint.complaint.Complaint_Quantity__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint},
                                                              new List<SObjectField>())[0];
            
             // ACT : Void complaint
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_VOID;
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
            
            // ASSERT : Complaint is voided without any errors
            System.assertEquals(true, result.isSuccess(), 'Complaint should be voided');
            
        }
        
    }
    
     /**
     * GIVEN : A complaint with empty multiple required fields
     * WHEN  : Complaint is closed
     * THEN  : Complaint is closed without throwing any errors
     */
    public testMethod static void givenAComplaintWithRequiredFieldsEmpty_WhenComplaintIsClosed_NoErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Defect_Code__c complaintConclusion = null;
        
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }

        System.runAs(standardUser) {
            
            // ARRANGE : Complaint with multiple required fields empty
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Complaint_Title__c = '';
            complaint.complaint.SQX_Department__c = null;
            complaint.complaint.Description__c = '';
            complaint.complaint.Description_As_Reported__c = '';
            complaint.complaint.Reported_Date__c = null;
            complaint.complaint.Occurrence_Date__c = null;
            complaint.complaint.Aware_Date__c = null;
            complaint.complaint.Outcome__c = '';
            complaint.complaint.Requested_Action__c = '';
            complaint.complaint.SQX_Part__c = null;
            complaint.complaint.Complaint_Quantity__c = null;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint},
                                                              new List<SObjectField>())[0];
            
            // ACT : Close complaint
            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_CLOSE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{SQX_Complaint__c.Activity_Code__c})[0];
           
            // ASSERT : Complaint is closed without any errors
            System.assertEquals(true, result.isSuccess(), 'Complaint should be closed');
            
        }
        
    }
}