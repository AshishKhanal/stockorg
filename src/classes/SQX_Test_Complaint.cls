/**
* this is a factory class that assists in creation of Complaint
*/
@isTest
public class SQX_Test_Complaint{

    public SQX_Complaint__c complaint {get; set; }
    public SQX_Test_Finding finding { get; set; }
    public SQX_Defect_Code__c complaintConclusion {get; set; }
    
    /**
    * constructor to create complaint
    * @adminUser user with admin access which is needed to create department for complaint
    * Create Reporting Default
    **/
    public SQX_Test_Complaint(User adminUser){
        SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.ComplaintType)
                            .setRequired(false,false,false,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(false, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(Date.Today().addDays(10), Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save();
                            
        initializeComplaint(finding, adminUser);
        
        integer randomNumber = (Integer) (Math.random() * 1000000);
        complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code' + randomNumber, 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category',
                                                        Description__c = 'Test Description' + randomNumber,
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
    }
    /**
    * default constructor which is not used, so made private
    **/
    private SQX_Test_Complaint(){
        this(null);
    }
    
    /**
    * connstuctor which creates conplaint with specified finding 
    **/
    public SQX_Test_Complaint(SQX_Test_Finding finding, User adminUser){
        initializeComplaint(finding, adminUser);
    }
    
    /**
    * add all required fields for finding
    */
    private void initializeComplaint(SQX_Test_Finding finding, User adminUser){
        this.finding = finding;
        complaint = new SQX_Complaint__c();
        complaint.SQX_Finding__c = finding != null ? finding.finding.Id : null;  
        complaint.Country_of_Origin__c = 'US';
        complaint.SQX_Department__c = randomDepartment(adminUser).Id;
        complaint.Description_As_Reported__c = 'Random Description';
        complaint.Aware_Date__c = Date.today();
        complaint.Complaint_Title__c = 'Random Title';
        complaint.Reported_Date__c = Date.today();
        complaint.Occurrence_Date__c = Date.today();
        complaint.Outcome__c = 'Concern';
        SQX_Part__c part = SQX_Test_Part.insertPart(null, adminUser, true, '');
        complaint.SQX_Part__c = part.Id;
        complaint.Complaint_Quantity__c = 1;
        complaint.Description__c = 'New Random Description';
    }

    /**
     * saves the new department for the complaint
     */
    public SQX_Department__c randomDepartment(User adminUser){
        SQX_Department__c randomDepartment= new SQX_Department__c();
        System.runas(adminUser){
            randomDepartment = new SQX_Department__c(
                                                 Name='Random Department'
                                               );
            insert randomDepartment;
            
        }
        return randomDepartment;       
    }
    
    /**
     * sets the new status of the complaint
     */
    public SQX_Test_Complaint setStatus(String newStatus){
        complaint.Status__c = newStatus;
        return this;
    }

    /**
     * sets the new stage of the complaint
     */
    public SQX_Test_Complaint setStage(String newStage){
        complaint.Record_Stage__c = newStage;
        return this;
    }

    /**
     * This method completes the related investigation and phr that have been automatically created by the system
     */
    public SQX_Test_Complaint completeInvestigationAndPhr() {
        List<SQX_Complaint__c> complaintRec = [SELECT Id, (SELECT Id FROM SQX_Investigations__r),
                                                   (SELECT Id FROM SQX_Product_History_Reviews__r)
                                           FROM SQX_Complaint__c
                                           WHERE Id = : this.complaint.Id];
        System.assert(complaintRec.size() == 1, 'No complaint record was found');
        for(SQX_Investigation__c investigation : complaintRec[0].SQX_Investigations__r) {
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            investigation.Completed_By__c = 'Sample';
            investigation.Completed_On__c = Date.today();
        }

        for(SQX_Product_History_Review__c phr : complaintRec[0].SQX_Product_History_Reviews__r) {
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Completed_By__c = 'Sample';
            phr.Completed_On__c = Date.today();
        }

        List<SObject> updateList = new List<SObject>();
        updateList.addAll((List<SObject>)complaintRec[0].SQX_Investigations__r);
        updateList.addAll((List<SObject>)complaintRec[0].SQX_Product_History_Reviews__r);

        update updateList;

        return this;
    }

    /**
    * saves the changes to the complaint object
    */
    public SQX_Test_Complaint save(){
        
        if(this.complaint.Id  == null){
            new SQX_DB().op_insert(new List<SQX_Complaint__c>{this.complaint}, new List<Schema.SObjectField>{
                    Schema.SQX_Complaint__c.Status__c,
                    Schema.SQX_Complaint__c.Country_of_Origin__c,
                    Schema.SQX_Complaint__c.SQX_Finding__c,
                    Schema.SQX_Complaint__c.SQX_Department__c,
                    Schema.SQX_Complaint__c.Description_As_Reported__c
                });
        }
        else{
            new SQX_DB().op_update(new List<SQX_Complaint__c>{this.complaint}, new List<Schema.SObjectField>{
                    Schema.SQX_Complaint__c.Status__c,
                    Schema.SQX_Complaint__c.Country_of_Origin__c,
                    Schema.SQX_Complaint__c.SQX_Finding__c,
                    Schema.SQX_Complaint__c.SQX_Department__c,
                    Schema.SQX_Complaint__c.Description_As_Reported__c
                });   
        }


        return this;

    }

    /**
    * This method runs a decision tree for a particular task object for the complaint.
    * @param related the decision tree task that is to be run for the complaint.
    * @return returns the newly inserted decision tree for the complaint and decision tree task.
    */
    public SQX_Decision_Tree__c runDecisionTreeTask(SQX_Task__c related){
        SQX_Decision_Tree__c dtree = new SQX_Decision_Tree__c(
            SQX_Task__c = related.Id,
            SQX_Complaint__c = complaint.Id,
            Name = 'RUN-DTREE-' + related.Name
        );
        
        new SQX_DB().op_insert(new List<SObject> { dtree }, new List<SObjectField> {
            SQX_Decision_Tree__c.SQX_Task__c,
			SQX_Decision_Tree__c.SQX_Complaint__c,
			SQX_Decision_Tree__c.Name
        });
        
        return dtree;
    }
    
	private static integer reportCount = 1, submissionCount = 1;
    
    /**
    * Adds a regulatory report that is linked with the decision tree run
    * @param dtree the decision tree run that is to be associated with the regulatory report
    * @param status the status of the newly added report.
    * @return returns the newly inserted regulatory report.
    */
    public SQX_Regulatory_Report__c addRegulatoryReport(SQX_Decision_Tree__c dtree, String status){
        return addRegulatoryReport(dtree, status, null, null, null);
    }
    
    /**
    * Adds a regulatory report that is liked with decision tree run
    * @param dtree the decision tree run that is to be associate with the regulatory report
    * @param status of the newly added report
    * @param duedate the date when the report is due
    * @param assignee the person who should be assigned the submission of the report
    * @return returns the newly inserted regulatory report object.
    */
    public SQX_Regulatory_Report__c addRegulatoryReport(SQX_Decision_Tree__c dtree, String status, Date dueDate, User assignee){
        return addRegulatoryReport(dtree, status, dueDate, assignee, null);
    }
    
    public SQX_Regulatory_Report__c addRegulatoryReport(SQX_Decision_Tree__c dtree, String status, Date dueDate, User assignee, SQX_Submission_History__c history){
        SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c(
            SQX_Decision_Tree__c = dtree != null ? dtree.Id : null,
            SQX_Complaint__c = complaint.Id,
            Status__c = status,
            SQX_Submission_History__c = history != null ? history.Id : null,
            Name = 'Regulatory Report ' + reportCount
        );
        
        if(dueDate != null)
            report.Due_Date__c = dueDate;
        else
            report.Due_Date__c = Date.today().addDays(10);
        
        if(assignee != null)
            report.SQX_User__c = assignee.Id;
        
        reportCount++;
        
        new SQX_DB().op_insert(new List<SObject> { report }, new List<SObjectField> {
            SQX_Regulatory_Report__c.SQX_Decision_Tree__c,
            SQX_Regulatory_Report__c.SQX_Complaint__c,
            SQX_Regulatory_Report__c.Status__c,
            SQX_Regulatory_Report__c.SQX_User__c,
            SQX_Regulatory_Report__c.Due_Date__c
        });
        
        return report;
    }
    
    private static integer policyCount = 1;
    /**
    * Adds a policy item to the the complaint object. The child object is added to the complaint object
    * @param applicable sets whether the policies is applicable or not
    * @param dueDate sets the due date of the policy
    * @param assignedTo sets the user who should be assigned the task
    * @return returns the complaint task that has been added.
    */
    public SQX_Complaint_Task__c addPolicy(Boolean applicable, Date dueDate, Id assignedTo){
        SQX_Complaint_Task__c task = new SQX_Complaint_Task__c(
        										Applicable__c = applicable,
            									SQX_User__c = assignedTo,
            									SQX_Complaint__c = complaint.Id,
            									Due_Date__c = dueDate,
            									Name = 'Test Policy-' + SQX_Test_Complaint.policyCount);
        policyCount++;
        
        new SQX_DB().op_insert(new List<SObject>{task}, new List<SObjectField> { Schema.SQX_Complaint_Task__c.Applicable__c,
            																	 		Schema.SQX_Complaint_Task__c.SQX_User__c,
            																	 Schema.SQX_Complaint_Task__c.SQX_Complaint__c,
            																	 Schema.SQX_Complaint_Task__c.Due_Date__c,
            																	 Schema.SQX_Complaint_Task__c.Name
            });
        
        return task;
    }

    /**
    * Adds submission history to the complaint with a given due date
    * @param dueDate the due date for the submission history if it is null today + 10 is used
    * @return returns the inserted submission history
    */
    public SQX_Submission_History__c addSubmissionHistory(Date dueDate){
        SQX_Submission_History__c history = new SQX_Submission_History__c(
            Name = 'History ' + submissionCount,
            Due_Date__c = dueDate == null ? Date.today().addDays(10) : dueDate,
            SQX_Complaint__c = complaint.Id,
            Submitted_By__c = 'Test'
        );
        
        submissionCount++;
        
        new SQX_DB().op_insert(new List<SObject> { history }, new List<SObjectField> {
            SQX_Submission_History__c.Name,
            SQX_Submission_History__c.SQX_Complaint__c,
            SQX_Submission_History__c.Comment__c,
            SQX_Submission_History__c.Confirmation_Code__c,
            SQX_Submission_History__c.Confirmation_Recieved_Date__c,
            SQX_Submission_History__c.Report_Number__c,
            SQX_Submission_History__c.Reg_Body__c
        });
        
        return history;
    }

    /**
     * method to submit Complaint
     */
    public void submit() {
        this.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_SUBMIT;
        this.complaint.Activity_Comment__c = 'Submitting Record';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
    * method to take ownership and inititate Complaint
     */
    public void takeOwnershipAndInitiate() {
        this.complaint.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE;
        this.complaint.Activity_Comment__c = 'Taking Ownership and Initiating Record';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to initiate Complaint
     */
    public void initiate() {
        this.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_INITIATE;
        this.complaint.Activity_Comment__c = 'Initiating Record';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to void Complaint
     */
    public void void() {
        this.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
        this.complaint.Activity_Comment__c = 'Voiding Record';
        this.complaint.Is_Locked__c = true;
        this.complaint.Resolution__c = SQX_Complaint.RESOLUTION_VOID;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to close Complaint
     */
    public void close() {
        this.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_CLOSE;
        this.complaint.Activity_Comment__c = 'Closing Record';
        this.complaint.Is_Locked__c = true;
        this.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
        this.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to reopen Complaint
     */
    public void reopen() {
        this.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_REOPEN;
        this.complaint.Activity_Comment__c = 'Reopening Record';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
}