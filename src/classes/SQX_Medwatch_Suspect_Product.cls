/**
*   Class to enclose Medwatch Suspect Product functionalities
*/


public with sharing class SQX_Medwatch_Suspect_Product {


    /**
     * Trigger handler for Medwatch trigger actions
    */
    public with sharing class SQX_Medwatch_Suspect_Product_Handler extends SQX_Trigger_Handler {

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newValues equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public SQX_Medwatch_Suspect_Product_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
            super(newValues, oldMap);
        }

        /**
        * Trigger methods:
        * a. After Insert/Update [clearActivityCode]: clears out activity code field value
        */
        protected override void onAfter() {
            clearActivityCode();
        }


        /**
         * Method clears out activity code field value
        */
        private void clearActivityCode() {

            final String ACTION_NAME = 'clearActivityCode';

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Medwatch_Suspect_Product__c.Activity_Code__c, RuleOperator.NotEquals, null);

            String objName = this.getObjName();

            this.resetView()
                .removeProcessedRecordsFor(objName, ACTION_NAME)
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Medwatch_Suspect_Product__c> recordsToUpdate = new List<SQX_Medwatch_Suspect_Product__c>();

            if(this.view.size() > 0){
                this.addToProcessedRecordsFor(objName, ACTION_NAME, this.view);

                for(SQX_Medwatch_Suspect_Product__c record : (List<SQX_Medwatch_Suspect_Product__c>) this.view) {
                    SQX_Medwatch_Suspect_Product__c recordToUpdate = new SQX_Medwatch_Suspect_Product__c(Id = record.Id);
                    recordToUpdate.Activity_Code__c = null;
                    recordsToUpdate.add(recordToUpdate);
                }

                new SQX_DB().op_update(recordsToUpdate, new List<SObjectField> { SQX_Medwatch_Suspect_Product__c.Activity_Code__c });
            }

        }

    }

}