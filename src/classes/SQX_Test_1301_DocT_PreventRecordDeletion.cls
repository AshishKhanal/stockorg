/**
* Unit tests for deleting of document training records when a user job function used.
*
* @author Shailesh Maharjan    
* @date 2015/08/04
* 
*/
@IsTest
public class SQX_Test_1301_DocT_PreventRecordDeletion {
    // removed test for obsolete SQX_Document_Training__c object
    // TODO: remove file while doing cleanup
}