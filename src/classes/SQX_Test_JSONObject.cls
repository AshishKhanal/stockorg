@isTest
public class SQX_Test_JSONObject{

    public SObject i_sObject;
    private Map<SObjectField, String> virtualMapping;
    private Map<SObjectField, Object> originalValues;

    public SQX_Test_JSONObject(SObject sObj){
        initialize(sObj);
    }

    public SQX_Test_JSONObject( String friendlyId, SObject sObj){
        initialize(sObj);
        //HACK: Id field has the same name for all the objects, hence we use the hack
        virtualMapping.put(SQX_Finding__c.fields.Id, friendlyId);
    }

    private void initialize(SObject sObj){
        this.i_sObject = sObj;
        virtualMapping = new Map<SObjectField, String>();
        originalValues = new Map<SObjectField, Object>();
    }

    /**
    * used to add realation between two objects to copy id from one object to another 
    */
    public SQX_Test_JSONObject addRelation(SObjectField sObjectField, String id){
        virtualMapping.put(sObjectField, id);

        return this;
    }

    /**
    * used to copy original values which will be used when updating the field through changeset
    */
    public SQX_Test_JSONObject addOriginalValue(SObjectField sObjectField, Object obj){
        originalValues.put(sObjectField, obj);

        return this;
    }

    /**
    * used to convert the records from the object to JSON
    */
    public Map<String, Object> toJSONObject(){
        Map<String,Object>  sObjectUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(i_sObject));

        for(SObjectField field : virtualMapping.keySet()){
            sObjectUT.put(field.getDescribe().getName(), virtualMapping.get(field));
        }

        if(originalValues.keySet().size() > 0){
            sObjectUT.put('originalValues', originalValues);

        }
                    
        return sObjectUT;
    }
}