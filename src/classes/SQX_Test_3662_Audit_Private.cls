/**
 * This test claass is used to test the managed sharing of audit and finding with audit team
 */
@IsTest
public class SQX_Test_3662_Audit_Private {
    
    @testSetup
    public static void commonSetup() {

        UserRole mockRole = SQX_Test_Account_Factory.createRole();

        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, null, 'sysAdmin');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user3');
        
    }
    
    /**
     * Given : Create an Audit
     * WHen : audit team with Editor role is added
     * Then : Audit is shared with audit team member with accesslevel 'Edit' and sharing reason.
     */
    static testmethod void givenAudit_WhenAuditTeamIsAdded_AuditIsSharedWithTeamMembersWithSharingReason() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User admin = allUsers.get('sysAdmin');
        User user1 = allUsers.get('user1');
        SQX_Test_Audit audit;
        System.runAs(admin){

            //Arrange: Create a audit and add user1 as team member
            audit = new SQX_Test_Audit(admin);
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.save();

            // Assert : Audit should be inserted
            System.assert(audit.audit.Id != null, 'Expected audit to be saved');
            
            // Act : Add audit team with editor role.
            SQX_Audit_Team__c team = new SQX_Audit_Team__c(SQX_Audit__c = audit.audit.id, SQX_User__c = user1.id, Role__c = SQX_Audit_Team.ROLE_EDITOR);
            new SQX_DB().op_insert(new List<SQX_Audit_Team__c> {team}, new List<Schema.SObjectField>{} ) ;
            
            // Assert : Ensure audit is shared with team with sharing reason
            List<SQX_Audit__share> auditShares = [SELECT Id, UserOrGroupId, RowCause, AccessLevel FROM SQX_Audit__share WHERE ParentId = :audit.audit.Id];
            System.assert(auditShares.size() == 3, 'Expected audit to be shared with 3 audit team but is shared with :' +auditShares.size() + 'audit team');
            
            Map<Id, List<SQX_Audit__share>> sharesById = new Map<Id, List<SQX_Audit__share>>();
            for (SQX_Audit__share share : auditShares) {
                List<SQX_Audit__share> auditAhareList = sharesById.get(share.UserOrGroupId);
                if(auditAhareList == null){
                    auditAhareList = new List<SQX_Audit__share>();
                    sharesById.put(share.UserOrGroupId, auditAhareList); 
                }
                auditAhareList.add(share);
            }
           
            // Assert : Owner has two shares with audit
            List<SQX_Audit__share> auditShares1 = sharesById.get(admin.Id);
            System.assert(auditShares1.size() == 2, 'owner has two sharing rule with audit');
            Set<String> rowCause = new Set<String>();
            for (SQX_Audit__share share : auditShares1) {
                rowCause.add(share.AccessLevel + share.RowCause);
            }
            System.assert(rowcause.contains('All' + 'Owner') && rowcause.contains(SQX_Managed_Sharing_Base.SHARING_ACCESS_LEVEL_EDIT + Schema.SQX_Audit__share.RowCause.SQX_Shared__c), 'Expected owner has two row causes');

            // Assert : audit team with member user1 should have sharing access level edit and is being shared with sharing reason
            System.assertEquals(SQX_Managed_Sharing_Base.SHARING_ACCESS_LEVEL_EDIT, sharesById.get(user1.Id)[0].AccessLevel, 'Expected the sharing access level to be edit but is :'+sharesById.get(user1.Id)[0].AccessLevel);
            System.assertEquals(Schema.SQX_Audit__share.RowCause.SQX_Shared__c, sharesById.get(user1.Id)[0].RowCause, 'Expected sharing reason but is '+sharesById.get(user1.Id)[0].RowCause);           
        }
    }
    
    /**
     * Given : Create an Audit with Audit team having Viewer role
     * WHen : team member is made owner of audit
     * Then : Role of audit team is changed to Editor.
     */
    static testmethod void givenAuditWithAuditTeam_WhenTeamMemberWithReadRoleIsMadeOwner_ThenRoleOfTeamMemberChangesToEditor() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User admin = allUsers.get('sysAdmin');
        User user1 = allUsers.get('user1');
        SQX_Test_Audit audit;
        System.runAs(admin){

            // Arrange: Create a audit and add user1 as team member
            audit = new SQX_Test_Audit(admin);
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.save();

            // Assert : Audit should be inserted
            System.assert(audit.audit.Id != null, 'Expected audit to be saved');
            
            // Add audit team with Viewer role.
            SQX_Audit_Team__c team = new SQX_Audit_Team__c(SQX_Audit__c = audit.audit.id, SQX_User__c = user1.id, Role__c = SQX_Audit_Team.ROLE_VIEWER);
            new SQX_DB().op_insert(new List<SQX_Audit_Team__c> {team}, new List<Schema.SObjectField>{} ) ;
            
            // Act : Make user of team member as audit owner
            audit.audit.OwnerId = user1.Id;
            audit.save();
            
            // Assert : role of team shoud be changed from Viewer to Editor
            SQX_Audit_Team__c team1 = [SELECT Id, Role__c FROM SQX_Audit_Team__c WHERE Id = :team.Id];
            System.assertEquals(SQX_Audit_Team.ROLE_EDITOR, team1.Role__c, 'Expected audit team role to be changed to Editor but is found '+team1.Role__c);
        }
    }
    
    /**
     * Given : Audit with audit team
     * WHen : Finding is added to audit
     * Then : Finding with is shared with audit team member with sharing reason
     */
    static testmethod void givenAudit_WhenFindingIsAdded_FindingIsSharedWithTeamMembersWithSharingReason() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User admin = allUsers.get('sysAdmin');
        User user1 = allUsers.get('user1');
        System.runAs(admin){
            
            // Arrange : Create audit and findings
            SQX_Test_Audit audit = new SQX_Test_Audit(admin);
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.save();
            
            // Add audit team
            SQX_Audit_Team__c team = new SQX_Audit_Team__c(SQX_Audit__c = audit.audit.id, SQX_User__c = user1.id, Role__c = SQX_Audit_Team.ROLE_EDITOR);
            new SQX_DB().op_insert(new List<SQX_Audit_Team__c> {team}, new List<Schema.SObjectField>{} ) ;
            
            // create finding
            SQX_Test_Finding auditFinding =  audit.addAuditFinding()
                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                    .save();
            
            // Assert : Finding should be shared with all team member
            List<SQX_Finding__share> findingShares = [SELECT Id, UserOrGroupId, RowCause, AccessLevel FROM SQX_Finding__share WHERE ParentId = :auditFinding.finding.Id];
            System.assert(findingShares.size() == 3, 'Expected finding to be shared with 3 audit team but is shared with :' +findingShares.size() + 'audit team');
            
            Map<Id, List<SQX_Finding__share>> sharesById = new Map<Id, List<SQX_Finding__share>>();
            for (SQX_Finding__share share : findingShares) {
                List<SQX_Finding__share> findingshareList = sharesById.get(share.UserOrGroupId);
                if(findingshareList == null){
                    findingshareList = new List<SQX_Finding__share>();
                    sharesById.put(share.UserOrGroupId, findingshareList); 
                }
                findingshareList.add(share); 
            }
           
            // Assert : Owner has two shares with finding
            List<SQX_Finding__share> findingShares1 = sharesById.get(admin.Id);
            System.assert(findingShares1.size() == 2, 'owner has two sharing rule with finding');

            Set<String> rowCause = new Set<String>();
            for (SQX_Finding__share share : findingShares1) {
                rowCause.add(share.AccessLevel + share.RowCause);
            }
            System.assert(rowcause.contains('All' + 'Owner') && rowcause.contains(SQX_Managed_Sharing_Base.SHARING_ACCESS_LEVEL_EDIT + Schema.SQX_Finding__share.RowCause.Managed_Sharing__c), 'Expected owner has two row causes');

            // Assert : audit team with member user1 should have sharing access level edit and is being shared with sharing reason
            System.assertEquals(SQX_Managed_Sharing_Base.SHARING_ACCESS_LEVEL_EDIT, sharesById.get(user1.Id)[0].AccessLevel, 'Expected the sharing access level to be edit but is :'+sharesById.get(user1.Id)[0].AccessLevel);
            System.assertEquals(Schema.SQX_Finding__share.RowCause.Managed_Sharing__c, sharesById.get(user1.Id)[0].RowCause, 'Expected sharing reason but is '+sharesById.get(user1.Id)[0].RowCause);
        }
    }

    /**
     * Given : Create an audit(audit object is public read).
     * When : An audit is tried to share having read accesslevel with user say user1.
     * Then : It should not be shared with user1 as audit is already public read.
     */
    static testmethod void givenPublicReadAudit_WhenAuditIsSharedHavingReadAccesslevelWithUser_ItShouldNotBeShared() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User admin = allUsers.get('sysAdmin');
        User user1 = allUsers.get('user1');
        System.runAs(admin) {

            // Arrange : Create audit
            SQX_Test_Audit audit = new SQX_Test_Audit(admin).save();

            Map<SObject,List<User>> users = new Map<SObject,List<User>>();//Map of audit Id with Users
            users.put(audit.audit, new List<User>{user1});

            // Act : Share audit with user
            new SQX_Managed_Sharing_Base().withoutSharing().shareRecord(SQX_Managed_Sharing_Base.SHARING_ACCESS_LEVEL_READ,  new List<SQX_Audit__c> {audit.audit}, users, null, Schema.SQX_Audit__Share.RowCause.SQX_Shared__c);
            
            // Assert : audit should not be shared with user1 as we are trying to shared read access with user1 and SQX_Audit__c is already public read
            List<SQX_Audit__Share> auditShares = [SELECT Id, UserOrGroupId, RowCause, AccessLevel FROM SQX_Audit__share WHERE ParentId = :audit.audit.Id AND UserOrGroupId = :user1.Id];
            System.assert(auditShares.size() == 0, 'Expected audit should not be shared with user1 with read access as audit is already public read but is being shared');
        }
    }
}