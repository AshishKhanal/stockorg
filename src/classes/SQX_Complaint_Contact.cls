/**
 * This class contains the logic related to complaint contact
 */
public with sharing class SQX_Complaint_Contact {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified() {
            
        }
        
         /**
        * The constructor for handling the trigger activities
        * @param newComplaintContacts the complaint contacts that have been newly added/updated
        * @param oldMap the map of old values of the compaint contact
        */ 
        public Bulkified(List<SQX_Complaint_Contact__c> newComplaintContacts, Map<Id, SQX_Complaint_Contact__c> oldMap){
            super(newComplaintContacts, oldMap);
        }
        
        /**
         * Method to prevent deletion of complaint contact if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Complaint_Contact__c> complaintContacts = new Map<Id, SQX_Complaint_Contact__c>((List<SQX_Complaint_Contact__c>)this.view);
            
            for(SQX_Complaint_Contact__c complaintContact : [SELECT Id FROM SQX_Complaint_Contact__c WHERE Id IN : complaintContacts.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    complaintContacts.get(complaintContact.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }
        
    }

}