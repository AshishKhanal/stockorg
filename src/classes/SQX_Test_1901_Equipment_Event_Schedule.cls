/**
* Unit tests for creating and updating task for an equipment event schedule
*/
@IsTest
public class SQX_Test_1901_Equipment_Event_Schedule {
    
    static boolean runAllTests = true,
                   run_givenEventScheduleTaskCreation = false,
                   run_givenUpdateEventScheduleNextDueDate_TaskDueDateUpdated = false,
                   run_givenEventSignOff_TaskCompleted = false;
    
    /**
    * unit tests for task creation for event schedule
    */
    public static testmethod void givenEventScheduleTaskCreation() {
        if (!runAllTests && !run_givenEventScheduleTaskCreation) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // create required equipment event schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false,
                Recurring_Event__c = true,
                Recurring_Interval__c = 3,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_MONTH,
                Next_Due_Date__c = System.today() + 2
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // ACT: activate and queue for task creation
            sch1.Active__c = true;
            sch1.Queue_Event_Task__c = true;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            sch1 = [SELECT Id, Next_Due_Date__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            
            System.assert(String.isBlank(sch1.Event_Task_Id__c) == false, 'Event task Id is expected.');
            
            Task t1 = [SELECT Id, WhatId, OwnerId, ActivityDate, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            
            System.assertEquals(sch1.Id, t1.WhatId);
            System.assertEquals(user1.Id, t1.OwnerId);
            System.assertEquals(sch1.Next_Due_Date__c, t1.ActivityDate);
            System.assertEquals('Not Started', t1.Status);
            
            
            // complete task as a part of event sign off
            t1.Status = 'Completed';
            result1 = Database.update(t1, false);
            System.assert(result1.isSuccess(), 'Task is expected to be saved.\n' + result1.getErrors());
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: remove completed task link as a part of event sign off and set next due date
            sch1.Event_Task_Id__c = '';
            sch1.Next_Due_Date__c = sch1.Next_Due_Date__c + 3;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            sch1 = [SELECT Id, Next_Due_Date__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            
            System.assert(String.isBlank(sch1.Event_Task_Id__c) == false, 'Event task Id is expected.');
            System.assertNotEquals((String)t1.Id, sch1.Event_Task_Id__c, 'New event task Id is expected.');
            
            Task t2 = [SELECT Id, WhatId, OwnerId, ActivityDate, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            
            System.assertEquals(sch1.Id, t2.WhatId);
            System.assertEquals(user1.Id, t2.OwnerId);
            System.assertEquals(sch1.Next_Due_Date__c, t2.ActivityDate);
            System.assertEquals('Not Started', t2.Status);
        }
    }
    
    /**
    * unit tests to ensure not-completed task due date to update when event next due date changes
    */
    public static testmethod void givenUpdateEventScheduleNextDueDate_TaskDueDateUpdated() {
        if (!runAllTests && !run_givenUpdateEventScheduleNextDueDate_TaskDueDateUpdated) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // create required equipment event schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false,
                Recurring_Event__c = true,
                Recurring_Interval__c = 3,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_MONTH,
                Next_Due_Date__c = System.today() + 2
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // activate and queue for task creation
            sch1.Active__c = true;
            sch1.Queue_Event_Task__c = true;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // get created task
            sch1 = [SELECT Id, Next_Due_Date__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assert(String.isBlank(sch1.Event_Task_Id__c) == false, 'Task is expected to be created.');
            
            Task t1 = [SELECT Id, WhatId, ActivityDate, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            System.assertEquals(sch1.Id, t1.WhatId);
            System.assertEquals(sch1.Next_Due_Date__c, t1.ActivityDate);
            System.assertEquals('Not Started', t1.Status);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: change event next due date
            sch1.Next_Due_Date__c = System.today() + 10;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // get task information
            sch1 = [SELECT Id, Next_Due_Date__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals((String)t1.Id, sch1.Event_Task_Id__c, 'Task link is not expected to be changed.');
            
            Task t2 = [SELECT Id, WhatId, ActivityDate, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            System.assertNotEquals(t1.ActivityDate, t2.ActivityDate);
            System.assertEquals(sch1.Next_Due_Date__c, t2.ActivityDate);
            
            
            // closing task manually
            t2.Status = 'Completed';
            result1 = Database.update(t2, false);
            System.assert(result1.isSuccess(), 'Task is expected to be saved.\n' + result1.getErrors());
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: change event next due date
            sch1.Next_Due_Date__c = System.today() + 5;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // get task information
            sch1 = [SELECT Id, Next_Due_Date__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals((String)t2.Id, sch1.Event_Task_Id__c, 'Task link is not expected to be changed.');
            
            Task t3 = [SELECT Id, WhatId, ActivityDate, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            System.assertEquals(t2.ActivityDate, t3.ActivityDate);
            System.assertNotEquals(sch1.Next_Due_Date__c, t3.ActivityDate);
        }
    }
    
    /**
    * unit tests to task competion when event is signed off
    */
    public static testmethod void givenEventSignOff_TaskCompleted() {
        if (!runAllTests && !run_givenEventSignOff_TaskCompleted) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // create required equipment event schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false,
                Recurring_Event__c = true,
                Recurring_Interval__c = 3,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_MONTH,
                Next_Due_Date__c = System.today() + 2
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // activate and queue for task creation
            sch1.Active__c = true;
            sch1.Queue_Event_Task__c = true;
            result1 = Database.update(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // get created task
            sch1 = [SELECT Id, Name, SQX_Equipment__c, Active__c, Next_Due_Date__c, Last_Performed_Date__c, Recurring_Event__c, Recurring_Interval__c,
                           Recurring_Unit__c, Schedule_Basis__c, Queue_Event_Task__c, Event_Task_Id__c
                    FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assert(String.isBlank(sch1.Event_Task_Id__c) == false, 'Task is expected to be created.');
            Task t1 = [SELECT Id, Status FROM Task WHERE Id = :sch1.Event_Task_Id__c];
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);
            
            // set up sign-off data
            ext.eventHistory.Performed_Date__c = System.today();
            ext.eventHistory.Performed_By__c = 'Mr Maintenance';
            
            // ACT: event sign-off
            PageReference pr = ext.signOff();
            
            System.assert(pr != null, 'Page reference is expected.');
            
            sch1 = [SELECT Id, Queue_Event_Task__c, Event_Task_Id__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            
            System.assertEquals(false, sch1.Queue_Event_Task__c);
            System.assert(String.isBlank(sch1.Event_Task_Id__c) == true, 'Completed task id is expected to be cleared.');
            
            t1 = [SELECT Id, Status FROM Task WHERE Id = :t1.Id];
            
            System.assertEquals('Completed', t1.Status);
        }
    }
}