/**
 * This class stores the constants, methods and workflow logic for SQX_Task__c object aka Task Library Object
*/
public with sharing class SQX_Task extends SQX_Trigger_Handler {
    // Task Type for CQ Task
    public static final String  TYPE_DECISION_TREE = 'Decision Tree',
                                TYPE_SCRIPT = 'Script',
                                TYPE_INVESTIGATION = 'Investigation',
                                TYPE_PHR = 'PHR',
                                TYPE_SAMPLE_REQUEST = 'Sample Request';

    // record type for CQ Task
    public static final String RECORD_TYPE_COMPLAINT_MANAGEMENT_STEP = 'Complaint_Management_Step',
                                RECORD_TYPE_SUPPLIER_MANAGEMENT_STEP = 'Supplier_Management_Step';
                                
    //This static field defines the string constant for Custom field called Record Type in Task object. Note this is different from standard record type.
    public static final String  RTYPE_COMPLAINT = 'Complaint',
                                RTYPE_SUPPLIER_INTRODUCTION = 'Supplier Introduction',
                                RTYPE_SUPPLIER_DEVIATION = 'Supplier Deviation';
    
    // defines the status of the task object.
    public static final String STATUS_COMPLETED = 'Completed',
                                STATUS_NOT_STARTED = 'Not Started';


    public static final String PRIORITY_HIGH = 'High',
                                PRIORITY_NORMAL = 'Normal',
                                PRIORITY_LOW = 'Low';

    public static final String TASK_SUBJECT_DOCUMENT_REVIEW = 'Please review the document.',
                               TASK_SUBJECT_CLOSURE_REVIEW = 'Closure review required.',
                               TASK_SUBJECT_EFFECTIVENESS_REVIEW = 'Effectiveness review pending.',
                               TASK_SUBJECT_OPEN_CAPA = 'Please respond to the CAPA.',
                               TASK_SUBJECT_REGULATORY_REPORT = 'Regulatory Reporting -  {0}';

    // defines task type to be created in the corresponding records
    public static final String TASK_TYPE_TASK = 'Task',
                               TASK_TYPE_DOC_REQUEST = 'Document Request',
                               TASK_TYPE_PERFORM_AUDIT = 'Perform Audit',
                               TASK_TYPE_APPROVAL = 'Approval';

    // record type of salesforce task
    public static final String RECORD_TYPE_CQ_TASK = 'CQ_Task';

    // answer type for question
    public static final String ANSWER_TYPE_OPTIONS_RADIO_BUTTON = 'Options - Radio Button',
                                ANSWER_TYPE_OPTIONS_PICKLIST = 'Options - Picklist',
                                ANSWER_TYPE_PICKLIST_VALUES = 'Picklist Values',
                                ANSWER_TYPE_TEXT_INPUT = 'Text Input',
                                ANSWER_TYPE_DATE_INPUT = 'Date Input',
                                ANSWER_TYPE_NUMBER_INPUT = 'Number Input',
                                ANSWER_TYPE_CUSTOM_COMPONENT = 'Custom Component';
    
    public static final String  BATCH_JOB_STATUS_ACTIVATION_MIGRATION_COMPLETED = '9.1 Migration Completed',
                                BATCH_JOB_STATUS_ACTIVATION_MIGRATION_FAILED = '9.1 Migration Failed',
                                BATCH_JOB_STATUS_DEFAULT = 'Created 9.1 Onwards';
                                
    public final static List<String> stepObjListToPreventDelete = new List<String>{SQX.OnBoardingStep, SQX.DeviationProcess, SQX.SupplierEscalationStep, SQX.SupplierInteractionStep, SQX.ComplaintTask};

    public static final String ACTION_COMPLETED_TASKS = 'completedTasks';
    /**
     * Used to cache the list of closed statuses so we don't end up querying multiple times in same transaction
     */
    static Set<String> closedStatuses = null;
    
    /**
    * set objects which are using perform audit record type
    */
    public static Set<String> objectMap = new Set<String>{ SQX.OnBoardingStep, SQX.DeviationProcess, SQX.SupplierInteractionStep, SQX.SupplierEscalationStep};

    // Used to hold set ids of tasks to be deleted
    static Set<Id> deleteAllowedTasks = new Set<Id>();

    /**
     * Gets the list of status that represent closed statuses(end statuses) of a task.
     * @returns list of closed statuses
     */
    public static Set<String> getClosedStatuses() {
        if (closedStatuses == null) {
            closedStatuses = new Set<String>();
            for(TaskStatus status : [SELECT ApiName FROM TaskStatus WHERE IsClosed = true]) {
                closedStatuses.add(status.ApiName);
            }
        }

        return closedStatuses;
    }

    /**
     * Method to set Tasks to be deleted
    */
    public static void allowDeletionFor(Set<Id> idsOfTasks){
        deleteAllowedTasks = idsOfTasks;
    }

    /**
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newSFTasks equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<Task> newSFTasks, Map<Id,Task> oldMap){
            
            super(newSFTasks, oldMap);
        }

        /**
        * This method prevents from deletion of task if it's parent is not closed or in void status
        * @author Sanjay Maharjan
        * @date 2018/8/27
        * @story [SQX-6366]
        * @return String error message
        */
        public Bulkified preventDeleteIfNotCompleted(){
            Rule notCompletedTaskRule = new Rule();
            notCompletedTaskRule.addRule(Schema.Task.Status, RuleOperator.NotEquals, STATUS_COMPLETED);
            this.resetView().applyFilter(notCompletedTaskRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<Task> taskList = (List<Task>) notCompletedTaskRule.evaluationResult;
            
            for (Task t : taskList ){
                if(String.isNotBlank(t.Child_What_Id__c)){
                    Id childId = (Id) t.Child_What_Id__c;
                    String objName = childId.getSObjectType().getDescribe().getName();
                    
                    if(stepObjListToPreventDelete.contains(objName) && !deleteAllowedTasks.contains(t.Id)) {
                        t.addError(Label.SQX_ERR_MSG_WHEN_DELETING_TASK_BEFORE_COMPLETION);
                    }
                }
            }

            return this;
        }

        /**
         * method to complete related task when sf task is completed
         */
        public Bulkified completeRelatedTasks(){
            this.resetView().removeProcessedRecordsFor('Task', ACTION_COMPLETED_TASKS);
            String actionName = 'completeRelatedTasks';
            List<Task> completedTaskList = new List<Task>();
            Map<String, List<SObject>> sobjectNameToRecord = new Map<String, List<SObject>>();

            for(Task t :  (List<Task>) this.view){
                if(String.isNotBlank(t.Child_What_Id__c)){
                    Id stepId = (Id) t.Child_What_Id__c;
                    SObject stepRecord = stepId.getSObjectType().newSObject();
                    stepRecord.put('id', stepId);
                    String sobjectName = stepId.getSObjectType().getDescribe().getName();
                    

                    if(getClosedStatuses().contains(t.Status) && stepObjListToPreventDelete.contains(sobjectName)){
                        completedTaskList.add(t);
                        List<SObject> stepRecords = sobjectNameToRecord.get(sobjectName);
                        if (stepRecords == null) {
                            stepRecords = new List<SObject>();
                            sobjectNameToRecord.put(sobjectName, stepRecords);
                        }
                        stepRecords.add(stepRecord);
                    }
                }

            }

            // add all processed step records to processedEntities so that they can be removed in next execution thus prevents recursion
            for (String sobjectName : sobjectNameToRecord.keySet()) {
                this.addToProcessedRecordsFor(sobjectName, SQX_Steps_Trigger_Handler.ACTION_COMPLETED_STEPS, sobjectNameToRecord.get(sobjectName));
            }
            

            if(completedTaskList.size() > 0){
                SQX_Task t = new SQX_Task();
                t.completeSteps(completedTaskList);
            }

            return this;
        }
    }
    
    /**
     * complete step when task is completed
     */
    public void completeSteps(List<Task> completedTaskList){
        /*
        * WITHOUT SHARING used
        * --------------------
        * when one of the assignee complete the task of the current step, 
        * it tries to update the status of the step, but assignee may have not have edit access to workflow policy 
        */
        Action act = getEscalatedAction();
        Map<Id, Task> taskMap = new Map<Id, Task>(completedTaskList);
        for(Task sfTask : completedTaskList){
            if(sfTask.Child_What_Id__c != null){
                
                Id childId = Id.valueOf(sfTask.Child_What_Id__c);
                String stepObjectName = childId.getSObjectType().getDescribe().getName();
                SObject stepRecord = Schema.getGlobalDescribe().get(stepObjectName).newSObject();
                
                stepRecord.put('Id', sfTask.Child_What_Id__c);
                stepRecord.put(SQX_Steps_Trigger_Handler.FIELD_COMMENT, sfTask.Description);
                stepRecord.put(SQX_Steps_Trigger_Handler.FIELD_STATUS, SQX_Steps_Trigger_Handler.STATUS_COMPLETE);
                stepRecord.put(SQX_Steps_Trigger_Handler.FIELD_COMPLETION_DATE, Date.today());
                
                act.op_update(taskMap.get(sfTask.Id), stepRecord);
            }  
        }
        
        act.op_commit();
    }
}