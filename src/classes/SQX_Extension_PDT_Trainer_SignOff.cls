/*
* Used for Trainer SignOff
*/
public with sharing class SQX_Extension_PDT_Trainer_SignOff extends SQX_Extension_PDT_SignOff_Base {
    
    public final static String URL_KEY_FOR_ID = 'dtnId';
    
    public SQX_Extension_PDT_Trainer_SignOff(ApexPages.StandardSetController controller) {
        super(controller, URL_KEY_FOR_ID, true);
    }
    
    /**
     * Redirects user to lightning component for trainer signOff
     */
    public PageReference recordSignOff(){
        String orgURL = SQX_Utilities.getBaseUrlForUser(true);
        String redirectUrl = orgURL + '/lightning/cmp/compliancequest__SQX_Training_Sign_Off_Records';
        Set<Id> pdts = new Set<Id>();
        List<SQX_Personnel_Document_Training__c> pdtList= getDocumentTrainingsToSignOff();
        pdtList.addAll(getDocumentTrrainingsRequiringAssessmentToPass());
        pdtList.addAll(getDocumentTrainingsNotRequiringSignOff());
        for(SQX_Personnel_Document_Training__c pdt : pdtList) {
           	pdts.add(pdt.Id);
        }
        PageReference pageRef = new PageReference(redirectUrl);
        pageRef.setRedirect(true);
        pageRef.getParameters().put('compliancequest__recordId',String.join(new List<Id>(pdts), ','));
        pageRef.getParameters().put('compliancequest__origin','trainerSignOff');
        return pageRef;
    }
}