/*
* This class used for controlled document collaboration
*/
global with sharing class SQX_Extension_Controlled_Doc_Collaborate {
    global String[] UserIDs { get; set; }
    global String CollaborationTitle { get; set;}
    public SQX_Controlled_Document__c cdInfo;
    public static final String  CODE_OF_SIG_INITIATE_COLLABORATION  = 'collaborate';
    Map<String, String> purposeOfSigMap = new Map<String, String> {
        CODE_OF_SIG_INITIATE_COLLABORATION => Label.SQX_PS_Initiate_Collaboration
    };

    global SQX_Extension_Controlled_Doc_Collaborate(ApexPages.StandardController controller){
        cdInfo = (SQX_Controlled_Document__c)controller.getRecord();
        CollaborationTitle = Label.CQ_UI_Collaboration_Title.Replace('{Document Number}',cdInfo.Document_Number__c).Replace('{Revision Number}',cdInfo.Revision__c == null ? '' : cdInfo.Revision__c);
    }


    /*
    * This method is used to save and initiate collaboration
    */ 
    global PageReference saveAndInitiateCollaboration(){
        SavePoint sp = null;
        String groupId;
        SQX_CollaborationProvider provider = SQX_Controlled_Document.createCollaborationProvider(cdInfo);
        try{

            Map<String,String> groupInfo = provider.initiateCollaboration(CollaborationTitle, UserIDs, cdInfo.Id); 

            if( groupInfo != null ){

                //set savepoint
                sp = Database.setSavePoint();

                groupId = groupInfo.get(SQX_SF_Collaboration_Provider.KEY_COLLABORATION_GROUP_ID);

                //  checking out and adding collaboration details to document
                SQX_Controlled_Document_Collaboration.checkOutForCollaboration(cdInfo, groupId, groupInfo.get(SQX_SF_Collaboration_Provider.KEY_COLLABORATION_CONTENT_ID));

                //track record history
                SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Controlled_Document__c.SObjectType), cdInfo.Id, string.valueOf(cdInfo.Id), Label.CQ_UI_Collaborate_Doc_Comment, purposeOfSigMap.get(CODE_OF_SIG_INITIATE_COLLABORATION), CODE_OF_SIG_INITIATE_COLLABORATION);

                return new PageReference('/'+ groupId);

            }

        }catch(Exception ex){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage())); 
            // rollback savepoint
            if(sp != null) {
                Database.rollback(sp);
            }
            provider.undoCollaborationInitiation(groupId);
        }
        return null;
    }

    /*
    * get all approvals user list
    */ 
    global List<SelectOption> getApprovalUsers(){
        SQX_Custom_Settings_Public__c cqSetting = SQX_Custom_Settings_Public__c.getInstance();
        List<SelectOption> userSelectedOptions=new List<SelectOption>();
        List<SQX_Controlled_Document_Approval__c> approvalInfo=new List<SQX_Controlled_Document_Approval__c>();
        if(cqSetting.Use_Approval_Matrix_For_Document__c){
            approvalInfo=[SELECT SQX_User__c,SQX_User__r.Name FROM SQX_Controlled_Document_Approval__c WHERE SQX_Controlled_Document__r.Id=:cdInfo.Id AND SQX_User__r.Id !=:UserInfo.getUserId() AND SQX_User__c != null ]; 
            if(approvalInfo.size()>0){
                for(SQX_Controlled_Document_Approval__c obj: approvalInfo){
                    userSelectedOptions.add(new SelectOption(obj.SQX_User__c,obj.SQX_User__r.Name));
                }
            }
        }else{
            SQX_Controlled_Document__c cdObj=[SELECT First_Approver__c, First_Approver__r.Name, Second_Approver__c,Second_Approver__r.Name,
                                              Third_Approver__c,Third_Approver__r.Name,Fourth_Approver__c,Fourth_Approver__r.Name,
                                              Fifth_Approver__c,Fifth_Approver__r.Name  FROM SQX_Controlled_Document__c WHERE Id=:cdInfo.Id];
                if(cdObj.First_Approver__c!=null && cdObj.First_Approver__c!=UserInfo.getUserId()){
                    userSelectedOptions.add(new SelectOption(cdObj.First_Approver__c,cdObj.First_Approver__r.Name));
                  }
                if(cdObj.Second_Approver__c!=null && cdObj.Second_Approver__c!=UserInfo.getUserId()){
                    userSelectedOptions.add(new SelectOption(cdObj.Second_Approver__c,cdObj.Second_Approver__r.Name));
                  }
                if(cdObj.Third_Approver__c!=null && cdObj.Third_Approver__c!=UserInfo.getUserId()){
                    userSelectedOptions.add(new SelectOption(cdObj.Third_Approver__c,cdObj.Third_Approver__r.Name));
                  }
                if(cdObj.Fourth_Approver__c!=null && cdObj.Fourth_Approver__c!=UserInfo.getUserId()){
                    userSelectedOptions.add(new SelectOption(cdObj.Fourth_Approver__c,cdObj.Fourth_Approver__r.Name));
                  }
                if(cdObj.Fifth_Approver__c!=null && cdObj.Fifth_Approver__c!=UserInfo.getUserId()){
                    userSelectedOptions.add(new SelectOption(cdObj.Fifth_Approver__c,cdObj.Fifth_Approver__r.Name));
                 }
        }
        return userSelectedOptions;
    }
}