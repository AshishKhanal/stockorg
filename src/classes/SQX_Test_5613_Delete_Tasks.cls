/**
* unit test for tasks delete when NSI record is void and open onboarding changes to draft
*/
@isTest
public class SQX_Test_5613_Delete_Tasks {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
    }
    
    /**
    * GIVEN : given NSI record with Onboarding steps and SF Tasks
    * WHEN : void the record
    * THEN : delete SF tasks and open onboarding steps change to draft
    */
    public static testMethod void GivenNSI_WhenVoidTheRecord_ThenDeleteTasksAndOnboardingStepsOpenChangeToDraft(){ 
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            // ARRANGE: NSI is created and policy Tasks 
            SQX_Test_NSI.createPolicyTasks(2, 'Task', adminUser, 1);
            SQX_Test_NSI.createPolicyTasks(2, 'Task', adminUser, 2);
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            nsi.submit();
            
            System.assertEquals(4, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id].size());
            
            // ACT: NSI is initiated
            nsi.initiate();
            
            // ASSERT: SF Task with lowest step are open
            System.assertEquals(2, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            // ASSERT: NSI Status is in progress
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, [SELECT Id, WOrkflow_Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Workflow_Status__c);
            
            //Assert: Tasks created
            System.assertEquals(2, [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id].size());
            
            //Act: NSI is void
            nsi.void();
            
            //Assert: Ensured that tasks deleted and open Onboarding steps changed to draft
            System.assertEquals(0, [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
        } 
    }
    
    /**
    * GIVEN : given NSI record with Onboarding steps and SF Tasks
    * WHEN : void the record
    * THEN : clear audit number in onboarding step
    */
    public static testMethod void GivenNSI_WhenVoidTheRecord_ThenClearAuditNumberInOnboardingStep(){ 
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            // ARRANGE: NSI is created and policy Tasks 
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_PERFORM_AUDIT, adminUser, 1);
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            nsi.submit();
            nsi.initiate();

            // ASSERT: NSI Status is in progress
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, [SELECT Id, WOrkflow_Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Workflow_Status__c);
            
            SQX_Test_Audit auditRecord = new SQX_Test_Audit().save();
            SQX_Onboarding_Step__c onboardingRecord = [SELECT Id,SQX_Audit_Number__c FROM SQX_Onboarding_Step__c];
            onboardingRecord.SQX_Audit_Number__c = auditRecord.audit.id;
            update onboardingRecord;
            System.assertEquals(auditRecord.audit.id, [Select sqx_audit_number__c from SQX_Onboarding_Step__c].sqx_audit_number__c);
            
            //Act: NSI is voided
            nsi.void();
            
            //Assert: Ensured that perform audit onboarding step have null value in audit_number
            System.assertEquals(null, [Select sqx_audit_number__c from SQX_Onboarding_Step__c].sqx_audit_number__c,'Expected, audit number to be blank if NSI is voided');
        } 
    }

    /**
    * GIVEN : given NSI record with Onboarding steps and SF Tasks
    * WHEN : closed the record
    * THEN : delete SF tasks and open onboarding steps change to draft
    */
    public static testMethod void GivenNSI_WhenClosedTheRecord_ThenDeleteTasksAndOnboardingStepsOpenChangeToDraft(){ 
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            // ARRANGE: NSI is created and policy Tasks 
            SQX_Test_NSI.createPolicyTasks(2, 'Task', adminUser, 1);
            SQX_Test_NSI.createPolicyTasks(2, 'Task', adminUser, 2);
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            nsi.submit();
            
            // ACT: NSI is initiated
            nsi.initiate();
            
            //Act: NSI is closed
            nsi.close();
            
            //Assert: Ensured that tasks deleted and open Onboarding steps changed to draft
            System.assertEquals(0, [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
        } 
    }
}