/**
* This test class checks for the following functionality in the document folder management.
*/
@isTest
public class SQX_Test_2342_Document_Folder_Management {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
   /**
    * When : When a Binder Id is selected 
    * Then : correct details of the binder are returned
    */
    testmethod static void whenBinderDetailIsRequested_BinderDetailsAreReturned(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Create a Binder
            SQX_Binder__c binder =new SQX_Binder__c(Name = 'Quality Management');
            new SQX_DB().op_insert(new List<SObject> { binder }, new List<SObjectField> {
                SQX_Binder__c.Name, SQX_Binder__c.SubFolders__c, SQX_Binder__c.Filter__c 
            });

            // Act: Request for binder details
            SQX_Binder__c resultObj=SQX_Document_Folder_Management.getBinder(binder.Id);
            
            // Assert: Ensure that the correct binder is returned
            System.assertEquals(binder.Id, resultObj.Id);
            
            
            // ACT (SQX-8211): update SQX_Binder__c
            update binder;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Binder__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Binder__c.SObjectType);
        }
            
       
    }
    
   /**
    * Given: A user with access to binders
    * When : List of binders is requested 
    * Then : the user is able to get the list of binders
    */
    public testmethod static void givenBinders_WhenBinderListIsRequested_ThenAccessibleBindersAreReturned(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            // Arrange: Create Binders as admin 
            List<SQX_Binder__c> binderList=new List<SQX_Binder__c>();
            SQX_Binder__c binder1 =new SQX_Binder__c(Name='Test1');
            SQX_Binder__c binder2 =new SQX_Binder__c(Name='Test2');
            binderList.add(binder1);
            binderList.add(binder2);
            List<Database.SaveResult> result = Database.insert(binderList, false);
            
            // Act: Request for the binder list
            List<SQX_Binder__c> resultList=SQX_Document_Folder_Management.getBinderList(1, 2000);
            
            // Assert: Ensure that both binders are returned
            System.assertEquals(2,resultList.size());
        }

        System.runAs(allUsers.get('standardUser')){
            // Act: Request for the binder list
            List<SQX_Binder__c> resultList=SQX_Document_Folder_Management.getBinderList(1, 2000);
            
            // Assert: Ensure that both binders are returned
            System.assertEquals(0,resultList.size());
        }
    }
    
       
    /**
    * Given: A controlled document with related document
    * When : related document list is requested
    * Then : the returned list contains the doc.
    */ 
    public testmethod static void whenRelatedDocumentIsRequested_ReturnedListContainsRelatedDoc(){
        final String EMPTY_FILTER = '{}', EMPTY_TEXT = '';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Create two controlled documents with reference. 
            SQX_Test_Controlled_Document docToRefer=new SQX_Test_Controlled_Document().save();
            SQX_Test_Controlled_Document docInfo=new SQX_Test_Controlled_Document().save();
            docInfo.addRelationTo(docToRefer,false, false);
            
            // Act : Fetch the related document list for document
            List<SQX_Document_Folder_Management.Document> docList = SQX_Document_Folder_Management.getRelatedDocuments(docInfo.doc.Id, null, null);
            
            // Assert: ensure that related document is returned.
            System.assertEquals(1, docList.size());
            System.assertEquals(docToRefer.doc.Id, docList.get(0).doc.Id);


            // Act: Fetch the related document count for document using search
            String filter = '{"field":"Id","operator":"eq","s_value":"' + docInfo.doc.Id  + '"}';
            docList = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, filter, null, null, null).documents;

            // Assert: Ensure that record is returned with corrected related count
            System.assertEquals(1, docList.size());
            System.assertEquals(1, docList.get(0).relatedCount);

        }
    }


    /**
    * When : controlled document are filtered by combination of document filter, topic, folder filter and search text
    * Then : matching documents are returned
    * 
    * Note: for some reason creating folder in test isn't supported and returns invalid cross reference id error.
    * Note: this test method also for [SQX-2473]
    */ 
    public testmethod static void givenSearchTextandFolderFilter_WhenSearchandClick_ListMatchingDocuments(){
        final String EMPTY_FILTER = '{}', EMPTY_TEXT = '';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){

            // Arrange: Create four documents
            //          2 of which are normal controlled documents (doc1, doc2)
            //          1 has a different name
            //          1 has a topic (doc4)
            List<SQX_Test_Controlled_Document> docList = new List<SQX_Test_Controlled_Document>();
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save();
            SQX_Test_Controlled_Document doc3 = new SQX_Test_Controlled_Document();
            SQX_Test_Controlled_Document doc4 = new SQX_Test_Controlled_Document().save();

            doc3.doc.Document_Number__c = 'Hello World Doc';
            doc3.doc.Title__c = 'Another Title';
            doc3.save();
            
            SObjectType topicType=Schema.getGlobalDescribe().get('Topic');
            SObjectType topicAssignmentType = Schema.getGlobalDescribe().get('TopicAssignment');
            if(topicType!=null && topicAssignmentType != null){
                SObject topic1=topicType.newSObject(); 
                topic1.put('Name', 'Topic1');
                insert topic1;
                
                SObject topicAsmt1 = topicAssignmentType.newSObject();
                topicAsmt1.put('EntityId', doc4.doc.Id);
                topicAsmt1.put('TopicId', topic1.Id);
                insert topicAsmt1;
                
                SObject topic2=topicType.newSObject(); 
                topic2.put('Name', 'Topic2');
                insert topic2;
                
                SObject topicAsmt2 = topicAssignmentType.newSObject();
                topicAsmt2.put('EntityId', doc3.doc.Id);
                topicAsmt2.put('TopicId', topic2.Id);
                insert topicAsmt2;
            }
            Test.startTest();

            // Act: Query using document filter only
            String documentFilterString = '{"field":"compliancequest__Title__c","operator":"startswith","s_value":"Doc"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, documentFilterString,null, null, null).documents;
            
            // Assert: Ensure that there are four documents
            System.assertEquals(3, matchingDocuments.size(), 'Expected all document to match topicT filter' + doc1.doc);


            // Act 2: Query using topic filter only
            String topicFilterString = '{"field":"Name","operator":"eq","s_value":"Topic1"}';
            matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, topicFilterString, EMPTY_FILTER,null, null, null).documents;

            // Assert 2: Ensure that one document (doc4) is returned
            System.assertEquals(1, matchingDocuments.size(), 'Expected only one document to match topic filter');
            System.assertEquals(doc4.doc.Id, matchingDocuments.get(0).doc.Id, 'Expected only 4th doc to be in topic filter list');


            // Act 3: Query using topic filter and document filter
            documentFilterString = '{"field":"compliancequest__Document_Number__c","operator":"eq","s_value":"Hello World Doc"}';
            topicFilterString = '{"field":"Name","operator":"eq","s_value":"Topic2"}';
            matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, topicFilterString, documentFilterString,null, null, null).documents;

            // Assert 3: Ensure that one document (doc4) is returned
            System.assertEquals(1, matchingDocuments.size(), 'Expected only one document to match both topic and document filter');
            System.assertEquals(doc3.doc.Id, matchingDocuments.get(0).doc.Id, 'Expected only 3th doc to be in filter list');


            // Arrange 4: Since SOSL queries aren't supported in test we need to setup data that will be returned by SOSL query
            Test.setFixedSearchResults(new Id[] { doc3.doc.Id, doc4.doc.Id });

            // Act 4: Query using search filter, document filter and topicFilter
            matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, 'Hello', EMPTY_FILTER, topicFilterString, documentFilterString,null, null, null).documents;

            // Assert 3: Ensure that one document (doc4) is returned
            System.assertEquals(1, matchingDocuments.size(), 'Expected only one document to match text and topic & document filter');
            System.assertEquals(doc3.doc.Id, matchingDocuments.get(0).doc.Id, 'Expected only 3th doc to be in filter list');


            // Smoke Test: Folder can't be created in test so it results in an error. Test case for coverage purpose only
            //             Smoke test for combinatiion of topic and folder filter
            String folderFilterString = '{"field":"Name","operator":"eq","s_value":"Folder_Test_1"}';
            matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, folderFilterString, topicFilterString, EMPTY_FILTER,null, null, null).documents;
            System.assertEquals(0, matchingDocuments.size(), 'Expected no documents to be in folder');

            // Smoke Test: Global search, set the docs to be returned and ensure that they are returned
            Test.setFixedSearchResults(new Id[] { doc3.doc.Id, doc4.doc.Id });
            matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs('Hello', EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, EMPTY_FILTER,null, null, null).documents;
            System.assertEquals(2, matchingDocuments.size(), 'Expected two document to match global filter');


            Test.stopTest();
        }
    }


    /**
     * Given : A user
     * When : Default Binder is set
     * Then : The default value is persisted correctly
     */ 
    testmethod static void givenDefaultFolder_ThenSaveInformation(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){           
            // Arrange: Create default folder as admin 
            String binderId='ayluuuygbbbb';
            String defaultFolderPath='2.1.1';

            // Act: Save default folder information
            SQX_Document_Folder_Management.setDefaultFolder(binderId,defaultFolderPath);
            SQX_User_Default_Option__c defaultFolder= [SELECT BinderId__c, DefaultFolder__c, SetupOwnerId 
                                                        FROM SQX_User_Default_Option__c
                                                        WHERE SetupOwnerId = : adminUser.Id];
            
            // Assert: Ensure that setupownerId equals to current user
            System.assertEquals(binderId, defaultFolder.BinderId__c);
            System.assertEquals(defaultFolderPath, defaultFolder.DefaultFolder__c);
        }

        User standUser= allUsers.get('standardUser');
        System.runAs(standUser){
            // Act: get default value for standardUser
            SQX_User_Default_Option__c option = SQX_Document_Folder_Management.getDefaultFolderInfo();

            // Assert: Ensure that null is returned because no default has been set
            System.assertEquals(null, option);

            // Arrange: Create default folder as admin 
            String binderId='stkbbkbmmb';
            String defaultFolderPath='2.1.2';
            
            // Act: Save default folder information
            SQX_Document_Folder_Management.setDefaultFolder(binderId,defaultFolderPath);

            // Assert: Ensure that option is set correctly
            option = SQX_Document_Folder_Management.getDefaultFolderInfo();
            System.assertEquals(binderId, option.BinderId__c);
            System.assertEquals(defaultFolderPath, option.DefaultFolder__c);
        }
    }


    /**
    * Given: Two controlled docs one with secondary enabled. (Doc1 Primary: .txt, Secondary : .pdf ; Doc2 Primary : .xlsx)
    * When : docs are requested
    * Then : the returned list contains the doc with proper extension.
    */ 
    public testmethod static void whenControlledDocumentAreRequest_ExtensionIsSetCorrectly(){
        final String EMPTY_FILTER = '{}', EMPTY_TEXT = '';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){

            // Arrange: Create two controlled documents
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document();

            doc1.updateContent(true, 'TestDocument.txt');
            doc1.updateContent(false, 'TestDocument.pdf'); //secondary content

            doc2.updateContent(true, 'TestDocument.xlsx');

            doc1.save();
            doc2.save();
            
            // Act: Fetch the related document count for document using search
            String filter = '{"field":"Name","operator":"startswith","s_value":"Doc"}';
            List<SQX_Document_Folder_Management.Document> docList = SQX_Document_Folder_Management.getMatchingDocs(EMPTY_TEXT, EMPTY_TEXT, EMPTY_FILTER, EMPTY_FILTER, filter, null, null, null).documents;

            // Assert: Ensure that record is returned with corrected extension
            System.assertEquals(2, docList.size());
            Map<Id, SQX_Document_Folder_Management.Document> docs = new Map<Id, SQX_Document_Folder_Management.Document>{
                docList.get(0).doc.Id => docList.get(0),
                docList.get(1).doc.Id => docList.get(1)
            };

            System.assertEquals('pdf', docs.get(doc1.doc.Id).extension, 'Doc1 should have pdf as extension');
            System.assertEquals('xlsx', docs.get(doc2.doc.Id).extension, 'Doc2 should have xlsx as extension');


        }
    }

    /**
     * Given Controlled Documents with following topics
     *       DOC1   Topic A
     *       DOC2   Topic B
     *       DOC3   Topic A, Topic B
     *      and following folder structure
     *      Folder 1 (Topic Filter: Topic A)
     *           Folder 1.1. (Topic Filter: Topic B)
     *      Folder 2 (Topic Filter: Topic B)
     *      Folder 3 (Topic Filter: Topic A or Topic B)
     * When a query in document folder is done
     * Then following result are returned
     *      Folder 1    DOC 1, DOC 3
     *      Folder 1.1  DOC 3
     *      Folder 2    DOC 2, DOC 3
     *      Folder 3    DOC 3 [Since folder filter ignores or operator and considers it as and]
     */
    static testmethod void givenDocumentWithTwoTopics_whenTopicQueriedUsingAnd_ThenTheDocumentIsReturned() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Controlled_Document doc1, doc2, doc3;

        System.runAs(adminUser){

            // Arrange: Create three controlled documents and folder filter
            doc1 = new SQX_Test_Controlled_Document().save();
            doc2 = new SQX_Test_Controlled_Document().save();
            doc3 = new SQX_Test_Controlled_Document().save();

            SObjectType topicType=Schema.getGlobalDescribe().get('Topic');
            SObjectType topicAssignmentType = Schema.getGlobalDescribe().get('TopicAssignment');
            if(topicType!=null && topicAssignmentType != null){
                SObject topic1 = topicType.newSObject();
                topic1.put('Name', 'Topic A');

                SObject topic2 = topicType.newSObject();
                topic2.put('Name', 'Topic B');
                insert new SObject[] { topic1, topic2 };

                SObject topicAsmt1 = topicAssignmentType.newSObject();
                topicAsmt1.put('EntityId', doc1.doc.Id);
                topicAsmt1.put('TopicId', topic1.Id);

                SObject topicAsmt2 = topicAssignmentType.newSObject();
                topicAsmt2.put('EntityId', doc2.doc.Id);
                topicAsmt2.put('TopicId', topic2.Id);

                SObject topicAsmt3 = topicAssignmentType.newSObject();
                topicAsmt3.put('EntityId', doc3.doc.Id);
                topicAsmt3.put('TopicId', topic1.Id);

                SObject topicAsmt4 = topicAssignmentType.newSObject();
                topicAsmt4.put('EntityId', doc3.doc.Id);
                topicAsmt4.put('TopicId', topic2.Id);
                insert new SObject[] { topicAsmt1, topicAsmt2, topicAsmt3, topicAsmt4 };
            }
            else {
                // no point failing test in org without topic
                return;
            }
        }

        System.runAs(standardUser) {
            final String [] topicQueries = new String[] {
                '{"field" : "Name", "operator" : "eq", "s_value" : "Topic A"}', // folder 1
                '{"field" : "Name", "operator" : "eq", "s_value" : "Topic B"}', // folder 2
                '{"logic" : "or", "filters" : [ {"field" : "Name", "operator" : "eq", "s_value" : "Topic A"}, {"field" : "Name", "operator" : "eq", "s_value" : "Topic B"} ] }', // folder 3
                '{"logic" : "and", "filters" : [ {"field" : "Name", "operator" : "eq", "s_value" : "Topic A"}, {"field" : "Name", "operator" : "eq", "s_value" : "Topic B"} ] }' // folder 1.1
            };

            Map<String, Set<Id>> expectation = new Map<String, Set<Id>>{
                topicQueries[0] => new Set<Id> {doc1.doc.Id, doc3.doc.Id},
                topicQueries[1] => new Set<Id> {doc2.doc.Id, doc3.doc.Id},
                topicQueries[2] => new Set<Id> {doc3.doc.Id},
                topicQueries[3] => new Set<Id> {doc3.doc.Id}
            };

            // Act: Get the result of the query
            for(String query : expectation.keySet()) {
                SQX_Document_Folder_Management.Result result = SQX_Document_Folder_Management.getMatchingDocs('', '', '', query, '', '20', '1', new String[] {});

                // Assert: result matches expectation
                Set<Id> docs = expectation.get(query);
                System.assertEquals(docs.size(), result.documents.size(), 'Document count expectation invalid' + query);
                for(SQX_Document_Folder_Management.Document doc : result.documents) {
                    System.assert(docs.contains(doc.doc.Id), 'Document not found in expectation ' + doc + ' exp: ' + query);
                }
            }
        }
    }

    /**
     * Given Controlled document with different title and number
     * When Matching record is queried
     * Then Result is returned in sorted order based on the sort field and sort order
     * @story SQX-3665
     */
    static testmethod void givenControlledDocuments_WhenMatchingRecordIsRequestedWithSortField_ThenResultIsSorted() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {
            // Arrange: Insert 10 document with different title
            List<SQX_Controlled_Document__c> docs = new List<SQX_Controlled_Document__c>();
            for(Integer i = 0; i < 10; i++) {
                SQX_Controlled_Document__c doc = new SQX_Test_Controlled_Document().doc;
                doc.Title__c = 'Title ' + i;
                doc.Document_Number__c = 'Document-Number-' + (9 - i); //since we will have 9, 8, 7, 6, ...
                docs.add(doc);
            }

            insert docs;

            String query = '{"field" : "compliancequest__Document_Status__c", "operator" : "eq", "s_value" : "Draft"}';

            // Act: Perform the query and get the result
            SQX_Document_Folder_Management.Result result = SQX_Document_Folder_Management.getMatchingDocs('', '', '', '', query, '20', '1', new String[] {}, null, null);

            // Assert: the docs are sorted by title in descending order
            String[] positions = new String[]{
                'Title 0', 'Title 1', 'Title 2', 'Title 3', 'Title 4',
                'Title 5', 'Title 6', 'Title 7', 'Title 8', 'Title 9'
            };
            for(Integer i = 0; i < result.documents.size(); i++) {
                SQX_Controlled_Document__c doc = result.documents[i].doc;
                System.assertEquals(positions[i], doc.Title__c, 'Descending Order mismatch');
            }

            // Act: get result by sorting in ascending order
            result = SQX_Document_Folder_Management.getMatchingDocs('', '', '', '', query, '20', '1', new String[] {}, null, 'asc');

            // Assert: the docs are sorted by title in ascending order
            positions = new String[]{
                'Title 0', 'Title 1', 'Title 2', 'Title 3', 'Title 4',
                'Title 5', 'Title 6', 'Title 7', 'Title 8', 'Title 9'
            };
            for(Integer i = 0; i < result.documents.size(); i++) {
                SQX_Controlled_Document__c doc = result.documents[i].doc;
                System.assertEquals(positions[i], doc.Title__c, 'Ascending Order mismatch');
            }

            // Act: get result by sorting using doc number in ascending order
            result = SQX_Document_Folder_Management.getMatchingDocs('', '', '', '', query, '20', '1', new String[] {}, new List<String> { 'compliancequest__Document_Number__c'}, 'desc');

            // Assert: the docs are sorted by title in ascending order
            positions = new String[]{
                'Title 0', 'Title 1', 'Title 2', 'Title 3', 'Title 4',
                'Title 5', 'Title 6', 'Title 7', 'Title 8', 'Title 9'
            };
            for(Integer i = 0; i < result.documents.size(); i++) {
                SQX_Controlled_Document__c doc = result.documents[i].doc;
                System.assertEquals(positions[i], doc.Title__c, 'Ascending Order mismatch');
            }
        }
    }
    
    /**
    * Given: A new document with content
    * When:  content text is searched in document folder
    * Then:  document with that content is returned
    * 
    * @Author: Sajal Joshi
    * @Date: 21/07/2017
    * @Story: [SQX-3693]
    */ 
    static testmethod void whenContentTextIsSearched_ThenDocumentWithThatContentIsReturned(){
        User u = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runas(u){
            // Arrange: Create a document with content
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            doc1.doc.Document_Number__c = 'Doc-1221';
            doc1.save(true);
            
            SQX_Controlled_Document__c doc = [SELECT Id, Document_Status__c, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: doc1.doc.Id];

            List<ContentVersion> cv = [SELECT ID, Controlled_Document__c, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId =: doc.Content_Reference__c];
            ContentVersion v2 = new ContentVersion(VersionData = Blob.valueOf('New Content Document'), 
                                                       ContentDocumentId = cv[0].ContentDocumentId,
                                                       PathOnClient = 'LargeFile.txt',
                                                       ContentUrl = '');
            
            // Act: Set fixed result, define document filter and look for matching document with content with text 'New Content'
            Test.setFixedSearchResults(new Id[]{doc1.doc.id});
            String documentFilterString = '{"field":"compliancequest__Document_Number__c","operator":"contains","s_value":"1221"}';
            List<SQX_Document_Folder_Management.Document> matchingDocuments = SQX_Document_Folder_Management.getMatchingDocs('', 'New Content', '{}', '{}', documentFilterString,null, null, null).documents;
            
            // Assert: Single Document with that content should be returned
            System.assertEquals(1, matchingDocuments.size());
        }
    }
}