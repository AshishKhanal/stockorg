/**
 * This class act as Item source for Supplier Management related items.
 */
public with sharing class SQX_Homepage_Source_Supplier_Items extends SQX_Homepage_ItemSource {
    
    /**
     * Supplier Items: Draft/Open/Complete items
     */
    
    private final String module = SQX_Homepage_Constants.MODULE_TYPE_SUPPLIER_MANAGEMENT;

    private User loggedInUser;
    
    /**
    * Constructor method
    */
    public SQX_Homepage_Source_Supplier_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    }
    
    /**
    * Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_New_Supplier_Introduction__c.SObjectType => new List<SObjectField> 
            {
                SQX_New_Supplier_Introduction__c.Name,
                SQX_New_Supplier_Introduction__c.CreatedDate,
                SQX_New_Supplier_Introduction__c.Status__c,
                SQX_New_Supplier_Introduction__c.OwnerId,
                SQX_New_Supplier_Introduction__c.Company_Name__c
            },
            SQX_Supplier_Deviation__c.SObjectType => new List<SObjectField>
            {
                SQX_Supplier_Deviation__c.Name,
                SQX_Supplier_Deviation__c.CreatedDate,
                SQX_Supplier_Deviation__c.Status__c,
                SQX_Supplier_Deviation__c.OwnerId
            },
            SQX_Supplier_Interaction__c.SObjectType => new List<SObjectField>
            {
                SQX_Supplier_Interaction__c.Name,
                SQX_Supplier_Interaction__c.CreatedDate,
                SQX_Supplier_Interaction__c.Status__c,
                SQX_Supplier_Interaction__c.Record_Stage__c,
                SQX_Supplier_Interaction__c.OwnerId
            },
            SQX_Supplier_Escalation__c.SObjectType => new List<SObjectField>
            {
                SQX_Supplier_Escalation__c.Name,
                SQX_Supplier_Escalation__c.CreatedDate,
                SQX_Supplier_Escalation__c.Status__c,
                SQX_Supplier_Escalation__c.Record_Stage__c,
                SQX_Supplier_Escalation__c.OwnerId
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }
    
    /**
    * returns a list of homepage items for the current user related to Supplier Management module
    */
    protected override List <SObject> getUserRecords() {

        List<SObject> supplierMgmtRecords = new List<SObject>();

        List <String> nsiStatuses = new list <String>
        {
            SQX_NSI.STATUS_DRAFT,
            SQX_NSI.STATUS_COMPLETE
        };

        List <SQX_New_Supplier_Introduction__c> nsiItems = [SELECT  Id,
                                                        Name,
                                                        Status__c,
                                                        Record_Stage__c,
                                                        CreatedDate,
                                                        Company_Name__c
                                                        FROM SQX_New_Supplier_Introduction__c
                                                        WHERE
                                                        (Status__c IN: nsiStatuses OR (Status__c = :SQX_NSI.STATUS_OPEN AND Record_Stage__c = :SQX_NSI.STAGE_ONHOLD))
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        List <String> supplierDeviationStatuses = new List<String>{
            SQX_Supplier_Deviation.STATUS_DRAFT,
            SQX_Supplier_Deviation.STATUS_COMPLETE
        };
        
        List<SQX_Supplier_Deviation__c> supplierDeviationItems = [SELECT  Id,
                                                        Name,
                                                        Status__c,
                                                        CreatedDate,
                                                        SQX_Account__r.Name,
                                                        Record_Stage__c
                                                        FROM SQX_Supplier_Deviation__c
                                                        WHERE
                                                        (Status__c IN: supplierDeviationStatuses OR (status__c = :SQX_Supplier_Deviation.STATUS_OPEN AND Record_Stage__c = :SQX_Supplier_Deviation.STAGE_ON_HOLD))
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        List <String> supplierInteractionStatuses = new List<String>{
            SQX_Supplier_Common_Values.STATUS_DRAFT,
            SQX_Supplier_Common_Values.STATUS_COMPLETE
        };
        
        List<SQX_Supplier_Interaction__c> supplierInteractionItems = [SELECT  Id,
                                                        Name,
                                                        Status__c,
                                                        CreatedDate,
                                                        SQX_Account__r.Name,
                                                        Record_Stage__c
                                                        FROM SQX_Supplier_Interaction__c
                                                        WHERE
                                                        (Status__c IN: supplierInteractionStatuses OR (status__c = :SQX_Supplier_Common_Values.STATUS_OPEN AND Record_Stage__c = :SQX_Supplier_Common_Values.STAGE_ON_HOLD))
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];
                                                    
        List<String> supplierEscalationStatuses = new List<String>{
            SQX_Supplier_Common_Values.STATUS_DRAFT,
            SQX_Supplier_Common_Values.STATUS_COMPLETE
        };

        List<SQX_Supplier_Escalation__c> supplierEscalationItems = [SELECT Id,
                                                        Name,
                                                        Status__c,
                                                        CreatedDate,
                                                        SQX_Account__r.Name,
                                                        Record_Stage__c
                                                        FROM SQX_Supplier_Escalation__c
                                                        WHERE
                                                        (Status__c IN: supplierEscalationStatuses OR (Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN AND Record_Stage__c = :SQX_Supplier_Common_Values.STAGE_ON_HOLD))
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        supplierMgmtRecords.addAll(nsiItems);
        supplierMgmtRecords.addAll(supplierDeviationItems);
        supplierMgmtRecords.addAll(supplierInteractionItems);
        supplierMgmtRecords.addAll(supplierEscalationItems);

        return supplierMgmtRecords;
    }
    
    /**
    * Method returns a SQX_Homepage_Item type item from the given sobject record
    * @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        if(item.getSObjectType() == SQX_Supplier_Deviation__c.SObjectType){
           return getSupplierDeviationItem((SQX_Supplier_Deviation__c) item);
        } else if(item.getSObjectType() == SQX_Supplier_Interaction__c.SObjectType){
            return getSupplierInteractionItem((SQX_Supplier_Interaction__c) item);
        } else if(item.getSObjectType() == SQX_Supplier_Escalation__c.SObjectType) {
            return getSupplierEscalationItem((SQX_Supplier_Escalation__c) item);
        } else {
           return getNsiItem((SQX_New_Supplier_Introduction__c) item); 
        }

    }
    
    /**
    * Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
    * @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getNsiItem(SQX_New_Supplier_Introduction__c item) {

        SQX_Homepage_Item nsiItem = new SQX_Homepage_Item();

        nsiItem.itemId = item.Id;

        nsiItem.createdDate = Date.valueOf(item.CreatedDate);

        nsiItem.moduleType = this.module;

        nsiItem.creator = loggedInUser;

        if (item.Status__c == SQX_NSI.STATUS_OPEN) {

            nsiItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            nsiItem.actions = getActionsForOpenSupplierItem(item.Id);
            nsiItem.feedText = getOpenSupplierIntroductionItemFeedText(item);

        } else if (item.Status__c == SQX_NSI.STATUS_DRAFT && item.Record_Stage__c == SQX_NSI.STAGE_DRAFT) {

            nsiItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            nsiItem.actions = getDraftSupplierItemActions(item.Id);
            nsiItem.feedText = getDraftSupplierIntroductionItemFeedText(item);

        } else if (item.Status__c == SQX_NSI.STATUS_DRAFT && item.Record_Stage__c == SQX_NSI.STAGE_TRIAGE) {

            nsiItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            nsiItem.actions = getTriageSupplierItemActions(item.Id);
            nsiItem.feedText = getTriageSupplierIntroductionItemFeedText(item);

        } else if (item.Status__c == SQX_NSI.STATUS_COMPLETE) {

            nsiItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            nsiItem.actions = getCompleteSupplierItemActions(item.Id);
            nsiItem.feedText = getCompleteSupplierIntroductionItemFeedText(item);

        }

        return nsiItem;
    }

    /**
    * Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
    * @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getSupplierDeviationItem(SQX_Supplier_Deviation__c item) {

        SQX_Homepage_Item supplierDeviationItem = new SQX_Homepage_Item();

        supplierDeviationItem.itemId = item.Id;

        supplierDeviationItem.createdDate = Date.valueOf(item.CreatedDate);

        supplierDeviationItem.moduleType = this.module;

        supplierDeviationItem.creator = loggedInUser;

        if (item.Status__c == SQX_Supplier_Deviation.STATUS_DRAFT && item.Record_Stage__c == SQX_Supplier_Deviation.STAGE_DRAFT) {
            
            supplierDeviationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            supplierDeviationItem.actions = getDraftSupplierItemActions(item.Id);
            supplierDeviationItem.feedText = getDraftSupplierDeviationItemFeedText(item);
            
        }else if (item.Status__c == SQX_Supplier_Deviation.STATUS_DRAFT && item.Record_Stage__c ==  SQX_Supplier_Deviation.STAGE_TRIAGE) {

            supplierDeviationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            supplierDeviationItem.actions = getTriageSupplierItemActions(item.Id);
            supplierDeviationItem.feedText = getTriageSupplierDeviationItemFeedText(item);

        }else if(item.Status__c == SQX_Supplier_Deviation.STATUS_OPEN){

                supplierDeviationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
                supplierDeviationItem.actions = getActionsForOpenSupplierItem(item.Id);
                supplierDeviationItem.feedText = getOpenSupplierDeviationItemFeedText(item);

        } else if (item.Status__c == SQX_Supplier_Deviation.STATUS_COMPLETE) {

            supplierDeviationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            supplierDeviationItem.actions = getCompleteSupplierItemActions(item.Id);
            supplierDeviationItem.feedText = getCompleteSupplierDeviationItemFeedText(item);

        }

        return supplierDeviationItem;
    }

    /**
    * Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
    * @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getSupplierInteractionItem(SQX_Supplier_Interaction__c item) {

        SQX_Homepage_Item supplierInteractionItem = new SQX_Homepage_Item();

        supplierInteractionItem.itemId = item.Id;

        supplierInteractionItem.createdDate = Date.valueOf(item.CreatedDate);

        supplierInteractionItem.moduleType = this.module;

        supplierInteractionItem.creator = loggedInUser;

        if (item.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT && item.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_DRAFT) {
            
            supplierInteractionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            supplierInteractionItem.actions = getDraftSupplierItemActions(item.Id);
            supplierInteractionItem.feedText = getDraftSupplierInteractionItemFeedText(item);
            
        }else if (item.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT && item.Record_Stage__c ==  SQX_Supplier_Common_Values.STAGE_TRIAGE) {

            supplierInteractionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            supplierInteractionItem.actions = getTriageSupplierItemActions(item.Id);
            supplierInteractionItem.feedText = getTriageSupplierInteractionItemFeedText(item);

        }else if(item.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN){

                supplierInteractionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
                supplierInteractionItem.actions = getActionsForOpenSupplierItem(item.Id);
                supplierInteractionItem.feedText = getOpenSupplierInteractionItemFeedText(item);

        } else if (item.Status__c == SQX_Supplier_Common_Values.STATUS_COMPLETE) {

            supplierInteractionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            supplierInteractionItem.actions = getCompleteSupplierItemActions(item.Id);
            supplierInteractionItem.feedText = getCompleteSupplierInteractionItemFeedText(item);

        }

        return supplierInteractionItem;
    }

    /**
    * Method return a SQX_Homepage_Item with correct values for item of different statuses from the give sobject record
    * @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getSupplierEscalationItem(SQX_Supplier_Escalation__c item) {

        SQX_Homepage_Item supplierEscalationItem = new SQX_Homepage_Item();

        supplierEscalationItem.itemId = item.Id;

        supplierEscalationItem.createdDate = Date.valueOf(item.CreatedDate);

        supplierEscalationItem.moduleType = this.module;

        supplierEscalationItem.creator = loggedInUser;

        if (item.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT && item.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_DRAFT) {
            
            supplierEscalationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            supplierEscalationItem.actions = getDraftSupplierItemActions(item.Id);
            supplierEscalationItem.feedText = getDraftSupplierEscalationItemFeedText(item);
            
        }else if (item.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT && item.Record_Stage__c ==  SQX_Supplier_Common_Values.STAGE_TRIAGE) {

            supplierEscalationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            supplierEscalationItem.actions = getTriageSupplierItemActions(item.Id);
            supplierEscalationItem.feedText = getTriageSupplierEscalationItemFeedText(item);

        }else if(item.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN){

                supplierEscalationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
                supplierEscalationItem.actions = getActionsForOpenSupplierItem(item.Id);
                supplierEscalationItem.feedText = getOpenSupplierEscalationItemFeedText(item);

        } else if (item.Status__c == SQX_Supplier_Common_Values.STATUS_COMPLETE) {

            supplierEscalationItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            supplierEscalationItem.actions = getCompleteSupplierItemActions(item.Id);
            supplierEscalationItem.feedText = getCompleteSupplierEscalationItemFeedText(item);

        }

        return supplierEscalationItem;
    }

    /**
    * Returns the respond action type for Open Supplier Item
    * @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getActionsForOpenSupplierItem(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);

        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    * Returns the view action type for Draft Supplier Item
    * @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getDraftSupplierItemActions(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);

        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

/**
    * Returns the view action type for triage Supplier Item
    * @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getTriageSupplierItemActions(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_INITIATE;

        PageReference pr = new PageReference('/' + itemId);

        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_INITIATE;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    * Returns the action for completed Supplier Item
    * @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getCompleteSupplierItemActions(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_CLOSE;

        PageReference pr = new PageReference('/' + itemId);

        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_CLOSE;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    * Method returns the open Supplier Introduction Item type feed text for the given item
    * @param item - Supplier Introduction Item
    */
    private String getOpenSupplierIntroductionItemFeedText(SQX_New_Supplier_Introduction__c item) {
        return getSupplierIntroductionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);
    }

    /**
    * Method returns the draft Supplier Introduction Item type feed text for the given item
    * @param item - Supplier Item
    */
    private String getDraftSupplierIntroductionItemFeedText(SQX_New_Supplier_Introduction__c item) {
        return getSupplierIntroductionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);
    }
    
    /**
    * Method returns the triage Supplier Introduction Item type feed text for the given item
    * @param item - Supplier Item
    */
    private String getTriageSupplierIntroductionItemFeedText(SQX_New_Supplier_Introduction__c item) {
        return getSupplierIntroductionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, item);
    }

    /**
    * Method returns the Complete Supplier Introduction Item type feed text for the given item
    * @param item - Supplier Item
    */
    private String getCompleteSupplierIntroductionItemFeedText(SQX_New_Supplier_Introduction__c item) {
        return getSupplierIntroductionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);
    }

    /**
    * Method replaces the given Supplier Introduction Item feed template with Supplier Item record's info and returns the string
    */
    private String getSupplierIntroductionItemFeedText(String template, SQX_New_Supplier_Introduction__c item)
    {
        return String.format(template, new String[] { item.Name, item.Company_Name__c});
    }
    
    
    /**
    * Method returns the open Supplier Deviation Item type feed text for the given item
    * @param item - Supplier Deviation Item
    */
    private String getOpenSupplierDeviationItemFeedText(SQX_Supplier_Deviation__c item) {
        return getSupplierDeviationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);
    }

    /**
    * Method returns the triage Supplier Deviation Item type feed text for the given item
    * @param item - Supplier Deviation Item
    */
    private String getTriageSupplierDeviationItemFeedText(SQX_Supplier_Deviation__c item) {
        return getSupplierDeviationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, item);
    }

    /**
    * Method returns the draft Supplier Deviation Item type feed text for the given item
    * @param item - Supplier Deviation Item
    */
    private String getDraftSupplierDeviationItemFeedText(SQX_Supplier_Deviation__c item) {
        return getSupplierDeviationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);
    }
    
    /**
    * Method returns the Complete Supplier Deviation Item type feed text for the given item
    * @param item - Supplier Deviation Item
    */
    private String getCompleteSupplierDeviationItemFeedText(SQX_Supplier_Deviation__c item) {
        return getSupplierDeviationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);
    }

    /**
    * Method replaces the given Supplier Deviation Item feed template with Supplier Deviation Item record's info and returns the string
    */
    private String getSupplierDeviationItemFeedText(String template, SQX_Supplier_Deviation__c item)
    {
        return String.format(template, new String[] { item.Name, item.SQX_Account__r.Name});
    }

    /**
    * Method returns the open Supplier Interaction Item type feed text for the given item
    * @param item - Supplier Interaction Item
    */
    private String getOpenSupplierInteractionItemFeedText(SQX_Supplier_Interaction__c item) {
        return getSupplierInteractionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);
    }

    /**
    * Method returns the draft Supplier Interaction Item type feed text for the given item
    * @param item - Supplier Interaction Item
    */
    private String getDraftSupplierInteractionItemFeedText(SQX_Supplier_Interaction__c item) {
        return getSupplierInteractionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);
    }
    
    /**
    * Method return the draft and triage Supplier Interaction Item type feed text for the given item
    * @param item -  Supplier Interaction Item
    */
    private String getTriageSupplierInteractionItemFeedText(SQX_Supplier_Interaction__c item) {
        return getSupplierInteractionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, item);
    }
    
    /**
    * Method returns the Complete Supplier Interaction Item type feed text for the given item
    * @param item - Supplier Interaction Item
    */
    private String getCompleteSupplierInteractionItemFeedText(SQX_Supplier_Interaction__c item) {
        return getSupplierInteractionItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);
    }

    /**
    * Method replaces the given Supplier Interaction Item feed template with Supplier Interaction Item record's info and returns the string
    */
    private String getSupplierInteractionItemFeedText(String template, SQX_Supplier_Interaction__c item)
    {
        return String.format(template, new String[] { item.Name, item.SQX_Account__r.Name});
    }

    /**
    * Method returns the draft Supplier Escalation Item type feed text for the given item
    * @param item - Supplier Escalation Item
    */
    private String getDraftSupplierEscalationItemFeedText(SQX_Supplier_Escalation__c item) {
        return getSupplierEscalationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);
    }

    /**
    * Method return the draft and triage Supplier Escalation Item type feed text for the given item
    * @param item -  Supplier Escalation Item
    */
    private String getTriageSupplierEscalationItemFeedText(SQX_Supplier_Escalation__c item) {
        return getSupplierEscalationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, item);
    }

    /**
    * Method returns the open Supplier Escalation Item type feed text for the given item
    * @param item - Supplier Escalation Item
    */
    private String getOpenSupplierEscalationItemFeedText(SQX_Supplier_Escalation__c item) {
        return getSupplierEscalationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);
    }
    
    /**
    * Method returns the Complete Supplier Escalation Item type feed text for the given item
    * @param item - Supplier Escalation Item
    */
    private String getCompleteSupplierEscalationItemFeedText(SQX_Supplier_Escalation__c item) {
        return getSupplierEscalationItemFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);
    }

    /**
    * Method replaces the given Supplier Escalation Item feed template with Supplier Deviation Item record's info and returns the string
    */
    private String getSupplierEscalationItemFeedText(String template, SQX_Supplier_Escalation__c item)
    {
        return String.format(template, new String[] { item.Name, item.SQX_Account__r.Name});
    }


}