/**
*    Interface for synchronous rendition
*/
global interface SQX_Sync_RenditionProvider{

    /**
    * Submits a request if the request is a new one (i.e without a job id) else poll the request (i.e if the request has job id) synchronously
    * @param request contains the rendition request for which the rendition is to be created or polled
    */
    SQX_RenditionResponse processRequestSync(SQX_RenditionRequest request);        
}