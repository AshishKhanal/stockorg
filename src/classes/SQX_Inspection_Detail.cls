/**
 * class to store the methods performed by the inspection detail and static variables related to inspection detail
 */
public with sharing class SQX_Inspection_Detail {
    public static final String INSPECTION_RESULT_FAIL = 'Fail',
                                INSPECTION_RESULT_PASS = 'Pass',
                                INSPECTION_RESULT_SKIP = 'Skip';

    public static final String INSPECTION_DETAIL_IMPACT_HIGH = 'High',
                                INSPECTION_DETAIL_IMPACT_MEDIUM = 'Medium',
                                INSPECTION_DETAIL_IMPACT_LOW = 'Low';
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newInspectionDetails equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Inspection_Detail__c> newInspectionDetails, Map<Id,SQX_Inspection_Detail__c> oldMap){
            
            super(newInspectionDetails, oldMap);
        }
        
        /**
        * method to calculate the result based on the given USL and LSL and the observed value
        */
        public Bulkified calculateTolerance(){
            Rule toleranceRule = new Rule();
            toleranceRule.addRule(SQX_Inspection_Detail__c.Observed_Value__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .applyFilter(toleranceRule, RuleCheckMethod.OnCreateAndEveryEdit);
            
            List<SQX_Inspection_Detail__c> inspectionDetailList = (List<SQX_Inspection_Detail__c>) toleranceRule.evaluationResult;
            if(inspectionDetailList.size() > 0){
                
                for(SQX_Inspection_Detail__c inspectionDetail : inspectionDetailList) {
                    if(inspectionDetail.Upper_Spec_Limit__c != null ){
                        if( inspectionDetail.Observed_Value__c > inspectionDetail.Upper_Spec_Limit__c) {
                            inspectionDetail.Result__c = INSPECTION_RESULT_FAIL;
                            continue;
                        }else{
                            inspectionDetail.Result__c = INSPECTION_RESULT_PASS;
                        }
                    }
                    if(inspectionDetail.Lower_Spec_Limit__c != null){
                        if(inspectionDetail.Observed_Value__c < inspectionDetail.Lower_Spec_Limit__c){
                            inspectionDetail.Result__c = INSPECTION_RESULT_FAIL;
                            continue;
                        }else{
                            inspectionDetail.Result__c = INSPECTION_RESULT_PASS;
                        }
                    }
                }
            }
            return this;
        }

        /**
         * method to prevent modification of inspection detail after inspection closure or void
         */
        public Bulkified preventModificationOfInspectionDetailAfterInspectionCompletion(){

            this.resetView();

            if(this.view.size() > 0){
                // get Specification ids
                Set<Id> ids = this.getIdsForField(SQX_Inspection_Detail__c.SQX_Inspection__c);
                
                Map<Id, SQX_Inspection__c> inspectionMap = new Map<Id, SQX_Inspection__c>([SELECT Id, Status__c, Approval_Status__c FROM SQX_Inspection__c WHERE Id IN: ids]);
                for(SQX_Inspection_Detail__c inspectionDetail : (List<SQX_Inspection_Detail__c>) this.view){
                    SQX_Inspection__c inspection = inspectionMap.get(inspectionDetail.SQX_Inspection__c);
                    if(inspection.Status__c == SQX_Inspection.INSPECTION_STATUS_CLOSED
                        || inspection.Status__c == SQX_Inspection.INSPECTION_STATUS_VOID){

                        inspectionDetail.addError(Label.SQX_ERR_MSG_INSPECTION_DETAIL_CANNOT_BE_MODIFIED);
                    }
                }
            }
            return this;
        }

        /**
         * method to calculate equipment calibration when the equipment is provided in the inspection detail
         */
        public Bulkified checkEquipmentCalibration(){

            Rule inspectionDetailEquipmentChangeRule = new Rule();
            inspectionDetailEquipmentChangeRule.addRule(Schema.SQX_Inspection_Detail__c.SQX_Equipment__c, RuleOperator.HasChanged);
            inspectionDetailEquipmentChangeRule.addRule(Schema.SQX_Inspection_Detail__c.SQX_Equipment__c, RuleOperator.NotEquals, null);

            this.resetView()
                .applyFilter(inspectionDetailEquipmentChangeRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Inspection_Detail__c> inspectionDetailList = (List<SQX_Inspection_Detail__c>)  inspectionDetailEquipmentChangeRule.evaluationResult;

            if(inspectionDetailList.size() > 0){

                Set<Id> relatedEquipmentIds = this.getIdsForField(inspectionDetailList, SQX_Inspection_Detail__c.SQX_Equipment__c);
                Map<Id, SQX_Equipment__c> equipmentMap = new Map<Id, SQX_Equipment__c>([SELECT Id, 
                                                                                                Last_Calibration_Date__c, 
                                                                                                Next_Calibration_Date__c 
                                                                                        FROM SQX_Equipment__c
                                                                                        WHERE Id =: relatedEquipmentIds]);

                for(SQX_Inspection_Detail__c inspectionDetail : inspectionDetailList){

                    SQX_Equipment__c equipment = equipmentMap.get(inspectionDetail.SQX_Equipment__c);
                    inspectionDetail.Equipment_Calibrated__c = equipment.Last_Calibration_Date__c <= Date.Today() && equipment.Next_Calibration_Date__c > Date.Today();
                }
            }


            return this;
        }

        /**
         * method to prevent deletion of inspection detail copied from inspection criteria
         */
        public Bulkified preventDeletionOfCopiedInspectionDetail(){

            this.resetView();

            if(this.view.size() > 0){
                for(SQX_Inspection_Detail__c inspectionDetail : (List<SQX_Inspection_Detail__c>) this.view){
                    if(inspectionDetail.SQX_Specification__c != null){
                        inspectionDetail.addError(Label.SQX_ERR_MSG_INSPECTION_DETAIL_CANNOT_BE_DELETED);
                    }
                }
            }

            return this;
        }
    }
}