/**
 * controller class for handling esig validation and esig policies
 */
global abstract class SQX_Controller_Sign_Off extends SQX_Esig_Policy_Provider {

    /**
     * flag to check if esig is enabled in the org
     */
    public Boolean ES_Enabled           { get; set; }

    /**
     * flag to check is username to be asked or not in esig
     */
    public Boolean ES_Ask_Username      { get; set; }

    /**
     * username of the user signing off the action
     */
    public String SigningOffUsername    { get; set; }

    /**
     * password of the user signing off the action
     */
    public String SigningOffPassword    { get; set; }

    /**
     * comment of the user signing off the action
     */
    public String RecordActivityComment { get; set; }

    /**
     * mainrecord id
     */
    public Id recordId                  { get; set; }

    /**
     * action for which esig is to be performed
     */
    public String actionName            { get; set; }

    /**
     * stores the purpose of the action
     */
    global String purposeOfSignature    { get; set; }

    /**
    * Instance of the main controller for the extension.
    */
    protected ApexPages.StandardController mainController;

    Map<String, Boolean> passMap = new Map<String, Boolean>();
    Map<String, Boolean> commentMap = new Map<String, Boolean>();

    Boolean hasPoliciesRead = false;
    
    /**
     * constructor method for the class
     * @param controller controller of the child class
     */
    public SQX_Controller_Sign_Off(ApexPages.StandardController controller){
        this(controller, null);
    }

    /**
     * constructor method of the class with standard controller and fields to get while calling extension
     * @param controller controller of the child class
     * @Param fieldsToLoad fields to load of the object when calling extension method
     */
    global SQX_Controller_Sign_Off(ApexPages.StandardController controller, List<SObjectField> fieldsToLoad) {

        super();

        mainController = controller;

        try{
            //inside try catch because test cases pass main record where fields can not be added.
            if(fieldsToLoad != null && fieldsToLoad.size() > 0){
                String [] sobjectFields = new List<String>();
                for(SObjectField field : fieldsToLoad){
                    sobjectFields.add(String.valueOf(field));
                }
                mainController.addFields(sobjectFields);
            }

        }
        catch(SObjectException ex){
        }

        getESSettings();
    }

    /**
     * method to get es settings of the org
     */
    private void getESSettings(){

        // get electronic signature settings
        SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getInstance();
        ES_Enabled = cqES.Enabled__c;
        ES_Ask_Username = false;
        if (ES_Enabled) {
            ES_Ask_Username = cqES.Ask_Username__c;
            if (ES_Ask_Username == false) {
                // set username of the current user when not asked 
                SigningOffUsername = UserInfo.getUserName();
            }
        }
    }

    /**
     * method to read the esig policy of the object
     */
    private void readPolicy() {

        this.addEsigPolicies(recordId.getSObjectType().getDescribe().getName());
        
        for(String act : getEsigPolicy().keySet()){
            SQX_CQ_Esig_Policies__c policy = getEsigPolicy().get(act);
            passMap.put(act, policy.Required__c);
            commentMap.put(act, policy.Comment__c);
        }

        hasPoliciesRead = true;
    }

    /**
    * get current user details for esig record activity
    * @return user details with username user and profile details
    */
    public String getUserDetails() {
        return SQX_Utilities.getUserDetailsForRecordActivity();
    }
    
    /**
     * method to check if comment is required
     * @return boolean value indicating comment is required or not
     */
    public Boolean getRequireComment(){
        if (hasPoliciesRead == false)
            readPolicy();
        
        if(commentMap.containsKey(actionName)){
            return commentMap.get(actionName);
        }
        return true;
    }
    
    /**
     * method to check if password is required
     * @return boolean value indicating password is required or not
     */
    public Boolean getPasswordRequired(){
        if (hasPoliciesRead == false)
            readPolicy();
        
        Boolean passwordRequired = false;
        if(ES_Enabled){
            if(passMap.containsKey(actionName)){
                passwordRequired = passMap.get(actionName);
            } else {
                passwordRequired = true;
            }
        }
        return passwordRequired;
    }

    /**
     * method to check if username is required
     * @return boolean value indicating password is required or not
     */
    public Boolean getUserNameRequired(){

        Boolean passwordReqd = getPasswordRequired();
        return (ES_Enabled && ES_Ask_Username && passwordReqd );
        
    }
    
    /**
     * method to validate credentials
     * @return boolean value indicating esig validation
     */
    protected Boolean hasValidCredentials(){
        Boolean esigValidation = true; 
        if (getPasswordRequired()) {
            // perform callout to validate username and password only when password is required and use policies if available
            esigValidation = new SQX_OauthEsignatureValidation().validateCredentials(SigningOffUsername, SigningOffPassword);
        }
        return esigValidation;
    }
    
    /**
    *
    * method to get the instance of the SQX_Controller_Sign_Off
    * @return instance of the SQX_Controller_Sign_Off
    */
    global SQX_Controller_Sign_Off getSelf(){
        return this;
    }
    
}