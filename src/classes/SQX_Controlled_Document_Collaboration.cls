/**
*   @author : Piyush Subedi
*   @description : helper method for controlled document collaboration
*/
public with sharing class SQX_Controlled_Document_Collaboration{    

    final static String ORIGIN_CHATTER_FILE = 'H';

    /**
    *   This method is used to checkout a controlled document
    *   @param doc - controlled document to be checked out
    */
    public static void checkOut(SQX_Controlled_Document__c doc){
        updateControlledDocCheckoutInfo(doc, false, true);
    }

    /**
    *   This method used to checkin a controlled document with the given content
    *   @param ContentVersion - content to be checked in
    *   @param doc - controlled document to be checked in
    */
    public static void checkIn(ContentVersion cvs, SQX_Controlled_Document__c doc){

        if( isUserCheckOutUser(doc) ){
            
            // removing lock from document
            undoCheckOut(doc, false);

            if(cvs != null){
                //  update the primary content in the record with the latest version of the file in the group
                ContentVersion cv = [SELECT Id, Title, PathOnClient, VersionData, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId = :cvs.ContentDocumentId AND IsLatest = true];                
                ContentVersion newCV = new ContentVersion();
                newCV.Title = cv.Title;
                newCV.PathOnClient = cv.PathOnClient;
                newCV.VersionData = cv.VersionData;
                newCV.Origin = ORIGIN_CHATTER_FILE; 
                newCV.ContentDocumentId = doc.Content_Reference__c;

                new SQX_DB().op_insert(new List<ContentVersion> { newCV }, new List<SObjectField>() ); 
            }
        }else{
            throw new SQX_ApplicationGenericException(Label.CQ_UI_Only_User_Who_Checked_Out_Can_Check_In);
        }
    }  

    /**
    *   This method used to revert a controlled document to pre-checkout state
    *   @param doc - controlled document to be reverted from check-out state
    *   @param checkPrivilege - boolean value - false/ : when called by close collaboration operation, else true
    */
    public static void undoCheckOut(SQX_Controlled_Document__c doc, Boolean checkPrivilege){
        /*  if privilege is to be checked , check if user can undo checkout
            if not, throw an error

            privilege verification is ignored when undocheckout is being called after a collaboration group is closed
            and, since privilege verification skipped, no sharing rules should be applied when updating the doc
        */
        Boolean escalatePrivilege = true;
        if(checkPrivilege){
            if (isUserCheckOutUser(doc)){
                escalatePrivilege =  false;
            }else if(!userHasSupervisoryPermission() ){
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_INSUFFICIENT_ACCESS_ON_DOC);
            }
        }
        updateControlledDocCheckoutInfo(doc, escalatePrivilege, false);
    }

    /**
    *   Shorthand method for undo checking out document while verifying if the user has necessary permission
    *   @param doc - controlled document to be reverted from check-out state
    */ 
    public static void undoCheckOut(SQX_Controlled_Document__c doc){
        undoCheckOut(doc, true);
    }

    /**
    *   This method is used to check out controlled document and add collaboration details to document
    *   @param doc - controlled document to be checked out for collaboration
    *   @param grpId - id of the collaboration group 
    *   @param contentId - id of the content stored in the collaboratin group
    */
    public static void checkOutForCollaboration(SQX_Controlled_Document__c doc, String grpId, String contentId){
        updateControlledDocCheckoutInfo(doc, false, true, grpId, contentId);
    }

    /**
    *   Create a copy of original controlled document content
    *   @param docId - controlled documenet id whose content is to be cloned
    *   @return content document id of the cloned content version
    */
    public static String cloneControlledDocumentContent(String docId){
        ContentVersion cv=new ContentVersion();

        String primaryContentId = [SELECT Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id=:docId].Content_Reference__c;
        ContentVersion primaryContent = [SELECT VersionData, PathOnClient FROM ContentVersion WHERE ContentDocumentId = :primaryContentId AND IsLatest = true];

        cv.VersionData=primaryContent.VersionData;
        cv.PathOnClient=primaryContent.PathOnClient;
        cv.RecordTypeId=SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CQCollaboration);
        cv.Controlled_Document__c = docId;

        new SQX_DB().op_insert(new List<ContentVersion>{cv}, new List<Schema.SObjectField>{
                ContentVersion.VersionData,
                ContentVersion.PathOnClient,
                ContentVersion.Controlled_Document__c
        });

        return [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;
    }

    /**
    * This method used to update the controlled document when checkout, checkin and undo checkout event fires
    * @param doc - document to be updated
    * @param escalatePrivilege - true, when sharing rules are to be bypassed
    * @param checkout - is used to identify if the update is for a checkout operation or a checkin/undocheckout operation
    */
    public static void updateControlledDocCheckoutInfo(SQX_Controlled_Document__c doc, Boolean escalatePrivilege, Boolean checkout ){
        updateControlledDocCheckoutInfo(doc, escalatePrivilege, checkout,'', '');
    }

    /**
    * This method used to update the controlled document when checkout, checkin and undo checkout event fires
    * @param doc - document to be updated
    * @param escalatePrivilege - true, when sharing rules are to be bypassed
    * @param checkout - is used to identify if the update is for a checkout operation or a checkin/undocheckout operation
    * @param groupId - id of the collaboration group
    * @param groupContentId - id of the content stored in collaboration group
    */ 
    public static void updateControlledDocCheckoutInfo(SQX_Controlled_Document__c doc, Boolean escalatePrivilege, Boolean checkout, String groupId, String groupContentId){

        String checkedOutBy;
        DateTime checkedOutOn;
        SQX_Controlled_Document__c docToUpdate = new SQX_Controlled_Document__c(Id = doc.Id);

        if( checkout ){
            checkedOutBy = UserInfo.getUserId();
            checkedOutOn = System.Datetime.now();
        }

        docToUpdate.Checked_Out__c = checkout;
        docToUpdate.SQX_Checked_Out_By__c = checkedOutBy;
        docToUpdate.Checked_Out_On__c=checkedOutOn;
        docToUpdate.Collaboration_Group_Id__c = groupId;
        docToUpdate.Collaboration_Group_Content_Id__c = groupContentId;

        SQX_DB db = new SQX_DB();
        if(escalatePrivilege){
            // bypassing sharing rules as there can be a case when a user without permissions in the doc decided to close collaboration
            db = db.withoutSharing();
        }

        db.op_update(new List<SQX_Controlled_Document__c> { docToUpdate }, new List<SObjectField> {
            SQX_Controlled_Document__c.Checked_Out__c,
            SQX_Controlled_Document__c.SQX_Checked_Out_By__c,
            SQX_Controlled_Document__c.Checked_Out_On__c,
            SQX_Controlled_Document__c.Collaboration_Group_Id__c,
            SQX_Controlled_Document__c.Collaboration_Group_Content_Id__c
        });
    }


    /*
    *   Returns true if the current user has document supervisory permission
    */ 
    public static Boolean userHasSupervisoryPermission(){
        return SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR);
    }


    /*
    *   Returns true if the current user is the one who checked out the document
    */ 
    public static Boolean isUserCheckOutUser(SQX_Controlled_Document__c doc){
        return ( doc.SQX_Checked_Out_By__c == UserInfo.getUserId() );
    }
    
    /*
    *   Returns true if the controlled document already checked out
    *   @param docId is a controlled document id
    */ 
    public static Boolean isControlledDocCheckout(Id docId){
        return [SELECT Checked_Out__c FROM SQX_Controlled_Document__c WHERE Id=:docId ].Checked_Out__c;
    }
}