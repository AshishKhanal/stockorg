@isTest
public class SQX_Test_2170_Document_Approval_Step{
    
    static Boolean runAllTests = true,
                   run_givenADoc_ItIsRedirectedToNewPageWhenDocApprovalStepIsCreated = false,
                   run_givenADocApprovalStep_ItIsDeletedWhenDeleteLinkIsClicked = false,
                   run_givenADocApprovalStep_NewRecordIsCreatedWhenItIsSaved = false,
                   run_givenADocApprovalStep_ItIsRedirectedToMainRecordWhenItIsCancelled = false,
                   run_givenADocApprovalStep_ItIsRedirectedToNewCreatePageWhenSaveAndNewIsClicked = false;
                   
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * ACTION: Create a controlled doc and add a doc approval step
     * EXPECTED: Redirected to new page when new doc approval is created
     * @date: 2016-04-26
     * @story: [SQX-2170]
    */
    public static testmethod void givenADoc_ItIsRedirectedToNewPageWhenDocApprovalStepIsCreated() {
        if (!runAllTests && !run_givenADoc_ItIsRedirectedToNewPageWhenDocApprovalStepIsCreated) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            // Arrange : Create a cpntrolled doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document().save();

            ApexPages.StandardController stdController = new ApexPages.StandardController(doc.doc);
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(stdController);

            // Act: Click the edit button
            PageReference ref = Page.SQX_Controlled_Document_Approval;
            ref.getParameters().put('id', doc.doc.Id);
            Test.setCurrentPage(ref);

            PageReference pr = ext.addNewDocumentApprovalStep();     

            // Assert: expected to redirect 
            System.assert(pr != null,
                'Expected to be redirected to create page');

            System.assert(pr.getURL().contains(Page.SQX_CDoc_AppStep.getURL()),
                'Expected SQX_CDoc_AppStep VF page URL to be returned.' + pr.getURL());

            System.assertEquals( doc.doc.Id , pr.getParameters().get('docId'),
                'Expected id parameter value to be same as doc id.');
        }
    }
    
    /**
     * ACTION: Create a controlled doc and add a doc approval step
     * EXPECTED: Doc appproval step is deleted when delete link is clicked
     * @date: 2016-04-26
     * @story: [SQX-2170]
    */
    public static testmethod void givenADocApprovalStep_ItIsDeletedWhenDeleteLinkIsClicked() {
        if (!runAllTests && !run_givenADocApprovalStep_ItIsDeletedWhenDeleteLinkIsClicked) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            // Create a controlled doc and its step
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document_Approval__c docApproval = new SQX_Controlled_Document_Approval__c(
                                                                    SQX_Controlled_Document__c = doc.doc.Id,
                                                                    Step__c = 1,
                                                                    SQX_User__c = standardUser.Id);

            insert docApproval;
            ApexPages.StandardController stdController = new ApexPages.StandardController(doc.doc);
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(stdController);

            // Act: delete the controlled doc
            PageReference ref = Page.SQX_Controlled_Document_Approval;
            ref.getParameters().put('stepId', docApproval.Id);
            Test.setCurrentPage(ref);
            
            PageReference pr = ext.deleteApprovalStep();     

            System.assert(pr != null, 'Expected to be redirected to detail page');
            System.assert(pr.getParameters().get('retUrl') == doc.doc.Id, 'Expected redirected url from salesforce standard contrller delete method should cotain doc id as retUrl but is '+pr.getParameters().get('retUrl'));
        }
    }
    
    /**
     * ACTION: Create a controlled doc and add a doc approval step
     * EXPECTED: Doc approval step is created when save is clicked
     * @date: 2016-04-26
     * @story: [SQX-2170]
    */
    public static testmethod void givenADocApprovalStep_NewRecordIsCreatedWhenItIsSaved() {
        if (!runAllTests && !run_givenADocApprovalStep_NewRecordIsCreatedWhenItIsSaved) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            // Arrange : Create a controlled doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document_Approval__c docApproval = new SQX_Controlled_Document_Approval__c(
                                                                    Step__c = 1,
                                                                    SQX_User__c = standardUser.Id);
            //Add a document approval step
            Test.setCurrentPage(Page.SQX_CDoc_AppStep);
            ApexPages.currentPage().getParameters().put('docId', doc.doc.Id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(docApproval);
            SQX_Extension_CDOc_AppStep ext = new SQX_Extension_CDOc_AppStep(stdController);

            PageReference pr = ext.saveOnly(); 

            // Assert : Documnet approval step is created
            docApproval =  [SELECT Id, SQX_Controlled_Document__c FROM SQX_Controlled_Document_Approval__c WHERE SQX_Controlled_Document__c =: doc.doc.Id];
            System.assertNotEquals(null, docApproval, 'Expected docapproval not to be null');

            System.assert(pr.getURL().contains(new ApexPages.StandardController(new SQX_Controlled_Document__c(Id = docApproval.SQX_Controlled_Document__c)).view().getURL()),
                'Expected main record VF page URL to be returned.' + pr.getURL());

        }
    }
    
    /**
     * ACTION: Create a controlled doc and add a doc approval step
     * EXPECTED: Redirected to main record when cancel is clicked
     * @date: 2016-04-26
     * @story: [SQX-2170]
    */
    public static testmethod void givenADocApprovalStep_ItIsRedirectedToMainRecordWhenItIsCancelled() {
        if (!runAllTests && !run_givenADocApprovalStep_ItIsRedirectedToMainRecordWhenItIsCancelled) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            // Act: Create a controlled doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document_Approval__c docApproval = new SQX_Controlled_Document_Approval__c(
                                                                    SQX_Controlled_Document__c = doc.doc.Id,
                                                                    Step__c = 1,
                                                                    SQX_User__c = standardUser.Id);

            // Act: Add a document approval
            Test.setCurrentPage(Page.SQX_CDoc_AppStep);
            ApexPages.StandardController stdController = new ApexPages.StandardController(docApproval);
            SQX_Extension_CDOc_AppStep ext = new SQX_Extension_CDOc_AppStep(stdController);

            PageReference pr = ext.returnMainRecord(); 

            System.assert(pr != null,
                'Expected to be redirected to main record page');

            List<SQX_Controlled_Document_Approval__c> docApprovals = [SELECT Id FROM SQX_Controlled_Document_Approval__c WHERE SQX_Controlled_Document__c =: doc.doc.Id];

            // Assert : No record is created
            System.assertEquals(0, docApprovals.size(), 'Expected the doc approval step to be null');

            System.assert(pr.getURL().contains(new ApexPages.StandardController(new SQX_Controlled_Document__c(Id = docApproval.SQX_Controlled_Document__c)).view().getURL()),
                'Expected main reocrd VF page URL to be returned.' + pr.getURL());

        }
    }
    
    /**
     * ACTION: Create a controlled doc and add a doc approval step
     * EXPECTED: Redirected to new page when save and new is clicked
     * @date: 2016-04-26
     * @story: [SQX-2170]
    */
    public static testmethod void givenADocApprovalStep_ItIsRedirectedToNewCreatePageWhenSaveAndNewIsClicked() {
        if (!runAllTests && !run_givenADocApprovalStep_ItIsRedirectedToNewCreatePageWhenSaveAndNewIsClicked) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            // Arrange : create a controlled doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document_Approval__c docApproval = new SQX_Controlled_Document_Approval__c(
                                                                   // SQX_Controlled_Document__c = doc.doc.Id,
                                                                    Step__c = 1,
                                                                    SQX_User__c = standardUser.Id);

            // act : click save and new button
            Test.setCurrentPage(Page.SQX_CDoc_AppStep);
            ApexPages.currentPage().getParameters().put('docId', doc.doc.Id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(docApproval);
            SQX_Extension_CDOc_AppStep ext = new SQX_Extension_CDoc_AppStep(stdController);

            PageReference pr = ext.saveAndNew(); 

            List<SQX_Controlled_Document_Approval__c> docApprovals = [SELECT Id FROM SQX_Controlled_Document_Approval__c WHERE SQX_Controlled_Document__c =: doc.doc.Id];

            // Assert : One document approval is created
            System.assertEquals(1, docApprovals.size(), 'Expected the doc approval step not to be null');

            System.assert(pr.getURL().contains(Page.SQX_CDoc_AppStep.getURL()),
                'Expected SQX_CDoc_AppStep VF page URL to be returned.' + pr.getURL());

            System.assertEquals( doc.doc.Id , pr.getParameters().get('docId'),
                'Expected id parameter value to be same as doc id.');
        }
    }
}