/*
 * test for Audit specify criteria document to use 
 * author: Sudeep Maharjan
 * date: 2016/09/05
 * story: SQX-2505
 **/
@IsTest
public class SQX_Test_2505_SpecifyCriteriaDocument {
    static final String STD_USER_1 = 'standardUser1',
                        ADMIN_USER_1 = 'adminUser1';

    @testSetup
    static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER_1);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STD_USER_1);
        
    }



    SQX_Audit__c audit = null; //the audit that was created
    SQX_Audit_Program__c program = null;



    private static Boolean  runAllTests = true,
                            run_givenAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditScopesAreAddedInAudit = false,
                            run_givenACurrentAuditCriteriaDocumentAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementAreAddedInAudit= false,
                            run_givenACurrentAuditCriteriaDocumentAndAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementsAreAddedInAudit= false,
                            run_givenAuditCriteriaIsBeingReferredByActiveAudit_WhenDocIsPreExpired_NoErrorIsPresented = false;
    /**
     * @description: When the active audit program is included in the audit then
     *               its the criterion document requirement related to audit program must be included in the audit checklist once it is scheduled
     *               and once the audit is scheduled audit program cannot be changed
     * @author: Sudeep Maharjan
     * @date: 2016/09/05
     * @sqx-no: [SQX-2505]
    */                        
    static testmethod void givenAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditScopesAreAddedInAudit(){
        if(!runAllTests && !run_givenAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditScopesAreAddedInAudit){
            return;
        }

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER_1);

        System.runas(users.get(STD_USER_1)){
            /**
             * Arrange:
             * Create two audit program with one program scope that refers to a criteria document.
             * and change the status of both audit program to active approved
             * and add audit with its status set to plan and save the audit
             */
            SQX_Test_Audit_Program program1 = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope1 = program1.createScopeWithNewCriteriaDocument(1, adminUser); //refers to a criteria with single requirement
            
            SQX_Test_Audit_Program program2 = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope2 = program2.createScopeWithNewCriteriaDocument(1, adminUser); //refers to a criteria with single requirement

            program1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program1.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program1.save();

            program2.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program2.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program2.save();

            system.debug(program1);
            system.debug(program2);
            system.debug(scope1);
            system.debug(scope2);
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            //Act 1: Add the audit program in audit and progarm is added to audit
            audit.includeInProgram(false, program1.program).save();

            //Assert 1: Audit program should be added to audit
            SQX_Audit__c auditData = [SELECT Id, SQX_Audit_Program__c, SQX_Audit_Criteria_Document__c  FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            System.assert(auditData.SQX_Audit_Program__c == program1.program.Id,'Expected to have program1 in the audit');
          
            // Act 2: Add the audit program in audit and progarm is added to audit check if the audit checklist is added in audit
            audit.includeInProgram(false, program2.program).save();

            List<SQX_Audit_Checklist__c> auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
         
            // Assert 2: Audit checklist should not be added to audit until it is in planned state
            System.assert(auditChecklistList.size() == 0, 'Expected the number of checklist to be equal to 0 when the audit is in planned state');
            
            // Assert 3: Audit program can be changed until audit is in planned state
            auditData = [SELECT Id, SQX_Audit_Program__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            System.assert(auditData.SQX_Audit_Program__c == program2.program.Id,'Expected to have program2 in the audit');
            audit.save();

            //Act 4: Change the status of audit to scheduled
            audit.setStage(SQX_Audit.STAGE_SCHEDULED).save();

            auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
          
            // Assert 4: Audit checklist should be added to audit and the number of audit checklist should be equal to the criterion document requirement in audit criteria dcument
            System.assert(auditChecklistList.size() == 1, 'Expected the number of checklist to be equal to the number of criterion requirements in audit critera once the audit is scheduled');

            //Act 5: Try to change the audit program of the scheduled audit
            try{
                audit.includeInProgram(false, program1.program).save();
            }
            catch(Exception e){
                //Assert 5: Should throw an expection that the audit program cannot be changed in scheduled audit
                Boolean expectedExceptionThrown =  e.getMessage().contains('Audit Program can only be changed while creating audit or is in Plan.') ? true : false;
                System.assertEquals(expectedExceptionThrown,true);       
            } 

        }
    }

    /**
     * @description: When the  audit criteria document in current state is included in the audit then
     *               its the criterion document requirement related to audit criteria document must be included in the audit checklist once it is scheduled
     *               and once the audit is scheduled audit program cannot be changed
     * @author: Sudeep Maharjan
     * @date: 2016/09/05
     * @sqx-no: [SQX-2505]
    */
    static testmethod void givenACurrentAuditCriteriaDocumentAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementAreAddedInAudit(){
        if(!runAllTests && !run_givenACurrentAuditCriteriaDocumentAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementAreAddedInAudit){
            return;
        }


        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER_1);

        SQX_Result_Type__c resultType;
        
        integer randomNumber = (integer)(Math.random() * 1000000);

        System.runas(adminUser){
            resultType = new SQX_Result_Type__c();
            resultType.Name = 'Yes/No/Skipped' + randomNumber;
            resultType.Description__c = 'Result Type Description' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Result_Type__c> {resultType}, new List<Schema.SObjectField>{
                Schema.SQX_Result_Type__c.Name,
                Schema.SQX_Result_Type__c.Description__c
                } ) ;
        }

        System.runas(users.get(STD_USER_1)){
            
            /**
             * Arrange:
             * Create audit criteria document with criteria requirement 
             * and change the status of audit criteria document to Current
             * and add audit with its status set to plan and save the audit
             */

            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document();
            auditCriteria.doc.RecordTypeId = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(auditCriteria.doc)).getRecordTypes().get(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            

            List<SQX_Doc_Criterion_Requirement__c> requirementsToInsert = new List<SQX_Doc_Criterion_Requirement__c>();
            List<String> auditResultTypes = new List<String>(SQX_Utilities.getPicklistValues(SQX.DocCriteriaRequirement, SQX.getNSNameFor('Result_Type__c')));
            System.assert(auditResultTypes.size() > 0, 'Expected Result_Type__c field in SQX_Doc_Criterion_Requirement__c object to have at least 1 value.');
            
            for(integer i = 0 ; i < 2; i++){
                randomNumber++;
                SQX_Doc_Criterion_Requirement__c criterionrequirement = new SQX_Doc_Criterion_Requirement__c();
                criterionrequirement.SQX_Controlled_Document__c = auditCriteria.doc.Id;
                criterionrequirement.Objective__c = 'RandomObjective-' + randomNumber;
                criterionrequirement.Section__c = 'RandomSection-' + randomNumber;
                criterionrequirement.Standard__c = 'RandomStandard-' + randomNumber;
                criterionrequirement.Sub_Section__c = 'RandomSection-' + randomNumber;
                criterionrequirement.Topic__c = 'Random Topic-' + randomNumber;
                criterionrequirement.SQX_Result_Type__c =  resultType.Id;
                criterionrequirement.Result_Type__c = auditResultTypes.size() == 1 ? auditResultTypes[0] : auditResultTypes[i];

                requirementsToInsert.add(criterionrequirement);
            }

            new SQX_DB().op_insert(requirementsToInsert, new List<Schema.SObjectField> {
                Schema.SQX_Doc_Criterion_Requirement__c.SQX_Controlled_Document__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Objective__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Section__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Standard__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Sub_Section__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Topic__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Result_Type__c,
                Schema.SQX_Doc_Criterion_Requirement__c.SQX_Result_Type__c
            });


            System.debug(requirementsToInsert);
            
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();


            audit.audit.SQX_Audit_Criteria_Document__c = auditCriteria.doc.Id;
            
            audit.save();

            List<SQX_Audit_Checklist__c> auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
            // Assert 1: Audit checklist should not be added to audit until it is in planned state 
            System.assert(auditChecklistList.size() == 0, 'Expected the number of checklist to be equal to 0 when the audit is in planned state');
            
            //Act 1: Change the status of audit to scheduled
            audit.setStage(SQX_Audit.STAGE_SCHEDULED).save();

            List<SQX_Audit_Checklist__c> auditChecklistListFromCriteria = [ SELECT Id, SQX_Audit_Criterion_Requirement__c, Result_Type__c
                                                                            FROM SQX_Audit_Checklist__c
                                                                            WHERE SQX_Audit_Criterion_Requirement__r.SQX_Controlled_Document__c =: auditCriteria.doc.Id];
            
            // Assert 2: Audit checklist should be added to audit and the added audit checklist should be same as specified in audit criteria document requirement 
            System.assertEquals(2, auditChecklistListFromCriteria.size(), 'Expected two audit criteria to be copied to the audit');
            
            // map result type with criterion requirement id
            Map<Id, String> reqResultTypes = new Map<Id, String>{
                requirementsToInsert[0].Id => requirementsToInsert[0].Result_Type__c,
                requirementsToInsert[1].Id => requirementsToInsert[1].Result_Type__c
            };
            
            System.assertEquals(reqResultTypes.get(auditChecklistListFromCriteria[0].SQX_Audit_Criterion_Requirement__c), auditChecklistListFromCriteria[0].Result_Type__c,
                'Expected Result_Type__c field value to be copied to checklist');
            System.assertEquals(reqResultTypes.get(auditChecklistListFromCriteria[1].SQX_Audit_Criterion_Requirement__c), auditChecklistListFromCriteria[1].Result_Type__c,
                'Expected Result_Type__c field value to be copied to checklist');
        }
    }

    /**
     * @description: When the audit criteria document  in current state and active audit program is included in the audit then
     *               its the criterion document requirement related to audit criteria document and audit program must be included in the audit checklist once it is scheduled
     *               and any there should be no duplicate checklist.
     * @author: Sudeep Maharjan
     * @date: 2016/09/05
     * @sqx-no: [SQX-2505]
    */
    static testmethod void givenACurrentAuditCriteriaDocumentAndAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementsAreAddedInAudit(){
        if(!runAllTests && !run_givenACurrentAuditCriteriaDocumentAndAnActiveAuditProgramAndAudit_WhenAuditIsScheduled_AllAuditCriteriaRequirementsAreAddedInAudit){
            return;
        }
        

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER_1);
        
        SQX_Result_Type__c resultType = new SQX_Result_Type__c();
        
        integer randomNumber = (integer)(Math.random() * 1000000);
        
        System.runAs(adminUser){
            
            resultType.Name = 'Yes/No/Skipped' + randomNumber;
            resultType.Description__c = 'Result Type Description' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Result_Type__c> {resultType}, new List<Schema.SObjectField>{
                Schema.SQX_Result_Type__c.Name,
                Schema.SQX_Result_Type__c.Description__c
                } ) ;
        }

        System.runas(users.get(STD_USER_1)){
        
            
            /**
             * Arrange:
             * Create Audit Criteria Document with Criteria Document Requirement and Audit Program with one Program Scope
             * and change the status of audit program to active approved and change the status of audit criteria document to Current
             * and add audit with its status set to plan and save the audit
             */

            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document();
            auditCriteria.doc.RecordTypeId = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(auditCriteria.doc)).getRecordTypes().get(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            
            List<SQX_Doc_Criterion_Requirement__c> requirementsToInsert = new List<SQX_Doc_Criterion_Requirement__c>();
            
            for(integer i = 0 ; i < 1; i++){
                randomNumber++;
                SQX_Doc_Criterion_Requirement__c criterionrequirement = new SQX_Doc_Criterion_Requirement__c();
                criterionrequirement.SQX_Controlled_Document__c = auditCriteria.doc.Id;
                criterionrequirement.Objective__c = 'RandomObjective-' + randomNumber;
                criterionrequirement.Section__c = 'RandomSection-' + randomNumber;
                criterionrequirement.Standard__c = 'RandomStandard-' + randomNumber;
                criterionrequirement.Sub_Section__c = 'RandomSection-' + randomNumber;
                criterionrequirement.Topic__c = 'Random Topic-' + randomNumber;
                criterionrequirement.SQX_Result_Type__c =  resultType.Id;

                requirementsToInsert.add(criterionrequirement);
            }

            new SQX_DB().op_insert(requirementsToInsert, new List<Schema.SObjectField> {
                Schema.SQX_Doc_Criterion_Requirement__c.SQX_Controlled_Document__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Objective__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Section__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Standard__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Sub_Section__c,
                Schema.SQX_Doc_Criterion_Requirement__c.Topic__c,
                Schema.SQX_Doc_Criterion_Requirement__c.SQX_Result_Type__c
            });


            SQX_Test_Audit_Program program1 = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope1 = program1.createScopeWithNewCriteriaDocument(1, adminUser); 
  
            program1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program1.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program1.save();

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            //Act 1: Add the audit program in audit and progarm is added to audit
            audit.includeInProgram(false, program1.program).save();
            //Act 2: Add the audit criteria document in audit
            audit.audit.SQX_Audit_Criteria_Document__c = auditCriteria.doc.Id;
            audit.save();
            //Act 3: Change the status of audit to scheduled
            audit.setStage(SQX_Audit.STAGE_SCHEDULED).save();

            List<SQX_Audit_Checklist__c>auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
            
            // Assert 1: Audit checklist should be equal to the number of criterion requirements in audit criteria plus number of criterion requirement specified in audit program scope
            System.assert(auditChecklistList.size() == 1,'Expected the number of checklist to be equal to the number of criterion requirements in audit criteria once the audit is scheduled');
            
            //Arrange: Create new audit and 
            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            //Act 4: Add the audit program in audit 
            audit1.includeInProgram(false, program1.program).save(); 

            //Act 5: Add the same audit criteria document as specified in audit prorgram in audit                           
            audit1.audit.SQX_Audit_Criteria_Document__c = scope1.SQX_Audit_Criteria_Document__c;
            audit1.save();
            //Act 6: Change the status of audit to scheduled
            audit1.setStage(SQX_Audit.STAGE_SCHEDULED).save();

            List<SQX_Audit_Checklist__c> auditChecklistList1 = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit1.audit.Id];
            //Assert 2: Audit checklist should be equal to the number of criterion requirements specified in audit program scope if the criterion requirement in audit is the same as criterion requirement specified in audit program scope 
        
            System.assert(auditChecklistList1.size() == 1,'Expected the number of checklist to be equal to the number of criterion requirements specified in audit program scope if the criterion requirement in audit is the same as criterion requirement specified in audit program scope once the audit is scheduled');
        }
    }

    /**
    * This test ensures that if an audit criteria document is being referred by an active audit(Not plan, void or draft) with its checklist copied
    * it can be obsoleted/pre-expired.
    */
    static testmethod void givenAuditCriteriaIsBeingReferredByActiveAudit_WhenDocIsPreExpired_NoErrorIsPresented(){
        if(!runAllTests && !run_givenAuditCriteriaIsBeingReferredByActiveAudit_WhenDocIsPreExpired_NoErrorIsPresented){
            return;
        }

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER_1);
        
        System.runAs(users.get(STD_USER_1)){
            // Arrange: Create a Audit criteria doc with checklist and an active Audit (Not in plan)
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save();

            SQX_Result_Type__c resultType = SQX_Test_Utilities.setupResultTypes(1, 2)[0];
            SQX_Doc_Criterion_Requirement__c [] req = auditCriteria.addCriterionRequirements(2, resultType.Id);

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.save();

            SQX_Audit_Checklist__c checklist = new SQX_Audit_Checklist__c();
            checkList.SQX_Audit__c = audit.audit.Id;
            checklist.SQX_Audit_Criterion_Requirement__c = req[0].Id;
            checklist.SQX_Result_Type__c = resultType.Id;
            checklist.Section__c = 'asd';
            checklist.Topic__c = 'asd';
            checklist.Objective__c = 'asd';
            new SQX_DB().op_insert(new List<SQX_Audit_Checklist__c> { checklist }, new List<SObjectField> { SQX_Audit_Checklist__c.SQX_Audit__c, SQX_Audit_Checklist__c.SQX_Audit_Criterion_Requirement__c });

            // Act: Try pre-expiring the criteria document
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c(Id = auditCriteria.doc.Id, Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE);
            Database.SaveResult result = Database.update(doc, false);

            // Assert: Ensure that audit criteria
            System.assertEquals(false, result.isSuccess());


            // Act: Try obsoleting the criteria document
            doc = new SQX_Controlled_Document__c(Id = auditCriteria.doc.Id, Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE);
            result = Database.update(doc, false);

            // Assert: Ensure that audit criteria is obsoleted
            System.assertEquals(true, result.isSuccess());

        }
    }

    /**
    * Given: a Audit Criteria with Current status,
    * When:  when an Active Audit Prgram is to the Audit Criteria
    * Then:  Audit Criteria cannot be obsolete
    * 
    * @author: Sajal Joshi
    * @date: 06/11/2016
    * @story: [SQX-2538]
    */ 
    public testmethod static void whenReferedByActiveAuditProgram_ThenAuditCriteriaCannotBeObsolete(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standUser = allUsers.get(STD_USER_1);
        System.runas(standUser){
            
            //Create Audit Criteria with Current Status
            SQX_Test_Controlled_Document auditCriteria= new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            //Arrange: Create a Audit Program with Audit Program Scope refered to a Current Audit Criteria
            SQX_Test_Audit_Program auditProgram = new SQX_Test_Audit_Program().save();
            SQX_Audit_Program_Scope__c scope = auditProgram.addScopeForCriteriaDocument(auditCriteria);
            auditProgram.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            auditProgram.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            auditProgram.save();
            
            //Act: Try to Obsolete Audit Criteria
            SQX_Controlled_Document__c criteria = new SQX_Controlled_Document__c(Id = auditCriteria.doc.Id, Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE, Expiration_Date__c = System.today());
            List<Database.SaveResult>  result = new SQX_DB().continueOnError().op_update(new List<SQX_Controlled_Document__c>{criteria}, new List<Schema.SObjectField>{
                                                                            SQX_Controlled_Document__c.Document_Status__c, SQX_Controlled_Document__c.Expiration_Date__c});
            
            //Assert: Ensure the Audit Criteria is not Updated
            System.assertEquals(false, result[0].isSuccess(), 'Expected update record to fail');
            
        }
    }    
}