@isTest
public class SQX_Test_1656_ContDoc_TitleDesc_Changed{
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }

    /**
    * Given     :   Controlled Document is created with a content document upladed for the doc
    * When      :   Title or description of contrlled doc is changed
    * Then      :   Title or description of related content document is also changed to controlled doc's title and description
    * @author Sagar Shrestha
    * @date 2016/04/07
    * @story [SQX-1656]
    */ 
    public testmethod static void givenUser_WhenControlledDocTitleOrDescChanged_ContentTitleOrDescAlsoChanged(){

        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        System.runas(standardUser){ 
            //Arrange  : Create document a1
            SQX_Test_Controlled_Document edoc = new SQX_Test_Controlled_Document();
            SQX_Controlled_Document__c a1Doc = edoc.doc;
            ApexPages.StandardController controller = new ApexPages.StandardController(a1Doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            a1Doc.Title__c = 'Document Title ';
            a1Doc.Description__c = 'Document Description ';
            a1Doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
            edoc.setStatus(SQX_Controlled_Document.STATUS_DRAFT);
            edoc.save(true); 

            
            SQX_Controlled_Document__c doc = [Select Title__c, Description__c, Content_Reference__c from SQX_Controlled_Document__c where Id = :a1Doc.Id];
            //get latest content version of the controlled doc a1
            ContentVersion cv = [Select Title, Description from ContentVersion WHERE ContentDocumentId = :doc.Content_Reference__c AND IsLatest=true];
            //Ensure that title and description of controlled doc and related content doc are synced at creation 
            System.assertEquals(doc.Title__c, cv.Title, 'Expected title of controlled doc and related content doc to be synced');
            System.assertEquals(doc.Description__c, cv.Description, 'Expected description of controlled doc and related content doc to be synced');

            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            //Act : Change title and description of controlled doc
            doc.Title__c = 'This title Should propagate to related content doc';
            doc.Description__c = 'This description Should propagate to related content doc';
            Update doc;


            //get latest content version of the controlled doc a1
            cv = [Select Title, Description from ContentVersion WHERE ContentDocumentId = :doc.Content_Reference__c AND IsLatest=true];

            //Assert : Ensure that title and description of controlled doc and related content doc are synced at creation 
            System.assertEquals(doc.Title__c, cv.Title, 'Expected title of controlled doc and related content doc to be synced');
            System.assertEquals(doc.Description__c, cv.Description, 'Expected description of controlled doc and related content doc to be synced');

        }

    }
    
    /**
     * Scenario 1:
     * ACTION: Create a doc by standard user 1 and try updating doc by standard user 2
     * EXPECTED: Doc should not be saved and should throw an error.
     * @date: 2016-05-24
     * @story: [SQX-2315]
    */
    public testMethod static void givenADoc_WhenTheDocISUpdatedCreatedByAnotherUser_ItGivesUser(){
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        User adminUser = userMap.get('adminUser');

        SQX_Test_Controlled_Document cDoc1 =  new SQX_Test_Controlled_Document();
        
        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            cDoc1.save(true);

        }

        System.runas(adminUser){

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Title__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            controlledDoc.Title__c = 'Changed Title';
            // Act : try updating doc by standard user 2
            Database.SaveResult result = Database.update(controlledDoc, false);

            // Assert : Doc should not be saved and should throw an error.
            System.assert(!result.isSuccess(), 'Expected the save not to be successful');

            System.assertEquals(Label.SQX_ERR_CONTROLLED_DOCUMENT_RELATED_CONTENT_NOT_SHARED, result.getErrors()[0].getMessage(), 'Expected to get correct error msg');

        }
    }

}