/*
* Unit test of Validation Rules for NSI Edit
*/ 
@isTest
public class SQX_Test_6115_NSI_Edit_ValidationRule {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, adminUser}); 
    }
    
    /*
    * Given: given NSI Close record
    * When: user try to edit the record and void or close the same record
    * Then : throws the validation error message
    */
    public static testMethod void givenNSIAlreadyClose_WhenEditAndCloseTheRecord_ThenThrowErrorMessage(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 
            
            //Arrange: Create NSI Record
            SQX_Test_NSI tNsi = new SQX_Test_NSI().save();

            tNsi.submit();
            tNsi.initiate();
            tNsi.close();

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Try to Edit the record Once record is Closed 
            tNsi.clear();
            tNsi.nsi.Email__c='abc@xyz.com';
            
            //Act:Try to update the record 
            List<Database.SaveResult> closeResult =new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c>{ tNsi.nsi }, new List<SObjectField>{});
            
            //Assert: display Validation Error
            System.assertEquals(1, closeResult[0].getErrors().size(), 'Expected exactly one error to be thrown but got ' + closeResult[0].getErrors().size());
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_EDIT_AFTER_LOCK_VALIDATION_ERROR_MSG, closeResult[0].getErrors()[0].getMessage());
        } 
    }
    
    /*
    * Given: given NSI Void record
    * When: user try to edit the record and void or closed the same record
    * Then : throws the validation error message
    */
    public static testMethod void givenNSIAlreadyVoid_WhenEditAndVoidTheRecord_ThenThrowErrorMessage(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){ 
            
            //Arrange: Create NSI Record
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            //Void the NSI Record
            nsi.void();

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act:Try to update the record 
            nsi.clear();
            nsi.nsi.Email__c = 'abc@xyz.com';

            List<Database.SaveResult> voidResult =new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi.nsi }, new List<SObjectField>{});

            //Assert: display Validation Error
            System.assertEquals(1, voidResult[0].getErrors().size(), 'Expected exactly one error to be thrown but got ' + voidResult[0].getErrors().size());
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_EDIT_AFTER_LOCK_VALIDATION_ERROR_MSG, voidResult[0].getErrors()[0].getMessage());
        } 
    }

    /**
     *  Method validates that specific actions can only be performed at certain scenario else error is thrown
     */
    private testmethod static void validateInvalidStateTransitions() {

        SavePoint sp;

        // ARRANGE : set valid/invalid scenarios

        final List<String> activities = new List<String>
        {
            SQX_Supplier_Common_Values.ACTIVITY_CODE_SUBMIT,
            SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE,
            SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART,
            SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID,
            SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE
        };

        final Map<Integer, List<String>> validTransitions = new Map<Integer, List<String>>
        {
            0 => new List<String> { SQX_Supplier_Common_Values.ACTIVITY_CODE_SUBMIT, SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID },     // after creation
            1 => new List<String> { SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE, SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID },   // after submission
            2 => new List<String> { SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE, SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART, SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID },    // after completion
            3 => new List<String> {}    // after closure, nothing should be valid
        };

        SQX_Test_NSI nsi = new SQX_Test_NSI();
        nsi.save();

        Test.startTest();

        // ACT : Perform actions one by one

        for(Integer key : validTransitions.keySet()) {
            
            // synchronize
            nsi.nsi = [SELECT Status__c, Record_Stage__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id];
            
            List<String> validActs = validTransitions.get(key);

            // first validating invalid step transitions
            for(Integer i=0; i < activities.size(); i++) {
                if(validActs.indexOf(activities.get(i)) == -1) {
                    Database.SaveResult result = executeAction(nsi.nsi, activities.get(i));

                    // ASSERT : Invalid transitions should not be allowed
                    System.assertEquals(false, result.isSuccess(), activities.get(i) + ' activity should not be allowed for the current record ' + nsi.nsi);
                    System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, result.getErrors()[0].getMessage());
                }
            }

            // perform valid actions but do not commit
            // ASSERT : valid actions should be permitted
            for(Integer i=1; i < validActs.size() ; i++) {
                sp = Database.setSavePoint();
                
                Database.SaveResult result = executeAction(nsi.nsi, validActs.get(i));
                System.assertEquals(true, result.isSuccess(), 'Expected action ' + validActs.get(i) + ' to be successful for record' + nsi.nsi + 'but got error ' + result.getErrors());
                
                Database.rollback(sp);
                sp = null;
            }


            // now validating that valid actions are allowed
            if(validActs.size() > 0) {
                Database.SaveResult result = executeAction(nsi.nsi, validActs.get(0));
                System.assertEquals(true, result.isSuccess(), 'Expected action ' + validActs.get(0) + ' to be successful for record' + nsi.nsi + ' but got error ' + result.getErrors());
            }

        }

        Test.stopTest();

    }

    /**
     *  Helper method to perform the specified action on the given NSI record
     */
    private static Database.SaveResult executeAction(SQX_New_Supplier_Introduction__c nsi, String activityCode) {

        SQX_BulkifiedBase.clearAllProcessedEntities();

        if(activityCode.equals(SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART)) {
            nsi.Current_Step__c = 1;
        }

        nsi.Activity_Code__c = activityCode;

        return new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c> { nsi }, new List<SObjectField> {}).get(0);
    }



}