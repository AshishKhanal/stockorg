/**
 * This test ensures that the functionalities defined in extension work correctly
 */
@IsTest
public class SQX_Test_Extension_Requirement_MultiEdit {
    final static String ADMIN_USER_1 = 'ADMIN_USER_1',
                        DOCUMENT_NUMBER = 'Test_Document';

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER_1);

        System.runAs(adminUser) {
            SQX_Job_Function__c[] jfs = new List<SQX_Job_Function__c>();
            for(Integer i = 0; i < 30; i++) {
                jfs.add(new SQX_Job_Function__c(Name = 'JF-' + i));
            }

            insert jfs;

            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.doc.Document_Number__c = DOCUMENT_NUMBER;
            doc.save();
        }

    }

    /**
     * This method ensures that when requirements or refresher are enabled then data is saved correctly
     * Given Document with no requirement
     * When requirement is enabled, refresher only is enabled and both requriment and refresher is enabled for Job Function
     * Then requirement is added for first and third case
     */
    testmethod static void givenDocWithoutRequirementAndJFs_WhenMultiEditing_DataIsSavedCorrectly() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            //Arrange: Get Document that was setup
            SQX_Controlled_Document__c doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: DOCUMENT_NUMBER];

            SQX_Extension_Requirement_MultiEdit ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));

            // Act: a. Enable requirement for first requirement
            //      b. Enable refresher only for second requirement
            //      c. Enable both requirement and refresher for third requirement
            //      And then execute a save.
            ext.dataSet[0].EnableRequirement = true;
            ext.dataSet[1].EnableRefresher = true;
            ext.dataSet[2].EnableRequirement = true;
            ext.dataSet[2].EnableRefresher = true;

            ext.save();
            
            // Assert: Ensure all requirements are inserted correctly.
            Map<Id, SQX_Requirement__c> reqs = new Map<Id, SQX_Requirement__c>();
            for(SQX_Requirement__c req : [SELECT Id, SQX_Job_Function__c, Require_Refresher__c 
                                          FROM SQX_Requirement__c
                                          WHERE SQX_Controlled_Document__c = : doc.Id]) {
                reqs.put(req.SQX_Job_Function__c, req);
            }

            System.assert(reqs.containsKey(ext.dataSet[0].JobFunctionId), 'Ensure that a requirement for 1st JF has been added');
            System.assert(!reqs.containsKey(ext.dataSet[1].JobFunctionId), 'Ensure that requirement for 2nd JF is not added');
            System.assert(reqs.containsKey(ext.dataSet[2].JobFunctionId), 'Ensure that requirement for 3rd JF is not added');
            System.assert(reqs.get(ext.dataSet[2].JobFunctionId).Require_Refresher__c, 'Ensure that req for 3rd JF has refresher');
            System.assert(!reqs.get(ext.dataSet[0].JobFunctionId).Require_Refresher__c, 'Ensure that req for 1st JF has no refresher');


            // Act: Disable the requirement for requirement 3
            ext.dataSet[2].EnableRequirement = false;
            ext.save();

            // Assert: Ensure that requirement is deleted
            System.assert([SELECT IsDeleted FROM SQX_Requirement__c WHERE SQX_Job_Function__c = : ext.dataSet[2].JobFunctionId ALL ROWS].IsDeleted, 'Ensure the previously activated requirement is deleted');

            // Arrange: Update the requirement for requirement 1 with refresher competency
            update new SQX_Requirement__c(Id = reqs.get(ext.dataSet[0].JobFunctionId).Id,
                                          Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT);

            

        }
    }

    /**
     * This method ensures that pagination doesn't remove users selection
     * Given Doc Without Req and JFs
     * When Enabling Requirement Across Page i.e. enabling refresher in 1st page and second page's 1st item and navigating back
     * Then Requirement is correctly added
     */
    testmethod static void givenDocWithoutReqAndJFs_WhenEnablingRequirementAcrossPage_ItIsDoneCorrectly() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            //Arrange: Get Document that was setup
            SQX_Controlled_Document__c doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: DOCUMENT_NUMBER];

            SQX_Extension_Requirement_MultiEdit ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));

            // Act: Enable requirement 1 and next + 1
            ext.dataSet[0].EnableRequirement = true;
            ext.next();
            ext.dataSet[0].EnableRequirement = true;
            ext.previous();
            ext.save();

            // Assert: Ensure that two requirements are added
            System.assertEquals(2, [SELECT Id FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c = :doc.Id].size());

        }
    }

    /**
     * This method ensures that default values are copied correctly to the newly added requirement
     * Given Doc Without Req and JFs
     * When Setting default values and enabling requirement
     * Then Requirement is added with new default value
     * When value is already present while enabling refresher
     * Then old value isn't overwritten
     */
    testmethod static void givenDocWithoutReqAndJFs_WhenSpecifyingDefaultValue_RequirementHasDefaultValueSet() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            //Arrange: Get Document that was setup
            SQX_Controlled_Document__c doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: DOCUMENT_NUMBER];

            SQX_Extension_Requirement_MultiEdit ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));

            
            // Act: Enable requirement and refresher for requirement 4 and specify default value
            ext.dataSet[3].EnableRequirement = true;
            ext.dataSet[3].EnableRefresher = true;
            ext.refresherDefaultValue.Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            ext.defaultValue.Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            ext.refresherDefaultValue.Days_to_Complete_Refresher__c = 33;
            ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c = 22;
            ext.refresherDefaultValue.Refresher_Interval__c = 171;
            ext.save();

            // Assert: Ensure that requirement for 4 has set default value
            SQX_Requirement__c req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];

            System.assertEquals(ext.defaultValue.Level_Of_Competency__c, req4.Level_Of_Competency__c);
            System.assertEquals(ext.refresherDefaultValue.Refresher_Competency__c, req4.Refresher_Competency__c);
            System.assertEquals(ext.refresherDefaultValue.Days_to_Complete_Refresher__c, req4.Days_to_Complete_Refresher__c);
            System.assertEquals(ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c, req4.Days_in_Advance_to_Start_Refresher__c);
            System.assertEquals(ext.refresherDefaultValue.Refresher_Interval__c, req4.Refresher_Interval__c);


            // Arrange: Change default value and disable refresher
            update new SQX_Requirement__c(Id = req4.Id, Require_Refresher__c = false);
            ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));
            ext.defaultValue.Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            ext.defaultValue.Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;

            // Act: Enable refresher and save
            ext.dataSet[3].EnableRefresher = true;
            ext.save();

            // Assert: Ensure that requirement 4's competency isn't changed
            req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];

           System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT, req4.Level_Of_Competency__c);
           System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT, req4.Refresher_Competency__c);

        }
    }

    /**
    * This method checks the initial load screen has correct value for Refresher Enabled and Requirement Enabled
    * Given Document without requirement and JF
    * When Extension is initially loaded
    * Then Enable Refresher and Enable Requirement are set appropriately.
    */
    testmethod static void givenDocWithoutRequirementAndJFs_WhenLoadingMultiEdit_DataSetIsSetCorrectly() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            //Arrange: Get Document that was setup
            SQX_Controlled_Document__c doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: DOCUMENT_NUMBER];

            SQX_Extension_Requirement_MultiEdit ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));

            // Act: Get the dataSet
            List<SQX_Extension_Requirement_MultiEdit.WrappedRequirement> reqs = new List<SQX_Extension_Requirement_MultiEdit.WrappedRequirement>();
            while(ext.pagedContainer.getHasNext()){
                reqs.addAll(ext.dataSet);
                ext.next();
            }

            // Assert: Ensure that dataset has returned records with no RefresherEnabled and RequirementEnabled
            //         Since, no requirements have been added as of yet
            for(SQX_Extension_Requirement_MultiEdit.WrappedRequirement req : reqs){
                System.assertEquals(false, req.EnableRequirement, 'Requirement is incorrectly set for ' + req);
                System.assertEquals(false, req.EnableRefresher, 'Refresher is incorrectly set for' + req);
            }
        }
    }

    /**
     * This method ensures that default values are not updated once set
     * Given Doc, Req and JFs
     * When Adding default values and enabling only requirement
     * Then requirement is created with only level of competency set
     * When Adding default values and enabling refresher
     * Then refresher fields are set with default value but level of competency is not updated
     * When refresher is disabled
     * Then all default values stay the same
     * When Updating default values and enabling refresher again
     * Then refresher is enabled but exiting default values are not overridden by new values
     */
    testmethod static void givenDocWithReqAndJFs_WhenUpdatingDefaultValue_DefaultValuesIsntUpdatedOnceSet() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            //Arrange: Get Document and job function that was setup and add a requirement
            SQX_Controlled_Document__c doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: DOCUMENT_NUMBER];
            SQX_Extension_Requirement_MultiEdit ext = new SQX_Extension_Requirement_MultiEdit(new ApexPages.StandardController(doc));

            // Act: Enable only requirement and set default values
            ext.dataSet[3].EnableRequirement = true;
            ext.defaultValue.Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            ext.refresherDefaultValue.Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            ext.refresherDefaultValue.Days_to_Complete_Refresher__c = 33;
            ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c = 22;
            ext.refresherDefaultValue.Refresher_Interval__c = 171;
            ext.save();
            
            SQX_Requirement__c req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c, Require_Refresher__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];
            
            // Assert: only requirement fields should be updated
            System.assertEquals(ext.defaultValue.Level_Of_Competency__c, req4.Level_Of_Competency__c, 'Level of compentency should be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Refresher_Competency__c, req4.Refresher_Competency__c, 'Refresher competency should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Level_Of_Competency__c, req4.Level_Of_Competency__c, 'Level of compentency should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Days_to_Complete_Refresher__c, req4.Days_to_Complete_Refresher__c, 'Days to complete refresher should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c, req4.Days_in_Advance_to_Start_Refresher__c, 'Days in advance to start refresher should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Refresher_Interval__c, req4.Refresher_Interval__c, 'refresher interval should not be updated');

            // Act: Set default values and enable refresher
            ext.dataSet[3].EnableRefresher = true;
            ext.defaultValue.Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            ext.refresherDefaultValue.Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            ext.refresherDefaultValue.Days_to_Complete_Refresher__c = 20;
            ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c = 20;
            ext.refresherDefaultValue.Refresher_Interval__c = 20;
            ext.save();

            // Assert: Ensure that only refresher and default values are updated
            req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c, Require_Refresher__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];
            
            System.assertEquals(ext.dataSet[3].EnableRefresher, req4.Require_Refresher__c, 'Require Refresher field should be updated');
            System.assertNotEquals(ext.defaultValue.Level_Of_Competency__c, req4.Level_Of_Competency__c, 'Level of compentency should not be updated');
            System.assertEquals(ext.refresherDefaultValue.Refresher_Competency__c, req4.Refresher_Competency__c, 'Refresher competency should be updated');
            System.assertEquals(ext.refresherDefaultValue.Days_to_Complete_Refresher__c, req4.Days_to_Complete_Refresher__c, 'Days to complete refresher should be updated');
            System.assertEquals(ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c, req4.Days_in_Advance_to_Start_Refresher__c, 'Days in advance to start refresher should be updated');
            System.assertEquals(ext.refresherDefaultValue.Refresher_Interval__c, req4.Refresher_Interval__c, 'refresher interval should be updated');
            
            // Act: Disbale refresher
            ext.dataSet[3].EnableRefresher = false;
            ext.save();

            // Assert: Ensure that default values are the same
            req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c, Require_Refresher__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];
            
            System.assertEquals(ext.dataSet[3].EnableRefresher, req4.Require_Refresher__c, 'Require Refresher field should be updated');
            System.assertEquals(ext.refresherDefaultValue.Refresher_Competency__c, req4.Refresher_Competency__c, 'Refresher competency should be updated');
            System.assertEquals(ext.refresherDefaultValue.Days_to_Complete_Refresher__c, req4.Days_to_Complete_Refresher__c, 'Days to complete refresher should be updated');
            System.assertEquals(ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c, req4.Days_in_Advance_to_Start_Refresher__c, 'Days in advance to start refresher should be updated');
            System.assertEquals(ext.refresherDefaultValue.Refresher_Interval__c, req4.Refresher_Interval__c, 'refresher interval should be updated');
            
            // Act: Change default values and enable refresher again
            ext.dataSet[3].EnableRefresher = true;
            ext.defaultValue.Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY;
            ext.refresherDefaultValue.Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY;
            ext.refresherDefaultValue.Days_to_Complete_Refresher__c = 40;
            ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c = 40;
            ext.refresherDefaultValue.Refresher_Interval__c = 40;
            ext.save();

            // Assert: Ensure that only refresher is updated and default values are not
            req4 = [SELECT Refresher_Competency__c, Level_Of_Competency__c, Days_to_Complete_Refresher__c,
                                              Days_in_Advance_to_Start_Refresher__c, Refresher_Interval__c, Require_Refresher__c
                                       FROM SQX_Requirement__c
                                       WHERE SQX_Job_Function__c = :ext.dataSet[3].JobFunctionId];
            
            System.assertEquals(ext.dataSet[3].EnableRefresher, req4.Require_Refresher__c, 'Require Refresher field should be updated');
            System.assertNotEquals(ext.defaultValue.Level_Of_Competency__c, req4.Level_Of_Competency__c, 'Level of compentency should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Refresher_Competency__c, req4.Refresher_Competency__c, 'Refresher competency should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Days_to_Complete_Refresher__c, req4.Days_to_Complete_Refresher__c, 'Days to complete refresher should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Days_in_Advance_to_Start_Refresher__c, req4.Days_in_Advance_to_Start_Refresher__c, 'Days in advance to start refresher should not be updated');
            System.assertNotEquals(ext.refresherDefaultValue.Refresher_Interval__c, req4.Refresher_Interval__c, 'refresher interval should not be updated');

        }
    }
}