/**
 * This Test class ensures that the Submitted By/Date and Initiation fields are populated when NSI record is submitted.
 */
@isTest
public class SQX_Test_NSI_SD_Update_Records {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
		User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
    }
    
    /**
     * Given: An NSI record 
     * When: NSI record is submitted
     * Then: Submitted By and Submitted Date fields must be populated with user and date.
     */
    @isTest
    public static void ensureSubmittedDateAndSubmittedByFieldsArePopulatedOnNSIRecordSubmit() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_NSI nsi;
        System.runAs(standardUser){
            // Arrange: NSI record is created
            nsi = new SQX_Test_NSI().save();
            
            // Act: Record is submitted
            nsi.submit();
            
            SQX_New_Supplier_Introduction__c updatedNsi = [SELECT
                                                           compliancequest__Submitted_By__c,
                                                           compliancequest__Submitted_Date__c
                                                           FROM
                                                           SQX_New_Supplier_Introduction__c
                                                           WHERE
                                                           Id =: nsi.nsi.Id];
            
            // Assert: Check if submitted by and submitted date fields are populated and matches to the expected current user and current date.
            System.assertEquals(standardUser.Id, updatedNsi.compliancequest__Submitted_By__c, 'Submitted by does not match after NSI record is submitted.');
            System.assertNotEquals(null, updatedNsi.compliancequest__Submitted_Date__c, 'Submitted date is not set when NSI record is submitted.');
        }
    }
    
    /**
     * Given: An SD record 
     * When: SD record is submitted
     * Then: Submitted By and Submitted Date fields must be populated with user and date.
     */
    @isTest
    public static void ensureSubmittedDateAndSubmittedByFieldsArePopulatedOnSDRecordSubmit() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Deviation  sd;
        System.runAs(standardUser){
            // Arrange: NSI record is created
            sd  = new SQX_Test_Supplier_Deviation ().save();
            
            // Act: Record is submitted
            sd.submit();
            
            SQX_Supplier_Deviation__c updatedSd = [SELECT
                                                   compliancequest__Submitted_By__c,
                                                   compliancequest__Submitted_Date__c
                                                   FROM
                                                   SQX_Supplier_Deviation__c
                                                   WHERE
                                                   Id =: sd.sd.Id];
            
            // Assert: Check if submitted by and submitted date fields are populated and matches to the expected current user and current date.
            System.assertEquals(standardUser.Id, updatedSd.compliancequest__Submitted_By__c, 'Submitted by does not match after SD record is submitted.');
            System.assertNotEquals(null, updatedSd.compliancequest__Submitted_Date__c, 'Submitted date is not set when SD record is submitted.');
        }
    }

    /**
     * Given: An NSI record 
     * When: NSI record is submitted and initiated
     * Then: Initiation date field must be populated with current date.
     */
    @isTest
    public static void ensureInitiationDateIsPopulatedOnNSIRecordInitiate() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        User assigneeUser = allUsers.get('assigneeUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        SQX_Test_NSI nsi;
        
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Perform Audit', assigneeUser, 1);
        }
        
        System.runAs(standardUser){
            // Arrange: NSI record is created
            nsi = new SQX_Test_NSI().save();
            
            // Act: Record is submitted and then initiated
            nsi.submit();
            nsi.initiate();
            
            SQX_New_Supplier_Introduction__c updatedNsi = [SELECT
                                                           compliancequest__Initiation_Date__c
                                                           FROM
                                                           SQX_New_Supplier_Introduction__c
                                                           WHERE
                                                           Id =: nsi.nsi.Id];
            
           // Assert: Check if initiation date is populated and matches to the expected date.
           System.assertNotEquals(null, updatedNsi.compliancequest__Initiation_Date__c, 'Initiation datetime does not match after NSI record is initiated.');
        }
    }

    /**
     * Given: An SD record 
     * When: SD record is submitted and initiated
     * Then: Initiation date field must be populated with current date.
     */
    @isTest
    public static void ensureInitiationDateIsPopulatedOnSDRecordInitiate() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        User assigneeUser = allUsers.get('assigneeUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        SQX_Test_Supplier_Deviation sd;
        
        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Perform Audit', assigneeUser, 1);
        }
        
        System.runAs(standardUser){
            // Arrange: SD record is created
            sd = new SQX_Test_Supplier_Deviation().save();
            
            // Act: Record is submitted and then initiated
            sd.submit();
            sd.initiate();
            
            SQX_Supplier_Deviation__c updatedSd = [SELECT
                                                   Initiation_Date__c
                                                   FROM
                                                   SQX_Supplier_Deviation__c
                                                   WHERE
                                                   Id =: sd.sd.Id];
            
            // Assert: Check if initiation date is populated and it means it is not null.
            System.assertNotEquals(null, updatedSd.compliancequest__Initiation_Date__c, 'Initiation datetime does not match after SD record is initiated.');
        }
    }
}