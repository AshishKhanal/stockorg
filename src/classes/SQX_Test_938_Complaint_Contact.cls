/**
 * test class to ensure that the complaint contact is added if not already added when contact is changed in Complaint
 * @author: Sagar Shrestha
 * @date: 2015-12-17
 * @story: [SQX-938] 
 */
@isTest
public class SQX_Test_938_Complaint_Contact{

    static boolean runAllTests = false,
                    givenComplaint_ContactChanged_ComplaintContactAdded = true;


    /**
     * Action: Complaint feld External Contact is changed
     * Expected: Complaint Contact Record is added if not already added
     * @author: Sagar Shrestha
     * @date: 2015-12-17
     */
    public testmethod static void givenComplaint_ContactChanged_ComplaintContactAdded(){

        if(!runAllTests && !givenComplaint_ContactChanged_ComplaintContactAdded){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        Account supplier;
        Contact contactA;
        Contact contactB;
        Contact contactC;

        SQX_Picklist_Value__c complaintContactCustomer = null;
        System.runas(adminUser){
            //create new Account and Contacts related to it
            supplier = SQX_Test_Account_Factory.createAccount();
            contactA = SQX_Test_Account_Factory.createContact(supplier);
            contactB = SQX_Test_Account_Factory.createContact(supplier);
            contactC = SQX_Test_Account_Factory.createContact(supplier);

            complaintContactCustomer = new SQX_Picklist_Value__c(Name = 'Customer', Value__c = 'Customer', Category__c = 'Complaint Contact Type');
			insert complaintContactCustomer;
        }
        
        
        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;

        System.runas(standardUser){
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act 1: change external contact to ContactA
            complaint.complaint.SQX_Account__c = supplier.Id;
            complaint.complaint.SQX_External_Contact__c = contactA.Id;
            complaint.save();

            //Assert 1 : Expected to add complaint contact
            List<SQX_Complaint_Contact__c> complaintContacts = [Select SQX_Contact__c,Name, First_Name__c, SQX_Account__c,Company_Name__c,Type__c, SQX_Type__c
                                                             from SQX_Complaint_Contact__c where SQX_Complaint__c = :complaint.complaint.Id];
            System.assertEquals(1, complaintContacts.size(), 'Expected to add external contact to complaint contact');
            System.assertEquals(contactA.Id, complaintContacts[0].SQX_Contact__c, 'Expected same contact to be added as to the complaint external is set');
            System.assertEquals(contactA.LastName, complaintContacts[0].Name,'Expected Last Name to be set');            
            System.assertEquals(contactA.FirstName, complaintContacts[0].First_Name__c,'Expected First Name to be set');            
            System.assertEquals(contactA.AccountId, complaintContacts[0].SQX_Account__c,'Expected Account ID to be set');            
            System.assertEquals(contactA.Account.Name, complaintContacts[0].Company_Name__c,'Expected Account ID to be set');            
            System.assertEquals(SQX_Complaint.CONTACT_TYPE_CUSTOMER, complaintContacts[0].Type__c,'Expected type to be set to customer');
            System.assertEquals(complaintContactCustomer.Id, complaintContacts[0].SQX_Type__c,'Expected type to be set to customer');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();



            //Act 2: change external contact to ContactB
            complaint.complaint.SQX_External_Contact__c = contactB.Id;
            complaint.save();  

            //Assert 2 : Expected to add complaint contact
            complaintContacts = [Select SQX_Contact__c from SQX_Complaint_Contact__c where SQX_Complaint__c = :complaint.complaint.Id];
            System.assertEquals(2, complaintContacts.size(), 'Expected to add external contact to complaint contact');
            Set<Id> complaintContactIds  = new Set<Id>();
            for(SQX_Complaint_Contact__c complaintContact: complaintContacts){
                complaintContactIds.add(complaintContact.SQX_Contact__c);
            }
            System.assertEquals(true, complaintContactIds.contains(contactA.Id));
            System.assertEquals(true, complaintContactIds.contains(contactB.Id));
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();


            

            //Act 3: change external contact to ContactC
            complaint.complaint.SQX_External_Contact__c = contactC.Id;
            complaint.save();   

            //Assert 3 : Expected to add complaint contact
            complaintContacts = [Select SQX_Contact__c from SQX_Complaint_Contact__c where SQX_Complaint__c = :complaint.complaint.Id];
            System.assertEquals(3, complaintContacts.size(), 'Expected to add external contact to complaint contact');
            complaintContactIds  = new Set<Id>();
            for(SQX_Complaint_Contact__c complaintContact: complaintContacts){
                complaintContactIds.add(complaintContact.SQX_Contact__c);
            }
            System.assertEquals(true, complaintContactIds.contains(contactA.Id));
            System.assertEquals(true, complaintContactIds.contains(contactB.Id));
            System.assertEquals(true, complaintContactIds.contains(contactC.Id));

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();



            
            //Act 4: change external contact to already added contact - ContactA
            complaint.complaint.SQX_External_Contact__c = contactA.Id;
            complaint.save();   

            //Assert 4 : Expected not to add complaint contact as that is already added
            complaintContacts = [Select SQX_Contact__c from SQX_Complaint_Contact__c where SQX_Complaint__c = :complaint.complaint.Id];
            System.assertEquals(3, complaintContacts.size(), 'Expected to remain complaint contact size to be same');

                    
        }
    }
}