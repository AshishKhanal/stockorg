/**
* Unit tests for Approval Matrix Step object
*/
@IsTest
public class SQX_Test_1981_Approval_Matrix_Step {

    static Boolean runAllTests = true,
                   run_givenApprovalMatrixStep_ZeroOrNegativeStepNumber_NotSaved = false;
    
    /**
    * ensures addition/modification of approval matrix step with zero or negative step number is not saved
    * related vaidation rule Prevent_Negative_Or_Zero_Step_Number
    */
    public testmethod static void givenApprovalMatrixStep_ZeroOrNegativeStepNumber_NotSaved() {
        if (!runAllTests && !run_givenApprovalMatrixStep_ZeroOrNegativeStepNumber_NotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // add required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1' );
            Database.SaveResult result1 = Database.insert(jf1, false);
            System.assert(result1.isSuccess() == true, 'Required job function is expected to be saved.');
            
            // add required approval matrix
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 1' );
            result1 = Database.insert(am1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            // ACT: add approval matrix step with zero step number
            SQX_Approval_Matrix_Step__c amStep1 = new SQX_Approval_Matrix_Step__c(
                SQX_Approval_Matrix__c = am1.Id,
                SQX_Job_Function__c = jf1.Id,
                Step__c = 0
            );
            result1 = Database.insert(amStep1, false);
            
            System.assert(result1.isSuccess() == false,
                'Approval matrix step with zero step number is not expected to be saved.');
            
            
            // ACT: add approval matrix step with negative step number
            amStep1 = new SQX_Approval_Matrix_Step__c(
                SQX_Approval_Matrix__c = am1.Id,
                SQX_Job_Function__c = jf1.Id,
                Step__c = -10
            );
            result1 = Database.insert(amStep1, false);
            
            System.assert(result1.isSuccess() == false,
                'Approval matrix step with negative step number is not expected to be saved.');
            
            
            // add required approval matrix step with valid step number
            amStep1 = new SQX_Approval_Matrix_Step__c(
                SQX_Approval_Matrix__c = am1.Id,
                SQX_Job_Function__c = jf1.Id,
                Step__c = 1
            );
            result1 = Database.insert(amStep1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix step is expected to be saved.');
            
            
            // ACT (SQX-8211): update SQX_Approval_Matrix_Step__c
            update amStep1;
            
            // ASSERT (SQX-8211): ensure long text field tracking for PHR, SQX_Approval_Matrix_Step__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Approval_Matrix_Step__c.SObjectType);
            
            // ACT: update approval matrix step with zero step number
            amStep1.Step__c = 0;
            result1 = Database.update(amStep1, false);
            
            System.assert(result1.isSuccess() == false,
                'Approval matrix step with zero step number is not expected to be saved.');
            
            
            // ACT: update approval matrix step with negative step number
            amStep1.Step__c = -5;
            result1 = Database.update(amStep1, false);
            
            System.assert(result1.isSuccess() == false,
                'Approval matrix step with negative step number is not expected to be saved.');
        }
    }
}