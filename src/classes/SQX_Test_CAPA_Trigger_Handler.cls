/**
* Unit tests for SQX_CAPA_Trigger_Handler
*
* @author Sagar Shrestha
* @date 2014/5/08
* 
*/
@isTest
public class SQX_Test_CAPA_Trigger_Handler{

    @testSetup
    static void setup() {

        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }

    /**
    *   Setup: None
    *   Action: Create A CAPA without Approver
    *   Expected: Approver is set to Owner
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void newCAPACreated_DefaultApproverIsOwner(){
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();
    
            //insert CAPA;
            Database.SaveResult result1 = Database.insert(capa.CAPA,false);
    
            //retrive values of from saved CAPA
            capa.CAPA= [SELECT OwnerId, SQX_Action_Approver__c from SQX_CAPA__c where Id = : capa.CAPA.Id] ;
    
            //assert that default approver is the owner
            System.assert(capa.CAPA.SQX_Action_Approver__c == capa.CAPA.OwnerId,
                 'Expected CAPA to assign default approver equals to Owner '+capa.CAPA);
        }
    }

    /**
    *   Setup: Create another user X
    *   Action: Create A CAPA with Approver set as X
    *   Expected: Approver is still X
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void newCAPACreated_ApproverIsSetAsSetUser(){
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){
        
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility()
                                                .save();
    
            //retrive values of from saved CAPA
            capa.CAPA= [SELECT OwnerId, SQX_Action_Approver__c from SQX_CAPA__c where Id = : capa.CAPA.Id] ;
    
            //assert that default approver is the owner
            System.assert(capa.CAPA.SQX_Action_Approver__c == newUser.Id,
                 'Expected CAPA to assign default approver equals user assigned '+capa.CAPA +'UserID is '+newUser.Id+ ' User is '+ newUser);
        }
    }


    /**
    *   Setup:  Create CAPA X in draft With Finding Y in closed state
    *   Action: Open(Publish) CAPAX
    *   Expected: Finding Y is  still closed
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void PublishCAPA_ClosedFindingStatusNotChanged() {
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){
        
            //setup
            SQX_Test_Finding primaryFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            //create CAPA
            SQX_CAPA__c CAPA= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding.supplier.Id,
                SQX_Finding__c=primaryFinding.finding.Id,
                SQX_External_Contact__C = primaryFinding.primaryContact.Id
                );
    
            //insert CAPA;
            Database.SaveResult result = Database.insert(CAPA,false);
    
            System.assert(result.isSuccess() == true,
                 'Expected the CAPA to be saved ' + result);
    
            //close the finding
            primaryFinding.finding.Status__c=SQX_Finding.STATUS_CLOSED;
            primaryFinding.setClosureComments('Now Closed');
            primaryFinding.save();
    
            //publish CAPA
            CAPA.Status__c=SQX_CAPA.STATUS_OPEN;
            update CAPA;
    
            //retrive values of finding from database
            SQX_Finding__c finding = [SELECT Status__c from SQX_Finding__c where Id = : primaryFinding.finding.Id] ;
    
            System.assert(finding.Status__c == SQX_Finding.STATUS_CLOSED,
                 'Expected Finding Status to be ' + SQX_Finding.STATUS_CLOSED + ' ' + finding);
        }    
    }

    /**
    *   Setup:  Create CAPA X open  With Finding Y open intially, then Close Finding Y
    *   Action: Close CAPAX with rating, closure comment
    *   Expected: Finding Y is  closed with old rating
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void CreateCAPA_CloseFinding_CloseCAPA_RatingAndClosureCommentsNotCopiedToFinding(){
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){
        
            //setup
            SQX_Test_Finding primaryFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(false,false,false,false,false) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            //create CAPA
            SQX_CAPA__c CAPA= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding.supplier.Id,
                SQX_Finding__c=primaryFinding.finding.Id,
                SQX_External_Contact__C = primaryFinding.primaryContact.Id,
                Needs_Effectiveness_Review__c= false
                );
    
            //insert CAPA;
            Database.SaveResult result = Database.insert(CAPA,false);
    
            System.assert(result.isSuccess() == true,
                 'Expected the CAPA to be saved ' + result);
    
            //publish CAPA
            CAPA.Status__c=SQX_CAPA.STATUS_COMPLETE;
            update CAPA;
    
            string findingCLosureComment='Finding Closed';
            Integer Rating= 3;
    
            //close the finding
            primaryFinding.finding.Status__c=SQX_Finding.STATUS_CLOSED;
            primaryFinding.setClosureComments(findingCLosureComment);
            primaryFinding.finding.Rating__c=Rating;
            primaryFinding.save();
    
            //close CAPA with rating and closure comments
            CAPA.Status__c=SQX_CAPA.STATUS_CLOSED;
            CAPA.Closure_Comment__c='Closed CAPA';
            CAPA.Rating__c=5;
            update CAPA;
    
            primaryFinding.synchronize();
    
            System.assert(primaryFinding.finding.Closure_Comments__c == findingCLosureComment,
                 'Expected Closure Comment not to be copied from CAPA. Finding is '+primaryFinding.finding+' CAPA is '+CAPA);
    
            System.assert(primaryFinding.finding.Rating__c == Rating,
                 'Expected Rating to be copied not to finding from CAPA. Finding is '+primaryFinding.finding+' CAPA is '+CAPA);
        }
    }

    /**
    *   Setup:  Create a open CAPA X with effectiveness review required
    *   Action: Complete CAPA X 
    *   Expected: An Effectiveness review child is added for CAPA X
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void CreateCAPA_ERRequired_ERChildCreated() {
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){
            //setup
            SQX_Test_Finding primaryFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            //create CAPA
            SQX_CAPA__c CAPA= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding.supplier.Id,
                SQX_Finding__c=primaryFinding.finding.Id,
                Needs_Effectiveness_Review__c=true,
                SQX_External_Contact__C = primaryFinding.primaryContact.Id
                );
    
            //insert CAPA;
            Database.SaveResult result = Database.insert(CAPA,false);
    
            System.assert(result.isSuccess() == true,
                 'Expected the CAPA to be saved ' + result);
    
            //publish CAPA
            CAPA.Status__c=SQX_CAPA.STATUS_OPEN;
            update CAPA;
    
            //Complete CAPA with rating and closure comments
            CAPA.Status__c=SQX_CAPA.STATUS_COMPLETE;
            update CAPA;
    
            Integer effectivenessReviewClildCount= [SELECT count() from SQX_Effectiveness_Review__c where SQX_CAPA__c = : CAPA.Id] ;
    
            System.assert(effectivenessReviewClildCount == 1,
                 'Expected Effectiveness Review to be added ' + effectivenessReviewClildCount +' '+ CAPA);
         }

    }

    /**
    *   Setup:  Create a open CAPA X with effectiveness review not required
    *   Action: Complete CAPA X 
    *   Expected: Effectiveness review is not added for CAPA X
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    * 
    */
    public static testmethod void CreateCAPA_ERNotRequired_ERChildNotCreated() {
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runas(newUser){
            //setup
            SQX_Test_Finding primaryFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            //create CAPA
            SQX_CAPA__c CAPA= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding.supplier.Id,
                SQX_Finding__c=primaryFinding.finding.Id,
                Needs_Effectiveness_Review__c=false,
                SQX_External_Contact__C = primaryFinding.primaryContact.Id
                );
    
            //insert CAPA;
            Database.SaveResult result = Database.insert(CAPA,false);
    
            System.assert(result.isSuccess() == true,
                 'Expected the CAPA to be saved ' + result);
    
            //publish CAPA
            CAPA.Status__c=SQX_CAPA.STATUS_OPEN;
            update CAPA;
    
            //Complete CAPA with rating and closure comments
            CAPA.Status__c=SQX_CAPA.STATUS_COMPLETE;
            update CAPA;
    
            Integer effectivenessReviewClildCount= [SELECT count() from SQX_Effectiveness_Review__c where SQX_CAPA__c = : CAPA.Id] ;
    
            System.assert(effectivenessReviewClildCount == 0,
                 'Expected Effectiveness Review to be added ' + effectivenessReviewClildCount +' '+ CAPA);
        }
    }

    /**
    *   Given: A CAPA is created with partner enabled user
    *   When:  External contact is partner user and capa is saved
    *   Then:  CAPA will have partnerenabled flag as true
    *
    *   Given: A CAPA is created with partner not enabled user
    *   When:  External contact is non partner user and capa is saved
    *   Then:  CAPA will have partnerenabled flag as false
    *   @date: 6/17/2016
    *   @story: [SQX-1307]
    *   @author: Manoj Thapa
    */
    public static testmethod void createCAPA_ExternalContactNonPartnerUser_IsNotPartnerEnabled() {
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
    
        System.runas(newUser){
            
            SQX_Test_Finding primaryFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();               
                
            //Arrange1: A CAPA is created with partner enabled user
            SQX_CAPA__c CAPA = new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding.supplier.Id,
                SQX_Finding__c=primaryFinding.finding.Id,
                SQX_External_Contact__C = primaryFinding.primaryContact.Id
                );

            insert CAPA;
            //Act1: Partner enabled CAPAs are fetched
            SQX_CAPA__c updatedCAPA = [Select Partner_enabled__c FROM SQX_CAPA__c WHERE Id = :CAPA.Id];

            //Assert1: Expected to be partner enabled
            System.assertEquals(true, updatedCAPA.Partner_enabled__c, 'Expected to be partner enabled');

            SQX_Test_Finding primaryFinding1 = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();

                   
            Contact primaryContact = SQX_Test_Account_Factory.createContact(primaryFinding1.supplier);                    
            //Arrange2: A CAPA is created with partner not enabled
            SQX_CAPA__c CAPA1 = new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =primaryFinding1.supplier.Id,
                SQX_Finding__c=primaryFinding1.finding.Id,
                SQX_External_Contact__C = primaryContact.Id
                );

            insert CAPA1;
            //ACT2: Partner not enabled CAPAs are fetched
            SQX_CAPA__c updatedCAPA1 = [Select Partner_enabled__c FROM SQX_CAPA__c WHERE Id = :CAPA1.Id];

            //Assert1:Expected to be partner not enabled
            System.assertEquals(false, updatedCAPA1.Partner_enabled__c, 'Expected to be partner not enabled');
        }
    }

    /**
     * Given a capa with open implementation
     * When capa is opened
     * Then it waits for implementation and doesn't completed automatically.
     * @story SQX-3721
     */
    public static testmethod void GivenCAPAWithOpenImplementation_WhenImplementationIsOpen_CAPAIsOpen() {
        User newUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(newUser) {
            // Arrange: Insert capa with nothing required, but has one open action
            SQX_CAPA__c capa = new SQX_CAPA__c(
                CAPA_Type__c = SQX_CAPA.TYPE_CUSTOMER,
                Issued_Date__c = Date.today(),
                Target_Due_Date__c = Date.today(),
                SQX_CAPA_Coordinator__c = newUser.Id,
                Title__c = 'asdasd',
                Containment_Required__c = false,
                Response_Required__c = false,
                Investigation_Required__c = false,
                Investigation_Approval__c = false,
                Corrective_Action_Required__c = false,
                Preventive_Action_Required__c = false,
                Done_Responding__c = true
            );

            SQX_DB db = new SQX_DB();
            db.op_insert(new SObject[] { capa }, new SObjectField[] {});

            SQX_Action__c act1 = new SQX_Action__c(SQX_CAPA__c = capa.Id, Status__c = SQX_Implementation_Action.STATUS_OPEN);
            db.op_insert(new SObject[] { act1 }, new SObjectField[] {});

            // Act: Open the capa
            db.op_update(new SObject[] { new SQX_CAPA__c(Id = capa.Id, Status__c = SQX_CAPA.STATUS_OPEN) }, new SObjectField[] {});

            // Assert: Ensure that the CAPA is still open
            System.assertEquals(SQX_CAPA.STATUS_OPEN, [SELECT Status__c FROM SQX_CAPA__c WHERE Id = :capa.Id].Status__c, 'Expected CAPA to be open');

            // Act: Complete(Skip) the action and update the capa (no changes similar to how response would trigger)
            act1.Status__c = SQX_Implementation_Action.STATUS_SKIPPED;
            db.op_update(new SObject[] { act1 }, new SObjectField[] {});

            db.op_update(new SObject[] { new SQX_CAPA__c(Id = capa.Id, Status__c = SQX_CAPA.STATUS_OPEN) }, new SObjectField[]{});

            // Assert: Ensure that CAPA is now complete
            System.assertEquals(SQX_CAPA.STATUS_COMPLETE, [SELECT Status__c FROM SQX_CAPA__c WHERE Id = :capa.Id].Status__c, 'Expected CAPA to be complete');
        }
    }
}