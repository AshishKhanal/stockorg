/**
 * test class for the complaint investigation
 * @author: Anish Shrestha
 * @date: 2015-12-24
 * @story: [SQX-1758]
 */
@isTest
 public class SQX_Test_1758_Complaint_Investigation{
//     static final Id complaintInvestigationRecordTypeId = Schema.SObjectType.SQX_Investigation__c.getRecordTypeInfosByName().get('Complaint Investigation').getRecordTypeId();
       final static String STD_USER_1 = 'Std user 1',
                           ADMIN_USER_1 = 'Admin user 1';
    
//     final static String ERROR_MESSAGE = 'Associated Item selected for Investigation does not belong to selected Complaint.';
    
     @testsetup
     static void setupData() {

         //Arrange: Create a complaint and initiate it
         UserRole mockRole = SQX_Test_Account_Factory.createRole();  
         User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_1);
         User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, ADMIN_USER_1);

         //System.runas(standardUser) {
         //    SQX_Test_Complaint complaint = new SQX_Test_Complaint(null, adminUser);
         //    complaint.save();

         //    Map<String, String> paramsInitiate = new Map<String, String>();
         //    paramsInitiate.put('nextAction', 'initiate');
         //    Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            
         //    SQX_Test_Part.insertPart(null, adminUser, true, '');
         //}
     }

//     /**
//      *   Action: Create a complaint and initaite it and Complete the investigation.
//      *   Expected: When complaint investigation is completed, completed date should be set to date investigation was completed
//      * @author: Anish Shrestha
//      * @date: 2015-12-24
//      * @story: [SQX-1758]
//      */
//     public testmethod static void givenAUser_WhenComplaintInvestigationIsCompleted(){

//         Map<String, User> users = SQX_Test_Account_Factory.getUsers();
//         User standardUser = users.get(STD_USER_1);
//         User adminUser = users.get(ADMIN_USER_1);

//         System.runas(standardUser){
//             SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c];

//             //Assert 1: Expected one investigation to be created
//             SQX_Investigation__c investigation = [SELECT Id, Status__c FROM SQX_Investigation__c WHERE SQX_Complaint__c =: complaintMain.Id];
//             System.assert(investigation.Id != null, 'Expected one investigation to be created when the complaint is initiated');
//             System.assertEquals(SQX_Investigation.STATUS_DRAFT, investigation.Status__c,'Expected the status to be in draft but found: ' + investigation.Status__c);

//             //Act 2: Complete the investigation
//             investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;

//             new SQX_DB().op_update(new List<SQX_Investigation__c>{investigation}, new List<SObjectField>{ SQX_Investigation__c.fields.Status__c });

//             //Assert 2: Expected the completed date is set to date investigation was completed
//             investigation = [SELECT Id, Status__c, Completed_On__c FROM SQX_Investigation__c WHERE Id = : investigation.Id];

//             System.assertEquals(SQX_Investigation.STATUS_PUBLISHED, investigation.Status__c, 'Expected the status to be completed but found: ' + investigation.Status__c);
//             System.assertEquals(Date.Today(), investigation.Completed_On__c, 'Expected the investigation completed date to be: ' + Date.Today() + ' but found: ' + investigation.Completed_On__c);
//         }
//     }
//     /**
//      *   Action: Create a complaint and intiate it and complete Investigation
//      *   Expected: After initiating complaint one Investigation is created,
//      *              To Complete the Investigation Investigation summary must be provided
//      * @author: Anish Shrestha
//      * @date: 2016-01-08
//      * @story: [SQX-915]
//      */
//     public testmethod static void givenAComplaintWithInvestigation_InvestigationSummaryMustBeProvidedToCompleteInvestigation(){

//         /**
//          *Arrange: Create the complaint and  
//          *          initiate the complaint
//         */
//         Map<String, User> users = SQX_Test_Account_Factory.getUsers();
//         User standardUser = users.get(STD_USER_1);
//         User adminUser = users.get(ADMIN_USER_1);

//         System.runas(standardUser){

//             SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c];

//             // Act: Initiate the complaint and complete the Investigation

//             SQX_Investigation__c investigation = new SQX_Investigation__c();
//             investigation = [SELECT Id, Investigation_Summary__c, Status__c FROM SQX_Investigation__c WHERE SQX_Complaint__c =: complaintMain.Id];

//             // Assert 1: Expected one investigation to be created after initiating the complaint
//             System.assert(investigation.Id != null, 'Expected one investigation to be created after initiating the complaint');

//             // Assert 2: Expected error when complaint is completed without providing product history review summary
//             try{

//                 new SQX_DB().op_update(new List<SQX_Investigation__c>{investigation}, new List<Schema.SObjectField>{
//                         Schema.SQX_Investigation__c.Status__c,
//                         Schema.SQX_Investigation__c.Investigation_Summary__c
//                     });
//             }
//             catch(Exception e){
//                 Boolean expectedExceptionThrown =  e.getMessage().contains('Investigation Summary must be provided to complete the Investigation.') ? true : false;
//                 system.assertEquals(expectedExceptionThrown,true);       
//             } 

//             // Assert 3: Expected error to be thrown when investigation is completed without anwering all investigation checklist
//             investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
//             investigation.Investigation_Summary__c = 'Random Summary';

//             // Assert 4: Expected investigation to be completed after product history summary is provided
//             new SQX_DB().op_update(new List<SQX_Investigation__c>{investigation}, new List<Schema.SObjectField>{
//                     Schema.SQX_Investigation__c.Status__c,
//                     Schema.SQX_Investigation__c.Investigation_Summary__c

//                 });

//             investigation = [SELECT Id, Status__c FROM SQX_Investigation__c WHERE Id =: investigation.Id];

//             System.assertEquals(SQX_Investigation.STATUS_PUBLISHED, investigation.Status__c, 'Expected the Investigatiion to be complete but found: ' + investigation.Status__c);
//         }
//     }
    
//     /**
//      * @author : Sanjay Maharjan
//      * @date : 2019-02-12
//      * @story : [SQX-7946]
//      * GIVEN : Complaints with respective associated items
//      * WHEN : When investigation is created with complaint and associated item
//      * THEN : Error should not be thrown
//      */
//     public testmethod static void givenComplaintsWithRespectiveAssociatedItems_WhenInvestigationIsCreated_ThenErrorShouldNotBeThrown(){
        
//         Map<String, User> users = SQX_Test_Account_Factory.getUsers();
//         User standardUser = users.get(STD_USER_1);
//         User adminUser = users.get(ADMIN_USER_1);
        
//         System.runAs(standardUser) {
            
//             // ARRANGE : Create part
//             SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
//             // ARRANGE : Create two complaints
//             SQX_Test_Complaint complaint1 = new SQX_Test_Complaint(adminUser).save();
//             SQX_Test_Complaint complaint2 = new SQX_Test_Complaint(adminUser).save();
            
//             // ARRANGE : Create two associated items 
//             SQX_Complaint_Associated_Item__c newAssociatedItem1 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint1.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id, Make_Primary__c = true);
//             SQX_Complaint_Associated_Item__c newAssociatedItem2 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint2.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id, Make_Primary__c = true);
//             new SQX_DB().op_insert(new List<SQX_Complaint_Associated_Item__c> {newAssociatedItem1, newAssociatedItem2}, new List<Schema.SObjectField>());
            
//             SQX_Investigation__c investigation = new SQX_Investigation__c();
//             investigation.SQX_Complaint__c = complaint1.complaint.Id;
//             investigation.SQX_Associated_Item__c = newAssociatedItem2.Id;
            
//             // ACT : Insert investigation
//            	Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Investigation__c> {investigation}, new List<Schema.SObjectField>())[0];
            
//             // ASSERT : Investigation should not be inserted and appropriate error message is thrown
//             System.assertEquals(false, result.isSuccess(), 'Investigation should not be inserted.');
//             System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message :' + ERROR_MESSAGE + ' should be thrown but got ' + result.getErrors());
            
//             investigation.SQX_Complaint__c = complaint1.complaint.Id;
//             investigation.SQX_Associated_Item__c = newAssociatedItem1.Id;
            
//             // ACT : Insert investigation
//             result = new SQX_DB().continueOnError().op_insert(new List<SQX_Investigation__c> {investigation}, new List<Schema.SObjectField>())[0];
            
//             // ASSERT : Investigation should be inserted.
//             System.assertEquals(true, result.isSuccess(), 'Investigation should be inserted.');
//         }
//     }

	/**
     * @date : 3/21/2019
     * @author : Paras BK
     * @story : SQX-7901
     *
     * Given : Create Completed Investigation
     * When : Try to void
	 * Then : Error occurs
	 *
	 * Given : Voided Investigation
	 * When : Try to modify record
	 * Then : Error occurs
     */
    public testmethod static void givenCompletedAndVoidInvestigation_WhenTryToModify_ErrorOccurs(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get(STD_USER_1);

        System.runas(standardUser){
        	
        	// Arrange : Create Completed Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            investigation.Investigation_Summary__c = 'Complete Investigation';
            insert investigation;

            // Act : Try to void completed Investigation
            investigation.Status__c = 'Void';
            Database.SaveResult result = Database.update(investigation, false);

            // Assert : Ensure completed Investigation cannot be voided.
            System.assert(result.isSuccess() == false, 'Completed Investigation cannot be void but is being voided.');
            final String errMsg = 'Record Status does not support action performed.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), errMsg), 'Expected ' + errMsg + 'but got ' + result.getErrors());

            // Arrange : Create and Void Investigation
            investigation = new SQX_Investigation__c();
            investigation.Investigation_Summary__c = 'Voiding Investigation';
            insert investigation;
            
            // void investigation
            investigation.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation;

            // Act : Try to modify void Investigation
            investigation.Investigation_Summary__c = 'Modifying Summary';
            result = Database.update(investigation, false);

            // Assert : Ensure completed Investigation cannot be voided.
            System.assert(result.isSuccess() == false, 'Completed Investigation cannot be void but is being voided.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), errMsg), 'Expected ' + errMsg + 'but got ' + result.getErrors());
        }
    }
 }