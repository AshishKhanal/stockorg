/**
* unit tests for batch jobs to process trainings and related PDJFS of activated or deactivated document requirements
*/
@isTest
public class SQX_Test_3474_RequirementProcessingBatch {
    
    static Boolean runAllTests = true,
        run_givenDocumentReleased_ActivatedRequriementsWithTrainingJobPending_BatchProcessesTrainingsSuccessfully = false,
        run_givenDeactivatedRequriementsWithPendingTraining_BatchProcessesTrainingsSuccessfully = false,
        run_givenActivatedRequriementUsingBatchJob_PjfsMoreThanMaxLimit_TrainingsGeneratedCompletelyBySuccessiveJobs = false;
    
    static String TEST_PSN_PREFIX = 'Test-3474-Psn-';
    static String TEST_JF_NAME = 'Test-3474-1';
    static String TEST_DOC_NUM = 'Test-3474-Doc-1';
    static Integer TOTAL_PSNS = 100;
    
    /**
    * creates provided number of non-user personnel objects with required fields using prefix in full names and identification
    */
    public static List<SQX_Personnel__c> createPersonnels(String prefix, Integer total) {
        List<SQX_Personnel__c> psns = new List<SQX_Personnel__c>();
        
        for (Integer i = 0; i < total; i++) {
            psns.add(new SQX_Personnel__c(
                Full_Name__c = prefix + i,
                Identification_Number__c = prefix + i
            ));
        }
        
        return psns;
    }
    
    /**
    * creates active personnel job functions objects for provided personnels and job function
    */
    public static List<SQX_Personnel_Job_Function__c> createActivePersonnelJobFunctions(List<SQX_Personnel__c> psns, SQX_Job_Function__c jf) {
        List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
        
        for (SQX_Personnel__c psn : psns) {
            pjfs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = psn.Id,
                SQX_Job_Function__c = jf.Id,
                Active__c = true
            ));
        }
        
        return pjfs;
    }
    
    /**
    * creates draft controlled document without content using provided document number 
    */
    public static SQX_Controlled_Document__c createDraftDocument(String docNum) {
        SQX_Controlled_Document__c doc = new SQX_Test_Controlled_Document().doc;
        doc.Document_Number__c = docNum;
        
        return doc;
    }
    
    /**
    * queries DB and returns requirement record with field values required in batch job processor
    */
    public static SQX_Requirement__c getRequirement(Id reqId) {
        return [SELECT Id,
                    Active__c,
                    SQX_Controlled_Document__c,
                    SQX_Job_Function__c,
                    SQX_Controlled_Document__r.Document_Number__c,
                    Skip_Revision_Training__c,
                    SQX_Initial_Assessment__c,
                    SQX_Revision_Assessment__c,
                    SQX_Controlled_Document__r.SQX_Initial_Assessment__c,
                    SQX_Controlled_Document__r.SQX_Revision_Assessment__c,
                    Training_Job_Last_Processed_Record__c,
                    Training_Job_Status__c,
                    Training_Job_Error__c,
                    SQX_Controlled_Document__r.Pending_Document_Training__c,
                    Training_Program_Step_Internal__c
                FROM SQX_Requirement__c
                WHERE Id = :reqId];
    }
    
    /**
    * enables/disables use batch job field of cq custom setting
    */
    public static void setTrainingBatchJobCustomSetting(Boolean useBatchJob) {
        SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
            Schema.SQX_Custom_Settings_Public__c.Use_Batch_Job_To_Process_REQ_Training__c => useBatchJob
        });
    }
    
    @testSetup
    static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');
        
        SQX_Job_Function__c jf = new SQX_Job_Function__c( Name = TEST_JF_NAME );
        SQX_Test_Job_Function jft = new SQX_Test_Job_Function(jf);
        
        System.runas(admin1) {
            // add required job functions
            jft.save();
        }
        
        System.runas(user1) {
            // add required personnels
            List<SQX_Personnel__c> psns = createPersonnels(TEST_PSN_PREFIX, TOTAL_PSNS);
            insert psns;
            
            // add required active pjfs
            insert createActivePersonnelJobFunctions(psns, jf);
        }
        
        System.runas(user2) {
            // add required draft document
            SQX_Controlled_Document__c doc = createDraftDocument(TEST_DOC_NUM);
            insert doc;
            
            // add required doc requirement
            insert new SQX_Requirement__c(
                SQX_Controlled_Document__c = doc.Id,
                SQX_Job_Function__c = jf.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Active__c = false
            );
        }
    }
    
    /**
    * Given:
    *   Personnels with active personnel job functions are added
    *   Draft document with requirement is added
    *   Batch job custom setting to process trainings is enabled
    * When:
    *   Document is released
    * Then:
    *   Requirement is activated with batch job status field value activation pending
    *   No trainings are generated for activated job function
    *   Pending training count in controlled document is zero
    * When:
    *   Batch job for activated requirement is executed to process all PJFs
    * Then:
    *   Batch job status field of the requirement is set to blank
    *   All PDJFs and trainings are generated for the job function
    *   Pending training count in controlled document has proper value
    */
    static testmethod void givenDocumentReleased_ActivatedRequriementsWithTrainingJobPending_BatchProcessesTrainingsSuccessfully() {
        if (!runAllTests && !run_givenDocumentReleased_ActivatedRequriementsWithTrainingJobPending_BatchProcessesTrainingsSuccessfully)
            return;
        
        // get users
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        
        List<SQX_Requirement__c> reqs;
        
        System.runas(users.get('admin1')) {
            // enable use batch job custom setting to process training
            setTrainingBatchJobCustomSetting(true);
        }
        
        System.runas(users.get('user2')) {
            // get draft document
            List<SQX_Controlled_Document__c> docs = [SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c WHERE Document_Number__c = :TEST_DOC_NUM];
            
            // ACT: release document
            docs[0].Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            new SQX_DB().op_update(docs, new List<Schema.SObjectField>{ });
            
            // get document requirements
            reqs = [SELECT Id, Active__c, Training_Job_Status__c, Training_Job_Last_Processed_Record__c, SQX_Controlled_Document__r.Pending_Document_Training__c
                    FROM SQX_Requirement__c
                    WHERE SQX_Controlled_Document__c IN :docs ];
            
            System.assertEquals(1, reqs.size());
            System.assertEquals(true, reqs[0].Active__c,
                'Requirement is expected to be active when document is released');
            System.assertEquals(SQX_Requirement.TRAINING_JOB_STATUS_ACTIVATION_PENDING, reqs[0].Training_Job_Status__c,
                'Training job status is expected to be pending when using batch job for activated requirements');
            System.assertEquals(null, reqs[0].Training_Job_Last_Processed_Record__c);
            System.assertEquals(0, reqs[0].SQX_Controlled_Document__r.Pending_Document_Training__c,
                'No trainings are expected to be generated');
            
            Test.startTest();
            
            // ACT: Start batch process for activated requirements
            SQX_Activated_Requirement_Processor processor = new SQX_Activated_Requirement_Processor();
            Database.executeBatch(processor);
            
            Test.stopTest();
        }
        
        System.runas(users.get('user1')) {
            // get document requirements with training information
            reqs = [SELECT Id, Active__c, Training_Job_Status__c, Training_Job_Last_Processed_Record__c, SQX_Controlled_Document__r.Pending_Document_Training__c,
                        ( SELECT Id FROM SQX_Personnel_Document_Job_Functions__r WHERE Is_Archived__c = false AND SQX_Personnel_Document_Training__c != null )
                    FROM SQX_Requirement__c
                    WHERE Id IN :reqs ];
            
            SQX_Personnel_Job_Function__c lastPjf =[SELECT Id
                                                    FROM SQX_Personnel_Job_Function__c
                                                    WHERE Personnel_Name__c LIKE :(TEST_PSN_PREFIX + '%')
                                                        AND SQX_Job_Function__r.Name = :TEST_JF_NAME
                                                    ORDER BY Id DESC
                                                    LIMIT 1];
            
            System.assertEquals(null, reqs[0].Training_Job_Status__c,
                'Trainings are expected to be processed successfully by batch job');
            System.assertEquals(lastPjf.Id, reqs[0].Training_Job_Last_Processed_Record__c,
                'Last processed record is expected to have last active PJF Id');
            System.assertEquals(TOTAL_PSNS, reqs[0].SQX_Personnel_Document_Job_Functions__r.size(),
                'All PDJFs are expected to be generated for the job function');
            System.assertEquals(SQX_Controlled_Document.MAX_PENDING_TRAINING_COUNT, reqs[0].SQX_Controlled_Document__r.Pending_Document_Training__c,
                'All trainings are expected to be generated for the job function');
        }
    }
    
    /**
    * Given:
    *   Personnels with active personnel job functions are added
    *   Document with requirement is added and released
    *   PDJFs and pending trainings are properly generated
    *   Batch job custom setting to process trainings is enabled
    * When:
    *   Requirement is deactivated
    * Then:
    *   Batch job status field of the requirement is set to deactivation pending
    *   Pending trainings are not manipulated
    * When:
    *   Batch job for deactivated requirement is executed to process all PDJFs
    * Then:
    *   Batch job status field of the requirement is set to blank
    *   All PDJFs and pending trainings are archived
    *   Pending training count in controlled document is zero
    */
    static testmethod void givenDeactivatedRequriementsWithPendingTraining_BatchProcessesTrainingsSuccessfully() {
        if (!runAllTests && !run_givenDeactivatedRequriementsWithPendingTraining_BatchProcessesTrainingsSuccessfully)
            return;
        
        // get users
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        
        List<SQX_Requirement__c> reqs;
        
        System.runas(users.get('admin1')) {
            // get draft document
            List<SQX_Controlled_Document__c> docs = [SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c WHERE Document_Number__c = :TEST_DOC_NUM];
            
            // ACT: release document
            docs[0].Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            new SQX_DB().op_update(docs, new List<Schema.SObjectField>{ });
            
            // get document requirements
            reqs = [SELECT Id, Active__c, Training_Job_Status__c, Training_Job_Last_Processed_Record__c
                    FROM SQX_Requirement__c
                    WHERE SQX_Controlled_Document__c IN :docs ];
            
            System.assertEquals(1, reqs.size());
            System.assertEquals(true, reqs[0].Active__c,
                'Requirement is expected to be active when document is released');
            System.assertEquals(null, reqs[0].Training_Job_Status__c,
                'Expected training batch job status to be blank when batch job is not enabled');
            System.assertEquals(TOTAL_PSNS, [SELECT COUNT() FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c IN :docs AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING],
                'Trainings are expected to be generated');
            
            // enable use batch job custom setting to process training
            setTrainingBatchJobCustomSetting(true);
            SQX_Requirement.cqSetting = null; // clear already cq setting from memory
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // deactivate requirement
            reqs[0].Active__c = false;
            new SQX_DB().op_update(reqs, new List<Schema.SObjectField>{ });
            
            // get document requirements with training information
            reqs = [SELECT Id, Active__c, Training_Job_Status__c, Training_Job_Last_Processed_Record__c
                    FROM SQX_Requirement__c
                    WHERE Id IN :reqs ];
            
            System.assertEquals(SQX_Requirement.TRAINING_JOB_STATUS_DEACTIVATION_PENDING, reqs[0].Training_Job_Status__c,
                'Training job status is expected to be pending when using batch job for deactivated requirements');
            System.assertEquals(TOTAL_PSNS, [SELECT COUNT() FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c IN :docs AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING],
                'Pending trainings are not expected to be processed');
            
            Test.startTest();
            
            // ACT: Start batch process for deactivated requirement
            SQX_Deactivated_Requirement_Processor processor = new SQX_Deactivated_Requirement_Processor();
            Database.executeBatch(processor);
            
            Test.stopTest();
            
            // get document requirements with training information
            reqs = [SELECT Id, Active__c, Training_Job_Status__c, Training_Job_Last_Processed_Record__c, SQX_Controlled_Document__r.Pending_Document_Training__c,
                        ( SELECT Id FROM SQX_Personnel_Document_Job_Functions__r WHERE Is_Archived__c = false )
                    FROM SQX_Requirement__c
                    WHERE Id IN :reqs ];
            
            SQX_Personnel_Job_Function__c lastPjf =[SELECT Id
                                                    FROM SQX_Personnel_Job_Function__c
                                                    WHERE Personnel_Name__c LIKE :(TEST_PSN_PREFIX + '%')
                                                        AND SQX_Job_Function__r.Name = :TEST_JF_NAME
                                                    ORDER BY Id DESC
                                                    LIMIT 1];
            
            System.assertEquals(null, reqs[0].Training_Job_Status__c,
                'Trainings are expected to be processed successfully by batch job');
            System.assertEquals(0, reqs[0].SQX_Controlled_Document__r.Pending_Document_Training__c,
                'Trainings count is expected to be zero');
            System.assertEquals(0, reqs[0].SQX_Personnel_Document_Job_Functions__r.size(),
                'All PDJFs are expected to be archived');
        }
    }
    
    /**
    * Given:
    *   Personnels with active personnel job functions are added
    *   Draft document with requirement is added
    *   Batch job custom setting to process trainings is enabled
    *   Max record limit to process by batch job for activated requirement is set
    *   2 additional personnels are added
    *   Max limit to process for training generation is set lower than total personnels
    *   Active personnel job function is added for the 2nd additional personnel
    * When:
    *   Document is released
    * Then:
    *   Requirement is activated with batch job status field value activation pending
    *   No trainings are generated for activated job function
    *   Pending training count in controlled document is zero
    * When:
    *   Batch job for activated requirement is executed to process pjfs of max limit
    * Then:
    *   Batch job status field of the requirement is not changed and is activation pending
    *   PDJFs and trainings for max limit are generated for the job function
    *   Pending training count in controlled document has proper value i.e. max limit
    * When:
    *   Active personnel job function is added for the 1st additional personnel
    * Then:
    *   Batch job status field of the requirement is not changed and is activation pending
    *   PDJFs and trainings are generated for the realted personnel
    *   Pending training count in controlled document is updated with proper value
    * When:
    *   Batch job for activated requirement is executed to process all remaining pjfs
    * Then:
    *   Batch job status field of the requirement is set to blank
    *   All PDJFs and trainings for remaining personnels are generated for the job function
    *   Pending training count in controlled document is updated with proper value
    *   No additional PDJFs and trainings are generated for 1st additional personnel and existing records are not modified
    */
    static testmethod void givenActivatedRequriementUsingBatchJob_PjfsMoreThanMaxLimit_TrainingsGeneratedCompletelyBySuccessiveJobs() {
        if (!runAllTests && !run_givenActivatedRequriementUsingBatchJob_PjfsMoreThanMaxLimit_TrainingsGeneratedCompletelyBySuccessiveJobs)
            return;
        
        // get users
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        
        System.runas(users.get('admin1')) {
            SQX_Activated_Requirement_Processor processor = new SQX_Activated_Requirement_Processor();
            // ACT with internal ASSERT: test setMaxPJFsToProcess() with max limit value
            processor.setMaxPJFsToProcess((Limits.getLimitDMLRows() / 2) - 1);
            
            // set max pjfs limit to process in a batch
            processor.setMaxPJFsToProcess(TOTAL_PSNS);
            
            // enable use batch job custom setting to process training
            setTrainingBatchJobCustomSetting(true);
            
            // get job function
            SQX_Job_Function__c jf = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = :TEST_JF_NAME];
            
            // add 2 additional personnels
            List<SQX_Personnel__c> psns = createPersonnels(TEST_PSN_PREFIX + 'Additional-', 2);
            new SQX_DB().op_insert(psns, new List<Schema.SObjectField>{ });
            
            // update total psns count
            TOTAL_PSNS = TOTAL_PSNS + 2;
            
            // add required active pjf for 2nd additional personnel
            new SQX_DB().op_insert(createActivePersonnelJobFunctions(new List<SQX_Personnel__c>{ psns[1] }, jf), new List<Schema.SObjectField>{ });
            
            // get draft document
            List<SQX_Controlled_Document__c> docs =[SELECT Id, Document_Status__c, (SELECT Id FROM SQX_Requirements__r)
                                                    FROM SQX_Controlled_Document__c WHERE Document_Number__c = :TEST_DOC_NUM];
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: release document
            docs[0].Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            new SQX_DB().op_update(docs, new List<Schema.SObjectField>{ });
            
            // get requirement
            SQX_Requirement__c req = getRequirement(docs[0].SQX_Requirements__r[0].Id);
            
            System.assertEquals(SQX_Requirement.TRAINING_JOB_STATUS_ACTIVATION_PENDING, req.Training_Job_Status__c,
                'Training job status is expected to be pending when using batch job for activated requirements');
            System.assertEquals(null, req.Training_Job_Last_Processed_Record__c);
            System.assertEquals(0, req.SQX_Controlled_Document__r.Pending_Document_Training__c,
                'No trainings are expected to be generated');
            
            // ACT: Start batch process for activated requirement
            processor.processBatch(req);
            
            System.assertEquals(TOTAL_PSNS - 2, SQX_Personnel_Document_Job_Function.MAX_RECORDS_TO_PROCESS_BY_TRAINING_BATCH_JOB,
                'Max record limit to process for activated job function is expected to be set');
            
            // get requirement
            req = getRequirement(req.Id);
            
            Integer expectedPendingTrainingCount = SQX_Personnel_Document_Job_Function.MAX_RECORDS_TO_PROCESS_BY_TRAINING_BATCH_JOB;
            
            System.assertEquals(SQX_Requirement.TRAINING_JOB_STATUS_ACTIVATION_PENDING, req.Training_Job_Status__c,
                'Training job status is expected to be pending when there are remaining records to process.');
            System.assertNotEquals(null, req.Training_Job_Last_Processed_Record__c);
            System.assertEquals(expectedPendingTrainingCount,
                [SELECT COUNT() FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c IN :docs AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING],
                'Trainings equal to max limit are expected to be generated');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // add required active pjf for 1st additional personnel while training is generated by batch job
            List<SQX_Personnel_Job_Function__c> pjfs = createActivePersonnelJobFunctions(new List<SQX_Personnel__c>{ psns[0] }, jf);
            new SQX_DB().op_insert(pjfs, new List<Schema.SObjectField>{ });
            
            String lastProcessedId = req.Training_Job_Last_Processed_Record__c;
            expectedPendingTrainingCount = expectedPendingTrainingCount + 1;
            
            // get requirement
            req = getRequirement(req.Id);
            
            System.assertEquals(SQX_Requirement.TRAINING_JOB_STATUS_ACTIVATION_PENDING, req.Training_Job_Status__c,
                'Training job status is expected to be pending when there are remaining records to process.');
            System.assertEquals(lastProcessedId, req.Training_Job_Last_Processed_Record__c);
            System.assertEquals(expectedPendingTrainingCount,
                [SELECT COUNT() FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c IN :docs AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING],
                'Additional training is expected to be generated.');
            
            // get pdjfs and trainings of 1st additional personnel
            List<SQX_Personnel_Document_Job_Function__c> pdjfs = [  SELECT Id, Is_Archived__c, SQX_Personnel_Document_Training__c, Training_Status__c
                                                                    FROM SQX_Personnel_Document_Job_Function__c
                                                                    WHERE SQX_Personnel_Job_Function__c = :pjfs[0].Id
                                                                        AND SQX_Requirement__c = :req.Id
                                                                        AND SQX_Personnel_Document_Training__c != null ];
            
            System.assertEquals(1, pdjfs.size(),
                'PDJF and pending training is expected to be generated');
            
            // ACT: batch process remaining pjf records for same activated requirement
            processor.processBatch(req);
            
            // get requirement
            req = getRequirement(req.Id);
            
            System.assertEquals(null, req.Training_Job_Status__c,
                'Trainings are expected to be processed successfully by batch job');
            System.assertEquals(TOTAL_PSNS,
                [SELECT COUNT() FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c IN :docs AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING],
                'Trainings count is expected to be proper');
            
            // get PDJFs and trainings of 1st additional personnel
            List<SQX_Personnel_Document_Job_Function__c> pdjfs2 = [ SELECT Id, Is_Archived__c, SQX_Personnel_Document_Training__c, Training_Status__c
                                                                    FROM SQX_Personnel_Document_Job_Function__c
                                                                    WHERE SQX_Personnel_Job_Function__c = :pjfs[0].Id
                                                                        AND SQX_Requirement__c = :req.Id ];
            
            System.assertEquals(1, pdjfs2.size(),
                'PDJFs and trainings are not expected to be added for already processed personnel');
            System.assertEquals(pdjfs[0].Id, pdjfs2[0].Id,
                'PDJF record is not expected to be changed');
            System.assertEquals(pdjfs[0].Is_Archived__c, pdjfs2[0].Is_Archived__c,
                'PDJF record is not expected to be changed');
            System.assertEquals(pdjfs[0].SQX_Personnel_Document_Training__c, pdjfs2[0].SQX_Personnel_Document_Training__c,
                'PDJF record is not expected to be changed');
            System.assertEquals(pdjfs[0].Training_Status__c, pdjfs2[0].Training_Status__c,
                'PDJF record is not expected to be changed');
        }
    }
    
}