/**
* Unit tests for Equipment Object change status for SQX-1454
* Unit test for E-Signature validation as per SQX-1468
*
* @author Shailesh Maharjan
* @date 2015/09/16
* 
*/
@IsTest
public class SQX_Test_1454_Equipment_Change_Status {
    static Boolean runAllTests = true,
                   run_givenChangeStatus = false,
                   run_givenChangeStatus_StatusSameAsPrevious_NotSaved = false,
                   run_givenSignOffChangeStatus = false;

    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; // Esignature is not validating password when consumer key is wrong.
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            if(Invalid_Request){
                response.setStatusCode(400);
                response.setBody('Error: 400');
            }
            else{
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();
                if(url.contains('rightPassword')){
                    response.setBody('{"id":"allcorrect"}');
                }
                else{
                    response.setBody('{"error":"incorrect password"}');
                }
            }
            
            return response;
        }
    }
    
    /**
    * Unit tests for Equipment Object change status
    *  As per SQX-1454
    */
    public testmethod static void givenChangeStatus(){
        if (!runAllTests && !run_givenChangeStatus) {
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){
            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c equipment = new SQX_Equipment__c(
                Name = '123',
                Equipment_Description__c = 'This is test equipment',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );

            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert(equipment, false);
            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr.isSuccess(), 'Expecting equipment to be Created');
            
            Id equipmentId = sr.getId();
            ApexPages.StandardController controller = new ApexPages.StandardController(equipment);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);

            //Act: Change Equipment Status to Active
            equipment.Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE;
            extension.recordActivityComment = 'Set Status to Active.';
            extension.changeStatus();

            //Act: retriving updated equipment record
            SQX_Equipment__c updatedEquipment = [SELECT Equipment_Status__c FROM SQX_Equipment__c WHERE Id = :equipmentId];

            //Assert: Assert if updating Equipment Status
            System.assertEquals(SQX_Equipment.STATUS_ACTIVE, updatedEquipment.Equipment_Status__c, 'Expected Status had been Updated');

            //Act: retriving equipment record activity record created by above equipment record
            //select equipment record activity
            SQX_Equipment_Record_Activity__c[] createdEra = [SELECT Id, Comment__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :equipmentId];

            System.assertEquals(1, createdEra.size(), 'Expected number of record Activity created is 1');
            
            String expectedStatusChangeInfo = SQX_Equipment__c.Equipment_Status__c.getDescribe().getLabel() + ' changed from ' + SQX_Equipment.STATUS_NEW + ' to ' + SQX_Equipment.STATUS_ACTIVE;
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo), 'Expected comment of record Activity created to contain status change information.');
            
            System.assert(createdEra[0].Comment__c.endsWith(extension.recordActivityComment), 'Expected comment of record Activity created to end with user comment.');
        }
    }
    
    /**
    * Unit tests for Equipment Object change status
    * @story SQX-1617
    */
    public testmethod static void givenChangeStatus_StatusSameAsPrevious_NotSaved() {
        if (!runAllTests && !run_givenChangeStatus_StatusSameAsPrevious_NotSaved) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser){
            // add new required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'eqp-samestatus',
                Equipment_Description__c = 'eqp-samestatus desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, false);
            
            Test.setCurrentPage(Page.SQX_Equipment_Status_Change);
            ApexPages.StandardController controller = new ApexPages.StandardController(eqp1);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);
            extension.ES_Enabled = false;
            
            // ACT: submit change equipment status without changing
            extension.recordActivityComment = 'Set Status to Active.';
            extension.changeStatus();
            
            // check for error message
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            Boolean errFound = false;
            for (ApexPages.Message msg : msgs) {
                if (msg.getDetail().contains(Label.SQX_ERR_MSG_EQUIPMENT_STATUS_NOT_CHANGED)) {
                    errFound = true;
                    break;
                }
            }
            
            System.assert(errFound, 'Expected error message not found for no changes.');
        }
    }
    
    /**
    * This test ensures User SignOff with E-signature to work as expected.
    *  As per SQX-1468
    */
    public testmethod static void givenSignOffChangeStatus() {
        if (!runAllTests && !run_givenSignOffChangeStatus) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        
        //Creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // User signoff
        System.runas(testUser1) {

            //Arrange: Create new Equipment Management field with all required fields
            SQX_Equipment__c equipment = new SQX_Equipment__c(
                Name = '123',
                Equipment_Description__c = 'This is test equipment',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );

            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert( equipment, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr.isSuccess(),
                'Expecting equipment reocord had been inserted');

            Test.setCurrentPage(Page.SQX_Equipment_Status_Change); //setting SQX_Equipment_Status_Change for Test

            ApexPages.StandardController controller = new ApexPages.StandardController(equipment);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);

            equipment.Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE;
            extension.recordActivityComment = 'Set Status to Active with E-signature.';
            extension.SigningOffUsername = testUser1.username;
            extension.SigningOffPassword = 'rightPassword';
            
            Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
            
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1454_Equipment_Change_Status';
            cqES.Consumer_Secret__c = 'SQX_Test_1454_Equipment_Change_Status';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            // Act
            extension.changeStatus();
            
            SQX_Equipment_Record_Activity__c era = new SQX_Equipment_Record_Activity__c();
            era = [SELECT Id, Comment__c, SQX_Equipment__c, Modified_By__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :equipment.Id];
            
            //Assert: Document Training status Compare 
            System.assert(era.SQX_Equipment__c == equipment.Id,
                'Expected Equipment record had been updated.');
            
            //Assert: Ensuring user signed off by value
            System.assert(era.Modified_By__c == extension.getUserDetails(),
                'Expected user had modified the record.');

            //Assert :ensuring User Signing off comment set
            System.assert(era.Comment__c.endsWith('Set Status to Active with E-signature.'),
                'Expected comment had been added to record history.');
        }
    }
}