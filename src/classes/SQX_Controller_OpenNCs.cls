/**
* This class is used to return all the open NCs for which the user is NC Coordinator.
*/
public with sharing class SQX_Controller_OpenNCs{
    
    /**
    * returns the list of NCs open capa for the currently logged in user's account 
    */
    public List<SQX_Nonconformance__c> getOpenNCs() {
            Set<ID> queue_current_user_IDs= SQX_Utilities.getQueueIdsForUser(UserInfo.getUserId());
            
            queue_current_user_IDs.add(UserInfo.getUserId());
            
            /*
            Nonconformance
            Title
            Department
            Priority
            status
            type
            Age
            Occurance date
            owner
            */

            return [

            SELECT Id, Name, OwnerId, Owner.Name, NC_Title__c, Department__c, Priority__c, Status__c, 
                Type_Of_Issue__c, Age__c, Occurrence_Date__c 
                    FROM SQX_Nonconformance__c
                    WHERE (Status__c =: SQX_NC.STATUS_OPEN OR Status__c=: SQX_NC.STATUS_TRIAGE ) 
                            and OwnerID in :queue_current_user_IDs
                    ORDER BY Name DESC];
        
    }

}