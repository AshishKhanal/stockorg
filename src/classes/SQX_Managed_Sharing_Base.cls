/**
* this class is a helper class that is used to share the given set of records, with an account's role 
*/
public with sharing class SQX_Managed_Sharing_Base{

    static Map<String, String> accountToGroupID = new Map<String,String>(); //prevents duplicate soql being issued if one has been already mapped in same transaction
    public static final String SHARING_ACCESS_LEVEL_EDIT = 'Edit',
                                SHARING_ACCESS_LEVEL_READ = 'Read';
    /**
    * this class shares all records with partner users of account set in the account field
    * any existing rule with the same sharing reason is removed and new ones are inserted.
    *
    * @param recordsToShare specifies the records for which sharing rules are to be added
    * @param accountFieldName specifies the name of the account field in the record
    * @param accessLevel specifies the access level to be used
    * @param sharingReason reason as to why record is being shared.
    */
    public void shareRecordWithPartnerUsers(List<sObject> recordsToShare, String accountFieldName, String accessLevel,
                                            String sharingReason){
        
        if(recordsToShare.size() == 0)
            return;

        //1. get the sharing table(sObject)
                                                
        String sharingSObjectName = 
            recordsToShare.get(0).getSObjectType().getDescribe().getName();

        sharingSObjectName = sharingSObjectName.endsWith('__c') == true ? sharingSObjectName.subString(0, sharingSObjectName.length()-3) : sharingSObjectName;
        sharingSObjectName =  sharingSObjectName + '__share';

        Schema.SObjectType sharingSObjectType =
            Schema.getGlobalDescribe().get(sharingSObjectName);
        
        System.assert(sharingSObjectType != null, 
            'Expected sharing object to be created but found ' + sharingSObjectName + ' missing ' );
        
        
        //2. get all the accounts for which the records are to be shared
        
        //2.i create a list of account to get the group id
        Set<String> accountIDs = new Set<String>();
        for(sObject recordToShare : recordsToShare){
            String accountID = (String)recordToShare.get(accountFieldName);
            if(accountToGroupID.containsKey(accountID)){
                System.debug('In cached result');
                continue;
            }
            accountIDs.add(accountID);
        }
        
        System.debug(accountIDs);
        
        //2.ii. get all the portal roles, because all the records are to be shared for the given role.
        if(accountIDs.size() > 0){ //accountIDs 0 means we have cached result

            List<UserRole> accountRoles = [SELECT ID, PortalAccountID FROM UserRole WHERE PortalAccountID IN : accountIDs];
            List<Group> userGroups = [SELECT ID, RelatedID, Type FROM Group WHERE RelatedID IN : accountRoles ];
            
            List<sObject> filteredRecordsToShare = new List<sObject>();
            for(sObject recordToShare : recordsToShare){
                String accountID = (String)recordToShare.get(accountFieldName);
                boolean isPresent = false;
                
                for(UserRole role : accountRoles){
                    if(role.PortalAccountID == accountID){
                        isPresent = true;
                        filteredRecordsToShare.add(recordToShare);
                        break;
                    }
                }
                
                if(isPresent == false){
                    recordToShare.addError('Sorry, no partner account has been enabled for this account');
                }
                
                accountIDs.add(accountID);
            }
                                                    
            if(filteredRecordsToShare.size() == 0)
                return;
            
            System.assert(accountRoles.size() > 0);
            System.assert(userGroups.size() > 0);
            
            //2.iii. prepare account -> groupID map because groupId will be used to add a sharing rule.

            
            for(UserRole accountRole : accountRoles){
                /*
                    Removed because user role and subordinates always overrides the role type
                    System.assert(accountToGroupID.containsKey(accountRole.PortalAccountID) == false, 
                              'Expected only one group for one type of role for the given account id but found multiple occurrences.');
                    The loop has been modified to check if a account->group mapping has been added and change the mapping
                    i) only if no mapping exists
                    ii) or if the new mapping is of Role and Subordinates type, catch - if there are multiple role and suboordinates
                    mapping then the last one gets used. 
                */
                Group selectedGroup = null;
                for(Group userGroup : userGroups){
                    if(userGroup.RelatedID == accountRole.ID){
                        if(selectedGroup == null || selectedGroup.Type != 'RoleAndSubordinates'){
                            accountToGroupID.put(accountRole.PortalAccountID, userGroup.ID);
                            selectedGroup = userGroup;
                        }
                        
                    }
                }
            }
        }
        
        //3. Query for current sharing rules and remove them
        String query = 'SELECT ID FROM ' + sharingSObjectName + ' WHERE RowCause =:sharingReason AND ParentID IN :recordsToShare'; 
        List<sObject> existingRules = Database.query(query);
        if(existingRules.size() > 0)
            
            /**
            * WITHOUT SHARING has been used 
            * ---------------------------------
            * Without sharing has been used because only users with “Modify All Data” permission can add or change Apex managed sharing on a record.
            * This will cause an error.
            */
            new SQX_DB().withoutSharing().op_delete(existingRules);


        //4. create new sharing rules
        List<sObject> newSharingRules = new List<sObject>();
        for(sObject recordToShare : recordsToShare){
            sObject sharingRule = sharingSObjectType.newSObject();
            
            sharingRule.put('AccessLevel', accessLevel);
            sharingRule.put('ParentId', recordToShare.get('Id'));
            if( String.isNotBlank(sharingReason) ) {
                sharingRule.put('RowCause', sharingReason);
            }
            
            if(accountToGroupID.containsKey((String)recordToShare.get(accountFieldName)) == false){
                System.debug('No group found for account ' + (String)recordToShare.get(accountFieldName));
                continue;
            }
            
            String groupID = accountToGroupID.get((String)recordToShare.get(accountFieldName));
            
            
            sharingRule.put('UserOrGroupId', (String)accountToGroupID.get((String)recordToShare.get(accountFieldName)));
            
            newSharingRules.add(sharingRule);
        }
        
        /**
        * WITHOUT SHARING has been used 
        * ---------------------------------
        * Without sharing has been used because only users with “Modify All Data” permission can add or change Apex managed sharing on a record.
        * This will cause an error.
        */
        this.withoutSharing().insertShares(newSharingRules);

    }

    /**
    * shares the records with the list of users
    * @param accessLevel the SF defined accesslevel that is to be set on the sharing rule
    * @param recordsToShare the list of objects that are to be shared
    * @param userToShareWith the map of users grouped by the sobject to be shared
    * @param sharedRecordIds the resulting map containing map of user, share rule.
    *i.e. Map<SObject, Map<User ID, Sharing Rule ID>>
    * @param sharingReason sharing reason for corresponding record used for managed sharing
    */
    public void shareRecord(String accessLevel, List<SObject> recordsToShare, Map<SObject,
     List<User>> userToShareWith, Map<Id, Map<Id, Id>> sharedRecordIds, String sharingReason){
        
        if(recordsToShare == null || recordsToShare.size() == 0)
            return;

        //check if the sharing is necessary exists
        //assumption is that all sObject type is of the same type
        SObject obj = recordsToShare.get(0);

        String sObjectType = '' + obj.getSObjectType();
        sObjectType = sObjectType.replace('__c', '__share');

        Schema.SObjectType shareType = Schema.getGlobalDescribe().get(sObjectType);
        if(shareType == null){
            System.debug('Sharing table not found for ' + sObjectType);
            return; //sharing is not necessary
        }

        System.debug('Adding sharing rules');

        List<SObject> sharingRulesToAdd = new List<SObject>();
        for(SObject record : recordsToShare){
            List<User> users = userToShareWith.get(record);
            if(users != null && users.size() > 0){
                for(User user : users){
                    SObject sharingRule = shareType.newSObject();

                    sharingRule.put('AccessLevel', accessLevel);
                    sharingRule.put('ParentId', record.get('Id'));
                    sharingRule.put('UserOrGroupId', user.Id);
                    if( sharingReason != null ) {
                        sharingRule.put('RowCause', sharingReason);
                    }

                    sharingRulesToAdd.add(sharingRule);
                }
            }
        }


        if(sharingRulesToAdd.size()  > 0){
            
            this.insertShares(sharingRulesToAdd);
            
            if(sharedRecordIds != null){
                for(SObject sharedRule : sharingRulesToAdd){
                    Id sharedEntityId = (Id)sharedRule.get('ParentId');
                    Map<Id, Id> sharedEntityMap = sharedRecordIds.get(sharedEntityId);
                    if(sharedEntityMap == null){
                        sharedEntityMap = new Map<Id, Id>();
                        sharedRecordIds.put(sharedEntityId, sharedEntityMap);
                    }

                    sharedEntityMap.put((Id)sharedRule.get('UserOrGroupId'), (Id)sharedRule.get('Id'));
                }
            }
        }
    }

    /**
     * Method to insert sobject shares and handle errors
     * @params sharingRulesToAdd list of shares to insert 
     **/     
    public SQX_Managed_Sharing_Base insertShares(List<SObject> sharingRulesToAdd){
        //insert sharing rules
        System.debug(sharingRulesToAdd);

        SQX_DB sqxDB = new SQX_DB();

        if(this.withoutSharingPrivilege)
            sqxDB.withoutSharing();

        List<Database.SaveResult> saveResults = sqxDB.continueOnError().op_insert(sharingRulesToAdd, new List<SObjectField>{});
        for (Database.SaveResult sr : saveResults) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted record. Record ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {

                    // ignore error that occurred while sharing read access for organization having already public read access.
                    // Note : some time error message might be little different than usual. for example : 
                    //      'field integrity exception: AccessLevel (trivial share level Read, for organization with default level Read)',
                    //      'field integrity exception: unknown (trivial share level Read, for organization with default level Read)';
                    // so instead of checking full error message, just validating statusCode of error
                    if (err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION) {
                        continue;
                    }
                    throw new SQX_ApplicationGenericException(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
        return this;
    }

    public Boolean withoutSharingPrivilege = false;

    /**
    * This method is a shorthand that can be used to set the without sharing privilege, which ensures that the sharing rule insertion is done without sharing model.
    */
    public SQX_Managed_Sharing_Base withoutSharing(){
        this.withoutSharingPrivilege = true;

        return this;
    }
}