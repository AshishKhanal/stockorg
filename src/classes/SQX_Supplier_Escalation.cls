/**
 * This class performs actions for Supplier Escalation.
 */
public with sharing class SQX_Supplier_Escalation {

    public static final List<String> CLOSED_ESCALATION_LEVELS = new List<String>{'0'};
    public static final String ACTIVITY_CODE_RESET = 'reset';
    
    /** 
     * Supplier Escalation Trigger Handler
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_Supplier_Trigger_Handler {
        
        /**
         * Default constructor for this class, that accepts old and new map from the trigger
         * @param newSupplierEscalations equivalent to trigger.New in salesforce
         * @param oldMap equivalent to trigger.OldMap in salesforce
         */
        public Bulkified(List<SQX_Supplier_Escalation__c> newSupplierEscalations, Map<Id,SQX_Supplier_Escalation__c> oldMap){
            
            super(newSupplierEscalations, oldMap);
        }
        
        /**
        * Method executes all actions that are run in Before Trigger context
        * a. [processVoidedRecords]: clears Supplier Escalation stage and restores escalation link in account to previous link when voided
        */
        protected override void onBefore() {
            if (Trigger.isUpdate) {
                processVoidedRecords();
            }
        }
        
        /**
         *  Method executes all actions that are run in After Trigger context
         * a. [createNewSupplierEscalationWhenEscalatedDeescalated]method to create further escalation record when escalated or deescalated and copy references
         * b. [updateAccountWithSupplierEscalation] This method will update SQX_Supplier_Escalation__c field of related Account of Supplier Escalation record when Suppier Escalation record is initiated or escalated or deescalated.
         * c. [revertTheEscalationWhenStatusIsReset] method to revert status and stage of source escalation when further action is voided
		 */
        protected override void onAfter() {
            if(Trigger.isUpdate) {
                createNewSupplierEscalationWhenEscalatedDeescalated();
                updateAccountWithSupplierEscalation();
                revertTheEscalationWhenStatusIsReset();
            }
        }
        
        /**
        * clears Supplier Escalation stage and restores escalation link in account to previous link when voided
        */
        private void processVoidedRecords() {
            final String ACTION_NAME = 'processVoidedRecords';

            resetView();
            removeProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME);

            List<SQX_Supplier_Escalation__c> voidedRecs = new List<SQX_Supplier_Escalation__c>();
            List<SQX_Supplier_Escalation__c> updateSourceEscalation = new List<SQX_Supplier_Escalation__c>();
            Map<Id, Account> accsToUpdate = new Map<Id, Account>();

            for (SQX_Supplier_Escalation__c rec : (List<SQX_Supplier_Escalation__c>)this.view) {
                SQX_Supplier_Escalation__c oldRec = (SQX_Supplier_Escalation__c)oldValues.get(rec.Id);

                if (rec.Status__c == SQX_Supplier_Common_Values.STATUS_VOID && oldRec.Status__c != SQX_Supplier_Common_Values.STATUS_VOID) {
                    voidedRecs.add(rec);

                    // clear stage
                    rec.Record_Stage__c = null;

                    // clear source escalation's further action and restart the record
                    if(String.isNotBlank(rec.SQX_Source_Escalation__c)){
                        updateSourceEscalation.add(new SQX_Supplier_Escalation__c(Id = rec.SQX_Source_Escalation__c, 
                                                                                  Activity_Code__c = ACTIVITY_CODE_RESET));
                    }
                    
                    if (oldRec.Record_Stage__c != SQX_Supplier_Common_Values.STAGE_DRAFT) {
                        // restore supplier escalation link of related account
                        Account acc = new Account(
                            Id = rec.SQX_Account__c,
                            SQX_Supplier_Escalation__c = rec.SQX_Source_Escalation__c
                        );
                        accsToUpdate.put(acc.Id, acc);
                    }
                }
            }

            addToProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME, voidedRecs);
            
            if (!accsToUpdate.isEmpty()) {
                /**
                * WITHOUT SHARING USED
                * --------------------
                * User voiding escalation may not have edit permission on the related account and source escalation
                */
                new SQX_DB().withoutSharing().op_update(new Map<SObjectType, List<SObject>>{
                    Account.SObjectType => accsToUpdate.values(),
                        SQX_Supplier_Escalation__c.SObjectType => updateSourceEscalation
                }, new Map<SObjectType, List<SObjectField>>{
                    Account.SObjectType => new List<SObjectField>{Account.SQX_Supplier_Escalation__c},
                        SQX_Supplier_Escalation__c.SObjectType => new List<SObjectField>{SQX_Supplier_Escalation__c.Activity_Code__c}
                });
            }
        }
        
        private void revertTheEscalationWhenStatusIsReset(){
            final String ACTION_NAME = 'revertTheEscalationWhenStatusIsReset';
            Rule resetEscalationRule = new Rule();
            resetEscalationRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, ACTIVITY_CODE_RESET);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME)
                .applyFilter(resetEscalationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(!this.view.isEmpty()){
                Map<Id, SObject> records = new Map<Id, SObject>(this.view);
    
                SQX_Step_Evaluation_Engine evaluationEngine = new SQX_Step_Evaluation_Engine();
    
                Map<Id, SObject> evaluatedRecords = evaluationEngine.evaluateSteps(new List<Id>(records.keySet()), false);  //only evaluate, do not update
                
                List<SQX_Supplier_Escalation__c> escalationList = new List<SQX_Supplier_Escalation__c>();
                SQX_Supplier_Escalation__c supplierEscalation;
                
                for(SQX_Supplier_Escalation__c escalation: (List<SQX_Supplier_Escalation__c>)records.values()){
                    supplierEscalation = (SQX_Supplier_Escalation__c)evaluatedRecords.get(escalation.Id);
                    
                    supplierEscalation.Result__c = null;
                    supplierEscalation.SQX_Further_Action__c = null;
                    supplierEscalation.Is_Locked__c = false;
                    supplierEscalation.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
                    escalationList.add(supplierEscalation);
                }
                new SQX_DB().op_update(escalationList, new List<SObjectField>{
                    SQX_Supplier_Escalation__c.Result__c,
                        SQX_Supplier_Escalation__c.SQX_Further_Action__c,
                        SQX_Supplier_Escalation__c.Is_Locked__c,
                        SQX_Supplier_Escalation__c.Status__c,
                        SQX_Supplier_Escalation__c.Workflow_Status__c,
                        SQX_Supplier_Escalation__c.Current_Step__c
                });
            }
        }

        /**
         * method to create further escalation record
         */
        private void createNewSupplierEscalationWhenEscalatedDeescalated(){
            
            final String ACTION_NAME = 'createNewSupplierEscalationWhenEscalatedDeescalated';
            Rule closedEscalationRule = new Rule();
            closedEscalationRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_ESCALATE);
            closedEscalationRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_DEESCALATE);
            closedEscalationRule.operator = RuleCombinationOperator.opOr;
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME)
                .applyFilter(closedEscalationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(!this.view.isEmpty()){
                this.addToProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME, this.view);
                Map<Id, SQX_Supplier_Escalation__c> closedSupplierEscalationMap = new Map<Id, SQX_Supplier_Escalation__c>((List<SQX_Supplier_Escalation__c>)this.view);
                List<SQX_Supplier_Escalation__c> newSupplierEscalationsToInsert = new List<SQX_Supplier_Escalation__c>();
                List<SQX_Supplier_Escalation__c> existingSupplierEscalationsToUpdate = new List<SQX_Supplier_Escalation__c>();
                List<SQX_Escalation_Reference__c> escalationReferencesToInsert = new List<SQX_Escalation_Reference__c>();
                Map<Id, SQX_Supplier_Escalation__c> oldNewSupplierEscalationMap = new Map<Id, SQX_Supplier_Escalation__c>();
                SQX_Supplier_Escalation__c escalation;
                SQX_Escalation_Reference__c escalationReference;
                String rationalForEscalation;
                Integer rationaleLength = SQX_Supplier_Escalation__c.Rationale_for_Escalation__c.getDescribe().getLength();
                
                List<SObjectField>  escalationFields = Schema.SObjectType.compliancequest__SQX_Supplier_Escalation__c.fields.getMap().values();
                List<SObjectField> createableFields=new List<SObjectField>();
                Set<SObjectField> ignoreFields = new Set<SObjectField> {
                        SQX_Supplier_Escalation__c.Target_Escalation_Level__c,
                        SQX_Supplier_Escalation__c.Activity_Code__c,
                        SQX_Supplier_Escalation__c.Activity_Comment__c,
                        SQX_Supplier_Escalation__c.Current_Step__c,
                        SQX_Supplier_Escalation__c.Initiation_Date__c,
                        SQX_Supplier_Escalation__c.Is_Locked__c,
                        SQX_Supplier_Escalation__c.Prevent_Name_Update__c,
                        SQX_Supplier_Escalation__c.Account_Type__c,
                        SQX_Supplier_Escalation__c.Result__c,
                        SQX_Supplier_Escalation__c.SQX_Further_Action__c,
                        SQX_Supplier_Escalation__c.Status__c,
                        SQX_Supplier_Escalation__c.Target_Escalation_Difference__c,
                        SQX_Supplier_Escalation__c.Workflow_Status__c
                };
                for(SObjectField field:escalationFields ){
                    //gets only which is createableFields fileds
                    if (!ignoreFields.contains(field) && field.getDescribe().isCreateable()){ // field is creatable
                        createableFields.add(field);
                    }
                }

                for(SQX_Supplier_Escalation__c supplierEscalation: closedSupplierEscalationMap.values()){
                    if(!CLOSED_ESCALATION_LEVELS.contains(supplierEscalation.Target_Escalation_Level__c)){
                        escalation = new SQX_Supplier_Escalation__c();
                        for(Schema.SObjectField field :createableFields){
                            escalation.put(field, supplierEscalation.get(field));   
                        }
                        rationalForEscalation = String.isNotBlank(supplierEscalation.Further_Action_Rationale__c) && String.isNotBlank(supplierEscalation.Rationale_for_Escalation__c) ? 
                                                supplierEscalation.Further_Action_Rationale__c+' '+supplierEscalation.Rationale_for_Escalation__c : 
                                                String.isNotBlank(supplierEscalation.Further_Action_Rationale__c) ? supplierEscalation.Further_Action_Rationale__c : 
                                                String.isNotBlank(supplierEscalation.Rationale_for_Escalation__c) ? supplierEscalation.Rationale_for_Escalation__c : '';
                        escalation.Rationale_for_Escalation__c = rationalForEscalation.abbreviate(rationaleLength);
                        
                        escalation.SQX_Source_Escalation__c = supplierEscalation.Id;
                        escalation.Escalation_Level__c = supplierEscalation.Target_Escalation_Level__c;
                        escalation.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_TRIAGE;
                        escalation.Submitted_Date__c = System.Datetime.now();
                        escalation.Submitted_By__c = UserInfo.getUserId();
                        escalation.Prior_Escalation_Level__c = supplierEscalation.Escalation_Level__c;
                        oldNewSupplierEscalationMap.put(supplierEscalation.Id, escalation);
                        newSupplierEscalationsToInsert.add(escalation);
                    }
                }
                
                SQX_DB db = new SQX_DB();
                db.op_insert(newSupplierEscalationsToInsert, new List<SObjectField>{
                    SQX_Supplier_Escalation__c.Org_Business_Unit__c,
                        SQX_Supplier_Escalation__c.Org_Division__c,
                        SQX_Supplier_Escalation__c.Org_Region__c,
                        SQX_Supplier_Escalation__c.Org_Site__c,
                        SQX_Supplier_Escalation__c.Related_Part_Numbers__c,
                        SQX_Supplier_Escalation__c.SQX_Standard_Service__c,
                        SQX_Supplier_Escalation__c.SQX_Account__c,
                        SQX_Supplier_Escalation__c.SQX_Part__c,
                        SQX_Supplier_Escalation__c.SQX_Supplier_Contact__c,
                        SQX_Supplier_Escalation__c.SQX_Supplier_Liaison__c,
                        SQX_Supplier_Escalation__c.Subject__c,
                        SQX_Supplier_Escalation__c.Rationale_for_Escalation__c,
                        SQX_Supplier_Escalation__c.SQX_Source_Escalation__c,
                        SQX_Supplier_Escalation__c.Escalation_Level__c,
                        SQX_Supplier_Escalation__c.Activity_Code__c,
                        SQX_Supplier_Escalation__c.Prior_Escalation_Level__c
                });
                
                for(SQX_Escalation_Reference__c reference: [SELECT Comment__c, Other_Reference__c,
                                                            Reference_Title__c, SQX_Related_Escalation__c,
                                                            SQX_Related_Finding__c, SQX_Related_Nonconformance__c,
                                                            RecordTypeId, SQX_Supplier_Escalation__c
                                                            FROM SQX_Escalation_Reference__c WHERE SQX_Supplier_Escalation__c IN: oldNewSupplierEscalationMap.keySet()]){
                                                                escalationReference = new SQX_Escalation_Reference__c();
                                                                escalationReference.Comment__c = reference.Comment__c;
                                                                escalationReference.Other_Reference__c = reference.Other_Reference__c;
                                                                escalationReference.SQX_Related_Escalation__c = reference.SQX_Related_Escalation__c;
                                                                escalationReference.SQX_Related_Finding__c = reference.SQX_Related_Finding__c; 
                                                                escalationReference.SQX_Related_Nonconformance__c = reference.SQX_Related_Nonconformance__c;
                                                                escalationReference.RecordTypeId = reference.RecordTypeId;
                                                                escalationReference.SQX_Supplier_Escalation__c = oldNewSupplierEscalationMap.get(reference.SQX_Supplier_Escalation__c).Id;                        
                                                                escalationReferencesToInsert.add(escalationReference);
                                                            }
                
                /**
                * WITHOUT SHARING USED
                * --------------------
                * User escalating/deescalating the record may not have edit permission on the new record as it is directly assigned to queue
                */
                 db.withoutSharing().op_insert(escalationReferencesToInsert, new List<SObjectField>{
                    SQX_Escalation_Reference__c.Comment__c,
                        SQX_Escalation_Reference__c.Other_Reference__c,
                        SQX_Escalation_Reference__c.SQX_Related_Escalation__c,
                        SQX_Escalation_Reference__c.SQX_Related_Finding__c,
                        SQX_Escalation_Reference__c.SQX_Related_Nonconformance__c,
                        SQX_Escalation_Reference__c.RecordTypeId,
                        SQX_Escalation_Reference__c.SQX_Supplier_Escalation__c
                });
                
                
                for(Id sourceSupplierEscalationId: oldNewSupplierEscalationMap.keySet()){
                    escalation = new SQX_Supplier_Escalation__c(Id = sourceSupplierEscalationId, 
                                                                SQX_Further_Action__c = oldNewSupplierEscalationMap.get(sourceSupplierEscalationId).Id,
                                                                Workflow_Status__c = null);
                    existingSupplierEscalationsToUpdate.add(escalation);
                }
                
                db.withSharing().op_update(existingSupplierEscalationsToUpdate, new List<SObjectField>{
                    SQX_Supplier_Escalation__c.SQX_Further_Action__c,
                        SQX_Supplier_Escalation__c.Workflow_Status__c
                });
            }
        }
        
        /**
         * This method will update SQX_Supplier_Escalation__c field of related Account of Supplier Escalation record when Suppier Escalation record is initiated or escalated or deescalated.
         * @story : SQX-6578 
         */
        private void updateAccountWithSupplierEscalation() {
            final String ACTION_NAME = 'updateAccountWithSupplierEscalation';
            Rule updateRule = new Rule();            
            updateRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE);
            updateRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_ESCALATE);
            updateRule.addRule(Schema.SQX_Supplier_Escalation__c.Activity_Code__c, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_DEESCALATE);
            updateRule.operator = RuleCombinationOperator.opOr;
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME)
                .applyFilter(updateRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!this.view.isEmpty()) {
                List<SQX_Supplier_Escalation__c> escalations = [SELECT Id, SQX_Account__c, SQX_Further_Action__c, Status__c FROM SQX_Supplier_Escalation__c WHERE Id IN: this.view];
                List<Account> accountsToBeUpdated = new List<Account>();
                for (SQX_Supplier_Escalation__c escalation : escalations) {
                    if(escalation.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN || escalation.Status__c == SQX_Supplier_Common_Values.STATUS_COMPLETE){
                        accountsToBeUpdated.add(new Account(Id = escalation.SQX_Account__c, SQX_Supplier_Escalation__c = escalation.Id));
                    }else{
                        accountsToBeUpdated.add(new Account(Id = escalation.SQX_Account__c, SQX_Supplier_Escalation__c = escalation.SQX_Further_Action__c));
                    }
                }
                // add processed records to avoid recursion
                this.addToProcessedRecordsFor(SQX.SupplierEscalation, ACTION_NAME, this.view);
                
                /**
                 * WITHOUT SHARING used
                 * --------------------
                 * Since the user who is performing action may not have edit access to account.
                 */
                new SQX_DB().withoutSharing().op_update(accountsToBeUpdated, new List<Schema.SObjectField> {Schema.Account.SQX_Supplier_Escalation__c});
            }
        }
    }
}