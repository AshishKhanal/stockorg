/**
* unit tests for NSI record activities
*/
@isTest
public class SQX_Test_5590_NSI_RecordActivities {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');

        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, standardUser2, adminUser});
    }

    /**
    * GIVEN : given NSI record
    * WHEN : completing the NSI record
    * THEN : capture the activity
    */
    public static testMethod void givenNSIRecord_WhenCompletingTheRecord_ThenCaptureTheActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        List<String> expectedRecordActivities = new List<String> 
        {
            Label.cq_ui_submitting_the_record,
            Label.SQX_PS_CAPA_Initiating_the_record,
            Label.cq_ui_completing_the_record
        };

        SQX_Test_NSI nsi;
        System.runAs(adminUser) {
            //Arrange: Initiate an NSI record
            SQX_Test_NSI.createPolicyTasks(1, 'Task', standardUser, 1);

            nsi = new SQX_Test_NSI().save();

            Test.startTest();
            nsi.submit();
            
            nsi.initiate();

            SQX_BulkifiedBase.clearAllProcessedEntities();
        }

        // ACT : Complete the record by completing the task
        System.runAs(standardUser) {

            Task t = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id];

            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            Test.stopTest();

        }

        // Assert : Complete activity is added along with submit and initate action
        List<SQX_NSI_Record_Activity__c> activitiesRecorded = [SELECT Id, Activity__c FROM SQX_NSI_Record_Activity__c WHERE SQX_New_Supplier_Introduction__c =: nsi.nsi.Id ORDER BY CreatedDate ASC];

        System.assertEquals(expectedRecordActivities.size(), activitiesRecorded.size());

        // assert order of activity
        for(integer i=0; i < activitiesRecorded.size(); i++) {
            System.assertEquals(expectedRecordActivities.get(i), activitiesRecorded.get(i).Activity__c);
        }

    }


    /**
    * GIVEN : given NSI record
    * WHEN : voiding the NSI record
    * THEN : capture the activity
    */
    public static testMethod void givenNSIRecord_WhenVoidingTheRecord_ThenCaptureTheActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            //Arrange: Retrieve NSI record
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            //Act: Voiding the record
            nsi.void();

            // Assert : Void activity is added
            List<SQX_NSI_Record_Activity__c> nsiRecordActivity =[SELECT Id, Activity__c FROM SQX_NSI_Record_Activity__c];
            System.assertEquals(1, nsiRecordActivity.Size());
            System.assertEquals(Label.sqx_ps_capa_voiding_the_record, nsiRecordActivity.get(0).Activity__c);

        }
    }

    /**
    * GIVEN : given NSI record with linked account
    * WHEN : save the record
    * THEN : transfer account number into NSI supplier number field
    */
    public static testMethod void giveNSI_WhenSave_ThenTransferAccountNumberValueIntoSupplierNumber(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Create account and retreive nsi record
            Account account = SQX_Test_Account_Factory.createAccount();
            account.AccountNumber = '1234567';
            new SQX_DB().op_update(new List<Account>{ account }, new List<SobjectField>{});

            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            //Act: save the record with linked account
            nsi.nsi.SQX_Account__c = account.Id;
            nsi.nsi.Supplier_Number__c = '';
            nsi.save();

            //Assert: Ensured that account number transfer into supplier number
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Supplier_Number__c FROM SQX_New_Supplier_Introduction__C WHERE Id =: nsi.nsi.Id];
            System.assertEquals(account.AccountNumber, nsi1.Supplier_Number__c);
        }
    }


    /**
     *  Given an nsi record
     *  When the nsi record is acted upon (edited or progressed further)
     *  Then record activities are captured
     */
    static testmethod void givenNSIRecord_WhenItMovesThroughVariousStages_RecordActivitiesAreCorrectlyCaptured() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){

            List<String> expectedRecordActivities = new List<String> 
            {
                Label.cq_ui_submitting_the_record,
                Label.SQX_PS_CAPA_Initiating_the_record,
                Label.cq_ui_completing_the_record,
                Label.SQX_PS_CAPA_Saving_the_record,
                Label.CQ_UI_Restarting_the_record,
                Label.cq_ui_completing_the_record,
                Label.SQX_PS_CAPA_Closing_the_record
            };

            //Arrange: an NSI record
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();


            Test.startTest();

            //ACT: Submitting the NSI record
            nsi.submit();
            
            //ACT : Editing the record, this activity should not be captured
            nsi.clear();
            nsi.nsi.Description__c = 'Changed Description';
            nsi.save();

            //Act: Initiating the record
            nsi.initiate();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT : Editing the record, this activity should be captured
            nsi.clear();
            nsi.nsi.Description__c = 'Changed Description';
            nsi.save();

            Test.stopTest();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT: Restarting the record
            nsi.restart();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT: Closing the record
            nsi.close();

            // Assert : Record Activities are appropriately added
            List<SQX_NSI_Record_Activity__c> activitiesRecorded = [SELECT Id, Activity__c FROM SQX_NSI_Record_Activity__c WHERE SQX_New_Supplier_Introduction__c =: nsi.nsi.Id ORDER BY CreatedDate ASC];

            System.assertEquals(expectedRecordActivities.size(), activitiesRecorded.size());

            // assert order of activity
            for(integer i=0; i < activitiesRecorded.size(); i++) {
                System.assertEquals(expectedRecordActivities.get(i), activitiesRecorded.get(i).Activity__c);
            }

        }

    }

    /**
    * GIVEN : given NSI record owned by Standard User with onboarding step assigned to another user
    * WHEN : NSI is voided
    * THEN : void succeeds
    */
    public static testMethod void givenNSIRecordOwnedByStandardUser_WhenNSIVoided_ThenVoidSucceeds(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser2 = allUsers.get('standardUser2');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // ARRANGE : Given a NSI with document request onboarding step
        	SQX_Test_NSI.createPolicyTasks(1, SQX_NSI.TASK_TYPE_DOC_REQUEST, standardUser2, 1);
        }
        
        System.runAs(standardUser){
            //Arrange: Retrieve NSI record
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            //Act: submit the record
            nsi.submit();
            
            //Act: initiate the record
            nsi.initiate();
            
            //Act: void the record
            nsi.void();
            
            // Assert : Void activity is added
            List<SQX_NSI_Record_Activity__c> nsiRecordActivity =[SELECT Id, Activity__c FROM SQX_NSI_Record_Activity__c where SQX_New_Supplier_Introduction__c  =: nsi.nsi.Id];
            System.assertEquals(3, nsiRecordActivity.Size());
            System.assertEquals(Label.sqx_ps_capa_voiding_the_record, nsiRecordActivity.get(2).Activity__c);
        }
    }
}