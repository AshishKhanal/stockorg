/**
*
*/
public with sharing class SQX_Resp_Inclusion_Approval{

    public with sharing class Bulkified extends SQX_BulkifiedBase{
    
        /**
        * default constructor for calling methods using Bulkified
        */
        public Bulkified(List<SQX_Resp_Inclusion_Approval__c > newInclusions, Map<Id,SQX_Resp_Inclusion_Approval__c > oldMap){
        
            super(newInclusions, oldMap);
        }
        
        /**
        * this method sets the default approver to the current user, approval time to now and approver name to current user
        * name. Note: Everything beside the approver can be set from a workflow.
        */
        public Bulkified setApproverInformation(){
            User currentUser = SQX_Utilities.getCurrentUser();
            for(SQX_Resp_Inclusion_Approval__c appInfo : (List<SQX_Resp_Inclusion_Approval__c>)this.newValues){
                appInfo.SQX_Approving_User__c = currentUser.Id;
                appInfo.Approver_Name__c = currentUser.Name;
                appInfo.Approved_On__c = DateTime.now();
            }
            
            return this;
        }
    }
}