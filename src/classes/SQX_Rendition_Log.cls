/**
*/
public with sharing class SQX_Rendition_Log {

    public static String STATUS_PENDING = 'Pending',
        				 STATUS_FAILURE = 'Failure',
        				 STATUS_COMPLETED = 'Completed',
                         STATUS_FETCHING = 'Fetching Rendition',
                         STATUS_ABORT = 'Abort',
                         STATUS_ABORT_MAX_RETRIES = 'Abort-Max Retries',
                         STATUS_FETCHING_THROUGH_PROXY = 'Fetching Through Proxy';
    
    public static final Integer MAX_RETRIES = 10;
    
    /**
    * Updates/Creates the log for the api interaction of the rendition request.
    * @param callMap the rendition request to response that is to be logged. 
    */
    public static void log(Map<SQX_RenditionRequest, SQX_RenditionResponse> callMap){
    	Set<String> logIds;
        Set<Id> docIds;
        final Integer MAX_ERROR_LENGTH = SQX_Rendition_Log__c.Last_Error__c.getDescribe().getlength();

    	logIds = new Set<String>();
    	docIds = new Set<Id>();

    	for(SQX_RenditionRequest request : callMap.keySet()){
    		docIds.add(request.document.Id);
    		logIds.add(callMap.get(request).JobId);
    	} 


    	Map<String,  SQX_Rendition_Log__c> docJob2Log = new Map<String, SQX_Rendition_Log__c>();

        for(SQX_Rendition_Log__c log : [SELECT Id, Content_Version_Id__c, SQX_Controlled_Document__c, Job_Id__c, Last_Error__c,
        									Last_Processed_On__c, Provider_Type__c, Retries__c, Started_On__c, Status__c
        									FROM SQX_Rendition_Log__c
        									WHERE SQX_Controlled_Document__c IN: docIds AND Job_Id__c IN: logIds
        									ORDER BY Last_Processed_On__c, Started_On__c DESC]){

        	docJob2Log.put(log.SQX_Controlled_Document__c + '' + log.Job_Id__c, log);
        }

        List<SQX_Rendition_Log__c> logsToInsert = new List<SQX_Rendition_Log__c>(), logsToUpdate = new List<SQX_Rendition_Log__c>();

        for(SQX_RenditionRequest request : callMap.keySet()){
        	SQX_RenditionResponse response = callMap.get(request);
			SQX_Controlled_Document__c doc = request.document;

	    	SQX_Rendition_Log__c log = new SQX_Rendition_Log__c();
	    	log.Provider_Type__c = request.providerName;
	        log.Content_Version_Id__c = request.hasContentChanged()  ? request.primaryContentVersion : request.content.Id;
            log.Status__c = response.Status == SQX_RenditionResponse.Status.Error ? STATUS_FAILURE :
            				response.Status == SQX_RenditionResponse.Status.Processing ? STATUS_PENDING :
                            response.Status == SQX_RenditionResponse.Status.RenditionReady ? STATUS_FETCHING :
                            response.Status == SQX_RenditionResponse.Status.FetchingThroughProxy ? STATUS_FETCHING_THROUGH_PROXY :
            				STATUS_COMPLETED;
        	
        	String key = doc.Id + '' + response.JobId;
        	if(docJob2Log.containsKey(key)){
        		SQX_Rendition_Log__c oLog = docJob2Log.get(key);

        		log.Id = oLog.Id;

        		if(response.Status == SQX_RenditionResponse.Status.Error){
        			log.Retries__c = olog.Retries__c + 1;
                    log.Status__c = log.Retries__c < MAX_RETRIES ? log.Status__c : STATUS_ABORT_MAX_RETRIES;
        			log.Last_Error__c = !String.IsBlank(response.ErrorDescription) && response.ErrorDescription.length() > MAX_ERROR_LENGTH ?
                                        response.ErrorDescription.subString(0, MAX_ERROR_LENGTH) : response.errorDescription;
	        	}
	        	else if(olog.Status__c != STATUS_FAILURE){
	        		log.Retries__c = 0;
	        		log.Last_Error__c = '';
	        	}

	        	log.Last_Processed_On__c = DateTime.Now();

	        	logsToUpdate.add(log);
        	}
        	else{
	        	log.SQX_Controlled_Document__c = doc.Id;
	        	log.Job_Id__c = response.JobId;
	        	log.Started_On__c = DateTime.Now();
	        	log.Retries__c = 0;

	        	logsToInsert.add(log);
        	}
        	
        }

        SQX_DB db = new SQX_DB();
        if(logsToUpdate.size() > 0){
        	db.op_update(logsToUpdate, new List<SObjectField> {
        		SQX_Rendition_Log__c.Retries__c,
        		SQX_Rendition_Log__c.Last_Error__c,
        		SQX_Rendition_Log__c.Last_Processed_On__c
        	});
        }

        if(logsToInsert.size() > 0){
        	db.op_insert(logsToInsert, new List<SObjectField> {
        		SQX_Rendition_Log__c.Retries__c,
        		SQX_Rendition_Log__c.Last_Error__c,
        		SQX_Rendition_Log__c.Last_Processed_On__c,
        		SQX_Rendition_Log__c.Retries__c,
        		SQX_Rendition_Log__c.Started_On__c,
        		SQX_Rendition_Log__c.Job_Id__c,
                SQX_Rendition_Log__c.Status__c
        	});
        }
    }

    /**
    * Simple override to log a request to the response
    */
    public static void log(SQX_RenditionRequest request, SQX_RenditionResponse response){
        log(new Map<SQX_RenditionRequest, SQX_RenditionResponse> {
        	request => response
        });

    }
    

    /**
    * Removes the rendition log for a given interaction the request response parameter correctly identifies
    * the related document and job.
    */
    public static void remove(SQX_RenditionRequest request, SQX_RenditionResponse response){
        SQX_Controlled_Document__c doc = request.document;

        List<SQX_Rendition_Log__c> logs = [SELECT Id FROM SQX_Rendition_Log__c WHERE
        										  SQX_Controlled_Document__c = : doc.Id AND Job_Id__c =: response.JobId];

        new SQX_DB().op_delete(logs);
    }


    /**
    * This method finds the rendition logs that are related to the documents and returns them
    * @param docs the document whose rendition logs are to be returned
    * @return returns the map of rendition log keyed by the document id.
    */
    public static Map<Id, SQX_Rendition_Log__c> getLogsFor(List<SQX_Controlled_Document__c> docs){

        Map<Id, SQX_Rendition_Log__c> doc2Log = new Map<Id, SQX_Rendition_Log__c>();

        for(SQX_Rendition_Log__c log : [SELECT Id, Content_Version_Id__c, SQX_Controlled_Document__c, Job_Id__c, Last_Error__c,
                                            Last_Processed_On__c, Provider_Type__c, Retries__c, Started_On__c, Status__c
                                            FROM SQX_Rendition_Log__c
                                            WHERE SQX_Controlled_Document__c IN: docs
                                            ORDER BY Last_Processed_On__c, Started_On__c ASC]){

            doc2Log.put(log.SQX_Controlled_Document__c, log);
        }

        return doc2Log;
    }


    /**
    *   This method returns all the items in rendition log that are no longer valid
    *   i.e Logs which do not have associated controlled document,
    *       Logs whose associated document no longer require CQ rendition
    */
    public static List<SQX_Rendition_Log__c> getAllStaleLogs(){

        return [SELECT  Id, Content_Version_Id__c, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Secondary_Content__c, Job_Id__c, Last_Error__c,
                        Last_Processed_On__c, Provider_Type__c, Retries__c, Started_On__c, Status__c
                        FROM SQX_Rendition_Log__c
                        WHERE SQX_Controlled_Document__c = '' OR SQX_Controlled_Document__r.Secondary_Content__c !=: SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO
                        ORDER BY Last_Processed_On__c, Started_On__c ASC];
    }


}