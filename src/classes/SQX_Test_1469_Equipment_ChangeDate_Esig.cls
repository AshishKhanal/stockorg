/**
* Unit tests related to calibration dates and interval for Equipment object
*/
@isTest
public class SQX_Test_1469_Equipment_ChangeDate_Esig{

    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; // Esignature is not validating password when consumer key is wrong.
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            if(Invalid_Request){
                response.setStatusCode(400);
                response.setBody('Error: 400');
            }
            else{
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();
                if(url.contains('rightPassword')){
                    response.setBody('{"id":"allcorrect"}');
                }
                else{
                    response.setBody('{"error":"incorrect password"}');
                }
            }
            
            return response;
        }
    }

    /**
    * This test ensures User SignOff with E-signature to work as expected.
    */
    public testmethod static void givenUserSignOff() {
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        
        //Creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // User signoff
        System.runas(testUser1) {

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c equipment = new SQX_Equipment__c();
            equipment.Name = '123test';
            equipment.Equipment_Description__c = 'This is test equipment';
            equipment.Requires_Periodic_Calibration__c = true;
            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Next_Calibration_Date__c = equipment.Last_Calibration_Date__c.addDays(10); //next calibration is after 10 days
            equipment.Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE ;
            equipment.Equipment_Status__c = SQX_Equipment.STATUS_NEW;


            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert( equipment, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr.isSuccess(), 'Expecting equipment reocord had been inserted');

            Test.setCurrentPage(Page.SQX_Equipment_Change_Calibration_Date); //setting SQX_Equipment_Change_Calibration_Date for Test

            ApexPages.StandardController controller = new ApexPages.StandardController(equipment);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);

            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Calibration_Interval__c = 50; //we have added calibration interval as 50 days
            extension.recordActivityComment = 'test for e-sig';
            extension.SigningOffUsername = testUser1.username;
            extension.SigningOffPassword = 'rightPassword';
            
            Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
            
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1469_Equipment_ChangeDate_Esig';
            cqES.Consumer_Secret__c = 'SQX_Test_1469_Equipment_ChangeDate_Esig';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            // Act
            extension.changeCalibrationDate();
            
            SQX_Equipment_Record_Activity__c era = new SQX_Equipment_Record_Activity__c();
            era = [SELECT Id, Comment__c, SQX_Equipment__c, Modified_By__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :equipment.Id];
            
            //Assert: Document Training status Compare 
            System.assert(era.SQX_Equipment__c == equipment.Id,
                'Expected Equipment record had been updated.');
            
            //Assert: Ensuring user signed off by value
            System.assert(era.Modified_By__c == extension.getUserDetails(),
                'Expected user had modified the record.');

            //Assert :ensuring User Signing off comment set
            System.assert(era.Comment__c.endsWith('test for e-sig'),
                'Expected comment had been added to record history.');
        }
    }


}