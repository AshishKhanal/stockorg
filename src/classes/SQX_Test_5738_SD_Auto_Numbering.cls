/**
* unit test for auto numbering
*/
@isTest
public class SQX_Test_5738_SD_Auto_Numbering {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
                System.runas(adminUser) {
            // add required auto numbers
            List<SQX_Auto_Number__c> autoNums = new List<SQX_Auto_Number__c>();
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Supplier Deviation',
                Next_Number__c = 0,
                Number_Format__c = 'SD-{1}',
                Numeric_Format__c = '0'
            ));
            new SQX_DB().op_insert(autoNums,  new List<Schema.SObjectField>{});
        }
    }
    
    /**
    * GIVEN : given supplier deviation record 
    * WHEN : save the record
    * THEN : update auto number in SD number(Name) field
    */
    public static testMethod void givenNSI_WhenSaveTheRecord_ThenUpdateAutoNumber(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: arrange sd record
            SQX_Supplier_Deviation__c sd = new SQX_Test_Supplier_Deviation().save().sd;
            
            //Assert: Ensured that auto number is reflected
            SQX_Supplier_Deviation__c sd1 = [SELECT Name FROM SQX_Supplier_Deviation__c WHERE Id =: sd.Id];
            System.assertEquals('SD-0', sd1.Name);
            
        }
    }
}