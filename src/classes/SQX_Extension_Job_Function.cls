/**
 * This is the extension class for SQX_Job_Function__c
 */
public with sharing class SQX_Extension_Job_Function {
    ApexPages.StandardController stdController;
    SQX_Job_Function__c mainRecord;
    
    // This variable is used to include deactivated pjf too
    public boolean includeDeactivatedPJF { get; set; }
    
    // This variable is used to include deactivated requirements 
    public boolean includeDeactivatedRequirement { get; set; }

    // Internal variable used to hold the previous value of includeDeactivatedPJF
    private boolean oldIncludeDeactivatedPJF = includeDeactivatedPJF;
    
    // Internal variable used to hold the previous value of includeDeactivatedRequirement
    private boolean oldIncludeDeactivatedRequirement = includeDeactivatedRequirement;
    
    /**
     * The instance of standard set controller useful for paginating accross records
     */
    private ApexPages.StandardSetController activePJFController;
    
    /**
     * The instance of standard set controller useful for paginating accross records
     */
    private ApexPages.StandardSetController activeRequirementController;
    
    /**
     * The default size of the page that is being used for pagination.
     */
    private final Integer DEFAULT_PAGE_SIZE = 10;
    
    /**
     * Constructor for page
     */
    public SQX_Extension_Job_Function(ApexPages.StandardController controller) {
        stdController = controller;
        mainRecord = (SQX_Job_Function__c)controller.getRecord();
        includeDeactivatedPJF = false;
        includeDeactivatedRequirement = false;
        activePJFController = null;
        activeRequirementController = null;
    }
    
    /**
     * This method returns the controller for personnel job functions
     */
    public ApexPages.StandardSetController getPersonnelJobFunctions() {
        if(activePJFController == null || includeDeactivatedPJF != oldIncludeDeactivatedPJF) {
            
            String dynamicQuery = 'SELECT {!FieldList} FROM SQX_Personnel_Job_Function__c {!Where_Clause} ORDER BY Personnel_Name__c ASC, Name DESC';
            String allFields = SQX_DynamicQuery.getFieldList(Schema.SQX_Personnel_Job_Function__c.getSObjectType().getDescribe().fields.getMap().values()).trim();
            dynamicQuery = dynamicQuery.replace('{!FieldList}', allFields);
            
            String whereClause = '';
            Id mainRecordId = mainRecord.Id;

            if(!includeDeactivatedPJF) {
                whereClause = 'WHERE SQX_Job_Function__c = :mainRecordId AND  Deactivation_Date__c = null';
            } else {
                whereClause = 'WHERE SQX_Job_Function__c = :mainRecordId';
            }

            dynamicQuery = dynamicQuery.replace('{!Where_Clause}', whereClause);
            activePJFController = new ApexPages.StandardSetController(Database.getQueryLocator(dynamicQuery));

            System.debug('Querying for ' + dynamicQuery);
            activePJFController.setPageSize(DEFAULT_PAGE_SIZE);
            oldIncludeDeactivatedPJF = includeDeactivatedPJF;
        }

        return activePJFController;
    }
    
    /**
     * This method returns the controller for Requirement 
     */
    public ApexPages.StandardSetController getRequirements() {
        if(activeRequirementController == null || includeDeactivatedRequirement != oldIncludeDeactivatedRequirement) {
            
            String dynamicQuery = 'SELECT {!FieldList} FROM SQX_Requirement__c {!Where_Clause} ORDER BY Training_Program_Step__c ASC';
            String allFields = SQX_DynamicQuery.getFieldList(Schema.SQX_Requirement__c.getSObjectType().getDescribe().fields.getMap().values()).trim();
            dynamicQuery = dynamicQuery.replace('{!FieldList}', allFields);
            
            String whereClause = '';
            Id mainRecordId = mainRecord.Id;

            if(!includeDeactivatedRequirement) {
                whereClause = 'WHERE SQX_Job_Function__c = :mainRecordId AND  Deactivation_Date__c = null';
                
            } else {
                whereClause = 'WHERE SQX_Job_Function__c = :mainRecordId';
            }

            dynamicQuery = dynamicQuery.replace('{!Where_Clause}', whereClause);
            activeRequirementController = new ApexPages.StandardSetController(Database.getQueryLocator(dynamicQuery));

            System.debug('Querying for ' + dynamicQuery);
            activeRequirementController.setPageSize(DEFAULT_PAGE_SIZE);
            oldIncludeDeactivatedRequirement = includeDeactivatedRequirement;
        }

        return activeRequirementController;
    }
}