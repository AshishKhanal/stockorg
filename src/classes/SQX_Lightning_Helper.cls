/**
 * This class contains the methods to interact with CQ lightning components for returning layout details etc. and rendering
 * them in the UI. Additionally, it provides a dynamic flow invocation endpoint too.
 */
public with sharing class SQX_Lightning_Helper {

    /**
     * Constants used for flow input and output and CQ layout
     */
    static final String FLOW_VARIABLE_IN_RECORDS = 'InRecords',
                        FLOW_VARIABLE_IN_ACTION = 'Action',
                        FLOW_VARIABLE_OUT_RECORDS = 'OutRecords',
                        FLOW_VARIABLE_EVAL_EXPR = 'EvaluationExpression',
                        /* Flow variable below this line are used by dynamically invoked flow, based on UI interaction */
                        FLOW_VARIABLE_SUCCESS_MESSAGE = 'SuccessMessage',
                        FLOW_VARIABLE_ERROR_MESSAGE = 'ErrorMessage',
                        FLOW_VARIABLE_INFO_MESSAGE = 'InfoMessage',
                        FLOW_VARIABLE_IMPACTED_FIELDS = 'ImpactedFields',
                        FLOW_VARIABLE_OUT_RECORD = 'OutRecord',
                        FLOW_VARIABLE_IN_RECORD = 'InRecord',
                        CACHE_RESOURCE_NAME = 'CQ_Resource_Cache',
                        CQ_INVOCABLE_ACTION_TYPE = 'Layout Initializer',
                        CQ_EMBEDDABLE_ACTION_TYPE = 'Action',
                        CQ_INVOCABLE_COMPONENT_TYPE = 'Flow';

    /**
     * A Data only class for encapsulating all the information returned by dynamic invocation of the flow
     */
    public class FlowReturn {

        /**
         * The field values of the object returned by the flow. Note: Since, SObject serialization isn't supported
         * this field provdes a String representation (Serialized) form for the SObject.
         */
        @AuraEnabled
        public String SObjValueJSON {get { return JSON.serialize(SObjValue); }}

        /**
         * The actual sobject that was returned by the flow.
         */
        transient public SObject SObjValue {get; set;}

        /**
         * The success message that is to be presented in the UI, returned by the flow.
         */
        @AuraEnabled
        public String SuccessMessage {get; set;}

        /**
         * The error message that is to be presented in the UI, returned by the flow.
         */
        @AuraEnabled
        public String ErrorMessage {get; set;}

        /**
         * The info message that is to be presented in the UI, returned by the flow.
         */
        @AuraEnabled
        public String InfoMessage {get; set;}

        /**
         * Represents the list of field values that are impacted by the flow. If the impacted field doesn't
         * have a value in SObjValue, it will reset/clear the value in the UI
         */
        @AuraEnabled
        public String[] ImpactedFields {get; set;}
    }

    /**
     * The method invokes the particular flow dynamically and then returns the processed object along with any message
     * @param namespace the namespace of the flow to be invoked
     * @param flowName the api name of the flow
     * @param obj the JSON representation of current model
     */
    @AuraEnabled
    public static FlowReturn invokeFlow(String namespace, String flowName, String obj) {
        Map<String, Object> inputVariableMap = new Map<String, Object>();
        inputVariableMap.put(FLOW_VARIABLE_IN_RECORD, JSON.deserialize(obj, SObject.class));

        Flow.Interview cqActionFlow = Flow.Interview.createInterview(namespace, flowName, inputVariableMap);
        cqActionFlow.start();

        FlowReturn fr = new FlowReturn();
        fr.SuccessMessage = (String)cqActionFlow.getVariableValue(FLOW_VARIABLE_SUCCESS_MESSAGE);
        fr.ErrorMessage = (String)cqActionFlow.getVariableValue(FLOW_VARIABLE_ERROR_MESSAGE);
        fr.InfoMessage = (String)cqActionFlow.getVariableValue(FLOW_VARIABLE_INFO_MESSAGE);
        fr.ImpactedFields = (String[])cqActionFlow.getVariableValue(FLOW_VARIABLE_IMPACTED_FIELDS);

        Map<Id, SObject> objectCache = new Map<Id, SObject>();
        SObject rec = (SObject)cqActionFlow.getVariableValue(FLOW_VARIABLE_OUT_RECORD);
        Map<String, SObjectField> fields = rec.getSObjectType().getDescribe().fields.getMap();
        for(SObjectField soField : fields.values()) {
            DescribeFieldResult fieldDescribe = soField.getDescribe();
            if(fieldDescribe.getType() == DisplayType.Reference) {
                SQX_Utilities.setReferenceField(rec, fieldDescribe, objectCache);
            }
        }
        fr.SObjValue = rec;

        return fr;
    }

    /**
     * This method returns the details that are used by CQ Lightning UI Layout, for showing the correct ui
     * @param objectType the sobject type for which the record type is to be returned
     * @param uiDefaults the JSON representation of the SObject if default value are being provided from the UI.
     *                   This value is forwarded to the default value provider flow so that it can take decisions based on it.
     * @param action the action that is being performed in the UI. Example create document, revise document etc.
     *               This value is forwarded to default value provider flow so that proper values may be suggested based on it.
     */
    @AuraEnabled
    public static SQX_Save_UI_Detail getDetails(String objectType, String uiDefaults, String action, Id recordId) {
        SQX_Save_UI_Detail detail = new SQX_Save_UI_Detail(objectType, uiDefaults, action, recordId);

        return detail;
    }

    /**
     * This method returns the applicable actions for the given purpose
     * @param  recordType the object whose applicable actions are to be returned
     * @param  purpose    the purpose for which actions are to be identified
     */
    @AuraEnabled
    public static CQ_Action__mdt[] getActionsFor(String recordType, String purpose){
        return [SELECT Component_Name__c, Component_Type__c, Namespace__c, compliancequest__Label__c, compliancequest__Purpose__c
            FROM CQ_Action__mdt
            WHERE Main_Record_API_Name__c = : recordType AND compliancequest__Purpose__c = : purpose
            AND CQ_Action_Type__c = : CQ_EMBEDDABLE_ACTION_TYPE
        ];
    }

    /**
     * Returns an object with specific Id for use in UI for all the fields. This used to transfer values dynamically in the UI
     * @param  recordId The Object Id whose field values are to be returned
     * @param  fields   The fields that are to be queried
     * @return          The SObject that queried
     */
    @AuraEnabled
    public static SObject getObjectWithId(Id recordId, String[] fields) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.operator = 'eq';
        filter.field = 'Id';
        filter.value = recordId;
        List<SObject> objs;
        DescribeSObjectResult objType = recordId.getSobjectType().getDescribe();
        if(fields == null) {
            objs = SQX_DynamicQuery.getallFields(objType, new Set<Schema.SObjectField>(), filter, new List<Schema.SObjectField>(), false);
        } else {
            objs = SQX_Extension_UI.getObjects(objType.getName(), fields, 1, filter);
        }

        return objs.isEmpty() ? null : objs[0];
    }

    /**
    * This method is used to complete SF task
    * @param 'objDetails' mainrecord details with json string format
    */
    @AuraEnabled
    public static string getCompleteTask(String objDetails){
        try{
            SObject sobjRecord = (SObject)JSON.deserialize(objDetails, SObject.class);

            User userInfo = SQX_Utilities.getCurrentUser();
            String assigneUserId = (String)sobjRecord.get('SQX_User__c');
            String stepId = (String)sobjRecord.get('Id');
            List<UserRecordAccess> accesses = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE RecordId = :stepId AND UserId = : userInfo.Id];
            
            if(userInfo.Id == assigneUserId || accesses.get(0).HasEditAccess){
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the assignee user might not have access to complete the task
                * This will cause and error.
                */
                new SQX_DB().withoutSharing().op_update(new List<SObject> { sObjRecord },new List<SObjectField> { });
            }else{
                AuraHandledException e = new AuraHandledException(Label.SQX_ERR_MSG_Don_t_have_access_to_complete_the_task);
                e.setMessage(Label.SQX_ERR_MSG_Don_t_have_access_to_complete_the_task);
                throw e;
            }
            
        }catch(Exception ex){
            AuraHandledException e = new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
            e.setMessage(ex.getMessage() + ex.getStackTraceString());
            throw e;
        }
        return null;
    }


    /**
     * This is a class that is used to pass all the required details to load the save UI. It will include
     * information of record types, default values for each record type and associated layout
     */
    public with sharing class SQX_Save_UI_Detail {
        /**
         * The label of the object, useful for displaying translated label in the UI
         */
        @AuraEnabled
        public String ObjectLabel {get; set;}

        /**
         * Indicates whether or not the object type has record types
         */
        @AuraEnabled
        public Boolean HasRecordTypes {get; set;}

        /**
         * Individual default values for and other details for a record type. Note: If an sobject
         * doesn't have a record type this list will still contain one item as 'Master' record type
         */
        @AuraEnabled
        public SQX_Save_UI_RecordType[] RecordTypes {get; set;}

        /**
         * The name of the resource which contains the details of layout. This is useful when customization
         * has created a different resource and it must be resolved dynamically.
         */
        @AuraEnabled
        public String LayoutName {get; set;}

        /**
         * Placeholder for returning any error to the UI.
         */
        @AuraEnabled
        public String Error {get; set; }

        /**
         * List of dynamic evaluation rules that need to be evaluated when UI manipulation is done. Each
         * Evaluation rule is a JSON of the following form returned by the flow.
         * [
         *   {
         *     "field": "<field name>",
         *     "operator": "eq|neq|changed|isnull|notnull",
         *     "value": "<value with eq or neq>",
         *     "setRequired": [
         *       <list of fields>
         *     ],
         *     "hideSection": [
         *       <list of section names to hide>
         *     ],
         *     "transfer": {
         *         "sourcefield": [ "target field" ... "target field"],
         *         ...
         *         "sourcefield": [ "target field" ... "target field"],
         *     },
         *     "invoke": { "ns": "<namespace>", "name": "<flow name>" }
         *   }
         * ]
         */
        @AuraEnabled
        public String[] EvalRules {get; set;}

        /**
         * The list of fields that required from the database level i.e. no validation rule only schema. This is useful
         * for tailoring standard lighting record edit form to behave consistently.
         */
        @AuraEnabled
        public List<String> DBRequiredFields {get; set;}

        /**
         * The list of fields that are readonly from database or policy level i.e. have updatable set to
         * false. This is useful for tailoring lightning record edit form to show readonly fields
         */
        @AuraEnabled
        public List<String> DBReadFields {get; set;}

        /**
         * Stores the field values of main record if record id has been passed
         */
        transient private SObject mainRecord;

        /**
         * Cache of current describe result
         */
        transient private Schema.DescribeSObjectResult sobjDescribe;

        /**
         * The default value input by the component.
         */
        transient private Map<String, Object> uiDefault;

        /**
         * The action for which the layout details are being generated.
         */
        transient private String action;

        /**
         * The object type for which the detail is being constructed
         */
        transient private String objType;

        /**
         * Constructor that loads all the required fields in the Lightning Details UI
         * @param  objType      The SObject Type for which the details are to be identified
         * @param  defaultValue The default value map provided by the lighting component
         * @param  action       the action that is being performed in the UI
         */
        public SQX_Save_UI_Detail(String objType, String defaultValue, String action, Id recordId) {
            try {
                uiDefault = String.isEmpty(defaultValue) ? null : (Map<String, Object>)JSON.deserializeUntyped(defaultValue);
                this.action = action;
                this.objType = objType;
                EvalRules = new List<String>();

                // replace all default value
                if(recordId != null) {
                    mainRecord = getObjectWithId(recordId, null);
                }

                loadDetailsForObject(recordId != null);
                loadDetailsForLayout();
            } catch(Exception ex) {
                Error = ex.getMessage();
            }
        }

        /**
         * Identify the name of the resource that needs to be loaded by the UI to get details of columns and fields in layout.
         * Note: The layout outside namespace prefix is preferred over CQ's original layout
         */
        private void loadDetailsForLayout() {
            for(StaticResource resource : [SELECT Name, NamespacePrefix FROM StaticResource WHERE Name =: CACHE_RESOURCE_NAME ORDER BY NamespacePrefix NULLS FIRST LIMIT 1] ) {
                LayoutName = (String.isBlank(resource.NamespacePrefix) ? '' : resource.NamespacePrefix + '__') + resource.Name;
            }
        }

        /**
         * Inspects the SObject schema to return details of the object type and invokes the flows for default values if required
         */
        private void loadDetailsForObject(Boolean loadObjectDefaults) {
            Map<String, SObjectType> globalSchema = Schema.getGlobalDescribe();
            System.debug(objType);
            sobjDescribe = globalSchema.get(objType).getDescribe();

            ObjectLabel = sobjDescribe.getLabel();
            loadRequiredAndReadFields();

            loadRecordTypes();
            loadDefaultValues();
        }

        /**
         * Inspects the metadata to identify the list of fields that are required and read from database/schema
         */
        private void loadRequiredAndReadFields() {
            DBRequiredFields = new List<String>();
            DBReadFields = new List<String>();
            Map<String, SObjectField> fields = sobjDescribe.fields.getMap();
            for(String field : fields.keySet()) {
                DescribeFieldResult fieldDesc = fields.get(field).getDescribe();
                if(!fieldDesc.isNillable()) {
                    DBRequiredFields.add(field);
                }
                if(!fieldDesc.isUpdateable()) {
                    DBReadFields.add(field);
                }
            }
        }

        /**
         * Inspects the metadata and identifies all the records types that are present in the object and sets them in the
         * record types list.
         */
        private void loadRecordTypes() {
            Map<Id, RecordTypeInfo> recordTypeInfos = sobjDescribe.getRecordTypeInfosById();
            HasRecordTypes = false;

            RecordTypes = new List<SQX_Save_UI_RecordType>();
            for(Id recordTypeId : recordTypeInfos.keySet()) {
                if(mainRecord == null || recordTypeInfos.size() == 1 || mainRecord.get('RecordTypeId') == recordTypeId) {
                    RecordTypeInfo info = recordTypeInfos.get(recordTypeId);
                    HasRecordTypes = HasRecordTypes || (!info.isMaster() && info.isAvailable());
                    if((info.isAvailable() && !info.isMaster()) || (info.isMaster() && recordTypeInfos.size() <= 1)) {
                        RecordTypes.add(new SQX_Save_UI_RecordType(info, sobjDescribe.sObjectType, uiDefault, mainRecord));
                    }
                }
            }
        }

        /**
         * Checks if any flow has to be invoked for default values and invokes the flow if present and saves the returned values.
         */
        private void loadDefaultValues() {
            for(CQ_Action__mdt cqAction : [SELECT Component_Name__c, Namespace__c FROM CQ_Action__mdt
                                              WHERE Main_Record_API_Name__c = : sobjDescribe.getName() AND CQ_Action_Type__c = : CQ_INVOCABLE_ACTION_TYPE
                                              AND Component_Type__c = :CQ_INVOCABLE_COMPONENT_TYPE
                                              ORDER By Namespace__c NULLS LAST]) {
                try {
                    invokeFlows(cqAction.Component_Name__c, cqAction.Namespace__c);
                } catch(Exception ex) {
                    throw new AuraHandledException(ex.getMessage() + ' : ' + ex.getTypeName());
                }
            }
        }

        /**
         * Internal method for dynamically invoke the flow which can provide the default value and evaluation rules
         * @param  flowname  The name of the flow which must be invoked to get the value
         * @param  namespace The namespace of the flow that is to be invoked.
         */
        private void invokeFlows(String flowname, String namespace) {
            Map<String, Object> inputVariableMap = getInputParam();
            Flow.Interview cqActionFlow = Flow.Interview.createInterview(namespace, flowName, inputVariableMap);

            cqActionFlow.start();
            SObject[] objects = (SObject[])cqActionFlow.getVariableValue(FLOW_VARIABLE_OUT_RECORDS);
            String evalRule = (String)cqActionFlow.getVariableValue(FLOW_VARIABLE_EVAL_EXPR);
            if(!String.isEmpty(evalRule)) {
                EvalRules.add(evalRule);
            }

            Integer idx = 0;
            if(objects != null) {
                
                for(SQX_Save_UI_RecordType rtype : RecordTypes) {
                    rtype.DefaultValue = objects[idx];
                    idx++;
                }   
            }
        }

        /**
         * Returns the input parameter (Sobject) for use in the flow to take decisions
         * @return   returns a list of parameters for the Default value flow
         */
        private Map<String, Object> getInputParam() {
            Map<String, Object> inputParams = new Map<String, Object>();
            List<SObject> sobjects = new List<SObject>();
            for(SQX_Save_UI_RecordType rtype : RecordTypes) {
                sobjects.add(rtype.DefaultValue);
            }
            inputParams.put(FLOW_VARIABLE_IN_RECORDS, sobjects);
            inputParams.put(FLOW_VARIABLE_IN_ACTION, action);

            return inputParams;
        }

    }

    /**
     * Contains the details regarding default values for a record type
     */
    public with sharing class SQX_Save_UI_RecordType {
        /**
         * The record type id for which this data is being created
         */
        @AuraEnabled
        public String RecordTypeId {get; set;}

        /**
         * The name of the record type
         */
        @AuraEnabled
        public String Name {get; set;}

        /**
         * The developer name for the record type
         */
        @AuraEnabled
        public String DeveloperName {get; set;}

        /**
         * Indicates whether or not the record type is default
         */
        @AuraEnabled
        public Boolean IsDefault {get; set;}

        /**
         * Serializes the DefaultValue for use in UI.
         */
        @AuraEnabled
        public String DefaultValueJSON {get { return JSON.serialize(DefaultValue); }}

        /**
         * The Defaultvalue for the record type
         */
        transient public SObject DefaultValue {get; set;}

        /**
         * Constructor for record type data
         * @param  info    the record type info for the record type that is being encapsulated
         * @param  objType the object type which is related to the object
         */
        public SQX_Save_UI_RecordType(RecordTypeInfo info, SObjectType objType, Map<String, Object> uiDefaults, SObject record) {
            RecordTypeId = info.isMaster() ? null : info.RecordTypeId;
            Name = info.Name;
            DeveloperName = info.DeveloperName;
            IsDefault = !info.isMaster() && info.DefaultRecordTypeMapping;
            DefaultValue = record == null ? objType.newSObject(RecordTypeId, true) : record;
            Map<String, SObjectField> fields = objType.getDescribe().fields.getMap();
            if(uiDefaults != null) {
                for(String key : uiDefaults.keySet()) {
                    if(fields.containsKey(key)) {
                        DescribeFieldResult fieldDescribe = fields.get(key).getDescribe();
                        if((DefaultValue.Id == null && fieldDescribe.isCreateable()) || (DefaultValue.Id != null && fieldDescribe.isUpdateable()) ){
                            DefaultValue.put(key, uiDefaults.get(key));
                        }
                    }
                }
            }
        }
    }
}