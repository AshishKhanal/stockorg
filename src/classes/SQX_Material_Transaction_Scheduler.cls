/**
* A scheduler class to schedule the batch jobs for processing incoming receipt material transactions.
* @story   SQX-2810 Inspection: Process Receipt
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-01-10
*/
global with sharing class SQX_Material_Transaction_Scheduler implements Schedulable {
    /*
    * Implementation of execute method for Material transaction batch job scheduler
    * @param SchedulableContext object
    * @return Nothing
    */
    global void execute(SchedulableContext sc){
        final String MATERIAL_TRANSACTION_BATCH_PROCESS = SQX_Material_Transaction_Job_Processor.JOB_NAME + (Test.isRunningTest() ? '-Test': '');        
        Set<String> jobStatuses = new Set<String>();
        for(CronTrigger tr : [SELECT CronJobDetail.Name, Id FROM CronTrigger WHERE CronJobDetail.Name LIKE : MATERIAL_TRANSACTION_BATCH_PROCESS]){
            jobStatuses.add(tr.CronJobDetail.Name);
        }
        if(!jobStatuses.contains(MATERIAL_TRANSACTION_BATCH_PROCESS)){
            SQX_Material_Transaction_Job_Processor processor = new SQX_Material_Transaction_Job_Processor();
            System.scheduleBatch(processor, SQX_Material_Transaction_Job_Processor.JOB_NAME, processor.SCHEDULE_AFTER_MIN, processor.BATCH_SZE);
        }
    }
}