/**
 * test class for voiding inspection
 */
@isTest
public with sharing class SQX_Test_2807_Void_Inspection {

    /**
     * TEST SCENARIOS:
     * 1. When inspection is in draft, it can be voided [givenAnInspectionInDraft_WhenInspectionIsVoided_InspectionIsVoided]
     * 2. When inspection is closed, it cannot be voided [givenAClosedInspection_WhenInspectionIsVoided_ErrorIsThrown]
     * 3. When inspection is open and voided by user without supervisor permission, error is thrown [givenAnOpenInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown]
     * 4. When inspection is open and voided by user with supervisor permission, it can be voided [givenAnOpenInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided]
     * 5. When inspection is completed and voided by user without supervisor permission, error is thrown [givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown]
     * 6. When inspection is completed and voided by user with supervisor permission, it can be voided [givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided]
     */
    
    public static Boolean runAllTests = true,
                            run_givenAnInspectionInDraft_WhenInspectionIsVoided_InspectionIsVoided = false,
                            run_givenAClosedInspection_WhenInspectionIsVoided_ErrorIsThrown = false,
                            run_givenAnOpenInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown = false,
                            run_givenAnOpenInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided = false,
                            run_givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown = false,
                            run_givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided = false;
    
    public static String PART_FAMILY_1 = 'PART_FAMILY_1',
                            PART_1 = 'PART_1',
                            PART_2 = 'PART_2',
                            PROCESS_1 = 'PROCESS_1';
    
    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * Given : An inspection in draft
    * When : Inspection is voided
    * Then : Inspection is voided
    */
    public testmethod static void givenAnInspectionInDraft_WhenInspectionIsVoided_InspectionIsVoided(){
        if (!runAllTests && !run_givenAnInspectionInDraft_WhenInspectionIsVoided_InspectionIsVoided) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            
            // Arrange : Create an inspection which is in draft
           SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id).setStatus(SQX_Inspection.INSPECTION_STATUS_DRAFT).save();

            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c 
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            System.assert(extension.getCanPerformAction(), 'User must be able to perform inspection');
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();
            inspect = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // Assert : Inspection is voided
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_VOID, inspect.Status__c);
        }
    }
    
    /**
    * Given : An inspection in closed
    * When : Inspection is voided
    * Then : Error is thrown
    */
    public testmethod static void givenAClosedInspection_WhenInspectionIsVoided_ErrorIsThrown(){
        if (!runAllTests && !run_givenAClosedInspection_WhenInspectionIsVoided_ErrorIsThrown) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and closed
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            // Assert : Inspection is closed
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            inspect = [SELECT Id, 
                               Name,
                               OwnerId,
                               Status__c,
                               Approval_Status__c,
                               Passed_Characteristics__c, 
                               Skipped_Characteristics__c,
                               Failed_Characteristics__c,
                               Total_Characteristics__c 
                       FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, inspect.Status__c);
            controller = new ApexPages.StandardController(inspect);
            extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();
            
            // Assert : Error is thrown
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_PREVENT_VOIDING_INSPECTION));
        }
    }
    
    /**
    * Given : An inspection in open status
    * When : Inspection is voided without supervisor permission
    * Then : Error is thrown
    */
    public testmethod static void givenAnOpenInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown(){
        if (!runAllTests && !run_givenAnOpenInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and initiated
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                               Name,
                               OwnerId,
                               Status__c,
                               Approval_Status__c,
                               Passed_Characteristics__c, 
                               Skipped_Characteristics__c,
                               Failed_Characteristics__c,
                               Total_Characteristics__c 
                       FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();
            
            // Assert : Error is thrown
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_PREVENT_VOIDING_INSPECTION));
        }
    }
    
    /**
    * Given : An inspection in open status
    * When : Inspection is voided by user with supervisor permission
    * Then : Inspection is voided
    */
    public testmethod static void givenAnOpenInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided(){
        if (!runAllTests && !run_givenAnOpenInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
            
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, SQX_Inspection.CUSTOM_PERMISSION_INSPECTION_SUPERVISOR);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and initiated
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                               Name,
                               OwnerId,
                               Status__c,
                               Approval_Status__c,
                               Passed_Characteristics__c, 
                               Skipped_Characteristics__c,
                               Failed_Characteristics__c,
                               Total_Characteristics__c 
                       FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();
            inspect = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // Assert : Inspection is voided
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_VOID, inspect.Status__c);
        }
    }
    
    /**
    * Given : An inspection in complete status
    * When : Inspection is voided by user without supervisor permission
    * Then : Error is thrown
    */
    public testmethod static void givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown(){
        if (!runAllTests && !run_givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithoutSupervisorPermission_ErrorIsThrown) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.inspection.Review_Required__c = true;
            inspection.inspection.SQX_Closure_Reviewer__c = adminUser.Id;
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            inspect = [SELECT Id, 
                            Name,
                            OwnerId,
                            Status__c,
                            Approval_Status__c,
                            Passed_Characteristics__c, 
                            Skipped_Characteristics__c,
                            Failed_Characteristics__c,
                            Total_Characteristics__c,
                            Review_Required__c
                       FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_COMPLETE, inspect.Status__c);
            
            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            
            controller = new ApexPages.StandardController(inspect);
            extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();
            
            // Assert : Error is thrown
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_PREVENT_VOIDING_INSPECTION));
        }
    }
    
    /**
    * Given : An inspection in complete status
    * When : Inspection is voided by user with supervisor permission
    * Then : Inspection is voided
    */
    public testmethod static void givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided(){
        if (!runAllTests && !run_givenAnCompleteInspection_WhenInspectionIsVoidedByUserWithSupervisorPermission_InspectionIsVoided) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, SQX_Inspection.CUSTOM_PERMISSION_INSPECTION_SUPERVISOR);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.inspection.Review_Required__c = true;
            inspection.inspection.SQX_Closure_Reviewer__c = adminUser.Id;
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            inspect = [SELECT Id, 
                            Name,
                            OwnerId,
                            Status__c,
                            Approval_Status__c,
                            Passed_Characteristics__c, 
                            Skipped_Characteristics__c,
                            Failed_Characteristics__c,
                            Total_Characteristics__c,
                            Review_Required__c
                       FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_COMPLETE, inspect.Status__c);
            
            // Act : Inspection is voided
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_VoidingInspection);
            
            controller = new ApexPages.StandardController(inspect);
            extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Void Inspection.';
            extension.saveRecord();

            inspect = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // Assert : Inspection is voided
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_VOID, inspect.Status__c);
        }
    }
}