/**
 *  This class defines the structure for the payload to be used when submitting
 *  a request to a CQ external service(currently in Heroku)
 */
public class SQX_External_Service_Payload {

    public enum JobType { GENERATE_XML, GENERATE_PDF, INSERT_CONTENT_TO_SF, GENERATE_PUBLIC_URL }
    public enum HttpMethod { GET, POST }

    Action[] actions;

    private transient integer jobNumber;

    public SQX_External_Service_Payload() {
        this.jobNumber = 0;
        this.actions = new Action[]{};
    }

    public String convertToJSONString() {
        return JSON.serialize(this);
    }

    public Action createAction() {
        Action newAction = new Action(this);
        this.actions.add(newAction);

        return newAction;
    }

    public JWTPrincipal getDefaultPrincipalForCurrentUser() {
        return new JWTPrincipal(UserInfo.getUserName(), SQX_Utilities.getLoginURL());
    }

    public class Action {

        transient SQX_External_Service_Payload payload;

        Job[] jobs;

        Action[] onSuccess;

        Action[] onFailure;

        public Action(SQX_External_Service_Payload payload) {
            this.payload = payload;
            this.jobs = new Job[]{};
            this.onSuccess = new Action[]{};
            this.onFailure = new Action[]{};
        }

        public Job addJob(JobType type, Integer[] dependentJobs, JobDetail detail) {
            Job newJob = new Job(this.payload.jobNumber++, type, dependentJobs, detail);
            this.jobs.add(newJob);

            return newJob;
        }

        public Action createSuccessAction() {
            Action newAction = new Action(this.payload);
            this.onSuccess.add(newAction);

            return newAction;
        }

        public Action createFailureAction() {
            Action newAction = new Action(this.payload);
            this.onFailure.add(newAction);

            return newAction;
        }
    }

    public class Job {

        Integer identifier;

        JobType type;

        Integer[] dependentJobs;

        JobDetail detail;

        public Job(Integer identifier, JobType type, Integer[] dependentJobs, JobDetail detail) {
            this.identifier = identifier;
            this.type = type;
            this.dependentJobs = dependentJobs;
            this.detail = detail;
        }

        // getters
        public Integer getIdentifier() {
            return this.identifier;
        }

    }

    public class JWTPrincipal {

        String username;

        String audience;

        public JWTPrincipal(String username, String audience) {
            this.username = username;
            this.audience = audience;
        }

    }

    public interface JobDetail {}

    public class HTTP_Request_Detail implements JobDetail {

        String endPoint;

        HttpMethod method;

        Map<String, String> headers;

        String body;

        JWTPrincipal principal;

        public HTTP_Request_Detail(String endpoint, HttpMethod method, String body, JWTPrincipal principal) {
            this.endPoint = endPoint;
            this.method = method;
            this.body = body;
            this.principal = principal;
            this.headers = new Map<String, String>();
        }

        public void addHeader(String key, String value) {
            this.headers.put(key, value);
        }
    }

}