@IsTest
public class SQX_Test_Defect{

    public static SQX_Part_Family__c  newPF1 = null;

    public static SQX_Defect__c prepareMockObject(Account account){
        SQX_Defect__c defect = new SQX_Defect__c();
        //defect.SQX_Account__c = account.Id;
        
        if(newPF1 == null){
            newPF1 = new SQX_Part_Family__c();
            newPF1.Name = 'dummyFamily';
            insert newPF1;
            

        }
        
          //add a part 
        SQX_Part__c myPart1 = new SQX_Part__c();
        mypart1.Name = 'newPart1';
        mypart1.Part_Number__c = 'prt111' + Math.Random();
        mypart1.Part_Risk_Level__c = 3;
        mypart1.Active__c = TRUE;
        mypart1.Part_Family__c = newPF1.id;
        Insert myPart1;   
    

        //add a part and then supplier part
        
        defect.SQX_Part__c = myPart1.ID;
        
        return defect;
    }
}