/**
* This class is used to test SI, SD, SE, and ST steps when edit with status 'completed' and resultb'Go' then Update * completion date internally
*/
@isTest
public class SQX_Test_7196_UpdateCompletionDate {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        System.runAs(adminUser){
            //Create NSI Record
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            SQX_Onboarding_Step__c obStep = new SQX_Onboarding_Step__c();
            obStep.SQX_Parent__c =nsi.nsi.Id;
            obStep.Name = 'NSITask';
            obStep.Status__c = SQX_NSI.STATUS_DRAFT;
            obStep.Step__c =1;
            obStep.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_TASK);
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { obStep },  new List<Schema.SObjectField>{});
            
            //Create SD Record
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();
            SQX_Deviation_Process__c sdStep = new SQX_Deviation_Process__c();
            sdStep.SQX_Parent__c =sd.sd.Id;
            sdStep.Name = 'NSITask';
            sdStep.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            sdStep.Step__c =1;
            sdStep.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, SQX_Steps_Trigger_Handler.RT_TASK);
            new SQX_DB().op_insert(new List<SQX_Deviation_Process__c> { sdStep },  new List<Schema.SObjectField>{});
            
            SQX_Test_Supplier_Escalation.createAccounts();           
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            //Create Supplier Escalation
            SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation(accounts[0]);
            se.save();
            SQX_Supplier_Escalation_Step__c seStep = new SQX_Supplier_Escalation_Step__c();
            seStep.SQX_Parent__c =se.supplierEscalation.Id;
            seStep.Name = 'SETask';
            seStep.Status__c = SQX_Supplier_Common_Values.STATUS_DRAFT;
            seStep.Step__c =1;
            seStep.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, SQX_Steps_Trigger_Handler.RT_TASK);
            new SQX_DB().op_insert(new List<SQX_Supplier_Escalation_Step__c> { seStep },  new List<Schema.SObjectField>{});
            
            //Create SI Record
            SQX_Test_Supplier_Interaction si= new SQX_Test_Supplier_Interaction().save();
            SQX_Supplier_Interaction_Step__c siStep = new SQX_Supplier_Interaction_Step__c();
            siStep.SQX_Parent__c =si.si.Id;
            siStep.Name = 'STTask';
            siStep.Status__c = SQX_Supplier_Common_Values.STATUS_DRAFT;
            siStep.Step__c =1;
            siStep.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, SQX_Steps_Trigger_Handler.RT_TASK);
            new SQX_DB().op_insert(new List<SQX_Supplier_Interaction_Step__c> { siStep },  new List<Schema.SObjectField>{});
        }
    }
    
    /**
    * GIVEN : given  records (SI, SD, ST and SE)
    * WHEN : when set status is complete and result is go
    * THEN : update completion date
    */  
    public static testMethod void givenRecords_WhenEditTheStatusAndResult_ThenUpdateCompletionDate(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Get NSI step
            SQX_Onboarding_Step__c nsiStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: 
                                              [SELECT Id FROM SQX_New_Supplier_Introduction__c]];

            //Act: Update step with status 'completed' and result 'Go'                                  
            nsiStep.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            nsiStep.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            update nsiStep;

            //Assert: Ensured that completion date updated
            SQX_Onboarding_Step__c resultNsiStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Onboarding_Step__c];
            System.assertNotEquals(null, resultNsiStep.Completion_Date__c);

            //Arrange: Get SD step
            SQX_Deviation_Process__c sdStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: 
                                              [SELECT Id FROM SQX_Supplier_Deviation__c]];

            //Act: Update step with status 'completed' and result 'Go' 
            sdStep.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            sdStep.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            update sdStep;

            //Assert: Ensured that completion date updated
            SQX_Deviation_Process__c resultSDStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Deviation_Process__c];
            System.assertNotEquals(null, resultSDStep.Completion_Date__c);

            //Arrange: Get SE step
            SQX_Supplier_Escalation_Step__c seStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: [SELECT Id FROM SQX_Supplier_Escalation__c]];

            //Act: Update step with status 'completed' and result 'Go'
            seStep.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            seStep.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            update seStep;

            //Assert: Ensured that completion date updated
            SQX_Supplier_Escalation_Step__c resultSEStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Supplier_Escalation_Step__c];
            System.assertNotEquals(null, resultSEStep.Completion_Date__c);

            //Arrange: Get ST step
            SQX_Supplier_Interaction_Step__c siStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: [SELECT Id FROM SQX_Supplier_Interaction__c]];

            //Act: Update step with status 'completed' and result 'Go'
            siStep.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            siStep.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            update siStep;

            //Assert: Ensured that completion date updated
            SQX_Supplier_Interaction_Step__c resultSIStep = [SELECT Id, Status__c, Result__c, Completion_Date__c FROM SQX_Supplier_Interaction_Step__c];
            System.assertNotEquals(null, resultSIStep.Completion_Date__c);
        }
    }
}