/**
* This class will contain the common methods and static strings required for the Equipment object 
*/
public with sharing class SQX_Extension_Equipment extends SQX_Controller_Sign_Off{
    
    private SQX_Equipment__c mainRecord = null;
    private SQX_Equipment__c mainRecordBeforeChange = null;
    private ApexPages.StandardController controller = null;
    
    /**
     * Add default policies
     */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            SQX_Equipment.PurposeOfSignature_Status_Change => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Equipment.PurposeOfSignature_Date_Change => this.POLICY_NO_COMMENT_REQUIRED
            
        };
        return defaultEsigPolicies;
    }

    /**
    */
    public SQX_Extension_Equipment(ApexPages.StandardController controller){
        
        super(controller);
        //getting equipment record from controller
        mainRecord = (SQX_Equipment__c)controller.getRecord();
        this.controller = controller;
        mainRecordBeforeChange = mainRecord.clone(true, true, true, true); // exact duplicate record of the main record to compare changes
        this.purposeOfSigMap.putAll( SQX_Equipment.purposeOfSigMap );
        recordId = mainRecord.Id;
    }
    
    /**
    * Updates equipment object with record activity
    */
    public PageReference updateWithRecordActivity(String purposeOfSignature, String activityCode, String changelist) {
        PageReference returnUrl = null;
        SavePoint sp = null;
        
        if(hasValidCredentials()) {
            try {
                //set savepoint
                sp = Database.setSavePoint();

                // updating the equipment record
                // any other user can change the equipment record with or without enough permission
                Database.SaveResult saveResult = new SQX_DB().withoutSharing().continueOnError().op_update(new List<SQX_Equipment__c>{ mainRecord }, new List<Schema.SObjectField>{
                    SQX_Equipment__c.fields.Equipment_Status__c,
                    SQX_Equipment__c.fields.Requires_Periodic_Calibration__c,
                    SQX_Equipment__c.fields.Last_Calibration_Date__c,
                    SQX_Equipment__c.fields.Next_Calibration_Date__c,
                    SQX_Equipment__c.fields.Calibration_Interval__c
                }).get(0);
                
                // if updating Equipment record is sucess
                if (saveResult.isSuccess()) {
                    // insert record activity
                    String activityComment = changelist + '\n' + RecordActivityComment;
                    SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Equipment__c.SObjectType), mainRecord.Id, (String)mainRecord.Id, activityComment, purposeOfSignature, activityCode);
                    
                    // set return url
                    PageReference defRetURL = new ApexPages.StandardController(mainRecord).View();
                    returnUrl = SQX_Utilities.getValidReturnUrl(null, defRetURL);
                }
                else{
                    for(Database.Error error : saveResult.getErrors()){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                    }
                    
                    Database.rollback(sp);
                }
            } catch(Exception ex) {
                // ignore error cause these will have been added by the dml exceptions
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                
                // rollback savepoint
                if (sp != null) {
                    Database.rollback(sp);
                }
            } 
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        
        return returnUrl;
    }
    
    /**
    * this method is called to create equipment record activity when Last Calibration Activity and Calibration Interval is updated in Equipment
    */
    public PageReference changeCalibrationDate() {
        //if Required Perodic Calibration is set false, then Next Calibration Date should be null
        if (!mainRecord.Requires_Periodic_Calibration__c) {
            mainRecord.Next_Calibration_Date__c = null;
        }
        
        // get change list
        List<String> changelist = SQX_Equipment.getChangeList(mainRecordBeforeChange, mainRecord, new List<Schema.SObjectField>{
                                                                                                    SQX_Equipment__c.fields.Requires_Periodic_Calibration__c,
                                                                                                    SQX_Equipment__c.fields.Calibration_Interval__c,
                                                                                                    SQX_Equipment__c.fields.Last_Calibration_Date__c,
                                                                                                    SQX_Equipment__c.fields.Next_Calibration_Date__c
        });
        
        if (changelist.size() == 0) {
            // show error message for no changes
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_EQUIPMENT_CHANGE_DATE_FIELDS_NOT_CHANGED));
            return null;
        }
        
        return updateWithRecordActivity(purposeOfSigMap.get(SQX_Equipment.PurposeOfSignature_Date_Change), SQX_Equipment.PurposeOfSignature_Date_Change, String.join(changelist, '\n'));
    }
    
    /**
    * To change the status of Equipment Record.
    */
    public PageReference changeStatus() {
        // get change list
        List<String> changelist = SQX_Equipment.getChangeList(mainRecordBeforeChange, mainRecord, new List<Schema.SObjectField>{ SQX_Equipment__c.fields.Equipment_Status__c });
        
        if (changelist.size() == 0) {
            // show error message for no status change
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_EQUIPMENT_STATUS_NOT_CHANGED));
            return null;
        }
        
        return updateWithRecordActivity(purposeOfSigMap.get(SQX_Equipment.PurposeOfSignature_Status_Change), SQX_Equipment.PurposeOfSignature_Status_Change, String.join(changelist, '\n'));
    }

    /**
     * action method that ensures the current action is for changing calibration date.
     */
    public void ChangeCalibrationDateAction(){
        actionName = SQX_Equipment.PurposeOfSignature_Date_Change;
        purposeOfSignature = SQX_Equipment.purposeOfSigMap.containsKey(actionName) ? SQX_Equipment.purposeOfSigMap.get(actionName) : '';
    }
    
    /**
     * action method that ensures the current action is for changing equipment status.
     */
    public void ChangeStatusAction(){
        actionName = SQX_Equipment.PurposeOfSignature_Status_Change;
        purposeOfSignature = SQX_Equipment.purposeOfSigMap.containsKey(actionName) ? SQX_Equipment.purposeOfSigMap.get(actionName) : '';
    }
}