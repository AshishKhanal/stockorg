/**
* this is a test class for covering the bug SQX-715
* It ensures that an actionplan included in a Non-conformance is not used to spawn an Action(Implementation).
*/
@IsTest
public class SQX_Test_715_NC_ActionPlan{
    
    /**
    * In line with SQX-715 this test ensures that no Action is created for an action plan
    * submitted in an NC. This is because only CAPA Action Plan should be converted to an
    * Action
    */
    public static testmethod void givenActionPlanInNC_ActionIsNotCreated(){
    
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            nc = new SQX_Test_NC(true);
            nc.nc.Investigation_Approval__c = false;

            nc.setDispositionRules(false, null, false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();

            nc.nc.OwnerId = ncCoordinator.Id;

            nc.save();
        }

        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();

            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            response.investigationsIncluded.get(0).addAnActionPlan(null, 'All done', null, true, Date.today());
            
            boolean exceptionOccurred = false;
            try{
                response.publishResponse();
            }
            catch(Exception ex){
                exceptionOccurred = true;
            }
            
            System.assertEquals(false, exceptionOccurred);
            nc.synchronize();

            //System.assertEquals( SQX_NC.STATUS_COMPLETE, nc.nc.Status__c);
        }
    }
}