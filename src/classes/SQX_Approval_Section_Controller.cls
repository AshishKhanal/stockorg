/**
 * This class is used to return Approval History information of a Controlled Document.
 */
public with sharing class SQX_Approval_Section_Controller {

    /**
     * Holds Id of Controlled Document from the VF Component
     */
    public Id docId{get; set;}

    /**
     * This method returns the latest approval process for the current document based on the Change order reference or Controlled Doc itself.
     * If the reference to change order is present, the priority is given to the Change order approvals.
     * @return ProcessInstanceHistory
     */
    public List<ProcessInstanceHistory> getDocApprovalHistory() {

        SQX_Controlled_Document__c document = [SELECT compliancequest__SQX_Change_Order__c, compliancequest__Document_Status__c, compliancequest__SQX_Change_Order__r.compliancequest__Status__c, compliancequest__SQX_Change_Order__r.compliancequest__Approval_Status__c, compliancequest__Document_Number__c, compliancequest__Approval_Status__c
                              FROM compliancequest__SQX_Controlled_Document__c
                              WHERE Id = : docId];

        boolean hasChangeOrder = document.compliancequest__SQX_Change_Order__c != null;
        boolean isChangeOrderImplementationApproved = false;
        isChangeOrderImplementationApproved = hasChangeOrder && document.compliancequest__SQX_Change_Order__r.compliancequest__Approval_Status__c == SQX_Change_Order.APPROVAL_STATUS_IMPLEMENTATION_APPROVED;

        List<ProcessInstanceHistory> approvalHistory = new List<ProcessInstanceHistory>();

        if((hasChangeOrder && !isChangeOrderImplementationApproved)
                   ||(document.compliancequest__Approval_Status__c ==null)){
            return approvalHistory;
        }
        Id targetId = isChangeOrderImplementationApproved ? document.compliancequest__SQX_Change_Order__c : docId;

        approvalHistory = SQX_Approval_Util.getApprovedProcessInstanceHistoryItems(targetId);

        return approvalHistory;
    }
}