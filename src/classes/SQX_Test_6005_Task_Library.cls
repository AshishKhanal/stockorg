/**
 * test class for task library
 */
@isTest
public class SQX_Test_6005_Task_Library {
    /**
     * GIVEN : NSI Task is created
     * WHEN : NSI Task is saved without step
     * THEN : Error is thrown
     * 
     * WHEN : NSI Task is saved with step
     * THEN : NSI Task is saved
     * 
     * @date: 2018/07/04
     * @story: [SQX-6005]
     */
    public static testMethod void givenNSITask_WhenStepIsBlank_ErrorIsThorwn(){
        
        // ARRANGE : NSI Task is created
        SQX_Task__c pTask = new SQX_Task__c(Name = 'Test_Task', 
                                            Allowed_Days__c = 10, 
                                            Record_Type__c = SQX_Task.RTYPE_SUPPLIER_INTRODUCTION,
                                            Task_Type__c = SQX_Task.TASK_TYPE_APPROVAL);
        
        // ACT : NSI Task is saved without step
        Database.SaveResult result = Database.insert(pTask, false);
        
        // ASSERT : Error is thrown
        System.assert(!result.isSuccess(), 'Save is not successful');
        System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Step is required.'));
        
        pTask.Step__c = 1;
        
        // ACT : NSI Task is saved with step
        result = Database.insert(pTask, false);
        
        // ASSERT : NSI Task is saved
        System.assert(result.isSuccess(), 'Save is successful');
    }
    
    /**
     * GIVEN : SD Task is created
     * WHEN : SD Task is saved without step
     * THEN : Error is thrown
     * 
     * WHEN : SD Task is saved with step
     * THEN : SD Task is saved
     * 
     * @date: 2018/07/04
     * @story: [SQX-6005]
     */
    public static testMethod void givenSDTask_WhenStepIsBlank_ErrorIsThorwn(){
        
        // ARRANGE : SD Task is created
        SQX_Task__c pTask = new SQX_Task__c(Name = 'Test_Task', 
                                            Allowed_Days__c = 10, 
                                            Record_Type__c ='Supplier Deviation',
                                            Task_Type__c = SQX_Task.TASK_TYPE_APPROVAL);
        
        // ACT : SD Task is saved without step
        Database.SaveResult result = Database.insert(pTask, false);
        
        // ASSERT : Error is thrown
        System.assert(!result.isSuccess(), 'Save is not successful');
        System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Step is required.'));
        
        pTask.Step__c = 1;
        
        // ACT : SD Task is saved with step
        result = Database.insert(pTask, false);
        
        // ASSERT : SD Task is saved
        System.assert(result.isSuccess(), 'Save is successful');
    }
    
    /**
     * GIVEN : Supplier Interaction Task is created
     * WHEN : SI Task is saved without step
     * THEN : Error is thrown
     * 
     * WHEN : SI Task is saved with step
     * THEN : SI Task is saved
     * 
     * @date: 2018/07/04
     * @story: [SQX-6005]
     */
    public static testMethod void givenSITask_WhenStepIsBlank_ErrorIsThorwn(){
        
        // ARRANGE : SD Task is created
        SQX_Task__c pTask = new SQX_Task__c(Name = 'Test_Task', 
                                            Allowed_Days__c = 10, 
                                            Record_Type__c ='Supplier Interaction',
                                            Task_Type__c = SQX_Task.TASK_TYPE_APPROVAL);
        
        // ACT : SD Task is saved without step
        Database.SaveResult result = Database.insert(pTask, false);
        
        // ASSERT : Error is thrown
        System.assert(!result.isSuccess(), 'Save is not successful');
        System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Step is required.'));
        
        pTask.Step__c = 1;
        
        // ACT : SD Task is saved with step
        result = Database.insert(pTask, false);
        
        // ASSERT : SD Task is saved
        System.assert(result.isSuccess(), 'Save is successful');
    }
    
    /**
     * GIVEN : Supplier Escalation Task is created
     * WHEN : SE Task is saved without step
     * THEN : Error is thrown
     * 
     * WHEN : SE Task is saved with step
     * THEN : SE Task is saved
     * 
     * @date: 2018/07/04
     * @story: [SQX-6005]
     */
    public static testMethod void givenSETask_WhenStepIsBlank_ErrorIsThorwn(){
        
        // ARRANGE : SD Task is created
        SQX_Task__c pTask = new SQX_Task__c(Name = 'Test_Task', 
                                            Allowed_Days__c = 10, 
                                            Record_Type__c ='Supplier Escalation',
                                            Task_Type__c = SQX_Task.TASK_TYPE_APPROVAL);
        
        // ACT : SD Task is saved without step
        Database.SaveResult result = Database.insert(pTask, false);
        
        // ASSERT : Error is thrown
        System.assert(!result.isSuccess(), 'Save is not successful');
        System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Step is required.'));
        
        pTask.Step__c = 1;
        
        // ACT : SD Task is saved with step
        result = Database.insert(pTask, false);
        
        // ASSERT : SD Task is saved
        System.assert(result.isSuccess(), 'Save is successful');
    }
}