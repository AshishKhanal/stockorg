/**
* This extension acts as a redirector for audit record as explained in [SQX-1484].
* It redirects to standard UI if the user is planning an audit in Audit Program, else it redirects to full UI.
*/
public with sharing class SQX_Extension_Audit_Selector {

    public final PageReference CUSTOM_AUDIT_UI = Page.SQX_Audit;

    private SQX_Audit__c mainRecord;
    ApexPages.StandardController audController;
    
    /**
    * Default constructor that intializes the redirector
    */
    public SQX_Extension_Audit_Selector(ApexPages.StandardController auditController) {
        audController = auditController;
        mainRecord = (SQX_Audit__c)auditController.getRecord();
    }

    /**
    * Checks if the audit is in plan or approval.
    * @return <code>true</code> if audit is in plan or approval status, <code>false</code> if audit is in other state
    */ 
    private boolean isInPlanOrApproval(){
        return (mainRecord.id==null || (mainRecord.Stage__c == SQX_Audit.STAGE_PLAN || mainRecord.Stage__c==SQX_Audit.STAGE_PLAN_APPROVAL));
    }

    /**
    * Checks if the audit belongs to a program or not
    * @return <code>true</code> if audit program has been set in the audit, else <code>false</code> otherwise
    */
    private boolean isFromProgram(){
        boolean isAssociatedWithProgram = false; 
        String programId = getParentsObjectId(ApexPages.currentPage().getParameters());
        try{
            //it is associated with a program if the mainrecord has audit program, which happens when editing a record
            //or when the passed program id i.e. url param is valid program id.
            isAssociatedWithProgram = mainRecord.SQX_Audit_Program__c != null || (programId != null && Id.valueOf(programId).getSObjectType() == SQX_Audit_Program__c.SObjectType);
        }
        catch(Exception ex){
            System.debug(ex);
        }

        return isAssociatedWithProgram;
    }

    /**
    * Returns the page reference where the user should be redirected to.
    */
    private PageReference getRedirectionURL(boolean isEditMode){
        PageReference ref;
        //if audit is in plan and belongs to program show standard layout
        //if audit is not in plan show VF
        if (isInPlanOrApproval() && isFromProgram()){
            ref = new PageReference('/' 
                    + (mainRecord.Id != null ? (String)mainRecord.Id : SQX_Audit__c.SObjectType.getDescribe().getKeyPrefix())
                    + (isEditMode ? '/e' : ''));
        }else{
            ref = CUSTOM_AUDIT_UI;    //todo:find overridden path
            if(isEditMode)
                ref.getParameters().put('editMode', 'true');         
        }

        Map<String, String> queryParams = ref.getParameters();
        queryParams.putAll(ApexPages.currentPage().getParameters());
        queryParams.put('nooverride', '1');
        queryParams.remove('save_new');//need to remove in final url otherwise SF will give error
        queryParams.remove('_CONFIRMATIONTOKEN');  
        return ref;
    }

    /**
    * redirector for edit links
    */
    public PageReference editorURL(){
        return getRedirectionURL(true);
    }

    /**
    * redirector for view links
    */
    public PageReference url() {
        return getRedirectionURL(false);
    }

    /**
    * This is a SF hack that checks if the passed URL contains lkid param which is used by SF to pass
    * parent's id to the client.
    * @return returns the parent sobject id i.e. the audit program's id in this case.
    */
    private Id getParentSObjectId(Map<String, String> params) {
        for (String key : params.keySet()) {
            if (key.endsWith('_lkid')) {
                return params.get(key);
            }
        }
        return null;
    }
}