/**
 * This unit test class used for to complete SF task only assignee users
 */ 
@isTest
public class SQX_Test_8767_Step_Assignee {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
    }
    
    /**
    * Given : create nsi record with open step. 
    * When : another user try to complete the SF task
    * Then : error message thrown.
    */
    @isTest
    public static void givenNSIWithOpenStep_WhenAnotherUserTryToCompleteTheSFTask_ThenThrowErrorMesssage() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser }); 
        //Arrange: Create NSI Record with open step.
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, SQX_NSI.TASK_TYPE_TASK, assigneeUser, 1);
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            nsi.submit();
            nsi.initiate();
            List<Task> tskList =[SELECT Id FROM Task WHERE OwnerId =: assigneeUser.Id];
            System.assertEquals(1, tskList.size());
        }
        
        //Act: Another user try to complete the SF task
        System.runAs(standardUser){
            SQX_Onboarding_Step__c onboarding =[SELECT Id, SQX_User__c FROM SQX_Onboarding_Step__c WHERE SQX_User__c =: assigneeUser.Id];
            String jsonStr = '{"attributes":{"type":"compliancequest__SQX_Onboarding_Step__c","url":"/services/data/v450/sobjects/compliancequest__SQX_Onboarding_Step__c/'+onboarding.Id+'"},"Id":"'+onboarding.Id+'","compliancequest__SQX_User__c":"'+onboarding.SQX_User__c+'" }';
            try{
                SQX_Lightning_Helper.getCompleteTask(jsonStr);
            }catch(Exception ex){
                String errMessage = ex.getMessage();
                //Assert: Ensured that thrown error message
                System.assert(errMessage.contains(Label.SQX_ERR_MSG_Don_t_have_access_to_complete_the_task), Label.SQX_ERR_MSG_Don_t_have_access_to_complete_the_task);
            }
        }
        
        //Act: Assignee User to complete the SF task
        System.runAs(assigneeUser){
            SQX_Onboarding_Step__c onboarding =[SELECT Id, SQX_User__c FROM SQX_Onboarding_Step__c WHERE SQX_User__c =: assigneeUser.Id];
            String jsonStr = '{"attributes":{"type":"compliancequest__SQX_Onboarding_Step__c","url":"/services/data/v450/sobjects/compliancequest__SQX_Onboarding_Step__c/'+onboarding.Id+'"},"Id":"'+onboarding.Id+'","compliancequest__SQX_User__c":"'+onboarding.SQX_User__c+'","compliancequest__Result__c":"Go","compliancequest__Status__c":"Complete" }';
            
            SQX_Lightning_Helper.getCompleteTask(jsonStr);
            List<Task> tskList =[SELECT Id, Status FROM Task WHERE OwnerId =: assigneeUser.Id];
            //Assert: Ensured that SF task completed successfully
            System.assertEquals('Completed', tskList[0].Status);
            
        }
    }
}