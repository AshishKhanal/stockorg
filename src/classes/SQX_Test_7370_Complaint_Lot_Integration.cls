/**
 * test class for complaint lot integration
 */
@isTest
public class SQX_Test_7370_Complaint_Lot_Integration {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        partFamily.Name = 'COMPUTER';
        insert partFamily;
        
        SQX_Part__c newPart = new SQX_Part__c();
        newPart.Name = 'MOUSE';
        newPart.Part_Number__c = 'MOUSE-001';
        newPart.Part_Risk_Level__c = 3;
        newPart.Active__c = true;
        newPart.Part_Family__c = partFamily.Id;
        
        insert newPart;
        
        SQX_Lot_Info__c lotInfo = new SQX_Lot_Info__c();
        lotInfo.Name = 'LOT_001';
        lotInfo.Part_Number__c = 'MOUSE-001';
        lotInfo.Manufacturing_Date__c = Date.today();
        lotInfo.Expiration_Date__c = Date.today().addDays(10);
        insert lotInfo;
        }
    }

    /**
     * GIVEN : A lot info is created
     * WHEN : Manufacturing Date and Exipration Date are entered
     * THEN : Record with proper date saves record otherwise fails
     */
    public testMethod static void givenLotInfo_MfgAndExpDateIsEntered_RecordIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Lot Info record is created
            SQX_Lot_Info__c lotInfo = new SQX_Lot_Info__c();
            lotInfo.Name = 'LOT_001';
            lotInfo.Part_Number__c = 'MOUSE-001';

            // ACT : Both manufacturing date and expiration date is entered
            lotInfo.Manufacturing_Date__c = Date.today();
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            Database.SaveResult result = Database.insert(lotInfo, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Both manufacturing date and expiration date is blank
            lotInfo.Manufacturing_Date__c = null;
            lotInfo.Expiration_Date__c = null;
            result = Database.update(lotInfo, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');
            
            // ACT : Manufacturing Date is blank
            lotInfo.Manufacturing_Date__c = Date.today();
            lotInfo.Expiration_Date__c = null;
            result = Database.update(lotInfo, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is blank
            lotInfo.Manufacturing_Date__c = null;
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(lotInfo, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is greater than manufacturing date
            lotInfo.Manufacturing_Date__c = Date.today().addDays(15);
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(lotInfo, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Expiration Date should be greater than Manufacturing Date.'), result.getErrors());

            // ACT : Expiration Date is greater than manufacturing date
            lotInfo.Manufacturing_Date__c = Date.today().addDays(5);
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(lotInfo, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Future date not allowed in Manufacturing Date'), result.getErrors());
        }
    }

    /**
     * GIVEN : A association item is created
     * WHEN : Manufacturing Date and Exipration Date are entered
     * THEN : Record with proper date saves record otherwise fails
     */
    public testMethod static void givenAssociationItem_MfgAndExpDateIsEntered_RecordIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Association item record is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            SQX_Complaint_Associated_Item__c associationItem = new SQX_Complaint_Associated_Item__c();
            associationItem.SQX_Part__c = [SELECT Id FROM SQX_Part__c].get(0).Id;
            associationItem.SQX_Part_Family__c = [SELECT Id FROM SQX_Part_Family__c].get(0).Id;
            associationItem.SQX_Complaint__c = complaint.complaint.Id;

            // ACT : Both manufacturing date and expiration date is entered
            associationItem.Manufacturing_Date__c = Date.today();
            associationItem.Expiration_Date__c = Date.today().addDays(10);
            Database.SaveResult result = Database.insert(associationItem, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Both manufacturing date and expiration date is blank
            associationItem.Manufacturing_Date__c = null;
            associationItem.Expiration_Date__c = null;
            result = Database.update(associationItem, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');
            
            // ACT : Manufacturing Date is blank
            associationItem.Manufacturing_Date__c = Date.today();
            associationItem.Expiration_Date__c = null;
            result = Database.update(associationItem, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is blank
            associationItem.Manufacturing_Date__c = null;
            associationItem.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(associationItem, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is greater than manufacturing date
            associationItem.Manufacturing_Date__c = Date.today().addDays(15);
            associationItem.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(associationItem, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Expiration Date should be greater than Manufacturing Date.'), result.getErrors());

            // ACT : Expiration Date is greater than manufacturing date
            associationItem.Manufacturing_Date__c = Date.today().addDays(5);
            associationItem.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(associationItem, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Future date not allowed in Manufacturing Date'), result.getErrors());
        }
    }

    /**
     * GIVEN : A complaint is created
     * WHEN : Manufacturing Date and Exipration Date are entered
     * THEN : Record with proper date saves record otherwise fails
     */
    public testMethod static void givenComplaint_MfgAndExpDateIsEntered_RecordIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Association item record is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);

            // ACT : Both manufacturing date and expiration date is entered
            complaint.complaint.Manufacturing_Date__c = Date.today();
            complaint.complaint.Expiration_Date__c = Date.today().addDays(10);
            complaint.save();
            
			SQX_Complaint__c comp = [SELECT Id FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            //ASSERT : Complaint is saved
            System.assert(comp.Id != null, 'Save is successful');
            
            // ACT : Both manufacturing date and expiration date is blank
            comp.Manufacturing_Date__c = null;
            comp.Expiration_Date__c = null;
            Database.SaveResult result = Database.update(comp, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');
            
            // ACT : Manufacturing Date is blank
            comp.Manufacturing_Date__c = Date.today();
            comp.Expiration_Date__c = null;
            result = Database.update(comp, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is blank
            comp.Manufacturing_Date__c = null;
            comp.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(comp, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');

            // ACT : Expiration Date is greater than manufacturing date
            comp.Manufacturing_Date__c = Date.today().addDays(15);
            comp.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(comp, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Expiration Date should be greater than Manufacturing Date.'), result.getErrors());

            // ACT : Expiration Date is greater than manufacturing date
            comp.Manufacturing_Date__c = Date.today().addDays(5);
            comp.Expiration_Date__c = Date.today().addDays(10);
            result = Database.update(comp, false);
            
            // ASSERT : Save is not successful
            System.assert(!result.isSuccess(), 'Expected save to be not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Future date not allowed in Manufacturing Date'), result.getErrors());
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Lot Info is selected
     * THEN : Details from lot info are copied
     */
    public testMethod static void givenAComplaint_WhenPartIsAdded_LotInfoDetailsAreCopied(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            
            // ACT : Lot Info is added
            complaint.complaint.SQX_Lot_Info__c = [SELECT Id FROM SQX_Lot_Info__c].get(0).Id;
            
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);
            Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint__c comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Lot Info details are copied
            System.assertEquals([SELECT Id FROM SQX_Part_Family__c].get(0).Id, comp.SQX_Part_Family__c);
            System.assertEquals([SELECT Id FROM SQX_Part__c].get(0).Id, comp.SQX_Part__c);
            System.assertEquals('LOT_001', comp.Lot_Number__c);
            System.assertEquals(Date.today(), comp.Manufacturing_Date__c);
            System.assertEquals(Date.today().addDays(10), comp.Expiration_date__c);
            
        }
    }
    
    /**
     * GIVEN : A complaint is created by selecting lot info
     * WHEN : Part and Lot Number is changed
     * THEN : Error is thrown
     */
    public testMethod static void givenAComplaintWithLotInfo_WhenParOrLotIsChanged_ErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();

            complaint.complaint.SQX_Lot_Info__c = [SELECT Id FROM SQX_Lot_Info__c].get(0).Id;

            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);
            Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint__c comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');

            // ACT : Lot Number is updated
            comp.Lot_Number__c = 'UPDATED_LOT';
            comp.Id = complaint.complaint.Id;
            Database.SaveResult result = Database.update(comp, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to fail');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Part and Lot Number cannot be changed when Lot Info is selected'), result.getErrors());

            SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
            partFamily.Name = 'UTILITIES';
            insert partFamily;
            
            SQX_Part__c newPart = new SQX_Part__c();
            newPart.Name = 'ROUTER';
            newPart.Part_Number__c = 'ROUTER-001';
            newPart.Part_Risk_Level__c = 3;
            newPart.Active__c = true;
            newPart.Part_Family__c = partFamily.Id;
            
            insert newPart;

            // ACT : Part is updated
            comp.Lot_Number__c = 'LOT_001';
            comp.SQX_Part__c = newPart.Id;
            result = Database.update(comp, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to fail');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Part and Lot Number cannot be changed when Lot Info is selected'), result.getErrors());
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Lot info is selected with part number whose part does not exist
     * THEN : Part will be blank in complaint
     */
    public testMethod static void givenAComplaintWithLotInfo_WhenLotInfoIsSelected_BlankPartIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();

            SQX_Lot_Info__c lotInfo = new SQX_Lot_Info__c();
            lotInfo.Name = 'LOT_001';
            lotInfo.Part_Number__c = 'MOUSE-002';
            lotInfo.Manufacturing_Date__c = Date.today();
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            insert lotInfo;
        
            // ACT : Lot Info is linked
            complaint.complaint.SQX_Lot_Info__c = lotInfo.Id;

            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);
            Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Complaint_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint__c comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');

            // ASSERT : Lot Info details are copied with blank part
            System.assertEquals(null, comp.SQX_Part__c);
            System.assertEquals(null, comp.SQX_Part_Family__c);
            System.assertEquals('LOT_001', comp.Lot_Number__c);
            System.assertEquals(Date.today(), comp.Manufacturing_Date__c);
            System.assertEquals(Date.today().addDays(10), comp.Expiration_date__c);

        }
    }
    
    /**
     * GIVEN : A association item is created
     * WHEN : Lot Info is selected
     * THEN : Details from lot info are copied
     */
    public testMethod static void givenAnAssociationItem_WhenPartIsAdded_LotInfoDetailsAreCopied(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Association Item is created with lot info
            SQX_Complaint_Associated_Item__c newAssociationItem = new SQX_Complaint_Associated_Item__c();
            
            newAssociationItem.SQX_Part__c = [SELECT Id FROM SQX_Part__c].get(0).Id;
            newAssociationItem.SQX_Part_Family__c = [SELECT Id FROM SQX_Part_Family__c].get(0).Id;
            newAssociationItem.SQX_Complaint__c = complaint.complaint.Id;
            newAssociationItem.Make_Primary__c = true;
            insert newAssociationItem;
            
            newAssociationItem.SQX_Lot_Info__c = [SELECT Id FROM SQX_Lot_Info__c].get(0).Id;

            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', newAssociationItem);
            Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint_Associated_Item__c associationItem = (SQX_Complaint_Associated_Item__c) calcFlow.getVariableValue('OutRecord');
            
            // ACT : Lot Number is changed
            associationItem.Lot_Number__c = 'UPDATED_LOT';
            associationItem.Id = newAssociationItem.Id;
            Database.SaveResult result = Database.update(associationItem, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to fail');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Part and Lot Number cannot be changed when Lot Info is selected'), result.getErrors());

            SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
            partFamily.Name = 'UTILITIES';
            insert partFamily;
            
            SQX_Part__c newPart = new SQX_Part__c();
            newPart.Name = 'ROUTER';
            newPart.Part_Number__c = 'ROUTER-001';
            newPart.Part_Risk_Level__c = 3;
            newPart.Active__c = true;
            newPart.Part_Family__c = partFamily.Id;
            
            insert newPart;

            // ACT : Part is changed
            associationItem.Lot_Number__c = 'LOT_001';
            associationItem.SQX_Part__c = newPart.Id;
            result = Database.update(associationItem, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to fail');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Part and Lot Number cannot be changed when Lot Info is selected'), result.getErrors());
            
        }
    }
    
    /**
     * GIVEN : A association item is created with lot info
     * WHEN : Lot or part is changed
     * THEN : Error is thrown
     */
    public testMethod static void givenAnAssociationItemWithLotInfo_WhenPartOrLotIsChanged_ErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Association Item is created
            SQX_Complaint_Associated_Item__c newAssociationItem = new SQX_Complaint_Associated_Item__c();
            
            // ACT : Lot info is added 
            newAssociationItem.SQX_Lot_Info__c = [SELECT Id FROM SQX_Lot_Info__c].get(0).Id;
            newAssociationItem.SQX_Part__c = [SELECT Id FROM SQX_Part__c].get(0).Id;
            newAssociationItem.SQX_Part_Family__c = [SELECT Id FROM SQX_Part_Family__c].get(0).Id;
            newAssociationItem.SQX_Complaint__c = complaint.complaint.Id;
            newAssociationItem.Make_Primary__c = true;
            
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', newAssociationItem);
            Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint_Associated_Item__c associationItem = (SQX_Complaint_Associated_Item__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Lot Info details are copied
            System.assertEquals([SELECT Id FROM SQX_Part__c].get(0).Id, associationItem.SQX_Part__c);
            System.assertEquals('LOT_001', associationItem.Lot_Number__c);
            System.assertEquals(Date.today(), associationItem.Manufacturing_Date__c);
            System.assertEquals(Date.today().addDays(10), associationItem.Expiration_date__c);
            
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Part is changed
     * THEN : Part Family is added
     */
    public testMethod static void givenAComplaint_WhenPartIsChanged_PartFamilyIsCopied(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
            partFamily.Name = 'TEST_FAMILY';
            insert partFamily;

            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.complaint.SQX_Part_Family__c = partFamily.Id;
            complaint.save();

            // ACT : Part is selected
            complaint.complaint.SQX_Part__c = [SELECT Id FROM SQX_Part__c].get(0).Id;
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);
            Flow.Interview.CQ_Complaint_Set_Part_Family_when_Part_is_set calcFlow = new Flow.Interview.CQ_Complaint_Set_Part_Family_when_Part_is_set(params);
            calcFlow.start();
            
            SQX_Complaint__c comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Part Family is copied
            System.assertEquals([SELECT Id FROM SQX_Part_Family__c WHERE Name = 'COMPUTER'].get(0).Id, comp.SQX_Part_Family__c);
            
        }
    }
    
    /**
     * GIVEN : A association item is created
     * WHEN : Part is selected
     * THEN : Part Family is auto selected
     */
    public testMethod static void givenAnAssociationItem_WhenPartIsAdded_PartFamilyIsAdded(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Association Item is created
            SQX_Complaint_Associated_Item__c newAssociationItem = new SQX_Complaint_Associated_Item__c();
            
            SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
            partFamily.Name = 'TEST_FAMILY';
            insert partFamily;
            
            newAssociationItem.SQX_Part_Family__c = partFamily.Id;
            newAssociationItem.SQX_Complaint__c = complaint.complaint.Id;
            insert newAssociationItem;
            
            // ACT : Part is added 
            newAssociationItem.SQX_Part__c = [SELECT Id FROM SQX_Part__c].get(0).Id;
            update newAssociationItem;
            
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', newAssociationItem);
            Flow.Interview.CQ_Associated_Item_Set_Part_Family_when_Part_is_set calcFlow = new Flow.Interview.CQ_Associated_Item_Set_Part_Family_when_Part_is_set(params);
            calcFlow.start();
            
            SQX_Complaint_Associated_Item__c associationItem = (SQX_Complaint_Associated_Item__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Part Family is copied
            System.assertEquals([SELECT Id, Part_Family__c FROM SQX_Part__c].get(0).Part_Family__c, associationItem.SQX_Part_Family__c);
            
        }
    }
    
    /**
     * GIVEN : A association item is created
     * WHEN : Lot info is selected with part number whose part does not exist
     * THEN : Part will be blank in complaint associated item
     */
    public testMethod static void givenAnAssociationItem_WhenLotInfoIsSelected_BlankPartIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Association Item is created
            SQX_Complaint_Associated_Item__c newAssociationItem = new SQX_Complaint_Associated_Item__c();
            
            newAssociationItem.SQX_Part_Family__c = [SELECT Id FROM SQX_Part_Family__c].get(0).Id;
            newAssociationItem.SQX_Complaint__c = complaint.complaint.Id;
            insert newAssociationItem;
            
            SQX_Lot_Info__c lotInfo = new SQX_Lot_Info__c();
            lotInfo.Name = 'LOT_001';
            lotInfo.Part_Number__c = 'MOUSE-002';
            lotInfo.Manufacturing_Date__c = Date.today();
            lotInfo.Expiration_Date__c = Date.today().addDays(10);
            insert lotInfo;
        
            // ACT : Lot Indo is linked
            newAssociationItem.SQX_Lot_Info__c = lotInfo.Id;

            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', newAssociationItem);
            Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change calcFlow = new Flow.Interview.CQ_Associated_Item_Set_Part_On_Lot_Info_Change(params);
            calcFlow.start();
            
            SQX_Complaint_Associated_Item__c associationItem = (SQX_Complaint_Associated_Item__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Lot Info details are copied with blank part
            System.assertEquals(null, associationItem.SQX_Part__c);
            System.assertEquals('LOT_001', associationItem.Lot_Number__c);
            System.assertEquals(Date.today(), associationItem.Manufacturing_Date__c);
            System.assertEquals(Date.today().addDays(10), associationItem.Expiration_date__c);

        }
    }
}