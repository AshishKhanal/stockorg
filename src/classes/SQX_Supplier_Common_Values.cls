/**
 *  Class that holds the common values across Supplier Management module
 */
public with sharing class SQX_Supplier_Common_Values {

    private SQX_Supplier_Common_Values() {}

    // Status
    public static final String STATUS_DRAFT = 'Draft',
                                STATUS_OPEN = 'Open',
                                STATUS_COMPLETE = 'Complete',
                                STATUS_CLOSED = 'Closed',
                                STATUS_VOID ='Void';

    // Stage
    public static final String STAGE_DRAFT = 'Draft',
                                STAGE_TRIAGE = 'Triage',
                                STAGE_ON_HOLD = 'On Hold',
                                STAGE_IN_PROGRESS = 'In Progress',
                                STAGE_VERIFICATION = 'Verification',
                                STAGE_CLOSED = 'Closed',
                                STAGE_ESCALATED = 'Escalated',
                                STAGE_DEESCALATED = 'De-escalated';


    // Workflow Status
    public static final String WORKFLOW_STATUS_NOT_STARTED = 'Not Started',
                                WORKFLOW_STATUS_ON_HOLD = 'On Hold',
                                WORKFLOW_STATUS_IN_PROGRESS = 'In Progress',
                                WORKFLOW_STATUS_COMPLETED = 'Complete';

    // Task types
    public static final String TASK_TYPE_APPROVAL = 'Approval',
                                TASK_TYPE_DOC_REQUEST = 'Document Request',
                                TASK_TYPE_PERFORM_AUDIT = 'Perform Audit',
                                TASK_TYPE_TASK = 'Task';

    // Record Types
    public static final String RT_TYPE_APPROVAL = 'Approval',
                                RT_TYPE_TASK = 'Task',
                                RT_TYPE_DOCUMENT_REQUEST = 'Document_Request',
                                RT_TYPE_PERFORM_AUDIT = 'Perform_Audit';

    // Result
    public static final String RESULT_REJECTED = 'Rejected',
                                RESULT_APPROVED = 'Approved',
                                RESULT_CONDITIONALLY_APPROVED = 'Conditionally Approved';

    // Activity Code
    public static final String ACTIVITY_CODE_SUBMIT = 'submit',
                                ACTIVITY_CODE_INITIATE = 'initiate',
                                ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE = 'takeownershipandinitiate',
                                ACTIVITY_CODE_COMPLETE = 'complete',
                                ACTIVITY_CODE_CLOSE = 'close',
                                ACTIVITY_CODE_VOID = 'void',
                                ACTIVITY_CODE_EXTEND = 'extend',
                                ACTIVITY_CODE_SAVE = 'save',
                                ACTIVITY_CODE_RESTART = 'restart',
                                ACTIVITY_CODE_REOPEN = 'reopen',
                                ACTIVITY_CODE_ESCALATE = 'escalate',
                                ACTIVITY_CODE_DEESCALATE = 'deescalate';

    // Record field names
    public static final String PARENT_FIELD_ACTIVITY_CODE = SQX.NSPrefix + 'Activity_Code__c',
                                PARENT_FIELD_ACTIVITY_COMMENT = SQX.NSPrefix + 'Activity_Comment__c',
                                PARENT_FIELD_IS_LOCKED = SQX.NSPrefix + 'Is_Locked__c',
                                PARENT_FIELD_STATUS = SQX.NSPrefix + 'Status__c',
                                PARENT_FIELD_STAGE = SQX.NSPrefix + 'Record_Stage__c',
                                PARENT_FIELD_CURRENT_STEP = SQX.NSPrefix + 'Current_Step__c',
                                PARENT_FIELD_WORKFLOW_STATUS = SQX.NSPrefix + 'Workflow_Status__c',
                                PARENT_FIELD_RESULT = SQX.NSPrefix + 'Result__c',
                                PARENT_FIELD_SUBMITTED_DATE = SQX.NSPrefix + 'Submitted_Date__c',
                                PARENT_FIELD_SUBMITTED_BY = SQX.NSPrefix + 'Submitted_By__c',
                                PARENT_FIELD_INITIATION_DATE = SQX.NSPrefix + 'Initiation_Date__c',
                                PARENT_FIELD_CLOSURE_COMMENT = SQX.NSPrefix + 'Closure_comment__c',
                                PARENT_FIELD_EVALUATE_WORKFLOW = SQX.NSPrefix + 'Evaluate_Workflow__c';

    // document type name
    public static final String DOCUMENT_NAME_SAMPLE_DOC = 'Sample Document Name';

    // Validation Error Msg
    public static final String RECORD_TRANSITION_VALIDATION_ERROR_MSG = 'Unable to perform the desired action on the record. Please ensure that the record\'s status/stage permits the action.',
                               RECORD_EDIT_AFTER_LOCK_VALIDATION_ERROR_MSG = 'Record status or Step status does not support the Action Performed.',
                               EXPIRATION_DATE_VALIDATION_ERROR_MSG = 'CQ Document Expiration Date is Required';
    
    //Doc Request with and without content
    public static final String  TASK_DESCRIPTION_WITHOUT_CONTENT = 'Upload the required document(s) in Files section and complete the Document Request Task.',
                                TASK_DESCRIPTION_WITH_CONTENT = 'Upload a populated document using the template file attached in Files section and complete the Document Request Task.';


    // Default map of activity code and corresponding new status for a record
    public static final Map<String, String> ACTIVITY_CODE_AND_STATUS_MAP = new Map<String, String>
    {
        ACTIVITY_CODE_SUBMIT => STATUS_DRAFT,
        ACTIVITY_CODE_INITIATE => STATUS_OPEN,
        ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE => STATUS_OPEN,
        ACTIVITY_CODE_COMPLETE => STATUS_COMPLETE,
        ACTIVITY_CODE_CLOSE =>  STATUS_CLOSED,
        ACTIVITY_CODE_VOID =>  STATUS_VOID,
        ACTIVITY_CODE_RESTART => STATUS_OPEN,
        ACTIVITY_CODE_REOPEN => STATUS_OPEN,
        ACTIVITY_CODE_ESCALATE => STATUS_CLOSED,
        ACTIVITY_CODE_DEESCALATE => STATUS_CLOSED
   };

    // Default map of activity code and corresponding new stage for a record
    public static final Map<String, String> ACTIVITY_CODE_AND_STAGE_MAP = new Map<String, String>
    {
        ACTIVITY_CODE_SUBMIT => STAGE_TRIAGE ,
        ACTIVITY_CODE_INITIATE => STAGE_IN_PROGRESS,
        ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE => STAGE_IN_PROGRESS,
        ACTIVITY_CODE_COMPLETE => STAGE_VERIFICATION,
        ACTIVITY_CODE_CLOSE =>  STAGE_CLOSED,
        ACTIVITY_CODE_VOID =>  null,
        ACTIVITY_CODE_RESTART => STAGE_IN_PROGRESS,
        ACTIVITY_CODE_REOPEN => STAGE_IN_PROGRESS,
        ACTIVITY_CODE_ESCALATE => STAGE_ESCALATED,
        ACTIVITY_CODE_DEESCALATE => STAGE_DEESCALATED
    };
}