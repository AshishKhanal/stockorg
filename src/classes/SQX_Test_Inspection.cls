/*
 * test class for the setup data for the inspection
 */
@isTest
public with sharing class SQX_Test_Inspection {
    
    public static final String ASSOCIATION_TYPE_PART = 'part',
                                ASSOCIATION_TYPE_PART_FAMILY = 'part family',
                                ASSOCIATION_TYPE_PROCESS = 'process';
    
    public SQX_Inspection__c inspection = null;

    /*
     * constructor class to create the product type inspection by default
     */
    public SQX_Test_Inspection(){
        this(null, SQX_Inspection.INSPECTION_TYPE_PRODUCT, null);
    }
    
    /*
     * constructor class to create inspection as per user input
     * @param {inspectionType} type of inspection
     * @param {inspectionTypeId} id of product or process depending upon inspection type
     */
    public SQX_Test_Inspection(User adminUser, String inspectionType, String inspectionTypeId){
        this(adminUser, inspectionType, inspectionTypeId, '1');
    }

    /**
     * Caches the department for the test
     */
    private static SQX_Department__c department = null;

    /**
     * Returns the department required for creating an inspection
     */
    private static SQX_Department__c GetDepartment(User adminUser) {
        if(department == null) {
            if(adminUser == null){
                adminUser =  SQX_Test_Account_Factory.getUser(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            }

            department = new SQX_Department__c();
            System.runas(adminUser){
                department.Name = 'Random Department';

                new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                        Schema.SQX_Department__c.Name
                    } ) ;
            }
        }

        return department;
    }

    /**
     * Caches the part that is necessary for the product inspection
     */
    private static SQX_Part__c part = null;

    /**
     * Returns the part that was created for inspection
     */
    private static SQX_Part__c GetPart() {
        if(part == null) {
            SQX_Part_Family__c pf = newPartFamily('Test_Family');
            part = newPart(pf, 'Test_Part');
        }
        return part;
    }
    /*
     * constructor class to create inspection as per user input
     * @param {inspectionType} type of inspection
     * @param {inspectionTypeId} id of product or process depending upon inspection type
     * @param {inspectionTypeId} product rev
     */
    public SQX_Test_Inspection(User adminUser, String inspectionType, String inspectionTypeId, String rev){
        String productId = inspectionType == SQX_Inspection.INSPECTION_TYPE_PRODUCT ? inspectionTypeId : null;
        if(String.isBlank(productId) && inspectionType == SQX_Inspection.INSPECTION_TYPE_PRODUCT){
            productId = GetPart().Id;
        }
        String processId = inspectionType == SQX_Inspection.INSPECTION_TYPE_PROCESS ? inspectionTypeId : null;
        initializeRecord(adminUser, inspectionType, productId, rev, processId);
    }

    /**
     * Internal method to complete the construction of the object
     */
    private void initializeRecord(User adminUser, String inspectionType, String productId, String rev, String processId) {
        inspection = new SQX_Inspection__c();
        inspection.Inspection_Type__c = inspectionType;
        inspection.SQX_Part__c = productId;
        inspection.Rev__c = rev;
        inspection.SQX_Process__c = processId;
        inspection.Inspection_Source__c = SQX_Inspection.INCOMING_SOURCE_INCOMING;
        inspection.SQX_Department__c = GetDepartment(adminUser).Id;
    }

    /**
     * constructor to create an inspection with provided configuration
     * @param adminUser the user who should be used to create a required objects (example: Department) if one doesn't exist
     * @param inspectionType the type of inspection that is being performed
     * @param productId the Id of the product that is related to the inspection
     * @param rev the revision of the product that is being provided
     * @param processId the process that is related to the inspection
     */
    public SQX_Test_Inspection(User adminUser, String inspectionType, String productId, String rev, String processId){
        initializeRecord(adminUser, inspectionType, productId, rev, processId);
    }
    
    /*
     * method to save inspection to database
     */
    public SQX_Test_Inspection save(){
        if(inspection.Id == null){
            new SQX_DB().op_insert(new List<SQX_Inspection__c>{inspection}, new List<Schema.SObjectField>{});
        } else {
            UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess
                                            FROM UserRecordAccess
                                            WHERE UserId =: UserInfo.getUserId()
                                            AND RecordId =: inspection.Id]; 

            if(userAccess.HasEditAccess || 
                SQX_Utilities.checkIfUserHasPermission(SQX_Inspection.CUSTOM_PERMISSION_INSPECTION_SUPERVISOR)){

                new SQX_DB().withoutSharing().op_update(new List<SQX_Inspection__c>{inspection}, new List<Schema.SObjectField>{});
            }
        }
        return this;
    }
    
    /*
     * method to set the status of the inspection
     * @param {status} status of the inspection
     */
    public SQX_Test_Inspection setStatus(String status){
        this.inspection.Status__c = status;
        return this;
    }
    // @description: setup data | part insertion
    public static SQX_Part_Family__c newPartFamily(String partFamilyName){
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = partFamilyName);
        insert partFamily;
        return partFamily;
    }
    
    // @description: setup data | part family insertion
    public static SQX_Part__c newPart(SQX_Part_Family__c partFamily, String partName){
        SQX_Part__c part = new SQX_Part__c(Name = partName,
                                          Part_Family__c = partFamily.Id,
                                          Part_Number__c = 'TEST_12345' + partName,
                                          Part_Risk_Level__c = 3);
        insert part;
        return part;
    }
    
    // @description: setup data | process insertion
    public static SQX_Standard_Service__c newProcess(String processName){
        SQX_Standard_Service__c process = new SQX_Standard_Service__c(
                                                Name = processName,
                                                Description__c = 'Test Process Description');
        
        insert process;
        return process;
    }
    
    // @description: setup data | association creation
    public static SQX_Association__c newAssociation(String docId, String partId, String partRev, String partFamilyId, String processId){
        SQX_Association__c association = new SQX_Association__c(
                                                SQX_Inspection_Criteria__c = docId,
                                                SQX_Part__c = partId,
                                                Rev__c = partRev,
                                                SQX_Part_Family__c = partFamilyId,
                                                SQX_Process__c = processId);
        return association;
    }
    
    /**
     * method to create an active inspection criteria with two specification and one association
     * @param associationMap        map of the type of the association to be created
     * @param numberOfSpecification number of specifications to be created
     */
    public static void createActiveInspectionCriteria(Map<String, String> associationMap, Integer numberOfSpecifications){
            
        // Inspection criteria is created
        SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
        cDoc.doc.RecordTypeId=new SQX_Extension_Controlled_Document(new ApexPages.StandardController(cDoc.doc)).getRecordTypes().get(SQX_Controlled_Document.RT_INSPECTION_CRITERIA_API_NAME);
        cDoc.save();
        
        Integer count = 0;
        // Association is added to the inspection criteria
        List<SQX_Association__c> associationToBeInserted = new List<SQX_Association__c>();
        for(String associationTypeId : associationMap.keySet()){
            
        SQX_Association__c associationForPart = new SQX_Association__c();
            count++;
            if(associationMap.get(associationTypeId) == SQX_Test_Inspection.ASSOCIATION_TYPE_PART){
                associationForPart = SQX_Test_Inspection.newAssociation(cDoc.doc.Id, associationTypeId, String.valueOf(count),null, null);
            } 
            else if(associationMap.get(associationTypeId) == SQX_Test_Inspection.ASSOCIATION_TYPE_PART_FAMILY){
                associationForPart = SQX_Test_Inspection.newAssociation(cDoc.doc.Id, null, null, associationTypeId, null);
            }
            else if(associationMap.get(associationTypeId) == SQX_Test_Inspection.ASSOCIATION_TYPE_PROCESS){
                associationForPart = SQX_Test_Inspection.newAssociation(cDoc.doc.Id, null, null, null, associationTypeId);
            }
            associationToBeInserted.add(associationForPart);
        }
        
        new SQX_DB().op_insert(associationToBeInserted, new List<Schema.SObjectField>{});
        
        // Specification is added to the inspection criteria
        List<SQX_Specification__c> specificationToBeInsperted = new List<SQX_Specification__c>();
        for(Integer index = 1; index <= numberOfSpecifications; index++){
            SQX_Specification__c specification = new SQX_Specification__c(Characteristics__c = 'Inspection Criteria Characteristics: ' + index, 
                                                                          Impact__c = 'High', 
                                                                          SQX_Inspection_Criteria__c = cDoc.doc.Id,
                                                                         	Measurement_Standard__c = 'SI');
            specificationToBeInsperted.add(specification);
        }
        new SQX_DB().op_insert(specificationToBeInsperted, new List<Schema.SObjectField>{});
        
        // Inspection criteria is activated
        cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
        
    }

}