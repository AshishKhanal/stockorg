/**
* This class will contain the common methods and static strings required for the Training Session Roster
*/
public with sharing class SQX_Training_Session_Roster {

    public static final String RESULT_COMPLETE = 'Complete';
    public static final String RESULT_INCOMPLETE = 'Incomplete';
    
    public static final String ACTIVITY_CODE_SEPARATOR = '#', // char separating activity code and date time in activity code field
                               ACTIVITY_CODE_DOCUMENT_TRAINING_GENERATION = 'DocumentTrainingGeneration',
                               ACTIVITY_CODE_DOCUMENT_TRAINING_COMPLETION = 'DocumentTrainingCompletion';

    /**
    * formats activity code by appending roster processing queued date
    */
    public static String formatActivityCode(String activityCode, DateTime queuedOn) {
        return activityCode + ACTIVITY_CODE_SEPARATOR + queuedOn;
    }
    
    /**
    * Bulkified class for Training Session Roster
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        public Bulkified() {
        }
        
        public Bulkified(List<SQX_Training_Session_Roster__c> newList , Map<Id, SQX_Training_Session_Roster__c> oldMap) {
            super(newList, oldMap);
        }
        
        /**
        * processes rosters as per activity codes
        * i) generate document trainings for "DocumentTrainingGeneration#": process rosters with complete/incomplete result as follows when session is complete
        *    a) clear session link from related pending document trainings of incomplete roster items
        *    b) create Pending Document Training for completed roster items on Subject and Training Fullfilled Requirement document
        * ii) complete document training for closed session: sign off and complete related pending training as trainer for closed session rosters with complete result
        */
        public Bulkified processDocumentTraining() {
            // action names
            final String PDT_GENERATION = ACTIVITY_CODE_DOCUMENT_TRAINING_GENERATION,
                PDT_COMPLETION = ACTIVITY_CODE_DOCUMENT_TRAINING_COMPLETION;
            
            Map<String, Map<Id, SQX_Training_Session_Roster__c>> tsrFiltered = new Map<String, Map<Id, SQX_Training_Session_Roster__c>>{
                PDT_GENERATION => new Map<Id, SQX_Training_Session_Roster__c>(),
                PDT_COMPLETION => new Map<Id, SQX_Training_Session_Roster__c>()
            };
            // get already processed records to prevent re-entrant
            Set<Id> processedIds_Generation = getProcessedRecordIdsFor(SQX.TrainingSessionRoster, PDT_GENERATION),
                processedIds_Completion = getProcessedRecordIdsFor(SQX.TrainingSessionRoster, PDT_COMPLETION);
            
            resetView();
            
            // filter rosters
            for (SQX_Training_Session_Roster__c r : (List<SQX_Training_Session_Roster__c>)view) {
                SQX_Training_Session_Roster__c rOld = (SQX_Training_Session_Roster__c)oldValues.get(r.Id);
                
                // process rosters when activity code changes and result is not blank
                if (rOld != null && r.Activity_Code__c != rOld.Activity_Code__c && String.isNotBlank(r.Activity_Code__c) && String.isNotBlank(r.Result__c)) {
                    // note: activity code and date time are concatenated by "#" and date time is used to allow reprocessing for reopened sessions
                    String activityCode = r.Activity_Code__c.split(ACTIVITY_CODE_SEPARATOR)[0];
                    
                    // list rosters to generate pdts
                    if (activityCode == PDT_GENERATION && !processedIds_Generation.contains(r.Id)) {
                        tsrFiltered.get(PDT_GENERATION).put(r.Id, r);
                    }
                    // list rosters with complete result to complete pdts
                    if (activityCode == PDT_COMPLETION && r.Result__c != RESULT_INCOMPLETE && !processedIds_Completion.contains(r.Id)) {
                        tsrFiltered.get(PDT_COMPLETION).put(r.Id, r);
                    }
                }
            }
            
            // generate pdts
            if (!tsrFiltered.get(PDT_GENERATION).isEmpty()) {
                addToProcessedRecordsFor(SQX.TrainingSessionRoster, PDT_GENERATION, tsrFiltered.get(PDT_GENERATION).values());
                
                new PrivilegeEscalator().generateDocumentTraining(tsrFiltered.get(PDT_GENERATION).values());
            }
            
            // complete pdts
            if (!tsrFiltered.get(PDT_COMPLETION).isEmpty()) {
                addToProcessedRecordsFor(SQX.TrainingSessionRoster, PDT_COMPLETION, tsrFiltered.get(PDT_COMPLETION).values());
                
                new PrivilegeEscalator().completePendingDocumentTraining(tsrFiltered.get(PDT_COMPLETION).values());
            }
            
            return this;
        }
        
        /**
        * When the Training Session is Complete or Void,
        * Training Session Roster cannot be manipulated.
        * @author Sajal Joshi
        * @date 2017/02/07
        * @story [SQX-2860]
        */
        public Bulkified rosterCannotbeManipulatedAfterTrainingIsCompleteOrVoid(){
            this.resetView();
            List<SQX_Training_Session_Roster__c> allRosters = (List<SQX_Training_Session_Roster__c>) this.view;
            
            if(allRosters.size() > 0){
                Map<Id, SQX_Training_Session__c> sessionsMap = new Map<Id, SQX_Training_Session__c>([
                    SELECT Id, Is_Locked__c
                    FROM SQX_Training_Session__c
                    WHERE Id IN :getIdsForField(allRosters, Schema.SQX_Training_Session_Roster__c.SQX_Training_Session__c)
                ]);
                
                for(SQX_Training_Session_Roster__c roster: allRosters){
                    if (sessionsMap.get(roster.SQX_Training_Session__c).Is_Locked__c == true) {
                        roster.addError(Label.SQX_ERR_ROSTER_CANNOT_BE_MANIPULATED_AFTER_TRAINING_IS_COMPLETE_VOID);
                    }
                }
            }
            return this;
        }

        /**
        * When roster is deleted,
        * training session link in document training is also removed.
        * @author Sajal Joshi
        * @date 2017/02/14
        * @story [SQX-2860]
        */
        public Bulkified deleteTrainingRelationWhenDeletingRoster(){
            this.resetView();
            List<SQX_Training_Session_Roster__c> allRosters = (List<SQX_Training_Session_Roster__c>) this.view;
            List<SQX_Personnel_Document_Training__c> updateTraining = new List<SQX_Personnel_Document_Training__c>();
            if(allRosters.size() > 0){
                Set<Id> personnelIds = new Set<Id>();
                Set<Id> sessionIds = new Set<Id>();
                Set<String> uniqueRosters = new Set<String>();
                
                for(SQX_Training_Session_Roster__c roster: allRosters){
                    personnelIds.add(roster.SQX_Personnel__c);
                    sessionIds.add(roster.SQX_Training_Session__c);
                    uniqueRosters.add(roster.Uniqueness_Constraint__c);
                }

                for(SQX_Personnel_Document_Training__c training: [SELECT SQX_Personnel__c, SQX_Training_Session__c FROM SQX_Personnel_Document_Training__c 
                                                                  WHERE SQX_Training_Session__c IN: sessionIds AND SQX_Personnel__c IN: personnelIds 
                                                                  AND Status__c =: SQX_Personnel_Document_Training.PENDING_STATUSES]){
                    if(uniqueRosters.contains(String.valueOf(training.SQX_Training_Session__c) + String.valueOf(training.SQX_Personnel__c))){
                        training.SQX_Training_Session__c = null;
                        updateTraining.add(training);
                    }
                }
                if (!updateTraining.isEmpty()) {
                    /*
                    * WITHOUT SHARING used
                    * --------------------
                    * Users/trainers may not have edit access on personnel and its child records (document training)
                    */
                    new SQX_DB().withoutSharing().op_update(updateTraining, new List<Schema.SObjectField>{
                        SQX_Personnel_Document_Training__c.SQX_Training_Session__c});
                }
            }
            return this;
        }

        /**
        * When Personnel is added to Roster,
        * If Personnel has incomplete pre-requisite training then appropriate error message is thrown
        * @author Sudeep Maharjan
        * @story [SQX-4429]
        */
        public Bulkified validatePersonnelWithIncompletePreRequisiteTrainings() {
            this.resetView();
            
            if (this.view.size() > 0) {
                Map<String, SQX_Training_Session_Roster__c> filteredRosterMap = new Map<String, SQX_Training_Session_Roster__c>();
                Map<Id, List<SQX_Training_Session_Roster__c>> tsRosterMap = new Map<Id, List<SQX_Training_Session_Roster__c>>();
                Map<Id, SQX_Personnel__c> personnelMap = new Map<Id, SQX_Personnel__c>();

                // Get Personnel and Training Session Ids
                for(SQX_Training_Session_Roster__c roster: (List<SQX_Training_Session_Roster__c>)this.view) {
                    SQX_Training_Session_Roster__c oldRosterVal = (SQX_Training_Session_Roster__c)this.oldValues.get(roster.Id);
                    
                    //Filter Training Session Rosters without comment and Personnel Field changed 
                    if((oldRosterVal == null || roster.SQX_Personnel__c != oldRosterVal.SQX_Personnel__c) && roster.Comment__c == null){
                        filteredRosterMap.put(roster.SQX_Training_Session__c+''+roster.SQX_Personnel__c, roster);
                        personnelMap.put(roster.SQX_Personnel__c, null);
                        List<SQX_Training_Session_Roster__c> rosters = tsRosterMap.get(roster.SQX_Training_Session__c);
                        if (rosters == null) {
                            rosters = new List<SQX_Training_Session_Roster__c>();
                            tsRosterMap.put(roster.SQX_Training_Session__c, rosters);
                        }
                        rosters.add(roster);
                    }
                }
                
                if (!filteredRosterMap.isEmpty()) {
                    Map<Id, String> allJfIdsWithName = new Map<Id, String>();
                    Map<Id, Set<Id>> docSessionsMap = new Map<Id, Set<Id>>();
                    Map<String, Set<String>> tsrIncompleteJfsMap = new Map<String, set<String>>();
                    Map<Id, Map<Id, Decimal>> psnJfsWithStepMap = new Map<Id, Map<Id, Decimal>>();
                    
                    // List active job functions of all personnel with pending trainings
                    for (SQX_Personnel_Job_Function__c pjf : [  SELECT Id,
                                                                       SQX_Job_Function__c,
                                                                       SQX_Personnel__c,
                                                                       Current_Training_Program_Step__c,
                                                                       Training_Status__c,
                                                                       SQX_Job_Function__r.Name,
                                                                       SQX_Personnel__r.Name,
                                                                       SQX_Personnel__r.Full_Name__c
                                                                FROM SQX_Personnel_Job_Function__c
                                                                WHERE SQX_Personnel__c IN: personnelMap.keySet()
                                                                    AND Active__c =: true
                                                                    AND Training_Status__c !=: SQX_Personnel_Job_Function.TRAINING_STATUS_CURRENT
                                                                ORDER BY SQX_Personnel__c ]) {
                        // update personnel details to map
                        personnelMap.put(pjf.SQX_Personnel__c, pjf.SQX_Personnel__r);

                        // list personnel job functions with pending trainings
                        Map<Id, Decimal> jfsWithStep = psnJfsWithStepMap.get(pjf.SQX_Personnel__c);
                        if (jfsWithStep == null) {
                            jfsWithStep = new Map<Id, Decimal>();
                            psnJfsWithStepMap.put(pjf.SQX_Personnel__c, jfsWithStep);
                        }
                        
                        // null training step in document requirements is considered as zero (0)
                        jfsWithStep.put(pjf.SQX_Job_Function__c, pjf.Current_Training_Program_Step__c == null ? 0 : pjf.Current_Training_Program_Step__c);
                        allJfIdsWithName.put(pjf.SQX_Job_Function__c, pjf.SQX_Job_Function__r.Name);
                    }
                    
                    // List subject and fulfilled requirements associated to training session
                    for (SQX_Training_Session__c ts : [ SELECT Id,
                                                            SQX_Controlled_Document__c,
                                                            ( SELECT SQX_Controlled_Document__c FROM SQX_Training_Fulfilled_Requirements__r )
                                                        FROM SQX_Training_Session__c
                                                        WHERE Id IN: tsRosterMap.keySet() ]) {
                        if (ts.SQX_Controlled_Document__c != null) {
                            if (!docSessionsMap.containsKey(ts.SQX_Controlled_Document__c)) {
                                docSessionsMap.put(ts.SQX_Controlled_Document__c, new Set<Id>{ ts.Id });
                            }
                            else {
                                docSessionsMap.get(ts.SQX_Controlled_Document__c).add(ts.Id);
                            }
                        }
                        for (SQX_Training_Fulfilled_Requirement__c tfr : ts.SQX_Training_Fulfilled_Requirements__r) {
                            if (!docSessionsMap.containsKey(tfr.SQX_Controlled_Document__c)) {
                                docSessionsMap.put(tfr.SQX_Controlled_Document__c, new Set<Id>{ ts.Id });
                            }
                            else {
                                docSessionsMap.get(tfr.SQX_Controlled_Document__c).add(ts.Id);
                            }
                        }
                    }
                    
                    // list and identify roster personnel with incomplete pre-requisite trainings
                    for (SQX_Controlled_Document__c doc : [ SELECT Id,
                                                                (
                                                                    SELECT SQX_Job_Function__c,
                                                                        Training_Program_Step_Internal__c
                                                                    FROM SQX_Requirements__r
                                                                    WHERE SQX_Job_Function__c IN :allJfIdsWithName.keySet()
                                                                        AND Active__c = true
                                                                    ORDER BY SQX_Job_Function__c, Training_Program_Step_Internal__c
                                                                )
                                                            FROM SQX_Controlled_Document__c
                                                            WHERE Id IN :docSessionsMap.keySet()
                                                            ]) {
                        // process all training sessions related to the controlled document
                        for (Id tsId : docSessionsMap.get(doc.Id)) {
                            // get all rosters related to training session
                            List<SQX_Training_Session_Roster__c> rosters = tsRosterMap.get(tsId);
                            // process when there are active requirements and training has rosters
                            if (!doc.SQX_Requirements__r.isEmpty() && rosters != null) {
                                for (SQX_Training_Session_Roster__c roster : rosters) {
                                    Map<Id, Decimal> pjfStepMap = psnJfsWithStepMap.get(roster.SQX_Personnel__c);
                                    if (pjfStepMap != null) {
                                        for (SQX_Requirement__c req : doc.SQX_Requirements__r) {
                                            // add job function with incomplete pre-requisite training for current roster
                                            if (pjfStepMap.get(req.SQX_Job_Function__c) < req.Training_Program_Step_Internal__c) {
                                                Set<String> incompleteJfNames = tsrIncompleteJfsMap.get(roster.Id);
                                                if (incompleteJfNames == null) {
                                                    incompleteJfNames = new Set<String>();
                                                    tsrIncompleteJfsMap.put(roster.SQX_Training_Session__c+''+roster.SQX_Personnel__c, incompleteJfNames);
                                                }
                                                incompleteJfNames.add(allJfIdsWithName.get(req.SQX_Job_Function__c));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        // add incomplete pre-requisite training error to rosters
                        for (String trainingSessionAndPersonnel : tsrIncompleteJfsMap.keySet()) {
                            SQX_Training_Session_Roster__c roster = filteredRosterMap.get(trainingSessionAndPersonnel);
                            SQX_Personnel__c psn = personnelMap.get(roster.SQX_Personnel__c);
                            
                            String errMsg = Label.SQX_ERR_MSG_PENDING_PREREQUISITE_EXIST_WHILE_ENROLLING_PSN_IN_TRAINING_SESSION;
                            String jfWithErrorList = String.join(new List<String>(tsrIncompleteJfsMap.get(trainingSessionAndPersonnel)), ', ');
                            errMsg = errMsg.replace('{JOB_FUNCTION_NAMES}', jfWithErrorList)
                                           .replace('{PSN_NUMBER}', psn.Name)
                                           .replace('{PSN_NAME}', psn.Full_Name__c);
                            filteredRosterMap.get(trainingSessionAndPersonnel).addError(errMsg);
                        }
                    }
                }
            }
            
            return this;
        }
    }
    
    /**
    * system operations for training session object that require to query and update records such as personnel document trainings
    */
    private without sharing class PrivilegeEscalator {
        
        /**
        * generates/resets pending document trainings for provided rosters
        *    a) clear session link from related pending document trainings of incomplete roster items
        *    b) create Pending Document Training for completed roster items on Subject and Training Fullfilled Requirement document of related session
        */
        public void generateDocumentTraining(List<SQX_Training_Session_Roster__c> rosters) {
            // maps rosters by session and personnel
            Map<Id, Map<Id, SQX_Training_Session_Roster__c>> tsnPsnRosterMap = new Map<Id, Map<Id, SQX_Training_Session_Roster__c>>();
            // maps personnel with complete roster result by session
            Map<Id, Set<Id>> completeRosterPsnIdMap = new Map<Id, Set<Id>>();
            Set<Id> completePsnIds = new Set<Id>();
            Set<Id> incompletePsnIds = new Set<Id>();
            Set<Id> incompleteSessionIds = new Set<Id>();
            // list personnel of session with incomplete roster result
            Set<String> incompletePsnSessions = new Set<String>();
            
            for (SQX_Training_Session_Roster__c roster : rosters) {
                Map<Id, SQX_Training_Session_Roster__c> psnRosters = tsnPsnRosterMap.get(roster.SQX_Training_Session__c);
                if (psnRosters == null) {
                    psnRosters = new Map<Id, SQX_Training_Session_Roster__c>();
                    tsnPsnRosterMap.put(roster.SQX_Training_Session__c, psnRosters);
                }
                psnRosters.put(roster.SQX_Personnel__c, roster);
                
                if (roster.Result__c == SQX_Training_Session_Roster.RESULT_INCOMPLETE) {
                    incompletePsnIds.add(roster.SQX_Personnel__c);
                    incompleteSessionIds.add(roster.SQX_Training_Session__c);
                    incompletePsnSessions.add('' + roster.SQX_Personnel__c + roster.SQX_Training_Session__c);
                }
                else {
                    completePsnIds.add(roster.SQX_Personnel__c);
                    
                    Set<Id> psns = completeRosterPsnIdMap.get(roster.SQX_Training_Session__c);
                    if (psns == null) {
                        psns = new Set<Id>();
                        completeRosterPsnIdMap.put(roster.SQX_Training_Session__c, psns);
                    }
                    psns.add(roster.SQX_Personnel__c);
                }
            }
            
            // a) clear session link from related pending document trainings of incomplete roster items
            if (!incompleteSessionIds.isEmpty()) {
                /**
                * WITHOUT SHARING used
                * --------------------
                * user having permission on session and roster may not have permission on personnel records
                */
                SQX_Trigger_Handler.Action updateAct = new SQX_Trigger_Handler.Action(true);
                
                for (SQX_Personnel_Document_Training__c pdt : [ SELECT SQX_Personnel__c,
                                                                    SQX_Controlled_Document__c,
                                                                    SQX_Training_Session__c
                                                                FROM SQX_Personnel_Document_Training__c
                                                                WHERE SQX_Personnel__c IN :incompletePsnIds
                                                                    AND SQX_Training_Session__c IN :incompleteSessionIds
                                                                    AND Status__c IN :SQX_Personnel_Document_Training.PENDING_STATUSES ]) {
                    if (incompletePsnSessions.contains('' + pdt.SQX_Personnel__c + pdt.SQX_Training_Session__c)) {
                        updateAct.op_update(tsnPsnRosterMap.get(pdt.SQX_Training_Session__c).get(pdt.SQX_Personnel__c), new SQX_Personnel_Document_Training__c(
                            Id = pdt.Id,
                            SQX_Training_Session__c = null
                        ));
                    }
                }
                
                updateAct.op_commit();
            }
            
            // b) create/use pending document trainings for completed roster items on Subject and Training Fullfilled Requirement document
            if (!completeRosterPsnIdMap.isEmpty()) {
                Map<Id, List<Id>> fulfilledReqIdsMap = new Map<Id, List<Id>>();
                Map<Id, Set<Id>> sessionDocIdsMap = new Map<Id, Set<Id>>();
                Set<Id> allDocIds = new Set<Id>();
                
                // map subject doc with session
                for (SQX_Training_Session__c ts : [ SELECT SQX_Controlled_Document__c FROM SQX_Training_Session__c
                                                    WHERE Id IN :completeRosterPsnIdMap.keySet() AND SQX_Controlled_Document__c != null]) {
                    sessionDocIdsMap.put(ts.Id, new Set<Id>{ ts.SQX_Controlled_Document__c });
                    allDocIds.add(ts.SQX_Controlled_Document__c);
                }
                
                // map fulfilled req doc with session
                for (SQX_Training_Fulfilled_Requirement__c req : [  SELECT SQX_Training_Session__c,
                                                                        SQX_Controlled_Document__c
                                                                    FROM SQX_Training_Fulfilled_Requirement__c
                                                                    WHERE SQX_Training_Session__c IN :completeRosterPsnIdMap.keySet()
                                                                    ORDER BY SQX_Training_Session__c, SQX_Controlled_Document__c ]) {
                    Set<Id> docIds = sessionDocIdsMap.get(req.SQX_Training_Session__c);
                    if (docIds == null) {
                        docIds = new Set<Id>();
                        sessionDocIdsMap.put(req.SQX_Training_Session__c, docIds);
                    }
                    docIds.add(req.SQX_Controlled_Document__c);
                    allDocIds.add(req.SQX_Controlled_Document__c);
                }
                
                if (!allDocIds.isEmpty()) {
                    // get valid controlled document for which trainings can be created
                    Map<Id, SQX_Controlled_Document__c> validDocMap = new Map<Id, SQX_Controlled_Document__c>([
                        SELECT Id,
                            SQX_Initial_Assessment__c,
                            Duration__c
                        FROM SQX_Controlled_Document__c
                        WHERE Id IN :allDocIds
                            AND Document_Status__c IN :SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING
                    ]);
                    
                    if (!validDocMap.isEmpty()) {
                        /**
                        * WITHOUT SHARING used
                        * --------------------
                        * user having permission on session and roster may not have permission on personnel records
                        */
                        SQX_Trigger_Handler.Action insertAct = new SQX_Trigger_Handler.Action(true),
                            updateAct = new SQX_Trigger_Handler.Action(true);
                        
                        Map<String, SQX_Personnel_Document_Training__c> pdtsToInsertMap = new Map<String, SQX_Personnel_Document_Training__c>();
                        Map<Id, SQX_Personnel_Document_Training__c> pdtsToUpdateMap = new Map<Id, SQX_Personnel_Document_Training__c>();
                        
                        // get existing pending trainings that can be used with training session
                        Map<String, List<SQX_Personnel_Document_Training__c>> pendingPdtsMap = new Map<String, List<SQX_Personnel_Document_Training__c>>();
                        for (SQX_Personnel_Document_Training__c pdt : [ SELECT Id,
                                                                            SQX_Personnel__c,
                                                                            SQX_Controlled_Document__c,
                                                                            SQX_Training_Session__c,
                                                                            SQX_Assessment__c,
                                                                            SQX_Personnel_Assessment__c,
                                                                            Completion_Date__c,
                                                                            Status__c
                                                                        FROM SQX_Personnel_Document_Training__c
                                                                        WHERE SQX_Personnel__c IN :completePsnIds
                                                                            AND SQX_Controlled_Document__c IN :validDocMap.keySet()
                                                                            AND Status__c IN :SQX_Personnel_Document_Training.PENDING_STATUSES ]) {
                            String psnDockey = '' + pdt.SQX_Personnel__c + pdt.SQX_Controlled_Document__c;
                            
                            List<SQX_Personnel_Document_Training__c> pdts = pendingPdtsMap.get(psnDockey);
                            if (pdts == null) {
                                pdts = new List<SQX_Personnel_Document_Training__c>();
                                pendingPdtsMap.put(psnDockey, pdts);
                            }
                            pdts.add(pdt);
                        }
                        
                        for (Id sessionId : completeRosterPsnIdMap.keySet()) {
                            if (sessionDocIdsMap.containsKey(sessionId)) {
                                Set<Id> psnIds = completeRosterPsnIdMap.get(sessionId);
                                
                                for (Id docId : sessionDocIdsMap.get(sessionId)) {
                                    SQX_Controlled_Document__c doc = validDocMap.get(docId);
                                    
                                    if (doc != null) {
                                        for (Id psnId : psnIds) {
                                            String psnDocKey = '' + psnId + docId;
                                            Boolean createNewTraining = true;
                                            
                                            if (pendingPdtsMap.containsKey(psnDocKey)) {
                                                // link any remaining pending training that is not associated to any training session
                                                for (SQX_Personnel_Document_Training__c pdt : pendingPdtsMap.get(psnDocKey)) {
                                                    if (pdt.SQX_Training_Session__c == null && !pdtsToUpdateMap.containsKey(pdt.Id)) {
                                                        // only link pending trainings when they are not linked to any sessions
                                                        pdtsToUpdateMap.put(pdt.Id, new SQX_Personnel_Document_Training__c(
                                                            Id = pdt.Id,
                                                            SQX_Training_Session__c = sessionId
                                                        ));
                                                        updateAct.op_update(tsnPsnRosterMap.get(sessionId).get(psnId), pdtsToUpdateMap.get(pdt.Id));
                                                    }
                                                }
                                                
                                                // no need to create if pending trainings exists
                                                createNewTraining = false;
                                            }
                                            
                                            if (!pdtsToInsertMap.containsKey(psnDocKey) && createNewTraining) {
                                                // creates new document training when no pending trainings exist to use with the training session
                                                Integer duration = 0;
                                                if (doc.Duration__c != null) {
                                                    duration = Integer.valueOf(doc.Duration__c);
                                                }
                                                
                                                SQX_Personnel_Document_Training__c pdt = new SQX_Personnel_Document_Training__c(
                                                    SQX_Personnel__c = psnId,
                                                    SQX_Controlled_Document__c = docId,
                                                    SQX_Training_Session__c = sessionId,
                                                    Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                                                    SQX_Assessment__c = doc.SQX_Initial_Assessment__c,
                                                    Due_Date__c = Date.today().addDays(duration),
                                                    Is_Created__c = true
                                                );
                                                
                                                pdtsToInsertMap.put(psnDocKey, pdt);
                                                insertAct.op_insert(tsnPsnRosterMap.get(sessionId).get(psnId), pdt);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        insertAct.op_commit();
                        updateAct.op_commit();
                    }
                }
            }
        }
        
        /**
        * signs off and completes pending document training as trainer for provided rosters
        */
        public void completePendingDocumentTraining(List<SQX_Training_Session_Roster__c> rosters) {
            /**
            * WITHOUT SHARING used
            * --------------------
            * user having permission on session and roster may not have permission on personnel records
            */
            SQX_Trigger_Handler.Action updateAct = new SQX_Trigger_Handler.Action(true);
            
            Map<Id, Map<Id, SQX_Training_Session_Roster__c>> tsnPsnRosterMap = new Map<Id, Map<Id, SQX_Training_Session_Roster__c>>();
            
            for (SQX_Training_Session_Roster__c r : rosters) {
                Map<Id, SQX_Training_Session_Roster__c> psnRosters = tsnPsnRosterMap.get(r.SQX_Training_Session__c);
                if (psnRosters == null) {
                    psnRosters = new Map<Id, SQX_Training_Session_Roster__c>();
                    tsnPsnRosterMap.put(r.SQX_Training_Session__c, psnRosters);
                }
                psnRosters.put(r.SQX_Personnel__c, r);
            }
            
            Id currentUserId = UserInfo.getUserId();
            String currentUserSignatureData = SQX_Utilities.getUserDetailsForRecordActivity();
            
            // perform trainer sign off for applicable pending trainings
            for (SQX_Personnel_Document_Training__c pdt : [ SELECT Id,
                                                                SQX_Training_Session__c,
                                                                SQX_Personnel__c,
                                                                SQX_Controlled_Document__c,
                                                                SQX_Assessment__c,
                                                                SQX_Personnel_Assessment__c,
                                                                Completion_Date__c,
                                                                Status__c
                                                            FROM SQX_Personnel_Document_Training__c
                                                            WHERE SQX_Training_Session__c IN :tsnPsnRosterMap.keySet()
                                                                AND Status__c IN :SQX_Personnel_Document_Training.PENDING_STATUSES
                                                                AND ( SQX_Assessment__c = null OR SQX_Personnel_Assessment__c != null ) ]) {
                updateAct.op_update(tsnPsnRosterMap.get(pdt.SQX_Training_Session__c).get(pdt.SQX_Personnel__c), new SQX_Personnel_Document_Training__c(
                    Id = pdt.Id,
                    Completion_Date__c = (pdt.Completion_Date__c == null ? Date.today() : pdt.Completion_Date__c),
                    SQX_Training_Approved_By__c = currentUserId,
                    Trainer_Signature__c = currentUserSignatureData,
                    Trainer_SignOff_Date__c = DateTime.now(), // current time
                    Trainer_Signoff_Comment__c = Label.SQX_MSG_PDT_TRAINER_SIGNED_OFF,
                    Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE
                ));
            }
            
            updateAct.op_commit();
        }
    }
}