/**
* Unit tests for implementation (SQX_Action__c) object to ensure that owner is same as assignee and main record owner has edit access on the record
*/
@IsTest
public class SQX_Test_1595_Action {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user2');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user3');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user4');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'admin1');
    }
    
    /**
    * This test ensures that the action assignee becomes the owner and record is shared with main record owner
    * when the action is open
    */
    public static testmethod void givenActionSaved_OwnerAsAssignee_SharedWithMainRecordOwner() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        User user3 = users.get('user3');
        User user4 = users.get('user4');
        User admin1 = users.get('admin1');
        System.assert(user1 != null);
        System.assert(user2 != null);
        System.assert(user3 != null);
        System.assert(user4 != null);
        System.assert(admin1 != null);
        
        System.runas(user1) {
            // add required capa finding
            SQX_Test_Finding capaFinding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType, false)
                .setRequired(true, false, false, false, false) //response required, containment required, investigation required, ca required, pa required
                .setStatus(SQX_Finding.STATUS_OPEN)
                .save();
            
            // add required capa
            SQX_CAPA__c capa = new  SQX_Test_CAPA_Utility().prepareMockCAPA(user1, capaFinding.finding, user1.Id);
            capa.OwnerId = user2.Id;
            insert capa;
            
            // update related capa
            capaFinding.finding.SQX_CAPA__c = capa.Id;
            update capaFinding.finding;
            
            // add required finding responses
            SQX_Test_Finding_Response capaResponse = new SQX_Test_Finding_Response(capaFinding.finding).save();
            
            // ACT: add required implementation for capa
            SQX_Action__c capaAction = SQX_Test_Implementation_Action.prepareMockObject(capa);
            capaAction.SQX_User__c = user3.Id;
            capaAction.Status__c = SQX_Implementation_Action.STATUS_OPEN;
            capaAction.Record_Action__c = null;
            insert capaAction;
            
            // get saved data
            capaAction = [SELECT Id, OwnerId, SQX_User__c, Main_Record_Owner_Id__c FROM SQX_Action__c WHERE Id = :capaAction.Id];
            
            // check owner of implementation to be same as assignee
            System.assertEquals(user3.Id, capaAction.OwnerId);
            // check main record owner if field value of implementation
            System.assertEquals(user2.Id, capaAction.Main_Record_Owner_Id__c);
            
            // get shared information with edit access
            SQX_Action__Share capaActionShare = [SELECT Id, UserOrGroupId FROM SQX_Action__Share WHERE ParentId = :capaAction.Id AND AccessLevel = 'edit'];
            
            // check implementation to be shared with main record owner
            System.assertEquals(user2.Id, capaActionShare.UserOrGroupId);
            
            
            // add required audit
            SQX_Test_Audit audit = new SQX_Test_Audit(admin1);
            audit.audit.SQX_Auditee_Contact__c = user4.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.setPolicy(false, true, true);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            
            // ACT: add required implementation for audit
            // Note: audit finding and response is not created since we are verifying main record owner id and sharing rules.
            SQX_Action__c auditAction = new SQX_Action__c(
                SQX_Audit__c = audit.audit.Id,
                Status__c = SQX_Implementation_Action.STATUS_OPEN,
                Due_Date__c = Date.Today(),
                SQX_User__c = user3.Id
            );
            insert auditAction;
            
            // get saved data
            auditAction = [SELECT Id, OwnerId, SQX_User__c, Main_Record_Owner_Id__c FROM SQX_Action__c WHERE Id = :auditAction.Id];
            
            // check owner of implementation to be same as assignee
            System.assertEquals(user3.Id, auditAction.OwnerId);
            // check main record owner if field value of implementation
            System.assertEquals(user1.Id, auditAction.Main_Record_Owner_Id__c);
            
            // get shared information with edit access
            SQX_Action__Share auditActionShare = [SELECT Id, UserOrGroupId FROM SQX_Action__Share WHERE ParentId = :auditAction.Id AND AccessLevel = 'edit'];
            
            // check implementation to be shared with main record owner
            System.assertEquals(user1.Id, auditActionShare.UserOrGroupId);
        }
    }


    /**
    * This test ensures that the action assignee doesn't become the owner when the record is completed 
    * directly or submitted for approval (i.e. not open)
    * @story SQX-2362
    */
    public static testmethod void givenOpenAction_WhenSaved_AssignedBecomesOwner(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        System.runas(user1) {
            // Arrange: Create a CAPA which will be used to submit the action
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility(user1).save();

            // Act: Add an action which hasn't been approved i.e. Record Action is still new
            SQX_Action__c capaAction = new SQX_Action__c(
                SQX_CAPA__c = capa.capa.Id,
                Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW,
                Due_Date__c = Date.Today().addDays(3),
                SQX_User__c = user2.Id
            );

            insert capaAction;


            // Assert: Ensure that the owner hasn't been changed and capa owner is still the assignee
            System.assertEquals(user1.Id, [SELECT OwnerId FROM SQX_Action__c WHERE Id = : capaAction.Id ].OwnerId, 'Capa Action should still be owned by the owner');


            // Act: Update the action to approve it i.e. change its status to OPEN
            capaAction.Record_Action__c = null;
            capaAction.Status__c = SQX_Implementation_Action.STATUS_OPEN;

            update capaAction;

            // Assert: Ensure that the owner has been changed to assignee
            System.assertEquals(user2.Id, [SELECT OwnerId FROM SQX_Action__c WHERE Id = : capaAction.Id ].OwnerId, 'Capa Action should be owned by the assignee');
            

            // Act: Add an action which has been directly completed
            SQX_Action__c capaAction2 = new SQX_Action__c(
                SQX_CAPA__c = capa.capa.Id,
                Status__c = SQX_Implementation_Action.STATUS_COMPLETE,
                Completion_Date__c = Date.Today(),
                Completed_By__c = 'User 2',
                SQX_User__c = user2.Id
            );

            insert capaAction2;

            // Assert: Ensure that owner is still User 1
            System.assertEquals(user1.Id, [SELECT OwnerId FROM SQX_Action__c WHERE Id = : capaAction2.Id ].OwnerId, 'Capa Action 2 should still be owned by the owner');


        }
    }


}