/**
 * controller class for file upload lightning component
 */
public with sharing class SQX_File_Upload_Controller {
    
    /**
     * method to get content documents list from complaint
     * @param recordId regulatory record id
     * @return list of content documents
     */
    @AuraEnabled
    public static List<Map<String,String>> getDocList(String recordId){
        Id recId = (Id) recordId;
        String sobjectName = recId.getSobjectType().getDescribe().getName();
        
        List<Map<String,String>> docMapList = null;
        
        String query = 'SELECT Id, compliancequest__SQX_Regulatory_Report__r.compliancequest__SQX_Complaint__c FROM ';
        query = query + sobjectName;
        query = query + ' WHERE Id = :recordId';
        List<SObject> objList = Database.query(query);
        if(objList.size() > 0){
            Set<Id> complaintIds = new Set<Id>();
            for(SObject obj : objList){
                complaintIds.add((Id) obj.getSobject('compliancequest__SQX_Regulatory_Report__r').get('compliancequest__SQX_Complaint__c'));
            }
            
            List<ContentDocumentLink> docLinkList = new List<ContentDocumentLink>();
            docLinkList = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :complaintIds];
            
            // adds content document ids from complaint to list
            Set<Id> docIds = new Set<Id>();
            if(docLinkList.size() > 0){
                for(ContentDocumentLink docLink : docLinkList){
                    docIds.add(docLink.ContentDocumentId);
                }
            }
            
            docLinkList = new List<ContentDocumentLink>();
            docLinkList = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :recId];
            
            // removes content document ids from regulatory record which is already added
            if(docLinkList.size() > 0){
                for(ContentDocumentLink docLink : docLinkList){
                    if(docIds.contains(docLink.ContentDocumentId)){
                        docIds.remove(docLink.ContentDocumentId);
                    }
                }
            }
            if(docIds.size() > 0){
                docMapList = new List<Map<String, String>>();
                for(ContentDocument doc : [SELECT Id, Title FROM ContentDocument WHERE Id IN :docIds]){
                    Map<String, String> docMap = new Map<String, String>();
                    docMap.put('Id', doc.Id);
                    docMap.put('Title', doc.Title);
                    docMap.put('ShowRecord', '/' + doc.Id);
                    docMapList.add(docMap);
                }
            }
        }
        return docMapList;
    }
    
    /**
     * method to link content documents to regulatory records
     * @param recordId regulatory record id
     * @param contentDocIds content document ids to link to regulatory record
     */
    @AuraEnabled
    public static void saveContentDocsToReport(String recordId, List<String> contentDocIds){
        List<ContentDocumentLink> docLinkList = new List<ContentDocumentLink>();
        for(String contentDocId : contentDocIds){
            ContentDocumentLink docLink = new ContentDocumentLink(ContentDocumentId = contentDocId, LinkedEntityId = recordId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED);
            docLinkList.add(docLink);
        }
        
        new SQX_DB().op_insert(docLinkList, new List<Schema.SObjectField>{ContentDocumentLink.ContentDocumentId, 
                                                                            ContentDocumentLink.LinkedEntityId, 
                                                                            ContentDocumentLink.ShareType});
    }
}