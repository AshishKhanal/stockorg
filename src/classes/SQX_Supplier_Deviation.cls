/**
 * class to perform actions for sd
 */
public with sharing class SQX_Supplier_Deviation {
    
    // Static value for the status of supplier deviation
    public static final String STATUS_DRAFT = 'Draft',
                                STATUS_OPEN = 'Open',
                                STATUS_COMPLETE = 'Complete',
                                STATUS_CLOSED = 'Closed',
                                STATUS_VOID ='Void';
    
    // Static value for the stage of supplier deviation
    public static final String STAGE_DRAFT = 'Draft',
                                STAGE_TRIAGE = 'Triage',
                                STAGE_ON_HOLD = 'On Hold',
                                STAGE_IN_PROGRESS = 'In Progress',
                                STAGE_VERIFICATION = 'Verification',
                                STAGE_CLOSED = 'Closed';
    
    // Static value for the workflow status of supplier deviation
    public static final String WORKFLOW_STATUS_NOT_STARTED = 'Not Started',
                                WORKFLOW_STATUS_ON_HOLD = 'On Hold',
                                WORKFLOW_STATUS_IN_PROGRESS = 'In Progress',
                                WORKFLOW_STATUS_COMPLETED = 'Complete';
    
    // Static value for the task type of supplier deviation
    public static final String TASK_TYPE_TASK = 'Task',
                                TASK_TYPE_DOC_REQUEST = 'Document Request',
                                TASK_TYPE_PERFORM_AUDIT = 'Perform Audit',
                                TASK_TYPE_APPROVAL = 'Approval';
    
    
    public static final String RT_TYPE_TASK = 'Task',
                                RT_TYPE_APPROVAL = 'Approval';
    
    // Static value for the SD result of supplier deviation
    public static final String SD_RESULT_REJECTED = 'Rejected',
                                SD_RESULT_APPROVED = 'Approved'; 
    
     /** 
     * SD Trigger Handler
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_Supplier_Trigger_Handler{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newSDs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Supplier_Deviation__c> newSDs, Map<Id,SQX_Supplier_Deviation__c> oldMap){
            super(newSDs, oldMap);
        }

        /**
        *  Method executes all actions that are run in Before Trigger context
        */
        protected override void onBefore() {
            if(Trigger.isUpdate) {

            }
        }

        /**
        *  Method executes all actions that are run in After Trigger context
        */
        protected override void onAfter() {
            if(Trigger.isUpdate) {
                transferTheInformationWhenSDClosed();
            }
        }


        /**
        * method to tranfer the supplier document information when SD is closed
        */
        public Bulkified transferTheInformationWhenSDClosed(){
            final String ACTION_NAME = 'transferTheInformationWhenSDClosed';
            Rule closeSDRule = new Rule();
            closeSDRule.addRule(Schema.SQX_Supplier_Deviation__c.Status__c, RuleOperator.Equals, STATUS_CLOSED);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.NSI, ACTION_NAME)
                .applyFilter(closeSDRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            List<SQX_Supplier_Deviation__c> sds = (List<SQX_Supplier_Deviation__c>) closeSDRule.evaluationResult;
            
            if(sds.size() > 0){
                SQX_Supplier_Deviation.informationTransfer(sds);
            }
            return this;
        }

    }

    /**
    * method to tranfer the supplier document information when SD is closed
    * @param Ids sd recordId
    */
    private static void informationTransfer(List<SQX_Supplier_Deviation__c> sds){
        
        
        SQX_Trigger_Handler triggerHandler = new SQX_Trigger_Handler();
        SQX_Trigger_Handler.Action act = triggerHandler.getAction();
        Map<Id, SQX_Supplier_Deviation__c> sdTriggerMap = new Map<Id, SQX_Supplier_Deviation__c>(sds);
        // retrieve sds information
        Map<Id,SQX_Supplier_Deviation__c> mapSD = new Map<Id,SQX_Supplier_Deviation__c>([SELECT Id, Result__c, SQX_Account__c, Activity_Comment__c,
                                                                                                        (
                                                                                                            SELECT Id, Name,Document_Name__c, Issue_Date__c,Needs_Periodic_Update__c,Notify_Before_Expiration__c,Expiration_Date__c, Due_Date__c, Comment__c,
                                                                                                            SQX_User__c, SQX_Parent__c,SQX_Parent__r.Part_Name__c, SQX_Parent__r.SQX_Part__c, SQX_Parent__r.Scope__c, SQX_Controlled_Document__c FROM SQX_Steps__r 
                                                                                                            WHERE RecordTypeId =: SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST) and Archived__c=false
                                                                                                        ) 
                                                                                                        FROM SQX_Supplier_Deviation__c WHERE Id IN: sds ]
                                                                                                      );
        if(mapSD.size()>0){
            List<SQX_Certification__c> sdrList;
            Map<Id, Id> mapSDPolicy = new Map<Id, Id>();
            Map<Id, Id> mapSDAccount = new Map<Id, Id>();
            
            // Create supplier document
            for(Id sdId : mapSD.keySet())  {
                SQX_Supplier_Deviation__c sd = mapSD.get(sdId);
                sdrList = new List<SQX_Certification__c>(); 
                if(sd.Result__c != SQX_Supplier_Deviation.SD_RESULT_REJECTED){
                    for(SQX_Deviation_Process__c dp : sd.SQX_Steps__r){
                        mapSDPolicy.put(dp.Id, sd.Id);
                        mapSDAccount.put(dp.Id, sd.SQX_Account__c);
                        SQX_Certification__c sdr = new SQX_Certification__c();
                        sdr.Part_Name__c = dp.SQX_Parent__r.Part_Name__c;
                        sdr.SQX_Part__c = dp.SQX_Parent__r.SQX_Part__c;
                        sdr.Document_Name__c = dp.Document_Name__c;
                        sdr.Description__c = dp.SQX_Parent__r.Scope__c;
                        sdr.Issue_Date__c = dp.Issue_Date__c;
                        sdr.Expire_Date__c = dp.Expiration_Date__c;
                        sdr.Notify_Before_Expiration__c = dp.Notify_Before_Expiration__c;
                        sdr.Needs_Periodic_Update__c = dp.Needs_Periodic_Update__c;
                        sdr.Account__c = sd.SQX_Account__c;
                        sdr.SQX_Controlled_Document__c = dp.SQX_Controlled_Document__c;
                        sdrList.add(sdr);
                    }
                }
            }
            if(sdrList.size()>0){
                new SQX_DB().op_insert(sdrList, new List<SobjectField>{});
            }
            
            if(!mapSDPolicy.isEmpty()){
                Set<Id> deviationProcessIds = mapSDPolicy.keySet();
                List<ContentDocumentLink> links = [SELECT ContentDocumentId, ShareType, LinkedEntityId FROM ContentDocumentLink
                                                   WHERE LinkedEntityId IN : deviationProcessIds];
                
                List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
                for(ContentDocumentLink cdl : links){
                    ContentDocumentLink cdlObj =new ContentDocumentLink();
                    cdlObj.ContentDocumentId = cdl.ContentDocumentId;
                    cdlObj.ShareType =  cdl.ShareType;
                    cdlObj.LinkedEntityId = mapSDAccount.get(cdl.LinkedEntityId);
                    cdlList.add(cdlObj);
                }
                // Add attachments
                new SQX_DB().op_insert(cdlList, new List<SObjectField> {
                            ContentDocumentLink.ContentDocumentId,
                            ContentDocumentLink.LinkedEntityId,
                            ContentDocumentLink.ShareType
                        });
             }
        }
        
    }
}