/**
 * Extension for PHR create page
*/

public with sharing class SQX_Extension_PHR {

    private SQX_Product_History_Review__c mainRecord;
    private Id complaintId;

    /**
     * Constructor
    */
    public SQX_Extension_PHR(ApexPages.StandardController controller) {
        mainRecord = (SQX_Product_History_Review__c) controller.getRecord();
        complaintId  = (Id) ApexPages.currentPage().getParameters().get('complaintId');
    }

    /**
     * Creates a new PHR record
    */
    public PageReference create() {

        try {
            mainRecord.SQX_Complaint__c = complaintId;
            mainRecord.Status__c = SQX_Product_History_Review.STATUS_DRAFT;
            Database.SaveResult result = new SQX_DB().op_insert(new List<SQX_Product_History_Review__c> { mainRecord }, new List<SObjectField> {})[0];

            PageReference pr = new PageReference('/' + result.getId());
            pr.setRedirect(true);

            return pr;
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }

        return null;
    }

}