/**
* This class will contain the common methods and static strings required for the Training Session
*/
public with sharing class SQX_Training_Session {

    public static final String STATUS_DRAFT = 'Draft',
                               STATUS_OPEN = 'Open',
                               STATUS_COMPLETE = 'Complete',
                               STATUS_CLOSED = 'Closed',
                               STATUS_VOID = 'Void';

    public static final String ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_QUEUED = 'Document Training Generation Queued',
                               ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_ERROR = 'Document Training Generation Error',
                               ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED = 'Document Training Completion Queued',
                               ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_ERROR = 'Document Training Completion Error';
    
    public static final String CUSTOM_PERMISSION_TRAINING_SESSION_SUPERVISOR = 'SQX_Training_Session_Supervisor';
     
    public static final String  PurposeOfSignature_InitiatingTrainingSession = 'Initiate',
                                PurposeOfSignature_CompletingTrainingSession = 'Complete',
                                PurposeOfSignature_ClosingTrainingSession = 'Close',
                                PurposeOfSignature_ReopeningTrainingSession = 'Reopen',
                                PurposeOfSignature_VoidingTrainingSession = 'Void';

    public static final Map<String, String> purposeOfSigMap = new Map<String, String>{
        PurposeOfSignature_InitiatingTrainingSession => Label.SQX_PS_Training_Session_Initiate,
        PurposeOfSignature_CompletingTrainingSession => Label.SQX_PS_Training_Session_Complete,
        PurposeOfSignature_ClosingTrainingSession => Label.SQX_PS_Training_Session_Close,
        PurposeOfSignature_ReopeningTrainingSession => Label.SQX_PS_Training_Session_Reopen,
        PurposeOfSignature_VoidingTrainingSession => Label.SQX_PS_Training_Session_Void
    };

    /**
    * returns use batch job custom setting to process rosters when training session state changes
    */
    public static Boolean useBatchJobToProcessRoster() {
        return SQX_Custom_Settings_Public__c.getInstance().Use_Batch_For_Session_Roster_Processing__c == true;
    }

    private SQX_Training_Session__c trainingSession = null,
                                    mainRecord = null;
    private List<SQX_Training_Session_Roster__c> roster = null;
    private Boolean escalate = false;
    private SQX_DB db;

    /**
     * Constructor to for setting the training session record
     * @param trainingSession training session to be used in the class
     */
    public SQX_Training_Session(SQX_Training_Session__c trainingSession, List<SQX_Training_Session_Roster__c> roster){
        this.trainingSession = trainingSession.clone(true);
        this.mainRecord = trainingSession;
        this.roster = roster;
        db = new SQX_DB();
        cudEnforced(); // set default escalate to false
    }
    
    /**
    * This method checks ensures that CUD security rules are enforced when performing an operation
    */
    private SQX_Training_Session cudEnforced() {
        db.withSharing();
        escalate = false;

        return this;
    }

    /**
    * This method checks if the user has edit access on the inspection record and if he or she doesn't
    * have edit rights but does have supervisor permission, escalates the permission for them.
    */
    private SQX_Training_Session escalateForSupervisor() {

        UserRecordAccess userAccess = [SELECT RecordId, HasEditAccess
                                    FROM UserRecordAccess
                                    WHERE UserId =: UserInfo.getUserId()
                                    AND RecordId =: trainingSession.Id];

        if(!userAccess.HasEditAccess &&
            SQX_Utilities.checkIfUserHasPermission(SQX_Training_Session.CUSTOM_PERMISSION_TRAINING_SESSION_SUPERVISOR)){
            /*
            * WITHOUT SHARING used
            * --------------------
            * Esclation for supervisor user to perform certain action such as void and reopen.
            */
            db.withoutSharing();
            escalate = true;
        }

        return this;
    }
    
    /**
     * method to save the training session and add record activity
     * @param actionName name of the action which is performed on the training session
     * @param comment user comment when performing the action
     * @param roster training session roster to be updated
     * @return refresh page to main record if save successful else throws error
     */
    private Database.SaveResult updateTrainingSession(String actionName, String comment){

        Database.SaveResult result = null;

        try{
            if(trainingSession.Status__c == SQX_Training_Session.STATUS_COMPLETE && !roster.isEmpty()){
                db.op_update(roster, new List<Schema.SObjectField>{SQX_Training_Session_Roster__c.Result__c});
            }
            
            result = db.op_update(new List<SQX_Training_Session__c>{trainingSession},
                                  new List<Schema.SObjectField>{SQX_Training_Session__c.Status__c}).get(0);
            
            SQX_Record_History.insertRecordHistory(String.valueOf(SQX_Training_Session__c.SObjectType), 
                                                   trainingSession.Id, 
                                                   (String) trainingSession.Id, 
                                                   comment, 
                                                   purposeOfSigMap.get(actionName),
                                                   true, 
                                                   actionName,
                                                   escalate);
        }catch(Exception ex){
            throw ex;
        }
        return result;
    }

    /**
    * checks if training session can be initiated by checking training session status
    * if training session status is draft, returns true
    * else returns false 
    * @param trainingSession Current training session to check status
    * @return boolean true or false 
    */
    public static boolean canInitiate(SQX_Training_Session__c trainingSession){
        Boolean canInitiate = false;
        if(trainingSession.Status__c == SQX_Training_Session.STATUS_DRAFT){
            canInitiate = true;
        }else if(trainingSession.Status__c == SQX_Training_Session.STATUS_OPEN){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_Err_Training_Session_Already_Open));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_INITIATE_TRAINING_SESSION));
        }
        return canInitiate;
    }
    
    /**
     * Check if training session can be completed
     * @param trainingSession Current training session to check status
     * @return boolean true or false 
     */ 
    public static boolean canComplete(SQX_Training_Session__c trainingSession){
        Boolean canComplete = false;
        if(trainingSession.Status__c == SQX_Training_Session.STATUS_DRAFT || trainingSession.Status__c == SQX_Training_Session.STATUS_OPEN){
            canComplete = true;
        }
        else if(trainingSession.Status__c == SQX_Training_Session.STATUS_COMPLETE){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_Err_Training_Session_Already_Complete));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_COMPLETE_TRAINING_SESSION));
        }
        return canComplete;
    }

    /**
     * Check if training session can be void
     * @param trainingSession Current training session to check status
     * @return boolean true or false 
     */
    public static boolean canVoid(SQX_Training_Session__c trainingSession){
        Boolean canVoid = false;

        if(trainingSession.Status__c == SQX_Training_Session.STATUS_DRAFT || trainingSession.Status__c == SQX_Training_Session.STATUS_CLOSED){
            canVoid = true;
        }
        else if(trainingSession.Status__c == SQX_Training_Session.STATUS_OPEN || trainingSession.Status__c == SQX_Training_Session.STATUS_COMPLETE){
            Boolean userHasSupervisoryPermission = SQX_Utilities.checkIfUserHasPermission(SQX_Training_Session.CUSTOM_PERMISSION_TRAINING_SESSION_SUPERVISOR);
            if(userHasSupervisoryPermission){
                canVoid = true;
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_VOID_TS_BY_CURRENT_USER));
            }
        }
        else if(trainingSession.Status__c ==  SQX_Training_Session.STATUS_VOID){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_Err_Training_Session_Already_Void));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_VOID_TRAINING_SESSION));
        }
        return canVoid;
    }
    /**
     * Check if training session can be reopened
     * @param trainingSession Current training session to check status
     * @return boolean true or false 
     */
    public static boolean canReopen(SQX_Training_Session__c trainingSession){
        Boolean canReopen = false;
        if(trainingSession.Status__c ==  SQX_Training_Session.STATUS_VOID){
            if(trainingSession.Previous_Status__c == SQX_Training_Session.STATUS_DRAFT){
                canReopen = true;
            }
            else if(trainingSession.Previous_Status__c ==  SQX_Training_Session.STATUS_OPEN || trainingSession.Previous_Status__c == SQX_Training_Session.STATUS_COMPLETE){
                Boolean userHasSupervisoryPermission = SQX_Utilities.checkIfUserHasPermission(SQX_Training_Session.CUSTOM_PERMISSION_TRAINING_SESSION_SUPERVISOR);
                if(userHasSupervisoryPermission){
                    canReopen = true;
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_REOPEN_TS_BY_CURRENT_USER));
                }
            }
        }
        else if(trainingSession.Status__c == SQX_Training_Session.STATUS_COMPLETE){
            canReopen = true;
        }
        else if(trainingSession.Status__c == SQX_Training_Session.STATUS_OPEN){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_Err_Training_Session_Already_Open));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_REOPEN_TRAINING_SESSION));
        }
        return canReopen;
    }
    
    /**
     * Check if training session can be closed
     * @param trainingSession Current training session to check status
     * @return boolean true or false 
     */
    public static boolean canClose(SQX_Training_Session__c trainingSession){
        Boolean canClose = true;
        if(trainingSession.Status__c == SQX_Training_Session.STATUS_CLOSED){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_Err_Training_Session_Already_Close));
            canClose = false;
        }
        return canClose;
    }
    
    /**
     * method to initiate training session
     * @param comment user comment
     * @return call to updateTrainingSession method result
     */
    public Database.SaveResult initiate(String comment){
        trainingSession.Status__c = SQX_Training_Session.STATUS_OPEN;
        
        return cudEnforced().updateTrainingSession(PurposeOfSignature_InitiatingTrainingSession, comment);
    }
    
    /**
     * method to reopen training session
     * @param comment user comment
     * @return call to updateTrainingSession method result
     */
    public Database.SaveResult reopen(String comment){
        if(trainingSession.Status__c == SQX_Training_Session.STATUS_COMPLETE ||  
           (trainingSession.Status__c == SQX_Training_Session.STATUS_VOID && trainingSession.Previous_Status__c == SQX_Training_Session.STATUS_COMPLETE)){
            trainingSession.Status__c = SQX_Training_Session.STATUS_OPEN;
        }
        else {
            trainingSession.Status__c = trainingSession.Previous_Status__c;
        }
        
        return escalateForSupervisor().updateTrainingSession(PurposeOfSignature_ReopeningTrainingSession, comment);
    }
    
    /**
     * method to void training session
     * @param comment user comment
     * @return call to updateTrainingSession method result
     */
    public Database.SaveResult void(String comment){
        trainingSession.Status__c = SQX_Training_Session.STATUS_VOID;
        
        return escalateForSupervisor().updateTrainingSession(PurposeOfSignature_VoidingTrainingSession, comment);
    }
    
    /**
     * method to complete training session
     * @param comment user comment
     * @return call to updateTrainingSession method result
     */
    public Database.SaveResult complete(String comment){
        trainingSession.Status__c = SQX_Training_Session.STATUS_COMPLETE;
        
        return cudEnforced().updateTrainingSession(PurposeOfSignature_CompletingTrainingSession, comment);
    }
    
    /**
     * method to close training session
     * @param comment user comment
     * @return call to updateTrainingSession method result
     */
    public Database.SaveResult close(String comment){
        trainingSession.Status__c = SQX_Training_Session.STATUS_CLOSED;
        
        return cudEnforced().updateTrainingSession(PurposeOfSignature_ClosingTrainingSession, comment);      
    }

    /**
    * Bulkified class for Training Session
    * without sharing is required to query/insert/update personnel and its child records when requirements of a controlled documents get activated/deactivated
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        public Bulkified() {
        }
        
        public Bulkified(List<SQX_Training_Session__c> newList , Map<Id, SQX_Training_Session__c> oldMap) {
            super(newList, oldMap);
        }

        /**
        * When the Training Session is transitioned from Draft to Open and Subject is given,
        * All the Related Document with Fulfilled Requirement Checked is copied to Training Fulfilled Requirement.
        * @author Sudeep Maharjan
        * @date 2017/01/23
        * @story [SQX-2859]
        */
        public Bulkified createTrainingFulfilledRequirementForRelatedControlledDocument() {

            final String ACTION_NAME = 'createTrainingFulfilledRequirementForRelatedControlledDocument';

            Rule filter = new Rule();
            filter.addTransitionRule(SQX_Training_Session__c.Status__c, RuleOperator.Transitioned, STATUS_OPEN, STATUS_DRAFT);
            filter.addRule(SQX_Training_Session__c.SQX_Controlled_Document__c, RuleOperator.NotEquals, null);

            this.resetView()
                .removeProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Training_Session__c> trSess = (List<SQX_Training_Session__c>) filter.evaluationResult;

            List<SQX_Training_Fulfilled_Requirement__c> trFulRequirement = new List<SQX_Training_Fulfilled_Requirement__c>();

            if (trSess.size() > 0) {
                this.addToProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME, filter.evaluationResult);
                
                for (SQX_Training_Session__c trainS : trSess){
                    // Get previous Training Fulfilled Requirement if present 
                    List<SQX_Training_Fulfilled_Requirement__c> previoustrFR = [SELECT Id,
                                                                                    SQX_Controlled_Document__c,
                                                                                    SQX_Training_Session__c
                                                                                FROM SQX_Training_Fulfilled_Requirement__c
                                                                                WHERE SQX_Training_Session__c =: trainS.Id];

                    List<SQX_Related_Document__c> relatedDoc = [SELECT Id, Fulfilled_Requirement__c, Referenced_Document__c
                                                                FROM SQX_Related_Document__c
                                                                WHERE Controlled_Document__c = :trainS.SQX_Controlled_Document__c
                                                                    AND Fulfilled_Requirement__c = true];

                    // Set is used to store unique ids of previously created Training Fulfilled Requirements
                    Set<Id> ids = new Set<Id>();

                    if( previoustrFR.size() > 0){

                        for (SQX_Training_Fulfilled_Requirement__c prvTrFR : previoustrFR){
                            ids.add(prvTrFR.SQX_Controlled_Document__c);
                        }
                        
                    }

                    for(SQX_Related_Document__c reltdDoc : relatedDoc){

                        if(!ids.contains(reltdDoc.Referenced_Document__c)){
                            // Copy Controlled Document to Training Fulfilled Requirement 
                            SQX_Training_Fulfilled_Requirement__c  trFR = new SQX_Training_Fulfilled_Requirement__c();
                            trFR.SQX_Training_Session__c = trainS.Id;
                            trFR.SQX_Controlled_Document__c = reltdDoc.Referenced_Document__c;
                            trFulRequirement.add(trFR);
                        } 

                    }
                    
                }

                if (trFulRequirement.size() > 0) {
                    new SQX_DB().op_insert(trFulRequirement, new List<Schema.SObjectField>{ 
                        SQX_Training_Fulfilled_Requirement__c.SQX_Training_Session__c,
                        SQX_Training_Fulfilled_Requirement__c.SQX_Controlled_Document__c});
                }
            }
            return this;
        }
        
        /**
         *  processes rosters of completed sessions to generate/reset pending document trainings
         *  processes rosters of closed sessions to trainer sign-off and complete all linked pending document trainings
         */
        public Bulkified processRostersOnCompletionOrClosure() {
            final String ACTION_NAME_COMPLETE = 'processRostersOnCompletion',
                ACTION_NAME_CLOSED = 'processRostersOnClosure';
            
            Boolean useBatchJob = useBatchJobToProcessRoster();
            DateTime now = System.now();
            Map<String, String> batchJobStatusMap = new Map<String, String>{
                STATUS_COMPLETE => ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_QUEUED,
                STATUS_CLOSED => ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED
            };
            Map<String, Map<Id, SQX_Training_Session__c>> tsFiltered = new Map<String, Map<Id, SQX_Training_Session__c>>{
                STATUS_COMPLETE => new Map<Id, SQX_Training_Session__c>(),
                STATUS_CLOSED => new Map<Id, SQX_Training_Session__c>()
            };
            Map<String, Set<Id>> tsIdsToProcess = new Map<String, Set<Id>>{
                STATUS_COMPLETE => new Set<Id>(),
                STATUS_CLOSED => new Set<Id>()
            };
            Map<SObjectType, List<SObject>> objsToUpdateMap = new Map<SObjectType, List<SObject>>();
            SObjectType sessionObjType = SQX_Training_Session__c.SObjectType,
                rosterObjType = SQX_Training_Session_Roster__c.SObjectType;
            
            // get already processed records to prevent re-entrant
            Set<Id> processedIds_Complete = getProcessedRecordIdsFor(SQX.TrainingSessionRoster, ACTION_NAME_COMPLETE),
                processedIds_Closed = getProcessedRecordIdsFor(SQX.TrainingSessionRoster, ACTION_NAME_CLOSED);
            
            resetView();
            
            for (SQX_Training_Session__c ts : (List<SQX_Training_Session__c>)view) {
                SQX_Training_Session__c oldTs = (SQX_Training_Session__c)oldValues.get(ts.Id);
                
                // filter completed or closed sessions with rosters to process on status change when previously queued batch job has processed successfully
                if ((ts.Status__c == STATUS_COMPLETE && !processedIds_Complete.contains(ts.Id)) || (ts.Status__c == STATUS_CLOSED && !processedIds_Closed.contains(ts.Id))) {
                    if (ts.Total_Trainees__c > 0 && oldTs != null && oldTs.Status__c != ts.Status__c && ts.Roster_Batch_Job_Status__c == null) {
                        // add to filtered list
                        tsFiltered.get(ts.Status__c).put(ts.Id, ts);
                        
                        if (useBatchJob == true) {
                            // queue session rosters for document training generation or completion as per session status using batch job
                            List<SObject> tsnToUpdateMap = objsToUpdateMap.get(sessionObjType);
                            if (tsnToUpdateMap == null) {
                                tsnToUpdateMap = new List<SObject>();
                                objsToUpdateMap.put(sessionObjType, tsnToUpdateMap);
                            }
                            tsnToUpdateMap.add(new SQX_Training_Session__c(
                                Id = ts.Id,
                                Roster_Batch_Job_Status__c = batchJobStatusMap.get(ts.Status__c),
                                Roster_Processing_Queued_On__c = now
                            ));
                        }
                        else {
                            // list completed or closed session to process rosters immediately
                            tsIdsToProcess.get(ts.Status__c).add(ts.Id);
                        }
                    }
                }
            }
            
            if (!tsFiltered.get(STATUS_COMPLETE).isEmpty() || !tsFiltered.get(STATUS_CLOSED).isEmpty()) {
                addToProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME_COMPLETE, tsFiltered.get(STATUS_COMPLETE).values());
                addToProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME_CLOSED, tsFiltered.get(STATUS_CLOSED).values());
                
                if (!tsIdsToProcess.get(STATUS_COMPLETE).isEmpty() || !tsIdsToProcess.get(STATUS_CLOSED).isEmpty()) {
                    // process rosters immediately when batch job is not enabled
                    for (SQX_Training_Session_Roster__c r : [SELECT SQX_Training_Session__c FROM SQX_Training_Session_Roster__c
                                                             WHERE SQX_Training_Session__c IN :tsIdsToProcess.get(STATUS_COMPLETE)
                                                                OR SQX_Training_Session__c IN :tsIdsToProcess.get(STATUS_CLOSED)
                                                             ORDER BY SQX_Training_Session__c, Result__c, Name]) {
                        // document training generation roster activity code for completed sessions
                        // and document training completion roster activity code for closed sessions
                        String activityCode = tsIdsToProcess.get(STATUS_COMPLETE).contains(r.SQX_Training_Session__c)
                                                ? SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_GENERATION
                                                : SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_COMPLETION;
                        
                        // set formatted activity code in roster
                        r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(activityCode, now);
                        r.Batch_Job_Error__c = null;
                        
                        // add to update list
                        List<SObject> tsrToUpdateMap = objsToUpdateMap.get(rosterObjType);
                        if (tsrToUpdateMap == null) {
                            tsrToUpdateMap = new List<SObject>();
                            objsToUpdateMap.put(rosterObjType, tsrToUpdateMap);
                        }
                        tsrToUpdateMap.add(r);
                    }
                }
                
                // FLS not checked for internal fields used for processing rosters with ot without batch job
                new SQX_DB().op_update(objsToUpdateMap,
                    new Map<SObjectType, List<SObjectField>>{
                        sessionObjType => new List<SObjectField>(),
                        rosterObjType => new List<SObjectField>()
                    }
                );
            }
            
            return this;
        }
        
        /**
         *  When Training session is void or reopened from complete state
         *  All pending document training created during completion are removed
         *  @author Sajal Joshi
         *  @date 09/03/2017
         *  @story [SQX-2876]
         */
        public Bulkified resetPendingDocumentTrainingsWhenVoided() {
            final String ACTION_NAME = 'resetPendingDocumentTrainingsWhenVoided';
            
            Rule voidSessionsFilter = new Rule();
            voidSessionsFilter.addRule(SQX_Training_Session__c.Status__c, RuleOperator.HasChanged);
            voidSessionsFilter.addRule(SQX_Training_Session__c.Status__c, RuleOperator.Equals, STATUS_VOID);
            
            Rule reopenCompletedSessionsFilter = new Rule();
            reopenCompletedSessionsFilter.addTransitionRule(SQX_Training_Session__c.Status__c, RuleOperator.Transitioned, STATUS_OPEN, STATUS_COMPLETE);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME)
                .groupByRules(new Rule[]{ voidSessionsFilter, reopenCompletedSessionsFilter }, RuleCheckMethod.onCreateAndEveryEdit);
            
            List<SQX_Training_Session__c> voidedSessions = (List<SQX_Training_Session__c>)voidSessionsFilter.evaluationResult;
            voidedSessions.addAll((List<SQX_Training_Session__c>)reopenCompletedSessionsFilter.evaluationResult);
            
            if (!voidedSessions.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.TrainingSession, ACTION_NAME, voidedSessions);
                
                new PrivilegeEscalator().resetPendingDocumentTrainings(voidedSessions);
            }
            
            return this;
        }
        
        /**
         * method to lock the training session after the session is completed or voided or closed
         * and save the previous status of the training session before it is voided
         */
        public Bulkified lockTrainingSession(){
            this.resetView();

            Map<Id, SObject> oldValues = this.oldValues;
            List<SQX_Training_Session__c> sessionList = (List<SQX_Training_Session__c>) this.view;

            if(!sessionList.isEmpty()){
                for (SQX_Training_Session__c session : sessionList) {
                    Set<String> lockStatuses = new Set<String>{ SQX_Training_Session.STATUS_COMPLETE,
                                                                SQX_Training_Session.STATUS_CLOSED,
                                                                SQX_Training_Session.STATUS_VOID };
                    session.Is_Locked__c = false;
                    
                    if (lockStatuses.contains(session.Status__c)) {
                        session.Is_Locked__c = true;
                        
                        if(session.Status__c == SQX_Training_Session.STATUS_VOID){
                            SQX_Training_Session__c oldSession = (SQX_Training_Session__c)oldValues.get(session.Id);
                            session.Previous_Status__c = oldSession.Status__c;
                        }
                    }
                }
            }
            
            return this;
        }
    }
    
    
    /**
    * system operations for training session object that require to query and update records such as personnel document trainings
    */
    private without sharing class PrivilegeEscalator {
        
        /**
        * resets document trainings for training sessions to restart or void sessions
        *   a) deletes pending training if it is created by the sesssion and has no active PDJFs
        *   b) clears session link from pending trainings either when it cannot be deleted or when it is not created by session
        * @param sessions    training sessions
        */
        public void resetPendingDocumentTrainings(List<SQX_Training_Session__c> sessions) {
            List<SQX_Personnel_Document_Training__c> ptsToDelete = new List<SQX_Personnel_Document_Training__c>();
            List<SQX_Personnel_Document_Training__c> pdsToUpdate = new List<SQX_Personnel_Document_Training__c>();
            
            for (SQX_Personnel_Document_Training__c pdt : [ SELECT Id, Is_Created__c, SQX_Training_Session__r.Status__c,
                                                                (
                                                                    SELECT Id
                                                                    FROM SQX_Personnel_Document_Job_Functions__r
                                                                    WHERE Is_Archived__c = false
                                                                )
                                                            FROM SQX_Personnel_Document_Training__c 
                                                            WHERE SQX_Training_Session__c IN :sessions 
                                                                AND Status__c IN :SQX_Personnel_Document_Training.PENDING_STATUSES ]) {
                if (pdt.Is_Created__c && pdt.SQX_Personnel_Document_Job_Functions__r.size() == 0) {
                    ptsToDelete.add(pdt);
                }
                else {
                    pdt.SQX_Training_Session__c = null;
                    pdsToUpdate.add(pdt);
                }
            }
            
            if (!ptsToDelete.isEmpty()) { 
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the running user may not have permission on 
                * personnel document training personnel
                */
                new SQX_DB().withoutSharing().op_delete(ptsToDelete);
            }
            
            if (!pdsToUpdate.isEmpty()) {
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the running user may not have permission on 
                * personnel document training personnel
                */
                new SQX_DB().withoutSharing().op_update(pdsToUpdate, new List<Schema.SObjectField>{ SQX_Personnel_Document_Training__c.SQX_Training_Session__c });
            }
        }
        
    }
}