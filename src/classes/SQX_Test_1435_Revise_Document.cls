@isTest
public class SQX_Test_1435_Revise_Document{
    /**
    * @story SQX-1435
    * Ensures revise will copy fields except mentioned in blacklist from parent document is copied while revising
    */
    public testmethod static void givenReviseDocument_CopyApprovers() {
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser) {
            //add a document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // ACT: revise document
            SQX_Test_Controlled_Document revisedDoc = testDoc.revise();
            
            //assert that status field is not copied
            System.assertEquals(SQX_Controlled_Document.STATUS_DRAFT, revisedDoc.doc.Document_Status__c,
                'Expected status not to be copied and status should be set as draft');

            //assert that title field is copied
            System.assertEquals(testDoc.doc.Title__c, revisedDoc.doc.Title__c,
                'Expected title field is copied');


            
        }
    }

    /**
    * When: when controlled document revise
    * Given: given checkout information
    * Then: checkout information fields is not be copied
    */   
    public testmethod static void WhenRevise_givenCheckoutInformation_ThenIsNotCopied() {
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser) {
            // Arrange:add a document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();    
            testDoc.doc.Checked_Out__c = true;
            testDoc.doc.SQX_Checked_Out_By__c = standardUser.Id;
            testDoc.doc.Checked_Out_On__c = Date.Today();
            testDoc.doc.Collaboration_Group_Content_Id__c = '121212';
            testDoc.doc.Collaboration_Group_Id__c = '4223232';  
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            testDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            testDoc.save();
            
            // ACT: revise document
            SQX_Test_Controlled_Document revisedDoc = testDoc.revise();

            // Assert: ensured that checkout information fields is not copied
            System.assertEquals(false, revisedDoc.doc.Checked_Out__c,'Expected Checked_Out__c field not to be copied');
            System.assertEquals(null, revisedDoc.doc.SQX_Checked_Out_By__c,'Expected SQX_Checked_Out_By__c field  not to be copied');
            System.assertEquals(null, revisedDoc.doc.Checked_Out_On__c ,'Expected Checked_Out_On__c field  not to be copied');
            System.assertEquals(null, revisedDoc.doc.Collaboration_Group_Content_Id__c,'Expected Collaboration_Group_Content_Id__c field  not to be copied');
            System.assertEquals(null, revisedDoc.doc.Collaboration_Group_Id__c,'Expected Collaboration_Group_Id__c field  not to be copied');
        }
    }
}