/**
* Unit tests for SQX_Controller_Upload_Attachment
*
* @author Sagar Shrestha
* @date 2014/5/13
*/
@isTest
public class SQX_Test_Controller_Upload_Attachment{

    /**
    *   Setup: Create an Account Object
    *   Action: Post an attachment to the account object
    *   Expected: An attachment is added
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    
     
    @testSetup static void setupTestData(){
       // add required users
      UserRole role = SQX_Test_Account_Factory.createRole();
      User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }


    public static testmethod void attachmentPostedOnAccount_attachmentIsAdded(){  

        Account account = SQX_Test_Account_Factory.createAccount();

        SQX_Controller_Upload_Attachment attach= new  SQX_Controller_Upload_Attachment();

        PageReference pageRef = Page.SQX_CommonActions;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_PARENT_ID, account.Id); 
        ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_UUID, 'abc'); //garbage value
        ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_ATTACH_TO_PARENT, account.Id); 

        attach.attachment.Body = Blob.valueOf('I am testing :-)');
        attach.attachment.Name = 'Test';
        attach.upload();

        string lastupload=attach.getLastUpload();

        string error= attach.ErrorMessage;

        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE Id = : attach.AttachmentId and parentId=:account.Id];

        System.assert(attachments.size() == 1, 
             'Expected the attachment to be added to the account, but could not find it');

    }

    /**
    *   Setup: Create a tempstore for current user
    *   Action: Post an attachment for the tempstore
    *   Expected: An attachment is added for the store
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    public static testmethod void tempstoreCreatedForCurrentUser_attachmentIsAddedForStore(){  

        SQX_Test_TempStore.givenARecordExistsInTempStore_CorrectValueIsReturned();

        SQX_Controller_Upload_Attachment attach= new  SQX_Controller_Upload_Attachment();

        PageReference pageRef = Page.SQX_CommonActions;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_PARENT_ID, UserInfo.getUserId()); 
        ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_UUID, 'abc'); //garbage value

        attach.attachment.Body = Blob.valueOf('I am testing :-)');
        attach.attachment.Name = 'Test';
        attach.upload();

        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE Id = : attach.AttachmentId ];

        System.assert(attachments.size() == 1, 
             'Expected the attachment to be added to user, but could not find it'+ attach.attachment +' '+attach.AttachmentId+' '+attachments.size());

    }
    /**  
    *   Given: Create an Account Object.
    *   When : Post an attachment with null body to the account object.
    *   Then : error is generated due to the custom exception when body and id of attachment is null
    *
    * @author: Paras Kumar Bishwakarma
    * @date: 2016/6/14
    * @story: [SQX-2090]
    */
    public static testmethod void givenAccount_WhenAttachementIsPostedWithBodyNUll_RelatedFileNotFoundError(){  
       
        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map 
        User stdUser = allUsers.get('standardUser');
        
        System.runas(stdUser){

            //Arrange : create the account object , set the current page and put the required test ids to the the current page
            Account account = SQX_Test_Account_Factory.createAccount(); 
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_PARENT_ID, account.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_UUID, 'abc'); //garbage value
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_ATTACH_TO_PARENT, account.Id);

            //create the SQX_Controller_Upload_Attachment object and set body to null.
            SQX_Controller_Upload_Attachment attach= new  SQX_Controller_Upload_Attachment();
            attach.attachment.Body = null;//body null
            attach.attachment.Name = 'Test';

            //Act: upload attachment
            attach.upload();
            string error= attach.ErrorMessage;

            //Assert : expected error should not be null
            System.assert(error!=null, 'Expected to throw error'+error); 
        }
    }
    /**
    *   Given: Create an Account Object  
    *   When: Post an attachment to the account object 
    *   Then:attachment is added
    *
    *   Given : Create an Account Object
    *   When : Post the attachement to the same object with attachment's body null
    *   Then: attachment is updated ie posted
    *
    * @author: Paras Kumar Bishwakarma
    * @date: 2016/6/14
    * @story: [SQX-2090]
    */
    public static testmethod void givenAccount_WhenAttachementIsPostedWithBodyNullAndIdNotNull_attachmentIsAdded(){  

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map 
        User stdUser = allUsers.get('standardUser');
        
        System.runas(stdUser){

            //Arrange : create the account object , set the current page and put the required test ids to the the current page
            Account account = SQX_Test_Account_Factory.createAccount(); 
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_PARENT_ID, account.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_UUID, 'abc'); //garbage value
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_ATTACH_TO_PARENT, account.Id);

            //create the SQX_Controller_Upload_Attachment object and set body to some value.
            SQX_Controller_Upload_Attachment attach= new  SQX_Controller_Upload_Attachment();
            attach.attachment.Body = Blob.valueOf('I am testing :-)');
            attach.attachment.Name = 'Test';

            //Act: upload attachment
            attach.upload();
            string error= attach.ErrorMessage;

            //Assert : expected error should be null
            System.assert(error==null, 'Expected to upload file but throws error'+error);
            attach.attachment.Body = null;//seting the body null

            //upload attachment again
            attach.upload();
            string error1= attach.ErrorMessage;

            //Assert : expected error1 should be null
            System.assert(error1==null, 'Expected to upload file but throws error'+error1); 
        }
    }
    /**
    *   Given: Create an Account Object  
    *   When: Post an attachment to the account object 
    *   Then:attachment is added 
    *
    *   Given : Create an Account Object.
    *   When : Post the attachement to the same object with attachment's body not null and id null
    *   Then: attachment is updated ie posted
    *
    * @author: Paras Kumar Bishwakarma
    * @date: 2016/6/14
    * @story: [SQX-2090]
    */
    public static testmethod void givenAccount_WhenAttachementIsPostedWithBodyNotNullAndIdNotNull_attachmentIsAdded(){  

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map 
        User stdUser = allUsers.get('standardUser');
        
        System.runas(stdUser){

            //Arrange : create the account object , set the current page and put the required test ids to the the current page
            Account account = SQX_Test_Account_Factory.createAccount(); 
            PageReference pageRef = Page.SQX_CommonActions;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_PARENT_ID, account.Id); 
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_UUID, 'abc'); //garbage value
            ApexPages.currentPage().getParameters().put(SQX_Controller_Upload_Attachment.PARAM_ATTACH_TO_PARENT, account.Id);

            //create the SQX_Controller_Upload_Attachment object and set body to some value.
            SQX_Controller_Upload_Attachment attach= new  SQX_Controller_Upload_Attachment();
            attach.attachment.Body = Blob.valueOf('I am testing :-)');
            attach.attachment.Name = 'Test';

            //Act: upload attachment
            attach.upload();
            string error= attach.ErrorMessage;

            //Assert : expected error should be null
            System.assert(error==null, 'Expected to upload file but throws error'+error);
            attach.attachment.Body = Blob.valueOf('I am testing1 :-)');//seting the body again
            
            //upload attachment again
            attach.upload();
            string error1= attach.ErrorMessage;

            //Assert : expected error1 should be null
            System.assert(error1==null, 'Expected to upload file but throws error'+error1); 
        }
    }
}

