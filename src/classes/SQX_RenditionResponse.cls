/**
* The response for the given rendition 
*/
global class SQX_RenditionResponse {
    global enum Status {RenditionReady, Processing, FetchingThroughProxy, Error}
    global enum ErrorCodes { UnsupportedFormat, NotSubmitted, UnknownError }
    
    /**
    * The status of the response
    */
    global Status status {get; set;}

    /**
    * Job Id of the request
    */
    global String jobId {get; set;}
    
    /**
    * The related content version if a response has blob
    */
    global ContentVersion result {get; set;}
    
    /**
    * Error code that is related to rendition
    */
    global ErrorCodes errorCode {get; set;}

    /**
    * the description of the error that has occurred
    */
    global String errorDescription {get; set;}

    /**
    * The size of file in bytes
    */
    public Integer bytes {get; set;}

    /**
    * Returns a completed rendition response for the given request
    * @param request the request that is to be completed
    * @return returns the completed/ready response
    */
    public static SQX_RenditionResponse getCompletedResponseFor(SQX_RenditionRequest request){
        SQX_RenditionResponse response = new SQX_RenditionResponse();
        response.status = Status.RenditionReady;
        response.jobId = request.jobId;

        return response;
    }
}