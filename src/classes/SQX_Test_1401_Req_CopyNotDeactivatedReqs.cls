/**
* Unit tests for copying not deactivated requirements from template document or parent document when revising documents
*
* @author Dibya Shrestha
* @date 2015/08/17
* 
*/
@IsTest
public class SQX_Test_1401_Req_CopyNotDeactivatedReqs {
    
    static Boolean runAllTests = true,
        run_givenCreateNewDocumentUsingTemplateDoc_NotDeactivatedReqsOfTemplateDocAreCopied = false,
        run_givenReviseDocument_NotDeactivatedReqsOfParentDocAreCopied = false;
    
    /**
    * This test ensures that not deactivated requirements of related template document are copied when creating new document using template document
    */
    public static testmethod void givenCreateNewDocumentUsingTemplateDoc_NotDeactivatedReqsOfTemplateDocAreCopied(){
        if (!runAllTests && !run_givenCreateNewDocumentUsingTemplateDoc_NotDeactivatedReqsOfTemplateDocAreCopied){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // creating required job functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            jf.save();
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            jf = new SQX_Test_Job_Function(jf2);
            jf.save();
            SQX_Job_Function__c jf3 = new SQX_Job_Function__c( Name = 'job function 3 for test' );
            jf = new SQX_Test_Job_Function(jf3);
            jf.save();
            
            // add required template document
            SQX_Test_Controlled_Document testTemplateDoc = new SQX_Test_Controlled_Document();
            testTemplateDoc.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            testTemplateDoc.save(true);
            testTemplateDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add deactivated requirement for template document
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testTemplateDoc.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            Database.insert(Req1, false);
            Req1.Active__c = false;
            Database.update(Req1, false);
            
            // add activated requirement for template document
            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testTemplateDoc.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Optional__c = false,
                Active__c = true
            );
            Database.insert(Req2, false);
            
            // add not activated requirement for template document
            SQX_Requirement__c Req3 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testTemplateDoc.doc.Id,
                SQX_Job_Function__c = jf3.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            Database.insert(Req3, false);
            
            // ACT: create new document using template document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.doc.Document_Type__c = testTemplateDoc.doc.Id;
            testDoc.save();
            
            // get requirements of newly created document
            List<SQX_Requirement__c> testDocReqs = [SELECT SQX_Job_Function__c, Level_of_Competency__c, Optional__c, Active__c
                                                    FROM SQX_Requirement__c
                                                    WHERE SQX_Controlled_Document__c = :testDoc.doc.Id];
            
            System.assert(testDocReqs.size() == 2,
                'Expected number of requirements to be created while using template document is 2.');
            
            SQX_Requirement__c copyOfReq1 = null;
            SQX_Requirement__c copyOfReq2 = null;
            SQX_Requirement__c copyOfReq3 = null;
            for (SQX_Requirement__c req : testDocReqs) {
                if (req.SQX_Job_Function__c == Req1.SQX_Job_Function__c) {
                    copyOfReq1 = req;
                }
                if (req.SQX_Job_Function__c == Req2.SQX_Job_Function__c) {
                    copyOfReq2 = req;
                }
                if (req.SQX_Job_Function__c == Req3.SQX_Job_Function__c) {
                    copyOfReq3 = req;
                }
            }
            
            System.assert(copyOfReq1 == null,
                'Expected job function of deactivated requirement not to be copied from template document.');
            
            System.assert(copyOfReq2 != null,
                'Expected job function of activated requirement to be copied from template document.');
            
            System.assert(copyOfReq3 != null,
                'Expected job function of not activated requirement to be copied from template document.');
            
            System.assert(copyOfReq2.Active__c == false,
                'Expected requirement of the new document not to be activated.');
            
            System.assert(copyOfReq3.Active__c == false,
                'Expected requirement of the new document not to be activated.');
            
            System.assert(copyOfReq2.Level_of_Competency__c == Req2.Level_of_Competency__c,
                'Expected level of competency of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq3.Level_of_Competency__c == Req3.Level_of_Competency__c,
                'Expected level of competency of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq2.Optional__c == Req2.Optional__c,
                'Expected optional value of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq3.Optional__c == Req3.Optional__c,
                'Expected optional value of the newly copied requirement to be same as that of template document.');
        }
    }
    
    /**
    * This test ensures that not deactivated requirements of parent document are copied when creating new document using template document
    */
    public static testmethod void givenReviseDocument_NotDeactivatedReqsOfParentDocAreCopied(){
        if (!runAllTests && !run_givenReviseDocument_NotDeactivatedReqsOfParentDocAreCopied){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // creating required job functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            jf.save();
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            jf = new SQX_Test_Job_Function(jf2);
            jf.save();
            SQX_Job_Function__c jf3 = new SQX_Job_Function__c( Name = 'job function 3 for test' );
            jf = new SQX_Test_Job_Function(jf3);
            jf.save();
            
            // add required document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add deactivated requirement for document
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            Database.insert(Req1, false);
            Req1.Active__c = false;
            Database.update(Req1, false);
            
            // add activated requirement for document
            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Optional__c = false,
                Active__c = true
            );
            Database.insert(Req2, false);
            
            // add not activated requirement for document
            SQX_Requirement__c Req3 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc.doc.Id,
                SQX_Job_Function__c = jf3.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            Database.insert(Req3, false);
            
            // ACT: revise current document
            SQX_Test_Controlled_Document revisedTestDoc = testDoc.revise();
            
            // get requirements of newly revised document
            List<SQX_Requirement__c> testDocReqs = [SELECT SQX_Job_Function__c, Level_of_Competency__c, Optional__c, Active__c
                                                    FROM SQX_Requirement__c
                                                    WHERE SQX_Controlled_Document__c = :revisedTestDoc.doc.Id];
            
            System.assert(testDocReqs.size() == 2,
                'Expected number of requirements to be created for the new revised document is 2.');
            
            SQX_Requirement__c copyOfReq1 = null;
            SQX_Requirement__c copyOfReq2 = null;
            SQX_Requirement__c copyOfReq3 = null;
            for (SQX_Requirement__c req : testDocReqs) {
                if (req.SQX_Job_Function__c == Req1.SQX_Job_Function__c) {
                    copyOfReq1 = req;
                }
                if (req.SQX_Job_Function__c == Req2.SQX_Job_Function__c) {
                    copyOfReq2 = req;
                }
                if (req.SQX_Job_Function__c == Req3.SQX_Job_Function__c) {
                    copyOfReq3 = req;
                }
            }
            
            System.assert(copyOfReq1 == null,
                'Expected job function of deactivated requirement not to be copied.');
            
            System.assert(copyOfReq2 != null,
                'Expected job function of activated requirement to be copied from the parent document when revised.');
            
            System.assert(copyOfReq3 != null,
                'Expected job function of not activated requirement to be copied from the parent document when revised.');
            
            System.assert(copyOfReq2.Active__c == false,
                'Expected requirement of the new document not to be activated.');
            
            System.assert(copyOfReq3.Active__c == false,
                'Expected requirement of the new document not to be activated.');
            
            System.assert(copyOfReq2.Level_of_Competency__c == Req2.Level_of_Competency__c,
                'Expected level of competency of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq3.Level_of_Competency__c == Req3.Level_of_Competency__c,
                'Expected level of competency of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq2.Optional__c == Req2.Optional__c,
                'Expected optional value of the newly copied requirement to be same as that of template document.');
            
            System.assert(copyOfReq3.Optional__c == Req3.Optional__c,
                'Expected optional value of the newly copied requirement to be same as that of template document.');
        }
    }
}