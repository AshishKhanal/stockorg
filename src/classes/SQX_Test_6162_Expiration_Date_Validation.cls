/**
* Test case for Expiration Date should not be less than Effective Date
*/
@IsTest
public class SQX_Test_6162_Expiration_Date_Validation {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation sdRecord= new SQX_Test_Supplier_Deviation().save();
        }
    }
    
    /**
    * GIVEN : given supplier deviation record with expiration date less than effective date 
    * WHEN : save the record 
    * THEN : error is thrown
    */ 
    public static testMethod void giveSDWithExpirationLessThanEffectiveDate_WhenSaveTheRecord_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Retrive SD record
            SQX_Supplier_Deviation__c sd = [SELECT Id, Name, Expiration_Date__c, Effective_Date__c FROM SQX_Supplier_Deviation__c ];
            
            //Act: Save the record
            sd.Effective_Date__c = Date.today();
            sd.Expiration_Date__c = Date.today()-1;
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__c> { sd },  new List<Schema.SObjectField>{});
            
            //Assert: Ensured that throws validation error message
            System.assert(result[0].isSuccess() == false, 'Expiration date grater than effective date.');
            System.assertEquals('Expiration Date should not be less than Effective Date', result[0].getErrors().get(0).getMessage());
        }
    }
    
    /**
    * GIVEN : given supplier deviation record with expiration date greater than effective date 
    * WHEN : save the record 
    * THEN : save successfully
    */ 
    public static testMethod void giveSDWithExpirationGreaterThanEffectiveDate_WhenSaveTheRecord_ThenSaveSuccussfully(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Retrive SD record
            SQX_Supplier_Deviation__c sd = [SELECT Id, Name, Expiration_Date__c, Effective_Date__c FROM SQX_Supplier_Deviation__c ];
            
            //Act: Save the record
            sd.Effective_Date__c = Date.today();
            sd.Expiration_Date__c = Date.today()+1;
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__c> { sd },  new List<Schema.SObjectField>{});
            
            //Assert: Ensured that save successfully
            System.assert(result[0].isSuccess() == true, 'Save successfully.');
        }
    }
}