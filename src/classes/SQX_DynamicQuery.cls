/**
* This class performs a dynamic query for any given object and returns the query result.
* It also accepts a query filter(Rule element)
*/
public with sharing class SQX_DynamicQuery{

    private final static String QUERY_FORMAT = 'SELECT {!FieldList}' + ' FROM {!ObjectName} {!FilterString} {!OrderByClause} LIMIT :pageSize OFFSET :offset ';

    static Map<Id, SObject> objectQueryCache = new Map<Id, SObject>();

    /**
     * Returns an object with matching Id
     * @param  objectId the object id to match object
     * @return          retursn the details of the sobject type
     */
    public static SObject getSObjectWithId(Id objectId, Boolean readOnlyIdAndName) {
        if(!objectQueryCache.containsKey(objectId)) {
            SObjectType objType = objectId.getSObjectType();

            SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
            filter.field = 'Id';
            filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter.value = objectId;

            List<SObject> objs;
            if(readOnlyIdAndName) {

                List<SObjectField> fieldsList = new List<SObjectField> { objType.getDescribe().fields.getMap().get('Id') };

                String nameField = getNameField(objType);
                if(!String.isBlank(nameField)) {
                    fieldsList.add(objType.getDescribe().fields.getMap().get(nameField));
                }

                objs = SQX_DynamicQuery.evaluate(1, 100, objectId.getSObjectType().getDescribe(), fieldsList, filter, new List<Schema.SObjectField>(), false);

            } else {
                objs = (List<SObject>)SQX_DynamicQuery.getallFields(objectId.getSObjectType().getDescribe(),new Set<Schema.SObjectField>(),filter,new List<Schema.SObjectField>(),false);
            }

            if(objs.isEmpty()) {
                throw new SQX_ApplicationGenericException('No matching record found');
            }

            objectQueryCache.put(objectId, objs.get(0));
        }

        return objectQueryCache.get(objectId);
    }


    /**
     * Returns an object with matching Id
     * @param  objectId the object id to match object
     * @return          retursn the details of the sobject type
     */
    public static SObject getSObjectWithId(Id objectId) {
        return getSObjectWithId(objectId, false);
    }

    /**
    * Finds a matching SObject instead of using SOQL query and returns the matching object
    * @param  searchString  the string to search and return the results
    * @param  pageNumber    the page number of the result to return
    * @param  pageSize      the size of page
    * @param  sobjType      the type of object to return
    * @param  fields        the list of fields for the object to return
    * @param  filter        the filter to apply for the object
    * @param  sortBy        the fields to order by
    * @param  sortAscending <code>true</code> to sort in ascending order
    * @return               return the list of sobjects
    */
    public static List<SObject> findSObject(String searchString, Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, List<Schema.SObjectField> fields, Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending){
        final String QUERY_FORMAT = 'FIND \'' + String.escapeSingleQuotes(searchString) + '\' IN ALL FIELDS ' +
                                    'RETURNING {!ObjectName}({!FieldList} {!FilterString} {!OrderByClause} LIMIT :pageSize OFFSET :offset)';

        Integer offset = (pageNumber - 1) * pageSize;

        String query  = prepare(QUERY_FORMAT, pageNumber, pageSize, sobjType, fields, filter, sortBy, sortAscending);

        return Search.query(query)[0];
    }

    /**
     * Evaluates the dynamic query with the context of given record and then returns the matching data
     * @param  record        the record which will drive the context
     * @param  pageNumber    the page number of the result to return
     * @param  pageSize      the size of individual pages
     * @param  sobjType      the type of object to query
     * @param  fields        the list of fields for the object to query
     * @param  filter        the filter to apply when evaluating the query
     * @param  sortBy        the list of fields to use for sorting
     * @param  sortAscending <code>true</code> to sort in ascending order else false
     * @return               returns the list of objects that match the filter of the requested type
     */
    public static List<SObject> evaluateWithCurrentRecord(SObject record, Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, List<Schema.SObjectField> fields, Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending) {
        CurrentRecord = record;
        List<SObject> results = null;
        try{
            results = evaluate(pageNumber, pageSize, sobjType, fields, filter, sortBy, sortAscending);
        }
        finally {
            CurrentRecord = null;
        }

        return results;
    }

    /**
    * performs a SOQL evaluation for a given object type, listing the requested fields and outputs the result for the given pagenumber with given pagesize
    * @param pageNumber the result's page that is required
    * @param pageSize the size of individual page
    * @param sobjType the object that is to be queried
    * @param fields the list of field that are to be retrieved
    * @param filters the filter (WHERE) clause that is to be applied
    * @param sortBy the fields by which the result is to be sorted
    * @param sortAscending <code>true</code> returns the result sorted in ascending order
    *                      <code>false</code> returns in descending order.
    */
    public static List<SObject> evaluate(Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, List<Schema.SObjectField> fields, Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending) {
        Integer offset = (pageNumber - 1) * pageSize;
        String query  = prepare(QUERY_FORMAT, pageNumber, pageSize, sobjType, fields, filter, sortBy, sortAscending);

        return Database.query(query);
    }

    private static String prepare(String queryFormat, Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, List<Schema.SObjectField> fields, Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending) {
        System.debug(sobjType.getName() + ' ' + pageNumber + ' ' + pageSize);
        if( !(sobjType.isQueryable() && sobjType.isAccessible()) ){
            throw new SQX_ApplicationGenericException(sobjType.getName() + ' can not be queried.');
        }

        String fieldList, filterString = '', orderByClause  = '';

        Map<String,SObjectField> fMap = sobjType.fields.getMap();
        String objName = sobjType.getName();

        String dynamicQuery = queryFormat.replace('{!ObjectName}', objName);

        //field list to retrieve
        fieldList = getFieldList(fields);

        //order by clause
        for(integer index = 0; index < sortBy.size() ; index++){
            orderByClause = orderByClause + sortBy.get(index).getDescribe().getName() + ', ';
        }

        if(orderByClause.length() > 0){
            orderByClause = orderByClause.trim().removeEnd(',') +
            (sortAscending == true ? ' ASC ' : ' DESC ');

            orderByClause = ' ORDER BY ' + orderByClause;
        }

        filterString = filter != null  ? filter.getWhereClause(sobjType.fields.getMap()) : '';

        dynamicQuery = dynamicQuery.replace('{!FieldList}', fieldList.trim());
        dynamicQuery = dynamicQuery.replace('{!FilterString}', filterString);
        dynamicQuery = dynamicQuery.replace('{!OrderByClause}', orderByClause);
        System.debug('Formed Query ' + dynamicQuery);

        return dynamicQuery;
    }

    /**
     * Returns the list of fields that can be queried based on the profile
     * @param fields the list of fields to include in the query string
     * @param localize boolean indicating whether or not picklist values should be translated
     * @returns returns the list of fields in comma separated manner to be used in query string
     */
    public static String getFieldList(List<Schema.SObjectField> fields) {
        String fieldList = 'Id';

        for(integer index = 0; index < fields.size(); index++){
            DescribeFieldResult fieldResult = fields.get(index).getDescribe();
            if(fieldResult.getName() == 'Id' || !fieldResult.isAccessible()){
                continue;
            }
            DisplayType fieldType = fieldResult.getType();
            String fieldName = fieldResult.getName();
            Boolean isCustomField = fieldResult.isCustom();
            fieldList += ', ' + fieldName;

            if(fieldType == DisplayType.PickList) {
                String picklistLabel = '';
                if(isCustomField){
                    picklistLabel = fieldName.replace('__c', '') + '__label';
                } else {
                    picklistLabel = fieldName + '__slabel';
                }
                fieldList += ', toLabel(' + fieldName + ') ' +  picklistLabel;
            }
            else if(fieldType == DisplayType.Reference) {
                SObjectType reference = fieldResult.getReferenceTo()[0];
                if(fieldResult.getRelationshipName() != null) {
                    String nameField = getNameField(reference);
                    if(!String.isBlank(nameField)) {
                        fieldList += ', ' + fieldResult.getRelationshipName() + '.' + nameField;
                    }
                }
            }
        }
        return fieldList;
    }
    
    /**
     * returns list of fields that can be used with dynamic query to read records
     * @param fldSet list of fields of a fieldset to include in the query string
     * @param fieldsToAdd additional list of fields that are not included in the provided fieldset
     * @returns returns the list of fields in comma separated manner to be used in query string
     */
    public static String getFieldList(FieldSet fldSet, List<SObjectField> fieldsToAdd) {
        Schema.DescribeSObjectResult sobj = fldSet.getSObjectType().getDescribe();
        Map<String, SObjectField> allFields = sobj.fields.getMap();
        Map<String, SObjectField> fieldsToFetch = new Map<String, SObjectField>();
        // add given fields
        if (fieldsToAdd != null) {
            for (SObjectField fld : fieldsToAdd) {
                String fldName = fld.getDescribe().getName().toLowerCase();
                fieldsToFetch.put(fldName, fld);
            }
        }
        // add fields from fieldset
        for (FieldSetMember field : fldSet.getFields()){
            String fieldPath = field.getFieldPath();
            SObjectField validField = allFields.get(fieldPath);
            
            // if the fieldset contais a period, it means it is cross reference field which is not currently supported
            if (fieldPath.contains('.')) {
                throw new SQX_ApplicationGenericException('No cross reference fields are supported');
            }
            
            // if a valid field object is not found throw an exception, this should never occur in a fieldset
            if (validField == null) {
                throw new SQX_ApplicationGenericException('The field ' + fieldPath + ' is not present in the object');
            }
            
            fieldsToFetch.put(fieldPath.toLowercase(), validField);
        }
        
        return getFieldList(fieldsToFetch.values());
    }
    
    private static String getNameField(SObjectType sobjType) {
        Map<String, SObjectField> fields = sobjType.getDescribe().fields.getMap();

        if(!fields.containsKey('Name') || !fields.get('Name').getDescribe().isNameField()) {
            for(String field : fields.keySet()) {
                if(fields.get(field).getDescribe().isNameField()) {
                    return field;
                }
            }
        }
        else{
            return 'Name';
        }
        
        return null;
    }

    /**
    * Evaluates a dynamic query on the object type based on the filter. This overload supports fieldset to be used to load
    * the fields.
    * @param pageNumber the result's page group that is to be returned starts from page 1
    * @param pageSize the maximum number of records to be returned
    * @param sobjType the object type that is to be queried
    * @param fields the fieldset that defines the fields that are to be fetched
    * @param filter the filter to be used to applied when querying
    * @param sortBy the sort field to be used
    * @param sortAscending <code>true</code> if the result is to be sorted in ascending order, <code>false</code> if in descending order
    */
    public static List<SObject> evaluate(Integer pageNumber, Integer pageSize,
        Schema.DescribeSObjectResult sobjType, List<Schema.FieldSetMember> fields,
        Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending){

        List<Schema.SObjectField> fieldsToFetch = new List<Schema.SObjectField>();

        //get the list of fields to be fetched from the fieldset, which will then passed to existing evaluate method
        Map<String, Schema.SObjectField> allFields = sobjType.fields.getMap();
        for(Schema.FieldSetMember field : fields){
            String fieldPath = field.getFieldPath();

            //if the fieldset contais a period, it means it is cross reference field which is not currently supported
            if(fieldPath.contains('.'))
                throw new SQX_ApplicationGenericException('No cross reference fields are supported');

            Schema.SObjectField validField = allFields.get(fieldPath);

            //if a valid field object is not found throw an exception, this should never occur in a fieldset
            if(validField == null)
                throw new SQX_ApplicationGenericException('The field ' + fieldPath + ' is not present in the object');

            fieldsToFetch.add(validField);
        }

        //pass the param to evaluate method and return the result
        return evaluate(pageNumber, pageSize, sobjType, fieldsToFetch, filter, sortBy, sortAscending);
    }

    /**
     *  Returns a list of all valid sobjectfields of the given sobject
     *  valid implies any field that is not in the blacklist in addition to Id field
     *  @param sobjType
     *  @param blacklist list of field to ignore
     */
    private static List<SObjectField> getAllSObjectFieldsFor(Schema.DescribeSObjectResult sobjType, Set<Schema.SObjectField> blacklist) {

        Map<String, Schema.SObjectField> fieldMap = sobjType.fields.getMap();
        List<Schema.SObjectField> fieldList = fieldMap.values();
        blacklist.add(fieldMap.get('Id'));//remove Id field as this field is added by SQX_DynamicQuery evaluate() function itself
        blacklist.add(fieldMap.get('LastViewedDate')); //removed LastViewedDate Which is visible in managed package
        blacklist.add(fieldMap.get('LastReferencedDate'));//removed LastReferencedDate Which is visible in managed package
        List<Schema.SObjectField> selectFields = new List<SObjectField>();

        if(test.isRunningTest() && sobjType.getSObjectType() == User.SObjectType) {
            // second condition is a hack to ensure that tests don't fail because of invalid fields like WirelessEmail.
            // need to evaluate if the hack is still required after next api version upgrade
            selectFields = new List<SObjectField> { User.Id, User.ContactId, User.UserType, User.Name, User.Username, User.Email, User.ProfileId, User.UserRoleId, User.LanguageLocaleKey, User.LocaleSidKey, User.TimeZoneSidKey, User.UserPreferencesLightningExperiencePreferred };

        } else if (fieldList != null){
            for (Schema.SObjectField ft : fieldList){ // loop through all field tokens (ft)
                if (!blacklist.contains(ft)){
                    // field is not in blacklist
                    selectFields.add(ft);
                }
            }
        }

        return selectFields;
    }

    /*
    * This method return list of records with all fields except specified in blacklist
    * @sobjType Sobject type on which query is to be performed
    * @blackList fields to be excluded for query
    * @filter the filter (WHERE) clause that is to be applied
    * @sortBy the fields by which the result is to be sorted
    * @sortAscending <code>true</code> returns the result sorted in ascending order
    *                      <code>false</code> returns in descending order.
    * @return returns list of sobjects with all fields except in blacklist
    */
    public static List<SObject> getallFields(Schema.DescribeSObjectResult sobjType, Set<Schema.SObjectField> blacklist, SQX_DynamicQuery.Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending){
        return getallFields(1, 100, sobjType, blacklist, filter, sortBy, sortAscending);
    }

    /*
    * This method return list of records with all fields except specified in blacklist
    * @pageNumber the page/offset that is to be retrieved
    * @pageSize the number of records to retreive
    * @sobjType Sobject type on which query is to be performed
    * @blackList fields to be excluded for query
    * @filter the filter (WHERE) clause that is to be applied
    * @sortBy the fields by which the result is to be sorted
    * @sortAscending <code>true</code> returns the result sorted in ascending order
    *                      <code>false</code> returns in descending order.
    * @return returns list of sobjects with all fields except in blacklist
    */
    public static List<SObject> getallFields(Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, Set<Schema.SObjectField> blacklist, SQX_DynamicQuery.Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending){
        return SQX_DynamicQuery.evaluate(pageNumber, pageSize, sobjType, getAllSObjectFieldsFor(sobjType, blacklist), filter, sortBy, sortAscending);
    }


    /**
    * Prepares a query with all the sobjectfields for the given sobject
    * @param pageNumber the result's page that is required
    * @param pageSize the size of individual page
    * @param sobjType the object that is to be queried
    * @param blacklist the list of field to be avoided
    * @param filter the filter (WHERE) clause that is to be applied
    * @param sortBy the fields by which the result is to be sorted
    * @param sortAscending <code>true</code> returns the result sorted in ascending order
    *                      <code>false</code> returns in descending order.
    */
    public static String prepareAllFieldsQuery(Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, Set<Schema.SObjectField> blacklist, SQX_DynamicQuery.Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending) {
        return prepare(QUERY_FORMAT, pageNumber, pageSize, sobjType, getAllSObjectFieldsFor(sobjType, blacklist), filter, sortBy, sortAscending);
    }

    /**
    * Prepares a query with all the sobjectfields for the given sobject
    * @param pageNumber the result's page that is required
    * @param pageSize the size of individual page
    * @param sobjType the object that is to be queried
    * @param fieldList the list of field to be avoided
    * @param filter the filter (WHERE) clause that is to be applied
    * @param sortBy the fields by which the result is to be sorted
    * @param sortAscending <code>true</code> returns the result sorted in ascending order
    *                      <code>false</code> returns in descending order.
    */
    public static String prepareQuery(Integer pageNumber, Integer pageSize, Schema.DescribeSObjectResult sobjType, List<Schema.SObjectField> fieldsList, SQX_DynamicQuery.Filter filter, List<Schema.SObjectField> sortBy, boolean sortAscending) {
        return prepare(QUERY_FORMAT, pageNumber, pageSize, sobjType, fieldsList, filter, sortBy, sortAscending);
    }




    /**
    * Represents the hierarchical filter that can be used to construct a query filter, either simple or a complex filter.
    */
    public with sharing class Filter{
        public List<Filter> filters {get; set;}
        public String field { get; set; }
        public String operator { get; set; }
        public Object value {get; set;}
        public boolean ignoreCase {get; set;}
        public String logic {get; set;}
        public String usv_function {get; set;}


        public String s_value {get; set { this.value = value; }}
        public Boolean b_value {get; set { this.value = value; }}
        public Double d_value {get; set { this.value = value; }}
        public Integer i_value {get; set { this.value = value; }}
        public Date dt_value {get; set { this.value = value; }}
        public DateTime dtt_value {get; set { this.value = value; }}
        public String usv_param {get; set;}

        public Filter(string f,string o, object v){
            field = f;operator = o; value=v;
            filters = new List<Filter>();
        }

        public Filter(){
            filters = new List<Filter>();
        }



        /**
        * method to convert the filter to a where string without adding field relation prefix and excluding any fields
        */
        public String toString(Map<String, Schema.SObjectField> allFields){
            return toString(allFields, null, null);
        }

        /**
        * actual method to convert the filter to a where string
        * @param allFields       fields of the filtering object
        * @param fieldNamePrefix prefix string needed to be added before field names while converting filters
        * @param excludeFields   fields to exclude while converting where string
        * @param return          returns converted where string
        */
        public String toString(Map<String, Schema.SObjectField> allFields, String fieldNamePrefix, Set<String> excludeFields){
            //this method checked for usv functions
            if(this.usv_function == FUNCTION_REFERENCE || this.usv_function == FUNCTION_REFERENCE_CURRENT_RECORD) {
                DescribeFieldResult fieldDesc = allFields.get(this.field).getDescribe();
                SObjectType objType = fieldDesc.getReferenceTo()[0];
                Map<String, SObjectField> refFields = objType.getDescribe().fields.getMap();
                SQX_DynamicQuery.Filter relFilter = new SQX_DynamicQuery.Filter();
                if(this.usv_function == FUNCTION_REFERENCE_CURRENT_RECORD) {
                    relFilter.usv_function = FUNCTION_CURRENT_RECORD;
                    relFilter.usv_param = (String)this.value;
                }
                else {
                    relFilter.value = this.value;
                }
                relFilter.field = this.usv_param;
                relFilter.operator = this.operator;
                return relFilter.toString(refFields, fieldDesc.getRelationshipName(), new Set<String>());
            }

            evaluateUserFunction();
            if (String.isBlank(fieldNamePrefix)) {
                // set empty string for null or blank value
                fieldNamePrefix = '';
            }
            else if (!fieldNamePrefix.endsWith('.')){
                // append '.'
                fieldNamePrefix += '.';
            }

            if (excludeFields == null) {
                // set empty list if null
                excludeFields = new Set<String>();
            }

            String convertedString = '';

            if(filters != null && filters.size() > 0){
                //for each filter deserialized
                for(integer index = 0; index < filters.size(); index++){
                    //deserialzing single filter from the array
                    String convertedFilterString = filters.get(index).toString(allFields, fieldNamePrefix, excludeFields);
                    // converted filtered string if not empty with required logic operator
                    if (!String.isBlank(convertedFilterString)) {
                        if (!String.isBlank(convertedString)) {
                            convertedString += (' ' + ( logic == null || logic == FILTER_LOGIC_AND ? FILTER_LOGIC_AND :  FILTER_LOGIC_OR )  + ' ');
                        }
                        convertedString += convertedFilterString;
                    }
                }

                if (!String.isBlank(convertedString)) {
                    convertedString = ' (' + convertedString + ') ';
                }
            }
            else if(field != null && !excludeFields.contains(field)){

                System.assert(allFields.containsKey(field), field + ' is not present in the object');

                DescribeFieldResult selectedField = allFields.get(field).getDescribe();
                System.assert(selectedField.isFilterable(), selectedField.getName() + ' is not filterable' );

                String fieldName = fieldNamePrefix + selectedField.getName();

                String stringOperator = '';
                if(operator == FILTER_OPERATOR_EQUALS)
                    stringOperator = ' = ';
                else if(operator == FILTER_OPERATOR_NOT_EQUALS)
                    stringOperator = ' != ';
                else if(operator == FILTER_OPERATOR_LESS_THAN)
                    stringOperator = ' < ';
                else if(operator == FILTER_OPERATOR_LESS_THAN_EQUALS)
                    stringOperator = ' <= ';
                else if(operator == FILTER_OPERATOR_GREATER_THAN)
                    stringOperator = ' > ';
                else if(operator == FILTER_OPERATOR_GREATER_THAN_EQUALS)
                    stringOperator = ' >= ';
                else if(operator == FILTER_OPERATOR_CONTAINS || operator == FILTER_OPERATOR_ENDSWITH || operator == FILTER_OPERATOR_STARTSWITH)
                    stringOperator = ' LIKE ';
                else if(operator == FILTER_OPERATOR_IN)
                    stringOperator = ' in: ';   // only support bind expression for now
                else
                    throw new SQX_ApplicationGenericException('Unsupported operator ' + operator);

                if(operator == FILTER_OPERATOR_IN) {
                    convertedString = fieldName + stringOperator + value;
                }
                else if(selectedField.getType() == DisplayType.Double || selectedField.getType() == DisplayType.Integer || selectedField.getType() == DisplayType.Percent){

                    System.assert(stringOperator != ' LIKE ', fieldName + '(Number) does not support ' + operator);
                    convertedString = fieldName + stringOperator + String.escapeSingleQuotes((String) value);
                }
                else if(selectedField.getType() == DisplayType.Date || selectedField.getType() == DisplayType.DateTime){
                    if(value == null) {
                        if(operator == FILTER_OPERATOR_GREATER_THAN || operator == FILTER_OPERATOR_GREATER_THAN_EQUALS) {
                            value = selectedField.getType() == DisplayType.Date ? '0000-01-01' : '0001-01-01T00:00:00Z';
                        } else if(operator == FILTER_OPERATOR_LESS_THAN || operator == FILTER_OPERATOR_LESS_THAN_EQUALS) {
                            value = selectedField.getType() == DisplayType.Date ? '9999-12-31' : '9999-12-31T00:00:00Z';
                        }
                    } else if(value instanceof Date || value instanceof DateTime) {
                        if(selectedField.getType() == DisplayType.Date) {
                            Date dt = (Date)value;
                            value = DateTime.newInstance(dt.year(), dt.month(), dt.day()).format('yyyy-MM-dd');
                        } else {
                            value = DateTime.valueOf(value).format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                        }
                    }
                    convertedString =  fieldName + stringOperator + value ;
                }
                else if(selectedField.getType() == DisplayType.Boolean){
                    System.assert(operator == FILTER_OPERATOR_EQUALS || operator == FILTER_OPERATOR_NOT_EQUALS,
                        'Unsupported operator (' + operator + ') for field ' + fieldName);

                    convertedString = fieldName + stringOperator + Boolean.valueOf(value);
                }
                else if(selectedField.getType() == DisplayType.Id){
                    System.assert(operator == FILTER_OPERATOR_EQUALS || operator == FILTER_OPERATOR_NOT_EQUALS,
                        'Unsupported operator (' + operator + ') for field ' + fieldName);
                    String idValue = value == null ? '' : Id.valueOf((String)value);
                    convertedString = fieldName + stringOperator + '\'' + idValue  + '\'';
                }
                else{
                    System.assert(stringOperator == ' LIKE ' || stringOperator == ' != ' || stringOperator == ' = ',
                        'Unsupported operator ('+ operator  +') for ' + fieldName );
                    String stringValue =  value == null ? 'null' : String.escapeSingleQuotes((String) value);

                    stringValue = ((operator == FILTER_OPERATOR_ENDSWITH ||operator == FILTER_OPERATOR_CONTAINS ) ? '%': '')
                                 + stringValue
                                 + ((operator == FILTER_OPERATOR_STARTSWITH ||operator == FILTER_OPERATOR_CONTAINS ) ? '%': '');

                    convertedString = fieldName + stringOperator + '\'' + stringValue + '\'';
                }
            }

            return convertedString;

        }


        /**
        * returns the where clause for the filter expression.
        * @param allFields the list of all fields of the object for which this filter is intended.
        */
        public String getWhereClause(Map<String, Schema.SObjectField> allFields){

            String  whereClause = toString(allFields),
                    filterString;
            filterString = String.isEmpty(whereClause) ? '' : ' WHERE '  + whereClause;

            return filterString;
        }


        /**
        * returns the where clause for the filter expression.
        * @param allFields the list of all fields of the object for which this filter is intended.
        * @param fieldNamePrefix prefix to be appended to fiels name
        */
        public String getWhereClause(Map<String, Schema.SObjectField> allFields, String fieldNamePrefix){
            String  whereClause = toString(allFields, fieldNamePrefix, null),
                    filterString;
            filterString = String.isEmpty(whereClause) ? '' : ' WHERE '  + whereClause;

            return filterString;
        }

        /**
        * set user function dynamic value
        */
        public void evaluateUserFunction(){
            if(this.usv_function == FUNCTION_USER_INFO){
                User userInfo=SQX_Utilities.getCurrentUser();
                this.value =userInfo.get(this.usv_param);
            }
            else if(this.usv_function == FUNCTION_PERSONNEL_INFO){
                SQX_Personnel__c personnel = SQX_Utilities.getPersonnelInfo();
                if(personnel != null){
                    this.value = personnel.get(this.usv_param);
                }
                else{
                    this.value = '';
                }
            }
            else if(this.usv_function == FUNCTION_ADD_DAYS){
                Integer val = Integer.valueOf(this.usv_param);
                this.value = Date.today().addDays(val);
            }
            else if(this.usv_function == FUNCTION_CURRENT_RECORD) {
                System.assert(CurrentRecord != null, 'Please set current record before using it in filter');
                this.value = CurrentRecord.get(this.usv_param);
            }
        }
    }

    public static final String FILTER_OPERATOR_CONTAINS = 'contains';
    public static final String FILTER_OPERATOR_STARTSWITH = 'startswith';
    public static final String FILTER_OPERATOR_ENDSWITH = 'endswith';
    public static final String FILTER_OPERATOR_EQUALS = 'eq';
    public static final String FILTER_OPERATOR_NOT_EQUALS = 'neq';
    public static final String FILTER_OPERATOR_LESS_THAN = 'lt';
    public static final String FILTER_OPERATOR_LESS_THAN_EQUALS = 'lte';
    public static final String FILTER_OPERATOR_GREATER_THAN = 'gt';
    public static final String FILTER_OPERATOR_GREATER_THAN_EQUALS = 'gte';
    public static final String FILTER_OPERATOR_IN = 'in';
    public static final String FUNCTION_USER_INFO = 'getUserInfo';
    public static final String FUNCTION_PERSONNEL_INFO = 'getPersonnelInfo';
    public static final String FUNCTION_CURRENT_RECORD = 'currentRecord';
    public static final String FUNCTION_REFERENCE = 'reference';
    public static final String FUNCTION_REFERENCE_CURRENT_RECORD = 'currentRecordReference';
    public static final String FUNCTION_ADD_DAYS = 'addDays';

    //"eq" (equal to), "neq" (not equal to), "lt" (less than), "lte" (less than or equal to), "gt" (greater than), "gte" (greater than or equal to), "startswith", "endswith", "contains"

    public static final String FILTER_LOGIC_AND = 'AND';
    public static final String FILTER_LOGIC_OR = 'OR';

    private static SObject CurrentRecord;

}