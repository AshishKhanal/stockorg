/**
 * factory class to create required records for NSI
 * NOTE : DO NOT ADD TEST CASE IN THIS CLASS
 */
@isTest
public class SQX_Test_NSI {
    public SQX_New_Supplier_Introduction__c nsi;

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
    }
    
    /**
     * constructor method to create NSI with required fields
     */
    public SQX_Test_NSI(){
        nsi = new SQX_New_Supplier_Introduction__c(Name = 'AUTO', 
                                                   Company_Name__c = 'TEST_ACCOUNT', 
                                                   Supplier_Number__c = 'SUP_0001', 
                                                   First_Name__c = 'Test', 
                                                   Last_Name__c = 'User', 
                                                   Phone__c = '1234567890',
                                                   Part_Name__c = 'PART 1',
                                                   Result__c  = 'Approved');
    }
    
    /**
     * method to set status of NSI
     */
    public SQX_Test_NSI setStatus(String status){
        nsi.Status__c = status;
        return this;
    }
    
    
    /**
     * method to set stage of NSI
     */
    public SQX_Test_NSI setStage(String stage){
        nsi.Record_Stage__c = stage;
        return this;
    }
    
    /**
     * method to save/update NSI
     */
    public SQX_Test_NSI save(){
        if(nsi.Id == null){
            new SQX_DB().op_insert(new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                   new List<SObjectField> {SQX_New_Supplier_Introduction__c.Name, 
                                                            SQX_New_Supplier_Introduction__c.First_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Last_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Phone__c,
                                                            SQX_New_Supplier_Introduction__c.Company_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Supplier_Number__c,
                                                            SQX_New_Supplier_Introduction__c.Result__c});
        } else {
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                   new List<SObjectField> {SQX_New_Supplier_Introduction__c.Name, 
                                                            SQX_New_Supplier_Introduction__c.First_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Last_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Phone__c,
                                                            SQX_New_Supplier_Introduction__c.Company_Name__c,
                                                            SQX_New_Supplier_Introduction__c.Supplier_Number__c,
                                                            SQX_New_Supplier_Introduction__c.Result__c});
        }
        
        return this;
    }


    /**
     * method to submit NSI
     */
    public void submit() {
        this.nsi.Activity_Code__c = 'submit';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }


    /**
     * method to initiate NSI
     */
    public void initiate() {
        this.nsi.Activity_Code__c = 'initiate';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to restart NSI
     */
    public void restart() {
        this.nsi.Activity_Code__c = 'restart';
        this.nsi.Current_Step__c = 1;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to close NSI
     */
    public void close() {
        this.nsi.Activity_Code__c = 'close';
        this.nsi.Is_Locked__c = true;

        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to close NSI
     */
    public void void() {
        this.nsi.Activity_Code__c = 'void';
        this.nsi.Is_Locked__c = true;

        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to clear existing field values
     */
    public void clear() {
        this.nsi = [SELECT Id, Activity_Code__c FROM SQX_New_Supplier_Introduction__c WHERE Id=: this.nsi.Id];
    }

    
    /**
     * method to redo onboarding step
     */
    public void redoOnboardingStep(SQX_Onboarding_Step__c onboardingStep){
        onboardingStep = [SELECT Id, 
                                 Step__c, 
                                 SQX_Parent__c, 
                                 Name, 
                                 SQX_User__c,
                                 Description__c,
                                 Due_Date__c,
                                 Allowed_Days__c,
                                 RecordTypeId,  
                                 SQX_Controlled_Document__c 
                            FROM SQX_Onboarding_Step__c 
                            WHERE Id =: onboardingStep.Id];

        SQX_Supplier_Redo_Step.RedoStepRequest req = new SQX_Supplier_Redo_Step.RedoStepRequest();
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_PRE;
        req.recordId = onboardingStep.Id;
        req.step = Integer.valueOf(onboardingStep.Step__c);
        req.assignee = onboardingStep.SQX_User__c;
        req.controlledDocument = onboardingStep.SQX_Controlled_Document__c;
        req.description = onboardingStep.Name;
        req.dueDate=onboardingStep.Due_Date__c;
        req.allowedDays=(Integer)onboardingStep.compliancequest__Allowed_Days__c;
        SQX_Supplier_Redo_Step.redoStep(req);
        
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_POST;
        SQX_Supplier_Redo_Step.redoStep(req);
    }
    
    /**
    * method to add user to queue
    */
    public static void addUserToQueue(List<User> usersToAssign){
        SQX_Test_Utilities.addUserToQueue(usersToAssign, 'CQ_Supplier_Introduction_Queue', SQX.NSI);
    }

    /*
        This number holds the number of NSIs created which shall make the policy task name unique
    */
    static Integer nsiNumber = 0;


    /**
     * method to create policy task of different task types
     */
    public static List<SQX_Task__c> createPolicyTasks(Integer numberOfTasks, String taskType, User assignee, Decimal step){
        List<SQX_Task__c> taskList = new List<SQX_Task__c>();
        for(Integer i = 0; i < numberOfTasks; i++){
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = assignee.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = 'Supplier Introduction', 
                                               Step__c = step,
                                               Active__c = true,
                                               Name = 'NSI_' + nsiNumber++ + '_Test_Policy_Task_' + i + 'Step ' + step,
                                               Task_Type__c = taskType);

            if(taskType == SQX_Task.TASK_TYPE_DOC_REQUEST){
                task.Document_Name__c = 'Sample Document Name';
            }
            taskList.add(task);
        }
        
        insert taskList;
        
        return taskList;
    }
}