/**
* This is class that encapsulates the logic used to integrate SF API calls. Since, CQ's NC has a complex object structure,
* SQX_NC_Staging acts as a denormalized/simpler form of it. NC Staging allows customers to insert NC using this object
* without having to understand the whole CQ structure.
*/
public without sharing class SQX_NC_Staging {

    /**
    * The list of the NC Staging objects that are to be converted into NC object or update the existing NC's data
    */
    List<sObject> scope;

    /**
    * Extra parameters that is sent to the NC Staging class, which will be used when trying to map from NC Staging to NC.
    * The currently supported parameters include the following
    * a) AddDepartmentIfNotFound => <code>true</code> tells the mapper to create a new department if it doesn't exist <code>false</code>
    * b) AddPartIfNotFound => tells the mapper to create a new part if an existing one is not found
    * c) DefaultQueueId => Sets the queue id of the newly created department
    */
    Map<string,object> params;
    public static final string  ADD_DEPT = 'AddDepartmentIfNotFound',
                                ADD_PART = 'AddPartIfNotFound',
                                DEFAULT_QUEUE_ID = 'DefaultQueueId',
                                TARGET_TYPE_ISSUE = 'Issue',
                                TARGET_TYPE_NC = 'NonConformance';


    /**
    * These are maps that determine how a field from NC Staging should be copied to NC or its child objects
    * customFieldMap provides a way to add new mappings in addition to what is specified in the field map.
    */
    Map<string,Map<string,SQX_FieldValue>> fieldMap,customFieldMap;
    
    /**
    * ncMap, departMap and partMap store Salesforce SObjects after querying during preprocess
    * ncMap => has all NonConformance's whose Reference number mentioned in NC Staging
    * departMap => has Department stored using the Department Name. Map of Department Name to Department
    * partMap => has Parts mapped using Part Number in NC Staging.
    * partLotList => has all impacted part/lots
    */
    Map<string,SQX_Nonconformance__c> ncMap = new Map<string,SQX_Nonconformance__c>();
    Map<string,SQX_Finding__c> findingMap = new Map<string,SQX_Finding__c>();
    Map<string,SQX_Department__c> departMap = new Map<string,SQX_Department__c>();
    Map<string,SQX_Part__c> partMap = new Map<string,SQX_Part__c>();
    Map<string,SQX_Standard_Service__c> serviceMap = new Map<string,SQX_Standard_Service__c>();
    Map<string,SQX_NC_Impacted_Product__c> partLotList = new Map<string,SQX_NC_Impacted_Product__c>();
    Map<string,Account> AcctMap= new Map<string,Account>();
    Map<string,Contact> contactMap  = new Map<string,Contact>();
    Map<string,SQX_Business_Unit__c> buMAP = new Map<string,SQX_Business_Unit__c>();
    Map<string,SQX_Division__c> divMap = new Map<string,SQX_Division__c>();
    Map<string,SQX_Site__c> siteMap = new Map<string,SQX_Site__c>();
    Map<string,SQX_Defect_Code__c> defectMap = new Map<string,SQX_Defect_Code__c>();

    /**
    * Default constructor
    * @param scopeToUse the list of NC Staging Objects that are to be transformed into NC object or its child object
    * @param fieldMapToUse additional map that can be used to configure custom fields in NC Staging to a particular field in NC object
    * @param paramsToUse the params help the execution by providing control settings or default values need to perform the translation
    */
    public SQX_NC_Staging(List<sObject> scopeToUse,Map<string,Map<string,SQX_FieldValue>> fieldMapToUse,Map<string,object> paramsToUse){
        scope = scopeToUse;
        params = (paramsToUse==null?new Map<string,object> ():paramsToUse);
        customFieldMap = fieldMapToUse;
    }


    /**
    * Preprocess loads the related NCs, departments and parts before proceeding into actual work. Currently it performs the following
    * functions
    * a) Loads all the NCs whose Reference Number matches the NC Staging's Reference Number
    * b) Loads all the Departments whose department name matches the NC Staging's Department Name
    * c) Loads all the Parts whose part number matches the NC Staging's Part Number
    * d) Creates departments and parts if the passed params have mentioned it.
    */
    private void preProcess(){
        SQX_NC_Staging__c ncStage;
        List<string> extReferences = new list<string>();
        List<string> partNumbers = new list<string>();
        List<string> serviceNames = new list<string>();
        List<string> deptNumbers = new list<string>();
        List<string> accounts = new list<string>();
        List<string> contacts = new List<string>();
        List<string> businessUnits = new List<string>();
        List<string> divisions = new List<string>();
        List<string> sites = new List<string>();
        List<string> defects = new List<string>();
        for ( integer i=0;i<scope.size();i++){
            ncStage = (SQX_NC_Staging__c) scope.get(i); 
            extReferences.add(ncStage.Reference_Number__c);
            if (ncStage.Part_Number__c != null) partNumbers.add(ncStage.Part_Number__c);
            if (ncStage.Process__c != null) serviceNames.add(ncStage.Process__c);
            if (ncStage.Department_Name__c != null) deptNumbers.add(ncStage.Department_Name__c);
            if (ncStage.Account_Name__c != null) accounts.Add(ncStage.Account_Name__c);
            if (ncStage.Contact_Email__c != null) contacts.Add(ncStage.Contact_Email__c);
            if (ncStage.Division__c != null) divisions.add(ncStage.Division__c);
            if (ncStage.Business_Unit__c != null) businessUnits.Add(ncStage.Business_Unit__c);
            if (ncStage.Site__c != null) sites.add(ncStage.Site__c);
            if (ncStage.Defect_Failure__c != null) defects.add(ncStage.Defect_Failure__c);
        }

        for (SQX_Nonconformance__c tmpNC : [select QMS_Reference_Number__c,id from SQX_Nonconformance__c where QMS_Reference_Number__c in :extReferences]){
            ncMap.put(tmpNC.QMS_Reference_Number__c,tmpNC);
        }

        for (SQX_Finding__c tmpFinding : [select QMS_Reference_Number__c,id from SQX_Finding__c where QMS_Reference_Number__c in :extReferences]){
            findingMap.put(tmpFinding.QMS_Reference_Number__c, tmpFinding);
        }

        for (SQX_Part__c tmpPart : ([select id,name,part_number__c,part_family__c from SQX_Part__c where part_number__c in :partNumbers])){
            partMap.put(tmpPart.part_number__c,tmpPart);
        }
        for (SQX_Standard_Service__c tmpServ : ([select id,name from SQX_Standard_Service__c where name in :serviceNames])){
            serviceMap.put(tmpServ.Name,tmpServ);
        }
        for (SQX_Department__c tmpDepart : ([select id,name,Queue_Id__c from SQX_Department__c where name in :deptNumbers])){
            departMap.put(tmpDepart.name,tmpDepart);
        }
        
        for (SQX_NC_Impacted_Product__c impProd : ([select id, SQX_Nonconformance__c, Lot_Number__c, Part_Number__c from SQX_NC_Impacted_Product__c  where SQX_Nonconformance__c in :ncMap.values()])){
            partLotList.put(impProd.SQX_Nonconformance__c + impProd.Part_Number__c + impProd.Lot_Number__c,impProd);
        }
        for (Account tmpAcct : [select id,name from Account where name in :accounts]){
            AcctMap.put(tmpAcct.name,tmpAcct);
        }
        for (contact tmpContact : [select id,email,accountid from Contact where email in :contacts]){
            contactMap.put(tmpContact.email,tmpContact);
        }
        for (SQX_Site__c tmpSite: [select id,name,SQX_Business_Unit__c from SQX_Site__c where name in :sites]){
            siteMap.put(tmpSite.Name,tmpSite);            
        }
        for (SQX_Business_Unit__c tmpBU: [select id,name,SQX_Division__c from SQX_Business_Unit__c where name in :businessUnits]){
            buMAP.put(tmpBU.Name,tmpBU);            
        }
        for (SQX_Division__c tmpDiv: [select id,name from SQX_Division__c where name in :divisions]){
            divMap.put(tmpDiv.name,tmpDiv);
        }
        for (SQX_Defect_Code__c tmpDefect : ([select id,name,Defect_Category__c,Description__c from SQX_Defect_Code__c where name in :defects])){
            defectMap.put(tmpDefect.name,tmpDefect);
        }
        
        List<string> errors = new list<string>();
        boolean addDept = false,addPart = false;       
        object param;
        SQX_Department__c dept; 
        SQX_Defect_Code__c defect;
        SQX_Part__c part; SQX_Standard_Service__c service;
        Account acct; Contact cntc;SQX_Division__c div;SQX_Business_Unit__c bu;SQX_Site__c site;
        List<SQX_Department__c> deptsToAdd = new List<SQX_Department__c>();
        param = params.get(ADD_DEPT);
        addDept =  (param == null? false : (boolean) param);
        param = params.get(ADD_PART);
        addPart =  (param == null? false : (boolean) param);

        //create departments and parts if necessary
        for (integer i=0;i<scope.size();i++){
           ncStage = (SQX_NC_Staging__c) scope.get(i); 
           dept = null;
           part = null;
           if (ncStage.Department_Name__c != null){dept = departMap.get(ncStage.Department_Name__c);}
           if (dept == null && addDept == true && !String.isBlank(ncStage.Department_Name__c) ) {
               dept = new SQX_Department__c();
               dept.Name = ncStage.Department_Name__c;
               dept.Queue_Id__c = (string) params.get(DEFAULT_QUEUE_ID); //todo: how best to get this?
               deptsToAdd.add(dept);
           }
           if (ncStage.part_number__c != null){
               part = partMap.get(ncStage.Part_Number__c);
               if ((part == null) && addPart == false){
                 errors.add('Part number does not exist in CQ');   
               }else{
                  //todo: future: part add on the fly. Has complexity of assigning part family, part type etc.
               }
            }
            if (ncStage.Process__c != null){
                service = serviceMap.get(ncStage.Process__c);
                if (service == null) errors.add('Service/Process does not exist in CQ');
            }
           if (ncStage.Account_Name__c != null){
                acct = AcctMap.get(ncStage.Account_Name__c);
                if (acct == null) errors.add('Account Name does not exist in CQ');
                if (acct != null && ncStage.Contact_Email__c != null){
                    cntc = contactMap.get(ncStage.Contact_email__c);
                    if (cntc == null) {
                        errors.add('Contact does not exist in CQ');}
                    else{
                        if (cntc.AccountId != acct.Id) errors.add('Contact does not belong to account');
                    }
                }
            }
            if (ncStage.Contact_Email__c != null && ncStage.Account_Name__c == null){
                errors.add('Contact cannot be speficied without Account');
            }
           if (ncStage.Division__c != null){
                div = divMap.get(ncStage.Division__c);
                if (div == null) errors.add('Division does not exist in CQ');
            }
           if (ncStage.Business_Unit__c != null && div != null){
                bu = buMAP.get(ncStage.Business_Unit__c);
                if (bu == null) {
                    errors.add('Business Unit does not exist in CQ');
                }else{
                    if (bu.SQX_Division__c != div.id){errors.add ('Business Unit does belong to the division');}
                }
            }
            if (ncStage.Site__c != null && div != null && bu != null){
                site = siteMap.get(ncStage.Site__c);
                if (site == null) {
                    errors.add('Site does not exist in CQ');
                }else{
                    if (site.SQX_Business_Unit__c != bu.id){errors.add('Site does not belong to the business unit');}
                }
            }
           if (ncStage.Defect_Failure__c != null){
                defect = defectMap.get(ncStage.Defect_Failure__c);
                if (defect == null) errors.add('Defect Code does not exist in CQ, send Defect_Failure__c for non validated value');
            }


           if (errors.size() > 0){
               ncStage.Status__c = 'Error';
               ncStage.Error_Description__c = JSON.serialize(errors);
           }
           errors.clear();
        }
        if (deptsToAdd.size() > 0){
            Database.SaveResult[] deptsAdded = new SQX_DB().op_insert(deptsToAdd, new List<SObjectField>{
                                                    SQX_Department__c.fields.Name,
                                                    SQX_Department__c.fields.Queue_Id__c});
            list<id> newDepts = new List<id>();
            for (Database.SaveResult sr : deptsAdded){
                newDepts.add(sr.getId());
            }
            for (SQX_Department__c tmpDepart : ([select id,name,Queue_Id__c from SQX_Department__c where id in :newDepts])){
                departMap.put(tmpDepart.name,tmpDepart);
            }
        }
    }

    /**
    * This is the method that is called to start processing NC Staging into NC.
    */
    public void processNC(){
        preProcess();
        mapAllFields();
        updateWithCustomMap();
        insertNCAndOrAddChildren();
    }

    /**
    * This method inserts a new NC or adds children to an existing NC.
    * The following behaviour can be seen in the method
    * a) When NC Staging is inserted with Reference Number isn't present in the SF org.
    *      A new nc is inserted
    *      Part, defect and disposition are inserted based on the fields in the NC
    *
    * b) When NC Staging is inserted with Reference Number present in the SF Org.
    *      New Part, defect and disposition are inserted for the NC.
    *      [NOTE: No updates happen for any of the record.]
    *
    * Any processing error that occur are stored in the NC Staging record's error description and the record is marked as Erroneous.
    * i.e. it's status is set to Error.
    */
    private void insertNCAndOrAddChildren(){
        SQX_NC_Staging__c ncStage;
        SQX_Department__c dept; 
        SQX_Part__c part;
        SQX_Nonconformance__c nc,existingNC;
        SQX_NC_Impacted_Product__c partLot;
        SQX_NC_Defect__c defect;
        SQX_Disposition__c disposition;
        final String TEMP_NC_ID = SQX.NonConformance + '-1',
                     TEMP_PART_LOT_ID = 'PART_LOT-1';
        String ncId = TEMP_NC_ID, partLotId = null;

        for(Integer i=0; i<scope.size(); i++){
            ncStage = (SQX_NC_Staging__c) scope.get(i); 
            if (ncStage.Status__c != 'Error'){ 
                try{
                    part = partMap.get(ncStage.Part_Number__c);
                    dept = departMap.get(ncStage.Department_Name__c);

                    if(ncStage.Target_Object_Type__c == TARGET_TYPE_ISSUE) {
                        SQX_Finding__c finding = new SQX_Finding__c();
                        finding.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Finding, SQX_Finding.ISSUE_RECORD_TYPE_DEVELOPER_NAME);
                        if(!findingMap.containsKey(ncStage.Reference_Number__c)) {
                            //only updates
                            final String FindingId = 'Finding-1';
                            if(dept != null)
                                finding.SQX_Department__c = dept.Id;
                            Map<String, SQX_FieldValue> findingMap = fieldMap.get(SQX.Finding);
                            resolveIdsInFinding(ncStage, findingMap);
                            assignValues(ncStage, finding, findingMap);

                            SQX_Upserter.addObjectToPersist(finding, FindingId);
                            //processing one at a time to catch individual errors
                            map<string,string> nextAction = new map<string,string>();
                            nextAction.put('nextAction',params.get('nextAction')!= null?(string)params.get('nextAction'):'submit');

                            ncStage.SQX_Finding__c = SQX_Extension_Finding.processChangeSetWithAction(FindingId, '{"changeSet": []}', null, nextAction, null, true);//set ApiIntegration to true to bypass esignature validation
                            ncStage.Status__c = 'Processed';
                        }
                    }
                    else {
                        existingNC = ncMap.get(ncStage.Reference_Number__c);

                        if(existingNC == null) {
                            nc = new SQX_Nonconformance__c();
                            //missing dept scenario should not occur unless creation on the fly has failed
                            if (dept != null)
                                nc.SQX_Department__c = dept.id;
                            Map<string,SQX_FieldValue> ncMap = new Map<string,SQX_FieldValue>(fieldMap.get(SQX.Nonconformance));
                            resolveIdsInNC(ncStage, ncMap);//resolvedIDs are record specific so take standard map and add
                            assignValues(ncStage, nc, ncMap);
                            SQX_Upserter.addObjectToPersist(nc, ncId);
                        }else {
                            //NC cannot be updated so no field transfer will occur
                            //part/lot/defect etc for existing NC will be added
                            ncId = existingNC.id;
                        }

                        //insert part lot
                        if(ncStage.part_number__c != null && ncStage.lot_number__c !=null) {
                            if (partLotList.containsKey(ncId + ncStage.Part_Number__c + ncStage.Lot_Number__c)){
                                partLotId = partLotList.get(ncId + ncStage.Part_Number__c + ncStage.Lot_Number__c).id;
                            }
                            else {
                                partLot = new SQX_NC_Impacted_Product__c();
                                partLot.SQX_Impacted_Part__c = part.Id;
                                Map<string,SQX_FieldValue> defectMap = new Map<string,SQX_FieldValue>(fieldMap.get(SQX.PartLotQty));
                                assignValues(ncStage,partLot,defectMap);
                                SQX_Upserter.addObjectToPersist(partLot, TEMP_PART_LOT_ID).addReference(SQX_NC_Impacted_Product__c.SQX_Nonconformance__c, ncId, true);
                            }
                        }
                        //insert defect
                        if (ncStage.Defect_Failure__c != null && ncStage.part_number__c != null){
                            defect  = new SQX_NC_Defect__c();
                            defect.id = null;
                            updateDefectCategory(ncStage);
                            assignValues(ncStage,defect,fieldMap.get(SQX.Defect));
                            defect.SQX_Part__c = part.id;

                            SQX_Upserter.addObjectToPersist(defect, 'DEFECT-1').addReference(SQX_NC_Defect__c.SQX_Nonconformance__c, ncId, true);
                        }
                        //insert disposition only if part and lot are identified
                        if (ncStage.Disposition__c != null && ncStage.part_number__c != null && ncStage.lot_number__c !=null){
                            disposition = new SQX_Disposition__c();
                            disposition.id = null;
                            disposition.SQX_Part__c = part.Id;
                            assignValues(ncStage,disposition,fieldMap.get(SQX.Disposition));
                            if (disposition.Completed_On__c != null){
                                disposition.Status__c = SQX_Disposition.STATUS_COMPLETE;
                            }else{
                                disposition.Status__c = SQX_Disposition.STATUS_OPEN;
                            }

                            SQX_Upserter.addObjectToPersist(disposition, 'DISPOSITION-1')
                                        .addReference(SQX_Disposition__c.SQX_Nonconformance__c, ncId, true)
                                        .addReference(SQX_Disposition__c.SQX_NC_Impacted_Product__c, partLotId, false);
                        }

                        //processing one at a time to catch individual errors
                        map<string,string> nextAction = new map<string,string>();
                        if (ncId == TEMP_NC_ID) {
                            nextAction.put('nextAction',params.get('nextAction')!= null?(string)params.get('nextAction'):'submit');
                        } else{
                            nextAction.put('nextAction', 'save');
                        }
                        ncStage.SQX_NC__c = SQX_Extension_NC.processChangeSetWithAction(ncId, '{"changeSet": []}', null, nextAction, null, true);//set ApiIntegration to true to bypass esignature validation
                        ncStage.Status__c = 'Processed';
                    }
                }
                catch (DmlException ex) {
                    ncStage.Status__c = 'Error';
                    for(Integer countDMLs = 0; countDMLs < ex.getNumDml(); countDMLs++)
                        ncStage.Error_Description__c  = (ncStage.Error_Description__c == null ? '' : ncStage.Error_Description__c) + ex.getDmlMessage(countDMLs) + '\r\n';
                    ncStage.Error_Description__c = ncStage.Error_Description__c+ '\r\nDetailed Error Description:\r\n'+  ex.getStackTraceString();
                }
                catch (Exception ex) {
                    ncStage.Status__c = 'Error';
                    ncStage.Error_Description__c = ex.getMessage() +'\r\n' + ex.getStackTraceString();
                }
            }
       }

        new SQX_DB().op_update(scope, new List<Schema.SObjectField> {
                SQX_NC_Staging__c.Status__c,
                SQX_NC_Staging__c.Error_Description__c,
                SQX_NC_Staging__c.SQX_NC__c
            });
    }
    
    /**
    * copies a field value from source to destination object using the field mapping information
    */
    private void assignValues(SObject fromSource,SObject toObject, Map<string,SQX_FieldValue> mapToUse){
        SQX_FieldValue fieldValue;
        for (String fieldName : mapToUse.keySet()) {
            fieldValue = mapToUse.get(fieldName);
            toObject.put(fieldName,(fieldValue.FieldName != null ? fromSource.get(fieldValue.FieldName): fieldValue.Value));
        }
    }

    /**
     * Method to set Ids field in finding map based on the nc staging object
     */
    private void resolveIdsInFinding(SQX_NC_Staging__c ncStage, Map<String, SQX_FieldValue> mapToUse) {
        resolveIdsCommon(ncStage, mapToUse);

        if (ncStage.Account_Name__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Supplier_Account__c', new SQX_FieldValue((object)AcctMap.get(ncStage.Account_Name__c).id));
        }
    }
    
    /* creates map with id value for NC fields that accept and need to translate to ID
    */
    private void resolveIdsInNC(SQX_NC_Staging__c ncStage, Map<string,SQX_FieldValue> mapToUse){
        resolveIdsCommon(ncStage, mapToUse);

        //add account and external contact to NC only if NC type is supplier or customer
        if ((ncStage.NC_Type__c == SQX_NC.TYPE_SUPPLIER || ncStage.NC_Type__c == SQX_NC.TYPE_CUSTOMER) && ncStage.Account_Name__c != null){
            Account acct = AcctMap.get(ncStage.Account_Name__c);
            mapToUse.put(SQX.NSPrefix+'SQX_Account__c', new SQX_FieldValue((object)acct.id));
            if (acct != null && ncStage.Contact_Email__c != null){
                Contact cntc = contactMap.get(ncStage.Contact_email__c);
                mapToUse.put(SQX.NSPrefix+'SQX_External_Contact__c', new SQX_FieldValue((object)cntc.id));

            }
        }
    }

    /**
     * Method to set Ids field in given map based on the nc staging object for both nc and finding
     */
    private void resolveIdsCommon(SQX_NC_Staging__c ncStage, Map<String, SQX_FieldValue> mapToUse) {
        if (ncStage.Division__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Division__c', new SQX_FieldValue((object)divMap.get(ncStage.Division__c).id));
        }
        if (ncStage.Business_Unit__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Business_Unit__c', new SQX_FieldValue((object)buMap.get(ncStage.Business_Unit__c).id));
        }
        if (ncStage.Site__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Site__c', new SQX_FieldValue((object)siteMap.get(ncStage.Site__c).id));
        }

        if (ncStage.Process__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Service__c', new SQX_FieldValue((object)serviceMap.get(ncStage.Process__c).id));
        }
        if (ncStage.Part_Number__c != null){
            mapToUse.put(SQX.NSPrefix+'SQX_Part__c', new SQX_FieldValue((object)partMap.get(ncStage.Part_Number__c).id));
        }
    }
    
    //Replicate the UI behavior. If defect is from standard table then populate the category 
    private void updateDefectCategory(SQX_NC_Staging__c ncStage){
        if (defectMap.get(ncStage.Defect_Failure__c) != null){
            ncStage.Defect_Code__c = defectMap.get(ncStage.Defect_Failure__c).id;
            ncStage.Defect_Category__c = defectMap.get(ncStage.Defect_Failure__c).Defect_Category__c; 
        }
    } 
    
    /**
    * updates the field mapping with new fields sent in the custom map
    */
    private void updateWithCustomMap(){
        map<string,SQX_FieldValue> mapToUpdate;
        if (customFieldMap != null){
            for(String objectName : customFieldMap.keySet()){
                if(fieldMap.containsKey(objectName)){
                    mapToUpdate = fieldMap.get(objectName);
                    mapToUpdate.putAll(customFieldMap.get(objectName));
                }
            }
        }
    }
    //populated default mapping
    private void mapAllFields(){
        fieldMap = new Map<string,Map<string,SQX_FieldValue>>{
            SQX.NonConformance => mapNCFields(),
            SQX.Finding => mapFindingFields(),
            SQX.Defect => mapDefectFields(),
            SQX.PartLotQty => mapPartLotFields(),
            SQX.Disposition => mapDispositionFields()
        };
    }

    private Map<String, SQX_FieldValue> mapFindingFields() {
        map<string,SQX_FieldValue> findingMap = new Map<string,SQX_FieldValue>();
        
        findingMap.put(SQX.NSPrefix+'Title__c',new SQX_FieldValue('Title__c'));

        mapNCFindingFields(findingMap);
        return findingMap;
    }

    /**
    * maps nc staging's field to NC
    */
    private Map<string,SQX_FieldValue> mapNCFields(){
        map<string,SQX_FieldValue> ncMap = new Map<string,SQX_FieldValue>();
        ncMap.put(SQX.NSPrefix+'Type__c',new SQX_FieldValue('NC_Type__c'));
        ncMap.put(SQX.NSPrefix+'Type_Of_Issue__c', new SQX_FieldValue('Issue_Type__c'));
        ncMap.put(SQX.NSPrefix+'Disposition_Approval__c',  new SQX_FieldValue(false));
        ncMap.put(SQX.NSPrefix+'Priority__c',new SQX_FieldValue('Priority__c'));
        ncMap.put(SQX.NSPrefix+'Occurrence_Date__c' ,new SQX_FieldValue('Occurence_Date__c'));

        mapNCFindingFields(ncMap);
        return ncMap;
    }

    /**
     * maps nc staging's field to common fields of both nc and finding
     */
    private void mapNCFindingFields(Map<String, SQX_FieldValue> fMap) {

        fMap.put(SQX.NSPrefix+'Description__c',new SQX_FieldValue('Description__c'));
        fMap.put(SQX.NSPrefix+'Unit_of_Measure__c',new SQX_FieldValue('Unit_of_Measure__c'));
        fMap.put(SQX.NSPrefix+'QMS_Reference_Number__c',new SQX_FieldValue('Reference_Number__c'));

        fMap.put(SQX.NSPrefix+'Org_Business_Unit__c',new SQX_FieldValue('Org_Business_Unit__c'));
        fMap.put(SQX.NSPrefix+'Org_Division__c',new SQX_FieldValue('Org_Division__c'));
        fMap.put(SQX.NSPrefix+'Org_Region__c',new SQX_FieldValue('Org_Region__c'));
        fMap.put(SQX.NSPrefix+'Org_Site__c',new SQX_FieldValue('Org_Site__c'));

        fMap.put(SQX.NSPrefix+'Containment_Required__c', new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Investigation_Required__c',new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Corrective_Action_Required__c',new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Preventive_Action_Required__c',new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Investigation_Approval__c',new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Response_Required__c', new SQX_FieldValue(false));
        fMap.put(SQX.NSPrefix+'Done_Responding__c', new SQX_FieldValue(true));
    }

    /**
    * maps nc staging's field to part lot (Part lot)
    */
    private Map<string,SQX_FieldValue> mapPartLotFields(){
        map<string,SQX_FieldValue> partLotMap = new Map<string,SQX_FieldValue>(); 
        partLotMap.put(SQX.NSPrefix+'Lot_Number__c',new SQX_FieldValue('Lot_Number__c'));
        partLotMap.put(SQX.NSPrefix+'Lot_Quantity__c',new SQX_FieldValue('Lot_Quantity__c'));
        return partLotMap;
    }

    /**
    * maps to defect field
    */
    private Map<string,SQX_FieldValue> mapDefectFields(){
        map<string,SQX_FieldValue> defectMap = new Map<string,SQX_FieldValue>();
        defectMap.put(SQX.NSPrefix+'Lot_Ser_Number__c', new SQX_FieldValue('Lot_Number__c'));
        defectMap.put(SQX.NSPrefix+'SQX_Defect_Code__c', new SQX_FieldValue('Defect_Code__c'));
        defectMap.put(SQX.NSPrefix+'Defect_Code__c', new SQX_FieldValue('Defect_Failure__c'));
        defectMap.put(SQX.NSPrefix+'Defect_Category__c', new SQX_FieldValue('Defect_Category__c'));
        defectMap.put(SQX.NSPrefix+'Defect_Description__c' , new SQX_FieldValue('Defect_Description__c'));
        defectMap.put(SQX.NSPrefix+'Defective_Quantity__c', new SQX_FieldValue('Defective_Quantity__c'));
        defectMap.put(SQX.NSPrefix+'Number_of_Defects__c', new SQX_FieldValue('Number_of_Defects__c'));
        return defectMap;
    }

    /**
    * maps to disposition's field
    */
    private Map<string,SQX_FieldValue> mapDispositionFields(){
        map<string,SQX_FieldValue> dispositionMap = new Map<string,SQX_FieldValue>(); 
        dispositionMap.put(SQX.NSPrefix+'Lot_Number__c', new SQX_FieldValue('Lot_Number__c'));
        dispositionMap.put(SQX.NSPrefix+'Disposition_Quantity__c',new SQX_FieldValue('Disposition_Qty__c'));
        dispositionMap.put(SQX.NSPrefix+'Disposition_Type__c',new SQX_FieldValue('Disposition__c'));
        dispositionMap.put(SQX.NSPrefix+'Disposition_By__c' ,new SQX_FieldValue('Disposition_By__c'));
        dispositionMap.put(SQX.NSPrefix+'Completed_By__c' ,new SQX_FieldValue('Disposition_Completion_By__c'));
        dispositionMap.put(SQX.NSPrefix+'Completed_On__c',new SQX_FieldValue('Disposition_Completion_Date__c'));
        dispositionMap.put(SQX.NSPrefix+'Comment__c',new SQX_FieldValue('Disposition_Comment__c'));        
        return dispositionMap;
    }
}