/**
 * test class to test NSI Restart functionality
 */
@isTest
public class SQX_Test_5509_NSI_Restart {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        SQX_Test_NSI nsi;
        SQX_Onboarding_Step__c wfPolicy = new SQX_Onboarding_Step__c();
        List<Task> sfTasks;
        Task sfTask;
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 1);
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 2);
        }
        System.runAs(standardUser){
            // ARRANGE : NSI is created
            nsi = new SQX_Test_NSI().save();
            
            nsi.submit();
            nsi.initiate();
            
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, [SELECT Id, WOrkflow_Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Workflow_Status__c);
            
        }
        System.runAs(assigneeUser){
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Step__c = 1];
            
            sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id AND Child_What_Id__c =: wfPolicy.Id];
            
            System.assertEquals(1, sfTasks.size());
            
            sfTask = sfTasks.get(0);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            System.assertEquals(2, [SELECT Id, Current_Step__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Current_Step__c);
            
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Step__c = 2];
            sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id AND Child_What_Id__c =: wfPolicy.Id];
            
            System.assertEquals(1, sfTasks.size());
            
            sfTask = sfTasks.get(0);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            System.assertEquals(SQX_NSI.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Status__c);
            
        }
    }
    
    /**
     * GIVEN : A NSI with workflow policies completed and onboarding step with step 1 is redone
     * WHEN : NSI is restarted
     * THEN : NSI is reopened and the redone onboarding steps are opened and current step will be equal to 1
     * @story : [SQX-5509]
     * @date : 2018/05/15
     */
    public static testMethod void givenNSIWithAllPoliciesCompleted_WhenNSIIsRestarted_RedoneWorkflowPolicyWillBeOpened(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
        SQX_Onboarding_Step__c wfPolicy = new SQX_Onboarding_Step__c();
        List<Task> sfTasks;
        Task sfTask;
        System.runAs(standardUser){
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id AND Step__c = 1];
            SQX_Test_NSI tnsi = new SQX_Test_NSI();
            tnsi.redoOnboardingStep(wfPolicy);

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : NSI is restarted
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            nsi.Workflow_Status__c = SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS;
            nsi.Activity_Code__c = 'restart';
            nsi.Current_Step__c = 1;
            
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});

            SQX_BulkifiedBase.clearAllProcessedEntities();

            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c = : nsi.Id AND Step__c = 1 AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            SQX_New_Supplier_Introduction__c currentNSI = [SELECT Id, Record_Stage__c, Status__c, Workflow_Status__c, Current_Step__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.Id];
            
            System.assertEquals(SQX_NSI.STATUS_OPEN, currentNSI.Status__c);
            System.assertEquals(SQX_NSI.STAGE_IN_PROGRESS, currentNSI.Record_Stage__c);
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, currentNSI.Workflow_Status__c);
            System.assertEquals(1, currentNSI.Current_Step__c);
            
        }
        Test.startTest();
        System.runAs(assigneeUser){
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id AND Step__c = 1 AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN];
            sfTasks = new List<Task>{[SELECT Id FROM Task WHERE WhatId =: nsi.Id AND Child_What_Id__c =: wfPolicy.Id]};
            
            // ASSERT : Redone workflow policy is opened
            System.assertEquals(1, sfTasks.size());
            
            sfTask = sfTasks.get(0);
            
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            System.assertEquals(SQX_NSI.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.Id].Status__c);
        } 
    }
    
    /**
     * GIVEN : A NSI with workflow policies completed and onboarding step with step 2 is redone
     * WHEN : NSI is restarted
     * THEN : NSI is reopened and the redone onboarding steps are opened and current step will be equal to 2
     * @story : [SQX-5509]
     * @date : 2018/05/15
     */
    public static testMethod void givenNSIWithAllPoliciesCompleted_WhenNSIIsRestarted_RedoneWorkflowPolicyWillBeOpenedWhichIsNotFIrstStep(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
        SQX_Onboarding_Step__c wfPolicy = new SQX_Onboarding_Step__c();
        List<Task> sfTasks;
        Task sfTask;
        System.runAs(standardUser){
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id AND Step__c = 2];
            SQX_Test_NSI tnsi = new SQX_Test_NSI();
            tnsi.redoOnboardingStep(wfPolicy);

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : NSI is restarted
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            nsi.Workflow_Status__c = SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS;
            nsi.Current_Step__c = 1;
            nsi.Activity_Code__c = 'restart';

            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});

            SQX_BulkifiedBase.clearAllProcessedEntities();

            System.assertEquals(0, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c = : nsi.Id AND Step__c = 1 AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c = : nsi.Id AND Step__c = 2 AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            SQX_New_Supplier_Introduction__c currentNSI = [SELECT Id, Record_Stage__c, Status__c, Workflow_Status__c, Current_Step__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.Id];
            
            System.assertEquals(SQX_NSI.STATUS_OPEN, currentNSI.Status__c);
            System.assertEquals(SQX_NSI.STAGE_IN_PROGRESS, currentNSI.Record_Stage__c);
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, currentNSI.Workflow_Status__c);
            System.assertEquals(2, currentNSI.Current_Step__c);
            
        }
        Test.startTest();
        System.runAs(assigneeUser){
            wfPolicy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id AND Step__c = 2 AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN];
            sfTasks = new List<Task>{[SELECT Id FROM Task WHERE WhatId =: nsi.Id AND Child_What_Id__c =: wfPolicy.Id]};
            
            // ASSERT : Redone workflow policy is opened
            System.assertEquals(1, sfTasks.size());
            
            sfTask = sfTasks.get(0);
            
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            System.assertEquals(SQX_NSI.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.Id].Status__c);
        } 
    }

}