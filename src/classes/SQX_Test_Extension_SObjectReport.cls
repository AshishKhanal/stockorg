/**
 * This is class tests for the functional aspect of the SQX_Extension_SObjectReport that has been created as global component so that
 * Clients can configure their reports based on their needs.
 * The following test cases are performed
 * i. When a audit's tree is fetched, correct data is exported
 * ii. When other object's data is fetched, an error is thrown (any unsupported object)
 */
@IsTest
public class SQX_Test_Extension_SObjectReport {

    /**
     * When a complaint is selected the data related to audit should be returned correctly
     */
    public static testmethod void givenComplaint_WhenTreeIsFetchedCorrectDataIsReturned(){
        //Arrange: Create an audit and add three findings to it and a note to audit
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        
        System.runas(standardUser){
            SQX_Test_Audit audit = new SQX_Test_Audit()
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
            
            //add two findings
            SQX_Test_Finding    finding1 = audit.addAuditFinding().setRequired(false, false, false, false, false).setStatus(SQX_Finding.STATUS_DRAFT).save(),
                                finding2 = audit.addAuditFinding().setRequired(false, false, false, false, false).setStatus(SQX_Finding.STATUS_DRAFT).save();
            
            //add a note
            Note note = new Note(ParentID = audit.audit.Id, Title = 'Boo ya', Body = 'Hello, from the other side');
            insert note;
            
            //Act: get the tree node from the report extension
            SQX_Extension_SObjectReport reportExt = new SQX_Extension_SObjectReport(new ApexPages.StandardController(new SQX_Audit__c(Id = audit.audit.Id)));
            SObjectTreeNode node = reportExt.mainRecordNode;
            
            
            //Assert: Ensure the main node is for the same complaint
            String findingRelationshipName = SQX.NSPrefix + 'SQX_Findings__r';
            System.assertEquals(audit.audit.Id, node.data.Id, 'Expected the main node to be for the same audit but found ' + node);
            System.assertEquals(true, node.childObjectsKeys.contains(findingRelationshipName), 'Expected audit finding to be in the node'  + node.childObjectsKeys);
            System.assertEquals(2, node.childObjects.get(findingRelationshipName).size(), 'Expected audit finding to be serialized');
            System.assertEquals(1, node.childObjects.get('Notes').size(), 'Expected a note for the audit');
           
            Map<Id, SObject> findingMap = new Map<Id, SQX_Finding__c>();
            for(SObjectTreeNode finding : node.childObjects.get(findingRelationshipName)){
                findingMap.put(finding.data.Id, finding.data);
            }
                
            System.assertEquals(true, findingMap.containsKey(finding1.finding.Id), 'Expected finding 1 to be in the data');
            System.assertEquals(true, findingMap.containsKey(finding2.finding.Id), 'Expected finding 2 to be in the data');
            
        }
    }
    
    /**
     * Invalid object types throw and exception for any object else than supported ones
     */
    public static testmethod void givenAnyUnsupportedObject_WhenTreeIsFetchedErrorIsReturned(){
        //Arrange: create a user object
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);
        
        System.runAs(adminUser){
            
            //Act: User type is not supported by the extension, fetch the node for user type
            Boolean hasError = false;
            try{
                SQX_Extension_SObjectReport reportExt = new SQX_Extension_SObjectReport(new ApexPages.StandardController(new User(Id = adminUser.Id)));
                SObjectTreeNode node = reportExt.mainRecordNode;
            }
            catch(SQX_ApplicationGenericException ex){
                hasError = true;
            }
            
            //Assert: Ensure that an exception was thrown
            System.assertEquals(true, hasError, 'Expected an exception to be thrown but wasn\'t.');
            
        }
    }
}