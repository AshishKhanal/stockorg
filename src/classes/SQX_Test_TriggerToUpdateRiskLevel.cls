@istest
public class SQX_Test_TriggerToUpdateRiskLevel{
    
    static testmethod void testtrigger1(){
        
        
        Account account = new Account();
        account.name = 'Test Account' + Math.random();
        account.AccountNumber = '_' + Math.random();
        account.Sic = '123123123';
        account.Status__c = 'Active';
        account.Approval_Status__c = 'Approved';
        insert account;
        
        SQX_Part_Family__c newPF1 = new SQX_Part_Family__c();
        newPF1.Name = 'dummyFamily';
        insert newPF1;
        
        SQX_Part__c p1 = new SQX_Part__c();
        p1.Name = 'partXXX';
        p1.Part_Number__c = 'pt1231';
        p1.Part_Risk_Level__c = 3;
        p1.Part_Description__c = 'new part for testing purpose';
        p1.Active__c = TRUE;
        p1.Part_Family__c = newPF1.id;
        insert p1;
        
        SQX_Supplier_Part__c SP1 = new SQX_Supplier_Part__c();        
        SP1.Account__c = account.id;
        SP1.Part__c = p1.id;
        SP1.Supplier_Part_Risk_Level__c = 1;
        SP1.Active__c = true;
        SP1.Approved__c = true;
        insert SP1;
        
        SQX_Supplier_Part__c SP2;
        SP2 = [select id, Supplier_Part_Risk_Level__c from SQX_Supplier_Part__c
               where id =:SP1.id];
        
        System.assert(SP2.Supplier_Part_Risk_Level__c==P1.Part_Risk_Level__c,
                      'Expected supplier part risk level to be same as part risk level, but found unequal (Reason: workflow updates value) ');
        
        p1.Part_Risk_Level__c = 1;
        update p1;

        SP2 = [select id, Supplier_Part_Risk_Level__c from SQX_Supplier_Part__c
               where id =:SP1.id];
          System.assert(SP2.Supplier_Part_Risk_Level__c==P1.Part_Risk_Level__c,
                      'Expected supplier part risk level to be same as part risk level, but found unequal(Reason trigger update value) ');
        
    }

}