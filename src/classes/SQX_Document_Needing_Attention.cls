/**
* @author Shailesh Maharjan
* @date 2015/11/16
* @return returns all documents that need attention whose status are Draft or Approved for the responsible user. 
*/
public with sharing class SQX_Document_Needing_Attention {

    public SQX_Document_Needing_Attention() {
    }
    
    public List<SQX_Controlled_Document__c> getDocuments() {
        Set<ID> queue_current_user_IDs= SQX_Utilities.getQueueIdsForUser(UserInfo.getUserId());
        queue_current_user_IDs.add(UserInfo.getUserId());
            
    List<String> docStatuses = new List<String>();
    docStatuses.add(SQX_Controlled_Document.STATUS_DRAFT);
    docStatuses.add(SQX_Controlled_Document.STATUS_APPROVED);
    
    return [SELECT Id, Name, Document_And_Rev__c, Title__c, Document_Status__c, Approval_Status__c, Date_Approved__c
            FROM SQX_Controlled_Document__c
            WHERE Document_Status__c IN :docStatuses and OwnerID IN :queue_current_user_IDs];
    }
}