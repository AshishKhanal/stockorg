/**
 * class for step evaluation
 */
public with sharing class SQX_Step_Evaluation_Engine{

    /**
     *  Method evaluates the child steps of the given parent records
     *  and sets the new workflow status and the next step of the parent record
     *  @param parentIds ids of parent records whose values are stored and evaluated
     *  @return evaluated parent records mapped with their Ids
     */
    public Map<Id, SObject> evaluateSteps(List<Id> parentIds) {
        return evaluateSteps(parentIds, true);
    }

    /**
     *  Method evaluates the child steps of the given parent records
     *  and sets the new workflow status and the next step of the parent record
     *  @param parentIds ids of the parent record where values are stored and evaluated
     *  @param updateRecords flag determines whether the parent records are to be updated and returned or just returned
    *  @return evaluated parent records mapped with their Ids
     */
    public Map<Id, SObject> evaluateSteps(List<Id> parentIds, Boolean updateRecords){

        List<SObject> parentRecords = getRecords(parentIds);

        Map<Id, SObject> evaluatedParentRecords = evaluateSteps(parentRecords.deepClone(true));

        if(updateRecords) {
            List<SObject> parentRecordsToUpdate = new List<SObject>();
            for(SObject oldRecord : parentRecords) {
                SObject newRecord = evaluatedParentRecords.get(oldRecord.Id);

                // == operator works for SObject in this case as it does a deep copy comparing every field value
                if(oldRecord != newRecord) {
                    parentRecordsToUpdate.add(newRecord);
                }
            }


            if(parentRecordsToUpdate.size() > 0) {

                /*
                * WITHOUT SHARING used
                * --------------------
                * Since the assigee has read only access to parent record 
                * when the user complete SF task created for the assigee, it updates on-boarding steps
                * and if all on-boarding steps are completed it will update parent record status
                */
                Map<String, Schema.SObjectField> fieldMap = parentRecordsToUpdate.get(0).getSObjectType().getDescribe().fields.getMap();
                new SQX_DB().withoutSharing().op_update(parentRecordsToUpdate,
                                                        new List<Schema.SObjectField> {
                                                            fieldMap.get(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS),
                                                            fieldMap.get(SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP)
                                                        });
            }
        }

        return evaluatedParentRecords;
    }


    /**
     *  Method evaluates the child steps of the given parent records
     *  and sets the new workflow status and the next step of the parent record
     *  @param parentRecords parent records whose values are stored and evaluated
     *  @return updated parent record mapped with its Id
     */
    private Map<Id, SObject> evaluateSteps(List<SObject> parentRecords) {

        Map<Id, SObject> updatedParentRecords = new Map<Id, SObject>();

        String workflowStatus;

        for(SObject parentRecord : parentRecords) {

            workflowStatus = SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED;  // if no step exists, complete the record

            List<SObject> steps = parentRecord.getSObjects(SQX_Steps_Trigger_Handler.childRelationshipApiName);

            if(steps != null && steps.size() > 0) {

                SObject currentStep = steps.get(0);

                if(SQX_Steps_Trigger_Handler.isStepNoGo(currentStep)) {
                    workflowStatus = SQX_Supplier_Common_Values.WORKFLOW_STATUS_ON_HOLD;
                } else {
                    workflowStatus = SQX_Supplier_Common_Values.WORKFLOW_STATUS_IN_PROGRESS;
                }

                parentRecord.put(SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP, Integer.valueOf(currentStep.get(SQX_Steps_Trigger_Handler.FIELD_STEP)));
            }

            parentRecord.put(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS, workflowStatus);

            updatedParentRecords.put(parentRecord.Id, parentRecord);
        }

        return updatedParentRecords;
    }


    /**
     *  Method queries the parent records of the given Ids along with their child steps
     *  @param parentIds ids of the parent records
     */
    private List<SObject> getRecords(List<Id> parentIds) {

        Id parentId = parentIds.get(0);
        String parentObject = parentId.getSObjectType().getDescribe().getName();

        String stepsQuery = SQX_Steps_Trigger_Handler.getLatestIncompleteStepQuery();

        String query = 'SELECT Id, ' + SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP + ',' + 
                        '(' + stepsQuery + ')' +
                        ' FROM ' + parentObject +
                        ' WHERE Id IN: parentIds';

        return Database.query(query);
    }
}