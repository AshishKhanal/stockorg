/**
 * test class for complaint task form rules evaluation
 */
@isTest
public class SQX_Test_7602_Complaint_Task_Evaluation {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        System.runAs(adminUser){
            SQX_Task__c task = new SQX_Task__c();
            task.Name = 'DT';
            task.Record_Type__c = 'Complaint';
            task.Task_Type__c = 'Decision Tree';
            task.Description__c = 'DT Description';
            task.SQX_User__c = standardUser.Id;
            task.Allowed_Days__c = 10;
            
            insert task;
        }
    }
    
    /**
     * GIVEN : Complaint Task create action is called
     * WHEN : Flow is invoked
     * THEN : Form rules are evaluated
     */
    public static testMethod void givenComplaintTask_WhenTaskIsSelected_FormRulesAreEvaluated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint Task create action is called
            Map<String, Object> params = new Map<String, Object>();
            params.put('Action', 'create');
            
            // ACT : Flow is invoked
            Flow.Interview.CQ_Complaint_Task_Form_Rules_Evaluation calcFlow = new Flow.Interview.CQ_Complaint_Task_Form_Rules_Evaluation(params);
            calcFlow.start();
            
            String output = (String) calcFlow.getVariableValue('EvaluationExpression');
            
            // ASSERT : Form rules is evaluated
            System.assert(String.isNotBlank(output), 'Output is not blank');
            System.assert(output.contains('CQ_Complaint_Task_Set_Task_Fields'), 'Experssion not evaluated');
            
        }
    }
    
    /**
     * GIVEN : Complaint Policy task is created
     * WHEN : Task is linked
     * THEN : Complaint Task fields are populated
     */
    public static testMethod void givenComplaintTask_WhenTaskIsSelected_ComplaintTaskFieldsArePopulated(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
        
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Complaint Task is created
            SQX_Task__c cqTask = [SELECT Id, Name, Description__c, Allowed_Days__c, SQX_User__c FROM SQX_Task__c LIMIT 1];
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = cqTask.Id;
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Task is linked
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaintTask);
            Flow.Interview.CQ_Complaint_Task_Set_Task_Fields calcFlow = new Flow.Interview.CQ_Complaint_Task_Set_Task_Fields(params);
            calcFlow.start();
            
            // ASSERT : Complaint Task fields are populated
            SQX_Complaint_Task__c updatedComplaintTask = (SQX_Complaint_Task__c) calcFlow.getVariableValue('OutRecord');
            System.assertEquals(cqTask.Name, updatedComplaintTask.Name);
            System.assertEquals(cqTask.Description__c, updatedComplaintTask.Task_Description__c);
            System.assertEquals(cqTask.SQX_User__c, updatedComplaintTask.SQX_User__c);
            System.assertEquals(Date.today().addDays(Integer.valueOf(cqTask.Allowed_Days__c)), updatedComplaintTask.Due_Date__c);
            
            String[] impactedFields = (String[]) calcFlow.getVariableValue('ImpactedFields');
            System.assert(impactedFields.contains('Name'), 'Impacted field Name is not included');
            System.assert(impactedFields.contains('compliancequest__Due_Date__c'), 'Impacted field compliancequest__Due_Date__c is not included');
            System.assert(impactedFields.contains('compliancequest__SQX_User__c'), 'Impacted field compliancequest__SQX_User__c is not included');
            System.assert(impactedFields.contains('compliancequest__Task_Description__c'), 'Impacted field compliancequest__Task_Description__c is not included');
            
            // ACT : Task is null
            complaintTask.SQX_Task__c = null;
            params = new Map<String, Object>();
            params.put('InRecord', complaintTask);
            calcFlow = new Flow.Interview.CQ_Complaint_Task_Set_Task_Fields(params);
            calcFlow.start();
            
            // ASSERT : All field values are cleared
            updatedComplaintTask = (SQX_Complaint_Task__c) calcFlow.getVariableValue('OutRecord');
            System.assertEquals(null, updatedComplaintTask.Name);
            System.assertEquals(null, updatedComplaintTask.Task_Description__c);
            System.assertEquals(null, updatedComplaintTask.SQX_User__c);
            System.assertEquals(null, updatedComplaintTask.Due_Date__c);
        }        
    }
    
    /**
     * GIVEN : Complaint is created
     * WHEN : Complaint Task is added with related task record type different than complaint
     * THEN : Error is thrown
     */
    public static testMethod void givenComplaintTask_WhenTaskIsSelectedWithRelatedTask_RelatedTaskWithComplaintRecordTypeShouldBeSelected(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 

            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            // ARRANGE : Complaint Task is created
            SQX_Task__c cqTask = [SELECT Id, Name, Description__c, Allowed_Days__c, SQX_User__c FROM SQX_Task__c LIMIT 1];
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = cqTask.Id;
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Task is linked with record type complaint
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaintTask);
            Flow.Interview.CQ_Complaint_Task_Set_Task_Fields calcFlow = new Flow.Interview.CQ_Complaint_Task_Set_Task_Fields(params);
            calcFlow.start();

            SQX_Complaint_Task__c updatedComplaintTask = (SQX_Complaint_Task__c) calcFlow.getVariableValue('OutRecord');
            updatedComplaintTask.SQX_Complaint__c = complaint.complaint.Id;

            // ASSERT : Save is successful
            Database.SaveResult result = Database.insert(updatedComplaintTask, false);
            System.assert(result.isSuccess(), 'Insertion not successful' + result.getErrors());

            SQX_Task__c task = new SQX_Task__c();
            task.Name = 'SI';
            task.Record_Type__c = 'Supplier Introduction';
            task.Task_Type__c = 'Approval';
            task.Description__c = 'SI Description';
            task.SQX_User__c = standardUser.Id;
            task.Allowed_Days__c = 10;
            task.Step__c = 1;
            insert task;

            // ACT : Task is linked with record type task
            updatedComplaintTask.SQX_Task__c = task.Id;
            result = Database.update(updatedComplaintTask,false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Update is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Selected Task record type should be Complaint.'));
        }
    }
}