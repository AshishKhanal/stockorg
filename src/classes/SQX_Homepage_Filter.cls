/**
*   Template class for filter JSON sent from the Homepage Component(UI)
*   @author - Piyush Subedi
*/
public with sharing class SQX_Homepage_Filter{

    /*
        Valid values for sort order
    */
    public Enum SortOrder { ASCENDING, DESCENDING }

    /*
        Valid values for source type 
    */
    public Enum Source { User } // add other source types like Org, Role Hierarchy here
    
    /*
        Specifies the type of source (User, Org, ... )
    */
    public Source sourceType {
        get{
            if(this.sourceType == null){
                this.sourceType = SQX_Homepage_Constants.DEFAULT_SOURCE_TYPE;
            }
            return this.sourceType;
        }
        set;
    }

    /*
        Specifies number of records to be returned by the item source
    */
    public Integer pageSize {
        get{
            if(this.pageSize == null){
                this.pageSize = SQX_Homepage_Constants.DEFAULT_PAGE_SIZE;
            }
            return this.pageSize;
        }
        set;
    }

    /*
        Field names(api names) using which to sort the records
    */
    public List<String> orderBy { 
        get{
            if(this.orderBy == null){
                this.orderBy = new List<String> { SQX_Homepage_Constants.ORDERIND_FIELD_CREATED_DATE};
            }
            return this.orderBy;
        }
        set;
    }

    /*
        Specifies the order in which the records are to be fetched ( ASC or DESC )
    */
    public SortOrder sortOrder { 
        get{
            if(this.sortOrder == null){
                this.sortOrder = SQX_Homepage_Constants.DEFAULT_SORT_ORDER;
            }
            return this.sortOrder;
        }
        set;
    }

    /*
        Holds a filter clause that can be used to filter the records returned
        FOR FUTURE USE
    */
    public String filterString { get; set; }

    /*
        Specifies the identifier for locating the next record that is to be sent
        Basically used to set offset clause
    */
    public String recordsAfter { get; set; }
}