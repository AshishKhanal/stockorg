/*
 * @author: Anish Shrestha
 * @date: 2014/12/09
 * @description: test for change owner of NC, CAPA, Audit
 */

@isTest
public class SQX_Test_1574_Change_Owner{

    @testSetup static void setupTestData(){
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');
        User standardUser3 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser3');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }

    static boolean runAllTests = true,
                    run_auditOwnerChanged_FindingOwnerChanged= false,
                    run_givenAuditOwnerIsChanged_RelatedRecordsOwnershipIsChanged = false;


    /**
    * Audit owner is changed, finding owner is also changed
    * New Owner should not replace previous owner in Audit team ie old owner is still a team member
    * Sharing rules should also be reevaluated
    * related story :[SQX-2369]
    */
    public static testMethod void auditOwnerChanged_FindingOwnerChanged(){

        if(!runAllTests && !run_auditOwnerChanged_FindingOwnerChanged){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole); 
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser3 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Test_Audit audit = null;
        
        System.runas(standardUser1){
            //Arrange : create new audit, add findings to it, add a team member
            audit = new SQX_Test_Audit();
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.addAuditFinding();
            audit.addAuditFinding();
            audit.save();
            SQX_Audit_Team__c newAuditor = new SQX_Audit_Team__c(SQX_User__c = standardUser2.Id, 
                                                                    SQX_Audit__c = audit.audit.Id);
            
            Database.SaveResult auditTeamInsert = Database.Insert(newAuditor,false);

            System.assertEquals(true, auditTeamInsert.isSuccess(), 'Expected audit team insertion to be successful');

            //Act : Change owner of Audit and save audit
            audit.audit.OwnerId = standardUser3.Id;
            audit.save();

            //Assert that related findings owner are synced
            List<SQX_Finding__c> findings = [SELECT OwnerId from SQX_Finding__c where SQX_Audit__c = :audit.audit.Id];
            for(SQX_Finding__c finding : findings){
                System.assertEquals(standardUser2.Id, finding.OwnerId, 'Expected Owner of Finding to be changed');
            }


            //Assert that new owner is a part of Audit team and old owner is also part of Audit Team
            List<SQX_Audit_Team__c> auditTeams = [Select SQX_User__c from SQX_Audit_Team__c WHERE SQX_Audit__c = :audit.audit.Id] ;

            Set<Id> userIds = new Set<Id>();
            for(SQX_Audit_team__c auditTeam : auditTeams){
                userIds.add(auditTeam.SQX_User__c);
            }
            System.assertEquals(true, userIds.contains(standardUser1.Id), 'Expected old owner to be in Audit Team');
            System.assertEquals(true, userIds.contains(standardUser3.Id), 'Expected new owner to be added from Audit Team');

            Schema.DescribeSObjectResult auditShareObject = Schema.getGlobalDescribe().get(SQX.AuditShare).getDescribe();
            Map<String, Schema.SObjectField> auditShareFieldMap = auditShareObject.fields.getMap();

            //Assert that audit share object keeps old owner as member and adds new owner
            SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
            filter.field = 'ParentId';
            filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter.value = audit.audit.Id;

            List<Schema.SObjectField> sortBy = new List<Schema.SObjectField>();
            sortBy.add(auditShareFieldMap.get('Id'));

            List<Schema.SObjectField> fields = new List<Schema.SObjectField>();
            fields.add(auditShareFieldMap.get('UserOrGroupId'));

            List<Sobject> auditShares = SQX_DynamicQuery.evaluate(1, 100, auditShareObject, fields, filter, sortBy, true);
            System.assertEquals(4, auditShares.size(), 'Expected record to be shared to three users as there are three persons in audit team and owner will have two sharings so total sharings must be four');

            Set<Id> userShareIds = new Set<Id>();
            for(Sobject auditShare : auditShares){
                userShareIds.add((Id)auditShare.get('UserOrGroupId'));
            }

            System.assertEquals(true, userShareIds.contains(standardUser1.Id), 'Expected old owner to be in Sharing');
            System.assertEquals(true, userShareIds.contains(standardUser3.Id), 'Expected new owner to be added in Sharing');
        }
    }


    /*
    * This test ensures that ownership of an audit with single audit team member (Default user) can be done without errors
    * Action: Change the ownership of audit with single team member (owner)
    * Expected: a. Ownership of audit is transferred, 
    *           b. Ownership of finding is transferred.
    *           c. Audit Team Member record is modified to reflect new owner.
    *           d. Sharing rules are correcly 
    * related story :[SQX-2369]
    */
    public static testMethod void givenAuditOwnerIsChanged_RelatedRecordsOwnershipIsChanged(){

        if(!runAllTests && !run_givenAuditOwnerIsChanged_RelatedRecordsOwnershipIsChanged){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole); 
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Test_Audit audit = null;
        
        System.runas(standardUser1){
            //Arrange : create new audit, add findings to it, add a team member
            audit = new SQX_Test_Audit();
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.addAuditFinding();
            audit.addAuditFinding();
            audit.save();
            
            //Act : Change owner of Audit and save audit
            audit.audit.OwnerId = standardUser2.Id;
            audit.save();

            //Assert that related findings owner are synced
            List<SQX_Finding__c> findings = [SELECT OwnerId from SQX_Finding__c where SQX_Audit__c = :audit.audit.Id];
            for(SQX_Finding__c finding : findings){
                System.assertEquals(standardUser2.Id, finding.OwnerId, 'Expected Owner of Finding to be changed');
            }


            //Assert that new owner is a part of Audit team and old owner is also part of Audit Team
            List<SQX_Audit_Team__c> auditTeams = [Select SQX_User__c from SQX_Audit_Team__c WHERE SQX_Audit__c = :audit.audit.Id] ;

            Set<Id> userIds = new Set<Id>();
            for(SQX_Audit_team__c auditTeam : auditTeams){
                userIds.add(auditTeam.SQX_User__c);
            }
            System.assertEquals(true, userIds.contains(standardUser1.Id), 'Expected old owner to be in Audit Team');
            System.assertEquals(true, userIds.contains(standardUser2.Id), 'Expected new owner to be added from Audit Team');

            Schema.DescribeSObjectResult auditShareObject = Schema.getGlobalDescribe().get(SQX.AuditShare).getDescribe();
            Map<String, Schema.SObjectField> auditShareFieldMap = auditShareObject.fields.getMap();

            //Assert that audit share object adds new owner but does not remove old owner
            SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
            filter.field = 'ParentId';
            filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter.value = audit.audit.Id;

            List<Schema.SObjectField> sortBy = new List<Schema.SObjectField>();
            sortBy.add(auditShareFieldMap.get('Id'));

            List<Schema.SObjectField> fields = new List<Schema.SObjectField>();
            fields.add(auditShareFieldMap.get('UserOrGroupId'));

            List<Sobject> auditShares = SQX_DynamicQuery.evaluate(1, 100, auditShareObject, fields, filter, sortBy, true);
            System.assertEquals(3, auditShares.size(), 'Expected record to be shared with both users as there are both new owner and old owner in audit team and new owner will have two sharing, so there are 3 sharings');

            Set<Id> userShareIds = new Set<Id>();
            for(Sobject auditShare : auditShares){
                userShareIds.add((Id)auditShare.get('UserOrGroupId'));
            }

            System.assertEquals(true, userShareIds.contains(standardUser1.Id), 'Expected old owner to be in Sharing');
            System.assertEquals(true, userShareIds.contains(standardUser2.Id), 'Expected new owner to be added in Sharing');
        }
    }

    /**
     * Given: Create Audit and add Audit Team Members.
     * When: One of team member is made the owner of audit
     * Then: The audit should have new owner but old owner is still the team member ie team size is same.
     * 
     * @date:2016/7/5
     * @author:Paras Kumar Bishwakarma
     * @story :[SQX-2369]
     */
    public static testmethod void givenAuditWithTeamMembers_WhenOwnerIsChangeToOneOfTeamMember_OwnerIsChangedAndOldOwnerIsInTeam(){
        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();

        //get user from the map 
        User standardUser1 = allUsers.get('standardUser1');
        User standardUser2 = allUsers.get('standardUser2');
        User standardUser3 = allUsers.get('standardUser3');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Audit audit = null;
        System.runas(standardUser1){

            //Arrange : create new audit, add findings to it
            audit = new SQX_Test_Audit(adminUser);
            audit.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit.setStatus(SQX_Audit.STATUS_OPEN);
            audit.addAuditFinding();
            audit.addAuditFinding();
            audit.save();

            //Create Audit Team and add a team member
            SQX_Audit_Team__c newAuditor = new SQX_Audit_Team__c(SQX_User__c = standardUser2.Id, 
                                                                SQX_Audit__c = audit.audit.Id);
            
            Database.SaveResult auditTeamInsert = Database.Insert(newAuditor,false);
            System.assertEquals(true, auditTeamInsert.isSuccess(), 'Expected audit team insertion to be successful');

            //Act : Change owner of Audit and save audit (here new owner is a previous member of Audit team)
            audit.audit.OwnerId = standardUser2.Id;
            audit.save();

            //Assert that the owner is changed successfully
            System.assertEquals(audit.audit.OwnerId,standardUser2.Id,'Expected new owner to be added to the audit');

            List<SQX_Audit_Team__c> auditTeams = [Select SQX_User__c from SQX_Audit_Team__c WHERE SQX_Audit__c = :audit.audit.Id] ;
           
            //Assert that the team size is two ie no change in team size when same team member is made owner         
            System.assertEquals(2,auditTeams.size(), 'Expected the team size not changed when same team member is made owner of audit');

            Set<Id> userIds = new Set<Id>();
            for(SQX_Audit_team__c auditTeam : auditTeams){
                userIds.add(auditTeam.SQX_User__c);
            }

            //Assert that new owner is a part of Audit team and old owner is also part of Audit Team
            System.assertEquals(true, userIds.contains(standardUser1.Id), 'Expected old owner to be in Audit Team');
            System.assertEquals(true, userIds.contains(standardUser2.Id), 'Expected new owner to be added from Audit Team');

        }
    }   
}
