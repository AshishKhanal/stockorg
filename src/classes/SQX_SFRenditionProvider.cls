/**
* This is Rendition Provider that fetches PDF from Salesforce for a given content document 
*/
public class SQX_SFRenditionProvider implements SQX_RenditionProvider {
    final String RENDITION_SERVICE_BASE_PATH = '/services/data/v36.0',
                 SALESFORCE_HOST = 'callout:' + SQX_Custom_Settings_Public__c.getInstance().SF_Rendition_Provider_Callout__c,
                 HTTP_GET = 'GET';

    final integer HTTP_ERROR_STATUS = 404, HTTP_CONTINUE_STATUS = 202, HTTP_OK_STATUS = 200;

    /**
    * Retrieves the PDF rendition from salesforce using salesforce's chatter rest api for the controlled content
    * @param request the rendition request containing detail regarding the rendition that is to be retrieved.
    * @return returns the rendition response containing the pdf blob.
    */
    public SQX_RenditionResponse retrieve(SQX_RenditionRequest request){
        final String RENDITION_SERVICE_PATH = RENDITION_SERVICE_BASE_PATH + '/connect/files/{ID}/rendition?type=PDF';


        SQX_RenditionResponse response = new SQX_RenditionResponse();
        
        ContentVersion ver = request.content;
        String url =  SALESFORCE_HOST + RENDITION_SERVICE_PATH.replace('{ID}', ver.ContentDocumentId);
        
        // create the http request for the url with the authorization
        HttpRequest req = new HttpRequest();
        req.setMethod(HTTP_GET);
        req.setEndpoint(url);
        req.setHeader('Content-type', 'application/json');
        
        
        // get the response from the api request            
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        // if the rendition was not found set an error and continue
        Integer statusCode = res.getStatusCode();
        
        
        response.jobId = ver.ContentDocumentId;
        if(statusCode == HTTP_ERROR_STATUS){
            
            response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
            response.errorDescription = res.getBody();
            response.status = SQX_RenditionResponse.Status.Error;
            
            // List<Object> jsonResponse = (List<Object>) JSON.deserializeUntyped(response.errorDescription);            
        }
        else if(statusCode == HTTP_CONTINUE_STATUS){
            response.status = SQX_RenditionResponse.Status.Processing;
        }
        else if(statusCode == HTTP_OK_STATUS){
            response.status = SQX_RenditionResponse.Status.RenditionReady;
            response.result = new ContentVersion(VersionData = res.getBodyAsBlob(), PathOnClient = 'Auto-' + DateTime.now() + '.pdf');
        }
        else{
            response.status = SQX_RenditionResponse.Status.Error;
            response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
            response.errorDescription = res.getBody();
        }

        return response;
    }

    /**
    * Queries salesforce's server for the status of the rendition request previously submitted.
    * @param requests the list of requests that where submitted previously and whose status is to be checked.
    */
    public Map<SQX_RenditionRequest, SQX_RenditionResponse> poll(SQX_RenditionRequest [] requests){
        final String QUERY_SERVICE_PATH = RENDITION_SERVICE_BASE_PATH + '/connect/files/batch/{FILE_IDS}';
        Map<SQX_RenditionRequest, SQX_RenditionResponse> response = new Map<SQX_RenditionRequest, SQX_RenditionResponse>();
        final String RENDITION_NOT_SCHEDULED = 'NotScheduled',
                     RENDITION_NOT_AVAILABLE = 'Na',
                     RENDITION_READY = 'Success',
                     RENDITION_PROCESSING = 'Processing',
                     RENDITION_FAILED = 'Failed';

        if(requests != null && requests.size() > 0){

            String ids = '';

            Map<Id, SQX_RenditionRequest> contentDoc2Request = new Map<Id, SQX_RenditionRequest>();

            for(SQX_RenditionRequest request : requests){
                ids += request.Content.ContentDocumentId + ',';
                contentDoc2Request.put(request.Content.ContentDocumentId, request);
            }

            if(ids.endsWith(','))
                ids = ids.substringBeforeLast(',');

            String url =  SALESFORCE_HOST + QUERY_SERVICE_PATH.replace('{FILE_IDS}', ids);

            // create the http request for the url with the authorization
            HttpRequest req = new HttpRequest();
            req.setMethod(HTTP_GET);
            req.setEndpoint(url);
            req.setHeader('Content-type', 'application/json');
            
            // get the response from the api request            
            Http http = new Http();
            HTTPResponse res = http.send(req);

            // if the rendition was not found set an error and continue
            Integer statusCode = res.getStatusCode();
            if(statusCode == HTTP_OK_STATUS){

                SFResponse sfResp = (SFResponse) JSON.deserialize(res.getBody(), SFResponse.class);
                for(SFResponseResults result : sfResp.results){
                    Id contentDocumentId = result.result.id;
                    String renditionStatus = result.result.pdfRenditionStatus;


                    SQX_RenditionRequest request =  contentDoc2Request.get(contentDocumentId);

                    System.assert(request != null, contentDoc2Request + '' + contentDocumentId + ' ' + result );

                    SQX_RenditionResponse r = new SQX_RenditionResponse();
                    r.status =  renditionStatus == RENDITION_READY ? SQX_RenditionResponse.Status.RenditionReady :
                                renditionStatus == RENDITION_PROCESSING ? SQX_RenditionResponse.Status.Processing : 
                                        SQX_RenditionResponse.Status.Error;

                    if(r.status ==  SQX_RenditionResponse.Status.Error){
                       
                        r.errorCode = renditionStatus == RENDITION_NOT_SCHEDULED ? SQX_RenditionResponse.ErrorCodes.NotSubmitted :
                                      (renditionStatus == RENDITION_NOT_AVAILABLE || renditionStatus == RENDITION_FAILED)   ? SQX_RenditionResponse.ErrorCodes.UnsupportedFormat :
                                      SQX_RenditionResponse.ErrorCodes.UnknownError;

                        r.errorDescription = renditionStatus == RENDITION_NOT_AVAILABLE ? Label.SQX_ERR_MSG_RENDITION_NOT_SUPPORTED.replace('{FILE_EXT}', request.content.FileExtension).replace('{PROVIDER}', this.getProviderName()) :
                                             JSON.serialize(result);                
                    }

                    r.jobId = contentDocumentId;

                    response.put(request, r);
                }
            }
            else{
            	for(SQX_RenditionRequest request : requests){
            		SQX_RenditionResponse r = new SQX_RenditionResponse();
        
			        r.jobId = request.Content.ContentDocumentId;
			        
		            r.status = SQX_RenditionResponse.Status.Error;
		            r.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
		            r.errorDescription = res.getBody();

					response.put(request, r);
				}
            }

        }

        return response;
    }
    
    /**
    * submits a request to salesforce to start processing the rendition
    * For salesforce both request submission and retrieval are same method. First request for retrieval acts as a submission request
    */
    public SQX_RenditionResponse submitRequest(SQX_RenditionRequest request){
        Map<SQX_RenditionRequest, SQX_RenditionResponse> pollResult = poll(new SQX_RenditionRequest[] { request });
        SQX_RenditionResponse response = pollResult.get(request);

        if(response.errorCode != SQX_RenditionResponse.ErrorCodes.UnsupportedFormat)
            response = retrieve(request);

        return response;
    }
    
    /**
    * this is identifiable name of the provider
    */
    public String getProviderName(){
        return 'Salesforce PDF Rendition Generator';
    }
    

    /**
    * Cancels the request to create a rendition
    */
    public void abort(SQX_RenditionRequest[] requests){
        // do nothing for salesforce because there is no api to cancel a rendition request.
    }


    /**
    * Checks if the selected rendition provider supports a particular rendition or not
    */
    public Map<SQX_RenditionRequest, Boolean> supportsRendition(SQX_RenditionRequest[] requests){
        Map<SQX_RenditionRequest, SQX_RenditionResponse> pollResult = poll(requests);
        Map<SQX_RenditionRequest, Boolean> result = new Map<SQX_RenditionRequest, Boolean>();

        for(SQX_RenditionRequest request : pollResult.keySet()){
            SQX_RenditionResponse response = pollResult.get(request);
            result.put(request, response.errorCode != SQX_RenditionResponse.ErrorCodes.UnsupportedFormat);
        }

        return result;
    } 


    //////// Salesforce Response's Object //////
    public class SFResponse {
        public List<SFResponseResults> results {get; set;}
    }
    
    public class SFResponseResults {
        public SFResponseResult result {get; set;}
        public Integer statusCode {get; set;}
    }

    public class SFResponseResult {
        public Id id {get; set;}
        public String pdfRenditionStatus {get; set;}
    }
}