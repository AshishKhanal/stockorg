/**
*   This test class contains test methods to validate that
*   fulfilled requirements receive credit when associated course is signed off
*   @author : Piyush Subedi
*   @date : 11/19/2017
*/
@IsTest
public class SQX_Test_3614_Course_SignOff_Credit {

    static String STANDARD_USER = 'standardUser',
                  STANDARD_USER_2 = 'standardUser2',
                  ADMIN_USER = 'adminUser';

    static String RELATED_DOCS_TITLE = 'Related Document';

    static String PERSONNEL_1 = 'P1';

    @testSetup
    static void commonSetup()
    {
        User admin = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, STANDARD_USER);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, STANDARD_USER_2);

        System.runAs(standardUser)
        {
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.mainRecord.SQX_User__c = standardUser.Id;
            personnel.mainRecord.Full_Name__c = PERSONNEL_1;
            personnel.save();

            // Prepare random docs (to be considered as related docs on subsequent test methods)
            SQX_Test_Controlled_Document rDoc1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT),
                                         rDoc2 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE),
                                         rDoc3 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);

            rDoc1.doc.Title__c = RELATED_DOCS_TITLE;
            rDoc2.doc.Title__c = RELATED_DOCS_TITLE;
            rDoc3.doc.Title__c = RELATED_DOCS_TITLE;

            new SQX_DB().op_insert(new List<SQX_Controlled_Document__c> { rDoc1.doc, rDoc2.doc, rDoc3.doc }, new List<Schema.SObjectField> {});

            // arrange an assessment
            SQX_Test_Assessment ta1 = new SQX_Test_Assessment()
                                            .setName('PENDING_ASSESSMENT')
                                            .setStatus(SQX_Assessment.STATUS_CURRENT);
            SQX_Test_Assessment ta2 = new SQX_Test_Assessment()
                                            .setName('COMPLETE_ASSESSMENT')
                                            .setStatus(SQX_Assessment.STATUS_CURRENT);
            new SQX_DB().op_insert(new List<SQX_Assessment__c> { ta1.assessment, ta2.assessment }, new List<Schema.SObjectField>{});

            // arrange a personnel assessment
            SQX_Personnel_Assessment__c pass = new SQX_Personnel_Assessment__c(
                SQX_Assessment__c = ta2.assessment.Id,
                SQX_Personnel__c = personnel.mainRecord.Id
            );
            new SQX_DB().op_insert(new List<SQX_Personnel_Assessment__c> { pass }, new List<Schema.SObjectField>{});

            SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt = new SQX_Personnel_Assessment_Attempt__c(
                        SQX_Personnel_Assessment__c = pass.Id,
                        Result__c = SQX_Assessment.ASSESSMENT_RESULT_SATISFACTORY,
                        Submitted_Date__c = Date.today());

            new SQX_DB().op_insert(new List<SQX_Personnel_Assessment_Attempt__c> { personnelAssessmentAttempt },
                                   new List<Schema.SObjectField> {});

            pass.SQX_Last_Assessment_Attempt__c = personnelAssessmentAttempt.Id;
            new SQX_DB().op_update(new List<SQX_Personnel_Assessment__c> { pass }, new List<Schema.SObjectField>{});

        }
    }

    /**
    *   Returns personnel record for the given user
    */
    static SQX_Personnel__c getPersonnel(String userId)
    {
        return [SELECT Id FROM SQX_Personnel__c WHERE SQX_User__c =: userId LIMIT 1];
    }

    /**
    *   Returns a list of documents to be used as related documents
    */
    static List<SQX_Controlled_Document__c> getRelatedDocuments()
    {
        return [SELECT Id FROM SQX_Controlled_Document__c WHERE Title__c =: RELATED_DOCS_TITLE];
    }

    /**
    *   Scenario Constructed:
    *   - Course with two fulfilled requirements
    *   - One of the requirements has existing PDT
    *   Given - PDT on a Course type controlled document record with multiple fulfilled requirements
    *   When - the PDT is user signed off
    *   Then - PDTs on fulfilled requirements are user signed off, generating them if necessary
    */
    static testmethod void givenDocumentTrainingOnACourseWithFulfilledRequirements_WhenTrainingIsUserSignedOff_TrainingOnFulfilledRequirementsAreUserSignedOff()
    {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser)
        {
            SQX_Personnel__c psn = getPersonnel(standardUser.Id);

            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                 .save();

            List<SQX_Controlled_Document__c> rDocs = getRelatedDocuments();

            SQX_Controlled_Document__c rDoc1 = rDocs.get(0),
                                       rDoc2 = rDocs.get(1),
                                       rDoc3 = rDocs.get(2);

            List<SQX_Related_Document__c> relatedDocs = new List<SQX_Related_Document__c>();
            for(SQX_Controlled_Document__c rDoc : rDocs)
            {
                 relatedDocs.add(new SQX_Related_Document__c(
                    Controlled_Document__c = cDoc1.doc.Id,
                    Referenced_Document__c = rDoc.Id,
                    Specific_Rev__c = false,
                    Fulfilled_Requirement__c = true
                ));
            }
            new SQX_DB().op_insert(relatedDocs, new List<Schema.SObjectField> {});

            // add pre-existing PDT for rDoc2
            SQX_Personnel_Document_Training__c pdt_rDoc2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );

            // add pre-existing PDT for rDoc3 but with pending assessment
            SQX_Personnel_Document_Training__c pdt_rDoc3 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc3.Id,
                SQX_Assessment__c = [SELECT Id FROM SQX_Assessment__c WHERE Name = 'PENDING_ASSESSMENT'].Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt_rDoc2, pdt_rDoc3 }, new List<Schema.SObjectField>{});

            // preparing PDT for course
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false
            );

            // ACT : user-sign off the document(course)
            pdt1.Completion_Date__c = System.today();
            pdt1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;

            Test.startTest();

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt1 }, new List<Schema.SObjectField>{});

            // ASSERT : Completed DT is created for rDoc1 and not for rDoc2

            integer numberOfTrainings = 0;
            for(SQX_Personnel_Document_Training__c pdt : [SELECT Id,
                                                            SQX_Personnel__c,
                                                            SQX_Controlled_Document__c,
                                                            Level_of_Competency__c,
                                                            Status__c,
                                                            User_SignOff_Date__c,
                                                            Trainer_SignOff_Date__c
                                                        FROM SQX_Personnel_Document_Training__c
                                                        WHERE (SQX_Controlled_Document__c =: rDoc1.Id OR SQX_Controlled_Document__c =: rDoc2.Id)
                                                        AND SQX_Personnel__c =: psn.Id])
            {

                // verifying that the dts have been user signed off
                System.assertEquals(null, pdt.Trainer_SignOff_Date__c);
                System.assertNotEquals(null, pdt.User_SignOff_Date__c);

                if(pdt.SQX_Controlled_Document__c == rDoc1.Id)
                {
                    // verifying that the newly created DT has been completed
                    System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdt.Status__c);

                    // verify that the field values have been copied
                    System.assertEquals(pdt1.Level_of_Competency__c, pdt.Level_of_Competency__c);
                }
                else
                {
                    // verifying that the existing pdt which required Trainer Approval has its status set correctly
                    System.assertEquals(SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING, pdt.Status__c);
                }

                numberOfTrainings++;
            }

            Test.stopTest();

            // expecting exactly two PDTs
            System.assertEquals(2, numberOfTrainings);

        }

    }


    /**
    *   Scenario Constructed:
    *   - Course with two fulfilled requirements
    *   - One of the requirements has existing PDT
    *   Given - PDT on a Course type controlled document record with multiple fulfilled requirements
    *   When - the PDT is trainer signed off
    *   Then - PDTs on fulfilled requirements are trainer signed off(i.e STATUS = COMPLETE), generating them if necessary
    */
    static testmethod void givenDocumentTrainingOnACourseWithFulfilledRequirements_WhenTrainingIsTrainerSignedOff_TrainingOnFulfilledRequirementsAreTrainerSignedOff()
    {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER);
        User standardUser = users.get(STANDARD_USER);

        SQX_Personnel_Document_Training__c pdt1;
        SQX_Personnel__c psn;
        SQX_Controlled_Document__c rDoc1,rDoc2,rDoc3;

        System.runAs(standardUser)
        {
            psn = getPersonnel(standardUser.Id);

            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                 .save();

            List<SQX_Controlled_Document__c> rDocs = getRelatedDocuments();

            rDoc1 = rDocs.get(0);
            rDoc2 = rDocs.get(1);
            rDoc3 = rDocs.get(2);

            List<SQX_Related_Document__c> relatedDocs = new List<SQX_Related_Document__c>();
            for(SQX_Controlled_Document__c rDoc : rDocs)
            {
                 relatedDocs.add(new SQX_Related_Document__c(
                    Controlled_Document__c = cDoc1.doc.Id,
                    Referenced_Document__c = rDoc.Id,
                    Specific_Rev__c = false,
                    Fulfilled_Requirement__c = true
                ));
            }

            new SQX_DB().op_insert(relatedDocs, new List<Schema.SObjectField> {});

            // add pre-existing PDT for rDoc2 without assessment
            SQX_Personnel_Document_Training__c pdt_rDoc2_1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );

            // add pre-existing PDT for rDoc2 but with completed assessment
            SQX_Personnel_Document_Training__c pdt_rDoc2_2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc2.Id,
                SQX_Assessment__c = [SELECT Id FROM SQX_Assessment__c WHERE Name = 'COMPLETE_ASSESSMENT'].Id,
                SQX_Personnel_Assessment__c = [SELECT Id FROM SQX_Personnel_Assessment__c LIMIT 1].Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING
            );

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt_rDoc2_1, pdt_rDoc2_2 }, new List<Schema.SObjectField>{});

            // preparing PDT for course
            pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                SQX_Trainer__c = adminUser.Id
            );

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt1 }, new List<Schema.SObjectField>{});
        }

        System.runAs(adminUser)
        {
            // ACT : trainer-sign off the document(course)
            pdt1.Trainer_SignOff_Date__c = System.today();
            pdt1.Completion_Date__c = System.today();
            pdt1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;

            Test.startTest();

            new SQX_DB().op_update(new List<SQX_Personnel_Document_Training__c> { pdt1 }, new List<Schema.SObjectField>{});

        }

        System.runAs(standardUser)
        {

            // ASSERT : Completed DT is created for rDoc1 and existing DT for rDoc2 has been completed as well
            List<SQX_Personnel_Document_Training__c> dts =  [SELECT Id,Level_of_Competency__c,SQX_Controlled_Document__c FROM SQX_Personnel_Document_Training__c
                                                                WHERE SQX_Personnel__c =: psn.Id
                                                                AND Status__c =: SQX_Personnel_Document_Training.STATUS_COMPLETE
                                                                AND Trainer_SignOff_Date__c != null
                                                                AND Id !=: pdt1.Id];

            Test.stopTest();

            // expecting exactly three PDTs ( 1 for rDoc1, 1 for rDoc2(updated doc), 1 for rDoc3)
            System.assertEquals(3, dts.size());

            Map<Id, Integer> pdtcount = new Map<Id, Integer>{
                rDoc1.Id => 0,
                rDoc2.Id => 0,
                rDoc3.Id => 0
            };
            for(SQX_Personnel_Document_Training__c dt : dts)
            {
                if(dt.SQX_Controlled_Document__c == rDoc2.Id)
                {
                    // expected level of competency to be upgraded
                    System.assertEquals(dt.Level_of_Competency__c, pdt1.Level_of_Competency__c);
                    pdtCount.put(rDoc2.Id, pdtCount.get(rDoc2.Id) + 1);
                }
                else if(dt.SQX_Controlled_Document__c == rDoc1.Id)
                {
                    pdtCount.put(rDoc1.Id, pdtCount.get(rDoc1.Id) + 1);
                }
                else if(dt.SQX_Controlled_Document__c == rDoc3.Id)
                {
                    pdtCount.put(rDoc3.Id, pdtCount.get(rDoc3.Id) + 1);
                }
                else
                {
                    System.assert(false, 'Unexpected PDT ' + dt);
                }
            }

            // validating that each related docs have exactly 1 complete PDT
            for(String key : pdtCount.keySet())
            {
                System.assertEquals(1, pdtCount.get(key), 'Unexpected number of PDTs');
            }
        }

    }


    static testmethod void givenCourseWithAssessment_WhenCourseIsCompleted_ThenPDTsForRelatedDocumentsAreCreatedOrUpdatedBasedOnAssessment(){

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER);
        User standardUser = users.get(STANDARD_USER);

        SQX_Personnel_Document_Training__c pdt1;
        SQX_Personnel__c psn;
        SQX_Controlled_Document__c rDoc1,rDoc2;

        String completedAssessmentId = [SELECT Id FROM SQX_Assessment__c WHERE Name = 'COMPLETE_ASSESSMENT'].Id;

        System.runAs(standardUser)
        {
            psn = getPersonnel(standardUser.Id);

            // Arrange : create a course with related documents and a pdt for the course with an assessment
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                 .save();

            // preparing PDT for course with assessment
            pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Assessment__c = completedAssessmentId,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                SQX_Trainer__c = adminUser.Id
            );

            List<SQX_Controlled_Document__c> rDocs = getRelatedDocuments();

            rDoc1 = rDocs.get(0);
            rDoc2 = rDocs.get(1);

            List<SQX_Related_Document__c> relatedDocs = new List<SQX_Related_Document__c>();
            for(SQX_Controlled_Document__c rDoc : rDocs)
            {
                 relatedDocs.add(new SQX_Related_Document__c(
                    Controlled_Document__c = cDoc1.doc.Id,
                    Referenced_Document__c = rDoc.Id,
                    Specific_Rev__c = false,
                    Fulfilled_Requirement__c = true
                ));
            }

            new SQX_DB().op_insert(relatedDocs, new List<Schema.SObjectField> {});

            // add pre-existing PDT for rDoc1 with some other assessment
            SQX_Personnel_Document_Training__c pdt_rDoc1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc1.Id,
                SQX_Assessment__c = [SELECT Id FROM SQX_Assessment__c WHERE Name = 'PENDING_ASSESSMENT'].Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );

            // add pre-existing PDT for rDoc2 but with same assessment
            SQX_Personnel_Document_Training__c pdt_rDoc2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.Id,
                SQX_Controlled_Document__c = rDoc2.Id,
                SQX_Assessment__c = completedAssessmentId,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt_rDoc1, pdt_rDoc2 }, new List<Schema.SObjectField>{});
        }

        System.runAs(adminUser)
        {
            // ACT : Trainer-sign off the document(course) and also link completed assessment
            pdt1.Trainer_SignOff_Date__c = System.today();
            pdt1.Completion_Date__c = System.today();
            pdt1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdt1.SQX_Personnel_Assessment__c = [SELECT Id FROM SQX_Personnel_Assessment__c LIMIT 1].Id;

            Test.startTest();

            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> { pdt1 }, new List<Schema.SObjectField>{});
        }

        System.runAs(standardUser)
        {
            // Assert : New pdt has to be created for rdoc1 and rdoc3 and existing pdt of rdoc2 has to be completed
            List<SQX_Personnel_Document_Training__c> pdts = [SELECT Id, SQX_Controlled_Document__c
                                                                    FROM SQX_Personnel_Document_Training__c
                                                                    WHERE SQX_Personnel__c =: psn.Id
                                                                    AND Status__c =: SQX_Personnel_Document_Training.STATUS_COMPLETE
                                                                    AND SQX_Assessment__c =: completedAssessmentId
                                                                    AND Trainer_SignOff_Date__c != null
                                                                    AND Id !=: pdt1.Id];

            System.assertEquals(3, pdts.size());

        }
    }

    /**
     * Given : document1 with pdt for personnel say psn, document2. document1 has related document record with Fulfilled Requirement checked referencing document2.
     * When : Personnel psn tries to user sing off the pdt
     * Then : User sign off should be successful and one complete training for document2 should be created.
     *
     * @author : Paras BK
     * @story : [SQX-5136]
     */
    static testmethod void givenDocumentTrainingOnACourseWithFulfilledRequirements_WhenPersonnelUserTriesToUserSignOff_ItShsoldBeSuccessful() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN_USER);
        User standardUser2 = users.get(STANDARD_USER_2);

        SQX_Personnel_Document_Training__c pdt1;
        SQX_Test_Personnel psn;
        SQX_Test_Controlled_Document document2;
        System.runAs(adminUser) {

            // add required personnel
            psn = new SQX_Test_Personnel(standardUser2);
            psn.save();
            
            // add document and related doc
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            document2 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            List<SQX_Related_Document__c> relatedDocs = new List<SQX_Related_Document__c>{
                new SQX_Related_Document__c(
                    Controlled_Document__c = document1.doc.Id,
                    Referenced_Document__c = document2.doc.Id,
                    Specific_Rev__c = false,
                    Fulfilled_Requirement__c = true
                )
            };

            insert relatedDocs;

            // preparing PDT for course
            pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn.mainRecord.Id,
                SQX_Controlled_Document__c = document1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false
            );

            insert pdt1;
        }

        // being standard user try to user sign off pdt
        System.runAs(standardUser2) {
            
            // ACT : user-sign off the document(course)
            pdt1.User_SignOff_Date__c = System.today();
            pdt1.Completion_Date__c = System.today();
            pdt1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            
            // using without sharing because standardUser2 does not have edit access to pdt1
            new SQX_DB().withoutSharing().op_update(new List<SQX_Personnel_Document_Training__c>{ pdt1}, new List<Schema.SObjectField>{});

            // ASSERT : Completed DT is created for rDoc1
            List<SQX_Personnel_Document_Training__c> dts = [ SELECT Id 
                                                             FROM SQX_Personnel_Document_Training__c
                                                             WHERE SQX_Personnel__c =: psn.mainRecord.Id
                                                                AND Status__c =: SQX_Personnel_Document_Training.STATUS_COMPLETE
                                                                AND SQX_Controlled_Document__c =: document2.doc.Id];

            // Assert : ensure one complete pdt for rDoc1 is created
            System.assertEquals(1, dts.size());
        }
    }
}