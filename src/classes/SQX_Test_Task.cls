/**
* This is a test factory clas that creates test objects for task type and also allows addition of answers, questions to the task.
*/
@IsTest
public class SQX_Test_Task {
	
    private static integer taskCount = 1, questionCount = 1, optionCount = 1;
    public SQX_Task__c task;
 
    /**
    * Constructor for creating a task object based on the details provided.
    * @param recordType The type of task record that is to be created, the list of values are defined in SQX_Task class
    * @param taskType The type of task, values for this are also defined in SQX_Task class
    * @param allowedDays the number of days that is provided for performing the task
    */   
    public SQX_Test_Task(String recordType, String taskType, Integer allowedDays){
        String name = 'Task-' + taskCount++;

        Map<String, Map<String, Id>> recordTypeMap = SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new List<String>{SQX.StepLibrary});
            
        Id complaintTask = recordTypeMap.get(SQX.StepLibrary).get( SQX_Task.RECORD_TYPE_COMPLAINT_MANAGEMENT_STEP);
        Id supplierTask = recordTypeMap.get(SQX.StepLibrary).get( SQX_Task.RECORD_TYPE_SUPPLIER_MANAGEMENT_STEP);
        
        task = new SQX_Task__c(
            Record_Type__c = recordType,
            Task_Type__c = taskType,
            Allowed_Days__c = allowedDays,
            Active__c = true,
            Step__c = 1,
            Name = name
        );

        if(recordType == SQX_Task.RTYPE_COMPLAINT){
            task.RecordTypeId = complaintTask;
        } else {
            task.RecordTypeId = supplierTask;
        }
    }
    
    /**
    * Constructor overload which internall calls the longer overload with allowedDays set to 10 days.
    */
    public SQX_Test_Task(String recordType, String taskType){
        this(recordType, taskType, 10);
    }
   
    
    /**
    * Saves the task object i.e. Inserts the task if it wasn't saved earlier, updates the task if it was saved earlier.
    */
    public SQX_Test_Task save(){
        if(task.Id == null){
            new SQX_DB().op_insert(new List<SQX_Task__c> { this.task }, new List<Schema.SObjectField> { 
                SQX_Task__c.Name, SQX_Task__c.Record_Type__c, SQX_Task__c.Task_Type__c, SQX_Task__c.Allowed_Days__c });
        }
        else {
			new SQX_DB().op_update(new List<SQX_Task__c> { this.task }, new List<Schema.SObjectField> { 
                SQX_Task__c.Name, SQX_Task__c.Record_Type__c, SQX_Task__c.Task_Type__c, SQX_Task__c.Allowed_Days__c });            
        }
        return this;
    }
    

    private static Id BasicQuestionTypeId = null; //cache the record type ids because we want to reuse it.
    private static Id DynamicQuestionTypeId = null;    

    /**
    * Adds a question to this particular instance of task. Please ensure that the task is saved before calling this method.
    * @return returns the newly created question that was added to the task.
    */
    public SQX_Task_Question__c addQuestion(){
        SQX_Task_Question__c question = new SQX_Task_Question__c(
            SQX_Task__c = this.task.Id,
            QuestionText__c = 'Blob',
            Name = this.task.Name + '-QUE-' + SQX_Test_Task.questionCount
        );

	//if task type is complaint's decision tree add dynamic question type, else add simple question type
        if(task.Record_Type__c == SQX_Task.RTYPE_COMPLAINT && task.Task_Type__c == SQX_Task.TYPE_DECISION_TREE){
            if(DynamicQuestionTypeId == null)
                DynamicQuestionTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX_Task_Question__c.getSObjectType() + '', 'Dynamic_Question');
            
            question.RecordTypeId = DynamicQuestionTypeId;
        }
        else{
            if(BasicQuestionTypeId == null)
                BasicQuestionTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX_Task_Question__c.getSObjectType() + '', 'Simple_Question');
            
            question.RecordTypeId = BasicQuestionTypeId;
        }
        
        SQX_Test_Task.questionCount++;

        new SQX_DB().op_insert(new List<SObject> { question }, new List<Schema.SObjectField> {
            SQX_Task_Question__c.SQX_Task__c,
            SQX_Task_Question__c.QuestionText__c,
            SQX_Task_Question__c.Name
        });
        
        return question;
    }
    
    
    /**
    * Adds an answer to the question that is being passed as a param
    * @param question the question whose answer option is to be added
    * @param nextQuestion the question which will be subsequently asked for selecting the answer option
    * @param reportable indicates whether or not selecting this option requires user to submit a report. Note: Reporting detail fields are added to the option.
    * @return returns the answer option that was recently added.
    */
    public SQX_Answer_Option__c addAnswer(SQX_Task_Question__c question, SQX_Task_Question__c nextQuestion, Boolean reportable){
        SQX_Answer_Option__c answer = new SQX_Answer_Option__c(
            Name = question.Name + '-OPT-' + SQX_Test_Task.optionCount,
            Reportable__c = reportable,
            Question__c = question.Id,
            Option_Value__c = 'OPT-' + SQX_Test_Task.optionCount
        );
        
        SQX_Test_Task.optionCount++;
        
        if(reportable){
            answer.RegulatoryBody__c = 'FDA';
            answer.Report_Name__c = 'Random Report';
            answer.Due_In_Days__c = 10;
        }
        
        if(nextQuestion != null){
            answer.Next_Question__c = nextQuestion.Id;
        }
        
        new SQX_DB().op_insert(new List<SObject> { answer }, new List<Schema.SObjectField> {
            SQX_Answer_Option__c.Reportable__c,
            SQX_Answer_Option__c.Next_Question__c,
            SQX_Answer_Option__c.Name,
            SQX_Answer_Option__c.Question__c
        });
        
        return answer;
    }
    
}
