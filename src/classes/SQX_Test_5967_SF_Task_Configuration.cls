/**
 * test class for salesforce task configuration
 */
@isTest
public class SQX_Test_5967_SF_Task_Configuration {


    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
    }

    /**
     * GIVEN : An onboarding step of NSI
     * WHEN : onboarding step is open
     * THEN : task is created with the configuration that was setup
     * @story : SQX-5967
     */
    public static testMethod void givenOnboardingStep_WhenStepIsOpen_TaskIsCreatedWithNecessaryConfiguration(){
        
        // ARRANGE : Onboarding step is created
        SQX_Test_NSI nsi = new SQX_Test_NSI();
        nsi.save();
        
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = : SQX_Steps_Trigger_Handler.RT_TASK AND SObjectType =: SQX.OnboardingStep];
        List<SQX_Onboarding_Step__c> wfPolicyList = new List<SQX_Onboarding_Step__c>();

        for(Integer i = 0; i < 200; i++){
            SQX_Onboarding_Step__c wfPolicy = new SQX_Onboarding_Step__c(Name='Test_Onboarding_Policy_' + i, 
                                                                        Step__c = 1, 
                                                                        SQX_Parent__c = nsi.nsi.Id, 
                                                                        Due_Date__c = Date.today().addDays(10),
                                                                        RecordTypeId = rt.Id,
                                                                        Status__c = SQX_Steps_Trigger_Handler.STATUS_DRAFT,
                                                                        SQX_User__c = UserInfo.getUserId());

            wfPolicyList.add(wfPolicy);
        }
        
        insert wfPolicyList;
        
        // ACT : Onboarding step is opened    
        SQX_Onboarding_Step__c  wfPolicy = wfPolicyList.get(0);    
        wfPolicy.Status__c = SQX_Steps_Trigger_Handler.STATUS_OPEN;
        update wfPolicy;
        
        // ASSERT : Task is created with the configuration that was setup
        System.assertEquals('Test_Onboarding_Policy_0', [SELECT Id, Name FROM SQX_Onboarding_Step__c WHERE Id = : wfPolicy.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].Name);
        System.assertEquals(1, [SELECT Id, Subject FROM Task WHERE Child_What_Id__c = : wfPolicy.Id AND WhatID = :nsi.nsi.Id AND Subject = 'Test_Onboarding_Policy_0'].size());
    }
}