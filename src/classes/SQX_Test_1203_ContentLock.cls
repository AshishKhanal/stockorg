/**
*/
@IsTest
public class SQX_Test_1203_ContentLock {



	/*Documents have to be added while creating Controlled Document 
  So while creating a new document, content has to be provided 
Document Number/Rev fields in Content may not be changed 
Content cannot be changed if user is not authorized to edit the related Controllled Document 
Content cannot be changed if document is not in 'Draft' state 
When a document is released then it moves to the release vault identified in the controlled document 
When a document is Obsoleted, it is archived in the release vault 
Once a document is checked in, user cannot change the internally stored reference to content 
Content in library cannot be deleted directly. It will be deleted when corresponding document is deleted*/

	private static boolean 	run_allTests = true,
							run_givenControlledDocIsObsolete_ContentIsArchived = false,
							run_givenContentVersionDocIsChanged_ItFails = false,
							run_givenControlledDocumentIsCreated_ContentIsCreated = false,
							run_givenControlledDocIsDeleted_ContentIsDeleted = false;


	/**
	* Ensures that when document is obsoleted content is archived
	*/
	public static testmethod void givenControlledDocIsObsolete_ContentIsArchived(){
    	if(!run_allTests && !run_givenControlledDocIsObsolete_ContentIsArchived)
    		return;

		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

		System.runAs(standardUser){
		
			//Arrange: create a controlled document
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			integer randomNumber = (Integer)( Math.random() * 1000000 );
			doc.Document_Number__c = 'Doc' + randomNumber;
			doc.Revision__c = 'REV-1';
			doc.Title__c = 'Document Title ' + randomNumber;
			doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();

			extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';

			extension.create();

			doc.Next_Review_Date__c = Date.today().addDays(25);
	    	doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

	    	new SQX_DB().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
	    			Schema.SQX_Controlled_Document__c.Next_Review_Date__c,
	    			Schema.SQX_Controlled_Document__c.Document_Status__c
			});

	    	//Act: Obsolete the doc

	    	doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
	    	new SQX_DB().continueOnError().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
	    			Schema.SQX_Controlled_Document__c.Document_Status__c
			});


	    	//Assert: Assert that the doc is archived
	    	doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = :doc.Id];

	    	System.assertEquals(0, [SELECT Id, IsArchived FROM ContentDocument WHERE Id = : doc.Content_Reference__c].size());


    	}

	}


	/**
	* this test ensures that the Content doc's controlled document can't be changed
	*/
	public static testmethod void givenContentVersionDocIsChanged_ItFails(){
    	if(!run_allTests && !run_givenContentVersionDocIsChanged_ItFails)
    		return;

		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

		System.runAs(standardUser){
		
			//Arrange: create a controlled document
			Id controlledDocTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor('ContentVersion', SQX_ContentVersion.RT_CONTROLLED_DOCUMENT);
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			integer randomNumber = (Integer)( Math.random() * 1000000 );
			doc.Document_Number__c = 'Doc' + randomNumber;
			doc.Revision__c = 'REV-1';
			doc.Title__c = 'Document Title ' + randomNumber;
			doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();

			extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';

			extension.create();

			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = :doc.Id];
	    	ContentDocument cdoc = [SELECT Id, LatestPublishedVersionId, IsArchived FROM ContentDocument WHERE Id = :doc.Content_Reference__c];


			ContentVersion version = [SELECT Id, RecordTypeId FROM ContentVersion WHERE Id = : cdoc.LatestPublishedVersionId];

			System.assertEquals(controlledDocTypeId, version.RecordTypeId);

			SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save(true);

            //Act: try changing the controlled document of the doc
            version.Controlled_Document__c = doc2.doc.Id; //should result in an error
            Database.SaveResult result = Database.update(version, false);

            //Assert: changes weren't saved and there was a failure
            System.assertEquals(false, result.isSuccess());
		}
	}


    /**
    * SQX-1203 : "Documents have to be added while creating Controlled document"
    * This test ensures that content document are created when controlled document is created.
    */
    public static testmethod void givenControlledDocumentIsCreated_ContentIsCreated(){
    	if(!run_allTests && !run_givenControlledDocumentIsCreated_ContentIsCreated)
    		return;

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			integer randomNumber = (Integer)( Math.random() * 1000000 );
			doc.Document_Number__c = 'Doc' + randomNumber;
			doc.Revision__c = 'REV-1';
			doc.Title__c = 'Document Title ' + randomNumber;
			doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();

			extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';

			//Act: create the document
			extension.create();

			//ASsert: content doc is created
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = :doc.Id];
			System.assertNotEquals(null, doc.Content_Reference__c);

			System.assertEquals(1, [SELECT Id, LatestPublishedVersionId, IsArchived FROM ContentDocument WHERE Id = :doc.Content_Reference__c].size());


        }
    }

    /**
    * Given: Two New Controlled Document 'document1 & document2'
    * When :  The document is deleted
    * Then : Its primary and secondary content is also deleted
    *
    * @author:  Sajal Joshi
    * @date: 2016/8/23
    */
    public static testmethod void givenControlledDocIsDeleted_ContentIsDeleted(){
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runAs(standardUser){
            
            //Arrange: create two controlled document
            //document1 with only primary content 
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.updateContent(true, 'Primary');
            document1.save(true);
            
            //document2 with both primary and secondary content
            SQX_Test_Controlled_Document document2 = new SQX_Test_Controlled_Document();
            document2.updateContent(false, 'Secondary');
            document2.save(true);
            
            document1.doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE id=: document1.doc.Id ];
            document2.doc = [SELECT Id, Content_Reference__c, Secondary_Content_Reference__c FROM SQX_Controlled_Document__c WHERE id=: document2.doc.Id ];
            
            String primaryContent1 = document1.doc.Content_Reference__c;
            String primaryContent2 = document2.doc.Content_Reference__c;
            String secondaryContent2 = document2.doc.Secondary_Content_Reference__c;
            
            //Act: Delete document1 controlled document
            delete document1.doc;
            
            //Assert: Controlled Document and its primary content is deleted
            SQX_Controlled_Document__c doc1 = [SELECT Id, isDeleted FROM SQX_Controlled_Document__c WHERE id=: document1.doc.Id ALL ROWS];
            System.assertEquals(true, doc1.IsDeleted);
            ContentDocument cd1 = [SELECT Id, isDeleted FROM ContentDocument WHERE id=: primaryContent1 ALL ROWS];            
            System.assertEquals(true, cd1.isDeleted);
            
            //Act: Delete document2 controlled document
            delete document2.doc;
            
            //Assert: Controlled Document document2 and its primary and secondary content is deleted
            SQX_Controlled_Document__c doc2 = [SELECT Id, isDeleted FROM SQX_Controlled_Document__c WHERE id=: document2.doc.Id ALL ROWS];
            System.assertEquals(true, doc2.IsDeleted);
            ContentDocument cd2 = [SELECT Id, isDeleted FROM ContentDocument WHERE id=: primaryContent2 ALL ROWS]; 
            System.assertEquals(true, cd2.IsDeleted);
            ContentDocument cd3 = [SELECT Id, isDeleted FROM ContentDocument WHERE id=: secondaryContent2 ALL ROWS];            
            System.assertEquals(true, cd3.isDeleted);

        }

	}

    /**
     * GIVEN : Controlled document content
     * WHEN : Content is archived
     * THEN : Error is thrown
     */
    static testmethod void givenControlledDocContent_WhenContentIsArchived_ThenErrorIsThrown(){

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runAs(standardUser){
            // ARRANGE : Controlled Document With Content
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document().save(true).synchronize();

            // ACT : Archive the content
            ContentDocument cd = new ContentDocument(Id = tcd.doc.Content_Reference__c);
            cd.IsArchived = true;
            Test.startTest();
            Database.SaveResult sr = new SQX_DB().continueOnError().op_update(new List<ContentDocument> { cd }, new List<SObjectField> {}).get(0);
            Test.stopTest();

            // ASSERT : Error is thrown
            System.assertEquals(false, sr.isSuccess(), 'Expected archival to fail');
            System.assertEquals(Label.SQX_ERR_MSG_CONTROLLED_DOC_CONTENT_CANNOT_BE_ARCHIVED, sr.getErrors().get(0).getMessage());
        }
    }

    /**
     * GIVEN : Controlled document content
     * WHEN : Content is deleted
     * THEN : Error is thrown
     */
    static testmethod void givenControlledDocContent_WhenContentIsDeleted_ThenErrorIsThrown(){

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runAs(standardUser){
            // ARRANGE : Controlled Document With Content
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document().save(true).synchronize();

            // ACT : Archive the content
            ContentDocument cd = new ContentDocument(Id = tcd.doc.Content_Reference__c);
            Test.startTest();
            Database.DeleteResult sr = new SQX_DB().continueOnError().op_delete(new List<ContentDocument> { cd }).get(0);
            Test.stopTest();

            // ASSERT : Error is thrown
            System.assertEquals(false, sr.isSuccess(), 'Expected archival to fail');
            System.assertEquals(Label.SQX_ERR_MSG_CONTROLLED_DOC_CONTENT_CANNOT_BE_DELETED, sr.getErrors().get(0).getMessage());
        }
    }

}