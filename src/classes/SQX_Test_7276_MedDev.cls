/**
 * Test class for MedDev records
 */ 
@isTest
public class SQX_Test_7276_MedDev {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
     * GIVEN: MedDev record with Status of submitter as Authorised Representative
     * WHEN: Authorised Representative details is not provided
     * THEN: Validation rule for Authorised Representative should restrict to save MedDev records
     */ 
    public static testMethod void givenMedDevRecordWithSubmitterAsAuthRep_WhenAuthRepDetailsIsNotProvided_ThenMedDevRecordShouldNotBeCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create MedDev records with save results
            Map<SQX_MedDev__c, Boolean> medDevWithSaveResults = new Map<SQX_MedDev__c, Boolean>{
                // Without Authorised Representative Name
                	new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative Contact Name
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative Address
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative Postcode
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative City
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative Phone
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol',  
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative E-mail
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB'
                    ) => false,
                        // Without Authorised Representative Country
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345'
                    ) => false,
                        // With all information of Authorised Representative
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info__c = 'Authorised Representative', 
                        AuthRepDetailsMan_ARAddress__c = 'London', 
                        AuthRepDetailsMan_ARCity__c = 'Manchester', 
                        AuthRepDetailsMan_ARContactPerson__c = 'John Smith', 
                        AuthRepDetailsMan_AREmailAddress__c = 'steve@gmail.com', 
                        AuthRepDetailsMan_ARName__c = 'Steve Carrol', 
                        AuthRepDetailsMan_ARPhone__c = '1234567890', 
                        AuthRepDetailsMan_ARPostCode__c = '12345', 
                        AuthRepDetailsMan_ARCountry__c = 'GB',
                        Admin_Info_ReportType__c = 'Initial', 
                        MfrInvestigation_MfrInitialCorrecAction__c = 'Initial corrective actions/preventive actions implemented by the manufacturer',
                        MfrInvestigation_MfrPrelimAnalysis__c='Manufacturers preliminary analysis',
                        Admin_Info_NCAName__c = 'Test NCA Name',
                        Admin_Info_NCAAddress__c = 'Test Address',
                        Admin_Info_ReportDate__c = Date.Today(),
                        Admin_Info_MfrInternalNo__c = 'Num 12345',
                        Admin_Info_Spht__c = 'Yes',
                        Admin_Info_EventClassification__c = 'Death',
                        Device_Info_DeviceClass__c = 'MDD Class III',
                        Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                        Admin_Info_MfrAwarenessDate__c = Date.Today(),
                        MfrDetails_MfrName__c = 'Test Manufacturer Name',
                        MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                        MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                        MfrDetails_MfrPostcode__c = 'BS 4519',
                        MfrDetails_MfrCity__c = 'Test City',
                        MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                        MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                        MfrDetails_MfrCountry__c = 'CA',
                        ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                        ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                        ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                        ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                        ReporterDetails_ReporterOrgCity__c = 'Test City',
                        ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                        ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                        ReporterDetails_ReporterOrgCountry__c = 'CA'
                    ) => true,
                        // With submitter as Manufacturer
                    new SQX_MedDev__c(
                        Admin_Info_ReportType__c = 'Initial', 
                        MfrInvestigation_MfrInitialCorrecAction__c = 'Initial corrective actions/preventive actions implemented by the manufacturer',
                        MfrInvestigation_MfrPrelimAnalysis__c='Manufacturers preliminary analysis',
                        Admin_Info_NCAName__c = 'Test NCA Name',
                        Admin_Info_NCAAddress__c = 'Test Address',
                        Admin_Info_ReportDate__c = Date.Today(),
                        Admin_Info_MfrInternalNo__c = 'Num 12345',
                        Admin_Info_Spht__c = 'Yes',
                        Admin_Info_EventClassification__c = 'Death',
                        Contact_Info__c = 'Manufacturer',
                        Device_Info_DeviceClass__c = 'MDD Class III',
                        Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                        Admin_Info_MfrAwarenessDate__c = Date.Today(),
                        MfrDetails_MfrName__c = 'Test Manufacturer Name',
                        MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                        MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                        MfrDetails_MfrPostcode__c = 'BS 4519',
                        MfrDetails_MfrCity__c = 'Test City',
                        MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                        MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                        MfrDetails_MfrCountry__c = 'CA',
                        ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                        ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                        ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                        ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                        ReporterDetails_ReporterOrgCity__c = 'Test City',
                        ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                        ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                        ReporterDetails_ReporterOrgCountry__c = 'CA') => true
            };
            
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssertMedDevRecords(medDevWithSaveResults);
        }
    }

     /**
    * GIVEN: MedDev record.
    * WHEN: Values to related fields are not provided.
    * THEN: The records should not be allowed to be saved.
    */ 
    public static testmethod void givenMeddevRecord_WhenValuesToRelatedFieldsAreNotProvided_RecordShouldNotBeAllowedToBeSaved(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create MedDev records with save results
            Map<SQX_MedDev__c, Boolean> medDevWithSaveResults = new Map<SQX_MedDev__c, Boolean>{
                
                    // Arrange:Countries sim events is required if mfr awareness sim events is set to Yes.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Other_Reporting_Info_CountriesSimEvents__c = '',
                        MfrInvestigation_MfrAwareSimEvents__c='Yes'
                    ) => false,
                    
                    // Arrange:Device operator other should be entered if device operator at event is set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Clinical_Event_Info_DeviceOperatorOther__c = '',
                        Clinical_Event_Info_DeviceOperatrAtEvent__c='Other'
                    ) => false,
                    
                    // Arrange:Device operator other should not be entered if device operator at event is not set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Clinical_Event_Info_DeviceOperatorOther__c = 'device operator other information',
                        Clinical_Event_Info_DeviceOperatrAtEvent__c='healthcare professional'
                    ) => false,
                    
                    // Arrange:Device usage other should be entered if usage operator at event is set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Device_Info_DeviceUsageOther__c = '',
                        Device_Info_DeviceUsage__c='Other'
                    ) => false,
                    
                    // Arrange:Device usage other should not be entered if usage operator at event is not set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Device_Info_DeviceUsageOther__c = 'device other usage information',
                        Device_Info_DeviceUsage__c='initial use'
                    ) => false,
                    
                    // Arrange:If report type is set to Final or follow-up then report number should be set.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Admin_Info_NCAReportNo__c = '',
                        Admin_Info_ReportType__c='Final'
                    ) => false,
                    
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Admin_Info_NCAReportNo__c = '',
                        Admin_Info_ReportType__c='Follow-up'
                    ) => false,
                    
                    // Arrange: Contact info reporter other should be entered if contact info is set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info_ReporterOtherText__c = '',
                        Contact_Info__c='Others'
                    ) => false,
                    
                    //  Arrange: contact info reporter other should not be entered if contact info is not set to other.
                    new SQX_MedDev__c(
                        Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                        Contact_Info_ReporterOtherText__c = 'Reporter Other Text',
                        Contact_Info__c='Manufacturer'
                    ) => false
               };
              
              // Assert: ensure that test results match with expected data when saving
              actAndAssertMedDevRecords(medDevWithSaveResults);
        }
    }
    
    /**
     * Helper method to save (act) and assert MedDev records
     */ 
    private static void actAndAssertMedDevRecords(Map<SQX_MedDev__c, Boolean>  data) {
        
        // ACT: Insert the records
        SQX_MedDev__c[] medDevs = new List<SQX_MedDev__c>(data.keySet());
        Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(medDevs, new List<Schema.SObjectField>{});
        data = data.clone();
        
        // Assert: Ensure that result matches
        for(Integer i = 0; i < saveResults.size(); i++) {
            SQX_MedDev__c medDev = medDevs[i];
            Database.SaveResult result = saveResults[i];
            
            System.assertEquals(data.get(medDev), result.isSuccess(), 'Failing on ' + medDev +  'Actual Result: ' + result);
        }
    }
}