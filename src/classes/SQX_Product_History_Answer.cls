/**
* Used to manipulate product history checklist object
*/
public with sharing class SQX_Product_History_Answer {
    /**
    * PHA Trigger Handler performs actions when PHA object is being manipulated
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newPHAs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Product_History_Answer__c> newPHAs, Map<Id,SQX_Product_History_Answer__c> oldMap){
            
            super(newPHAs, oldMap);
        }

        /**
        * This method prevent to delete product history checklist records
        */
        public Bulkified preventPHRChecklistDeletion(){
            this.resetView();   
            for(SQX_Product_History_Answer__c pha : (List<SQX_Product_History_Answer__c>) this.view) {
                pha.addError(Label.SQX_ERR_MSG_CANNOT_DELETE_RECORD);
            }
            
            return this;
        }
    }
}