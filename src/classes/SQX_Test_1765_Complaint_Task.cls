/**
* Following tests are performed by the test class
* a. If a new complaint is created, default tasks are created from task library
*    i. Task will include duplicate task i.e. similar task with multiple task type
*    ii. Task with/without default assignee
*    iii. Task with/without number of days allowed
*
* b. If a complaint is initiated, tasks are converted into SF tasks
*    i.  If not Open SF task is not created
*    ii. If not Open SF task is not created when fiddling with applicable flag.
*    iii. Task is created only if Policy is applicable
*    iv. Once initiated if Policy is changed to applicable task is created
*
* c. Validation Rules in task library
*    i. Allowed_Days can't be negative ??
*
* d. Validation Rules in task library
*    i. Assignee can't be null. ??
*/
@IsTest
public class SQX_Test_1765_Complaint_Task{

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                         Active__c = true,
                                                         Defect_Category__C = 'Test_Category',
                                                         Description__c = 'Test Description',
                                                         Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }        
    }

    public static boolean   runAll = true,
                            run_givenComplaintIsCreated_TasksAreCreatedFromLibrary = false,
                            run_givenComplaintIsInitiated_TasksAreConvertedToSFTasks = false,
                            run_givenComplaintIsClosed_PendingTasksDeleted= false;

    /**
    * This test ensures that if a new complaint is created, tasks are defaulted from the task library
    *           and a. If duplicate task have been created they wiil be created
    *               b. Task with default assignee have default assignee copied over
    *               c. Task without default assignee will have default assignee set to the owner/creator
    *               d. Task with number of days allowed will have due date = Creation Date + Number of days allowed
    *               e. Task without number of days allowed will have due date = Creation Date
    */
    public static testmethod void givenComplaintIsCreated_TasksAreCreatedFromLibrary(){
        if(!runAll && !run_givenComplaintIsCreated_TasksAreCreatedFromLibrary)
            return;

        //Arrange: Create 4 tasks in task library related to complaint
        //         Task 1 has no default assignee and allowed days
        //         Task 2 has default assignee
        //         Task 3 has allowed days
        //         Task 4 has both default assignee and allowed days.
        //         
        //         Task 1 and Task 2 are of same type 
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_Task_Library task1, task2, task3, task4;
        
        System.runAs(adminUser){
            task1 = new SQX_Test_Task_Library();
            task1.task.SQX_User__c = standardUser.Id;
            task1.save();
            
            task2 = new SQX_Test_Task_Library();
            task2.task.SQX_User__c = standardUser2.Id;
            task2.save();
            
            task3 = new SQX_Test_Task_Library(SQX_Test_Task_Library.TaskType.ComplaintsInvestigation);
            task3.task.SQX_User__c = standardUser.Id;
            task3.task.Allowed_Days__c = 10;
            task3.save();
            
            task4 = new SQX_Test_Task_Library(SQX_Test_Task_Library.TaskType.ComplaintsSampleRequest);
            task4.task.SQX_User__c = standardUser.Id;
            task4.task.Allowed_Days__c = 5;
            task4.save();
        }
        
        System.runAs(standardUser){
            
            //Act: Save the complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            complaint.initiate();
            
            //Assert: Ensure that 4 policy tasks have been created for the complaint
            List<SQX_Complaint_Task__c> complaintTasks = [SELECT Id, Applicable__c, SQX_User__c, Comment__c, SQX_Complaint__c, Due_Date__c, 
                                                          SQX_Task__c, Task_Description__c
                                                         FROM SQX_Complaint_Task__c
                                                         WHERE SQX_Complaint__c = : complaint.complaint.Id];
            
            System.assertEquals(4, complaintTasks.size(), 'Expected all the tasks in task library to be created for task');
            
            Map<Id, SQX_Complaint_Task__c> task2ComplaintTaskMap = new Map<Id, SQX_Complaint_Task__c>();
            for(SQX_Complaint_Task__c complaintTask : complaintTasks)
                task2ComplaintTaskMap.put(complaintTask.SQX_Task__c, complaintTask);
            
            
            //Assert: Ensure that task without assignee and allowed days has assignee set to creator and due date set to today. 
            SQX_Complaint_Task__c complaintTask = task2ComplaintTaskMap.get(task1.task.Id);
            System.assertEquals(Date.today(), complaintTask.Due_Date__c);
            System.assertEquals(standardUser.Id, complaintTask.SQX_User__c);
            
            //Assert: Ensure that task with default assignee and no allowed days has assignee set to default assignee and due date set to today
            complaintTask = task2ComplaintTaskMap.get(task2.task.Id);
            System.assertEquals(Date.today(), complaintTask.Due_Date__c);
            System.assertEquals(task2.task.SQX_User__c, complaintTask.SQX_User__c);
            
            //Assert: Ensure that task with allowed days and no default assignee has assignee set to creator and due date set to today + allowed days
            complaintTask = task2ComplaintTaskMap.get(task3.task.Id);
            System.assertEquals(Date.today().addDays((Integer)task3.task.Allowed_Days__c), complaintTask.Due_Date__c);
            System.assertEquals(standardUser.Id, complaintTask.SQX_User__c);
            
            //Assert: Ensure that task with allowed days and default assignee has correct assignee and default days
            complaintTask = task2ComplaintTaskMap.get(task4.task.Id);
            System.assertEquals(Date.today().addDays((Integer)task4.task.Allowed_Days__c), complaintTask.Due_Date__c);
            System.assertEquals(task4.task.SQX_User__c, complaintTask.SQX_User__c);
        
        }
        
    }
    
    /**
     * This test ensures that if a new complaint is intiated after it is created, all the complaint tasks(policies) are converted to SF tasks
     *      and a. If not Open SF task is not created
     *          b. If not Open SF task is not created when fiddling with applicable flag.
     *          c. Task is created only if Policy is applicable
     *          d. If policy is changed to not applicable task isn't deleted if the task has been completed
     *          e. Once initiated if Policy is changed to applicable task is created
     *          f. If the applicable flag isn't changed, but policy is updated duplicate task isn't created.
     */
    public static testmethod void givenComplaintIsInitiated_TasksAreConvertedToSFTasks(){
        if(!runAll && !run_givenComplaintIsInitiated_TasksAreConvertedToSFTasks)
            return;
        
        //Arrange: Create a complaint object, using Standard User
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(standardUser){
            
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_DRAFT);
            complaint.save();
            
            //Act: Create three policies two applicable and one not applicable [Case a]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            SQX_Complaint_Task__c policy1 = complaint.addPolicy(false, Date.today(), adminUser.Id),
                                  policy2 = complaint.addPolicy(true, Date.today(), standardUser.Id),
                                  policy3 = complaint.addPolicy(false, Date.today(), standardUser.Id);
            
            Set<Id> policyIds = new Set<Id> { policy1.Id, policy2.Id, policy3.Id };
            
            //Assert: Since complaint is in draft stage ensure that no SF tasks have been created
            System.assertEquals(0, [SELECT Id FROM Task WHERE Child_What_Id__c IN: policyIds].size());
            
            
            
            //Act: Change applicable of policy1, fiddling in draft stage [Case b]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            policy1.Applicable__c = true;
            new SQX_DB().op_update(new List<SObject> {policy1}, new List<SObjectField> { Schema.SQX_Complaint_Task__c.Applicable__c });
            
            //Assert: Ensure that no SF tasks have been created by fiddling with the flag when in draft
            System.assertEquals(0, [SELECT Id FROM Task WHERE Child_What_Id__c IN: policyIds].size());
            
            
            //Act: Open Complaint (Initiate), so tasks should now be created [Case c]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            complaint.initiate();
            
            //Assert: Ensure that SF tasks have been created for all applicable policies
            //since policy 1 and 2 are applicable only two task should exist
            Map<Id, Task> complaintTask2TaskMap = new Map<Id, Task>();
            for(Task task : [SELECT Id, Status, Child_What_Id__c FROM Task WHERE Child_What_Id__c IN: policyIds])
                complaintTask2TaskMap.put(task.Child_What_Id__c, task);
            
            System.assertEquals(2, complaintTask2TaskMap.keySet().size());
            System.assertEquals(true, complaintTask2TaskMap.containsKey(policy1.Id));
            System.assertEquals(true, complaintTask2TaskMap.containsKey(policy2.Id));
            System.assertEquals(false, complaintTask2TaskMap.containsKey(policy3.Id));
            
            //Act: Change policy to not applicable, doesn't delete associate task if it has been started [Case e]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Task policy2sTask = complaintTask2TaskMap.get(policy2.Id);
            policy2sTask.Status = 'Completed';
            update policy2sTask;
            
            policy1.Applicable__c = false;
            new SQX_DB().op_update(new List<SObject> {policy2}, new List<SObjectField> { Schema.SQX_Complaint_Task__c.Applicable__c });
            
            
            //Assert: Ensure that the SF task associated with policy1 is deleted
            System.assertEquals(false, [SELECT Id, IsDeleted FROM Task WHERE Child_What_Id__c = : policy2.Id].IsDeleted);
            
            
            //Act: Change policy to applicable, adds the associated sf task [Case f]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            policy3.Applicable__c = true;
            new SQX_DB().op_update(new List<SObject> {policy3}, new List<SObjectField> { Schema.SQX_Complaint_Task__c.Applicable__c });
            
            //Assert: Ensure that the SF task is created for the newly applicable policy.
            System.assertEquals(1, [SELECT Id FROM Task WHERE Child_What_Id__c = : policy3.Id].size());
            
            
            //Act: Update the policy without any flag changes, shouldn't duplicate task [Case g]
            SQX_BulkifiedBase.clearAllProcessedEntities();
            policy3.Comment__c = 'Test Build';
            new SQX_DB().op_update(new List<SObject> {policy3}, new List<SObjectField> { Schema.SQX_Complaint_Task__c.Name });
            
            //Assert: Ensure that the SF task is not duplicated
            System.assertEquals(1, [SELECT Id FROM Task WHERE Child_What_Id__c = : policy3.Id].size());
            
            
        }
    }    

    /**
     * GIVEN : Complaint with complaint task with blank due date
     * WHEN : Complaint is initiated
     * THEN : Due Date is calculated for complaint task as per allowed days
     */
    static testmethod void givenComplaintWithPolicy_WhenComplaintIsInitiated_DueDateIsCalculated(){
        if(!runAll && !run_givenComplaintIsCreated_TasksAreCreatedFromLibrary)
            return;

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_Task_Library cqTask;
        
        System.runAs(adminUser){
            
            cqTask = new SQX_Test_Task_Library(SQX_Test_Task_Library.TaskType.ComplaintsSampleRequest);
            cqTask.task.SQX_User__c = standardUser.Id;
            cqTask.task.Allowed_Days__c = 5;
            cqTask.save();
        }
        
        System.runAs(standardUser){
            
            // ARRANGE : Complaint is created with complaint task with blank due date
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            //Assert: Ensure that 4 policy tasks have been created for the complaint
            List<SQX_Complaint_Task__c> complaintTasks = [SELECT Id, Applicable__c, SQX_User__c, Comment__c, SQX_Complaint__c, Due_Date__c, 
                                                          SQX_Task__c, Task_Description__c
                                                         FROM SQX_Complaint_Task__c
                                                         WHERE SQX_Complaint__c = : complaint.complaint.Id];
            
            System.assertEquals(1, complaintTasks.size(), 'Expected all the tasks in task library to be created for task');
            SQX_Complaint_Task__c complaintTask = complaintTasks.get(0);
            System.assertEquals(null, complaintTask.Due_Date__c, 'Expected due date to be null');

            // ACT : Complaint is initiated
            complaint.initiate();

            // ASSERT : Due Date is calculated
            complaintTask = [SELECT Id, Due_Date__c FROM SQX_Complaint_Task__c WHERE Id = :complaintTask.Id];
            System.assertEquals(Date.today().addDays(5), complaintTask.Due_Date__c, 'Expected due date to be 5 days from today');
        }
    }

}