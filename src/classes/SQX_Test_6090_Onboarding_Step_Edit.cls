/*
 * Unit test class for onboarding step record that is edited by record owner.
 */ 

@isTest
public class SQX_Test_6090_Onboarding_Step_Edit {
    
    final static String cannotEditErrMsg = 'Onboarding step in open status cannot be edited.';
    final static String onBoardWithOpenAuditErrMsg = 'Onboarding step with open audit cannot be completed.';
    
    @testSetup
    public static void commonSetup() {
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
         
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, adminUser});
        SQX_Test_NSI nsi;
        
        System.runAs(adminUser) {
            //Create policy tasks
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_PERFORM_AUDIT, standardUser, 1);
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, standardUser, 2);
        }
        System.runAs(standardUser) {
            //Create NSI record and take it to in progress stage
            nsi= new SQX_Test_NSI().save();
            nsi.submit();
            nsi.initiate();
        }
    }
    
    /**
     * Given: Onboarding step in draft status,
     * When: Draft onboarding step fields are edited, 
     * Then: record is allowed to be saved.	
     */
    @isTest
    public static void givenOnboardingStepInDraftStatus_WhenUserEditRecord_NoErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: Get onboarding step record with draft status.
            SQX_New_Supplier_Introduction__c nsi = [Select id, Record_stage__c from SQX_New_Supplier_Introduction__c];
            SQX_Onboarding_Step__c draftOnboardingStep = [Select id, name, status__c, result__c , comment__c
                                                          from SQX_Onboarding_Step__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_DRAFT and
                                                          SQX_Parent__c = :nsi.id];
            //Act: Record fields are updated.
            draftOnboardingStep.Comment__c = 'Just a random comment.';
            draftOnboardingStep.Name = 'Ramesh';
            List<Database.SaveResult> draftResultForNameChange =  new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> {draftOnboardingStep}, new List<Schema.SObjectField>{});
            
            //Assert: Make sure record update is success.
            System.assertEquals(true,draftResultForNameChange[0].isSuccess());
        }
    }
    
    /**
     * Given: Onboarding step in open status,
     * When: Open onboarding step fields apart from status, result, applicable, comment are edited, 
     * Then: Validation error is thrown.	
     */
    @isTest
    public static void givenOpenOnboardingStep_WhenUserEditRecord_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            //Arrange: Get onboarding step record with open status
            SQX_New_Supplier_Introduction__c nsi = [Select id, Record_stage__c from SQX_New_Supplier_Introduction__c];
            SQX_Onboarding_Step__c onboardingStep = [Select id, name, status__c, result__c , comment__c
                                                     from SQX_Onboarding_Step__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_OPEN and
                                                     SQX_Parent__c = :nsi.id limit 1];
            //Act: Any allowed field is edited
            onboardingStep.Comment__c = 'Just a random comment.';
            List<Database.SaveResult> resultAgain =  new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> {onboardingStep}, 
                                                                                              new List<Schema.SObjectField>{});
            //Assert: Make sure that fields are updated
            System.assertEquals(true, resultAgain[0].isSuccess());
            
            //Act: try updating field that is not allowed to be modified
            onboardingStep.allowed_days__c = 5;
            List<Database.SaveResult> result =  new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> {onboardingStep}, new List<Schema.SObjectField>{});
            
            //Assert: Validation error message is thrown
            System.assertEquals(cannotEditErrMsg,result[0].getErrors()[0].getMessage());
            
            
        } 
    }
    
    /**
     * Given: Onboarding step in open status,
     * When: Open onboarding step is completed
     * Then: related sf task of onboarding task is also completed
     */
    @isTest
    public static void givenOpenOnboardingStep_WhenOnboardingIsCompleted_RelatedSFTaskIsAlsoCompleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Onboarding_Step__c onboardingStep;
        System.runAs(standardUser){
            //Arrange: Get onboarding step record with open status
            SQX_New_Supplier_Introduction__c nsi = [Select id, Record_stage__c, status__c from SQX_New_Supplier_Introduction__c];

            onboardingStep = [Select id, name, status__c, result__c , comment__c
                                                     from SQX_Onboarding_Step__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_OPEN and
                                                     SQX_Parent__c = :nsi.id];
            onboardingStep.status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            onboardingStep.result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            onboardingStep.comment__c = 'Let us go comment';
            
            //Assert:Make sure task is not completed as onboarding is not completed
            List<Task> taskList = [Select id, status from Task where Child_What_Id__c =:onboardingStep.id];
            System.assertEquals(taskList.size(), 1, 'Expected 1 SF task');
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, taskList[0].status, 'Expected task to be in not started status');
            
            //Act:Update onboarding with complete status
            update onboardingStep;
        
            //Assert: Make sure sf task is completed
         	List<Task> taskListAfter_completingOnboardingStep = [Select id, status, description from Task where Child_What_Id__c =:onboardingStep.id];
            System.assertEquals(taskListAfter_completingOnboardingStep.size(), 1, 'Expected 1 SF task');
            System.assertEquals('Completed', taskListAfter_completingOnboardingStep[0].status);  
            System.assertEquals('Let us go comment', taskListAfter_completingOnboardingStep[0].description);
        }
    }
    
}