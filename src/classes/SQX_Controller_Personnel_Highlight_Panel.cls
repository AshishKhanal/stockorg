/**
 * Controller class for lightning component
 * @author : Piyush Subedi
 * @story  : [SQX-5316],[SQX-7241]
 */
public with sharing class SQX_Controller_Personnel_Highlight_Panel {
    // number of record to be fetched or shown in the highlights pannel
    public static final Integer RECORD_SIZE = 10;
    
    /**
     * Returns the personnel related records like job functions, certificates
     * @param personnelId id of personnel for which records to be fetched and returned.
     */
    @AuraEnabled
    public static PersonnelWrapper getPersonnelData(String personnelId) {
        SQX_Personnel__c psn = [SELECT Id, SQX_User__r.SmallPhotoUrl, SQX_User__r.Name, SQX_User__c, (SELECT Id, SQX_Job_Function__c, Job_Function_Name__c FROM SQX_Personnel_Job_Functions__r WHERE Active__c = true ORDER BY SQX_Job_Function__c LIMIT :RECORD_SIZE), (SELECT Id, Name, Title__c, SQX_Job_Function__c, Completion_Date__c FROM SQX_Training_Certificates__r ORDER BY SQX_Job_Function__c) FROM SQX_Personnel__c WHERE Id =: personnelId AND Active__c = true];

        List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
        
        for (SQX_Personnel_Job_Function__c pjf : psn.SQX_Personnel_Job_Functions__r) {
            jfs.add(new SQX_Job_Function__c(Id = pjf.SQX_Job_Function__c, Name = pjf.Job_Function_Name__c));
        }
        
        // only unique and latest certificate for a job function should be listed
        Map<Id, SQX_Training_Certificate__c> jfToCertificateMap = new Map<Id, SQX_Training_Certificate__c>();
        for (SQX_Training_Certificate__c tcf : psn.SQX_Training_Certificates__r) {
            // only put 10 unique certificates for now
            // TODO : go with proper query if possible instead doing like this.
            if (jfToCertificateMap.size() < RECORD_SIZE) {
                if (jfToCertificateMap.get(tcf.SQX_Job_Function__c) ==  null) {
                    jfToCertificateMap.put(tcf.SQX_Job_Function__c, tcf);
                } else {
                    // if more than one certifications for a job function then select that certificate which has latest completion date i.e. recent certificate.
                    if (tcf.Completion_Date__c > jfToCertificateMap.get(tcf.SQX_Job_Function__c).Completion_Date__c) {
                        jfToCertificateMap.put(tcf.SQX_Job_Function__c, tcf);
                    }
                }
            } else {
                break;
            }
        }
        
        // instantiate personnel's user wrapper and set all properties
        PersonnelUserWrapper psnUserWrapper;
        if (String.isNotBlank(psn.SQX_User__c)) {
            psnUserWrapper = new PersonnelUserWrapper();
            psnUserWrapper.name = psn.SQX_User__r.Name;
            psnUserWrapper.photoUrl = psn.SQX_User__r.SmallPhotoUrl;
            psnUserWrapper.userId = psn.SQX_User__c;
        }
        
        // now instantiate perosnnel wrapper and set the properties value
        PersonnelWrapper psnWrapper = new PersonnelWrapper();
        psnWrapper.jfs = jfs;
        psnWrapper.certificates = jfToCertificateMap.values();
        psnWrapper.userWrapper = psnUserWrapper;
        
        return psnWrapper;
    }
    
    /**
     * Personnel wrapper class which holds required properties like list of jf, list of certificates, user informations.
     */
    public class PersonnelWrapper {
        @AuraEnabled
        public List<SQX_Job_Function__c> jfs {get; set;}
        
        @AuraEnabled
        public PersonnelUserWrapper userWrapper {get; set;}
        
        @AuraEnabled
        public List<SQX_Training_Certificate__c> certificates {get; set;}
    }
    
    /**
     * User wrapper class which holds name, photourl and id of personnel user.
     */
    public class PersonnelUserWrapper {
        @AuraEnabled
        String name { get; set; }
        
        @AuraEnabled
        String photoUrl { get; set; }
        
        @AuraEnabled
        Id userId { get; set; }
    }
}