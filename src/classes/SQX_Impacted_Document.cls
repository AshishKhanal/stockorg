/**
 * class contains all the logical requirements of the impacted documents
 */
public with sharing class SQX_Impacted_Document {
    
    // valid status of impacted document
    public static final String STATUS_DRAFT = 'Draft',
                                STATUS_OPEN = 'Open',
                                STATUS_COMPLETE = 'Complete',
                                STATUS_SKIPPED = 'Skipped';

    

    /**
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newImpactedDocuments equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Impacted_Document__c> newImpactedDocuments, Map<Id,SQX_Impacted_Document__c> oldMap){
            super(newImpactedDocuments, oldMap);
        }
        
        /**
         * creates new task when assignee is changed and completes old assignee task
         */
        public Bulkified updateTaskForImpactedDocument(){
            final String ACTION_NAME = 'completeTaskForImpactedDocument';

            Rule changedAssigneeImpactedDocumentRule = new Rule();
            changedAssigneeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.Status__c, RuleOperator.Equals, SQX_Impacted_Document.STATUS_OPEN);
            changedAssigneeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.SQX_User__c, RuleOperator.NotEquals, null);
            changedAssigneeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.SQX_User__c, RuleOperator.HasChanged);

            Rule openImpactedDocumentRule = new Rule();
            openImpactedDocumentRule.addTransitionRule(Schema.SQX_Impacted_Document__c.Status__c, RuleOperator.Transitioned, SQX_Impacted_Document.STATUS_OPEN, SQX_Impacted_Document.STATUS_DRAFT);
            openImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.SQX_User__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.ImpactedDocument, ACTION_NAME)
                .groupByRules(new Rule[]{changedAssigneeImpactedDocumentRule, openImpactedDocumentRule}, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Impacted_Document__c> impactedDocumentList = new List<SQX_Impacted_Document__c> ();
            
            impactedDocumentList.addAll((List<SQX_Impacted_Document__c>) changedAssigneeImpactedDocumentRule.evaluationResult);
            impactedDocumentList.addAll((List<SQX_Impacted_Document__c>) openImpactedDocumentRule.evaluationResult);

            if(impactedDocumentList.size() > 0){
                addToProcessedRecordsFor(SQX.ImpactedDocument, ACTION_NAME, impactedDocumentList);
                
                Set<Id> docIds = getIdsForField(impactedDocumentList, Schema.SQX_Impacted_Document__c.SQX_Controlled_Document__c);
                
                Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__C>([SELECT Id, SQX_Change_Order__c FROM SQX_Controlled_Document__c WHERE Id =: docIds AND SQX_Change_Order__c = null]);
                List<SQX_Impacted_Document__c> docsWithoutCO = new List<SQX_Impacted_Document__c>();
                Set<Id> impactedDocIds = new Set<Id>();
                Set<Id> capaIds = new Set<Id>();
                for(SQX_Impacted_Document__c doc : impactedDocumentList){
                    if(docMap.containsKey(doc.SQX_Controlled_Document__c) || !String.isBlank(doc.Document_Number__c)){
                        docsWithoutCO.add(doc);
                        impactedDocIds.add(doc.Id);
                        capaIds.add(doc.SQX_CAPA__c);
                    }
                }

                List<Task> taskList = new List<Task>();
                taskList = [SELECT Id, Status FROM Task WHERE Child_What_Id__c IN : impactedDocIds AND WhatId IN : capaIds];

                if(taskList.size() > 0){
                    completeOldImpactedDocumentTasks(taskList);
                }
                createTaskAndUpdateImpactedDocument(docsWithoutCO);
            }

            return this;
        }
        
        /**
         * completes task when impacted document is completed
         */
        public Bulkified completeTaskForImpactedDocument(){

            Rule completeImpactedDocumentRule = new Rule();
            completeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.Status__c, RuleOperator.Equals, SQX_Impacted_Document.STATUS_COMPLETE);
            completeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.Status__c, RuleOperator.Equals, SQX_Impacted_Document.STATUS_SKIPPED);
            completeImpactedDocumentRule.operator = RuleCombinationOperator.OpOr;
            
            this.resetView()
                .applyFilter(completeImpactedDocumentRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Impacted_Document__c> impactedDocumentList = (List<SQX_Impacted_Document__c>) completeImpactedDocumentRule.evaluationResult;

            if(impactedDocumentList.size() > 0){
                Set<Id> impactedDocIds = new Set<Id>(),
                        capaIds = new Set<Id>();
                for(SQX_Impacted_Document__c impactedDoc: impactedDocumentList) {
                    impactedDocIds.add(impactedDoc.Id);
                    capaIds.add(impactedDoc.SQX_CAPA__c);
                }

                List<Task> taskList = new List<Task>();
                taskList = [SELECT Id, Status FROM Task WHERE Child_What_Id__c IN : impactedDocIds AND WhatId IN : capaIds];
                if(taskList.size() > 0){
                    completeOldImpactedDocumentTasks(taskList);
                }
            }
            return this;
        }
        
        /**
         * completes impacted document if the selected controlled document is already in current status
         */
        public Bulkified completeImpactedDocument(){
            Rule completeImpactedDocumentRule = new Rule();
            completeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.SQX_Controlled_Document__c, RuleOperator.HasChanged);
            completeImpactedDocumentRule.addRule(Schema.SQX_Impacted_Document__c.SQX_Controlled_Document__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .applyFilter(completeImpactedDocumentRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Impacted_Document__c> impactedDocumentList = (List<SQX_Impacted_Document__c>) completeImpactedDocumentRule.evaluationResult;

            if(impactedDocumentList.size() > 0){
                Set<Id> docIds = getIdsForField(impactedDocumentList, Schema.SQX_Impacted_Document__c.SQX_Controlled_Document__c);
                
                Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: docIds]);
                for(SQX_Impacted_Document__c impactedDocument : impactedDocumentList){
                    if(docMap.containsKey(impactedDocument.SQX_Controlled_Document__c)){
                        String docStatus = docMap.get(impactedDocument.SQX_Controlled_Document__c).Document_Status__c;
                        if(docStatus == SQX_Controlled_Document.STATUS_CURRENT || docStatus == SQX_Controlled_Document.STATUS_PRE_EXPIRE){
                            impactedDocument.Status__c = SQX_Impacted_Document.STATUS_COMPLETE;
                            impactedDocument.Completion_Date__c = Date.today();
                        }
                    }
                }
            }
            return this;
        }
        
        /**
         * updates CAPA to check if CAPA can be completed if all the impacted documents are completed
         */
        public Bulkified updateCAPA(){
            this.resetView();
            
            List<SQX_Impacted_Document__c> impactedDocumentList = (List<SQX_Impacted_Document__c>) this.view;
            
            if(impactedDocumentList.size() > 0){
                Set<Id> docIds = getIdsForField(impactedDocumentList, Schema.SQX_Impacted_Document__c.SQX_CAPA__c);
                
                List<SQX_CAPA__c> capas = [SELECT Id FROM SQX_CAPA__c WHERE Id =: docIds];
                
                new SQX_DB().op_update(capas, new List<Schema.SObjectField> {});
            }
            return this;
        }

        /**
         * delete task if the related impacted document is deleted
         */
        public Bulkified deleteRelatedTasks(){
            this.resetView();

            List<SQX_Impacted_Document__c> deletedImpactedDocs = (List<SQX_Impacted_Document__c>) this.view;

            if(deletedImpactedDocs.size() > 0){
                Set<Id> impactedDocIds = new Set<Id>(),
                        capaIds = new Set<Id>();
                for(SQX_Impacted_Document__c impactedDoc: deletedImpactedDocs) {
                    impactedDocIds.add(impactedDoc.Id);
                    capaIds.add(impactedDoc.SQX_CAPA__c);
                }

                List<Task> tasksToDelete = [SELECT Id FROM Task WHERE Child_What_Id__c IN : impactedDocIds AND WhatId IN : capaIds];

                /*
                * WITHOUT SHARING used
                * --------------------
                * When impacted document is deleted by the user with the access to CAPA,
                * user may not have access to task as task is created against the assignee
                * so without sharing is uses so that task is deleted when the user deletes the impacted document
                */
                new SQX_DB().withoutSharing().op_delete(tasksToDelete);
            }
            return this;
        }
    }
    
    /**
     * creates new task for the inspection and updates the task Id for the inspection
     * @param impactedDocumentList list of impacted document whose task is to be created
     */
    public static void createTaskAndUpdateImpactedDocument(List<SQX_Impacted_Document__c> impactedDocumentList){
        
        List<Task> tasksToBeCreated = new List<Task>();
        Map<Id, SQX_Impacted_Document__c> impactedDocumentMap = new Map<Id, SQX_Impacted_Document__c>();
        for(SQX_Impacted_Document__c impactedDocument : impactedDocumentList){
            impactedDocumentMap.put(impactedDocument.Id, impactedDocument);
            Task taskToCreate =SQX_Task_Template.createTaskFor(impactedDocument, 
                                                               impactedDocument.SQX_User__c, 
                                                               null, 
                                                               impactedDocument.Due_Date__c,
                                                               SQX_Task_Template.TaskType.ImpactedDocumentOpened);
            
            tasksToBeCreated.add(taskToCreate);
        }
        
        new SQX_DB().op_insert(tasksToBeCreated, new List<SObjectField>{Task.WhatId,
            Task.OwnerId,
            Task.Subject,
            Task.ActivityDate,
            Task.Priority,
            Task.Status});
    }
    
    /**
     * completes task of the inspection
     * @param taskIds ids of the task to be completed
     */
    public static void completeOldImpactedDocumentTasks(List<Task> taskList){
        for(Task taskToBeCompleted : taskList){
            taskToBeCompleted.Status = SQX_Task.STATUS_COMPLETED;
        }
        
        /*
        * WITHOUT SHARING used
        * --------------------
        * When impacted document is completed by the user, task should be completed
        * so when the user completes the task, the user may not have access to update the task since it might be assigned to different user
        */
        new SQX_DB().withoutSharing().op_update(taskList, new List<SObjectField>{Task.Status});
    }
}