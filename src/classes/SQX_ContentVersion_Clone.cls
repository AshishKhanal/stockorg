/**
* This class provides basic functionality to clone a content in salesforce and add content document link or reference to the
* object initiating the clone
*/
public with sharing class SQX_ContentVersion_Clone {

    private Map<Id, SObject[]> contentsByInitiators;
    private Map<Id, Id[]> cloneContentbySObject;
    private Set<Id> allContentVersionIds;
    private Map<Id, ContentVersion> savedContentVersions;
    private Map<Id, SObject> initiators;
    Map<String, ContentVersion> defaultValues;
    private boolean failOnError;
    /**
     * Default constructor for clone class
     */
    public SQX_ContentVersion_Clone(Boolean failOnError) {
        contentsByInitiators = new Map<Id, SObject[]>();
        defaultValues = new Map<String, ContentVersion>();
        this.failOnError = true;
    }

    /**
     * Adds the content document to be cloned to the list of object that needs to be cloned
     */
    public void cloneContentFor(SObject obj, Id contentDocumentId) {
        cloneContentFor(obj, contentDocumentId, null);
    }

    /**
     * Adds the content document to be cloned to the list of object that needs to be cloned
     * @param  obj               the object that initiated the cloning request ex. controlled document which owns the content.
     * @param  contentDocumentId the id of the content document that has to be cloned.
     * @param  defaultValue      the template content version containing details about the values that have to be set
     */
    public void cloneContentFor(SObject obj, Id contentDocumentId, ContentVersion defaultValue) {
        if(contentDocumentId == null) {
            return;
        }
        List<SObject> objects = contentsByInitiators.get(contentDocumentId);
        if(objects == null) {
            objects = new List<SObject>();
            contentsByInitiators.put(contentDocumentId, objects);
        }

        if(defaultValue != null) {
            defaultValues.put(getKey(obj, contentDocumentId), defaultValue);
        }

        objects.add(obj);
    }

    /**
     * Returns the key that should be used in the map to categorize the value based on obj and contentdocid
     */
    private String getKey(SObject obj, Id contentDocId) {
        return obj.Id + ' ' + contentDocId;
    }

    /**
     * Performs the actual cloning of the records by fetching the data from salesforce
     * @param  changeOwner <code>true</code> if the owner of the record is to be changed to the current user.
     */
    public void cloneRecords() {
        Map<Id, ContentVersion> contentsToClone = new Map<Id, ContentVersion>();
        
        if(contentsByInitiators.isEmpty()) {
            return;
        }
        for(ContentVersion version : new Escalator().getContentVersions(contentsByInitiators.keySet())) {
            contentsToClone.put(version.ContentDocumentId, version);
        }

        Map<Id, ContentVersion[]> contentClones = new Map<Id, ContentVersion[]>();
        List<ContentVersion> allVersions = new List<ContentVersion>();
        initiators = new Map<Id, SObject>();
        Map<ContentVersion, Id> contentOwners = new Map<ContentVersion, Id>();

        for(Id contentDocId : contentsByInitiators.keySet()) {
            for(SObject obj : contentsByInitiators.get(contentDocId)) {
              if(!contentsToClone.containsKey(contentDocId)) {
                  if(failOnError) {
                    throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_CANNOT_VIEW_CONTROLLED_DOCUMENT_CONTENT);
                  }
                   obj.addError(Label.SQX_ERR_MSG_CANNOT_VIEW_CONTROLLED_DOCUMENT_CONTENT);
                   continue;
                }
                ContentVersion version = contentsToClone.get(contentDocId).clone();
                version.ContentBodyId  = null;
                version.ContentDocumentId = null;
                String key = getKey(obj, contentDocId);
                if(defaultValues.containsKey(key)) {
                    Map<String, Object> fieldValues = defaultValues.get(key).getPopulatedFieldsAsMap();
                    for(String field: fieldValues.keySet()) {
                        if(field == 'OwnerId' && ((Id)fieldValues.get(field)) != UserInfo.getUserId()) {
                            contentOwners.put(version, (Id)fieldValues.get(field));
                        } else {
                            version.put(field, fieldValues.get(field));
                        }
                    }
                }
                ContentVersion[] versions = contentClones.get(obj.Id);
                if(versions == null) {
                    versions = new List<ContentVersion>();
                    contentClones.put(obj.Id, versions);
                }
                versions.add(version);
                initiators.put(obj.Id, obj);
                allVersions.add(version);
            }
        }

        new SQX_DB().op_insert(allVersions, new SObjectField[]{});
        List<ContentVersion> contentToUpdate = new List<ContentVersion>();
        contentOwners = contentOwners.clone();
        for(ContentVersion version : contentOwners.keySet()) {
            contentToUpdate.add(new ContentVersion(Id = version.Id, OwnerId = contentOwners.get(version)));
        }
        
        if(!contentToUpdate.isEmpty()) {
            new SQX_DB().op_update(contentToUpdate, new SObjectField[]{});
        }

        cloneContentbySObject = new Map<Id, Id[]>();
        allContentVersionIds = new Set<Id>();
        for(Id parentId : contentClones.keySet()) {
            for(ContentVersion version : contentClones.get(parentId)) {
                allContentVersionIds.add(version.Id);
                List<Id> ids = cloneContentbySObject.get(parentId);
                if(ids == null) {
                    ids = new List<Id>();
                    cloneContentbySObject.put(parentId, ids);
                }
                ids.add(version.Id);
            }
        }
    }

    /**
     * Returns the contentverions related to its content document id
     */
    public without sharing class Escalator {
        public List<ContentVersion> getContentVersions(Set<Id> contentDocumentIds){
            return [SELECT Title, TagCsv, PathOnClient, VersionData, ContentDocumentId, ContentUrl FROM ContentVersion
                                    WHERE ContentDocumentId IN: contentDocumentIds AND IsLatest = true];
        }
    }
    
    /**
     * Returns the list of content version along with content document id for all the newly inserted content versions.
     */
    private Map<Id, ContentVersion> getContentVersionIds() {
        if(savedContentVersions == null) {
            savedContentVersions = new Map<Id, ContentVersion>([SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN : allContentVersionIds]);
        }

        return savedContentVersions;
    }

    /**
     * Links the newly created content with the objects that initiated the request.
     * @param  shareType the share type that is to be added in the link. This is standard SF's Content Document link sharetype like "V" "I"
     */
    public void linkDocumentsToInitiators(String shareType) {
        
        if(contentsByInitiators.isEmpty()) {
            return;
        }
        List<ContentDocumentLink> contentLinks = new List<ContentDocumentLink>();        Map<Id, ContentVersion> allVersions = getContentVersionIds();
        for(Id parentId : cloneContentbySObject.keySet()) {
            for(Id versionId : cloneContentbySObject.get(parentId)) {
                contentLinks.add(new ContentDocumentLink(ContentDocumentId = allVersions.get(versionId).ContentDocumentId, LinkedEntityId = parentId,
                                                        ShareType = shareType));
            }
        }

        new SQX_DB().continueOnError().op_insert(contentLinks, new SObjectField[] { ContentDocumentLink.ContentDocumentId, ContentDocumentLink.LinkedEntityId });
    }

    /**
     * Updates the field in the initating object with the id of the newly created content document id
     * @param  field the field that should store the id of new content document.
     * @param  clone <code>true</code> if a copy of the object is to be set instead of actual object. This is useful when update has to be done after insert/update.
     * @return returns the list of records with the modified values
     */
    public List<SObject> updateDocumentIdInInitiators(String field, Boolean clone) {
        
        if(contentsByInitiators.isEmpty()) {
            return new List<SObject>();
        }
        Map<Id, ContentVersion> allVersions = getContentVersionIds();
        List<SObject> updatedObjects = new List<SObject>();

        for(Id parentId : cloneContentbySObject.keySet()) {
            if(cloneContentbySObject.get(parentId).size() > 1) {
                throw new SQX_ApplicationGenericException('Ids will be overwritten because same sobject is referring to multiple contents');
            }
            Id versionId = cloneContentbySObject.get(parentId).get(0);
            Id contentDocId = allVersions.get(versionId).ContentDocumentId;
            SObject initiator = initiators.get(parentId);
            String key = getKey(initiator, contentDocId);
            
            if(!defaultValues.containsKey(key) || (defaultValues.containsKey(key) && defaultValues.get(key).ContentDocumentId != contentDocId)) {
                if(clone) {
                    initiator = initiator.getSObjectType().newSObject(initiator.Id);
                }
                initiator.put(field, contentDocId);
                updatedObjects.add(initiator);
            }
        }

        return updatedObjects;
    }
}