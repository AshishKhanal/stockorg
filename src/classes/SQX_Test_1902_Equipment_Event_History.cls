/**
* Unit tests for Equipment Event History object
*/
@IsTest
public class SQX_Test_1902_Equipment_Event_History {
    
    static boolean runAllTests = true,
                   run_givenEventHistory_FuturePerformedDate_ErrorShown = false;
    
    /**
    * unit test for Prevent_Future_Performed_Date validation rule
    */
    public static testmethod void givenEventHistory_FuturePerformedDate_ErrorShown() {
        if (!runAllTests && !run_givenEventHistory_FuturePerformedDate_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            Date initialNextDueDate = System.today() + 1;
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Next_Due_Date__c = initialNextDueDate,
                Recurring_Event__c = true,
                Recurring_Interval__c = 2,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_WEEK,
                Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_PERFORMED_DATE
            );
            result1 = Database.insert(sch1, false);
            
            
            // creating Equipment Event History with future performed date
            SQX_Equipment_Event_History__c hist1 = new SQX_Equipment_Event_History__c(
                SQX_Equipment__c = eqp1.Id,
                SQX_Equipment_Event_Schedule__c = sch1.Id,
                Performed_Date__c = System.today() + 1,
                Performed_By__c = 'user',
                Sign_Off_Date__c = System.today(),
                Sign_Off_By__c = user1.username
            );
            
            // ACT: saving event history with future performed date
            result1 = Database.insert(hist1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event history is not expected to be saved.\n' + result1.getErrors());
            
            String errmsg_Prevent_Future_Performed_Date = 'Performed Date cannot be a future date.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result1.getErrors(), errmsg_Prevent_Future_Performed_Date),
                'Expected error message "' + errmsg_Prevent_Future_Performed_Date + '" for validation rule Prevent_Future_Performed_Date not found.\n'
                + result1.getErrors());
        }
    }
}