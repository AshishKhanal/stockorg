/**
* tests for SQX_Extension_Personnel methods
* @story 1660
*/
@IsTest
public class SQX_Test_Extension_Personnel {
    
    static Boolean runAllTests = true,
        run_givenImportAllNewAcitveUsers = false,
        run_givenImportAllActiveUsers_NoRemainingUsers_ErrorShown = false,
        run_givenImportOneUser = false,
        run_givenCreateNonUser = false,
        run_givenListCompletedPersonnelDocumentTraining = false,
        run_givenListPendingPersonnelDocumentTraining = false;
    
    /*
    * tests importing all active users that are not mapped in personnel
    * @story SQX-1660
    */
    public testmethod static void givenImportAllNewAcitveUsers() {
        if (!runAllTests && !run_givenImportAllNewAcitveUsers) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user3 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // make a user inactive useing system admin
            user3.isActive = false;
            Database.SaveResult sr = Database.update(user3, false);
            System.assert(sr.isSuccess() == true, 'Required inactive user is expected to be saved.\n' + sr.getErrors());
        }
        
        System.runas(user2) {
            // setup personnel create page
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            ApexPages.StandardController sc = new ApexPages.standardController(new SQX_Personnel__c());
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(sc);
            
            // ACT: import all users
            ext.createActionToTake = SQX_Personnel.CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS;
            ext.create();
            
            // get page messages
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            
            System.assert(msgs.size() == 1, 'Only 1 page message is expected after importing all users.');
            System.assert(msgs[0].getDetail().contains('records have been imported successfully'),
                'Page message containing "records have been imported successfully" is expected.');
            
            // get create personnel records
            Set<Id> userIds = new Set<Id>{ user1.Id, user2.Id, user3.Id };
            List<User> users = [SELECT Id, (SELECT Id FROM SQX_Personnels__r) FROM User WHERE Id IN :userIds];
            Map<Id, User> userMap = new Map<Id, User>(users);
            
            System.assert(userMap.get(user1.Id).SQX_Personnels__r.size() == 1,
                'Personnel record is expected to be imported for 1st active user.');
            System.assert(userMap.get(user2.Id).SQX_Personnels__r.size() == 1,
                'Personnel record is expected to be imported for 2nd active user.');
            System.assert(userMap.get(user3.Id).SQX_Personnels__r.size() == 0,
                'Personnel record is expected not to be imported for 3rd inactive user.');
       }
    }
    
    /*
    * tests importing all active users that are not mapped in personnel
    * @story SQX-1660
    */
    public testmethod static void givenImportAllActiveUsers_NoRemainingUsers_ErrorShown() {
        if (!runAllTests && !run_givenImportAllActiveUsers_NoRemainingUsers_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(user1) {
            // import all users
            SQX_Personnel.importAllUsers();
            
            // setup personnel create page
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            ApexPages.StandardController sc = new ApexPages.standardController(new SQX_Personnel__c());
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(sc);
            
            // ACT: import all users
            ext.createActionToTake = SQX_Personnel.CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS;
            ext.create();
            
            // get page messages
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            
            System.assert(msgs.size() == 1, 'Only 1 page message is expected after importing all users.');
            System.assert(msgs[0].getDetail().equals('There are no new active users to import.'),
                'Page message containing "There are no new active users to import." is expected.');
       }
    }
    
    /*
    * tests importing one user for both create and createAndNew actions
    * @story SQX-1660
    */
    public testmethod static void givenImportOneUser() {
        if (!runAllTests && !run_givenImportOneUser) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // setup personnel create page
            SQX_Personnel__c p = new SQX_Personnel__c();
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            ApexPages.StandardController sc = new ApexPages.standardController(p);
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(sc);
            
            // ACT: import user1
            ext.createActionToTake = SQX_Personnel.CREATE_ACTION_IMPORT_USER;
            p.SQX_User__c = user1.Id;
            PageReference pr = ext.create();
            
            System.assert(pr != null, 'PageReference is expected after importing one user.');
            
            User pUser = [SELECT Id, (SELECT Id FROM SQX_Personnels__r) FROM User WHERE Id = :user1.Id];
            System.assert(pUser.SQX_Personnels__r.size() == 1, 'Expected personnel record to be created.');
            
            String expectedURL = new ApexPages.StandardController(pUser.SQX_Personnels__r[0]).View().getURL();
            System.assertEquals(expectedURL, pr.getURL());
            
            
            // setup personnel create page for user2
            p = new SQX_Personnel__c();
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            sc = new ApexPages.standardController(p);
            ext = new SQX_Extension_Personnel(sc);
            
            // ACT: import user2
            ext.createActionToTake = SQX_Personnel.CREATE_ACTION_IMPORT_USER;
            p.SQX_User__c = user2.Id;
            pr = ext.createAndNew();
            
            System.assert(pr != null, 'PageReference is expected after importing one user.');
            
            pUser = [SELECT Id, (SELECT Id FROM SQX_Personnels__r) FROM User WHERE Id = :user1.Id];
            System.assert(pUser.SQX_Personnels__r.size() == 1, 'Expected personnel record to be created.');
            
            System.assertEquals(Page.SQX_Personnel_Create.getURL(), pr.getURL());
       }
    }
    
    /*
    * tests creating non user for both create and createAndNew actions
    * @story SQX-1660
    */
    public testmethod static void givenCreateNonUser() {
        if (!runAllTests && !run_givenCreateNonUser) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(user1) {
            // create SQX_Test_Personnel from unique Identification Number
            SQX_Test_Personnel idNum = new SQX_Test_Personnel();
            
            // setup personnel create page
            SQX_Personnel__c p = new SQX_Personnel__c();
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            ApexPages.StandardController sc = new ApexPages.standardController(p);
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(sc);
            
            System.assertEquals(SQX_Personnel.CREATE_ACTION_NON_USER, ext.createActionToTake,
                'Expected default create action value to be "' + SQX_Personnel.CREATE_ACTION_NON_USER + '".');
            
            System.assertEquals(user1.Id, p.OwnerId, 'Owner is expected to be current user when creating non user.');
            
            // ACT: create non user
            p.Full_Name__c = 'Mark I';
            p.Identification_Number__c = 'I' + idNum.mainRecord.Identification_Number__c;
            PageReference pr = ext.create();
            
            System.assert(pr != null, 'PageReference is expected after creating record.');
            
            List<SQX_Personnel__c> pNew = [SELECT Id FROM SQX_Personnel__c WHERE Identification_Number__c = :p.Identification_Number__c];
            System.assert(pNew.size() == 1, 'Expected personnel record to be created.');
            
            String expectedURL = new ApexPages.StandardController(pNew[0]).View().getURL();
            System.assertEquals(expectedURL, pr.getURL());
            
            
            // setup personnel create page
            p = new SQX_Personnel__c();
            Test.setCurrentPage(Page.SQX_Personnel_Create);
            sc = new ApexPages.standardController(p);
            ext = new SQX_Extension_Personnel(sc);
            
            // ACT: create another non user
            ext.createActionToTake = SQX_Personnel.CREATE_ACTION_NON_USER;
            p.Full_Name__c = 'Mark II';
            p.Identification_Number__c = 'II' + idNum.mainRecord.Identification_Number__c;
            pr = ext.createAndNew();
            
            System.assert(pr != null, 'PageReference is expected after create record.');
            
            pNew = [SELECT Id FROM SQX_Personnel__c WHERE Identification_Number__c = :p.Identification_Number__c];
            System.assert(pNew.size() == 1, 'Expected personnel record to be created.');
            
            System.assertEquals(Page.SQX_Personnel_Create.getURL(), pr.getURL());
       }
    }
    
    /*
    * tests for completed personnel document training list
    * @story SQX-1671
    */
    public testmethod static void givenListCompletedPersonnelDocumentTraining() {
        if (!runAllTests && !run_givenListCompletedPersonnelDocumentTraining) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(user1) {
            // add required personnel
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(user2);
            Database.SaveResult sr = personnel.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // add required controlled documents
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save();
            doc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required completed personnel document training records
            SQX_Personnel_Document_Training__c dtn1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Completion_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                SQX_User_Signed_Off_By__c = user2.Id,
                User_Signature__c = user2.Name,
                User_SignOff_Date__c = DateTime.now()
            );
            sr = Database.insert(dtn1, false);
            System.assert(sr.isSuccess(), 'Required completed personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            // add required pending personnel document training records
            SQX_Personnel_Document_Training__c dtn2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            sr = Database.insert(dtn2, false);
            System.assert(sr.isSuccess(), 'Required pending personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            SQX_Personnel_Document_Training__c dtn3 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc1.doc.Id,
                SQX_Trainer__c = user1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                SQX_User_Signed_Off_By__c = user2.Id,
                User_Signature__c = user2.Name,
                User_SignOff_Date__c = DateTime.now()
            );
            sr = Database.insert(dtn3, false);
            System.assert(sr.isSuccess(), 'Required pending personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            // ACT: get pernding list using extension
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(new ApexPages.StandardController(personnel.mainRecord));
            ext.getCompletedPersonnelDocumentTrainingController().setPageSize(100);
            Map<Id, SQX_Personnel_Document_Training__c> dts = new Map<Id, SQX_Personnel_Document_Training__c>((List<SQX_Personnel_Document_Training__c>)ext.getCompletedPersonnelDocumentTrainingController().getRecords());
            
            System.assertEquals(1, dts.size(),
                'Expected 1 pending personnel document training records.');
            
            System.assert(dts.keySet().contains(dtn1.Id),
                'Expected completed personnel document training to be listed.');
            
            System.assert(dts.keySet().contains(dtn2.Id) == false,
                'Expected pending personnel document training not to be listed.');
                
            System.assert(dts.keySet().contains(dtn3.Id) == false,
                'Expected trainer approval pending personnel document training not to be listed.');
       }
    }
    
    /*
    * tests for pending personnel document training list
    * @story SQX-1671
    */
    public testmethod static void givenListPendingPersonnelDocumentTraining() {
        if (!runAllTests && !run_givenListPendingPersonnelDocumentTraining) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(user1) {
            // add required personnel
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(user2);
            Database.SaveResult sr = personnel.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // add required controlled documents
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save();
            doc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required completed personnel personnel document training records
            SQX_Personnel_Document_Training__c dtn1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Completion_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                SQX_User_Signed_Off_By__c = user2.Id,
                User_Signature__c = user2.Name,
                User_SignOff_Date__c = DateTime.now()
            );
            sr = Database.insert(dtn1, false);
            System.assert(sr.isSuccess(), 'Required completed personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            // add required pending personnel document training records
            SQX_Personnel_Document_Training__c dtn2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            sr = Database.insert(dtn2, false);
            System.assert(sr.isSuccess(), 'Required pending personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            SQX_Personnel_Document_Training__c dtn3 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = doc1.doc.Id,
                SQX_Trainer__c = user1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                SQX_User_Signed_Off_By__c = user2.Id,
                User_Signature__c = user2.Name,
                User_SignOff_Date__c = DateTime.now()
            );
            sr = Database.insert(dtn3, false);
            System.assert(sr.isSuccess(), 'Required pending personnel document training record is expected to be saved.\n' + sr.getErrors());
            
            // ACT: get pernding list using extension
            SQX_Extension_Personnel ext = new SQX_Extension_Personnel(new ApexPages.StandardController(personnel.mainRecord));
            ext.getPendingPersonnelDocumentTrainingController().setPageSize(100);
            Map<Id, SQX_Personnel_Document_Training__c> dts = new Map<Id, SQX_Personnel_Document_Training__c>((List<SQX_Personnel_Document_Training__c>)ext.getPendingPersonnelDocumentTrainingController().getRecords());
            
            System.assertEquals(2, dts.size(),
                'Expected 2 pending personnel document training records.');
            
            System.assert(dts.keySet().contains(dtn1.Id) == false,
                'Expected completed personnel document training not to be listed.');
            
            System.assert(dts.keySet().contains(dtn2.Id),
                'Expected pending personnel document training to be listed.');
                
            System.assert(dts.keySet().contains(dtn3.Id),
                'Expected trainer approval pending personnel document training to be listed.');
       }
    }
}