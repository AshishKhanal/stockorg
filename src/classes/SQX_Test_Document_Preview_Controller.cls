/*
* Unit test class for SQX_Document_Preview_Controller
*/
@isTest
public class SQX_Test_Document_Preview_Controller {
    
    /**
     * When: Api name and label of api field is requested
     * Then: send api name and label info of cont doc
     */
    @isTest
    public static void whenAPIInfoOfContDocIsRequested_ThenReturnAPINameAndLabelInfo() {
        Map<String, String> contDocApiAndLabels = SQX_Document_Preview_Controller.getFieldApiNameAndLabels();
        System.assert(contDocApiAndLabels != null, 'Expecteded api name and labels of contDoc');
        System.assert(contDocApiAndLabels.get('compliancequest__draft_vault__c') != null, 'Expected Draft vault to have some value');
    }
}