/**
 * test class to test complaint flow
 */
@isTest
public class SQX_Test_7448_Complaint_Flow {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    
        Group complaintQueue = new Group(Name = 'Complaint Queue', Type = 'Queue');
        insert complaintQueue;
        
        QueueSObject queueObject = new QueueSObject(QueueId = complaintQueue.Id, SObjectType = Schema.SObjectType.SQX_Complaint__c.getName());
        insert queueObject;
        
        GroupMember gMember = new GroupMember(GroupId = complaintQueue.Id, UserOrGroupId = standardUser.Id);
        insert gMember;
        
        System.runAs(adminUser){

            SQX_Department__c department = new SQX_Department__c (Name = 'Complaint', Queue_Id__c = complaintQueue.Id);
            insert department;
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Complaint is submitted
     * THEN : Status is Draft and stage is triage
     */
    public testMethod static void givenAComplaint_WhenComplaintIsSubmitted_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            SQX_Department__c department = [SELECT Id, Queue_Id__c FROM SQX_Department__c WHERE Name = 'Complaint'];
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.complaint.SQX_Department__c = department.Id;
            complaint.save();
            
            // ACT : Complaint is submitted
            complaint.submit();
            SQX_Complaint__c submittedComplaint = [SELECT Id, OwnerId, Status__c, Record_Stage__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is Draft and stage is triage
            System.assertEquals(SQX_Complaint.STATUS_DRAFT, submittedComplaint.Status__c);
            System.assertEquals(SQX_Complaint.STAGE_TRIAGE, submittedComplaint.Record_Stage__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Complaint.ACTIVITY_CODE_SUBMIT].size());
            System.assertNotEquals(null, department.Id);
            System.assertEquals(department.Queue_Id__c, submittedComplaint.OwnerId);
        }
    }

    /**
     * GIVEN : A complaint is created and submitted
     * WHEN : Complaint ownership is take
     * THEN : Status is Open and stage is in progress
     */
    public testMethod static void givenAComplaint_WhenComplaintIsSubmittedAndOwnershipIsTaken_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint = null;
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and submitted
            complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            Database.SaveResult result = Database.insert(task, false);
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            complaint.submit();
        }

        System.runas(adminUser){
            
            // ACT : Complaint is initiated
            complaint.takeOwnershipAndInitiate();
            
            SQX_Complaint__c submittedComplaint = [SELECT Id, Status__c, Record_Stage__c, OwnerId, Open_Date__c, Is_Initiated__c, Activity_Code__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is Open and stage is In Progress
            System.assertEquals(SQX_Complaint.STATUS_OPEN, submittedComplaint.Status__c);
            System.assertEquals(SQX_Complaint.STAGE_INPROGRESS, submittedComplaint.Record_Stage__c);
            System.assertEquals(adminUser.Id, submittedComplaint.OwnerId);
            System.assertEquals(Date.today(), submittedComplaint.Open_Date__c);
            System.assertEquals(true, submittedComplaint.Is_Initiated__c);
            System.assertEquals(null, submittedComplaint.Activity_Code__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE].size());
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Complaint is initiated
     * THEN : Status is Open and stage is in progress
     */
    public testMethod static void givenAComplaint_WhenComplaintIsInitiatedDirectly_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint is created and submitted
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            insert task;
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            
            // ACT : Complaint is initiated
            complaint.initiate();
            
            SQX_Complaint__c initiatedComplaint = [SELECT Id, Status__c, Record_Stage__c, Open_Date__c, Is_Initiated__c, Activity_Code__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is Open and stage is In Progress
            System.assertEquals(SQX_Complaint.STATUS_OPEN, initiatedComplaint.Status__c);
            System.assertEquals(SQX_Complaint.STAGE_INPROGRESS, initiatedComplaint.Record_Stage__c);
            System.assertEquals(Date.today(), initiatedComplaint.Open_Date__c);
            System.assertEquals(true, initiatedComplaint.Is_Initiated__c);
            System.assertEquals(null, initiatedComplaint.Activity_Code__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Complaint.ACTIVITY_CODE_INITIATE].size());
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Complaint is void
     * THEN : Status is void and stage is null
     */
    public testMethod static void givenAComplaint_WhenComplaintIsVoid_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and submitted
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            complaint.initiate();
            
            // ACT : Complaint is void
            complaint.void();
            
            SQX_Complaint__c voidComplaint = [SELECT Id, Status__c, Is_Locked__c, Record_Stage__c, Resolution__c, Activity_Code__c, Closed_By__c, Close_Date__c  FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is Open and stage is In Progress
            System.assert(voidComplaint.Is_Locked__c, 'Complaint is not locked');
            System.assertEquals(Date.today(), voidComplaint.Close_Date__c);
            System.assertEquals(standardUser.Id, voidComplaint.Closed_By__c);
            System.assertEquals(SQX_Complaint.STATUS_VOID, voidComplaint.Status__c);
            System.assertEquals(SQX_Complaint.RESOLUTION_VOID, voidComplaint.Resolution__c);
            System.assertEquals(null, voidComplaint.Record_Stage__c);
            System.assertEquals(null, voidComplaint.Activity_Code__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Complaint.ACTIVITY_CODE_VOID].size());
        }
    }
    
    /**
     * GIVEN : A complaint is created and initiated
     * WHEN : Complaint is closed
     * THEN : Status is closed and stage is closed
     */
    public testMethod static void givenAComplaint_WhenComplaintIsClosed_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and initiated
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            complaint.initiate();
            
            // ACT : Complaint is closed
            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.close();
            
            SQX_Complaint__c closedComplaint = [SELECT Id, Status__c, Is_Locked__c, Record_Stage__c, Activity_Code__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is closed and stage is closed
            System.assert(closedComplaint.Is_Locked__c, 'Complaint is not locked');
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, closedComplaint.Status__c);
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, closedComplaint.Record_Stage__c);
            System.assertEquals(null, closedComplaint.Activity_Code__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Complaint.ACTIVITY_CODE_CLOSE].size());
        }
    }
    
    /**
     * GIVEN : A complaint is created and initiated and closed
     * WHEN : Complaint is reopened
     * THEN : Status is draft and stage is draft
     */
    public testMethod static void givenAComplaint_WhenComplaintIsReopened_StatusAndStageAreSetCorrectly(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and initiated and closed
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();

            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            insert task;
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            
            complaintTask.compliancequest__Status__c = SQX_Complaint_Task.STATUS_COMPLETE;
            update complaintTask;

            complaint.initiate();

            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.close();
            
            // ACT : Complaint is reopened
            complaint.complaint.Reason_For_Reopen__c = 'Data Correction';
            complaint.reopen();
            
            SQX_Complaint__c reopenComplaint = [SELECT Id, Status__c, Record_Stage__c, Resolution__c, Reopen_Date__c, Close_Date__c, Activity_Code__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ASSERT : Status is draft and stage is draft
            System.assertEquals(SQX_Complaint.STATUS_OPEN, reopenComplaint.Status__c);
            System.assertEquals(SQX_Complaint.STAGE_INPROGRESS, reopenComplaint.Record_Stage__c);
            System.assertEquals(Date.today(), reopenComplaint.Reopen_Date__c);
            System.assertEquals(null, reopenComplaint.Activity_Code__c);
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Record_Activity__c WHERE Activity_Code__c =: SQX_Complaint.ACTIVITY_CODE_REOPEN].size());
        }
    }

    /**
     * GIVEN : An complaint is created
     * WHEN : Complaint is closed with blank resolution
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaint_WhenComplaintIsClosedWithBlankResolution_ErrorIsThrown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ACT : Complaint is closed with blank resolution
            SQX_Complaint__c closedComplaint = new SQX_Complaint__c();
            closedComplaint.Id = complaint.complaint.Id;
            closedComplaint.Status__c = SQX_Complaint.STATUS_CLOSED;
            closedComplaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSED;
            closedComplaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            Database.SaveResult result = Database.update(closedComplaint, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(),'Resolution is required when closing or voiding complaint.'), result.getErrors());

            // ACT : Complaint is closed with resolution
            closedComplaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            result = Database.update(closedComplaint, false);
            
            // ASSERT : Complaint is closed
            System.assert(result.isSuccess(), 'Save is not successful'+ result.getErrors());
        }
    }
    
    /**
     * GIVEN : An complaint is created
     * WHEN : Complaint is void with blank resolution
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaint_WhenComplaintIsVoidWithBlankResolution_ErrorIsThrown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ACT : Complaint is void with blank resolution
            SQX_Complaint__c voidComplaint = new SQX_Complaint__c();
            voidComplaint.Id = complaint.complaint.Id;
            voidComplaint.Status__c = SQX_Complaint.STATUS_VOID;
            voidComplaint.Record_Stage__c = null;
            Database.SaveResult result = Database.update(voidComplaint, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(),'Resolution is required when closing or voiding complaint.'), result.getErrors());

            // ACT : Complaint is void with resolution
            voidComplaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            result = Database.update(voidComplaint, false);
            
            // ASSERT : Complaint is void
            System.assert(result.isSuccess(), 'Save is not successful'+ result.getErrors());
        }
    }

    /**
     * GIVEN : An complaint is created
     * WHEN : Complaint is closed with blank conclusion code
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaint_WhenComplaintIsClosedWithBlankConclusionCode_ErrorIsThrown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            // ACT : Complaint is closed with blank conclusion code
            SQX_Complaint__c closedComplaint = new SQX_Complaint__c();
            closedComplaint.Id = complaint.complaint.Id;
            closedComplaint.Status__c = SQX_Complaint.STATUS_CLOSED;
            closedComplaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSED;
            closedComplaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            Database.SaveResult result = Database.update(closedComplaint, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(),'Conclusion Code is required when closing complaint.'), result.getErrors());

            // ACT : Complaint is closed with conclusion code
            closedComplaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            result = Database.update(closedComplaint, false);
            
            // ASSERT : Complaint is closed
            System.assert(result.isSuccess(), 'Save is not successful'+ result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint is edited
     * WHEN : Flow is invoked
     * THEN : Form rules are evaluated
     */
    /*public static testMethod void givenComplaint_WhenComplaintIsEdited_FormRulesIsEvaluated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint edit action is called
            Map<String, Object> params = new Map<String, Object>();
            params.put('Action', 'edit');
            
            // ACT : Flow is invoked
            Flow.Interview.CQ_Complaint_Form_Rules_Evaluation calcFlow = new Flow.Interview.CQ_Complaint_Form_Rules_Evaluation(params);
            calcFlow.start();
            
            String output = (String) calcFlow.getVariableValue('EvaluationExpression');
            
            // ASSERT : Form rules is evaluated
            System.assert(String.isNotBlank(output), 'Output is not blank');
            System.assert(output.contains('CQ_Complaint_Set_Account_From_Contact'), 'Experssion not evaluated');
        }
    }*/
    
    /**
     * GIVEN : Complaint is created
     * WHEN : Contact is selected
     * THEN : Account is auto populated
     *
     * WHEN : Contact is cleared
     * THEN : Account is cleared
     */
    public static testMethod void givenComplaint_WhenContactIsSelected_AccountIsSelected(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        Account acc = null;
        Contact con = null;
        System.runAs(adminUser){
            acc = new Account(Name = 'Test Account');
            insert acc;

            con = new Contact (FirstName = 'Test', LastName = 'Contact', AccountId = acc.Id);
            insert con;

        }
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);

            // ACT : Contact is selected
            complaint.complaint.SQX_External_Contact__c = con.Id;
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);
            
            Flow.Interview.CQ_Complaint_Set_Account_From_Contact calcFlow = new Flow.Interview.CQ_Complaint_Set_Account_From_Contact(params);
            calcFlow.start();
            
            SQX_Complaint__c comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Account is selected
            System.assertEquals(acc.Id, comp.SQX_Account__c);
            System.assertEquals(acc.Name, comp.Company_Name__c);

            // ACT : Contact is cleared
            complaint.complaint.SQX_External_Contact__c = null;

            params = new Map<String, Object>();
            params.put('InRecord', complaint.complaint);

            calcFlow = new Flow.Interview.CQ_Complaint_Set_Account_From_Contact(params);
            calcFlow.start();
            
            comp = (SQX_Complaint__c) calcFlow.getVariableValue('OutRecord');
            
            // ASSERT : Account is cleared
            System.assertEquals(null, comp.SQX_Account__c);
            System.assert(String.isBlank(comp.Company_Name__c), 'Value is not cleared');
        }
    }
    
    /**
     * GIVEN : Complaint is changed
     * WHEN : Flow is invoked
     * THEN : Form rules are evaluated
     */
    /*public static testMethod void givenComplaint_WhenComplaintIsChanged_FormRulesIsEvaluated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint edit action is called
            Map<String, Object> params = new Map<String, Object>();
            params.put('Action', 'create');
            
            // ACT : Flow is invoked
            Flow.Interview.CQ_Complaint_Form_Rules_Evaluation calcFlow = new Flow.Interview.CQ_Complaint_Form_Rules_Evaluation(params);
            calcFlow.start();
            
            String output = (String) calcFlow.getVariableValue('EvaluationExpression');
            
            // ASSERT : Form rules is evaluated
            System.assert(String.isNotBlank(output), 'Output is not blank');
            String capaField = SQX_Complaint__c.SQX_CAPA__c.getDescribe().getName(),
                    changeField = SQX_Complaint__c.Change_Control_Number__c.getDescribe().getName();
            System.assert(output.contains(capaField), 'Experssion not evaluated' + output);
            System.assert(output.contains(changeField), 'Experssion not evaluated' + output);
        }
    }*/

    /**
     * GIVEN : Complaint is created with CAPA Escalation set to true
     * WHEN : Complaint is updated without CAPA
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaint_WhenCAPAIsEscalated_CAPAIsRequired(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        system.RunAs(standardUser){
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility().save();

            // ACT : Complaint is updated setting capa escalation to true and capa with blank value
            SQX_Complaint__c comp = complaint.complaint;
            comp.CAPA_Escalation__c = true;
            Database.SaveResult result = Database.update(comp, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'CAPA is required for CAPA escalation.'), result.getErrors());

            // ACT : Complaint is saved with CAPA
            comp.SQX_CAPA__c = capa.capa.Id;
            result = Database.update(comp, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful');
        }
    }

    /**
     * GIVEN : Complaint is created with Change Control set to true
     * WHEN : Complaint is updated without Change Control Number
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaint_WhenChangeControlIsSetToTrue_ChangeControlNumberIsRequired(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        system.RunAs(standardUser){
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Complaint is updated setting change control to true and change control number with blank value
            SQX_Complaint__c comp = complaint.complaint;
            comp.Change_Control__c = true;
            Database.SaveResult result = Database.update(comp, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Change Control Number is required.'), result.getErrors());

            // ACT : Complaint is saved with change control number
            comp.Change_Control_Number__c = 'CHG-000001';
            result = Database.update(comp, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful');
        }
    }

    /**
     * GIVEN : Complaint is created
     * WHEN : Reported Date is set to blank
     * THEN : Reported Age is calculated from created date
     *
     * WHEN : Reported Date is set to past date
     * THEN : Reported Age is calculated accordingly
     */
    @IsTest
    static void givenComplaint_WhenReportedDateIsBlank_ReportedAgeIsCalculatedFromCreatedDate(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        system.RunAs(standardUser){
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Reported date is set to null
            SQX_Complaint__c comp = complaint.complaint;
            comp.Reported_Date__c = null;
            Database.SaveResult result = Database.update(comp, false);
            System.assert(result.isSuccess(), 'Save is not successful');

            // ASSERT : Reported Age is calcuated from created date
            System.assertEquals(0, [SELECT Id, Age_Reported__c FROM SQX_Complaint__c WHERE Id =:complaint.complaint.Id].Age_Reported__c);

            // ACT : Reported Date is set
            comp.Reported_Date__c = Date.today().addDays(-10);
            comp.Occurrence_Date__c = Date.today().addDays(-10);
            comp.Aware_Date__c = Date.today().addDays(-10);
            result = Database.update(comp, false);
            System.assert(result.isSuccess(), 'Save is not successful');

            // ASSERT : Reported Age is calculated from reported date
            System.assertEquals(10, [SELECT Id, Age_Reported__c FROM SQX_Complaint__c WHERE Id =:comp.Id].Age_Reported__c);
        }
    }

    
    /**
     * GIVEN : A complaint is created, submitted and closed
     * WHEN : Complaint is reopened
     * THEN : Owner is changed to current user
     */
    public testMethod static void givenComplaint_WhenReopened_OwnerIsChangedToCurrentUser(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category3',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runas(standardUser){ 
            SQX_Department__c department = [SELECT Id, Queue_Id__c FROM SQX_Department__c WHERE Name = 'Complaint'];
            // ARRANGE : Complaint is created, submitted and closed
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.complaint.SQX_Department__c = department.Id;
            complaint.save();
            
            complaint.submit();
            SQX_Complaint__c submittedComplaint = [SELECT Id, OwnerId, Status__c, Record_Stage__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            System.assertEquals(department.Queue_Id__c, submittedComplaint.OwnerId);

            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.close();
            
            // ACT : Complaint is reopened
            complaint.complaint.Reason_For_Reopen__c = 'Data Correction';
            complaint.reopen();

            // ASSERT : Owner is changed to current user
            submittedComplaint = [SELECT Id, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            System.assertEquals(standardUser.Id, submittedComplaint.OwnerId);
        }
    }
    
    /**
     * GIVEN : Complaint is created
     * WHEN : Complaint Defect is inserted
     * THEN : Defective Quantity and no. of defects are added
     */
    public static testMethod void givenComplaintWithComplaintDefect_WhenDefectCodeIsSelected_RelatedFieldsArePopulated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Defect_Code__c defectCode = null;
        SQX_Part__c part = null;

        System.runAs(adminUser){
            defectCode = new SQX_Defect_Code__c(Name = 'Complaint Conclusion', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Complaint Conclusion',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert defectCode;

            part = SQX_Test_Part.insertPart(part, adminUser, true, '');
        }
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Complaint Defect is inserted
            SQX_Complaint_Defect__c cDefect = new SQX_Complaint_Defect__c();
            cDefect.Defect_Category__C = 'Category';
            cDefect.Defect_Code__c = 'Test';
            cDefect.SQX_Complaint__c = complaint.complaint.Id;
            cDefect.SQX_Defect_Code__c = defectCode.Id;
            cDefect.SQX_Part__c = part.Id;
            Database.SaveResult result = Database.insert(cDefect, false);

            System.assert(result.isSuccess(), 'Save is not successful'+ result.getErrors());

            cDefect = [SELECT Defective_Quantity__c, Number_of_defects__c, Name, Defect_Code__c, Defect_Category__C FROM SQX_Complaint_Defect__c WHERE Id =: cDefect.Id];

            // ASSERT : Defective Quantity and no of defects are added
            System.assertEquals([SELECT Complaint_Quantity__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id].Complaint_Quantity__c, cDefect.Defective_Quantity__c);
            System.assertEquals(0, cDefect.Number_of_defects__c);
            
            System.assertEquals(defectCode.Name, cDefect.Defect_Code__c);
            System.assertEquals(defectCode.Defect_Category__C, cDefect.Defect_Category__C);
            
        }
    }
}