/**
* @author Sudeep Maharjan
* @date 2017/01/27
* @description this class is the extension controller for Training Session
*/
public with sharing class SQX_Extension_Training_Session extends SQX_Controller_Sign_Off {
    
    public static Integer SF_LIMIT_QUERY_LOCATOR_ROWS { get { return Limits.getLimitQueryLocatorRows(); } }
    public static Integer SF_LIMIT_VF_COLLECTION_SIZE { get { return 1000; } }
    public static final Integer DEFAULT_PAGE_SIZE = 1000;
    
    public static final String  VALUE_TRUE = 'true',
                                VALUE_FALSE = 'false';
    
    public List<SQX_Training_Session_Roster__c> rosters { get; set; }
    public String recordActivityComment { get; set; }
    public Boolean closeImmediately { get; set; }
    
    private SQX_Training_Session__c mainRecord      = null;
    private ApexPages.StandardController controller = null;

    /**
     * static string of param passed in the URL
     */
    public static final String PARAM_ID     = 'id',
                               PARAM_MODE   = 'mode';

    /**
    * Add default policies
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            SQX_Training_Session.PurposeOfSignature_InitiatingTrainingSession => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Training_Session.PurposeOfSignature_CompletingTrainingSession => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Training_Session.PurposeOfSignature_ClosingTrainingSession => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Training_Session.PurposeOfSignature_ReopeningTrainingSession => this.POLICY_NO_COMMENT_REQUIRED,
            SQX_Training_Session.PurposeOfSignature_VoidingTrainingSession => this.POLICY_NO_COMMENT_REQUIRED
            
        };
        return defaultEsigPolicies;
    }

    /**
    * Training session extension constructor
    */
    public SQX_Extension_Training_Session(ApexPages.StandardController controller){

        super(controller, new List<SObjectField> {  Schema.SQX_Training_Session__c.Status__c,
                                                    Schema.SQX_Training_Session__c.Previous_Status__c,
                                                    Schema.SQX_Training_Session__c.Total_Trainees__c,
                                                    Schema.SQX_Training_Session__c.SQX_Controlled_Document__c});
        
        this.purposeOfSigMap.putAll(SQX_Training_Session.purposeOfSigMap);

        //getting Training Session record from controller
        mainRecord = (SQX_Training_Session__c)controller.getRecord();
        this.controller = controller;
        // gets the action type from the URL
        if(ApexPages.currentPage() != null){
            recordId = mainRecord.Id;
            actionName = ApexPages.currentPage().getParameters().get(PARAM_MODE);
            
            rosters = new List<SQX_Training_Session_Roster__c>();
            closeImmediately = false;
            if (mainRecord.Total_Trainees__c > 0 && mainRecord.Total_Trainees__c <= SF_LIMIT_VF_COLLECTION_SIZE
                && actionName == SQX_Training_Session.PurposeOfSignature_CompletingTrainingSession) {
                    
                    rosters = [ SELECT SQX_Personnel__c, Personnel_Name__c, Result__c
                               FROM SQX_Training_Session_Roster__c
                               WHERE SQX_Training_Session__c =: mainRecord.Id
                               ORDER BY Result__c DESC, Personnel_Name__c, SQX_Personnel__c ];
                }
            purposeOfSignature = SQX_Training_Session.purposeOfSigMap.containsKey(actionName) ? SQX_Training_Session.purposeOfSigMap.get(actionName) : '';
        }
    }

    /**
    * method to update training session and create Record activity for Training Session activity
    * @return url to redirect the page or throws error
    */
    public PageReference saveRecord(){
        PageReference ref = null;
        Database.SaveResult result = null;
        Savepoint sp = null;
        SQX_Training_Session ts = new SQX_Training_Session(mainRecord, rosters);
        
        if(hasValidCredentials()){
            try{
                sp = Database.setSavepoint();
                
                if(actionName == SQX_Training_Session.PurposeOfSignature_InitiatingTrainingSession) {
                    if (SQX_Training_Session.canInitiate(mainRecord)){
                        result = ts.initiate(RecordActivityComment);
                    }
                }
                else if(actionName == SQX_Training_Session.PurposeOfSignature_CompletingTrainingSession) {
                    if (SQX_Training_Session.canComplete(mainRecord)){
                        result = ts.complete(RecordActivityComment);
                        if(result.isSuccess() && closeImmediately){
                            result = ts.close(RecordActivityComment);
                        }
                    }
                }
                else if(actionName==SQX_Training_Session.PurposeOfSignature_VoidingTrainingSession) {
                    if(SQX_Training_Session.canVoid(mainRecord)){
                        result = ts.void(RecordActivityComment);
                    }
                }
                else if(actionName == SQX_Training_Session.PurposeOfSignature_ReopeningTrainingSession) {
                    if(SQX_Training_Session.canReopen(mainRecord)){
                        result = ts.reopen(RecordActivityComment);
                    }
                }
                else if(actionName == SQX_Training_Session.PurposeOfSignature_ClosingTrainingSession) {
                    if(SQX_Training_Session.canClose(mainRecord)){
                        result = ts.close(RecordActivityComment);
                    }
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_PREVENT_CHANGING_TRAINING_SESSION));
                }
                if(result != null && result.isSuccess()){
                    ref = new ApexPages.StandardController(mainRecord).view();
                }
            }catch(Exception ex){
                if(sp != null){
                    Database.rollback(sp);
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        return ref;
    }
    
    /**
     * method to check if action is valid for training session
     * @return <code>true</code> is action is valid, else <code>false</code>
     */
    public Boolean getIsActionValid(){
        Boolean isValid = false;
        if(purposeOfSigMap.containsKey(actionName)){
            isValid = true;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_PREVENT_CHANGING_TRAINING_SESSION));
        }
        return isValid;
    }
    
    /**
    * set controller for rosters
    */
    ApexPages.StandardSetController ctlrRoster = null;
    
    /**
    * initiates and returns set controller to list rosters when closing a session to avoid VF page collection size exception
    */
    public ApexPages.StandardSetController getRosterSetController() {
        if (ctlrRoster == null && actionName == SQX_Training_Session.PurposeOfSignature_ClosingTrainingSession
            && mainRecord.Total_Trainees__c <= SF_LIMIT_QUERY_LOCATOR_ROWS) {
            // set rosters set controller
            ctlrRoster = new ApexPages.StandardSetController(Database.getQueryLocator([
                SELECT Id, SQX_Personnel__c, Personnel_Name__c, Result__c
                FROM SQX_Training_Session_Roster__c
                WHERE SQX_Training_Session__c = :mainRecord.Id
                ORDER BY Result__c, Personnel_Name__c, SQX_Personnel__c
            ]));
            
            ctlrRoster.setPageSize(DEFAULT_PAGE_SIZE);
        }
        
        return ctlrRoster;
    }
    
    /**
    * returns user signed off status of all roster trainees (uses ctlrRoster) on the subject when closing a session
    */
    public Map<Id, String> getUserSignOffStatuses() {
        Map<Id, String> userSignedOffMap = new Map<Id, String>();
        
        if (getRosterSetController() != null) {
            // list all personnels with default value as empty string so that VF page works fine
            for (SQX_Training_Session_Roster__c roster : (SQX_Training_Session_Roster__c[])ctlrRoster.getRecords()) {
                userSignedOffMap.put(roster.SQX_Personnel__c, '');
            }
            
            if (mainRecord.SQX_Controlled_Document__c != null && userSignedOffMap.size() > 0) {
                // get training statuses of the personnel for the training session
                for (AggregateResult res : [SELECT  SQX_Personnel__c psnId,
                                                    Status__c trainingStatus
                                            FROM SQX_Personnel_Document_Training__c 
                                            WHERE SQX_Training_Session__c =: mainRecord.Id
                                                AND SQX_Personnel__c IN :userSignedOffMap.keySet()
                                                AND Status__c != :SQX_Personnel_Document_Training.STATUS_OBSOLETE
                                                AND SQX_Controlled_Document__c =: mainRecord.SQX_Controlled_Document__c
                                            GROUP BY SQX_Personnel__c, Status__c]) {
                    Id psnId = (Id)res.get('psnId');
                    String trainingStatus = (String)res.get('trainingStatus');
                    
                    if (trainingStatus == SQX_Personnel_Document_Training.STATUS_PENDING) {
                        // user sign off is pending on some or all trainings
                        userSignedOffMap.put(psnId, VALUE_FALSE);
                    }
                    else if (userSignedOffMap.get(psnId) != VALUE_FALSE) {
                        // personnel has user signed off or completed training and no trainings pending user sign off
                        userSignedOffMap.put(psnId, VALUE_TRUE);
                    }
                }
            }
        }
        
        return userSignedOffMap;
    }
    
}