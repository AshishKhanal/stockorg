/**
 *  Class represents the response of a eContent operations
*/
global class SQX_eContent_Response {

    /**
    *   This property holds the registration identifer
    */
    global String regId { get; set; }

    /**
    *   This property specifies whether the content has been completed(i.e finished viewing or finished working on)
    */
    global Boolean complete { get; set; }

    /**
     *  This property specifies whether the content has been successfully completed
    */
    global Boolean successful { get; set; }

    /**
    *   This property holds the result of the registration
    */
    global Object result { get; set; } /* holds the result of a given registration */

}