/**
 * Extension class for processing eContent related actions(preview, launch)
*/
public with sharing class SQX_Controller_eContent extends SQX_Controller_Sign_Off{

    final public static String PARAM_ID = 'id';

    public Id CourseId {get; set;}

    public Id ContentId {get; set;}

    public Boolean CanSignOff {get; set;}

    public Boolean IsPassed {get; set;}


    /*
        Activity codes for sign off
    */
    public final static String CODE_OF_ESIG_SCORM_DOC_TRAINING_SIGN_OFF = 'scorm_training_signoff',
                               CODE_OF_ESIG_SCORM_ASSESSMENT_SIGN_OFF = 'scorm_assessment_signoff';

    /*
        All Purpose of Signatures
    */
    final Map<String, String> psMap = new Map<String, String> {
        CODE_OF_ESIG_SCORM_DOC_TRAINING_SIGN_OFF => 'SCORM Training Sign Off',
        CODE_OF_ESIG_SCORM_ASSESSMENT_SIGN_OFF => 'SCORM Assessment Sign Off'
    };


    /**
     * Method to set purpose of sig
     * @param isAssessmentSignOff if assessment is played, the method adds assessment related purpose of sig else adds training related purpose of sig
     */
    public SQX_Controller_eContent setPurposeOfSig(Boolean isAssessmentSignOff) {

        this.actionName = isAssessmentSignOff ? CODE_OF_ESIG_SCORM_ASSESSMENT_SIGN_OFF : CODE_OF_ESIG_SCORM_DOC_TRAINING_SIGN_OFF;

        this.purposeOfSignature = psMap.get(this.actionName);

        this.purposeOfSigMap.put(this.actionName, this.purposeOfSignature);

        return this;
    }

    public SQX_Controller_eContent(String recordIdParam) {
        super(null);

        try{
            if(!String.isEmpty(recordIdParam)){

                this.recordId = Id.valueOf(recordIdParam);

                Schema.SObjectType recordType = recordId.getSObjectType();

                if(recordType == SQX_Controlled_Document__c.SObjectType){
                    String contentDocId = [SELECT Id,Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: recordId].Content_Reference__c;
                    ContentId = [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: contentDocId AND IsLatest = true].Id;
                }
                else if(recordType == SQX_Assessment__c.SObjectType){
                    String contentDocId = ContentId = [SELECT Id, Scorm_Content_Reference__c FROM SQX_Assessment__c WHERE Id =: recordId].Scorm_Content_Reference__c;
                    ContentId = [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: contentDocId AND IsLatest = true].Id;
                }
                else if(recordType == SQX_Personnel_Document_Training__c.SObjectType){

                    SQX_Personnel_Document_Training__c tr = [SELECT Status__c, SQX_Controlled_Document__r.Is_Scorm_Content__c, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Content_Reference__c,
                                                                     SQX_Personnel__r.SQX_User__c, Can_Take_Assessment__c, SQX_Training_Session__c, Is_Training_Session_Complete__c
                                                             FROM SQX_Personnel_Document_Training__c WHERE Id =: recordId];

                    if(tr.Status__c == SQX_Personnel_Document_Training.STATUS_PENDING && tr.SQX_Controlled_Document__r.Is_Scorm_Content__c) {
                        Boolean canPerformAction = (tr.SQX_Personnel__r.SQX_User__c == UserInfo.getUserId());
                        
                        if(!canPerformAction) { throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD); }
                        
                        CourseId = tr.Id;
                        ContentId = [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: tr.SQX_Controlled_Document__r.Content_Reference__c AND IsLatest = true].Id;
                        CanSignOff = !tr.Can_Take_Assessment__c && (String.isBlank(tr.SQX_Training_Session__c) || tr.Is_Training_Session_Complete__c );
                        
                        this.recordId = tr.Id;
                        setPurposeOfSig(false);
                    }
                }
                else if(recordType == SQX_Personnel_Assessment__c.SObjectType){

                    SQX_Personnel_Assessment__c pas = new PrivilegeEscalator().getPersonnelAssessment(recordID);

                    Boolean canPerformAction = (pas.SQX_Personnel__r.SQX_User__c == UserInfo.getUserId());

                    if(!canPerformAction) { throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD); }

                    CourseId = pas.Id;

                    // esclating privilege to view SCORM Assessment content since it is private and is visible only to assessment owner
                    ContentId = new PrivilegeEscalator().getContentVersion(pas.SQX_Assessment__r.Scorm_Content_Reference__c).Id;
                    CanSignOff = true;

                    this.recordId = pas.Id;
                    setPurposeOfSig(true);
                }
                else{
                    recordId = null;
                    throw new SQX_ApplicationGenericException(String.format(Label.SQX_ERR_MSG_UNSUPPORTED_SOBJECT, new List<String> { recordType.getDescribe().getName() }));
                }
            }

        }catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }

    }


    public SQX_Controller_eContent(){
        this(ApexPages.currentPage().getParameters().get(PARAM_ID));
    }


    /**
    *   Returns the url for previewing eContent
    *   @param recordId identifier for the content to be previewed
    */
    public static String getPreviewUrl(String recordId){

        PageReference pr = Page.SQX_eContent;
        pr.getParameters().put(PARAM_ID, recordId);

        return pr.getUrl();

    }

    /**
    * This method helps in signing off the assessment or course
     */
    public PageReference signOff(){
        PageReference retUrl = null;
        try
        {
            if(recordId.getSObjectType() == SQX_Personnel_Document_Training__c.SObjectType) {
                List<SQX_Personnel_Document_Training__c> dtn = new List<SQX_Personnel_Document_Training__c>{
                            new SQX_Personnel_Document_Training__c(Id = this.recordId)
                                };

                ApexPages.StandardSetController ss = new ApexPages.StandardSetController(dtn);
                ss.setSelected(dtn);

                SQX_Extension_PDT_User_SignOff ext = new SQX_Extension_PDT_User_SignOff(ss);
                ext.SigningOffUsername = this.SigningOffUsername;
                ext.SigningOffPassword = this.SigningOffPassword;
                ext.SigningOffComment = this.RecordActivityComment;
                ext.signOff(true);
            }
            else if(recordId.getSObjectType() == SQX_Personnel_Assessment__c.SObjectType){
                // personnel assessment
                Id assessmentId = [SELECT SQX_Assessment__c FROM SQX_Personnel_Assessment__c WHERE Id = :recordId].SQX_Assessment__c;

                SQX_Controller_Take_Assessment cta = new SQX_Controller_Take_Assessment(assessmentId);

                cta.purposeOfSigMap = this.purposeOfSigMap;
                cta.actionName = this.actionName;
                cta.purposeOfSignature = this.purposeOfSignature;

                cta.SigningOffUsername = this.SigningOffUsername;
                cta.SigningOffPassword = this.SigningOffPassword;
                cta.RecordActivityComment = this.RecordActivityComment;
                cta.lastvalidAttempt.Result__c = IsPassed ? SQX_Assessment.ASSESSMENT_RESULT_SATISFACTORY : SQX_Assessment.ASSESSMENT_RESULT_UNSATISFACTORY;
                cta.submitAssessmentResult();
            }

            if(ApexPages.getMessages().size() == 0) {
                retUrl = new PageReference(SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));
            }
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage() + ex.getStackTraceString()));
        }

        return retUrl;

    }

    /**
    * Inner class for previlege escalating
    */
    private without sharing class PrivilegeEscalator
    {
        public ContentVersion getContentVersion(Id contentDocumentId)
        {
            return [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: contentDocumentId AND IsLatest = true];
        }

        public SQX_Personnel_Assessment__c getPersonnelAssessment(Id recordId) {
            return [SELECT Id, SQX_Assessment__c, SQX_Assessment__r.Scorm_Content_Reference__c,
                                         SQX_Personnel__r.SQX_User__c, SQX_Personnel__r.Identification_Number__c,
                                         SQX_Last_Assessment_Attempt__c, SQX_Last_Assessment_Attempt__r.Result__c FROM SQX_Personnel_Assessment__c WHERE Id =: recordId];
        }
    }

}