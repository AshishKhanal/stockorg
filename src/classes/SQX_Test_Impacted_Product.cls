@IsTest
public class SQX_Test_Impacted_Product{

    public testmethod static void givenImpactedProductExists_ItCantBeDuplicated(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
    
            //setup
            SQX_Test_Finding findingContainer = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
            .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
            .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
            .setStatus(SQX_Finding.STATUS_DRAFT)
            .save();
            
            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random');
            insert partFamily;
            
            integer randomNumber = (Integer)(Math.random() * 1000000);
            SQX_Part__c part = new SQX_Part__c(Part_Family__c = partFamily.Id,
                                              Name = '123',
                                               Part_Number__c = 'Random' + randomNumber,
                                               Part_Risk_Level__c = 3,
                                               Active__c = true);
            
            insert part;
            
            
            SQX_Impacted_Product__c impactedProduct = new SQX_Impacted_Product__c(SQX_Impacted_Part__c = part.Id,
                                                                            SQX_Finding__c = findingContainer.finding.Id);
            
            insert impactedProduct;
            
            //lets try inserting an impacted customer again
            impactedProduct.Id = null;
            
            Database.SaveResult result = Database.insert(impactedProduct, false);
            System.assert(result.isSuccess() == false, 'Expected duplicate to be prevented');
        
        }
    }
}