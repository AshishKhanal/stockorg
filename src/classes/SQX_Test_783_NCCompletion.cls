/**
* This test ensures that impacted part update, updates the disposition
* additionally that when disposition is completed actual disposed is set to disposed qty
* disposed qty in impacted product is updated when disposition is completed.
*/
@IsTest
public with sharing class SQX_Test_783_NCCompletion {
    
    static boolean  runAllTests = true,
    				run_givenImpactePartLotIsChanged_DispositionIsUpdated = false,
    				run_givenDispositionISCompleted_QtyDisposedIsUpdated = false;

    /**
    * This test ensures that if impacted part lot has changed then it is updated in disposition too.
    */
    public testmethod static void givenImpactePartLotIsChanged_DispositionIsUpdated(){
    	
    	if(!runAllTests && !run_givenImpactePartLotIsChanged_DispositionIsUpdated)
    		return;


    	UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        Id impactedProductId = null, dispositionId = null;
        SQX_Part__c part = null;
        
        System.runas(standardUser){
            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
            
            //an nc with only disposition required
            nc = new SQX_Test_NC(true);
            
            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            
            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.SQX_Impacted_Part__c = part.Id;
            product.SQX_Nonconformance__c = nc.nc.Id;
            product.Lot_Number__c = 'asd';
            product.Lot_Quantity__c = 100;
            
            new SQX_DB().op_insert(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_Nonconformance__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c
                    });
            
            nc.nc.OwnerId = ncCoordinator.Id; //ownership transferred because this is what happens using the queue

            nc.save();

            impactedProductId = product.Id;

        }

        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);
            
            response.save();
            
            SQX_Disposition__c disposition = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Open',
                                                         Lot_Number__c = 'Invalid-Lot-Number', 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 100);
            
            try {                                            
                new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                    SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                });

                System.assert(false, 'An exception should have been thrown when inserting the disposition since lot is invalid');
            }
            catch(DmlException ex){
                //SQX-783 invalid part-lot exception, an exception should be thrown that will be ignored
            }


            //SQX-783 lot-number val
            //change the lot-number back to valid one
            disposition.Lot_Number__c = 'asd';

            new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                    SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                });

            System.assert(disposition.Id != null, 'Disposition should have been saved but wasnt');
            System.assert([SELECT Id, SQX_NC_Impacted_Product__c 
                           FROM SQX_Disposition__c 
                           WHERE Id = : disposition.Id].SQX_NC_Impacted_Product__c == impactedProductId
                          , 'Impacted product has been set');
            
            
            dispositionId = disposition.Id;

        }
        
        System.runas(ncCoordinator){
            SQX_Part__c newPart = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
            
            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.Id = impactedProductId;
            product.Lot_Number__c = 'LOT-123';
            product.SQX_Impacted_Part__c = newPart.Id;
            
            new SQX_DB().op_update(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c
                    });
            
            SQX_Disposition__c disposition = [SELECT Id, SQX_Part__c, Lot_Number__c
                                              FROM SQX_Disposition__c
                                              WHERE Id = : dispositionId];
            
            // 783: Assertions that lot and part have been updated.
            System.assertEquals(newPart.Id, disposition.SQX_Part__c);
            System.assertEquals(product.Lot_Number__c, disposition.Lot_Number__c);
        }


    }



    public testmethod static void givenDispositionISCompleted_QtyDisposedIsUpdated(){
    	
    	if(!runAllTests && !run_givenDispositionISCompleted_QtyDisposedIsUpdated)
    		return;


    	UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        Id impactedProductId = null, dispositionId = null;
        SQX_Part__c part = null;
        
        System.runas(standardUser){
            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
            
            //an nc with only disposition required
            nc = new SQX_Test_NC(true);
            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            
            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.SQX_Impacted_Part__c = part.Id;
            product.SQX_Nonconformance__c = nc.nc.Id;
            product.Lot_Number__c = 'LOT-123';
            product.Lot_Quantity__c = 100;
            
            new SQX_DB().op_insert(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_Nonconformance__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c
                    });
            
            nc.nc.OwnerId = ncCoordinator.Id; //ownership transferred because this is what happens using the queue
            
            nc.save();
            
            impactedProductId = product.Id;
            
        }
        
        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);
            
            response.save();
            
            SQX_Disposition__c disposition1 = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Open',
                                                         Lot_Number__c = 'LOT-123', 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 50),
            				   disposition2 = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Complete',
                                                         Lot_Number__c = 'LOT-123', 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 50,
                                                         Completed_On__c = Date.today().addDays(-12),
                                                         Completed_By__c = 'PRB',
                                                         Comment__c = 'Done');
            
            new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition1, disposition2}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                    });
            
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition1);
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition2);
            response.publishResponse();
            
            SQX_NC_Impacted_Product__c impactedProduct = [SELECT Id, Total_Quantity_Disposed__c 
                                                          FROM SQX_NC_Impacted_Product__c
                                                          WHERE Id = : impactedProductId];
            
            //Assert: assert that impacted product's total quantity disposed is set to the completed disposition quantity
			System.assertEquals(50, impactedProduct.Total_Quantity_Disposed__c);

			disposition1.New_Status__c = 'Complete';
			disposition1.Completed_On__c = Date.today().addDays(-5);
			disposition1.Completed_By__c = 'PRB';
			disposition1.Comment__c = 'Done';

			new SQX_DB().op_update(new List<SQX_Disposition__c>{disposition1}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                    });
            
            
            SQX_Test_Finding_Response response2 = new SQX_Test_Finding_Response(nc.nc);
            response2.save();
            
            
            response2.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition1);
            response2.publishResponse();
            
            
            impactedProduct = [SELECT Id, Total_Quantity_Disposed__c 
                               FROM SQX_NC_Impacted_Product__c
                               WHERE Id = : impactedProductId];
            
            //Assert: assert that impacted product's total quantity disposed is set to the total of completed disposition quantity
            System.assertEquals(100, impactedProduct.Total_Quantity_Disposed__c);
            
            
            
            //Assert: Actual disposed is set to Total Disposed in Disposition
            
            List<SQX_Disposition__c> dispositions = [SELECT Id, Actual_Quantity_Disposed__c, Disposition_Quantity__c FROM SQX_Disposition__c
                                                     WHERE ID = : disposition1.Id OR ID = : disposition2.Id ]; 
            
            System.assertEquals(dispositions.get(0).Disposition_Quantity__c, dispositions.get(0).Actual_Quantity_Disposed__c );
            System.assertEquals(dispositions.get(1).Disposition_Quantity__c, dispositions.get(1).Actual_Quantity_Disposed__c );
        }
        
    }
    
}