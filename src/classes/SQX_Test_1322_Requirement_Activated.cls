/**
* Unit tests for creation or modification of document training records when a requirement is activated.
*/
@IsTest
public class SQX_Test_1322_Requirement_Activated {
    // removed tests for obsolete object SQX_Document_Training__c
    // TODO: remove this file while cleaning up
}