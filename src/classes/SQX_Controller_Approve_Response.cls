/**
* controller for the dashboard component, which lists all responses that need approval
* Investigation Approval from the Customer:
*     a.The system does not tell the customer if they are approving Investigation or any other record.
*     b.Investigation Response to be approved displays on the portal page. However, the Items to Approve do not display the Finding number or anything related to Investigation. “Related To” field shows Response Number but the customer would not have known the Response Number. The ideal columns to be displayed:
*     i.  Action
*     ii. Related To – Response No
*     iii.    Parent Record – SCAR No.
*     iv. Supplier against the SCAR was created
*     v.  Type – Investigation or Response/Investigation
*     vi. Most Recent Approver – Not a required column
*     vii.    Title of Finding so the approver knows what they are approving.
*
* @author Pradhanta Bhandari
* @date 2014/4/30
* @requirement SQX-317
*/
public with sharing class SQX_Controller_Approve_Response {
    
    /**
    * gets responses that require approval for the current user.
    * @author Pradhanta Bhandari
    * @date 2014/4/30
    * @return returns a List of finding response and Audit response that requires approval from the running user (Current user)
    */
    public List<ResponseToApprove> getResponseForApproval(){
        
        Set<ID> queue_current_user_IDs= SQX_Utilities.getQueueIdsForUser(UserInfo.getUserId());
            
        queue_current_user_IDs.add(UserInfo.getUserId());

        //1. get the list of all workitems, these are all pending for approval
        List<ProcessInstanceWorkItem> workItems = [SELECT  ProcessInstance.TargetObjectId , ActorID
                                                    FROM ProcessInstanceWorkitem 
                                                    WHERE ActorID in : queue_current_user_IDs];
                                                    
        //2. create a list of all target items that qualify for the purpose
        Schema.SObjectType responseType = new SQX_Finding_Response__c().getSObjectType();
        Schema.SObjectType auditResponseType = new SQX_Audit_Response__c().getSObjectType();

        List<Id> allWorkItemTargetObjects = new List<Id>();

        List<Id> auditWorkItemTargetObjects = new List<Id>();
        Map<Id, ProcessInstanceWorkItem> targetObjectToWorkItem = new Map<Id, ProcessInstanceWorkItem>();

        for(ProcessInstanceWorkItem workItem : workItems){
            if(workItem.ProcessInstance.TargetObjectId.getSObjectType() == responseType){
                allWorkItemTargetObjects.add(workItem.ProcessInstance.TargetObjectId);
            }
            else if(workItem.ProcessInstance.TargetObjectId.getSObjectType() == auditResponseType) {
                auditWorkItemTargetObjects.add(workItem.ProcessInstance.TargetObjectId);
            }

            targetObjectToWorkItem.put(workItem.ProcessInstance.TargetObjectId, workItem);
        }
        
        // creating list of SObject type
        List<ResponseToApprove> allResponses = new List<ResponseToApprove>();


        // list retrieving required data from finding response object
        for(SQX_Finding_Response__c response : [SELECT Id, Name,Response_Summary__c, CAPA_Id__c, CAPA_Name__c
, Submitted_On__c,Submitted_By__c, SQX_Response_Approver__c, SQX_Response_Approver__r.Name,
        SQX_Finding__r.Name, SQX_Finding__r.Id, SQX_Finding__r.SQX_Supplier_Account__c,
        SQX_Finding__r.SQX_Supplier_Account__r.Name, SQX_Finding__r.Title__c,

        SQX_CAPA__r.Name, SQX_CAPA__r.Id, SQX_CAPA__r.SQX_Account__c,
        SQX_CAPA__r.SQX_Account__r.Name, SQX_CAPA__r.Title__c, SQX_CAPA__r.Target_Due_Date__c,

        SQX_NonConformance__r.Name, SQX_NonConformance__r.Id, SQX_NonConformance__r.SQX_Account__c,
        SQX_NonConformance__r.SQX_Account__r.Name, SQX_NonConformance__r.NC_Title__c

        FROM SQX_Finding_Response__c WHERE Id IN : allWorkItemTargetObjects and SQX_Finding__r.Status__c!= :SQX_Finding.STATUS_CLOSED ORDER BY Submitted_On__c DESC  NULLS LAST]){

            allResponses.add(new ResponseToApprove(response, targetObjectToWorkItem.get(response.Id)));

        }


        // list retrieving required data from audit response object
        for(SQX_Audit_Response__c response : [SELECT Id, Name, Submitted_On__c, Submitted_By__c, Response_Approver__c, Response_Approver__r.Name, SQX_Audit__r.Name, SQX_Audit__r.Id, SQX_Audit__r.Account__r.Name, Audit_Response_Summary__c, SQX_Audit__c, SQX_Audit__r.Title__c
        FROM SQX_Audit_Response__c WHERE Id IN : auditWorkItemTargetObjects  ORDER BY Submitted_On__c DESC  NULLS LAST]){
            allResponses.add(new ResponseToApprove(response, targetObjectToWorkItem.get(response.Id)));
        }

        return allResponses; 

    }

    /**
    * Wrapper class used to provide the response and its actor
    */
    public with sharing class ResponseToApprove {
        //the wrapped work item
        public ProcessInstanceWorkItem workItem {get; set;}

        public SObject relatedItem {get; set;}

        public SQX_CAPA__c capa {get; set;}

        public ResponseToApprove(){

        }

        public ResponseToApprove(SObject relatedItem, ProcessInstanceWorkItem workItem){
            this.relatedItem = relatedItem;
            this.workItem = workItem;
            SQX_Finding_Response__c response = relatedItem instanceof SQX_Finding_Response__c ? (SQX_Finding_Response__c) relatedItem : null;
            capa = response != null ? response.SQX_CAPA__r : null;
        }


    }
    
}