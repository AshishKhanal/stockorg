@isTest
public class SQX_Test_1967_Controlled_Doc_Expire {

    static Boolean runAllTests = true,
                   run_givenADoc_WhenDocIsPreExpired_ExpirationDateMustBeSet = false,
                   run_givenADoc_WhenDocumentReviewIsRetire_DocumentShouldBeCurRent = false,
                   run_givenADoc_WhenDocumentReviewIsContinueUse_DocumentShouldBeCurrent = false,
                   run_givenADoc_WhenExpirationDateIsToday_DocumentShouldBeExpire = false,
                   run_givenADoc_WhenDocReivewIsRetire_ExpirationDateMustBeSet = false,
                   run_givenADoc_WhenDocIsApproved_DocCanBeReleasedManually = false,
                   run_givenADoc_WhenDocIsRevisedFromPreReleaseAndReleased_PreviousVersionMustBeObsolete = false,
                   run_givenADoc_WhenDocIsRevisedFromPreExpireAndReleased_PreviousVersionMustBeObsolete = false,
                   run_givenADoc_WhenDocReivewIsRetireAndContinueToUse_NoErrorIsThrown = false;
                   
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * Scenario 1:
     * ACTION: Pre-Expire the doc without adding expiration date
     * EXPECTED: Doc should not be saved and should throw an error.
     *
     * Scenario 2:
     * ACTION: Pre-Expire the doc adding expiration date
     * EXPECTED: Doc should be saved.
     * @date: 2016-02-08
     * @story: [SQX-1967]
    */
    public testMethod static void givenADoc_WhenDocIsPreExpired_ExpirationDateMustBeSet(){
        if (!runAllTests && !run_givenADoc_WhenDocIsPreExpired_ExpirationDateMustBeSet) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Expiration_Date__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act 1: Set document status to pre-expire and expiration date to null
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            controlledDoc.Expiration_Date__c = null;

            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            // Assert 1: Expected the doc not to be saved
            System.assertEquals(false, docStatus.isSuccess(), 'Expected the doc not to be saved');

            // Act 2: Set expiration date and save controlled doc
            controlledDoc.Expiration_Date__c = Date.Today().addDays(15);

            docStatus =  Database.update(controlledDoc, false);

            // Assert 2: Expected doc to be saved.
            System.assertEquals(true, docStatus.isSuccess(), 'Expected the doc to be saved but found errors: ' + docStatus.getErrors());

        }
    }

    /**
     * Scenario 1:
     * ACTION: Controlled Doc is current and document review decision is Retire
     * EXPECTED: Doc Status to be in current
     *
     * @date: 2016-02-08
     * @story: [SQX-1967] , modified in [SQX-3639]
    */
    public testMethod static void givenADoc_WhenDocumentReviewIsRetire_DocumentShouldBeCurrent(){
        if (!runAllTests && !run_givenADoc_WhenDocumentReviewIsRetire_DocumentShouldBeCurRent) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Approval_Status__c, 
                                                               Expiration_Date__c  FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act : Set document status to current
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            SQX_Document_Review__c docReview = new SQX_Document_Review__c(
                                                        Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                                                        Controlled_Document__c = cDoc1.doc.Id,
                                                        Performed_Date__c = Date.Today(),
                                                        Expiration_Date__c  = Date.Today().addDays(10));

            Database.SaveResult reviewDocStatus =  Database.insert(docReview, false);
            System.assertEquals(true, reviewDocStatus.isSuccess() , 'Expected the document review to be saved but error occured with error: ' + reviewDocStatus.getErrors());
            
            controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            // Assert 1: Expected doc status to be current
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c, 'Expected the doucment status to be current but found: ' + controlledDoc.Document_Status__c);

        }
    }

    /**
     * Scenario 1:
     * ACTION: Controlled Doc is pre-expire and document review decision is continue use
     * EXPECTED: Doc Status to be current
     *
     * @date: 2016-02-08
     * @story: [SQX-1967]
    */
    public testMethod static void givenADoc_WhenDocumentReviewIsContinueUse_DocumentShouldBeCurrent(){
        if (!runAllTests && !run_givenADoc_WhenDocumentReviewIsContinueUse_DocumentShouldBeCurrent) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Approval_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act 2: Set document status to pre-expire
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;

            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            SQX_Document_Review__c docReview = new SQX_Document_Review__c(
                                                        Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE,
                                                        Controlled_Document__c = cDoc1.doc.Id,
                                                        Expiration_Date__c  = Date.Today().addDays(10),
                                                        Performed_Date__c = Date.Today(),
                                                        Next_Review_Date__c = Date.Today()
                                                        );

            Database.SaveResult reviewDocStatus =  Database.insert(docReview, false);
            System.assertEquals(true, reviewDocStatus.isSuccess() , 'Expected the document review to be saved but error occured with error: ' + reviewDocStatus.getErrors());
            
            controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Assert 1: Expected doc status to be current
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c, 'Expected the doucment status to be current but found: ' + controlledDoc.Document_Status__c);

        }
    }

    /**
     * Scenario 1:
     * ACTION: Controlled Doc is pre-expire and expiration date is today
     * EXPECTED: Doc Status to be expire
     *
     * @date: 2016-02-08
     * @story: [SQX-1967]
    */
    public testMethod static void givenADoc_WhenExpirationDateIsToday_DocumentShouldBeExpire(){
        if (!runAllTests && !run_givenADoc_WhenExpirationDateIsToday_DocumentShouldBeExpire) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Expiration_Date__c  FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act 2: Set document status to pre-expire and expiration date to today
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            controlledDoc.Expiration_Date__c  = Date.Today();


            Database.SaveResult docStatus =  Database.update(controlledDoc, false);
            System.assertEquals(true, docStatus.isSuccess(), 'Expected the doc to be updated sucessfully but found error: ' + docStatus.getErrors());

            controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            // Assert 1: Expected doc status to be expire
            System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, controlledDoc.Document_Status__c, 'Expected the doucment status to be obsolete but found: ' + controlledDoc.Document_Status__c);

        }
    }

    /**
     * Scenario 1:
     * ACTION: Save document with review decsion as retire without adding expiration date
     * EXPECTED: Doc Review should not be saved and should throw an error.
     *
     * Scenario 2:
     * ACTION: Save document with review decsion as retire by adding expiration date
     * EXPECTED: Doc Review should be saved
     * @date: 2016-02-08
     * @story: [SQX-1967]
    */
    public testMethod static void givenADoc_WhenDocReivewIsRetire_ExpirationDateMustBeSet(){
        if (!runAllTests && !run_givenADoc_WhenDocReivewIsRetire_ExpirationDateMustBeSet) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Approval_Status__c, 
                                                               Expiration_Date__c  FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act 1: Retire the document review without entering expiration date.
            SQX_Document_Review__c docReview = new SQX_Document_Review__c(
                                                        Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                                                        Controlled_Document__c = cDoc1.doc.Id,
                                                        Performed_Date__c = Date.Today());

            Database.SaveResult reviewDocStatus =  Database.insert(docReview, false);
            
            // Assert 1: Review should not be saved
            System.assertEquals(false, reviewDocStatus.isSuccess() , 'Expected the document review not to be saved.');
            
            // Act 2: Retire the document entering the expiration date
            docReview = new SQX_Document_Review__c(
                                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                                Controlled_Document__c = cDoc1.doc.Id,
                                Performed_Date__c = Date.Today(),
                                Expiration_Date__c = Date.Today().addDays(10));

            reviewDocStatus =  Database.insert(docReview, false);

            // Assert 2: Document should be saved.
            System.assertEquals(true, reviewDocStatus.isSuccess() , 'Expected the document review to be saved.');
            
            controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c, 'Expected the doucment status to be current but found: ' + controlledDoc.Document_Status__c);

        }
    }

    /**
     * Scenario 1
     * ACTION: When the doc is released, doc can be released manually by setting effective date to future
     * EXPECTED: Doc can be released manually and its doc status will be pre-release
     *
     * Scenario 2:
     * ACTION: When the doc is released, doc can be released manually by setting effective date to today
     * EXPECTED: Doc can be released manually and its doc status will be current
     *
     * @date: 2016-02-08
     * @story: [SQX-1967]
    */
    public testMethod static void givenADoc_WhenDocIsApproved_DocCanBeReleasedManually(){
        if (!runAllTests && !run_givenADoc_WhenDocIsApproved_DocCanBeReleasedManually) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Approval_Status__c, 
                                                               Auto_Release__c,
                                                               Effective_Date__c  FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            //SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(controlledDoc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            // Act 1 : Set document status to approved and effective date of future
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
            controlledDoc.Auto_Release__c = false;
            controlledDoc.Effective_Date__c = Date.Today().addDays(5);
            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            extension.Release();
            
            controlledDoc =  [SELECT Document_Status__c, Effective_Date__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : controlledDoc.Id];


            //Assert: Expected the status of doc to be pre-release
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_RELEASE, controlledDoc.Document_Status__c , 'Expected the document to be in pre-release state after doc is released but found ' + controlledDoc.Document_Status__c);
        

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act : Set effective date to less than today
            controlledDoc.Date_Issued__c = Date.Today().addDays(-1);
            controlledDoc.Effective_Date__c = Date.Today().addDays(-1);
            docStatus =  Database.update(controlledDoc, false);

            System.assert(docStatus.isSuccess(), 'Doc update failed' + docStatus.getErrors());
            
            controlledDoc =  [SELECT Document_Status__c, Effective_Date__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : controlledDoc.Id];

            //Assert:  Expected the status of doc to be current
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c , 'Expected the document to be in current state after doc is released but found ' + controlledDoc.Document_Status__c);
        
        }
    }

    /**
     * Scenario 1:
     * ACTION: Pre-Release the doc and revise the doc and relase the newly created doc
     * EXPECTED: Previous Doc should be obsolete.
     *
     * @date: 2016-02-25
     * @story: [SQX-2016]
    */
    public testMethod static void givenADoc_WhenDocIsRevisedFromPreReleaseAndReleased_PreviousVersionMustBeObsolete(){
        if (!runAllTests && !run_givenADoc_WhenDocIsRevisedFromPreReleaseAndReleased_PreviousVersionMustBeObsolete) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create the doc, pre-release the doc and revise the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                        .save();

            cDoc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            cDoc1.doc.Effective_Date__c = Date.Today().addDays(10);
            cDoc1.save();

            cDoc1.synchronize();
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_RELEASE, cDoc1.doc.Document_Status__c, 'Expected the doc to be in pre-release status');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act 1: Create the revision of the doc and pre release the doc
            SQX_Test_Controlled_Document cDoc2 = cDoc1.revise();
            cDoc2.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            cDoc2.synchronize(); 
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //release the document
            cDoc2.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            cDoc2.save();

            // Assert 1 : Expected the previous doc to be obsolete
            cDoc1.synchronize();
            System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, cDoc1.doc.Document_Status__c, 'Expected the document to be obsolete.');
        }
    }

    /**
     * Scenario 1:
     * ACTION: Pre-Expire the doc and revise the doc and relase the newly created doc
     * EXPECTED: Previous Doc should be obsolete.
     *
     * @date: 2016-02-25
     * @story: [SQX-2016]
    */
    public testMethod static void givenADoc_WhenDocIsRevisedFromPreExpireAndReleased_PreviousVersionMustBeObsolete(){
        if (!runAllTests && !run_givenADoc_WhenDocIsRevisedFromPreExpireAndReleased_PreviousVersionMustBeObsolete) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create the doc, pre-release the doc and revise the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                        .setStatus(SQX_Controlled_Document.STATUS_DRAFT)
                                                        .save();

            cDoc1.doc.Revision__c = '1';
            cDoc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            cDoc1.doc.Expiration_Date__c = Date.Today().addDays(10);
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc1 = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_EXPIRE, controlledDoc1.Document_Status__c, 'Expected the doc to be in pre-expire status');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act 1: Create the rivision of the doc and release the doc
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document()
                                                        .setStatus(SQX_Controlled_Document.STATUS_DRAFT)
                                                        .save();


            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            cDoc2.doc.Revision__c = '2';
            cDoc2.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            cDoc2.doc.Document_Number__c = cDoc1.doc.Document_Number__c;
            cDoc2.save();

            // Assert 1 : Expected the previous doc to be obsolete
            controlledDoc1 = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, controlledDoc1.Document_Status__c, 'Expected the document to be obsolete.');


        }
    }

    /**
     * Scenario 1:
     * ACTION: Create a controlled doc and add requriement and release the document
     *          Deactivate the requirement, add document review to pre-expire the document 
     *          and then again add document review to continue to use the document
     * EXPECTED: No error is thrown 
     * @story: [SQX-2154]
    */
    public testMethod static void givenADoc_WhenDocReivewIsRetireAndContinueToUse_NoErrorIsThrown(){
        if (!runAllTests && !run_givenADoc_WhenDocReivewIsRetireAndContinueToUse_NoErrorIsThrown) {
            return;
        }
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            // Arrange: Create a controlled doc and add requriement and release the document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            Database.SaveResult jf1Result = Database.insert(jf1, false);

            // adding requirement
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND

            );
            Database.SaveResult result1 = Database.insert(Req1, false);
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for draft document.');

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // setting document status to current
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.save();

            // deactivate the requirement
            Req1.Active__c = false;
            update Req1;

            // add document review to retire the document
            SQX_Document_Review__c docReview = new SQX_Document_Review__c(
                                                        Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                                                        Controlled_Document__c = cDoc1.doc.Id,
                                                        Performed_Date__c = Date.Today(),
                                                        Expiration_Date__c = Date.Today().addDays(10));

            Database.SaveResult reviewDocStatus =  Database.insert(docReview, false);
            
            System.assertEquals(true, reviewDocStatus.isSuccess() , 'Expected the document review  to be saved.');
            
            // Act 1: Add document review to continue to use the doucment
            docReview = new SQX_Document_Review__c(
                                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE,
                                Controlled_Document__c = cDoc1.doc.Id,
                                Performed_Date__c = Date.Today(),
                                Next_Review_Date__c =  Date.Today().addDays(30));

            reviewDocStatus =  Database.insert(docReview, false);

            // Assert 1: Document Review should be saved.
            System.assertEquals(true, reviewDocStatus.isSuccess() , 'Expected the document review to be saved.');
            
            // Assert : Document status should be current
            SQX_Controlled_Document__c controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c, 'Expected the doucment status to be current but found: ' + controlledDoc.Document_Status__c);

        }
    }
    /**
    * Given: A controlled document which is in pre-expire with expiration set
    * When: A review of type "Continue use" is added
    * Then: the expiration date on controlled doc is cleared
    * @author:  Sajal Joshi
    * @date: 2016/8/11
    * @story: [SQX-2424]
    */
    testmethod static void WhenAContinueUseReviewIsAdded_ThenTheExpirationDateIsCleared(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runas(standardUser){ 
            // Arrange: Create a controlled document (doc1) and pre-expire it with "Retire" review
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save(); 
            doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_EXPIRE);
            doc1.doc.Expiration_Date__c = System.Today()+10;
            doc1.save();
            
            // Act: Add a review of type "Continue use"
            SQX_Document_Review__c rev1 = new SQX_Document_Review__c();
            rev1.Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE;
            rev1.Performed_Date__c = System.Today();
            rev1.Controlled_Document__c = doc1.doc.Id;
            rev1.Next_Review_Date__c = System.today();       
            insert rev1;
            
            // Assert: Ensure that the expiration date was cleared
            doc1.synchronize();
            System.assertEquals(null, doc1.doc.Expiration_Date__c, 'The Expiration Date of Controlled Document is set to null');
            
        }
    }
}