/**
* Unit tests for adding active personnel job function for an inactive personnel
*
* @author Shailesh Maharjan
* @date 2015/12/02
* 
*/
@IsTest
public class SQX_Test_1774_Personnel_Job_Function {
    static Boolean runAllTests = true,
                   run_givenInactivePersonnel_ActivePersonnelJobFunctionNotSaved = false;
    
    private static Boolean checkErrorMessage(Database.Error[] errs, String errmsg) {
        for (Database.Error err : errs) {
            if (err.getMessage().contains(errmsg)) {
                return true;
            }
        }
        return false;
    }

    /**
    * This Test ensures that active personnel job function cannot be adding for an inactive personnel.
    */
    public testmethod static void givenInactivePersonnel_ActivePersonnelJobFunctionNotSaved(){
        if (!runAllTests && !run_givenInactivePersonnel_ActivePersonnelJobFunctionNotSaved){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for PJF test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);

            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for PJF test' );
            jfResult = Database.insert(jf2, false);

            // add required personnel
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.mainRecord.Active__c = false;
            p.save();

            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf1
            Database.SaveResult result1 = Database.insert(pjf1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected new inactive Personnel job function to be saved.');

            // ACT: activate pjf1
            pjf1.Active__c = true;
            result1 = Database.update(pjf1, false);
            
            System.assert(result1.isSuccess() == false,
                'Expected Personnel job function not to activate for inactive personnel.');

            System.assert(checkErrorMessage(result1.getErrors(), 'You cannot activate Personnel Job Function for an inactive Personnel.'),
                'Expected error message "You cannot activate Personnel Job Function for an inactive Personnel."');
            
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf2.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = true
            );

            // ACT: new active personnel job function
            result1 = Database.insert(pjf2, false);
            
            System.assert(result1.isSuccess() == false,
                'Expected Personnel job function not to activate for inactive personnel.');

            System.assert(checkErrorMessage(result1.getErrors(), 'You cannot activate Personnel Job Function for an inactive Personnel.'),
                'Expected error message "You cannot activate Personnel Job Function for an inactive Personnel."');
        }
    }
}