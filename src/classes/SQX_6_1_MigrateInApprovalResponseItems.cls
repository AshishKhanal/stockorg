/**
* Batch processor to recall inclusion items of responses in-approval, updates status and/or approval status to approval pending and lock those records as per new process.
* Added in version  : 6.1.0
* Used for versions : all earlier versions
*/
global with sharing class SQX_6_1_MigrateInApprovalResponseItems implements Database.Batchable<SObject> {
    String recallComment = '';
    
    /**
    * debug message for limits
    */
    void logLimits(boolean isStart) {
        System.debug(String.format('SQX_6_1_MigrateInApprovalResponseItems.execute() {0} limits, cpu time: {1}, soql: {2}/{3}, dml: {4}/{5}',
                                    new String[]{
                                        (isStart ? 'starting' : 'ending'),
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
    }
    
    /**
    * sets comment for all recalled response inclusions
    */
    global void setRecallComment(String comment) {
        this.recallComment = comment;
    }
    
    global SQX_6_1_MigrateInApprovalResponseItems() {
        this.recallComment = 'Recalled using SQX_6_1_MigrateInApprovalResponseItems';
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id FROM SQX_Finding_Response__c WHERE Status__c = \'' + SQX_Finding_Response.STATUS_IN_APPROVAL + '\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> objList) {
        logLimits(true);
        
        if (objList.size() > 0) {
            // list response ids
            Set<Id> objIds = new Map<Id, SObject>(objList).keySet();
            
            // get response inclusions
            List<SQX_Response_Inclusion__c> inclusions = [SELECT Id, SQX_Action__c, SQX_Containment__c, SQX_Disposition__c, SQX_Investigation__c, SQX_Containment__r.Status__c,
                                                                 SQX_Disposition__r.Approval_Status__c, SQX_Disposition__r.Status__c, SQX_Investigation__r.Approval_Status__c, SQX_Investigation__r.Status__c
                                                          FROM SQX_Response_Inclusion__c
                                                          WHERE SQX_Response__c IN :objIds];
            
            if (inclusions.size() > 0) {
                // list inclusion ids to recall
                Map<Id, SObject> objsToRecall = new Map<Id, SObject>();
                for (SQX_Response_Inclusion__c inc : inclusions) {
                    if (inc.SQX_Action__c != null) {
                        // Note: action items do not need to be updated
                        objsToRecall.put(inc.SQX_Action__c, null);
                    }
                    else if (inc.SQX_Containment__c != null) {
                        SQX_Containment__c con = new SQX_Containment__c(
                            Id = inc.SQX_Containment__c,
                            Status__c = inc.SQX_Containment__r.Status__c
                        );
                        objsToRecall.put(con.Id, con);
                    }
                    else if (inc.SQX_Disposition__c != null) {
                        SQX_Disposition__c dis = new SQX_Disposition__c(
                            Id = inc.SQX_Disposition__c,
                            Status__c = inc.SQX_Disposition__r.Status__c,
                            Approval_Status__c = inc.SQX_Disposition__r.Approval_Status__c
                        );
                        objsToRecall.put(dis.Id, dis);
                    }
                    else if (inc.SQX_Investigation__c != null) {
                        SQX_Investigation__c inv = new SQX_Investigation__c(
                            Id = inc.SQX_Investigation__c,
                            Status__c = inc.SQX_Investigation__r.Status__c,
                            Approval_Status__c = inc.SQX_Investigation__r.Approval_Status__c
                        );
                        objsToRecall.put(inv.Id, inv);
                    }
                }
                
                // get pending process instance work items 
                List<ProcessInstance> pendingItems = [SELECT Id, TargetObjectId, (SELECT Id FROM Workitems)
                                                      FROM ProcessInstance
                                                      WHERE TargetObjectId IN :objsToRecall.keySet() AND Status = 'Pending'];
                
                if (pendingItems.size() > 0) {
                    // list pending work items to recall
                    List<Approval.ProcessWorkitemRequest> pendingRequests = new List<Approval.ProcessWorkitemRequest>();
                    final String recallAction = 'Removed';
                    for (ProcessInstance ins : pendingItems) {
                        if (ins.Workitems.size() > 0) {
                            Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
                            req.setWorkItemId(ins.Workitems.get(0).Id);
                            req.setAction(recallAction);
                            req.setComments(this.recallComment);
                            
                            pendingRequests.add(req);
                        }
                    }
                    
                    if (pendingRequests.size() > 0) {
                        // recall approval requests for pending response inclusions
                        List<Approval.ProcessResult> results = Approval.process(pendingRequests, false);
                        
                        // list recalled items Ids
                        List<Id> itemsToLock = new List<Id>();
                        // cluster to avoid SF limit of 10 clusters per update, this makes it easy to send it to CUD, FLS
                        Map<SObjectType, List<SObject>> objsToUpdateMap = new Map<SObjectType, List<SObject>>();
                        for (Approval.ProcessResult res : results) {
                            if (res.isSuccess()) {
                                // update and/or lock successfully recalled inclusion items
                                Id entityId = res.getEntityId();
                                itemsToLock.add(entityId);
                                SObject obj = objsToRecall.get(entityId);
                                if (obj != null) {
                                    SObjectType objType = obj.getSObjectType();
                                    if (objsToUpdateMap.containsKey(objType)) {
                                        objsToUpdateMap.get(objType).add(obj);
                                    } else {
                                        objsToUpdateMap.put(objType, new List<SObject>{ obj });
                                    }
                                }
                            }
                        }
                        
                        if (objsToUpdateMap.size() > 0) {
                            List<SObject> objsToUpdate = new List<SObject>();
                            for (SObjectType objType : objsToUpdateMap.keySet()) {
                                objsToUpdate.addAll(objsToUpdateMap.get(objType));
                            }
                            /**
                            * WITHOUT SHARING has been used 
                            * ---------------------------------
                            * Without sharing has been used because the running user might not have access to update status and/or approval status of the recalled inclusions
                            * This will cause and error.
                            */
                            new SQX_DB().withoutSharing().op_update(objsToUpdate, new List<Schema.SObjectField>{});
                        }
                        
                        if (itemsToLock.size() > 0) {
                            // lock all inclusions of in-approval responses
                            Approval.lock(itemsToLock);
                        }
                        
                        if (itemsToLock.size() != pendingRequests.size()) {
                            System.debug('Failed to recall approval request for some or all response inclusions.');
                        }
                    }
                }
            }
        }
        
        logLimits(false);
    }
    
    global void finish(Database.BatchableContext bc) { }
}