/**
* This is Rendition Provider that fetches PDF from Salesforce for a given content document 
*/
public with sharing class SQX_PDFRenditionProvider1 implements SQX_RenditionProvider,SQX_Sync_RenditionProvider {

    final String CLIENT_ID = '##EASYPDF_CLIENTID##',
                 CLIENT_SECRET = '##EASYPDF_CLIENTSECRET##',
                 EASYPDF_HOST = 'https://api.easypdfcloud.com',
                 END_POINT='https://www.easypdfcloud.com/oauth2/token';

    final String HTTP_GET = 'GET',
                 HTTP_POST = 'POST',
                 HTTP_PUT = 'PUT',
                 HTTP_DELETE = 'DELETE',
                 HTTP_PATCH = 'PATCH';

    final String SALESFORCE_PROVIDER = 'salesforce',
                 EASYPDF_PROVIDER = 'easypdf';

    final String STATUS_COMPLETE = 'completed',
                 STATUS_RUNNING = 'running',
                 UNSUPPORTED_FORMAT = 'Unsupported format';

    final String EASYPDF_DEFAULT_FILE_NAME = 'CQ_' + UserInfo.getOrganizationId(),
                 DEFAULT_PROFILE_PAGE_NAME = 'CQ_Profile.pdf',
                 DEFAULT_STAMP_PDF_NAME = 'stamp.pdf',
                 DEFAULT_PRINT_SCRIPT_NAME = 'script.js';

    final String RENDITION_JOB_OPERATION = 'operation',
                 RENDITION_JOB_START = 'start',
                 RENDITION_JOB_STOP = 'stop';

    final String CONTENT_TYPE_URLENCODED ='application/x-www-form-urlencoded',
                 CONTENT_TYPE_JSON = 'application/json',
                 CONTENT_TYPE_MULTIPART_FORM_DATA = 'multipart/form-data',
                 CONTENT_TYPE_OCTET_STREAM = 'application/octet-stream',
                 CONTENT_TYPE_STREAM = 'stream',
                 CONTENT_TYPE_SIMPLE_HTTP_REQUEST = 'simpleHttpRequest',
                 CONTENT_TYPE_HANDLE_RECORD = 'handleRecord',
                 ACTION_TYPE_HTTP_REQUEST = 'httpRequest';

    final String EASYPDF_DOWNLOAD_CONTENT_PATH = '/v1/jobs/{JOB_ID}/output/?type=file',
                 CONTENT_VERSION_QUERY_PATH = '/services/data/v41.0/sobjects/ContentVersion',
                 UPDATE_RECORD_PATH = '/services/data/v41.0/sobjects/' + SQX.NSPrefix + 'SQX_Controlled_Document__c/{CONTROLLED_DOCUMENT_ID}',
                 CONTENT_VERSION_QUERY_PATH_WITH_VERSION_DATA = '/services/data/v41.0/sobjects/ContentVersion/{CONTENT_VERSION_ID}/VersionData',
                 EASYPDF_UPLOAD_CONTENT_PATH =  '/v1/jobs/{JOB_ID}/input/{PC_FILE_NAME}',
                 QUERY_SERVICE_PATH = '/v1/jobs/{JOB_ID}';

    final Integer HTTP_ERROR_STATUS = 404,
                  HTTP_CONTINUE_STATUS = 202, 
                  HTTP_OK_STATUS = 200,
                  HTTP_BAD_REQUEST=400,
                  HTTP_CREATED_STATUS = 201;

    public static final Integer LIMIT_SYNC_CALLOUT_REQUEST_SIZE = 6 * 1024 * 1024,  // 6 MB
                                LIMIT_ASYNC_CALLOUT_REQUEST_SIZE = 11 * 1024 * 1024;   // 11 MB

    final Integer POLLING_TIME_OUT_LIMIT_SYNC = 20 * 1000;  // unit in ms, equivalent to 20 secs

    final String  EXCEPTION_MSG_READ_TIME_OUT = 'Read timed out';

    String access_token;

    Integer timeOutLimit;

    // Flag to determine if instance of this class is made from test or not.
    public static Boolean isTest = false;
    
    /**
    * Retrieves the PDF rendition from salesforce using salesforce's chatter rest api for the controlled content
    * @param request the rendition request containing detail regarding the rendition that is to be retrieved.
    * @return returns the rendition response containing the pdf blob.
    */
    public SQX_RenditionResponse retrieve(SQX_RenditionRequest request){

        SQX_RenditionResponse resp = retrieveMetadata(request);

        SQX_RenditionResponse response = new SQX_RenditionResponse();

        if(resp.status == SQX_RenditionResponse.Status.RenditionReady && isLargeContent(resp.bytes)){
            response = retrieveThroughProxy(request);
            if(response.status == SQX_RenditionResponse.Status.Processing) {
                response.status = SQX_RenditionResponse.Status.FetchingThroughProxy;
            }
        } else if(resp.status == SQX_RenditionResponse.Status.Error){
            response = resp;
        }else {
            final String RENDITION_SERVICE_PATH = '/v1/jobs/{JOBID}/output?type=file';

            PageReference ref =new PageReference(EASYPDF_HOST + RENDITION_SERVICE_PATH.replace('{JOBID}', request.jobId));
            ref.getParameters().put('type', 'file');
            String url = ref.getUrl();

            HttpRequest req = createHttpRequest(HTTP_GET, url);

            Http http = new Http();
            HTTPResponse res = http.send(req);
            Integer statusCode = res.getStatusCode();
            response.jobId = request.jobId;
            if(statusCode == HTTP_CONTINUE_STATUS){
                response.status = SQX_RenditionResponse.Status.Processing;
            }
            else if(statusCode == HTTP_OK_STATUS){
                response.status = SQX_RenditionResponse.Status.RenditionReady;
                response.result = new ContentVersion(VersionData = res.getBodyAsBlob(), PathOnClient = 'Auto-' + DateTime.now() + '.pdf');
            }
            else{
                response.status = SQX_RenditionResponse.Status.Error;
                response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
                response.errorDescription = res.getBody();
            }
        }
        return response;
    }
	
    /**
    * Method to send request to proxy server with required information to retrieve file from salesforce
    * @param request the rendition request containing detail regarding the rendition that is to be retrieved.
    * @return SQX_RenditionResponse type object for the given http response
    */
    public SQX_RenditionResponse retrieveThroughProxy(SQX_RenditionRequest request){
        SQX_Controlled_Document__c document = [SELECT Title__c, Description__c, Auto_Submit_For_Approval__c, Secondary_Content_Reference__c, Draft_Vault__c
                                                FROM SQX_Controlled_Document__c
                                                WHERE Id = : request.document.Id];
        Id recordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CONTROLLED_CONTENT);
        String contentDocumentId = document.Secondary_Content_Reference__c;
        String draftVault = document.Draft_Vault__c;
        String reasonForChange = Label.SQX_MSG_AUTOMATIC_GENERATED_COMMENT.replace('{PROVIDERNAME}', request.providerName);
        String data = null;
        String fileName = 'Auto-' + DateTime.now() + '.pdf';
        String contentVersionContent = null;
        String salesforceContentRequestMethod = null;
        String truncatedDescription = SQX_Utilities.getTruncatedText(document.Description__c, ContentVersion.Description.getDescribe().getLength(), true);
        String action = 'auto, removeRenditionLog';
        if(String.isBlank(contentDocumentId)) {
            if(document.Auto_Submit_For_Approval__c == SQX_Controlled_Document.APPROVAL_SUBMISSION_STATUS_HOLD) {
                action += ', checkInSecondaryContent,submitForApproval';
            } else {
                action += ', checkInSecondaryContent,';
            }
        } else {
            if(document.Auto_Submit_For_Approval__c == SQX_Controlled_Document.APPROVAL_SUBMISSION_STATUS_HOLD) {
                action += ', submitForApproval';
            }
        }
        if(String.isBlank(contentDocumentId)){
            contentVersionContent = '{"fileName" : "' + fileName + '", "data" : {"PathOnClient" : "' + fileName + '", "ReasonForChange" : "' + reasonForChange + '", "FirstPublishLocationId" : "' + draftVault + '", "RecordTypeId" : "' + recordTypeId + '", "' + SQX.NsPrefix + 'Controlled_Document__c" : "' + request.document.Id + '", "Title" : "' + document.Title__c + '", "Description" : "' + truncatedDescription + '", "' + SQX.NSPrefix + 'CQ_Actions__c" : "' + action + '"}}';
        } else {
            contentVersionContent = '{"fileName" : "' + fileName + '", "data" : {"PathOnClient" : "' + fileName + '", "ReasonForChange" : "' + reasonForChange + '", "ContentDocumentId" : "' + contentDocumentId + '", "' + SQX.NSPrefix + 'CQ_Actions__c" : "' + action + '"}}';
        }
        salesforceContentRequestMethod = HTTP_POST;

        String downloadContentUrl = EASYPDF_HOST + EASYPDF_DOWNLOAD_CONTENT_PATH.replace('{JOB_ID}', request.jobId);

        String contentVersionUrl = Url.getSalesforceBaseUrl().toExternalForm() + CONTENT_VERSION_QUERY_PATH;
        
        String updateRecordUrl = Url.getSalesforceBaseUrl().toExternalForm() + UPDATE_RECORD_PATH.replace('{CONTROLLED_DOCUMENT_ID}', request.document.Id);
        String updateRecordContent = JSON.serialize(new Map<String, SObject> {'data' => new SQX_Controlled_Document__c(Synchronization_Status__c = 'Failed', Rendition_Job_Error__c = '{!errorLogToken}')});

        SQX_RequestPayload requestPayload = new SQX_RequestPayload();

        SQX_RequestPayload.Principal easypdfPrincipal = new SQX_RequestPayload.Principal(CLIENT_ID, END_POINT);
        SQX_RequestPayload.Principal salesforcePrincipal = new SQX_RequestPayload.Principal(UserInfo.getUserName(), getSalesforceEndPoint());

        SQX_RequestPayload.Content easypdfContent = new SQX_RequestPayload.Content(CONTENT_TYPE_STREAM, downloadContentUrl, null, null, HTTP_GET, easypdfPrincipal);
        SQX_RequestPayload.Content salesforceContent = new SQX_RequestPayload.Content(CONTENT_TYPE_STREAM, contentVersionUrl, CONTENT_TYPE_MULTIPART_FORM_DATA, contentVersionContent, salesforceContentRequestMethod, salesforcePrincipal);
        SQX_RequestPayload.Content salesforceUpdateControlledDocumentContent = new SQX_RequestPayload.Content(CONTENT_TYPE_HANDLE_RECORD, updateRecordUrl, CONTENT_TYPE_JSON, updateRecordContent, HTTP_PATCH, salesforcePrincipal);

        SQX_RequestPayload.Action easypdfAction = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, EASYPDF_PROVIDER, new SQX_RequestPayload.Content[]{ easypdfContent });

        //Success Action upsert content version 
        SQX_RequestPayload.Action salesforceAction = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, SALESFORCE_PROVIDER, new SQX_RequestPayload.Content[]{ salesforceContent });

        //Failure Action Update Controlled document to set secondary content failed
        SQX_RequestPayload.Action salesforceUpdate = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, SALESFORCE_PROVIDER, new SQX_RequestPayload.Content[]{ salesforceUpdateControlledDocumentContent });

        requestPayload.action = new SQX_RequestPayload.Action[] { easypdfAction };
        requestPayload.success = new SQX_RequestPayload.Action[] { salesforceAction };
        requestPayload.failure = new SQX_RequestPayload.Action[] { salesforceUpdate };

        //Submit request to heroku proxy server
        SQX_RenditionResponse response = submitRequestToProxyServer(requestPayload);
        response.jobId = request.jobId;
        return response;
    }
    
    /**
    * Retrieves information about PDF is ready to be retrieved
    * @param request the rendition request containing detail regarding the rendition whose metadata information is to be retrieved.
    * @return returns the rendition response containing metadata information of file to be retrieved
    */
    public SQX_RenditionResponse retrieveMetadata(SQX_RenditionRequest request){
        final String RENDITION_SERVICE_PATH = '/v1/jobs/{JOBID}/output?type=metadata';

        PageReference ref =new PageReference(EASYPDF_HOST + RENDITION_SERVICE_PATH.replace('{JOBID}', request.jobId));
        ref.getParameters().put('type', 'metadata');
        String url = ref.getUrl();
        SQX_RenditionResponse response = new SQX_RenditionResponse();

        HttpRequest req = createHttpRequest(HTTP_GET, url);

        Http http = new Http();
        HTTPResponse res = http.send(req);
        return handleMetadataHttpResponse(res);
    }

    /**
    * Queries salesforce's server for the status of the rendition request previously submitted.
    * @param requests the list of requests that where submitted previously and whose status is to be checked.
    */
    public Map<SQX_RenditionRequest, SQX_RenditionResponse> poll(SQX_RenditionRequest [] requests){
        final String QUERY_SERVICE_PATH = '/v1/jobs/{JOB_ID}';
        Map<SQX_RenditionRequest, SQX_RenditionResponse> response = new Map<SQX_RenditionRequest, SQX_RenditionResponse>();

        if(requests != null && requests.size() > 0){

            for(SQX_RenditionRequest request : requests){
                String url =  EASYPDF_HOST + QUERY_SERVICE_PATH.replace('{JOB_ID}', request.jobId);

                HttpRequest req = createHttpRequest(HTTP_GET, url);

                Http http = new Http();

                HTTPResponse res = http.send(req);
                SQX_RenditionResponse r = handleHttpResponse(res);
                r.jobId = request.jobId;

                response.put(request, r);
            }
        }
        return response;
    }

    /**
    * submits a request to salesforce to start processing the rendition
    * For salesforce both request submission and retrieval are same method. First request for retrieval acts as a submission request
    */
    public SQX_RenditionResponse submitRequest(SQX_RenditionRequest request){

        String  fileExt = request.content.PathOnClient.substringAfterLast('.');

        String pcFileName;
        
        Integer contentSize = [SELECT ContentSize FROM ContentVersion WHERE Id =: request.content.Id].ContentSize;

        // create dynamic workflow based on the given rendition request
        SQX_PDFWorkflowGenerator generator = new SQX_PDFWorkflowGenerator(request);

        String workflow = generator.generate();

        Blob profilePageAsPDF = generator.profilePageAsPDF;

        /** Callout to generate a dynamic workflow*/ 
        SQX_RenditionResponse response = createDynamicWorkflowRequest(workflow);

        if( response.errorCode == null ){
            // if request has been successfully submitted

            String jobId = response.jobId; // new job's id

            // checking if pdf is to be stamped
            Blob stampingPdf = generator.stampPdf;

            String profilePageAsPDFName;

            if(generator.profilePageInclusionType == SQX_PDFWorkflowGenerator.SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_APPEND && profilePageAsPDF != null) {
                profilePageAsPDFName = '2.pdf';
                pcFileName = '1_' + request.document.Id + '.' + fileExt;
            } else {
                profilePageAsPDFName = '1.pdf';
                pcFileName = '2_' + request.document.Id + '.' + fileExt;
            }

            // Size of file bigger than saleforce limit
            if(isLargeContent(contentSize)) {

                if(profilePageAsPDF != null) {
                    response = uploadAdditionalFileRequest(jobId, profilePageAsPDF, profilePageAsPDFName);
                }

                if( response.errorCode == null && stampingPdf != null){
                    /** Callout : uploading stamp pdf */
                    response = uploadAdditionalFileRequest(jobId, stampingPdf, DEFAULT_STAMP_PDF_NAME);
                }

                // Call proxy server in order to send request to upload
                if( response.errorCode == null) {

                    String contentVersionUrl = Url.getSalesforceBaseUrl().toExternalForm() + CONTENT_VERSION_QUERY_PATH_WITH_VERSION_DATA.replace('{CONTENT_VERSION_ID}', request.content.Id);

                    String easypdfUploadContentPathWithFileName = EASYPDF_UPLOAD_CONTENT_PATH.replace('{PC_FILE_NAME}', pcFileName);
                    String uploadContentUrl = EASYPDF_HOST + easypdfUploadContentPathWithFileName.replace('{JOB_ID}', jobId);

                    String startStopJobUrl = EASYPDF_HOST + QUERY_SERVICE_PATH.replace('{JOB_ID}', jobId);
                    String startStopJobContent = 'operation=start';

                    String updateRecordUrl = Url.getSalesforceBaseUrl().toExternalForm() + UPDATE_RECORD_PATH.replace('{CONTROLLED_DOCUMENT_ID}', request.document.Id);
                    String updateRecordContent = JSON.serialize(new Map<String, SObject> {'data' => new SQX_Controlled_Document__c(Synchronization_Status__c = 'Failed', Rendition_Job_Error__c = '{!errorLogToken}')});

                    SQX_RequestPayload requestPayload = new SQX_RequestPayload();

                    SQX_RequestPayload.Principal salesforcePrincipal = new SQX_RequestPayload.Principal(UserInfo.getUserName(), getSalesforceEndPoint());
                    SQX_RequestPayload.Principal easypdfPrincipal = new SQX_RequestPayload.Principal(CLIENT_ID, END_POINT);

                    SQX_RequestPayload.Content salesforceContent = new SQX_RequestPayload.Content(CONTENT_TYPE_STREAM, contentVersionUrl, null, null, HTTP_GET, salesforcePrincipal);
                    SQX_RequestPayload.Content easypdfContent = new SQX_RequestPayload.Content(CONTENT_TYPE_STREAM, uploadContentUrl, CONTENT_TYPE_OCTET_STREAM, null, HTTP_PUT, easypdfPrincipal);
                    SQX_RequestPayload.Content easypdfStartJobContent = new SQX_RequestPayload.Content(CONTENT_TYPE_SIMPLE_HTTP_REQUEST, startStopJobUrl, CONTENT_TYPE_URLENCODED, startStopJobContent, HTTP_POST, easypdfPrincipal);
                    SQX_RequestPayload.Content salesforceUpdateControlledDocumentContent = new SQX_RequestPayload.Content(CONTENT_TYPE_HANDLE_RECORD, updateRecordUrl, CONTENT_TYPE_JSON, updateRecordContent, HTTP_PATCH, salesforcePrincipal);

                    SQX_RequestPayload.Action salesforceAction = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, SALESFORCE_PROVIDER, new SQX_RequestPayload.Content[]{ salesforceContent });
                    SQX_RequestPayload.Action easypdfAction = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, EASYPDF_PROVIDER, new SQX_RequestPayload.Content[]{ easypdfContent });

                    //Success Action Start Job 
                    SQX_RequestPayload.Action easypdfStartJobAction = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, EASYPDF_PROVIDER, new SQX_RequestPayload.Content[]{ easypdfStartJobContent });

                    //Failure Action Update Controlled document to set secondary content failed
                    SQX_RequestPayload.Action salesforceUpdate = new SQX_RequestPayload.Action(ACTION_TYPE_HTTP_REQUEST, SALESFORCE_PROVIDER, new SQX_RequestPayload.Content[]{ salesforceUpdateControlledDocumentContent });

                    requestPayload.action = new SQX_RequestPayload.Action[] { salesforceAction, easypdfAction };
                    requestPayload.success = new SQX_RequestPayload.Action[] { easypdfStartJobAction };
                    requestPayload.failure = new SQX_RequestPayload.Action[] { salesforceUpdate };

                    //Submit request to heroku proxy server
                    response = submitRequestToProxyServer(requestPayload);
                    response.jobId = jobId;

                }
            } else {

                Blob pcData = [SELECT VersionData FROM ContentVersion WHERE Id =: request.content.Id].VersionData; // primary content data

                if( profilePageAsPDF != null ){

                    // profile page is to be included in the secondary content

                    if(generator.profilePageInclusionType == SQX_PDFWorkflowGenerator.SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_APPEND ){
                        // first send the primary content and then profile page

                        /** Callout : uploading first file */
                        response = uploadAdditionalFileRequest(jobId, pcData, pcFileName);

                        if( response.errorCode == null ){
                            // if request has been successfully submitted

                            /** Callout : uploading additional file */
                            response = uploadAdditionalFileRequest(jobId, profilePageAsPDF, profilePageAsPDFName);
                        }
                    }else{
                        /*  pre-pend profile page
                            send profile page first followed by primary content
                        */

                        /** Callout : uploading first file */
                        response = uploadAdditionalFileRequest(jobId, profilePageAsPDF, profilePageAsPDFName);

                        if( response.errorCode == null ){
                            // if request has been successfully submitted

                            /** Callout : uploading additional file */
                            response = uploadAdditionalFileRequest(jobId, pcData, pcFileName);
                        }
                    }

                }else{
                    // if profile page is to be excluded, only one callout is required since rendition can be started immediately

                    /** Callout : uploading primary content */
                    response = uploadAdditionalFileRequest(jobId, pcData, pcFileName);
                }

                if( response.errorCode == null && stampingPdf != null){

                    /** Callout : uploading stamp pdf */
                    response = uploadAdditionalFileRequest(jobId, stampingPdf, DEFAULT_STAMP_PDF_NAME);

                }

                if( response.errorCode == null ){
                    // if request has been successfully submitted

                    /** Callout : starting rendition */
                    response = toggleJobExecution(jobId, RENDITION_JOB_START);

                    if( response.errorCode == null ){
                        // adding jobId to the response 
                        response.jobId = jobId;
                    }
                }
            }

        }
        return response;
    }
    
    /**
     * queries provider for the status of the rendition request previously submitted
     * the query requires the request to wait for 30 secs for the job to complete
     * @param previously submitted rendition request
    */
    private SQX_RenditionResponse pollSync(SQX_RenditionRequest request){

        SQX_RenditionResponse response = new SQX_RenditionResponse();

        if(request != null){

            String QUERY_SERVICE_PATH = '/v1/jobs/{JOB_ID}/event';

            String url =  EASYPDF_HOST + QUERY_SERVICE_PATH.replace('{JOB_ID}', request.jobId);

            // create the http request for the url with the authorization
            HttpRequest req = createHttpRequest(HTTP_POST, url, '' , '');

            response = performCallout(req);

        }

        response.jobId = request.jobId;

        return response;
    }
    
    /**
     * submits/polls a request to rendition provider to process the rendition synchronously
     * @param rendition request which is to be rendered
    */    
    public SQX_RenditionResponse processRequestSync(SQX_RenditionRequest request){
        
        // set timeout limit for synchronous rendition which is greater than the default 10s limit
        timeOutLimit = POLLING_TIME_OUT_LIMIT_SYNC;

        SQX_RenditionResponse resp;

        try{

            if(String.isBlank(request.jobId)){
                // implies that the request has not been submitted
                resp = submitRequest(request);
                if(resp.status == SQX_RenditionResponse.Status.Processing){
                    request.jobId = resp.jobId;
                    resp = pollSync(request);
                }
            }else{
                resp = pollSync(request);
            }

            if(resp.status == SQX_RenditionResponse.Status.RenditionReady){
                resp = retrieve(request);
            }

        }catch(System.CalloutException ex){

            /*
                Since we are polling synchronously, we can hit time out exception for some heavy content files
                Hence, in such cases, we will be poll asynchronously(that is not waiting for completion) and return the result

                NOTE: 'Read Timed Out' error message seems to be generated by the system independent of the language
                Hence, translation won't affect the behavior and we can safely check for 'Read Timed Out' message in the exception message thrown
            */
            if( ex.getMessage().contains(EXCEPTION_MSG_READ_TIME_OUT) ){

                if(!String.isBlank(request.jobId)){
                    resp = poll( new List<SQX_RenditionRequest> { request } ).get(request);
                }else{
                    /*
                        This part is hit when submission of request itself hits time out limit
                        We shall leave submission job for batch job processor
                    */
                    resp = new SQX_RenditionResponse();
                    resp.status = SQX_RenditionResponse.Status.Processing;
                }

            }else{
                throw ex;
            }
        }
        return resp;
    }

    /**
    *   This method is used to send an http request to create the given workflow
    *   @param workflow - json string that specifies the workflow to be generated
    *   @return response after performing the callout to create workflow
    */
    private SQX_RenditionResponse createDynamicWorkflowRequest(String workflow){

        PageReference callOutUrl = new PageReference(EASYPDF_HOST + '/v1/jobs');

        // get url from page reference
        String url = callOutUrl.getUrl();

        // create request to set dynamic workflow
        HttpRequest req = createHttpRequest(HTTP_PUT, url, CONTENT_TYPE_JSON, workflow);

        return performCallout(req);
    }

    /**
    *   This method submits an http request to upload additional file for merging into the given job
    *   @param jobId - job to which the additional file is to be uploaded
    *   @param content - content which is to be merged
    *   @param fileName - name of the file containing the content
    *   @return SQX_RenditionResponse after performing the callout
    */
    private SQX_RenditionResponse uploadAdditionalFileRequest(String jobId, Blob content, String fileName){

        PageReference callOutUrl = new PageReference(EASYPDF_HOST + '/v1/jobs/' + jobId + '/input/' + fileName);

        String url = callOutUrl.getUrl();

        HttpRequest req = createHttpRequest(HTTP_PUT, url, CONTENT_TYPE_OCTET_STREAM);

        req.setBodyAsBlob(content);

        return performCallout(req);
    }

    /**
    *   This method submits an http request to start/stop job execution
    *   @param jobId - id of the job whose operation is to be toggled
    *   @param operation - start or stop
    *   @return SQX_RenditionResponse after performing the callout
    */
    private SQX_RenditionResponse toggleJobExecution(String jobId, String operation){

        PageReference callOutUrl = new PageReference(EASYPDF_HOST + '/v1/jobs/' + jobId);

        callOutUrl.getParameters().put(RENDITION_JOB_OPERATION, operation);

        String url = callOutUrl.getUrl();

        HttpRequest req = createHttpRequest(HTTP_POST, url, CONTENT_TYPE_URLENCODED, '');

        return performCallout(req);
    }

    /**
    *   Short hand method for sending an http request
    *   @param req- HttpRequest
    *   @return HttpResponse - response after submitting the request
    */
    private SQX_RenditionResponse performCallout(HttpRequest req){

        Http http = new Http();

        HttpResponse res = http.send(req);

        return handleHttpResponse(res);
    }

    /**
    *   Short hand method for creating an HttpRequest object
    *   @param method - HttpRequest method (POST, PUT, GET)
    *   @param url - endpoint url
    *   @return HttpRequest object
    */
    private HttpRequest createHttpRequest(String method, String url){
        return createHttpRequest(method, url, null);
    }

    /**
    *   Short hand method for creating an HttpRequest object
    *   @param method - HttpRequest method (POST, PUT, GET)
    *   @param url - endpoint url
    *   @param contentType - HttpRequest header : ContentType
    *   @return HttpRequest object
    */
    private HttpRequest createHttpRequest(String method, String url, String contentType){
        return createHttpRequest(method, url, contentType, null);
    }

    /**
    *   Short hand method for creating an HttpRequest object
    *   @param method - HttpRequest method (POST, PUT, GET)
    *   @param contentType - HttpRequest header : ContentType
    *   @param url - endpoint url
    *   @param body - body to be sent as part of the request
    *   @return HttpRequest object
    */
    private HttpRequest createHttpRequest(String method, String url, String contentType, String body){

        access_token = getAccessToken();

        HttpRequest req = new HttpRequest();

        req.setMethod(method);
        req.setHeader('Authorization','Bearer '+ access_token);

        if( !String.isBlank(contentType) ){
            req.setHeader('Content-type', contentType);
        }

        req.setEndpoint(url);

        if( body != null ){
            req.setBody(body);
        }

        if( timeOutLimit != null){
            req.setTimeOut(timeOutLimit);
        }

        return req;
    }

    /**
    *   Short hand method for converting HttpResponse to SQX_RenditionResponse
    *   @param res - HttpResponse object
    *   @return SQX_RenditionResponse type object for the given http response
    */
    private SQX_RenditionResponse handleHttpResponse(HttpResponse res){

        SQX_RenditionResponse response = new SQX_RenditionResponse();

        Integer statusCode = res.getStatusCode();

        if( statusCode == HTTP_CREATED_STATUS ||
            statusCode == HTTP_OK_STATUS || 
            statusCode == HTTP_CONTINUE_STATUS ){

            /* While uploading additional input files easypdf returns the status code but no data
               An empty json response '{}' also technically qualifies as No Response.
               Therefore adding the test for empty response
               Please refer to SQX-6408 for more details */
            if(res.getBody().length() > 0 && res.getBody() != '{}'){

                EasyCloudResponse rs = (EasyCloudResponse) JSON.deserialize(res.getBody(), EasyCloudResponse.class);

                if( statusCode == HTTP_CREATED_STATUS ){

                    response.jobId = rs.jobId;

                }else if( statusCode == HTTP_OK_STATUS ){

                    /*
                        Note : EasyPdf returns an OK status as an acknowledge of reception of request
                        Hence, even if the request is erroneous, the status code will still be OK
                        So, the following code is to handle errors returned from the response
                    */

                    List<ErrorDetail> errors = rs.detail != null ? rs.detail.errors : null;

                    if( errors != null && errors.size() > 0 ){

                        response.status = SQX_RenditionResponse.Status.Error;
                        response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
                        response.errorDescription = rs.detail.errors.get(0).message;

                    }else if( rs.status == STATUS_RUNNING ){
                        response.status = SQX_RenditionResponse.Status.Processing;
                    }
                    else if( rs.status != STATUS_COMPLETE ){

                        response.status = SQX_RenditionResponse.Status.Error;
                        response.errorCode = SQX_RenditionResponse.ErrorCodes.UnsupportedFormat;
                        response.errorDescription = UNSUPPORTED_FORMAT;

                    }else{

                        response.status = SQX_RenditionResponse.Status.RenditionReady;

                    }

                }
            }

            if( response.status == null ){
                response.status = SQX_RenditionResponse.Status.Processing;
            }
        }
        else{

            response.status = SQX_RenditionResponse.Status.Error;
            response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
            response.errorDescription = res.getBody();

        }
        return response;
    }

    /**
    * Method for converting HttpResponse of metadata to SQX_RenditionResponse
    * @param res - HttpResponse object
    * @return SQX_RenditionResponse type object for the given http response
    */
    private SQX_RenditionResponse handleMetadataHttpResponse(HttpResponse res){
        SQX_RenditionResponse response = new SQX_RenditionResponse();

        Integer statusCode = res.getStatusCode();

        if( statusCode == HTTP_OK_STATUS ){

            if(res.getBody().length() > 0){

                EasyCloudMetadataResponse rs = (EasyCloudMetadataResponse) JSON.deserialize(res.getBody(), EasyCloudMetadataResponse.class);
                response.status = SQX_RenditionResponse.Status.RenditionReady;
                response.bytes = rs.bytes;
            }
        }
        else{

            response.status = SQX_RenditionResponse.Status.Error;
            response.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError;
            response.errorDescription = res.getBody();

        }
        return response;
    }

    /**
    * get access token
    */
    private String getAccessToken(){

        if( String.isBlank(access_token) ){

            String reqbody ='grant_type=client_credentials' +
                            '&client_id=' +
                                EncodingUtil.urlEncode(CLIENT_ID,'UTF-8')+
                            '&client_secret='+
                                EncodingUtil.urlEncode(CLIENT_SECRET,'UTF-8')+
                            '&scope=epc.api';

            HttpRequest req = new HttpRequest();

            req.setMethod(HTTP_POST);
            req.setHeader('Content-type', CONTENT_TYPE_URLENCODED);
            req.setEndpoint(END_POINT);
            req.setBody(reqbody);

            Http http = new Http();
            HTTPResponse res = http.send(req);

            Integer statusCode=res.getStatusCode();

            if(statusCode == HTTP_OK_STATUS){

                EasyCloudResponse obj =(EasyCloudResponse)JSON.deserialize(res.getBody(),EasyCloudResponse.class);
                access_token = obj.access_token;

            }else {
                throw new SQX_ApplicationGenericException( res.getBody() );
            }

        }

        return access_token;
    }

    /**
    * this is identifiable name of the provider
    */
    public String getProviderName(){
        return 'CQ';
    }

    /**
    * Cancels the request to create a rendition
    */
    public void abort(SQX_RenditionRequest[] requests){

    }

    /**
    * Checks if the selected rendition provider supports a particular rendition or not
    */
    public Map<SQX_RenditionRequest, Boolean> supportsRendition(SQX_RenditionRequest[] requests){
        Map<SQX_RenditionRequest, Boolean> result = new Map<SQX_RenditionRequest, Boolean>();
        Set<String> supportedExtensions = new  Set<String>{'doc','docx','txt','xlsx','xls','html','pdf','vsd','vdx','ppt', 'pptx',
                                                            'gif', 'png', 'jpg', 'jpeg', 'jpg', 'jfif', 'tif', 'tiff', 'text', 'rtf'};
        for(SQX_RenditionRequest request : requests){
            if(supportedExtensions.contains(request.content.FileExtension)){
                result.put(request, true);
            }else{
                result.put(request, false);  
            }
        }
        return result;
    } 

    /**
    * Method to send request to proxy server with required parameters
    * @param request payload containing information about servers to call 
    * @return SQX_RenditionResponse type object for the given http response
    */
    private SQX_RenditionResponse submitRequestToProxyServer(SQX_RequestPayload requestPayload) {

        SQX_RenditionResponse renditionResponse = new SQX_RenditionResponse();

        Http http = new Http();

        HttpRequest httpRequest = new HttpRequest();
        string calloutStr = SQX_Utilities.getCalloutStr('CQ_LargeRenditionServer');
        httpRequest.setEndpoint(calloutStr + '/upload');
        httpRequest.setMethod(HTTP_POST);
        httpRequest.setBody(JSON.serialize(requestPayload));
        httpRequest.setHeader('Content-Type', CONTENT_TYPE_JSON);
        
        HttpResponse httpResponse = http.send(httpRequest);

        if(httpResponse.getStatusCode() == HTTP_OK_STATUS) {
            renditionResponse.status = SQX_RenditionResponse.Status.Processing;
        } else {
            renditionResponse.status = SQX_RenditionResponse.Status.Error;
            renditionResponse.errorCode = SQX_RenditionResponse.ErrorCodes.UnknownError; 
            renditionResponse.errorDescription = httpResponse.getbody();
        }

        return renditionResponse;

    }

    /**
    * Method to check if contentSize is greater than maximum size for synchronous http callout and also check if instance of this class is made from test or not
    * @param contentSize is the size of the content that has to compared with Salesforce limit
    * @return flag to indicate whether contentSize is greater than maximum size for synchronous http callout and whether instance of this class is made from test or not
    */
    public Boolean isLargeContent(Integer contentSize) {
        return (contentSize > LIMIT_SYNC_CALLOUT_REQUEST_SIZE) || isTest;
    }

    /**
    * Method to get salesforce endpoint
    * @return String is the endpoint that can be normal org or sandbox 
    */
    public String getSalesforceEndPoint() {
        String salesforceEndpoint = 'https://login.salesforce.com';
        if([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox) {
            salesforceEndpoint = 'https://test.salesforce.com';
        }
        return salesforceEndpoint;
    }

//////// easyPDF Response's Object //////

    public class EasyCloudResponse {
        public String jobId {get; set;}
        public String workflowID {get; set;}
        public String status {get; set;}
        public Boolean finished {get; set;}
        public String access_token{get;set;}
        public Detail detail{get;set;}
    }

    public class Detail{
        public List<ErrorDetail> errors {get; set;}
    }

    public class ErrorDetail{
        public String taskName {get; set;}
        public String fileName {get; set;}
        public String message {get; set;}
        public String taskType {get; set;}
        public String errorType {get; set;}
    }

    // easyPDF metadata response object
    public class EasyCloudMetadataResponse {
        public String isFolder {get; set;}
        public String name {get; set;}
        public Integer bytes {get; set;}
        public String mime {get; set;}
        public String modifiedDate {get; set;}
    }
}