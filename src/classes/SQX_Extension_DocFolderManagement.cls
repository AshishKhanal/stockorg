/**
 * Extension class Document Folder Management page. It contains the logic to dynamically load
 * customer script
 */
public with sharing class SQX_Extension_DocFolderManagement {

    /**
     * Returns the name of customer script layout for the doc folder management
     */
    public String getCuScriptLayout() {
        return SQX_Utilities.getPageName('_CuScript_Layout', '');
    }	
}