/**
* This is controller responsible for generating the audit trail report of compliance quest objects
* such as Audit, Audit Program, Controlled Document, 
*/
public with sharing class SQX_Controller_Audit_Trail_Report {
    
    ///Audit trail filter properties

    /**
    * The name of the object that is being audit trailed. E.g. Audit Number, Part Number etc
    */
    public String objectName { get; set; }

    /**
    * The Id of the object that is being audit trailed
    */
    public Id objectId { get; set; }

    /**
    * The type of object that we want to get the audit trail of object type 
    */
    public String objectType { get; set; }

    private SObjectType i_objectType; /*This field is used internally to enforce strong typing of sobjectype*/

    /**
    * The property stores the field that is to be checked for changes
    */
    public String field {get; set;}

    /**
    * The date and time starting from which, the changes made are to be listed
    */
    public DateTime changedFrom {get; set;}

    /**
    * The date and time till which, the changes made are to be listed.
    */
    public DateTime changedUntil{get; set;}

    /**
    * The property that enables/disables, the filter to include deleted records. When this is true deleted records are included
    * otherwise they aren't
    */
    public Boolean includeDeleted {get; set;}

    /**
    * The property that enables/disables the inclusion of child records
    */
    public Boolean includeChildren {get; set;}


    ///Audit trail ui properties

    /**
    * The list of fields that supported for audit trial for a particular type
    */
    public List<SelectOption> fieldNames { get; set; } 

    /**
    * The list of record that are to be displayed
    */
    transient public List<HistoryWrapper> histories {get; set;}

    /**
    * The list of object types that are supported for the object.
    */
    public List<SelectOption> objectTypes {get; set; }

    /**
    * This fields owner field will be used to force SF to render a lookup field, so that user can select the correct field
    */
    public Note note {get; set;}

    transient public Boolean pdfMode {get; set;}
    public String SelectedObjectName {get; set;}

    public String performedFromDate {get; set;}
    public String performedTillDate {get; set;}

    public void pdfize(){
        pdfMode = true;
        if(changedFrom != null)
            performedFromDate = changedFrom.format();
        if(changedUntil != null)
            performedTillDate = changedUntil.format();

        generateReport();
    }


    ///Constants
    public static final String  QUERY_PARAM_DEL = 'del',
                                QUERY_PARAM_OBJID = 'objid',
                                QUERY_PARAM_DEL_TRUE_VALUE = '1',
                                QUERY_PARAM_USERID = 'userid';


    public SQX_Controller_Audit_Trail_Report(){
        if(includeDeleted == null){
            includeDeleted = true;
            includeChildren = false;
            note = new Note();
        }

        loadParamsFromURL(); //sets values of properties from the query params sent in the request
        reloadFields();     //reloads the list of fields supported for the object if any is sent using the query param
        populateObjectType(); //finds and records the list of objects that can be audit trailed by CQ app
    }

    /**
    * This method searches for all the objects in the given namespace for which a tab exists. The objects that are found are used
    * available for the audit trail history.
    */
    private Set<String> populateObjectType(){

        //add a none entry by default
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('-', '--None--'));
        Set<String> addedEntity = new Set<String>();

        //get the namespace of the package, in this case compliance quest.
        final String namespace = SQX.NSPrefix.remove('__');

        Set<String> topLevelObjects = new Set<String>();

        Map<String, SObjectType> gdesc = Schema.getGlobalDescribe();

        Set<String> mainObjects = new Set<String>();

        //get global schema and get the list of tab sets
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        for(Schema.DescribeTabSetResult tsr : tabSetDesc){

            //if tab set belongs to same namespace and tab's object has field history tracking enabled
            //i.e. <Object>__history exists we fetch the object
            if(tsr.getNamespace() == namespace){
                addToLog(tsr.getLabel() + 'Is in our namespace');

                for(Schema.DescribeTabResult tab : tsr.getTabs()){

                    String  tabObj = tab.getSobjectName(),
                            historyObject = tabObj.replace('__c', '__history');

                    if(!String.isBlank(tabObj) && tabObj.startsWith(namespace)){

                        //if the object is not accessible to the current user don't show it
                        if(!gdesc.get(tabObj).getDescribe().isAccessible()){
                            continue;
                        }
                    
                        if(!addedEntity.contains(tabObj)){
                            addedEntity.add(tabObj);
                            addToLog('Found tab ' + tabObj);

                            if(gdesc.containsKey(historyObject)){
                                mainObjects.add(tabObj.toLowerCase());
                            }
                        }
                    }
                }

            }
        }

        for(String sObj : gdesc.keySet()){
            if(sObj.endsWith('__c') || !sObj.contains('__')){
                if(sObj.startsWith(SQX.NSPrefix) && gdesc.get(sObj).getDescribe().isAccessible()){

                    String historyObject =  sObj.replace('__c', '__history');
                    if(gdesc.containsKey(historyObject)){
                        Schema.DescribeSObjectResult fieldRes = gdesc.get(sObj).getDescribe();

                        Map<String, String> val = new Map<String, String>();
                        val.put('Parent', mainObjects.contains(sObj) ?  'Main Objects' : 'Others');
                        val.put('Field', fieldRes.getLabel());
                        options.add(new SelectOption(sObj, JSON.serialize(val)));
                    }
                }
            }
        }

        //set the supported types
        this.objectTypes = options;


        return topLevelObjects;
    }

    /**
    * If the object id or other params are being sent in query param initializes the properties and calls generate report if necessary.
    */
    private void loadParamsFromURL(){
        try{
            String delParam = ApexPages.currentPage().getParameters().get(QUERY_PARAM_DEL); 
            if(!String.isBlank(delParam))
                this.includeDeleted =  delParam == QUERY_PARAM_DEL_TRUE_VALUE;
            
            Id id = (Id) ApexPages.currentPage().getParameters().get(QUERY_PARAM_OBJID);
            if(id != null){
                this.objectId = id;
                generateReport();
            }

            
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ex.getStackTraceString()));
        }
    }

    /**
    * Map of fields that are supported for a particular object type i.e. If NonConformance is the parent record, it relates finding,
    * impacted product etc when nonconformance's audit trail report is being generated.
    */
    private final Map<String, SObjectDataLoader.SerializeConfig> relatedObjs = new Map<String, SObjectDataLoader.SerializeConfig> {
        SQX.NonConformance.toLowerCase() => new SQX_Extension_NC(new ApexPages.StandardController(new SQX_NonConformance__c())).getDataLoaderConfig(),
        SQX.Audit.toLowerCase() => new SQX_Extension_Audit(new ApexPages.StandardController(new SQX_Audit__c())).getDataLoaderConfig() ,
        SQX.CAPA.toLowerCase() => new SQX_Extension_CAPA(new ApexPages.StandardController(new SQX_CAPA__c())).getDataLoaderConfig(),
        SQX.Complaint.toLowerCase() => new SQX_Extension_Complaint(new ApexPages.StandardController(new SQX_Complaint__c())).getDataLoaderConfig(),
        SQX.AuditProgram.toLowerCase() => 
                new SQX_Extension_Audit_Program(new ApexPages.StandardController(new SQX_Audit_Program__c())).getDataLoaderConfig(),
        SQX.ChangeOrder.toLowerCase() =>
                 new SQX_Extension_Change_Order(new ApexPages.StandardController(new SQX_Change_Order__c())).getDataLoaderConfig(),
        SQX.Finding.toLowerCase() => new SQX_Extension_Finding(new ApexPages.StandardController(new SQX_Finding__c())).getDataLoaderConfig()
    };


    /**
    */
    public void reloadFields(){
        fieldNames = new List<SelectOption>();

        SObjectDataLoader.SerializeConfig config = relatedObjs.get(this.objectType);

        Set<SObjectType> addedObjects = new Set<SObjectType>();


        fieldNames.add(new SelectOption('', '--None--' ));



        this.i_objectType = Schema.getGlobalDescribe().get(this.objectType);
        if(this.i_objectType != null && this.i_objectType.getDescribe().isAccessible()){
            addFieldsForObject(this.i_objectType);
        }

        if(config != null){



            for(SObjectField field : config.followRelationships){
                SObjectType sobjType = field.getDescribe().getReferenceTo().get(0); //we are assuming here that lookup field will only point to one object, which is correct for custom objects. 

                if(sobjType.getDescribe().isAccessible()){
                    if(!addedObjects.contains(sobjType)){
                        addedObjects.add(sobjType);
                        addFieldsForObject(sobjType);
                    }
                }
            }

            for(SObjectField field : config.followChildRelationships){
                SObjectType sobjType = getChildRelationForField(field).getChildSObject(); 
                if(sobjType.getDescribe().isAccessible()){
                    if(!addedObjects.contains(sobjType)){
                        addedObjects.add(sobjType);
                        addFieldsForObject(sobjType);
                    }
                }
            }
        }

    }

    static Map<String, SObjectType> allObjects;
    private static boolean historyExists(SObjectType objectType){
        if(allObjects == null)
            allObjects =  Schema.getGlobalDescribe();
        String objectName = '' + objectType;

        return allObjects.containsKey(objectName.replace('__c', '__history')) ? true : 
               allObjects.containsKey(objectName + '__history');
    }

    private Schema.ChildRelationship getChildRelationForField(SObjectField field){
        Schema.ChildRelationship retVal = null;
        Schema.DescribeFieldResult fieldDesc = field.getDescribe();
        Schema.DescribeSObjectResult res = fieldDesc.getReferenceTo().get(0).getDescribe();

        for(Schema.ChildRelationship rel : res.getChildRelationShips()){
            if(rel.getField() == field){
                retVal = rel;
                break;
            }
        }

        return retVal;
    }

    private void addFieldsForObject(SObjectType type){


        if(SQX_Controller_Audit_Trail_Report.historyExists(type)){
            DescribeSObjectResult result = type.getDescribe();
            Map<String, SObjectField> fieldMap = result.fields.getMap();
            for(String s : fieldMap.keySet()){
                DescribeFieldResult r = fieldMap.get(s).getDescribe();
                if(r.isUpdateable() && r.isAccessible()){
                    Map<String, String> val = new Map<String, String>();
                    val.put('Parent', result.getLabel());
                    val.put('Field', r.getLabel());
                    fieldNames.add(new SelectOption(type + '#' + r.getName(), JSON.serialize(val)));
                }
            }
        }
    }



    
    
    transient public String debugLog {get; set; }
    
    /**
    * developement method that is used to add the log to the textbox's output if debug log is true. 
    */
    private void addToLog(String debugLog){
        if(String.isBlank(this.debugLog))
            this.debugLog = '';
        this.debugLog += debugLog + '\r\n';
    }
    


    /**
    * validates and gets the main record for the current http request.
    */
    private boolean validate(){
        boolean isValid = false;

        addToLog('Validating and Finding main record');

        /*
        * Either object id or object name should be provided or object type and date range should be provided
        */

        Boolean isObjectIdProvided = !String.isBlank(this.objectId),
                isObjectNameProvided = !String.isBlank(this.objectName) && !String.isBlank(this.objectType),
                isObjectNameOnlyProvided = !String.isBlank(this.objectName) && String.isBlank(this.objectType),
                isDateRangeProvided = this.changedFrom != null && this.changedUntil != null && !String.isBlank(this.objectType);

        if(!isObjectIdProvided && !isObjectNameProvided && !isDateRangeProvided){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CQ_UI_Record_Id_must_be_provided));

            return isValid;
        }

        isValid = true;

        this.i_objectType = null;


        if(isObjectIdProvided){
            this.i_objectType = this.objectId.getSObjectType();
            this.objectType = (this.i_objectType + '').toLowerCase();
        }
        else{
            this.i_objectType = Schema.getGlobalDescribe().get(this.objectType);
        }

        if(this.i_objectType == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CQ_UI_Object_type_has_not_been_provided));
        }
        else if(!this.i_objectType.getDescribe().isAccessible()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CQ_UI_Object_type_is_not_accessible));
        }
        else{
            if(isObjectNameProvided){

                String query = 'SELECT Id FROM ' + String.escapeSingleQuotes(this.i_objectType+'') + ' WHERE Name =: objectName LIMIT 2';

                if(this.includeDeleted)
                    query += ' ALL ROWS ';


                addToLog('Object Type ' + this.objectType);
                addToLog('Querying for: ' + query);

                List<SObject> objects = Database.query(query);
                addToLog('Found matching objects ' + objects);
                isValid = false;

                if(objects.size() == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CQ_UI_No_matching_object_found));
                }
                else if(objects.size() == 1){
                    this.objectId = objects.get(0).Id;
                    isValid = true;
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CQ_UI_Found_multiple_records_matching_the_record_number));                
                }

            }
        }

        return isValid;
    }

    public List<SObject> getHistoryOf(List<HistoryWrapper> histories, Set<Id> parentIds){
        List<Id> parents = new List<Id>(parentIds);
        if(parents.size() == 0)
            return null;


        SObjectType objectType = parents.get(0).getSObjectType(); 

        return getHistoryOf(histories, parentIds, objectType);
    }

    /**
    * Gets the history of objects from the __History object
    * @param histories the list of history wrapper where history record is to be added
    * @parentIds the set of ids whose history record are to be fetched. Note all the ids must be of same type
    */
    public List<SObject> getHistoryOf(List<HistoryWrapper> histories, Set<Id> parentIds, SObjectType objectType){

        
        String fieldFilter = null;

        if(!String.isBlank(this.field) && this.field != '-'){
            String objType = this.field.subString(0, this.field.indexOf('#'));
            if(objType != (objectType + ''))
                return null;

            fieldFilter = this.field.subString(this.field.indexOf('#') + 1);
        }

        DescribeSObjectResult objType = objectType.getDescribe();
        String objectHistoryType = objType.getName().replace('__c', '__history');

        addToLog('Querying for ' + objectHistoryType);


        if(!Schema.getGlobalDescribe().containsKey(objectHistoryType)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, Label.CQ_UI_Does_not_have_field_history_tracking_enabled.replace('{ObjectType}', objType.getLabel())));
            return null;
        }

        SObjectType i_objectHistoryType = Schema.getGlobalDescribe().get(objectHistoryType);


        String query = 'SELECT CreatedById,CreatedDate,Field,Id,IsDeleted,NewValue,OldValue,ParentId FROM ' + String.escapeSingleQuotes(i_objectHistoryType+'')  + ' WHERE';

        if(parentIds != null)
            query += ' ParentId IN : parentIds AND';

        Id performedById = Note.OwnerId;
        if(Note.OwnerId != null)
            query += ' CreatedById = : performedById AND';

        if(changedFrom != null)
            query += ' CreatedDate >= : changedFrom AND';

        if(changedUntil != null)
            query += ' CreatedDate < : changedUntil AND';

        if(!String.isBlank(fieldFilter))
            query += ' Field = : fieldFilter ';


        if(query.endsWith('AND'))
            query = query.subString(0, query.length() - 3); //remove and


        if(this.includeDeleted)
            query += ' ALL ROWS ';

        addToLog('Querying for ' + query);

        List<SObject> totalObjs = Database.query(query);
        for(SObject history : totalObjs){
            histories.add(new HistoryWrapper(history));
        }

        addToLog('Found ' + totalObjs.size() + ' object');

        return totalObjs;
    }

    
    /**
    * action for generating the actual report
    */
    public void generateReport(){

        if(validate()){

            this.includeChildren = string.isBlank(this.field) || this.includeChildren;

            addToLog('Include children is ' + this.includeChildren);


            SObjectType ot = Schema.getGlobalDescribe().get(this.objectType);

            HistoryNode rootNode = computeHistoryNode(ot, relatedObjs.get(this.objectType)); 
            histories = rootNode.queryHistory(this);
            //histories = computeHistoryNode(SQX_CAPA__c.getSObjectType(), SQX_Extension_CAPA.getDataLoaderConfig()).queryHistory(this);
            histories.sort();

            
            String reportDateFormat = DateTime.now().format();
            String reportGenerationCompleteMsg = Label.CQ_UI_Report_generation_complete.replace('{ReportFormat}', reportDateFormat);
            reportGenerationCompleteMsg = reportGenerationCompleteMsg.replace('{HistorySize}', String.valueof(histories.size()));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, reportGenerationCompleteMsg));

                
            addToLog('Number of items found: ' + histories.size());
            
            addToLog('Finished Report Generation');


            if(histories.size()  > 1000){

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.CQ_UI_Salesforce_limit_has_been_reached ));
            }
            while(histories.size() > 1000){
                histories.remove(histories.size() - 1);
            }

        }
        
    }

    /**
    */
    public HistoryNode computeHistoryNode(SObjectType rootNodeType, SObjectDataLoader.SerializeConfig config){
        Map<SObjectType, HistoryNode> objectNodes = new Map<SObjectType, HistoryNode>();
        HistoryNode rootNode = new HistoryNode(rootNodeType);


        rootNode.isOneIsToOne = false;
        rootNode.parentNode = null;

        objectNodes.put(rootNodeType, rootNode);

        if(config != null){

            for(SObjectField field : config.followRelationships){

                SObjectType childObjectType = field.getDescribe().getReferenceTo().get(0);
                SObjectType parentObjectType = getChildRelationForField(field).getChildSObject();

                HistoryNode parentNode = objectNodes.get(parentObjectType),
                            childNode = objectNodes.get(childObjectType);

                if(parentNode == null){

                    addToLog('Adding entry for ' + parentObjectType);
                    parentNode = new HistoryNode(parentObjectType);
                    objectNodes.put(parentObjectType, parentNode);
                }
                
                childNode = new HistoryNode(childObjectType);
                objectNodes.put(childObjectType, childNode);
                childNode.parentNode = parentNode;
                childNode.fieldName = '' + field;
                childNode.isOneIsToOne = true;
                parentNode.childNodes.add(childNode);
            

            }

            if(includeChildren){
                for(SObjectField field : config.followChildRelationships){
                    Schema.DescribeFieldResult fieldDesc = field.getDescribe();
                    SObjectType parentObjectType = fieldDesc.getReferenceTo().get(0);
                    Schema.ChildRelationship childRel = getChildRelationForField(field);
                    SObjectType childObjectType = childRel.getChildSObject();

                    HistoryNode parentNode = objectNodes.get(parentObjectType),
                                childNode = objectNodes.get(childObjectType);



                    if(parentNode == null){
                        addToLog('Adding entry for ' + parentObjectType);
                        parentNode = new HistoryNode(parentObjectType);
                        objectNodes.put(parentObjectType, parentNode);
                    }
                    
                
                    childNode = new HistoryNode(childObjectType);
                    objectNodes.put(childObjectType, childNode);
                    childNode.parentNode = parentNode;
                    childNode.isOneIsToOne = false;
                    childNode.fieldName = '' + field;
                    childNode.relationshipName = childRel.getRelationshipName();
                    parentNode.childNodes.add(childNode);
                }
            }

        }


        return  rootNode;

    }



    private with sharing class HistoryNode {
        public SObjectType objectType {get; set;}
        public HistoryNode parentNode {get; set;}
        public List<HistoryNode> childNodes {get; set;}

        public String relationshipName {get; set;}
        
        public boolean isOneIsToOne {get; set;}
        public boolean isHistoryTrackingEnabled {get; set;}

        public String fieldName {get; set;}
        
        public HistoryNode(SObjectType objectType){
            this.objectType = objectType;
            this.isHistoryTrackingEnabled = SQX_Controller_Audit_Trail_Report.historyExists(objectType);
            this.childNodes = new List<HistoryNode>();
        }


        public List<HistoryWrapper> queryHistory(SQX_Controller_Audit_Trail_Report controller){
            List<HistoryWrapper> retList = new List<HistoryWrapper>();

            if(!String.isBlank(controller.objectId)){
                controller.addToLog('Performing forward query');
                return forwardQuery(new Set<Id> { controller.objectId }, new Map<Id, Id>(), controller);
            }
            else{
                controller.addToLog('Performing reverse query');
                retList = reverseQuery(controller.changedFrom, controller.changedUntil, controller);
                //remove all changes whose parent doesn't map to root node.

                
                for(Integer index = retList.size() - 1; index >= 0; index--){
                    HistoryWrapper hist = retList.get(index);
                    String parentType = hist.parent != null ? hist.parent.getSObjectType() + '' : '';
                    /*if( !hist.sObjectApiName.equalsIgnoreCase(controller.objectType)
                        && !parentType.equalsIgnoreCase(controller.objectType))
                    {
                        retList.remove(index);
                    }*/
                }
            }




            return retList;
            
        }


        private List<HistoryWrapper> forwardQuery(Set<Id> objectIds, Map<Id, Id> parentIds, SQX_Controller_Audit_Trail_Report controller){
            List<HistoryWrapper> retList = new List<HistoryWrapper>();

            controller.addToLog('History Tracking for ' + this.objectType + ' is ' + this.isHistoryTrackingEnabled);
            //query the current elements
            if(this.isHistoryTrackingEnabled){
                controller.getHistoryOf(retList, objectIds);
            }

            //if child nodes are present we need to query for each child node
            if(this.childNodes.size()  > 0){
                String query = 'SELECT Id, ';

                for(HistoryNode node : childNodes){
                    if(node.isOneIsToOne){
                        String relName = node.fieldName.replace('__c', '__r');
                        if(relName.endsWith('Id')){
                            relName = relName.subString(0, relName.length() - 1);
                        }

                        query += (' ' + String.escapeSingleQuotes(relName) + '.Id,' );
                    }
                    else{
                        query += '(SELECT Id FROM ' + String.escapeSingleQuotes(node.relationshipName) + '),';
                    }
                }

                if(query.endsWith(',')){
                    query = query.subString(0, query.length() - 1);

                    query += ' FROM ' + String.escapeSingleQuotes(this.objectType + '');
                    query += ' WHERE Id IN :  objectIds'; //in only supports 200 elements might have to come back to this


                    List<SObject> nodeObjs = Database.query(query);

                    for(HistoryNode node : childNodes){
                        if(!node.isOneIsToOne){
                            Set<Id> childObjectIds = new Set<Id>();
                            for(SObject nodeObj : nodeObjs){
                                List<SObject> sobjects = nodeObj.getSObjects(node.relationshipName);
                                if(sobjects != null){
                                    Map<Id, SObject> relObj = new Map<Id, SObject>(sobjects);
                                    childObjectIds.addAll(relObj.keySet());
                                }
                            }

                            if(childObjectIds.size() > 0){
                                retList.addAll(node.forwardQuery(childObjectIds, parentIds, controller));
                            }
                        }
                        else{
                            Set<Id> relatedObjIds = new Set<Id>();
                            for(SObject nodeObj : nodeObjs){
                                String relName = node.fieldName.replace('__c', '__r');
                                if(relName.endsWith('Id')){
                                    relName = relName.subString(0, relName.length() - 1);
                                }

                                SObject rel = nodeObj.getSObject(relName);
                                if(rel != null)
                                    relatedObjIds.add(rel.Id);
                            }

                            if(relatedObjIds.size()  > 0){
                                retList.addAll(node.forwardQuery(relatedObjIds, parentIds, controller));
                            }
                        }
                    }

                    

                }


            }


            return retList;
        }

        private List<HistoryWrapper> reverseQuery(DateTime performedFrom, DateTime performedUntil, SQX_Controller_Audit_Trail_Report controller){
            List<HistoryWrapper> retList = new List<HistoryWrapper>();

            //query the current elements
            if(this.isHistoryTrackingEnabled && performedFrom != null && performedUntil != null){
                controller.getHistoryOf(retList, null, this.objectType);
            }

            if(this.childNodes.size() > 0){
                for(HistoryNode node : childNodes){
                    controller.addToLog('Querying for ' + node);

                    List<HistoryWrapper> childList = node.reverseQuery(performedFrom, performedUntil, controller);
                    retList.addAll(childList);


                }
            }

            return retList;
        }


        public override String toString(){

            return 'Object Type: ' + objectType;
        }
    }


    /**
    * This class helps wraps history objects of multiple types and sort them according to the date, type and field.
    */
    public with sharing class HistoryWrapper implements Comparable {

        public SObject history {get; set;}
        
        public String historySObjectApiName {get; set;}

        public String sObjectType {get; set;}

        public String sObjectApiName {get; set;}

        public SObject parent {get; set;}

        public Boolean isFieldHistory {get; set;}

        /**
        * Default constructor used to wrap the history object.
        */
        public HistoryWrapper(SObject history){
            this.history = history;
            this.historySObjectApiName = history.getSObjectType().getDescribe().getName();
            Schema.DescribeSObjectResult objDesc = ((Id)history.get('ParentId')).getSObjectType().getDescribe(); 
            this.sObjectType = objDesc.getLabel();
            this.sObjectApiName = objDesc.getName();
            this.isFieldHistory = objDesc.fields.getMap().containsKey((String)history.get('Field'));
        }

        public void setParent(Id parentId){
            this.parent = parentId.getSObjectType().newSObject();
            this.parent.Id = parentId;
        }

        /**
        * Compares how two history objects should be sorted.
        */
        public Integer compareTo(Object compareTo) {
            Integer retVal = 0;
            if(compareTo instanceOf HistoryWrapper){
                HistoryWrapper cmpTo = (HistoryWrapper)compareTo;
                DateTime thisCreatedDate = (DateTime)this.history.get('CreatedDate'),
                         cmpToCreatedDate = (DateTime)(cmpTo.history.get('CreatedDate'));

                retVal = thisCreatedDate > cmpToCreatedDate ? -1 : thisCreatedDate < cmpToCreatedDate ? 1 : 0;


                //if the creation date is same we sort based on type
                if(retVal == 0){
                    if(this.parent != null && cmpTo.parent != null ){

                        String  parentId = (String)this.parent.Id,
                                    cmpToParentId = (String)cmpTo.parent.Id;

                        retVal = parentId.compareTo(cmpToParentId);
                    }
                    

                    
                    //if the type is same we sort by field
                    if(retVal == 0){
                        String  field = (String)this.history.get('Field'),
                                cmpToField = (String)cmpTo.history.get('Field');

                        //we want created to be placed at the bottom any other field changes go above the record
                        retVal = field == 'Created' ? 1 : field.compareTo(cmpToField);
                    }

                }
                
            }

            return retVal; //we don't care for other types
        }
    }
}