/**
* Extension class for Equipment Event Schedule object
*/
public with sharing class SQX_Equipment_Event_Schedule {
    
    // Recurring units
    public static final String  RECURRING_UNIT_DAY = 'Day',
                                RECURRING_UNIT_WEEK = 'Week',
                                RECURRING_UNIT_MONTH = 'Month',
                                RECURRING_UNIT_QUARTER = 'Quarter',
                                RECURRING_UNIT_HALF_YEAR = 'Half Year',
                                RECURRING_UNIT_YEAR = 'Year',
    
    // Schedule basis
                                SCHEDULE_BASIS_DUE_DATE = 'Due Date',
                                SCHEDULE_BASIS_PERFORMED_DATE = 'Performed Date';

    public static final String PurposeOfSignature_Event_Sign_Off = 'EventSignOff';
    
    /**
     * map of activity code to purpose of signature label
     */
    public static final Map<String, String> purposeOfSigMap = new Map<String, String>{
        PurposeOfSignature_Event_Sign_Off => Label.SQX_PS_Equipment_Sign_Off_Maintenance_Event
    };
    
    /**
    * returns calculated next due date for the recurring schedule
    */
    public static Date calculateNextDueDate(Date performedDate, Integer recurringInterval, String recurringUnit) {
        if (performedDate != null && recurringInterval > 0 && !String.isBlank(recurringUnit)) {
            if (recurringUnit == RECURRING_UNIT_DAY)
                return performedDate.addDays(recurringInterval);
            
            if (recurringUnit == RECURRING_UNIT_WEEK)
                return performedDate.addDays(7 * recurringInterval);
            
            if (recurringUnit == RECURRING_UNIT_MONTH)
                return performedDate.addMonths(recurringInterval);
            
            if (recurringUnit == RECURRING_UNIT_QUARTER)
                return performedDate.addMonths(3 * recurringInterval);
            
            if (recurringUnit == RECURRING_UNIT_HALF_YEAR)
                return performedDate.addMonths(6 * recurringInterval);
            
            if (recurringUnit == RECURRING_UNIT_YEAR)
                return performedDate.addYears(recurringInterval);
        }
        
        return null;
    }
    
    /**
    * returns performed date depending on the schedule basis.
    * if schedule basis is 'Due Date' and due date is not null then returns due date, else returns last performed date.
    */
    public static Date getPerformedDateByScheduleBasis(String scheduleBasis, Date performedDate, Date dueDate) {
        return (scheduleBasis == SCHEDULE_BASIS_DUE_DATE && dueDate != null ? dueDate : performedDate);
    }
    
    /**
    * returns calculated next due date for the recurring schedule using schedule basis
    */
    public static Date calculateNextDueDateByScheduleBasis(String scheduleBasis, String recurringUnit, Date performedDate, Date dueDate, Integer recurringInterval) {
        Date performedDateToUse = getPerformedDateByScheduleBasis(scheduleBasis, performedDate, dueDate);
        return calculateNextDueDate(performedDateToUse, recurringInterval, recurringUnit);
    }
    
    /**
    * creates new task for a event schedule with proper subject and description and assigned to equipment owner
    */
    public static Task createEventScheduleTask(SQX_Equipment__c eqp, SQX_Equipment_Event_Schedule__c sch) {
        Task tsk = new Task(
            WhatId = sch.Id,
            OwnerId = eqp.OwnerId,
            Subject = 'Equipment - ' + eqp.Name + ' - ' + sch.Name,
            ActivityDate = sch.Next_Due_Date__c,
            Priority = 'Normal',
            Status = 'Not Started',
            Description = 'Equipment - ' + eqp.Name + ' is due for ' + sch.Name + '.'
        );
        
        return tsk;
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified(List<SQX_Equipment_Event_Schedule__c> newList, Map<Id, SQX_Equipment_Event_Schedule__c> oldMap) {
            super(newList, oldMap);
        }
        
        /**
        * Calculates and sets next due date when blank for an recurring event schedule with last performed date and proper recurrence information
        */
        public Bulkified setCalculatedNextDueDateWhenNotSet() {
            final String ACTION_NAME = 'setCalculatedNextDueDateWhenNotSet';
            boolean handleProcessedRecords = trigger.isUpdate; // record ids are null for before insert trigger
            
            if (handleProcessedRecords)
                this.resetView().removeProcessedRecordsFor(SQX.EquipmentEventSchedule, ACTION_NAME);
            
            if (this.view.size() > 0) {
                // process recurring equipment event schedule with last performed date and proper recurrence information and blank next due date
                boolean trueValue = true;
                Rule filterRule = new Rule();
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Recurring_Event__c, RuleOperator.Equals, trueValue);
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Next_Due_Date__c, RuleOperator.Equals, null);
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Last_Performed_Date__c, RuleOperator.NotEquals, null);
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Recurring_Interval__c, RuleOperator.NotEquals, null);
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Recurring_Unit__c, RuleOperator.NotEquals, null);
                
                this.applyFilter(filterRule, RuleCheckMethod.OnCreateAndEveryEdit);
                
                if (filterRule.evaluationResult.size() > 0) {
                    if (handleProcessedRecords)
                        this.addToProcessedRecordsFor(SQX.EquipmentEventSchedule, ACTION_NAME, filterRule.evaluationResult);
                    
                    for (SQX_Equipment_Event_Schedule__c sch : (List<SQX_Equipment_Event_Schedule__c>)filterRule.evaluationResult) {
                        // calculate next due date for last performed date with recurring schedule information
                        Date nextDueDate = SQX_Equipment_Event_Schedule.calculateNextDueDate(sch.Last_Performed_Date__c, (Integer)sch.Recurring_Interval__c, sch.Recurring_Unit__c);
                        
                        if (nextDueDate != null) {
                            // set valid calculated next due date 
                            sch.Next_Due_Date__c = nextDueDate;
                        }
                    }
                }
            }
            
            return this;
        }
        
        /**
        * Creates task or updates due date for exsiting not-completed task for a scheduled equipment event
        */
        public Bulkified createOrUpdateTask() {
            final String ACTION_NAME = 'createOrUpdateTask';
            
            this.resetView().removeProcessedRecordsFor(SQX.EquipmentEventSchedule, ACTION_NAME);
            
            if (this.view.size() > 0) {
                // filter records with active queued records with next due date
                boolean trueValue = true;
                Rule filterRule = new Rule();
                filterRule.addRule(Schema.SQX_Equipment_Event_Schedule__c.Queue_Event_Task__c, RuleOperator.Equals, trueValue);
                
                this.applyFilter(filterRule, RuleCheckMethod.OnCreateAndEveryEdit);
                
                if (filterRule.evaluationResult.size() > 0) {
                    this.addToProcessedRecordsFor(SQX.EquipmentEventSchedule, ACTION_NAME, filterRule.evaluationResult);
                    
                    List<SQX_Equipment_Event_Schedule__c> schList = (List<SQX_Equipment_Event_Schedule__c>)filterRule.evaluationResult;
                    Set<Id> eqpIds = new Set<Id>();
                    Set<Id> taskIds = new Set<Id>();
                    List<Task> tasksToAdd = new List<Task>();
                    List<Task> tasksToUpdate = new List<Task>();
                    Map<Id, Task> schTaskMap = new Map<Id, Task>();
                    
                    for (SQX_Equipment_Event_Schedule__c sch : schList) {
                        // create equipment id list
                        eqpIds.add(sch.SQX_Equipment__c);
                        // create existing task id list
                        if (!String.isBlank(sch.Event_Task_Id__c)) {
                            Id taskId = (Id)sch.Event_Task_Id__c;
                            taskIds.add(taskId);
                        }
                    }
                    
                    // get required equipment and existing open tasks
                    Map<Id, SQX_Equipment__c> eqpMap = new Map<Id, SQX_Equipment__c>([SELECT Id, Name, OwnerId FROM SQX_Equipment__c WHERE Id IN :eqpIds]);
                    Map<Id, Task> taskMap = new Map<Id, Task>([SELECT Id, WhatId, OwnerId, ActivityDate, Status, IsArchived, IsClosed, IsDeleted
                                                               FROM Task WHERE Id IN :taskIds ALL ROWS]);
                    
                    for (SQX_Equipment_Event_Schedule__c sch : schList) {
                        SQX_Equipment__c eqp = eqpMap.get(sch.SQX_Equipment__c);
                        Task schTask;
                        
                        if (!String.isBlank(sch.Event_Task_Id__c)) {
                            Id eventTaskId = (Id)sch.Event_Task_Id__c;
                            if (taskMap.get(eventTaskId) != null) {
                                // set associated task
                                schTask = taskMap.get(eventTaskId);
                            }
                        }
                        
                        if (schTask == null) {
                            // create new task
                            schTask = SQX_Equipment_Event_Schedule.createEventScheduleTask(eqp, sch);
                            // add to insert list
                            tasksToAdd.add(schTask);
                        } else {
                            if (schTask.IsArchived == false && schTask.IsClosed == false && schTask.IsDeleted == false && schTask.Status != 'Completed'
                                && schTask.ActivityDate != sch.Next_Due_Date__c) {
                                // update existing task's next due date to updated next due date of the event if the associated task is not archieved, closed or deleted
                                schTask.ActivityDate = sch.Next_Due_Date__c;
                                // add to update list
                                tasksToUpdate.add(schTask);
                            }
                        }
                        
                        // add schedule task map
                        schTaskMap.put(sch.Id, schTask);
                    }
                    
                    if (tasksToAdd.size() > 0) {
                        // insert tasks
                        new SQX_DB().op_insert(tasksToAdd, new List<Schema.SObjectField>{ });
                    }
                    
                    if (tasksToUpdate.size() > 0) {
                        // update tasks
                        new SQX_DB().op_update(tasksToUpdate, new List<Schema.SObjectField>{ });
                    }
                    
                    // link schedules with its proper task id
                    for (SQX_Equipment_Event_Schedule__c sch : schList) {
                        sch.Event_Task_Id__c = (String)schTaskMap.get(sch.Id).Id;
                    }
                }
            }
            
            return this;
        }
    }
}