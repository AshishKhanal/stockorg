/*
 * test for in Audit_Criteria_class and trigger
 * author: Anish Shrestha
 * date: 2015/05/04
 * story: SQX-1093
 **/
@isTest
public class SQX_Test_Audit_Result_Type_Trigger{

    static boolean runAllTests = true,
                    run_givenUser_SameResultTypeCannotBeCreatedTwice = false,
                    run_givenUser_SameResultTypeValueCannotBeCreatedTwice = false;

    /**
    * Result Type with same name cannot be inserted
    * @author Anish Shrestha
    * @date 2015/05/18
    * @story [SQX-1163]
    */ 
    public testMethod static void givenUser_SameResultTypeCannotBeCreatedTwice(){

        if(!runAllTests && !run_givenUser_SameResultTypeCannotBeCreatedTwice){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){
            SQX_Result_Type__c newResultType = new SQX_Result_Type__c(
                                                        Name = 'RandomType',
                                                        Description__c = 'RandomDescription');

            insert newResultType;

            newResultType = [SELECT Name FROM SQX_Result_Type__c WHERE Id =: newResultType.Id];

            System.assert(newResultType.Name == 'RandomType', 'Result Type must be inserted');



            try{
                newResultType.Name = 'RandomType';

                upsert newResultType;
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Duplicate value on record') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 
        }

    }

    /**
    * Result Type Value with same name cannot be inserted
    * @author Anish Shrestha
    * @date 2015/05/18
    * @story [SQX-1163]
    */ 
    public testMethod static void givenUser_SameResultTypeValueCannotBeCreatedTwice(){

        if(!runAllTests && !run_givenUser_SameResultTypeValueCannotBeCreatedTwice){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){
            SQX_Result_Type__c newResultType = new SQX_Result_Type__c(
                                                        Name = 'RandomType',
                                                        Description__c = 'RandomDescription');

            insert newResultType;

            newResultType = [SELECT Id, Name FROM SQX_Result_Type__c WHERE Id =: newResultType.Id];

            System.assert(newResultType.Name == 'RandomType', 'Result Type must be inserted');

            SQX_Result_Type_Value__c newResultTypeValue = new SQX_Result_Type_Value__c(
                                                                    Name='TypeA',
                                                                    SQX_Result_Type__c = newResultType.Id);
            insert newResultTypeValue;

            System.assert(newResultTypeValue.Name == 'TypeA', 'Result Type must be inserted');

            try{

                newResultTypeValue.Name = 'TypeA';

                upsert newResultTypeValue;
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Duplicate value on record') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 
        }

    }
}