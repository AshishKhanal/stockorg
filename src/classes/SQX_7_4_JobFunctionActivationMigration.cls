/**
* Batch processor for activating Job Functions 
* Added in version  : 7.4
* Used for versions : less than 7.4
* @author  Sudeep Maharjan
*
* WITHOUT SHARING has been used 
* ---------------------------------
* Without sharing has been used because the running user may not have access on all Job Functions 
*/
global without sharing class SQX_7_4_JobFunctionActivationMigration implements Database.Batchable<sObject>{
    /**
     * default batch job statuses to skip migration process
     */
    List<String> excludeMigrationStatuses = new List<String>{
                                                    SQX_Job_Function.BATCH_JOB_STATUS_ACTIVATION_MIGRATION_COMPLETED,
                                                    SQX_Job_Function.BATCH_JOB_STATUS_DEFAULT
                                            };
    
    /**
     * default constructor for job function activation migration
     */
    global SQX_7_4_JobFunctionActivationMigration() { }
    
    /**
     * constructor for job function activation migration
     * @param excludeBatchJobStatuses   skips job function migration for provided batch job statuses including default and activation completed statuses
     */
    global SQX_7_4_JobFunctionActivationMigration(List<String> excludeBatchJobStatuses) {
        if (excludeBatchJobStatuses != null) {
            excludeMigrationStatuses.addAll(excludeBatchJobStatuses);
        }
    }
    
    /**
     * Gets invoked when the batch job starts and selects all job functions which have not been migrated
     * @param bc reference to the Database.BatchableContext object
     * @return Database.QueryLocator object that contains the job functions passed to the job
     */ 
    global database.QueryLocator start(Database.BatchableContext bc){
        // returns inactive job functions that have not been migrated
        return Database.getQueryLocator([SELECT 
                                             Active__c,
                                             Activation_Date__c,
                                             CreatedDate
                                         FROM SQX_Job_Function__c
                                         WHERE Active__c = false
                                            AND Batch_Job_Status__c NOT IN :excludeMigrationStatuses]);
    }
    
    /** 
     * Execute the method to activate job functions 
     * @param bc reference to the Database.BatchableContext object
     * @param scope list of sObjects containing inactive Job Functions
     */
    global void execute(Database.BatchableContext bc, List<sObject> scope){

        // Activation of Job function will activate PJFs and Requirements related to it. This will inturn create Trainings for personnel.
        // Activation of requirement and pjfs must be stopped during migration.
        // Thus 'bypassBulkifiedTrigger' is set to true to by pass related triggers.
        SQX_Job_Function.bypassBulkifiedTrigger = true;

        // List to store job functions to be updated
        List<SQX_Job_Function__c> jfsToUpdate = new List<SQX_Job_Function__c>();
        for(SQX_Job_Function__c jf: (List<SQX_Job_Function__c>)scope){
            jf.Active__c = true;
            jf.Activation_Date__c =  date.valueOf(jf.CreatedDate);
            jf.Batch_Job_Status__c = SQX_Job_Function.BATCH_JOB_STATUS_ACTIVATION_MIGRATION_COMPLETED;
            jfsToUpdate.add(jf);
        }

        //List for jfs which failed in migration
        List<SQX_Job_Function__c> failedJFs = new List<SQX_Job_Function__c>();

        /**
         * Without Sharing is used because migration should affect all data
         * Update continues even when error occurs during batch update
         */
        Database.SaveResult[] saveResults = new SQX_DB()
                                                .withoutSharing()
                                                .continueOnError()
                                                .op_update(jfsToUpdate, new List<SObjectField>{
                                                                            SQX_Job_Function__c.Active__c, 
                                                                            SQX_Job_Function__c.Batch_Job_Status__c});
        for(Integer i = 0; i < saveResults.size(); i++){
            Database.SaveResult saveResult = saveResults[i];
            SQX_Job_Function__c jf = jfsToUpdate[i];

            //If any jf is failed to migrate then add status of Migration Failed
            if(!saveResult.isSuccess()){
                SQX_Job_Function__c failedjf = new SQX_Job_Function__c();
                failedjf.Id = jf.Id;
                failedjf.Batch_Job_Status__c = SQX_Job_Function.BATCH_JOB_STATUS_ACTIVATION_MIGRATION_FAILED;
                failedjf.Batch_Job_Error__c = SQX_Utilities.getFormattedErrorMessage(saveResult.getErrors());
                failedjfs.add(failedjf);
            }
        }

        //If any jf failed to migrate exist update jf
        if(!failedjfs.isEmpty()){

            /**
            * Without Sharing is used because migration should affect all data
            * Update is halted and reverted if error occurs during batch update
            */
            new SQX_DB().withoutSharing().op_update(failedjfs, new List<SObjectField>{SQX_Job_Function__c.Batch_Job_Status__c, SQX_Job_Function__c.Batch_Job_Error__c});
        }
    }
    
    /** 
     * Used to execute post-processing operations and is called once after all batches are processed
     * @param bc reference to the Database.BatchableContext object
     */
    global void finish(Database.BatchableContext bc){ }
}