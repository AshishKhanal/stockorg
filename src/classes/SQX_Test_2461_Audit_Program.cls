/** 
* Test class for the Audit Program its validation rules.
*/
@isTest
public class SQX_Test_2461_Audit_Program{
    private static Boolean  runAllTests = true,
                            givenAduditProgram_WhenAuditIsVoided_AuditIsVoidedSuccessfully = false,
                            givenAduditProgram_WhenAuditIsClosed_AuditIsClosedSuccessfully = false,
                            givenAduditProgram_WhenAduditProgramScopeIsCreatedWithAuditCriteriaDocument_InsertionIsSuccessfull = false;
    @testSetup
    public static void commonSetup() {
        
        //add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'standardUser');
    }

    /**
    * Given: Creat an Audit Program and save.
    * When: Audit program is voided 
    * Then: Audit Program is Voided successfully ie status is Void
    *
    * @date:2016/8/30
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2461]
    */
    public testmethod static void givenAduditProgram_WhenAduditProgramIsVoided_AduditProgramIsVoidedSuccessfully(){
        if(!runAllTests && !givenAduditProgram_WhenAuditIsVoided_AuditIsVoidedSuccessfully){
            return;
        }

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create and save the audit program
            SQX_Test_Audit_Program auditProgram = new SQX_Test_Audit_Program().save();

            //Assert : Audit proram is successfully saved
            System.assert(auditProgram.program.Id != null, 'Expected audit program to be saved');

            //Create next action
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', SQX_Extension_Audit_Program.NXTACT_VOID);
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            //Act : Void the Audit Program
            String mainRecordId = SQX_Extension_Audit_Program.processChangeSetWithAction(''+auditProgram.program.Id, '{"changeSet": []}', null, params , null);

            //Assert :  Process changeset returns the Id of the main record
            System.assertEquals(''+auditProgram.program.Id,mainRecordId,'Expected to retrun main record Id');
            //Assert : Audit program is voided successfully
            SQX_Audit_Program__c auditProgram1 = [SELECT Status__c from SQX_Audit_Program__c WHERE Id =: auditProgram.program.Id];
            System.assertEquals(SQX_Audit_Program.STATUS_VOID,auditProgram1.Status__c,'Expected the Audit Program to be void');
            
            //Act : Void the Audit Program Again to test Exception
            try{
                SQX_Extension_Audit_Program.processChangeSetWithAction(''+auditProgram.program.Id, '{"changeSet": []}', null, params , null);
                System.assert(false,'Excepted to throw exception');
            }catch(Exception e){
                System.assertEquals(Label.SQX_ERR_MSG_AUDIT_PROGRAM_IS_ALREADY_VOID, e.getMessage(),'Excpected the exception message value is from Custom label');
            }
        }
    }

    /**
    * Given: Creat an Audit Program and save.
    * When: Audit program is closed 
    * Then: Audit Program is closed successfully ie status is Closed
    *
    * @date:2016/8/30
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2461]
    */
    public testmethod static void givenAduditProgram_WhenAduditProgramIsClosed_AduditProgramIsClosedSuccessfully(){
        if(!runAllTests && !givenAduditProgram_WhenAuditIsClosed_AuditIsClosedSuccessfully){
            return;
        }

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create and save the audit program
            SQX_Test_Audit_Program auditProgram = new SQX_Test_Audit_Program().save();

            //Assert : Audit proram is successfully saved
            System.assert(auditProgram.program.Id != null, 'Expected audit program to be saved');

            //Create next action
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', SQX_Extension_Audit_Program.NXTACT_CLOSE);
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            //Act : Void the Audit Program
            String mainRecordId = SQX_Extension_Audit_Program.processChangeSetWithAction(''+auditProgram.program.Id, '{"changeSet": []}', null, params , null);

            //Assert :  Process changeset returns the Id of the main record
            System.assertEquals(''+auditProgram.program.Id,mainRecordId,'Expected to retrun main record Id');
            //Assert : Audit program is void successfully
            SQX_Audit_Program__c auditProgram1 = [SELECT Status__c from SQX_Audit_Program__c WHERE Id =: auditProgram.program.Id];
            System.assertEquals(SQX_Audit_Program.STATUS_CLOSED,auditProgram1.Status__c,'Expected the Audit Program to be closed');
            
            //Act : Close the Audit Program Again to test Exception
            try{
                SQX_Extension_Audit_Program.processChangeSetWithAction(''+auditProgram.program.Id, '{"changeSet": []}', null, params , null);
                System.assert(false,'Excepted to throw exception');
            }catch(Exception e){
                System.assertEquals(Label.SQX_ERR_MSG_AUDIT_PROGRAM_IS_ALREADY_CLOSED, e.getMessage(),'Excpected the exception message value is from Custom label');
            }
        }
    }

    /**
    * Given: Creat an Audit Program and save.
    * When: Audit program scope is created and saved with audit criteria document and save
    * Then: Audit Program Scope should be persisted successfully
    *
    * @date:2016/8/30
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2504]
    */
    public testmethod static void givenAduditProgram_WhenAduditProgramScopeIsCreatedWithAuditCriteriaDocument_InsertionIsSuccessfull(){
        if(!runAllTests && !givenAduditProgram_WhenAduditProgramScopeIsCreatedWithAuditCriteriaDocument_InsertionIsSuccessfull){
            return;
        }

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            //Arrange: Create and save the audit program
            SQX_Test_Audit_Program auditProgram = new SQX_Test_Audit_Program().save();

            //Assert : Audit proram is successfully saved
            System.assert(auditProgram.program.Id != null, 'Expected audit program to be saved');

            //Arrange : Create Controlled Document with status current and record type 'Audit_Criteria'
            SQX_Test_Controlled_Document controlledDoc = new SQX_Test_Controlled_Document().save();
            controlledDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            controlledDoc.doc.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ControlledDoc, SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            controlledDoc.save();

            //Arrange : Create the Audit program scope with audit criteria document
            integer randomNumber = (integer)(Math.random() * 1000000);

            SQX_Audit_Program_Scope__c scope = new SQX_Audit_Program_Scope__c(
                SQX_Audit_Criteria_Document__c = controlledDoc.doc.Id,
                SQX_Audit_Program__c = auditProgram.program.Id,
                Name = 'RandomAuditProgramScope-' + randomNumber
            );

            //Act : Insert the audit program having audit criteria document
            List<DataBase.SaveResult> result = new SQX_DB().op_insert(new List<SQX_Audit_Program_Scope__c> { scope }, new List<Schema.SObjectField>{});

            //Assert : Audit program scope insertion is successfull
            System.assert(result[0].isSuccess(),'Expected program scope to be inserted');
        }
    }
}