/**
* @author Sagar Shrestha
* @date 2014/11/18
* @description Provides an implementation for the HttpCalloutMock interface to specify the provide a way to send in mock requests 
* Goto https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_restful_http_testing_httpcalloutmock.htm for more details
*/
@Istest
public class SQX_Test_OauthEsignatureValidation {
    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; //[SQX-853] Esignature is not validating password when consumer key is wrong.
        public String recievedPassword {get; set;}
        /**
        * @author Sagar Shrestha
        * @date 2014/11/18
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();

            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            PageReference ref = new PageReference(req.getEndpoint());
            this.recievedPassword = ref.getParameters().get('password');

            if(Invalid_Request){
                response.setStatusCode(400);
                response.setBody('Error: 400');
            }
            else{
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();

                if(url.contains('rightPassword')){
                    response.setBody('{"id":"allcorrect"}');
                }
                else{
                    response.setBody('{"error":"incorrect password"}');
                }
            
            }


            return response;
        }
    }
    
    public testmethod static void eSigVerification(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','rightPassword'));
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','anyPassword'));
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','rightPassword'));
        System.assertEquals(true, new SQX_OauthEsignatureValidation().validateCredentials(UserInfo.getUserName(),'rightPassword'));
        Test.stopTest();
    }

    /**
    * @author : Anish Shrestha
    * @date : 2014/12/11
    * @description: test for esignature when there is invalid consumer key or secret.
    * @story [SQX-853]
    */

    public testmethod static void eSigVerificationWithInvalidRequest(){
        Test.startTest();
        MockAuthClass mockAuthObject =  new MockAuthClass();
        mockAuthObject.Invalid_Request = true;
        Test.setMock(HttpCalloutMock .class, mockAuthObject);//instruct the Apex runtime to send this fake response
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','rightPassword'));
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','anyPassword'));
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials('anyusername','rightPassword'));
        System.assertEquals(false, new SQX_OauthEsignatureValidation().validateCredentials(UserInfo.getUserName(),'rightPassword'));
        Test.stopTest();
    }

    /**
    * SQX-2182
    * Test that simple or complex password are returned properly
    **/
    public testmethod static void passwordCharsVerification(){
        Test.startTest();
        MockAuthClass mock = new MockAuthClass();
        Test.setMock(HttpCalloutMock.class, mock);//instruct the Apex runtime to send this fake response

        final String simplePassword = 'ambarkaarCQ';
        new SQX_OauthEsignatureValidation().validateCredentials(UserInfo.getUserName(),simplePassword);
        System.assertEquals(simplePassword, mock.recievedPassword);

        final String complexPassword = 'asdasd@#asda21?12312asdasd';
        new SQX_OauthEsignatureValidation().validateCredentials(UserInfo.getUserName(),complexPassword);
        System.assertEquals(complexPassword, mock.recievedPassword);
        Test.stopTest();
    }

}