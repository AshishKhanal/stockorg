@isTest  
private class MetadataServiceExamplesTest
{ 
    @IsTest
    private static void coverMetaDataServiceExamples(){
        //Arrange: create a saveresult instance with saveresult set as false
        MetadataService.SaveResult saveResult = new MetadataService.SaveResult();
        saveResult.success = false;
        //handleSaveResult should throw exception
        try{
            MetadataServiceExamples.handleSaveResults(saveResult);
        }
        catch(Exception e){
            System.assertEquals(true, e.getMessage().contains('Request failed with no specified error'), 'Should throw exception with no specified message as no error are mentioned');
        }

        // Arrange Add error to saveresult
        MetadataService.Error error1 = new MetadataService.Error();
        error1.message = 'This is the error';
        saveResult.errors= new List<MetadataService.Error>{error1};
        saveResult.success= false;

        // Act : handleSaveResult should throw exception containing error message as set in error
        try{
            MetadataServiceExamples.handleSaveResults(saveResult);
        }
        catch(Exception e){
            //Asset that exception contains error message
            System.assertEquals(true, e.getMessage().contains('This is the error'), 'Should throw exception containing message in error message');
        }

        //set save result as false
        saveResult.success = true;
        //Should not throw exception
        MetadataServiceExamples.handleSaveResults(saveResult);//shouldn't throw exception
        
    }
}