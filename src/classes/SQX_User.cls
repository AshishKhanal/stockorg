/**
* @author Dibya Shrestha
* This class will handles trigger for SF User object
*/
public with sharing class SQX_User {

    // SF internal user's UserType.
    public static final String USER_TYPE_AUTOMATEDPROCESS = 'AutomatedProcess';
    
    /**
    * Syncs/copies existing personnel record of an updated user details
    */
    @future
    public static void syncPersonnelFromUser(Map<Id, String> oldUsernames, Map<Id, Id> oldManagerIds) {
        // get latest records with required fields
        Map<Id, User> users = new Map<Id, User>(
            [SELECT Id, FirstName, LastName, Username, Email, ManagerId, IsActive FROM User WHERE Id IN :oldUsernames.keySet()]
        );
        
        // get personnel records of the updated users
        List<SQX_Personnel__c> updateList = [SELECT Id, Identification_Number__c, SQX_User__c, OwnerId
                                             FROM SQX_Personnel__c
                                             WHERE SQX_User__c IN :users.keySet()];
        
        // update fields
        for (SQX_Personnel__c p : updateList) {
            User newUser = users.get(p.SQX_User__c);
            String oldUsername = oldUsernames.get(p.SQX_User__c);
            Id oldManagerId = oldManagerIds.get(p.SQX_User__c);
            
            p.Full_Name__c = newUser.FirstName + ' ' + newUser.LastName;
            p.Email_Address__c = newUser.Email;
            p.Active__c = newUser.isActive;
            if (p.Identification_Number__c.equalsIgnoreCase(oldUsername)) {
                p.Identification_Number__c = newUser.Username;
            }
            if (p.OwnerId.equals(oldManagerId) && newUser.ManagerId != null) {
                p.OwnerId = newUser.ManagerId;
            }
        }
        
        if (updateList.size() > 0) {
            // update related personnel records of updated users for which current user may or may not have permissions
            new SQX_DB().withoutSharing().op_update(updateList, new List<Schema.SObjectField>{
                Schema.SQX_Personnel__c.Full_Name__c,
                Schema.SQX_Personnel__c.SQX_User__c,
                Schema.SQX_Personnel__c.Identification_Number__c,
                Schema.SQX_Personnel__c.Email_Address__c,
                Schema.SQX_Personnel__c.OwnerId,
                Schema.SQX_Personnel__c.Active__c
            });
        }
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified(List<User> newList, Map<Id,User> oldMap){
            super(newList, oldMap);
        }
        
        /**
        * Syncs/copies existing personnel record of an updated user details
        */
        public Bulkified syncPersonnelRecords() {
            if (this.oldValues.size() > 0) {
                Map<Id, String> oldUsernames = new Map<Id, String>();
                Map<Id, Id> oldManagerIds = new Map<Id, Id>();
                
                for (Id uId : this.oldValues.keySet()) {
                    User oldValue = (User)this.oldValues.get(uId);
                    oldUsernames.put(uId, oldValue.Username);
                    oldManagerIds.put(uId, oldValue.ManagerId);
                }
                
                syncPersonnelFromUser(oldUsernames, oldManagerIds);
            }
            
            return this;
        }
    }
}