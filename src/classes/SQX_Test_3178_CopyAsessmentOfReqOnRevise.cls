/**
 * This test ensures transfering assessment field values to new revision when revised
 */
@isTest
public class SQX_Test_3178_CopyAsessmentOfReqOnRevise {

     @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
    }
    
    /**
     * Given : Controlled document with requirements having initial, revision and refresher assessments.
     * When : Doc is revised
     * Then : Only valid assessments(ie non obsolete) should be transfered to newly created requirement of revised doc
     */
    public testmethod static void givenDocWithRequirement_WhenDocIsRevised_ThenAllValidAssessmentsOfRequirementIsTransferedToNewRequirementOfRevisedDoc() {
        
        SQX_Test_Controlled_Document testDoc;

        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            
            // Arrange : Create document
            testDoc = new SQX_Test_Controlled_Document();
            testDoc.save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            // Arrange : create the required 3 assessments
            SQX_Test_Assessment initialAssessment = new SQX_Test_Assessment().save();
            SQX_Test_Assessment revisionAssessment = new SQX_Test_Assessment().save();
            SQX_Test_Assessment refresherAssessment = new SQX_Test_Assessment().save();
            
            // Arrange : creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            Database.SaveResult jf1Result = Database.insert(jf1, false);

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false,
                SQX_Initial_Assessment__c = initialAssessment.assessment.Id,
                SQX_Revision_Assessment__c = revisionAssessment.assessment.Id,
                Require_Refresher__c = true,
                Refresher_Interval__c = 30, 
                Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                Days_to_Complete_Refresher__c = 2,
                Days_in_Advance_To_Start_Refresher__c = 5,
                SQX_Refresher_Assessment__c = refresherAssessment.assessment.Id
            );
            
            // insert the requirement
            List<Database.SaveResult> result = new SQX_DB().op_insert(new List<SQX_Requirement__c>{Req1},
                                                                new List<Schema.SObjectField>{
                                                                        SQX_Requirement__c.SQX_Controlled_Document__c,
                                                                        SQX_Requirement__c.SQX_Job_Function__c,
                                                                        SQX_Requirement__c.Level_of_Competency__c,
                                                                        SQX_Requirement__c.Active__c,
                                                                        SQX_Requirement__c.SQX_Initial_Assessment__c,
                                                                        SQX_Requirement__c.SQX_Revision_Assessment__c,
                                                                        SQX_Requirement__c.Require_Refresher__c,
                                                                        SQX_Requirement__c.Refresher_Interval__c,
                                                                        SQX_Requirement__c.Refresher_Competency__c,
                                                                        SQX_Requirement__c.Days_to_Complete_Refresher__c,
                                                                        SQX_Requirement__c.Days_in_Advance_To_Start_Refresher__c,
                                                                        SQX_Requirement__c.SQX_Refresher_Assessment__c
                                                                    });
            
            // Assert : ensure the requirement is inserted properly.
            System.assert(result[0].isSuccess(), 'Requirement should be inserted');
            
            // Obsolete the initial assessment
            initialAssessment.assessment.Status__c = SQX_Assessment.STATUS_OBSOLETE;
            initialAssessment.save();            
            
            // ACT: revise documentdoc.
            testDoc = testDoc.revise();
            testDoc.doc.Revision__c = 'RANDOM-2';
            testDoc.save();
            
            // Assert : Ensure that only revision and refresher assessments are transfered to requirement of revised doc since intial assessment is oboselte.
            SQX_Requirement__c revisedReq = [SELECT SQX_Initial_Assessment__c, SQX_Revision_Assessment__c, SQX_Refresher_Assessment__c FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c =: testDoc.doc.Id];
            System.assertEquals(Req1.SQX_Refresher_Assessment__c, revisedReq.SQX_Refresher_Assessment__c,'Expected the refresher assessment should be transfered');
            System.assertEquals(Req1.SQX_Revision_Assessment__c, revisedReq.SQX_Revision_Assessment__c,'Expected the revision assessment should be transfered');
            System.assert(String.isBlank(revisedReq.SQX_Initial_Assessment__c),'Obsolete initial assessment should not be copied to revised requirement');
            
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            testDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            testDoc.save();
        }
        
        // Act : Revise the document being standard user who does not have read access on the assessment of requirement.
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser1')) {
            Test.startTest();
            testDoc = testDoc.revise();

            // Assert : Ensure that none of the assessments get copied to new requirement of revised document
            SQX_Requirement__c revisedReq = [SELECT SQX_Initial_Assessment__c, SQX_Revision_Assessment__c, SQX_Refresher_Assessment__c FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c =: testDoc.doc.Id];
            System.assertEquals(null, revisedReq.SQX_Refresher_Assessment__c,'Refresher assessment should be null to newly created rquirement of revised doc as user has no read access on the assessment');
            System.assertEquals(null, revisedReq.SQX_Revision_Assessment__c,'Revision assessment should be null to newly created rquirement of revised doc as user has no read access on the assessment');
            System.assertEquals(null,revisedReq.SQX_Initial_Assessment__c,'Initial assessment should be null to newly created rquirement of revised doc as user has no read access on the assessment');
            Test.stopTest();
        }
    }
}