/*
* Unit test for prevent draft record to close in SD and NSI.
*/
@isTest
public class SQX_Test_7465_Prevent_Close_Action {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
    }
    
    /*
     * Given: Draft Supplier Deviation Record
     * When: User tries to close record
     * Then: Validation Error message is thrown
     */
    @isTest
    public static void givenSDDraftRecord_whenUserClose_ThenErrorIsThrown(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            //Arrange: Create Supplier Deviation Record
            SQX_Test_Supplier_Deviation sdRecord = new SQX_Test_Supplier_Deviation().save();
            
            //Act: Close the SD draft record
            sdRecord.sd.Status__c = SQX_Supplier_Deviation.STATUS_CLOSED;
            sdRecord.sd.Activity_Code__c = 'close';
            Database.SaveResult result = Database.update(sdRecord.sd, false);

            //Assert: Validation Error message 'Unable to perform the desired action on the record. Please ensure that the record's status/stage permits the action.' is thrown.
            System.assertEquals('Unable to perform the desired action on the record. Please ensure that the record\'s status/stage permits the action.',result.getErrors()[0].getMessage());
        }
    }
    
    /*
     * Given: Draft Supplier Introduction Record
     * When: User tries to close record
     * Then: Validation Error message is thrown
     */
    @isTest
    public static void givenNSIDraftRecord_whenUserClose_ThenErrorIsThrown(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            //Arrange: Create NSI Record
            SQX_Test_NSI nsiRecord = new SQX_Test_NSI().save();
            
            //Act: Close the NSI draft record
            nsiRecord.nsi.Status__c = SQX_NSI.STATUS_CLOSED;
            nsiRecord.nsi.Activity_Code__c = 'close';
            Database.SaveResult result = Database.update(nsiRecord.nsi, false);

            //Assert: Validation Error message 'Unable to perform the desired action on the record. Please ensure that the record's status/stage permits the action.' is thrown.
            System.assertEquals('Unable to perform the desired action on the record. Please ensure that the record\'s status/stage permits the action.',result.getErrors()[0].getMessage());
        }
    }
}