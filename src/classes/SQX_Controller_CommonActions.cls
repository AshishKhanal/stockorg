/**
* Responsible for receiving actions from buttons in standard layout
* i. Revising investigation
* ii. Publishing response
*/
public with sharing class SQX_Controller_CommonActions{
    
    transient private Id redirectToId = null;
    //public because simplifies key access in test classes
    public static final String ACTION_GET_KEY = 'action'; /** GET URL parameter for setting the action to be performed */
    public static final String OBJECT_ID_GET_KEY = 'id'; /** GET URL parameter for sending the ID of response */
    

    public static final String HTTP_HEADER_REFERER  = 'Referer';

    public static final string ACTION_PUBLISH_RESPONSE = 'publish';
    public static final string ACTION_REVISE_INVESTIGATION = 'revise';


    /**
    * returns the redirect url to navigate to by the view
    * @return if redirectToURL is set to null from the page, redirects back to the referring page. 
    */
    public Id getRedirectToId(){
        if(redirectToId == null)
            return Id.valueof(ApexPages.currentPage().getParameters().get(OBJECT_ID_GET_KEY)) ;
            
        return  redirectToId ;
    }
    
    /**
    * nothing in the constructor
    */
    public SQX_Controller_CommonActions(){

    }
    

    /**
    * receives the action from the page on every load.
    * It is responsible for performing the correct action based on the GET parameters passed
    * Currently supported get parameters are
    * id -> findingResponse : if the action is for a finding response
    *        -> investigation : if the action is for an investigation
    * action -> publish : to publish a given finding response
    *        -> revise : if the investigation is to be revised, creates a new investigation and redirects to it.
    */
    public PageReference performAction(){
        
        try{
            ID objectID = ID.valueOf(ApexPages.currentPage().getParameters().get(OBJECT_ID_GET_KEY));
            String action = ApexPages.currentPage().getParameters().get(ACTION_GET_KEY);

            Schema.SObjectType objectType = objectID.getSObjectType();
            String sObjectName = objectType.getDescribe().getName();
            
            if(sObjectName == SQX.Response){
                performActionsForResponse(objectID, action);
            }
            else if(sObjectName == SQX.Investigation){
                performActionsForInvestigation(objectID,action);
            }
            else if(sObjectName == SQX.Finding){
                performActionsForFinding(objectID, action);
            }
            else if(sObjectName == SQX.Capa){
                performActionsForCAPA(objectID, action);
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid object found'));
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        

        Id redirectId = getRedirectToId();
        SObject sobjectToRedirect = redirectId.getSObjectType().newSObject();
        sobjectToRedirect.put('Id', getRedirectToId());
        PageReference reference =  new ApexPages.StandardController(sobjectToRedirect).view();
    
        return reference;
    }
    
    /**
    * cancels the intended action from being performed
    */
    public PageReference cancelAction(){
        Id redirectId = getRedirectToId();
        SObject sobjectToRedirect = redirectId.getSObjectType().newSObject();
        sobjectToRedirect.put('Id',redirectID);
        
        PageReference reference =  new ApexPages.StandardController(sobjectToRedirect).view();

        return reference;
    }

    /**
    * performs a given action on the provided CAPA ID
    * @param capaID the ID of the CAPA to changed
    * @param action the action to be performed on the CAPA, supported action is 'publish'
    */    
    private void performActionsForCAPA(Id capaID, String action){
        List<SQX_CAPA__c> capas = [SELECT ID, Status__c from SQX_CAPA__c
                                                           WHERE ID = :capaID];
        if(capas.size() == 1){
            SQX_CAPA__c capa = capas.get(0);
            
            if(action == ACTION_PUBLISH_RESPONSE){
                if(capa.Status__c == SQX_Finding.STATUS_DRAFT){
                    capa.Status__c = SQX_Finding.STATUS_OPEN;
                    new SQX_DB().op_update(new List<SQX_CAPA__c>{capa}, new List<SObjectField>{SQX_CAPA__c.field.Status__c});

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Publishing CAPA. Please wait...'));
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CAPA is already published'));
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Action'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record does not exists.'));
        }
    }

    /**
    * performs a given action on the provided finding ID
    * @param findingID the ID of the finding to changed
    * @param action the action to be performed on the finding, supported action is 'publish'
    */    
    private void performActionsForFinding(Id findingID, String action){
        List<SQX_Finding__c> findings = [SELECT ID, Status__c from SQX_Finding__c
                                                           WHERE ID = :findingID];
        if(findings.size() == 1){
            SQX_Finding__c finding = findings.get(0);
            
            if(action == ACTION_PUBLISH_RESPONSE){
                if(finding.Status__c == SQX_Finding.STATUS_DRAFT){
                    finding.Status__c = SQX_Finding.STATUS_OPEN;
                    new SQX_DB().op_update(new List<SQX_Finding__c>{finding}, new List<SObjectField>{SQX_Finding__c.field.Status__c});
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Publishing finding. Please wait...'));
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Finding is already published'));
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Action'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record does not exists.'));
        }
    }
    
    

    /**
    * performs a given action on the provided response ID
    * @param responseID the ID of the response to changed
    * @param action the action to be performed on the response, supported action is 'publish'
    */    
    private void performActionsForResponse(Id responseID, String action){
        List<SQX_Finding_Response__c> responses = [SELECT ID, Status__c from SQX_Finding_Response__c
                                                           WHERE ID = :responseID];
        if(responses.size() == 1){
            SQX_Finding_Response response = new SQX_Finding_Response(responses.get(0));
            
            if(action == ACTION_PUBLISH_RESPONSE){
                response.publish();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Publishing response. Please wait...'));
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Action'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record does not exists.'));
        }
    }

    
    /**
    * performs a given action on the provided investigation id
    * @param investigationID the ID of the investigation to manipulated
    * @param action the action to be performed, supported action is 'revise'
    */
    private void performActionsForInvestigation(Id investigationID, string action){
        List<SQX_Investigation__c> investigations = [SELECT ID,Status__c,Approval_Status__c FROM SQX_Investigation__c
                                                     WHERE ID = :investigationID];
        
        if(investigations.size() == 1){
            SQX_Investigation investigation = new SQX_Investigation(investigations.get(0));
            
            if(action == ACTION_REVISE_INVESTIGATION){
                if(investigations.get(0).Status__c == SQX_Investigation.STATUS_PUBLISHED){
                    SQX_Investigation__c clonedInvestigation = investigation.cloneInvestigationAndRelatedList();
                    redirectToId = clonedInvestigation.Id;
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only completed investigations can be revised'));
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Action'));
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record does not exists.'));
        }
    }
    

    /**
    * returns the referrer of the HTTP header to the view.
    */
    public String getReferer(){
        return ApexPages.currentPage().getHeaders().get(HTTP_HEADER_REFERER);
    }
    
}