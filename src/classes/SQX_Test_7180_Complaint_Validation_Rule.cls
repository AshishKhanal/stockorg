/*
 * Test class for validation rule of patient involved field
 */
@isTest
public class SQX_Test_7180_Complaint_Validation_Rule {
    @testSetup
    public static void commonSetup(){
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');   
    }
    
    /*
     * Given: Complaint record
     * When: Patient Involved is checked and required fields are not filled
     * Then: Throw validation error
     */
    public static testMethod void givenComplaint_WhenRequiredFieldsNotFilled_ThenThrowValidationError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: Create complaint record
            List<compliancequest__SQX_Complaint__c> complaints = new List<compliancequest__SQX_Complaint__c>();
            complaints.add(new compliancequest__SQX_Complaint__c(compliancequest__Country_of_Origin__c = 'AF',
                                                                 compliancequest__Complaint_Title__c = 'TestComplaint1',
                                                                 compliancequest__Patient_Involved__c = true,
                                                                 compliancequest__Patient_Gender__c = 'Male',
                                                                 compliancequest__Patient_Weight__c = 40,
                                                                 compliancequest__Unit_Of_Weight__c	= 'Kilograms',
                                                                 compliancequest__Patient_Age__c = 40));
            complaints.add(new compliancequest__SQX_Complaint__c(compliancequest__Country_of_Origin__c = 'AF',
                                                                 compliancequest__Complaint_Title__c = 'TestComplaint2',
                                                                 compliancequest__Patient_Involved__c = false,
                                                                 compliancequest__Patient_Gender__c = null,
                                                                 compliancequest__Patient_Age__c = 0));
            complaints.add(new compliancequest__SQX_Complaint__c(compliancequest__Country_of_Origin__c = 'AF',
                                                                 compliancequest__Complaint_Title__c = 'TestComplaint3',
                                                                 compliancequest__Patient_Involved__c = true,
                                                                 compliancequest__Patient_Age__c = 0,
                                                                 compliancequest__Patient_Gender__c = null,
                                                                 compliancequest__Patient_Weight__c = 0,
                                                                 compliancequest__Unit_Of_Weight__c	= null));
            
            //Act: Save the complaints records
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(complaints, new List<Schema.SObjectField>{});
            
            //Assert: Verify the validation rules
            System.assertEquals(true,result[0].isSuccess(),'Complaint record should be saved'+ result[0].getErrors());
            System.assertEquals(true,result[1].isSuccess(),'Complaint record should be saved');
            System.assertEquals(false,result[2].isSuccess(),'Complaint record should not be saved and validation error is thrown');
            System.assertEquals(result[2].getErrors()[0].getMessage(),'Customer Age should be greater than zero');
            System.assertEquals(result[2].getErrors()[1].getMessage(),'Customer Gender is required');
            System.assertEquals(result[2].getErrors()[2].getMessage(),'Customer Weight should be greater than zero');
            System.assertEquals(result[2].getErrors()[3].getMessage(),'Unit of weight is required');           
        }
    }
}