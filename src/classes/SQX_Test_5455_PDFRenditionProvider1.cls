/**
* This test class is for testing integration of salesforce with proxy server
*/
@isTest
public class SQX_Test_5455_PDFRenditionProvider1 {

    @testSetup
    public static void commonSetup() {
        //Add required users 
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Test_Controlled_Document controlledDocument = new SQX_Test_Controlled_Document();
            controlledDocument.updateContent(true, 'Content.txt');
            controlledDocument.doc.Document_Number__c = 'Controlled_Document_1';
            controlledDocument.save();
        }
    }
    
    final static Integer MODE_SUBMIT = 10,
                         MODE_SUBMIT_ERROR = 30,
                         MODE_FETCH = 70;
    
    final static String JOB_ID = '00000000005E8B5C',
                        PROVIDER_NAME = 'Provider Name 1';

    /*
    * return controlled documents information
    * @param documentNumber is controlled document number
    */
    static SQX_Controlled_Document__c getDoc(String documentNumber){
        return [SELECT Id, Content_Reference__c, Description__c, Title__c, 
                Synchronization_Status__c, Approval_Status__c, Document_Status__c, Secondary_Content_Reference__c, OwnerId
                FROM SQX_Controlled_Document__c
                WHERE Document_Number__c = : documentNumber];
    }

    /**
    * Given: Primary Content with file size larger than Salesforce Limit
    * When: Primary Content with file size larger than Salesforce limit is sent for rendition
    * Then: Proxy Server is called rather than easypdf
    *
    * @author: Sanjay Maharjan
    * @date: 6/25/2018
    * @story: [SQX-5455]
    */
    testmethod static void givenPrimaryContentLargerThanSFLimit_WhenSubmitForRendition_ThenProxyServerIsCalled() {

        SQX_PDFRenditionProvider1.isTest = true;
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            // Arrange: Create controlled document, rendition request and mock http callout
            SQX_Controlled_Document__c doc = getDoc('Controlled_Document_1');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.Content = [SELECT Id, PathOnClient, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            SQX_RenditionProvider provider = new SQX_PDFRenditionProvider1();
            //MODE_SUBMIT
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_SUBMIT;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            
            //Act: submit the http request to easyPDF API
            SQX_RenditionResponse res=provider.submitRequest(request);
            
            //Assert: Ensure that the status was processing 
            System.assertEquals(SQX_RenditionResponse.Status.Processing, res.status);

            //MODE_SUBMIT_ERROR           
            MockHttpResponseGenerator mockHttpObj1= new MockHttpResponseGenerator();
            mockHttpObj1.Mode=MODE_SUBMIT_ERROR;
            Test.setMock(HttpCalloutMock.class, mockHttpObj1);
            //Act: submit the http request to easyPDF API
            SQX_RenditionResponse res1=provider.submitRequest(request);
            //Assert: Ensure that the status was Error 
            System.assertEquals(SQX_RenditionResponse.Status.Error, res1.status);
            System.assertEquals(SQX_RenditionResponse.ErrorCodes.UnknownError, res1.errorCode);
            Test.stopTest();
        }
    }
    
    /**
    * Given: Secondary Content with file size larger than Salesforce Limit
    * When: Retrieving From Easypdf server
    * Then: Proxy Server is called rather than easypdf
    *
    * @author: Sanjay Maharjan
    * @date: 6/25/2018
    * @story: [SQX-5455]
    */
    testmethod static void givenSecondaryContentLargerThanSFLimit_WhenRetrieveFromEasyPdf_ThenProxyServerIsCalled() {

        SQX_PDFRenditionProvider1.isTest = true;
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            // Arrange: Create controlled document, rendition request and mock http callout
            SQX_Controlled_Document__c doc = getDoc('Controlled_Document_1');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.jobId = JOB_ID;
            request.providerName = PROVIDER_NAME;
            request.Content = [SELECT Id, PathOnClient, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            SQX_PDFRenditionProvider1 provider = new SQX_PDFRenditionProvider1();
            //MODE_FETCH
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_FETCH;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            
            //Act: submit the retrive data
            SQX_RenditionResponse res=provider.retrieveThroughProxy(request);
            //Assert: Ensure the status was Processing
            System.assertEquals(SQX_RenditionResponse.Status.Processing, res.status);
            Test.stopTest();
        }
    }

    /**
    * Given: Pdf file is being processed in easy pdf server
    * When: Metadata of the file is fetched
    * Then: Required information of the file is fetched
    *
    * @author: Sanjay Maharjan
    * @date: 6/25/2018
    * @story: [SQX-5455]
    */
    testmethod static void givenFileProcessedInEasyPdfServer_WhenMetadataOfFileIsFetched_ThenRequiredInformationOfFileIsRecieved() {

        SQX_PDFRenditionProvider1.isTest = true;

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){

            SQX_Controlled_Document__c doc = getDoc('Controlled_Document_1');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.jobId=JOB_ID;
            request.Content = [SELECT Id, PathOnClient, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            SQX_PDFRenditionProvider1 provider = new SQX_PDFRenditionProvider1();
            //MODE_FETCH
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_FETCH;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            
            //Act: submit the retrive data
            SQX_RenditionResponse res=provider.retrieveMetadata(request);
            //Assert: Ensure the status was RenditionReady
            System.assertEquals(SQX_RenditionResponse.Status.RenditionReady, res.status);
            System.assertEquals(500, res.bytes);
            Test.stopTest();

        }

    }
    
    /**
    * Rendition Mock class mimicking the behaviour http call
    */
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        public Integer Mode {get; set;}
        
        public HTTPResponse respond(HTTPRequest req) { 
            String url = req.getEndPoint();
            HttpResponse res = new HttpResponse();
            
            System.debug('Request Endpoint : ' + req.getEndpoint());
            System.debug('Request Body' + req.getBody());
            
            if(this.Mode == MODE_SUBMIT) {
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"access_token": "AFFZquARFkcZC","token_type": "bearer","expires_in": "3600","scope": "epc.api"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('workflows')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"jobID": "00000000005E8B5C","workflowID": "0000000005200C07"}');
                    res.setStatusCode(201);   
                }
                else if(url.contains('input')){
                    res.setStatusCode(200);
                }
                else if(url.contains('/v1/jobs/')){
                    res.setStatusCode(200);
                }
                else if(url.endsWith('/v1/jobs')){
                    res.setBody('{"jobID": "00000000005E8B5C","workflowID": "0000000000000000"}');
                    res.setStatusCode(201);
                }
                else if(url.contains('CQ_LargeRenditionServer')){
                    res.setStatusCode(200);
                }
            }
            else if(this.Mode == MODE_SUBMIT_ERROR){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"error": "invalid_client"}');
                    res.setStatusCode(400); 
                }
                else if(url.contains('workflows')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"error":"invalid_token"}');
                    res.setStatusCode(401);   
                }
                else if(url.contains('input')){
                    res.setStatusCode(404);
                }
                else if(url.contains('/v1/jobs')){
                    res.setStatusCode(404);
                }
            }
            else if(this.Mode == MODE_FETCH){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"access_token": "AFFZquARFkcZC","token_type": "bearer","expires_in": "3600","scope": "epc.api"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('metadata')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"bytes" : "500"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('output')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"status": "OK"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('CQ_LargeRenditionServer')) {
                    res.setStatusCode(200);
                }
            }
            return res;
        }  
    }
    
}