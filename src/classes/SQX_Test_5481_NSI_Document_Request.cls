/**
 * test class to test document request task type for nsi
 */
@isTest
public class SQX_Test_5481_NSI_Document_Request {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        User controlledDocCreator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'controlledDocCreator');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser }); 
        System.runAs(controlledDocCreator){
            
            // ARRANGE : Given a NSI with document request onboarding step
            SQX_Test_NSI.createPolicyTasks(1, SQX_NSI.TASK_TYPE_DOC_REQUEST, assigneeUser, 1);
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            doc1.save(true).synchronize();
            
            ContentDocumentLink cdl = new ContentDocumentLink(LinkedEntityID = doc1.doc.Id, ContentDocumentId = doc1.doc.Valid_Content_Reference__c, ShareType = 'V');
            insert cdl;
            
            SQX_Task__c cqTask = [SELECT Id FROM SQX_Task__c LIMIT 1];
            cqTask.SQX_Controlled_Document__c = doc1.doc.Id;
            update cqTask;
        }

        System.runAs(standardUser){
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();
            
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id].size());
        }
    }
    
    /**
     * GIVEN : A NSI with document request policy task
     * WHEN : NSI is initiated
     * THEN : new content document is created and linked to sf task of the onboarding step
     * @story : [SQX-5481]
     * @date : 2018/05/15 
     */
    public static testMethod void givenNSIAndPolicyTasks_WhenNsiIsSubmitted_ThenPolicyTaskIsCopiedToWorkflowPolicy(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser){

            SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.Activity_Code__c = 'initiate';

            // ACT : NSI is initiated
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});           
            
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.Id];
            
            // ASSERT :new content document is created and linked to sf task of the onboarding step
            System.assertEquals(1, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: sfTask.Id].size());
        }
    }
    
    /**
     * GIVEN : A NSI with document request policy task
     * WHEN : NSI is initiated for obsolete controlled doc
     * THEN : Error is thrown
     * @story : [SQX-5481]
     * @date : 2018/05/15 
     */
    public static testMethod void givenNSIAndPolicyTasks_WhenObsoleteControlledDocIsReferred_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){

            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            doc1.save().synchronize();

            SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];

            SQX_Onboarding_Step__c customStep =  new SQX_Onboarding_Step__c(SQX_Parent__c= nsi.id,
                                                                            Name='CustomStep',
                                                                            Step__c=1,
                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                            Due_Date__c=Date.today(),
                                                                            SQX_Controlled_Document__c=doc1.doc.id,
                                                                            RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, 'Document_Request'),
                                                                            Document_Name__c = 'Sample Document Name'
                                                                            );
            insert customStep;

            doc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE);
            doc1.save();

            // ACT : NSI is initiated with one of the steps linked with obsolete controlled document

            SQX_New_Supplier_Introduction__c currentNSI = new SQX_New_Supplier_Introduction__c(Id = nsi.Id,
                                                                                               Activity_Code__c = 'initiate');

            Test.startTest();
            Database.SaveResult result = Database.update(currentNSI, false);
            Test.stopTest();

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Document cannot be obsoleted');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_Step_Refer_Doc_Status_Other_Than_Pre_Release_And_Current.replace('{StepName}', customStep.Name)), result.getErrors());
        }
    }
    
    /**
     * GIVEN : A NSI with document request policy task
     * WHEN : Content Document is deleted
     * THEN : Error is thrown
     * @story : [SQX-5481]
     * @date : 2018/05/15 
     */
    public static testMethod void givenNSIAndPolicyTasks_WhenSFIsCreatedWithContentDocument_ContentDocumentCannotBeDeleted(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser){

            SQX_BulkifiedBase.clearAllProcessedEntities();
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.Activity_Code__c = 'initiate';

            
            // ACT : NSI is initiated
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.Id];
            
            List<ContentDocumentLink>  cdLinks = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: sfTask.Id];
            System.assertEquals(1, cdLinks.size());
            
            ContentDocument cqDocument = [SELECT Id FROM ContentDocument WHERE Id =: cdLinks.get(0).ContentDocumentId];
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // ACT  : Content Document is deleted
           	Database.DeleteResult result = Database.delete(cqDocument, false);
            
            // ASSERT : Error is thrown.
            System.assert(!result.isSuccess(), 'Content Document cannot be deleted');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_CQ_DOCUMENT_CANNOT_BE_DELETED));
        }
    }
    
    /**
     * GIVEN : A NSI with document request policy task
     * WHEN : NSI is initiated
     * THEN : new content document is created and linked to sf task of the onboarding step
     * 
     * WHEN : SF Task is completed
     * THEN : All details of salesforce task are copied to onboarding step
     * 
     * @story : [SQX-5481]
     * @date : 2018/05/15 
     */
    public static testMethod void givenNSIAndPolicyTasks_WhenSFTasksAreCompleted_ThenContentDocuemntIsLinkedWithOnBoardingStepAndNSI(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_New_Supplier_Introduction__c nsi = new SQX_New_Supplier_Introduction__c();
        System.runAs(standardUser){

            nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.Activity_Code__c = 'initiate';
            
            // ACT : NSI is initiated
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.Id];
            
            // ASSERT :new content document is created and linked to sf task of the onboarding step
            System.assertEquals(1, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: sfTask.Id].size());
        }
        System.runAs(assigneeUser){
            List<SQX_Onboarding_Step__c> onboardingsSteps = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id];
            SQX_Onboarding_Step__c onboardingsStep = onboardingsSteps.get(0);
            
            // ACT: On boarding step is completed
            Task nsiTask = [SELECT Id FROM Task WHERE Status != :SQX_Task.STATUS_COMPLETED AND WhatId =: nsi.Id];
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(nsiTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST, Date.today(), Date.today().addDays(10));

            System.assertEquals(1, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: nsi.Id].size());
            System.assertEquals(1, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: onboardingsStep.Id].size());
            
            
            SQX_Onboarding_Step__c onboardingStep = [SELECT Id, Issue_Date__c, Expiration_Date__c FROM SQX_Onboarding_Step__c LIMIT 1];
            
            // ASSERT : All details are copied
            System.assertEquals(Date.today(), onboardingStep.Issue_Date__c);
            System.assertEquals(Date.today().addDays(10), onboardingStep.Expiration_Date__c);
            
        }   
    }
    
    /**
     * GIVEN : A NSI with document request policy task
     * WHEN : NSI is initiated
     * THEN : new content document is created and linked to sf task of the onboarding step and new version can be uploaded
     * @story : [SQX-5481]
     * @date : 2018/05/15 
     */
    public static testMethod void givenOpenOnboardingStepOfDocumentRequest_WhenCVIsUpdated_NewVersionMustBeUploaded(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
        System.runAs(standardUser){

            nsi.Activity_Code__c = 'initiate';
            
            // ACT : NSI is initiated
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, new List<Schema.SObjectField> {});
            System.assertEquals(1, [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
        }
        
        System.runAs(assigneeUser){
            
            Task sfTask = [SELECT Id FROM Task WHERE WhatId =: nsi.Id];
            
            // ASSERT :new content document is created and linked to sf task of the onboarding step
            System.assertEquals(1, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: sfTask.Id].size());
            
            // ASSERT : New Version can be uploaded
            ContentVersion cv = new ContentVersion();
            cv.VersionData = Blob.valueOf('SampleContent');
            cv.PathOnClient = 'Test.txt';
            cv.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: sfTask.Id].ContentDocumentId;
            
            insert cv;
        }
    }
    
    /**
     * GIVEN: A NSI with onboarding step 
     * WHEN: User try to add the doc request with different Controlled doc's document status
                    1) Draft status
                    2)Current status
                    3)Pre-Release status
                    4)Pre-Expire status
                    5)Obsolete status
     * THEN : User get error message  when controlled doc's document status other than pre-Release and current 
     * @story : [SQX-6159]
     * @date : 2018/08/02
     */
    @isTest
    public static void givenNSIAndPolicyTasks_WhenControlledDocOtherThanPreReleaseAndCurrentIsReferred_ThenErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){

            SQX_New_Supplier_Introduction__c nsi = [SELECT Id FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            String obRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, 'Document_Request');

            //Arrange: Controlled doc to draft status
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            SQX_Onboarding_Step__c policyforDraft=  new SQX_Onboarding_Step__c (SQX_Parent__c=nsi.id,
                                                                                     Step__c=1,
                                                                                     SQX_User__c=UserInfo.getUserId(),
                                                                                     Due_Date__c=Date.today(),
                                                                                     SQX_Controlled_Document__c=doc1.doc.id,
                                                                                     Document_Name__c = 'Sample Document Name',
                                                                                     RecordTypeId = obRecordTypeId
                                                                                    );
            //Act :Add onboarding step of Draft controlled doc
            List<Database.SaveResult> resultforDraft = new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c>{policyforDraft}, new List<Schema.SObjectField>{});
            //Assert:on-boarding step for controlled doc is save
            System.assert(!resultforDraft[0].isSuccess(),'Only Pre-Release and Current Controlled Documents can be referred.');
            System.assertEquals('Only Pre-Release and Current Controlled Documents can be referred.',resultforDraft[0].getErrors()[0].getMessage());

            //Arrange :update Control doc to Current status
            doc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc1.doc}, new List<Schema.SObjectField> {});
            SQX_Onboarding_Step__c policyforCurrent=  new SQX_Onboarding_Step__c (SQX_Parent__c=nsi.id,
                                                                                     Step__c=1,
                                                                                     SQX_User__c=UserInfo.getUserId(),
                                                                                     Due_Date__c=Date.today(),
                                                                                     SQX_Controlled_Document__c=doc1.doc.id,
                                                                                     Document_Name__c = 'Sample Document Name',
                                                                                     RecordTypeId = obRecordTypeId
                                                                                    );
            //Act :Try To insert the onboarding status of controlled doc's current status
            List<Database.SaveResult> resultforCurrent = new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c>{policyforCurrent}, new List<Schema.SObjectField>{});
            //Assert: Save the onboarding step
            System.assertEquals(true, resultforCurrent[0].isSuccess(),'Record save Successfully>>' + resultforCurrent[0].getErrors());

            //Arrange:Update doc to pre-release
            doc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc1.doc}, new List<Schema.SObjectField> {});
            SQX_Onboarding_Step__c policyforPreRelease=  new SQX_Onboarding_Step__c ( SQX_Parent__c=nsi.id,
                                                                                      Step__c=1,
                                                                                      SQX_User__c=UserInfo.getUserId(),
                                                                                      Due_Date__c=Date.today(),
                                                                                      SQX_Controlled_Document__c=doc1.doc.id,
                                                                                      Document_Name__c = 'Sample Document Name',
                                                                                      RecordTypeId = obRecordTypeId
                                                                                    );
            //Act :Add onboarding step of pre-Release controlled doc
            List<Database.SaveResult> resultforPreRelease = new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c>{policyforPreRelease}, new List<Schema.SObjectField>{});
            //Assert:on-boarding step for controlled doc is save
            System.assertEquals(true, resultforPreRelease[0].isSuccess(),'Record save Successfully>>' );

            //Arraneg:update controlled doc to pre expire
            doc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            doc1.doc.Expiration_Date__c  = Date.Today()+1;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc1.doc}, new List<Schema.SObjectField> {});
            SQX_Onboarding_Step__c policyforPreExpire=  new SQX_Onboarding_Step__c ( SQX_Parent__c=nsi.id,
                                                                                     Step__c=1,
                                                                                     SQX_User__c=UserInfo.getUserId(),
                                                                                     Due_Date__c=Date.today(),
                                                                                     SQX_Controlled_Document__c=doc1.doc.id,
                                                                                     Document_Name__c = 'Sample Document Name',
                                                                                     RecordTypeId = obRecordTypeId
                                                                                    );
            //Act :Add onboarding step of pre-Expire controlled doc
            List<Database.SaveResult> resultforPreExpire = new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c>{policyforPreExpire}, new List<Schema.SObjectField>{});
            //Assert: Verify the Step Number Validation Rule Error message
            System.assert(!resultforPreExpire[0].isSuccess(),'Only Pre-Release and Current Controlled Documents can be referred.');
            System.assertEquals('Only Pre-Release and Current Controlled Documents can be referred.',resultforPreExpire[0].getErrors()[0].getMessage());

            //Arrange:update controlled doc to Obsolete
            doc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            doc1.doc.Expiration_Date__c  = Date.Today()+1;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc1.doc}, new List<Schema.SObjectField> {});
            SQX_Onboarding_Step__c policyforObsolete=  new SQX_Onboarding_Step__c ( SQX_Parent__c=nsi.id,
                                                                                    Step__c=1,
                                                                                    SQX_User__c=UserInfo.getUserId(),
                                                                                    Due_Date__c=Date.today(),
                                                                                    SQX_Controlled_Document__c=doc1.doc.id,
                                                                                    Document_Name__c = 'Sample Document Name',
                                                                                    RecordTypeId = obRecordTypeId
                                                                                   );
            //Act :Add onboarding step of Obsolete controlled doc
            List<Database.SaveResult> resultforObsolete= new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c>{policyforObsolete}, new List<Schema.SObjectField>{});
            //Assert: Verify the Step Number Validation Rule Error message
            System.assert(!resultforObsolete[0].isSuccess(),'Only Pre-Release and Current Controlled Documents can be referred.');
            System.assertEquals('Only Pre-Release and Current Controlled Documents can be referred.',resultforObsolete[0].getErrors()[0].getMessage());
        }
    }

    /**
     * GIVEN : A NSI with document request policy task added controlled doc 
     * WHEN : NSI is initiated and complete the task and redor the onboarding policy
     * THEN : Redo onboarding step  should contain controll doc
     * @story : [SQX-6237]
     * @date : 2018/08/11
     */
     @isTest
    public static void givenOnboardingPolicyWithControllDoc_WhenUserRedoPolicy_ThenControlledDocTransferToRedoTask(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User assigneeUser=allUsers.get('assigneeUser');
        SQX_Test_NSI nsi = new SQX_Test_NSI();
        List<SQX_Onboarding_Step__c> wfPolicy=new List<SQX_Onboarding_Step__c>();
        System.runAs(adminUser){
            Test.startTest();
            // ARRANGE : NSI is created 
            nsi = new SQX_Test_NSI().save();
            //Submitting the record
            nsi.submit();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act: Initiating the record
            nsi.initiate();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Assert: controlled doc onboarding  step
            System.assertEquals(1,[SELECT Id, SQX_Parent__c,SQX_Controlled_Document__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id And SQX_Controlled_Document__c!=null].size());
            Test.stopTest();
        }
        System.runAs(assigneeUser){

            wfPolicy = [SELECT Id, SQX_Parent__c,SQX_Controlled_Document__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id And Status__c=:SQX_Steps_Trigger_Handler.STATUS_OPEN And SQX_Controlled_Document__c!=null];
            System.assertEquals(1, wfPolicy.size());
            List<Task> sfTasks = new List<Task>{[SELECT Id FROM Task WHERE WhatId =: nsi.nsi.Id AND Child_What_Id__c =: wfPolicy[0].Id]};

            System.assertEquals(1, sfTasks.size());
            //sf task complete 
            Task sfTask = sfTasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
            
        }
        System.runAs(adminUser){
            //onboarding step  redo 
            nsi.redoOnboardingStep(wfPolicy[0]);
            
            List<SQX_Onboarding_Step__c> redoWfPolicy = [SELECT Id, SQX_Parent__c,SQX_Controlled_Document__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id  And SQX_Controlled_Document__c !=null ];
            System.assertEquals(2, redoWfPolicy.size());
        }
    }

    /**
     * GIVEN : A NSI with document request policy task with document name
     * WHEN : NSI is initiated
     * THEN : document name is transfer to onboarding step  when nsi is submit 
     * @story : [SQX-6312]
     * @date : 2018/08/22
     */
    @isTest
    public static void givenPolicyTaskWithDocumentName_WhenNsiCreateAndsubmit_ThenPolicyTaskDocumentNameTransferToOnboardingStep(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            //Arrange:Create nsi policy task 
            SQX_Test_NSI.createPolicyTasks(1, SQX_NSI.TASK_TYPE_DOC_REQUEST, adminUser, 2);
            //Update policy task with doc  name
            SQX_BulkifiedBase.clearAllProcessedEntities();
            List<SQX_Task__c> tasks1 = [SELECT Id, Name,Document_Name__c, Task_Type__c FROM SQX_Task__c WHERE Record_Type__c =: SQX_Task.RTYPE_SUPPLIER_INTRODUCTION and step__c=2 ]; 
            tasks1[0].Document_Name__c='Sample Document Name';
            update tasks1;
            //create nsi record
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();

            nsi.submit();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            nsi.initiate();

            //Act:Retrive onbaording step with draft and related policy task 
            List<SQX_Onboarding_Step__c> onboardingList=[SELECT Id,Document_Name__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_DRAFT ];
            List<SQX_Task__c> tasks = [SELECT Id, Name,Document_Name__c, Task_Type__c FROM SQX_Task__c WHERE Record_Type__c =: SQX_Task.RTYPE_SUPPLIER_INTRODUCTION and Step__c=:2]; 
            //Assert:Verify document request transfer from policy task to onboarding step
            System.assertEquals(1, onboardingList.size());
            System.assertEquals(tasks[0].Document_Name__c, onboardingList[0].Document_Name__c);
        }
    }

    @isTest
    public static void givenOnboardingStepOfTypeDocRequest_WhenRecordIsSavedWithoutDocName_ThenThrowError(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            SQX_Onboarding_Step__c onboardRecord = [Select id from SQX_Onboarding_Step__c];
            onboardRecord.document_name__c = null;
            Database.SaveResult result = Database.update(onboardRecord, false) ;
            System.assert(!result.isSuccess(), 'Expected onboardRecord not to be updated with null document name');
            System.assertEquals('Document Name is required for Document Request record type.', result.getErrors()[0].getMessage());
        }
    }
}