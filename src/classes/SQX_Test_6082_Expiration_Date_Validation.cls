/**
* unit test class for expiration date should not be less than Issue Date
*/
@isTest
public class SQX_Test_6082_Expiration_Date_Validation {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            //Create workflow policy(on-boarding step)
            SQX_Onboarding_Step__c wp = new SQX_Onboarding_Step__c();
            wp.SQX_Parent__c =nsi.nsi.Id;
            wp.Name = 'TestName';
            wp.Status__c = SQX_NSI.STATUS_DRAFT;
            wp.Step__c =1;
            wp.Due_Date__c = Date.today();
            wp.SQX_User__c = adminUser.Id;
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { wp },  new List<Schema.SObjectField>{});
            
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Onboarding_Step__c wp1 = new SQX_Onboarding_Step__c();
            wp1.SQX_Parent__c =nsi.nsi.Id;
            wp1.Name = 'TestName1';
            wp1.Status__c = SQX_NSI.STATUS_DRAFT;
            wp1.Step__c =1;
            wp1.Due_Date__c = Date.today();
            wp1.SQX_User__c = adminUser.Id;
            wp1.Issue_Date__c = Date.today();
            wp1.Expiration_Date__c = Date.today()+1;
            wp1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            wp1.compliancequest__Document_Name__c  = 'Sample Document Name';
            wp1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnBoardingStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{});
        }
    }
    
    /**
    * GIVEN : given NSI Task
    * WHEN : when save the record with expiration date less than issue date
    * THEN : throw validation error message
    */  
    public static testMethod void givenTask_WhenSaveRecordWithExpirationDateLessThanIssueDate_ThenThrowValidationErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: retrive workflow policy
            SQX_Onboarding_Step__c wp1 = [SELECT Id, Expiration_Date__c, Issue_Date__c FROM SQX_Onboarding_Step__c LIMIT 1];
            wp1.Issue_Date__c = Date.today().addDays(-1);
            wp1.Expiration_Date__c = Date.today().addDays(-10);
            
            //Act: save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{ SQX_Onboarding_Step__c.Expiration_Date__c });
            
            //Assert: Ensured that throws validation error message
            System.assert(result[0].isSuccess() == false, 'Expiration date grater than issue date.');
            System.assertEquals('Issue Date cannot be greater than Expiration Date.', result[0].getErrors().get(0).getMessage());
        }
    }
    
    /**
    * GIVEN : given NSI Task
    * WHEN : when save the record with issue date greater than current date
    * THEN : throw validation error message
    */  
    public static testMethod void givenTask_WhenSaveRecordWithIssueDateGreaterThanCurrentDate_ThenThrowValidationErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: retrive workflow policy
            SQX_Onboarding_Step__c wp1 = [SELECT Id, Expiration_Date__c, Issue_Date__c FROM SQX_Onboarding_Step__c LIMIT 1];
            wp1.Issue_Date__c = Date.today()+2;
            
            //Act: save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{ SQX_Onboarding_Step__c.Expiration_Date__c });
            
            //Assert: Ensured that throws validation error message
            System.assert(result[0].isSuccess() == false, 'Expiration date grater than issue date.');
            System.assertEquals('Issue Date cannot be greater than Current Date.', result[0].getErrors().get(0).getMessage());
        }
    }

    /**
    * GIVEN : given NSI Task
    * WHEN : when save the record with expiration date greater than or equal to issue date
    * THEN : save record without validation error
    */  
    public static testMethod void givenTask_WhenSaveRecordWithExpirationDateGraterThanOrEqualToIssueDate_ThenSaveTheRecordWithoutErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: retrive workflow policy
            SQX_Onboarding_Step__c wp1 = [SELECT Id, Expiration_Date__c, Issue_Date__c FROM SQX_Onboarding_Step__c LIMIT 1];

            // expiration date equal to issue date
            wp1.Issue_Date__c = Date.today();
            wp1.Expiration_Date__c = Date.today();
            
            //Act: save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{ SQX_Onboarding_Step__c.Expiration_Date__c });
            
            //Assert: Ensured that save record without validation error
            System.assert(result[0].isSuccess() == true, 'Save successfully.');
            
            // expiration date grater than issue date
            wp1.Issue_Date__c = Date.today();
            wp1.Expiration_Date__c = Date.today()+5;
            
            //Act: save the record
            List<Database.SaveResult> result1 = new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{ SQX_Onboarding_Step__c.Expiration_Date__c });
            
            //Assert: Ensured that save record without validation error
            System.assert(result1[0].isSuccess() == true, 'Save successfully.');
        }
    }
}