/**
* Unit tests for closing and voiding calibration records
* @story SQX-1463
*/
@IsTest
public class SQX_Test_1463_Calibration_Edit {
    static Boolean runAllTests = true,
                   run_givenCloseCalibration_CalibrationClosed = false,
                   run_givenCloseCalibration_DefaultNewEquipmentStatusIsSet = false,
                   run_givenCloseVoidCalibration_DefaultNewEquipmentStatusIsNotSet = false,
                   run_givenCloseCalibration_FutureCalibrationDate_NotSavedAndStatusNotChanged = false,
                   run_givenCloseCalibration_StatusClose_CalibrationNotClosed = false,
                   run_givenVoidCalibration_CalibrationVoided = false;
    
    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; // Esignature is not validating password when consumer key is wrong.
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            if(Invalid_Request){
                response.setStatusCode(400);
                response.setBody('Error: 400');
            }
            else{
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();
                if(url.contains('rightPassword')){
                    response.setBody('{"id":"allcorrect"}');
                }
                else{
                    response.setBody('{"error":"incorrect password"}');
                }
            }
            
            return response;
        }
    }
    
    private static Boolean checkPageMessages(String expectedMsg) {
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        for (ApexPages.Message msg : msgs) {
            if (msg.getDetail().equals(expectedMsg)){
                return true;
            }
        }
        return false;
    }
    
    /**
    * Unit tests for closing calibration
    */
    public testmethod static void givenCloseCalibration_CalibrationClosed() {
        if (!runAllTests && !run_givenCloseCalibration_CalibrationClosed) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-123',
                Equipment_Description__c = 'EQP 123 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );
            Database.insert(eqp1, false);
            
            // add required calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN
            );
            Database.insert(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            
            // change calibration record and signing off values
            cal1.New_Equipment_Status__c = SQX_Calibration.NEW_EQUIPMENT_STATUS_ACTIVE;
            cal1.Calibration_Date__c = Date.today().addDays(-1);
            cal1.New_Calibration_Interval__c = 10;
            cal1.Next_Calibration_Date__c = cal1.Calibration_Date__c.addDays(10);
            extension.SigningOffUsername = standardUser.username;
            extension.SigningOffPassword = 'rightPassword';
            extension.RecordActivityComment = 'Closing calibration record.';
            
            Test.setMock(HttpCalloutMock.class, new MockAuthClass()); //instruct the Apex runtime to send this fake response
            
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'ConsumerKey';
            cqES.Consumer_Secret__c = 'Secret';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            // ACT: call save action to close
            extension.save();
            
            // get updated calibration record
            SQX_Calibration__c updatedCal1 = [SELECT Id, Calibration_Status__c, Sign_Off_By__c, Sign_Off_Date__c, Sign_Off_Comment__c
                                              FROM SQX_Calibration__c
                                              WHERE Id =: cal1.Id];
            
            System.assertEquals(SQX_Calibration.STATUS_CLOSE, updatedCal1.Calibration_Status__c,
                'Expected closed calibration record status to be "' + SQX_Calibration.STATUS_CLOSE + '".');
            
            System.assert(updatedCal1.Sign_Off_By__c != null, 'Expected signed-off by value for closed calibration record to be set.');
            
            System.assert(updatedCal1.Sign_Off_Date__c != null, 'Expected signed-off date value for closed calibration record to be set.');
            
            System.assertEquals(extension.RecordActivityComment, updatedCal1.Sign_Off_Comment__c,
                'Expected signed-off comment value for closed calibration record to be set.');
            
            // get related equipment
            SQX_Equipment__c updatedEqp1 = [SELECT Id, Equipment_Status__c, Last_Calibration_Date__c, Calibration_Interval__c, Next_Calibration_Date__c
                                            FROM SQX_Equipment__c
                                            WHERE Id =: eqp1.Id];
            
            System.assertEquals(SQX_Equipment.STATUS_ACTIVE, updatedEqp1.Equipment_Status__c,
                'Expected status of the related equipment record to be "' + SQX_Equipment.STATUS_ACTIVE + '".');
            
            System.assertEquals(cal1.Calibration_Date__c, updatedEqp1.Last_Calibration_Date__c,
                'Expected last calibration date of related equipment record to be set as calibration date of closed calibration record.');
            
            System.assertEquals(cal1.New_Calibration_Interval__c, updatedEqp1.Calibration_Interval__c,
                'Expected calibration interval of related equipment record to be set from closed calibration record.');
            
            System.assertEquals(cal1.Next_Calibration_Date__c, updatedEqp1.Next_Calibration_Date__c,
                'Expected next calibration date of related equipment record to be set from closed calibration record.');
            
            //select equipment record activity
            SQX_Equipment_Record_Activity__c[] createdEra = [SELECT Id, Comment__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :updatedEqp1.Id];
            
            System.assertEquals(1, createdEra.size(), 'Expected equipment record activity to be created.');
            
            String expectedActivity = SQX_Equipment__c.Equipment_Status__c.getDescribe().getLabel() + ' changed from ' + SQX_Equipment.STATUS_NEW + ' to ' + SQX_Equipment.STATUS_ACTIVE;
            System.assert(createdEra[0].Comment__c.contains(expectedActivity), 'Expected comment of equipment record activity created to contain status change information.');
            
            expectedActivity = SQX_Equipment__c.Last_Calibration_Date__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedActivity), 'Expected comment of record Activity created to contain Last Calibration Date change information.');
            
            expectedActivity = SQX_Equipment__c.Calibration_Interval__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedActivity), 'Expected comment of record Activity created to contain Calibration Interval change information.');
            
            expectedActivity = SQX_Equipment__c.Next_Calibration_Date__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedActivity), 'Expected comment of record Activity created to contain Next Calibration Date change information.');
            
            System.assert(createdEra[0].Comment__c.endsWith(extension.RecordActivityComment), 'Expected comment of record Activity created to end with user comment.');
            
            
            // update closed record
            updatedCal1.Calibration_Date__c = Date.today();
            
            // ACT: update signed off record
            Database.SaveResult sr = Database.update(updatedCal1, false);
            
            System.assert(sr.isSuccess() == false, 'Expected modification of signed off calibration record not to be saved.');
        }
    }
    
    /**
    * tests default new equipment status to be set as per final calibration result when closing calibration
    */
    public testmethod static void givenCloseCalibration_DefaultNewEquipmentStatusIsSet() {
        if (!runAllTests && !run_givenCloseCalibration_DefaultNewEquipmentStatusIsSet) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'Eqpforclosedcal',
                Equipment_Description__c = 'Eqpforclosedcal desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, false);
            
            // add required open calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN
            );
            Database.insert(cal1, false);
            
            // set final calibration result to "NA"
            cal1.Final_Calibration_Result__c = SQX_Calibration.CALIBRATION_RESULT_NA;
            Database.update(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            
            String expectedValue = SQX_Calibration.getDetaultNewEquipmentStatus(cal1);
            System.assertEquals(expectedValue, cal1.New_Equipment_Status__c,
                'Expected default new equipment status to be "' + expectedValue + '".');
            
            
            // set final calibration result to "Within Tolerance"
            cal1.Final_Calibration_Result__c = SQX_Calibration.CALIBRATION_RESULT_WITHIN_TOLERANCE;
            Database.update(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            controller = new ApexPages.StandardController(cal1);
            extension = new SQX_Extension_Calibration(controller);
            
            expectedValue = SQX_Calibration.getDetaultNewEquipmentStatus(cal1);
            System.assertEquals(expectedValue, cal1.New_Equipment_Status__c,
                'Expected default new equipment status to be "' + expectedValue + '".');
            
            
            // set final calibration result to "Out of Tolerance"
            cal1.Final_Calibration_Result__c = SQX_Calibration.CALIBRATION_RESULT_OUT_OF_TOLERANCE;
            Database.update(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            controller = new ApexPages.StandardController(cal1);
            extension = new SQX_Extension_Calibration(controller);
            
            expectedValue = SQX_Calibration.getDetaultNewEquipmentStatus(cal1);
            System.assertEquals(expectedValue, cal1.New_Equipment_Status__c,
                'Expected default new equipment status to be "' + expectedValue + '".');
        }
    }
    
    /**
    * tests default new equipment status not to be set as per final calibration result when voiding calibration or closing not open calibration
    */
    public testmethod static void givenCloseVoidCalibration_DefaultNewEquipmentStatusIsNotSet() {
        if (!runAllTests && !run_givenCloseVoidCalibration_DefaultNewEquipmentStatusIsNotSet) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'Eqpforclosedcal',
                Equipment_Description__c = 'Eqpforclosedcal desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, false);
            
            // add required open calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN,
                Final_Calibration_Result__c = SQX_Calibration.CALIBRATION_RESULT_WITHIN_TOLERANCE
            );
            Database.insert(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for voiding calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'void');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            
            // get default new equipment status for calibration record
            String defaultStatus = SQX_Calibration.getDetaultNewEquipmentStatus(cal1);
            
            System.assertNotEquals(defaultStatus, cal1.New_Equipment_Status__c,
                'Expected default new equipment status not to be set when voiding calibration.');
            
            // voiding calibration
            extension.save();
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            controller = new ApexPages.StandardController(cal1);
            extension = new SQX_Extension_Calibration(controller);
            
            System.assertNotEquals(defaultStatus, cal1.New_Equipment_Status__c,
                'Expected default new equipment status not to be set when closing calibration that is not open.');
        }
    }
    
    /**
    * Unit tests future calibration date error while closing calibration
    */
    public testmethod static void givenCloseCalibration_FutureCalibrationDate_NotSavedAndStatusNotChanged() {
        if (!runAllTests && !run_givenCloseCalibration_FutureCalibrationDate_NotSavedAndStatusNotChanged) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'Eqpforclosedcal',
                Equipment_Description__c = 'Eqpforclosedcal desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );
            Database.insert(eqp1, false);
            
            // add required closed calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN
            );
            Database.insert(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            extension.ES_Enabled = false;
            extension.ES_Ask_Username = false;
            
            // set future calibration date, other required values and signing off values
            cal1.New_Equipment_Status__c = SQX_Calibration.NEW_EQUIPMENT_STATUS_ACTIVE;
            cal1.Calibration_Date__c = Date.today().addDays(1);
            cal1.New_Calibration_Interval__c = 10;
            cal1.Next_Calibration_Date__c = cal1.Calibration_Date__c.addDays(10);
            extension.SigningOffUsername = standardUser.username;
            extension.SigningOffPassword = 'rightPassword';
            extension.RecordActivityComment = 'Closing calibration record.';
            
            // ACT: call save action to close
            extension.save();
            
            // check for error message
            System.assert(checkPageMessages('Calibration Date cannot be greater than today\'s date.'),
                'Expected error message not found for future calibration date when closing a calibration record.');
            
            System.assertEquals(SQX_Calibration.STATUS_OPEN, cal1.Calibration_Status__c,
                'Expected status of calibration record to be open when error occurred while closing.');
            
            System.assertEquals(null, cal1.Sign_Off_By__c,
                'Expected sign-off field to be null when error occurred while closing.');
        }
    }
    
    /**
    * Unit tests for closing calibration with void status
    */
    public testmethod static void givenCloseCalibration_StatusClose_CalibrationNotClosed() {
        if (!runAllTests && !run_givenCloseCalibration_StatusClose_CalibrationNotClosed) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'Eqpforclosedcal',
                Equipment_Description__c = 'Eqpforclosedcal desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );
            Database.insert(eqp1, false);
            
            // add required closed calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_CLOSE,
                New_Equipment_Status__c = SQX_Calibration.NEW_EQUIPMENT_STATUS_ACTIVE,
                New_Calibration_Interval__c = 10,
                Next_Calibration_Date__c = Date.today(),
                Sign_Off_By__c = 'test signed off value',
                Sign_Off_Date__c = System.now(),
                Sign_Off_Comment__c = 'test comment'
            );
            Database.insert(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for closing calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'close');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            extension.ES_Enabled = false;
            extension.ES_Ask_Username = false;
            
            // signing off values
            extension.SigningOffUsername = standardUser.username;
            extension.SigningOffPassword = 'rightPassword';
            extension.RecordActivityComment = 'Closing calibration record.';
            
            // ACT: call save action to close
            extension.save();
            
            // check for error message
            System.assert(checkPageMessages(Label.SQX_ERR_MSG_Calibration_Record_Already_Close_Cannot_Close_Again),
                'Expected error message not found for future calibration date when closing a calibration record.');
        }
    }
    
    /**
    * Unit tests for voiding calibration
    * @story 1611
    */
    public testmethod static void givenVoidCalibration_CalibrationVoided() {
        if (!runAllTests && !run_givenVoidCalibration_CalibrationVoided) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EqpForVoiding',
                Equipment_Description__c = 'EqpForVoiding desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );
            Database.insert(eqp1, false);
            
            // add required calibration
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN
            );
            Database.insert(cal1, false);
            
            // ACT: open SQX_Calibration_Edit page for voiding calibration
            Test.setCurrentPage(Page.SQX_Calibration_Edit);
            ApexPages.currentPage().getParameters().put('mode', 'void');
            ApexPages.StandardController controller = new ApexPages.StandardController(cal1);
            SQX_Extension_Calibration extension = new SQX_Extension_Calibration(controller);
            extension.ES_Enabled = false;
            extension.ES_Ask_Username = false;
            
            // signing off values
            extension.SigningOffUsername = standardUser.username;
            extension.SigningOffPassword = 'rightPassword';
            extension.RecordActivityComment = 'Voiding calibration record.';
            
            // ACT: call save action to void
            extension.save();
            
            // get updated calibration record
            SQX_Calibration__c updatedCal1 = [SELECT Id, Calibration_Status__c, Sign_Off_By__c, Sign_Off_Date__c, Sign_Off_Comment__c
                                              FROM SQX_Calibration__c
                                              WHERE Id =: cal1.Id];
            
            System.assertEquals(SQX_Calibration.STATUS_VOID, updatedCal1.Calibration_Status__c,
                'Expected voided calibration record status to be "' + SQX_Calibration.STATUS_VOID + '".');
            
            System.assert(updatedCal1.Sign_Off_By__c != null, 'Expected signed-off by value for voided calibration record to be set.');
            
            System.assert(updatedCal1.Sign_Off_Date__c != null, 'Expected signed-off date value for voided calibration record to be set.');
            
            System.assertEquals(extension.RecordActivityComment, updatedCal1.Sign_Off_Comment__c,
                'Expected signed-off comment value for voided calibration record to be set.');
            
            
            // update voided record
            updatedCal1.Performed_By__c = 'changed';
            
            // ACT: update signed off record
            Database.SaveResult sr = Database.update(updatedCal1, false);
            
            System.assert(sr.isSuccess() == false, 'Expected modification of signed off calibration record not to be saved.');
        }
    }
}