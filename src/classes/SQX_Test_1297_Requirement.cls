/**
* Unit tests for Requirement Object
*
* @author Shailesh Maharjan
* @date 2015/07/03
* 
*/
@IsTest
public class SQX_Test_1297_Requirement {
    
    static Boolean runAllTests = true,
                   run_givenAddRequirement = false,
                   run_givenDeleteRequirement = false,
                   run_givenEditRequirement = false,
                   run_givenMakeControlledDocumentObsolete = false,
                   run_givenCopyRequirementOfRevisedControlledDoc = false,
                   run_givenReleaseDocumentWithRequirementsByApprover= false;
    
    static final String STD_USER_1 = '1297_STD_USER_1',
                        STD_USER_2 = '1297_STD_USER_2';
    
    @testSetup
    static void commonSetup() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_1);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_2);
    }
    
    
    /**
    * This test ensures addition of new / duplicate Requirement to work as expected.
    * REQ is duplicate when another inactive and not deactivated REQ exists with same Controlled Document,
    *   and can have many deactivated REQs with same Controlled Document.
    */
    public testmethod static void givenAddRequirement(){
        if (!runAllTests && !run_givenAddRequirement){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STD_USER_1);
        
        //get required test user
        User testUser1 = allUsers.get(STD_USER_2);
        
        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = Date.Today();
            
             // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            jf = new SQX_Test_Job_Function(jf2);
            Database.SaveResult jf2Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            //Act
            Database.SaveResult result1 = Database.insert(Req1, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected level of competency value for Requirement.');
            
            //Arrange
            Req1.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            //Act
            result1 = Database.insert(Req1, false);
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for draft document.');
            
            //Act: released test controlled doc.
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            //Assert test req1 activation
            // fetch activation date and Active status
            Req1 = [SELECT Id, Active__c, Activation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];

            System.assert(Req1.Active__c == true,
                'Expected Active to be checked when releasing Controlled Doc.');

            System.assert(Req1.Activation_Date__c != null,
                'Expected activation date to be set when releasing Controlled Doc.');
            
            //Arrange
            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c= SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            result1 = Database.insert(Req2, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for Current document.');
            //Arrange
            SQX_Requirement__c Req3 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c= SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            //Act
            result1 = Database.insert(Req3, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected duplicate Requirement not to be saved.');
            
            //Act: Activating Req2
            Req2.Active__c = true;
            result1 = Database.update(Req2, false);
            
            result1 = Database.insert(Req3, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected duplicate Requirement not to be saved.');
            
            //Act: Deactivating Req2
            Req2.Active__c = false;
            result1 = Database.update(Req2, false);
            
            result1 = Database.insert(Req3, false);
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Requirement with existing deactivated Requirement to be saved.');

           //Arrange; released test controlled doc.
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();

            //Act: test req1 activation
            // fetch activation date and Active status
            Req1 = [SELECT Id, Active__c, Deactivation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];
            //Assert
            System.assert(Req1.Active__c == false,
                'Expected Active to be unchecked when retiring Controlled Doc.');

            System.assert(Req1.Deactivation_Date__c != null,
                'Expected Deactivation date to be set when retiring Controlled Doc.');
            
            //Arrange: Adding Requirements
             SQX_Requirement__c Req4 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c= SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            //Act
            result1 = Database.insert(Req4, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Requirement not to be saved for obsolete document.');
            
        }
    }

    /**
    * This test ensures deletion of a Requirement to work as expected.
    * Once activated REQ cannot be deleted, otherwise it can be deleted.
    */
    public testmethod static void givenDeleteRequirement(){
        if (!runAllTests && !run_givenDeleteRequirement){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STD_USER_1);

        System.runas(standardUser) {
            //Arrange: creating required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // creating required Requirement
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            
            //Act
            Database.SaveResult saveResult1 = Database.insert(Req1, false);
            
            Database.DeleteResult delResult1 = Database.delete(Req1, false);
            
            //Assert: ensuring not activated REQ to be deleted
            System.assert(delResult1.isSuccess() == true,
                'Expected not activated Requirement to be deleted.');
            
            //Arrange: 
           SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            //Act
            Database.SaveResult saveResult2 = Database.insert(Req2, false);
            
            Database.DeleteResult delResult2 = Database.delete(Req1, false);
            
            //Assert; ensuring once activated REQ not to be deleted
            System.assert(delResult2.isSuccess() == false,
                'Expected once activated Requirement not to be deleted.');
        }
    }
    
    /**
    * This test ensures modification of fields of a Requirement to work as expected.
    * Cannot edit Requirement and/or user of a once activated REQ.
    * Cannot set active for a deactivated REQ.
    * SQX-4425: Ensures Training Program Step cannot have negative value
    */
    public testmethod static void givenEditRequirement(){
        if (!runAllTests && !run_givenEditRequirement){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STD_USER_1);
        
        System.runas(standardUser) {
            //Arrange: creating required controlled docs
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // creating required Requirement
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            jf = new SQX_Test_Job_Function(jf2);
            Database.SaveResult jf2Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = false,
                Active__c = false
            );
            Database.SaveResult r1 = Database.insert(Req1, false);
            
            // Arrange: SQX-4425: Set negative Training Program Step value
            Req1.Training_Program_Step__c = -1;
            
            // Act: SQX-4425: Set negative Training Program Step value
            r1 = new SQX_DB().continueOnError().op_update(new List<SQX_Requirement__c> { Req1 }, new List<Schema.SObjectField> { Schema.SQX_Requirement__c.Training_Program_Step__c }).get(0);
            
            // Assert: SQX-4425: Negative training program step value should not be saved with proper error message
            String expErrMsg = 'Training Program Step cannot be less than zero.';
            System.assert(r1.isSuccess() == false,
                'Expected saving requirement with negative Training Program Step value to fail');
            System.assert(SQX_Utilities.checkErrorMessage(r1.getErrors(), expErrMsg),
                'Expected error message "' + expErrMsg + '"; Errors: ' + r1.getErrors());
            
            Req1.Training_Program_Step__c = null; // Set null Training Program Step value
            Req1.SQX_Job_Function__c = jf1.Id;
            //Act
            Database.SaveResult r2 = Database.update(Req1, false);
            // Assert
            System.assert(r2.isSuccess() == true,
                'Expected modification of Requirement.');
                
            //Arrange: activate Req1
            Req1.Active__c = true;

            //Act
            Database.SaveResult r4 = Database.update(Req1, false);
            
            // fetch activation date
            Req1 = [SELECT Id, Activation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];

            // Assert
            System.assert(Req1.Activation_Date__c != null,
                'Expected activation date to be set when activating a Requirement.');
            
            //Arrange: change Job Function
            Req1.SQX_Job_Function__c = jf2.Id;
            Database.SaveResult r5 = Database.update(Req1, false);
            
            System.assert(r5.isSuccess() == false,
                'Expected modification of once activated Requirement not to be saved.');
            
            //Arrange: change Level Of Competency
            Req1 = new SQX_Requirement__c( Id = Req1.Id);
            Req1.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            r5 = Database.update(Req1, false);
            
            System.assert(r5.isSuccess() == false,
                'Expected modification of once activated Requirement not to be saved.');
            
            //Arrange: change Optional
            Req1 = new SQX_Requirement__c( Id = Req1.Id);
            Req1.Optional__c = true;
            r5 = Database.update(Req1, false);
            
            System.assert(r5.isSuccess() == false,
                'Expected modification of once activated Requirement not to be saved.');
            
            // Arrange: SQX-4425: change Training Program Step for activated REQ
            Req1 = new SQX_Requirement__c( Id = Req1.Id, Training_Program_Step__c = 10 );
            
            // Act: SQX-4425: save modified Training Program Step value for activated REQ
            r5 = Database.update(Req1, false);
            
            // Assert: SQX-4425: modified Training Program Step value should not be saved for activated REQ
            System.assert(r5.isSuccess() == false,
                'Expected modification of Training Program Step of once activated Requirement not to be saved.');
                
            //Arrange: deactivate Req1
            Req1 = new SQX_Requirement__c( Id = Req1.Id, Active__c = false);
            //Act
            Database.SaveResult r6 = Database.update(Req1, false);
            
            //Assert: fetch deactivation date
            Req1 = [SELECT Id, Deactivation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];

            System.assert(Req1.Deactivation_Date__c != null,
                'Expected deactivation date to be set when deactivating a Requirement.');
            
            //Arrange: reactivate Req1
            Req1.Active__c = true;
            //Act
            Database.SaveResult r7 = Database.update(Req1, false);
            //Assert
            System.assert(r7.isSuccess() == false,
                'Expected reactivation of deactivated Requirement not to be saved.');
        }
    }
    
    /**
    * This test ensures deactivation of requirements when controlled doc is obsolete.
    */
    public testmethod static void givenMakeControlledDocumentObsolete(){
        if (!runAllTests && !run_givenMakeControlledDocumentObsolete){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STD_USER_1);
        
        System.runas(standardUser) {
            //Arrange: Creating required controlled docs
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // creating required Requirement
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            Database.SaveResult r1 = Database.insert(Req1, false);

            //Act:Change the Status to Obsolete and Save
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            //Assert: fetch activation date
            Req1 = [SELECT Id, Active__c, Activation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];

            System.assert(Req1.Active__c == false,
                'Expected deactivation of requirement when controlled document is obsolete.');
        }
    }

    /**
    * This test ensures Copy of requirements when controlled doc is revised.
    */
    public testmethod static void givenCopyRequirementOfRevisedControlledDoc(){
        if (!runAllTests && !run_givenCopyRequirementOfRevisedControlledDoc){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STD_USER_1);
        
        System.runas(standardUser) {
            //Arrange: creating required controlled docs
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // creating required Requirement
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = true,
                Training_Program_Step__c = 1
            );
            Database.SaveResult r1 = Database.insert(Req1, false);

            System.assert(r1.isSuccess() == true,
                'Expected Requirement to be saved.');

            //Act: Revise from the parent document and Save. 
            SQX_Test_Controlled_Document cDoc2 = cDoc1.revise().save();

            List<SQX_Requirement__c> reqs = [SELECT Id, SQX_Job_Function__c, Level_of_Competency__c, Optional__c, Active__c, Training_Program_Step__c FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c = :cDoc2.doc.Id];
            
            //Assert : Requirement should be copied to newly revised document
            System.assert(reqs.size() == 1,
                'Expected requirement to be copied from parent document.' + reqs.size());

            System.assert(reqs[0].SQX_Job_Function__c == Req1.SQX_Job_Function__c,
                'Expected requirement to be copied with same job function from parent document.');

            System.assert(reqs[0].Level_of_Competency__c == Req1.Level_of_Competency__c,
                'Expected requirement to be copied with same level of competency value from parent document.');

            System.assert(reqs[0].Optional__c == Req1.Optional__c,
                'Expected requirement to be copied with same optional value from parent document.');

            System.assert(reqs[0].Active__c == false,
                'Expected requirement to be copied with active unchecked to draft document.');

            System.assert(reqs[0].Training_Program_Step__c == 1,
                'Expected requirement to be copied with training program step.');
        }
    }
    
    /**
    * This test ensure draft documents with requirements can be released by using approver.
    */
    public testmethod static void givenReleaseDocumentWithRequirementsByApprover(){
        if (!runAllTests && !run_givenReleaseDocumentWithRequirementsByApprover){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUserCreator = allUsers.get(STD_USER_1);
        User standardUserApprover = allUsers.get(STD_USER_2);
        
        SQX_Test_Controlled_Document cDoc1;
        SQX_Requirement__c Req1;
        
        System.runas(standardUserCreator) {
            // Arrange: creating required draft controlled doc
            cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = Date.Today();
            
             // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            jf = new SQX_Test_Job_Function(jf2);
            Database.SaveResult jf2Result  = jf.save();

            Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            //Act
            Database.SaveResult result1 = Database.insert(Req1, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected level of competency value for Requirement.');
            
            //Arrange
            Req1.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            //Act
            result1 = Database.insert(Req1, false);
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for draft document.');
        }
        
        System.runas(standardUserApprover) {

            //Act: released test controlled doc.
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            new SQX_DB().withoutSharing().op_update(new List<SQX_Controlled_Document__c> { cDoc1.doc }, new List<Schema.SObjectField> { Schema.SQX_Controlled_Document__c.Document_Status__c });

            //Assert test req1 activation
            // fetch activation date and Active status
            Req1 = [SELECT Id, Active__c, Activation_Date__c FROM SQX_Requirement__c WHERE Id = :Req1.Id];

            System.assert(Req1.Active__c == true,
                'Expected Active to be checked when releasing Controlled Doc.');

            System.assert(Req1.Activation_Date__c != null,
                'Expected activation date to be set when releasing Controlled Doc.');
        }
    }
}