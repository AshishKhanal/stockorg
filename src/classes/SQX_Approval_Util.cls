/**
* This is a helper class that sends records to approval process, allows it to be approved, recalled or rejected.
*
*/
global with sharing class SQX_Approval_Util{

    

    private static final String PROCESS_PENDING_STATUS = 'Pending';
    public static final String APPROVE_KEY = 'Approve';
    public static final String REJECT_KEY = 'Reject';
    public static final String RECALL_KEY = 'Removed'; /** salesforce approval process util keys*/
    
    /**
    * This method checks if an object has been successfully submitted for approval
    * @param objectToBeChecked the sobject which is to be checked as to whether it is in approval or not.
    * @return <code>true</code> if it is in approval
    *         <code>false</code> if it is not in approval.
    */
    public static boolean checkIfObjectIsInApproval(sObject objectToBeChecked){
        List<ProcessInstance> approvalProcesses = 
                    [SELECT ID, Status, TargetObjectID,
                     (SELECT Id FROM 
                      WorkItems)
                     FROM ProcessInstance
                     WHERE
                     Status = : PROCESS_PENDING_STATUS AND 
                     TargetObjectID = :objectToBeChecked.ID];
        
        //if it has one ore more work items then it is in approval
        boolean foundInApproval = approvalProcesses.size() > 0 && approvalProcesses.get(0).WorkItems.size() > 0;
        
        return foundInApproval; 
    }

    /**
    * This method is used to submit a list of sobjects to a predefined approval
    * @param sObjectsToBeSubmitted the list of sobject that are to be submitted for approval process
    */
    public static void submitForApproval(List<sObject> sObjectsToBeSubmitted) {
        submitForApproval(sObjectsToBeSubmitted, '');
    }
    
    /**
    * This method is used to submit a list of sobjects to a predefined approval
    * @param sObjectsToBeSubmitted the list of sobject that are to be submitted for approval process
    * @param submissionComment this is a submit to approval comment
    */
    public static void submitForApproval(List<sObject> sObjectsToBeSubmitted, String submissionComment){
        if(sObjectsToBeSubmitted == null || sObjectsToBeSubmitted.size() == 0)
            return;
    
        List<Approval.ProcessSubmitRequest> submitRequests = new List<Approval.ProcessSubmitRequest>();
        
        //remove duplicates if any. This is done for SQX-1353, where same record is shared to multiple findings but should be send only once for approval
        sObjectsToBeSubmitted= new List<SObject>(new Set<SObject>(sObjectsToBeSubmitted));
        
        for(sObject objectToBeSubmitted : sObjectsToBeSubmitted){
            
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setObjectId(objectToBeSubmitted.Id);
            request.setComments(submissionComment);
            submitRequests.add(request);
        }
        
        if (submitRequests.size() > 0) {
            if (Test.isRunningTest()) {
                SQX_Test_Mock_Approval_Util.process(submitRequests);
            } else {
                Approval.process(submitRequests, true);
            }
        }
    }

    public static void approveRejectRecallRecords(List<sObject> objectsToBeApproved, List<sObject> objectsToBeRejected,
                                                  List<sObject> objectsToBeRecalled){
        approveRejectRecallRecords(objectsToBeApproved, objectsToBeRejected, objectsToBeRecalled, '', '', '', null);
    }
    
    /**
    * this method can approve/reject/recall the list of objects from an approval process
    * @param objectsToBeApproved the list of sobject to be approved
    * @param objectsToBeRejected the entities/sobjects that are to be rejected
    * @param objectsToBeRecalled list of items/sobjects that are to recalled.
    */
    public static void approveRejectRecallRecords(List<sObject> objectsToBeApproved, List<sObject> objectsToBeRejected,
                                                  List<sObject> objectsToBeRecalled, String approvalRemark, String rejectionRemark, String recallRemark, Id originalApproverId){
        
        performActionOnRecord(objectsToBeApproved, APPROVE_KEY, approvalRemark, originalApproverId);
        performActionOnRecord(objectsToBeRejected, REJECT_KEY, rejectionRemark, originalApproverId);
        performActionOnRecord(objectsToBeRecalled, RECALL_KEY, recallRemark, originalApproverId);
    }
    
    /**
    * this is a reusable method that performs a given action approve/reject/recall on the entities
    * @param objectList the list of object which is to be approved/rejected/recalled
    * @param action the action that is to be performed which is one of Approve, Reject or Recall
    * @param remark the remark to place as comment
    * @param originalApproverId the Id of the original approver.
    */
    private static void performActionOnRecord(List<sObject> objectList,String action, String remark, Id originalApproverId){
        if(objectList == null || objectList.size() == 0)
            return;
        
        String approvalActionText = action.toLowerCase();
        if (action == RECALL_KEY) {
            approvalActionText = 'recall';
        }

        //remove duplicates if any. This is done for SQX-1353, where same record is shared to multiple findings but should be send only once performing any ation
         objectList = new List<SObject>(new Set<SObject>(objectList));
        if(Test.isRunningTest()){
            redirectToMockApproval(objectList, action);
            return;
        }
        
        //fetch the work item for the individual objects 
        List<ProcessInstance> approvalProcesses = 
                    [SELECT ID, Status, TargetObjectID,
                     (SELECT Id, OriginalActorID FROM 
                      WorkItems)
                     FROM ProcessInstance
                     WHERE
                     Status = : PROCESS_PENDING_STATUS AND 
                     TargetObjectID IN :objectList];
        
        if (approvalProcesses.size() == 0) {
            // throw error when no workitems were found
            throw new SQX_ApplicationGenericException(
                String.format('Could not find pending approval workitems to {0}.', new List<String>{ approvalActionText })
            );
        }
        else {
            List<Approval.ProcessWorkitemRequest> workitemsToProcess = new List<Approval.ProcessWorkitemRequest>();
            
            for(ProcessInstance apr : approvalProcesses){
                ProcessInstanceWorkitem workitem = null;

                if(apr.WorkItems.size() != 1){
                    //multiple approver support
                    if(originalApproverId != null){
                        for(ProcessInstanceWorkitem wi : apr.WorkItems){
                            //find the work item for the approver.
                            if(wi.OriginalActorID == originalApproverId){
                                workitem = wi;
                                break;
                            }
                        }
                    }
                }
                else{
                    workitem = apr.WorkItems.get(0);
                    if(originalApproverId != null){
                        if(workitem.OriginalActorID != originalApproverId){
                            workitem = null; //if originalApproverId is provided ensures that workitem is for that approver
                        }
                    }
                }

                //if there is no work item
                if (workitem == null) {
                    throw new SQX_ApplicationGenericException(
                        String.format('Could not find pending approval workitems to {0} for record {1}.', new List<String>{ approvalActionText, (String)apr.TargetObjectID })
                    );
                }

                Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
                req.setWorkItemId(workitem.Id);
                req.setAction(action);
                req.setComments(remark);
                
                workitemsToProcess.add(req);
            }
            
            if (workitemsToProcess.size() > 0) {
                Approval.process(workitemsToProcess, true);
            }
        }
    }

    /**
    * this is a method that performs a given action approve/reject/recall on the entities
    * @param objectList List of sobject related to workItems, used to set error in the objects if approval fails
    * @param processInstanceWorkitems the list of ProcessInstanceWorkitem which is to be approved/rejected/recalled
    * @param action the action that is to be performed which is one of Approve, Reject or Recall
    * @param remark the remark to place as comment
    */
    public static void performActionOnWorkItems(List<sObject> objectList, List<Id> processInstanceWorkitemIds, String action, String remark){
        List<Approval.ProcessWorkitemRequest> allApprovalRequests = new List<Approval.ProcessWorkitemRequest>();

        if(Test.isRunningTest()){
            redirectToMockApproval(objectList, action);
            return;
        }

        for(Id processInstanceWorkitemId : processInstanceWorkitemIds){
            Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
            req.setWorkItemId(processInstanceWorkitemId);
            req.setAction(action);
            req.setComments(remark);
            
            allApprovalRequests.add(req);
        }

        if (allApprovalRequests.size() > 0) {
            Approval.process(allApprovalRequests, true);
        }
    }

    /**
    * this is a method that redirects approval request to mock approval util, so that apex test can perform testing related to approval
    * @param objectList List of sobject related to workItems, used to set error in the objects if approval fails
    * @param action the action that is to be performed which is one of Approve, Reject or Recall
    */
    private static void  redirectToMockApproval(List<sObject> objectList, String action){
        List<ResultWrapper> results = SQX_Test_Mock_Approval_Util.processApproval(objectList, action);
            Map<Id, SObject> allObjects =  new Map<Id,SObject>(objectList);
            for(ResultWrapper result : results){
                if(result.isSuccess()){
                    continue;
                }
                
                allObjects.get(result.getEntityId()).addError('error occurred while submitting');
            }
    }
    
    /**
    * gets ProcessInstanceWorkitem related to an itemId
    */
    public static ProcessInstanceWorkitem getProcessInstanceWorkitem(Id itemId) {
        List<ProcessInstanceWorkitem> items = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId, ProcessInstance.Status, CreatedDate, OriginalActorId, ActorId
                                               FROM ProcessInstanceWorkitem
                                               WHERE Id = :itemId];
        
        if (items.size() == 1) {
            return items[0];
        } else {
            return null;
        }
    }
    
    /**
    * gets ProcessInstanceWorkitems assigned to a user and/or the user's queue on an object
    */
    public static List<ProcessInstanceWorkitem> getProcessInstanceWorkitems(Id userID, Id targetObjectId, Boolean includeUserQueues) {
        Set<ID> actorIds = new Set<Id>();
        actorIds.add(userId);
        
        if (includeUserQueues) {
            List<GroupMember> userQueues = [SELECT Group.Id FROM GroupMember WHERE UserOrGroupId = :userID AND Group.Type = 'Queue'];
            for (GroupMember q : userQueues) {
                actorIds.add(q.Group.Id);
            }
        }
        
        List<ProcessInstanceWorkitem> items = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId, ProcessInstance.Status, CreatedDate, OriginalActorId, ActorId
                                               FROM ProcessInstanceWorkitem
                                               WHERE ProcessInstance.TargetObjectId = :targetObjectId AND ActorId IN :actorIds
                                               ORDER BY CreatedDate DESC];
        
        return items;
    }
    
    /**
    * lists approval history items per ProcessInstance of an object
    */
    public static List<ProcessInstance> getProcessInstanceItems(Id targetObjectId) {
        return [SELECT ( SELECT Id, ProcessInstanceId, TargetObjectId, CreatedDate, StepStatus, IsPending, ProcessNode.Name,
                                ActorId, OriginalActorId, Comments, Actor.Name, OriginalActor.Name
                         FROM StepsAndWorkitems
                         ORDER BY IsPending DESC, CreatedDate DESC, Id DESC),
                        Status
                FROM ProcessInstance
                WHERE TargetObjectId = :targetObjectId
                ORDER BY CreatedDate DESC, Id DESC];
    }

    /**
    * This method returns only the approved processes
    * @return ProcessInstanceHistory
    */
    public static List<ProcessInstanceHistory> getApprovedProcessInstanceHistoryItems(Id targetObjectId) {
        
        List<ProcessInstanceHistory> approvalHistory = new List<ProcessInstanceHistory>();
        
        List<ProcessInstance> approvedInstances = [SELECT ( SELECT Id, ProcessInstanceId, TargetObjectId, CreatedDate, StepStatus, IsPending, ProcessNode.Name,
                                ActorId, OriginalActorId, Comments, Actor.Name, OriginalActor.Name
                         FROM StepsAndWorkitems
                         WHERE StepStatus = :SQX_Controlled_Document.APPROVAL_APPROVED
                         ORDER BY CreatedDate DESC),
                        Status
                FROM ProcessInstance
                WHERE TargetObjectId = :targetObjectId AND Status = :SQX_Controlled_Document.APPROVAL_APPROVED
                ORDER BY CreatedDate ASC LIMIT 1];
        if(approvedInstances.size()>0){
            approvalHistory.addAll(approvedInstances.get(0).StepsAndWorkitems);
        }
        
        return approvalHistory;
    }
    
    /**
    * lists approval history items of an object
    */
    public static List<ProcessInstanceHistory> getProcessInstanceHistoryItems(Id targetObjectId) {
        List<ProcessInstanceHistory> result = new List<ProcessInstanceHistory>();
        
        // get approval history items
        List<ProcessInstance> items = SQX_Approval_Util.getProcessInstanceItems(targetObjectId);
        
        // set result approval history items
        for (ProcessInstance i : items) {
            for (ProcessInstanceHistory h : i.StepsAndWorkitems) {
                result.add(h);
            }
        }
        
        return result;
    }

    /**
     * This method returns lists of pending ProcessinstanceNode of an object
     * @param targetObjectId ID of the object affected by this approval process instance.
     * note : ProcessinstanceNode Represents a step in an instance of an approval process.
     */
    public static List<ProcessinstanceNode> getPendingProcessInstanceNode( Id targetObjectId ) {
        return [ SELECT Id, 
                        NodeStatus, 
                        ProcessNodeName 
                        FROM ProcessinstanceNode 
                        WHERE nodeStatus = :PROCESS_PENDING_STATUS 
                        AND ProcessInstance.TargetObjectId = :targetObjectId 
                        AND ProcessInstance.Status = :PROCESS_PENDING_STATUS ];
    }
    
    /**
    * returns record access information for items for a user
    */
    public static Map<Id, UserRecordAccess> getUserRecordAccess(Set<Id> items, Id userId, Boolean returnAllItems) {
        Map<Id, UserRecordAccess> result = new Map<Id, UserRecordAccess>();
        
        if (returnAllItems) {
            // set no access for each items as default
            for (Id i : items) {
                result.put(i, new UserRecordAccess());
            }
        }
        
        // get access information for items
        List<UserRecordAccess> accessItems = [SELECT RecordId, HasAllAccess, HasDeleteAccess, HasEditAccess, HasReadAccess, HasTransferAccess, MaxAccessLevel
                                              FROM UserRecordAccess
                                              WHERE RecordId = :items AND UserId = :userId];
        
        // set obtained access information for each found item
        for (UserRecordAccess i : accessItems) {
            result.put(i.RecordId, i);
        }
        
        return result;
    }
    
    
    /**
    * helps encapsulate the approval process result, mocks it up like it is changeable
    * useful for approval process tests
    */
    public with sharing class ResultWrapper{
        public Approval.ProcessResult result;
        public boolean i_isSuccess;
        public List<Database.Error> i_errors;
        public Id i_entityID;
        public boolean isSuccess(){
            if(result != null){
                return result.isSuccess();
            }
            
            return i_isSuccess;
        }
        
        public Id getEntityID(){
            if(result != null){
                return result.getEntityID();
            }
            
            return i_entityID;
        }
        
        public List<Database.Error> getErrors(){
            if(result != null){
                return result.getErrors();
            }
            
            return i_errors;
        }
        
        public ResultWrapper(){
            
        }
        
        public ResultWrapper(Approval.ProcessResult result){
            this.result = result;
        }
    }


    
    /**
    * helper method to wrap returned result with wrapper.
    */
    public static List<ResultWrapper> getResults(List<Approval.ProcessResult> results){
        List<ResultWrapper> wrapper = new List<ResultWrapper>();
        for(Approval.ProcessResult result : results){
            wrapper.add(new ResultWrapper(result));
        }
        
        return wrapper;
    }
    
    
    ////// Approval Util Lock/Unlock support from API Version 34.0////
    private boolean i_withSharing,
    				i_failOnError;
    
    public SQX_Approval_Util(){
    	this.i_withSharing = true;
    	this.i_failOnError = true;
    }
    
	/**
	* Controls whether or not to obey the sharing rules for objects
	* @param withSharing when set to <code>true</code> causes the transaction to obey sharing rules, else does not obey sharing rules.
	*/
    public SQX_Approval_Util withSharing(Boolean withSharing){
    	this.i_withSharing = withSharing;
    	
    	return this;
    }
    
	/**
	* Manages whether or not an exception should be thrown when any records fails to lock or unlock
	* @param failOnError when set to <code>true</code> causes the transaction to fail by throwing an exception i.e. All or none
	*					 when set to <code>false</code> causes the transaction to go through even if not all objects are locked/unlocked.
	*/
    public SQX_Approval_Util failOnError(Boolean failOnError){
    	this.i_failOnError = failOnError;
    	
    	return this;
    }
    
	/**
	* Locks the records, so that edit can not be done.
	* @param objectsToLock the list of SF ids to be locked.
	* @return returns the result of locking the records
	*/
    public Approval.LockResult[] lock(List<SObject> objectsToLock){
    	return lock(new List<Id>(new Map<Id, SObject>(objectsToLock).keySet()));
    }

    
	/**
	* Locks the records, so that edit can not be done by any one else than admin
	* @param idsToLock the list of SF ids that are to be locked
	* @return returns the result of locking the records.
	*/
    public Approval.LockResult[] lock(List<Id> idsToLock){
    	Approval.LockResult [] result = null;
    	
    	if(this.i_withSharing){
    		result = Approval.lock(idsToLock, this.i_failOnError);
    	}
    	else{
    		result = new Approval_Escalator().lock(idsToLock, this.i_failOnError);
    	}
    	
    	return result;
    }

    /**
     * Locks the records, so that edit can not be done by any one else than admin
     * @param idsToLock the list of SF ids that are to be locked
     */
    @InvocableMethod(label='CQ Lock Records' description='Locks records whose ids are being passed in the method')
    global static void lockRecords(List<Id> idsToLock) {
        new SQX_Approval_Util().lock(idsToLock);
    }
    
	/**
	* Unlocks the records, so that edit can be done
	* @param objectsToUnlock the list of SF Objects that are to be unlocked
	* @return returns the result of unlocking the records.
	*/
    public Approval.UnlockResult[] unlock(List<SObject> objectsToUnLock){
    	return unlock(new List<Id>(new Map<Id, SObject>(objectsToUnLock).keySet()));
    }
   
	/**
	* Unlocks the record, so that they can be edited again
	* @param idsToUnlock the list of SF Ids that are to be unlocked
	* @return returns the result of unlocking the records. Note: an exception is thrown if the any failure occurs and failOnError is set tot true.
	*/ 
    public Approval.UnlockResult[] unlock(List<Id> idsToUnlock){
    	
    	Approval.UnlockResult [] result = null;
    	
    	if(this.i_withSharing){
    		result = Approval.unlock(idsToUnlock, this.i_failOnError);
    	}
    	else{
    		result = new Approval_Escalator().unlock(idsToUnlock, this.i_failOnError);
    	}
    	
    	return result;
    }
    
    /**
    * internally used for escalating privileges when without sharing is required when locking the records
    */
    private without sharing class Approval_Escalator{
    	
		/**
		* Locks the record without looking at the sharing rules, hence used as a method when certain actions must be performed with system privilege
		*/
	    public Approval.LockResult[] lock(List<Id> idsToLock, boolean failOnError){
	    	return Approval.lock(idsToLock, failOnError);
	    }
	    
		/**
		* Unlocks the record without obeying the sharing rules
		* @param idsToUnlock the list of SF Ids that are to be unlocked
		* @param If <code>true</code> will fail the transaction on any error, else it will try to complete the transaction for one or more record
		* @return returns the result of unlocking the records. Note: an exception is thrown if the any failure occurs and failOnError is set tot true.
		*/
	    public Approval.UnlockResult[] unlock(List<Id> idsToUnlock, boolean failOnError){
    		return Approval.unlock(idsToUnlock, failOnError);
	    }
    }
    
}
