@IsTest
public class SQX_Test_Personnel {
    
    public SQX_Personnel__c mainRecord;
    
    private static Boolean checkErrorMessage(Database.Error[] errs, String errmsg) {
        for (Database.Error err : errs) {
            if (err.getMessage().contains(errmsg)) {
                return true;
            }
        }
        return false;
    }
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole, 'AdminUser1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'StdUser1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'StdUser2');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'StdUser3');
    }
    
    public SQX_Test_Personnel() {
        mainRecord = new SQX_Personnel__c();
        Integer randomNumber = (Integer)( Math.random() * 1000000 );
        
        mainRecord.Full_Name__c = 'Personnel' + randomNumber;
        mainRecord.Identification_Number__c = mainRecord.Full_Name__c;
        mainRecord.Active__c = true;
    }

    public SQX_Test_Personnel(String personnelFullName) {
        this();
        mainRecord.Full_Name__c = personnelFullName;
    }
    
    public SQX_Test_Personnel(User val) {
        mainRecord = SQX_Personnel.copyFieldsFromUser(val, new SQX_Personnel__c());
    }
    
    public Database.SaveResult save() {
        Database.SaveResult sr;
        
        if (mainRecord.Id == null) {
            sr = Database.insert(mainRecord, false);
        } else {
            sr = Database.update(mainRecord, false);
        }
        
        return sr;
    }
    
    /*
    * ensures addition of personnel record with all required fields
    */
    public testmethod static void givenAddPersonnel_RecordSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            Database.SaveResult sr;
            
            // create new personnel record with all required fields
            SQX_Test_Personnel test = new SQX_Test_Personnel();
            
            // ACT: add personnel record
            sr = Database.insert(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == true,
                'Expected Personnel to be created.\n' + sr.getErrors());
        }
    }
    
    /*
    * ensures required field Full_Name__c is set before saving
    */
    public testmethod static void givenSavePersonnel_FullNameNotSet_RecordNotSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            Database.SaveResult sr;
            
            // create new personnel record with all required fields
            SQX_Test_Personnel test = new SQX_Test_Personnel();
            String tmp = test.mainRecord.Full_Name__c;
            
            // remove Full_Name__c value
            test.mainRecord.Full_Name__c = '';
            
            // ACT: add personnel record without Full_Name__c
            sr = Database.insert(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == false,
                'Expected Personnel not to be created without required field Full_Name__c.\n' + sr.getErrors());
            
            
            // set Full_Name__c value
            test.mainRecord.Full_Name__c = tmp;
            
            // ACT: add personnel record
            sr = Database.insert(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == true,
                'Expected Personnel to be created.\n' + sr.getErrors());
            
            
            // remove Full_Name__c value
            test.mainRecord.Full_Name__c = '';
            
            // ACT: update personnel record without Full_Name__c
            sr = Database.update(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == false,
                'Expected Personnel not to be updated without required field Full_Name__c.\n' + sr.getErrors());
        }
    }
    
    /*
    * ensures required field Identification_Number__c is set before saving
    */
    public testmethod static void givenSavePersonnel_IdentificationNumberNotSet_RecordNotSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            Database.SaveResult sr;
            
            // create new personnel record with all required fields
            SQX_Test_Personnel test = new SQX_Test_Personnel();
            String tmp = test.mainRecord.Identification_Number__c;
            
            // remove Identification_Number__c value
            test.mainRecord.Identification_Number__c = '';
            
            // ACT: add personnel record without Identification_Number__c
            sr = Database.insert(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == false,
                'Expected Personnel not to be created without required field Identification_Number__c.\n' + sr.getErrors());
            
            
            // set Identification_Number__c value
            test.mainRecord.Identification_Number__c = tmp;
            
            // ACT: add personnel record
            sr = Database.insert(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == true,
                'Expected Personnel to be created.\n' + sr.getErrors());
            
            
            // remove Identification_Number__c value
            test.mainRecord.Identification_Number__c = '';
            
            // ACT: update personnel record without Identification_Number__c
            sr = Database.update(test.mainRecord, false);
            
            System.assert(sr.isSuccess() == false,
                'Expected Personnel not to be updated without required field Identification_Number__c.\n' + sr.getErrors());
        }
    }
    
    /*
    * ensures proper taining status is set as per related incomplete document trainings
    */
    public testmethod static void givenProperTrainingStatusIsSet() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            Database.SaveResult sr;
            Database.DeleteResult dr;
            Database.UndeleteResult ur;
            
            // ACT: add new personnel record
            SQX_Test_Personnel testPsn = new SQX_Test_Personnel();
            testPsn.save();
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_CURRENT, testPsn.mainRecord.Training_Status__c,
                'Training Status is expected to be "Current" when new personnel record is added.');
            
            
            // add required document
            SQX_Test_Controlled_Document testdoc = new SQX_Test_Controlled_Document().save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add pending document training
            SQX_Personnel_Document_Training__c dtn1 = new SQX_Personnel_Document_Training__c(
                SQX_Controlled_Document__c = testdoc.doc.Id,
                SQX_Personnel__c = testPsn.mainRecord.Id,
                SQX_Trainer__c = standardUser.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            
            // ACT: add pending document training
            sr = Database.insert(dtn1, false);
            
            System.assert(sr.isSuccess(), 'Required document training is expected to be saved.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_PENDING, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Pending" when document training is added.');
            
            
            // ACT: delete pending document training
            dr = Database.delete(dtn1, false);
            
            System.assert(dr.isSuccess(), 'Pending document training is expected to be deleted.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_CURRENT, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Current" when document training is deleted.');
            
            
            // ACT: undelete pending document training
            ur = Database.undelete(dtn1, false);
            
            System.assert(ur.isSuccess(), 'Pending document training is expected to be undeleted.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_PENDING, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Pending" when document training is undeleted.');
            
            
            // set document training status to training approval pending
            dtn1.Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING;
            
            // ACT: update document training
            sr = Database.update(dtn1, false);
            
            System.assert(sr.isSuccess(), 'Modified document training is expected to be saved.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_PENDING, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Pending" when document training is updated.');
            
            
            // set document training status to complete
            dtn1.Completion_Date__c = System.today();
            dtn1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            
            // ACT: update document training
            sr = Database.update(dtn1, false);
            
            System.assert(sr.isSuccess(), 'Modified document training is expected to be saved.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_CURRENT, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Current" when document training is updated.');
            
            
            // add another required document
            SQX_Test_Controlled_Document testdoc2 = new SQX_Test_Controlled_Document().save();
            testdoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add pending document training
            SQX_Personnel_Document_Training__c dtn2 = new SQX_Personnel_Document_Training__c(
                SQX_Controlled_Document__c = testdoc2.doc.Id,
                SQX_Personnel__c = testPsn.mainRecord.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            
            // ACT: add pending document training
            sr = Database.insert(dtn2, false);
            
            System.assert(sr.isSuccess(), 'Required document training is expected to be saved.\n' + sr.getErrors());
            
            // read training status
            testPsn.mainRecord = [SELECT Id, Training_Status__c FROM SQX_Personnel__c WHERE Id = :testPsn.mainRecord.Id];
            
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_PENDING, testPsn.mainRecord.Training_Status__c,
                'Personnel Training Status is expected to be "Pending" when incomplete document training exists.');
            
            
            // ACT: make controlled document obsolete to make pending training obsolete
            testdoc2.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // read obsolete training and personnel training status
            dtn2 = [SELECT Id, Status__c, SQX_Personnel__r.Training_Status__c FROM SQX_Personnel_Document_Training__c WHERE Id = :dtn2.Id];
            
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_OBSOLETE, dtn2.Status__c,
                'Pending training is expected to be obsolete for obsolete document.');
            System.assertEquals(SQX_Personnel.TRAINING_STATUS_CURRENT, dtn2.SQX_Personnel__r.Training_Status__c,
                'Personnel Training Status is expected to be "Current" when obsolete document training exists with no pending trainings.');
        }
    }
    
    /*
    * tests duplicate Identification Number value while creating, and updating 
    */
    public testmethod static void givenDuplicateIdentificationNumber_NotSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            // add personnel record
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            Database.SaveResult sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // create duplicate record
            SQX_Test_Personnel p2 = new SQX_Test_Personnel();
            p2.mainRecord.Identification_Number__c = p1.mainRecord.Identification_Number__c;
            
            // ACT: add duplicate record
            sr = p2.save();

            System.assert(sr.isSuccess() == false, 'Record with duplicate Indentification Number value is expected not to be saved.');
            System.assert(checkErrorMessage(sr.getErrors(), 'duplicate value found'),
                'Expected error message containing "duplicate value found" not found.');
            
            // create new non-duplicate record
            p2 = new SQX_Test_Personnel();
            sr = p2.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // update personnel record with duplicate value
            p2.mainRecord.Identification_Number__c = p1.mainRecord.Identification_Number__c;
            
            // ACT: update record with duplicate value
            sr = p2.save();

            System.assert(sr.isSuccess() == false, 'Record with duplicate Indentification Number value is expected not to be saved.');
            System.assert(checkErrorMessage(sr.getErrors(), 'duplicate value found'),
                'Expected error message containing "duplicate value found" not found.');
        }
    }
    
    /*
    * tests duplicate user value while importing, creating, and updating
    */
    public testmethod static void givenDuplicateUser_NotSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        System.assert(standardUser != null);
        
        System.runas(standardUser) {
            // add personnel record for standardUser
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(standardUser);
            Database.SaveResult sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // ACT: import duplicate record
            sr = SQX_Personnel.importUser(standardUser.Id);
            
            System.assert(sr.isSuccess() == false, 'Record with duplicate user value is expected not to be saved.');
            System.assert(checkErrorMessage(sr.getErrors(), 'duplicate value found'),
                'Expected error message containing "duplicate value found" not found.');
            
            // create duplicate record for standardUser
            SQX_Test_Personnel p2 = new SQX_Test_Personnel();
            p2.mainRecord.SQX_User__c = standardUser.Id;
            
            // ACT: add duplicate record
            sr = p2.save();

            System.assert(sr.isSuccess() == false, 'Record with duplicate user value is expected not to be saved.');
            System.assert(checkErrorMessage(sr.getErrors(), 'duplicate value found'),
                'Expected error message containing "duplicate value found" not found.');
            
            // create new non-duplicate record
            p2.mainRecord.SQX_User__c = null;
            sr = p2.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // update personnel record with duplicate user value
            p2.mainRecord.SQX_User__c = standardUser.Id;
            // ACT: update record with duplicate user value
            sr = p2.save();

            System.assert(sr.isSuccess() == false, 'Record with duplicate user value is expected not to be saved.');
            System.assert(checkErrorMessage(sr.getErrors(), 'duplicate value found'),
                'Expected error message containing "duplicate value found" not found.');
        }
    }
    
    /*
    * tests personnel record sync when user details are modified
    */
    public testmethod static void givenUserModified_RelatedPersonnelSynced() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('AdminUser1');
        User user2 = users.get('StdUser1');
        User user3 = users.get('StdUser2');
        System.assert(user1 != null);
        System.assert(user2 != null);
        System.assert(user3 != null);
        
        System.runas(user1) {
            // set user1 manager to user1
            user2.ManagerId = user1.Id;
            Database.SaveResult sr = Database.update(user2, false);
            System.assert(sr.isSuccess(), 'Required user record is expected to be saved.');
            
            // add personnel record for user1
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // modify user details
            user2.FirstName = 'mod' + user2.FirstName;
            user2.LastName = user2.LastName + 'mod';
            user2.UserName = 'mod' + user2.UserName;
            user2.Email = 'modemail' + user2.Email;
            user2.ManagerId = user3.Id;
            user2.IsActive = false;
            
            Test.startTest();
            
            // ACT: update modified user
            sr = Database.update(user2, false);
            
            Test.stopTest();
            
            System.assert(sr.isSuccess() == true, 'Modified user details expected to be saved.');
            
            // get related personnel record of user2
            p1.mainRecord = [SELECT Id, Full_Name__c, Identification_Number__c, Email_Address__c, OwnerId, Active__c
                             FROM SQX_Personnel__c WHERE Id = :p1.mainRecord.Id];
            
            System.assertEquals(user2.FirstName + ' ' + user2.LastName, p1.mainRecord.Full_Name__c,
                'Full Name value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.UserName, p1.mainRecord.Identification_Number__c,
                'Identification Number value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.Email, p1.mainRecord.Email_Address__c,
                'Email Address value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.ManagerId, p1.mainRecord.OwnerId,
                'Owner value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.IsActive, p1.mainRecord.Active__c,
                'Active value of the related personnel expected to be synced with modified user details.');
        }
    }
    
    /*
    * tests personnel record sync excluding Identification Number
    */
    public testmethod static void givenUserModified_IdNumberNotSameAsUsername_RelatedPersonnelSyncedExcludingIdNumber() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('AdminUser1');
        User user2 = users.get('StdUser1');
        User user3 = users.get('StdUser2');
        System.assert(user1 != null);
        System.assert(user2 != null);
        System.assert(user3 != null);
        
        System.runas(user1) {
            // set user1 manager to user1
            user2.ManagerId = user1.Id;
            Database.SaveResult sr = Database.update(user2, false);
            System.assert(sr.isSuccess(), 'Required user record is expected to be saved.');
            
            // add personnel record for user1
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            p1.mainRecord.Identification_Number__c = user2.Id; // setting value other than username
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // modify user details
            user2.FirstName = 'mod' + user2.FirstName;
            user2.LastName = user2.LastName + 'mod';
            user2.UserName = 'mod' + user2.UserName;
            user2.Email = 'modemail' + user2.Email;
            user2.ManagerId = user3.Id;
            user2.IsActive = false;
            
            Test.startTest();
            
            // ACT: update modified user
            sr = Database.update(user2, false);
            
            Test.stopTest();
            
            System.assert(sr.isSuccess() == true, 'Modified user details expected to be saved.');
            
            // get related personnel record of user2
            p1.mainRecord = [SELECT Id, Full_Name__c, Identification_Number__c, Email_Address__c, OwnerId, Active__c
                             FROM SQX_Personnel__c WHERE Id = :p1.mainRecord.Id];
            
            System.assertEquals(user2.FirstName + ' ' + user2.LastName, p1.mainRecord.Full_Name__c,
                'Full Name value of the related personnel expected to be synced with modified user details.');
            System.assertNotEquals(user2.UserName, p1.mainRecord.Identification_Number__c,
                'Identification Number value of the related personnel not expected to be synced with modified user details.');
            System.assertEquals(user2.Id, p1.mainRecord.Identification_Number__c,
                'Identification Number value of the related personnel expected to be same as previous value.');
            System.assertEquals(user2.Email, p1.mainRecord.Email_Address__c,
                'Email Address value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.ManagerId, p1.mainRecord.OwnerId,
                'Owner value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.IsActive, p1.mainRecord.Active__c,
                'Active value of the related personnel expected to be synced with modified user details.');
        }
    }
    
    /*
    * tests personnel record sync excluding Owner
    */
    public testmethod static void givenUserModified_OwnerNotSame_RelatedPersonnelSyncedExcludingOwner() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('AdminUser1');
        User user2 = users.get('StdUser1');
        User user3 = users.get('StdUser2');
        User user4 = users.get('StdUser3');
        System.assert(user1 != null);
        System.assert(user2 != null);
        System.assert(user3 != null);
        System.assert(user4 != null);
        
        System.runas(user1) {
            // set user1 manager to user4
            user2.ManagerId = user3.Id;
            Database.SaveResult sr = Database.update(user2, false);
            System.assert(sr.isSuccess(), 'Required user record is expected to be saved.');
            
            // add personnel record for user1
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            // modify user details
            user2.FirstName = 'mod' + user2.FirstName;
            user2.LastName = user2.LastName + 'mod';
            user2.UserName = 'mod' + user2.UserName;
            user2.Email = 'modemail' + user2.Email;
            user2.ManagerId = user4.Id;
            user2.IsActive = false;
            
            Test.startTest();
            
            // ACT: update modified user
            sr = Database.update(user2, false);
            
            Test.stopTest();
            
            System.assert(sr.isSuccess() == true, 'Modified user details expected to be saved.');
            
            // get related personnel record of user2
            p1.mainRecord = [SELECT Id, Full_Name__c, Identification_Number__c, Email_Address__c, OwnerId, Active__c
                             FROM SQX_Personnel__c WHERE Id = :p1.mainRecord.Id];
            
            System.assertEquals(user2.FirstName + ' ' + user2.LastName, p1.mainRecord.Full_Name__c,
                'Full Name value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.UserName, p1.mainRecord.Identification_Number__c,
                'Identification Number value of the related personnel expected to be synced with modified user details.');
            System.assertEquals(user2.Email, p1.mainRecord.Email_Address__c,
                'Email Address value of the related personnel expected to be synced with modified user details.');
            System.assertNotEquals(user2.ManagerId, p1.mainRecord.OwnerId,
                'Owner value of the related personnel not expected to be synced with modified user details.');
            System.assertEquals(user1.Id, p1.mainRecord.OwnerId,
                'Owner value of the related personnel expected to be same as previous value.');
            System.assertEquals(user2.IsActive, p1.mainRecord.Active__c,
                'Active value of the related personnel expected to be synced with modified user details.');
        }
    }
    
    /*
    * tests personnel job function activation when personnel activated
    * @story SQX-1668
    */
    public testmethod static void givenPersonnelActivated_NotActivatedPersonnelJobFunctionActivated() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('StdUser1');
        System.assert(user1 != null);
        
        System.runas(user1) {
            Database.SaveResult sr;
            
            // add required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for testing' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            sr = jf.save();
            
            // add required personnel record
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // add required personnel job function
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            sr = Database.insert(pjf1, false);
            System.assert(sr.isSuccess(), 'Required personnel job function is expected to be saved.\n' + sr.getErrors());
            // deactivate pjf1
            pjf1.Active__c = false;
            sr = Database.update(pjf1, false);
            System.assert(sr.isSuccess(), 'Required personnel job function is expected to be saved.\n' + sr.getErrors());
            
            // deactivate personnel
            p1.mainRecord.Active__c = false;
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // add required personnel job function
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            sr = Database.insert(pjf2, false);
            System.assert(sr.isSuccess(), 'Required personnel job function is expected to be saved.\n' + sr.getErrors());
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: activated personnel
            p1.mainRecord.Active__c = true;
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // get updated pjf1
            pjf1 = [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf1.Id];
            
            System.assertEquals(false, pjf1.Active__c,
                'Deactivated personnel job function is expected not to be changed when personnel is activated.');
            
            // get updated pjf2
            pjf2 = [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf2.Id];
            
            System.assertEquals(true, pjf2.Active__c,
                'Not activated personnel job function is expected to be active when personnel is activated.');
        }
    }
    
    /*
    * tests personnel job function deactivation when personnel deactivated
    * @story SQX-1668
    */
    public testmethod static void givenPersonnelDeactivated_ActivePersonnelJobFunctionDeactivated() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('StdUser1');
        System.assert(user1 != null);
        
        System.runas(user1) {
            Database.SaveResult sr;
            
            // add required job functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1 for testing' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            sr = jf.save();
            
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for testing' );
            jf = new SQX_Test_Job_Function(jf2);
            sr = jf.save();
            
            // add required personnel record
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // add required personnel job functions
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            sr = Database.insert(pjf1, false);
            System.assert(sr.isSuccess(), 'Required personnel job function is expected to be saved.\n' + sr.getErrors());
            
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = false
            );
            sr = Database.insert(pjf2, false);
            System.assert(sr.isSuccess(), 'Required personnel job function is expected to be saved.\n' + sr.getErrors());
            
            // ACT: deactivate personnel
            p1.mainRecord.Active__c = false;
            sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.\n' + sr.getErrors());
            
            // get updated pjf1
            pjf1 = [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf1.Id];
            
            System.assertEquals(false, pjf1.Active__c,
                'Active personnel job function is expected to be deactivated when personnel is deactivated.');
            
            // get updated pjf2
            pjf2 = [SELECT Id, Active__c, Deactivation_Date__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf2.Id];
            
            System.assertEquals(false, pjf2.Active__c,
                'Inactive personnel job function is expected not to be changed when personnel is deactivated.');
            System.assert(pjf2.Deactivation_Date__c == null,
                'Inactive personnel job function is expected not to be changed when personnel is deactivated.');
        }
    }
}