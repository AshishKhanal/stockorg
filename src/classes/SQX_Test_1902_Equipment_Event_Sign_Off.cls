/**
* Unit tests for SQX_Extension_Equipment_Event_Schedule for event sign off
*/
@IsTest
public class SQX_Test_1902_Equipment_Event_Sign_Off {
    
    static boolean runAllTests = true,
                   run_givenEventSignOff_RecurringEvent = false,
                   run_givenEventSignOff_NonRecurringEventWithNextDueDate_NextDueDateCleared = false,
                   run_givenEventSignOff_ZeroFileSizeError = false,
                   run_givenEquipmentwithEventSchedule_whenEventScheduleIsSignedOffbyQueueMembers_thenEventScheduleIsSignedOffSuccessfully = false;
    
    /**
    * unit test for recurring equipment event sign off
    */
    public static testmethod void givenEventSignOff_RecurringEvent() {
        if (!runAllTests && !run_givenEventSignOff_RecurringEvent) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            Date initialNextDueDate = System.today() + 1;
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Next_Due_Date__c = initialNextDueDate,
                Recurring_Event__c = true,
                Recurring_Interval__c = 2,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_WEEK,
                Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_PERFORMED_DATE
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required recurring equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            Date performedDate = System.today() - 2;
            Date expectedNextDueDate = SQX_Equipment_Event_Schedule.calculateNextDueDateByScheduleBasis(sch1.Schedule_Basis__c, sch1.Recurring_Unit__c,
                performedDate, sch1.Next_Due_Date__c, (Integer)sch1.Recurring_Interval__c);
            
            // set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);
            
            // set up sign-off data
            ext.eventHistory.Performed_Date__c = performedDate;
            ext.eventHistory.Performed_By__c = 'Mr Maintenance';
            ext.eventHistory.Comments__c = 'Replaced a bolt.';
            ext.isFileUploaded = 'true';
            Attachment historyAttachment = ext.getEventHistoryAttachment();
            historyAttachment.Body = Blob.valueOf('Equipment working good after maintenance.');
            historyAttachment.Name = 'report.txt';
            historyAttachment.Description = 'maintenance report';
            
            // ACT: event sign-off
            PageReference pr = ext.signOff();
            
            System.assert(pr != null, 'Page reference is expected.');
            
            sch1 = [SELECT Id, Name, Last_Performed_Date__c, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            
            System.assertEquals(performedDate, sch1.Last_Performed_Date__c);
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c);
            
            List<SQX_Equipment_Event_History__c> history = [SELECT Id, Name, SQX_Equipment__c, SQX_Equipment_Event_Schedule__c, Performed_Date__c, Performed_By__c,
                                                                   Scheduled_Due_Date__c, Comments__c, Sign_Off_Date__c, Sign_Off_By__c,
                                                                   (SELECT Id, Name, Description FROM Attachments)
                                                            FROM SQX_Equipment_Event_History__c WHERE ID = :ext.eventHistory.Id];
            
            System.assertEquals(1, history.size(), 'Signed off equipment event history is expected to be saved.');
            
            System.assertEquals(eqp1.Id, history[0].SQX_Equipment__c);
            System.assertEquals(sch1.Id, history[0].SQX_Equipment_Event_Schedule__c);
            System.assertEquals(ext.eventHistory.Performed_By__c, history[0].Performed_By__c);
            System.assertEquals(performedDate, history[0].Performed_Date__c);
            System.assertEquals(initialNextDueDate, history[0].Scheduled_Due_Date__c);
            System.assertEquals(ext.eventHistory.Comments__c, history[0].Comments__c);
            System.assert(history[0].Sign_Off_Date__c != null, 'Event sign off date is expected to be set.');
            System.assertEquals(ext.getUserDetails(), history[0].Sign_Off_By__c);
            
            System.assertEquals(1, history[0].Attachments.size(), 'One attachment is expected to be saved for the event history.');
            System.assertEquals(historyAttachment.Name, history[0].Attachments[0].Name);
            System.assertEquals(historyAttachment.Description, history[0].Attachments[0].Description);
            
            
            // ACT (SQX-8211): update SQX_Equipment_Event_History__c
            update history;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Equipment_Event_History__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Equipment_Event_History__c.SObjectType);
        }
    }
    
    /**
    * unit test for non-recurring equipment event sign off to verify next due date is cleared
    */
    public static testmethod void givenEventSignOff_NonRecurringEventWithNextDueDate_NextDueDateCleared() {
        if (!runAllTests && !run_givenEventSignOff_NonRecurringEventWithNextDueDate_NextDueDateCleared) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            Date initialNextDueDate = System.today() + 1;
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Next_Due_Date__c = initialNextDueDate,
                Recurring_Event__c = false,
                Recurring_Interval__c = 2,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_WEEK,
                Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_PERFORMED_DATE
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required recurring equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);
            
            // set up sign-off data
            ext.eventHistory.Performed_Date__c = System.today() - 2;
            ext.eventHistory.Performed_By__c = 'Mr Maintenance';
            
            // ACT: event sign-off
            PageReference pr = ext.signOff();
            
            System.assert(pr != null, 'Page reference is expected.');
            
            sch1 = [SELECT Id, Name, Last_Performed_Date__c, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            
            System.assertEquals(null, sch1.Next_Due_Date__c);
        }
    }
    
    /**
    * unit test for equipment event sign off with attachment file size zero
    */
    public static testmethod void givenEventSignOff_ZeroFileSizeError() {
        if (!runAllTests && !run_givenEventSignOff_ZeroFileSizeError) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Recurring_Event__c = false
            );
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required recurring equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);
            
            // set up sign-off data
            ext.eventHistory.Performed_Date__c = System.today();
            ext.eventHistory.Performed_By__c = 'Mr Performer';
            ext.isFileUploaded = 'true';
            Attachment historyAttachment = ext.getEventHistoryAttachment();
            historyAttachment.Body = Blob.valueOf('');
            historyAttachment.Name = 'zerofilesize.txt';
            
            // ACT: event sign-off
            ext.signOff();
            
            ApexPages.Message[] msgs = ApexPages.getMessages();
            
            System.assert(SQX_Utilities.checkPageMessage(msgs, 'Uploaded file does not have any content.'), '1 page error message is expected.');
        }
    }

    /**
    * Given: Queue is created with two users A and B
    *        Equipment is created by User A with Equipment Event Schedule added
    *        Task is created for the user A
    * When : Ownership is transferred to Queue and Equipment Event is Signed off by user B
    * Then : No error message is thrown and Equipment Event Schedule is Signed off successfully
    */
    public static testmethod void givenEquipmentwithEventSchedule_whenEventScheduleIsSignedOffbyQueueMembers_thenEventScheduleIsSignedOffSuccessfully() {
        if (!runAllTests && !run_givenEquipmentwithEventSchedule_whenEventScheduleIsSignedOffbyQueueMembers_thenEventScheduleIsSignedOffSuccessfully) {
            return;
        }

        // Create Standard Users A & B(Queue Members) , C (Non Queue Member) & Admin user 
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUserA = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserB = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserC = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        //Create Queue
        Group queue = new Group();
        queue.Name = 'NewTestGroup';
        queue.DeveloperName = 'New_Test_Queue';
        queue.Type = 'Queue';
        insert queue;

        QueueSObject sobj = new QueueSObject();
        sObj.SobjectType = SQX.Equipment;
        sObj.QueueId = queue.Id;
        insert sObj;

        // add queue members
        GroupMember queueMembers = new GroupMember(GroupId = queue.Id, UserOrGroupId = standardUserA.Id);
        insert queueMembers;

        queueMembers = new GroupMember(GroupId = queue.Id, UserOrGroupId = standardUserB.Id);
        insert queueMembers;

        SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c();

        System.runAs(standardUserA) {

            // Arrange: Add Equipement
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );

            //Assert: Equipment is expected to be saved.
            List<DataBase.SaveResult> result1 = new SQX_DB().continueOnError().op_insert(new List<SQX_Equipment__c>{eqp1}, new List<Schema.SObjectField>{});
            System.assert(result1[0].isSuccess(), 'Required equipment is expected to be saved.\n' + result1[0].getErrors());

            // Arrange: Add Equipment Event Schedule
            Date initialNextDueDate = System.today() + 1;
            sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Next_Due_Date__c = initialNextDueDate,
                Recurring_Event__c = false,
                Recurring_Interval__c = 2,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_WEEK,
                Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_PERFORMED_DATE
            );

            //Assert: Equipment event schedule is expected to be saved
            result1 = new SQX_DB().continueOnError().op_insert(new List<SQX_Equipment_Event_Schedule__c>{sch1}, new List<Schema.SObjectField>{});
            System.assert(result1[0].isSuccess(), 'Equipment event schedule is expected to be saved.\n' + result1[0].getErrors());

            // Act: Create Task for the event schedule 
            sch1.Queue_Event_Task__c = true;
            new SQX_DB().op_update(new List<SQX_Equipment_Event_Schedule__c>{sch1}, new List<Schema.SObjectField>{});

            // Assert: Task must be created 
            List<Task> task = [SELECT Id, Status FROM Task WHERE WhatId =:sch1.Id ];
            System.assertEquals(true, task.size() > 0);
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, task[0].Status);
            
            // sync task id to sch1
            sch1.Event_Task_Id__c = task[0].Id;

            //Act: Change the owner of Equipment to Queue
            eqp1.OwnerId = queue.Id;
            new SQX_DB().op_update(new List<SQX_Equipment__c>{eqp1}, new List<Schema.SObjectField>{});
        }

        // Try signing off with non queue member 
        System.runAs(standardUserC) {

            // Arrange: set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);

            // Arrange: set up sign-off data
            ext.eventHistory.Performed_Date__c = System.today() - 2;
            ext.eventHistory.Performed_By__c = 'Mr Maintenance';
            
            // ACT: event sign-off  
            ext.signOff();

            // Assert: Error message is thrown when non queue member tries to sign off Equipment Event Schedule 
            ApexPages.Message[] msgs = ApexPages.getMessages();
            System.assert(SQX_Utilities.checkPageMessage(msgs, 'insufficient access rights on cross-reference id'), 'User with insufficient access rights on equipment should not be able to signOff');
            
        }

        // Try signing off with queue member 
        System.runAs(standardUserB) {
            
            //Arrange: Set up data for event sign-off
            ApexPages.StandardController sc = new ApexPages.standardController(sch1);
            SQX_Extension_Equipment_Event_Schedule ext = new SQX_Extension_Equipment_Event_Schedule(sc);
            
            //Arrange: Set up sign-off data
            ext.eventHistory.Performed_Date__c = System.today() - 2;
            ext.eventHistory.Performed_By__c = 'Mr Maintenance';
            
            // ACT: Event sign-off
            PageReference pr = ext.signOff();

            // Assert: Equipment Event Schedule is signed off 
            System.assert(pr != null, 'Page reference is expected.');
        }
    }
}