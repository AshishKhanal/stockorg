/**
 * This class contains the logic related to complaint defect
 */
public with sharing class SQX_Complaint_Defect {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified() {
            
        }
        
        /**
         * The constructor for handling the trigger activities
         * @param newComplaintDefects the complaint defects that have been newly added/updated
         * @param oldMap the map of old values of the complaint defect
         */
        public Bulkified(List<SQX_Complaint_Defect__c> newComplaintDefects, Map<Id, SQX_Complaint_Defect__c> oldMap) {
            super(newComplaintDefects, oldMap);
        }
        
        /**
         * Method to prevent deletion of complaint defect if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords() {
            
            this.resetView();
            
            Map<Id, SQX_Complaint_Defect__c> complaintDefects = new Map<Id, SQX_Complaint_Defect__c>((List<SQX_Complaint_Defect__c>)this.view);
            
            for(SQX_Complaint_Defect__c complaintDefect : [SELECT Id FROM SQX_Complaint_Defect__c WHERE Id IN : complaintDefects.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                complaintDefects.get(complaintDefect.Id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
        }
        
    }

}