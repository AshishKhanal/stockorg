/**
* unit tests for supplier deviation record activities
*/
@isTest
public class SQX_Test_5730_SD_RecordActivities {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, adminUser});
    }


    /**
    * GIVEN : given supplier deviation record
    * WHEN : completing the supplier deviation record
    * THEN : captured the activity
    */
    public static testMethod void givenSupplierDeviationRecord_WhenCompletingTheRecord_ThenCaptureTheActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');


        List<String> expectedRecordActivities = new List<String> 
        {
            Label.cq_ui_submitting_the_record,
            Label.SQX_PS_CAPA_Initiating_the_record,
            Label.cq_ui_completing_the_record
        };

        SQX_Test_Supplier_Deviation sd;
        System.runAs(adminUser) {
            //Arrange: Initiate an SD record
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', standardUser, 1);

            sd = new SQX_Test_Supplier_Deviation().save();

            Test.startTest();
            sd.submit();

            sd.initiate();

            SQX_BulkifiedBase.clearAllProcessedEntities();
        }

        // ACT : Complete the record by completing the task
        System.runAs(standardUser) {

            Task t = [SELECT Id FROM Task WHERE WhatId =: sd.sd.Id];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            Test.stopTest();

        }

        // Assert : Complete activity is added along with submit and initate action
        List<SQX_Supplier_Deviation_Record_Activity__c> activitiesRecorded = [SELECT Id, Activity__c FROM SQX_Supplier_Deviation_Record_Activity__c WHERE SQX_Supplier_Deviation__c =: sd.sd.Id ORDER BY CreatedDate ASC];

        System.assertEquals(expectedRecordActivities.size(), activitiesRecorded.size());

        // assert order of activity
        for(integer i=0; i < activitiesRecorded.size(); i++) {
            System.assertEquals(expectedRecordActivities.get(i), activitiesRecorded.get(i).Activity__c);
        }
    }

    /**
    * GIVEN : given supplier deviation record
    * WHEN : voiding the supplier deviation record
    * THEN : captured the activity
    */
    public static testMethod void givenSupplierDeviationRecord_WhenVoidingTheRecord_ThenCaptureTheActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            //Arrange: Retrieve supplier deviation record
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();

            //Act: Voiding the record
            sd.void();

            //Assert: Ensured that voiding activity was created
            List<SQX_Supplier_Deviation_Record_Activity__c> sdRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Deviation_Record_Activity__c];
            System.assertEquals(1, sdRecordActivity.size());
            System.assertEquals(Label.sqx_ps_capa_voiding_the_record, sdRecordActivity[0].Activity__c);
        }
    }


    /**
     *  Given an SD record
     *  When the SD record is acted upon (edited or progressed further)
     *  Then record activities are captured
     */
    static testmethod void givenSDRecord_WhenItMovesThroughVariousStages_RecordActivitiesAreCorrectlyCaptured() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){

            List<String> expectedRecordActivities = new List<String> 
            {
                Label.cq_ui_submitting_the_record,
                Label.SQX_PS_CAPA_Initiating_the_record,
                Label.cq_ui_completing_the_record,
                Label.SQX_PS_CAPA_Saving_the_record,
                Label.CQ_UI_Extending_The_Record,
                Label.CQ_UI_Restarting_the_record,
                Label.cq_ui_completing_the_record,
                Label.SQX_PS_CAPA_Closing_the_record
            };

            //Arrange: an SD record
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();


            Test.startTest();

            //ACT: Submitting the record
            sd.submit();

            //ACT : Editing the record, this activity should not be captured
            sd.clear();
            sd.sd.Reason_for_Deviation__c = 'Changed Description';
            sd.save();

            //Act: Initiating the record
            sd.initiate();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT : Editing the record, this activity should be captured
            sd.clear();
            sd.sd.Reason_for_Deviation__c = 'Changed Description';
            sd.save();

            Test.stopTest();
            
            //extending the record
            SQX_BulkifiedBase.clearAllProcessedEntities();
            sd.extend();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT: Restarting the record
            sd.restart();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //ACT: Closing the record
            sd.close();

            // Assert : Record Activities are appropriately added
            List<SQX_Supplier_Deviation_Record_Activity__c> activitiesRecorded = [SELECT Id, Activity__c FROM SQX_Supplier_Deviation_Record_Activity__c WHERE SQX_Supplier_Deviation__c =: sd.sd.Id ORDER BY CreatedDate ASC];

            System.assertEquals(expectedRecordActivities.size(), activitiesRecorded.size());

            // assert order of activity
            for(integer i=0; i < activitiesRecorded.size(); i++) {
                System.assertEquals(expectedRecordActivities.get(i), activitiesRecorded.get(i).Activity__c);
            }

        }

    }
}