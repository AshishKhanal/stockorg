/**
 * Test class to primarily test the Report Submission Acknowledgement flow as described in SQX-7547
*/

@isTest
public class SQX_Test_7547_Report_Acknowledgement {

    /**
     * Test data setup
    */
    @testsetup
    static void setupInitialData() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');

        System.runAs(standardUser) {

            // creating a complaint record
            SQX_Test_Complaint complaintWrapper = new SQX_Test_Complaint(null, adminUser).save();

            // setup extension
            SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c(
                SQX_Complaint__c = [SELECT Id FROM SQX_Complaint__c LIMIT 1].Id,
                Status__c = SQX_Regulatory_Report.STATUS_PENDING,
                Name = 'My Report',
                Due_Date__c = Date.today().addDays(10)
            );
            insert report;

            // create a medwatch record
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c(
                SQX_Regulatory_Report__c = report.Id
            );
            insert medwatch;

            // creating a report submission record
            SQX_Submission_History__c submission = new SQX_Submission_History__c(
                SQX_Complaint__c = complaintWrapper.complaint.Id,
                Name = '5-day MDR',
                Submitted_Date__c = Date.today(),
                Submitted_By__c = 'Some submitter',
                Status__c = SQX_Submission_History.STATUS_PENDING
            );
            insert submission;
        }
    }

    // TESTING VALIDATION RULES

    final static String VALIDATION_ERROR_MSG_FOR_Ensure_ACK2_Is_Set_Before_ACK3 = 'ACK3 details cannot be set without setting ACK2 status first',
                        VALIDATION_ERROR_MSG_FOR_Ensure_ACK1_Is_Set_Before_ACK2 = 'ACK2 details cannot be set without setting ACK1 status first',
                        VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK1_Receiption = 'Received Date cannot be a future date',
                        VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK2_Receiption = 'Received Date cannot be a future date',
                        VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK3_Receiption = 'Received Date cannot be a future date',
                        VALIDATION_ERROR_MSG_FOR_Prevent_Record_Edit_Once_Locked = Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD;

    /**
     * Given : A Submission History/Report Submission record
     * When : Acknowledgement details are entered without respecting the order (example : setting ACK2 details before setting ACK1)
     * Then : Validation fails
    */
    static testmethod void givenSubmissionRecord_WhenAcknowledgementDetailsAreEnteredOutOfOrder_ThenErrorIsThrown() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {
            Database.SaveResult saveResult;

            // ARRANGE: Get a submission record
            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            Test.startTest();

            // ACT: Setting ACK3 details before ACK2 status is set
            submission.ACK3_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK3_Received_Date__c = DateTime.now();
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> { SQX_Submission_History__c.ACK3_Status__c, SQX_Submission_History__c.ACK3_Received_Date__c })[0];

            // ASSERT: Error is thrown
            System.assertEquals(false, saveResult.isSuccess(), 'Expected update to fail');
            System.assert(SQX_Utilities.checkErrorMessage(saveResult.getErrors(), VALIDATION_ERROR_MSG_FOR_Ensure_ACK2_Is_Set_Before_ACK3), 'Expected error message not found. Instead found ' + saveResult.getErrors());

            // ACT: Setting ACK2 details before ACK1 status is set
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Received_Date__c = DateTime.now();
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> { SQX_Submission_History__c.ACK2_Status__c, SQX_Submission_History__c.ACK2_Received_Date__c })[0];

            // ASSERT: Error is thrown
            System.assertEquals(false, saveResult.isSuccess(), 'Expected update to fail');
            System.assert(SQX_Utilities.checkErrorMessage(saveResult.getErrors(), VALIDATION_ERROR_MSG_FOR_Ensure_ACK1_Is_Set_Before_ACK2), 'Expected error message not found. Instead found ' + saveResult.getErrors());

            // ACT: Setting ACK1 status
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK1_Received_Date__c = DateTime.now();
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> { SQX_Submission_History__c.ACK1_Status__c, SQX_Submission_History__c.ACK1_Received_Date__c })[0];

            Test.stopTest();
            // ASSERT: Update should pass after setting first acknowledgement
            System.assertEquals(true, saveResult.isSuccess(), 'Expected update to pass but got errors : ' + saveResult.getErrors());
        }
    }

    /**
     * Given : A Submission History/Report Submission record
     * When : Future date is set as Received Date in any of the acknowledgements
     * Then : Validation fails
    */
    static testmethod void givenSubmissionRecord_WhenAFutureDateIsSetAsAcknowledgementReceivedDate_ThenErrorIsThrown() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {
            Database.SaveResult saveResult;

            // ARRANGE: Get a submission record
            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            Test.startTest();

            // ACT: Setting future date in all three acknowledgements
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK1_Received_Date__c = DateTime.now().addMinutes(1);
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Received_Date__c = DateTime.now().addHours(1);
            submission.ACK3_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK3_Received_Date__c = DateTime.now().addDays(1);
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {})[0];

            Test.stopTest();


            // ASSERT: Error is thrown
            System.assertEquals(false, saveResult.isSuccess(), 'Expected update to fail');
            System.assertEquals(3, saveResult.getErrors().size(), 'Expected exactly 3 validation error messages');

            for(Database.Error error : saveResult.getErrors()) {
                if(!error.getMessage().equals(VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK1_Receiption) ||
                      !error.getMessage().equals(VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK2_Receiption) ||
                      !error.getMessage().equals(VALIDATION_ERROR_MSG_FOR_Prevent_Future_Date_For_ACK3_Receiption) ) {
                    System.assert(false, 'Unexpected error message ' + error.getMessage());
                }
            }

        }
    }


    /**
     * Given: A locked Submission History/Report Submission record
     * When: User tires to update the locked record
     * Then: Validation fails
    */
    static testmethod void givenSubmissionRecord_WhenUserTriesToEditLockedSubmissionRecord_ThenErrorIsThrown() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {
            Database.SaveResult saveResult;

            // ARRANGE: Get a submission record and lock it
            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            Test.startTest();

            // locking the record
            submission.Is_Locked__c = true;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> { SQX_Submission_History__c.Is_Locked__c });


            // ACT : Editing the record after locking
            submission.Message_ID__c = '12345';
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> { SQX_Submission_History__c.Message_ID__c })[0];

            Test.stopTest();

            // ASSERT: Error is thrown
            System.assertEquals(false, saveResult.isSuccess(), 'Expected update to fail');
            System.assert(SQX_Utilities.checkErrorMessage(saveResult.getErrors(), 'Record Status does not support the Action Performed.'), 'Expected error message not found. Instead found ' + saveResult.getErrors());

        }
    }


    // TESTING AUTOMATED PROCESSES


    /**
     * Given : A Submission History/Report Submission record
     * When : The final acknowledgement passes
     * Then : Record Status is set as Complete and is locked
    */
    static testmethod void givenSubmissionRecord_WhenFinalAcknowledgementIsSetAsPassed_ThenRecordStatusIsSetToCompleteAndIsLocked() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {

            // ARRANGE: Get a submission record
            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            Test.startTest();

            // ACT: Setting all successful acknowledgements
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK3_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {});

            Test.stopTest();

            // ASSERT: Record status is complete
            submission = [SELECT Status__c, Is_Locked__c FROM SQX_Submission_History__c WHERE Id =: submission.Id];

            System.assertEquals(SQX_Submission_History.STATUS_COMPLETE, submission.Status__c);

            // ASSERT: Record is locked for editing
            System.assertEquals(true, submission.Is_Locked__c, 'Expected record to be locked for editing after completion');

        }
    }


    /**
     * Given : A Submission History/Report Submission record
     * When : An acknowledgement fails
     * Then : Record Status is set as Aborted and is locked
    */
    static testmethod void givenSubmissionRecord_WhenAnAcknowledgementIsSetAsFailed_ThenRecordStatusIsSetAccordingly() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {

            // ARRANGE: Get a submission record
            SQX_Regulatory_Report__c report = [SELECT Id FROM SQX_Regulatory_Report__c LIMIT 1];

            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            submission.SQX_Regulatory_Report__c = [SELECT Id FROM SQX_Regulatory_Report__c LIMIT 1].Id;
            update submission;

            report.SQX_Submission_History__c = submission.Id;
            update report;

            Test.startTest();

            // ACT: Setting failed acknowledgement
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_FAILED;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {});

            // ASSERT: Record status is set as Transmission failure
            submission = [SELECT Status__c FROM SQX_Submission_History__c WHERE Id =: submission.Id];

            System.assertEquals(SQX_Submission_History.STATUS_TRANSMISSION_FAILURE, submission.Status__c);

            // ACT: Setting failed acknowledgement
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_FAILED;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {});

            // ASSERT: Record status is set as Transmission failure
            submission = [SELECT Status__c FROM SQX_Submission_History__c WHERE Id =: submission.Id];

            System.assertEquals(SQX_Submission_History.STATUS_TRANSMISSION_FAILURE, submission.Status__c);

            // ACT: Setting failed acknowledgement
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK3_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_FAILED;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {});

            // ASSERT: Record status is set as Submission failure and record is locked as well
            submission = [SELECT Status__c, Is_Locked__c FROM SQX_Submission_History__c WHERE Id =: submission.Id];

            System.assertEquals(SQX_Submission_History.STATUS_SUBMISSION_FAILURE, submission.Status__c);
            System.assertEquals(true, submission.Is_Locked__c);

            Test.stopTest();
        }
    }

    /**
     *  GIVEN : Report Submission record whose link is present in the Regulatory Report
     *  WHEN : When the submission completes
     *  THEN : The related report is complete
     */
    static testmethod void givenSubmissionRecordLinkedWithAReport_WhenTheSubmissionCompletes_ThenTheRelatedReportIsComplete() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {

            // ARRANGE: Get a submission record
            SQX_Submission_History__c submission = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];
            submission.SQX_Regulatory_Report__c = [SELECT Id FROM SQX_Regulatory_Report__c LIMIT 1].Id;
            update submission;

            SQX_Regulatory_Report__c report = [SELECT Id FROM SQX_Regulatory_Report__c LIMIT 1];
            report.SQX_Submission_History__c = submission.Id;
            update report;

            Test.startTest();

            // ACT: Setting failed acknowledgement
            submission.ACK1_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK2_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            submission.ACK3_Status__c = SQX_Submission_History.ACKNOWLEDGEMENT_STATUS_PASSED;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> { submission }, new List<SObjectField> {});

            // ASSERT: Record status is set as Submission failure and record is locked as well
            System.assertEquals(SQX_Regulatory_Report.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Regulatory_Report__c WHERE Id =: submission.SQX_Regulatory_Report__c].Status__c);

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A locked Medwatch record
     * WHEN: The record or any of it's child records are edited
     * THEN: Validation error is thrown
    */
    static testmethod void givenAMedwatchRecord_WhenTheRecordIsSubmitted_ThenTheRecordAlongWithItsChildRecordsShouldBeLockedForEditing() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(standardUser) {
            List<Database.SaveResult> saveResults;

            // Arrange : A medwatch record
            Test.setCurrentPage(Page.SQX_Medwatch_Editor);

            SQX_Complaint__c complaint = [SELECT Id FROM SQX_Complaint__c LIMIT 1];

            SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c(
                SQX_Complaint__c = complaint.Id,
                Status__c = SQX_Regulatory_Report.STATUS_PENDING,
                Name = 'My Report',
                Due_Date__c = Date.today().addDays(10)
            );
            insert report;

            SQX_MedWatch__c medwatch = new SQX_MedWatch__c(
                SQX_Regulatory_Report__c = report.Id
            );
            SQX_Extension_Medwatch medwatchExt = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatch));

            Test.startTest();

            medwatchExt.save();

            // submitting record
            SQX_Submission_History__c submission = new SQX_Submission_History__c(
                SQX_Regulatory_Report__c = medwatch.SQX_Regulatory_Report__c,
                SQX_Complaint__c = complaint.Id,
                Name = '5-day MDR',
                Submitted_Date__c = Date.today(),
                Submitted_By__c = 'Some submitter',
                Status__c = SQX_Submission_History.STATUS_PENDING
            );
            insert submission;

            report.SQX_Submission_History__c = submission.Id;
            update report;

            // ACT: Editing the child records as well as the main record
            saveResults = new SQX_DB().continueOnError().op_update(
                new Map<SObjectType, List<SObject>> {
                    SQX_Medwatch__c.SObjectType => new SObject[] { new SQX_Medwatch__c(Id = medwatch.Id) },
                    SQX_MedWatch_Suspect_Product__c.SObjectType => new SObject[] { new SQX_MedWatch_Suspect_Product__c(Id = medwatchExt.Suspects[0].Id) },
                    SQX_Medwatch_D11_Medical_Product__c.SObjectType => new SObject[] { new SQX_Medwatch_D11_Medical_Product__c(Id = medwatchExt.D11MedicalProducts[0].Id) },
                    SQX_Medwatch_F10_Device_Code__c.SObjectType => new SObject[] { new SQX_Medwatch_F10_Device_Code__c(Id = medwatchExt.F10DeviceCodes[0].Id) },
                    SQX_Medwatch_F10_Patient_Code__c.SObjectType => new SObject[] { new SQX_Medwatch_F10_Patient_Code__c(Id = medwatchExt.F10PatientCodes[0].Id) },
                    SQX_Medwatch_H6_Patient_Code__c.SObjectType => new SObject[] { new SQX_Medwatch_H6_Patient_Code__c(Id = medwatchExt.H6PatientCodes[0].Id) },
                    SQX_Medwatch_H6_Device_Code__c.SObjectType => new SObject[] { new SQX_Medwatch_H6_Device_Code__c(Id = medwatchExt.H6DeviceCodes[0].Id) },
                    SQX_Medwatch_H6_Method__c.SObjectType => new SObject[] { new SQX_Medwatch_H6_Method__c(Id = medwatchExt.H6MethodCodes[0].Id) },
                    SQX_Medwatch_H6_Result__c.SObjectType => new SObject[] { new SQX_Medwatch_H6_Result__c(Id = medwatchExt.H6ResultCodes[0].Id) },
                    SQX_Medwatch_H6_Conclusion__c.SObjectType => new SObject[] { new SQX_Medwatch_H6_Conclusion__c(Id = medwatchExt.H6ConclusionCodes[0].Id) }
                },
                new Map<SObjectType, List<SObjectField>> {
                    SQX_Medwatch__c.SObjectType => new List<SObjectField>(),
                    SQX_MedWatch_Suspect_Product__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_D11_Medical_Product__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_F10_Device_Code__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_F10_Patient_Code__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_H6_Patient_Code__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_H6_Device_Code__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_H6_Method__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_H6_Result__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_H6_Conclusion__c.SObjectType => new List<SObjectField>()
                }
            );

            // ASSERT: Error is thrown for every record update
            for(Database.SaveResult result : saveResults) {
                System.assertEquals(false, result.isSuccess(), 'Expected update to fail because of lock ' + (result.getId() != null ? result.getId().getSObjectType() + '' : ''));
                System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD), 'Expected ' + Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD + ' as error message but found ' + result.getErrors());
            }

            // Editing null values records
            saveResults = new SQX_DB().continueOnError().op_update(
                new Map<SObjectType, List<SObject>> {
                    SQX_Medwatch_Null_Value__c.SObjectType => new SObject[] { new SQX_Medwatch_Null_Value__c(Id = medwatchExt.NullValues.Id) },
                    SQX_Medwatch_Suspect_Product_Null_Value__c.SObjectType => new SObject[] { new SQX_Medwatch_Suspect_Product_Null_Value__c(Id = medwatchExt.Suspects[0].SQX_Suspect_Product_Null_Value__c) },
                    SQX_Medwatch_D11_Medical_Prod_Null_Value__c.SObjectType => new SObject[] { new SQX_Medwatch_D11_Medical_Prod_Null_Value__c(Id = medwatchExt.D11MedicalProducts[0].SQX_D11_Medical_Product_Null_Value__c) }
                }, new Map<SObjectType, List<SObjectField>> {
                    SQX_Medwatch_Null_Value__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_Suspect_Product_Null_Value__c.SObjectType => new List<SObjectField>(),
                    SQX_Medwatch_D11_Medical_Prod_Null_Value__c.SObjectType => new List<SObjectField>()
                }
            );

            // ASSERT: Error is thrown for every record update
            for(Database.SaveResult result : saveResults) {
                System.assertEquals(false, result.isSuccess(), 'Expected update to fail for because of lock!!');
                System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD), 'Expected ' + Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD + ' as error message but found ' + result.getErrors());
            }

            // ACT: Link new content with the locked Medwatch record
            ContentVersion cv = new ContentVersion(
                PathOnClient = 'Test.txt',
                VersionData = Blob.valueOf('Test')
            );
            insert cv;

            ContentDocumentLink link = new ContentDocumentLink(
                ContentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId,
                LinkedEntityId = medwatch.Id,
                ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED
            );

            saveResults = new SQX_DB().continueOnError().op_insert(new List<ContentDocumentLink> { link }, new List<SObjectField> {});

            // ASSERT: Error is thrown
            System.assertEquals(false, saveResults[0].isSuccess(), 'Expected update to fail');
            System.assert(SQX_Utilities.checkErrorMessage(saveResults[0].getErrors(), Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD), 'Expected error message not found. Instead found ' + saveResults[0].getErrors());

            Test.stopTest();
        }
    }
}