/**
* tests SQX_Extension_Controlled_Document.getDocRevisions() using showAllRevisions values
* tests SQX_Controller_Document_Revision_History.getDocRevisions() using showAllRevisions values
*/
@isTest
public class SQX_Test_2356_ControlledDoc_RevHistory {
    @testSetup
    public static void commonSetup() {
        // add required users
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensures revision history list of a controlled document
    */
    public static testmethod void givenGetDocumentRevisions() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('user1')) {
            /* Arranged doc revs:
                Rev Date_Issued__c  Status
                1   Today() - 5     Obsolete
                2                   Draft
                3   Today() - 3     Current
                4   Today() - 2     Pre-Release
                5                   Approved
            */
            // add required controlled document revisions
            SQX_Test_Controlled_Document obsoleteRev = new SQX_Test_Controlled_Document().save();
            obsoleteRev.doc.Date_Issued__c = System.today() - 5;
            obsoleteRev.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            SQX_Test_Controlled_Document draftRev = obsoleteRev.revise();
            draftRev.doc.Revision__c = 'RANDOM-2';
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            draftRev.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            SQX_Test_Controlled_Document currentRev = draftRev.revise();
            currentRev.doc.Date_Issued__c = System.today() - 3;
            currentRev.doc.Revision__c = 'RANDOM-5';
            currentRev.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            currentRev.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            Test.startTest();
            
            SQX_Test_Controlled_Document preReleaseRev = currentRev.revise();
            preReleaseRev.doc.Date_Issued__c = System.today() - 2;
            preReleaseRev.doc.Revision__c = 'RANDOM-6';
            preReleaseRev.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            SQX_Test_Controlled_Document approvedRev = preReleaseRev.revise();
            approvedRev.doc.Auto_Release__c = false;
            approvedRev.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();
            
            
            // ACT : get default document revision list for preReleaseRev
            Map<String, List<SQX_Controlled_Document__c>> revsMap = new Map<String, List<SQX_Controlled_Document__c>>();
            // SQX-2356
            SQX_Extension_Controlled_Document currentRevExt = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(currentRev.doc));
            revsMap.put('SQX-2356', currentRevExt.getDocRevisions());
            // SQX-5614
            SQX_Controller_Document_Revision_History revCntlr = new SQX_Controller_Document_Revision_History();
            revCntlr.mainRecord = currentRev.doc;
            revsMap.put('SQX-5614', revCntlr.getDocRevisions());
            
            // ASSERT : default revision history should only list prior released docs as per Date Issued of currentRev
            for (String jiraNum : revsMap.keySet()) {
                List<SQX_Controlled_Document__c> defaultRevs = revsMap.get(jiraNum);
                
                System.assertEquals(1, defaultRevs.size(), jiraNum + ': ' + defaultRevs);
                System.assertEquals(obsoleteRev.doc.Id, defaultRevs[0].Id, jiraNum + ' Default Revision History ' + defaultRevs + ' for ' + currentRev.doc);
            }
            
            
            revsMap.clear();
            
            // ACT: get document revision list with showAllRevisions as true for preReleaseRev
            // SQX-2356
            currentRevExt.showAllRevisions = true;
            revsMap.put('SQX-2356', currentRevExt.getDocRevisions());
            // SQX-5614
            revCntlr.showAllRevisions = true;
            revsMap.put('SQX-5614', revCntlr.getDocRevisions());
            
            // ASSERT : revision history with showAllRevisions as true should list all revs ordered by Data Issued desc and Rev desc
            for (String jiraNum : revsMap.keySet()) {
                List<SQX_Controlled_Document__c> allRevs = revsMap.get(jiraNum);
                
                System.assertEquals(5, allRevs.size(), jiraNum + ': all revisions are expected.' + allRevs);
                System.assertEquals(approvedRev.doc.Id, allRevs[0].Id, jiraNum + ': 1st item in list containing all revisions not in order. ' + allRevs);
                System.assertEquals(draftRev.doc.Id, allRevs[1].Id, jiraNum + ': 2nd item in list containing all revisions not in order. ' + allRevs);
                System.assertEquals(preReleaseRev.doc.Id, allRevs[2].Id, jiraNum + ': 3rd item in list containing all revisions not in order. ' + allRevs);
                System.assertEquals(currentRev.doc.Id, allRevs[3].Id, jiraNum + ': 4th item in list containing all revisions not in order. ' + allRevs);
                System.assertEquals(obsoleteRev.doc.Id, allRevs[4].Id, jiraNum + ': 5th item in list containing all revisions not in order. ' + allRevs);
            }
            
            Test.stopTest();
        }
    }
    
    /**
    * ensures revision history list sort order
    */
    public static testmethod void givenGetDocumentRevisions_ProperSortedList() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('user1')) {
            /* Arranged doc revs:
                Doc#    Rev     Date_Issued__c  Status
                d1      (null)  Today()         Pre-Release
                d1      2       Today()         Pre-Release
                d1      3                       Draft
                d2      (null)                  Draft
                d2      2                       Draft
            */
            // add required controlled document revisions
            SQX_Test_Controlled_Document d1_rev1null = new SQX_Test_Controlled_Document();
            d1_rev1null.doc.Revision__c = null;
            d1_rev1null.save();
            d1_rev1null.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            SQX_Test_Controlled_Document d1_rev2 = d1_rev1null.revise();
            d1_rev2.doc.Revision__c = '2';
            d1_rev2.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            SQX_Test_Controlled_Document d1_rev3 = d1_rev2.revise(); // revise sets Revision__c as 3
            
            // ACT: get default document revision list for d1_rev3
            SQX_Extension_Controlled_Document ext_d1_rev3 = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(d1_rev3.doc));
            List<SQX_Controlled_Document__c> revs = ext_d1_rev3.getDocRevisions();
            
            System.assertEquals(2, revs.size(), revs);
            System.assertEquals(d1_rev2.doc.Id, revs[0].Id, '1st item in default revisions list not in order. ' + revs + ' for ' + d1_rev3.doc);
            System.assertEquals(d1_rev1null.doc.Id, revs[1].Id, '2nd item in default revisions list not in order. ' + revs + ' for ' + d1_rev3.doc);
            
            // ACT: get document revision list with showAllRevisions as true for d1_rev3
            ext_d1_rev3.showAllRevisions = true;
            revs = ext_d1_rev3.getDocRevisions();
            
            System.assertEquals(3, revs.size());
            System.assertEquals(d1_rev3.doc.Id, revs[0].Id, '1st item in list containing all revisions not in order. ' + revs);
            System.assertEquals(d1_rev2.doc.Id, revs[1].Id, '2nd item in list containing all revisions not in order. ' + revs);
            System.assertEquals(d1_rev1null.doc.Id, revs[2].Id, '3rd item in list containing all revisions not in order. ' + revs);
            
            
            Test.startTest();
            
            SQX_Test_Controlled_Document d2_rev1null = new SQX_Test_Controlled_Document();
            d2_rev1null.doc.Revision__c = null;
            d2_rev1null.save();
            
           // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            d2_rev1null.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            d2_rev1null.save();            
            
            SQX_Test_Controlled_Document d2_rev2 = d2_rev1null.revise();
            d2_rev2.doc.Revision__c = '2';
            d2_rev2.save();
            
            // ACT: get default document revision list for d2_rev1null
            SQX_Extension_Controlled_Document ext_d2_rev1null = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(d2_rev1null.doc));
            revs = ext_d2_rev1null.getDocRevisions();
            
            System.assertEquals(0, revs.size(), revs);
            
            // ACT: get document revision list with showAllRevisions as true for d1_rev3
            ext_d2_rev1null.showAllRevisions = true;
            revs = ext_d2_rev1null.getDocRevisions();
            
            System.assertEquals(2, revs.size());
            System.assertEquals(d2_rev2.doc.Id, revs[0].Id, '1st item in list containing all revisions not in order. ' + revs);
            System.assertEquals(d2_rev1null.doc.Id, revs[1].Id, '2nd item in list containing all revisions not in order. ' + revs);
            
            Test.stopTest();
        }
    }
}