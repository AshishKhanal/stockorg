/**
* This test ensures that The Expiration date should be updated when the document is Obsolete
*
*/
@IsTest
public class SQX_Test_1285_UpdateExpireDate{

    // when Document status is Obsolete and Expire Date is specified
    public static testmethod void givenDocumentIsObsolete_ExpireDateIsNotUpdated(){

        //Arrange: Setup the program, and set the audit as closed.
        SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document().save();
        testDoc.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE);
        testDoc.doc.Expiration_Date__c = Date.TODAY() +1; //expire date set today + 1 day
        testDoc.save();

         //Act: Attempt to select above Document
        SQX_Controlled_Document__c retriveDoc = new SQX_Controlled_Document__c();
        retriveDoc = [SELECT Expiration_Date__c FROM SQX_Controlled_Document__c WHERE Id = :testDoc.doc.Id];
        

        //Assert: Expire date Should not be Today date
        System.assertEquals(Date.TODAY() + 1, retriveDoc.Expiration_Date__c, 'Expire date should not be today date ' + (Date.TODAY() + 1));                                          
 
    }

}