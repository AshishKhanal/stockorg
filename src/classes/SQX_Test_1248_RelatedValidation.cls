/**
*/
@IsTest
public class SQX_Test_1248_RelatedValidation {

    private static boolean runAllTests = true,
                           run_givenDocIsInApprovalOrObsolete_NoNewRelationCanBeAddedOrModified = false,
                           run_givenDocSelfReferences_ItFails = false,
                           run_givenDocDuplicatesReference_ItFails = false;

    /**
    * this rule ensures that once a document is sent for approval or is obsolete, no new relation can be added.
    * and no exsiting rule can be modifed
    */
    public static testmethod void givenDocIsInApprovalOrObsolete_NoNewRelationCanBeAddedOrModified(){
        if(!runAllTests && !run_givenDocIsInApprovalOrObsolete_NoNewRelationCanBeAddedOrModified)
            return;
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
            //Arrange: create controlled document A.1, B.1 and C.1 which will be related.
            //         A.1 -> Current
            //         B.1 -> In Approval
            //         C.1 -> Obsolete
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save(),
                                         b1 = new SQX_Test_Controlled_Document().save(),
                                         c1 = new SQX_Test_Controlled_Document().save();

            a1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            c1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();

            b1.doc.First_Approver__c = standardUser.Id;
            b1.doc.Approval_Status__c =  SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            b1.save();
            
            
            //Act: try to add document relation between the two elements
            Database.SaveResult savea1_b1 = a1.addRelationTo(b1),
                                saveb1_a1 = b1.addRelationTo(a1),
                                savec1_a1 = c1.addRelationTo(a1),
                                savea1_c1 = a1.addRelationTo(c1);
                                
            //Assert: Ensure that a1 -> b1 was successfully added
            //                    b1 -> a1 failed
            //                    c1 -> a1 failed
            //                    a1 -> c1 succeeded
            System.assertEquals(true, savea1_b1.isSuccess());
            System.assertEquals(false, saveb1_a1.isSuccess());
            System.assertEquals(false, savec1_a1.isSuccess());
            System.assertEquals(true, savea1_b1.isSuccess());
            
        }
    }
    
    /**
    * This test ensures that if a document tries to add a self reference it doesn't succeed.
    */
    public static testmethod void givenDocSelfReferences_ItFails(){
        if(!runAllTests && !run_givenDocSelfReferences_ItFails)
            return;
            
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
            //Arrange: create controlled document A.1, 
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save();

            a1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
                                         
            
            //Act: try to add document relation between the same doc
            Database.SaveResult savea1_a1 = a1.addRelationTo(a1);
            
            //Assert: that the record didn't save
            System.assertEquals(false, savea1_a1.isSuccess());
            
        }
        
    }
    
    /**
    * This test ensures that if a duplicate relation is added an error is thrown
    */
    public static testmethod void givenDocDuplicatesReference_ItFails(){
        if(!runAllTests && !run_givenDocSelfReferences_ItFails)
            return;
            
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
            //Arrange: create controlled document A.1, B.1 and a relation from a1 -> b1
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save(),
                                         b1 = new SQX_Test_Controlled_Document().save(),
                                         c1 = new SQX_Test_Controlled_Document().save();

            a1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            b1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            c1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
               
                                     
            a1.addRelationTo(b1);
            Database.SaveResult a1ToC1 = a1.addRelationTo(c1);
            
            //Act: Attempt to add the relation again
            Database.SaveResult saveResult = a1.addRelationTo(b1);
            
            //Assert: that the duplicate relation wasn't saved
            System.assertEquals(false, saveResult.isSuccess());
            
            //Act: try changing the existing relation to duplicate
            SQX_Related_Document__c updateRel = new SQX_Related_Document__c(Id = a1ToC1.getId(),
                Controlled_Document__c = a1.doc.Id,
                Referenced_Document__c = b1.doc.Id
            );
            
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Related_Document__c> {updateRel },
                                    new List<Schema.SObjectField>{
                                            Schema.SQX_Related_Document__c.Referenced_Document__c
                                    }).get(0);
                                    
            //Assert: the update failed
            System.assertEquals(false, saveResult.isSuccess());
            
        
        }
    }
}