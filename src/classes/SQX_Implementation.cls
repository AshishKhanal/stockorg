/**
* This class contains the common logic for handling Implementation class of change order.
* ---------------------------------------------------------------------------------------
* | NOTE: Change Object has the API name as SQX_Implementation__c. So the code present  |
* | in this class is for the change object not IMPLEMENTATION object.                   |
* ---------------------------------------------------------------------------------------
*
*/
public with sharing class SQX_Implementation {
	
	public static final string 	STATUS_OPEN = 'Open',
								STATUS_COMPLETE = 'Complete',
                                STATUS_SKIPPED = 'Skipped',
                                STATUS_READY = 'Ready';
								
								
	/** CQ Aware types. Naming Convention: AWARE_<Object Type>_<Action> **/
    public static final String 	AWARE_DOCUMENT_NEW = 'New Document',
    							AWARE_DOCUMENT_REVISE = 'Revise Document',
								AWARE_DOCUMENT_OBSOLETE = 'Obsolete Document';
								
	public static final String	RECORD_TYPE_ACTION = 'Action',
								RECORD_TYPE_PLAN = 'Plan';



    // this method is used to open the implementation when the change order is approved
    public static void approveAllActionsForChange(Set<Id> changeIds){
        List<SQX_Implementation__c> actions = getActionsForChange(changeIds),
                            actionsToUpdate = new List<SQX_Implementation__c>();
        
        lockAllActionsForChange(false, actions);
        Id  planRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, RECORD_TYPE_PLAN),
            actionRecordTypeId = SQX_Utilities.getRecordTypeIdByDevelopernameFor(SQX.Implementation, RECORD_TYPE_ACTION);
        
        for(SQX_Implementation__c action : actions){
            if(action.RecordTypeId == planRecordTypeId){
                action.RecordTypeId = actionRecordTypeId;
                action.Status__c = STATUS_OPEN;
                action.Is_Approved__c = true;
                action.Is_Locked__c=true;
                if (action.SQX_User__c != null) {
                    // set assignee as owner when approved
                    action.OwnerId = action.SQX_User__c;
                }
                actionsToUpdate.add(action);
            }
            else if(action.Status__c == SQX_Implementation.STATUS_SKIPPED && action.Is_Skipped_Approved__c !=true){
                action.Is_Skipped_Approved__c = true;
                if (action.SQX_User__c != null) {
                    // set assignee as owner when approved
                    action.OwnerId = action.SQX_User__c;
                }
                actionsToUpdate.add(action);    
            }
            else if (action.SQX_User__c != null && action.OwnerId != action.SQX_User__c) {
                // set assignee as owner when approved
                action.OwnerId = action.SQX_User__c;
                actionsToUpdate.add(action);
            }
        }
        
        // without sharing is used because when the change order is approved, status of the implemenation are also changed
        // but the user who approved the change order may not have access to the implemenatation
        new SQX_DB().withoutSharing()
                    .op_update(actionsToUpdate, new List<SObjectField> { 
                        SQX_Implementation__c.Status__c,
                        SQX_Implementation__c.Is_Approved__c,
                        SQX_Implementation__c.RecordTypeId });
    }
    

    // this method is used for locking all the actions when the change order is send for approval
    public static void lockAllActionsForChange(Boolean lock, Set<Id> parentIds){
    	lockAllActionsForChange(lock, getActionsForChange(parentIds));
    }
    
    /**
    * this method is used for locking all the actions when the change order is send for approval
    */
    private static void lockAllActionsForChange(Boolean lock, List<SQX_Implementation__c> actions){
    	
    	/**
    	* WITHOUT SHARING used:
    	* ------------------------
    	* Note: without sharing has been used because not all implementations may be owned by the user who is sending the record for approval
    	*      or not all implementations may be owned by the user who is approving.
    	* 		
    	* Once approved, the records must be unlocked for editing. While approving they must be locked.
    	* Example: In case of Change Order when a change order is sent for approval, all related implementations must be locked so that
    	*		   approver can get a consistent view of what ever he/she is approving.
    	*		   Once approver approves the record he/she will unlock the implementations for further processing i.e. completing the implementation.
    	*/
    	SQX_Approval_Util approvalUtil = new SQX_Approval_Util().withSharing(false);
    	
    	if(lock){
    		approvalUtil.lock(actions);
    	}
    	else{
    		approvalUtil.unlock(actions);
    	}
    	
    }
    
    // list of implemenation to be changed
    private static List<SQX_Implementation__c> getActionsForChange(Set<Id> parentIds){
    	return [SELECT Id, 
    					Name, 
    					Status__c, 
    					Is_Approved__c, 
    					Is_Skipped_Approved__c,
                        SQX_User__c,
                        OwnerId,
    					RecordTypeId FROM SQX_Implementation__c WHERE SQX_Change_Order__c IN :parentIds];
    }   

    /**
    * This class handles the trigger events related to implementation object and performs various actiosn
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newActions equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Implementation__c> newActions, Map<Id,SQX_Implementation__c> oldMap){
        
            super(newActions, oldMap);
        }

        /**
         * change the assignee as owner whenever the assignee is changed implementation or complete
         * @date: 2016-02-22
         * @story: SQX-1933
         */
        public Bulkified syncActionOwner(){
            final String ACTION_NAME = 'syncActionOwner';
            Rule implementationToBeSynced = new Rule();
            Boolean trueVal = true;
            implementationToBeSynced.addRule(Schema.SQX_Implementation__c.Sync_Assignee_As_Owner__c, RuleOperator.Equals, (Object) trueVal);

            this.resetView()
                .applyFilter(implementationToBeSynced, RuleCheckMethod.OnCreateAndEveryEdit);

            for(SQX_Implementation__c implementation : (List<SQX_Implementation__c>) implementationToBeSynced.evaluationResult){
                if(implementation.SQX_User__c != null){
                    implementation.OwnerId = implementation.SQX_User__c; // checking if owner is not necessary
                }
                else if(implementation.SQX_User__c == null) {
                    implementation.SQX_User__c = implementation.OwnerId; // default the assignee to owner if no assignee has been created.
                                                                         // this will prevent issues with SF tasks not having an owner.
                }
                
            }

            return this;

        }
        
        /**
         * ensure that change record have edit access if not access than 
         * give error message "Insufficient privilege to create record of type Change"
         */
        public Bulkified ensureChangeRecordHaveEditAccess(){

            this.resetView();
            Set<Id> changeOrderIds = this.getIdsForField(SQX_Implementation__c.SQX_Change_order__c);
            Map<Id, SQX_Change_Order__c> coEditAccessMap = new Map<Id, SQX_Change_Order__c>([
                SELECT Id, UserRecordAccess.HasEditAccess FROM SQX_Change_Order__c WHERE Id IN: changeOrderIds
            ]);
            String coObjLabel = SQX_Change_Order__c.SObjectType.getDescribe().getLabel();
            for(SQX_Implementation__c imp : (List<SQX_Implementation__c>)this.view){
                if(!coEditAccessMap.get(imp.SQX_Change_order__c).UserRecordAccess.HasEditAccess){
                    imp.addError(Label.SQX_ERR_MSG_INSUFFICIENT_ACCESS_ON_OBJECT_CREATE.replace('{objectType}', coObjLabel));
                }
            }
            return this;
        }

        /**
         * This trigger rule sets the Change Order Field in Controlled Document when Implementation refers to the controlled document
         * as being revised or new document. It adds an error if a controlled doc already refers to a another CO.
         */
        public Bulkified setChangeOrderOnDocs(){
            final String ACTION_NAME = 'setChangeOrderOnDocs';

            Rule newDocumentIdHasChangedRule = new Rule();
            newDocumentIdHasChangedRule.addRule(SQX_Implementation__c.SQX_Controlled_Document_New__c, RuleOperator.HasChanged);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Implementation, ACTION_NAME)
                .applyFilter(newDocumentIdHasChangedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(newDocumentIdHasChangedRule.evaluationResult.size()  > 0){
                this.addToProcessedRecordsFor(SQX.Implementation, ACTION_NAME, newDocumentIdHasChangedRule.evaluationResult);

                // Get the old values for the controlled document. We will use the old values to detach any older reference
                Map<Id, SQX_Implementation__c> oldVals = (Map<Id, SQX_Implementation__c>)this.oldValues;
                Map<Id, SQX_Implementation__c> newVals = new Map<Id, SQX_Implementation__c>((List<SQX_Implementation__c>)newDocumentIdHasChangedRule.evaluationResult);

                // This map will help in attaching a controlled doc to change order
                Map<Id, SQX_Implementation__c> controlledDoc2Implementation = new Map<Id, SQX_Implementation__c>();
                
                Set<Id> controlledDocsToDetach = new Set<Id>();
                
                for(Id docId : newVals.keySet()){
                    SQX_Implementation__c currentVal = newVals.get(docId);


                    Id  docToDetach = oldVals.get(docId).SQX_Controlled_Document_New__c,
                        docToAttach = currentVal.SQX_Controlled_Document_New__c;
                    
                    // If old document link exists we will have to detach i.e. null the old value
                    if(docToDetach != null){
                        controlledDocsToDetach.add(docToDetach);
                    }
                    
                    // If new document link has been established we will have to update the new doc link
                    if(docToAttach != null){

                        // if the map already contains the refernce then it means that two implementations are referring to same
                        // controlled doc in the transaction add an error for all but the first implementation
                        if(controlledDoc2Implementation.containsKey(docToAttach)){
                        currentVal.addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_HAS_CHANGE_ORDER);
                        }
                        else{
                            controlledDoc2Implementation.put(docToAttach, currentVal);
                        }
                    }
                }
                
                // Get all the controlled documents that are to be updated.
                // REVIEWME: Do we have to consider the status of the controlled document such as obsolete ?
                List<SQX_Controlled_Document__c> controlledDocs = [SELECT Id, SQX_Change_Order__c 
                                                                   FROM SQX_Controlled_Document__c 
                                                                   WHERE Id IN : controlledDocsToDetach 
                                                                        OR Id IN : controlledDoc2Implementation.keySet()
                                                                   FOR UPDATE];
                

                for(SQX_Controlled_Document__c document : controlledDocs){
                    SQX_Implementation__c implementation = controlledDoc2Implementation.get(document.Id);
                    
                    // If the document doesn't belong to attach map and is present in detach map
                    if(implementation == null && controlledDocsToDetach.contains(document.Id)){
                        document.SQX_Change_Order__c = null; // Detach a change order
                    }
                    else if(implementation != null){

                        // if change order is already specified in the controlled document add an error
                        if(document.SQX_Change_Order__c != null && document.SQX_Change_Order__c != implementation.SQX_Change_Order__c){
                            implementation.addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_HAS_CHANGE_ORDER);
                        }
                        else{
                            // else attach the change order to the doc
                            document.SQX_Change_Order__c = implementation.SQX_Change_Order__c;
                        }
                    }
                }
                
                /**
                * WITHOUT SHARING used
                * -----------------------
                * The person updating the implementation may or may not have edit rights on the controlled document.
                * Usually it is the same person creating/revising a doc.
                * But even change order owner can complete the doc who may not have edit rights on the doc.
                */
                new SQX_DB().withoutSharing().op_update(controlledDocs, new List<SObjectField> { SQX_Controlled_Document__c.SQX_Change_Order__c });
                
                
            }
            
            
            
            
            return this;
        }

        /**
        *  Method prevent deletion of implementation when change order record is void or closed
        */
        public Bulkified preventDeletionOnClosureAndVoid(){
            this.resetView();
            List<SQX_Implementation__c> impList = (List<SQX_Implementation__c>) this.view;
            if(impList.size() > 0){
                for(SQX_Implementation__c record : impList) {
                    if((Boolean) record.Is_Parent_Locked__c || (Boolean) record.Is_Locked__c) {
                        record.addError(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE);
                    }
                }
            } 
            return this;
        }
        

        /**
         * add the permission to edit the action when the assigee is different than the main record owner
         * @date: 2016-02-22
         * @story: SQX-1933
         */
        public Bulkified shareRecordWithOwner(){
            final String ACTION_NAME = 'shareRecordWithOwner';
            Rule implementationWithActionRule = new Rule();
            
            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.OwnerId, RuleOperator.HasChanged);
            
            List<SQX_Implementation__c> implementationChanged = new List<SQX_Implementation__c>();

            this.resetView()
                .removeProcessedRecordsFor(SQX.Implementation, ACTION_NAME)
                .applyFilter(implementationWithActionRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            implementationChanged = (List<SQX_Implementation__c>) implementationWithActionRule.evaluationResult;

            if(implementationChanged.size() > 0){
                this.addToProcessedRecordsFor(SQX.Implementation, ACTION_NAME, implementationChanged);
                
                List<SQX_Implementation__Share> implementationToBeShared = new List<SQX_Implementation__Share>();

                for(SQX_Implementation__c action : implementationChanged){

                    if(action.OwnerId != action.Main_Record_Owner__c){

                        SQX_Implementation__Share share = new SQX_Implementation__Share(
                            AccessLevel = 'edit',
                            ParentId = action.Id,
                            UserOrGroupId = action.Main_Record_Owner__c
                        ); 

                        implementationToBeShared.add(share); //capa R/W access
                    }
                }

                //NOTE: Without sharing has been used because the implementaion can be completed by a user with only Read access 
                //on the change order. So, he/she won't be able to add any new sharing rules to the change order
                if(implementationToBeShared.size() > 0){

                    //continue on error has been used to prevent any sharing related issue from stopping the transaction
                    //this is considered none critical. sharing rule can always be added manually by the user.
                    new SQX_DB().withoutSharing().continueOnError().op_insert(implementationToBeShared, new List<SObjectField>{});
                }


            }

            return this;
        }
        
        // this method is used to complete the change order when all the actions are complete
        public Bulkified completeChangeOrderWhenAllActionsComplete(){
            final string ACTION_NAME = 'completeChangeOrderWhenAllActionsComplete';
            
            Id actionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, RECORD_TYPE_ACTION);
            Rule completeActionsRule = new Rule(),
                 skippedActionsRule = new Rule();
                 
            Rule [] rules = new Rule [] { completeActionsRule, skippedActionsRule };
                 
            completeActionsRule.addRule(SQX_Implementation__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);
            completeActionsRule.addRule(SQX_Implementation__c.RecordTypeId, RuleOperator.Equals, actionRecordTypeId);
            completeActionsRule.addRule(SQX_Implementation__c.SQX_Change_Order__c, RuleOperator.NotEquals, null);
            
            skippedActionsRule.addRule(SQX_Implementation__c.Status__c, RuleOperator.Equals, STATUS_SKIPPED);
            skippedActionsRule.addRule(SQX_Implementation__c.RecordTypeId, RuleOperator.Equals, actionRecordTypeId);
            skippedActionsRule.addRule(SQX_Implementation__c.SQX_Change_Order__c, RuleOperator.NotEquals, null);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Action, ACTION_NAME)
                .groupByRules(rules, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            Set<Id> changesToComplete = new Set<Id>();
            
            for(Rule evalRule : rules){
                if(evalRule.evaluationResult.size() > 0){
                    this.addToProcessedRecordsFor(SQX.ChangeOrder, ACTION_NAME, evalRule.evaluationResult);
                    
                    changesToComplete.addAll(getIdsForField(evalRule.evaluationResult, SQX_Implementation__c.SQX_Change_Order__c));
                }
            }
            
            SQX_Change_Order.checkForCompletion(changesToComplete);
            
            return this;
        }

        
        /**
        * Manage tasks based on the status of the action and its record type.
        * If action record type is Action task is created
        * @story SQX-1948
        */
        public Bulkified createSFTasks(){
            final String ACTION_NAME = 'createSFTasks';
            Id implementationActionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, RECORD_TYPE_ACTION);
            Rule implementationWithActionRule = new Rule();

            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.RecordTypeId, RuleOperator.equals, implementationActionRecordTypeId);
            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.Status__c, RuleOperator.equals, SQX_Implementation.STATUS_OPEN);

            this.resetView()
                .applyFilter(implementationWithActionRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            /*
            * Create new task for this task since the new requirement has been created.
            */
            if(implementationWithActionRule.evaluationResult.size() > 0){
                List<SQX_Implementation__c> actionTasks = (List<SQX_Implementation__c>) implementationWithActionRule.evaluationResult;
                Map<SQX_Implementation__c, Task> actionToTasks = new Map<SQX_Implementation__c, Task>();

                for(SQX_Implementation__c actionTask : actionTasks){
                    if(actionTask.TaskId__c == null){
                        Task newTask = SQX_Task_Template.createTaskFor(new SQX_Change_Order__c(Id = actionTask.SQX_Change_Order__c), 
                                                                actionTask.SQX_User__c, 
                                                                new Map<String, SObject> {'Task' => actionTask}, 
                                                                actionTask.Due_Date__c, 
                                                                SQX_Task_Template.TaskType.ActionOpened);
    
                        actionToTasks.put(actionTask, newTask);
                    }
                }

                if(actionToTasks.values().size() > 0){
                    new SQX_DB().op_insert(actionToTasks.values(), new List<Schema.SObjectField> {Task.Subject, Task.Status, Task.Description });

                    for(SQX_Implementation__c action : actionToTasks.keySet()){
                        action.TaskId__c = actionToTasks.get(action).Id;
                    }
                }

            }

            return this;
        }
        
        /**
        * Manage tasks based on the status of the action and its record type.
        * If action record type is Action task, task is updated as per their status
        * @story SQX-1948
        */
        public Bulkified updateSFTasks(){
            final String ACTION_NAME = 'updateSFTasks';
            Id implementationActionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, RECORD_TYPE_ACTION);
            Rule implementationWithActionRule = new Rule();

            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.RecordTypeId, RuleOperator.equals, implementationActionRecordTypeId);
            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.Status__c, RuleOperator.notEquals, STATUS_READY);
            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.Status__c, RuleOperator.notEquals, STATUS_OPEN);

            this.resetView()
                .removeProcessedRecordsFor(SQX.Action, ACTION_NAME)
                .applyFilter(implementationWithActionRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(implementationWithActionRule.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.Action, ACTION_NAME, implementationWithActionRule.evaluationResult);
                
                Set<Id> taskIdsToComplete = new Set<Id>();
                
                for(SQX_Implementation__c action : (List<SQX_Implementation__c>) implementationWithActionRule.evaluationResult){
                    
                    if((action.Status__c == STATUS_COMPLETE || action.Status__c == STATUS_SKIPPED) && 
                       action.TaskId__c != null){
                           taskIdsToComplete.add(action.TaskId__c);
                       }
                }
                
                if(taskIdsToComplete.size() > 0){
                    List<Task> tasksToComplete = [SELECT Id, Status, WhatId FROM Task WHERE Id IN: taskIdsToComplete AND Status != : SQX_Task.STATUS_COMPLETED FOR UPDATE];
                    
                    for(Task task : tasksToComplete){
                        task.Status = SQX_Task.STATUS_COMPLETED;
                    }
                    
                    /*
                     * WITHOUT SHARING used
                     * -----------------------------
                     * Task are assigned to specific user, but the implementation may be completed by some other user so we need to escalate the privilege
                     */
                    new SQX_DB().withoutSharing().op_update(tasksToComplete, new List<SObjectField> {Task.Status});
                }

            }

            return this;

        }

        /**
        * Manage tasks based on the change of the action tasks of CO
        * If due date and assignee are changed, related SF tasks are updated
        * @story SQX-1948
        */
        public Bulkified syncSFTasks(){
            final String ACTION_NAME = 'syncSFTasks';
            Id implementationActionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, RECORD_TYPE_ACTION);
            Rule implementationWithActionRule = new Rule();

            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.SQX_User__c, RuleOperator.HasChanged);
            implementationWithActionRule.addRule(Schema.SQX_Implementation__c.Due_Date__c, RuleOperator.HasChanged);
            implementationWithActionRule.operator = RuleCombinationOperator.OpOr;
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Action, ACTION_NAME)
                .applyFilter(implementationWithActionRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(implementationWithActionRule.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.Action, ACTION_NAME, implementationWithActionRule.evaluationResult);
                
                Set<Id> taskforUpdate = new Set<Id>();
                List<SQX_Implementation__c> changedActions =  new List<SQX_Implementation__c>();
                
                for(SQX_Implementation__c action : (List<SQX_Implementation__c>) implementationWithActionRule.evaluationResult){
                    if(action.RecordTypeId == implementationActionRecordTypeId){
                           taskforUpdate.add(action.TaskId__c);
                           changedActions.add(action);
                       }
                }
                
                if(taskforUpdate.size() > 0){
                    List<Task> tasksToComplete = [SELECT Id, ActivityDate, OwnerId, Status, WhatId FROM Task WHERE Id IN: taskforUpdate AND Status != : SQX_Task.STATUS_COMPLETED FOR UPDATE];
                    
                    for(SQX_Implementation__c changedAction : changedActions){
                        for(Task task : tasksToComplete){
                            if(changedAction.TaskId__c == task.Id){
                                task.ActivityDate = changedAction.Due_Date__c;
                                task.OwnerId = changedAction.SQX_User__c;
                            }
                        }
                    
                    }
                    /*
                     * WITHOUT SHARING used
                     * -----------------------------
                     * Task are assigned to specific user, but the implementation may be changed by some other user so we need to escalate the privilege
                     */
                    new SQX_DB().withoutSharing().op_update(tasksToComplete, new List<SObjectField> {Task.ActivityDate, Task.OwnerId});
                }

            }

            return this;

        }

    }   
}