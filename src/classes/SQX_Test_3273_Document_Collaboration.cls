/**
*   @description : Test class for the specific story [ prevent deletion of collaboration content ]
*   @author : Piyush Subedi
*   @story : SQX-3273
*/

@isTest
public class SQX_Test_3273_Document_Collaboration{

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');

        System.runAs(standardUser){

            /* Creating a controlled document */
            SQX_Test_Controlled_Document tstDoc = new SQX_Test_Controlled_Document();
            tstDoc.doc.Title__c = 'Doc1';
            tstDoc.save(true);

            /* Initiate collaboration */
            SQX_Extension_Controlled_Doc_Collaborate ext = new SQX_Extension_Controlled_Doc_Collaborate(new ApexPages.StandardController(tstDoc.doc));
            ext.CollaborationTitle='Test Group';
            ext.UserIDs = new List<String> { adminUser.Id };
            ext.saveAndInitiateCollaboration();

        }
    }

    /**
    *   Given : A controlled document collaboration group content
    *   When : User tries to delete the content
    *   Then : System prevents the deletion
    */
    static testmethod void givenControlledDocCollaborationGroupContent_WhenUserAttemptsToDeleteTheContent_ThenSystemPreventsDeletion(){

        // Arrange : Get the collaboration group content
        SQX_Controlled_Document__c doc = [SELECT Collaboration_Group_Id__c,
                                                 Collaboration_Group_Content_Id__c FROM SQX_Controlled_Document__c WHERE Title__c = 'Doc1' LIMIT 1];

        ContentDocument cd = [SELECT Id FROM ContentDocument WHERE Id =: doc.Collaboration_Group_Content_Id__c];

        // Act1 : Delete collaboration content
        Database.DeleteResult dr = new SQX_DB().continueOnError().op_delete(new List<ContentDocument> { cd }).get(0);

        // Assert1 : Expected error is thrown
        String errMsg = Label.CQ_UI_Cannot_Delete_Collaboration_Content;
        System.assertEquals(false, dr.isSuccess());
        System.assert(SQX_Utilities.checkErrorMessage(dr.getErrors(), errMsg), 'Expected error message: ' + errMsg);


        // Act2 : Close collaboration group and then delete content
        new SQX_Controller_Close_Collaboration(doc.Collaboration_Group_Id__c)
            .closeCollaboration();

        dr = new SQX_DB().op_delete(new List<ContentDocument> { cd }).get(0);

        // Assert2 : Expected content to be deleted successfully
        System.assertEquals(true, dr.isSuccess());
    }
}