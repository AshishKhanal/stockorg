/**
 * Class provides specific details required for submission of a Medwatch record
*/

public with sharing class SQX_Medwatch_eSubmitter extends SQX_Regulatory_Report_eSubmitter {

    public static final String RECORD_TYPE_MEDWATCH = 'MEDWATCH';

    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * the desired actions(XML generation and Insertion) for the given Medwatch Record
    */
    protected override String constructPayloadForSubmission(Id medwatchId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act, successAct1;
        SQX_External_Service_Payload.Job job1, job2;
        SQX_External_Service_Payload.HTTP_Request_Detail httpRequestDetail;
        SQX_External_Service_Payload.JWTPrincipal principal;
        Map<String,String> labelByApiName;

        Submission_Job_Detail submissionJobDetail;

        String baseUrl, versionDataQueryPath, versionDataInsertionPath;

        String recordJSON;

        payload = new SQX_External_Service_Payload();

        recordJSON = this.getRecordJSONFor(medwatchId, getSerializationConfig()).replaceAll(SQX.NSPrefix, '');   // removing Namespace for easier conversion

        baseUrl = Url.getSalesforceBaseUrl().toExternalForm();

        principal = payload.getDefaultPrincipalForCurrentUser();

        //versionDataQueryPath = baseUrl + '/services/data/v44.0/sobjects/ContentDocument/{0}/LatestPublishedVersion/VersionData';
        versionDataInsertionPath = baseUrl + '/services/data/v44.0/sobjects/ContentVersion/';

        // first action : XML generation
        act = payload.createAction();

        // before setting the first action, we need to have the additional info which is required to query for content associated with the medwatch record
        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(baseUrl, SQX_External_Service_Payload.HttpMethod.GET, null, principal);
		
        // List of sobject types for medwatch and all its child objects.
        List<Schema.SObjectType> sObjectTypeList = new List<Schema.SObjectType>{SQX_Medwatch__c.SObjectType,SQX_Medwatch_Suspect_Product__c.SObjectType,
                                                    SQX_Medwatch_D11_Medical_Product__c.SObjectType,SQX_Medwatch_H6_Conclusion__c.SObjectType,
                                                    SQX_Medwatch_H6_Device_Code__c.SObjectType,SQX_Medwatch_H6_Method__c.SObjectType,
                                                    SQX_Medwatch_H6_Patient_Code__c.SObjectType,SQX_Medwatch_H6_Result__c.SObjectType,
                                                    SQX_Medwatch_F10_Device_Code__c.SObjectType,SQX_Medwatch_F10_Patient_Code__c.SObjectType};
                                                        
        
        // get the map linking api name of the fields with the label for Medwatch
        labelByApiName = this.getFieldApiNameLabelMap(sObjectTypeList);

        // now adding additional data required for XML generation which is the record json
        submissionJobDetail = new Submission_Job_Detail(recordJSON, httpRequestDetail, labelByApiName);

        job1 = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_XML, null, submissionJobDetail);

        // second action : On Success of XML generation, insert the content to SF
        successAct1 = act.createSuccessAction();

        ContentVersion cv = new ContentVersion(
            PathOnClient = getRecordName(medwatchId) + EXTENSION_XML,
            CQ_Actions__c = String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { getRegReportIdFor(medwatchId) }),
            RecordTypeId = SQX_ContentVersion.getRegulatoryContentRecordType()
        );

        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(versionDataInsertionPath, SQX_External_Service_Payload.HttpMethod.POST, JSON.serialize(cv), principal);

        job2 = successAct1.addJob(SQX_External_Service_Payload.JobType.INSERT_CONTENT_TO_SF, new Integer[] { job1.getIdentifier() }, httpRequestDetail);
		
        return payload.convertToJSONString();
    }
    
    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * validation of the given Medwatch Record
    */
    protected override String constructPayloadForValidation(Id medwatchId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act;
        SQX_External_Service_Payload.Job job1;
        SQX_External_Service_Payload.HTTP_Request_Detail httpRequestDetail;
        SQX_External_Service_Payload.JWTPrincipal principal;
        Map<String,String> labelByApiName;

        Submission_Job_Detail submissionJobDetail;

        String baseUrl;

        String recordJSON;

        payload = new SQX_External_Service_Payload();

        recordJSON = this.getRecordJSONFor(medwatchId, getSerializationConfig()).replaceAll(SQX.NSPrefix, '');   // removing Namespace for easier conversion

        baseUrl = Url.getSalesforceBaseUrl().toExternalForm();

        principal = payload.getDefaultPrincipalForCurrentUser();

        // first action : XML generation
        act = payload.createAction();

        // before setting the first action, we need to have the additional info which is required to query for content associated with the medwatch record
        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(baseUrl, SQX_External_Service_Payload.HttpMethod.GET, null, principal);

        // List of sobject types for medwatch and all its child objects.
        List<Schema.SObjectType> sObjectTypeList = new List<Schema.SObjectType>{SQX_Medwatch__c.SObjectType,SQX_Medwatch_Suspect_Product__c.SObjectType,
                                                    SQX_Medwatch_D11_Medical_Product__c.SObjectType,SQX_Medwatch_H6_Conclusion__c.SObjectType,
                                                    SQX_Medwatch_H6_Device_Code__c.SObjectType,SQX_Medwatch_H6_Method__c.SObjectType,
                                                    SQX_Medwatch_H6_Patient_Code__c.SObjectType,SQX_Medwatch_H6_Result__c.SObjectType,
                                                    SQX_Medwatch_F10_Device_Code__c.SObjectType,SQX_Medwatch_F10_Patient_Code__c.SObjectType};
                                                        
        
        // get the map linking api name of the fields with the label for Medwatch
        labelByApiName = this.getFieldApiNameLabelMap(sObjectTypeList);

        // now adding additional data required for XML generation which is the record json
        submissionJobDetail = new Submission_Job_Detail(recordJSON, httpRequestDetail,labelByApiName);

        job1 = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_XML, null, submissionJobDetail);

        return payload.convertToJSONString();
    }

    /**
     * Returns the config to be used when serializing the Medwatch Record
    */
    protected override SObjectDataLoader.SerializeConfig getSerializationConfig() {
        return new SObjectDataLoader.SerializeConfig()
            .followChild(SQX_Medwatch_D11_Medical_Product__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_F10_Device_Code__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_F10_Patient_Code__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_H6_Device_Code__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_H6_Patient_Code__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_H6_Conclusion__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_H6_Result__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_H6_Method__c.SQX_Medwatch__c)
            .followChild(SQX_Medwatch_Suspect_Product__c.SQX_Medwatch__c)
            .follow(SQX_Medwatch__c.SQX_Medwatch_Null_Value__c)
            .follow(SQX_Medwatch_Suspect_Product__c.SQX_Suspect_Product_Null_Value__c)
            .follow(SQX_Medwatch_D11_Medical_Product__c.SQX_D11_Medical_Product_Null_Value__c);
    }

    private SQX_Medwatch__c recordDetailCache = null;

    /**
     * Medwatch record query
    */
    protected override SObject getRecordDetail(String medwatchId) {
        if(recordDetailCache == null) {
            recordDetailCache = [SELECT Name, SQX_Regulatory_Report__c FROM SQX_Medwatch__c WHERE Id =: medwatchId];
        }
        return recordDetailCache;
    }


    /**
     * Job Detail type of class specific to Medwatch submission job
    */
    private class Submission_Job_Detail implements SQX_External_Service_Payload.JobDetail {

        /**
         * Medwatch record (along with the children records) as JSON
         * Used to map values to XML/PDF
        */
        public String recordJSON;

        /**
         * Instructs the external service to use Medwatch schema for xml generation
        */
        public String recordType;

        /**
         * Contains details about querying SF for content data
         * Used when adding attachment to the XML
        */
        public SQX_External_Service_Payload.HTTP_Request_Detail contentQueryInfo;


        public Map<String,String> labelByApiName;

        public Submission_Job_Detail(String recordJSON, SQX_External_Service_Payload.HTTP_Request_Detail contentQueryInfo, Map<String,String> labelByApiName) {
            this.recordJSON = recordJSON;
            this.contentQueryInfo = contentQueryInfo;
            this.recordType = RECORD_TYPE_MEDWATCH;
            this.labelByApiName = labelByApiName;
        }
    }



    /**
     * Trigger handler for Medwatch trigger actions
    */
    public with sharing class SQX_Medwatch_Trigger_Handler extends SQX_Trigger_Handler {

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newValues equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public SQX_Medwatch_Trigger_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
            super(newValues, oldMap);
        }

        /**
        * Trigger methods:
        * a. Before Update [setSubmissionNumberOnSubmit]: Sets the submission number in record(Mfr Report #, UF Importer # etc) before submitting the record for XML/PDF conversion
        */
        protected override void onBefore() {
            setSubmissionNumberOnSubmit();
        }


        /**
        * Trigger methods:
        * a. After Insert/Update [clearActivityCode]: clears out activity code field value
        */
        protected override void onAfter() {
            if(Trigger.isUpdate) {
                invokeChildRecordsValidation();
            }
            clearActivityCode();
        }

        /**
         * Method sets the submission number in record(Mfr Report #, UF Importer # etc) before submitting the record for XML/PDF conversion
        */
        private void setSubmissionNumberOnSubmit() {

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Medwatch__c.Activity_Code__c, RuleOperator.NotEquals, SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_SUBMIT);
            recordsWithActivityCodeRule.addRule(SQX_Medwatch__c.Header_Mfr_Report_Number__c, RuleOperator.Equals, null);
            recordsWithActivityCodeRule.addRule(SQX_Medwatch__c.Header_UF_Importer_Report_Number__c, RuleOperator.Equals, null);

            this.resetView()
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(this.view.size() > 0){

                Map<Id, List<SQX_Medwatch__c>> medwatchRecordsByRegulatoryReport = new Map<Id, List<SQX_Medwatch__c>>();

                for(SQX_Medwatch__c record : (List<SQX_Medwatch__c>) this.view) {
                    if(!medwatchRecordsByRegulatoryReport.containsKey(record.SQX_Regulatory_Report__c)) {
                        medwatchRecordsByRegulatoryReport.put(record.SQX_Regulatory_Report__c, new List<SQX_Medwatch__c>());
                    }

                    medwatchRecordsByRegulatoryReport.get(record.SQX_Regulatory_Report__c).add(record);
                }

                if(medwatchRecordsByRegulatoryReport.size() > 0) {

                    final String REPORTING_SITE_MANUFACTURING_SITE = 'Manufacturing Site',  REPORTING_SITE_IMPORTER = 'User Facility/Importer Site';

                    Map<String, List<SQX_Medwatch__c>> medwatchRecordsBySchemeName = new Map<String, List<SQX_Medwatch__c>>();
                    Map<Id, Boolean> isMfrByRegulatoryReport = new Map<Id, Boolean>();

                    // first get all the Auto number schemes
                    for(SQX_Regulatory_Report__c report : [SELECT SQX_Reporting_Default__r.Reporting_Site__c, SQX_Reporting_Default__r.SQX_Imp_Dis_Num_Scheme__r.Name,
                                                           SQX_Reporting_Default__r.SQX_Man_Num_Scheme__r.Name FROM SQX_Regulatory_Report__c WHERE Id IN: medwatchRecordsByRegulatoryReport.keySet()
                                                           AND (SQX_Reporting_Default__r.Reporting_Site__c =: REPORTING_SITE_MANUFACTURING_SITE OR SQX_Reporting_Default__r.Reporting_Site__c =: REPORTING_SITE_IMPORTER)])
                    {
                        for(SQX_Medwatch__c medwatch : medwatchRecordsByRegulatoryReport.get(report.Id)) {
                            Boolean isMfr = report.SQX_Reporting_Default__r.Reporting_Site__c == REPORTING_SITE_MANUFACTURING_SITE;

                            String schemeName =  isMfr ? report.SQX_Reporting_Default__r.SQX_Man_Num_Scheme__r.Name : report.SQX_Reporting_Default__r.SQX_Imp_Dis_Num_Scheme__r.Name;

                            isMfrByRegulatoryReport.put(report.Id, isMfr);

                            if(!medwatchRecordsBySchemeName.containsKey(schemeName)) {
                                medwatchRecordsBySchemeName.put(schemeName, new List<SQX_Medwatch__c>());
                            }

                            medwatchRecordsBySchemeName.get(schemeName).add(medwatch);
                        }
                    }

                    if(medwatchRecordsBySchemeName.size() > 0) {
                        // get the auto number for all the schemes
                        List<SQX_AutoNumberingSetting> settings = new List<SQX_AutoNumberingSetting>();
                        for(String schemeName : medwatchRecordsBySchemeName.keySet()) {
                            SQX_AutoNumberingSetting setting = new SQX_AutoNumberingSetting();
                            setting.AutoNumberNameToUse = schemeName;
                            setting.PrefixToUse = String.valueOf(Date.today().year());

                            settings.add(setting);
                        }

                        SQX_AutoNumberGenerator.getNewSequenceNumber(settings);

                        // set the auto number in the corresponding medwatch record
                        for(SQX_AutoNumberingSetting setting : settings) {
                            String schemeName = setting.AutoNumberNameToUse,
                                    submissionNumber = setting.FormattedAutoNumber;

                            for(SQX_Medwatch__c medwatch : medwatchRecordsBySchemeName.get(schemeName)) {
                                Boolean isMfr = isMfrByRegulatoryReport.get(medwatch.SQX_Regulatory_Report__c);
                                if(isMfr) {
                                    medwatch.Header_Mfr_Report_Number__c = submissionNumber;
                                    medwatch.G9_MFR_Report_Number__c = submissionNumber;	// todo: make this a formula field that derives its value from Header Mfr Report Number
                                } else {
                                    medwatch.Header_UF_Importer_Report_Number__c = submissionNumber;
                                    medwatch.F2_UF_Importer_Report_Number__c = submissionNumber; // todo: make this a formula field that derives its value from Header UF Importer Report Number
                                }
                            }

                        }
                    }
                }
            }

        }

        /**
        * Method ensures that the validation is performed on medwatch child records as well
        */
        private void invokeChildRecordsValidation() {

            final String ACTION_NAME = 'invokeChildRecordsValidation';

            Rule validationGoingOnRule = new Rule();
            validationGoingOnRule.addRule(SQX_Medwatch__c.Activity_Code__c, RuleOperator.Equals, SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE);
            validationGoingOnRule.addRule(SQX_Medwatch__c.Activity_Code__c, RuleOperator.Equals, SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_SUBMIT);
            validationGoingOnRule.operator = RuleCombinationOperator.OpOr;

            String objName = this.getObjName();

            this.resetView()
                .removeProcessedRecordsFor(objName, ACTION_NAME)
                .applyFilter(validationGoingOnRule, RuleCheckMethod.OnCreateAndEveryEdit);

            if(this.view.size() > 0) {
                this.addToProcessedRecordsFor(objName, ACTION_NAME, this.view);

                Map<Id, SQX_Medwatch__c> medwatchById = new Map<Id, SQX_Medwatch__c>((List<SQX_Medwatch__c>) this.view);

                List<SQX_Medwatch_Suspect_Product__c> suspectProducts = [SELECT Id, SQX_Medwatch__c FROM SQX_Medwatch_Suspect_Product__c WHERE SQX_Medwatch__c IN: medwatchById.keySet()];
                for(SQX_Medwatch_Suspect_Product__c suspectProduct : suspectProducts) {
                    suspectProduct.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
                }

                List<Database.SaveResult> saveResults = new SQX_DB().continueOnError().op_update(suspectProducts, new List<SObjectField> { SQX_Medwatch_Suspect_Product__c.Activity_Code__c });

                integer index=0;
                Map<Id, List<String>> errMsgsByMedwatch = new Map<Id, List<String>>();
                for(Database.SaveResult saveResult : saveResults) {
                    SQX_Medwatch_Suspect_Product__c suspectProduct = suspectProducts.get(index++);

                    List<String> errMsgs = new List<String>();
                    for(Database.Error err : saveResult.getErrors()) {
                        errMsgs.add(err.getMessage());
                    }

                    if(errMsgs.size() > 0) {
                        if(!errMsgsByMedwatch.containsKey(suspectProduct.SQX_Medwatch__c)) {
                            errMsgsByMedwatch.put(suspectProduct.SQX_Medwatch__c, new List<String>{});
                        }

                        errMsgsByMedwatch.get(suspectProduct.SQX_Medwatch__c).addAll(errMsgs);
                    }
                }

                for(Id medwatchId : errMsgsByMedwatch.keySet()) {
                    medwatchById.get(medwatchId).addError(String.join(errMsgsByMedwatch.get(medwatchId), ','));
                }

            }

        }


        /**
         * Method clears out activity code field value
        */
        private void clearActivityCode() {

            final String ACTION_NAME = 'clearActivityCode';

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Medwatch__c.Activity_Code__c, RuleOperator.NotEquals, null);

            String objName = this.getObjName();

            this.resetView()
                .removeProcessedRecordsFor(objName, ACTION_NAME)
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Medwatch__c> recordsToUpdate = new List<SQX_Medwatch__c>();

            if(this.view.size() > 0){
                this.addToProcessedRecordsFor(objName, ACTION_NAME, this.view);

                for(SQX_Medwatch__c record : (List<SQX_Medwatch__c>) this.view) {
                    SQX_Medwatch__c recordToUpdate = new SQX_Medwatch__c(Id = record.Id);
                    recordToUpdate.Activity_Code__c = null;
                    recordsToUpdate.add(recordToUpdate);
                }

                new SQX_DB().op_update(recordsToUpdate, new List<SObjectField> { SQX_Medwatch__c.Activity_Code__c });
            }
        }

    }

}