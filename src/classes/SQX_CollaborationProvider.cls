/**
*   Interface that represents collaboration provider
*/
global interface SQX_CollaborationProvider{

    /**
    *   Initiates/Creates a collaboration group with the given content
    *   @param collaborationTitle - collaboration name
    *   @param members - list of initial members' ids to be added to the group
    *   @param Id - id of the controlled document record which has initiated the collaboration
    *   @return Map - map of group info ('GROUP_ID' -> newly formed group's id, 'CONTENT_ID' -> id of the content stored in the new group)
    */
    Map<String, String> initiateCollaboration(String collaborationTitle, String[] members, String Id);

    /**
    *   Closes/Archives/Deletes collaboration group
    *   @param groupId - Id of the collaboration group to be closed
    */
    void closeCollaboration(String groupId);

    /**
    *   Retrieves content from the group
    *   @param groupId - Id of the collaboration group whose content is to be retrieved
    *   @param contentId - Id of the content stored in the group which is to be retrieved
    */
    ContentVersion retrieveGroupContent(String groupId, String contentId);

    /**
    *   Retrieves info(title, url) of content stored in the collaboration group
    *   @param groupId - Id of the group whose content's info is to be retrieved
    *   @param contentId - Id of the content stored in the group
    *   @return map of info (TITLE -> contentTitle , URL -> contentURL, ...)
    */
    Map<String,String> retrieveGroupContentInfo(String groupId, String contentId);

    /**
    *   Removes content from the group, Most likely to be used in unison with close collaboration
    *   @param groupId - Id of the collaboration group whose content is to be deleted
    *   @param contentId - Id of the content to be deleted
    */
    void deleteGroupContent(String groupId, String contentId);

    /**
    *   Adds members to collaboration group
    *   @param user - list of SF users to be added to the group
    *   @param grpId - id of the group to which the member is to be added
    */
    void addMembers(List<User> users, String grpId);

    /**
    *   Removes members from the collaboration group    
    *   @param users - list of SF users to be removed from the group
    *   @param grpId - id of the group from which the member is to be removed
    */
    void removeMembers(List<User> users, String grpId);

    /**
    *  On close collaboration action, If we get any validation errors, the action should be rollback
    *  @param groupId - Id of the collaboration group to be undo close
    */
    void undoCloseCollaboration(String groupId);

    /**
    *   Method to revert collaboration group creation
    *   @param groupId - Id of the collaboration group which is to be deleted
    */
    void undoCollaborationInitiation(String groupId);
}