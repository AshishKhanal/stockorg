/**
 * This helper class for the assessment will provide all the functionalities required for the assessment to process.
 * @story : SQX-2680
 */
global class SQX_Question_List {
    /**
     * Assessment that will be available for actual processing i.e. preview or real assessment process
     */
    public SQX_Assessment__c assessment;

    /**
     * Assessment question list that will be asked in the particular assessment
     */
    public List<SQX_Assessment_Question__c> questionsList;

    /**
     * End of question
     */
    private final static Integer EOQ = -1;

    /**
     * declaring static values
     */
    private final static String EOL = '\r\n',
                               NEW_LINE = '\n',
                               SPACE = ' ',
                               CLOSE_PARANTHESIS = ')';
    
    /**
     * Holds the total questions to ask in the assessment
     */
    public Integer totalQuestionToAsk { get; set; }
    
    /**
     * This variable tells whether the assessment is in process or not
     */
    public boolean isAssessmentInProcess { get; set; }
    
    /**
     * This is used to get the current step of the running assessment
     */
    global Integer currentStep { get; set; }

    /**
     * Used to show achieved percentage of assessment
     */
    public Decimal achievedPercentage { get; set; }
    
    /**
     * Used to show number of correct answers user will give in assessment
     */
    public Decimal questionsCorrectlyAnswered { get; set; }
    
    /**
     * Used to show the final/overak result of the assessment
     */
    public String finalResult { get; set; }
    
    /**
     * Used to hold the passing percentage of the given assessment
     */
    public Decimal passingPercentage { get; set; }

    /**
     * This variable shows whether the assessment is ranodmize type or not
     */
    public boolean randomize;

    /**
     * This variable holds the current instance of the QuestionNode
     */
    public QuestionNode currentNode;

    /**
     * Maped Questions between question number and corresponding Question
     */
    public Map<Integer, SQX_Assessment_Question__c> questionByNum;

    /**
     * SQX_Question_List constructor
     * @param assessment : asessement which is used for preview/real-assessment
     * @param question : list of question that is asked in the coresponding assessment
     */    
    public SQX_Question_List(SQX_Assessment__c assessment, List<SQX_Assessment_Question__c> questions){
        this.assessment = assessment;
        this.questionsList = questions;
        this.totalQuestionToAsk = (Integer)assessment.Total_Questions_To_Ask__c;
        this.randomize = assessment.Randomize__c;
        this.passingPercentage = assessment.Passing_Percentage__c;
        
        if(this.questionsList.size() > 0){

            //if the assessment is randomize then shuffle the questions list
            if(this.randomize ){
                shuffleQuestionsList();
            }

            //build the index map
            buildIndex();
        }
        currentStep = 0;
        isAssessmentInProcess = true;
    }
    
    /**
     * This method builds up the map of questions
     */
    private void buildIndex() {
        questionByNum = new Map<Integer, SQX_Assessment_Question__c>();
        Integer index = 0;
        for (SQX_Assessment_Question__c question : questionsList) {
            if (randomize) {

                /** 
                 * note : index is being coverted in to question number to make uniform map of question number 
                 * to coresponding question for both randomize and non randomize assessment
                 */
                question.Question_Number__c = index;
            }
             
            questionByNum.put(Integer.valueOf((question.Question_Number__c)), question);
            
            index++;
        }
    }
    
    /**
     * This method suffles the questions list
     */
    private void shuffleQuestionsList() {
        final Double multiplier = Math.pow(10, Math.ceil(Math.log10(questionsList.size())));
        
        for (Integer i = questionsList.size() - 1; i >= 0; i--) {
            Integer randomIndex = Math.mod((Integer) (Math.random() * multiplier), questionsList.size());
            SQX_Assessment_Question__c temp = questionsList[i];
            questionsList[i] = questionsList[randomIndex];
            questionsList[randomIndex] = temp;
        }
    }
    
    /**
     * This method returns the next question node
     */
    public QuestionNode getNextQuestion(){
        if (currentStep >= totalQuestionToAsk || currentStep >= questionsList.size()) {
            return null;
        }

        if (currentNode == null) {

            //creating question node first time
            currentNode = randomize ? createNewNode(currentStep):createNewNode(Integer.valueOf((questionsList.get(0).Question_Number__c)));
        } else {

            QuestionNode nextNode = currentNode.nextNode;
            Integer nextQuestion;
            
            if (randomize) {
                nextQuestion = currentStep;
            } else {
                nextQuestion = currentNode.getPath();
                
                // if the next question number is not valid then end the assessment
                if(!questionByNum.containsKey(nextQuestion)){
                    nextQuestion = EOQ;
                }
            }
            
            if (nextQuestion == EOQ) {
                return null;
            }

            if (nextNode == null || nextNode.question.Question_Number__c != nextQuestion) {
                nextNode = createNewNode(nextQuestion);           
                nextNode.previousNode = currentNode;
                currentNode.nextNode = nextNode;
            }
            
            currentNode = nextNode;
        }
        currentStep++;

        //set the answerIndex of the node with currentStep as this is usefull to know which step has which question node
        currentNode.answerIndex = currentStep;

        return currentNode;
    }
    
    /**
     * This method returns the previous question node
     */
    public QuestionNode getPreviousQuestion(){
        if (currentNode != null && currentNode.previousNode != null) {
            currentStep--;
            currentNode = currentNode.previousNode;
        }
        return currentNode;
    }

    /**
     * This method checks whether the current node has next question node or not
     */
    global boolean hasNextQuestionNode() {
        return (currentStep < totalQuestionToAsk);
    }

    /**
     * This method checks whether the current node has previous question node or not
     */
    global boolean hasPreviousQuestionNode() {
        return (currentNode.previousNode != null);
    }
    
    /**
     * This method creates and returns the new node 
     */
    private QuestionNode createNewNode(Integer questionNumber) {
        QuestionNode node = new QuestionNode();
        node.question = questionByNum.get(questionNumber);
        
        return node;
    }
    
    /**
     * This methdd calcuates the assessment result 
     */
    public void calculateResult() {
        questionsCorrectlyAnswered = 0;
        isAssessmentInProcess = false;
        List<String> answers = new List<String>();
        QuestionNode node = currentNode;
        while ( node != null ) {
            if(node.isCorrectlyAnswered()){
                questionsCorrectlyAnswered++;
            }
            node = node.previousNode;
        }
        achievedPercentage = ((questionsCorrectlyAnswered / totalQuestionToAsk) * 100).setScale(2, RoundingMode.HALF_UP); //get up to two Decimal places
        finalResult = (achievedPercentage >= passingPercentage) ? SQX_Assessment.ASSESSMENT_RESULT_SATISFACTORY : SQX_Assessment.ASSESSMENT_RESULT_UNSATISFACTORY;
    }
    
    /**
     * This inner class wrapped the asked question with other required parameters.
     * This class also maintains the double linked list of the asked questions
     */
    global class QuestionNode{

        //question to be asked in the assessment
        public SQX_Assessment_Question__c question { get; set; }

        //this variable holds the user answer for the particualr question
        public String answer { get; set; }

        //this variable tells whether personnel's given answer for a particular question is true or false
        public boolean answerCorrect { get; set; }

        //this variable holds the next question node of the current node
        public QuestionNode nextNode {get; set; }

        //this variable holds the next question node of the current node
        public QuestionNode previousNode {get; set; }

        //this variable holds the step number of the asked question node
        public Integer answerIndex { get; set; }

        /**
         * This method returns next question number for non randomize type assessment
         */
        public Integer getPath() {
            Integer questionNumber;

            // if the question is type in answer type or if the answer is blank then always go to the next question of the current question.
            if(this.question.Type_In_Answer__c || String.isBlank(answer)){
                questionNumber =(Integer) this.question.Question_Number__c + 1;
            } else{
                String qnNumber;

                // replace \r\n with \n as only widnows treats it as new line whereas other operating system treats \n as new line.
                if(!String.isBlank(this.question.Navigation__c)){
                    
                    String[] navList = this.question.Navigation__c.replaceAll(EOL, NEW_LINE).split(NEW_LINE);
                    for (String l : navList) {
                        String[] lSplit = l.split('=');
                        if (answer == lSplit.get(0).trim()) { //A=2 ==> [A, 2]
                            qnNumber = lSplit.size() > 1 ? lSplit.get(1).trim() : '-1';
                            break;
                        }
                    } 
                    if(qnNumber == null){
                        qnNumber = SQX_Assessment_Question.QUESTION_NAVIGATION_NEXT;
                    }
                } else {
                    qnNumber = SQX_Assessment_Question.QUESTION_NAVIGATION_NEXT;
                }
                if(qnNumber.equalsIgnoreCase(SQX_Assessment_Question.QUESTION_NAVIGATION_END) || qnNumber.equals('-1')){
                    questionNumber = EOQ;
                } else if(qnNumber.equalsIgnoreCase(SQX_Assessment_Question.QUESTION_NAVIGATION_NEXT)) {
                    questionNumber =(Integer) this.question.Question_Number__c + 1;
                } else {
                     questionNumber = Integer.valueOf(qnNumber);
                }
            }        
        return questionNumber;
        }

        /**
         * This method returns the answer options for the asked questions
         */
        public List<SelectOption> getAnswerOptions() {
            String validAnswers = this.question.Answer_Options_Long__c.replaceAll(EOL, NEW_LINE);
            List<SelectOption> answers = new List<SelectOption>();
        
            /**
             * This logic separates label and value for the answer otpions
             * eg. answer option might be like A)Yes(not sure). In this case user should be see answer option as ==> Yes(not sure)
             */
            for (String answer : validAnswers.split(NEW_LINE)) {
                answer = answer.trim();
                if (answer.length() > 1) {
                    Integer idx = answer.indexOf(CLOSE_PARANTHESIS);
                    if (idx < 1) {
                        // if the question options does not contain ')' or ')' (i.e. separator) is first character
                        // then we don't show any further answers options so go out of the loop
                        // e.g. skip answer is ")Yes" or "Yes"
                        break;
                    }
                    String option = answer.subString(idx + 1, answer.length());
                    answers.add(new SelectOption(answer.subString(0, idx), option.trim()));
                }
            }
            return answers;
        }

        /**
         * This metod compares the user answer and actual answer of the question and returns corresponding true or false
         * things to consider : if the answer contains multiple spaces inside it(not at start or end),
         *                      then it will be replaced by single space.
         * eg. actual answer = 'animal is   dog'== > 'animal is dog'
         *     user answer = 'animal   is dog' == > 'animal is dog'
         */
        public boolean isCorrectlyAnswered() {
            this.answerCorrect = false;
            if ( !String.isBlank( this.answer )) {
                if ( this.question.Type_In_Answer__c ) {
                    String correctAns = this.question.Correct_Answer__c.trim().replaceAll('\\s+', SPACE);
                    String userAns = this.answer.trim().replaceAll('\\s+', SPACE);
                    this.answerCorrect = correctAns.length() == userAns.length() && correctAns.equalsIgnoreCase(userAns);

                } else {
                    this.answerCorrect = this.answer == this.question.Correct_Answer__c;
                }
            }
            return this.answerCorrect ;
        }
    }
}