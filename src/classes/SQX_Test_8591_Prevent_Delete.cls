/**
* This test case for to prevent product history review checklist.
*/
@isTest
public class SQX_Test_8591_Prevent_Delete {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * GIVEN : Complaint is created with PHR checklists
     * WHEN : closed complaint record
     * THEN : Error is thrown
     */
    public static testMethod void givenComplaintRecord_WhenEditPHA_ThenThrowErrorMessage(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            //Arrange: Create complaint and PHR with prodct history checklist
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            
            SQX_Product_History_Review__c product_history_review = new SQX_Product_History_Review__c();
            product_history_review.SQX_Complaint__c = complaint1.complaint.Id;
            insert product_history_review;
            
            SQX_Product_History_Answer__c product_history_answer = new SQX_Product_History_Answer__c();
            product_history_answer.SQX_Product_History_Review__c = product_history_review.Id;
            insert product_history_answer;
            
            //Act: Closed the complaint record
            complaint1.close();

            //Assert: Ensured that to prevent delete record
            List<SQX_Product_History_Answer__c> phaList = [SELECT Id FROM SQX_Product_History_Answer__c];
            Database.DeleteResult deleteResults = new SQX_DB().continueOnError().op_delete(phaList)[0];
            System.assertEquals(Label.SQX_ERR_MSG_CANNOT_DELETE_RECORD, deleteResults.getErrors().get(0).getMessage());
        }
    }
}