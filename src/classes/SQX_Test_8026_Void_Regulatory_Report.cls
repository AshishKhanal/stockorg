/*
 * test class for voiding regulatory report
 */
@isTest
public class SQX_Test_8026_Void_Regulatory_Report {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * GIVEN : Complaint with regulatory report
     * WHEN : Regulatory report is voided
     * THEN : Only report with pending status is voided
     */
    public testMethod static void givenComplaintWithRegReport_WhenRegReportIsVoid_OnlyPendingReportsCanBeVoid(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created with regulatory reports of different staus
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Regulatory_Report__c pendingRegReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING);

            // ACT : Regulatory report with pending status is voided
            pendingRegReport.Activity_Code__c = SQX_Regulatory_Report.ACTIVITY_CODE_VOID;
            Database.SaveResult result = Database.update(pendingRegReport, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());

            // ACT : Regulatory report with void status is voided
            pendingRegReport.Activity_Code__c = SQX_Regulatory_Report.ACTIVITY_CODE_VOID;
            result = Database.update(pendingRegReport, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Only Pending Reports can be voided.'), result.getErrors());

            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            SQX_Regulatory_Report__c completedRegReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_COMPLETE, null, null, submissionHistory);
            
            // ACT : Regulatory report with complete status is voided
            completedRegReport.Activity_Code__c = SQX_Regulatory_Report.ACTIVITY_CODE_VOID;
            result = Database.update(completedRegReport, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' + result.getErrors());
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Only Pending Reports can be voided.'), result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint with regulatory report
     * WHEN : Submitted Regulatory report is voided
     * THEN : Error is thrown
     */
    public testMethod static void givenComplaintWithRegReport_WhenSubmittedRegReportIsVoid_ErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created with regulatory report
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            SQX_Regulatory_Report__c pendingRegReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_COMPLETE, null, null, submissionHistory);
            
            // ACT : Submitted Regulatory report with pending status is voided
            pendingRegReport.Activity_Code__c = SQX_Regulatory_Report.ACTIVITY_CODE_VOID;
            Database.SaveResult result = Database.update(pendingRegReport, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' + result.getErrors());
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Report is already submitted, it cannot be voided.'), result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint with regulatory report created from decision tree
     * WHEN : Regulatory report is voided
     * THEN : Report is voided
     */
    public testMethod static void givenComplaintWithRegReportCreatedFromDT_WhenRegReportIsVoid_ReportIsVoid(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Task task = null;
        SQX_Answer_Option__c answerOption = null;
        System.runas(adminUser){
            
            // ARRANGE : Create a CQ Task
            task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task.task.SQX_User__c = standardUser.Id;
            task.save();
            
            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();
            
            // ARRANGE : Create Answer Option
            answerOption = task.addAnswer(question, null, false);
            
            // ARRANGE : Create Answer Option Attributes with reportable flag checked and all required fields.
            SQX_Answer_Option_Attribute__c answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true);
            Database.insert(answerOptionAttribute, false);
        }
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created with decision tree
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Decision_Tree__c decisionTree = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint.complaint.Id,
                                                                         SQX_Task__c = task.task.Id);
            insert decisionTree;

            SQX_DT_Answer__c decisionTreeAnswer = new SQX_DT_Answer__c(SQX_Decision_Tree__c = decisionTree.Id, 
                                                                       SQX_Answer_Option__c = answerOption.Id);
            insert decisionTreeAnswer;
            
            SQX_Create_Regulatory_Report.createRegulatoryReportBasedOnAnswerOptionAttribute(new List<SQX_Decision_Tree__c>{decisionTree});

            // ACT : Form name of regulatory information is changed created from Decision Tree
            SQX_Regulatory_Report__c reportWithDT = [SELECT Id, SQX_Decision_Tree__c FROM SQX_Regulatory_Report__c WHERE SQX_Decision_Tree__c =: decisionTree.Id];

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Regulatory report is voided
            reportWithDT.Activity_Code__c = SQX_Regulatory_Report.ACTIVITY_CODE_VOID;
            Database.SaveResult result = Database.update(reportWithDT, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
        }
    }
}