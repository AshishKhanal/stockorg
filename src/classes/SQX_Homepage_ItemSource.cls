/**
* This is an abstract class that defines the Sources of homepage items  
* @story   SQX-3159 Homepage UI and Framework Implementation
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-06-03
*/
public with sharing abstract class SQX_Homepage_ItemSource {

    private static final Integer DEFAULT_PAGE_SIZE = 100;
    
    /*
        Holds filter properties to be applied to records when retrieving them
    */
    public SQX_Homepage_Filter filter { get; set; }

    /**
     * Indicates whether or not the source is dynamic and can alter its queries based on CRUD/FLS checks
     */
    public Boolean isDynamic {get; set;}

    /**
     * Holds the query mappings which will be modified by CRUD/FLS check and subsequently used by implementing source(child class)
     * to perform query on any remaining/valid items.
     */
    protected Map<String,String> dynamicQueryMap {get; set;}

    /**
    *   Method to be overriden by child classes to return all the sobject types and the corresponding fields being queried in the child class
    */
    protected virtual Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        // this method has to be overriden which is why error is thrown if not implemented
        throw new SQX_ApplicationGenericException('Please ensure that FLS check has been integrated');
    }


    /**
    *   Method to be overridden by child classes to return all of user records
    */
    protected virtual List<SObject> getUserRecords(){
        return new List<SObject>();
    }


    /**
    *   Method to be overridden by child classes to return all of user records related objects
    */
    protected virtual List<Object> getUserObjects(){
        return new List<Object>();
    }

    /**
     *  Method to be overridden by child class to return true if the child class returns
     *  Objects instead of SObjects when requested for user records
     *  Ex: Items To Approve type items
    */
    protected virtual Boolean returnsObjects(){
        return false;
    }

    // ///////////////////////// FOR FUTURE USE ////////////////////////////// //

            // /**
            // *   Method to be overridden by child classes to return entire org's records ( FOR ADMIN USERS )
            // */
            // protected virtual List<SObject> getOrgWideRecords(Integer pageSize, List<Schema.SObjectField> orderBy, Boolean sortDescending){
            //     return new List<SObject>();
            // }

            // /**
            // *   Method to be overridden by child classes to return all of records based on role hierarchy
            // */
            // protected virtual List<SObject> getHierarchyWiseRecords(Integer pageSize, List<Schema.SObjectField> orderBy, Boolean sortDescending){
            //     return new List<SObject>();
            // }

    // ////////////////////////////////////////////////////////////////////// //


    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param rec - Record to be converted to home page item
    */
    protected virtual SQX_Homepage_Item getHomePageItemFromRecord(SObject rec){
        if(!this.returnsObjects()){
            throw new SQX_ApplicationGenericException('Method has to be implemented by child class');
        }
        return null;
    }

    /**
    *   Method returns a SQX_Homepage_Item type item from the given object
    *   @param object - Contains information necessary for creating a homepage item
    */
    protected virtual SQX_Homepage_Item getHomePageItemFromObject(Object obj){
        if(this.returnsObjects()){
            throw new SQX_ApplicationGenericException('Method has to be implemented by child class');
        }
        return null;
    }

    /**
     * This method construct and returns the home page action url based on whether is in community context or normal context.
     * @param pr instance of page reference containing necessary parameters.
     * 
     * Note : community supported url formate : baseurl/recodId or baseurl/recordId?otherparams(optional)
     */
    protected String getHomePageItemActionUrl(PageReference pr) {
        String baseUrl = SQX_Homepage_Constants.SF_BASE_URL;
        String actionUrl = baseUrl + pr.getUrl();
        // search for Id in the url if available
        String recordId = pr.getParameters().get('Id');
        // if the user context is community and url has Id as parameter then construct action url for home page items as 'baseurl/recordId'
        if ( SQX_Utilities.isCommunityUserContext() && recordId != null) {
           actionUrl = baseUrl + '/' + recordId; 
        }
        return actionUrl;
    }

    /**
    *   Constructor
    */
    public SQX_Homepage_ItemSource(){ }

    /**
    * Fetches list of items from the source, converts them to standardized CQ Home Page Item and returns those items
    */
    public List<SQX_Homepage_Item> getHomePageItems(){

        List<SQX_Homepage_Item> items = new List<SQX_Homepage_Item>();

        if(this.returnsObjects())
        {
            for(Object obj : getObjects()){
                items.add(this.getHomePageItemFromObject(obj));
            }
        }
        else
        {
            for(SObject record : getRecords()){
                items.add(this.getHomePageItemFromRecord(record));
            }
        }
        

        return items;
    }


    /**
    *   Method returns a list of records associated for a particular home page item
    */
    private List<SObject> getRecords(){

        List<SObject> records = new List<SObject>();

        if(this.checkAccessibility())
        {
            if( this.filter.sourceType == SQX_Homepage_Filter.Source.User ){

                records = this.getUserRecords();
            }
        }

        return records;
    }

    /**
    *   Method returns a list of records associated for a particular home page item
    */
    private List<Object> getObjects(){

        List<Object> records = new List<SObject>();

        if(this.checkAccessibility())
        {
            if( this.filter.sourceType == SQX_Homepage_Filter.Source.User ){
                records = this.getUserObjects();
            }
        }

        return records;
    }


    // ####################################### HELPER METHODS START ############################################# //


    /**
    *   Method returns <code>true</code> if all of the given sobjecttypes are accessible by the current user
    *   else returns <code>false</code>
    *   @params objectTypes list of object types to be checked
    */
    public Boolean checkAccessibility(){

        Map<SObjectType, List<SObjectField>> objectsFieldsMap = getProcessableEntities();

        if(objectsFieldsMap.keySet().size() > 0)
        {
            SQX_Lightning_CRUD_FLS_Checker permissionChecker = new SQX_Lightning_CRUD_FLS_Checker();

            // check accessibility of sobjects
            Map<SObjectType, Boolean> accessibility = permissionChecker.getObjectsAccessibility(objectsFieldsMap.keySet());

            for(SObjectType objectType : accessibility.keySet())
            {
                if(!accessibility.get(objectType) && isDynamic == true){
                    dynamicQueryMap.remove(objectType.getDescribe().getName());
                    objectsFieldsMap.remove(objectType);
                } else if (!accessibility.get(objectType)) {
                    return false;
                }
            }

            // check accessibility of sobjectfields
            // if accessibility permission is missing on any one of the fields, exception is raised
            try {
            permissionChecker.checkFieldsAccessibility(objectsFieldsMap);
            } catch(Exception ex) {
                // disable field check for dynamic source
                // TODO: based on the field accessibility modify the source to only query related items example investigation with NC but no CAPA etc.
                if(isDynamic != false) {
                    throw ex;
                }
            }
        }

        return true;
    }


    /**
     * Method returns the urgency type of an item based on the days between current date and due date and the given limit for criticality
     * @param dueDate - due date of the item
     * @param criticalityLimit - limit(number of days) for an item to be critical or high
    */
    public SQX_Homepage_Item.Urgency getItemUrgency(Date dueDate, Integer criticalityLimit){

        Integer daysBetween = System.today().daysBetween(dueDate);

        return  daysBetween < 0 ?
                    SQX_Homepage_Item.Urgency.Critical :
                daysBetween <= criticalityLimit ?
                    SQX_Homepage_Item.Urgency.High :
                    SQX_Homepage_Item.Urgency.Low;
    }


    /**
    *  Method returns a shorted version of the given text
    *  Note : Max length is considered to be 255 which is the length of the 'Subject' field in 'Task' object
    *  @param text - Text to be shortened 
    */
    public String getShortenedDescription(String text){

        return SQX_Utilities.getTruncatedText(text, SQX_Homepage_Constants.SHORT_DESC_MAX_CHARACTERS, true);
    }

    // ####################################### HELPER METHODS END ############################################# //

}