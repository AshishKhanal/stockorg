/**
*   @description : Test class for SQX-2677 which simulates the submit for approval button that supports immediate rendition
*   @author : Piyush Subedi
*   @story : 2677
*/
@isTest
public class SQX_Test_2677_Controlled_Document{
    static Blob result = Blob.valueOf(''); // stores the entire content of the secondary content generated after rendition

    public class MockHttpClass implements HttpCalloutMock{
        Boolean pendingMode = false;

        public void enablePendingMode(){
            this.pendingMode = true;
        }

        public HttpResponse respond(HttpRequest request){
            
            HttpResponse response = new HttpResponse();

            String endPoint = request.getEndpoint();
            Integer statusCode = 200;
            
            if( endPoint.contains('metadata') ){
                // retrieve content size
                response.setBody('{"bytes" : "500"}');

            }else if( endPoint.contains('output') ){
                // retrieve content request
                response.setBodyAsBlob(result);

            }else if( endPoint.contains('input') ){
                // upload content request
                result = Blob.valueOf(result.toString() + request.getBodyAsBlob().toString());

            }else if( endPoint.contains('token') ){
                // get access token request
                response.setBody('{"access_token" : "0000"}');

            }else if( endPoint.contains('event') ){
                // polling request
                if(this.pendingMode){
                    statusCode = 202; // continue status
                }else{
                    response.setBody('{"status" : "completed"}');
                }

            }else if( endPoint.endsWith('/v1/jobs') ){
                // set workflow request
                SQX_Test_2521_Secondary_Format_Setting.workflowJson = request.getBody();

                response.setBody('{"jobID" : "0000"}');
                statusCode = 201;
            }

            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(statusCode);

            return response;
        }
    }

    static Boolean runAllTests = true,
                    run_givenControlledDocumentWithSecondaryContentStatusNotInSync_WhenUserSubmitsTheDocForApproval_ThenRenditionOfDocumentShouldBeAttempted = false,
                    run_givenControlledDocumentWithSecondaryContentStatusNotInSyncWhoseRenditionTakesMoreThanThirtySecs_WhenUserSubmitsTheDocForApproval_ThenSynchronizationStatusShouldBePendingAndApprovalShouldBePutOnHold = false,
                    run_givenControlledDocumentWithSecondaryContentDisabled_WhenUserSubmitsTheDocForApproval_ThenDocShouldBeSubmittedForApproval = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users       
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        System.runAs(standardUser){

            // custom setting to define CQ's rendition provider
            SQX_Rendition_Providers__c providerSetting = new SQX_Rendition_Providers__c(
                Name = 'easyPDFProvider',
                Extension__c = '*',
                Provider_Class__c = 'SQX_PDFRenditionProvider1',
                Provider_Namespace__c = SQX.NSPrefix.substringBefore('__')
            );
            insert providerSetting;

            // creating a controlled document with secondary content enabled and out of sync
            SQX_Test_Controlled_Document tcd1 = new SQX_Test_Controlled_Document();
            tcd1.doc.Document_Number__c = 'CDOC_1';
            tcd1.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO;
            tcd1.doc.Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC;
            tcd1.save(true);

            // creating a controlled document with secondary content disabled
            SQX_Test_Controlled_Document tcd2 = new SQX_Test_Controlled_Document();
            tcd2.doc.Document_Number__c = 'CDOC_2';
            tcd2.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_DISABLED;
            tcd2.doc.First_Approver__c = standardUser.Id;
            tcd2.save();
        }
    }

    /**
    * returns controlled document with the given document number
    * @param documentNumber is controlled document number
    */
    static SQX_Controlled_Document__c getDoc(String documentNumber){

        List<SQX_Controlled_Document__c> docs = [SELECT Id
                                                 FROM SQX_Controlled_Document__c WHERE Document_Number__c = : documentNumber];

        if( docs.size() > 0)
            return docs.get(0);

        return null;
    }

    /**
    *   Given a document [CDOC_1] whose Secondary Content Status is not 'In Sync'
    *   When user clicks 'Submit For Approval' button
    *   Then [CDOC_1] should be submitted for rendition, secondary Content Status should be set to In Sync and document should be sent for approval
    */
    static testmethod void givenControlledDocumentWithSecondaryContentStatusNotInSync_WhenUserSubmitsTheDocForApproval_ThenRenditionOfDocumentShouldBeAttempted(){
        
        if(!runAllTests && !run_givenControlledDocumentWithSecondaryContentStatusNotInSync_WhenUserSubmitsTheDocForApproval_ThenRenditionOfDocumentShouldBeAttempted){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange: fetch controlled document with secondary content enabled and out of sync
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document();
            tstObj.doc = getDoc('CDOC_1');
            tstObj.synchronize();

            SQX_Extension_Controlled_Document docExt = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(tstObj.doc));

            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

            Test.startTest();

            // Act: Submit doc for approval  
            PageReference pr = docExt.syncDocumentForSubmission();

            Test.stopTest();

            tstObj.synchronize();

            // Assert : doc has been synchronized
            System.assertEquals(SQX_Controlled_Document.SYNC_STATUS_IN_SYNC, tstObj.doc.Synchronization_Status__c);
        }
    }

    /**
    *   Given a document [CDOC_1] whose Secondary Content Status is not 'In Sync' and whose rendition might take more than 30 secs
    *   When user clicks 'Submit For Approval' button
    *   Then [CDOC_1] should be submitted for rendition and if rendition takes more than 30 secs, Secondary Content Status should be set to 'Pending' and Auto Submit For Approval should be set to True
    */
    static testmethod void givenControlledDocumentWithSecondaryContentStatusNotInSyncWhoseRenditionTakesMoreThanThirtySecs_WhenUserSubmitsTheDocForApproval_ThenSynchronizationStatusShouldBePendingAndApprovalShouldBePutOnHold(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryContentStatusNotInSyncWhoseRenditionTakesMoreThanThirtySecs_WhenUserSubmitsTheDocForApproval_ThenSynchronizationStatusShouldBePendingAndApprovalShouldBePutOnHold){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange: fetch controlled document with secondary content enabled and out of sync
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document();
            tstObj.doc = getDoc('CDOC_1');
            tstObj.synchronize();

            SQX_Extension_Controlled_Document docExt = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(tstObj.doc));

            MockHttpClass mockClass = new MockHttpClass();
            mockClass.enablePendingMode();  // ensuring that rendition is not immediately complete
            Test.setMock(HttpCalloutMock.class, mockClass);

            Test.startTest();

            // Act: Submit doc for approval
            PageReference pr = docExt.syncDocumentForSubmission();

            Test.stopTest();

            tstObj.synchronize();

            // Assert : doc synchronization is pending
            System.assertEquals(SQX_Controlled_Document.SYNC_STATUS_PENDING, tstObj.doc.Synchronization_Status__c);

            // asserting that doc approval is put on hold
            System.assertEquals(SQX_Controlled_Document.APPROVAL_SUBMISSION_STATUS_HOLD, tstObj.doc.Auto_Submit_For_Approval__c);
        }
    }

    /**
    *   Given a document [CDOC_2] whose Secondary Content is disabled
    *   When user clicks 'Submit For Approval' button
    *   Then [CDOC_2] should be submitted for approval
    */
    static testmethod void givenControlledDocumentWithSecondaryContentDisabled_WhenUserSubmitsTheDocForApproval_ThenDocShouldBeSubmittedForApproval(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryContentDisabled_WhenUserSubmitsTheDocForApproval_ThenDocShouldBeSubmittedForApproval){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange: fetch controlled document with secondary content disabled
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document();
            tcd.doc = getDoc('CDOC_2');
            tcd.synchronize();

            SQX_Extension_Controlled_Document docExt = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(tcd.doc));

            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

            Test.startTest();

            // Act: submit doc for approval
            docExt.submitDocForApproval();

            Test.stopTest();

            tcd.synchronize();

            // Assert : the document has been sent for approval
            System.assertEquals(SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL, tcd.doc.Approval_Status__c);
        }
    }
    /**
     * GIVEN : A Controlled Document in Current status
     * WHEN : Requirements are added(Active and Inactive Requirements) 
     * THEN : Returns Active requirements
     * @story : [SQX-5964]
     */
    public testmethod static void givenControlledDocumentInCurrentStatus_WhenRequirementsAdded_ThenReturnActiveRequirements(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : Create a controlled document
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            doc1.save();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            ApexPages.StandardController controller = new ApexPages.StandardController(doc1.doc);
            SQX_Extension_Controlled_Document docExtension = new SQX_Extension_Controlled_Document(controller);

            // Create Job Functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            jf.save();

            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2' );
            jf = new SQX_Test_Job_Function(jf2);
            jf.save();
            
            SQX_Job_Function__c jf3 = new SQX_Job_Function__c( Name = 'job function 3' );
            jf = new SQX_Test_Job_Function(jf3);
            jf.save();

            // ACT :  Add Requirements for the document
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = doc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = true
            );
            Database.insert(Req1, false);

            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = doc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            Database.insert(Req2, false);

            SQX_Requirement__c Req3 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = doc1.doc.Id,
                SQX_Job_Function__c = jf3.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = false,
                Active__c = true
            );
            Database.insert(Req3, false);

            // ASSERT : Ensure only active requirements are returned
            System.assertEquals(2, docExtension.getDocActiveRequirements().size());
            // Ensure all requirements returned are active
            for(SQX_Requirement__c req : docExtension.getDocActiveRequirements()){
                System.assertEquals(true, req.Active__c);
            }
        }
    }
}