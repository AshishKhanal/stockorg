/**
* Batch processor for the NC Staging to NC Conversion
*/
global with sharing class SQX_NC_Staging_Processor implements Database.Batchable <sObject>{
    public class invalidDataException extends Exception{}

    //default batch size of 2 for the next schedule
    global Integer BATCH_SIZE = 2;

    //default duration for next schedule in minutes
    global Integer SCHEDULE_AFTER_IN_MIN = 10;
    
    //default the department creation to true
    public boolean CREATE_DEPARTMENT_IF_NOT_FOUND = true;

    //this flag can be used in tests to disable rescheduling
    //for normal usage this value is always true, for tests it is set to false but can be changed
    public boolean RESCHEDULE = !Test.isRunningTest();

    /**
    * Returns the query that will fetch the relevant data.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            [SELECT Account_Name__c,Business_Unit__c,Contact_Email__c,Defective_Quantity__c,Defect_Category__c,Unit_of_Measure__c,
            Defect_Code__c,Defect_Description__c,Defect_Failure__c,SQX_Department__c,Department_Name__c,Description__c,Disposition_Qty__c,
            Disposition__c,Disposition_By__c,Disposition_Date__c,Disposition_Completion_Date__c,Disposition_Completion_By__c,Disposition_Comment__c,
            Division__c,Error_Description__c,Issue_Type__c,Lot_Number__c,NC_Type__c,SQX_NC__c,Number_of_Defects__c,Occurence_Date__c,Part_Family__c, Target_Object_Type__c,
            Part_Name__c,Part_Number__c,SQX_Part__c,Priority__c,Process__c,Lot_Quantity__c,Reference_Number__c,Site__c,Status__c,Title__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,Name,OwnerId,
            Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c
            FROM SQX_NC_Staging__c
            WHERE Status__c = 'Pending' Order By CreatedDate ASC]);
    }

    /**
    * Default batch method that transforms NC Staging to NC 
    */
    global void execute(Database.BatchableContext bc,List<sObject> scope){
        Map<string,Map<string,SQX_FieldValue>> customizedFieldMap = null;
        Map<string,object> paramsToUse =  new Map<string,object>();
        paramsToUse.put(SQX_NC_Staging.ADD_DEPT, CREATE_DEPARTMENT_IF_NOT_FOUND);

        //get default queue
        SQX_Custom_Settings_Public__c setting = SQX_Custom_Settings_Public__c.getInstance();
        if(setting.Default_Queue_Name__c != null){
            //search for a queue with the same name
            List<Group> queues = [SELECT Id, Name, Type  FROM Group WHERE DeveloperName =: setting.Default_Queue_Name__c AND Type = 'Queue'];
            if(queues.size() > 0){
                paramsToUse.put(SQX_NC_Staging.DEFAULT_QUEUE_ID, queues.get(0).Id);
            }
        }
        
        executeOverride(bc, scope, customizedFieldMap, paramsToUse);
    }

    /**
    * This is a method that must be called by any client side override. It provides mechanism to pass custom field map and custom params
    */
    global void executeOverride(Database.BatchableContext bc,List<sObject> scope, Map<String, Map<String, SQX_FieldValue>> customizedFieldMap, Map<String, Object> paramsToUse){

        SQX_NC_Staging processor = new SQX_NC_Staging(scope,customizedFieldMap,paramsToUse);        
        processor.ProcessNC();
    }

    /**
    * This method is called by the batch executor to perform cleanups. In case of CQ this method reschedules the NC Staging to NC 
    * transformation after X minutes till 8:00 PM.
    */
    global void finish(Database.BatchableContext bc){
        //schedule next loop. Will be based on some sort of setup to stop at certain time daily
        if (RESCHEDULE && system.now().format('hhmmss','America/Los_Angeles')<'200000'){
            //system.scheduleBatch(batchable, jobName, minutesFromNow, scopeSize)
            System.scheduleBatch(new SQX_NC_Staging_Processor(), 'CQ-Process NC Staging '+ system.now().format('hhmmss','America/Los_Angeles'), SCHEDULE_AFTER_IN_MIN , BATCH_SIZE);
           }
    }    
}