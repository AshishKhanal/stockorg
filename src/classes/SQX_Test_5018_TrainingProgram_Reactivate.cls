@isTest
class SQX_Test_5018_TrainingProgram_Reactivate {

    @testSetup
    static void commonSetup() {
        // add required users
        User admin = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin');
    }
    
    /**
    * creates personnel job functions for a job function
    */
    static List<SQX_Personnel_Job_Function__c> createPJFs(SQX_Job_Function__c jf, List<SQX_Personnel__c> psns) {
        List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
        for (Integer i = 0; i < psns.size(); i++) {
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Job_Function__c = jf.Id, SQX_Personnel__c = psns[i].Id ));
        }
        return pjfs;
    }
    
    /**
    * creates document requirement steps for a job function
    */
    static List<SQX_Requirement__c> createREQs(SQX_Job_Function__c jf, List<SQX_Test_Controlled_Document> docs, Decimal firstStep) {
        List<SQX_Requirement__c> reqs = new List<SQX_Requirement__c>();
        for (Integer i = 0; i < docs.size(); i++) {
            reqs.add(new SQX_Requirement__c(
                            SQX_Job_Function__c = jf.Id,
                            SQX_Controlled_Document__c = docs[i].doc.Id,
                            Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                            Training_Program_Step__c = firstStep + i
            ));
        }
        return reqs;
    }
    
    /**
    * Common test case for training program reactivation with training generation batch job ON/OFF
    * Given:
    *   "Use Batch Job To Process REQ Training" custom setting is ON/OFF (provided)
    *   Create a training program with 2 personnels and 2 steps
    *   Complete all trainings of 2nd personnel
    *   Deactivate the training program
    *   Add same personnels and same steps
    * When:
    *   The training program is reactivated
    * Then:
    *   1st personnel should be at 1st step training with pending training status
    *   2nd personnel should be at 2nd step training with current training status
    */
    static void runJobFunctionReactivationTestCase(Boolean useBatchJobForTrainingGeneration, Decimal firstStep) {
        SQX_Activated_Requirement_Processor activatedReqProc;
        SQX_Deactivated_Requirement_Processor deactivatedReqProc;
        
        if (useBatchJobForTrainingGeneration) {
            // ARRANGE: turn on training generation batch job when needed
            SQX_Test_3474_RequirementProcessingBatch.setTrainingBatchJobCustomSetting(true);
            activatedReqProc = new SQX_Activated_Requirement_Processor();
            deactivatedReqProc = new SQX_Deactivated_Requirement_Processor();
        }
        
        // ARRANGE: add required current controlled documents
        List<SQX_Test_Controlled_Document> docs = new List<SQX_Test_Controlled_Document>();
        docs.add(new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT));
        docs.add(new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT));
        docs[0].save();
        docs[1].save();
        
        // ARRANGE: add required training program and personnels
        SQX_Job_Function__c jf = new SQX_Job_Function__c( Name = 'Training Program 1' );
        List<SQX_Personnel__c> psns = new List<SQX_Personnel__c>{ new SQX_Test_Personnel().mainRecord, new SQX_Test_Personnel().mainRecord };
        List<SObject> sobjs = new List<SObject>();
        sobjs.add(jf);
        sobjs.addAll((List<SObject>)psns);
        insert sobjs;
        
        // ARRANGE: add required personnel job functions and document requirements
        sobjs.clear();
        List<SQX_Personnel_Job_Function__c> currentPJFs = createPJFs(jf, psns);
        List<SQX_Requirement__c> currentREQs = createREQs(jf, docs, firstStep);
        sobjs.addAll((List<SObject>)currentPJFs);
        sobjs.addAll((List<SObject>)currentREQs);
        insert sobjs;
        
        // ARRANGE: activate training program
        SQX_BulkifiedBase.clearAllProcessedEntities();
        jf.Active__c = true;
        update jf;
        
        if (useBatchJobForTrainingGeneration) {
            // ARRANGE: run training generation batch job when needed
            activatedReqProc.processBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(currentREQs[0].Id));
            activatedReqProc.processBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(currentREQs[1].Id));
        }
        
        // ASSERT: ensure pending trainings are generated for step 1 for both personnels
        List<SQX_Personnel_Document_Job_Function__c> psn1PDJFs = new List<SQX_Personnel_Document_Job_Function__c>();
        List<SQX_Personnel_Document_Job_Function__c> psn2PDJFs = new List<SQX_Personnel_Document_Job_Function__c>();
        for (SQX_Personnel_Document_Job_Function__c pdjf : [SELECT Id, Personnel_Id__c, SQX_Requirement__c, SQX_Personnel_Document_Training__c, Training_Status__c,
                                                                   SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                                                                   SQX_Personnel_Document_Training__r.Status__c
                                                            FROM SQX_Personnel_Document_Job_Function__c]) {
            if (pdjf.Personnel_Id__c == psns[0].Id) {
                psn1PDJFs.add(pdjf);
            }
            if (pdjf.Personnel_Id__c == psns[1].Id) {
                psn2PDJFs.add(pdjf);
            }
        }
        // asserts for psn 1
        System.assertEquals(1, psn1PDJFs.size(), 'Expected pdjfs count for psn 1');
        System.assertNotEquals(null, psn1PDJFs[0].SQX_Personnel_Document_Training__c, 'Expected training to be generated for psn 1');
        System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, psn1PDJFs[0].Training_Status__c, 'Expected training status for psn 1');
        System.assertEquals(firstStep, psn1PDJFs[0].SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                            'Expected PJF Current Training Program Step for psn 1');
        System.assertEquals(currentREQs[0].Id, psn1PDJFs[0].SQX_Requirement__c, 'Training should be generated for first step training for psn 1');
        // asserts for psn 2
        System.assertEquals(1, psn2PDJFs.size(), 'Expected pdjfs count for psn 2');
        System.assertNotEquals(null, psn2PDJFs[0].SQX_Personnel_Document_Training__c, 'Expected training to be generated for psn 2');
        System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, psn2PDJFs[0].Training_Status__c, 'Expected training status for psn 2');
        System.assertEquals(firstStep, psn2PDJFs[0].SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                            'Expected PJF Current Training Program Step for psn 2');
        System.assertEquals(currentREQs[0].Id, psn2PDJFs[0].SQX_Requirement__c, 'Training should be generated for first step training for psn 2');
        
        Test.startTest();
        
        // ARRANGE: complete step 1 training for 2nd personnel
        psn2PDJFs[0].SQX_Personnel_Document_Training__r.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
        psn2PDJFs[0].SQX_Personnel_Document_Training__r.Completion_Date__c = System.today();
        update psn2PDJFs[0].SQX_Personnel_Document_Training__r;
        
        // list 1st step completed training id for 2nd personnel
        List<Id> psn2PdtIds = new List<Id>();
        psn2PdtIds.add(psn2PDJFs[0].SQX_Personnel_Document_Training__c);
        
        // ASSERT: ensure 2nd step pending training is created only for 2nd personnel
        List<SQX_Personnel_Document_Job_Function__c> step2pdjfs1 = [
            SELECT Id, Personnel_Id__c, SQX_Requirement__c, SQX_Personnel_Document_Training__c, Training_Status__c,
                   SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                   SQX_Personnel_Document_Training__r.Status__c
            FROM SQX_Personnel_Document_Job_Function__c
            WHERE SQX_Requirement__c = :currentREQs[1].Id
        ];
        System.assertEquals(1, step2pdjfs1.size(), 'Expected step 2 pdjfs count');
        System.assertEquals(psns[1].Id, step2pdjfs1[0].Personnel_Id__c, 'Expected personnel as psn 2');
        System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, step2pdjfs1[0].Training_Status__c, 'Expected training status for psn 2');
        System.assertEquals(firstStep + 1, step2pdjfs1[0].SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                            'Expected PJF Current Training Program Step for psn 2');
        
        // ARRANGE: complete 2nd step training for 2nd personnel
        step2pdjfs1[0].SQX_Personnel_Document_Training__r.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
        step2pdjfs1[0].SQX_Personnel_Document_Training__r.Completion_Date__c = System.today();
        update step2pdjfs1[0].SQX_Personnel_Document_Training__r;
        
        // list 2nd step completed training id for 2nd personnel
        psn2PdtIds.add(step2pdjfs1[0].SQX_Personnel_Document_Training__c);
        
        // ASSERT: ensure 2nd personnel completes all training for the training program
        System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_CURRENT,
                            [SELECT Training_Status__c FROM SQX_Personnel_Job_Function__c WHERE Id = :currentPJFs[1].Id].Training_Status__c,
                            'Expected all trainings to be completed for psn 2');
        
        // ARRANGE: deactivate training program
        SQX_BulkifiedBase.clearAllProcessedEntities();
        jf.Active__c = false;
        update jf;
        
        if (useBatchJobForTrainingGeneration) {
            // ARRANGE: run training deactivation batch job when needed
            deactivatedReqProc.processBatch([
                SELECT Id,
                    SQX_Requirement__c,
                    Controlled_Document_Id__c
                FROM SQX_Personnel_Document_Job_Function__c
                WHERE Is_Archived__c = false
                    AND Active_Job_Function__c = false
                    AND SQX_Requirement__r.Active__c = false
                    AND SQX_Requirement__r.Training_Job_Status__c = :SQX_Requirement.TRAINING_JOB_STATUS_DEACTIVATION_PENDING
                ORDER BY SQX_Personnel_Document_Training__c NULLS FIRST, SQX_Requirement__c
            ]);
        }
        
        // ASSERT: ensure there are no pending trainings for both personnels
        Integer psn1PdjfsCount = 0, psn2PdjfsCount = 0;
        for (SQX_Personnel_Document_Job_Function__c pdjf : [SELECT Id, Personnel_Id__c, SQX_Requirement__c, SQX_Personnel_Document_Training__c, Training_Status__c,
                                                                   SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                                                                   SQX_Personnel_Document_Training__r.Status__c
                                                            FROM SQX_Personnel_Document_Job_Function__c]) {
            if (pdjf.Personnel_Id__c == psns[0].Id) {
                psn1PdjfsCount++;
                System.assertEquals(SQX_Personnel_Document_Training.STATUS_OBSOLETE, pdjf.Training_Status__c, 'Expected only obsolete trainings to exist for psn 1');
            }
            if (pdjf.Personnel_Id__c == psns[1].Id) {
                psn2PdjfsCount++;
                System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf.Training_Status__c, 'Expected only completed trainings to exist for psn 2');
            }
        }
        System.assertEquals(1, psn1PdjfsCount, 'Expected training count for psn 1');
        System.assertEquals(2, psn2PdjfsCount, 'Expected training count for psn 2');
        
        // ARRANGE: add new personnel job functions and document requirements similar to prior steps
        currentPJFs = createPJFs(jf, psns);
        currentREQs = createREQs(jf, docs, firstStep);
        sobjs.clear();
        sobjs.addAll((List<SObject>)currentPJFs);
        sobjs.addAll((List<SObject>)currentREQs);
        insert sobjs;
        
        // ACT: reacivate training program with both personnel and document requirements with same training program steps
        SQX_BulkifiedBase.clearAllProcessedEntities();
        jf.Active__c = true;
        update jf;
        
        if (useBatchJobForTrainingGeneration) {
            // ACT: run training generation batch job when needed
            activatedReqProc.processBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(currentREQs[0].Id));
            activatedReqProc.processBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(currentREQs[1].Id));
        }
        
        // ASSERT: ensure trainings are generated as expected for both personnel
        psn1PDJFs.clear();
        psn2PDJFs.clear();
        for (SQX_Personnel_Document_Job_Function__c pdjf : [SELECT Id, Personnel_Id__c, SQX_Requirement__c, SQX_Personnel_Document_Training__c, Training_Status__c,
                                                                   SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                                                                   SQX_Personnel_Job_Function__r.Training_Status__c,
                                                                   SQX_Personnel_Document_Training__r.Status__c
                                                            FROM SQX_Personnel_Document_Job_Function__c
                                                            WHERE SQX_Personnel_Job_Function__c IN :currentPJFs]) {
            if (pdjf.Personnel_Id__c == psns[0].Id) {
                psn1PDJFs.add(pdjf);
            }
            if (pdjf.Personnel_Id__c == psns[1].Id) {
                psn2PDJFs.add(pdjf);
                System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf.Training_Status__c, 'Expected training to be complete for psn 2');
                System.assertEquals(psn2PdtIds[psn2PDJFs.size() - 1], pdjf.SQX_Personnel_Document_Training__c, 'Expected already completed training to be used for psn 2');
            }
        }
        // asserts for psn 1
        System.assertEquals(1, psn1PDJFs.size(), 'Expected pdjfs count for psn 1');
        System.assertNotEquals(null, psn1PDJFs[0].SQX_Personnel_Document_Training__c, 'Expected training to be generated for psn 1');
        System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, psn1PDJFs[0].Training_Status__c, 'Expected training status for psn 1');
        System.assertEquals(firstStep, psn1PDJFs[0].SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                            'Expected PJF Current Training Program Step to be 1st step for psn 1');
        System.assertEquals(currentREQs[0].Id, psn1PDJFs[0].SQX_Requirement__c, 'Training should be generated for first step training for psn 1');
        System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_PENDING,
                            psn1PDJFs[0].SQX_Personnel_Job_Function__r.Training_Status__c,
                            'Expected PJF Training Status for psn 2');
        // asserts for psn 2
        System.assertEquals(currentREQs.size(), psn2PDJFs.size(), 'Expected pdjfs count for psn 2');
        System.assertEquals(firstStep + 1, psn2PDJFs[0].SQX_Personnel_Job_Function__r.Current_Training_Program_Step__c,
                            'Expected PJF Current Training Program Step to be last step for psn 2');
        System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_CURRENT,
                            psn2PDJFs[0].SQX_Personnel_Job_Function__r.Training_Status__c,
                            'Expected PJF Training Status for psn 2');
        
        Test.stopTest();
    }
    
    /**
    * runs test case for training program reactivation with training generation batch job OFF
    */
    testmethod static void givenBatchJobTrainingGenerationIsOFF_TrainingProgramIsReactivatedWithPreviousStepsAndPersonnels_TrainingsAreProperlyGenerated() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('admin')) {
            // execute test case with training generation batch job setting off
            runJobFunctionReactivationTestCase(false,0);
        }
    }
    
    /**
    * runs test case for training program reactivation with training generation batch job ON
    */
    testmethod static void givenBatchJobTrainingGenerationIsON_TrainingProgramIsReactivatedWithPreviousStepsAndPersonnels_TrainingsAreProperlyGenerated() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('admin')) {
            // execute test case with training generation batch job setting on
            runJobFunctionReactivationTestCase(true,0);
        }
    }
    
    /**
     * runs test case for training program reactivation with training generation batch job ON first step of requirement is more than zero say 1
     */
    testmethod static void givenBatchJobTrainingGenerationIsON_TrainingProgramIsReactivatedWithNewStepsAndSamePersonnels_TrainingsAreProperlyGenerated() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('admin')) {
            // execute test case with training generation batch job setting on and starting step is 1
            runJobFunctionReactivationTestCase(true, 1);
         }
     }
}
