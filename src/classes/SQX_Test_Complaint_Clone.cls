/**
 * Test class to perform tests related to Complaint cloning
*/
@isTest
public class SQX_Test_Complaint_Clone {

    @testsetup
    static void setup() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'standardUser');

        Account supplier;
        Contact contactA;
        Contact contactB;
        System.runas(adminUser){

            // adding this picklist value to ensure Type is set in complaint contact
            SQX_Picklist_Value__c complaintContactCustomer = new SQX_Picklist_Value__c(Name = 'Customer', Value__c = 'Customer', Category__c = 'Complaint Contact Type');
            insert complaintContactCustomer;

            //create new Account and Contacts related to it to associate with complaint
            supplier = SQX_Test_Account_Factory.createAccount();
            contactA = SQX_Test_Account_Factory.createContactObject(supplier);

            // setting additional values to ensure that the values are transferred on clone
            contactA.FirstName = 'Something';
            contactA.Email = 'Something@something.com';
            contactA.Phone = '12345668798';

            insert contactA;

            contactB = SQX_Test_Account_Factory.createContact(supplier);
        }

        System.runAs(standardUser) {

            // creating random parts
            SQX_Part__c part = SQX_Test_Part.insertParts(1).get(0);

            // create a complaint record
            SQX_Test_Complaint tComplaint = new SQX_Test_Complaint(adminUser);
            tComplaint.complaint.SQX_Part__c = part.Id;
            tComplaint.complaint.SQX_External_Contact__c = contactA.Id;
            tComplaint.complaint.SQX_Account__c = supplier.Id;
            tComplaint.complaint.Report_Source__c = 'Customer';
            tComplaint.complaint.Method_of_Contact__c = 'Email';
            tComplaint.complaint.Patient_Age__c = 30;
            tComplaint.complaint.Send_Return_Hazard_Kit__c = true;
            tComplaint.save();

            SQX_BulkifiedBase.clearAllProcessedEntities();

            // add one more contact
            tComplaint.complaint.SQX_External_Contact__c = contactB.Id;
            tComplaint.save();

            // create an associated item and assign it to the complaint above
            SQX_Complaint_Associated_Item__c item = new SQX_Complaint_Associated_Item__c(
                                                        SQX_Complaint__c = tComplaint.complaint.Id,
                                                        SQX_Part_Family__c = part.Part_Family__c,
                                                        Manufacturing_Date__c = Date.today(),
                                                        Expiration_Date__c = Date.today(),
                                                        SQX_Part__c = part.Id);
            insert item;

            // create a script execution record for the given complaint
            SQX_Script_Execution__c scriptExec = new SQX_Script_Execution__c(SQX_Complaint__c = tComplaint.complaint.Id);
            insert scriptExec;
        }

    }


    /**
     *  Returns the default inclusion fieldset for the given sobject type
     */
    private static String getCloneInclusionFieldSetFieldsForQuery(SObjectType objType) {

        FieldSet fs = objType.getDescribe().fieldsets.getMap().get(SQX_Clone_Records.DEFAULT_FIELD_INCLUSION_FIELDSET);

        return SQX_DynamicQuery.getFieldList(fs, null);
    }


    /**
     * Common method to assert that the fields are getting transferred on clone
     */
    private static void assertFieldValuesGotTransferred(SObject existingRecord, SObject newRecord, String concatenatedFieldList) {
        assertFieldValuesGotTransferred(existingRecord, newRecord, concatenatedFieldList, new List<String>());
    }

    /**
     * Common method to assert that the fields are getting transferred on clone
     */
    private static void assertFieldValuesGotTransferred(SObject existingRecord, SObject newRecord, String concatenatedFieldList, List<String> fieldsToIgnore) {
        fieldsToIgnore.add('Id');
        for(String field : concatenatedFieldList.split(',')) {
            field = field.trim();
            if(fieldsToIgnore.contains(field) || field.startsWith('toLabel') || field.contains('.')) {
                continue;
            }
            System.assertEquals(existingRecord.get(field), newRecord.get(field), 'Expected {field} to be transferred on clone of {objType}'.replace('{field}', field).replace('{objType}', existingRecord.getSObjectType().getDescribe().getName()));
        }
    }


    /**
     *  GIVEN: Complaint Record
     *  WHEN: It is cloned (with Copy Associated and Script flags set to false)
     *  THEN: Cross Reference Complaint is the only entity that is created along with the new complaint record
     */
    testmethod static void givenComplaintRecord_WhenItIsCloned_ThenDesiredFieldValuesAreTransferredAndCrossReferenceComplaintIsCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Create a complaint

            String cloneFieldSetFieldsForComplaint = getCloneInclusionFieldSetFieldsForQuery(SQX_Complaint__c.SObjectType);
            String cloneFieldSetFieldsForContact = getCloneInclusionFieldSetFieldsForQuery(SQX_Complaint_Contact__c.SObjectType);

            String complaintQuery = 'SELECT {fields}, compliancequest__Description__c FROM SQX_Complaint__c LIMIT 1'.replace('{fields}', cloneFieldSetFieldsForComplaint);

            SQX_Complaint__c existingComplaint = (SQX_Complaint__c) Database.query(complaintQuery).get(0);

            String complaintContactQuery = 'SELECT {fields} FROM SQX_Complaint_Contact__c WHERE SQX_Complaint__c = \'{id}\' ORDER BY FIRST_NAME__c'.replace('{fields}', cloneFieldSetFieldsForContact).replace('{id}', existingComplaint.Id);

            // following is not an external contact
            List<SQX_Complaint_Contact__c> existingComplaintContacts =  Database.query(complaintContactQuery);
            existingComplaintContacts.get(0).Note__c = 'Just a good note';
            existingComplaintContacts.get(1).Note__c = 'HELLONOTE';
            update existingComplaintContacts;
            // ACT : Clone
            Test.startTest();

            SQX_Complaint__c newComplaint = new SQX_Complaint__c();
            newComplaint.SQX_Parent_Complaint__c = existingComplaint.Id;
            newComplaint.Description__c = 'My new description';
            insert newComplaint;

            // ASSERT : New complaint has to be created with values transferred
            String newComplaintQuery = 'SELECT {fields}, compliancequest__Description__c FROM SQX_Complaint__c WHERE Id = \'{id}\''.replace('{fields}', cloneFieldSetFieldsForComplaint).replace('{id}', newComplaint.Id);
            newComplaint = (SQX_Complaint__c) Database.query(newComplaintQuery).get(0);

            assertFieldValuesGotTransferred(existingComplaint, newComplaint, cloneFieldSetFieldsForComplaint);
            // negative test - ensure that fields that are not in fieldset are not transferred over
            System.assertNotEquals(existingComplaint.Description__c, newComplaint.Description__c, 'Expected Description not to be transferred on clone but got transferred');
            System.assertEquals('My new description', newComplaint.Description__c, 'Description set from UI got overwriten');

            System.assertEquals(1, [SELECT Id FROM SQX_Cross_Reference_Complaint__c WHERE SQX_Complaint__c =: newComplaint.Id
                                    AND SQX_Related_Complaint__c =: existingComplaint.Id].size(), 'Expected exactly 1 cross reference complaint record to be found after clone');
            System.assertEquals(0, [SELECT Id FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c =: newComplaint.Id].size(), 'No associated items should have been transferred');
            System.assertEquals(0, [SELECT Id FROM SQX_Complaint_Script_Execution_Link__c WHERE SQX_Complaint__c =: newComplaint.Id].size(), 'No script execution should have been linked');

            // complaint contacts have to be created as well
            String newComplaintContactsQuery = 'SELECT {fields} FROM SQX_Complaint_Contact__c WHERE SQX_Complaint__c = \'{id}\' ORDER BY FIRST_NAME__c'.replace('{fields}', cloneFieldSetFieldsForContact).replace('{id}', newComplaint.Id);
            List<SQX_Complaint_Contact__c> contactsInNewComplaint = (List<SQX_Complaint_Contact__c>) Database.query(newComplaintContactsQuery);

            System.assertEquals(2, contactsInNewComplaint.size(), 'Expected exactly 2 complaint contacts to be transferred to the new complaint record on clone');

            // complaint contact will be automatically created for external contact, we need to check if complaint contact was created for the other contact
            SQX_Complaint_Contact__c nonExternalContactInNewComplaint;
            for(integer i=0; i<contactsInNewComplaint.size();i++) {
                assertFieldValuesGotTransferred(existingComplaintContacts.get(i), contactsInNewComplaint.get(i), cloneFieldSetFieldsForContact, new List<String> { SQX_Complaint_Contact__c.SQX_Complaint__c.getDescribe().getName() });
            }
            // ensure desired values are transferred

            Test.stopTest();

        }
    }

    /**
     *  GIVEN: Complaint Record
     *  WHEN: It is cloned with Copy Associated Item flag set to true
     *  THEN: Cross Reference Complaint and Associated Items are created along with the new complaint record
     */
    testmethod static void givenComplaintRecord_WhenItIsClonedWithCopyAssociatedItemFlagChecked_ThenNewComplaintShouldHaveNewAssociatedItem() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Create a complaint
            SQX_Complaint__c existingComplaint = [SELECT Id, SQX_Part__c, (SELECT Id FROM SQX_Associated_Items__r) FROM SQX_Complaint__c LIMIT 1];

            String cloneFieldSetFieldsForAI = getCloneInclusionFieldSetFieldsForQuery(SQX_Complaint_Associated_Item__c.SObjectType);

            String queryForAI = 'SELECT {fields} FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c = \'{id}\''.replace('{fields}', cloneFieldSetFieldsForAI).replace('{id}', existingComplaint.Id);

            SQX_Complaint_Associated_Item__c existingAI = (SQX_Complaint_Associated_Item__c) Database.query(queryForAI).get(0);

            // ACT : Clone
            Test.startTest();

            SQX_Complaint__c newComplaint = new SQX_Complaint__c();
            newComplaint.SQX_Parent_Complaint__c = existingComplaint.Id;
            newComplaint.Copy_Associated_Item__c= true;
            newComplaint.SQX_Part__c = existingComplaint.SQX_Part__c;
            insert newComplaint;

            // ASSERT : New complaint has to be created with values transferred
            System.assertEquals(1, [SELECT Id FROM SQX_Cross_Reference_Complaint__c WHERE SQX_Complaint__c =: newComplaint.Id
                                                                                AND SQX_Related_Complaint__c =: existingComplaint.Id].size(), 'Expected exactly 1 cross reference complaint record to be found after clone');
            System.assertEquals(0, [SELECT Id FROM SQX_Complaint_Script_Execution_Link__c WHERE SQX_Complaint__c =: newComplaint.Id].size(), 'No script execution should have been linked');

            String queryFornewAI = 'SELECT {fields} FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c = \'{id}\''.replace('{fields}', cloneFieldSetFieldsForAI).replace('{id}', newComplaint.Id);

            List<SQX_Complaint_Associated_Item__c> clonedItems = (List<SQX_Complaint_Associated_Item__c>) Database.query(queryFornewAI);
            System.assertEquals(1, clonedItems.size(), 'Expected exaclty one new associated item to have been created');

            SQX_Complaint_Associated_Item__c clonedItem = clonedItems.get(0);
            System.assertEquals(false, clonedItem.Is_Primary__c, 'Did not expect the associated item to be primary since it wasnt primary in the primary complaint');

            assertFieldValuesGotTransferred(existingAI, clonedItem, cloneFieldSetFieldsForAI, new List<String> { SQX_Complaint_Contact__c.SQX_Complaint__c.getDescribe().getName() });

            // ACT : Make item primary and create a clone of the complaint

            SQX_BulkifiedBase.clearAllProcessedEntities();

            clonedItem.Make_Primary__c = true;  // make primary
            update clonedItem;

            System.assertEquals(true, [SELECT Is_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id =: clonedItem.Id].Is_Primary__c, 'Expected item to have been made primary by the previous action');

            // create a clone of the new complaint and copy primary associated item
            newComplaint.SQX_Parent_Complaint__c = newComplaint.Id;
            newComplaint.Copy_Associated_Item__c = true;
            newComplaint.Id = null;
            insert newComplaint;

            // ASSERT : The newly created complaint should have a primary associated item
            System.assertEquals(true, [SELECT Is_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c =: newComplaint.Id].Is_Primary__c, 'Expected the newly create complaint to have a primary associated item');

            Test.stopTest();

        }
    }


    /**
     *  GIVEN: Complaint Record
     *  WHEN: It is cloned with Copy Script Execution flag set to true
     *  THEN: Cross Reference Complaint and Script Execution Items are created along with the new complaint record
     */
    testmethod static void givenComplaintRecord_WhenItIsClonedWithCopyScriptExecutionFlagChecked_ThenNewComplaintShouldHaveScriptExecutionLinked() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Create a complaint
            SQX_Complaint__c existingComplaint = [SELECT Id, Complaint_Title__c FROM SQX_Complaint__c LIMIT 1];
            SQX_Script_Execution__c scriptExec = [SELECT Id FROM SQX_Script_Execution__c WHERE SQX_Complaint__c =: existingComplaint.Id];

            // ACT : Clone
            Test.startTest();

            SQX_Complaint__c newComplaint = new SQX_Complaint__c();
            newComplaint.SQX_Parent_Complaint__c = existingComplaint.Id;
            newComplaint.Copy_Associated_Item__c = true;
            newComplaint.Copy_Script_Execution__c = true;
            newComplaint.Complaint_Title__c = 'My new title';

            insert newComplaint;

            // ASSERT : New complaint has to be created with values transferred
            System.assertEquals(1, [SELECT Id FROM SQX_Cross_Reference_Complaint__c WHERE SQX_Complaint__c =: newComplaint.Id
                                    AND SQX_Related_Complaint__c =: existingComplaint.Id].size(),
                                    'Expected exactly 1 cross reference complaint record to be found after clone');
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c =: newComplaint.Id].size(),
                                    'No associated items should have been transferred');
            List<SQX_Complaint_Script_Execution_Link__c> compExecLinks = [  SELECT Id FROM SQX_Complaint_Script_Execution_Link__c
                                                                            WHERE SQX_Complaint__c =: newComplaint.Id AND SQX_Script_Execution__c =: scriptExec.Id];
            System.assertEquals(1, compExecLinks.size(),
                                    'No script execution have been linked');
            
            
            // ACT (SQX-8211): update SQX_Complaint_Script_Execution_Link__c
            update compExecLinks;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Complaint_Script_Execution_Link__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Complaint_Script_Execution_Link__c.SObjectType);

            // additional test
            // ensure that even though the field is in fieldset, if the field has been set in the UI, the UI value should be retained
            System.assertEquals('My new title', newComplaint.Complaint_Title__c, 'Title got overwritten');

            Test.stopTest();

        }
    }



    /**
     *  GIVEN: Complaint Record and (non-primary) Associated Item
     *  WHEN: Clone from associated item is initiated
     *  THEN: New Complaint record is created with a primary associated item
     */
    testmethod static void givenComplaintRecordAndNonPrimaryAssociatedItem_WhenCloneFromAssociatedItemIsInvoked_ThenNewComplaintIsCreatedWithPrimaryAssociatedItem() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Create a complaint and an associated item
            SQX_Complaint__c existingComplaint = [SELECT Id, SQX_Part__c FROM SQX_Complaint__c LIMIT 1];

            SQX_Part__c part = SQX_Test_Part.insertParts(1).get(0);
            SQX_Complaint_Associated_Item__c nonPrimaryItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = existingComplaint.Id,
                                            SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id);
            insert nonPrimaryItem;

            System.assertEquals(false, [SELECT Is_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id =: nonPrimaryItem.Id].Is_Primary__c, 'Expected item to be non-primary intially but wasn\'t the case');


            // ACT : Clone from associated item
            Test.startTest();

            SQX_Complaint__c newComplaint = new SQX_Complaint__c();
            newComplaint.SQX_Parent_Complaint__c = existingComplaint.Id;
            newComplaint.Copy_Associated_Item__c = true;
            newComplaint.SQX_Part__c = existingComplaint.SQX_Part__c;
            newComplaint.SQX_Cloned_From_Associated_Item__c = nonPrimaryItem.Id;   // clone from associated item

            insert newComplaint;

            // ASSERT : New complaint has to be created along with two associated items among which one is primary
            System.assertEquals(1, [SELECT Id FROM SQX_Cross_Reference_Complaint__c WHERE SQX_Complaint__c =: newComplaint.Id
                                                                                AND SQX_Related_Complaint__c =: existingComplaint.Id].size(), 'Expected exactly 1 cross reference complaint record to be found after clone');
            System.assertEquals(0, [SELECT Id FROM SQX_Complaint_Script_Execution_Link__c WHERE SQX_Complaint__c =: newComplaint.Id].size(), 'No script execution should have been linked');

            List<SQX_Complaint_Associated_Item__c> clonedItems = [SELECT Id, Is_Primary__c, SQX_Part__c FROM SQX_Complaint_Associated_Item__c WHERE SQX_Complaint__c =: newComplaint.Id];
            System.assertEquals(2, clonedItems.size(), 'Expected exaclty two new associated items to be created');

            for(SQX_Complaint_Associated_Item__c clonedItem : clonedItems) {
                System.assertEquals(clonedItem.SQX_Part__c == nonPrimaryItem.SQX_Part__c, clonedItem.Is_Primary__c, 'Expected item to be primary if the item was created after cloning complaint from it\'s parent item');
            }

            Test.stopTest();

        }
    }

}