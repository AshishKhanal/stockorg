/**
* This class tests the source for Document Management related items
* @author  Anuj Bhandari
* @date    17-04-2017
*/
public with sharing class SQX_Homepage_Source_DocumentMgmt_Items extends SQX_Homepage_ItemSource {


    /*
        Includes: Documents Needing Attention and Draft items
    */
    private String module = SQX_Homepage_Constants.MODULE_TYPE_DOCUMENT_MANAGEMENT;

    private User loggedInUser;

    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_DocumentMgmt_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    }


    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Controlled_Document__c.SObjectType => new List<SObjectField> 
            {
                SQX_Controlled_Document__c.Name,
                SQX_Controlled_Document__c.Title__c,
                SQX_Controlled_Document__c.Document_Status__c,
                SQX_Controlled_Document__c.Approval_Status__c,
                SQX_Controlled_Document__c.CreatedDate,
                SQX_Controlled_Document__c.OwnerId
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
     *   returns a Listof homepage items for the current user related to Controlled Document Module
     */
    protected override List<SObject> getUserRecords() {

        List<String> docStatuses = new List<String>
        {
            SQX_Controlled_Document.STATUS_DRAFT,
            SQX_Controlled_Document.STATUS_APPROVED
        };

        List<SQX_Controlled_Document__c> items = [
                                                    SELECT Id,
                                                            Name,
                                                            Title__c,
                                                            Document_Status__c,
                                                            Approval_Status__c,
                                                            CreatedDate,
                                                            OwnerId
                                                    FROM SQX_Controlled_Document__c
                                                    WHERE Document_Status__c IN: docStatuses
                                                    AND OwnerID IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                ];

        return items;
    }

    /**
     *   Method returns a SQX_Homepage_Item type item from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        return getDocumentItem((SQX_Controlled_Document__c) item);

    }

    /**
     *   Method returns a SQX_Homepage_Item with correct values of controlled documents of different statuses
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getDocumentItem(SQX_Controlled_Document__c item) {

        SQX_Homepage_Item tItem = new SQX_Homepage_Item();

        tItem.itemId = item.Id;

        tItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        tItem.moduleType = this.module;

        tItem.actions = getNeedingAttentionActions(tItem.itemId);

        tItem.creator = loggedInUser;

        if (item.Document_Status__c == SQX_Controlled_Document.STATUS_DRAFT) {

            if (item.Approval_Status__c == SQX_Controlled_Document.APPROVAL_REJECTED) {

                tItem.feedText = getRejectedDraftDocumentFeedText(item);

                // set action type
                tItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

            } else {

                // set feed text for the current Draft document
                tItem.feedText = getDraftDocumentFeedText(item);

                // this item falls into the Draft Items List
                tItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;


            }

        } else {

            // set feed text for the Approved documents item
            tItem.feedText = getApprovedDocumentFeedText(item);

            // set action type
            tItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

        }

        return tItem;

    }

    /**
     *   Returns the action associated with needing action items
     *   @param itemId - id of the record for which action is to be set
     */
    private List<SQX_Homepage_Item_Action> getNeedingAttentionActions(string itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);
        pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List<SQX_Homepage_Item_Action> { action };
    }


    /**
     *   Method returns the Draft feed text for the given item
     */
    private String getDraftDocumentFeedText(SQX_Controlled_Document__c item) {
        return getDocumentFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_DRAFT, item);
    }

    /**
     *   Method returns Approval feed text for the given item
     */
    private String getApprovedDocumentFeedText(SQX_Controlled_Document__c item) {
        return getDocumentFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_APPROVED, item);
    }

    /**
     *   Method returns Rejected feed text for the given item
     */
    private String getRejectedDraftDocumentFeedText(SQX_Controlled_Document__c item) {
        return getDocumentFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_REJECTED, item);
    }

    private String getDocumentFeedText(String template, SQX_Controlled_Document__c item)
    {
        return String.format(template, new String[] {
             item.Name,
             item.Title__c
        });
    }

}
