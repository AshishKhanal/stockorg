/**
* @author Sagar Shrestha
* @date 2014/5/12
* @description This is a helper class that provides primitive functionality to setup and test CAPA 
*/
@IsTest
public class SQX_Test_CAPA_Utility extends SQX_Test_Base_Class{

    public SQX_Test_Finding finding;
    public SQX_CAPA__c CAPA;

    /**
    * @author Sagar Shrestha
    * @date 2014/5/12
    * @description creates a CAPA object with finding already defined
    */
    public SQX_Test_CAPA_Utility(){
        this.finding  = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();
                            
        
        //creates a CAPA with action approver as current user
        this.CAPA= prepareMockCAPA(finding.supplier, finding.finding, null, finding.primaryContact.Id);
    }

    /**
    * creates a CAPA object for testing of with given user
    * @author Sagar Shrestha
    * @date 2014/5/12
    * @param user Action Approver is set as this user
    */
    public SQX_Test_CAPA_Utility(User user){
        this.finding  = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();

        this.CAPA= prepareMockCAPA(finding.supplier, finding.finding,user.Id, finding.primaryContact.Id);
    }
    
    /**
    * creates an internal CAPA object for testing of with given implementation approver and capa coordinator
    * @author Pradhanta Bhandari
    * @date 2014/8/13
    * @param actionApprover the user who is responsible for approving the action
    * @param capaCoordinator the user responsible for managing/responding to the CAPA.
    */
    public SQX_Test_CAPA_Utility(User actionApprover, User capaCoordinator){
        this.finding  = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType, false)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();

        this.CAPA = prepareMockCAPA(capaCoordinator, finding.finding, actionApprover.Id);
    }

    public SQX_Test_CAPA_Utility(Boolean withoutFinding, User actionApprover, User capaCoordinator) {
        System.assert(withoutFinding, 'Please use other override for creating nc with finding. Parameter kept only to provide override until migration is complete');
        this.CAPA = prepareMockCAPA(capaCoordinator, null, actionApprover.Id);
    }
    

    public SQX_CAPA__c prepareMockCAPA(Account account, SQX_Finding__c finding, string userID){
        return prepareMockCAPA(account, finding, userID, null);
    }

    /**
    * Creates a mock CAPA 
    * @author Sagar Shrestha
    * @date 2014/5/12
    * @param account CAPA Supplier is set as this account
    * @param finding CAPA primary Finding is set as this account
    * @param userID CAPA Action Approver is set as this User
    */

    public SQX_CAPA__c prepareMockCAPA(Account account, SQX_Finding__c finding, string userID, Id primaryContact)
    {
         SQX_CAPA__c CAPA = new SQX_CAPA__c(
            Issued_Date__c=Date.today(),
            Target_Due_Date__c=Date.today(),
            SQX_Account__c =account.Id,
            SQX_Finding__c= (finding != null ? finding.Id : null),
            SQX_Action_Approver__c=userId,
            SQX_External_Contact__c = primaryContact,
            CAPA_Type__c = SQX_CAPA.TYPE_SUPPLIER,
            SQX_Investigation_Approver__c = finding.SQX_Investigation_Approver__c,
            Response_Required__c = true,
            Containment_Required__c = true,
            Investigation_Required__c = true,
            Corrective_Action_Required__c = true,
            Preventive_Action_Required__c = true,
            Investigation_Approval__c = true
            );

         return CAPA;
    }
    
    /**
    * Creates an Internal CAPA
    * @param capaCoordinator the user who will be handling the CAPA equivalent to External contact for a SCAR
    * @param finding the underlying finding
    * @param userId CAPA ActionApprover is set as this User
    */
    public SQX_CAPA__c prepareMockCAPA(User capaCoordinator, SQX_Finding__c finding, String userID)
    {
         SQX_CAPA__c CAPA = new SQX_CAPA__c(
            Issued_Date__c=Date.today(),
            Target_Due_Date__c=Date.today().addDays(30),
            SQX_Finding__c= finding != null ? finding.Id : null,
            SQX_Action_Approver__c=userId,
            SQX_CAPA_Coordinator__c = capaCoordinator.Id,
            CAPA_Type__c = SQX_CAPA.TYPE_INTERNAL,
            SQX_Investigation_Approver__c = finding != null ? finding.SQX_Investigation_Approver__c : capaCoordinator.Id
            );

         return CAPA;
    }

    /**
    * @author Sagar Shrestha
    * @date 2014/5/12
    * @description save (update/insert) the CAPA to the data store 
    */
    public SQX_Test_CAPA_Utility save(){
        upsert CAPA;

        return this;
    }


    /**
     * Given CAPA without completion date
     * When tasks are completed i.e. has response, investigation and containment etc
     * Then completion date is set correctly
     */
    static testmethod void givenCAPAWithoutCompletionDate_WhenTaskssAreCompleted_ThenCompletionDateIsSet() {
        SQX_CAPA.Bulkified capaBulk = new SQX_CAPA.Bulkified();
        SQX_CAPA__c capa = new SQX_CAPA__c(Has_Response__c = true);

        //Act: Invoke the method to set completion dates
        capaBulk.setCompletionDates(capa);

        //Assert: Ensure that completion date of response is set
        System.assertEquals(Date.Today(), capa.Completion_Date_Response__c);
        System.assertEquals(null, capa.Completion_Date_Investigation__c);

        //Arrange: Set has investigation completion and has containment
        capa.Has_Investigation__c = true;
        capa.Has_Containment__c = true;

        // Act: Invoke method to set completion dates
        capaBulk.setCompletionDates(capa);

        // Assert: ensure that completion date of containment and investigation is set
        System.assertEquals(Date.Today(), capa.Completion_Date_Containment__c);
        System.assertEquals(Date.Today(), capa.Completion_Date_Investigation__c);


    }

    /**
     * Given CAPA with completion date
     * When tasks are completed i.e. has response, investigation and containment etc
     * Then completion date is not updated
     */
    static testmethod void givenCAPAWithCompletionDate_WhenTaskssAreCompleted_ThenCompletionDateIsntSet() {
        SQX_CAPA.Bulkified capaBulk = new SQX_CAPA.Bulkified();
        SQX_CAPA__c capa = new SQX_CAPA__c(Has_Response__c = true, Completion_Date_Response__c = Date.today().addDays(-2),
            Has_Investigation__c = true, Completion_Date_Investigation__c = Date.today().addDays(-3),
            Has_Containment__c = true, Completion_Date_Containment__c = Date.today().addDays(-4));

        //Act: Invoke the method to set completion dates
        capaBulk.setCompletionDates(capa);

        //Assert: Ensure that completion date of response is set
        System.assertEquals(Date.Today().addDays(-2), capa.Completion_Date_Response__c);
        System.assertEquals(Date.Today().addDays(-3), capa.Completion_Date_Investigation__c);
        System.assertEquals(Date.Today().addDays(-4), capa.Completion_Date_Containment__c);
    }
}