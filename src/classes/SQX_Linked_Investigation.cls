/**
 * Trigger handler class for SQX_Linked_Investigation object.
 */
public with sharing class SQX_Linked_Investigation {

    /**
     * This method checks for scop match between complaint and investigation and update Complaint with Primary Investigation.
     */
    public static void checkScopeMatchBetweenComplaintAndInvestigationAndUpdateComplaint(List<SQX_Investigation__c> investigations, List<SQX_Complaint__c> complaints) {
        for (SQX_Investigation__c investigation : investigations) {
            for (SQX_Complaint__c complaint : complaints) {
                // only update primary investigation if it does not exist
                if ( complaint.SQX_Primary_Investigation__c == null
                        && investigation.SQX_Part_Family__c != null && investigation.SQX_Defect_Code__c != null
                        && investigation.SQX_Part_Family__c == complaint.SQX_Part_Family__c && investigation.SQX_Defect_Code__c == complaint.SQX_Complaint_Code__c) {
                    
                    complaint.SQX_Primary_Investigation__c = investigation.Id;

                    // if part exists then check for part match too
                    if (investigation.SQX_Part__c != null && investigation.SQX_Part__c != complaint.SQX_Part__c) {
                        complaint.SQX_Primary_Investigation__c = null;
                    }
                }
            }
        }

        /*
         * update complaint
         * #########################
         *  WITHOU SHARING USED
         * #########################
         * user may not have edit permission on complait or associated item.
         */
        new SQX_DB().withoutSharing().op_update(complaints, new List<Schema.SObjectField> {Schema.SQX_Complaint__c.SQX_Primary_Investigation__c});
    }

    /**
     * This method checks for scop match between associated item and investigation and update AI with Primary Investigation.
     */
    public static void checkScopeMatchBetweenAssociatedImtemAndInvestigationAndUpdateAI(List<SQX_Investigation__c> investigations, List<SQX_Complaint_Associated_Item__c> items) {
        for (SQX_Investigation__c investigation : investigations) {
            for (SQX_Complaint_Associated_Item__c ai : items) {
                if (ai.SQX_Primary_Investigation__c == null 
                        && investigation.SQX_Part_Family__c != null && investigation.SQX_Defect_Code__c != null
                        && investigation.SQX_Part_Family__c == ai.SQX_Part_Family__c && investigation.SQX_Defect_Code__c == ai.SQX_Complaint__r.SQX_Complaint_Code__c) {
                    
                    ai.SQX_Primary_Investigation__c = investigation.Id;

                    // Note : we don't need to update complaint with primary investigaion in case of AI, as we already have a process builder which make complaint in sync with AI if AI is primary
                    // process builder name is 'CQ Complaint Associated Item - Synchronize Complaint With Associated Item'
                    // if part exists then check for part match too
                    if (investigation.SQX_Part__c != null && investigation.SQX_Part__c != ai.SQX_Part__c) {
                        ai.SQX_Primary_Investigation__c = null;
                    }
                }
            }
        }

        /*
         * update associated items.
         * #########################
         *  WITHOU SHARING USED
         * #########################
         * user may not have edit permission on complait or associated item.
         */
        new SQX_DB().withoutSharing().op_update(items, new List<Schema.SObjectField> {Schema.SQX_Complaint_Associated_Item__c.SQX_Primary_Investigation__c});
    }
    
    /**
     * This method notifies Complaint that its primary investigation is being closed.
     */
    public static void notifyAboutPrimaryInvestigationCompletion(Set<Id> closedInvestigationIds) {
        SQX_Complaint.notifyAboutPrimaryInvestigationCompletion(closedInvestigationIds);
    }
    /**
     * Inner class to perform database operation in bulk.
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        /**
         * Default constructor for the bulkified base.
         */
        public Bulkified(List<SQX_Linked_Investigation__c> newLinkedInvestigations, Map<Id, SQX_Linked_Investigation__c> oldLinkedInvestigations){
            super(newLinkedInvestigations, oldLinkedInvestigations);
        }
        
        /**
         * 
         * Removes reference of Primary Investigation from Complaint and Associated Item.
         * before delete
         */
        public Bulkified removeLinkOfInvestigation() {
            final String ACTION_NAME = 'removeLinkOfInvestigation';
            Rule deleteRule = new Rule();
            deleteRule.addRule(Schema.SQX_Linked_Investigation__c.Is_Primary__c, RuleOperator.Equals, true);
            this.resetView()
                .removeProcessedRecordsFor(SQX.LinkedInvestigation, ACTION_NAME)
                .applyFilter(deleteRule, RuleCheckMethod.OnCreateAndEveryEdit);
                        
            if (deleteRule.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.LinkedInvestigation, ACTION_NAME, deleteRule.evaluationResult);
                Set<Id> complaintIds = getIdsForField(deleteRule.evaluationResult, Schema.SQX_Linked_Investigation__c.SQX_Complaint__c);
                Set<Id> associatedItemIds = getIdsForField(deleteRule.evaluationResult, Schema.SQX_Linked_Investigation__c.SQX_Associated_Item__c);
                
                List<SQX_Complaint__c> complaints = new List<SQX_Complaint__c>();
                List<SQX_Complaint_Associated_Item__c> associatedItems = new List<SQX_Complaint_Associated_Item__c>();
                
                // update ai                    
                for (Id associatedItemId : associatedItemIds) {
                    associatedItems.add(new SQX_Complaint_Associated_Item__c(Id = associatedItemId, SQX_Primary_Investigation__c = null));
                }
                
                // update complaint
                complaints = [SELECT Id, SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id IN :complaintIds AND (SQX_Primary_Associated_Item__c IN :associatedItemIds OR SQX_Primary_Associated_Item__c = null)];
                for (SQX_Complaint__c complaint : complaints) {
                    complaint.SQX_Primary_Investigation__c = null;
                }
                
                /*
                 *  WITHOU SHARING USED
                 * #########################
                 * user may not have edit permission on complait or associated item.
                 */
                new SQX_DB().withoutSharing().op_update(complaints, new List<Schema.SObjectField> {Schema.SQX_Complaint__c.SQX_Primary_Investigation__c});
                
                new SQX_DB().withoutSharing().op_update(associatedItems, new List<Schema.SObjectField> {Schema.SQX_Complaint_Associated_Item__c.SQX_Primary_Investigation__c});
                
            }
            return this;
        }

        /*
         * when new Linked Investigation is created, value is set in Unique Investigation field
         * which is the concatenation of Complaint, Investigation and Association Item
         * SQX-7990 by Tribhuvan Maharjan
         */
        public Bulkified setUniqueInvestigation(){
            this.resetView();
            
            for(SQX_Linked_Investigation__c linInvestigations : (List<SQX_Linked_Investigation__c>)this.view){
                linInvestigations.Unique_Investigation__c = ''+linInvestigations.compliancequest__SQX_Complaint__c + 
                    + linInvestigations.compliancequest__SQX_Investigation__c + 
                    + linInvestigations.compliancequest__SQX_Associated_Item__c;
            }
            return this;
        }
    }
}