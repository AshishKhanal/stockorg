/**
 * Test class for validating Supplier Escalation closure functionality
 **/ 
@isTest
public class SQX_Test_Supplier_Escalation_Close {
    
    // Map of supplier escalation fields and values to set and compare with new escalation
    private static Map<SObjectField, String> fieldValuesToSet = new Map<SObjectField, String>{
        SQX_Supplier_Escalation__c.Org_Business_Unit__c => 'Sample Business Unit',
            SQX_Supplier_Escalation__c.Org_Division__c => 'Sample Division',
            SQX_Supplier_Escalation__c.Org_Region__c => 'Sample Region',
            SQX_Supplier_Escalation__c.Org_Site__c => 'Sample Site',
            SQX_Supplier_Escalation__c.Related_Part_Numbers__c => 'Part 1, Part2, Part 3',
            SQX_Supplier_Escalation__c.Subject__c => 'Test Escalation'
            };
                
   	private static String someRandomString = 'Some Random String';
                
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
        User anotherStandardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'anotherStandardUser'); 
        
        System.runAs(standardUser) {

            // create account record
            Account account = new Account(
                Name = SQX_Test_Supplier_Escalation.ACCOUNT_NAME 
            );
            insert account;
            
            //Create contact for Account
            insert new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = account.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
                        
            createNewEsalationAndReference(standardUser, SQX_Supplier_Common_Values.STATUS_COMPLETE, SQX_Supplier_Common_Values.STAGE_VERIFICATION);
        }
    }
    
    /**
     * Method for getting all field values for a given sobject type
     **/ 
    public static List<SObject> getRecords(SObjectType type, String fieldName, Object value){
        
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.field = fieldName;
        filter.operator = 'eq';
        filter.value = value;

        return SQX_DynamicQuery.getAllFields(type.getDescribe(), new Set<SObjectField>(), filter, new List<SObjectField>(), false);
    }
    
    /**
     * Method to create new supplier escalation and reference
     **/ 
    private static void createNewEsalationAndReference(User standardUser, String status, String stage){
        SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
        for(SObjectField field: fieldValuesToSet.keySet()){
            escalation.supplierEscalation.put(field, fieldValuesToSet.get(field));
        }
        
        escalation.supplierEscalation.Rationale_for_Escalation__c = someRandomString;
        escalation.supplierEscalation.Further_Action_Rationale__c = someRandomString;
        escalation.supplierEscalation.Escalation_Level__c = '2';
        escalation.supplierEscalation.SQX_Supplier_Liaison__c = standardUser.Id;
        
        escalation.supplierEscalation.Status__c = status;
        escalation.supplierEscalation.Record_Stage__c = stage;
        escalation.save();
        SQX_BulkifiedBase.clearAllProcessedEntities();
        
        SQX_Escalation_Reference__c reference = new SQX_Escalation_Reference__c(SQX_Supplier_Escalation__c = escalation.supplierEscalation.Id, 
                                                                                RecordTypeId = Schema.SObjectType.SQX_Escalation_Reference__c.getRecordTypeInfosByDeveloperName().get('Other_Reference').getRecordTypeId(), 
                                                                                Other_Reference__c = someRandomString,
                                                                                Comment__c = someRandomString);
        
        new SQX_DB().op_insert(new List<SQX_Escalation_Reference__c>{reference}, new List<SObjectField>{});
        
    }
    
    /*
     * GIVEN: New supplier escalation in complete status
     * WHEN: Record is escalated
     * THEN: New escalation record is created with target escalation
     */
    static testmethod void whenRecordIsEscalated_ThenNewEscalatedRecordIsCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            validateEscalationDeEscalation(standardUser, SQX_Supplier_Common_Values.ACTIVITY_CODE_ESCALATE, '3');
        }
    }
    
    /*
     * GIVEN: New supplier escalation in complete status
     * WHEN: Record is deescalated
     * THEN: New escalation record is created with target escalation
     */
    static testmethod void whenRecordIsDeescalated_ThenNewDeescalatedRecordIsCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            validateEscalationDeEscalation(standardUser, SQX_Supplier_Common_Values.ACTIVITY_CODE_DEESCALATE, '1');
        }
    }
    
    /*
     * GIVEN: New supplier escalation in complete status
     * WHEN: Record is deescalated to zero target escalation
     * THEN: No new escalation is created
     */
    static testmethod void whenRecordIsDeescalatedToZero_ThenNoNewRecordIsCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            validateEscalationDeEscalation(standardUser, SQX_Supplier_Common_Values.ACTIVITY_CODE_DEESCALATE, '0');
        }
    }
    
    /**
     * Method to validate new escalation and reference
     **/ 
    private static void validateEscalationDeEscalation(User standardUser, String activityCode, String targetEscalation){
        
        // Arrange: Create new escalation
        SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
        escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
        escalation.supplierEscalation.Target_Escalation_Level__c = targetEscalation;
        escalation.supplierEscalation.Activity_Code__c = activityCode;
        escalation.supplierEscalation.Is_Locked__c = true;
        escalation.save();
        SQX_BulkifiedBase.clearAllProcessedEntities();
        escalation.synchronize();
        
        Account account = [SELECT SQX_Supplier_Escalation__c, Supplier_Escalation_Level__c FROM Account WHERE Name =: SQX_Test_Supplier_Escalation.ACCOUNT_NAME LIMIT 1];
        
        if(SQX_Supplier_Escalation.CLOSED_ESCALATION_LEVELS.contains(targetEscalation)){
            System.assert(escalation.supplierEscalation.SQX_Further_Action__c == null, 'Further Action should be blank');
            System.assert(account.SQX_Supplier_Escalation__c == null, 'Account\'s Active Supplier Escalation should be cleared when deescalated to 0');
        }else{
            System.assert(escalation.supplierEscalation.SQX_Further_Action__c != null, 'Further Action should be set');
            
            System.assertEquals(escalation.supplierEscalation.SQX_Further_Action__c, account.SQX_Supplier_Escalation__c, 'Account\'s Active EScalation should be updated to new escalation');
            
            List<SQX_Supplier_Escalation__c> escalations = getRecords(escalation.supplierEscalation.getSObjectType(), 'Id', escalation.supplierEscalation.SQX_Further_Action__c);
            
            System.assertEquals(escalations.size(), 1, 'One new supplier escalation should be created while escalating');
            
            for(SObjectField field: fieldValuesToSet.keySet()){
                System.assertEquals(fieldValuesToSet.get(field), escalations[0].get(field));
            }
            System.assertEquals(escalation.supplierEscalation.SQX_Account__c, escalations[0].SQX_Account__c);
            System.assertEquals(escalation.supplierEscalation.SQX_Supplier_Contact__c, escalations[0].SQX_Supplier_Contact__c);
            System.assertEquals(escalation.supplierEscalation.Target_Escalation_Level__c, escalations[0].Escalation_Level__c);
            System.assertEquals(escalation.supplierEscalation.Id, escalations[0].SQX_Source_Escalation__c);
            System.assertEquals(someRandomString+' '+someRandomString, escalations[0].Rationale_for_Escalation__c);
            System.assertEquals(escalation.supplierEscalation.SQX_Supplier_Liaison__c, escalations[0].SQX_Supplier_Liaison__c);
            System.assertEquals(escalation.supplierEscalation.Part_Name__c, escalations[0].Part_Name__c);
            
            List<SQX_Escalation_Reference__c> priorEscalationReference = getRecords(SQX_Escalation_Reference__c.SObjectType, SQX.NSPrefix + 'SQX_Supplier_Escalation__c', escalation.supplierEscalation.Id);
            
            List<SQX_Escalation_Reference__c> escalationReferences = getRecords(SQX_Escalation_Reference__c.SObjectType, SQX.NSPrefix + 'SQX_Supplier_Escalation__c', escalations[0].Id);
            
            System.assertEquals(escalationReferences.size(), 1, 'One escalation reference should be copied over to new escalation');
            
            System.assertEquals(priorEscalationReference[0].RecordTypeId, escalationReferences[0].RecordTypeId);
            System.assertEquals(someRandomString, escalationReferences[0].Other_Reference__c);
            System.assertEquals(null, escalations[0].Activity_Code__c,'Activity code is not copied to new escalated');
            System.assertEquals(null, escalations[0].Target_Escalation_Level__c,'Target escalation level is not copied');
            System.assertNotEquals(escalation.supplierEscalation.Target_Escalation_Level__c,escalations[0].Target_Escalation_Level__c,'Target escalation level must not be copied');
        }
    }
    
    /*
     * GIVEN: new supplier escalation in complete status with escalation reference
     * WHEN: escalation reference is deleted
     * THEN: no error is thrown
     */
    static testmethod void whenEscalationIsNotClosed_ThenReferenceCanBeDeleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in complete status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            List<SQX_Escalation_Reference__c> reference = [SELECT Id, Other_Reference__c FROM SQX_Escalation_Reference__c 
                                                           WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            
            // Act: Delete the related reference
            Database.DeleteResult result =  new SQX_DB().continueOnError().op_delete(reference)[0];
            
            // Assert: Record should be deleted successfully
            System.assert(result.isSuccess(), 'No error should be thrown while deleting references from completed escalation but has error. '+result.getErrors());
        }
    }
    
    /*
     * GIVEN: new supplier escalation in complete status with escalation reference
     * WHEN: escalation reference is updated
     * THEN: no error is thrown
     */
    static testmethod void whenEscalationIsNotClosed_ThenReferenceCanBeUpdated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in complete status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            List<SQX_Escalation_Reference__c> reference = [SELECT Id, Other_Reference__c FROM SQX_Escalation_Reference__c 
                                                           WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            
            // Act: Update the related reference
            reference[0].Other_Reference__c = 'Updated Reference';
            Database.SaveResult result =  new SQX_DB().continueOnError().op_update(reference, new List<SObjectField>{})[0];
            
            // Assert: Record should be updated successfully
            System.assert(result.isSuccess(), 'No error should be thrown while updating references from completed escalation but has error. '+result.getErrors());
        }
    }
    
    /*
     * GIVEN: new supplier escalation in closed status with escalation reference
     * WHEN: escalation reference is deleted
     * THEN: Error is thrown
     */
    static testmethod void whenEscalationIsClosed_ThenReferenceCannotBeDeleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in closed status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.supplierEscalation.Is_Locked__c = true;
            escalation.escalate('3');
            
            // Act: Delete the related reference
            List<SQX_Escalation_Reference__c> reference = [SELECT Id FROM SQX_Escalation_Reference__c WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            Database.DeleteResult result =  new SQX_DB().continueOnError().op_delete(reference)[0];
            
            // Assert: Record should not be saved
            System.assert(!result.isSuccess(), 'Escalation Reference should not be deleted after escalation is closed');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), Label.SQX_ERR_REFERENCE_CANNOT_BE_DELETED), 'Expected error: ' + Label.SQX_ERR_REFERENCE_CANNOT_BE_DELETED + ' Actual Errors: ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: new supplier escalation in void status with escalation reference
     * WHEN: escalation reference is deleted
     * THEN: Error is thrown
     */
    static testmethod void whenEscalationIsVoid_ThenReferenceCannotBeDeleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in void status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.voidEscalation();
            
            // Act: Delete the related reference
            List<SQX_Escalation_Reference__c> reference = [SELECT Id FROM SQX_Escalation_Reference__c WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            Database.DeleteResult result =  new SQX_DB().continueOnError().op_delete(reference)[0];
            
            // Assert: Record should not be saved
            System.assert(!result.isSuccess(), 'Escalation Reference should not be deleted after escalation is closed');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), Label.SQX_ERR_REFERENCE_CANNOT_BE_DELETED), 'Expected error: ' + Label.SQX_ERR_REFERENCE_CANNOT_BE_DELETED + ' Actual Errors: ' + result.getErrors());
        }
    }

    /*
     * GIVEN: new supplier escalation in void status with escalation reference
     * WHEN: escalation reference is deleted
     * THEN: Error is thrown
     */    
    static testmethod void whenEscalationIsClosed_ThenReferenceCannotBeUpdated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in closed status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.supplierEscalation.Is_Locked__c = true;
            escalation.escalate('3');
            
            // Act: Update the related reference
            List<SQX_Escalation_Reference__c> reference = [SELECT Id, Other_Reference__c FROM SQX_Escalation_Reference__c 
                                                           WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            
            reference[0].Other_Reference__c = 'Updated Reference';
            Database.SaveResult result =  new SQX_DB().continueOnError().op_update(reference, new List<SObjectField>{})[0];
            
            // Assert: Record should not be updated
            System.assert(!result.isSuccess(), 'Escalation Reference should not be updated after escalation is closed');
            String errorMsg = 'Escalation Reference cannot be inserted or updated after Supplier Escalation is closed or void.';
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMsg), 'Expected error: ' + errorMsg + ' Actual Errors: ' + result.getErrors());
        }
    }

    /*
     * GIVEN: new supplier escalation in void status with escalation reference
     * WHEN: escalation reference is updated
     * THEN: Error is thrown
     */   
    static testmethod void whenEscalationIsVoid_ThenReferenceCannotBeUpdated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation with reference in void status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.voidEscalation();
            
            List<SQX_Escalation_Reference__c> reference = [SELECT Id, Other_Reference__c FROM SQX_Escalation_Reference__c 
                                                           WHERE SQX_Supplier_Escalation__c =: escalation.supplierEscalation.Id];
            
            // Act: Update the related reference
            reference[0].Other_Reference__c = 'Updated Reference';
            Database.SaveResult result =  new SQX_DB().continueOnError().op_update(reference, new List<SObjectField>{})[0];
            
            // Assert: Record should not be updated
            System.assert(!result.isSuccess(), 'Escalation Reference should not be updated after escalation is void');
            String errorMsg = 'Escalation Reference cannot be inserted or updated after Supplier Escalation is closed or void.';
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMsg), 'Expected error: ' + errorMsg + ' Actual Errors: ' + result.getErrors());

        }
    }

    /*
     * GIVEN: new supplier escalation in void status
     * WHEN: new escalation reference is inserted
     * THEN: Error is thrown
     */   
    static testmethod void whenEscalationIsVoid_ThenReferenceCannotBeInserted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation in void status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.voidEscalation();
            
            // Act: Add new escalation reference
            SQX_Escalation_Reference__c reference = new SQX_Escalation_Reference__c(SQX_Supplier_Escalation__c = escalation.supplierEscalation.Id, 
                                                                                    RecordTypeId = Schema.SObjectType.SQX_Escalation_Reference__c.getRecordTypeInfosByDeveloperName().get('Other_Reference').getRecordTypeId(), 
                                                                                    Other_Reference__c = someRandomString,
                                                                                    Comment__c = someRandomString);
            
            Database.SaveResult result =  new SQX_DB().continueOnError().op_insert(new List<SQX_Escalation_Reference__c>{reference}, new List<SObjectField>{})[0];
            
            // Assert: Record should not be inserted
            System.assert(!result.isSuccess(), 'Escalation Reference should not be inserted after escalation is void');
            String errorMsg = 'Escalation Reference cannot be inserted or updated after Supplier Escalation is closed or void.';
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMsg), 'Expected error: ' + errorMsg + ' Actual Errors: ' + result.getErrors());

        }
    }
    
    /*
     * GIVEN: new supplier escalation in closed status
     * WHEN: new escalation reference is inserted
     * THEN: Error is thrown
     */   
    static testmethod void whenEscalationIsClosed_ThenReferenceCannotBeInserted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: Create new supplier escalation in void status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c WHERE SQX_Supplier_Liaison__c =: standardUser.Id];
            escalation.supplierEscalation.Is_Locked__c = true;
            escalation.escalate('3');
            
            // Act: Add new escalation reference
            SQX_Escalation_Reference__c reference = new SQX_Escalation_Reference__c(SQX_Supplier_Escalation__c = escalation.supplierEscalation.Id, 
                                                                                    RecordTypeId = Schema.SObjectType.SQX_Escalation_Reference__c.getRecordTypeInfosByDeveloperName().get('Other_Reference').getRecordTypeId(), 
                                                                                    Other_Reference__c = someRandomString,
                                                                                    Comment__c = someRandomString);
            
            Database.SaveResult result =  new SQX_DB().continueOnError().op_insert(new List<SQX_Escalation_Reference__c>{reference}, new List<SObjectField>{})[0];
            
            // Assert: Record should not be inserted
            System.assert(!result.isSuccess(), 'Escalation Reference should not be inserted after escalation is closed');
            String errorMsg = 'Escalation Reference cannot be inserted or updated after Supplier Escalation is closed or void.';
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), errorMsg), 'Expected error: ' + errorMsg + ' Actual Errors: ' + result.getErrors());

        }
    }
}