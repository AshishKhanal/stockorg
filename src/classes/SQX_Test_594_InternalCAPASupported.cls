/**
 * This test class ensures that given an internal CAPA, the basics are done right.
 * a) A CAPA Coordinator can be specified.
 * b) A task is assigned to CAPA Coordinator
 */
@isTest
private class SQX_Test_594_InternalCAPASupported {

    static testMethod void givenAnInternalCAPAIsCreated_ItGetsCreatedAndTaskIsAssigned() {
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
        	 capaCoordinator = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        	 
        //1. Issue the capa as capa Owner
        System.runas(capaOwner){
        	SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility(true, capaCoordinator, capaCoordinator);
        	
        	//a. create the CAPA and ensure that it can be saved
        	Database.SaveResult result = Database.insert(capa.CAPA,false);
        	
        	System.assert(result.isSuccess(), 'Internal CAPA could not be created by ' + capaOwner);
        	
        	
        	//b. initiate the CAPA
        	SQX_CAPA__c openCAPA = new SQX_CAPA__c (Id = capa.CAPA.Id, Status__c = SQX_Capa.STATUS_OPEN);
        	
        	result = Database.update(openCAPA, false);
        	
        	System.assert(result.isSuccess(), 'Internal CAPA could not be opened by ' + capaOwner);
        	
        	
        	//c. ensure that a task has been created for CAPA Coordinator
        	System.assert(SQX_Task_Template.taskExistsFor(openCapa, capaCoordinator.Id, SQX_Task_Template.TaskType.CAPAOpened) == true,
        		'Expected an CAPA open task exists for CAPA coordinator but found none');
        }
    }
}