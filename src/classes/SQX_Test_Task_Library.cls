@IsTest
public class SQX_Test_Task_Library {
    
    private static integer TaskCount = 1;

   	public SQX_Task__c task;
    
    public enum TaskType { ComplaintsDecisionTree, ComplaintsInvestigation,  ComplaintsPHR, ComplaintsSampleRequest }
    
    /**
     *  default constructor creates a task of decision tree type. We don't actually have any requirement to make it default
     */
    public SQX_Test_Task_Library(){
        task = createTask(SQX_Test_Task_Library.TaskType.ComplaintsDecisionTree);
    }
    
    /**
     * Create a task of specific task type
     */ 
   	public SQX_Test_Task_Library(TaskType taskType){
    	task = createTask(taskType);        
	}
    
    /*
     * actual method that has the logic for instantiation of task object
     */
    private SQX_Task__c createTask(TaskType taskType){
        SQX_Task__c task = new SQX_Task__c(Name = 'Task-' + TaskCount, Allowed_Days__c = 0);
        TaskCount++;
        
        task.Record_Type__c = 'Complaint'; //Right now all we have is complaint type, we could have made it conditional using the task type
        if(taskType == SQX_Test_Task_Library.TaskType.ComplaintsDecisionTree)
            task.Task_Type__c = 'Decision Tree';
        else if(taskType == SQX_Test_Task_Library.TaskType.ComplaintsInvestigation)
            task.Task_Type__c = 'Investigation';
        else if(taskType == SQX_Test_Task_Library.TaskType.ComplaintsPHR)
            task.Task_Type__c = 'PHR';
        else if(taskType == SQX_Test_Task_Library.TaskType.ComplaintsSampleRequest)
            task.Task_Type__c = 'Sample Request';
        
        return task;
    }
    
    
    
    /**
    * saves/updates the task object into task library
	*/
    public SQX_Test_Task_Library save(){
        if(this.task.Id == null){
            new SQX_DB().op_insert(new List<SObject>{this.task}, new List<SObjectField> { Schema.SQX_Task__c.Name, Schema.SQX_Task__c.Task_Type__c, Schema.SQX_Task__c.Record_Type__c });
        }
        else{
            new SQX_DB().op_update(new List<SObject>{this.task}, new List<SObjectField> { Schema.SQX_Task__c.Name, Schema.SQX_Task__c.Task_Type__c, Schema.SQX_Task__c.Record_Type__c });
        }
        
        return this;
    }
    
    //TODO: add support for decision tree, will be done as part of dtree's implementation
}