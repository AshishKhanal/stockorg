/**
 * Class to execute REST class to external systems
*/

public class SQX_External_REST_Service_Invoker {

    final String REQUEST_METHOD_POST = 'POST',  // make enum ?
                 REQUEST_METHOD_GET = 'GET';

    final Integer HTTP_RESPONSE_STATUS_OK = 200;

    public static final String HTTP_HEADER_CONTENT_TYPE = 'Content-Type';	// make enum ?

    public static final String CONTENT_TYPE_JSON = 'application/json';

    private Http http;

    private HttpRequest httpRequest;

    private HttpResponse httpResponse;

    public SQX_External_REST_Service_Invoker() {}

    /**
     * Invokes a HTTP GET service at the given endpoint
     * @param endPoint service to invoke
    */
    public String invokeGetService(String endPoint) {
        return invokeGetService(endPoint, null);
    }

    /**
     * Invokes a HTTP GET service at the given endpoint
     * @param endPoint service to invoke
     * @param headers HTTP Request Headers
    */
    public String invokeGetService(String endPoint, Map<String, String> headers) {
        return invokeService(endPoint, REQUEST_METHOD_GET, null, headers);
    }


    /**
     * Invokes a HTTP POST service at the given endpoint
     * @param endPoint service to invoke
     * @param bdody request body
    */
    public String invokePostService(String endPoint, String body) {
        return invokePostService(endPoint, body, null);
    }


    /**
     * Invokes a HTTP POST service at the given endpoint
     * @param endPoint service to invoke
     * @param body request body
     * @param headers request headers
    */
    public String invokePostService(String endPoint, String body, Map<String, String> headers) {
        return invokeService(endPoint, REQUEST_METHOD_POST, body, headers);
    }


    /**
     * Invokes an HTTP service at the given endpoint
     * @param endPoint service to invoke
     * @param requestMethod GET/POST
     * @param requestBody request body
     * @param headers HTTP Request Headers
    */
    private String invokeService(String endPoint, String requestMethod, String requestBody, Map<String, String> requestHeaders) {
        http = new Http();

        httpRequest = new HttpRequest();
        httpRequest.setEndpoint(endPoint);
        httpRequest.setMethod(requestMethod);

        if(requestBody != null) {
            httpRequest.setBody(requestBody);
        }

        if(requestHeaders != null) {
            for(String key : requestHeaders.keySet()) {
                httpRequest.setHeader(key, requestHeaders.get(key));
            }
        }

        httpResponse = http.send(httpRequest);

        if(httpResponse.getStatusCode() == HTTP_RESPONSE_STATUS_OK) {
            return httpResponse.getBody();
        }

        throw new SQX_ApplicationGenericException(httpResponse.getStatusCode() + ' : ' + httpResponse.getBody());
    }

}