/**
* CLass that creates report records based on custom metadata provided
*/
public with sharing class SQX_Create_Report {


    // Different types of report
    public static final String REPORT_TYPE_MEDWATCH='Medwatch',
                               REPORT_TYPE_MEDDEV='Meddev',
                               REPORT_TYPE_CANADA='Canada',
                               REPORT_TYPE_GENERAL='General';

    private static Map<String, SObject> REG_RECORD_MAP = new Map<String, SObject>
    {
        REPORT_TYPE_MEDWATCH => new SQX_Medwatch__c(),
        REPORT_TYPE_MEDDEV => new SQX_Meddev__c(),
        REPORT_TYPE_CANADA => new SQX_Canada_Report__c(),
        REPORT_TYPE_GENERAL => new SQX_General_Report__c()
    };

    /**
     *Creates report and links it with Regulatory Report.
     *
     *@param ids The list of ids of the regulatory report
     */
    @InvocableMethod(label='CQ Create Report' description='Creates report')
    public static List<Id> createReport(List<SQX_Create_Report_Configuration> configs){
        
        SavePoint sp;
        
        try{
            
            // Throw error in case multiple configurations are passed.
            if(configs.size() > 1){
                throw new SQX_ApplicationGenericException('Multiple configurations not allowed');   
            } 
            
            //Get the regulatory report
            SQX_Regulatory_Report__c regReport = [SELECT Id,Report_Name__c,Reg_Body__c
                                                  from SQX_Regulatory_Report__c where Id =: configs[0].reportId ];
            
            // Get the Report Setting from Custom Metadata Type
            CQ_Regulatory_Report_Setting__mdt reportSetting = new SQX_Regulatory_Report().getSubmissionSettingsForReport(configs[0].reportId);
            
            List<Id> recordIds=new List<Id>();
            
            sp = Database.setSavepoint();
            
            // If initial report is selected for the regulatory report, then create a final report for it.
            // Otherwise go with the default flow.
            if (!String.isBlank(configs[0].initialReportId)){
                // clone initial report
                recordIds = cloneInitialReport(configs);
                regReport.SQX_Initial_Regulatory_Report__c =configs[0].initialReportId;
            }
            else{
                recordIds= create(reportSetting,regReport);
            }
            
            // store the report id in the regulatory report.
            regReport.Report_Id__c =recordIds[0];
            new SQX_DB().op_update(new List<SObject> { regReport }, new List<SObjectField> {});
            
            
            return recordIds;
            
        }catch (Exception ex) {
            
            if(sp != null) {
                Database.rollback(sp);
            }
            
            throw ex;
        }
    }
    
    /**
     * Method that creates different reports based on Regulatory Report name
     * @param: reportSetting Contains the information about the report type.
     * @param: regReport Regulatory report for which the report is being created.
     */
    private static List<Id> create(CQ_Regulatory_Report_Setting__mdt reportSetting, SQX_Regulatory_Report__c regReport){
        
        String reportType = REPORT_TYPE_GENERAL;
    
        if(reportSetting != null && REG_RECORD_MAP.containsKey(reportSetting.Report_Type__c)) {
            reportType = reportSetting.Report_Type__c;
        }

        SObject report = REG_RECORD_MAP.get(reportType);
        report.clear();
        report.put('compliancequest__SQX_Regulatory_Report__c', regReport.Id);

        new SQX_DB().op_insert(new List<SObject> { report }, new List<SObjectField> {});
   
        return new List<Id>{report.Id};
    }
    
    /**
     * Clones the initial reports.
     * 
     * @param: reportConfigs Contains the id of the initial report.
     */
    private static List<Id> cloneInitialReport(List<SQX_Create_Report_Configuration> reportConfigs){
        
        SQX_Clone_Configuration cloneConfig = new SQX_Clone_Configuration();
                
        // Query the initial regualatory report
        SQX_Regulatory_Report__c initialRegReport = [SELECT Id, Report_Id__c FROM SQX_Regulatory_Report__c WHERE Id =: reportConfigs[0].initialReportId];
        
       
        
        // Necessary configuration for cloning the record.
        Map<String, String> jsonDefaultMap = new Map<String, String>();
        jsonDefaultMap.put('compliancequest__SQX_Regulatory_Report__c', reportConfigs[0].reportId);
        cloneConfig.jsonDefaults = JSON.Serialize(jsonDefaultMap);
        cloneConfig.ids = new List<String>{initialRegReport.Report_Id__c};
        cloneConfig.useInclusionFieldset=true;
        
        List<Id> ids = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{cloneConfig});
        return ids;
    }
}