/**
 * This test covers the most probable uses case of lightning layout class, such as getting the object describe correctly,
 * fetching the records works correctly.
 */
@IsTest
public class SQX_Test_Lightning_Helper {

    /**
     * Given: An Object Type without Record Types (CAPA in this case)
     * When: 'getDetails' method was invoked
     * Then: Information of SObject Type is Returned for Master type
     */
    testmethod static void givenObjectWithoutRecordTypes_WhenGetDetailsIsInvoked_ThenInformationOfMasterTypeIsReturned() {
        final String MASTER = 'Master';

        // Act: Get details for revise action for controlled document
        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = SQX_Lightning_Helper.getDetails(SQX.CAPA, null, 'revise', null);


        // Assert: Ensure that detail is correct
        System.assertEquals(1, detail.RecordTypes.size(), 'Master Record Type is returned');
        System.assertEquals(false, detail.HasRecordTypes);
        System.assertEquals(MASTER, detail.RecordTypes[0].Name);
    }

    /**
     * Given: An Object Type with Record Types (Controlled Document in this case)
     * When: 'getDetails' method was invoked
     * Then: Information of SObject Type is Returned for each type
     */
    testmethod static void givenObjectWithRecordTypes_WhenGetDetailsIsInvoked_ThenInformationOfSObjectTypeIsReturnedForEachType() {
        // Act: Get details for revise action for controlled document
        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = SQX_Lightning_Helper.getDetails(SQX.ControlledDoc, null, 'revise', null);


        // Assert: Ensure that detail is correct
        DescribeSObjectResult result = SQX_Controlled_Document__c.SObjectType.getDescribe();
        System.assertEquals(true, detail.HasRecordTypes);
        System.assertEquals(result.getRecordTypeInfos().size() - 1, detail.RecordTypes.size(), 'All but master record type must be returned');
        System.assertEquals(result.getLabel(), detail.ObjectLabel, 'Object label should be set to record name');
        Map<String, SObjectField> fields = result.fields.getMap();
        for(String field : fields.keySet()) {
            DescribeFieldResult fieldDesc = fields.get(field).getDescribe();
            if(!fieldDesc.isNillable()) {
                System.assert(detail.DBRequiredFields.contains(field), 'Required field ' + field + ' missing in returned result');
            }

            if(!fieldDesc.isUpdateable()) {
                System.assert(detail.DBReadFields.contains(field), 'Read field ' + field + ' missing in returned result');
            }
        }
    }


    /**
     * Given: An Object With Record Types
     * When: 'getDetail' is invoked with a default value (Document Number in Controlled Document)
     * Then: default object returned for each record type contains the values supplied in default map.
     */
    testmethod static void givenObjectWithRecordTypes_WhenGetDetailsIsInvokedWithDefault_ThenObjectReturnedHasDefaultValuesSet() {
        // Act: Get details for revise action for controlled document with default value document number
        SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c(
            Document_Number__c = 'Should Be this'
        );
        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = SQX_Lightning_Helper.getDetails(SQX.ControlledDoc, JSON.serialize(doc), 'revise', null);


        // Assert: Ensure that default value is set correctly
        for(SQX_Lightning_Helper.SQX_Save_UI_RecordType rType : detail.RecordTypes){
            System.assertEquals(doc.Document_Number__c, (String) rType.DefaultValue.get(SQX_Controlled_Document__c.Document_Number__c));
        }
    }

    /**
     * When: 'getDetail' is invoked with an invalid object or invalid default value (JSON String)
     * Then: error message is set appropriately
     */
    testmethod static void whenGetDetailsIsInvokedForInvalidObjectName_ThenErrorIsReturned() {
        // Act: Invoke get details with invalid object
        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = SQX_Lightning_Helper.getDetails('Invalid Object', null, 'revise', null);

        // Assert: ensure that error is set in detail object
        System.assertNotEquals(null, detail.Error);


        // Act: Invoke get details with invalid default value
        detail = SQX_Lightning_Helper.getDetails(SQX.ControlledDoc, '{Invalid JSON}', 'revise', null);

        // Assert: ensure that error is set in detail object
        System.assertNotEquals(null, detail.Error);
    }

    /**
     * Given: A SObject exists (Account exists)
     * When: 'getObjectWithId' is invoked with the Object Id and Fields
     * Then: The particular object with the field values is returned
     */
    testmethod static void givenSobjectAccount_WhenGetObjectWithId_ThenObjectWithFields() {
        Account account = new Account();
        account.Name = 'Burlington Textiles';
        insert account;

        // Act: Get Account with Name
        Account fetchedAccount = (Account) SQX_Lightning_Helper.getObjectWithId(account.Id, new String[] {'Id', 'Name'});

        // Assert: Ensure the name is same as one inserted
        System.assertEquals(account.Id, fetchedAccount.Id);
        System.assertEquals(account.Name, fetchedAccount.Name);
    }

    /**
     * Given: An existing SObject (account in this case)
     * When: Get Detail is Invoked with record id
     * Then: all default value returned has details of the existing record
     */
    testmethod static void givenSObjectAccount_WhenGetDetailWithIdIsInvoked_ThenDefaultValuesHaveFieldSetCorrectly() {
        Account acc = new Account();
        acc.Name = 'Burlington Textiles';
        insert acc;

        // Act: Get Account with Name
        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = SQX_Lightning_Helper.getDetails('Account', null, 'revise', acc.Id);

        // Assert: Ensure the name is same as one inserted
        for(SQX_Lightning_Helper.SQX_Save_UI_RecordType rType : detail.RecordTypes){
            System.assertEquals(acc.Name, (String) rType.DefaultValue.get(Account.Name));
            System.assertEquals(acc.Id, rType.DefaultValue.Id);
        }
    }


    /**
     * i. tesmethod for flow invocation should be added after NSI or anyother module is committed.
     * (Both default and invoke flow)
     * ii. Check for proper resource name selection
     */

    /* 
     * Test methods for SQX_VF_Kendo_Helper 
     * TODO: Invoke flow test remaining
     */

    /**
     * Given: SObject (Contact) has a reference id of Account
     * When: Details of the record are fetched
     * Then: Contact's Account reference field has Account set 
     */
    testmethod static void givenSObjectWithReferenceIdSet_WhenKendoHelperIsInvoked_ReferenceFieldIsSet() {
        //Arrange: Create an account to refer in Contact
        Account acct = new Account(Name = 'Test');
        insert acct;

        Contact ct = new Contact(AccountId = acct.Id);

        // Act: fetch the details to use in the UI
        SQX_VF_Kendo_Helper helper = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(ct));
        Map<String, Object> detail = (Map<String, Object>) JSON.deserializeUntyped(helper.getDetails());

        // Assert: Ensure that account field of contact has been initialized
        Map<String, Object> defaultVal = (Map<String,Object>) ((List<Object>)detail.get('RecordTypes')).get(0);
        Map<String, Object> readContact = (Map<String, Object>) JSON.deserializeUntyped((String) defaultVal.get('DefaultValueJSON'));
        System.assert(readContact.containsKey('Account'), 'Account field must be present in account' + detail);
        Map<String, Object> readContactAct = (Map<String, Object>) readContact.get('Account');
        System.assertEquals(acct.Id, (String)readContactAct.get('Id'), 'Reference field must be containing the same account');
    }

    /**
     * Given: an account
     * When: Url parameter is passed using cq convention when creating contact
     * Then: fields are set correctly 
     */
    testmethod static void givenAccount_WhenContactIsCreatedWithCQConvention_ReferenceFieldIsSet() {
        //Arrange: Create an account to refer in Contact
        Account acct = new Account(Name = 'Test');
        insert acct;

        Contact ct = new Contact();

        // Act: fetch the details to use in the UI with appropriate page reference set with cq name standard
        PageReference ref = new PageReference('/ref');
        ref.getParameters().put('cq_AccountId', acct.Id);
        ref.getParameters().put('cq_FirstName', 'Hello');
        ref.getParameters().put('cq_InvalidField', 'asdasd');
        Test.setCurrentPage(ref);
        SQX_VF_Kendo_Helper helper = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(ct));
        Map<String, Object> detail = (Map<String, Object>) JSON.deserializeUntyped(helper.getDetails());

        // Assert: Ensure that account field of contact has been initialized and name field has been set
        Map<String, Object> defaultVal = (Map<String,Object>) ((List<Object>)detail.get('RecordTypes')).get(0);
        Map<String, Object> readContact = (Map<String, Object>) JSON.deserializeUntyped((String) defaultVal.get('DefaultValueJSON'));
        System.assert(readContact.containsKey('Account'), 'Account field must be present in account' + detail);
        Map<String, Object> readContactAct = (Map<String, Object>) readContact.get('Account');
        System.assertEquals(acct.Id, (String)readContactAct.get('Id'), 'Reference field must be containing the same account');
        System.assertEquals('Hello', (String)readContact.get('FirstName'), 'Name should be set to hello');
    }

    /**
     * Given: New Contact
     * When: Page title is requested
     * Then: New SObject title is returned
     */
    testmethod static void givenNewContact_WhenContactIsCreatedAndTitleIsRequested_ThenTitleIsSetCorrectlyForCreate() {
        Contact ct = new Contact();

        // Act: fetch the title
        SQX_VF_Kendo_Helper helper = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(ct));
        String title = helper.getPageTitle();

        // Assert: Ensure that title is returned correctly
        System.assertEquals(Label.CQ_UI_New_SObject.replace('{0}', Contact.SObjectType.getDescribe().getLabel()), title);
    }

    /**
     * Given: Saved account
     * When: Page title is requested
     * Then: Edit Account Name title is returned
     */
    testmethod static void givenSavedAccount_WhenAccountIsEditedAndTitleIsRequested_ThenTitleIsSetCorrectlyForEdit() {
        //Arrange: Create an account
        Account act = new Account(Name = 'Test');
        insert act;

        // Act: get record type and page title
        SQX_VF_Kendo_Helper helper = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(act));
        String title = helper.getPageTitle();
        String objectType = helper.getObjRecordType();

        // Assert: Ensure that page title is set correctly
        System.assertEquals(Label.CQ_UI_Edit_SObject.replace('{0}', act.Name), title);
        System.assertEquals('Master', objectType);
    }

    /**
     * Given: Object with record type
     * When: Page title is requested
     * Then: New Onboarding step title is returned
     */
    testmethod static void givenOnboardingStep_WhenStepIsCreateddAndTitleIsRequested_ThenTitleIsSetCorrectlyForCreate() {

        SQX_Onboarding_Step__c onboardingStep = new SQX_Onboarding_Step__c();
        onboardingStep.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnBoardingStep, SQX_Steps_Trigger_Handler.RT_TASK);

        // Act: get record type and page title
        SQX_VF_Kendo_Helper helper = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(onboardingStep));
        String objectType = helper.getObjRecordType();
        String title = helper.getPageTitle();

        // Assert: Ensure that title and record type are returned correctly
        System.assertEquals('Task', objectType);
        System.assertEquals(Label.CQ_UI_New_SObject.replace('{0}', SQX_Onboarding_Step__c.SObjectType.getDescribe().getLabel()) + ' - CQ Task', title);
    }

    /**
     * Given: Changeset JSON
     * When: Save is executed
     * Then: Record is saved
     */
     testmethod static void givenChangeSet_WhenProcessActionIsCalled_ThenRecordIsSaved() {
        // Arrange: Create changeset for saving
        String accountName = 'Account-1234-asdasd';
        SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
        SQX_Test_JSONObject actObject = new SQX_Test_JSONObject('Account-1231', new Account(Name = accountName));
        changeSet.addToChangeSet(actObject);

        // Act: call process change set with actions
        SQX_VF_Kendo_Helper.processChangeSetWithAction('Account-1231', changeSet.toJSON(), new SObject[]{}, new Map<String, String>(), new SQX_OauthEsignatureValidation(), 'Account');

        // Assert: Ensure that an account is saved
        System.assertEquals(1, [SELECT Id FROM Account WHERE Name = :accountName].size());
     }
}