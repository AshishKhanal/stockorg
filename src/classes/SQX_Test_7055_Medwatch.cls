/**
 * Test Class for Medwatch records
 */
@isTest
public class SQX_Test_7055_Medwatch {
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }

    /**
     * Returns a new medwatch record with the required fields filled along with additional field value provided
     */
    static SQX_Medwatch__c getNewMedwatch(SObjectField field, Object value) {
        return getNewMedwatch(new Map<SObjectField, Object> { field => value });
    }

    /**
     * Returns a new medwatch record with the required fields filled along with additional values provided
     */
    static SQX_Medwatch__c getNewMedwatch(Map<SObjectField, Object> valuesByField) {
        SQX_Medwatch__c medwatch = new SQX_Medwatch__c(
            B5_Describe_event_or_problem__c = 'Prob',
            D2_a_Common_Device_Name__c = 'Cod',
            D2_b_Procode__c = 'Pro',
            Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE
        );

        if(valuesByField != null) {
            for(SObjectField field : valuesByField.keySet()) {
                medwatch.put(field, valuesByField.get(field));
            }
        }
            
        return medwatch;
    }

    
    /**
     * GIVEN: Medwatch record with valid and invalid Phone Number data
     * WHEN: Save the record
     * THEN: Phone field validation should work for record with invalid Phone Number data
     *          and record with valid Phone Number data should be saved successfully
     */  
    
    public static testmethod void ensurePhoneFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create valid, invalid phone number data 
            Map<SQX_Medwatch__c, Boolean> medwatchWithSaveResults = new Map<SQX_Medwatch__c, Boolean> {
                getNewMedwatch(SQX_Medwatch__c.E1_Phone_Number__c, '0123abc@#') => false,
                getNewMedwatch(SQX_Medwatch__c.F5_Phone_Number__c, 'Phone235') => false,
                getNewMedwatch(SQX_Medwatch__c.G2_Phone_Number__c, '98Qwe9879') => false,
                getNewMedwatch(SQX_Medwatch__c.E1_Phone_Number__c, '1(427)968-5943') => true,
                getNewMedwatch(SQX_Medwatch__c.F5_Phone_Number__c, '+1(427)968-5943x31827') => true,
                getNewMedwatch(SQX_Medwatch__c.G2_Phone_Number__c, '+444()8593282104') => true
            };
                
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssert(medwatchWithSaveResults);
        }
    }
    /**
     * GIVEN: Medwatch record with valid and invalid Postal Code data
     * WHEN: Save the record
     * THEN: Postal Code field validation should work thrown for record with invalid Postal Code data
     *          and record with valid Postal Code data should be saved successfully
     */
    public static testmethod void ensurePostalCodeFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create valid, invalid Postal Code data 
            Map<SQX_Medwatch__c, Boolean> medwatchWithSaveResults = new Map<SQX_Medwatch__c, Boolean> {
                getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Postal_Code__c , 'PC77%') => false,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Postal_Code__c , 'Postal##') => false,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Postal_Code__c , '8097!!') => false,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Postal_Code__c, 'ABCE???') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Postal_Code__c, '9879@@') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Site_Postal_Code__c , '98Qwe9879') => true,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Manufacturer_Postal_Code__c , '98Q(9879)') => false,
                    getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Postal_Code__c , 'PC 71') => true,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Postal_Code__c , '1234') => true,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Postal_Code__c , 'UK 44') => true,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Postal_Code__c , 'JPN77') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Postal_Code__c , 'arg') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Site_Postal_Code__c , '0123456789') => true,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Manufacturer_Postal_Code__c , 'Postal') => true
                    };
            
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssert(medwatchWithSaveResults);
        }
    }
    
    /**
     * GIVEN: Medwatch record with valid and invalid Zip Code & Zip Code Extension data
     * WHEN: Save the record
     * THEN: Zip Code & Zip Code Extension Validation should work for record with invalid Zip Code & Zip Code Extension data
     *          and record with valid Zip Code & Zip Code Extension data should be saved successfully
     */
    public static testmethod void ensureZipCodeAndZipCodeExtensionFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create valid, invalid Zip Code & Zip Code Extension data 
            Map<SQX_Medwatch__c, Boolean> medwatchWithSaveResults = new Map<SQX_Medwatch__c, Boolean> {
                getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Zip_Code__c, '666666') => false,
                    getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Zip_Code_Extension__c, '55555') => false,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Zip_Code__c, '1234') => false,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Zip_Code_Extension__c , '123') => false,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Zip_Code__c , '12AA') => false,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Zip_Code_Extension__c , '12AA') => false,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Mfr_Zip_Code__c , 'QQ34') => false,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Mfr_Zip_Code_Extension__c , 'QQ33') => false,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Zip_Code__c , 'aaQQ') => false,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Zip_Code_Extension__c , 'AAqq') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Zip_Code__c , 'kk89') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Zip_Code_Extension__c , 'kk77') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Site_Zip_Code__c , '1PT0') => false,
                    getNewMedwatch(SQX_Medwatch__c.G1_Manufacturing_Site_Zip_Code_Extension__c , '1PT0') => false,
                    getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Zip_Code__c , '12345') => true,
                    getNewMedwatch(SQX_Medwatch__c.D3_Mfr_Zip_Code_Extension__c , '1234') => true,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.D9_Reprocessor_Zip_Code_Extension__c , '4444') => true,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.E1_Initial_Reporter_Zip_Code_Extension__c , '4444') => true,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Mfr_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.F14_Device_Mfr_Zip_Code_Extension__c , '4444') => true,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.F3_User_Facility_Zip_Code_Extension__c , '4444') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Contact_Zip_Code_Extension__c , '4444') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Mfr_Site_Zip_Code__c , '55555') => true,
                    getNewMedwatch(SQX_Medwatch__c.G1_Manufacturing_Site_Zip_Code_Extension__c , '4444') => true
                    };
            
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssert(medwatchWithSaveResults);
        }
    }
    
    /**
     * GIVEN: Medwatch record with valid and invalid Date Data (Date of Birth, Date of Death, Date of Event)
     * WHEN: Save the record
     * THEN: Date field validation should work for record with invalid Date data
     *          and record with valid Date data should be saved successfully
     */
    public static testmethod void ensureDateFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create valid, invalid Date data 
            Map<SQX_Medwatch__c, Boolean> medwatchWithSaveResults = new Map<SQX_Medwatch__c, Boolean> {
                getNewMedwatch(SQX_Medwatch__c.B3_Date_of_Event__c , Date.today()+2) => false,
                    getNewMedwatch(SQX_Medwatch__c.A2_Date_of_Birth__c , Date.today()+20) => false,
                    getNewMedwatch(SQX_Medwatch__c.B2_Date_of_death__c , Date.today()+16) => false,
                    getNewMedwatch(SQX_Medwatch__c.B3_Date_of_Event__c , Date.today()) => true,
                    getNewMedwatch(SQX_Medwatch__c.A2_Date_of_Birth__c , Date.today()-10) => true,
                    getNewMedwatch(SQX_Medwatch__c.B2_Date_of_death__c , Date.today()-18) => true
                    };
            
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssert(medwatchWithSaveResults);
        }
    }

    /**
     * GIVEN: Medwatch record with valid and invalid Unit Field Data (Age, Age Unit, Weight, Weight Unit, Approximate Age of Device, Approximate Age Unit)
     * WHEN: Save the record
     * THEN: Unit field validation should work for record
     */
    public static testmethod void ensureUnitFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create valid, invalid Unit Field data 
            Map<SQX_Medwatch__c, Boolean> medwatchWithSaveResults = new Map<SQX_Medwatch__c, Boolean> {
                getNewMedwatch(SQX_Medwatch__c.A2_Age__c , 12) => false,
                    getNewMedwatch(SQX_Medwatch__c.A2_Age_Unit__c , 'YR') => false,
                    getNewMedwatch(SQX_Medwatch__c.A4_Weight__c , 200) => false,
                    getNewMedwatch(SQX_Medwatch__c.A4_Weight_Unit__c , 'lbs') => false,
                    getNewMedwatch(SQX_Medwatch__c.F9_Approximate_age_of_device__c , 5) => false,
                    getNewMedwatch(SQX_Medwatch__c.F9_Approximate_Age_Unit__c , 'YR') => false,
                    getNewMedwatch(new Map<SObjectField, Object> { SQX_Medwatch__c.A2_Age__c => 12, SQX_Medwatch__c.A2_Age_Unit__c => 'YR' }) => true,
                    getNewMedwatch(new Map<SObjectField, Object> { SQX_Medwatch__c.A4_Weight__c => 180, SQX_Medwatch__c.A4_Weight_Unit__c => 'lbs' }) => true,
                    getNewMedwatch(new Map<SObjectField, Object> { SQX_Medwatch__c.F9_Approximate_age_of_device__c => 7, SQX_Medwatch__c.F9_Approximate_Age_Unit__c => 'YR' }) => true
                    };

            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssert(medwatchWithSaveResults);
        }
    }
    
    /**
     * GIVEN: Suspect Product record with valid and invalid Unit Field Data (Dose, Dose Unit, Frequency, Frequency Unit)
     * WHEN: Save the record
     * THEN: Unit field validation should work for record
     */
    public static testmethod void ensureSuspectProductUnitFieldValidationWorks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            
            // Arrange: Create Medwatch record
            SQX_Medwatch__c medWatch = getNewMedwatch(null);
            medWatch.Activity_Code__c = null;
            insert medWatch;

            // Create Suspect Product with valid, invalid Unit Field data 
            Map<SQX_Medwatch_Suspect_Product__c, String> suspectProductWithSaveResults = new Map<SQX_Medwatch_Suspect_Product__c, String> {
                new SQX_Medwatch_Suspect_Product__c(C3_Dose__c = 12, SQX_Medwatch__c = medWatch.Id) => 'Dose Unit is required.',
                new SQX_Medwatch_Suspect_Product__c(C3_Dose_Unit__c = '001', SQX_Medwatch__c = medWatch.Id) => 'Dose is required.',
                new SQX_Medwatch_Suspect_Product__c(C3_Frequency__c = 200, SQX_Medwatch__c = medWatch.Id) => 'Frequency Unit is required.',
                new SQX_Medwatch_Suspect_Product__c(C3_Frequency_Unit__c = '804', SQX_Medwatch__c = medWatch.Id) => 'Frequency is required.'
            };
            insert new List<SQX_Medwatch_Suspect_Product__c>(suspectProductWithSaveResults.keySet());

            Test.startTest();

            // Act: Invoke validation
            medWatch.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            Database.SaveResult sr = new SQX_DB().continueOnError().op_update(new List<SQX_Medwatch__c> { medwatch }, new List<SObjectField> {}).get(0);

            // Assert: ensure that test results match with expected data when saving
            System.assertEquals(false, sr.isSuccess(), 'Expected validation to fail');
            
            String[] errMsgs = sr.getErrors()[0].getMessage().split(',');
            List<String> expectedErrMsgs = suspectProductWithSaveResults.values();
            for(String errMsg : errMsgs) {
                System.assert(expectedErrMsgs.contains(errMsg), 'Unexpected error message ' + errMsg);
            }

            Test.stopTest();
        }
    }

    /**
     * GIVEN: Medwatch with reporting site set to UF/Importer.
     * WHEN: Save the record and the for use by is not set to Importer.
     * THEN: Validation error should be thrown.
     */
    public static testmethod void givenMedwatchWithReportingSiteSetToUFImporter_whenMedwatchIsValidated_validationErrorShouldBeThrown(){
    		
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
        	
            // Arrange: Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            
            // Arrange : create all required records.
            SQX_Reporting_Default__c reporting = new SQX_Reporting_Default__c();
            reporting.Reporting_Site__c = 'User Facility/Importer Site';
            insert reporting;
            
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'US:30-Day MDR Test2018812';
            regulatoryReport.Report_Name__c = 'US:30-Day MDR';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.Reg_Body__c = 'FDA';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_Reporting_Default__c = reporting.Id;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            
            insert regulatoryReport;
            
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.B5_Describe_event_or_problem__c = 'asdasd';
            medwatch.B7_Other_Relevant_History__c= 'asdasd';
            medwatch.D1_Brand_Name__c='asdas';
            medwatch.D2_a_Common_Device_Name__c = 'asdas';
            medwatch.D2_b_Procode__c = 'as';
            
            // Arrange: Do not set the value to Importer.
            medwatch.F1_For_Use_By__c='';
            
            medwatch.SQX_Regulatory_Report__c =  regulatoryReport.Id;
            medwatch.Activity_Code__c= 'Validate';
            
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            medwatchList.add(medwatch);
            
            String errorMessage='';
            
            try{
           		//Act : insert the record;
            	insert medwatchList;
            }catch(Exception ex){
                errorMessage=ex.getMessage();
            }
            
            // Assert: The validation error should be thrown.
            System.assertEquals(true,errorMessage.contains('F1 - For Use By should be set to Importer.'));
        }
	}


    /**
     * Helper method to save (act) and assert Medwatch record with valid and invalid data
     */ 
    private static void actAndAssert(Map<SQX_Medwatch__c, Boolean>  data) {
        
        // ACT: Insert the records
        SQX_Medwatch__c[] mdws = new List<SQX_Medwatch__c>(data.keySet());
        Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(mdws, new List<Schema.SObjectField>{});
        data = data.clone();
        
        // Assert: Ensure that result matches
        for(Integer i = 0; i < saveResults.size(); i++) {
            SQX_Medwatch__c mdw = mdws[i];
            Database.SaveResult result = saveResults[i];
            
            System.assertEquals(data.get(mdw), result.isSuccess(), 'Failing on ' + mdw +  'Actual Result: ' + result);
        }
    }

}