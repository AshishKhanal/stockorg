/**
* Test for submission record creation of Medwatch Record.
*/
@IsTest
public class SQX_Test_Medwatch_SubmissionHistory {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
      }
    
    /**
     * GIVEN : A Medwatch Record
     * WHEN :  When the content version for the record is created.
     * THEN :  Submission history for the Medwatch record is created.
     */
    public testMethod static void givenMedwatch_WhenContentVersionIsInserted_SubmissionHistoryIsCreated(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
           
            //ARRANGE: Create a complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            //ARRANGE: Create regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);

            //ARRANGE: Create another regulatory report [ this additional report is being created to ensure that when submitting both the reports even if one of the reports fails the other should work]
            SQX_Regulatory_Report__c regReport2 = new SQX_Regulatory_Report__c();
            regReport2.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport2.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport2.SQX_Complaint__c=complaint.complaint.Id;
            regReport2.Due_Date__c= Date.today().addDays(3);

            insert new List<SQX_Regulatory_Report__c> { regReport, regReport2 };
            
            // ARRANGE : Medwatch is created
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.B5_Describe_event_or_problem__c='Event Description';
            medwatch.B7_Other_Relevant_History__c='Other relevant history';
            medwatch.D2_a_Common_Device_Name__c='Common Device Name';
            medwatch.D2_b_Procode__c='COD';
            medwatch.SQX_Regulatory_Report__c=regReport.Id;

            SQX_Medwatch__c medwatch2 = new SQX_Medwatch__c();
            medwatch2.B5_Describe_event_or_problem__c='Event Description';
            medwatch2.B7_Other_Relevant_History__c='Other relevant history';
            medwatch2.D2_a_Common_Device_Name__c='Common Device Name';
            medwatch2.D2_b_Procode__c='COD';
            medwatch2.SQX_Regulatory_Report__c=regReport2.Id;
            
            insert new List<SQX_Medwatch__c> { medwatch, medwatch2 };
            
            regReport.Report_Id__c = medwatch.Id;
            regReport2.Report_Id__c = medwatch2.Id;

            update new List<SQX_Regulatory_Report__c> { regReport, regReport2 };
                
             // Create file content that goes in attachment section for medwatch.
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='Medwatch-File';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with Medwatch');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // link it with medwatch
            ContentDocumentLink lnk = new ContentDocumentLink (LinkedEntityId =medwatch2.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED);
            
            insert lnk;

            Test.startTest();

            // Act: Create contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { medwatch.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=medwatch.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;
            
            // Assert: Submission history for Mewdatch needs to be created.
            SQX_Submission_History__c subHistory = [SELECT Id, SQX_Medwatch__c, Status__c FROM SQX_Submission_History__c LIMIT 1];
            System.assertEquals(medwatch.Id, subHistory.SQX_Medwatch__c);
            System.assertEquals(SQX_Submission_History.STATUS_PENDING, subHistory.Status__c);
            
            // Assert: Get content Document Link
            ContentDocumentLink link = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistory.Id];
            
            // Assert: ContentDocumentLink should not be null.
            System.assertNotEquals(null, link);

            // Assert: Reg Report should have the new submission history linked
            System.assertEquals(subHistory.Id, [SELECT SQX_Submission_History__c FROM SQX_Regulatory_Report__c WHERE Id =: regReport.Id].SQX_Submission_History__c);

            // Act: Mocking submission by inserting content version again despite the pending submission
            cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { medwatch.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=medwatch.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');

            // ACT : Mocking submission for second medwatch record which has not yet been submitted
            ContentVersion cv2 = new ContentVersion();
            cv2.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { medwatch2.SQX_Regulatory_Report__c });
            cv2.Title='Test-Title';
            cv2.PathOnClient=medwatch.Name + '.xml';
            cv2.VersionData = Blob.valueOf('This is the test data');

            List<Database.SaveResult> results = Database.insert(new List<ContentVersion> { cv, cv2 }, false);

            // Assert: Error is thrown for the first record as pending submission already exists and for the other report, it should be successfully submitted            System.assertEquals(false, result.isSuccess());
            Database.SaveResult result = results.get(0);
            System.assertEquals(false, result.isSuccess());
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), 'A submission record already exists for the Regulatory Report.'), 'Unexpected error received ' + result.getErrors());

            result = results.get(1);
            System.assertEquals(true, result.isSuccess(), 'Expected insertion to pass but failed with error ' + result.getErrors());

            // Assert: Submission history for Mewdatch2 needs to be created.
            subHistory = [SELECT Id, SQX_Medwatch__c, Status__c FROM SQX_Submission_History__c WHERE SQX_Medwatch__c =: medwatch2.Id];
            System.assertEquals(SQX_Submission_History.STATUS_PENDING, subHistory.Status__c);
            
            // Assert: Get content Document Link
            List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistory.Id];
            
            // Assert: There should be 2 ContentDocumentLinks.
            System.assertEquals(2, links.size());

            // Assert: Reg Report should have the new submission history linked
            System.assertEquals(subHistory.Id, [SELECT SQX_Submission_History__c FROM SQX_Regulatory_Report__c WHERE Id =: regReport2.Id].SQX_Submission_History__c);


            Test.stopTest();

     }
    }
    
    /**
     * GIVEN: Medwatch record linked with Followup Regulatory Report.
     * WHEN:  When the content version for the record is created.
     * THEN: A Submission History record should be created for that Medwatch report and isFollowup should be set
     * to true and report number should be populated with the followup number
     */
    static testMethod void givenFollowupMedwatchRecord_WhenContentVersionOfTheMedwatchIsCreated_ThenIsFollowupAndReportNumberShouldBeSetInTheSubmissionRecord(){
       
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            
            // ARRANGE: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            // ARRANGE: Create submissionRecord for parent regulatory report.
            SQX_Submission_History__c submissionRecord = new SQX_Submission_History__c(
                Status__c = 'Complete',
                SQX_Complaint__c = complaint.complaint.Id,
                Submitted_By__c= UserInfo.getName(),
                SQX_Submitted_By__c = UserInfo.getUserId(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA,
                Submitted_Date__c=System.today(),
                Due_Date__c = Date.today() + 2
            );
            
            insert submissionRecord;
            
             //ARRANGE: Create parent regulatory report
            SQX_Regulatory_Report__c regulatoryReportParent = new SQX_Regulatory_Report__c();
            regulatoryReportParent.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regulatoryReportParent.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regulatoryReportParent.SQX_Complaint__c=complaint.complaint.Id;
            regulatoryReportParent.Due_Date__c= Date.today().addDays(3);
            regulatoryReportParent.SQX_Submission_History__c = submissionRecord.Id;
            regulatoryReportParent.Status__c = 'Complete';
            
            insert regulatoryReportParent;

            //ARRANGE: Create followup regulatory report 
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            regReport.SQX_Follow_Up_Of__c = regulatoryReportParent.Id;
            regReport.compliancequest__Follow_Up_Number_Internal__c =3;
            
			insert regReport;
            
             // ARRANGE : Medwatch is created
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.B5_Describe_event_or_problem__c='Event Description';
            medwatch.B7_Other_Relevant_History__c='Other relevant history';
            medwatch.D2_a_Common_Device_Name__c='Common Device Name';
            medwatch.D2_b_Procode__c='COD';
            medwatch.SQX_Regulatory_Report__c=regReport.Id;

    		insert medwatch;
            
            regReport.Report_Id__c = medwatch.Id;

            update regReport;
            
            // Act: Create contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { medwatch.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=medwatch.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;
            
            // ASSERT: Query submissionHistory
            List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id, SQX_General_Report__c, Status__c,Report_Number__c,
                                                                    Is_Follow_Up__c FROM SQX_Submission_History__c WHERE SQX_Medwatch__c =:medwatch.Id ];
            // ASSERT: The report number and is followup should be set.
            System.assertEquals('003', subHistoryRecordList[0].Report_Number__c);
            System.assertEquals(true, subHistoryRecordList[0].Is_Follow_Up__c);
        }
    }
    
    /**
     * GIVEN : Complaint is created with attachments
     * WHEN :  Medwatch record is created and complaint file is linked
     * THEN :  Complaint file is linked with medwatch record as well
     */
    public testMethod static void givenComplaintWithFiles_WhenMedwatchIsCreated_ComplaintFilesCanBeLinkedWithMedwatch(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
           
            //ARRANGE: Create a complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            ContentVersion cv = new ContentVersion();
            cv.Title='Test-Title';
            cv.PathOnClient='Complaint.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;

            cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id];

            ContentDocumentLink cdLink = new ContentDocumentLink(ContentDocumentId = cv.ContentDocumentId, 
                                                                    LinkedEntityId = complaint.complaint.Id,
                                                                    ShareType = 'V');

            insert cdLink;
            
            //ARRANGE: Create regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            insert regReport;
            
            // ACT : Medwatch is created
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.B5_Describe_event_or_problem__c='Event Description';
            medwatch.B7_Other_Relevant_History__c='Other relevant history';
            medwatch.D2_a_Common_Device_Name__c='Common Device Name';
            medwatch.D2_b_Procode__c='COD';
            medwatch.SQX_Regulatory_Report__c=regReport.Id;
            
            insert medwatch;

            Test.startTest();

            List<Map<String,String>> complaintFiles = SQX_File_Upload_Controller.getDocList(medwatch.Id);
            System.assertEquals(1, complaintFiles.size());
            System.assertEquals(cdLink.ContentDocumentId, complaintFiles.get(0).get('Id'));

            // ACT : Complaint file is linked with medwatch
            SQX_File_Upload_Controller.saveContentDocsToReport(medwatch.Id, new List<String>{complaintFiles.get(0).get('Id')});

            List<ContentDocumentLink> reportCDLinks = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :medwatch.Id];

            // ASSSERT : Complaint file is linked with medwatch successfully
            System.assertEquals(1, reportCDLinks.size());
            System.assertEquals(cdLink.ContentDocumentId, reportCDLinks.get(0).ContentDocumentId);

        }
    }

    
     /**
     * GIVEN: Medwatch record.
     * WHEN:  When user other than Medwatch creator adds attachments.
     * THEN: A Submission should not fail.
     */
    static testMethod void givenMedwatchRecord_WhenUserOtherThanMedwatchCreatorAddsAttachments_ThenSubmissionShouldNotFail(){
        
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER);
        
        SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
        
        SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
        SQX_Test_Complaint complaint; 
        
        System.runAs(adminUser) {
            
            // ARRANGE: Create Complaint, set assignee, save and initiate.
            complaint= new SQX_Test_Complaint(adminUser);
            complaint.save();
            complaint.addPolicy(true,Date.today(),standardUser.Id);
            complaint.initiate();     
        }
        
        system.runAs(standardUser){
            
            // ARRANGE: Create Regulatory report
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            insert regReport;
            
            medwatch.SQX_Regulatory_Report__c=regReport.Id;
            
            // ARRANGE: Create medwatch and link it with regulatory report.
            insert medwatch;
            
            regReport.Report_Id__c = medwatch.Id;
            update regReport;
        }
        
        
        system.runas(adminUser){
            
            // ARRANGE: Create file content that goes in attachment section for MEDWATCH.
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='Medwatch-File';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with Medwatch');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // ARRANGE: link it with medwatch
            ContentDocumentLink link = new ContentDocumentLink (LinkedEntityId =medwatch.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            
            insert link;
        }
         
        
        
        System.runAs(standardUser) {
            
            // Act: Create contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { medwatch.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=medwatch.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;
        }
            
        // ASSERT: Query submissionHistory
        List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id FROM SQX_Submission_History__c];
        
        // Assert: Get content Document Link
        List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistoryRecordList[0].Id];
        
        // Assert: There should be 2 ContentDocumentLinks.
        System.assertEquals(2, links.size());
    }
}