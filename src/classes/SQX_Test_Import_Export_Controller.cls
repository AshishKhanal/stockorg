/**
 * Test import export controller functionalities
*/

@isTest
public class SQX_Test_Import_Export_Controller {


    /**
     * GIVEN : Random SObject records(CQ Task in this case)
     * WHEN : Export service is invoked
     * THEN : Serialized records json is returned
     */
    static testmethod void givenCQTaskRecordWithChildRecords_WhenExportServiceInInvoked_ThenSerializedRecordsAreReturned() {

        // ARRANGE CQ Task record along with child records

        // setup a Task record along with a question and an answer option
        SQX_Test_Task tsk
            = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();

        SQX_Task_Question__c tskQst = tsk.addQuestion();

        SQX_Answer_Option__c ansOpt = tsk.addAnswer(tskQst, null, false);

        String result;
        List<String> relationsToFollow = new List<String> {
            SQX_Task_Question__c.SObjectType + '.' + SQX_Task_Question__c.SQX_Task__c,
            SQX_Answer_Option__c.SObjectType + '.' + SQX_Answer_Option__c.Question__c
        };


        // ACT Invoke export service
        Test.startTest();
        result = SQX_Data_Import_Export_Controller.export(new List<Id> { tsk.Task.Id }, relationsToFollow);
        Test.stopTest();

        // ASSERT Desired result has been retrieved
        System.assertNotEquals(null, result, 'No result returned');

        // validate records retrieved
        Set<String> expectedObjectTypes = new Set<String>
        {
                SQX_Task__c.SObjectType.getDescribe().getName(),
                SQX_Task_Question__c.SObjectType.getDescribe().getName(),
                SQX_Answer_Option__c.SObjectType.getDescribe().getName(),

                /* default sobjecttypes followed are below */
                Attachment.SObjectType.getDescribe().getName(),
                ContentDocumentLink.SObjectType.getDescribe().getName()
        };

        SObjectDataLoader.RecordsBundle recBundle = (SObjectDataLoader.RecordsBundle) System.JSON.deserialize(result, SObjectDataLoader.RecordsBundle.class);
        System.assertEquals(expectedObjectTypes.size(), recBundle.RecordSetBundles.size(), 'Unexpected number of objectypes found');
        for(SObjectDataLoader.RecordSetBundle rstBundle : recBundle.RecordSetBundles) {
            System.assert(expectedObjectTypes.contains(rstBundle.ObjectType), 'Unexpected objecttype found ' + rstBundle.ObjectType);
        }
    }

    /**
     * GIVEN : Records as Serialized JSON
     * WHEN : Import Service is Invoked
     * THEN : Records are imported/inserted and inserted records are returned
     */
    static testmethod void givenSerializedRecordsJSON_WhenImportServiceIsInvoked_ThenImportedRecordsAreReturned() {

        // NOTE : import/deserialization has already been tested in SQX_Test_SObjectDataLoader

        // ARRANGE : Data json (1 task, 2 question, 2 answer options)
        String taskRecordJSON
            = '{"recordTypeMap":{"0125C0000003SMjQAM":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Id":"0125C0000003SMjQAM","Description":"This record type is for questions that are to be used in dynamic questionnaire, where the next question depends on the answer given by the user. Example. Decision Tree Based Questionnaire","DeveloperName":"Dynamic_Question","Name":"Dynamic Question","SobjectType":"compliancequest__SQX_Task_Question__c"}},"RecordSetBundles":[{"Records":[{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"compliancequest__Active__c":true,"compliancequest__Allowed_Days__c":5,"compliancequest__Description__c":"Australian Regulatory Submission Assessment Decision Tree","compliancequest__Include_In_Extension__c":false,"compliancequest__Record_Type__c":"Complaint","compliancequest__Task_Type__c":"Decision Tree","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2O5C000000XI6KUAW","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia - TGA","OwnerId":"0055C000000varjQAA","SystemModstamp":"2018-12-11T10:41:34.000+0000","CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"Owner":{"attributes":{"type":"Name","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}}],"ObjectType":"compliancequest__SQX_Task__c"},{"Records":[{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"compliancequest__CQ_Task_Name__c":"Australia - TGA","compliancequest__QuestionText__c":"Could death or serious injury occur if the event were to recur? CHANGED","compliancequest__SQX_Task__c":"a2O5C000000XI6KUAW","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2N5C0000005TU2UAM","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia.01","RecordTypeId":"0125C0000003SMjQAM","SystemModstamp":"2018-12-11T10:41:34.000+0000","compliancequest__SQX_Task__r":{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"Name":"Australia - TGA","Id":"a2O5C000000XI6KUAW"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"RecordType":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Name":"Dynamic Question","Id":"0125C0000003SMjQAM"}},{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000006TU2UAM"},"compliancequest__CQ_Task_Name__c":"Australia - TGA","compliancequest__QuestionText__c":"Could death or serious injury occur if the event were to recur? CHANGED","compliancequest__SQX_Task__c":"a2O5C000000XI6KUAW","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2N5C0000006TU2UAM","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia.02","RecordTypeId":"0125C0000003SMjQAM","SystemModstamp":"2018-12-11T10:41:34.000+0000","compliancequest__SQX_Task__r":{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"Name":"Australia - TGA","Id":"a2O5C000000XI6KUAW"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"RecordType":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Name":"Dynamic Question","Id":"0125C0000003SMjQAM"}}],"ObjectType":"compliancequest__SQX_Task_Question__c"},{"Records":[{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3zUUAQ"},"compliancequest__Actionable__c":false,"compliancequest__Option_Value__c":"No","compliancequest__Question__c":"a2N5C0000005TU2UAM","compliancequest__RegulatoryBody__c":"Australia TGA","compliancequest__Report_Name__c":"No Reporting Required","compliancequest__Reportable__c":false,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3zUUAQ","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"No","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"Name":"Australia.02","Id":"a2N5C0000005TU2UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}},{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3zfUAA"},"compliancequest__Actionable__c":false,"compliancequest__Due_In_Days__c":25,"compliancequest__Option_Value__c":"Yes","compliancequest__Question__c":"a2N5C0000005TU2UAM","compliancequest__RegulatoryBody__c":"Australia TGA","compliancequest__Report_Name__c":"TGA - 30 Day","compliancequest__Reportable__c":true,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3zfUAA","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"Yes","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"Name":"Australia.02","Id":"a2N5C0000005TU2UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}}],"ObjectType":"compliancequest__SQX_Answer_Option__c"},{"Records":[],"ObjectType":"Attachment"},{"Records":[],"ObjectType":"ContentDocumentLink"}],"RecordAccess":{"a2O5C000000XI6KUAW":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a2O5C000000XI6KUAW"},"RecordId":"a2O5C000000XI6KUAW","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"All","Id":"a2O5C000000XI6KUAW"},"a0N5C000000a3zfUAA":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zfUAA"},"RecordId":"a0N5C000000a3zfUAA","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zfUAA"},"a0N5C000000a3zUUAQ":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zUUAQ"},"RecordId":"a0N5C000000a3zUUAQ","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zUUAQ"},"a0N5C000000a3yyUAA":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3yyUAA"},"RecordId":"a0N5C000000a3yyUAA","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3yyUAA"},"a2N5C0000005TU2UAM":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a2N5C0000005TU2UAM"},"RecordId":"a2N5C0000005TU2UAM","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a2N5C0000005TU2UAM"},"a0N5C000000a3zYUAQ":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zYUAQ"},"RecordId":"a0N5C000000a3zYUAQ","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zYUAQ"}}}';

        // ACT Import records
        Test.startTest();
        String result = SQX_Data_Import_Export_Controller.import(taskRecordJSON);

        // ASSERT Task record is created
        System.assertNotEquals(null, result, 'No result returned. Expected result to be not null');

        Id tskId = Id.valueOf(result);
        System.assertEquals(SQX_Task__c.SObjectType, tskId.getSObjectType(), 'Expected task object to have been created');
        String descr = [SELECT Description__c FROM SQX_Task__c WHERE Id =: tskId].Description__c;

        // assert child records have been created as well
        List<SQX_Task_Question__c> questions = [SELECT Id FROM SQX_Task_Question__c WHERE SQX_Task__c =: tskId];
        System.assertEquals(2, questions.size(), 'Expected question records to have been created');
        System.assertEquals(2, [SELECT Id FROM SQX_Answer_Option__c WHERE Question__c IN: new Map<Id, SQX_Task_Question__c>(questions).keySet()].size(), 'Expected answer options to have been created');

        // ACT Import new child records into the same record to check for upsert case (1 task, 3 question, 4 answer options)
        taskRecordJSON = '{"recordTypeMap":{"0125C0000003SMjQAM":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Id":"0125C0000003SMjQAM","Description":"This record type is for questions that are to be used in dynamic questionnaire, where the next question depends on the answer given by the user. Example. Decision Tree Based Questionnaire","DeveloperName":"Dynamic_Question","Name":"Dynamic Question","SobjectType":"compliancequest__SQX_Task_Question__c"}},"RecordSetBundles":[{"Records":[{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"compliancequest__Active__c":true,"compliancequest__Allowed_Days__c":5,"compliancequest__Description__c":"Australian Regulatory Submission Assessment Decision Tree Changed","compliancequest__Include_In_Extension__c":false,"compliancequest__Record_Type__c":"Complaint","compliancequest__Task_Type__c":"Decision Tree","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2O5C000000XI6KUAW","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia - TGA","OwnerId":"0055C000000varjQAA","SystemModstamp":"2018-12-11T10:41:34.000+0000","CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"Owner":{"attributes":{"type":"Name","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}}],"ObjectType":"compliancequest__SQX_Task__c"},{"Records":[{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"compliancequest__CQ_Task_Name__c":"Australia - TGA","compliancequest__QuestionText__c":"Could death or serious injury occur if the event were to recur? CHANGED","compliancequest__SQX_Task__c":"a2O5C000000XI6KUAW","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2N5C0000005TU2UAM","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia.03","RecordTypeId":"0125C0000003SMjQAM","SystemModstamp":"2018-12-11T10:41:34.000+0000","compliancequest__SQX_Task__r":{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"Name":"Australia - TGA","Id":"a2O5C000000XI6KUAW"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"RecordType":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Name":"Dynamic Question","Id":"0125C0000003SMjQAM"}},{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"compliancequest__CQ_Task_Name__c":"Australia - TGA","compliancequest__QuestionText__c":"Could death or serious injury occur if the event were to recur? CHANGED","compliancequest__SQX_Task__c":"a2O5C000000XI6KUAW","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2N5C0000005TU2UAM","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia.02","RecordTypeId":"0125C0000003SMjQAM","SystemModstamp":"2018-12-11T10:41:34.000+0000","compliancequest__SQX_Task__r":{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"Name":"Australia - TGA","Id":"a2O5C000000XI6KUAW"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"RecordType":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Name":"Dynamic Question","Id":"0125C0000003SMjQAM"}},{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU9UAM"},"compliancequest__CQ_Task_Name__c":"Australia - TGA","compliancequest__QuestionText__c":"Did a serious injury or death occur?","compliancequest__SQX_Task__c":"a2O5C000000XI6KUAW","CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:34.000+0000","Id":"a2N5C0000005TU9UAM","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:34.000+0000","Name":"Australia.01","RecordTypeId":"0125C0000003SMjQAM","SystemModstamp":"2018-12-11T10:41:34.000+0000","compliancequest__SQX_Task__r":{"attributes":{"type":"compliancequest__SQX_Task__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task__c/a2O5C000000XI6KUAW"},"Name":"Australia - TGA","Id":"a2O5C000000XI6KUAW"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"RecordType":{"attributes":{"type":"RecordType","url":"/services/data/v44.0/sobjects/RecordType/0125C0000003SMjQAM"},"Name":"Dynamic Question","Id":"0125C0000003SMjQAM"}}],"ObjectType":"compliancequest__SQX_Task_Question__c"},{"Records":[{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3zUUAQ"},"compliancequest__Actionable__c":false,"compliancequest__Option_Value__c":"No","compliancequest__Question__c":"a2N5C0000005TU2UAM","compliancequest__RegulatoryBody__c":"Australia TGA","compliancequest__Report_Name__c":"No Reporting Required","compliancequest__Reportable__c":false,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3zUUAQ","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"No","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"Name":"Australia.02","Id":"a2N5C0000005TU2UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}},{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3zfUAA"},"compliancequest__Actionable__c":false,"compliancequest__Due_In_Days__c":25,"compliancequest__Option_Value__c":"Yes","compliancequest__Question__c":"a2N5C0000005TU2UAM","compliancequest__RegulatoryBody__c":"Australia TGA","compliancequest__Report_Name__c":"TGA - 30 Day","compliancequest__Reportable__c":true,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3zfUAA","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"Yes","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"Name":"Australia.02","Id":"a2N5C0000005TU2UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}},{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3yyUAA"},"compliancequest__Actionable__c":false,"compliancequest__Due_In_Days__c":7,"compliancequest__Option_Value__c":"Yes","compliancequest__Question__c":"a2N5C0000005TU9UAM","compliancequest__RegulatoryBody__c":"Australia TGA","compliancequest__Report_Name__c":"TGA - 10 Day","compliancequest__Reportable__c":true,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3yyUAA","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"Yes","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU9UAM"},"Name":"Australia.01","Id":"a2N5C0000005TU9UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}},{"attributes":{"type":"compliancequest__SQX_Answer_Option__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Answer_Option__c/a0N5C000000a3zYUAQ"},"compliancequest__Actionable__c":false,"compliancequest__Next_Question__c":"a2N5C0000005TU2UAM","compliancequest__Option_Value__c":"No","compliancequest__Question__c":"a2N5C0000005TU9UAM","compliancequest__Reportable__c":false,"CreatedById":"0055C000000varjQAA","CreatedDate":"2018-12-11T10:41:35.000+0000","Id":"a0N5C000000a3zYUAQ","IsDeleted":false,"LastModifiedById":"0055C000000varjQAA","LastModifiedDate":"2018-12-11T10:41:35.000+0000","Name":"No","SystemModstamp":"2018-12-11T10:41:35.000+0000","compliancequest__Next_Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU2UAM"},"Name":"Australia.02","Id":"a2N5C0000005TU2UAM"},"compliancequest__Question__r":{"attributes":{"type":"compliancequest__SQX_Task_Question__c","url":"/services/data/v44.0/sobjects/compliancequest__SQX_Task_Question__c/a2N5C0000005TU9UAM"},"Name":"Australia.01","Id":"a2N5C0000005TU9UAM"},"CreatedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"},"LastModifiedBy":{"attributes":{"type":"User","url":"/services/data/v44.0/sobjects/User/0055C000000varjQAA"},"Name":"Admin User","Id":"0055C000000varjQAA"}}],"ObjectType":"compliancequest__SQX_Answer_Option__c"},{"Records":[],"ObjectType":"Attachment"},{"Records":[],"ObjectType":"ContentDocumentLink"}],"RecordAccess":{"a2O5C000000XI6KUAW":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a2O5C000000XI6KUAW"},"RecordId":"a2O5C000000XI6KUAW","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"All","Id":"a2O5C000000XI6KUAW"},"a2N5C0000005TU9UAM":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a2N5C0000005TU9UAM"},"RecordId":"a2N5C0000005TU9UAM","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a2N5C0000005TU9UAM"},"a0N5C000000a3zfUAA":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zfUAA"},"RecordId":"a0N5C000000a3zfUAA","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zfUAA"},"a0N5C000000a3zUUAQ":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zUUAQ"},"RecordId":"a0N5C000000a3zUUAQ","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zUUAQ"},"a0N5C000000a3yyUAA":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3yyUAA"},"RecordId":"a0N5C000000a3yyUAA","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3yyUAA"},"a2N5C0000005TU2UAM":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a2N5C0000005TU2UAM"},"RecordId":"a2N5C0000005TU2UAM","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a2N5C0000005TU2UAM"},"a0N5C000000a3zYUAQ":{"attributes":{"type":"UserRecordAccess","url":"/services/data/v44.0/sobjects/UserRecordAccess/a0N5C000000a3zYUAQ"},"RecordId":"a0N5C000000a3zYUAQ","HasEditAccess":true,"HasDeleteAccess":true,"HasReadAccess":true,"MaxAccessLevel":"Delete","Id":"a0N5C000000a3zYUAQ"}}}';

        result = SQX_Data_Import_Export_Controller.import(taskRecordJSON);

        // ASSERT New child records have been imported
        tskId = Id.valueOf(result);
        System.assertEquals(SQX_Task__c.SObjectType, tskId.getSObjectType(), 'Expected task object to have been created');

        // asserting that task field has been updated
        String newDesc = [SELECT Description__c FROM SQX_Task__c WHERE Id =: tskId].Description__c;
        System.assertNotEquals(descr, newDesc, 'Expected description to have been updated');

        // assert child records have been created
        questions = [SELECT Id FROM SQX_Task_Question__c WHERE SQX_Task__c =: tskId];
        System.assertEquals(3, questions.size(), 'Expected question records to have been created');
        System.assertEquals(4, [SELECT Id FROM SQX_Answer_Option__c WHERE Question__c IN: new Map<Id, SQX_Task_Question__c>(questions).keySet()].size(), 'Expected answer options to have been created');

        Test.stopTest();

    }

    /**
     *  GIVEN : Serialized JSON that contains a lookup to another object which is not present in the json
     *  WHEN : Import service is invoked
     *  THEN : The service tries to resolve the dependency and throws error if it cannot
     */
    static testmethod void givenSerializedRecordsJSONWithUnresolveableDependency_WhenImportServieIsInvoked_ThenErrorIsThrownIfNoSuchRecordExistsInOrg() {

        // Arrange : Create a serialized Answer Option json with unresolvable dependency on Next Task Question

        SQX_Task__c task = new SQX_Task__c();
        task.Name = 'New Task';
        task.Task_Type__c = SQX_Task.TASK_TYPE_TASK;
        task.Allowed_Days__c = 10;
        insert task;

        SQX_Task_Question__c newQuestion1 = new SQX_Task_Question__c();
        newQuestion1.Name = 'New Question1';
        newQuestion1.QuestionText__c = 'New Question';
        newQuestion1.SQX_Task__c = task.Id;
        insert newQuestion1;

        SQX_Task_Question__c newQuestion2 = new SQX_Task_Question__c();
        newQuestion2.Name = 'New Question2';
        newQuestion2.QuestionText__c = 'New Question';
        newQuestion2.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX_Task_Question__c.getSObjectType().getDescribe().getName(), 'Dynamic_Question');
        newQuestion2.SQX_Task__c = task.Id;

        SQX_Answer_Option__c newOption = new SQX_Answer_Option__c();
        newOption.Option_Value__c = 'New Option';
        newOption.Reportable__c = false;
        newOption.Question__c = newQuestion1.Id;
        newOption.Question__r = newQuestion1;
        newOption.Next_Question__c = 'aBJ0Q0000008PCdWAM';    // random Id, resolver should not be able to resolve this
        newOption.Next_Question__r = newQuestion2;    // random Id, resolver should not be able to resolve this
        newOption.put('Id', 'a8v0Q0000004CUJQA2');

        SObjectDataLoader.RecordSetBundle recordSetBundle = new SObjectDataLoader.RecordSetBundle();
        recordSetBundle.ObjectType = SQX_Answer_Option__c.getSObjectType().getDescribe().getName();
        recordSetBundle.Records = new Set<SObject> { newOption };

        SObjectDataLoader.RecordsBundle recordsBundle = new SObjectDataLoader.RecordsBundle();
        recordsBundle.RecordSetBundles = new List<SObjectDataLoader.RecordSetBundle> { recordSetBundle };

        String recordJSON = JSON.serialize(recordsBundle);

        String expectedErrMsg;
        String errThrown;

        // Act : Try importing the json
        Test.startTest();
        try {
            SQX_Data_Import_Export_Controller.import(recordJSON);
        } catch(AuraHandledException ex) {
            errThrown = ex.getMessage();
        }

        // Assert : Import should fail because of unresolveable dependency
        System.assertNotEquals(null, errThrown, 'Expected error message to have been thrown due to unresolveable dependency');

        expectedErrMsg = String.format(Label.CQ_ERR_MSG_DATA_IMPORT_RESOLVE_FAILED,
                                new List<String> { 
                                    SQX_Answer_Option__c.Next_Question__c.getDescribe().getRelationshipName(),
                                    SQX_Answer_Option__c.SObjectType.getDescribe().getName(),
                                    '0',
                                    'New Question2'
                                });

        System.assertEquals(expectedErrMsg, errThrown, 'Error message mismatch');

        // ACT : insert the task question first and then try importing
        insert newQuestion2;

        String result = SQX_Data_Import_Export_Controller.import(recordJSON);

        // ASSERT : New answer option has to be created
        System.assertNotEquals(null, result, 'Import should have been successful with a new answer option inserted');
        Id ansOptId = Id.valueOf(result);

        System.assertEquals(ansOptId, [SELECT Id FROM SQX_Answer_Option__c WHERE Question__c =: newQuestion1.Id AND Next_Question__c =: newQuestion2.Id ].Id);

        Test.stopTest();
    }
}