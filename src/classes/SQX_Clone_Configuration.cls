/**
* The class is used as a paramter to various flows that will help in cloning records
*/
global class SQX_Clone_Configuration {
    
    /*
    * Holds record ids that will be cloned.
    */
    @InvocableVariable(label='ParentIds' description='Contains ids of record that will be cloned' required=true)
	global String[] ids;
    
    /*
    * Holds json data that will overwrite cloned record fields value.  
    */
    @InvocableVariable(label='JsonDefaults' description='Contains Fieldname and its value in json format' required=true)
    global String jsonDefaults;
    
    /*
    * Name of the fieldSet where the fieldset contains those field that will not be a part of cloned records
    */
    @InvocableVariable(label='fieldSetName' description='Contain fields that will not be present in cloned record')
    global String fieldSetName;
    
    /*
    * Holds fieldname that will have parent record id in clonedrecord
    */
    @InvocableVariable(label='relationalField' description='Contains fieldname that will have value of parent record id')
    global String relationalField;
    
    /*
    * Holds information about whether the inclusion or exclusion fieldset should be used.
    */
    @InvocableVariable(label='useInclusionFieldset' description='Contains the information that tell whether or not inclusion fieldset should be used')
    global Boolean useInclusionFieldset;
    
}