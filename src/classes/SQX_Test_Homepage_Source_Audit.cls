/**
*   This class contains test cases for ensuring that audit source is providing homepage items with desired values
*   @author - Piyush Subedi
*   @date - 06-04-2017
*/
@isTest
public class SQX_Test_Homepage_Source_Audit{

    public static final String AUDIT_1 = 'Audit 1';

    /*
        Audit source currently returns items of type :
        1. Open Audits - Needing Response
        2. Open Actions
    */


    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser){

            /* Creating an open audit with open finding */
            SQX_Test_Audit ad1 = new SQX_Test_Audit(adminUser);
            ad1.audit.Start_Date__c = System.today().addDays(-5);
            ad1.audit.SQX_Auditee_Contact__c = standardUser.Id;
            ad1.audit.Title__c = AUDIT_1;
            ad1.save();

            // Creating an audit with Complete Status and Ready For Closure Stage
            SQX_Test_Audit ad2 = new SQX_Test_Audit(adminUser);
            ad2.audit.Start_Date__c = System.today().addDays(-5);
            ad2.audit.End_Date__c = System.today().addDays(-2);
            ad2.audit.SQX_Auditee_Contact__c = standardUser.Id;
            ad2.audit.Status__c = SQX_Audit.STATUS_COMPLETE;
            ad2.audit.Stage__c = SQX_Audit.STAGE_READY_FOR_CLOSURE;
            ad2.save();

            // adding finding
            ad1.addAuditFinding()
                .setStatus(SQX_Finding.STATUS_OPEN)
                .setRequired(true, false, false, false, false)
                .save();

            // adding implementation action with description
            SQX_Action__c implementation1 = new SQX_Action__c 
                                            (
                                                Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                Due_Date__c = Date.Today(),
                                                Status__c = SQX_Implementation_Action.STATUS_OPEN,
                                                Description__c = 'I am really really really really really really really ' +
                                                                 'really really really really really really really really ' +
                                                                 'really really really really really really really really ' + 
                                                                 'really really really really really really really really ' +
                                                                 'really really really really really really really really long.'    // long description for testing trimming logic
                                            );

            // adding implementation action without description
            SQX_Action__c implementation2 = new SQX_Action__c 
                                            (
                                                Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                Due_Date__c = Date.Today(),
                                                Status__c = SQX_Implementation_Action.STATUS_OPEN
                                            );


            implementation1.SQX_Audit__c = ad1.audit.Id;
            implementation2.SQX_Audit__c = ad1.audit.Id;

            new SQX_DB().op_insert(new List<SQX_Action__c> { implementation1, implementation2 },
                                    new List<Schema.SObjectField>{ SQX_Action__c.Plan_Type__c,
                                                                   SQX_Action__c.Due_Date__c,
                                                                   SQX_Action__c.Record_Action__c,
                                                                   SQX_Action__c.SQX_Audit__c,
                                                                   SQX_Action__c.Description__c });
        }
    }


    /**
    *   Given : An audit with open finding and implemenation and also a homepage component expecting records from Open Audit NR source
    *   When : Audit is completed
    *   Then : Open Audit NR and Open Action type item is added to the homepage with desired values
    *   @story - 3166
    */
    testmethod static void givenAuditWithOpenFindingAndImplementation_WhenTheAuditIsCompleted_ThenCorrepondingOpenAuditNRAndOpenActionItemsShouldBeAddedToHomepageComponent(){

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser){

            // Arrange1: Fetch an open audit with open finding and implementation
            SQX_Audit__c ad1 = [SELECT  Id, Name, CreatedDate, Title__c, End_Date__c,      
                                        (SELECT Name, Description__c, CreatedDate, Due_Date__c, SQX_User__r.Name,
                                          SQX_Audit__r.Name, SQX_Audit__r.Title__c, SQX_Audit__c
                                          FROM SQX_Actions__r) 
                                        FROM SQX_Audit__c 
                                        WHERE Title__c =: AUDIT_1
                                        LIMIT 1];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { new SQX_Homepage_Source_Audit_Items() };


            // Act1: Complete Audit
            ad1.End_Date__c = System.today().addDays(-2);
            ad1.Status__c = SQX_Audit.STATUS_COMPLETE;
            Database.SaveResult dbResult = new SQX_DB().op_update( new List<SQX_Audit__c> { ad1 }, 
                                                                  new List<Schema.SObjectField> { SQX_Audit__c.End_Date__c, SQX_Audit__c.Status__c}).get(0);

            // Assert1: Validate database save
            System.assert(dbResult.isSuccess(), 'Expected save to be successful');

            // Act2: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());


            // Assert2: Open Audit-NR and Open Action items have been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size() >= 3, 'Expected at-least three items');

            for(Object hpObj : hpItems){

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_NEEDING_RESPONSE){

                    validateOpenAuditNRItem(ad1, hpItem);

                }else if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_COMPLETE){

                    if(hpItem.itemId == ad1.SQX_Actions__r.get(0).Id){
                        validateOpenActionItem(ad1.SQX_Actions__r.get(0), hpItem);
                    }else if(hpItem.itemId == ad1.SQX_Actions__r.get(1).Id){
                        validateOpenActionItem(ad1.SQX_Actions__r.get(1), hpItem);
                    }else{
                        System.assert(false, 'Unknown action item : ' + hpItem);
                    }

                } else if(hpItem.actionType != SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE){
                    System.assert(false, 'Unknown item fetched : '+ hpItem);
                }

            }

        }

    }

    /**
    *   Given : An Audit With Status Complete and Stage Ready For Closure
    *   When : Homepage Component is retrieved
    *   Then : Audit to be Closed is added to the homepage with desired values
    *   @story - SQX-1438, SQX-4514
    */
     testmethod static void givenAuditWithStatusCompleteAndStageReadyForClosure_WhenHomepageComponentIsRetrieved_ThenAuditToBeClosedIsAddedToHomepageComponent(){

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser'),
             adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        // Adding audit for a different user(i.e not standard user)
        // To validate SQX-4514
        System.runAs(adminUser)
        {
            SQX_Test_Audit ad = new SQX_Test_Audit(adminUser);
            ad.audit.Start_Date__c = System.today().addDays(-5);
            ad.audit.End_Date__c = System.today().addDays(-2);
            ad.audit.SQX_Auditee_Contact__c = stdUser.Id; // setting contact as standard user
            ad.audit.Status__c = SQX_Audit.STATUS_COMPLETE;
            ad.audit.Stage__c = SQX_Audit.STAGE_READY_FOR_CLOSURE;
            ad.save();
        }

        System.runAs(stdUser){

            // Arrange1: Fetch an open audit with Status Complete and Stage Ready For Closure
            SQX_Audit__c ad2 = [SELECT  Id, Name, Title__c, Stage__c, Status__c
                                        FROM SQX_Audit__c 
                                        WHERE Status__c =: SQX_Audit.STATUS_COMPLETE AND Stage__c =: SQX_Audit.STAGE_READY_FOR_CLOSURE
                                        AND OwnerId =: stdUser.Id
                                        LIMIT 1];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { new SQX_Homepage_Source_Audit_Items() };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert2: Ready for Closure Audit have been fetched with desired values
            System.assertNotEquals(hpItems, null);

            // Expecting exactly one Audit to be closed item
            // not expecting audit record of some other user(this validates SQX-4514)
            System.assert(hpItems.size() == 1, 'Expected exactly one item');

            for(Object hpObj : hpItems){

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE){

                    validateAuditToBeClosed(ad2, hpItem);

                }else{
                    System.assert(false, 'Unknown item fetched : '+ hpItem);
                }

            }

        }
     }

    /**
    *   Method validates the homepage item of type Open Audit - NR
    */
    private static void validateOpenAuditNRItem(SQX_Audit__c ad1, SQX_Homepage_Item hpItem){

        System.assertEquals(ad1.End_Date__c, hpItem.dueDate);

        System.assertEquals(2, hpItem.overdueDays);

        System.assertEquals(SQX_Homepage_Item.Urgency.Critical, hpItem.urgency);

        System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_AUDIT, hpItem.moduleType);

        System.assertNotEquals(hpItem.actions, null);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_RESPOND, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_Audit_Response'));
        pr.getParameters().put('Id',ad1.Id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_REPLY, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_RESPOND , new String[] { ad1.Name, ad1.Title__c });
        System.assertEquals(expectedFeedText, hpItem.feedText);

        System.assertEquals(false, hpItem.supportsBulk);
    }


    /**
    *   Method validates the homepage item of type Open Action
    */
    private static void validateOpenActionItem(SQX_Action__c imp, SQX_Homepage_Item hpItem){

        System.assertEquals(imp.Due_Date__c, hpItem.dueDate);

        System.assertEquals(0, hpItem.overdueDays);

        System.assertEquals(SQX_Homepage_Item.Urgency.High, hpItem.urgency);

        System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_AUDIT, hpItem.moduleType);

        System.assertNotEquals(hpItem.actions, null);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_RESPOND, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_Audit_Response'));
        pr.getParameters().put('Id',imp.SQX_Audit__c);
        pr.getParameters().put('initialTab','responseHistoryTab');
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_REPLY, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        // validating feed text
        if(!String.isBlank(imp.Description__c)){

            System.assert(hpItem.feedText.contains('...'), 'Expected description to be trimmed but got ' + hpItem.feedText);
        }else{

            String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITHOUT_SUBJECT ,
                                                new String[] { 
                                                    imp.SQX_Audit__r.Name, imp.SQX_Audit__r.Title__c });

            System.assertEquals(expectedFeedText, hpItem.feedText);
        }

        System.assertEquals(false, hpItem.supportsBulk);
    }

    /**
    *   Method validates the homepage item of Audit to be closed
    */
    private static void validateAuditToBeClosed(SQX_Audit__c ad2, SQX_Homepage_Item hpItem){

        System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_AUDIT, hpItem.moduleType);

        System.assertNotEquals(hpItem.actions, null);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_CLOSE, act.name);

        PageReference pr = new PageReference('/' + ad2.id);
        pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_CLOSE, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS , new String[] { ad2.Name, ad2.Title__c });

        System.assertEquals(expectedFeedText, hpItem.feedText);

        System.assertEquals(false, hpItem.supportsBulk);

    }
}