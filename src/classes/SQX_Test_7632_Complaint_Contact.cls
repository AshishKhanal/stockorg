/**
 * test class for complaint contact form rules evaluation
 */
@isTest
public class SQX_Test_7632_Complaint_Contact {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    
        System.runAs(adminUser){
            Account acc = SQX_Test_Account_Factory.createAccount();
            Contact con = SQX_Test_Account_Factory.createContact(acc);
        }
    }
    
    /**
     * GIVEN : Complaint Contact create action is called
     * WHEN : Flow is invoked
     * THEN : Form rules are evaluated
     */
    public static testMethod void givenComplaintContact_WhenContactIsSelected_FormRulesAreEvaluated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            // ARRANGE : Complaint Task create action is called
            Map<String, Object> params = new Map<String, Object>();
            params.put('Action', 'create');
            
            // ACT : Flow is invoked
            Flow.Interview.CQ_Complaint_Contact_Form_Rules_Evaluation calcFlow = new Flow.Interview.CQ_Complaint_Contact_Form_Rules_Evaluation(params);
            calcFlow.start();
            
            String output = (String) calcFlow.getVariableValue('EvaluationExpression');
            
            // ASSERT : Form rules is evaluated
            System.assert(String.isNotBlank(output), 'Output is not blank');
            System.assert(output.contains('CQ_Complaint_Contact_Set_Contact_Fields'), 'Experssion not evaluated');
        }
    }
    
    /**
     * GIVEN : Complaint Contact task is created
     * WHEN : Contact is linked
     * THEN : Complaint Contact fields are populated
     */
    public static testMethod void givenComplaintContact_WhenContactIsSelected_ComplaintContactFieldsArePopulated(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
        
            SQX_Test_Complaint complaint = new SQX_Test_Complaint( adminUser);
            complaint.save();
            
            Contact cqContact = [SELECT Id, Name, FirstName, AccountId, Email, Phone FROM Contact LIMIT 1];
            Account cqAccount = [SELECT Id, Name FROM Account LIMIT 1];
            
            // ARRANGE : Complaint Task is created
            SQX_Complaint_Contact__c complaintContact = new SQX_Complaint_Contact__c();
            complaintContact.SQX_Contact__c = cqContact.Id;
            complaintContact.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Contact is linked
            Map<String, Object> params = new Map<String, Object>();
            params.put('InRecord', complaintContact);
            Flow.Interview.CQ_Complaint_Contact_Set_Contact_Fields calcFlow = new Flow.Interview.CQ_Complaint_Contact_Set_Contact_Fields(params);
            calcFlow.start();
            
            // ASSERT : Complaint Contact fields are populated
            SQX_Complaint_Contact__c updatedComplaintContact = (SQX_Complaint_Contact__c) calcFlow.getVariableValue('OutRecord');
            System.assertEquals(cqAccount.Id, updatedComplaintContact.SQX_Account__c);
            System.assertEquals(cqAccount.Name, updatedComplaintContact.Company_Name__c);
            
            // ACT : Contact is null
            complaintContact.SQX_Contact__c = null;
            params = new Map<String, Object>();
            params.put('InRecord', complaintContact);
            calcFlow = new Flow.Interview.CQ_Complaint_Contact_Set_Contact_Fields(params);
            calcFlow.start();
            
            // ASSERT : All field values are cleared
            updatedComplaintContact = (SQX_Complaint_Contact__c) calcFlow.getVariableValue('OutRecord');
            System.assertEquals(null, updatedComplaintContact.SQX_Account__c);
            System.assertEquals(null, updatedComplaintContact.Company_Name__c);
        }        
    }

    /**
     * GIVEN : A Complaint with complaint contact
     * WHEN : Contact is saved with invalid type
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaintWithComplaintContact_WhenInvalidTypeIsSelected_ErrorIsThrown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        test.startTest();
        SQX_Picklist_Value__c invalidCategory = null;
        SQX_Picklist_Value__c validCategory = null;
        system.RunAs(adminUser){
            invalidCategory = new SQX_Picklist_Value__c(Name = 'Invalid', Value__c = 'Invalid', Category__c = 'Invalid');
            insert invalidCategory;

            validCategory = new SQX_Picklist_Value__c(Name = 'Valid', Value__c = 'Valid', Category__c = 'Complaint Contact Type');
            insert validCategory;
        }
        system.RunAs(standardUser){
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Contact with invalid type is added
            SQX_Complaint_Contact__c complaintContact = new SQX_Complaint_Contact__c(SQX_Complaint__c = complaint.complaint.Id, First_Name__c = 'Test', Name = 'Contact', SQX_Type__c = invalidCategory.Id);
            Database.SaveResult result = Database.insert(complaintContact, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Picklist Value does not match filter criteria.'), result.getErrors());

            // ACT : Contact with valid type is added
            complaintContact = new SQX_Complaint_Contact__c(SQX_Complaint__c = complaint.complaint.Id, First_Name__c = 'Test', Name = 'Contact', SQX_Type__c = validCategory.Id);
            result = Database.insert(complaintContact, false);

            // ASSERT : Conntact is saved
            System.assert(result.isSuccess(), 'Save is  not successful' + result.getErrors());
        }
            
        test.stopTest();
        
    }
}