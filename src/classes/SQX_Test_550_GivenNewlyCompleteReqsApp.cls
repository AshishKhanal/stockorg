/**
 * This test ensures that if an implementation is added as complete and "changes implementation requires approval"
 * the response is sent for approval
 */
@isTest
private class SQX_Test_550_GivenNewlyCompleteReqsApp {

    static testMethod void givenApprovalIsRequiredAndImplRespIsSubmittedComplete_ItIsSentforApproval() {
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User investigationApprover = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER , null);
        DateTime startTime = DateTime.now();
        
        System.runas(capaOwner){
                    
            Account supplier = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(supplier);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);

            SQX_CAPA__c capa = new SQX_Test_CAPA_Utility(true, capaOwner, primaryContactUser).capa;
            
            insert capa;
            
            capa.Status__c = SQX_CAPA.STATUS_OPEN;
            update capa;
            
            /* construct a response and submit for approval */
            System.runas(primaryContactUser){
                ApexPages.StandardController sc = new ApexPages.StandardController(capa);
                SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
                controller.initializeTemporaryStorage();
                
                Test.startTest();
            
            
                
                SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.Id,
                                                                              Response_Summary__c = 'Response');
                
                SQX_Action__c action = new SQX_Action__c(SQX_CAPA__c = capa.Id, Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_COMPLETE,
                										 Completion_Date__c = Date.Today());
                
                SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Action');
                
                Map<String,Object>  changeSet = new Map<String, Object>(),
                                    responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                    actionUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(action)),
                                    inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped
                
                //set virtual IDSs
                responseUT.put('Id', 'Response-1');
                actionUT.put('Id', 'Action-1');
                inclusion2UT.put('Id', 'Inclusion-2');
                
                inclusion2UT.put(SQX.getNSNameFor('SQX_Action__c'), 'Action-1');
                inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
                
                List<Object> changes = new List<Object>();
                
                changes.add(responseUT);
                changes.add(actionUT);
                changes.add(inclusion2UT);
                
                changeSet.put('changeSet', changes);
            
                final Map<String, String> params = new Map<String, String> {
                    'comment' =>'Mock comment', 
                    'purposeOfSignature' =>'Mock purposeOfSignature',  
                    'nextAction' => 'submitresponse'};
            
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params));
            
            	List<SQX_Finding_Response__c> responses = [SELECT Id, Status__c FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.Id];
                System.assertEquals(1, responses.size());
                System.assertEquals(SQX_Finding_Response.STATUS_IN_APPROVAL, responses.get(0).Status__c);
            	
            	Test.stopTest();
            }

           
        
        }
    }
}