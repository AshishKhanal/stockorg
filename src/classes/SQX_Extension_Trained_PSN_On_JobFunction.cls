/**
* Generate the list of personnel who have completed personnel document training
*/
public with sharing class SQX_Extension_Trained_PSN_On_JobFunction {
    SQX_Job_Function__c mainRecord;
    public List<TrainedPersonnel_Wrapper> trainedPersonnelList { get; set; }

    public SQX_Extension_Trained_PSN_On_JobFunction(ApexPages.StandardController controller) {
        mainRecord = (SQX_Job_Function__c)controller.getRecord();
        List<SQX_Personnel_Document_Training__c> completedPersonnelDocumentTrainingList = new List<SQX_Personnel_Document_Training__c>();
        Map<Id, TrainedPersonnel_Wrapper> trainedPsnMap = new Map<Id, TrainedPersonnel_Wrapper>();

        if (mainRecord.Id != null) {
            // get all completed personnel document trainings for controlled document with current status
            completedPersonnelDocumentTrainingList =
                [SELECT Id, Name, SQX_Personnel__c, SQX_Controlled_Document__c, Level_of_Competency__c, Optional__c, Due_Date__c, Trainer_Approval_Needed__c,
                        SQX_Personnel__r.Name, Personnel_Name__c, Document_Number__c, Document_Revision__c, Document_Title__c, Completion_Date__c
                 FROM SQX_Personnel_Document_Training__c
                 WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_COMPLETE
                       AND SQX_Controlled_Document__r.Document_Status__c = :SQX_Controlled_Document.STATUS_CURRENT
                       AND SQX_Personnel__c IN (SELECT SQX_Personnel__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c = :mainRecord.Id AND Active__c = true)
                       AND SQX_Controlled_Document__c IN (SELECT SQX_Controlled_Document__c FROM SQX_Requirement__c WHERE SQX_Job_Function__c = :mainRecord.Id AND Active__c = true)
                 ORDER BY Personnel_Name__c, SQX_Personnel__r.Name, Document_Number__c, Document_Revision__c, Document_Title__c, Completion_Date__c DESC];
            
            if (completedPersonnelDocumentTrainingList.size() > 0) {
                // populate report data if completed personnel trainings exist
                for (SQX_Personnel_Document_Training__c pdt : completedPersonnelDocumentTrainingList) {
                    if (trainedPsnMap.containsKey(pdt.SQX_Personnel__c)) {
                        // add personnel document training
                        trainedPsnMap.get(pdt.SQX_Personnel__c).documentTrainings.add(pdt);
                    } else {
                        // create new trained personnel wrapper
                        TrainedPersonnel_Wrapper wrapper = new TrainedPersonnel_Wrapper();
                        // set personnel record details
                        wrapper.personnelId = pdt.SQX_Personnel__c;
                        wrapper.personnelRecordNumber = pdt.SQX_Personnel__r.Name;
                        wrapper.personnelFullName = pdt.Personnel_Name__c;
                        // set job function
                        wrapper.jobFunction = mainRecord;
                        // add personnel document training
                        wrapper.documentTrainings.add(pdt);
                        
                        trainedPsnMap.put(pdt.SQX_Personnel__c, wrapper);
                    }
                }
            }
        }
        
        // set report data
        trainedPersonnelList = trainedPsnMap.values();
    }
    
    /**
    * wrapper class for trained personnel list for a job function
    */
    public class TrainedPersonnel_Wrapper {
        public SQX_Job_Function__c jobFunction { get; set; }
        public Id personnelId { get; set; }
        public String personnelRecordNumber { get; set; }
        public String personnelFullName { get; set; }
        public List<SQX_Personnel_Document_Training__c> documentTrainings { get; set; }
        
        public TrainedPersonnel_Wrapper() {
            documentTrainings = new List<SQX_Personnel_Document_Training__c>();
        }
    }
}