/**
* Batch processor for the changing status of controlled document
*/
global without sharing class SQX_Document_Batch_Job_Processor implements Database.Batchable <sObject>{
    public class invalidDataException extends Exception{}

    //default batch size of 5 for the next schedule
    global static Integer BATCH_SIZE = 5;
    //Instance specific configurable variable for batch size
    global Integer BATCH_SZE = 5;
    //default duration for next schedule in minutes
    global static Integer SCHEDULE_AFTER_IN_MIN = 2;
    //Instance specific configurable variable for next schedule in minutes
    global Integer SCHEDULE_AFTER_MIN = 2;

    public static string JOB_NAME = 'CQ-Document Batch Job Processing';
    
    //this flag can be used in tests to disable rescheduling
    //for normal usage this value is always true, for tests it is set to false but can be changed
    public boolean RESCHEDULE = !Test.isRunningTest();

    /**
    * Returns the query that will fetch the all batch queued controlled document.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            [SELECT Id, Batch_Job_Status__c, Document_Status__c,Document_Number__c
            FROM SQX_Controlled_Document__c
            WHERE Batch_Job_Status__c = :SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED 
            Order By LastModifiedDate ASC]);
    }

    /**
    * batch method that changes the document status 
    */
    global void execute(Database.BatchableContext bc,List<sObject> scope){        
        processBatchJob(scope);
    }

    /**
    * changes status of controlled document, In case of update error, updates error field with error message
    * @param allControlledDocs list of all queued controlled docs which are to be processed
    */
    public void processBatchJob(List<SQX_Controlled_Document__c> allControlledDocs){
        Map<Id, String> oldBatchJobStatusMap = new Map<Id, String>();
        Set<String> docNumbers = new Set<String>();
        
        for(SQX_Controlled_Document__c cdoc : allControlledDocs){
            
            if(docNumbers.contains(cdoc.Document_Number__c.toUpperCase())){
                continue;
            }
            
            docNumbers.add(cdoc.Document_Number__c.toUpperCase());
            String newStatus = cdoc.Document_Status__c;

            if(cdoc.Batch_Job_Status__c == SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED){
                newStatus = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            }
            else if(cdoc.Batch_Job_Status__c == SQX_Controlled_Document.BATCH_RELEASE_QUEUED){
                newStatus = SQX_Controlled_Document.STATUS_CURRENT;
            }
            else if(cdoc.Batch_Job_Status__c == SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED){
                newStatus = SQX_Controlled_Document.STATUS_OBSOLETE;
            }
            
            cdoc.Document_Status__c = newStatus;

            oldBatchJobStatusMap.put(cdoc.Id, cdoc.Batch_Job_Status__c);//make old batch job map to use in case of errors
            cdoc.Batch_Job_Status__c = '';
            cdoc.Batch_Job_Error__c = '';
        }
        
        Database.SaveResult [] results = new SQX_DB().continueOnError().op_update(allControlledDocs,  
                new List<Schema.SObjectField>{Schema.SQX_Controlled_Document__c.Document_Status__c,
                                              Schema.SQX_Controlled_Document__c.Batch_Job_Status__c,  
                                              Schema.SQX_Controlled_Document__c.Batch_Job_Error__c
                                            }
        );

        if(results.size()>0){
            List<SQX_Controlled_Document__c> errorDocs = new List<SQX_Controlled_Document__c>();
            String oldBatchJobStatus ='';
            for(Integer i = 0; i < results.size(); i++){
                Database.SaveResult result = results[i];
                if(!result.isSuccess() && !isRecoverableError(result)){
                    SQX_Controlled_Document__c doc = allControlledDocs.get(i);

                    SQX_Controlled_Document__c document = new SQX_Controlled_Document__c(Id = doc.Id);
                    
                    document.Batch_Job_Error__c = SQX_Utilities.getFormattedErrorMessage(result.getErrors());
                    oldBatchJobStatus = oldBatchJobStatusMap.get(doc.Id);
                    if(oldBatchJobStatus == SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED){
                        document.Batch_Job_Status__c = SQX_Controlled_Document.BATCH_PRE_RELEASE_ERROR;
                    }
                    else if(oldBatchJobStatus == SQX_Controlled_Document.BATCH_RELEASE_QUEUED){
                        document.Batch_Job_Status__c = SQX_Controlled_Document.BATCH_RELEASE_ERROR;
                    }
                    else if(oldBatchJobStatus == SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED){
                        document.Batch_Job_Status__c = SQX_Controlled_Document.BATCH_OBSOLESCENCE_ERROR;
                    }
                    
                    errorDocs.add(document);
                }
            }

            new SQX_DB().op_update(errorDocs,  new List<Schema.SObjectField>{Schema.SQX_Controlled_Document__c.Batch_Job_Status__c,
                                                                                    Schema.SQX_Controlled_Document__c.Batch_Job_Error__c});   
        }

          
    
        
    }
    /**
    *This method checks if the error is recoverable or not.
    */
    private static boolean isRecoverableError(Database.SaveResult result){
        boolean isErrorRecoverable = false;
        for(Database.Error error : result.getErrors()){
            if(error.getStatusCode() == StatusCode.UNABLE_TO_LOCK_ROW){
                isErrorRecoverable=true;
                break;
            }
        }
        return isErrorRecoverable;
    }
    /**
    * This method is called by the batch executor to perform cleanups. In case of CQ this method reschedules the Document Batch Job Processing Staging 
    * every 2 minutes
    */
    global void finish(Database.BatchableContext bc){
        //schedule next loop.
        if (RESCHEDULE ){
            System.scheduleBatch(new SQX_Document_Batch_Job_Processor(), JOB_NAME, SCHEDULE_AFTER_MIN , BATCH_SZE);
        }
    }    
}