/*
 * Unit tests for Supplier Escalation Restart Flow
 */
@isTest
public class SQX_Test_Supplier_Escalation_Restart {

    /*
    *   Following scenarios are to tested
    *   1. If there are no applicable steps added after completion, when the record is restarted, the record should move to Complete/Verification
    *   2. If there are applicable steps added after completed, when the record is restarted, the record should move to Open/In Progress with Step number equal to added step's number
    */

    /**
     *  Test setup
     */
    @testSetup
    public static void commonSetup(){

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');

        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> { standardUser });

        System.runAs(adminUser) {

            // create account record
            List<Account> accounts = new List<Account>();
            accounts.add(new Account(
                Name = SQX_Test_Supplier_Escalation.ACCOUNT_NAME 
            ));

            insert accounts;
            
            //Create contact for Account
            List<Contact> contacts = new List<Contact>();
            contacts.add(new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = accounts[0].Id,
                Email = System.now().millisecond() + 'test@test.com'
            ));
            insert contacts;

            SQX_Test_Supplier_Escalation.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, standardUser, 1);
        }

        System.runAs(standardUser) {
            // Create a new completed SE record
            SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation();
            se.save();
            se.submit();
            se.initiate();

            Task t = [SELECT Id FROM Task WHERE WhatId =: se.supplierEscalation.Id];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
        }
    }


    /**
     *  Returns <code>true</code> if the given Supplier Escalation record is open
     */
    private static Boolean isRecordOpen(SQX_Supplier_Escalation__c se) {
        return se.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN &&
                se.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_IN_PROGRESS &&
                se.Workflow_Status__c == SQX_Supplier_Common_Values.WORKFLOW_STATUS_IN_PROGRESS;
    }

    /**
     * Method returns a SQX_Test_Supplier_Escalation object holding a completed SE record
     */
    private static SQX_Test_Supplier_Escalation getCompletedSupplierEscalationTestObject() {
        SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation();
        se.supplierEscalation = [SELECT Id FROM SQX_Supplier_Escalation__c LIMIT 1];

        return se.synchronize();
    }


    /**
     *  Given : Completed SE record with no applicable steps added after completion
     *  When : SE is restarted
     *  Then : Record is complete
     */
    private static testmethod void givenCompletedSIWithNoApplicableStepsRemaining_WhenTheRecordIsRestarted_ThenTheRecordIsSetAsComplete() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {

            // ARRANGE : A completed SE record
            SQX_Test_Supplier_Escalation se = getCompletedSupplierEscalationTestObject();

            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, se.supplierEscalation.Status__c, 'Expected escalation record to be complete');

            // ACT : Restarting the record
            Test.startTest();
            se.restart();
            Test.stopTest();

            // ASSERT : record should be complete
            se.synchronize();
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, se.supplierEscalation.Status__c);
        }

    }


    /**
     *  Given : Completed SE record and an added Step
     *  When : Record is restarted
     *  Then : Record is Open
     */
    private static testmethod void givenCompletedSIWithApplicableStepsAdded_WhenTheRecordIsRestarted_ThenTheRecordIsSetAsOpen() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {

            // ARRANGE : A Completed SE record
            SQX_Test_Supplier_Escalation se = getCompletedSupplierEscalationTestObject();

            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, se.supplierEscalation.Status__c, 'Expected escalation record to be complete');

            // adding an applicable step
            SQX_Supplier_Escalation_Step__c step = new SQX_Supplier_Escalation_Step__c(
                SQX_Parent__c= se.supplierEscalation.Id,
                Name='CustomStep',
                Step__c=2,
                SQX_User__c=UserInfo.getUserId(),
                Due_Date__c=Date.today(),
                RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, SQX_Steps_Trigger_Handler.RT_TASK)
            );
            insert step;

            // ACT : Restarting the record
            Test.startTest();
            se.restart();
            Test.stopTest();

            // ASSERT : record should be open with correct step number
            se.synchronize();
            System.assert(isRecordOpen(se.supplierEscalation), 'Expected record to be open but found ' + se.supplierEscalation);
            System.assertEquals(1, [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: se.supplierEscalation.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size(), 'Expected exactly 1 step to be opened');
            System.assertEquals(2, se.supplierEscalation.Current_Step__c, 'Invalid step number in record after restart');
        }

    }

}