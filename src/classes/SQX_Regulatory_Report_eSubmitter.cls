/**
 * Various Regulatory Report types(e.g. Medwatch, MedDev) shall extend this class
 * to define specific XML/PDF Export Configurations for eSubmission
*/

public abstract with sharing class SQX_Regulatory_Report_eSubmitter {

    // file extensions
    protected final String EXTENSION_XML = '.xml',
                         EXTENSION_PDF = '.pdf';

    private final String EXPORT_SERVICE_BASE_URL = SQX_Utilities.getCalloutStr('CQ_Regulatory_Submissions_Export_Service');

    private final String EXPORT_SERVICE_URL = EXPORT_SERVICE_BASE_URL + '/export';

    private final String POLL_SERVICE_URL = EXPORT_SERVICE_BASE_URL + '/poll/{job-id}';

    private final String JOB_INFO_SUBMISSION_RECORD_ID = 'submissionRecordId';

    public final static String JOB_STATUS_PENDING = 'Pending',
                                JOB_STATUS_COMPLETE = 'Completed',
                                JOB_STATUS_ERROR = 'Error';

    private final String JOB_INFO_KEY_STATUS = 'status';

    private final String FIELD_ACTIVITY_CODE = 'compliancequest__Activity_Code__c';

    public final static String ACTIVITY_CODE_SUBMIT = 'Submit',
                                ACTIVITY_CODE_VALIDATE = 'Validate';



    /**
     * Method to submit the given Regulatory Record(Medwatch/MedDev) for XML and/or PDF generation
     * @param recordId reg record Id
    */
    public String submit(Id recordId) {
        String payload = constructPayloadForSubmission(recordId);

        return submitRequest(payload);
    }


    /**
     * Method to validate the regulatory record using external service
     * This is only valid for Medwatch and MedDev where the XML schema(XSD) file can be used to validate the record
     * Returns a list of validation errors from server
    */
    public String validate(Id recordId) {
        String payload = constructPayloadForValidation(recordId);

        return submitRequest(payload);
    }


    /**
     * Method to invoke to perform SF Validations (i.e invoke validation rules)
     * Ideally, to be called before sending request to external service to validate
     * Returns a list of errors if found
    */
    public List<String> validateInSF(String recordId) {

        SObjectType objType = Id.valueOf(recordId).getSObjectType();

        SObject regRecord = objType.newSObject();
        regRecord.Id = recordId;
        regRecord.put(FIELD_ACTIVITY_CODE, ACTIVITY_CODE_VALIDATE);

        return performUpdate(regRecord);
    }

    /**
     * Method to prepare reg. record before submission
    */
    public List<String> prepareRecordForSubmission(Id recordId) {

        SObjectType objType = Id.valueOf(recordId).getSObjectType();

        SObject regRecord = objType.newSObject();
        regRecord.Id = recordId;
        regRecord.put(FIELD_ACTIVITY_CODE, ACTIVITY_CODE_SUBMIT);

        return performUpdate(regRecord);
    }


    /**
     * Method to submit the given Regulatory Record(Medwatch/MedDev) PDF generation
     * @param recordId reg record Id
    */
    public String exportToPDF(Id recordId) {
        String payload = constructPayloadForPDFGeneration(recordId);

        return submitRequest(payload);
    }

    /**
     * Method to submit Export request to the external service
     * @param requestBody payload to be sent with the request
    */
    private String submitRequest(String requestBody) {

        Map<String, String> requestHeaders = new Map<String, String>();
        requestHeaders.put(SQX_External_REST_Service_Invoker.HTTP_HEADER_CONTENT_TYPE, SQX_External_REST_Service_Invoker.CONTENT_TYPE_JSON);

        return new SQX_External_REST_Service_Invoker().invokePostService(EXPORT_SERVICE_URL, requestBody, requestHeaders);
    }


    /**
     * Method to poll the status of the given job
    */
    public String poll(String jobId) {

        return new SQX_External_REST_Service_Invoker().invokeGetService(POLL_SERVICE_URL.replace('{job-id}', jobId));
    }


    /**
     * Method to poll the status of the given job
     * The job has to be a report submission job
    */
    public String pollSubmission(String jobId, Id recordId) {

        String pollInfo = poll(jobId);	// first poll from server

        // now directly see if the submission record has been created
        // in case like MedDev which expects multiple submission assets(XML and PDF),
        // if XML generation is complete, the submission record will have been created
        // and hence the user can be navigated to the submission record

        Map<String, Object> pollInfoAsMap = (Map<String, Object>) JSON.deserializeUntyped(pollInfo);

        String status = pollInfoAsMap.get('status').toString();

        String regRecordQuery;

        if (status == JOB_STATUS_COMPLETE) {
            regRecordQuery = 'SELECT compliancequest__SQX_Regulatory_Report__r.compliancequest__SQX_Submission_History__c FROM ' + recordId.getSobjectType() + ' WHERE Id =: recordId';
        } else {
            regRecordQuery = 'SELECT compliancequest__SQX_Regulatory_Report__r.compliancequest__SQX_Submission_History__c FROM ' + recordId.getSobjectType() + ' WHERE Id =: recordId'
                            + ' AND compliancequest__SQX_Regulatory_Report__r.compliancequest__SQX_Submission_History__r.compliancequest__Status__c = \'' + SQX_Submission_History.STATUS_GENERATING_ADDITIONAL_FILES + '\'';
        }

        List<SObject> regRecords = Database.query(regRecordQuery);

        if(regRecords.size() > 0) {

            String newSubHistoryRecord = (String) regRecords[0].getSObject('compliancequest__SQX_Regulatory_Report__r').get('compliancequest__SQX_Submission_History__c');

            if(!String.isBlank(newSubHistoryRecord)) {
                // add the newly creating submission record id
                pollInfoAsMap.put(JOB_INFO_SUBMISSION_RECORD_ID, newSubHistoryRecord);
                pollInfoAsMap.put(JOB_INFO_KEY_STATUS, JOB_STATUS_COMPLETE);    // by setting it to complete, the polling with stop even though additional assets are yet to be attached
            }

        }

        return JSON.serialize(pollInfoAsMap);
    }


    /**
     * Return true if the reg record supports XML Validation else return false
    */
    public virtual Boolean supportsXMLValidation() {
        return true;
    }


    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * the desired actions(ex. XML generation, content insertion) for the given Regulatory Record
     * @param recordId id of the regulatory record
    */
    protected abstract String constructPayloadForSubmission(Id recordId);

    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * the validation action for the given Regulatory Record
     * @param recordId id of the regulatory record
    */
    protected virtual String constructPayloadForValidation(Id recordId) {
        throw new SQX_ApplicationGenericException('Validation Not Supported');
    }

    /**
     * Returns the config to be used when serializing the Regulatory Record
    */
    protected virtual SObjectDataLoader.SerializeConfig getSerializationConfig() {
        return new SObjectDataLoader.SerializeConfig();
    }

    /**
     * Query Reg record
    */
    protected abstract SObject getRecordDetail(String recordId);

    /**
     * Returns the name of the reg record whose id is provided
     * Used to set the name of the xml file generated
    */
    protected String getRecordName(Id recordId) {
        return (String) getRecordDetail(recordId).get('Name');
    }


    /**
     * Returns the Id of the parent Regulatory Report record
    */
    protected Id getRegReportIdFor(Id recordId) {
        return (Id) getRecordDetail(recordId).get('compliancequest__SQX_Regulatory_Report__c');
    }


    /**
     * Returns the name of the template to be sent to the External service
     * This is the template that will be filled up by the service
     *
     * Queries the metadata record to fetch the template name based on form name
    */
    protected String getPDFTemplateName(Id recordId) {

        Id reportId = getRegReportIdFor(recordId);

        CQ_Regulatory_Report_Setting__mdt reportSetting = new SQX_Regulatory_Report().getSubmissionSettingsForReport(reportId);

        return (reportSetting == null ? null : reportSetting.Form_Type__c);
    }


    /**
     * Method to generate the payload(request body) that instructs the external service
     * to create PDF for the given Regulatory Record
     * @param recordId id of the regulatory record
    */
    protected virtual String constructPayloadForPDFGeneration(Id recordId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act, successAct;
        SQX_External_Service_Payload.Job job1, job2;

        Export_To_PDF_Job_Detail pdfJobDetail;

        String recordJSON;

        payload = new SQX_External_Service_Payload();

        SObjectDataLoader.SerializeConfig configToUse = getSerializationConfig().translateLabels(); // we need labels in PDF not the values

        recordJSON = this.getRecordJSONFor(recordId, configToUse);

        // first action : PDF generation
        act = payload.createAction();

        // now adding additional data required for XML generation which is the record json
        pdfJobDetail = new Export_To_PDF_Job_Detail(recordJSON, getPDFTemplateName(recordId), getRecordName(recordId));

        job1 = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_PDF, null, pdfJobDetail);

        // second action : Get publicly accessible url to access the PDF
        successAct = act.createSuccessAction();

        job2 = successAct.addJob(SQX_External_Service_Payload.JobType.GENERATE_PUBLIC_URL, new Integer[] { job1.getIdentifier() } , null);

        return payload.convertToJSONString();
    }


    /**
     * Method uses SObjectDataLoader to serialize the given record
     * Using SObjectDataLoader ensures that all the field which have values are returned
     * @param recordId Id of the regulatory record
     * @param serializationConfig config to use for serialization of record
    */
    protected String getRecordJSONFor(Id recordId, SObjectDataLoader.SerializeConfig serializationConfig) {

        Map<String, Set<SObject>> recordJSONAsMap = new Map<String, Set<SObject>>();

        String recordDetails = SObjectDataLoader.serialize(new Set<Id> { recordId }, serializationConfig);

        // the format of the JSON provided above needs to converted into a specific format that makes it easier for the external service to work upon

        SObjectDataLoader.RecordsBundle bundle = (SObjectDataLoader.RecordsBundle) JSON.deserializeStrict(recordDetails, SObjectDataLoader.RecordsBundle.class);

        for(SObjectDataLoader.RecordSetBundle sBundle : bundle.RecordSetBundles) {
            recordJSONAsMap.put(sBundle.ObjectType, sBundle.Records);
        }

        return JSON.serialize(recordJSONAsMap);
    }

    /**
     * Returns the map : api_name => label.
     * @param sobjectTypes sobject type for Medwatch/Meddev and all all their child objects.
     */
    protected Map<String,String> getFieldApiNameLabelMap(List<Schema.SObjectType> sobjectTypes){
     
        Map<String,String> labelByApiName =  new Map<String,String>();
        for(Schema.SObjectType sobjectType: sobjectTypes){
            
            Map<String, Schema.SObjectField> fields = sobjectType.getDescribe().fields.getMap();

            for(String apiName : fields.keySet()){
                labelByApiName.put(apiName.replaceAll(SQX.NSPrefix, '') , fields.get(apiName).getDescribe().getLabel().replaceAll(SQX.NSPrefix, ''));
            }
        }
        
        return labelByApiName;
    }



    /**
     * Job Detail for PDF Export job
    */
    public class Export_To_PDF_Job_Detail implements SQX_External_Service_Payload.JobDetail {

        /**
         * Medwatch record (along with the children records) as JSON
         * Used to map values to XML/PDF
        */
        public String recordJSON;

        /**
         * Defines which form/template to use(e.g 3500A)
        */
        public String templateToUse;
        
        /**
         * Name to set for the PDF file generated
        */
        public String fileName;

        public Export_To_PDF_Job_Detail(String recordJSON, String templateToUse, String fileName) {
            this.recordJSON = recordJSON;
            this.templateToUse = templateToUse;
            this.fileName = fileName;
        }
    }


    /**
     * Method perform Database update and captures errors gracefully
     * Returns a list of errors if any
    */
    private List<String> performUpdate(SObject recordToUpdate) {
        List<String> errorsList = new List<String>();

        Database.SaveResult saveResult = new SQX_DB().continueOnError().op_update(new List<SObject> { recordToUpdate }, new List<SObjectField> {}).get(0);

        if(!saveResult.isSuccess()) {

            for(Database.Error error : saveResult.getErrors()) {
                errorsList.add(error.getMessage());
            }
        }

        return errorsList;
    }

}