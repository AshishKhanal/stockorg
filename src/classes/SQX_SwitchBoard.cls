/**
* this class contains the logic to turn on/off various logics in the code
*/
public with sharing class SQX_SwitchBoard{
    static Map<DescribeSObjectResult, Map<String, Boolean>> switches = null;
    
    public static final String SHARE_FINDING_WITH_ACCOUNT = 'shareFinding';
    
    
    /**
    * singleton to initialize the switch values during a transaction
    */
    private static Map<DescribeSObjectResult, Map<String, Boolean>> getSwitches(){
        if(switches == null){
            switches = new Map<DescribeSObjectResult, Map<String, Boolean>>();
            Map<String, Boolean> findingSwitches = new Map<String, Boolean>();
            
            findingSwitches.put(SHARE_FINDING_WITH_ACCOUNT , true);
            
            switches.put(SObjectType.SQX_Finding__c,findingSwitches);

        }
        
        return switches;
    }
    
    /**
    * checks whether a certain functionality is available or not and returns accordingly. 
    * It works on the basis of explicit negation
    * @param objType the type of object for which the functionality is to be tested
    * @param action the action which is to be checked
    * @return <code>true</code> if the action is found enabled or it is not found i.e. defaults to true
    *         <code>false</code> if the action is disabled
    */
    public static boolean isEnabled(DescribeSObjectResult objType, String action){
        boolean isDisabled = false; //default is always enabled
        
        isDisabled =  getSwitches().get(objType) != null && 
                     getSwitches().get(objType).get(action) != null &&
                     !getSwitches().get(objType).get(action);
        
                     
        return !isDisabled; 
    }
    

}