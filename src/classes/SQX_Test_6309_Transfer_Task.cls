@isTest
public class SQX_Test_6309_Transfer_Task {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
    * GIVEN : given Extension SD record
    * WHEN : when submitted the record
    * THEN : transfer only include in extension task
    */  
    public static testMethod void givenExtensionSDRecord_WhenSubmittedTheRecord_ThenTransferIncludeInExtensionTasks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(adminUser){
            // Arrange: Create Extension SD record
            SQX_Supplier_Deviation__c sd = new SQX_Test_Supplier_Deviation().save().sd;
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            SQX_Part__c testPart = SQX_Test_Part.insertPart(null,new User(Id = UserInfo.getUserId()), true, '');
            SQX_Supplier_Deviation__c sd1 = new SQX_Supplier_Deviation__c(Name = 'TestName', 
                                                                         Phone__c = '1234567890',
                                                                         SQX_Account__c = account.Id, 
                                                                         SQX_External_Contact__c = contact.Id,
                                                                         SQX_Supplier_Deviation__c = sd.Id,
                                                                         SQX_Part__c = testPart.Id);
            
            new SQX_DB().op_insert(new List<SQX_Supplier_Deviation__c> {sd1}, new List<SObjectField>{});

            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = standardUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = 'Supplier Deviation', 
                                               Step__c = 1,
                                               Include_In_Extension__c = true,
                                               Name = 'Test',
                                               Task_Type__c = SQX_Task.TASK_TYPE_DOC_REQUEST,
                                               Document_Name__c = 'Sample Document Name');
            
            insert task;
            
            SQX_Task__c task1 = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = standardUser.Id, 
                                               Description__c = 'Test_DESP1', 
                                               Record_Type__c = 'Supplier Deviation', 
                                               Step__c = 1,
                                               Name = 'Test1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            insert task1;
            
            //Act: Submitting the record
            sd1.Status__c =SQX_Supplier_Deviation.STATUS_DRAFT;
            sd1.Record_Stage__c =SQX_Supplier_Deviation.STAGE_TRIAGE;
            sd1.Activity_Code__c = 'submit';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__c> {sd1}, new List<SObjectField>{});
            
            //Assert: Ensured that only include in extension task was transferred
            List<SQX_Deviation_Process__c> dpList =[SELECT Id FROM SQX_Deviation_Process__c];
            System.assertEquals(1, dpList.size());
        }
    }
}