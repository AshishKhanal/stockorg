/**
* Unit tests for locked/unlocked state of inclusion items of a response when the response is submitted for approval, approved, rejected or recalled
*/
@IsTest
public class SQX_Test_1595_LockResponseInclusion {

    static final String ADMIN_USER = 'Admin_User_1',
                        STD_USER_1 = 'Standard_User_1',
                        STD_USER_2 = 'Standard_User_2',
                        LOT_NUMBER = 'LOT#12345';
    @testsetup
    static void setupData() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_1);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_2);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, ADMIN_USER);
    
        System.runas(user1) {
            // add required part
            SQX_Part__c part = SQX_Test_Part.insertPart(null, user1, true, '');
            
            
            // add required nc
            SQX_Test_NC ncUtil = new SQX_Test_NC(true)
                .setDispositionRules(true, Date.today(), false, null);
            ncUtil.save();
            
            // add required impacted product
            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(
                SQX_Nonconformance__c = ncUtil.nc.Id,
                SQX_Impacted_Part__c = part.Id,
                Lot_Number__c = LOT_NUMBER,
                Lot_Quantity__c = 100
            );

            insert impactedProduct;
            
            // add required capa
            SQX_Test_CAPA_Utility capa = new  SQX_Test_CAPA_Utility(true, user2, user2).save();
            
        }
    }

    /*
    * Lock/Unlock states of inclusion items when response is submitted for approval, approved, rejected or recalled
    *   Inclusion item type:            In Approval:    Approved:   Rejected:   Recalled:
    *   Containment                     Lock            Lock        Lock        Unlock
    *   Disposition (Complete status)   Lock            Lock        Unlock      Unlock
    *   Disposition (other statuses)    Lock            Unlock      Unlock      Unlock
    *   Implementation                  Lock            Unlock      Unlock      Unlock
    *   Investigation                   Lock            Lock        Lock        Unlock
    */
    
    public static testmethod void givenResponseSubmittedForApprovalAndRecalled() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get(STD_USER_1),
             user2 = users.get(STD_USER_2),
             user3 = users.get(ADMIN_USER);

        SQX_Test_Finding_Response capaResponse, ncResponse;
        Id containmentId, investigationId, implementationId, disposition1Id, disposition2Id, disposition3Id;
        
        System.runas(user1) {
            SQX_Nonconformance__c nc = [SELECT Id FROM SQX_Nonconformance__c];
            SQX_CAPA__c capa = [SELECT Id FROM SQX_CAPA__c];
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c];

            // add required dispositions
            SQX_Disposition__c disposition1 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_DRAFT
            );

            SQX_Disposition__c disposition2 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_OPEN
            );

            SQX_Disposition__c disposition3 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 20,
                New_Status__c = SQX_Disposition.STATUS_COMPLETE,
                Completed_On__c = System.today() - 1,
                Completed_By__c = 'user'
            );
            insert new List<SQX_Disposition__c>{ disposition1, disposition2, disposition3 };
            disposition1Id = disposition1.Id;
            disposition2Id = disposition2.Id;
            disposition3Id = disposition3.Id;
            
            // add required finding responses
            capaResponse = new SQX_Test_Finding_Response(capa).save();
            ncResponse = new SQX_Test_Finding_Response(nc).save();
            
            // add required containment and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_CONTAINMENT);
            containmentId = capaResponse.inclusions[0].SQX_Containment__c;
            
            // add required investigation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            investigationId = capaResponse.inclusions[1].SQX_Investigation__c;
            
            // add required implementation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_ACTION);
            implementationId = capaResponse.inclusions[2].SQX_Action__c;
            
            // attach dispositions to nc response
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition1);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition2);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition3);
            
            // ACT: responses submitted for approval
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_IN_APPROVAL;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PENDING;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
            
            // inclusion items must be in locked state when submitted for approval
            System.assertEquals(true, Approval.isLocked(containmentId));
            System.assertEquals(true, Approval.isLocked(investigationId));
            System.assertEquals(true, Approval.isLocked(implementationId));
            System.assertEquals(true, Approval.isLocked(disposition1Id));
            System.assertEquals(true, Approval.isLocked(disposition2Id));
            System.assertEquals(true, Approval.isLocked(disposition3Id));
            
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // ACT: responses recalled
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_RECALLED;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
            
            // inclusion items must be in unlocked state when approval request is recalled
            System.assertEquals(false, Approval.isLocked(containmentId));
            System.assertEquals(false, Approval.isLocked(investigationId));
            System.assertEquals(false, Approval.isLocked(implementationId));
            System.assertEquals(false, Approval.isLocked(disposition1Id));
            System.assertEquals(false, Approval.isLocked(disposition2Id));
            System.assertEquals(false, Approval.isLocked(disposition3Id));
        }
    }
    
    public static testmethod void givenResponseApproved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get(STD_USER_1),
             user2 = users.get(STD_USER_2),
             user3 = users.get(ADMIN_USER);

        SQX_Test_Finding_Response capaResponse, ncResponse;
        Id containmentId, investigationId, implementationId, disposition1Id, disposition2Id, disposition3Id;
        
        System.runas(user1) {

            SQX_Nonconformance__c nc = [SELECT Id FROM SQX_Nonconformance__c];
            SQX_CAPA__c capa = [SELECT Id FROM SQX_CAPA__c];
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c];

            // add required dispositions
            SQX_Disposition__c disposition1 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_DRAFT
            );
            SQX_Disposition__c disposition2 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_OPEN
            );
            SQX_Disposition__c disposition3 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 20,
                New_Status__c = SQX_Disposition.STATUS_COMPLETE,
                Completed_On__c = System.today() - 1,
                Completed_By__c = 'user'
            );
            insert new List<SQX_Disposition__c>{ disposition1, disposition2, disposition3 };
            disposition1Id = disposition1.Id;
            disposition2Id = disposition2.Id;
            disposition3Id = disposition3.Id;
            
            // add required finding responses
            capaResponse = new SQX_Test_Finding_Response(capa).save();
            ncResponse = new SQX_Test_Finding_Response(nc).save();
            
            // add required containment and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_CONTAINMENT);
            containmentId = capaResponse.inclusions[0].SQX_Containment__c;
            
            // add required investigation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            investigationId = capaResponse.inclusions[1].SQX_Investigation__c;
            
            // add required implementation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_ACTION);
            implementationId = capaResponse.inclusions[2].SQX_Action__c;
            
            // attach dispositions to nc response
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition1);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition2);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition3);
            
            // required in-approval responses
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_IN_APPROVAL;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PENDING;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // ACT: responses approved
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_APPROVED;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
            
            // inclusion items must be in proper locked/unlocked state when approval request is approved
            System.assertEquals(true, Approval.isLocked(containmentId)); // containment must be in locked state
            System.assertEquals(true, Approval.isLocked(investigationId)); // investigation must be in locked state
            System.assertEquals(false, Approval.isLocked(implementationId)); // implementation must be in unlocked state
            System.assertEquals(false, Approval.isLocked(disposition1Id)); // disposition with new status as draft must be in unlocked state
            System.assertEquals(false, Approval.isLocked(disposition2Id)); // disposition with new status as open must be in unlocked state
            System.assertEquals(true, Approval.isLocked(disposition3Id)); // disposition with new status as complete must be in locked state
        }
    }
    
    public static testmethod void givenResponseRejected() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get(STD_USER_1),
             user2 = users.get(STD_USER_2),
             user3 = users.get(ADMIN_USER);

        SQX_Test_Finding_Response capaResponse, ncResponse;
        Id containmentId, investigationId, implementationId, disposition1Id, disposition2Id, disposition3Id;
        
        System.runas(user1) {
            SQX_Nonconformance__c nc = [SELECT Id FROM SQX_Nonconformance__c];
            SQX_CAPA__c capa = [SELECT Id FROM SQX_CAPA__c];
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c];
            
            // add required dispositions
            SQX_Disposition__c disposition1 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_DRAFT
            );
            SQX_Disposition__c disposition2 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 10,
                New_Status__c = SQX_Disposition.STATUS_OPEN
            );
            SQX_Disposition__c disposition3 = new SQX_Disposition__c(
                SQX_Nonconformance__c = nc.Id,
                Lot_Number__c = LOT_NUMBER, 
                SQX_Part__c =  part.Id, 
                Disposition_Quantity__c = 20,
                New_Status__c = SQX_Disposition.STATUS_COMPLETE,
                Completed_On__c = System.today() - 1,
                Completed_By__c = 'user'
            );
            insert new List<SQX_Disposition__c>{ disposition1, disposition2, disposition3 };
            disposition1Id = disposition1.Id;
            disposition2Id = disposition2.Id;
            disposition3Id = disposition3.Id;
            
            // add required finding responses
            capaResponse = new SQX_Test_Finding_Response(capa).save();
            ncResponse = new SQX_Test_Finding_Response(nc).save();
            
            // add required containment and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_CONTAINMENT);
            containmentId = capaResponse.inclusions[0].SQX_Containment__c;
            
            // add required investigation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            investigationId = capaResponse.inclusions[1].SQX_Investigation__c;
            
            // add required implementation and attach to capa response
            capaResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_ACTION);
            implementationId = capaResponse.inclusions[2].SQX_Action__c;
            
            // attach dispositions to nc response
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition1);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition2);
            ncResponse.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition3);
            
            // required in-approval responses
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_IN_APPROVAL;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PENDING;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // ACT: responses rejected
            capaResponse.response.Status__c = ncResponse.response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
            capaResponse.response.Approval_Status__c = ncResponse.response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_REJECTED;
            update new List<SQX_Finding_Response__c>{ capaResponse.response, ncResponse.response };
            
            // inclusion items must be in proper locked/unlocked state when approval request is rejected
            System.assertEquals(true, Approval.isLocked(containmentId)); // containment must be in locked state
            System.assertEquals(true, Approval.isLocked(investigationId)); // investigation must be in locked state
            System.assertEquals(false, Approval.isLocked(implementationId)); // implementation must be in unlocked state
            System.assertEquals(false, Approval.isLocked(disposition1Id)); // disposition must be in unlocked state
            System.assertEquals(false, Approval.isLocked(disposition2Id)); // disposition must be in unlocked state
            System.assertEquals(false, Approval.isLocked(disposition3Id)); // disposition must be in unlocked state
        }
    }
}