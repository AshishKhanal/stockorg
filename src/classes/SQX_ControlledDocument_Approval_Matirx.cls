/**
* class for the approval matrix of the controlled document
*/
global with sharing class SQX_ControlledDocument_Approval_Matirx{

    /**
     * This method copies the approval matrix steps to the controlled documents provided in the list.
     * @param controlledDocuments the list of controlled documents whose approval matrix step is to be set.
     */
    @InvocableMethod(Label='Copy approval matrix\'s steps to Controlled Document')
    global static void copyApprovalMatrixStepsToCD(List<SQX_Controlled_Document__c> controlledDocuments){
        SQX_ApprovalMatrix_Helper.copyApprovalMatrixSteps(controlledDocuments);
    }
}