/**
 * This unit test class for new code changes for LX controlled document
 */ 
@isTest
public class SQX_Test_5313_Controlled_Document_LX {
    @testSetup
    public static void commonSetup() {
        //Add required users 
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Test_Controlled_Document controlledDocument = new SQX_Test_Controlled_Document();
            controlledDocument.updateContent(true, 'Content.txt');
            controlledDocument.doc.Document_Number__c = 'Controlled_Document_1';
            controlledDocument.save();
        }
    }
    /*
    * return controlled documents information
    * @param documentNumber is controlled document number
    */
    static SQX_Controlled_Document__c getDoc(String documentNumber){
        return [SELECT Id, Content_Reference__c, Description__c, Title__c, 
                Synchronization_Status__c, Approval_Status__c, Document_Status__c, Secondary_Content_Reference__c, OwnerId
                FROM SQX_Controlled_Document__c
                WHERE Document_Number__c = : documentNumber];
    }
    
    /**
     * This test case for add different types of page messages and check if page has any message
     */ 
    @isTest
    static void ensureThatPageErrorAddedAndCheckPageHasAnyMessage()
    {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser'); 
        System.runAs(adminUser){
            //Arrange: Create diff. types of error message in page
            SQX_Action_Response response1 = new SQX_Action_Response();
            response1.errorMessages.add('Error Message..');
            response1.infoMessages.add('Information Message..');
            response1.infoMessages.add('Confirmation Message..');
            
            SQX_Action_Response response2 = new SQX_Action_Response();
            response2.errorMessages.add('Error Message1..');
            
            //Act: Add response1 messages
            response1.addMessages();
            
            //Act: check if any message in page
            if(!response2.hasMessages()){
                //Add response2 message
                response2.addMessages();
            }
            
            //Assert: Ensured that response1 page messages should be added and response2 message not added
            ApexPages.Message[] messages = ApexPages.getMessages();
            System.assertEquals(3, messages.size());
        }
    }
    
    /**
     * This test case for to get current user record permission
     */ 
    @isTest
    static void ensureThatGetCurrentUserRecordPermissions()
    {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser'); 
        System.runAs(adminUser){
            //Arrange: get controlled document
            SQX_Controlled_Document__c doc = getDoc('Controlled_Document_1');
            
            //Act: get current user record permissions
            SQX_Extension_Ctrl_Document_Checkout.SQX_Permissions permissions = SQX_Extension_Ctrl_Document_Checkout.getPermissions(doc.Id);
            
            //Assert: Ensured that current user record permissions should be listed
            system.assertEquals(true, permissions.hasEditAccess);
            system.assertEquals(false, permissions.isSupervisorUser);
            system.assertEquals(false, permissions.isUserCheckOutUser);
        }  
    }
    
    /**
     * This test case for to save controlled document secondary format setting
     */ 
    @isTest
    static void ensuredThatSecondaryFormatSettingSave()
    {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser'); 
        System.runAs(adminUser){
            //Arrange: get controlled document
            SQX_Controlled_Document__c doc = getDoc('Controlled_Document_1');
            
            //Act: update secondary format setting
            SQX_Document_Preview_Controller.updateSecondaryFormat(Label.CQ_UI_Auto,doc.Id, 'CQ Landscape');
            String secondaryFormatSetting=[SELECT Id,Secondary_Format_Setting__c FROM compliancequest__SQX_Controlled_Document__c
                                     WHERE Id = : doc.Id].Secondary_Format_Setting__c;
            //Assert: ensured that secondary format setting updated successfully
            System.assertEquals('CQ Landscape', secondaryFormatSetting);
        }
    }
}