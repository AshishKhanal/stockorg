/**
* tests for ContentVersion object
*/
@isTest
public class SQX_Test_ContentVersion {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensures record type of CQ Content Version cannot be changed to other type
    * verifies error message using custom label SQX_ERR_MSG_CANNOT_CHANGE_CONTENTVERSION_RECORD_TYPE
    */
    public static testmethod void givenChangeContentVersionRecordTypeOfControlledDocument_ErrorThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        System.runas(user1) {
            // get content version record type other than CQ Content
            Set<Id> controlledRecordTypes = SQX_ContentVersion.getControlledRecordTypes();
            List<RecordType> cvNotCQRecTypes = [SELECT Id
                                                FROM RecordType
                                                WHERE SObjectType = :SQX.ContentVersion AND Id NOT IN :controlledRecordTypes
                                                LIMIT 1];
            
            System.assertEquals(1, cvNotCQRecTypes.size(), 'SETUP REQUIRED: Add atleast 1 content record type other than CQ Content before running this test.');
            
            // add required controlled document
            SQX_Test_Controlled_Document cdoc1 = new SQX_Test_Controlled_Document()
                .updateContent(true, 'cdoc1primary.txt')
                .save();
            
            // get content version of cdoc1
            ContentVersion cv1 = [SELECT Id, ContentDocumentId, RecordTypeId FROM ContentVersion WHERE ContentDocumentId = :cdoc1.doc.Content_Reference__c];
            
            // ACT: change record type of content version of a controlled document
            cv1.RecordTypeId = cvNotCQRecTypes[0].Id;
            Database.SaveResult res1 = Database.update(cv1, false);
            
            System.assertEquals(false, res1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(res1.getErrors(), Label.SQX_ERR_MSG_CANNOT_CHANGE_CONTENTVERSION_RECORD_TYPE));
        }
    }
    
    /**
    * ensures content version of a controlled document cannot be changed when linked controlled document does not refer to that content
    * verifies error message using custom label SQX_ERR_MSG_CONTROLLED_DOC_CANT_BE_REFERENCED
    */
    public static testmethod void givenChangeContentVersionControlledDocumentToAnotherControlledDocument_ErrorThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        System.runas(user1) {
            // add required controlled documents
            SQX_Test_Controlled_Document cdoc1 = new SQX_Test_Controlled_Document()
                .updateContent(true, 'cdoc1primary.txt')
                .save();
            SQX_Test_Controlled_Document cdoc2 = new SQX_Test_Controlled_Document()
                .updateContent(true, 'cdoc2primary.txt')
                .save();
            
            // get content version of cdoc1
            ContentVersion cv1 = [SELECT Id, Controlled_Document__c FROM ContentVersion WHERE ContentDocumentId = :cdoc1.doc.Content_Reference__c];
            
            // ACT: change content version of cdoc1 to cdoc2
            cv1.Controlled_Document__c = cdoc2.doc.Id;
            Database.SaveResult res1 = Database.update(cv1, false);
            
            System.assertEquals(false, res1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(res1.getErrors(), Label.SQX_ERR_MSG_CONTROLLED_DOC_CANT_BE_REFERENCED));
        }
    }
    
    /**
    * ensures no error occurs when record type is changed between record types other than CQ type
    */
    public static testmethod void givenChangeContentVersionRecordType_CQRecordTypeNotUsed_Saved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        System.runas(user1) {
            // get content version record type other than CQ Content
            Set<Id> controlledRecordTypes = SQX_ContentVersion.getControlledRecordTypes();
            List<RecordType> cvNotCQRecTypes = [SELECT Id
                                                FROM RecordType
                                                WHERE SObjectType = :SQX.ContentVersion AND Id NOT IN :controlledRecordTypes
                                                LIMIT 2];
            
            System.assertEquals(2, cvNotCQRecTypes.size(), 'SETUP REQUIRED: Add atleast 2 content record types other than CQ Content before running this test.');
            
            // add required content
            ContentVersion cv1 = new ContentVersion(
                RecordTypeId = cvNotCQRecTypes[0].Id,
                VersionData = Blob.valueOf('SampleContent'),
                PathOnClient = 'somefile.txt'
            );
            insert cv1;
            
            // confirm record type
            cv1 = [SELECT Id, RecordTypeId FROM ContentVersion WHERE Id = :cv1.Id];
            
            System.assertEquals(cvNotCQRecTypes[0].Id, cv1.RecordTypeId);
            
            // ACT: set another record type other than CQ type
            cv1.RecordTypeId = cvNotCQRecTypes[1].Id;
            Database.SaveResult res1 = Database.update(cv1, false);
            
            System.assertEquals(true, res1.isSuccess());
            
            cv1 = [SELECT Id, RecordTypeId FROM ContentVersion WHERE Id = :cv1.Id];
            
            System.assertEquals(cvNotCQRecTypes[1].Id, cv1.RecordTypeId);
        }
    }

    /**
      * Given: Content of type regulatory report attached in report submission
      * When: Update/Deletion of those content is tried.
      * Then: Error should be thrown.
	 */
     static testmethod void givenRegReportTypeContent_WhenUpdateAndDeletionIsAttempted_ThenErrorIsThrown() {

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');

        System.runAs(user1) {
			
            // Arrange: Create content version for record type regulatory report.
            ContentVersion cv = new ContentVersion(
                RecordTypeId = SQX_ContentVersion.getRegulatoryContentRecordType(),
                PathOnClient = 'Test.txt',
                VersionData = Blob.valueOf('MOCK')
            );
            insert cv;
			
            // Arrange: Update the contentversion.
            cv = [SELECT Id, ContentDocumentId, PathOnClient, VersionData FROM ContentVersion WHERE Id =: cv.Id];

            ContentVersion newContent = new ContentVersion(
            	VersionData = cv.VersionData,
                ContentDocumentId = cv.ContentDocumentId,
                PathOnClient = cv.PathOnClient
            );

            Test.startTest();
			
            // Act: Insert the updated contentversion in database.
            Database.SaveResult sr = Database.insert(newContent, false);

            Test.stopTest();
			
            // Assert: The update should fail.
            System.assertEquals(false, sr.isSuccess(), 'Expected content insertion to fail as it would update the regulatory report but it passed');
            System.assertEquals(Label.CQ_ERR_MSG_REGULATORY_CONTENT_CANNOT_BE_UPDATED_OR_DELETED, sr.getErrors()[0].getMessage(), 'Unexpected error message');

            // ACT : Try deleting the file
            Database.DeleteResult dr = Database.delete(new ContentDocument(Id = cv.ContentDocumentId), false);
			
            // Assert: The file deletion should fail.
            System.assertEquals(false, dr.isSuccess(), 'Expected content deletion to fail but it passed');
            System.assertEquals(Label.CQ_ERR_MSG_REGULATORY_CONTENT_CANNOT_BE_UPDATED_OR_DELETED, dr.getErrors()[0].getMessage(), 'Unexpected error message');
        }

    }
}