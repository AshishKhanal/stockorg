/*
 * Unit test that does not allow modification of Supplier Deviation number after it's creation.
 */

@isTest
public class SQX_Test_5743_Prevent_Editing_Of_SD_Num {

     @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
    }
    
    /*
     * Given: Any Supplier Deviation Record
     * When: User tries to edit supplier Deviation number 
     * Then: Validation Error msg, 'You cannot edit supplier deviation number.' is thrown
     */
    @isTest
    public static void givenSDRecord_whenUserEditsSDNumber_ThenSDNumberCannotBeModifiedErrorIsThrown(){
        
        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')) {
            //Arrange: Create Supplier Deviation Record
            SQX_Test_Supplier_Deviation sdRecord = new SQX_Test_Supplier_Deviation().save();
            System.assert(sdRecord.sd.Id != null, 'Expected Supplier Deviation record to be created');
            
            //Act: Edit Supplier Deviation Number and update the record
            sdRecord.sd.Name = 'SD-010Modified';
            Database.SaveResult result = Database.update(sdRecord.sd, false);

            //Assert: Validation Error message 'You cannot edit supplier deviation number.' is thrown.
            System.assertEquals('Supplier Deviation Number cannot be modified.',result.getErrors()[0].getMessage(), 'Expected, A validation error to be thrown when user updates SD number');
            //Assert: Make sure that Prevent_Name_Update_Flag__c field is set to true by Process Builder
            System.assertEquals(true, [SELECT prevent_name_update_flag__c FROM SQX_Supplier_Deviation__c WHERE Id =: sdRecord.sd.Id].prevent_name_update_flag__c);

        }
    }
}