/**
* SQX Effectiveness Review object contains the helper methods and status values for the corresponding sobject.
* @author Pradhanta Bhandari
* @date 2014/2/17
*/
public with sharing class SQX_Effectiveness_Review{
    public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_COMPLETED = 'Complete';
    public static final String STATUS_VOID = 'Void';
    public static final String RESOLUTION_CLOSED_EFFECTIVE = 'Closed-Effective';
    public static final String RESOLUTION_CLOSED_INEFFECTIVE = 'Closed-Ineffective';
    public static final String RESOLUTION_SCHEDULE_ANOTHER_REVIEW = 'Schedule another review';
    public static final String RESOLUTION_VOID = 'Void';

    private static Map<String, Set<Id>> objectsProcessed = new Map<String, Set<Id>>();
    
    /**
    * container for all bulkified/trigger operation of the corresponding sobject (SQX_Effectiveness_Review__c)
    * @author Pradhanta Bhandari
    * @date 2014/2/17
    */
    public with sharing class Bulkified{
        public List<SQX_Effectiveness_Review__c> oldData { get; set; }
        public List<SQX_Effectiveness_Review__c> data {get; set;}

        public Map<SObjectType, List<sObject>> objectsToUpdate {get; set;}
        public Map<SObjectType, List<sObject>> objectsToDelete {get; set;}
        public Map<SObjectType, List<sObject>> objectsToInsert {get; set;}

       
        private void initMaps(){
            this.objectsToInsert = new Map<SObjectType, List<sObject>>();
            this.objectsToDelete = new Map<SObjectType, List<sObject>>();
            this.objectsToUpdate = new Map<SObjectType, List<sObject>>();
        }
        
        
        /**
        * default constructor nothing much
        * @author Pradhanta Bhandari
        * @date 2014/2/17
        */
        public Bulkified(){
            this.data = new List<SQX_Effectiveness_Review__c>();
            this.oldData = null;
            initMaps();
        }
        
        /**
        * constructor that can accept data
        * @author Pradhanta Bhandari
        * @date 2014/2/17
        * @param data the data to work with
        */
        public Bulkified(List<SQX_Effectiveness_Review__c> data){
            this.data = data;
            this.oldData = null;
            initMaps();
        }
        
        /**
        *  constructor that can accept data
        * @author Pradhanta Bhandari
        * @date 2014/2/17
        * @param data the data to work with
        * @param oldData the old/previous value of the passed data useful for bulkifying in update queries
        */
        public Bulkified(List<SQX_Effectiveness_Review__c> data, List<SQX_Effectiveness_Review__c> oldData ){
            this.data = data;
            this.oldData = oldData;
            initMaps();
        }
        
        /**
        * filters all reviews that have completed status and are either effective or in effective, if it is update 
        *               ensures that there has been some change
        * @author Pradhanta Bhandari
        * @date 2014/2/17
        * @param isUpdate boolean indicates whether this call is for update query or insert
        * @return the container object with the reviews which are the only ones that are completed
        */
        public Bulkified filterEffectiveOrInEffectiveReviews(boolean isUpdate){
            if(data.size() == 0)
                return this;
            
            
            Bulkified filtered = new Bulkified();
            
            if(isUpdate){
                System.assert(isUpdate,
                    'Expected the number of previous and new values to be same for update queries');
            }
                
            if(isUpdate){
                filtered.oldData = new List<SQX_Effectiveness_Review__c>();
            }
            
            for(integer index = 0; index < this.data.size(); index++){
                SQX_Effectiveness_Review__c review = this.data.get(index);
                boolean hasChanged = true;
                
                if(isUpdate){
                    SQX_Effectiveness_Review__c oldReview = this.oldData.get(index);
                    
                    hasChanged = false;
                    
                    //check if status or resolution has changed
                    if(oldReview.Status__c != review.Status__c ||
                     oldReview.Resolution__c != review.Resolution__c ){
                         hasChanged = true;
                    }
                }
                
                if(hasChanged){
                
                    if(review.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE ||
                       review.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_CLOSED_INEFFECTIVE ||
                       review.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_VOID){
                       filtered.data.add(review);
                       
                       if(isUpdate) {
                           filtered.oldData.add(this.oldData.get(index));
                       }
                    }
                }
            }

            return filtered;
        }
        
        /**
        * this method closes all capas that are related to the effectiveness review, if they are not already
        *      closed
        * @author Pradhanta Bhandari
        * @date 2014/2/17
        * @return returns the same bulkified object for further processing.
        * @lastmodified 2014/2/21 [Sagar]
        *               2014/3/18 [Pradhanta] added support for completion if called multiple times
        * 
        */
        public Bulkified closeRelatedCapas(){
            Map<Id, SQX_CAPA__c> capasToUpdate = new Map<Id, SQX_CAPA__c>();

            if(objectsProcessed.containsKey('closeRelatedCapas') == false){
                objectsProcessed.put('closeRelatedCapas', new Set<Id>());
            }

            Set<Id> alreadyProcessed = objectsProcessed.get('closeRelatedCapas');
            
            
            Map<Id, SQX_Effectiveness_Review__c> mappedReview = new Map<Id, SQX_Effectiveness_Review__c>();
            
            for(integer index = 0; index < this.data.size(); index++){
                SQX_Effectiveness_Review__c review = this.data.get(index);

                //if the effectiveness review has already been considered
                if(alreadyProcessed.contains(review.Id))
                    continue;

                if(review.Id != null)
                        alreadyProcessed.add(review.Id);

                mappedReview.put(review.Id, review);
            }

            //if no review is valid
            if(mappedReview.size() == 0)
                return this;
            
            //fetch all findings that are not closed
            List<SQX_Effectiveness_Review__c> reviews = 
                        [SELECT Id, SQX_CAPA__r.Id, SQX_CAPA__r.Status__c
                        FROM SQX_Effectiveness_Review__c  
                        WHERE Id IN : this.data 
                        AND SQX_CAPA__r.Status__c != :SQX_CAPA.STATUS_CLOSED];
            
            for(SQX_Effectiveness_Review__c review: reviews){
                if(capasToUpdate.containsKey(review.SQX_CAPA__r.Id)){
                    // only if the same finding is being changed in the same transaction
                    // could not just ignore everything and let the last one override
                    // further requirements will make it clearer
                    //TODO: confirm implementation
                    mappedReview.get(review.Id).addError('A review has modified the object in same transaction');
                    continue;
                }
                
                SQX_Effectiveness_Review__c relatedReview = mappedReview.get(review.Id);
                
                
                
                System.assert(relatedReview != null,
                    'Expected review to be mapped but found none for ' + review + ' in ' + relatedReview);

                    
                SQX_CAPA__c capa = new SQX_CAPA__c();
                capa.Id = review.SQX_CAPA__r.Id;
                capa.Status__c = SQX_CAPA.STATUS_CLOSED;
                capa.Resolution__c = (relatedReview.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE) ?
                                     SQX_CAPA.RESOLUTION_EFFECTIVE : (relatedReview.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_VOID) ? SQX_CAPA.RESOLUTION_VOID : SQX_CAPA.RESOLUTION_INEFFECTIVE;
                                    
                capa.Closure_Comment__c = relatedReview.Review__c;
                capa.Rating__c = Decimal.valueOf(relatedReview.Rating__c); //SQX-61 close finding with a rating [Added by Sagar on 2014/2/21]
                if (relatedReview.Create_New_Revision__c == true) {
                    capa.Create_New_Revision__c = true;
                }

                capasToUpdate.put(capa.Id, capa);
            }
            
            
            
            
            if(capasToUpdate.values().size() > 0){
                //update only when necessary
                //don't waste no DML queries
                //update findingsToUpdate.values();
                //add to final update list
                SObjectType capaType = capasToUpdate.values().get(0).getSObjectType();
                List<SObject> allCapas = this.objectsToUpdate.get(capaType);
                if(allCapas == null){
                    allCapas = capasToUpdate.values();
                    objectsToUpdate.put(capaType, allCapas);
                }
                else{
                    allCapas.addAll((List<SObject>)capasToUpdate.values());
                }
                
            }    
        
            return this;
        }

        /**
        * this method  creates a task for each review object in the current trigger, if it is still pending
        * @author Pradhanta Bhandari
        * @date 2014/2/25
        * @return returns the same bulkified object for further processing.
        * @lastmodified 2014/3/18 [Pradhanta] added support for multiple call in same transaction
        * 
        */
        public Bulkified createTaskForReviews(){
            
            List<Task> tasksToBeInserted = new List<Task>();
            if(objectsProcessed.containsKey('createTaskForReviews') == false){
                objectsProcessed.put('createTaskForReviews', new Set<Id>());
            }

            Set<Id> alreadyProcessed = objectsProcessed.get('createTaskForReviews');

            for(SQX_Effectiveness_Review__c review: this.Data){

                if(review.Status__c == SQX_Effectiveness_Review.STATUS_PENDING && alreadyProcessed.contains(review.Id) == false){
                    if(review.Id != null)
                        alreadyProcessed.add(review.Id);

                    tasksToBeInserted.add(SQX_Task_Template.createTaskFor(review, Id.valueOf(review.Effectiveness_Reviewer__c), null,
                     review.Review_Started_On__c,
                        SQX_Task_Template.TaskType.EffectivenessReviewToDo));
                }
            }

            if(tasksToBeInserted.size() > 0){
                SObjectType taskType = tasksToBeInserted.get(0).getSObjectType();
                List<SObject> allTasks = this.objectsToInsert.get(taskType);
                if(allTasks == null){
                    allTasks = tasksToBeInserted;
                    objectsToInsert.put(taskType, allTasks);
                }
                else{
                    allTasks.addAll((List<SObject>)tasksToBeInserted);
                }
            }


            return this;
        }


        /**
        * this method  creates a new effectiveness review for all reviews that are scheduled for another review
        * @author Pradhanta Bhandari
        * @date 2014/2/25
        * @return returns the same bulkified object for further processing.
        * 
        */
        public Bulkified createEffectivenessReviewForScheduledReviews(){
            List<SQX_Effectiveness_Review__c> reviewsToBeAdded = new List<SQX_Effectiveness_Review__c>();

            if(objectsProcessed.containsKey('createEffectivenessReviewForScheduledReviews') == false){
                objectsProcessed.put('createEffectivenessReviewForScheduledReviews', new Set<Id>());
            }

            Set<Id> alreadyProcessed = objectsProcessed.get('createEffectivenessReviewForScheduledReviews');

            for(SQX_Effectiveness_Review__c review: this.Data){
                if(review.Status__c == SQX_Effectiveness_Review.STATUS_COMPLETED &&
                    review.Resolution__c == SQX_Effectiveness_Review.RESOLUTION_SCHEDULE_ANOTHER_REVIEW){

                    if(alreadyProcessed.contains(review.Id))
                        continue;

                    if(review.Id != null)
                        alreadyProcessed.add(review.Id);
                    SQX_Effectiveness_Review__c newReview = new SQX_Effectiveness_Review__c(
                                                            SQX_CAPA__c = review.SQX_CAPA__c,
                                                            Review_Started_On__c = review.Next_Review_Date__c,
                                                            Status__c = SQX_Effectiveness_Review.STATUS_PENDING
                                                            );

                    reviewsToBeAdded.add(newReview);
                }
            }

            if(reviewsToBeAdded.size() > 0){
                SObjectType reviewType = reviewsToBeAdded.get(0).getSObjectType();
                List<SObject> allReviews = this.objectsToInsert.get(reviewType);
                if(allReviews == null){
                    allReviews = reviewsToBeAdded;
                    objectsToInsert.put(reviewType, allReviews);
                }
                else{
                    allReviews.addAll((List<SObject>)reviewsToBeAdded);
                }
            }
            
            

            return this;

        }

        /**
        * this method  creates a task for each review object in the current trigger, if it is still pending
        * @author Pradhanta Bhandari
        * @date 2014/2/25
        * @return returns the same bulkified object for further processing.
        * 
        */
        public Bulkified completeAllTasks(){
            List<Task> taskToBeClosed = new List<Task>();

            if(objectsProcessed.containsKey('completeAllTasks') == false){
                objectsProcessed.put('completeAllTasks', new Set<Id>());
            }

            Set<Id> alreadyProcessed = objectsProcessed.get('completeAllTasks');


            List<Id> completeReviews = new List<Id>();
            for(SQX_Effectiveness_Review__c review: this.Data){
                if(review.Status__c == SQX_Effectiveness_Review.STATUS_COMPLETED || review.Status__c == 'Void'){

                    if(alreadyProcessed.contains(review.Id))
                        continue;
                    
                    if(review.Id != null)
                        alreadyProcessed.add(review.Id);
                    

                    completeReviews.add(review.Id);

                }
            }

            for(List<Task> tasks : [SELECT Id, Status FROM Task WHERE WhatId IN : completeReviews AND (Status = 'In Progress' OR Status = 'Not Started' )]){
                for(Task task : tasks){
                    task.Status = 'Completed';
                    taskToBeClosed.add(task);
                }
            }

            if(taskToBeClosed.size() > 0){
                /*SQX-1056 -> Bug Found: When someone who is not assigned a task for the effectiveness review tries
                to close the task. This happens when a CAPA Owner tries to close an effectiveness review. Since, a 
                CAPA Owner doesn't have read/write privileges on effectiveness reviewer's task.

                Bottom Line: A CAPA owner has R/W access on effectiveness review but not on the task of effectiveness review

                Fix: Since this is a non-stopping issue, we ignore if any such error occurs,Note the 
                continueOnError() on the update request. */
                List<SObjectField> taskFields = new List<SObjectField>{
                                                    Task.fields.Status,
                                                    Task.fields.WhatId,
                                                    Task.fields.OwnerId,
                                                    Task.fields.Subject,
                                                    Task.fields.ActivityDate,
                                                    Task.fields.Priority, 
                                                    Task.fields.Status
                                                };

                new SQX_DB().continueOnError().op_update(taskToBeClosed, taskFields); 
                /* SQX-1056 continue on error if a task is not closed, because this issue is not critical 
                SObjectType taskType = taskToBeClosed.get(0).getSObjectType();
                List<SObject> allTasks = this.objectsToUpdate.get(taskType);
                if(allTasks == null){
                    allTasks = taskToBeClosed;
                    objectsToUpdate.put(taskType, allTasks);
                }
                else{
                    allTasks.addAll((List<SObject>)taskToBeClosed); 
                } */
            }
            
            

            return this;

        }



        /**
        * This method saves all the records as necessary at the end of a change.
        * @author Pradhanta Bhandari
        * @date 2014/2/26
        */
        public Bulkified persistAllRecords(){
            Map<SObjectType, List<SObjectField>> fieldsToSet = new Map<SObjectType, List<SObjectField>>();

            fieldsToSet.put(new Task().getSObjectType(), new List<SObjectField>{
                Task.fields.Status,
                Task.fields.WhatId,
                Task.fields.OwnerId,
                Task.fields.Subject,
                Task.fields.ActivityDate,
                Task.fields.Priority, 
                Task.fields.Status
            });

            fieldsToSet.put(new SQX_CAPA__c().getSObjectType(), new List<SObjectField>{
                SQX_CAPA__c.fields.Status__c,
                SQX_CAPA__c.fields.Resolution__c,
                SQX_CAPA__c.fields.Rating__c,
                SQX_CAPA__c.fields.Closure_Comment__c
            });

            fieldsToSet.put(new SQX_Effectiveness_Review__c().getSObjectType(), new List<SObjectField>{
                SQX_Effectiveness_Review__c.fields.SQX_CAPA__c,
                SQX_Effectiveness_Review__c.fields.Review_Started_On__c,
                SQX_Effectiveness_Review__c.fields.Status__c
            });

            if(this.objectsToInsert.size() > 0){
                new SQX_DB().op_insert(this.objectsToInsert, fieldsToSet );
            }

            if(this.objectsToUpdate.size() > 0){
                new SQX_DB().op_update(this.objectsToUpdate, fieldsToSet );
            }

            if(this.objectsToDelete.size() > 0){
                new SQX_DB().op_delete(this.objectsToDelete);
            }

            return this;
        }
        
     
    }
}