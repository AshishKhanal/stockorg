/**
* This class will contain the common methods and static strings required for the Supplier Document object
**/
public with sharing class SQX_Supplier_Document {
    
    /** 
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newSuppDoc equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Certification__c> newSuppDoc, Map<Id, SQX_Certification__c> oldMap){
            
            super(newSuppDoc, oldMap);
        }
        
        /**
         * method to sync CQ_Account_Owner__c field with Account.CQ_Owner__c
         */
        public Bulkified syncCQAccountOwnerField() {
            
            this.resetView();
            
            // get account ids
            Set<Id> accIds = getIdsForField(this.view, SQX_Certification__c.Account__c);
            
            // get CQ_Owner__c field values
            Map<Id, Account> accountMap = new Map<Id, Account>([ SELECT Id, CQ_Owner__c FROM Account WHERE Id IN :accIds ]);
            
            // sync CQ_Account_Owner__c with account CQ_Owner__c
            for (SQX_Certification__c supDoc : (List<SQX_Certification__c>)this.view) {
                Account acc = accountMap.get(supDoc.Account__c);
                supDoc.CQ_Account_Owner__c = acc != null ? acc.CQ_Owner__c : null;
            }
            
            return this;
        }
        
        /**
         * method to reset Process_Expired_Document__c and Process_Expiry_Notification__c flags when Expiration Date and/or Notify Before Expiration changes
         */
        public Bulkified resetFlagsWhenExpirationInformationIsUpdated() {
            
            this.resetView();
            
            for (SQX_Certification__c supDoc : (List<SQX_Certification__c>)this.view) {
                // get old values
                SQX_Certification__c oldSupDoc = (SQX_Certification__c)oldValues.get(supDoc.Id);
                
                Boolean isExpirationDateChanged = supDoc.Expire_Date__c != oldSupDoc.Expire_Date__c;
                
                // reset Process_Expired_Document__c when expiration date is updated
                if (isExpirationDateChanged) {
                    supDoc.Process_Expired_Document__c = false;
                }
                
                // reset Process_Expiry_Notification__c when expiration date or notify before expiration is updated
                if (isExpirationDateChanged || supDoc.Notify_Before_Expiration__c != oldSupDoc.Notify_Before_Expiration__c) {
                    supDoc.Process_Expiry_Notification__c = false;
                }
            }
            
            return this;
        }

        /**
         * method to set Expired Supplier Document in Account
         */
        public Bulkified setExpiredSupplierDocument(){

            final String ACTION_NAME = 'setExpiredSupplierDocument';
            final Boolean trueValue = true;

            //rule to filter supplier documents
            Rule ruleExpSupDoc = new Rule();
            ruleExpSupDoc.addRule(SQX_Certification__c.Process_Expired_Document__c, RuleOperator.Equals, trueValue);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.SupplierDocument, ACTION_NAME)
                .applyFilter(ruleExpSupDoc, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(!this.view.isEmpty()){
                this.addToProcessedRecordsFor(SQX.SupplierDocument, ACTION_NAME, this.view);

                // get account ids
                Set<Id> accIds = getIdsForField(this.view, SQX_Certification__c.Account__c);
                
                // get Accounts which are not expired
                List<Account> accsToUpdate = [  SELECT Id
                                                FROM Account 
                                                WHERE Id IN :accIds 
                                                    AND Expired_Supplier_Document__c = false ];

                for (Account acc : accsToUpdate) {
                    //set Expired Supplier Document in Account to true
                    acc.Expired_Supplier_Document__c = true;
                }

                /**
                * WITHOUT SHARING USED
                * --------------------
                * Without sharing has been used because the running user may not have permission to update the Account
                * This will cause an error.
                */
                new SQX_DB().withoutSharing().op_update(accsToUpdate, new List<SObjectField>{ Account.Expired_Supplier_Document__c });
            }
            return this;
        }
    }
}