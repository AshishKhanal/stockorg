@isTest
public class SQX_Test_2266_Distribution_Library{
    
    static final String USER_STD_1 = '1796-StdUser-1',
                        USER_ADMIN_1 = '1796-Admin-1';

    static final String DRAFT_LIB_TYPE = 'Draft',
                        RELEASE_LIB_TYPE = 'Release',
                        DISTRIBUTION_LIB_TYPE = 'Distribution';                      

    @testSetup
    static void setupTest(){        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
       User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, USER_STD_1);

        List<Id> userIdList = new List<Id>();
         userIdList.add(standardUser.Id);
         
        SQX_Test_Utilities.createLibraries(userIdList);
    }

    static Boolean runAllTests = true,
                   run_givenADoc_DistributionLibraryCannotBeSameAsDraftOrReleaseLibrary = false;


    /**
     * SETUP: Create two controlled doc.
     * SCENARIO 1
     * ACTION: Add distribution library same as draft library
     * EXPECTED: Save should not be successful and must throw an error
     * SCENARIO 2
     * ACTION: Add distribution library same as release library
     * EXPECTED: Save should not be successful and must throw an error
     * SCENARIO 3
     * ACTION: Add distribution library different from draft and release library
     * EXPECTED: Save should  be successful
     * @date: 2016-05-12
     * @story: [SQX-2266]
    */
    static testmethod void givenADoc_DistributionLibraryCannotBeSameAsDraftOrReleaseLibrary(){
        if (!runAllTests && !run_givenADoc_DistributionLibraryCannotBeSameAsDraftOrReleaseLibrary){
            return;
        }
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {
            // Arrange: Create two controlled document  with primary content.
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'DOC-2-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);

            cDoc2.doc.Title__c = 'Document - 2';
            cDoc2.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc2.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);

            cDoc1.save();
            cDoc2.save();

            SQX_Controlled_Document__c controlledDoc1 = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            SQX_Controlled_Document__c controlledDoc2 = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc2.doc.Id];

            // Act 1: Add distribution library same as draft library
            controlledDoc1.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);

            // Act2: Add distribution library same as release library
            controlledDoc2.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);

            Database.SaveResult result1 = Database.update(controlledDoc1, false);
            Database.SaveResult result2 = Database.update(controlledDoc2, false);

            // Assert 1: Save should not be successful and must throw an error
            System.assert(!result1.isSuccess(), 'Save should not be successful');

            // Assert 2: Save should not be successful and must throw an error
            System.assert(!result2.isSuccess(), 'Save should not be successful');

            String errormsg = 'Distribution Library cannot be same as Draft Library or Release Library.';
            String actualErrMsg1 = result1.getErrors()[0].getMessage();
            String actualErrMsg2 = result2.getErrors()[0].getMessage();

            System.assert(actualErrMsg1.contains(errormsg), 'Expected to get correct error msg' + actualErrMsg1);
            System.assert(actualErrMsg2.contains(errormsg), 'Expected to get correct error msg' + actualErrMsg2);

            // Act 3: Add distribution library different from draft and release library
            controlledDoc1.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            controlledDoc2.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            result1 = Database.update(controlledDoc1, false);
            result2 = Database.update(controlledDoc2, false);

            // Assert 3: Save should  be successful
            System.assert(result1.isSuccess(), 'Save should be successful');
            System.assert(result2.isSuccess(), 'Save should be successful');
        }
    }

    /**
     * Given: Three documents all with secondary content one in sync and others out of sync (one of which doesn't have distribution library)
     * When:  The docs are released
     * Then:  Then docs that are out of sync doesn't get pushed to distribution library, where as one in sync gets pushed to distribution library
     * When:  doc in out of sync comes in sync
     * Then:  doc gets pushed to distribution library
     * When: doc without distribution library has distribution library specified
     * Then: doc doesn't get pushed to distribution library because it is still out of sync
     */
    static testmethod void givenAnOutOfSyncDocument_WhenReleased_ContentIsDistributedOnlyAfterSync(){
        if (!runAllTests && !run_givenADoc_DistributionLibraryCannotBeSameAsDraftOrReleaseLibrary){
            return;
        }
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {
            // Arrange: Create two controlled document  with both content one in sync other out of sync
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt')
                                                   .updateContent(false, 'Doc-1-Secondary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            cDoc1.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc1.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_MANUAL;
            cDoc1.save();

            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt')
                                                   .updateContent(false, 'Doc-1-Secondary.txt');

            cDoc2.doc.Title__c = 'Document - 2';
            cDoc2.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc2.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            cDoc2.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc2.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_MANUAL;
            cDoc2.save();


            SQX_Test_Controlled_Document cDoc3 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-3-Primary.txt')
                                                   .updateContent(false, 'Doc-3-Secondary.txt');

            cDoc3.doc.Title__c = 'Document - 3';
            cDoc3.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc3.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            cDoc3.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_MANUAL;
            cDoc3.save();

            // suppress removal of old revision because it causes an issue with test where content is created and deleted in same
            // transaction
            new SQX_BulkifiedBase().addToProcessedRecordsFor(SQX.ControlledDoc, 'onReleaseRemoveVersionInReleaseContent', new List<SObject> {cDoc1.doc, cDoc2.doc, cDoc3.doc});

            Test.startTest();

            //Act: Release both documents
            update new SQX_Controlled_Document__c[] {
                new SQX_Controlled_Document__c(Id = cDoc1.doc.Id, Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC, Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT),
                new SQX_Controlled_Document__c(Id = cDoc2.doc.Id, Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_IN_SYNC, Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT),
                new SQX_Controlled_Document__c(Id = cDoc3.doc.Id, Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC, Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT)
            };

            //Assert: Ensure that doc 1 hasn't been distributed since it is out of sync but doc 2 has been distributed. and doc 3 isn't distributed too
            System.assertEquals(0, [SELECT Id FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c = : cDoc1.doc.Id].size(), 'No release should have been added');
            System.assertEquals(1, [SELECT Id FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c = : cDoc2.doc.Id].size(), 'Release should have been added');
            System.assertEquals(0, [SELECT Id FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c = : cDoc3.doc.Id].size(), 'No release should have been added');

            //Act: Sync doc 1
            update new SQX_Controlled_Document__c(Id = cDoc1.doc.Id, Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_IN_SYNC);

            //Assert: Ensure that doc 1 has been distributed since it is now in sync
            System.assertEquals(1, [SELECT Id FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c = : cDoc1.doc.Id].size(), 'Release should have been added');

            //Act: add distribution library to doc3
            update new SQX_Controlled_Document__c(Id = cDoc3.doc.Id, Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE));

            //Assert: doc3 isn't distributed yet
            System.assertEquals(0, [SELECT Id FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c = : cDoc3.doc.Id].size(), 'No release should have been added');

            Test.stopTest();
        }
    }
}