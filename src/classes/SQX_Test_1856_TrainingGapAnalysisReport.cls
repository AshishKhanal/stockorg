/**
* Unit test to ensure SQX_Controller_TrainingGapAnalysisReport controller logics for Training Gap Analysis report
*/
@IsTest
public class SQX_Test_1856_TrainingGapAnalysisReport {
    
    public static boolean runAllTests = true,
                          run_givenNoReportFiltersAreProvided_ErrorShown = false,
                          run_givenReportFiltersAreProvided_ProperReportDataGenerated = false;
    
    /***
    * ensures error for no filter provided by user
    */
    public static testmethod void givenNoReportFiltersAreProvided_ErrorShown() {
        if (!runAllTests && !run_givenNoReportFiltersAreProvided_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser) {
            // initialize report controller
            SQX_Controller_TrainingGapAnalysisReport controller = new SQX_Controller_TrainingGapAnalysisReport();
            
            // ACT: call generate report without setting any filter values
            controller.generateReport();
            
            // read page messages for errors
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            
            System.assertEquals(1, msgs.size(), '1 page message is expected.');
            System.assert(msgs[0].getDetail().contains('filter value is required to generate report.'),
                'Page message containing "filter value is required to generate report." is expected.');
        }
    }
    
    /***
    * ensures proper report data is generated using provided personnel and/or job function filters
    */
    public static testmethod void givenReportFiltersAreProvided_ProperReportDataGenerated() {
        if (!runAllTests && !run_givenReportFiltersAreProvided_ProperReportDataGenerated) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = SQX_Test_Job_Function.createJFs(3, true);
            Database.insert(jfs, false); 

            SQX_Job_Function__c jf1 = jfs[0];
            SQX_Job_Function__c jf2 = jfs[1];
            SQX_Job_Function__c jf3 = jfs[2];
            
            // add required personnel
            SQX_Test_Personnel psn1 = new SQX_Test_Personnel();
            psn1.save();
            SQX_Test_Personnel psn2 = new SQX_Test_Personnel();
            psn2.save();
            SQX_Test_Personnel psn3 = new SQX_Test_Personnel();
            psn3.save();
            
            // add required controlled documents
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
                                            
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().save();
                                            
            

            // add required active personnel job functions
            SQX_Personnel_Job_Function__c pjf11 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = psn1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            SQX_Personnel_Job_Function__c pjf12 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = psn1.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = true
            );
            SQX_Personnel_Job_Function__c pjf23 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = psn2.mainRecord.Id,
                SQX_Job_Function__c = jf3.Id,
                Active__c = true
            );
            SQX_Personnel_Job_Function__c pjf33 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = psn3.mainRecord.Id,
                SQX_Job_Function__c = jf3.Id,
                Active__c = true
            );
            Database.insert(new List<SQX_Personnel_Job_Function__c>{ pjf11, pjf12, pjf23, pjf33 }, false);

            // add required active requirements
            SQX_Requirement__c req11 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND
                
            );
            SQX_Requirement__c req12 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc2.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND
               
            );
            SQX_Requirement__c req23 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc2.doc.Id,
                SQX_Job_Function__c = jf3.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND
               
            );
            Database.insert(new List<SQX_Requirement__c>{ req11, req12, req23 }, false);


            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            cDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

             
            SQX_Personnel_Document_Training__c pdt11 = [SELECT Id FROM SQX_Personnel_Document_Training__c 
                                                        WHERE SQX_Controlled_Document__c = :cDoc1.doc.Id 
                                                        AND SQX_Personnel__c = :psn1.mainRecord.Id].get(0);
            
            SQX_Personnel_Document_Training__c pdt12 = [SELECT Id FROM SQX_Personnel_Document_Training__c 
                                                        WHERE SQX_Controlled_Document__c = :cDoc2.doc.Id 
                                                        AND SQX_Personnel__c = :psn1.mainRecord.Id].get(0);
            
            SQX_Personnel_Document_Training__c pdt22 = [SELECT Id FROM SQX_Personnel_Document_Training__c 
                                                        WHERE SQX_Controlled_Document__c = :cDoc2.doc.Id 
                                                        AND SQX_Personnel__c = :psn2.mainRecord.Id].get(0);

            SQX_Personnel_Document_Training__c pdt32 = [SELECT Id FROM SQX_Personnel_Document_Training__c 
                                                        WHERE SQX_Controlled_Document__c = :cDoc2.doc.Id
                                                        AND SQX_Personnel__c = :psn3.mainRecord.Id].get(0);

            SQX_Personnel_Document_Training__c pdtNoJF = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = psn1.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today()
            );
            Database.insert(new List<SQX_Personnel_Document_Training__c>{ pdtNoJF}, false);

            // initialize report controller with personnel filter
            SQX_Controller_TrainingGapAnalysisReport controller = new SQX_Controller_TrainingGapAnalysisReport();
            controller.filterBy.SQX_Personnel__c = psn1.mainRecord.Id;
            
            Test.startTest();
            // ACT: call generate report with personnel filter
            controller.generateReport();
            
            // read page messages for errors
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            String errorMessages='';
            for(ApexPages.Message msg : msgs){
                errorMessages= errorMessages+ msg.getSummary() + '\r\n';
            }
            
            /*two messages
            * Draft Library is required. 
            * Release Library is required. 
            * are expected here
            */
//            System.assertEquals(2, msgs.size(), 'Two page messages are expected.' + errorMessages);
            
            System.assertEquals(1, controller.reportData.size(), 'Report data is expected to be generated with 1 personnel record.');
            
            System.assertEquals(psn1.mainRecord.Id, controller.reportData[0].personnel.Id);
            
            System.assertEquals(2, controller.reportData[0].jobFunctions.size(), 'Report data is expected to be generated with 3 job functions.');
            
            System.assertEquals(jf1.Name, controller.reportData[0].jobFunctions[0].jobFunction.Name, 'Expected proper job function.');
            
            System.assertEquals(1, controller.reportData[0].jobFunctions[0].documentTrainings.size(), 'Expected 1 personnel document training.');
            
            System.assertEquals(pdt11.Id, controller.reportData[0].jobFunctions[0].documentTrainings[0].Id, 'Expected proper personnel document training.');
            
            System.assertEquals(1, controller.reportData[0].jobFunctions[1].documentTrainings.size(), 'Expected 1 personnel document training.');
            
            System.assertEquals(pdt12.Id, controller.reportData[0].jobFunctions[1].documentTrainings[0].Id, 'Expected proper personnel document training.');

            System.assertEquals(jf2.Name, controller.reportData[0].jobFunctions[1].jobFunction.Name, 'Expected proper job function.');

            
            
            // set job function filter and remove personnel filter
            controller.filterBy.SQX_Personnel__c = null;
            controller.filterBy.SQX_Job_Function__c = jf3.Id;
            
            // ACT: call generate report with job function filter
            controller.generateReport();
            
            // read page messages for errors
            msgs = ApexPages.getMessages();

            errorMessages='';
            for(ApexPages.Message msg : msgs){
                errorMessages= errorMessages+ msg.getSummary() + '\r\n';
            }
            
           // System.assertEquals(2, msgs.size(), 'Two page messages are expected. '+ errorMessages);
            
            System.assertEquals(2, controller.reportData.size(), 'Report data is expected to be generated with 2 personnel records.');
            
            SQX_Controller_TrainingGapAnalysisReport.Personnel_Wrapper psn2Wrapper = null;
            SQX_Controller_TrainingGapAnalysisReport.Personnel_Wrapper psn3Wrapper = null;
            for (SQX_Controller_TrainingGapAnalysisReport.Personnel_Wrapper pw : controller.reportData) {
                if (pw.personnel.Id == psn2.mainRecord.Id) {
                    psn2Wrapper = pw;
                }
                if (pw.personnel.Id == psn3.mainRecord.Id) {
                    psn3Wrapper = pw;
                }
            }
            System.assert(psn2Wrapper != null, 'Expected personnel in generated report data.');
            System.assert(psn3Wrapper != null, 'Expected personnel in generated report data.');
            
            System.assertEquals(1, psn2Wrapper.jobFunctions.size(), 'Report data is expected to be generated with 1 job function.');
            
            System.assertEquals(jf3.Name, psn2Wrapper.jobFunctions[0].jobFunction.Name, 'Expected proper job function.');
            
            System.assertEquals(1, psn2Wrapper.jobFunctions[0].documentTrainings.size(), 'Expected 1 personnel document training.');
            
            System.assertEquals(pdt22.Id, psn2Wrapper.jobFunctions[0].documentTrainings[0].Id, 'Expected proper personnel document training.');
            
            System.assertEquals(1, psn3Wrapper.jobFunctions.size(), 'Report data is expected to be generated with 1 job function.');
            
            System.assertEquals(jf3.Name, psn3Wrapper.jobFunctions[0].jobFunction.Name, 'Expected proper job function.');
            
            System.assertEquals(1, psn3Wrapper.jobFunctions[0].documentTrainings.size(), 'Expected 1 personnel document training.');
            
            System.assertEquals(pdt32.Id, psn3Wrapper.jobFunctions[0].documentTrainings[0].Id, 'Expected proper personnel document training.');
            
            
            // set both personnel and job function filters
            controller.filterBy.SQX_Personnel__c = psn1.mainRecord.Id;
            controller.filterBy.SQX_Job_Function__c = jf2.Id;
            
            // ACT: call generate report with both filters
            controller.generateReport();
            
            // read page messages for errors
            msgs = ApexPages.getMessages();
            
            errorMessages='';
            for(ApexPages.Message msg : msgs){
                errorMessages= errorMessages+ msg.getSummary() + '\r\n';
            }
            
//            System.assertEquals(2, msgs.size(), 'Two page messages are expected. '+ errorMessages);
            
            System.assertEquals(1, controller.reportData.size(), 'Report data is expected to be generated with 1 personnel record.');
            
            System.assertEquals(psn1.mainRecord.Id, controller.reportData[0].personnel.Id);
            
            System.assertEquals(1, controller.reportData[0].jobFunctions.size(), 'Report data is expected to be generated with 1 job function.');
                
            System.assertEquals(jf2.Name, controller.reportData[0].jobFunctions[0].jobFunction.Name, 'Expected proper job function.');
            
            System.assertEquals(1, controller.reportData[0].jobFunctions[0].documentTrainings.size(), 'Expected 1 personnel document training.');
            
            System.assertEquals(pdt12.Id, controller.reportData[0].jobFunctions[0].documentTrainings[0].Id, 'Expected proper personnel document training.');
            
            Test.stopTest();
        }
    }
}