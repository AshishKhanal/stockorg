@IsTest
public class SQX_Test_Personnel_Document_Job_Function {
    @testSetup
    public static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'standardUser');
        
        SQX_Job_Function__c jf1, jf2, jf3, jf4;
        SQX_Personnel__c p1, p2;
        
        System.runAs(admin1) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(4, true);
            insert jfs;

            jf1 = jfs[0];
            jf2 = jfs[1];
            jf3 = jfs[2];
            jf4 = jfs[3];
        }
    
        System.runAs(standardUser) {
            // add required personnels
            p1 = new SQX_Test_Personnel().mainRecord;
            p2 = new SQX_Test_Personnel().mainRecord;
            p1.Full_Name__c = 'P1';
            p2.Full_Name__c = 'P2';
            p1.Active__c = true;
            p2.Active__c = true;
            insert new List<SQX_Personnel__c> { p1, p2 };
            
            // add required active personnel job functions
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf2.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf3.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf4.Id, Active__c = false ));
            insert pjfs;
        }
    }

    /*
    * This returns Map of Job Function 
    */
    static Map<String, SQX_Job_Function__c> getJobFunctions() {
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        
        Map<String, SQX_Job_Function__c> jfMap = new Map<String, SQX_Job_Function__c>();
        for (SQX_Job_Function__c jf : jfs) {
            jfMap.put(jf.Name, jf);
        }
        
        return jfMap;
    }
    
    /*
    * This returns Map of Personnels
    */
    static Map<String, SQX_Personnel__c> getPersonnels() {
        List<SQX_Personnel__c> psns = [SELECT Id, Name, Full_Name__c, Identification_Number__c, SQX_User__c, Active__c FROM SQX_Personnel__c];
        
        Map<String, SQX_Personnel__c> psnMap = new Map<String, SQX_Personnel__c>();
        for (SQX_Personnel__c psn : psns) {
            psnMap.put(psn.Full_Name__c, psn);
        }
        
        return psnMap;
    }

    /**  
    * Given: New Controlled Document with requirement.
    * When : Requirement is activated. 
    * Then : New Personnel Document Job Function is Created.
    */
    public testmethod static void whenRequirementIsActivated_NewPDJFIsCreated(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');                    
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');               
        
        System.runas(standardUser) {

            //Arrange1: Create new Control Document
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.save();
            
            //Act1: Requirement is created and Controlled Document is Made Current 
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY);

            new SQX_DB().op_insert(new List<SObject>{req1}, new List<SObjectField>{});

            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();

            //Assert1 : New PDJF is created with training status as pending 
            List<SQX_Personnel_Document_Job_Function__c> pdjf = [SELECT Id, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req1.Id];
            System.assertEquals(1, pdjf.size());
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, pdjf[0].Training_Status__c);
        }
    }

    /**  
    * Given: Controlled Document with Active Requirement and Document Training.
    * When : When Training Status is Changed. 
    * Then : PDJF Training Status is Updated.
    */
    public testmethod static void whenTrainingStatusIsChanged_PDJFTrainingStatusIsChanged(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');              
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');
                       
        System.runas(standardUser) {
            //Arrange1: Create new Control Document
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.save();
            
            //Act1: Requirement is created and Controlled Document is Made Current 
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY);

            new SQX_DB().op_insert(new List<SObject>{req1}, new List<SObjectField>{});

            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();

            //Get PDJF
            List<SQX_Personnel_Document_Job_Function__c> pdjf = [SELECT Id, 
                                                                        Training_Status__c 
                                                                FROM SQX_Personnel_Document_Job_Function__c 
                                                                WHERE Personnel_Id__c =: p1.Id 
                                                                AND  Job_Function_Id__c =: jf1.Id
                                                                AND SQX_Requirement__c =: req1.Id];

            //Assert: PDJF is created and Training Status is Pending                                                 
            System.assertEquals(1, pdjf.size());
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, pdjf[0].Training_Status__c);

            SQX_Personnel_Document_Training__c docTraining = [SELECT Id, 
                                                                    Status__c
                                                              FROM SQX_Personnel_Document_Training__c 
                                                              WHERE SQX_Controlled_Document__c = :document1.doc.Id 
                                                              AND SQX_Personnel__c = :p1.Id].get(0);

            docTraining.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            docTraining.Completion_Date__c = Date.Today();

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            new SQX_DB().op_update(new List<SObject>{docTraining}, new List<SObjectField>{});

            pdjf = [SELECT Id, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req1.Id];

            //Assert: When Document Training's Status is changed PDJF Training Status is also changed                                                
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf[0].Training_Status__c);

        }
    }

    /**  
    * Given: Controlled Document with Active Requirement and Document Training.
    * When : When Training Due Date is Changed. 
    * Then : PDJF Training Due Date is Updated.
    */
    public testmethod static void whenDueDateIsChanged_PDJFTrainingDueDateIsChanged(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');
                            
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');
                       
        
        System.runas(standardUser) {
            //Arrange1: Create new Control Document
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.save();
            
            //Act1: Requirement is created and Controlled Document is Made Current 
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY);

            new SQX_DB().op_insert(new List<SObject>{req1}, new List<SObjectField>{});

            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();

            //Assert: New PDJF is Created
            List<SQX_Personnel_Document_Job_Function__c> pdjf = [SELECT Id, 
                                                                        Training_Status__c 
                                                                FROM SQX_Personnel_Document_Job_Function__c 
                                                                WHERE Personnel_Id__c =: p1.Id 
                                                                AND  Job_Function_Id__c =: jf1.Id
                                                                AND SQX_Requirement__c =: req1.Id];

            System.assertEquals(1, pdjf.size());
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, pdjf[0].Training_Status__c);

            
            SQX_Personnel_Document_Training__c docTraining = [SELECT Id, 
                                                                     Status__c, 
                                                                     Due_Date__c
                                                              FROM SQX_Personnel_Document_Training__c 
                                                              WHERE SQX_Controlled_Document__c = :document1.doc.Id 
                                                              AND SQX_Personnel__c = :p1.Id];

            

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act: Change document training's due date
            docTraining.Due_Date__c = Date.today().addDays(15);
            new SQX_DB().op_update(new List<SObject>{docTraining}, new List<SObjectField>{});

            //Assert: Document Training's due date is updated
            docTraining = [SELECT Id, 
                                  Status__c, 
                                  Due_Date__c 
                           FROM SQX_Personnel_Document_Training__c 
                           WHERE Id =:docTraining.Id];

            System.assertEquals(Date.today().addDays(15), docTraining.Due_Date__c);      
        }   
    }

    /**  
    * Given: Controlled Document with Active Requirement and Document Training.
    * When : Based on PDJF Training Status PJF Status is Evaluated
    *        If any of the document training is Overdue then PJF Status is Overdue.
    *        If no training is overdue and any one of the training is pending then PJF Status is Pending.
    *        If no training are overdue or pending then the PJF status is Current
    * Then : PDJF Training Due Date is Updated.
    */
    public testmethod static void givenControlledDocumentWithActiveRequirement_WhenPDJFStatusIsUpdated_PJFStatusIsEvaluated(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1'),
                            jf4 = jfs.get('JF4');
            
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1'),
                            p2 = psns.get('P2');

        System.runas(standardUser) {

            //Arrange: Create new Control Document
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.doc.Effective_Date__c = Date.Today().addDays(10);
            document1.save();
            
            //Act: Requirement is created and Activated
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 20, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Days_to_Complete_Refresher__c = 10,
                                                             Days_in_Advance_To_Start_Refresher__c = 5);

            new SQX_DB().op_insert(new List<SObject>{req1}, new List<SObjectField>{});

            document1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            document1.save();

            //Arrange: Create another Control Document
            SQX_Test_Controlled_Document document2 = new SQX_Test_Controlled_Document();
            document2.doc.Effective_Date__c = Date.Today().addDays(10);
            document2.save();
        
            //Act: Requirement is created and Activated
            SQX_Requirement__c req2 = new SQX_Requirement__c(SQX_Controlled_Document__c = document2.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 100, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Days_to_Complete_Refresher__c = 10,
                                                             Days_in_Advance_To_Start_Refresher__c = 10);
            new SQX_DB().op_insert(new List<SObject>{req2}, new List<SObjectField>{});

            document2.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            document2.save();

            Test.startTest();
            SQX_Personnel_Document_Job_Function__c pdjf1 = [SELECT Id, 
                                                                   Is_Overdue__c, 
                                                                   Controlled_Document_Status__c, 
                                                                   Training_Status__c 
                                                            FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req1.Id];

            SQX_Personnel_Document_Job_Function__c pdjf2 = [SELECT Id, 
                                                                   Is_Overdue__c, 
                                                                   Controlled_Document_Status__c, 
                                                                   Training_Status__c 
                                                            FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req2.Id];

            // Assert: Training status is NA for inactive PJF                                                
            List<SQX_Personnel_Job_Function__c> pjf = [SELECT Id,
                                                        Training_Status__c
                                                        FROM SQX_Personnel_Job_Function__c 
                                                        WHERE SQX_Job_Function__c =: jf4.Id 
                                                        AND SQX_Personnel__c =:p2.Id ];
            System.assertEquals(1, pjf.size());    
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_NA, pjf[0].Training_Status__c); 
                                                                                                      
            pjf = [SELECT Id,
                        Training_Status__c
                        FROM SQX_Personnel_Job_Function__c 
                        WHERE SQX_Job_Function__c =: jf1.Id 
                        AND SQX_Personnel__c =:p1.Id ];
           
            //Assert 1: Only one Personnel Job Function is Created 
            //           PJF Status is Pending when Controlled Document is Pre-Release and Document Training is Pending.
            System.assertEquals(1, pjf.size());    
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_PENDING, pjf[0].Training_Status__c);

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act: Set Controlled Document as Current
            document1.doc.Date_Issued__c = Date.Today();
            document1.doc.Effective_Date__c = Date.Today();
            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            // Act: PDJF Is overdue flag is checked through time-based workflow, when effective date is met and Personnel Document Training is still 'Pending'
            //      This has been done manually to replicate the behaviour.
            pdjf1.Is_Overdue__c = true;
            new SQX_DB().op_update(new List<SObject>{pdjf1}, new List<SObjectField>{});

            pjf = [SELECT Id,
                    Training_Status__c
                    FROM SQX_Personnel_Job_Function__c 
                    WHERE SQX_Job_Function__c =: jf1.Id 
                    AND SQX_Personnel__c =:p1.Id ];

            //Assert 2: When PDT status => Pending, Controlled Document Status => Current
            //          Then PJF Training Status => Overdue
            //          Even if only one Training for the Personnel is overdue                                            
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_OVERDUE, pjf[0].Training_Status__c);

            
            //Act: Set Controlled Document2 as Current
            document2.doc.Effective_Date__c = Date.Today();
            document2.doc.Date_Issued__c = Date.Today();
            document2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            // Act: PDJF Is overdue flag is checked through time-based workflow, when effective date is met and Personnel Document Training is still 'Pending'
            //      This has been done manually to replicate the behaviour.
            pdjf2.Is_Overdue__c = true;
            new SQX_DB().op_update(new List<SObject>{pdjf2}, new List<SObjectField>{});

            pdjf1 = [SELECT Id, Is_Overdue__c, Controlled_Document_Status__c, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req1.Id];

            pdjf2 = [SELECT Id, Is_Overdue__c, Controlled_Document_Status__c, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c 
                                                            WHERE Personnel_Id__c =: p1.Id 
                                                            AND  Job_Function_Id__c =: jf1.Id
                                                            AND SQX_Requirement__c =: req2.Id];
            
            pjf = [SELECT Id,
                    Training_Status__c
                    FROM SQX_Personnel_Job_Function__c 
                    WHERE SQX_Job_Function__c =: jf1.Id 
                    AND SQX_Personnel__c =:p1.Id ];   

            // Assert 3: PJF Training status must be Overdue if any one of the PDJF Training Status is Overdue                                            
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_OVERDUE, pjf[0].Training_Status__c);

            SQX_Personnel_Document_Training__c pdt1 = [SELECT Id, Status__c, Completion_Date__c
                                                      FROM SQX_Personnel_Document_Training__c
                                                      WHERE SQX_Controlled_Document__c =:document1.doc.Id
                                                      AND SQX_Personnel__c =: p1.Id].get(0);

            //Act: Complete one of the document training 
            pdt1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdt1.Completion_Date__c = Date.Today();
            new SQX_DB().op_update(new List<SObject>{pdt1}, new List<SObjectField>{});

            pdjf1 = [SELECT Id, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c WHERE Id =: pdjf1.Id].get(0);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf1.Training_Status__c);

            pjf = [SELECT Id,
                    Training_Status__c
                    FROM SQX_Personnel_Job_Function__c 
                    WHERE Id =:pjf[0].Id]; 

            //Assert : When one of the two training is overdue, PJF status should be overdue. (Overdue status has higher precedence than other status ) 
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_OVERDUE, pjf[0].Training_Status__c);

            //Act: Complete rest of the document training 
            SQX_Personnel_Document_Training__c pdt2 = [SELECT Id, Status__c, Completion_Date__c
                                                      FROM SQX_Personnel_Document_Training__c
                                                      WHERE SQX_Controlled_Document__c =:document2.doc.Id
                                                      AND SQX_Personnel__c =: p1.Id].get(0);
            pdt2.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdt2.Completion_Date__c = Date.Today();
            new SQX_DB().op_update(new List<SObject>{pdt2}, new List<SObjectField>{});

            //Assert: When Training is completed, respective PDJF's Training status is also updated
            pdjf2 = [SELECT Id, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c WHERE Id =: pdjf2.Id].get(0);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf2.Training_Status__c);

            pjf = [SELECT Id,
                    Training_Status__c
                    FROM SQX_Personnel_Job_Function__c 
                    WHERE Id =:pjf[0].Id]; 
            // Assert 7: When both of the training is complete, PJF status should be complete. 
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_CURRENT, pjf[0].Training_Status__c);

            // Test for Refresher Document Training.
            // When Document Training = Pending
            //      Due Date > Today's Date
            //      PJF Status = Overdue   

            //Get Refresher PDJF
            SQX_Personnel_Document_Job_Function__c  pdjf1R = [SELECT Id,
                                                                    Training_Status__c,
                                                                    Training_Type__c,
                                                                    Next_Refresh_Date__c,
                                                                    Is_Overdue__c
                                                             FROM SQX_Personnel_Document_Job_Function__c
                                                             WHERE Personnel_Id__c =: p1.Id 
                                                             AND  Job_Function_Id__c =: jf1.Id 
                                                             AND Training_Type__c =:SQX_Personnel_Document_Job_Function.TRAINING_TYPE_REFRESHER
                                                             AND Controlled_Document_Id__c =: document1.doc.Id];

            SQX_Personnel_Document_Job_Function__c pdjf2R = [SELECT Id,
                                                                    Training_Status__c,
                                                                    Training_Type__c,
                                                                    Next_Refresh_Date__c,
                                                                    Is_Overdue__c
                                                             FROM SQX_Personnel_Document_Job_Function__c
                                                             WHERE Personnel_Id__c =: p1.Id 
                                                             AND  Job_Function_Id__c =: jf1.Id 
                                                             AND Training_Type__c =:SQX_Personnel_Document_Job_Function.TRAINING_TYPE_REFRESHER
                                                             AND Controlled_Document_Id__c =: document2.doc.Id];   

            //Assert: Is Overdue flag must be false                                              
            System.assertEquals(false, pdjf1R.Is_Overdue__c);
            System.assertEquals(false, pdjf2R.Is_Overdue__c);        

            //Act: Create Refresher Document Training
            pdjf1R.Create_Refresher__c = true;
            pdjf2R.Create_Refresher__c = true;
            new SQX_DB().op_update(new List<SObject>{pdjf1R, pdjf2R}, new List<SObjectField>{});
            
            pjf = [SELECT Id,   
                          Training_Status__c
                    FROM SQX_Personnel_Job_Function__c 
                    WHERE Id =:pjf[0].Id];

            // Assert: PJF status must be In Progress if Due Date is less than Today's Date                                           
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_PENDING, pjf[0].Training_Status__c);

            SQX_Personnel_Document_Training__c rpdt1 = [SELECT Id, 
                                                               Due_Date__c, 
                                                               Status__c,
                                                               Completion_Date__c
                                                        FROM SQX_Personnel_Document_Training__c
                                                        WHERE SQX_Controlled_Document__c =:document1.doc.Id
                                                        AND SQX_Personnel__c =: p1.Id
                                                        AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING].get(0);

            SQX_Personnel_Document_Training__c rpdt2 = [SELECT Id, 
                                                               Due_Date__c, 
                                                               Status__c, 
                                                               Completion_Date__c
                                                        FROM SQX_Personnel_Document_Training__c
                                                        WHERE SQX_Controlled_Document__c =:document2.doc.Id
                                                        AND SQX_Personnel__c =: p1.Id
                                                        AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING].get(0);
            //Act: Update Due Date of one Document Training                                            
            SQX_BulkifiedBase.clearAllProcessedEntities();                                            
            rpdt1.Due_Date__c = Date.Today().addDays(-20);
            new SQX_DB().op_update(new List<SObject>{rpdt1}, new List<SObjectField>{});

            pdjf1R = [SELECT Id,
                            Training_Status__c,
                            Training_Type__c,
                            Next_Refresh_Date__c,
                            Is_Overdue__c
                      FROM SQX_Personnel_Document_Job_Function__c
                      WHERE SQX_Personnel_Document_Training__c =: rpdt1.Id];

            //Assert : When Due Date is smaller than todays date, Is Overdue flag is set         
            System.assertEquals(true, pdjf1R.Is_Overdue__c);

            pjf = [SELECT Id,
                          Training_Status__c
                   FROM SQX_Personnel_Job_Function__c 
                   WHERE Id =:pjf[0].Id];

            // Assert: PJF training status must be overdue when Is_Overdue flag for any one of the PDJF is set 
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_OVERDUE, pjf[0].Training_Status__c);

            // Act: Deactivate PJF for Job function JF1 and Personnel P1
            pjf[0].Active__c = false;
            new SQX_DB().op_update(new List<SObject>{pjf[0]}, new List<SObjectField>{});

            pjf = [SELECT Id,
                          Training_Status__c
                   FROM SQX_Personnel_Job_Function__c 
                   WHERE Id =:pjf[0].Id];
            System.assertEquals(SQX_Personnel_Job_Function.TRAINING_STATUS_NA, pjf[0].Training_Status__c); 

            Test.stopTest();
        }
    }   
}