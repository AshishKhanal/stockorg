@isTest  
private class SQX_Test_Schema_Generator{

    /**
    * Dummy Metadata API web service mock class (see MetadataCreateJobTest.cls for a better example)
    **/
    private class WebServiceMockImpl implements WebServiceMock 
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) 
        {
            if(request instanceof MetadataService.updateMetadata_element)
                response.put('response_x', new MetadataService.updateMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element)
                response.put('response_x', new MetadataService.createMetadataResponse_element());
            return; 
        }
    }    

    /*
     * GIVEN : An finidng object
     * WHEN : Schema is generated
     * THEN: Fields with proper schema is generated
     * @date: 2018/02/07
    */
    @IsTest
    private static void givenObject_SchemaIsGenerated(){
        Test.startTest();
        PageReference pageRef = Page.SQX_Schema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(SQX_Controller_Schema_Generator.PARAM_SCHEMA_OF, SQX.Finding);
        ApexPages.currentPage().getParameters().put(SQX_Controller_Schema_Generator.PARAM_PRETTIFY, null);

        
        SQX_Controller_Schema_Generator controller_Schema_Generator = new SQX_Controller_Schema_Generator();
        String schema_Finding= controller_Schema_Generator.getSchemaJSON();
        
        String schema_find = schema_Finding.remove('SQXSchema.' + SQX.Finding + ' = ');
        Map<String, Object> findingSchema = (Map<String, Object>) JSON.deserializeUntyped(schema_find);
        System.Assert(schema_Finding!=null, 'Expected schema to be loaded');
        
        System.assertEquals(SQX.Finding, findingSchema.get('sObjectName'));
        System.assertEquals(SObjectType.SQX_Finding__c.label, findingSchema.get('sObjectLabel'));
        
        Map<String, Object> findingFieldsSchema = (Map<String, Object>) findingSchema.get('fields'); 
        
        // Assert : Picklist
        Map<String, Object> findingStatusSchema = (Map<String, Object>) findingFieldsSchema.get(SObjectType.SQX_Finding__c.fields.Status__c.name);     
        System.assertEquals('values', findingStatusSchema.get('type'));
        
        // Assert : Lookup
        Map<String, Object> findingPartSchema = (Map<String, Object>) findingFieldsSchema.get(SObjectType.SQX_Finding__c.fields.SQX_Part__c.name);     
        System.assertEquals('reference', findingPartSchema.get('type'));
        
        // Assert : Checkbox
        Map<String, Object> findingCAPARequiredSchema = (Map<String, Object>) findingFieldsSchema.get(SObjectType.SQX_Finding__c.fields.CAPA_Required__c.name);     
        System.assertEquals('boolean', findingCAPARequiredSchema.get('type'));
        
        // Assert : Text
        Map<String, Object> findingTitleSchema = (Map<String, Object>) findingFieldsSchema.get(SObjectType.SQX_Finding__c.fields.Title__c.name);     
        System.assertEquals('string', findingTitleSchema.get('type'));
        
        // Assert : Date
        Map<String, Object> findingOpenDateSchema = (Map<String, Object>) findingFieldsSchema.get(SObjectType.SQX_Finding__c.fields.Open_Date__c.name);     
        System.assertEquals('date', findingOpenDateSchema.get('type'));
        
        // Assert : Child Relationship
        List<Object> findingChilrenSchema = (List<Object>) findingSchema.get('childRelationships');     
        System.assert(findingChilrenSchema.size() > 0);


        controller_Schema_Generator.getName();
        controller_Schema_Generator.getNameSpaceInitializer();
        controller_Schema_Generator.getLanguages();
    }

}