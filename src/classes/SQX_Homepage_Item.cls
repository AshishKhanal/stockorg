/**
* This class includes all the properties that homepage items from all the sources  need to have when they are converted to standardized CQ Homepage Item
* @story   SQX-3159 Homepage UI and Framework Implementation
* @author  Anuj Bhandari
* @date    2017-06-03
*/
public with sharing class SQX_Homepage_Item {

    /*
        Holds a unique identifier for the item
    */
    public Id itemId { get; set; }

    /*
        Holds a list of actions associated with the given item
    */
    public List<SQX_Homepage_Item_Action> actions { get; set; }

    /*
        Holds the name(displayed in the UI) of the action type for the given item
    */
    public String actionType { get; set; }

    /*
        Holds the name(displayed in the UI) of the module the item is associated to
    */
    public String moduleType { get; set; }

    /*
        Holds an instance of the user who is behind the creation of this item
    */
    public User creator {get; set; }

    /*
        Holds the item's created date
    */
    public Date createdDate { get; set; }

    /*
        Holds the related record
    */
    public SObject relatedRecord { get; set; }


    /*
        Holds the age of the item
    */
    public Integer itemAge {
        get {
            Date crDate = this.createdDate;
            Long dtCreated = DateTime.newInstance(crDate.year(), crDate.month(), crDate.day()).getTime();
            Long dtToday = DateTime.now().getTime();
            return (integer) ((dtToday - dtCreated)/(1000*60*60*24));
        }
        private set;
    }


    /*
        Holds overdue date if applicable to the item, else null
    */
    public Date dueDate { get; set; }


    /*
        Holds the number of days the item has gone ahead of the due date
        Set to 0 for cases when due date is less than or equal to current date
    */
    public Integer overdueDays {
        get{
            Integer ovDays = 0;
            if(this.dueDate != null){
                Integer daysDiff = System.today().daysBetween(dueDate);
                ovDays = daysDiff >= 0 ? 0 : daysDiff * -1;
            }
            return ovDays;
        }
        private set;
    }

    /*
        Holds the text to be displayed in the feed for the item
    */
    public String feedText { get; set; }


    /*
        Holds the urgency value(Critical, High, Low) based on the due date
        Ex : if the item's overdue date has exceeded a certain limit specified by the item source,
        this property could be set to Critical or High by the source
    */
    public Urgency urgency { get; set; }

    /*
        Holds the number of days before item becomes due
        This will only hold value if the item's urgency is high(that is due date is approaching close)
    */
    public Integer urgencyDays {
        get{
            if(this.urgency != null &&
                this.urgency == SQX_Homepage_Item.Urgency.High)
            {
                return System.today().daysBetween(this.dueDate);
            }
            return null;
        }
        private set;
    }


    /*
        Property holds the boolean value signifying whether or not the item supports bulk
        <code>true</code> if one of the actions for this item supports bulk else <code>false</code>
    */
    public Boolean supportsBulk {
        get{
            Boolean supportsBulk = false;
            for(SQX_Homepage_Item_Action action : this.actions){
                if(action.supportsBulk){
                    supportsBulk = true;
                    break;
                }
            }
            return supportsBulk;
        }
    }

    /* Possible values for urgency */
    public Enum Urgency { Critical, High, Low }
}