/**
 * Test class for suspect product.
 */ 
@IsTest
public class SQX_Test_8166_Suspect_Product {
    
    private static final String MSG_DRUG_TYPE_REQUIRED ='Drug Type is Required if approval number is entered';
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * GIVEN : A Suspect product
    * WHEN :  When the approval number is entered but not the drug type.
    * THEN :  Exception should be thrown.
    */
    public testMethod static void givenSuspectProduct_WhenApprovalNumberIsEnteredButNotDrugType_ExceptionShouldBeThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            
            //ARRANGE: Create a complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            //ARRANGE: Create regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            insert regReport;
            
            // ARRANGE : Medwatch is created
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.SQX_Regulatory_Report__c=regReport.Id;
            
            insert medwatch;
            
            // ARRANGE : Suspect Product is created
            SQX_Medwatch_Suspect_Product__c susProd = new SQX_Medwatch_Suspect_Product__c();
            susProd.C1_Approval_Number__c ='132';
            susProd.Activity_Code__c = 'Validate';
            
            String actualException = '';
            try{
                // Act: Try to insert suspect product.
                insert susProd;    
            }catch(Exception ex){
                actualException = ex.getMessage();
            }
            
            // Assert: The desired exception should be thrown.
            System.assert(true,actualException.contains(MSG_DRUG_TYPE_REQUIRED));  
        }
    }
}