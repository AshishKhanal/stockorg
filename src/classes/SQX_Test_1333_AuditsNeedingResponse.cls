/**
* This test ensures that only Audits with status Complete with its Finding status Open are returned for Needing Response in homepage component layout
*
*/
@IsTest
public class SQX_Test_1333_AuditsNeedingResponse{
    private static Boolean runAllTests = true,
                            run_auditNeedingResponseReturned= false;

    public static testmethod void auditNeedingResponseReturned(){
            if(!runAllTests && !run_auditNeedingResponseReturned){
                return;
            }
            User auditeeContact =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            
            System.runas(auditeeContact){
                //Arrange: Setup the Audit, and set the audit as complete.
                SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
                audit1.setStatus(SQX_Audit.STATUS_COMPLETE);
                audit1.audit.Start_Date__c = Date.Today();
                audit1.audit.End_Date__c = Date.Today().addDays(10);
                audit1.audit.SQX_Auditee_Contact__c = auditeeContact.Id;
                audit1.save();

                // Act: Attempt to Insert a Finding, with Finding as Open 
                SQX_Test_Finding auditFinding1 =  audit1.addAuditFinding()
                                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .save();
                SQX_Test_Finding auditFinding2 =  audit1.addAuditFinding()
                                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .save();


                //Arrange: Setup the Audit, and set the audit as complete.
                SQX_Test_Audit audit2 = new SQX_Test_Audit(adminUser);
                audit2.setStatus(SQX_Audit.STATUS_COMPLETE);
                audit2.audit.Start_Date__c = Date.Today();
                audit2.audit.End_Date__c = Date.Today().addDays(10);
                audit2.audit.SQX_Auditee_Contact__c = auditeeContact.Id;
                audit2.save();

                // Act: Attempt to Insert a Finding, with Finding as Draft 
                SQX_Test_Finding auditFinding3 =  audit2.addAuditFinding()
                                                    .setStatus(SQX_Finding.STATUS_DRAFT)
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .save();
                SQX_Test_Finding auditFinding4 =  audit2.addAuditFinding()
                                                    .setStatus(SQX_Finding.STATUS_DRAFT)
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .save();

                //Arrange: Setup the Audit, and set the audit as complete.
                SQX_Test_Audit audit3 = new SQX_Test_Audit();
                audit3.setStatus(SQX_Audit.STATUS_COMPLETE);
                audit3.audit.Start_Date__c = Date.Today();
                audit3.audit.End_Date__c = Date.Today().addDays(10);
                audit3.audit.SQX_Auditee_Contact__c = auditeeContact.Id;
                audit3.save();

                // Act: Attempt to Insert a Finding, with Finding as Closed 
                SQX_Test_Finding auditFinding5 =  audit3.addAuditFinding()
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .setClosureComments('Closed')
                                                    .setStatus(SQX_Finding.STATUS_CLOSED)
                                                    .save();
                SQX_Test_Finding auditFinding6 =  audit3.addAuditFinding()
                                                    .setRequired(false, false, false, false, false) //response, containment, and investigation required
                                                    .setClosureComments('Closed')
                                                    .setStatus(SQX_Finding.STATUS_CLOSED)
                                                    .save();

               
                SQX_Controller_Complete_Audits responseNeeded = new SQX_Controller_Complete_Audits();
                
                //invoking the method inside SQX_Controller_Complete_Audits class

                List<SQX_Audit__c> auditsToBeResponsed =  responseNeeded.getCompleteAudits();

                //Assert: responseNeeded must have only one record 
                System.assertEquals(1, auditsToBeResponsed.size());
                //expected list to have audit1 saved
                System.assertEquals(audit1.audit.Id ,auditsToBeResponsed[0].Id );       
            }
                                                                                                                     


    }

}