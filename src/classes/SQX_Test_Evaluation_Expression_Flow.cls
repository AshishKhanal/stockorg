/*
 * Test to ensure layout initializer custom flow for setting Complaint's value
 */
@isTest
public class SQX_Test_Evaluation_Expression_Flow {
	public static final String FLOW_VARIABLE_IN_RECORDS = 'InRecords',
                            FLOW_VARIABLE_IN_ACTION = 'Action',
                            FLOW_VARIABLE_EVAL_EXPR = 'EvaluationExpression';
    
    static Flow.Interview executeInvokedFlow(String namespace, String flowName, Map<String, Object> inputVariableMap ) {
        Flow.Interview cqActionFlow = Flow.Interview.createInterview(namespace, flowName, inputVariableMap);
        cqActionFlow.start();

        return cqActionFlow;
    }
 
    /**
     * This test ensures that layout initializer flow for complaint sets evaluation expression correctly for create action
     */
    static testmethod void ensureThatEvalExpressionIsSetForCreateComplaintInitializer() {
        Flow.Interview flowResult = executeInvokedFlow(null, 'CQ_Complaint_Form_Rules_Evaluation', new Map<String, Object> {
            FLOW_VARIABLE_IN_RECORDS => new SQX_Complaint__c[] {
                new SQX_Complaint__c()
            },
            FLOW_VARIABLE_IN_ACTION => 'create'
        });
        
        System.assertNotEquals(null, flowResult.getVariableValue(FLOW_VARIABLE_EVAL_EXPR));
    }
    
    /**
     * This test ensures that layout initializer flow for complaint sets evaluation expression correctly for edit actions
     */
    static testmethod void ensureThatEvalExpressionIsSetForEditComplaintInitializer() {
        Flow.Interview flowResult = executeInvokedFlow(null, 'CQ_Complaint_Form_Rules_Evaluation', new Map<String, Object> {
            FLOW_VARIABLE_IN_RECORDS => new SQX_Complaint__c[] {
                new SQX_Complaint__c()
            },
            FLOW_VARIABLE_IN_ACTION => 'edit'
        });
        
        System.assertNotEquals(null, flowResult.getVariableValue(FLOW_VARIABLE_EVAL_EXPR));
    }
}