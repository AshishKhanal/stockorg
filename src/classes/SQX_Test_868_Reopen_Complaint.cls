/**
 * test class to ensure all the reopen functionality works correctly
 * @author: Sagar Shrestha
 * @date: 2015-12-22
 * @story: [SQX-868]
 */
@isTest
public class SQX_Test_868_Reopen_Complaint{
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                         Active__c = true,
                                                         Defect_Category__C = 'Test_Category',
                                                         Description__c = 'Test Description',
                                                         Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }        
    }
    static boolean runAllTests = true,
                    run_givenAUser_DefaultPolicySet_ClosedComplaintCanBeReopenedWithClosureComments= false,
                    run_givenAUser_PolicyDefined_ClosedComplaintCanBeReopenedWithoutClosureComments= false,
                    run_givenComplaint_Reopened_ResolutionResetIfVoided= false;
    /**
     *  Secnario 1
     *  Given: Default Policy for reopen complaint is that comment is required.
     *  Action:  Closed complaint is reopened without comments
     *  Expected: Complaint reopen is unsuccessful
     *
     *  Secnario 2
     *  Action: Default Policy where comment in reopen is required, Closed complaint is reopened with comments
     *  Expected: Complaint reopen is successful. Complaint Status is Open, Close Date is set to null, and reopen date is set to today
     * @author: Sagar Shrestha
     * @date: 2015-12-22
     * @story: [SQX-868]
     */
    public testmethod static void givenAUser_DefaultPolicySet_ClosedComplaintCanBeReopenedWithClosureComments(){

        if(!runAllTests && !run_givenAUser_DefaultPolicySet_ClosedComplaintCanBeReopenedWithClosureComments){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Arrange: Create complaint and close cmplaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(null, adminUser);
            complaint.save();
            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            ApexPages.StandardController controller = new ApexPages.StandardController(complaintMain);
            SQX_Extension_Complaint extComplaint =  new SQX_Extension_Complaint(controller);

            //Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;

            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.SQX_Conclusion_Code__c, 
                                                                                            SQX_Complaint__c.fields.Resolution__c});
                                                                                            
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            paramsClose.put('comment', 'Closing Complaint');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be in Closed and found: ' + complaintMain.Status__c);

            //Act1 : Reopen the complaint
            Map<String, String> paramsReopenWithoutComment = new Map<String, String>();
            paramsReopenWithoutComment.put('nextAction', 'reopen');
            paramsReopenWithoutComment.put('comment', '');
            //Assert: should throw an error
            try{
                result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopenWithoutComment, null);
           
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Comment hasn\'t been provided for the action') ? true : false;
                system.assertEquals(true, expectedExceptionThrown, e.getMessage());       
            } 

            //Act2 : Reopen the complaint
            Map<String, String> paramsReopenWithComment = new Map<String, String>();
            paramsReopenWithComment.put('nextAction', 'reopen');
            paramsReopenWithComment.put('comment', 'Reopening Complaint');

            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopenWithComment, null);
            //Assert2 : Should be successfully saved
            System.assertEquals(complaint.complaint.Id, result);

            complaintMain = [SELECT Id, Status__c, Close_Date__c, Reopen_Date__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert2 : Expected Complaint Status is Open, Close Date is set to null, and reopen date is set to today
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);
            System.assertEquals(complaintMain.Close_Date__c, null , 'Expected the close date to be null bu found : ' + complaintMain.Close_Date__c);
            System.assertEquals(complaintMain.Reopen_Date__c, Date.Today() , 'Expected the reopen Date to be Today and found: ' + complaintMain.Reopen_Date__c);


            //test for code coverage
            extComplaint.getComplaintRecordTypeID();
            extComplaint.getChangeSetJSON();
            extComplaint.getDataLoaderConfig();
            extComplaint.getCompleteDataJSON();
            extComplaint.getSecurityMatrix();
        }

        
    }


    /**
     *  Secnario 1
     *  Action: Policy Defined that Reopen comments is not required, Closed complaint is reopened without comments
     *  Expected: Complaint reopen is successful
     *
     * @author: Sagar Shrestha
     * @date: 2016-01-08
     * @story: [SQX-868]
     */
    public testmethod static void givenAUser_PolicyDefined_ClosedComplaintCanBeReopenedWithoutClosureComments(){
        if(!runAllTests && !run_givenAUser_PolicyDefined_ClosedComplaintCanBeReopenedWithoutClosureComments){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        System.runas(adminUser){
            // make comments not required for reopen complaint through esig policy
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = false;
            cqES.Ask_Username__c = false;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration';
            esigConfiguration.Object_Type__C = SQX.Complaint;
            esigConfiguration.Action__c = 'reopen';
            esigConfiguration.Required__c = false;
            esigConfiguration.Comment__c = false;
            //Act: Save an esig config
            estest.allEsigPolicyList.add(esigConfiguration);

            PageReference r = estest.saveElectronicSignatureSetting();
        }

        System.runas(standardUser){
            //Arrange: Create complaint and close complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(null, adminUser);
            complaint.save();
            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            ApexPages.StandardController controller = new ApexPages.StandardController(complaintMain);
            SQX_Extension_Complaint extComplaint =  new SQX_Extension_Complaint(controller);

            //Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;

            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.SQX_Conclusion_Code__c, 
                                                                                            SQX_Complaint__c.fields.Resolution__c});
                                                                                            
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            paramsClose.put('comment', 'Closing Complaint');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be in Closed and found: ' + complaintMain.Status__c);


            //Act : Reopen the complaint
            Map<String, String> paramsReopenWithoutComment = new Map<String, String>();
            paramsReopenWithoutComment.put('nextAction', 'reopen');
            paramsReopenWithoutComment.put('comment', '');

            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopenWithoutComment, null);

            //Assert : Should be successfully processed as comment is not required now
            System.assertEquals(complaint.complaint.Id, result);
           
        }
    }


    /**
     *  Secnario 1
     *  Given: Complaint is voided
     *  Action: Complaint is reopened
     *  Expected: Complaint reopen is successful and resolution is set to blank
     *
     *  Secnario 2
     *  Given : Complaint is closed by selecting a resolution 
     *  Action: Complaint is reopened
     *  Expected: Complaint reopen is successful and resolution is not reset to blank
     * @author: Sagar Shrestha
     * @date: 2015-01-12
     * @story: [SQX-1879]
     */
    public testmethod static void givenComplaint_Reopened_ResolutionResetIfVoided(){

        if(!runAllTests && !run_givenComplaint_Reopened_ResolutionResetIfVoided){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Arrange: Create complaint and void cmplaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(null, adminUser);
            complaint.save();
            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            ApexPages.StandardController controller = new ApexPages.StandardController(complaintMain);
            SQX_Extension_Complaint extComplaint =  new SQX_Extension_Complaint(controller);

            //Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            Map<String, String> paramsVoid = new Map<String, String>();
            paramsVoid.put('nextAction', 'Void');
            paramsVoid.put('comment', 'Voiding Complaint');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsVoid, null);
            complaintMain = [SELECT Id, Status__c, Resolution__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Expected the status of the complaint to be in STATUS_CLOSED, and resolution is void
            System.assertEquals(SQX_Complaint.STATUS_VOID , complaintMain.Status__c, 'Expected the status to be in void and found: ' + complaintMain.Status__c);
            System.assertEquals(SQX_Complaint.RESOLUTION_VOID, complaintMain.Resolution__c , 'Expected the resolution to be in Void and found: ' + complaintMain.Resolution__c); 

            //Act1 : Reopen the complaint
            Map<String, String> paramsReopenWithComment = new Map<String, String>();
            paramsReopenWithComment.put('nextAction', 'reopen');
            paramsReopenWithComment.put('comment', 'Reopening Complaint');

            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopenWithComment, null);
            //Should be successfully saved
            System.assertEquals(complaint.complaint.Id, result);

            complaintMain = [SELECT Id, Status__c, Resolution__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert1 : Expected Complaint Status is Open, and resolution to be blank
            System.assertEquals(SQX_Complaint.STATUS_DRAFT ,complaintMain.Status__c, 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);
            System.assertEquals(true,  string.isBlank(complaintMain.Resolution__c) , 'Expected the resolution to be in blank and found: ' + complaintMain.Resolution__c); 

            //Close Complaint
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            paramsClose.put('comment', 'Closing Complaint');
            complaintMain = [SELECT Resolution__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            SQX_Test_ChangeSet changeset = new SQX_Test_ChangeSet();
            complaintMain.Resolution__c = 'Feedback';
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            changeSet.addChanged(complaintMain)
                     .addOriginalValue(SQX_Complaint__c.Resolution__c, null)
                     .addOriginalValue(SQX_Complaint__c.SQX_Conclusion_Code__c, null);
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaintMain.Id, changeSet.toJSON(), null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c, Resolution__c FROM SQX_Complaint__c WHERE Id =: complaintMain.Id];

            //Expected Complaint Status is Closed, and resolution to feedback
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, complaintMain.Status__c, 'Expected the status to be in Closed and found: ' + complaintMain.Status__c);
            System.assertEquals('Feedback',  complaintMain.Resolution__c , 'Expected the resolution to be in feedback and found: ' + complaintMain.Resolution__c); 

            //Act 2: Reopen Complaint
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopenWithComment, null);
            //Assert2 : Should be successfully saved
            System.assertEquals(complaint.complaint.Id, result);

            complaintMain = [SELECT Id, Status__c, Resolution__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert2 : Expected Complaint Status is Open, and resolution not to be reset
            System.assertEquals(SQX_Complaint.STATUS_DRAFT ,complaintMain.Status__c, 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);
            System.assertEquals('Feedback',  complaintMain.Resolution__c , 'Expected the resolution to be in feedback and found: ' + complaintMain.Resolution__c); 

        }

    }

    /**
     * GIVEN : Complaint is closed
     * WHEN : Complaint is reopened
     * THEN : Reason for Reason is required
     */
    @IsTest
    static void givenComplaint_WhenReopened_ReasonIsRequired(){
      
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'complaintConclusion', 
                                                         Active__c = true,
                                                         Defect_Category__C = 'complaintConclusion',
                                                         Description__c = 'complaintConclusion',
                                                         Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }   
        System.runas(standardUser){
            //Arrange: Create a complaint and closed
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.save();

            complaint.initiate();
            
            complaint.close();

            // ACT : Complaint is reopened without reason for reopen
            SQX_Complaint__c complaintToReopen = new SQX_Complaint__c(Id = complaint.complaint.Id, Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_REOPEN);
            Database.SaveResult result = Database.update(complaintToReopen, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Reason For Reopen is required to reopen complaint.'), result.getErrors());
 			//clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Complaint is reopend with reason for reopen
            complaintToReopen.Reason_For_Reopen__c = 'Data Correction';
            result = Database.update(complaintToReopen, false);

            // ASSERT : Complaint is reopened
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
            complaintToReopen = [SELECT Id, Record_Stage__c, Status__c FROM SQX_Complaint__c WHERE Id = :complaintToReopen.Id];
            System.assertEquals(SQX_Complaint.STATUS_OPEN, complaintToReopen.Status__c);
            System.assertEquals(SQX_Complaint.STAGE_INPROGRESS, complaintToReopen.Record_Stage__c);
        }
    }
}