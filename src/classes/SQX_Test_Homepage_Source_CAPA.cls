/**
*   This class contains test cases for ensuring that capa source is providing homepage items with desired values
*   @author - Piyush Subedi
*   @date - 21-04-2017
*/
@isTest
public class SQX_Test_Homepage_Source_CAPA{


    @testSetup
    public static void commonSetup() {

        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser){

            /* Creating an open CAPA record */
            SQX_Test_CAPA_Utility capa1 = new SQX_Test_CAPA_Utility(true, standardUser, standardUser).save();
            capa1.CAPA.Status__c = SQX_CAPA.STATUS_OPEN;
            capa1.save();

            /* Creating a draft CAPA record */
            SQX_Test_CAPA_Utility capa2 = new SQX_Test_CAPA_Utility(true, adminUser, adminUser).save();
            capa2.save();

            /* adding implementations/actions for the given capa */
            SQX_Action__c implementation1 = new SQX_Action__c
                                            (
                                                Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                Due_Date__c = Date.Today(),
                                                Status__c = SQX_Implementation_Action.STATUS_OPEN
                                            );
            SQX_Action__c implementation2 = new SQX_Action__c
                                            (
                                                Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_PREVENTIVE,
                                                Due_Date__c = Date.Today().addDays(2),
                                                Status__c = SQX_Implementation_Action.STATUS_OPEN,
                                                Description__c = 'Test Record - Nothing to worry'
                                            );

            implementation1.SQX_CAPA__c = capa1.CAPA.Id;
            implementation2.SQX_CAPA__c = capa1.CAPA.Id;

            new SQX_DB().op_insert(new List<SQX_Action__c> { implementation1, implementation2 },
                                    new List<Schema.SObjectField>{ SQX_Action__c.Plan_Type__c,
                                                                   SQX_Action__c.Due_Date__c,
                                                                   SQX_Action__c.Record_Action__c,
                                                                   SQX_Action__c.SQX_CAPA__c,
                                                                   SQX_Action__c.Description__c });
        }
    }


    /**
    *   Given : A set of CAPA records
    *   When : Homepage items are fetched
    *   Then : Desired CAPA items are returned
    */
    testmethod static void givenCAPARecords_WhenHomepageItemsAreFetched_ThenDesiredCAPAItemsAreReturned(){

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');

        System.runAs(stdUser){

            List<SQX_CAPA__c> capa=[select id from SQX_CAPA__c];
            /*added a investigation  for created capa */
            SQX_Investigation__c investigation=new SQX_Investigation__c
                                                (
                                                    SQX_CAPA__C=capa[0].Id,
                                                    Investigation_Summary__c='capa investigation',
                                                    Status__c=SQX_Investigation.STATUS_IN_APPROVAL,
                                                    Approval_Status__c=SQX_Investigation.APPROVAL_STATUS_PENDING
                                                );
            insert investigation;
            
            // Arrange: add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { new SQX_Homepage_Source_CAPA_Items() };

            // Act: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // retrieve CAPA records
            Map<Id, SQX_CAPA__c> capaRecords = new Map<Id, SQX_CAPA__c> ([SELECT Id, Name, Status__c, CAPA_Title__c,
                                                                                (SELECT Id, Name, Due_Date__c, Description__c, 
                                                                                        SQX_CAPA__r.Name, SQX_Capa__r.CAPA_Title__c FROM
                                                                                        SQX_Actions__r),
                                                                                (SELECT Id, SQX_CAPA__r.Title__c,SQX_CAPA__C,
                                                                                        Approval_Status__c,Name,SQX_CAPA__r.Name,
                                                                                        Status__c,CreatedDate from SQX_Investigations__r)
                                                                                FROM SQX_CAPA__c]);

            // Assert: Items retrieved are as expected
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size() == 5, 'Expected exactly 5 items but received ' + hpItems.size() + ' items');

            for(Object hpObj : hpItems){

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_CAPA, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(false, hpItem.supportsBulk);

                if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_NEEDING_RESPONSE){

                    System.assert(capaRecords.containsKey(hpItem.itemId));
                    validateOpenCAPAItem(capaRecords.get(hpItem.itemId), hpItem);

                }else if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS){

                    System.assert(capaRecords.containsKey(hpItem.itemId));
                    validateDraftCAPAItem(capaRecords.get(hpItem.itemId), hpItem);

                }else if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_COMPLETE){
                    
                    Boolean isPresent = false;
                    for(SQX_CAPA__c record : capaRecords.values()){

                        if(record.SQX_Actions__r != null){
                            
                            for(SQX_Action__c act : record.SQX_Actions__r){

                                if(hpItem.itemId == act.Id){
                                    isPresent = true;
                                    validateOpenActionItem(act, hpItem);
                                    break;
                                }
                            }
                        }
                        if(isPresent){
                            break;
                        }
                    }
                    if(!isPresent){
                        System.assert(false, 'Unknown item ' + hpItem);
                    }

                }else if (hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE){
                    Boolean isinvestigationPresent = false;
                    for(SQX_CAPA__c record : capaRecords.values()){
                        if(record.SQX_Investigations__r != null){
                            for(SQX_Investigation__c invesact : record.SQX_Investigations__r){
                                if(hpItem.itemId == invesact.Id){
                                    isinvestigationPresent = true;
                                    validateCapaInvestigationItem(invesact, hpItem);
                                    break;
                                }
                            }
                        }
                        if(isinvestigationPresent){
                            break;
                        }
                    }
                    if(!isinvestigationPresent){
                        System.assert(false, 'Unknown item ' + hpItem);
                    }
                }
                else{
                    System.assert(false, 'Unknown item fetched : '+ hpItem);
                }

            }

        }

    }

    /*
    *   Method to verify if the given homepage item is a valid Open CAPA item
    */
    private static void validateOpenCAPAItem(SQX_CAPA__c capa, SQX_Homepage_Item hpItem){

        // asserting age
        System.assertEquals(0, hpItem.itemAge);

        // asserting item actions
        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_RESPOND, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_CAPA_Response'));
        pr.getParameters().put('Id',capa.Id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);
        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_REPLY, act.actionIcon);
        System.assertEquals(false, act.supportsBulk);

        // asserting feed text
        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_RESPOND, new String[] { capa.Name, String.isBlank(capa.CAPA_Title__c) ? '' : capa.CAPA_Title__c});
        System.assertEquals(expectedFeedText, hpItem.feedText);
    }


    /*
    *   Method to verify if the given homepage item is a valid Draft CAPA item
    */
    private static void validateDraftCAPAItem(SQX_CAPA__c capa, SQX_Homepage_Item hpItem){

        // asserting age
        System.assertEquals(0, hpItem.itemAge);

        // asserting item actions
        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);
        PageReference pr = new PageReference('/' + capa.Id);
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);
        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);
        System.assertEquals(false, act.supportsBulk);

        // asserting feed text
        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, new String[] { capa.Name, String.isBlank(capa.CAPA_Title__c) ? '' : capa.CAPA_Title__c});
        System.assertEquals(expectedFeedText, hpItem.feedText);

    }


    /**
    *   Method to verify if the given homepage item is a valid Open Action Item
    */
    private static void validateOpenActionItem(SQX_Action__c imp, SQX_Homepage_Item hpItem){

        System.assertEquals(imp.Due_Date__c, hpItem.dueDate);

        System.assertEquals(0, hpItem.overdueDays);

        System.assertEquals(SQX_Homepage_Item.Urgency.High, hpItem.urgency);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_RESPOND, act.name);
        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_CAPA_Response'));
        pr.getParameters().put('Id',imp.SQX_CAPA__c);
        pr.getParameters().put('initialTab','responseHistoryTab');
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_REPLY, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        // validating feed text
        String expectedFeedText;
        if(!String.isBlank(imp.Description__c)){

            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITH_SUBJECT ,
                                                new String[] { 
                                                    imp.Description__c, imp.SQX_CAPA__r.Name, String.isBlank(imp.SQX_CAPA__r.CAPA_Title__c) ? '' :  imp.SQX_CAPA__r.CAPA_Title__c});

        }else{

            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITHOUT_SUBJECT ,
                                                new String[] { 
                                                    imp.SQX_CAPA__r.Name, String.isBlank(imp.SQX_CAPA__r.CAPA_Title__c) ? '' :  imp.SQX_CAPA__r.CAPA_Title__c });
        }
        System.assertEquals(expectedFeedText, hpItem.feedText);

    }


    /**
    *   Given : A set of CAPA records and corresponding actions
    *   When : CAPA is voided
    *   Then : No items are returned
    *   Story : SQX-4715
    */
    testmethod static void givenCAPARecords_WhenRecordsAreVoided_ThenNoItemsAreReturned(){
        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');

        System.runAs(stdUser){

            // Arrange : get homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            System.assert(hpItems.size() > 0, 'Expecting homepage items for non-void CAPAs');

            Test.startTest();

            // Act : void capas
            List<SQX_CAPA__c> capas = [SELECT Id, (SELECT Id FROM SQX_Actions__r WHERE Status__c =: SQX_Implementation.STATUS_OPEN) FROM SQX_CAPA__c];
            for(SQX_CAPA__c capa : capas)
            {
                SQX_CAPA__c changedCapa = new SQX_CAPA__c(Id = capa.Id, Closure_Comment__c = 'voided');
                Map<String, Object> submissionSet = new Map<String, Object>();
                List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

                Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedCapa));
                Map<String, Object> originalValues = new Map<String, Object>();

                originalValues.put(SQX.getNSNameFor('Closure_Comment__c'), null);

                changeSet.add(capaMap);

                submissionSet.put('changeSet', changeSet);

                capaMap.put('originalValues', originalValues);

                String stringChangeSet= JSON.serialize(submissionSet);

                //void
                Map<String, String> paramsVoid = new Map<String, String>();
                paramsVoid.put('nextAction', 'void');
                paramsVoid.put('recordID', capa.Id);
                paramsVoid.put('purposeOfSignature', 'Voiding CAPA');
                paramsVoid.put('comment', 'Voiding CAPA');

                // validating that there are open actions in CAPA
                System.assertNotEquals(null, capa.SQX_Actions__r);

                SQX_Extension_CAPA.processChangeSetWithAction(capa.Id, stringChangeSet, null, paramsVoid);
            }

            Test.stopTest();

            // Assert : Expected voided items to be excluded from homepage

            // expecting Open actions to be skipped
            for(SQX_Action__c action : [SELECT Id, Status__c FROM SQX_Action__c WHERE SQX_CAPA__c IN: capas])
            {
                System.assertEquals(SQX_Implementation.STATUS_SKIPPED, action.Status__c);
            }

            homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));
            hpItems = (List<Object>) homepageItems.get('items');

            System.assertEquals(0, hpItems.size());
        }
    }

    /**
    *   Method to verify if the given homepage item is a valid capa investigation item
    */
    private static void validateCapaInvestigationItem(SQX_Investigation__c inves, SQX_Homepage_Item hpItem){

        System.assertEquals(0, hpItem.overdueDays);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_APPROVE_REJECT, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_CAPA'));
        pr.getParameters().put('Id',inves.SQX_CAPA__c);
        pr.getParameters().put('initialTab','responseHistoryTab');
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        // asserting feed text
        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE, new String[] { inves.Name,String.isBlank(inves.SQX_CAPA__r.Title__c) ? '' :  inves.SQX_CAPA__r.Title__c });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }
}