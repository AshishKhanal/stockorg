/**
* This class encapsulates the decision tree logics
*/
public with sharing class SQX_Decision_Tree {
    

    
    /**
    * The code that will be called from the trigger.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        /**
        * Default constructor
        */
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newDecisionTrees equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Decision_Tree__c> newDecisionTrees, Map<Id,SQX_Decision_Tree__c> oldMap){
        
            super(newDecisionTrees, oldMap);
        }
        
        /**
        * This method records the creator of the decision tree record and the date the decision tree record was created.
        * @story 1762
        * @return returns this object for chaining other calls
        */
        public Bulkified captureCreatorOfDecisionTree(){
            
            this.resetView();
            
            Set<ID> taskIds = this.getIdsForField(SQX_Decision_Tree__c.SQX_Task__c);
            Map<Id, SQX_Task__c> tasks = new Map<Id, SQX_Task__c>();
            if(taskIds.size() > 0)
            	 tasks = new Map<Id, SQX_Task__c>([SELECT Id, Name FROM SQX_Task__c WHERE Id IN : taskIds]);
                                                  
            for(SQX_Decision_Tree__c decisionTree : (List<SQX_Decision_Tree__c>)this.view ){
                decisionTree.SQX_User__c = UserInfo.getUserId();
                decisionTree.Run_Date__c = Date.today();
                
                if(tasks.containsKey(decisionTree.SQX_Task__c))
                    decisionTree.Name = tasks.get(decisionTree.SQX_Task__c).Name;
                
                //TODO: Consider not setting run by (User) and run date for decision tree where they have values
                //      There might be cases where historical data is being imported and values are incorrectly reassigned.
            }
            
            return this;
        }
        
        /**
        * This method voids all previously generated reports by a decision tree run that are still in progress.
		*/
        public Bulkified voidPreviouslyGeneratedReportingRequirement(){
            final String ACTION_NAME = 'voidPrevGeneratedReports';
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.DecisionTree, ACTION_NAME);
            
            if(this.view.size() > 0){
                this.addToProcessedRecordsFor(SQX.DecisionTree, ACTION_NAME, this.view);
                List<SQX_Decision_Tree__c> decisionTrees = (List<SQX_Decision_Tree__c>)this.view;
                Set<Id> complaintIds = this.getIdsForField(SQX_Decision_Tree__c.SQX_Complaint__c); 
                Set<Id> dtreeTaskIds = this.getIdsForField(SQX_Decision_Tree__c.SQX_Task__c);
                
                
                /*
                 * ----------
                 * Info:
                 * We need to void all the regulatory reports created by the previous run. The following structure exists
                 * 
                 * Decision Tree Run -> has decision tree's task Id 
                 * Regulatory Report -> has decision tree Id that added the report. Also has a formula field for decision tree's task Id
                 * 
                 * When a new decision tree run is added, we need to locate all the regulatory report for the decision tree's complaint that have the same task Id
                 * 
                 * ---------
                 * Details:
                 * 
                 * Get the list of complaints and associated regulatory report that have pending status have the decision tree task id related to one of the decision
                 * tree task that we have run.
                 * 
                 * It should be noted that regulatory report isn't exact i.e. it doesn't filter out all the reports to be voided, reports from unrelated tasks will
                 * also be fetched, but the filtering does reduce the number of records that we must iterate over in order to void.
                 */
                Map<Id, SQX_Complaint__c> complaints = new Map<Id, SQX_Complaint__c>(
                        [SELECT Id, 
                            (
                                SELECT Id, SQX_Decision_Tree__c, Decision_Tree_Task__c, Status__c 
                                FROM SQX_Regulatory_Reports__r 
                                WHERE Decision_Tree_Task__c IN : dtreeTaskIds AND Status__c = : SQX_Regulatory_Report.STATUS_PENDING
                            ),
                            (
                                SELECT Id, SQX_Task__c FROM SQX_Decision_Trees__r
                                WHERE SQX_Task__c IN : dtreeTaskIds AND  Is_Archived__c != True
                            )
                        FROM SQX_Complaint__c
                        WHERE Id IN : complaintIds]
                );
                
                
                Map<Id, SQX_Regulatory_Report__c> reportsToUpdate = new Map<Id, SQX_Regulatory_Report__c>();
                
                Map<Id, SQX_Decision_Tree__c> dtreeRunsToArchive = new Map<Id, SQX_Decision_Tree__c>();
                
                //loop through the query result and void all the reports that are for the same complaint and task as that of decision tree.
                for(SQX_Decision_Tree__c dtree : decisionTrees){
                    SQX_Complaint__c complaint = complaints.get(dtree.SQX_Complaint__c);
                    for(SQX_Regulatory_Report__c report : complaint.SQX_Regulatory_Reports__r){
                        //if the report hasn't been added by the same decision tree [Not actually necessary for inserts, just a precaution]
                        //              has the same task as the decision tree task
                        //              and hasn't been previously added into reports to be modified.
                        if(report.SQX_Decision_Tree__c != dtree.Id && report.Decision_Tree_Task__c == dtree.SQX_Task__c && !reportsToUpdate.containsKey(report.Id)){
                            reportsToUpdate.put(report.Id, new SQX_Regulatory_Report__c(Id = report.Id,
                                                                                        Status__c = SQX_Regulatory_Report.STATUS_VOID, 
                                                                                        SQX_Decision_Tree_Void__c = dtree.Id ));
                        }
                    }
                    
                    for(SQX_Decision_Tree__c oldDtree : complaint.SQX_Decision_Trees__r){
                        if( oldDtree.Id != dtree.Id &&
                            oldDtree.SQX_Task__c == dtree.SQX_Task__c && dtreeRunsToArchive.containsKey(oldDtree.Id) == false){
                            dtreeRunsToArchive.put(oldDtree.Id, new SQX_Decision_Tree__c(Id = oldDtree.Id, Is_Archived__c = true));
                        }
                    }
                }
                
                SQX_DB dbMgr = new SQX_DB();
                dbMgr.op_update(dtreeRunsToArchive.values(), new List<SObjectField> { SQX_Decision_Tree__c.Is_Archived__c });
                dbMgr.op_update(reportsToUpdate.values(), new List<Schema.SObjectField>{SQX_Regulatory_Report__c.Status__c, SQX_Regulatory_Report__c.SQX_Decision_Tree_Void__c});
            }
            
            return this;
        }

        /**
        * Method to complete complaint task when decision tree is created referring to the cq task.
        */
        public Bulkified completeComplaintTask(){
            final String ACTION_NAME = 'updateRelatedSFtask';

            this.resetView()
                .removeProcessedRecordsFor(SQX.DecisionTree, ACTION_NAME);//Remove all the previously processed records

            List<SQX_Decision_Tree__c> decisionTrees = (List<SQX_Decision_Tree__c>)this.view;

            if(decisionTrees.size() > 0){
                this.addToProcessedRecordsFor(SQX.DecisionTree, ACTION_NAME, decisionTrees);

                Set<Id> cqTaskIds =  this.getIdsForField(decisionTrees, SQX_Decision_Tree__c.SQX_Task__c);
                Set<Id> complaintIds = this.getIdsForField(decisionTrees, SQX_Decision_Tree__c.SQX_Complaint__c);

                //complete related complaint tasks
                SQX_Complaint_Task.completeComplaintTasks(complaintIds, cqTaskIds);
            }
            return this;
        }
        
        /**
         * This method is to prevent deletion of record if parent is locked
         * @author Sanjay Maharjan
         * @date 2019-01-14
         * @story [SQX-7470]
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Decision_Tree__c> decisionTrees = new Map<Id, SQX_Decision_Tree__c>((List<SQX_Decision_Tree__c>)this.view);
            
            for(SQX_Decision_Tree__c decisionTree : [SELECT Id FROM SQX_Decision_Tree__c WHERE Id IN : decisionTrees.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    decisionTrees.get(decisionTree.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }
   }
}