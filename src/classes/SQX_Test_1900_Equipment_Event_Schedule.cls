/**
* Unit tests for Equipment Event Schedule object
*/
@IsTest
public class SQX_Test_1900_Equipment_Event_Schedule {
    
    static boolean runAllTests = true,
                   run_givenEventSchedule_DuplicateEventNameForSameEquiment_ErrorShown = false,
                   run_givenEventSchedule_InvalidControlledDocument_ErrorShown = false,
                   run_givenEventSchedule_LastPerformedFutureDate_ErrorShown = false,
                   run_givenEventSchedule_NegativeInterval_ErrorShown = false,
                   run_givenRecurringEventSchedule_RelatedFieldInformationNotSet_ErrorsShown = false,
                   run_givenRecurringEventSchedule_NextDueDateNotSet_CalculatedDateIsSet = false;
    
    /**
    * unit tests for uniqueness constraint for equipment event schedule object
    */
    public static testmethod void givenEventSchedule_DuplicateEventNameForSameEquiment_ErrorShown() {
        if (!runAllTests && !run_givenEventSchedule_DuplicateEventNameForSameEquiment_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // adding required Equipment
            SQX_Equipment__c eqp2 = new SQX_Equipment__c (
                Name = 'Equipment2',
                Equipment_Description__c = 'Equipment2 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp2, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // adding required Equipment Event Schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false
            );
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // creating event schedule with same event name for same equipment
            SQX_Equipment_Event_Schedule__c sch2 = new SQX_Equipment_Event_Schedule__c(
                Name = 'MainTENance eVENT',
                SQX_Equipment__c = eqp1.Id,
                Active__c = true
            );
            
            // ACT: saving duplicate event for same equipment
            result1 = Database.insert(sch2, false);
            
            System.assert(result1.isSuccess() == false,
                'Duplicate equipment event schedule is not expected to be saved.\n' + result1.getErrors());
            
            String errmsg_DuplicateRecord = 'duplicate value found';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result1.getErrors(), errmsg_DuplicateRecord),
                'Expected error message containing "' + errmsg_DuplicateRecord + '" not found.\n' + result1.getErrors());
            
            
            // ACT: add same event name for another equipment
            sch2.SQX_Equipment__c = eqp2.Id;
            result1 = Database.insert(sch2, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule with same event name for another equipment is expected to be saved.\n' + result1.getErrors());
        }
    }
    
    /**
    * unit tests for controlled document filter in applicable procedure field of equipment event schedule object.
    * applicable procedure should refer to not obsolete document of controlled document record type.
    */
    public static testmethod void givenEventSchedule_InvalidControlledDocument_ErrorShown() {
        if (!runAllTests && !run_givenEventSchedule_InvalidControlledDocument_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required documents
            SQX_Test_Controlled_Document obsoleteDoc = new SQX_Test_Controlled_Document().save();
            obsoleteDoc.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            SQX_Test_Controlled_Document templateDoc = new SQX_Test_Controlled_Document();
            templateDoc.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            templateDoc.save();
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating equipment event schedule with obsolete controlled document
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false,
                SQX_Applicable_Procedure__c = obsoleteDoc.doc.Id
            );
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule refering to obsolete controlled document is not expected to be saved.\n' + result1.getErrors());
            
            String errmsg_ControlledDocumentFilter = 'Controlled Document that is not obsolete can only be used for Applicable Procedure.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_ControlledDocumentFilter),
                'Expected error message "' + errmsg_ControlledDocumentFilter + '" not found.\n' + result1.getErrors());
            
            
            // creating equipment event schedule with template document
            sch1.SQX_Applicable_Procedure__c = templateDoc.doc.Id;
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule refering to template document is not expected to be saved.\n' + result1.getErrors());
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_ControlledDocumentFilter),
                'Expected error message "' + errmsg_ControlledDocumentFilter + '" not found.\n' + result1.getErrors());
        }
    }
    
    /**
    * unit tests for Prevent_Last_Performed_Future_Date validation rule
    */
    public static testmethod void givenEventSchedule_LastPerformedFutureDate_ErrorShown() {
        if (!runAllTests && !run_givenEventSchedule_LastPerformedFutureDate_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Recurring_Event__c = false,
                Last_Performed_Date__c = Date.today() + 1
            );
            
            // ACT: Save recurring equipment event schedule with future date in last performed date
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule is not expected to be saved.');
            
            String errmsg_Prevent_Last_Performed_Future_Date = 'Last Performed Date cannot be future date.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Prevent_Last_Performed_Future_Date),
                'Expected error message "' + errmsg_Prevent_Last_Performed_Future_Date + '" for validation rule Prevent_Last_Performed_Future_Date not found.\n'
                + result1.getErrors());
            
            
            // adding required equipment event schedule
            sch1.Last_Performed_Date__c = null;
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // setting last performed date to future date
            sch1.Last_Performed_Date__c = Date.today() + 1;
            
            // ACT: Update recurring equipment event schedule with future date in last performed date
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule is not expected to be saved.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Prevent_Last_Performed_Future_Date),
                'Expected error message "' + errmsg_Prevent_Last_Performed_Future_Date + '" for validation rule Prevent_Last_Performed_Future_Date not found.\n'
                + result1.getErrors());
        }
    }
    
    /**
    * unit tests for Prevent_Negative_Recurring_Interval validation rule
    */
    public static testmethod void givenEventSchedule_NegativeInterval_ErrorShown() {
        if (!runAllTests && !run_givenEventSchedule_NegativeInterval_ErrorShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Recurring_Event__c = false,
                Recurring_Interval__c = -1
            );
            
            // ACT: Save recurring equipment event schedule with negative recurring interval
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule is not expected to be saved.');
            
            String errmsg_Prevent_Negative_Recurring_Interval = 'Recurring Interval cannot be a negative number.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Prevent_Negative_Recurring_Interval),
                'Expected error message "' + errmsg_Prevent_Negative_Recurring_Interval + '" for validation rule Prevent_Negative_Recurring_Interval not found.\n'
                + result1.getErrors());
            
            
            // adding required equipment event schedule
            sch1.Recurring_Interval__c = 5;
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // setting negative recurring interval
            sch1.Recurring_Interval__c = -1;
            
            // ACT: Update recurring equipment event schedule with negative recurring interval
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Equipment event schedule is not expected to be saved.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Prevent_Negative_Recurring_Interval),
                'Expected error message "' + errmsg_Prevent_Negative_Recurring_Interval + '" for validation rule Prevent_Negative_Recurring_Interval not found.\n'
                + result1.getErrors());
        }
    }
    
    /**
    * unit tests for validation rules related to a recurring event schedule
    * related validation rules:
    *   - Recurring_Event_Interval_Required
    *   - Recurring_Event_Schedule_Basis_Required
    *   - Recurring_Event_Unit_Required
    */
    public static testmethod void givenRecurringEventSchedule_RelatedFieldInformationNotSet_ErrorsShown() {
        if (!runAllTests && !run_givenRecurringEventSchedule_RelatedFieldInformationNotSet_ErrorsShown) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Recurring_Event__c = true,
                Schedule_Basis__c = null
            );
            
            // ACT: Save recurring equipment event schedule with recurring information not set
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Recurring equipment event schedule with no recurring information is not expected to be saved.');
            
            String errmsg_Recurring_Event_Interval_Required = 'Recurring Interval is required and must be greater than zero for a recurring event.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Interval_Required),
                'Expected error message "' + errmsg_Recurring_Event_Interval_Required + '" for validation rule Recurring_Event_Interval_Required not found.\n' + result1.getErrors());
            
            String errmsg_Recurring_Event_Schedule_Basis_Required = 'Schedule Basis is required for a recurring event.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Schedule_Basis_Required),
                'Expected error message "' + errmsg_Recurring_Event_Schedule_Basis_Required + '" for validation rule Recurring_Event_Schedule_Basis_Required not found.\n'
                + result1.getErrors());
            
            String errmsg_Recurring_Event_Unit_Required = 'Recurring Unit is required for a recurring event.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Unit_Required),
                'Expected error message "' + errmsg_Recurring_Event_Unit_Required + '" for validation rule Recurring_Event_Unit_Required not found.\n' + result1.getErrors());
            
            
            // adding required equipment event schedule
            sch1.Recurring_Interval__c = 5;
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_DAY;
            sch1.Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_DUE_DATE;
            result1 = Database.insert(sch1, false);
            System.assert(result1.isSuccess(), 'Required equipment event schedule is expected to be saved.\n' + result1.getErrors());
            
            // setting recurring information to blank
            sch1.Recurring_Interval__c = null;
            sch1.Recurring_Unit__c = null;
            sch1.Schedule_Basis__c = null;
            
            // ACT: Update recurring equipment event schedule with blank recurring information
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Recurring equipment event schedule with no recurring information is not expected to be saved.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Interval_Required),
                'Expected error message "' + errmsg_Recurring_Event_Interval_Required + '" for validation rule Recurring_Event_Interval_Required not found.\n' + result1.getErrors());
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Schedule_Basis_Required),
                'Expected error message "' + errmsg_Recurring_Event_Schedule_Basis_Required + '" for validation rule Recurring_Event_Schedule_Basis_Required not found.\n'
                + result1.getErrors());
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Unit_Required),
                'Expected error message "' + errmsg_Recurring_Event_Unit_Required + '" for validation rule Recurring_Event_Unit_Required not found.\n' + result1.getErrors());
            
            
            // setting negative recurring interval
            sch1.Recurring_Interval__c = -5;
            
            // ACT: Update recurring equipment event schedule with negative recurring interval
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Recurring equipment event schedule with no recurring information is not expected to be saved.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Interval_Required),
                'Expected error message "' + errmsg_Recurring_Event_Interval_Required + '" for validation rule Recurring_Event_Interval_Required not found.\n' + result1.getErrors());
            
            
            // setting zero recurring interval
            sch1.Recurring_Interval__c = 0;
            
            // ACT: Update recurring equipment event schedule with zero recurring interval
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess() == false,
                'Recurring equipment event schedule with no recurring information is not expected to be saved.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), errmsg_Recurring_Event_Interval_Required),
                'Expected error message "' + errmsg_Recurring_Event_Interval_Required + '" for validation rule Recurring_Event_Interval_Required not found.\n' + result1.getErrors());
        }
    }
    
    /**
    * unit tests to ensure next due date value is calculated properly and set for a recurring event schedule has null next due date with last performed date and
    * proper recurrence information
    */
    public static testmethod void givenRecurringEventSchedule_NextDueDateNotSet_CalculatedDateIsSet() {
        if (!runAllTests && !run_givenRecurringEventSchedule_NextDueDateNotSet_CalculatedDateIsSet) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(user1) {
            Database.SaveResult result1;
            
            // adding required Equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c (
                Name = 'Equipment1',
                Equipment_Description__c = 'Equipment1 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_ACTIVE,
                Requires_Periodic_Calibration__c = false
            );
            result1 = Database.insert(eqp1, false);
            System.assert(result1.isSuccess(), 'Required equipment is expected to be saved.\n' + result1.getErrors());
            
            // creating Equipment Event Schedule
            Integer recurringInterval = 2;
            Date lastPerformedDate = Date.today();
            
            SQX_Equipment_Event_Schedule__c sch1 = new SQX_Equipment_Event_Schedule__c(
                Name = 'Maintenance Event',
                SQX_Equipment__c = eqp1.Id,
                Active__c = false,
                Recurring_Event__c = false,
                Recurring_Interval__c = recurringInterval,
                Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_DAY,
                Schedule_Basis__c = SQX_Equipment_Event_Schedule.SCHEDULE_BASIS_DUE_DATE,
                Last_Performed_Date__c = lastPerformedDate
            );
            
            // expected calculated next due date
            Date expectedNextDueDate = lastPerformedDate.addDays(recurringInterval);
            
            // ACT: Save non-recurring equipment event schedule
            result1 = Database.insert(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(null, sch1.Next_Due_Date__c, 'Next Due Date is not expected to be calculated for a non-recurring event.');
            
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // setting recurring event
            sch1.Recurring_Event__c = true;
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
            
            
            // set unit to week and next due date to null
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_WEEK;
            sch1.Next_Due_Date__c = null;
            expectedNextDueDate = lastPerformedDate.addDays(7 * recurringInterval);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
            
            
            // set unit to month and next due date to null
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_MONTH;
            sch1.Next_Due_Date__c = null;
            sch1.Recurring_Event__c = true;
            expectedNextDueDate = lastPerformedDate.addMonths(recurringInterval);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
            
            
            // set unit to quarter and next due date to null
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_QUARTER;
            sch1.Next_Due_Date__c = null;
            sch1.Active__c = true;
            expectedNextDueDate = lastPerformedDate.addMonths(3 * recurringInterval);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
            
            
            // set unit to half year and next due date to null
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_HALF_YEAR;
            sch1.Next_Due_Date__c = null;
            expectedNextDueDate = lastPerformedDate.addMonths(6 * recurringInterval);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
            
            
            // set unit to year and next due date to null
            sch1.Recurring_Unit__c = SQX_Equipment_Event_Schedule.RECURRING_UNIT_YEAR;
            sch1.Next_Due_Date__c = null;
            expectedNextDueDate = lastPerformedDate.addYears(recurringInterval);
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: save recurring equipment event schedule
            result1 = Database.update(sch1, false);
            
            System.assert(result1.isSuccess(),
                'Equipment event schedule is expected to be saved.');
            
            sch1 = [SELECT Id, Next_Due_Date__c FROM SQX_Equipment_Event_Schedule__c WHERE Id = :sch1.Id];
            System.assertEquals(expectedNextDueDate, sch1.Next_Due_Date__c, 'Next Due Date is expected to be properly calculated.');
        }
    }
}