/**
* Test Class for preventing performed date to be future date in Sample Tracking.
*/
@isTest
public class SQX_Test_8470_Sample_Tracking {
    static SQX_Sample_Tracking__c[] getSampleTrackings(DateTime performedDate) {
        SQX_Sample_Tracking__c sample = new SQX_Sample_Tracking__c();
        List<SQX_Sample_Tracking__c> sampleList = new List<SQX_Sample_Tracking__c>();
        sample.From_Location__c='Sample Location';
        sample.Performed_By__c='asdasd';
        DateTime dateAndTime = performedDate;
        sample.Performed_Date__c=dateAndTime;
        sample.To_Location__c='Sample Location';
        sampleList.add(sample);
        
        return sampleList;
    }

    /**
    * Given: A sample tracking with future performed date
    * When: it is inserted
    * Then: an error is thrown saying that future date can't be set on performed date
    */
    static testmethod void givenSampleTrackingWithFutureDate_WhenItIsSaved_AnErrorIsThrown(){
        // Arrange: Create a sample tracking record with future performed date
        List<SQX_Sample_Tracking__c> sampleList = getSampleTrackings(DateTime.now().addDays(10));
        
        // Act: Try to save the record with future date
        Database.SaveResult[] saveResults = Database.insert(sampleList, false);
        
        // Assert: Ensure that the record wasn't saved
        System.assert(!saveResults[0].isSuccess(), 'Sample tracking with future performed date shouldnt get inserted');
    }
    
    
    
    /**
    * Given: A sample tracking with future performed date
    * When: it is inserted
    * Then: an error is thrown saying that future date can't be set on performed date
    */
    static testmethod void givenSampleTrackingWithPastDate_WhenItIsSaved_AnErrorIsntThrown(){
        // Arrange: Create a sample tracking record with future performed date
        List<SQX_Sample_Tracking__c> sampleList = getSampleTrackings(DateTime.now());
        
        // Act: Try to save the record with future date
        Database.SaveResult[] saveResults = Database.insert(sampleList, false);
        
        // Assert: Ensure that the record wasn't saved
        System.assert(saveResults[0].isSuccess(), 'Sample tracking with today as performed date should get inserted. Error: ' + saveResults[0]);
    }
}