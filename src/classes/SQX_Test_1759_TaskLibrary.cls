/**
 * This test ensures that the logic added in the task library i.e. task object, question object or answer option object are working.
 * a. Task (Decision Tree) name is unique. [givenDecisionTreeHasDuplicateName_ItIsntSaved]
 * b. As an admin user, user can setup decision tree task with questions and answers, where answers may lead to new question and can also state max allowed days. [givenDecisionTree_ItCanBeSaved]
*/
@IsTest
public class SQX_Test_1759_TaskLibrary {

    public static boolean   runAllTests = true,
                            run_givenDecisionTreeHasDuplicateName_ItIsntSaved = false,
                            run_givenDecisionTree_ItCanBeSaved = false;
    
    /**
     * This test ensures that a decision tree task's name is unique.
     * 
     * i.e. If a task has a unique name it is saved
     *      and If a task with existing name is given it isn't saved.
     * 
     */
    public static testmethod void givenDecisionTreeHasDuplicateName_ItIsntSaved(){
        if(!runAllTests && !run_givenDecisionTreeHasDuplicateName_ItIsntSaved){
            return;
        }
        
        //Arrange: Create users to save the task in the system
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            //Act: Create a task 'task1' that of complaint decision tree type and save it
            Boolean hadErrorWhileSaving = false;
            SQX_Test_Task task1 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE);
            Exception error;
            try{
                task1.save();    
            }
            catch(Exception ex){
                hadErrorWhileSaving = true;
                error = ex;
            }
            
            //Assert: Ensure that no error was thrown when a unique task is being saved
            System.assertEquals(false, hadErrorWhileSaving, 'Expected unique task to be saved but failed. Error: ' + error);
            
            
            //Act: Create a duplicate task with the same name;
            SQX_Test_Task task2 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE);
            task2.task.Name = task1.task.Name;
            error = null;
            
            try{
                task2.save();    
            }
            catch(Exception ex){
                hadErrorWhileSaving = true;
                error = ex;
            }
            
            //Assert: Ensure that an error occurred while saving the duplicate task. Hence save failed
            System.assertEquals(true, hadErrorWhileSaving,  'Expected error to occur while trying to save a duplicate task. Error: ' + error);
            
        }

    }
    
    
    /**
     * This test ensures that a user can setup decision tree task with questions and answers,
     * where answers may lead to new question and can also state max allowed days
     * [Warning this is more or less a permission check for admin user]
     */ 
    public static testmethod void givenDecisionTree_ItCanBeSaved(){
        if(!runAllTests && !run_givenDecisionTree_ItCanBeSaved){
            return;
        }
        
        //Arrange: create admin user who will be inserting the items into the database.
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            //Act: Create a task in task library
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            
            //Assert: The task should have saved, note a test failure will occur if save failed. So assertion isn't necessary.
            System.assertEquals(1, [SELECT Id FROM SQX_Task__c WHERE Id =: task.task.Id].size(), 'Expected task to be saved, but couldn\'t find one');
            
            //Act: Add a two question in task library
            SQX_Task_Question__c question1 = task.addQuestion(),
                                 question2 = task.addQuestion();
            
            //Assert: The questions should have saved.
            System.assertEquals(2, [SELECT Id FROM SQX_Task_Question__c WHERE SQX_Task__c = : task.task.Id].size(), 'Expected questions to be added for the decision tree task');
            
            
            //Act: Add two options to the question1 one is simple option, other leads to question2
            SQX_Answer_Option__c    option1 = task.addAnswer(question1, null, false),
                                    option2 = task.addAnswer(question1, question2, true);
            
            
            //Assert: Two options must have been saved
            System.assertEquals(2, [SELECT Id FROM SQX_Answer_Option__c WHERE Question__c = : question1.Id OR Question__c = : question2.Id].size(), 'Expected options to be added for decision tree task');
        }
    }
    
    /**
     * GIVEN : A step library and question
     * WHEN : Picklist Values Answer Type is selected
     * THEN : Picklist Category is required
     */ 
    public static testmethod void givenStepLibrary_WhenPicklistValuesAnswerTypeIsSelected_PicklistCategoryIsRequired(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            //ARRANGE: Create a task in step library
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            
            //Act: Add a question of type picklist values without picklist category
            SQX_Task_Question__c question1 = task.addQuestion();
            question1.Answer_Type__c = SQX_Task.ANSWER_TYPE_PICKLIST_VALUES;
            
            Database.SaveResult result = Database.update(question1, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Picklist Category is required for Picklist Values Answer Type'));

            // ACT : Add picklist category
            question1.Picklist_Category__c = 'TEST_PICKLIST_VALUE';
            result = Database.update(question1, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is successful');
            
        }
    }
    
    /**
     * GIVEN : A step library and question
     * WHEN : Custom Component Answer Type is selected
     * THEN : Custom Component Name is required
     */ 
    public static testmethod void givenStepLibrary_WhenCustomComponentAnswerTypeIsSelected_CustomComponentNameIsRequired(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            //ARRANGE: Create a task in step library
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            
            //Act: Add a question of type picklist values without picklist category
            SQX_Task_Question__c question1 = task.addQuestion();
            question1.Answer_Type__c = SQX_Task.ANSWER_TYPE_CUSTOM_COMPONENT;
            
            Database.SaveResult result = Database.update(question1, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Custom Component Name is required for Custom Component Answer Type'));

            // ACT : Add picklist category
            question1.Custom_Component_Name__c = 'TEST_COMPONENT';
            result = Database.update(question1, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is successful');
            
        }
    }
    
    /**
     * GIVEN : A step library and question
     * WHEN : Options Answer Type is selected
     * THEN : Next Question should be blank
     */ 
    public static testmethod void givenStepLibrary_WhenOptionsAnswerTypeIsSelected_NextQuestionShouldBeBlank(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            //ARRANGE: Create a task in step library
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            
            //Act: Add a question of type options - radio button and next question selected
            SQX_Task_Question__c question1 = task.addQuestion();
            SQX_Task_Question__c question2 = task.addQuestion();
            question1.SQX_Next_Question__c = question2.Id;
            question1.Answer_Type__c = SQX_Task.ANSWER_TYPE_OPTIONS_RADIO_BUTTON;
            
            Database.SaveResult result = Database.update(question1, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Next Question cannot be selected for Options Answer Type'), result.getErrors());

            
            //Act: Add a question of type options - picklist and next question selected
            question1.Answer_Type__c = SQX_Task.ANSWER_TYPE_OPTIONS_PICKLIST;
            
            result = Database.update(question1, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Next Question cannot be selected for Options Answer Type'));

            // ACT : Remove next question
            question1.SQX_Next_Question__c = null;
            result = Database.update(question1, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
            
        }
    }
    
}