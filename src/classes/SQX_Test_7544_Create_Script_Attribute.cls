/**
 * Test class for creating script attribute 
 */
@IsTest
public class SQX_Test_7544_Create_Script_Attribute {
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
    }
    
    /**
     * GIVEN : Script Execution with Answer Option Attribute
     * WHEN : Create Script Attribute is invoked
     * THEN  :  Script Attributes is created as per number of Answer Option Attribute
    */
    static testmethod void givenScriptExecutions_WhenCreateScriptAttributeInvoked_ThenScriptAttributeAreCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Create a CQ Task
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            
            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();
            
            // ARRANGE : Create Answer Option
            SQX_Answer_Option__c answerOption = task.addAnswer(question, null, false);
            
            // ARRANGE : Create Answer Option Attributes
            SQX_Answer_Option_Attribute__c answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value');
            insert answerOptionAttribute;
            
            answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                       Attribute_Type__c = 'Sample Attribute Type',
                                                                       Value__c = 'Test Value 1');
            insert answerOptionAttribute;
            
            answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                       Attribute_Type__c = 'Sample Attribute Type',
                                                                       Value__c = 'Test Value 1');
            insert answerOptionAttribute;
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ARRANGE : Create Script Execution
            SQX_Script_Execution__c scriptExecution = new SQX_Script_Execution__c(SQX_Complaint__c = complaint.complaint.Id);
            insert scriptExecution;
            
            // ARRANGE : Create Script Response
            SQX_Script_Response__c scriptResponse = new SQX_Script_Response__c(SQX_Script_Execution__c = scriptExecution.Id, 
                                                                               SQX_Answer_Option__c = answerOption.Id,
                                                                               Response__c = 'Test Response');
            insert scriptResponse;
            
            // ACT : Invoke Create Script Attribute method
            SQX_Create_Script_Attribute.createScriptAttributeRecordBasedOnAnswerOptionAttribute(new List<SQX_Script_Execution__c>{scriptExecution});
            
            List<SQX_Script_Attribute__c> scriptAttributes = [SELECT Id FROM SQX_Script_Attribute__c]; 
            
            // ASSERT : Script Attributes are created as per number of unique Answer Option Attribute
            System.assertEquals(2, scriptAttributes.size());
            
            
            // ACT (SQX-8211): Update SQX_Script_Attribute__c, SQX_Script_Attribute__c
            update new List<SObject>{ scriptAttributes[0], scriptResponse };
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Script_Attribute__c, SQX_Script_Attribute__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Script_Attribute__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Script_Response__c.SObjectType);
        }
    }

    /**
     * GIVEN : Script Execution with no Answer Option
     * WHEN : Create Script Attribute is invoked
     * THEN : Script Attributes should not be created
     */
    static testmethod void givenScriptExecutionsWithoutAnswerOption_WhenCreateScriptAttributeInvoked_ThenNoScriptAttributeIsCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ARRANGE : Create Script Execution
            SQX_Script_Execution__c scriptExecution = new SQX_Script_Execution__c(SQX_Complaint__c = complaint.complaint.Id);
            insert scriptExecution;
            
            // ARRANGE : Create Script Response
            SQX_Script_Response__c scriptResponse = new SQX_Script_Response__c(SQX_Script_Execution__c = scriptExecution.Id,
                                                                               Response__c = 'Test Response');
            insert scriptResponse;
            
            // ACT : Invoke Create Script Attribute method
            SQX_Create_Script_Attribute.createScriptAttributeRecordBasedOnAnswerOptionAttribute(new List<SQX_Script_Execution__c>{scriptExecution});
            
            List<SQX_Script_Attribute__c> scriptAttributes = [SELECT Id FROM SQX_Script_Attribute__c]; 
            
            // ASSERT : Script Attributes is not created
            System.assertEquals(0, scriptAttributes.size());
        }
    }
}