/*
 * Unit test for testing validation rules of Supplier Interaction Step
 */

@isTest
public class SQX_Test_Supplier_Interaction_Step {
    
    public SQX_Supplier_Interaction__c si;
    
    @testSetup
    public static void commonSetup(){
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser, adminUser});
        
        System.runAs(adminUser) {
            // create auto number record for supplier interaction
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Interaction',
                Next_Number__c = 1,
                Number_Format__c = 'ST-{1}',
                Numeric_Format__c = '0000'
            );
        }
    }
    
    /**
     * Given: Supplier Interaction record
     * When: New step is added with previous step number which is in complete status
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenNewStepAdded_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        String description = '';
        
        System.runAs(adminUser){
            //Arrange: Create two policy tasks
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', adminUser, 1);
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Perform Audit', adminUser, 2);
            
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Initiate the record
            supInt.initiate();
            
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, Result__c, Status__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN];
            Task tsk = [SELECT Id FROM Task WHERE WhatId =: supInt.si.Id AND Child_What_Id__c =: siSteps.Id];
            
            //Act: Complete a task
            SQX_BulkifiedBase.clearAllProcessedEntities();
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(tsk.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            
            //Act: Get workflow status of Supplier Interaction
            SQX_Supplier_Interaction__c suppInteraction = [SELECT Id, Workflow_Status__c, Current_Step__c FROM SQX_Supplier_Interaction__c WHERE Id =: supInt.si.id];
            System.assert(suppInteraction.Workflow_Status__c != 'Complete', 'Workflow status should not be completed.');
            System.assert(suppInteraction.Current_Step__c == 2, 'Expected current step is 2');
            
            //Act: Add step with step number 1 again
            SQX_Supplier_Interaction_Step__c step = new SQX_Supplier_Interaction_Step__c(Step__c = 1, SQX_User__c = standardUser.Id, SQX_Parent__c = suppInteraction.Id);
            List<Database.SaveResult> result = Database.insert(new List<SQX_Supplier_Interaction_Step__c> {step}, false);
            
            //Assert: Validation error
            String ExpErrMsg = 'Steps cannot be added for step levels that are completed. Add steps to active step level or for a non completed step level';
            System.assertEquals(false,result[0].isSuccess(), 'Step should not be saved with step number 1 again.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), ExpErrMsg), 'Expected : ' + ExpErrMsg + 'Actual : ' + result[0].getErrors()[0].getMessage());

            // retrive the open intraction step 
            step = [SELECT Id,step__c,Status__c,Is_Parent_Locked__c,Is_Locked__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c = :suppInteraction.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN];
            //complete and no go the sf task 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Task sfTask = [SELECT Id, Status  FROM Task WHERE WhatId =: suppInteraction.Id AND Child_What_Id__c = :step.Id];          
            description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_PERFORM_AUDIT);

            //retrive  the NoGo intraction record
            step = [SELECT Id,Step__c,Status__c,Result__c FROM SQX_Supplier_Interaction_Step__c WHERE Id = :step.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_COMPLETE and Result__c =:SQX_Steps_Trigger_Handler.RESULT_NO_GO ];
            String obRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, 'Task');
            //Act : add complete supplier intraction id while creating new intraction step 
            List<SQX_Supplier_Interaction_Step__c> intractionPolicy=    new SQX_Supplier_Interaction_Step__c[]{
                                                                        new SQX_Supplier_Interaction_Step__c (
                                                                                                            SQX_Parent__c=suppInteraction.Id,
                                                                                                            Step__c=1,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            SQX_Parent_Step__c=step.id,
                                                                                                            RecordTypeId =obRecordTypeId
                                                                                                        ),
                                                                        new SQX_Supplier_Interaction_Step__c (
                                                                                                            SQX_Parent__c=suppInteraction.Id,
                                                                                                            Step__c=2,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            SQX_Parent_Step__c=step.id,
                                                                                                            RecordTypeId =obRecordTypeId
                                                                                                        )};

            List<DataBase.SaveResult> redoResult = new SQX_DB().continueOnError().op_insert(intractionPolicy, new List<Schema.SObjectField>{});
            //Assert: Verify the Validation Rule Error message
            System.assert(!redoResult[0].isSuccess(),ExpErrMsg);
            System.assertEquals(ExpErrMsg,redoResult[0].getErrors()[0].getMessage(),'while redo escalation step which is complete step user should get error message');
            System.assertEquals(true, redoResult[1].isSuccess(),'Record save Successfully>>' + redoResult[1].getErrors());
        }
    }
    
    /**
     * Given: Supplier Interaction record with policy task
     * When: Result is not set in task
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenResultNotSet_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set Result null
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id,Result__c, Status__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];           
            siSteps.Status__c = SQX_Supplier_Common_Values.STATUS_COMPLETE;
            siSteps.Result__c = null;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Error message for not setting Result
            String ExpErrMsg = 'Please set result in order to complete step';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Result must be set on complete interaction step');
        }
    }
    
    /**
     * Given: Supplier Interaction record with policy task
     * When: Expiration date is greater than Issue date
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenExpirationDateGreaterThanIssueDate_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set Issue date and Expiration date
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id,Issue_Date__c, Expiration_Date__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];           
            siSteps.Issue_Date__c = Date.today();
            siSteps.Expiration_Date__c = Date.today()-5;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Issue date cannot be less than Expiration date
            String ExpErrMsg = 'Issue Date cannot be greater than Expiration Date.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Issue date must be greater than expiration date');
        }
    }
    
    /**
     * Given: Supplier Interaction record with policy task
     * When: Assignee is blank
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenAssigneeIsBlank_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            //Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set blank Assignee  
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id,Status__c, SQX_User__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            siSteps.SQX_User__c = null;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Assignee field should not be empty
            String ExpErrMsg = 'Assignee cannot be blank.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Assignee must be set in interaction step.');
        }
    }
    
    /**
     * Given: Voided Supplier Interaction record 
     * When: User tries to edit interaction step
     * Then: Throw validation error
     */
    public static testMethod void givenVoidedSI_WhenUserEditsInteractionStep_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Void the record
            supInt.void();
            
            //Act: Try to edit step record
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, Is_Locked__c, Is_Parent_Locked__c, Allowed_Days__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.Allowed_Days__c = 5;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Step cannot be edited after it is locked
            String ExpErrMsg = 'Record status or Step status does not support the action performed.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Record cannot be edited once it is locked.');
        }
    }
    
    /**
     * Given: Supplier interaction with policy task 
     * When: Obsolete controlled document is referred to task
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenObsoleteDocumentReferred_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){    
            //Arrange: Create controlled document
            SQX_Test_Controlled_Document testControlledDocument = new SQX_Test_Controlled_Document();
            testControlledDocument.doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
            testControlledDocument.save();
        
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set obsolete document in step
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, Status__c, SQX_Controlled_Document__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            siSteps.SQX_Controlled_Document__c = testControlledDocument.doc.Id;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Obsolete controlled document should not be set in step
            String ExpErrMsg = 'Only Pre-Release and Current Controlled Documents can be referred.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Obsolete document cannot be referred.');
        }
    }
    
    /**
     * Given: Supplier interaction with policy task 
     * When: Redo incomplete step
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenRedoIncompleteStep_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Try to redo incomplete step
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, Redo_Policy__c, Status__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.Redo_Policy__c = true;
            siSteps.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Incomplete step should not be able to redo
            String ExpErrMsg = 'Cannot redo step which has not been completed.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Incomplete step cannot be redone.');
        }
    }
    
    /**
     * Given: Supplier interaction with policy task 
     * When: Voided Audit is used in step
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenVoidedAuditUsedInAudit_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){    
            //Arrange: Create Audit record
            SQX_Test_Audit testAudit = new SQX_Test_Audit(adminUser).save();
            testAudit.audit.Status__c = SQX_Audit.STATUS_VOID;
            testAudit.save();
        
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set voided Audit in step
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, SQX_Audit_Number__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.SQX_Audit_Number__c = testAudit.audit.Id;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Voided Audit should not be set in step
            String ExpErrMsg = 'Audit with void status cannot be used.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Voided Audit cannot be referred in step.');
        }
    }
    
    /**
     * Given: Supplier interaction with policy task 
     * When: Step number is less than zero
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenStepNumberLessThanZero_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){
            // Act: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Act: Set step number less than zero
            SQX_Supplier_Interaction_Step__c siSteps = [SELECT Id, Step__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id];
            siSteps.Step__c = -1;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Step number should not be less than zero
            String ExpErrMsg = 'Step should be greater than zero';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage(), 'Step number should be greater than zero');
        }
    }

    /**
     * Given: Supplier Interaction  step
     *         a)Doc Request Step with Result Go and  expiration date empty
     *         b)step with need periodic update and notify before expiration is set  
     * When: a)user try to complete sf task  with out expiration date
     *        b)user try redo the supplier Interaction step
     * Then: a)User get error message "CQ Document Expiration Date is Required"
     *       b) Redo record gets copy need periodic update and need before expiration
     * @story : SQX-6833
     */
    @isTest
    public static void givenInteractionStepWithDocRequestRecordType_WhenUserTryToCompleteSfTaskWithoutExpirationDate_ThenUserGetErrorMessage(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {

            // Arrange :Create a SI Record 
             SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();

            
            String obRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, 'Task');
            //add supplier escalation steps 
            List<SQX_Supplier_Interaction_Step__c> interactionPolicies= new SQX_Supplier_Interaction_Step__c[]{
                                                                    new SQX_Supplier_Interaction_Step__c (   SQX_Parent__c=supInt.si.Id,
                                                                                                            Step__c=1,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            Due_Date__c=Date.today(),
                                                                                                            Allowed_Days__c=10,
                                                                                                            Needs_Periodic_Update__c=true,
                                                                                                            Notify_Before_Expiration__c=2,
                                                                                                            RecordTypeId = obRecordTypeId
                                                                                                )};

            //Try To insert escalation Step with needs periodic update and notify before expiration date
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(interactionPolicies, new List<Schema.SObjectField>{});
            System.assertEquals(true, result[0].isSuccess(),'Record save failed >>' + result[0].getErrors());

            // Submit the SI record
            supInt.setStage(SQX_Supplier_Common_Values.STAGE_TRIAGE);
            supInt.save();
            // SI is initiated
            supInt.initiate();
            //Arrange: retrive escalation step and sf task
            SQX_Supplier_Interaction_Step__c interactionPolicy=[select id ,Allowed_Days__c,Due_Date__c from SQX_Supplier_Interaction_Step__c where SQX_Parent__c=:supInt.si.Id];

            interactionPolicy.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            interactionPolicy.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            interactionPolicy.Comment__c = 'Record is completed and result is nogo description.';

             //Try to complete sf task  with out adding expiration date 
            List<Database.SaveResult> results = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction_Step__c>{interactionPolicy}, new List<Schema.SObjectField>{});
            //Assert: verify validation error message 
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(results[0].getErrors(), SQX_Supplier_Common_Values.EXPIRATION_DATE_VALIDATION_ERROR_MSG),
                          'Task update should fail with error: ' + SQX_Supplier_Common_Values.EXPIRATION_DATE_VALIDATION_ERROR_MSG + '. Actual Errors: ' + results[0].getErrors());
            
            SQX_BulkifiedBase.clearAllProcessedEntities();

            interactionPolicy.Result__c = SQX_Steps_Trigger_Handler.RESULT_NO_GO;
            interactionPolicy.Expiration_Date__c=Date.today();
            //Should save the sf task 
            List<Database.SaveResult> successResults = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction_Step__c>{interactionPolicy}, new List<Schema.SObjectField>{});
            
            //Assert: record save successfully
            System.assertEquals(true, successResults[0].isSuccess(),'Record save failed >>' + successResults[0].getErrors());

            
             //Act: Redo the record
            SQX_Supplier_Interaction_Step__c interactionStepRecord =  [SELECT Id,Needs_Periodic_Update__c,Notify_Before_Expiration__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id
                                                            AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                                            AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
          
            SQX_Test_Supplier_Interaction os = new SQX_Test_Supplier_Interaction();
            os.redoStep(interactionStepRecord);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            //retrive the redo record 
            SQX_Supplier_Interaction_Step__c redoRecord = [SELECT Id,Needs_Periodic_Update__c,Notify_Before_Expiration__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: supInt.si.Id AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN];
            //Assert: //Assert: Verify that Needs Periodic Update and Notify Before Expiration is transfer 
            System.assertEquals(interactionStepRecord.Needs_Periodic_Update__c,redoRecord.Needs_Periodic_Update__c,'Needs Periodic Update should be transfer to new onboardding');
            System.assertEquals(interactionStepRecord.Notify_Before_Expiration__c,redoRecord.Notify_Before_Expiration__c,'Notify Before Expiration should be transfer to new onboardding');
       }
    }

    /**
     * Given: supplier interaction in open status,
     * When: Open onboarding step fields apart from status, result, applicable, comment are edited, 
     * Then: Validation error is thrown.	
     */
    @isTest
    public static void giveninteraction_WhenUserEditRecord_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        
        System.runAs(standardUser){
            
             // Act: Create supplier interaction document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            supInt.initiate();

            SQX_Supplier_Interaction_Step__c onboardingStep = [Select id, name, status__c, result__c , comment__c
                                                     from SQX_Supplier_Interaction_Step__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_OPEN and
                                                     SQX_Parent__c = :supInt.si.id limit 1];
            //Act: Any allowed field is edited
            onboardingStep.Comment__c = 'Just a random comment.';
            List<Database.SaveResult> resultAgain =  new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction_Step__c> {onboardingStep}, 
                                                                                              new List<Schema.SObjectField>{});
            //Assert: Make sure that fields are updated
            System.assertEquals(true, resultAgain[0].isSuccess());
            
            //Act: try updating field that is not allowed to be modified
            onboardingStep.allowed_days__c = 5;
            List<Database.SaveResult> result =  new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction_Step__c> {onboardingStep}, new List<Schema.SObjectField>{});
            
            //Assert: Validation error message is thrown
            System.assertEquals('Supplier Interaction step in open status cannot be edited.',result[0].getErrors()[0].getMessage());
            
            
        } 
    }

    
    
        /**
    * GIVEN : given SI Task
    * WHEN : when save the record with issue date greater than current date
    * THEN : throw validation error message
    */  
    public static testMethod void givenTask_WhenSaveRecordWithIssueDateGreaterThanCurrentDate_ThenThrowValidationErrorMessage(){
           Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        
        System.runAs(standardUser){
            
             // Act: Create supplier interaction task record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            //Act: Submit the record
            supInt.submit();
            supInt.initiate();

          
            //Arrange: retrive workflow policy
            SQX_Supplier_Interaction_Step__c wp1 = [SELECT Id, Expiration_Date__c, Issue_Date__c FROM SQX_Supplier_Interaction_Step__c LIMIT 1];
            wp1.Issue_Date__c = Date.today()+2;
            
            //Act: save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction_Step__c> { wp1 },  new List<Schema.SObjectField>{ SQX_Supplier_Interaction_Step__c.Expiration_Date__c });
            
            //Assert: Ensured that throws validation error message
            System.assert(result[0].isSuccess() == false, 'Expiration date grater than issue date.');
            System.assertEquals('Issue Date cannot be greater than Current Date.', result[0].getErrors().get(0).getMessage());
        }
    }
    
    
}