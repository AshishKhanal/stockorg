/*
* author: Dibya Shrestha
* description: Base extension class used for Personnel Document Training Signing Off process for User and Trainer
*/
public with sharing abstract class SQX_Extension_PDT_SignOff_Base extends SQX_Training_Signoff {
    
    ApexPages.StandardSetController standardSetController;

    /**
    * returns true when document compare package page is available in the org
    */
    public Boolean getIsDocComparePackagePageAvailable() {
        return SQX_Controller_Document_Compare.isDocComparePackagePageAvailable;
    }

    /**
    * SQX_Extension_PDT_SignOff_Base constructor
    * @param controller StandardSetController of an apex page
    * @param urlKey URL query string key holding sign off item Id. If this key is not found in URL then selected items from controller is used.
    * @param isTrainerSignOff It determines for whom a personnel document training is being signed off. false value for User sign off and true value for Trainer sign off.
    */
    public SQX_Extension_PDT_SignOff_Base(ApexPages.StandardSetController controller, String urlKey, Boolean isTrainerSignOff) {
        // initialize required values
        this.standardSetController = controller;
        this.isTrainerSignOff = isTrainerSignOff;
        this.documentTrainingList = new List<SQX_Personnel_Document_Training__c>();

        HasAllScormContents = false;    // setting default to false

        // read/set electronic signature custom settings
        setElectronicSignatureSettings();
        
        Set<Id> documentTrainingIds = new Set<Id>();
        Boolean urlKeyFound = ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().containsKey(urlKey);
        
        if (urlKeyFound) {
            String[] dtnIdList = ApexPages.currentPage().getParameters().get(urlKey).split(',', 0);
            
            // only set valid Id from url
            for (String strId : dtnIdList) {
                try {
                    if (String.isNotBlank(strId)) {
                        Id dtnId = strId;
                        documentTrainingIds.add(dtnId);
                    }
                } catch (Exception e) { }
            }
        } else {
            // set documentTrainingIds from controller if list is null
            documentTrainingIds = new Map<Id, SQX_Personnel_Document_Training__c>((List<SQX_Personnel_Document_Training__c>)controller.getSelected()).keySet();
        }
        
        if (documentTrainingIds.size() > 0) {
            initializeDocumentTrainings(documentTrainingIds).addMessages();
        }
    }
    
    public virtual PageReference signOff()
    {
        return signOff(false);
    }

    public PageReference signOff(Boolean allowScormSignOff){
        PageReference retURL = null;
        SQX_Action_Response response = signOffUsingApi(allowScormSignOff);
        if(response.errorMessages.isEmpty()){
            retURL = SQX_Utilities.getValidReturnUrl(null, null);
        } else {
            response.addMessages();
        }

        return retURL;
    }

}