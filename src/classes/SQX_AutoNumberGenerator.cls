/**
* class used for generating auto numbers using Auto Number (SQX_Auto_Number__c) object
*/
global with sharing class SQX_AutoNumberGenerator {
    
    /**
    * generates next sequence numbers based on Auto Number settings
    */
    @InvocableMethod(Label = 'Generates next sequence numbers based on Auto Number settings')
    global static List<SQX_AutoNumberingSetting> getNewSequenceNumber(List<SQX_AutoNumberingSetting> settings) {
        Set<String> autoNumberNames = new Set<String>();
        Map<String, SQX_Auto_Number__c> autoNumbers = new Map<String, SQX_Auto_Number__c>();
        
        for (SQX_AutoNumberingSetting setting : settings) {
            if (!String.isBlank(setting.AutoNumberNameToUse)) {
                autoNumberNames.add(setting.AutoNumberNameToUse);
            }
        }
        
        if (autoNumberNames.size() > 0) {
            for (SQX_Auto_Number__c autoNum : [ SELECT Name, Next_Number__c, Number_Format__c, Numeric_Format__c
                                                FROM SQX_Auto_Number__c
                                                WHERE Name IN :autoNumberNames FOR UPDATE ]) {
                autoNumbers.put(autoNum.Name.toUppercase(), autoNum);
            }
        }
        
        for (SQX_AutoNumberingSetting setting : settings) {
            if (!String.isBlank(setting.AutoNumberNameToUse)) {
                SQX_Auto_Number__c autoNumberToUse = autoNumbers.get(setting.AutoNumberNameToUse.toUppercase());
                
                if (autoNumberToUse != null) {
                    Integer nextNumber = (Integer)autoNumberToUse.Next_Number__c;
                    setting.AutoNumber = nextNumber;
                    // increment next auto number by 1 for another record
                    autoNumberToUse.Next_Number__c = nextNumber + 1;
                    
                    String numericFormat = '',
                        formattedNextNumber = String.valueOf(nextNumber);
                    
                    if (setting.NumericFormatToUse != null) {
                        numericFormat = setting.NumericFormatToUse;
                    }
                    else if (!String.isBlank(autoNumberToUse.Numeric_Format__c)) {
                        numericFormat = autoNumberToUse.Numeric_Format__c;
                    }
                    
                    // format next number
                    Integer numFormatLength = numericFormat.length();
                    if (numFormatLength > 0 && formattedNextNumber.length() < numFormatLength) {
                        // apply numeric format when next number character length is lower than numeric format length
                        formattedNextNumber = numericFormat + formattedNextNumber;
                        formattedNextNumber = formattedNextNumber.right(numFormatLength);
                    }
                    
                    String prefixToUse = setting.PrefixToUse != null ? setting.PrefixToUse : '',
                        suffixToUse = setting.SuffixToUse != null ? setting.SuffixToUse : '',
                        numberFormatToUse = '';
                    
                    if (setting.NumberFormatToUse != null) {
                        numberFormatToUse = setting.NumberFormatToUse;
                    }
                    else if (!String.isBlank(autoNumberToUse.Number_Format__c)) {
                        numberFormatToUse = autoNumberToUse.Number_Format__c;
                    }
                    
                    setting.FormattedAutoNumber = String.format(numberFormatToUse, new list<String>{ prefixToUse, formattedNextNumber, suffixToUse });
                }
            }
        }
        
        if (autoNumbers.size() > 0) {
            // update next auto numbers
            // without sharing is used since auto number setting is created by one user and used in other objects by other users
            new SQX_DB().withoutSharing().op_update(autoNumbers.values(), new List<SObjectField>{ SQX_Auto_Number__c.Next_Number__c });
        }
        
        return settings;
    }
    
}