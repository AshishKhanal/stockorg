/**
 * This test class handle necessary unit tests for Part Link record.
 */
@isTest
public class SQX_Test_Part_Link {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * Given : Two part record
     * When : Part link is created by linking two part one as Parent Part and another as Related Part
     * Then : Part Link is successfully created.
     */
    public static testMethod void givenTwoPartRecord_WhenPartLinkIsCreatedOnePartAsParentAndAnotherAsRelated_PartLinkIsCreatedSuccessfully() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            // Arrange :  Create two parts record
            SQX_Part__c part1 = SQX_Test_Part.insertPart(null, adminUser, true, 'Expected part to be created but is not created.');
            SQX_Part__c part2 = SQX_Test_Part.insertPart(null, adminUser, true, 'Expected part to be created but is not created.');
            
            // Act : create part link record
            SQX_Part_Link__c partLink = new SQX_Part_Link__c(
                SQX_Parent_Part__c = part1.Id,
                SQX_Related_Part__c = part2.Id,
                Active__c = true,
                Relationship_Type__c = 'Kit'
            );
            
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Part_Link__c> {partLink}, new List<Schema.SObjectField> {
                    Schema.SQX_Part_Link__c.SQX_Parent_Part__c,
                    Schema.SQX_Part_Link__c.SQX_Related_Part__c,
                    Schema.SQX_Part_Link__c.Active__c,
                    Schema.SQX_Part_Link__c.Relationship_Type__c});
            
            // Assert : Enusre record is created successfully
            System.assert(result[0].isSuccess(), 'Expected part link record to be inserted successfully');
            System.assert(result[0].Id != null, 'Expected part link record to be existed but got ' + result[0].Id);
            
            
            // ACT (SQX-8211): update SQX_Part_Link__c
            update partLink;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Part_Link__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Part_Link__c.SObjectType);
        }
    }
    
    /**
     * Given : Two one part record.
     * When : Part link is created by linking same part as Parent Part and Related Part
     * Then : Validation Error is thrown as same part cannot be added as both Parent and Related
     */
    public static testMethod void givenOnePartRecord_WhenPartLinkIsCreatedUsingSamePartForParentAndRelated_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            // Arrange :  Create two parts record
            SQX_Part__c part1 = SQX_Test_Part.insertPart(null, adminUser, true, 'Expected part to be created but is not created.');
            
            // Act : create part link record
            SQX_Part_Link__c partLink = new SQX_Part_Link__c(
                SQX_Parent_Part__c = part1.Id,
                SQX_Related_Part__c = part1.Id,
                Active__c = true,
                Relationship_Type__c = 'Kit'
            );
            
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Part_Link__c> {partLink}, new List<Schema.SObjectField> {
                    Schema.SQX_Part_Link__c.SQX_Parent_Part__c,
                    Schema.SQX_Part_Link__c.SQX_Related_Part__c,
                    Schema.SQX_Part_Link__c.Active__c,
                    Schema.SQX_Part_Link__c.Relationship_Type__c});
            
            // Assert : Enusre validation error is thrown
            final String msg = 'Cannot use same part as both parent as well as related part.';
            System.assert(result[0].isSuccess() == false, 'Expected part link record not to be inserted');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), msg), 'Expected : ' + msg + ' but got ' + result[0].getErrors());
        }
    }
}