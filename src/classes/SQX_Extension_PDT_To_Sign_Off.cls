/**
* Extension class for SQX_Personnel_Document_Training__c object that handles document training user/trainer sign off list for current user
*/
public with sharing class SQX_Extension_PDT_To_Sign_Off {
    
    List<SQX_Personnel_Document_Training__c> documentTrainingsToSignOff;
    
    public String trainerSignOffUrlKey { get; set; }
    public String userSignOffUrlKey { get; set; }
    
    public SQX_Extension_PDT_To_Sign_Off(ApexPages.StandardController controller) {
        trainerSignOffUrlKey = SQX_Extension_PDT_Trainer_SignOff.URL_KEY_FOR_ID;
        userSignOffUrlKey = SQX_Extension_PDT_User_SignOff.URL_KEY_FOR_ID;
        
        Id userId = userInfo.getUserId(); // current user
        documentTrainingsToSignOff = [SELECT Id, Name, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, SQX_Controlled_Document__r.Is_Scorm_Content__c, Document_Number_Rev__c, SQX_Personnel__c, Personnel_Name__c,
                                             SQX_Trainer__c, SQX_Trainer__r.Name, Level_Of_Competency__c, Optional__c, Due_Date__c, Status__c, Trainer_Approval_Needed__c,
                                             SQX_User_Signed_Off_By__c, User_SignOff_Date__c, SQX_Training_Approved_By__c, Trainer_SignOff_Date__c, Is_Refresher__c,  SQX_Training_Session__c,
                                             Can_View_And_Sign_Off__c, Can_Take_Assessment__c, SQX_Assessment__c, SQX_Personnel_Assessment__c, Is_Training_Session_Complete__c, Title__c
                                      FROM SQX_Personnel_Document_Training__c
                                      WHERE (
                                                (
                                                    SQX_Personnel__r.SQX_User__c = :userId
                                                    AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING
                                                    AND SQX_Controlled_Document__r.Document_Status__c != :SQX_Controlled_Document.STATUS_OBSOLETE
                                                )
                                                OR
                                                (
                                                    SQX_Trainer__c = :userId
                                                    AND Status__c = :SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING
                                                )
                                            )];
    }
    
    /**
    * Determines if an item in the sign off list requires user sign off
    */
    public Boolean getRequireUserSignOff() {
        for (SQX_Personnel_Document_Training__c dt : documentTrainingsToSignOff) {
            if (dt.Status__c == SQX_Personnel_Document_Training.STATUS_PENDING && !dt.Can_Take_Assessment__c && !dt.SQX_Controlled_Document__r.Is_Scorm_Content__c) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * Determines if an item in the sign off list requires trainer sign off
    */
    public Boolean getRequireTrainerSignOff() {
        for (SQX_Personnel_Document_Training__c dt : documentTrainingsToSignOff) {
            if (dt.Status__c == SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * Returns sign off list for current user.
    */
    public List<SQX_Personnel_Document_Training__c> getSignOffList() {
        if (documentTrainingsToSignOff == null) {
            documentTrainingsToSignOff = new List<SQX_Personnel_Document_Training__c>();
        }
        
        return documentTrainingsToSignOff;
    }


}