/**
* This class is for assigning task to investigator of capa during transition from draft to open
* @author Sanjay Maharjan
* @date 1/24/2018
* SQX-4510
*/
public class SQX_CAPA_Assign_Task_To_Investigator {
    
    /**
    * This invocable method is called by process builder that activates when capa goes from draft to open
    */
    @InvocableMethod(label='Assign Task To Investigator' description='Assign Task to Investigator when CAPA goes from Draft Status to Open')
    public static void assignTaskToInvestigator(List<SQX_CAPA__c> capaList) {        
        SQX_CAPA.Bulkified capaProcessor = new SQX_CAPA.Bulkified(capaList, new Map<Id, SQX_CAPA__c>()).assignTaskToInvestigator();
    }
}