
@IsTest
public class SQX_Test_1102_Organization_Hierarchy {

    static boolean runAllTests = true,
                    run_givenAUser_Department_Values_Copied = true;
    /*                
    *this test ensures new department field value is copied to old department field 
    */               
    public testmethod static void givenAUser_Department_Values_Copied(){

        if(!runAllTests && !run_givenAUser_Department_Values_Copied){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){

            departmentResearch = new SQX_Department__c(
                                                     Name='Research1'
                                                    );
            insert departmentResearch;
        }

        System.runas(standardUser){

            SQX_Nonconformance__c nonConformance = new SQX_Nonconformance__c (
                                            Type__c = SQX_NC.TYPE_INTERNAL,
                                            Type_Of_Issue__c = SQX_NC.ISSUE_TYPE_PRODUCT,
                                            Disposition_Required__c = false,
                                            Occurrence_Date__c = Date.Today(),
                                            SQX_Department__c = departmentResearch.Id);
            
            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));
                                         
                                         
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
            
            Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance ));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            ncMap.put('Id', 'NC-1');
            ncMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
                
            changeSet.add(ncMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> paramssave = new Map<String, String>();
            paramsSave.put('nextAction', 'Save');
            Id result= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,paramsSave);
            
            System.assert(string.valueOf(result.getsObjectType()) == SQX.NonConformance, 
                    'Expected to process changeset'+ changeSet);

            SQX_Nonconformance__c currentNC= [Select Department__c from SQX_Nonconformance__c WHERE Id= :result];
            System.assert(currentNC.Department__c=='Research1', 'Department Name should be copied to Old department value and should be Research1 but is' + currentNC.Department__c);
        }

    }
    
    
    


}