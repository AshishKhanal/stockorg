/**
 * This class will contain common methods and static Strings required for ContentDocumentLink object.
 */
public with sharing class SQX_ContentDocumentLink {

    public static Boolean disableForMigration = false;

    /** 
     * ContentDocumentLink Trigger Handler.
     * Bulkified class to handle the actions performed by the trigger on bulk data
     * It contains the trigger method to prevent modification of ContentDocument records through ContentDocumentLink trigger.
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        // Constructor
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newVersions equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<ContentDocumentLink> newDocLinks, Map<Id,ContentDocumentLink> oldMap){
        
            super(newDocLinks, oldMap);
        }

        /**
         * This method prevents adding files or delete files to locked records like NSI, SD, Onboarding and Deviation Process
         */
        public Bulkified preventContentUploadAndDeletionOnLockedRecord() {
            if(disableForMigration) {
                return this;
            }
            this.resetView();
            List<ContentDocumentLink> contentDocLinks = (List<ContentDocumentLink>) this.view;
            List<Id> nsiIds = new List<Id>(),
                     sdIds = new List<Id>(),
                     supplierEscalationIds = new List<Id>(), 
                     supplierInteractionIds = new List<Id>(), 
                     onboardingStepIds = new List<Id>(),
                     deviationProcessIds = new List<Id>(),
                     escalationStepIds = new List<Id>(),
                     interactionStepIds = new List<Id>(),
                     complaintIds = new List<Id>(),
                     medwatchIds = new List<Id>();

            Map<Id, Id> auditLinkIds = new Map<Id, Id>();

            // capture all linked entitity ids
            for (ContentDocumentLink link : contentDocLinks) {
                Id sobjectId = link.LinkedEntityId;
                SObjectType objType = sobjectId.getSobjectType(); 
                if (objType == compliancequest__SQX_New_Supplier_Introduction__c.SObjectType) {
                    nsiIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Supplier_Deviation__c.SObjectType) {
                    sdIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Onboarding_Step__c.SObjectType) {
                    onboardingStepIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Deviation_Process__c.SObjectType) {
                    deviationProcessIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Supplier_Escalation__c.SObjectType) {
                    supplierEscalationIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Supplier_Interaction__c.SObjectType) {
                    supplierInteractionIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Supplier_Escalation_Step__c.SObjectType) {
                    escalationStepIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Supplier_Interaction_Step__c.SObjectType) {
                    interactionStepIds.add(sobjectId);
                } else if (objType == SQX_Audit__c.SObjectType) {
                    auditLinkIds.put(sObjectId, link.ContentDocumentId);
                } else if (objType == compliancequest__SQX_Complaint__c.SObjectType) {
                    complaintIds.add(sobjectId);
                } else if (objType == compliancequest__SQX_Medwatch__c.SObjectType) {
                    medwatchIds.add(sobjectId);
                }
            }

            Map<Id, SQX_New_Supplier_Introduction__c> nsiMap;
            Map<Id, SQX_Supplier_Deviation__c> sdMap;
            Map<Id, SQX_Supplier_Escalation__c> supplierEscalationMap;
            Map<Id, SQX_Supplier_Interaction__c> supplierInteractionMap;
            Map<Id, SQX_Onboarding_Step__c> onboardingMap;
            Map<Id, SQX_Deviation_Process__c> deviationProcessMap;
            Map<Id, SQX_Supplier_Escalation_Step__c> escalationStepMap;
            Map<Id, SQX_Supplier_Interaction_Step__c> interactionStepMap;
            Map<Id, SQX_Complaint__c> complaintMap;
            
            Map<Id, SQX_Medwatch__c> medwatchMap;

            // will hold combined linked entity ids in single list
            Set<Id> allLinkedEntityIds = new Set<Id>();

            // will hold all linked locked medwatch record ids
            // the reason behind maintaing two sets and not using 'allLinkedEntityIds' is because of the label being used
            // the label used for other modules here is not valid for Medwatch
            Set<Id> linkedLockedMedwatchRecords = new Set<Id>();

            // get locked entity ids
            if (nsiIds.size() > 0) {
                nsiMap = new Map<Id, SQX_New_Supplier_Introduction__c> ([SELECT Id, Is_Locked__c FROM SQX_New_Supplier_Introduction__c WHERE Id IN :nsiIds AND Is_Locked__c = true]);
                allLinkedEntityIds.addAll(nsiMap.keySet());
            }
            if (sdIds.size() > 0) {
                sdMap = new Map<Id, SQX_Supplier_Deviation__c> ([SELECT Id, Is_Locked__c FROM SQX_Supplier_Deviation__c WHERE Id IN :sdIds AND Is_Locked__c = true]);
                allLinkedEntityIds.addAll(sdMap.keySet());
            }
            if (supplierEscalationIds.size() > 0) {
                supplierEscalationMap = new Map<Id, SQX_Supplier_Escalation__c> ([SELECT Id, Is_Locked__c FROM SQX_Supplier_Escalation__c WHERE Id IN :supplierEscalationIds AND Is_Locked__c = true]);
                allLinkedEntityIds.addAll(supplierEscalationMap.keySet());
            }
            if (supplierInteractionIds.size() > 0) {
                supplierInteractionMap = new Map<Id, SQX_Supplier_Interaction__c> ([SELECT Id, Is_Locked__c FROM SQX_Supplier_Interaction__c WHERE Id IN :supplierInteractionIds AND Is_Locked__c = true]);
                allLinkedEntityIds.addAll(supplierInteractionMap.keySet());
            }
            if (onboardingStepIds.size() > 0) {
                onboardingMap = new Map<Id, SQX_Onboarding_Step__c> ([SELECT Id, Is_Locked__c FROM SQX_Onboarding_Step__c WHERE Id IN :onboardingStepIds AND (Is_Locked__c = true OR Is_Parent_Locked__c = true)]);
                allLinkedEntityIds.addAll(onboardingMap.keySet());
            }
            if (deviationProcessIds.size() > 0) {
                deviationProcessMap = new Map<Id, SQX_Deviation_Process__c> ([SELECT Id, Is_Locked__c FROM SQX_Deviation_Process__c WHERE Id IN :deviationProcessIds AND (Is_Locked__c = true OR Is_Parent_Locked__c = true)]);
                allLinkedEntityIds.addAll(deviationProcessMap.keySet());
            }
            if (escalationStepIds.size() > 0) {
                escalationStepMap = new Map<Id, SQX_Supplier_Escalation_Step__c> ([SELECT Id, Is_Locked__c FROM SQX_Supplier_Escalation_Step__c WHERE Id IN :escalationStepIds AND (Is_Locked__c = true OR Is_Parent_Locked__c = true)]);
                allLinkedEntityIds.addAll(escalationStepMap.keySet());
            }
            if (interactionStepIds.size() > 0) {
                interactionStepMap = new Map<Id, SQX_Supplier_Interaction_Step__c> ([SELECT Id, Is_Locked__c FROM SQX_Supplier_Interaction_Step__c WHERE Id IN :interactionStepIds AND (Is_Locked__c = true OR Is_Parent_Locked__c = true)]);
                allLinkedEntityIds.addAll(interactionStepMap.keySet());
            }
            if(complaintIds.size() >0) {
                complaintMap = new Map<Id, SQX_Complaint__c>([SELECT Id, Is_Locked__c FROM SQX_Complaint__c WHERE Id IN :complaintIds AND Is_Locked__c = true]);
                allLinkedEntityIds.addAll(complaintMap.keySet());
            }
            if (medwatchIds.size() > 0) {
                medwatchMap = new Map<Id, SQX_Medwatch__c> ([SELECT Id FROM SQX_Medwatch__c WHERE Id IN: medwatchIds AND Is_Locked__c = true]);
                linkedLockedMedwatchRecords.addAll(medwatchMap.keySet());
            }

            if(auditLinkIds.size() > 0 && Trigger.isDelete) {
                Map<Id, SQX_Audit__c> auditMap = new Map<Id, SQX_Audit__c>([SELECT Id FROM SQX_Audit__c WHERE Id IN : auditLinkIds.keySet() AND Spreadsheet_Id__c IN : auditLinkIds.values() AND  Spreadsheet_Id__c != null ]);
                allLinkedEntityIds.addAll(auditMap.keySet());
            }

            // if any locked entity found then add error to ContentDocumentLink record
            for (ContentDocumentLink docLink : contentDocLinks) {
                if (allLinkedEntityIds.contains(docLink.LinkedEntityId)) {
                    docLink.addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
                } else if(linkedLockedMedwatchRecords.contains(docLink.LinkedEntityId)) {
                    docLink.addError(Label.CQ_ERR_MSG_CANNOT_PERFORM_ACTION_ON_LOCKED_RECORD);
                }
            }
            return this;
        }
    }
}