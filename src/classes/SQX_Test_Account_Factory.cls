@IsTest
public without sharing class SQX_Test_Account_Factory{
    public static final String USER_TYPE_PARTNER = 'Partner';
    public static final String USER_TYPE_COMMUNITY = 'Community';
    public static final String PROFILE_TYPE_PARTNER = 'SQX_Community_ADMIN';
    public static final String PROFILE_TYPE_COMMUNITY = 'SQX_Community_User';
    public static final String PROFILE_TYPE_CUSTOMER_ADMIN = 'CQ_System_Administrator';
    public static final String PROFILE_TYPE_CUSTOMER_SYS_ADMIN = 'CQ_System_Administrator_SYS';
    public static final String PROFILE_TYPE_CUSTOMER = 'CQ_Standard_User';
    public static final String PROFILE_TYPE_INITIATOR = 'SQR Standard User';
    public static final String PROFILE_TYPE_SQR_ADMIN = 'SQR Admin';
    
    public static final String ACCOUNT_TYPE_CUSTOMER = 'Customer';
    public static final String ACCOUNT_TYPE_SUPPLIER = 'Supplier';
    
    public static final String DATA_MIGRATION_CUSTOM_PERMISSION_NAME = 'SQXDataMigrationCustomPermission';

    private static UserRole runningUserRole = null;


    private static Map<String, User> userCache = new Map<String, User>();


    /**
    * @author Pramesh Bhattarai
    * @date 2014
    * This method creates a new user if no previous user of the given type has been created. Else returns the last user of the type.
    * @return new user if no previous user of the given type has been created. Else returns the last user of the type.
    */
    public static User getUser(String permissionSetName){
        if(userCache.containsKey(permissionSetName))
            return userCache.get(permissionSetName);

        //create a new user
        return createUser(null, permissionSetName, null);
    }

    /**
    * queries and returns all created user list groupped by permission set name and identifier key.
    * Note: AboutMe field of User object stores identifier key in format "<permissionSetName>:<identifierKey>".
    */
    public static Map<String, User> getUsers() {
        List<User> users = [SELECT Id, 
                                   Name, 
                                   Username, 
                                   FirstName, 
                                   LastName, 
                                   Email, 
                                   CommunityNickname, 
                                   ProfileId, 
                                   UserRoleId, 
                                   Alias, 
                                   ContactId, 
                                   ManagerId, 
                                   AboutMe,
                                   IsActive
                                FROM User WHERE AboutMe LIKE '%:%'];
        
        Map<String, User> usersMap = new Map<String, User>();
        for (User usr : users) {
            List<String> aboutMeInfo = usr.AboutMe.split(':');
            usersMap.put(aboutMeInfo[1], usr);
        }
        
        return usersMap;
    }
    
    public static User createUser(Contact contact, String permissionSetName, UserRole role){
        return createUser(contact, permissionSetName, role, '');
    }
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    * @description Creates a user with the given contact and profile
    * @param contact the contact that is to be created
    * @param profileName the name of the profile
    * @param roleID the role which is to be assigned to the user
    * @param identifierKey key to identify user in tests and stored in AboutMe field of User object in format "<permissionSetName>:<identifierKey>".
    * @return returns the newly created user
    */
    public static User createUser(Contact contact, String permissionSetName, UserRole role, String identifierKey) {
        integer randomNumber = (Integer)( Math.random() * 1000000 );
        List<Profile> profiles = new List<Profile>();
        
        String aboutMe = permissionSetName + ':' + (String.isBlank(identifierKey) ? '' + randomNumber : identifierKey); // Note: permissionSetName may change later

        if(permissionSetName==PROFILE_TYPE_PARTNER || permissionSetName==PROFILE_TYPE_COMMUNITY )
        {
            profiles = [SELECT ID FROM Profile WHERE Name = 'Partner Community User'];
        }
        else if(permissionSetName == PROFILE_TYPE_CUSTOMER_SYS_ADMIN){
            profiles = [SELECT ID FROM Profile WHERE Name = : 'System Administrator'];
            permissionSetName = PROFILE_TYPE_CUSTOMER_ADMIN; //we want to use both sys admin profile and permission set
        }
        else{
            profiles = [SELECT ID FROM Profile WHERE Name = 'Standard User'];
        }

        System.assert(profiles.size() > 0,
                     'Expected at least on profile to match but found none');
        
        User user = new User();
        user.Username =  'a' + randomNumber + '@test' + randomNumber + '.com';
        user.Email = 'a' + randomNumber + '@test' + randomNumber + '.com';
        user.FirstName =  'a' + randomNumber;
        user.LastName = 'a' + randomNumber;
        user.CommunityNickname = 's' + randomNumber;
        user.ProfileId = profiles[0].id;
        
        user.Alias =  '' + randomNumber;
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.AboutMe = aboutMe;
        
        if(contact != null){
            user.ContactId = contact.Id;
            user.Contact = contact;
        }

        if(role != null){
            user.UserRoleId = role.ID;
        }
        
        insert user;

        List<PermissionSet> permissionSets = [SELECT ID FROM PermissionSet WHERE Name = :permissionSetName];

        if(permissionSets.size() > 0){
            PermissionSetAssignment  assignPermissionSet=   new PermissionSetAssignment(AssigneeId=user.Id, PermissionSetId=permissionSets.get(0).Id);
            insert assignPermissionSet;

        }

        userCache.put(permissionSetName, user);//just cache the last user of the same type


        return user;
    }

    public static User createUser(Contact contact, String permissionSetName){
        return createUser(contact, permissionSetName, null);
    }

    /**
    * creates the role
    */
    public static UserRole createRole(){
        integer randomNumber = (Integer)( Math.random() * 1000000 );
        UserRole role  = new UserRole(Name = 'CEO' + randomNumber);

        insert role;
            
        return role;
    }

    /**
     *	This will create a new sobject for Contact object with required values
	 *	This can be used by test classes to add additional info to the contact before inserting
	*/
    public static Contact createContactObject(Account account) {
        integer randomNumber = (Integer) (Math.random() * 1000000);
        
        //create partner account
        Contact contact = new Contact();
        contact.LastName = 'con' + randomNumber;
        contact.AccountId = account.ID;
        contact.Account = account;
        
        return contact;
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    * @description creates a contact under the account
    * @param account the account under which the contact is to be created
    * @return returns the newly created contact
    */
    public static Contact createContact(Account account){

        Contact contact = createContactObject(account);
        
        insert contact;
        
        return contact;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    * @description creates an account
    * @return returns an account
    * @lastmodifiedby Pradhanta Bhandari [2014/3/4]
    */
    public static Account createAccount(){
        //2014-3-4 Pradhanta Bhandari: refactored the account creation to allow creation of various account type
        
        return createAccount(ACCOUNT_TYPE_SUPPLIER);
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    * @description creates an account
    * @param accountType the type of account whether supplier or customer
    * @return returns an account
    */
    public static Account createAccount(String accountType){
        integer randomNumber = (Integer) (Math.random() * 1000000);
        
        Account newAccount = new Account();
        newAccount.Name = 'Test-' + Date.today()  + '-' + randomNumber;
        newAccount.Approval_Status__c = 'Approved';
        newAccount.Status__c = 'Active';
            
        insert newAccount;
        
        return newAccount;
    }
    
    /**
    * This method adds the given custom permission to permission set
    * @param permissionSetName api name of the permission set
    * @param customPermissionDeveloperName api name of the custom permission
    * @param userId Id of the user to whome the custom permission is to be assigned
    */
    public static void addPermissionToPermissionSet(String permissionSetName, String customPermissionDeveloperName){
        
        //get the Id of the permission set with the name as supplied in method argument
        List<PermissionSet> ps = getPermissionSet(permissionSetName);
        if(ps.size() > 0){
            
            //Create the Setup Entity Access
            SetupEntityAccess sea = new SetupEntityAccess();
            sea.ParentId = ps[0].Id;
            sea.SetupEntityId = getCustomPermissionByDeveloperName(new List<String>{customPermissionDeveloperName})[0].Id;
            insert sea;
        }
    }

    /**
    * This method returns the list of custom permission based on the developer names
    * @param customPermissionDeveloperName list of names of custom permission api name
    * @return returns list of custom permission.
    */
    public static List<CustomPermission> getCustomPermissionByDeveloperName(List<String> customPermissionDeveloperName){
        return [SELECT Id FROM CustomPermission WHERE DeveloperName IN: customPermissionDeveloperName];
    }

    /**
    * This method returns the permission set based on the name of the permission set
    * @param permissionSetName name of the permission set
    * @return returns permission set.
    */
    public static List<PermissionSet> getPermissionSet(String permissionSetName){
        return [SELECT Id FROM PermissionSet WHERE Name =: permissionSetName];
    }

    // Check user preference lightning mode or not
    public static Boolean doesEnvironmentHaveLightning(){
        return User.getSObjectType().getDescribe().fields.getMap().containsKey('UserPreferencesLightningExperiencePreferred');
    }
    
    // Enable lightning for user
    public static void enableLightning(User user){
        ((SObject)user).put('UserPreferencesLightningExperiencePreferred', true);
        update user;
    }
    
    // Disable lightning for user
    public static void disableLightning(User user){
        ((SObject)user).put('UserPreferencesLightningExperiencePreferred', false);
        update user;
    }
}