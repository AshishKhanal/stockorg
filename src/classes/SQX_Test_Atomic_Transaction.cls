/**
 * This test class checks for transaction atomicity in the processchangeset and submit response
 * methods of various extension methods.
 * It must be understood that SF doesn't enforce atomic transaction in VF Remoting Action, however
 * it does enforce it other invocation such as triggers and vf page actions
 */
@IsTest
public class SQX_Test_Atomic_Transaction {
    private static boolean  runAllTests = true,
                            run_givenInvalidCAPA_ItIsntPersisted = false,
                            run_givenInvalidCAPAResponse_ItIsntPersisted = false,
                            run_givenInvalidNC_ItIsntPersisted = false,
                            run_givenInvalidNCResponse_ItIsntPersisted = false,
                            run_givenInvalidNCWithNote_ItIsntPersisted = false,
                            run_givenInvalidAudit_ItIsntPersisted = false,
                            run_givenInvalidAuditResponse_ItIsntPersisted = false;
        
    private static User createStandardUser(boolean associateRole){
        UserRole role = null;
        if(associateRole){
            role = SQX_Test_Account_Factory.createRole();
        }

        return SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
    }
    
    /**
     * This test checks for transaction in CAPA extension ui's processchangeset
     */
    public testmethod static void givenInvalidCAPA_ItIsntPersisted(){
        if(!runAllTests && !run_givenInvalidCAPA_ItIsntPersisted){
            return;
        }
        
        User stdUser = createStandardUser(false);
        
        System.runAs(stdUser){
            //Arrange: create CAPA changeset for processing the record with invalid NOTE.
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType, false)
                                                                         .setRequired(false, false, false, false, false);
            finding.finding.QMS_Reference_Number__c = 'Hello World';
            changeSet.addChanged('Finding-1', finding.finding);
            
            SQX_CAPA__c capa = new SQX_CAPA__c(
                                                    Issued_Date__c=Date.today().addDays(30), //setting issued date > target date to cause an exception
                                                    Target_Due_Date__c=Date.today(),
                                                    SQX_Action_Approver__c=stdUser.Id,
                                                    SQX_CAPA_Coordinator__c = stdUser.Id,
                                                    CAPA_Type__c = SQX_CAPA.TYPE_INTERNAL
                                                    );
            changeSet.addChanged('CAPA-1', capa)
                             .addRelation(SQX_Capa__c.fields.SQX_Finding__c, 'Finding-1');
            
            //Act: insert the record
            Exception ex = null;
            String processedId = null;
            try{
                    processedId = SQX_Extension_CAPA.processChangeSetWithAction('CAPA-1', changeSet.toJSON(), new SObject[]{}, new Map<String, String> { 'nextAction' => 'save'}, null);
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: Ensure that exception was thrown and neither finding nor capa was inserted
            System.assertNotEquals(null, ex);
            System.assertEquals(0, [SELECT Id FROM SQX_CAPA__c].size());
            
            System.assertEquals(0, [SELECT Id, Name FROM SQX_Finding__c WHERE QMS_Reference_NUmber__c = 'Hello World'].size());
        }
                            
    }
    
    /**
     * This test esnures that transaction is atomic for response. 
     */
    public testmethod static void givenInvalidCAPAResponse_ItIsntPersisted(){
        if(!runAllTests && !run_givenInvalidCAPAResponse_ItIsntPersisted)
            return;
        
        
        User stdUser = createStandardUser(true);
        
        
        System.runAs(stdUser){
                
            //Arrange: Create an Open CAPA that we can respond too.
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            SQX_Finding__c finding = capa.finding.finding;
            
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            SQX_Extension_CAPA_Supplier supplierExt = new SQX_Extension_CAPA_Supplier(new ApexPages.StandardController(capa.capa));
            supplierExt.initializeTemporaryStorage();
            
            //Act: create a response and submit it
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Hello world', SQX_Finding__c = finding.Id));
            changeSet.addChanged('Containment-1', new SQX_Containment__c(SQX_Finding__c = finding.Id, Containment_Summary__c = 'Test', Completion_Date__c = Date.today()));
            changeSet.addChanged('Inclusion-1', new SQX_Response_Inclusion__c(Type__c = SQX_Response_Inclusion.INC_TYPE_CONTAINMENT))
                             .addRelation(SQX_Response_Inclusion__c.fields.SQX_Containment__c, 'Containment-1');
                             //we are skipping response in response inclusion this should result in an error and therefore cause the transaction to fail
            
            Exception ex = null;
            String processedId = null;
            try{
                    // submitResponse(String findingId, String changeSet, String jsonResponse, boolean sendForApproval, SQX_OauthEsignatureValidation electronicSignatureData, Map<String, String> params)
                    processedId = SQX_Extension_UI.submitResponse(capa.capa.Id, '', changeSet.toJSON(), false, null, new Map<String, String>{'nextAction' => 'submitresponse' });
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: an exception was thrown and no containment or response was added.
            System.assertNotEquals(SQX_Extension_UI.OK_STATUS, processedId);
            System.assertEquals(0, [SELECT Id FROM SQX_Containment__c WHERE SQX_Finding__c = : finding.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_Finding__c = : finding.Id].size());
        }
            
            
    }


    /**
    * This test checks for transaction in NC extension UI's processchangeset
    */
    public testmethod static void givenInvalidNC_ItIsntPersisted(){
        if(!runAllTests && !run_givenInvalidNC_ItIsntPersisted){
            return;
        }
        

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runAs(adminUser){
            //Arrange: create NC changeset
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType, false)
                                                                         .setRequired(false, false, false, false, false);
            finding.finding.QMS_Reference_Number__c = 'Hello World';
            changeSet.addChanged('Finding-1', finding.finding);
            
            
            SQX_NonConformance__c nc = new SQX_NonConformance__c();
            nc.Occurrence_Date__c = Date.Today();
            
            changeSet.addChanged('NC-1', nc)
                             .addRelation(SQX_NonConformance__c.fields.SQX_Finding__c, 'Finding-1');

            //set disposition required and due date to null which causes an error
            nc.Disposition_Required__c = true;
            nc.Due_Date_Disposition__c = null;
            
            //Act: insert the record
            Exception ex = null;
            String processedId = null;
            try{
                    processedId = SQX_Extension_NC.processChangeSetWithAction('NC-1', changeSet.toJSON(), new SObject[]{}, new Map<String, String> { 'nextAction' => 'save'}, null);
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: Ensure that exception was thrown and neither finding nor NC was inserted
            System.assertNotEquals(null, ex);
            System.assertEquals(0, [SELECT ID FROM SQX_NonConformance__c].size());
            System.assertEquals(0, [SELECT Id, Name FROM SQX_Finding__c WHERE QMS_Reference_NUmber__c = 'Hello World'].size());
        }
    }


    /*
    * TODO: Fix this testcase, the exact reason of this test case's failure can't be identified. It passes when it is run in dev org, but fails when multiple tests are run.
    * This test esnures that transaction is atomic for response. 
    *
    public testmethod static void givenInvalidNCResponse_ItIsntPersisted(){
        if(!runAllTests && !run_givenInvalidNCResponse_ItIsntPersisted)
            return;
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        
        System.runAs(adminUser){
                
            //Arrange: Create an OPEN NC so that we can respond too.
            SQX_Test_NC nc = new SQX_Test_NC().save();
            SQX_Finding__c finding = nc.finding.finding;
            
            nc.setStatus(SQX_NC.STATUS_OPEN).save();

            
            SQX_Extension_NC ncExt = new SQX_Extension_NC(new ApexPages.StandardController(nc.nc));
            ncExt.initializeTemporaryStorage();
            
            //Act: create a response and submit it
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Hello world', SQX_Finding__c = finding.Id));
            changeSet.addChanged('Containment-1', new SQX_Containment__c(SQX_Finding__c = finding.Id, Containment_Summary__c = 'Test', Completion_Date__c = Date.today()));
            changeSet.addChanged('Inclusion-1', new SQX_Response_Inclusion__c(Type__c = SQX_Response_Inclusion.INC_TYPE_CONTAINMENT))
                             .addRelation(SQX_Response_Inclusion__c.fields.SQX_Containment__c, 'Containment-1');
                             //we are skipping response in response inclusion this should result in an error and therefore cause the transaction to fail
            
            Exception ex = null;
            String processedId = null;
            try{
                    // submitResponse(String findingId, String changeSet, String jsonResponse, boolean sendForApproval, SQX_OauthEsignatureValidation electronicSignatureData, Map<String, String> params)
                    processedId = SQX_Extension_NC.submitResponse(finding.Id, '', changeSet.toJSON(), false, null, new Map<String, String>{'nextAction' => 'submitresponse' });
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: an exception was thrown and no containment or response was added.
            System.assertNotEquals(SQX_Extension_UI.OK_STATUS, processedId);
            System.assertEquals(0, [SELECT Id FROM SQX_Containment__c WHERE SQX_Finding__c = : finding.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_Finding__c = : finding.Id].size());
        }
            
            
    } */

    /**
    * This test checks for transaction atomicity in NC extension UI's processchangeset
    */
    public testmethod static void givenInvalidNCWithNote_ItIsntPersisted(){
        if(!runAllTests && !run_givenInvalidNCWithNote_ItIsntPersisted){
            return;
        }
        

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runAs(adminUser){
            //Arrange: create NC changeset for processing the record with invalid NOTE.
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType, false)
                                                                         .setRequired(false, false, false, false, false);
            finding.finding.QMS_Reference_Number__c = 'Hello World';
            changeSet.addChanged('Finding-1', finding.finding);
                                    
            SQX_NonConformance__c nc = new SQX_NonConformance__c();
            nc.Occurrence_Date__c = Date.Today();
            nc.Disposition_Required__c = false;
            nc.Due_Date_Disposition__c = null;

            changeset.addChanged('Department-1', new SQX_Department__c(Name = 'Random Department'));
            
            changeSet.addChanged('NC-1', nc)
                             .addRelation(SQX_NonConformance__c.fields.SQX_Finding__c, 'Finding-1')
                             .addRelation(SQX_NonConformance__c.fields.SQX_Department__c, 'Department-1');

            //Adding  a note that is invalid i.e. doesn't have a title
            changeset.addChanged('Note-1', new Note())
                             .addRelation(Note.fields.ParentId, 'NC-1');
            
            
            //Act: insert the record
            Exception ex = null;
            String processedId = null;
            try{
                    processedId = SQX_Extension_NC.processChangeSetWithAction('NC-1', changeSet.toJSON(), new SObject[]{}, new Map<String, String> { 'nextAction' => 'save'}, null);
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: Ensure that exception was thrown and neither finding nor capa was inserted
            System.assertNotEquals(null, ex);
            
            System.assertEquals(0, [SELECT Id, Name FROM SQX_Finding__c WHERE QMS_Reference_NUmber__c = 'Hello World'].size(), 'No finding should be saved');
            System.assertEquals(0, [SELECT Id, Name FROM SQX_NonConformance__c].size(), 'No NC should be saved');
            System.assertEquals(0, [SELECT Id, Name FROM SQX_Department__c].size(), 'No Department should be saved');
        }
    }



    /**
    * This test ensures that when an invalid audit record is created it fails and transaction is rolledback
    */
    public testmethod static void givenInvalidAudit_ItIsntPersisted(){

        if(!runAllTests && !run_givenInvalidAudit_ItIsntPersisted)
            return;


        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runAs(adminUser){
            //Arrange: create CAPA changeset for processing the record with invalid NOTE.
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            
            changeset.addChanged('Department-1', new SQX_Department__c(Name = 'New Department'));

            changeset.addChanged('Audit-1', new SQX_Audit__c())
                     .addRelation(SQX_Audit__c.fields.SQX_Department__c, 'Department-1');
            
            //Act: insert the record
            Exception ex = null;
            String processedId = null;
            try{
                processedId = SQX_Extension_Audit.processChangeSetWithAction('Audit-1', changeSet.toJSON(), new SObject[]{}, new Map<String, String> { 'nextAction' => 'save'}, null);
            }
            catch(Exception e){
                    //ignore the exception
                    ex = e;
            }
            
            
            //Assert: Ensure that exception was thrown and neither finding nor capa was inserted
            System.assertNotEquals(null, ex);
            System.assertEquals(0, [SELECT Id FROM SQX_Audit__c].size());
            System.assertEquals(0, [SELECT Id, Name FROM SQX_Department__c WHERE Name = 'New Department'].size());
        }
    }


    /**
    *
    */
    public testmethod static void givenInvalidAuditResponse_ItIsntPersisted(){

        if(!runAllTests && !run_givenInvalidAuditResponse_ItIsntPersisted)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role),
             responseSubmitter = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        SQX_Test_Audit audit;
        SQX_Test_Finding auditFinding1, auditFinding2;

        System.runAs(adminUser){

            audit = new SQX_Test_Audit(adminUser);

            audit.audit.SQX_Auditee_Contact__c = responseSubmitter.Id;
            audit.setPolicy(true, false, false);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            auditFinding1 =  audit.addAuditFinding()
                                                .setRequired(true, true, true, false, false) //response, containment, and investigation required
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();
            auditFinding2 =  audit.addAuditFinding()
                                                .setRequired(true, true, false, false, false)//response, and containment required
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();
        }

        System.runas(responseSubmitter) {

            //Prepare a response for submission with invalid note on one of the response

            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

            changeSet.addChanged('AuditResponse-1', new SQX_Audit_Response__c(SQX_Audit__c= audit.audit.Id,
                                                                            Audit_Response_Summary__c ='Audit Response for finding 1'));

            changeSet.addChanged('responseForFinding1-1', new SQX_Finding_Response__c( 
                                                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                                                Response_Summary__c = 'Response for audit'))
                     .addRelation(SQX_Finding_Response__c.fields.SQX_Audit_Response__c, 'AuditResponse-1');


            changeSet.addChanged('responseForFinding2-1', new SQX_Finding_Response__c( 
                                                                                SQX_Finding__c = auditFinding2.finding.Id,
                                                                                Response_Summary__c = 'Response for audit'))
                      .addRelation(SQX_Finding_Response__c.fields.SQX_Audit_Response__c, 'AuditResponse-1');

            changeSet.addChanged('Containment-1', new SQX_Containment__c(SQX_Finding__c = auditFinding1.finding.Id,
                                                                                         Containment_Summary__c = 'Containment for finding1',
                                                                                         Completion_Date__c = Date.today()));

            changeSet.addChanged('Containment-2', new SQX_Containment__c(SQX_Finding__c = auditFinding2.finding.Id,
                                                                                         Containment_Summary__c = 'Containment for finding2',
                                                                                         Completion_Date__c = Date.today()));

            changeSet.addChanged('Investigation-1', new SQX_Investigation__c(SQX_Finding__c = auditFinding1.finding.Id,
                                                                             Investigation_Summary__c = 'Investigation for finding1'));

            changeSet.addChanged('Inclusion-1', new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Containment__c, 'Containment-1')
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Response__c,'responseForFinding1-1');

            changeSet.addChanged('Inclusion-2', new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Investigation__c, 'Investigation-1')
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Response__c,'responseForFinding2-1');

            changeSet.addChanged('Inclusion-3', new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Containment__c, 'Containment-2')
                     .addRelation(SQX_Response_Inclusion__c.fields.SQX_Response__c,'responseForFinding1-1');


            //invalid note
            changeSet.addChanged('Note-1', new Note()).addRelation(Note.fields.ParentId, 'responseForFinding1-1');


            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'submitresponse');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');



            /*
            * Act : Submit audit response for audit
            */
            boolean wasExceptionThrown = false;
            String retMsg = null;

            try{
                retMsg = SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params );
            }
            catch(Exception ex){
                wasExceptionThrown = true;
            }

            //Assert: record was inserted successfylly
            System.assertNotEquals(SQX_Extension_UI.OK_STATUS, retMsg);
            System.assertEquals(0, [SELECT Id FROM SQX_Containment__c WHERE SQX_Finding__c = : auditfinding1.finding.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_Finding__c  = : auditfinding1.finding.Id].size());

            
        }
    }
}