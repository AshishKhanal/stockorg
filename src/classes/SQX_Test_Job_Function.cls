/**
* Unit tests for extension of job function
*/
@IsTest
public class SQX_Test_Job_Function {
    SQX_Job_Function__c jf {get; private set;}

    /**
     * constructor method to make job function active by default
     * @param jf job function to create
     */
    public SQX_Test_Job_Function(SQX_Job_Function__c jf){
        this.jf = jf;
        jf.Active__c = true;
    }

    /**
     * constructor method to give users option to activate or deactivate process builder
     * @param jf job function to create
     * @param activeOnSave flag to set job function active or inactive on create
     */
    public SQX_Test_Job_Function(SQX_Job_Function__c jf, Boolean activeOnSave){
        this.jf = jf;
        jf.Active__c = activeOnSave;
    }

    /**
     * creates number of job functions passed in the param numberOfJFs
     * @param numberOfJFs number of job function to create
     * @param activeOnSave flag to set job function active or inactive on create
     * @return list of jfs created
     */
    public static List<SQX_Job_Function__c> createJFs(Integer numberOfJFs, Boolean activeOnSave){
        List<SQX_Job_Function__c> jfsToCreate = new List<SQX_Job_Function__c>();
        for(Integer i = 1; i <= numberOfJFs; i++){
            SQX_Job_Function__c jf = new SQX_Job_Function__c(Name = 'JF' + i, Active__c = activeOnSave);
            jfsToCreate.add(jf);
        }

        return jfsToCreate;
    }

    /**
     * method to save job function
     * @return returns the result status of the saved record
     */
    public Database.SaveResult save(){
        Database.SaveResult result = null;
        if(jf.Id == null){
            result = Database.insert(jf, false);
        } else {
            result = Database.update(jf, false);
        }
        return result;
    }
    /**
    * ensures SQX_Job_Function.getActiveUsers() and SQX_Job_Function.getActiveUsersForJobFunctions() to return active users
    */
    public static testmethod void givenGetActiveUsers() {
        /*
        * user, personnel and pjfs setup for testing:
        *                   user1   user2   user3   user4   user5   user6  user7(different JF)
        *                   =====   =====   =====   =====   =====   =====  ===================
        * active user       true    true    true    false   true    true   true
        * active personnel  true    true    false   true    true    -      true
        * active PJF        true    false   false   true    -       -      true
        */
        
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user3 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user4 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user5 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user6 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user7 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(2, true);
            insert jfs;
            SQX_Job_Function__c jf1 = jfs[0];
            SQX_Job_Function__c jf2 = jfs[1];
            
            // add required personnel records
            SQX_Personnel__c p1 = SQX_Personnel.copyFieldsFromUser(user1, new SQX_Personnel__c()); p1.Active__c = true;
            SQX_Personnel__c p2 = SQX_Personnel.copyFieldsFromUser(user2, new SQX_Personnel__c()); p2.Active__c = true;
            SQX_Personnel__c p3 = SQX_Personnel.copyFieldsFromUser(user3, new SQX_Personnel__c()); p3.Active__c = true;
            SQX_Personnel__c p4 = SQX_Personnel.copyFieldsFromUser(user4, new SQX_Personnel__c()); p4.Active__c = true;
            SQX_Personnel__c p5 = SQX_Personnel.copyFieldsFromUser(user5, new SQX_Personnel__c()); p5.Active__c = true;
            SQX_Personnel__c p7 = SQX_Personnel.copyFieldsFromUser(user7, new SQX_Personnel__c()); p7.Active__c = true;
            insert new List<SQX_Personnel__c>{ p1, p2, p3, p4, p5, p7 };
            
            // use start and stop test to execute future method
            Test.startTest();
            
            // make user4 inactive
            user4.IsActive = false;
            update user4;
            
            Test.stopTest();
            
            // make p4 active
            p4.Active__c = true;
            update p4;
            
            // add required PJF records
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true );
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf1.Id, Active__c = false );
            SQX_Personnel_Job_Function__c pjf3 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p3.Id, SQX_Job_Function__c = jf1.Id, Active__c = true );
            SQX_Personnel_Job_Function__c pjf4 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p4.Id, SQX_Job_Function__c = jf1.Id, Active__c = true );
            SQX_Personnel_Job_Function__c pjf7 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p7.Id, SQX_Job_Function__c = jf2.Id, Active__c = true );
            insert new List<SQX_Personnel_Job_Function__c>{ pjf1, pjf2, pjf3, pjf4, pjf7 };
            
            // make p3 inactive
            p3.Active__c = false;
            update p3;
            
            // ACT: get active users for jf1
            Set<Id> userIds = new Set<Id>();
            for (SQX_Job_Function.SQX_JobFunctionUser_Wrapper u : SQX_Job_Function.getActiveUsers(jf1.Id, null)) {
                userIds.add(u.Id);
            }
            
            System.assertEquals(1, userIds.size());
            System.assert(userIds.contains(user1.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user2.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user3.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user4.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user5.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user6.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user7.Id) == false, 'User is not expected to be returned.');
            
            // ACT: get active users for null value
            userIds = new Set<Id>();
            for (SQX_Job_Function.SQX_JobFunctionUser_Wrapper u : SQX_Job_Function.getActiveUsers(null, null)) {
                userIds.add(u.Id);
            }
            
            System.assert(userIds.size() >= 6);
            System.assert(userIds.contains(user1.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user2.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user3.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user4.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user5.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user6.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user7.Id), 'User is expected to be returned.');
            
            // create filters as sent by kendo dropdown
            SQX_DynamicQuery.Filter filter1 = new SQX_DynamicQuery.Filter(),
                                    filter2 = new SQX_DynamicQuery.Filter(),
                                    mainFilter = new SQX_DynamicQuery.Filter();
            mainFilter.logic = 'and';
            mainFilter.filters.add(filter1);
            mainFilter.filters.add(filter2);
            
            filter1.field = 'JobFunctionId';
            filter1.operator = 'eq';
            filter1.value = null;
            
            filter2.field = 'Name';
            filter2.operator = 'contains';
            filter2.value = user5.FirstName;
            
            // ACT: get active users for null value with filters
            userIds = new Set<Id>();
            for (SQX_Job_Function.SQX_JobFunctionUser_Wrapper u : SQX_Job_Function.getActiveUsers(null, mainFilter)) {
                userIds.add(u.Id);
            }
            
            System.assertEquals(1, userIds.size());
            System.assert(userIds.contains(user1.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user2.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user3.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user4.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user5.Id), 'User is expected to be returned.');
            System.assert(userIds.contains(user6.Id) == false, 'User is not expected to be returned.');
            System.assert(userIds.contains(user7.Id) == false, 'User is not expected to be returned.');
            
            
            // ACT: get active users for jf2
            Map<Id, List<User>> usersMap = SQX_Job_Function.getActiveUsersForJobFunctions(new Set<Id>{ jf2.Id }, null, null);
            Map<Id, User> users = new Map<Id, User>(usersMap.get(jf2.Id));
            
            System.assertEquals(1, usersMap.size());
            System.assert(users != null, 'Active user list is expected to be returned.');
            System.assertEquals(1, users.size());
            System.assert(users.get(user1.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user2.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user3.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user4.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user5.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user6.Id) == null, 'User is not expected to be returned.');
            System.assert(users.get(user7.Id) != null, 'User is expected to be returned.');
            
            // ACT: get active users for jf2 with filters
            usersMap = SQX_Job_Function.getActiveUsersForJobFunctions(new Set<Id>{ jf2.Id }, mainFilter, new Set<String>{ 'JobFunctionId' });
            
            System.assertEquals(1, usersMap.size());
            System.assertEquals(0, usersMap.get(jf2.Id).size());
        }
    }
}