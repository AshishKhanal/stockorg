/**
* Represents the request payload for proxy server 
*/
public class SQX_RequestPayload {
    public Action[] action;
    public Action[] success;
    public Action[] failure;
    
    /**
    * Inner class representing actions to be taken
    */
    public class Action {
        
        public Action(){}
        
        public Action(String type, String provider, Content[] content){
            this.type = type;
            this.provider = provider;
            this.content = content;
        }
        
        public String type;
        public String provider;
        public Content[] content;
    }
    
    /**
    * Inner class representing content of request payload
    */
    public class Content {
        
        public Content(){}
        
        public Content(String type, String url, String mimetype, String content, String method, Principal principal){
            this.type = type;
            this.url = url;
            this.mimetype = mimetype;
            this.content = content;
            this.method = method;
            this.principal = principal;
        }
        public String type;
        public String url;
        public String mimetype;
        public String content;
        public String method;
        public Principal principal;
    }
    
    /**
    * Inner class identifying user
    */
    public class Principal {
        
        public Principal(){}
        
        public Principal(String id, String url) {
            this.id = id;
            this.url = url;
        }

        public String id;
        public String url;
    }

}