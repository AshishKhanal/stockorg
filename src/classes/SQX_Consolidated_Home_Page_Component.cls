/**
* This is the controller class that passes list of homepage items to the UI component
* @story   SQX-3159 Homepage UI and Framework Implementation
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-06-03
*/
public with sharing class SQX_Consolidated_Home_Page_Component {

    /*
        Holds a list of all item sources from which to fetch homepage items from
    */
    public static List<SQX_Homepage_ItemSource> allItemSources = new List<SQX_Homepage_ItemSource>
    {
        new SQX_Homepage_Source_Audit_Items(),
        new SQX_Homepage_Source_Capa_Items(),
        new SQX_Homepage_Source_ChangeOrder_Items(),
        new SQX_Homepage_Source_Complaint_Items(),
        new SQX_Homepage_Source_DocumentMgmt_Items(),
        new SQX_Homepage_Source_Finding_Items(),
        new SQX_Homepage_Source_Inspection_Items(),
        new SQX_Homepage_Source_ItemsToApprove(),
        new SQX_Homepage_Source_NC_Items(),
        new SQX_Homepage_Source_Supplier_Items(),
        new SQX_Homepage_Source_Training_Items()
    };

    /**
    * Compiles list of homepage items from given sources
    * @param filterString - serialized string that contains info about various filters to be applied to the records when fetching
    * @return serialized list of homepage items from the source
    */
    @AuraEnabled
    public static String getHomePageItems(String filterString){

        SQX_Homepage_Filter filter;

        if(String.isBlank(filterString)){
            filterString = '{}';
        }

        filter = (SQX_Homepage_Filter) JSON.deserialize(filterString, SQX_Homepage_Filter.Class);

        List<SQX_Homepage_ItemSource> sourceItems = new List<SQX_Homepage_ItemSource>();

        /* set filter for sources */
        for( SQX_Homepage_ItemSource item: allItemSources ){
            item.filter = filter;
            sourceItems.add(item);
        }

        return JSON.serialize( getHomePageItemsFromSources(sourceItems) );
    }

     /**
     * Returns the pending items for given current user
     */
    @AuraEnabled
    public static String getPendingItems(){

        SQX_Homepage_Filter filter = (SQX_Homepage_Filter) JSON.deserialize('{}', SQX_Homepage_Filter.Class);
        List<SQX_Homepage_Source_Training_Items> sourceItems = new List<SQX_Homepage_Source_Training_Items>();

        /* set filter for sources */
        SQX_Homepage_Source_Training_Items item = new SQX_Homepage_Source_Training_Items();
        item.filter = filter;
        item.needingOnlyPendingTrainings = true;
        sourceItems.add(item);

        return JSON.serialize( getHomePageItemsFromSources(sourceItems) );
    }

    /**
    * Compiles list of homepage items from given sources
    * @param containers : list of sources from which to fetch items from
    * @return list of homepage items from the source
    */
    private static HomepageItems getHomePageItemsFromSources(List<SQX_HomePage_ItemSource> containers){

        HomepageItems hpItems = new HomepageItems();

        List<SQX_Homepage_Item> items = new List<SQX_Homepage_Item>();

        List<String> exceptionMsgs = new List<String>();

        for(SQX_Homepage_ItemSource item : containers){

            try{
                List<SQX_Homepage_Item> hItems = item.getHomePageItems();

                if( hItems.size()> 0 ){
                    items.addAll(hItems);
                }
            }catch(Exception ex){
                exceptionMsgs.add(ex.getMessage());
            }
        }

        hpItems.items = items;
        hpItems.errors = exceptionMsgs;
        
        return hpItems;
    }

    /**
        Wrapper class representing homepage items returned from each sources
    */
    public class HomepageItems
    {
        public List<SQX_Homepage_Item> items;
        public List<String> errors;
        public Map<String, String> iconsMap;
        
        public HomepageItems()
        {
            iconsMap = new Map<String, String>();
            iconsMap.putAll(SQX_Homepage_Constants.ACTION_ICON_MAP);
            iconsMap.putAll(SQX_Homepage_Constants.MODULE_ICON_MAP);
        }
    }
}