/**
* Test to ensure migration script SQX_6_1_MigrateObsoleteDocumentTrainings (using SQX_PostInstall_Processor) sets pending trainings of obsolete controlled documents to obsolete
*/
@isTest
public class SQX_Test_2129_MigratePendingTrainings {
    @testSetup
    public static void commonSetup() {
        // add required users
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensures only pending document trainings of obsolete controlled documents are set to obsolete when upgraded or migrated
    */
    public static testmethod void givenRetireDocumentWithPendingTrainings_TrainingsStatusObsolete() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {
            // add required personnel
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            p1.mainRecord.Active__c = true;
            p1.save();
            
            // add required obsolete controlled doc
            SQX_Test_Controlled_Document obsCDoc1 = new SQX_Test_Controlled_Document().save();
            obsCDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // add required released controlled doc
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().save();
            cDoc2.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            // Note: Adding required trainings as migrated data since training cannot be added to obsolete controlled document
            // add required pending training of obsCDoc1
            SQX_Personnel_Document_Training__c dt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = obsCDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                IsMigrated__c = true,
                Uniqueness_Constraint__c = '' + p1.mainRecord.Id + obsCDoc1.doc.Id
            );
            // add required trainer approval pending document training of obsCDoc1
            SQX_Personnel_Document_Training__c dt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = obsCDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                IsMigrated__c = true,
                Uniqueness_Constraint__c = '' + p1.mainRecord.Id + obsCDoc1.doc.Id + 'dt2'
            );
            // add required completed document training of obsCDoc1
            SQX_Personnel_Document_Training__c dt3 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = obsCDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                IsMigrated__c = true,
                Uniqueness_Constraint__c = '' + p1.mainRecord.Id + obsCDoc1.doc.Id + 'dt3'
            );
            // add required completed document training of cDoc2
            SQX_Personnel_Document_Training__c dt4 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today()
            );
            insert new List<SQX_Personnel_Document_Training__c> { dt1, dt2, dt3, dt4 };
            
            Test.startTest();
            
            // ACT: start post install migration
            SQX_PostInstall_Processor processor = new SQX_PostInstall_Processor();
            Test.testInstall(processor, new Version(5,0));
            
            Test.stopTest();
            
            // get document trainings with Obsolete status
            List<SQX_Personnel_Document_Training__c> dts = [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_OBSOLETE];
            
            // expected pending trainings to be obsolete controlled document to be obsolete
            System.assertEquals(1, dts.size());
            System.assertEquals(dt1.Id, dts[0].Id);
        }
    }
}