/**
 * helper class for complaint association item trigger
 */
public with sharing class SQX_Complaint_Association_Item {


    /**
    * Complaint Association Item Trigger Handler performs actions when Complaint Association Item object is being manipulated, 
    * It primarily assigns default values, and updates related finding.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newComplaintAssociatedItems equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Complaint_Associated_Item__c> newComplaintAssociatedItems, Map<Id,SQX_Complaint_Associated_Item__c> oldMap){
        
            super(newComplaintAssociatedItems, oldMap);
        }

        /**
         *  Method to prevent deletion of primary association item
         */
        public Bulkified preventPrimaryAssociationItemDeletion() {
            this.resetView();
            
            if(this.view.size() > 0){
                for(SQX_Complaint_Associated_Item__c associatedItem : (List<SQX_Complaint_Associated_Item__c>)this.view){
                    if(associatedItem.Is_Primary__c){
                        associatedItem.addError(Label.SQX_ERR_MSG_Primary_Associated_Item_cannot_be_deleted);
                    }
                }
            }
            return this;
        }
        
        /**
         * Method to prevent deletion of complaint association item if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords() {
            
            this.resetView();
            
            Map<Id, SQX_Complaint_Associated_Item__c> complaintAssociatedItems = new Map<Id, SQX_Complaint_Associated_Item__c>((List<SQX_Complaint_Associated_Item__c>)this.view);
            
            for(SQX_Complaint_Associated_Item__c complaintAssociatedItem : [SELECT Id FROM SQX_Complaint_Associated_Item__c WHERE Id IN : complaintAssociatedItems.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                complaintAssociatedItems.get(complaintAssociatedItem.Id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
        }
        /**
        * Method to copy associated item id to complaint's primary associated item
        */
        public Bulkified syncPrimaryAssociatedItem(){
            Rule primaryAssociatedItemRule = new Rule();
            primaryAssociatedItemRule.addRule(Schema.SQX_Complaint_Associated_Item__c.Make_Primary__c, RuleOperator.Equals, TRUE);

            this.resetView()
                .applyFilter(primaryAssociatedItemRule, RuleCheckMethod.OnCreateAndEveryEdit);
            
            List<SQX_Complaint_Associated_Item__c> associatedItemList = (List<SQX_Complaint_Associated_Item__c>) primaryAssociatedItemRule.evaluationResult;
            if(associatedItemList.size() > 0){
                Map<Id, SQX_Complaint_Associated_Item__c> complaintAssociatedItemMap = new Map<Id, SQX_Complaint_Associated_Item__c>();
                List<SQX_Complaint__c> complaintsToUpdate = new List<SQX_Complaint__c>();
                for(SQX_Complaint_Associated_Item__c associatedItem : associatedItemList){
                    complaintAssociatedItemMap.put(associatedItem.SQX_Complaint__c, associatedItem);
                }

                for(Id complaintId : complaintAssociatedItemMap.keySet()){
                    
                    SQX_Complaint_Associated_Item__c associatedItem = complaintAssociatedItemMap.get(complaintId);
                    SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaintId, 
                                                                        SQX_Primary_Associated_Item__c = associatedItem.Id,
                                                                        SQX_Primary_Investigation__c = associatedItem.SQX_Primary_Investigation__c);
                    complaintsToUpdate.add(complaint);
                }

                if(complaintsToUpdate.size() > 0){
                    new SQX_DB().op_update(complaintsToUpdate, new List<Schema.SObjectField>{SQX_Complaint__c.SQX_Primary_Associated_Item__c, 
                                                                                                SQX_Complaint__c.SQX_Primary_Investigation__c});
                }
            }
            return this;
        }
    }
}