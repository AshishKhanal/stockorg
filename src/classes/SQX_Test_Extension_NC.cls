/**
* Unit tests for SQX_Extension_NC
*
* @author Sagar Shrestha
* @date 2014/5/22
* 
*/
@isTest
public class SQX_Test_Extension_NC{
    
    /**
    *   Setup: create NC, save 
    *   Action: void NC
    *   Expected: should have status closed and resolution void
    *
    * @author Sagar Shrestha
    * @date 2015/3/24
    * 
    */
    public static testmethod void voidNC(){ 
            UserRole mockRole = SQX_Test_Account_Factory.createRole();  
            User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
            User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
            SQX_Test_NC nc = null;
            User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
            
            System.runas(adminUser){
                nc = new SQX_Test_NC(new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                                .setRequired(false,false,true,false,false) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(false, false, false)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_DRAFT)
                                .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                                .save());

                nc.setDispositionRules(false, null, false, null)
                  .save();

                nc.setStatus(SQX_NC.STATUS_TRIAGE).save(); 

                SQX_Finding__c changedFinding = new SQX_Finding__c(Closure_Comments__c = 'Voided', Id = [Select SQX_Finding__c from SQX_Nonconformance__c where Id=:nc.nc.Id ].SQX_Finding__c);

                Map<String, Object> submissionSet = new Map<String, Object>();
                List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

                Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedFinding));
                Map<String, Object> originalValues = new Map<String, Object>();

                originalValues.put(SQX.getNSNameFor('Closure_Comments__c'), null);

                changeSet.add(findingMap);

                submissionSet.put('changeSet', changeSet);

                findingMap.put('originalValues', originalValues);
               

                String stringChangeSet= JSON.serialize(submissionSet);                      
                
                //void
                Map<String, String> paramsVoid = new Map<String, String>();
                paramsVoid.put('nextAction', 'Void');
                paramsVoid.put('recordID', nc.nc.Id);
                paramsVoid.put('purposeOfSignature', 'voiding CAPA');

                String result= SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id,stringChangeSet, null, paramsVoid);

                SQX_Nonconformance__c voidedNC=[Select Status__c, Resolution__c from SQX_Nonconformance__c where Id= :nc.nc.Id]; 

                System.assert(voidedNC.Status__c=='Closed', 
                    'Expected to void CAPA but is '+voidedNC.Status__c);

                System.assert(voidedNC.Resolution__c==SQX_Finding.RESOLUTION_VOID, 
                    'Expected to void CAPA but is '+voidedNC.Resolution__c);
            }
    }

    /**
    *   Setup: create NC, save 
    *   Action: send response, reject response; 
    *   Expected: response should have approval status='Rejected'
    *
    *   Action: send response, approve response; 
    *   Expected: response should have approval status='Approved'
    *
    * @author Sagar Shrestha
    * @date 2015/3/24
    * 
    */
    public static testmethod void givenResponsePublised_ResponseCanBeApprovedorRejected(){

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            nc = new SQX_Test_NC(true);

            nc.setDispositionRules(false, null, false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();

            nc.nc.OwnerId = ncCoordinator.Id;

            nc.save();
        }

        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();

            //response.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            
            
            boolean exceptionOccurred = false;
            try{
                response.publishResponse();
            }
            catch(Exception ex){
                exceptionOccurred = true;
            }
            
            System.assertEquals(false, exceptionOccurred);
            nc.synchronize();

            Map<String, String> paramsReject = new Map<String, String>();
            paramsReject.put('nextAction', 'reject');
            paramsReject.put('recordID', response.response.Id);

            SQX_Finding_Response__c findingResponse = [SELECT Id, Approval_Status__c, Status__c FROM SQX_Finding_Response__c WHERE Id =: response.response.Id];

            Id result= SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, '{"changeSet": []}', new SObject[]{}, paramsReject);

            System.assert(findingResponse.Status__c == SQX_Finding_Response.STATUS_PUBLISHED, 'Expected the reponse to be published but found status' + findingResponse.Status__c);

            SQX_Test_Finding_Response response2 = new SQX_Test_Finding_Response(nc.nc);

            response2.save();

            //response2.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            //response2.investigationsIncluded.get(0).addAnActionPlan(null, 'All done', null, true, Date.today());
            
            exceptionOccurred = false;
            try{
                response2.publishResponse();
            }
            catch(Exception ex){
                exceptionOccurred = true;
            }
            
            System.assertEquals(false, exceptionOccurred);
            nc.synchronize();

            Map<String, String> paramsApprove = new Map<String, String>();
            paramsApprove.put('nextAction', 'approve');
            paramsApprove.put('recordID', response2.response.Id);

            findingResponse = [SELECT Id, Approval_Status__c, Status__c FROM SQX_Finding_Response__c WHERE Id =: response2.response.Id];

            Id resultApprove= SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, '{"changeSet": []}', new SObject[]{}, paramsApprove);

            System.assert(findingResponse.Status__c == SQX_Finding_Response.STATUS_PUBLISHED, 'Expected the reponse to be published but found  status' + findingResponse.Status__c);
        }
    }

    /**
     * Given: An NC is created
     * When : NC is Submitted
     * Then: Only two record activities are created for save and submit
     * @date:6/14/2016
     * @author:Manoj Thapa
     * @story no:SQX_2156
     */
    public static testmethod void givenNC_WhenNCIsSavedSubmittedAndInitiated_OnlyRelatedActivityAreCreated(){

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        Group queue;
        SQX_Department__c randomDepartment;

        
        // Arrange1:  queue Id is assigned to  Department
         
        System.runas(adminUser){

            queue = new Group();
                queue.Name = 'TestGroup';
                queue.DeveloperName = 'Test_Queue';
                queue.Type = 'Queue';

                insert queue;
            

            QueueSObject sobj = new QueueSObject(SobjectType = SQX.NonConformance,
                                                    QueueId = queue.Id);
     
            insert sObj;

            sobj = new QueueSObject(SobjectType = SQX.Finding,
                                                    QueueId = queue.Id);
     
            insert sObj;

            // add queue members
            GroupMember queueMembers = new GroupMember(GroupId = queue.Id, UserOrGroupId = standardUser.Id);
            insert queueMembers;

            queueMembers = new GroupMember(GroupId = queue.Id, UserOrGroupId = adminUser.Id);
            insert queueMembers;

            randomDepartment = new SQX_Department__c(
                                                 Name='Random Department',
                                                 Queue_Id__c = queue.Id
                                               );
            insert randomDepartment;

        }

        System.runas(standardUser){
            //Arrange2 : a NC is created and department Id is assigned to the NC
            nc = new SQX_Test_NC(true).save();

            nc.nc.SQX_Department__c = randomDepartment.Id;
            nc.save();

            System.assert(nc.nc.Id != null, 'No NC is created');
            //Act1: recordActivity is created for the given NC during submit action
            Map<String, String> recActivity = new Map<String, String>();
            recActivity.put('nextAction', 'Submit');
            recActivity.put('recordID', nc.nc.Id);

            String result = SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, '{"changeSet": []}', new SObject[]{}, recActivity);

            List<SQX_NC_Record_Activity__c> ncRecordActivityList = [SELECT Id FROM SQX_NC_Record_Activity__c WHERE SQX_Nonconformance__c =: nc.nc.Id];
            //Assert1: checked the number of ncRecordActivity
            System.assertEquals(1, ncRecordActivityList.size());

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act2: recordActivity is created for the given NCduring takeownershipandinitiate action

            recActivity = new Map<String, String>();
            recActivity.put('nextAction', 'takeownershipandinitiate');
            recActivity.put('recordID', nc.nc.Id);

            String result1 = SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, '{"changeSet": []}', new SObject[]{}, recActivity);

             ncRecordActivityList = [SELECT Id FROM SQX_NC_Record_Activity__c WHERE SQX_Nonconformance__c =: nc.nc.Id];
             //Assert: checked the number of ncRecordActivity
            System.assertEquals(2, ncRecordActivityList.size());

            }
        }

    }
