/**
* @author Shailesh Maharjan
* @date 2015/12/05
* extension class for Personnel Job Function object
*/
public with sharing class SQX_Extension_Personnel_Job_Function {
    
    ApexPages.StandardController standardController;
    SQX_Personnel_Job_Function__c currentPJF;
    PageReference saveAndNewReturnURL;

    private static final String PARAM_JF = 'jobfunction',
                                PARAM_PERSONNEL = 'personnel';

    /**
    * This method generates customized messages that are proper to display to users
    */
    private String getCustomizedErrorMessage(String message) {
        String msg = message;

        if (msg.contains('Unique_New_Or_Active_Constraint__c')) {
            String fieldLabel = Schema.SObjectType.SQX_Personnel_Job_Function__c.fields.Unique_New_Or_Active_Constraint__c.getLabel();
            
            SQX_Personnel_Job_Function__c dupRec = [SELECT Id, Name FROM SQX_Personnel_Job_Function__c
                                               WHERE SQX_Job_Function__c = :currentPJF.SQX_Job_Function__c
                                               AND SQX_Personnel__c = :currentPJF.SQX_Personnel__c
                                               AND Deactivation_Date__c = NULL];
             
            PageReference viewRef = new ApexPages.StandardController(dupRec).view();

            msg = 'Duplicate value on record: <a target="_blank" href="' + viewRef.getUrl() + '">' + dupRec.Name + '</a> (' + fieldLabel + ')';
        }
        else if (msg.contains('insufficient access rights on cross-reference id')){
            String fieldLabel = Schema.SObjectType.SQX_Personnel_Job_Function__c.fields.Unique_New_Or_Active_Constraint__c.getLabel();
            List<SQX_Personnel__c> psns = [ SELECT Name
                                            FROM SQX_Personnel__c
                                            WHERE Id = :currentPJF.SQX_Personnel__c];
            
            msg = 'You do not have sufficient access on the personnel';
            if (!psns.isEmpty()) {
                PageReference viewpjfRef = new ApexPages.StandardController(psns[0]).view();
                
                msg += ' <a target="_blank" href="' + viewpjfRef.getUrl() + '">' + psns[0].Name + '</a> ';
            }
            else {
                msg += ' (' + currentPJF.SQX_Personnel__c + ') ';
            }
            msg += '. Please contact the owner of the record or your administrator if access is necessary.';
        }

        return msg;
    }
    
    /**
    * Internal method that sets Save and New Return URL
    */
    void setSaveAndNewReturnURL() {
        // return url to SQX_New_Personnel_Job_Function page with original query params
        saveAndNewReturnURL = Page.SQX_New_Personnel_Job_Function;
    
        for (String param : ApexPages.currentPage().getParameters().keySet()) {
            saveAndNewReturnURL.getParameters().put(param, ApexPages.currentPage().getParameters().get(param));
        }
    }

    /**
    * Internal method that returns redirect URL
    * @param isSaveAndNew boolean value to denote is save and new 
    */
    PageReference getReturnURL(boolean isSaveAndNew) {
        PageReference retUrl;
        
        if (isSaveAndNew) {
            retUrl = saveAndNewReturnURL;
        } else {
            // return url to view page of saved record
            retUrl = new ApexPages.StandardController(currentPJF).view();
        }
        
        retUrl.setRedirect(true);
        return retUrl;
    }
    
    public SQX_Extension_Personnel_Job_Function(ApexPages.StandardController controller) {
        this.standardController = controller;
        this.currentPJF = (SQX_Personnel_Job_Function__c)controller.getRecord();
        setSaveAndNewReturnURL();

        // set passed personnel and Job Function values for new record
        if (currentPJF.Id == null) {

            String personnelQueryParam = Apexpages.currentPage().getParameters().get(PARAM_PERSONNEL);
            String jobfunctionQueryParam = Apexpages.currentPage().getParameters().get(PARAM_JF);

            if (String.isNotBlank(personnelQueryParam)) {

                // set personnel value
                setJobFunction((Id)personnelQueryParam);
            }

            if (String.isNotBlank(jobfunctionQueryParam)) {
                
                // set job function value
                setJobFunction((Id)jobfunctionQueryParam);
            }
        }
    }

    /**
    * Internal method that sets the Job_function for the record type Id that is passed as argument
    * @param recordId id of the record that is to be queried
    */
    private void setJobFunction(Id recordId) {
        String objectType = recordId.getSObjectType().getDescribe().getName();
        String dynamicQuery = 'SELECT Id FROM ' + objectType + ' WHERE Id = :recordId';
        List<SObject> records = Database.query(dynamicQuery);
        if (records.size() == 1) {
            currentPJF.put(objectType, records[0].Id);
        }
    }
   
    /**
    * saves current new record
    * @return returns the saveresult of the current new Personnel Job Function object
    */
    PageReference saveRecord(boolean isSaveAndNew) {
        PageReference returnTo = null;
        
        Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Personnel_Job_Function__c> { currentPJF }, new List<Schema.SObjectField>{ }).get(0);
        
        if (result.isSuccess()) {
            returnTo = getReturnURL(isSaveAndNew);
        } else {
            // customize error messages, if required, and return error messages to same page
            for (Database.Error error : result.getErrors()) {
                String msg = getCustomizedErrorMessage(error.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
            }
        }

        return returnTo;
    }
    
    /**
    * save action for custom vf page
    */
    public PageReference save() {
        return saveRecord(false);
    }
    
    /**
    * save and new action for custom vf page
    */
    public PageReference saveAndNew() {
        return saveRecord(true);
    }
}