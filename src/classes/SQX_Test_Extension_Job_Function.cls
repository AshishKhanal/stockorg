/**
 * Test for SQX_Extension_Job_Function class and its methods.
 */
@isTest
public class SQX_Test_Extension_Job_Function {
    
    @testSetup
    public static void commonSetup() {
        
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');
        
        // creating common required records
        System.runAs(standardUser) {

            // create job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            jf.save();
            
            // create personnel
            SQX_Personnel__c p1 = new SQX_Test_Personnel('PersonnelName1').mainRecord;
            SQX_Personnel__c p2 = new SQX_Test_Personnel('PersonnelName2').mainRecord;
            
            insert(new List<SQX_Personnel__c>{ p1, p2});
                
            // add required PJF records
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true );
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf1.Id, Active__c = false );
            
            insert(new List<SQX_Personnel_Job_Function__c>{ pjf1, pjf2});
        }
        
    }
    
    /**
     * Given : Job function say jf1, create two pjfs say pjf1 and pjf2
     * When : getActivePJFs() method of extension class is called
     * Then : It should return 2 active pjfs
     *
     * When : pjf1 is deactivated
     * Then : It will return only 1 active pjf
     *
     * When : The includeDeactivatedPJF flag of extension is checked
     * Then : It will return 2 pjfs i.e. one is active and another is deactive
     */ 
    public static testmethod void ensurecCorrectNumberofPJFsRecordAreReturnedByExtension() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            SQX_DB sqxDb = new SQX_DB();

            // Arrange : get the job function, personnels and pjfs
            SQX_Job_Function__c jf1 = [SELECT Id from SQX_Job_Function__c WHERE Name = 'job function 1'];
            
            List<SQX_Personnel__c> personnels = [SELECT Id FROM SQX_Personnel__c WHERE Full_Name__c IN :new List<String> {'PersonnelName1', 'PersonnelName2'}];
                
            // add required PJF records
            List<SQX_Personnel_Job_Function__c> pjfList = [SELECT Id, Active__c, Deactivation_Date__c, Personnel_Name__c, Name FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c = :jf1.Id AND
                                                 SQX_Personnel__c IN :personnels ORDER BY Personnel_Name__c ASC, Name DESC];
            
            // Act : get the pjf record by calling getActivePJFs() method of extension.
            ApexPages.StandardController sc = new ApexPages.standardController(jf1);
            SQX_Extension_Job_Function ext = new SQX_Extension_Job_Function(sc);
            ApexPages.StandardSetController activePJFController = ext.getPersonnelJobFunctions();
            
            // Assert: There will be 2 pjfs
            List<SObject> pjfs = activePJFController.getRecords();
            System.assert(pjfs.size() == 2, 'Expected 2 active pjf but got ' + pjfs.size());
            
            // deactivate the pjf1
            pjfList[0].Active__c = false;
            pjfList[0].Deactivation_Date__c = System.today();
            sqxDb.op_update(pjfList, new List<Schema.SObjectField> {
                                            SQX_Personnel_Job_Function__c.Deactivation_Date__c,
                                            SQX_Personnel_Job_Function__c.Active__c
                                            });

            // get active pjf
            ext = new SQX_Extension_Job_Function(sc);
            activePJFController = ext.getPersonnelJobFunctions();
            pjfs = activePJFController.getRecords();

            // Assert: There will be 1 pjf and that will be pjf2
            System.assert(pjfs.size() == 1, 'Expected 1 active pjf but got ' + pjfs.size());
            System.assert(pjfs[0].Id == pjfList[1].Id, 'Expected pjf1 record to be active');
            
            // Act : checked includeDeactivatedPJF flag to include the deactivated pjfs too
            ext.includeDeactivatedPJF = true;
            
            // get active and deactivated pjfs
            activePJFController = ext.getPersonnelJobFunctions();
            pjfs = activePJFController.getRecords();

            // Assert: There will be 2 pjfs
            System.assert(pjfs.size() == 2, 'Expected total 2 pjf but got ' + pjfs.size());
            System.assert(pjfs[0].Id  == pjfList[0].Id && pjfs[0].get('Active__c') == false, 'Expected pjf1 to be deactivated');
            System.assert(pjfs[1].Id  == pjfList[1].Id && pjfs[1].get('Active__c') == false && pjfs[1].get('Deactivation_Date__c') == null, 'pjf2 should not be deactivated');
        }
    }
    
    /**
     * Given : Job Function and Two requirement (req1 and req2) with two Controlled Document 
     * When : getRequirements() method of extension class is called
     * Then : It should return 2 active requirements
     *
     * When : req2 is deactivated
     * Then : It will return only 1 active requirement
     *
     * When : The includeDeactivatedRequirement flag of extension is checked
     * Then : It will return 2 requirements i.e. one is active and another is deactived
     */ 
    public static testmethod void ensureCorrectNumberofRequirementRecordsAreReturnedByExtension() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            SQX_DB sqxDb = new SQX_DB();

            // Arrange : Get Job Function
            SQX_Job_Function__c jf1 = [SELECT Id from SQX_Job_Function__c WHERE Name = 'job function 1'];
            
            // Arrange: Create 2 Controlled Documents
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.save();
            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Test_Controlled_Document document2 = new SQX_Test_Controlled_Document();
            document2.save();
            document2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            //Arrange : Add Requirements with Training Program Step 1 and 2 
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = true);

            SQX_Requirement__c req2 = new SQX_Requirement__c(SQX_Controlled_Document__c = document2.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 2,
                                                             Active__c = true);

            sqxDb.op_insert(new List<SObject>{req1, req2}, new List<SObjectField>{});

            // Act: Get the requirement record by calling getRequirements() method of extension.
            ApexPages.StandardController sc = new ApexPages.standardController(jf1);
            SQX_Extension_Job_Function ext = new SQX_Extension_Job_Function(sc);
            ApexPages.StandardSetController activeRequirementController = ext.getRequirements();
            
            // Assert: There must be 2 requirements
            List<SObject> requirements = activeRequirementController.getRecords();
            System.assert(requirements.size() == 2, 'Expected 2 requirements but got ' + requirements.size());
            
            // Act: Deactivate the requirement (req2)
            req2.Active__c = false;
            req2.Deactivation_Date__c = System.today();
            sqxDb.op_update(new List<SObject>{req2}, new List<Schema.SObjectField> {
                                            SQX_Requirement__c.Deactivation_Date__c,
                                            SQX_Requirement__c.Active__c
                                            });

            // Act: Get Active Requirement
            ext = new SQX_Extension_Job_Function(sc);
            activeRequirementController = ext.getRequirements();
            requirements = activeRequirementController.getRecords();

            // Assert: Extension must return only 1 requirement
            System.assert(requirements.size() == 1, 'Expected 1 requirement but got ' + requirements.size());
            System.assert(requirements[0].Id == req1.Id, 'Expected requirement record to be active');
            
            // Act: Check includeDeactivatedRequirement flag to include the deactivated requirements too
            ext.includeDeactivatedRequirement = true;
            
            // Act: Get Actived and Deactivated requirements
            activeRequirementController = ext.getRequirements();
            requirements = activeRequirementController.getRecords();

            // Assert: There must be 2 requirements
            System.assert(requirements.size() == 2, 'Expected total of 2 requirements but got ' + requirements.size());
            System.assert(requirements[0].Id  == req1.Id && req1.get('Active__c') == true && requirements[0].get('Deactivation_Date__c') == null, 'Req1 should not be deactivated');
            System.assert(requirements[1].Id  == req2.Id && req2.get('Active__c') == false, 'Expected Req1 to be deactivated');
        }
    }
}