/**
 * Used by document preview component to show hide/perform various actions
 * TODO: Add unit test for the class.
 */
public with sharing class SQX_Document_Preview_Controller {

    /**
     * returns a map where controlled document field api_name is set as key and field labels as value
     */
    public static Map<String, String> getFieldApiNameAndLabels(){
        Map<String, String> fieldApiAndLabel = new Map<String, String>();
        Map<String, Schema.SObjectField> fieldMap = SQX_Controlled_Document__c.SObjectType.getDescribe().fields.getMap();
        
        for(String fieldName : fieldMap.keySet()) {
            fieldApiAndLabel.put(fieldName, fieldMap.get(fieldName).getDescribe().getLabel());
        }
        return fieldApiAndLabel;
    }
    /**
     * returns details of content such as libraries
     * @param  docId docId the doc to check
     * @return       detail of the document
     */
    @AuraEnabled
    public static PreviewResult getPreviewContent(String docId) {
        PreviewResult result = new PreviewResult();
        SQX_Controlled_Document__c doc = [SELECT Id, SQX_Checked_Out_By__r.Name, Content_Reference__c, Draft_Vault__c, Distribution_Vault__c,
                                                Release_Vault__c,Secondary_Content__c,Document_Status__c 
                                        FROM SQX_Controlled_Document__c WHERE Id = : docId];
        Map<Id, ContentWorkSpace> workspaces = new Map<Id, ContentWorkspace>([SELECT Id, Name FROM ContentWorkspace WHERE Id IN : new String[] {doc.Draft_Vault__c, doc.Distribution_Vault__c, doc.Release_Vault__c}]);
        result.DraftLibrary = String.isBlank(doc.Draft_Vault__c) ? '' : workspaces.containsKey(doc.Draft_Vault__c) ? workspaces.get(doc.Draft_Vault__c).Name : ('Not Accessible - (' + doc.Draft_Vault__c + ')');
        result.ReleaseLibrary = String.isBlank(doc.Release_Vault__c) ? '' : workspaces.containsKey(doc.Release_Vault__c) ? workspaces.get(doc.Release_Vault__c).Name : ('Not Accessible - (' + doc.Release_Vault__c + ')');
        result.DistributionLibrary = String.isBlank(doc.Distribution_Vault__c) ? '' : workspaces.containsKey(doc.Distribution_Vault__c) ? workspaces.get(doc.Draft_Vault__c).Name : ('Not Accessible - (' + doc.Distribution_Vault__c + ')');
        List<ContentDocument> contentList =[SELECT Id,ParentID FROM ContentDocument WHERE Id = : doc.Content_Reference__c];
        result.isPrimaryContentPrivate = contentList.size() == 1 && contentList.get(0).ParentId == null;
        result.CheckedOutBy = doc.SQX_Checked_Out_By__r == null ? '' : doc.SQX_Checked_Out_By__r.Name;
        result.fieldApiNameAndLabels = getFieldApiNameAndLabels();
        result.isInApproval = [SELECT Id FROM ProcessInstance WHERE TargetObjectId = :docId AND Status = 'Pending'].size() == 1;
        compliancequest__SQX_Custom_Settings_Public__c customSetting = compliancequest__SQX_Custom_Settings_Public__c.getOrgDefaults();
        result.draftAndDisabled= (doc.Secondary_Content__c !=null || customSetting.compliancequest__Controlled_Document_Secondary_Format__c !=Label.CQ_UI_Disabled) && doc.Document_Status__c==Label.CQ_UI_Flow_Draft;
        result.secondaryFormatSettingOptions = getSecondaryFormatSettings();
        result.userHasSupervisoryPermission = SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR);
        return result;
    }

    /**
     * synchronizes and moves a document to public library
     * @param  documentId  the document to move
     * @param  moveLibrary true/false to move the library
     */
    @AuraEnabled
    public static SQX_Action_Response syncContentAndMoveToPublicLibrary(String documentId, Boolean moveLibrary) {
        try {
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = documentId);
            SQX_Extension_Controlled_Document document = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));
            document.doc = doc;
            SQX_Action_Response resp = document.syncContentAndMoveToPublicLibrary(moveLibrary);
            return resp;
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
        }
    }

    /**
     * This method is used to synchronize the content
     * @param  documentId the document that is to be submitted
     */
    @AuraEnabled
    public static SQX_Action_Response syncDoc(String documentId) {
        try {
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = documentId);
            SQX_Extension_Controlled_Document document = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));
            document.doc = doc;
            SQX_Action_Response resp = document.syncDocForSubmissionAPI();
            return resp;
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
        }
    }

    /**
     * Submits a document for approval
     * @param  documentId the document that is to be submitted
     */
    @AuraEnabled
    public static SQX_Action_Response submitForapprovalProcess(String documentId) {
        try {
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = documentId);
            SQX_Extension_Controlled_Document document = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));
            document.doc = doc;
            SQX_Action_Response resp = document.submitDocForApprovalAPI();
            return resp;
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * update the secondary content id with uploaded files
     * @param  secondaryContentId file which is uploaded when its secondary format is manual 
     * @param recordId record details id related to controlled document
     */
    @AuraEnabled
    public static String addControlledDocumentSecondaryContentReference(String secondaryContentId,String recordId) {
        SQX_Controlled_Document__c document= new SQX_Controlled_Document__c();
        try {

            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = recordId);
            document = [SELECT Id, OwnerId, Is_Modifiable__c, Content_Reference__c, Secondary_Content_Reference__c, Draft_Vault__c, Description__c, Title__c, Synchronization_Status__c, Auto_Submit_For_Approval__c, Secondary_Content__c, Approval_Status__c, Secondary_Format_Setting__c FROM SQX_Controlled_Document__c  WHERE Id = : doc.Id];
            if(document.Secondary_Content_Reference__c==null){
            document.Secondary_Content_Reference__c=secondaryContentId;
            document.Synchronization_Status__c=SQX_Controlled_Document.SYNC_STATUS_IN_SYNC;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c> { document },new List<SObjectField> { SQX_Controlled_Document__c.Secondary_Content_Reference__c});
            return null;
            }
 
        }catch(Exception ex) {
           exceptionHandler(ex.getMessage() + ex.getStackTraceString());
        }
        
        if(document.Secondary_Content_Reference__c!=null){
            exceptionHandler(Label.CQ_UI_Secondary_Content_Error_Message);
            
        }
        return null;
    }

    /**
      * exceptionHandler method throw AuraHandledException 
      * @param  errorMsg is a custom error message
      */
     public static void exceptionHandler(String errorMsg) {
          //Note:constructor for the AuraHandledException only sets the string argument to be the message sent to the client-end, 
          //while calling getMessage() on the exception through apex returns a different string that the constructor defaults to the value "Script-thrown Exception". 
          //For testing purposes,adding the message to the exception with setMessage() so that it can be accessed through apex:
          AuraHandledException e = new AuraHandledException(errorMsg);
          e.setMessage(errorMsg);
          throw e;
     }

    /**
    * updateSecondaryFormat update the secondary format when record is in draft state
    * @param  secondaryFormat format type that is to be set for controlled document
    * @param  recordId  record details id related to controlled document
    * @param  secondaryFormatSetting format setting that is to be set for ctrl doc
    */
    @AuraEnabled
    public static String updateSecondaryFormat(String secondaryFormat,String recordId, String secondaryFormatSetting){
     
        try{
            compliancequest__SQX_Controlled_Document__c controlDoc=[SELECT Id, compliancequest__Secondary_Content_Reference__c, compliancequest__Secondary_Content__c, compliancequest__Secondary_Format_Setting__c FROM compliancequest__SQX_Controlled_Document__c where Id=:recordId];
            controlDoc.compliancequest__Secondary_Content__c = secondaryFormat == '' ? controlDoc.compliancequest__Secondary_Content__c : secondaryFormat;
            controlDoc.compliancequest__Secondary_Format_Setting__c=secondaryFormatSetting == '' ? controlDoc.compliancequest__Secondary_Format_Setting__c : secondaryFormatSetting;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c> { controlDoc },new List<SObjectField> { SQX_Controlled_Document__c.compliancequest__Secondary_Content__c, SQX_Controlled_Document__c.compliancequest__Secondary_Format_Setting__c });
   
        }catch(Exception ex){
           exceptionHandler(ex.getMessage() + ex.getStackTraceString()); 
        }
         return null;
    }

    /**
    * updateScormValue update the scorm contact
    * @param  scormVal set course scorm contact value
    * @param  recordId  record details id related to controlled document
    */
    @AuraEnabled
    public static String updateScormValue(Boolean scormVal,String recordId){
     
        try{
            compliancequest__SQX_Controlled_Document__c controlDoc=[SELECT Id, compliancequest__Is_Scorm_Content__c FROM compliancequest__SQX_Controlled_Document__c where Id=:recordId];
            controlDoc.compliancequest__Is_Scorm_Content__c = scormVal;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c> { controlDoc },new List<SObjectField> { SQX_Controlled_Document__c.compliancequest__Is_Scorm_Content__c });
   
        }catch(Exception ex){
           exceptionHandler(ex.getMessage() + ex.getStackTraceString()); 
        }
         return null;
    }

    /**
     * Submits a document for approval
     * @param  documentId the document that is to be submitted
     */
    @AuraEnabled
    public static void submitDocForApproval(String documentId) {
        try {
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = documentId);
            SQX_Extension_Controlled_Document document = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));
            document.doc = doc;
            document.submitDocForApproval();
        }
        catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
        }
    }
    
    /**
     * This method returns list of all the records under Secondary Format Setting
     */
    private static List<SelectOption> getSecondaryFormatSettings(){
        List<SelectOption> options = new List<SelectOption>();
        List<Secondary_Format_Setting__mdt> settings = [SELECT Id, MasterLabel, DeveloperName FROM Secondary_Format_Setting__mdt];
        
        // adding all the records to select option
        for ( Secondary_Format_Setting__mdt setting : settings ){
            options.add( new SelectOption(setting.DeveloperName, setting.MasterLabel) );
        }
        return options;
    }

    /**
     * Details of the document for preview component
     */
    public class PreviewResult {
        @AuraEnabled
        public String DraftLibrary {get; set;}

        @AuraEnabled
        public String ReleaseLibrary {get; set;}

        @AuraEnabled
        public String DistributionLibrary {get; set;}

        @AuraEnabled
        public String CheckedOutBy {get; set;}

        @AuraEnabled
        public Boolean isPrimaryContentPrivate {get; set;}

        @AuraEnabled
        public Boolean isInApproval {get; set;}
        
        @AuraEnabled
        public Map<String, String> fieldApiNameAndLabels {get;set;}

        @AuraEnabled
        public Boolean draftAndDisabled {get;set;}

        @AuraEnabled
        public List<SelectOption> secondaryFormatSettingOptions {get; set;}
        
        @AuraEnabled
        public Boolean userHasSupervisoryPermission{get; set;}
    }
    
    /**
     * Details of the secondary format settings
     */
    public class SelectOption {
        public SelectOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
        
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
    }
}