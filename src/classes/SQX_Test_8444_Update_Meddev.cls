/**
 * This test class is to check if follow up meddev has type of report set
 */
@isTest
public class SQX_Test_8444_Update_Meddev {
    
    @testSetup
    public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * GIVEN : Follow up Regulatory Report.
     * WHEN : Follow up Meddev is created.
     * THEN : Type of report is set as follow up
     */
    public static testmethod void givenFollowUpRegulatoryReport_WhenFollowUpMeddevIsCreated_ThenTypeOfReportIsSet() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();

            // ARRANGE : Create Regulatory Report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c(
                SQX_User__c = standardUser.Id,
                SQX_Complaint__c = complaint.complaint.Id,
                Due_Date__c = Date.today(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                Status__c=SQX_Regulatory_Report.STATUS_PENDING
            );
            insert regulatoryReport;

            // ARRANGE : Create Meddev
            SQX_Meddev__c meddev = new SQX_Meddev__c(
                SQX_Regulatory_Report__c = regulatoryReport.Id
            );
            insert meddev;

            // ARRANGE : Update report id in reg report
            regulatoryReport.Report_Id__c = meddev.Id;
            update regulatoryReport;

            // ARRANGE : Create Report Submission
            SQX_Submission_History__c regSubmission = new SQX_Submission_History__c();
            regSubmission.Status__c='Complete';
            regSubmission.SQX_Complaint__c=complaint.complaint.Id;
            regSubmission.Submitted_By__c='Submitter';
            regSubmission.SQX_Regulatory_Report__c = regulatoryReport.Id;
            regSubmission.SQX_Meddev__c = meddev.Id;
            insert regSubmission;

            Test.startTest();

            // ACT : Create a followup report
            List<Id> ids = SQX_Create_Followup.createFollowup(new List<Id>{regulatoryReport.Id});

            // ASSERT : Report Type should be set as follow up
            SQX_Meddev__c newMeddev = [SELECT Id, Admin_Info_ReportType__c  FROM SQX_Meddev__c WHERE Id =: Ids[0]];
            System.assertEquals('Follow Up', newMeddev.Admin_Info_ReportType__c);

            Test.stopTest();

        }
        
    }

}