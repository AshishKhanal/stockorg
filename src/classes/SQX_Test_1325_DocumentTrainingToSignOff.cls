/**
* Unit tests for dashboard component extension for current user's document training sign off list.
*
* @author Dibya Shrestha
* @date 2015/08/03
* 
*/
@IsTest
public class SQX_Test_1325_DocumentTrainingToSignOff {
    
    static Boolean runAllTests = true,
                   run_givenViewDocumentTrainingSignOffList = false;
    
    private static SQX_Document_Training__c doUserSignOff(SQX_Document_Training__c dt, User user, String newStatus) {
        dt.SQX_User_Signed_Off_By__c = user.Id;
        dt.User_Signature__c = user.Name;
        dt.User_SignOff_Date__c = DateTime.now();
        dt.Status__c = newStatus;
        
        return dt;
    }
    
    private static SQX_Document_Training__c doTrainerSignOff(SQX_Document_Training__c dt, User user) {
        dt.SQX_Training_Approved_By__c = user.Id;
        dt.Trainer_Signature__c = user.Name;
        dt.Trainer_SignOff_Date__c = DateTime.now();
        dt.Status__c = SQX_Document_Training.STATUS_COMPLETE;
        
        return dt;
    }
    
    /**
    * tests for sign off list with pending / in approval / complete status document trainings
    */
    public static testmethod void givenViewDocumentTrainingSignOffList() {
        if (!runAllTests && !run_givenViewDocumentTrainingSignOffList) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User currentStandardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(currentStandardUser) {
            // add required controlled document
            SQX_Test_Controlled_Document testDoc1 = new SQX_Test_Controlled_Document();
            testDoc1.save();
            SQX_Test_Controlled_Document testDoc2 = new SQX_Test_Controlled_Document();
            testDoc2.save();

            testDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            testDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required pending document training for current user as trainee
            SQX_Document_Training__c dt1 = new SQX_Document_Training__c(
                SQX_User__c = currentStandardUser.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.insert(dt1, false);
            
            // add required pending document training for current user as trainee
            SQX_Document_Training__c dt2 = new SQX_Document_Training__c(
                SQX_User__c = currentStandardUser.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = testDoc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.insert(dt2, false);
            
            // add required document training for current user as trainer to approve
            SQX_Document_Training__c dt3 = new SQX_Document_Training__c(
                SQX_User__c = standardUser.Id,
                SQX_Trainer__c = currentStandardUser.Id,
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.insert(dt3, false);
            // do user sign off for dt3 for approval step
            dt3 = doUserSignOff(dt3, standardUser, SQX_DOCUMENT_TRAINING.STATUS_TRAINER_APPROVAL_PENDING);
            Database.update(dt3, false);
            
            // add required pending document training without current user as trainee / trainer
            SQX_Document_Training__c dt4 = new SQX_Document_Training__c(
                SQX_User__c = standardUser.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = testDoc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.insert(dt4, false);
            
            // Act: View page SQX_Document_Training_To_Sign_Off for current user
            Test.setCurrentPage(Page.SQX_Document_Training_To_Sign_Off);
            ApexPages.StandardController controller = new ApexPages.StandardController(new SQX_Document_Training__c());
            SQX_Extension_DTN_To_Sign_Off ext = new SQX_Extension_DTN_To_Sign_Off(controller);
            
            System.assert(ext.getSignOffList().size() == 3,
                'Expected current VF page to have list to sign off with 3 items.');
            
            System.assert(ext.getRequireUserSignOff(),
                'Expected current VF page to have user sign off list for current user.');
            
            System.assert(ext.getRequireTrainerSignOff(),
                'Expected current VF page to have trainer sign off list for current user.');
            
            
            // do user sign off for dt1 to complete
            dt1 = doUserSignOff(dt1, currentStandardUser, SQX_DOCUMENT_TRAINING.STATUS_COMPLETE);
            Database.update(dt1, false);
            
            // do user sign off for dt2 for approval step
            dt2 = doUserSignOff(dt2, currentStandardUser, SQX_DOCUMENT_TRAINING.STATUS_TRAINER_APPROVAL_PENDING);
            Database.update(dt2, false);
            
            // do trainer sign off for dt3 to complete
            dt3 = doTrainerSignOff(dt3, currentStandardUser);
            Database.update(dt3, false);
            
            // Act: View page SQX_Document_Training_To_Sign_Off for current user after sign off complete
            Test.setCurrentPage(Page.SQX_Document_Training_To_Sign_Off);
            controller = new ApexPages.StandardController(new SQX_Document_Training__c());
            ext = new SQX_Extension_DTN_To_Sign_Off(controller);
            
            System.assert(ext.getSignOffList().size() == 0,
                'Expected current VF page not to have list to sign off for document trainings for current user.');
            
            System.assert(ext.getRequireUserSignOff() == false,
                'Expected current VF page not to have user/trainee sign off list for current user.');
            
            System.assert(ext.getRequireTrainerSignOff() == false,
                'Expected current VF page not to have trainer sign off list for current user.');
        }
    }
}