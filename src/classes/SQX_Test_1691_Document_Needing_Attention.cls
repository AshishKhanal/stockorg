/*
 * test for in Document that are needed to get attention on home page component
 * author: Shailesh Maharjan
 * date: 2015/11/16
 * story: SQX-1691
 **/
@isTest
public class SQX_Test_1691_Document_Needing_Attention{
    static Boolean runAllTests = true,
                   run_givenCheckStatus = false;

    public testmethod static void givenCheckStatus(){
        if (!runAllTests && !run_givenCheckStatus) {
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
          
        System.runas(standardUser) {
            //Arrange :
                //Creating New Controlled Document with different Status
            SQX_Test_Controlled_Document docDraft = new SQX_Test_Controlled_Document().save();
            
            SQX_Test_Controlled_Document docApproved = new SQX_Test_Controlled_Document().save();
            docApproved.doc.Auto_Release__c = false;
            docApproved.doc.Document_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
            docApproved.save();
            
            SQX_Test_Controlled_Document docCurrent = new SQX_Test_Controlled_Document().save();
                docCurrent.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
                
            SQX_Test_Controlled_Document docObsolete = new SQX_Test_Controlled_Document().save();
                docObsolete.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // ACT: get required list as per set on controller
            SQX_Document_Needing_Attention controller = new SQX_Document_Needing_Attention();
            List<SQX_Controlled_Document__c> docList = controller.getDocuments();
            
            Set<Id> docIds = new Set<Id>();
            for (SQX_Controlled_Document__c d : docList) {
                docIds.add(d.Id);
            }

            //Assert: Comparing size of the document 
            System.assertEquals(2, docList.size(),
                'Expected two documents to be listed.');
            
            //Assert: Controlled Document list status Compared
            System.assert(docIds.contains(docDraft.doc.Id),
                'Expected Draft Status document to be listed.');
            System.assert(docIds.contains(docApproved.doc.Id),
                'Expected Approved Status document to be listed.');
        }
    }

}