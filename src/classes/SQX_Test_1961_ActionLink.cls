/**
* This test checks for the following features that were developed as a part of SQX-1961
* a. The URL based parameter passing to set the change order for new doc works
* b. The URL based parameter passing to set the change order for revise doc works
* c. This test ensures that ownership of Implementation is changed to the assignee, when it is changed to action type
*/
@IsTest
public class SQX_Test_1961_ActionLink {

    /**
    * When creating a new doc from change order's New Document action. The id of the implementation is passed through the URL.
    * This test ensures that when the id is passed from the URL. The newly created doc has the related Change Order and 
    * the implementation's new document gets updated
    */
    static testmethod void givenAnImplnInNewDoc_ImplnIsUpdatedDocHasChangeOrder(){
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);
        
        System.runAs(standardUser){
            // Arrange: Create a change order and add an action of new doc type
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            
            SQX_Implementation__c implementation = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, standardUser, 'Hello World', Date.today());
            implementation.Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            insert implementation;
            
            
            // Act: Create a new document using controlled documents extension
            PageReference pageRef = Page.SQX_Controlled_Document_Editor;
            pageRef.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementation.Id);
            Test.setCurrentPage(pageRef);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(new SQX_Test_Controlled_Document().doc));
            extension.hasLargeFile = true;
            PageReference newDocRef = extension.Create();
            
            // Assert: Ensure that the newly created doc has change order set and the implementation is updated
            SQX_Implementation__c fetchedImplementation = [SELECT Id, SQX_Controlled_Document_New__c FROM SQX_Implementation__c WHERE Id = : implementation.Id];
            System.assert(fetchedImplementation.SQX_Controlled_Document_New__c != null, 'Expected new controlled document to have a link with the doc but found ' + fetchedImplementation);
            System.assertEquals(changeOrder.changeOrder.Id,
                                [SELECT SQX_Change_Order__c FROM SQX_Controlled_Document__c WHERE Id = : fetchedImplementation.SQX_Controlled_Document_New__c].SQX_Change_Order__c,
                               'Expected the controlled document to have change order set but found something else :) ');
            
            
                
        }
    }
    
    /**
    * When revising an existing doc from change order's Revise Document action. The id of the implementation is passed through the URL.
    * This test ensures that when the id is passed from the URL. The newly created doc has the related Change Order and 
    * the implementation's new document gets updated
    */
    static testmethod void givenAnImplnInReviseDoc_ImplnIsUpdatedDocHasChangeOrder(){
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);
        
        System.runAs(standardUser){
            // Arrange: Create a controlled doc that has to revised and Create a change order and add an action of new doc type.
            SQX_Test_Controlled_Document controlledDocToRevise = new SQX_Test_Controlled_Document().save(true);
            controlledDocToRevise.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            
            SQX_Implementation__c implementation = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, standardUser, 'Hello World', Date.today());
            implementation.Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            implementation.SQX_Controlled_Document__c = controlledDocToRevise.doc.Id;
            insert implementation;
            
            
            // Act: Create a new document using controlled documents extension
            PageReference pageRef = Page.SQX_Revise_Controlled_Doc;
            pageRef.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementation.Id);
            Test.setCurrentPage(pageRef);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(controlledDocToRevise.doc));
            PageReference newDocRef = extension.Revise();
            
            // Assert: Ensure that the newly created doc has change order set and the implementation is updated
            SQX_Implementation__c fetchedImplementation = [SELECT Id, SQX_Controlled_Document_New__c FROM SQX_Implementation__c WHERE Id = : implementation.Id];
            System.assert(fetchedImplementation.SQX_Controlled_Document_New__c != null, 'Expected new controlled document to have a link with the doc but found ' + fetchedImplementation);
            System.assertEquals(changeOrder.changeOrder.Id,
                                [SELECT SQX_Change_Order__c FROM SQX_Controlled_Document__c WHERE Id = : fetchedImplementation.SQX_Controlled_Document_New__c].SQX_Change_Order__c,
                               'Expected the controlled document to have change order set but found something else :) ');
            
            
                
        }
    }

    /**
    *  a. Given CO with Controlled Doc related Complete CQ Aware actions is released
    *    Then effective and expiration date of controlled docs are updated
    *     
    *  b. Given CO with Controlled Doc related Skipped CQ Aware actions is released
    *     Then effective and expiration date of controlled docs aren't updated
    * 
    */
    static testmethod void givenCOIsReleased_DatesAreSyncedInDoc(){
        
        // Arrange: Create a change order in complete state with 
        //          a. A Controlled Doc that has been revised
        //          b. A Controlled Doc that has been created
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runAs(standardUser1){
            SQX_Test_Controlled_Document docToRevise = new SQX_Test_Controlled_Document().save(true),
                                         docToDoNothing = new SQX_Test_Controlled_Document().save();

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            docToRevise.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            docToRevise.save();
            
            SQX_Test_Change_Order changeOrder =  new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            
            SQX_Implementation__c [] implementations = new SQX_Implementation__c[]{
                    
                    changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'We have got to do something', null),
                    changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'Yes, we have got to do something', null),
                    changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'Yes, we have got to do something', null)
            };
                            
            implementations[0].Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            implementations[0].SQX_Controlled_Document__c = docToRevise.doc.Id;
            
            implementations[1].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            
            implementations[2].Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            implementations[2].SQX_Controlled_Document__c = docToDoNothing.doc.Id;
            
            insert implementations;

            // Approve the change and complete all the actions
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            changeOrder.save();
            
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED;
            changeOrder.setStatus(SQX_Change_Order.STATUS_OPEN);
            changeOrder.changeOrder.Record_Stage__c=SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrder.save();
            
            
            // Revise the doc to revise
            PageReference pageRef = Page.SQX_Revise_Controlled_Doc;
            pageRef.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementations[0].Id);
            Test.setCurrentPage(pageRef);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(docToRevise.doc));
            PageReference newDocRef = extension.Revise();
            SQX_Implementation__c fetchedImplementation = [SELECT Id, SQX_Controlled_Document_New__c FROM SQX_Implementation__c WHERE Id = : implementations[0].Id];
            SQX_Controlled_Document__c revisedDoc = new SQX_Controlled_Document__c(Id = fetchedImplementation.SQX_Controlled_Document_New__c,
                                                                                   Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE,
                                                                                  Effective_Date__c = Date.today().addDays(5));
            
            // Create a new doc
            pageRef = Page.SQX_Controlled_Document_Editor;
            pageRef.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, implementations[1].Id);
            Test.setCurrentPage(pageRef);
            extension = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(new SQX_Test_Controlled_Document().doc));
            extension.hasLargeFile = true;
            newDocRef = extension.Create();
            fetchedImplementation = [SELECT Id, SQX_Controlled_Document_New__c FROM SQX_Implementation__c WHERE Id = : implementations[1].Id];
            SQX_Controlled_Document__c newDoc = new SQX_Controlled_Document__c(Id = fetchedImplementation.SQX_Controlled_Document_New__c,
                                                                               Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE,
                                                                               Effective_Date__c = Date.today().addDays(5));
            
            
            Test.startTest();
            
            update new List<SObject> {revisedDoc, newDoc};
                
            //To skip the plan we have to adjust plan and submit for approval
            //Arrange :Adjust plan change order
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_ADJUST_PLAN;
            
            //Act: update change order to adjust the plan
            List<Database.SaveResult> successResults = new SQX_DB().op_update(new List<SQX_Change_Order__c>{changeOrder.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            //Assert:adjust plan successfully
            System.assertEquals(true,successResults[0].isSuccess(),'adjust plan co successfully');  
            System.assertEquals('RePlan',[SELECT Id, Status__c,Record_Stage__c,Approval_Status__c  FROM SQX_Change_Order__c
                                          WHERE Id = : changeOrder.Id].Approval_Status__c );
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            //Arrange: skipped the plan after adjust
            SQX_Implementation__c skipImpl= new SQX_Implementation__c(Id = implementations[2].Id, Status__c = SQX_Implementation.STATUS_SKIPPED);
            
            List<DataBase.SaveResult> skipPlanResult = new SQX_DB().op_update(new List<SQX_Implementation__c>{skipImpl}, new List<Schema.SObjectField>{});
            //Assert:complete successfully ans plan is skipped
            System.assertEquals(true,skipPlanResult[0].isSuccess(),'implementation can be skipped in adjust plan'); 
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Submit for approval change order
            SQX_Change_Order__c Co=[SELECT Id, Status__c,Record_Stage__c,Approval_Status__c,(SELECT Id, Status__c, Title__c FROM SQX_Implementations__r)
                                    FROM SQX_Change_Order__c
                                    WHERE Id = : changeOrder.Id];
            Co.Approval_Status__c= SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;

            List<DataBase.SaveResult> submitApprovalResult = new SQX_DB().op_update(new List<SQX_Change_Order__c>{ Co}, new List<Schema.SObjectField>{});
            //Assert: submitted change order successfully and approval status as plan approval
            System.assertEquals(true,submitApprovalResult[0].isSuccess(),'submit co successfully'); 
            System.assertEquals('Plan Approval',[SELECT Id, Status__c,Record_Stage__c,Approval_Status__c  FROM SQX_Change_Order__c
                                                    WHERE Id = : changeOrder.Id].Approval_Status__c );
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // Act : Approve the change order
            Co.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED;
            Co.Status__c=SQX_Change_Order.STATUS_OPEN;
            Co.Record_Stage__c =SQX_Change_Order.STATUS_IMPLEMENTATION;
            List<DataBase.SaveResult> approvedCo = new SQX_DB().op_update(new List<SQX_Change_Order__c>{Co}, new List<Schema.SObjectField>{});
            System.assertEquals(true,approvedCo[0].isSuccess(),'implementation approved'); 

            SQX_BulkifiedBase.clearAllProcessedEntities();
            // By now all the actions should have completed and change order should have completed
            System.assertEquals(SQX_Change_Order.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Status__c);
            System.assertEquals(SQX_Change_Order.STAGE_COMPLETE, [SELECT Record_Stage__c  FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Record_Stage__c	);
            // Act: Now release the change order with release date in future
            Date newReleaseDate =  Date.today().addDays(10);
            update new SQX_Change_Order__c(Id = changeOrder.Id ,Status__c = SQX_Change_Order.STATUS_COMPLETE,Record_Stage__c=SQX_Change_Order.STAGE_COMPLETE ,Effective_Date__c =  newReleaseDate);
            

            // Assert: Ensure that release and expiration date are synchronized
            Map<Id, SQX_Controlled_Document__c> controlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Effective_Date__c, Expiration_Date__c FROM SQX_Controlled_Document__c
                                                                  WHERE Id IN : new List<Id> { revisedDoc.Id, newDoc.Id, docToDoNothing.doc.Id }]);
            System.assertEquals(newReleaseDate, controlledDocs.get(revisedDoc.Id).Effective_Date__c, 'Expected revised docs release date to be set based on CO');
            System.assertEquals(newReleaseDate, controlledDocs.get(newDoc.Id).Effective_Date__c, 'Expected new docs release date to be set based on CO');
            
            System.assertEquals(null, controlledDocs.get(docToDoNothing.doc.Id).Expiration_Date__c, 'Expected obsolete docs expiration date to remain same.');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act: Set the expiration date
            Date newerReleaseDate =  Date.today().addDays(15);
            update new SQX_Change_Order__c(Id = changeOrder.Id, Effective_Date__c =  newerReleaseDate);
            
            // Assert: Ensure that the docs expiration have been updated.
            controlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Effective_Date__c, Expiration_Date__c FROM SQX_Controlled_Document__c
                                                                  WHERE Id IN : new List<Id> { revisedDoc.Id, newDoc.Id, docToDoNothing.doc.Id }]);
            System.assertEquals(newerReleaseDate, controlledDocs.get(revisedDoc.Id).Effective_Date__c, 'Expected revised docs release date to be set based on CO');
            System.assertEquals(newerReleaseDate, controlledDocs.get(newDoc.Id).Effective_Date__c, 'Expected new docs release date to be set based on CO');
            
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act: Now release the CO today
            update new SQX_Change_Order__c(Id = changeOrder.Id, Effective_Date__c =  Date.today());
            
            // Assert: All controlled docs are released.
            System.assertEquals(SQX_Change_Order.STATUS_CLOSED, [SELECT Status__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Status__c, 'Expected change order to be closed');
            controlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Document_Status__c, Effective_Date__c, Expiration_Date__c FROM SQX_Controlled_Document__c
                                                                  WHERE Id IN : new List<Id> { revisedDoc.Id, newDoc.Id, docToDoNothing.doc.Id }]);
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDocs.get(revisedDoc.Id).Document_Status__c, 'Expected revised docs to be released when CO is released');
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDocs.get(newDoc.Id).Document_Status__c, 'Expected new docs to be released when CO is released');
           
            //Arrange: add impact with job function in closed change order
            SQX_Job_Function__c jobFunction = new SQX_Job_Function__c( Name = 'job function' );
            SQX_Impact__c   impact = new SQX_Impact__c(
                        SQX_Change_Order__c = changeOrder.Id,
                        RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION),
                        Description_of_Impact__c = 'Impact Job funtion Test',
                        SQX_Job_Function__c = jobFunction.Id
            );
            //Act: try to add the impact 
            List<SQX_Impact__c> impactList2 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result2 = new SQX_DB().continueOnError().op_insert(impactList2, new List<Schema.SObjectField>{});
            //Assert:Validation error message should display : Parent record is locked.
            System.assert(SQX_Utilities.checkErrorMessage(result2[0].getErrors(), 'Parent record is locked.'), 'Parent record is locked. when user try to add impact  in closed change order');

            Test.stopTest();

                        
        }

    }



    /**
    * 
    *  a. Given CO with Released/Expired Controlled Doc related Complete CQ Aware actions is released
    *     Then effective and expiration date of controlled docs aren't updated
    */
    static testmethod void givenCOIsReleasedWithObsolete_DatesAreSyncedInDoc(){
        
        // Arrange: Create a change order in complete state with 
        //          a. A Controlled Doc that has been obsoleted
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runAs(standardUser1){
            SQX_Test_Controlled_Document docToObsolete = new SQX_Test_Controlled_Document().save();
            
            SQX_Test_Change_Order changeOrder =  new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            
            SQX_Implementation__c [] implementations = new SQX_Implementation__c[]{
                    
                    changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'We have got to do something', null)
            };
                
            implementations[0].Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            implementations[0].SQX_Controlled_Document__c = docToObsolete.doc.Id;
            
            insert implementations;

            // Approve the change and complete all the actions
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            changeOrder.save();
            
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED;
            changeOrder.setStatus(SQX_Change_Order.STATUS_OPEN);
            changeOrder.changeOrder.Record_Stage__c=SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrder.save();
            
            // Obsolete the doc to obsolete
            docToObsolete.doc.Expiration_Date__c = Date.today().addDays(5);
            docToObsolete.setStatus(SQX_Controlled_Document.STATUS_PRE_EXPIRE).save();
            
            
            // By now all the actions should have completed and change order should have completed
            System.assertEquals(SQX_Change_Order.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Status__c);
            System.assertEquals(SQX_Change_Order.STAGE_COMPLETE, [SELECT Record_Stage__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Record_Stage__c	);
            
            //Arrange: add impact with job function in complete change order
            SQX_Job_Function__c jobFunction = new SQX_Job_Function__c( Name = 'job function' );
            SQX_Impact__c   impact = new SQX_Impact__c(
                        SQX_Change_Order__c = changeOrder.Id,
                        RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION),
                        Description_of_Impact__c = 'Impact Job funtion Test',
                        SQX_Job_Function__c = jobFunction.Id
            );
            //Act: try to add the impact 
            List<SQX_Impact__c> impactList2 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result2 = new SQX_DB().continueOnError().op_insert(impactList2, new List<Schema.SObjectField>{});
            //Assert:Validation error message should display : Parent record is locked.
            System.assert(SQX_Utilities.checkErrorMessage(result2[0].getErrors(), 'Parent record is locked.'), 'Parent record is locked. when user try to add impact  in complete change order');
            
            // Act: Now release the change order with release date in future
            Date newReleaseDate =  Date.today().addDays(10);
            update new SQX_Change_Order__c(Id = changeOrder.Id ,Status__c = SQX_Change_Order.STATUS_COMPLETE,Record_Stage__c =SQX_Change_Order.STAGE_COMPLETE, Effective_Date__c =  newReleaseDate);
            
            // Assert: Ensure that release and expiration date are synchronized
            Map<Id, SQX_Controlled_Document__c> controlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Effective_Date__c, Expiration_Date__c FROM SQX_Controlled_Document__c
                                                                  WHERE Id IN : new List<Id> {  docToObsolete.doc.Id }]);
            System.assertEquals(newReleaseDate, controlledDocs.get(docToObsolete.doc.Id).Expiration_Date__c, 'Expected obsoleted docs expiration date to be set based on CO');
            
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            
            
            
            // Act: Now obsolete a document
            docToObsolete.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            Date newerReleaseDate =  Date.today().addDays(15);
            update new SQX_Change_Order__c(Id = changeOrder.Id, Effective_Date__c =  newerReleaseDate);
            
            // Assert: Ensure that the obsolete docs expiration hasn't been updated but others have been.
            controlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Effective_Date__c, Expiration_Date__c FROM SQX_Controlled_Document__c
                                                                  WHERE Id IN : new List<Id> { docToObsolete.doc.Id }]);

            System.assertNotEquals(newerReleaseDate, controlledDocs.get(docToObsolete.doc.Id).Expiration_Date__c, 'Expected obsoleted docs expiration date to be set based on CO');
            
                        
        }

    }

    /**
    * This test ensures the following:
    * a. When a Change (SQX_Implementation__c) for a change order is in plan, no ownership transfer is performed.
    * b. When a Change is in action, ownership is transferred to the Assignee
    */
    static testmethod void givenActionOwnershipWorks_Correctly(){
        
        // Arrange: Create two users and a change order, which we will use to check for ownership change in action.
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
             

        System.runAs(standardUser1){
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            
            // Act: Add a planned change with assignee set as standard user 2, another with assignee set as standard user 1 and last one unassigned
            SQX_Implementation__c [] changes = new SQX_Implementation__c [] {
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'Hello', Date.today()),
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser2, 'Hello', Date.today()),
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, null, 'Hello', Date.today())
            };
                
            insert changes;
                
            // Assert: Ensure that the owner of the changes are still standardUser1 i.e. no ownership change has been done.
            for(SQX_Implementation__c implementation : [SELECT Id, OwnerId FROM SQX_Implementation__c WHERE Id IN :changes]){
                System.assertEquals(standardUser1.Id, implementation.OwnerId, 'Expected the owner of implmentation to not change but found ' + implementation);
            }
            
            
            // Act: Now change the plans to actions
            changeOrder.setStatus(SQX_Change_Order.STATUS_OPEN);
            changeOrder.changeOrder.Record_Stage__c=SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrder.save();
            Id actionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, SQX_Implementation.RECORD_TYPE_ACTION);
            update new SQX_Implementation__c[] {
                new SQX_Implementation__c(Id = changes[0].Id, RecordTypeId = actionRecordTypeId),
                new SQX_Implementation__c(Id = changes[1].Id, RecordTypeId = actionRecordTypeId),
                new SQX_Implementation__c(Id = changes[2].Id, RecordTypeId = actionRecordTypeId)
            };
            
            // Assert: Ensure that ownerhip has been set properly
            Map<Id, SQX_Implementation__c> implementations = new Map<Id, SQX_Implementation__c>([SELECT Id, OwnerId FROM SQX_Implementation__c WHERE Id IN :changes]);
            System.assertEquals(standardUser1.Id, implementations.get(changes[0].Id).OwnerId,
                                'Expected the owner to remain same since it is assigned to standard user 1 but found ' + implementations.get(changes[0].Id));
            System.assertEquals(standardUser2.Id, implementations.get(changes[1].Id).OwnerId,
                                'Expected the owner to change since it is assigned to standard user 2 but found ' + implementations.get(changes[1].Id));
            System.assertEquals(standardUser1.Id, implementations.get(changes[2].Id).OwnerId,
                                'Expected the owner to remain same since it is assigned to no one but found ' + implementations.get(changes[2].Id));
            
            
            // Act: Add three new actions one assigned to std user1, other to std user2 and another unassigned
            changes = new SQX_Implementation__c [] {
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, standardUser1, 'Hello', Date.today()),
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, standardUser2, 'Hello', Date.today()),
                changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, null, 'Hello', Date.today())
            };
                
            insert changes;
            
            // Assert: Ensure that ownership was correctly set
            implementations = new Map<Id, SQX_Implementation__c>([SELECT Id, OwnerId FROM SQX_Implementation__c WHERE Id IN :changes]);
            System.assertEquals(standardUser1.Id, implementations.get(changes[0].Id).OwnerId,
                                'Expected the owner to remain same since it is assigned to standard user 1 but found ' + implementations.get(changes[0].Id));
            System.assertEquals(standardUser2.Id, implementations.get(changes[1].Id).OwnerId,
                                'Expected the owner to change since it is assigned to standard user 2 but found ' + implementations.get(changes[1].Id));
            System.assertEquals(standardUser1.Id, implementations.get(changes[2].Id).OwnerId,
                                'Expected the owner to remain same since it is assigned to no one but found ' + implementations.get(changes[2].Id));
            
        }
    }
}