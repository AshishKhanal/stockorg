/**
* Unit tests for re-evaluating document trainings for activated/deactivated job functions
*/
@IsTest
public class SQX_Test_1302_DTN_JobFunctionActivation {
    // removed tests for obsolete object SQX_Document_Training__c
    // TODO: remove this file while cleaning up
}