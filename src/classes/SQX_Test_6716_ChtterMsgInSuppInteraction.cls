/*
* Unit test to check if FeedItem is posted on chatter or not under supplier interaction record
* when record is rejected and closed.
*/
@isTest
public class SQX_Test_6716_ChtterMsgInSuppInteraction {
    final static String chatterMsg = 'SI Record has been closed and rejected.';
    final static String closeAction = 'Closing the record';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {  adminUser, standardUser, assigneeUser});
        
    }
    
    /*
    * Given: A Supplier Interaction Record
    * When: Any Supplier Interaction record is rejected and closed
    * Then: a chatter notification is sent with closure_comment as the body of the message.
    *       and closure_comment is copied in comment_long of record activity
    */
    @isTest
    public static void givenSIRecord_WhenRecordIsClosedRejected_ChatterMsgIsSent() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser, adminUser, assigneeUser});
        String description = '';
        SQX_Test_Supplier_Interaction siRecord;
        System.runAs(adminUser) {
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, SQX_Supplier_Common_Values.TASK_TYPE_TASK, assigneeUser, 1);
        }
        
        System.runAs(standardUser) {
            //Act: Supplier Interaction Record is initiated and ownership taken
            siRecord = new SQX_Test_Supplier_Interaction().save();
            siRecord.submit();
            siRecord.initiate();
            
            //Assert: Make sure that Interaction record is in 'Inprogress' Stage
            System.assertEquals(SQX_Supplier_Common_Values.STAGE_IN_PROGRESS, [Select Record_Stage__c from sqx_supplier_interaction__c where id = :siRecord.si.id].Record_Stage__c);
        }
        
        System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: siRecord.si.Id];
            Task sfTask = sfTasks.get(0);
            
            //Act: Complete the task
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            
            SQX_Supplier_Interaction__c suppRecord = [Select id, record_stage__c, status__c from sqx_supplier_interaction__c];
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, suppRecord.status__c, 'Expected siRecord to have complete status.');
            System.assertEquals(SQX_Supplier_Common_Values.STAGE_VERIFICATION, suppRecord.record_stage__c, 'Expected siRecord to have verification stage.');
            
        }
        System.runAs(standardUser) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            siRecord.si.result__c = SQX_Supplier_Common_Values.RESULT_REJECTED;
            siRecord.si.closure_comment__c = 'SI Record has been closed and rejected.';
            siRecord.close();
            
            //Assert: Make sure record activity has the comment, 'SI Record has been closed and rejected.'
            //SQX_Supplier_Interaction_Record_Activity__c activitiesRecorded = [SELECT Id, activity__c, comment_long__c FROM SQX_Supplier_Interaction_Record_Activity__c WHERE SQX_Supplier_Interaction__c =: siRecord.si.Id and Activity__c = :closeAction ORDER BY CreatedDate ASC];
            //System.assertEquals(siRecord.si.closure_comment__c, activitiesRecorded.comment_long__c);
        }
        
        //Assert: Make sure that feeditem with body 'Record has been closed and rejected.' is created.    
        FeedItem feed = [SELECT parentid, body FROM FeedItem WHERE parentId = :siRecord.si.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST];
        System.assertEquals(true, feed != null, 'Expected, a feedMessage to be present in chatter');
        System.assertEquals(true, feed.body.contains(chatterMsg));
        
    }
}