/**
* Type and Inclusion reason strings defination for Response Inclusion
*/
public with sharing class SQX_Response_Inclusion{
    public static final string INC_TYPE_CONTAINMENT = 'Containment';
    public static final string INC_TYPE_ACTION = 'Action';
    public static final string INC_TYPE_INVESTIGATION = 'Investigation';
    public static final string INC_TYPE_DISPOSITION = 'Disposition';
    public static final string INC_TYPE_FINDING = 'Finding';
    
    public static final string INC_REASON_NEW = 'New';
    public static final string INC_REASON_SKIP = 'Skip';
    public static final string INC_REASON_COMPLETE = 'Complete';
    public static final string INC_REASON_PUBLISH = 'Publish';
    public static final string INC_REASON_READY_FOR_SUBMISSION = 'Ready for Submission';
    
    public static final string INCLUSION_REJECTED = 'Rejected';
}
