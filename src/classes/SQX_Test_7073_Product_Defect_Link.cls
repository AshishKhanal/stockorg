/**
 * test class for product defect link
 */
@isTest
public class SQX_Test_7073_Product_Defect_Link {
    @TestSetup
    static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        SQX_Part__c part = null;
        SQX_Task__c task = null;
        System.runAs(adminUser){
            part = SQX_Test_Part.insertParts(1).get(0);
            update part;

            SQX_Test_Task cqTask = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE);
            task = cqTask.task;
            task.Active__c = true;
            insert task;
        }   
    }

    /**
     * GIVEN : A product defect link is created
     * WHEN : Inactive part is linked
     * THEN : Error is thrown
     */
    @IsTest
    static void givenProductDefectLink_WhenInactivePartIsUsed_ErrorIsThown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        test.startTest();
        system.RunAs(adminUser){
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c LIMIT 1];
            SQX_Task__c cqTask = [SELECT Id FROM SQX_Task__c LIMIT 1];
            part.Active__c = false;
            update part;

            // ARRANGE : Product defect  link is created
            SQX_Product_Defect_Link__c pdLink = new SQX_Product_Defect_Link__c();
            pdLink.SQX_Script_to_Execute__c = cqTask.Id;

            // ACT : Inactive part is linked
            pdLink.SQX_Part__c = part.Id;
            Database.SaveResult result = Database.insert(pdLink, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Selected Part is not active.'), result.getErrors());

            // ACT : Part is made active
            part.Active__c = true;
            update part;

            // ASSERT : Save is successful
            result = Database.insert(pdLink, false);
            System.assert(result.isSuccess(), 'Save is not successful');
            
            
            // ACT (SQX-8211): update SQX_Product_Defect_Link__c
            update pdLink;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Product_Defect_Link__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Product_Defect_Link__c.SObjectType);
        }
        
        test.stopTest();
      
    }

    /**
     * GIVEN : A product defect link is created
     * WHEN : Inactive cqtasj is linked
     * THEN : Error is thrown
     */
    @IsTest
    static void givenProductDefectLink_WhenInactiveTaskIsUsed_ErrorIsThown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        test.startTest();
        system.RunAs(adminUser){
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c LIMIT 1];
            SQX_Task__c cqTask = [SELECT Id FROM SQX_Task__c LIMIT 1];
            cqTask.Active__c = false;
            update cqTask;

            // ARRANGE : Product defect  link is created
            SQX_Product_Defect_Link__c pdLink = new SQX_Product_Defect_Link__c();
            pdLink.SQX_Part__c = part.Id;

            // ACT : Inactive cq task is linked
            pdLink.SQX_Script_to_Execute__c = cqTask.Id;
            Database.SaveResult result = Database.insert(pdLink, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Selected Script is not active.'), result.getErrors());

            // ACT : cq task is made active
            cqTask.Active__c = true;
            update cqTask;

            // ASSERT : Save is successful
            result = Database.insert(pdLink, false);
            System.assert(result.isSuccess(), 'Save is not successful');
        }
        
        test.stopTest();
      
    }
}