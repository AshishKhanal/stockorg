/**
* extension class of SQX_Personnel_Job_Function__c to retrain
*/
public with sharing class SQX_Extension_Retrain_PJF {
    
    /**
    * main personnel job function record to retrain
    */
    SQX_Personnel_Job_Function__c mainRecord;
    
    /**
    * determines whether retrain action is performed or not and is used for avoiding next action in VF pages
    */
    public Boolean retrainPerformed { get; set; }
    
    /**
    * user input object to get retrain comment
    */
    public SQX_Personnel_Document_Job_Function__c retrainInput { get; set; }
    
    /**
    * new document trainings generated due to retrain action
    */
    public List<SQX_Personnel_Document_Training__c> retrainResult { get; set; }
    
    /**
    * extension constructor for standard controller
    */
    public SQX_Extension_Retrain_PJF(ApexPages.StandardController controller) {
        // initialize input object
        retrainInput = new SQX_Personnel_Document_Job_Function__c();
        retrainResult = new List<SQX_Personnel_Document_Training__c>();
        retrainPerformed = false;
        
        if (!Test.isRunningTest()) {
            // these fields are requried to process retrain
            // bypass this in tests as these fields are passed with the controller
            controller.addFields(new String[] {
                'Name',
                'SQX_Personnel__c',
                'SQX_Job_Function__c',
                'Active__c'
            });
        }
        
        // get main record
        mainRecord = (SQX_Personnel_Job_Function__c)controller.getRecord();
    }
    
    /**
    * action method to process retrain
    * @return <code>null</code> as success or failure messages are shown in VF page
    */
    public PageReference retrain() {
        if (mainRecord.Active__c) {
            // process valid personnel job function
            try {
                retrainResult = SQX_Personnel_Document_Job_Function.retrain(mainRecord, retrainInput.Comment__c);
                
                if (retrainResult.size() > 0) {
                    // display successfully generated document trainings count
                    String infoMsg = Label.SQX_MSG_RETRAIN_PJF_PENDING_DOCUMENT_TRAININGS_GENERATED;
                    infoMsg = infoMsg.replace('{RecordsCount}', '' + retrainResult.size());
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, infoMsg));
                }
                else {
                    // show message for no trainings generated to retrain
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.SQX_MSG_RETRAIN_PJF_NO_DOCUMENT_TRAININGS_GENERATED));
                }
                
                retrainPerformed = true;
            }
            catch (Exception ex) {
                // show unknown error message in VF page
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        }
        else {
            // show invalid personnel job function error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_RETRAIN_INVALID_PERSONNEL_JOB_FUNCTION));
        }
        
        return null; // return null as we need to display number of trainings generated
    }
    
    /**
    * action to close retrain page
    * @returns  redirects to first generated document training 
    */
    public PageReference close() {
        return new ApexPages.StandardController(retrainResult[0]).view();
    }
    
}