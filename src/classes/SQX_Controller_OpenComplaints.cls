/**
* This class is used to return all the open Complaints for which the user is Complaint Coordinator.
*/
public with sharing class SQX_Controller_OpenComplaints{
    
    /**
    * returns the list of Complaints for the currently logged in user's account 
    */
    public List<SQX_Complaint__c> getOpenComplaints() {
            //get all queues which current user is a part of
            Set<ID> queue_current_user_IDs= SQX_Utilities.getQueueIdsForUser(UserInfo.getUserId());
            
            queue_current_user_IDs.add(UserInfo.getUserId());
            /*
            Complaint Number 
            Outcome
            Account
            Reported Date
            Aware Date
            Product 
            Age 
            Status
            
            Return all Complaints in triage or Open state whose owner is current user or is a queue which user is a part of
            */

            return [

            SELECT Id, Name, Outcome__c, SQX_Account__c, Reported_Date__c,  Aware_Date__c, SQX_Part__c, Age__c, Status__c  
                    FROM SQX_Complaint__c
                    WHERE (Status__c =: SQX_Complaint.STATUS_OPEN OR Record_Stage__c=: SQX_Complaint.STAGE_TRIAGE) 
                            and OwnerID in :queue_current_user_IDs
                    ORDER BY Name DESC];
        
    }

}