/**
 * test class to test different scenarios for void and close complaint
 */
@isTest
public class SQX_Test_7147_Close_Void_Complaint {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

            System.runas(adminUser){

                // ARRANGE : Create CQ task
                SQX_Task__c task1 = new SQX_Task__c(Allowed_Days__c = 30, 
                                                    SQX_User__c = standardUser.Id, 
                                                    Description__c = 'Test_DESP', 
                                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                                    Step__c = 1,
                                                    Name = 'task1',
                                                    Task_Type__c = SQX_Task.TYPE_SAMPLE_REQUEST);
                
                Database.SaveResult result = Database.insert(task1, false);
                
                // ARRANGE : Create Complaint Conclusion code
                SQX_Defect_Code__c complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                                                Active__c = true,
                                                                                Defect_Category__C = 'Test_Category',
                                                                                Description__c = 'Test Description',
                                                                                Type__c = 'Complaint Conclusion');
                insert complaintConclusion;
            }
    }
    
    /**
     * GIVEN : Complaint is created in draft status
     * WHEN : Complaint is closed
     * THEN : Save is successful
     */
    public static testMethod void givenDraftCompaint_WhenClosed_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();

            // ACT : Complaint is closed
            complaint1.close();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
        }
    }
    
    /**
     * GIVEN : Complaint is created in draft status
     * WHEN : Complaint is void
     * THEN : Save is successful
     */
    public static testMethod void givenDraftCompaint_WhenVoid_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();

            // ACT : Complaint is void
            complaint1.void();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_VOID, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
        }
    }
    
    /**
     * GIVEN : Complaint is created and initiated
     * WHEN : Complaint is closed
     * THEN : Save is successful
     */
    public static testMethod void givenOpenCompaint_WhenClosed_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        SQX_Task__c task2 = null;
        System.runas(adminUser){
            
             task2 = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = standardUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'task2',
                                    Task_Type__c = SQX_Task.TYPE_SAMPLE_REQUEST);
            
            Database.SaveResult result = Database.insert(task2, false);
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and initiated
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(2, sfTaskList.size());
            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            Task sfTask = sfTaskList.get(0);
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());
            
            // ACT : Complaint is closed
            complaint1.close();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
            System.assertEquals(1, [SELECT Id FROM Task].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_DRAFT].size());
        }
    }
    
    
    /**
     * GIVEN : Complaint is created and initiated
     * WHEN : Complaint is void
     * THEN : Save is successful
     */
    public static testMethod void givenOpenCompaint_WhenVoid_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        SQX_Task__c task2 = null;
        System.runas(adminUser){
            
             task2 = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = standardUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'task2',
                                    Task_Type__c = SQX_Task.TYPE_SCRIPT);
            
            Database.SaveResult result = Database.insert(task2, false);
        }
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and initiated
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(2, sfTaskList.size());
            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            Task sfTask = [SELECT Id FROM Task WHERE Subject = 'task1' LIMIT 1];
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            // ACT : Complaint is void
            complaint1.void();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_VOID, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
            System.assertEquals(1, [SELECT Id FROM Task].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_DRAFT].size());
        }
    }
    
    /**
     * GIVEN : Complaint is created and completed
     * WHEN : Complaint is closed
     * THEN : Save is successful
     */
    public static testMethod void givenCompleteCompaint_WhenClosed_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and completed
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(1, sfTaskList.size());

            Task sfTask = sfTaskList.get(0);
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);            
            
            // ACT : Complaint is closed
            complaint1.close();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
        }
    }
    
    /**
     * GIVEN : Complaint is created and completed
     * WHEN : Complaint is void
     * THEN : Save is successful
     */
    public static testMethod void givenCompleteCompaint_WhenVoid_SaveIsSuccessful(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and completed
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(1, sfTaskList.size());

            Task sfTask = sfTaskList.get(0);
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);            

            // ACT : Complaint is void
            complaint1.void();

            // ASSERT : Save is successful
            System.assertEquals(SQX_Complaint.STATUS_VOID, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);
        }
    }
    
    
    /**
     * GIVEN : Complaint is created and send for closure review
     * WHEN : Complaint is void
     * THEN : Error is thrown
     */
    public static testMethod void givenCompleteCompaintSendForClosureReview_WhenVoid_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and send for closure review
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.complaint.Require_Closure_Review__c = true;
            complaint1.complaint.Closure_Review_By__c = adminUser.Id;
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(1, sfTaskList.size());

            Task sfTask = sfTaskList.get(0);
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);            

            SQX_Complaint__c complaint = new SQX_Complaint__c (Id = complaint1.complaint.Id);
            complaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSURE_REVIEW;
            Database.SaveResult result = Database.update(complaint, false);

            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());

            // ACT : Complaint is void
            complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
            complaint.Activity_Comment__c = 'Voiding Record';
            complaint.Is_Locked__c = true;
            complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            result = Database.update(complaint, false);

            // ASSERT :Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' );
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_COMPLAINT_SENT_FOR_REVIEW_CANNOT_BE_VOID_CLOSED),result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint is created and void
     * WHEN : Complaint is closed
     * THEN : Error is thrown
     */
    public static testMethod void givenVoidCompaint_WhenClosed_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and voided
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();

            complaint1.void();
            
            // ACT : Complaint is void
            SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaint1.complaint.Id);
            complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_CLOSE;
            complaint.Activity_Comment__c = 'Closing Record';
            complaint.Is_Locked__c = true;
            complaint.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            Database.SaveResult result = Database.update(complaint, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' );
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Unable to perform the desired action on the record.'),result.getErrors());

        }
    }
    
    /**
     * GIVEN : Complaint is created and closed
     * WHEN : Complaint is void
     * THEN : Save is successful
     */
    public static testMethod void givenClosedCompaint_WhenVoid_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and closed
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.close();

            // ACT : Complaint is vpod
            SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaint1.complaint.Id);
            complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
            complaint.Activity_Comment__c = 'Voiding Record';
            complaint.Is_Locked__c = true;
            complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            Database.SaveResult result = Database.update(complaint, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' );
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Unable to perform the desired action on the record.'),result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint is created and completed with pending regulatory report
     * WHEN : Complaint is closed
     * THEN : Errror is thrown
     */
    public static testMethod void givenCompleteCompaintWithPendingRegReport_WhenClosed_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        SQX_Task__c task2 = null;
        System.runas(adminUser){
            
             task2 = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = standardUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'task2',
                                    Task_Type__c = SQX_Task.TYPE_DECISION_TREE);
            
            Database.SaveResult result = Database.insert(task2, false);
        }
        System.runas(standardUser){ 
            // ARRANGE : Complaint is created and completed with pending regulatory report
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(2, sfTaskList.size());
            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            Task sfTask = [SELECT Id FROM Task WHERE Subject = 'task1' LIMIT 1];
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());
            
            Test.startTest();

            SQX_Decision_Tree__c    task1Run1 = complaint1.runDecisionTreeTask(task2);
            SQX_Regulatory_Report__c report1 = complaint1.addRegulatoryReport(task1Run1, SQX_Regulatory_Report.STATUS_PENDING);

            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);

            // ACT : Complaint is closed
            SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaint1.complaint.Id);
            complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_CLOSE;
            complaint.Activity_Comment__c = 'Closing Record';
            complaint.Is_Locked__c = true;
            complaint.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            Database.SaveResult result = Database.update(complaint, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' );
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_WHEN_CLOSING_COMPLAINT_WITH_PENDING_TASKS),result.getErrors());
        }
    }
    
    /**
     * GIVEN : Complaint is created and completed with pending regulatory report
     * WHEN : Complaint is void
     * THEN : Errror is thrown
     */
    public static testMethod void givenCompleteCompaintWithPendingRegReport_WhenVoid_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        SQX_Task__c task2 = null;
        System.runas(adminUser){
            
             task2 = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = standardUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'task2',
                                    Task_Type__c = SQX_Task.TYPE_DECISION_TREE);
            
            Database.SaveResult result = Database.insert(task2, false);
        }
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and completed with pending regulatory report
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.save();
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(2, sfTaskList.size());
            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            Task sfTask = [SELECT Id FROM Task WHERE Subject = 'task1' LIMIT 1];
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;

            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_OPEN].size());

            Test.startTest();
            
            SQX_Decision_Tree__c    task1Run1 = complaint1.runDecisionTreeTask(task2);
            SQX_Regulatory_Report__c report1 = complaint1.addRegulatoryReport(task1Run1, SQX_Regulatory_Report.STATUS_PENDING);

            System.assertEquals(2, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND Status__c = : SQX_Complaint_Task.STATUS_COMPLETE].size());
            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint1.complaint.Id].Status__c);

            // ACT : Complaint is void
            SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaint1.complaint.Id);
            complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
            complaint.Activity_Comment__c = 'Voiding Record';
            complaint.Is_Locked__c = true;
            complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            Database.SaveResult result = Database.update(complaint, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful' );
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_WHEN_CLOSING_COMPLAINT_WITH_PENDING_TASKS),result.getErrors());
        }
    }
}