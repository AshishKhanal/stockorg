@IsTest
public class SQX_Test_1172_NC_Default_Approver {
    static boolean runAllTests = true,
                    run_createNC_DispositionApprover_SetAsOwner = true,
                    run_createNC_Approvers_SetAsCurrentUser= true;

    /*
    *  Test to check that default disposition approver should be NC owner
    */
     public static testmethod void createNC_DispositionApprover_SetAsOwner(){
        if(!runAllTests && !run_createNC_DispositionApprover_SetAsOwner){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            nc = new SQX_Test_NC(new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                            .setRequired(false,false,true,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save());

            nc.setDispositionRules(false, null, false, null)//requireDisposition, dispositionDueDate, approveDisposition, dispositionApprover
              .save();
            nc.synchronize();

            System.assert(nc.nc.SQX_Disposition_Approver__c ==nc.nc.OwnerId, 'Expected Disposition Approver to be set as owner of record as disposition approver was set to null');
        }
    }

    /*
    *  Test to check that default disposition approver and investigation approver should be current user, when NC owner is not a user  and both are null
    */
     public static testmethod void createNC_Approvers_SetAsCurrentUser(){
        if(!runAllTests && !run_createNC_Approvers_SetAsCurrentUser){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        Group g = new Group();
        g.Name = 'Sagar';
        g.DeveloperName = 'Sagar_Queue';
        g.Type = 'Queue';

        insert g;

        QueueSObject sobj1 = new QueueSObject();
        sObj1.SobjectType = 'compliancequest__SQX_Nonconformance__c';
        sObj1.QueueId = g.Id;

        QueueSObject sobj2 = new QueueSObject();
        sObj2.SobjectType = 'compliancequest__SQX_Finding__c';
        sObj2.QueueId = g.Id;

        insert sObj1;
        insert sObj2;

        System.runas(standardUser){
            nc = new SQX_Test_NC(new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                            .setRequired(false,false,true,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save());

            nc.nc.OwnerId=g.Id; 
            nc.setDispositionRules(false, null, false, null)//requireDisposition, dispositionDueDate, approveDisposition, dispositionApprover
              .save();
            nc.synchronize();
            nc.finding.save();

            System.assert(nc.nc.SQX_Disposition_Approver__c == UserInfo.getUserId(), 'Expected Disposition Approver to be set as current user of record as disposition approver was set to null and owner is not user');
            System.assert([Select SQX_Investigation_Approver__c from SQX_Finding__c WHERE Id= :nc.finding.finding.Id].SQX_Investigation_Approver__c ==UserInfo.getUserId(), 'Expected Investigation Approver to be set as current user of record as Investigation approver was set to null and owner is not user');
        }
    }
}