/**
* unit test for NSI record submit only draft status
*/
@isTest
public class SQX_Test_5515_Submit_Access_Only_Draft {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * GIVEN : given nsi record 
    * WHEN : when status and stage is draft
    * THEN : submitting the record
    */  
    public static testMethod void givenNSI_WhenStatusISDraft_ThenSubmitTheNSIRecord(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Retrieve NSI record
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            
            //Act: Submitting the NSI record
            nsi.submit();

            //Assert: Ensured that status and stage is draft and triage
            nsi.nsi = [SELECT Id, Status__c, Record_Stage__c FROM SQX_New_Supplier_Introduction__c WHERE Id = :nsi.nsi.Id];
            System.assertEquals(SQX_NSI.STATUS_DRAFT, nsi.nsi.Status__c);
            System.assertEquals(SQX_NSI.STAGE_TRIAGE, nsi.nsi.Record_Stage__c);
        }
    }
    
    /**
    * GIVEN : given nsi record 
    * WHEN : when status and stage is open ans inProgress
    * THEN : not to be submitted the record (fire validation error)
    */
    public static testMethod void givenNSI_WhenStatusISOpen_ThenSubmitTheNSIRecord(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Retrieve NSI record
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();

            // Submitting the NSI record
            nsi.submit();

            // Initiate the NSI record
            nsi.initiate();

            // Act: Try to Submit NSI
            nsi.nsi.Status__c = SQX_NSI.STATUS_DRAFT;
            nsi.nsi.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            nsi.nsi.Activity_Code__c = 'submit';
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c> {nsi.nsi}, new List<SObjectField>{});

            // Assert : Ensure nsi is not Submitted
            System.assert(result[0].isSuccess() == false, 'Expected : NSI should not be submitted when status is open and inProgress . Actual : NSI is initiated');
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,result[0].getErrors().get(0).getMessage());
        }
    }

    /**
    * GIVEN: given nsi record 
    * WHEN: when user click restart button  in a different status than other than complete 
    * THEN: user get error message (fire validation error)
    */
    @isTest
    public static void givenNSIRecord_WhenUserClickRestartButtonOtherThanCompleteStatus_ThenErrorMessageShouldDisplayed(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            //Arranage : create a nsi record
            SQX_New_Supplier_Introduction__c nsi = new SQX_Test_NSI().save().nsi;

            //create  workflow policy
            SQX_Onboarding_Step__c wp = new SQX_Onboarding_Step__c();
            wp.SQX_Parent__c =nsi.Id;
            wp.Name = 'TestName';
            wp.Status__c = SQX_NSI.STATUS_DRAFT;
            wp.Step__c =1;
            wp.Start_Date__c=System.today();
            wp.Due_Date__c = Date.today();
            wp.SQX_User__c = adminUser.Id;
            wp.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_TASK);
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { wp },
                                   new List<Schema.SObjectField>{
                                       SQX_Onboarding_Step__c.SQX_Parent__c,
                                          SQX_Onboarding_Step__c.Name ,
                                           SQX_Onboarding_Step__c.Status__c,
                                           SQX_Onboarding_Step__c.Step__c,
                                           SQX_Onboarding_Step__c.Due_Date__c,
                                           SQX_Onboarding_Step__c.SQX_User__c
                                   });

            //Nsi is restarted
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS ;
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Activity_Code__c = 'restart';

            //Act: Try to restart the record when it is in draft status 
            List<DataBase.SaveResult> draftStatusResult = new SQX_DB().continueOnError().op_update(
                                                          new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                          new List<Schema.SObjectField>{
                                                             SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                             SQX_New_Supplier_Introduction__c.Status__c,
                                                             SQX_New_Supplier_Introduction__c.Activity_Code__c
                                                          });

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Assert: Validation rule gives an error message
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, draftStatusResult[0].getErrors()[0].getMessage());

            // Submitting the NSI record
            nsi.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            nsi.Status__c = SQX_NSI.STATUS_DRAFT;
            nsi.Activity_Code__c = 'submit';
    
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                     new List<Schema.SObjectField>{
                                                         SQX_New_Supplier_Introduction__c.Record_Stage__c
                                                        
                                                     });


            //Act: Initiating the record
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS ;
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Activity_Code__c = 'inititate';
            nsi.Current_Step__c = 1;

            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                     new List<Schema.SObjectField>{
                                                         SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                         SQX_New_Supplier_Introduction__c.Status__c,
                                                         SQX_New_Supplier_Introduction__c.Current_Step__c
                                                     });

            SQX_BulkifiedBase.clearAllProcessedEntities();


            //Restart when record  in open status
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS ;
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Activity_Code__c = 'restart';
            
            //Act: Try to restart the record when it is in open status 
            List<DataBase.SaveResult> openStatusResult = new SQX_DB().continueOnError().op_update(
                                                         new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                         new List<Schema.SObjectField>{
                                                            SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                            SQX_New_Supplier_Introduction__c.Status__c,
                                                            SQX_New_Supplier_Introduction__c.Activity_Code__c
                                                         });

            SQX_BulkifiedBase.clearAllProcessedEntities();


            //Assert: Validation rule gives an error message
             System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, openStatusResult[0].getErrors()[0].getMessage());

            //Nsi Record is in complete status 
            nsi.Record_Stage__c = SQX_NSI.STAGE_VERIFICATION ;
            nsi.Status__c = SQX_NSI.STATUS_COMPLETE;
            nsi.Activity_Code__c = 'complete';
            new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                     new List<Schema.SObjectField>{
                                                         SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                         SQX_New_Supplier_Introduction__c.Status__c
                                                     });

            SQX_BulkifiedBase.clearAllProcessedEntities();


            //Restart is done in the complete status.
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS ;
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Activity_Code__c = 'restart';
            List<DataBase.SaveResult> completeToOpenStatusResult = new SQX_DB().continueOnError().op_update(
                                                new List<SQX_New_Supplier_Introduction__c>{nsi}, 
                                                new List<Schema.SObjectField>{
                                                    SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                    SQX_New_Supplier_Introduction__c.Status__c,
                                                    SQX_New_Supplier_Introduction__c.Activity_Code__c
                                                });


            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Assert : restart is done successfully 
            System.assertEquals(true,completeToOpenStatusResult[0].isSuccess(), 'restart is successful.');

            //Arranage : create a new nsi record for void status
            SQX_New_Supplier_Introduction__c nsiRecord = new SQX_Test_NSI().save().nsi;

            //Void the nsi record
            nsiRecord.Record_Stage__c = SQX_NSI.STAGE_CLOSED ;
            nsiRecord.Status__c = SQX_NSI.STATUS_VOID;
            nsi.Activity_Code__c = 'void';
            new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c>{nsiRecord}, 
                                                     new List<Schema.SObjectField>{
                                                            SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                            SQX_New_Supplier_Introduction__c.Status__c
                                                     });

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Restart the nsi record
            nsiRecord.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS ;
            nsiRecord.Status__c = SQX_NSI.STATUS_OPEN;
            nsiRecord.Activity_Code__c = 'restart';
            //Act:Try to restart when it is in void status
             List<DataBase.SaveResult> VoidStatusResult = new SQX_DB().continueOnError().op_update(
                                                          new List<SQX_New_Supplier_Introduction__c>{nsiRecord}, 
                                                          new List<Schema.SObjectField>{
                                                              SQX_New_Supplier_Introduction__c.Record_Stage__c,
                                                              SQX_New_Supplier_Introduction__c.Status__c,
                                                              SQX_New_Supplier_Introduction__c.Activity_Code__c
                                                           });
            //Assert:Verify the Validation Rule Error message
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, VoidStatusResult[0].getErrors()[0].getMessage());
        }
    }
}