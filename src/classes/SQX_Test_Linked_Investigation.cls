/*
 * Test class for Linked Investigation trigger
 */
@isTest
public class SQX_Test_Linked_Investigation {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(adminUser){

            // creating random parts
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(2);
        }
    }
    
    //ensure duplicate linked investion is not created
    public static testMethod void ensureUniqueInvestigation(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: Create complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save(); 
            
            //Arrange: Create investigation record
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.Revision__c = 1;
            investigation.Investigation_Summary__c = 'Test investigation';  
            insert investigation;
            
            //Arrange: Create part and associated item record
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Complaint_Associated_Item__c newAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint.complaint.Id, SQX_Part_Family__c = part_2.Part_Family__c, SQX_Part__c = part_2.Id, Make_Primary__c = true);
            insert newAssociatedItem;
            
            String concatenatedFields = ''+complaint.complaint.Id+investigation.Id+newAssociatedItem.Id;
            
            //Arrange: Create Linked Investigation record
            List<SQX_Linked_Investigation__c> lInvs = new List<SQX_Linked_Investigation__c>();
            lInvs.add(new SQX_Linked_Investigation__c(compliancequest__SQX_Associated_Item__c = newAssociatedItem.Id,
                                                      compliancequest__SQX_Complaint__c = complaint.complaint.Id,
                                                      compliancequest__SQX_Investigation__c = investigation.Id));
            
            lInvs.add(new SQX_Linked_Investigation__c(compliancequest__SQX_Complaint__c = complaint.complaint.Id,
                                                      compliancequest__SQX_Investigation__c = investigation.Id));
            
            lInvs.add(new SQX_Linked_Investigation__c(compliancequest__SQX_Associated_Item__c = newAssociatedItem.Id,
                                                      compliancequest__SQX_Complaint__c = complaint.complaint.Id,
                                                      compliancequest__SQX_Investigation__c = investigation.Id));
            
            lInvs.add(new SQX_Linked_Investigation__c(compliancequest__SQX_Complaint__c = complaint.complaint.Id,
                                                      compliancequest__SQX_Investigation__c = investigation.Id));
            //Act: Insert Linked Investigation records
            Database.SaveResult[] results = Database.insert(lInvs,false);
            
            //Assert: Check duplicate record
            System.assertEquals(true,results[0].isSuccess(),'Linked Investigation should be saved');
            System.assertEquals(true,results[1].isSuccess(),'Linked Investigation should be saved'+results[1].getErrors());
            System.assertEquals(true,!results[2].isSuccess(),'Linked Investigation should not be saved when it is duplicate'); 
            System.assertEquals(true,!results[3].isSuccess(),'Linked Investigation should not be saved when it is duplicate'); 
            
            //Act: Query Unique_Investigation field from Linked Investigation
            lInvs = [SELECT Unique_Investigation__c FROM SQX_Linked_Investigation__c WHERE Id=: results[0].Id];
            
            //Assert: Check value in Unique_Investigation field
            System.assertEquals(concatenatedFields,lInvs[0].Unique_Investigation__c,'Ids of complaint, investigation and associated item should be concatenated and stored in  Unique_Investigation field');
        }
    }
}