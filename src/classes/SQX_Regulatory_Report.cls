/**
* This class contains the logic related to regulatory reporting
*/
public with sharing class SQX_Regulatory_Report {
    
    //The list of statuses for the regulatory report.
    public static final String 	STATUS_PENDING = 'Pending',
                                STATUS_VOID = 'Void',
                                STATUS_COMPLETE = 'Complete';

    public static final String REPORT_NAME_5_DAY_MDR = 'US:5-Day MDR',
                                REPORT_NAME_30_DAY_MDR = 'US:30-Day MDR',
                                REPORT_NAME_2_DAY_EMA = 'EU:2-Day MDV',
                                REPORT_NAME_30_DAY_PMDA = 'JP:30-day ARR',
                                REPORT_NAME_2_DAY_MDIR = 'AU:2-day MDIR',
                                REPORT_NAME_10_DAY_MDPR = 'CA:10-day MDPR';
    
    public static final String REGULATORY_BODY_FDA = 'FDA',
                                REGULATORY_BODY_EMA = 'EMA',
                                REGULATORY_BODY_TGA = 'TGA',
                                REGULATORY_BODY_HEALTH_CANADA = 'Health Canada';
    
    // Unique name to be recorded for Save action 
    public static final String RECORD_SAVE_ACTIVITY = 'recordActivity_Save';

    /**
     * Method returns the metadata record associated with the given regulatory report record
     * It looks over in the metadata records to see if the form name matches the one in regulatory record
     * Returns null if no match is found
    */
    public CQ_Regulatory_Report_Setting__mdt getSubmissionSettingsForReport(Id reportId) {

        return getSubmissionSettingsForReports(new Set<Id> { reportId }).get(reportId);
    }
    
    
    /**
     * Method returns the metadata record associated with the given regulatory report record
     * It looks over in the metadata records to see if the form name matches the one in regulatory record
    */
    public Map<Id, CQ_Regulatory_Report_Setting__mdt> getSubmissionSettingsForReports(Set<Id> reportIds) {

        Map<Id, CQ_Regulatory_Report_Setting__mdt> settingsMap = new Map<Id, CQ_Regulatory_Report_Setting__mdt>();
        
        Map<String, List<SQX_Regulatory_Report__c>> uniqueReports = new Map<String, List<SQX_Regulatory_Report__c>>();

        List<String> reportNamesToLookFor = new List<String>();
        List<String> regBodiesToLookFor = new List<String>();
        for(SQX_Regulatory_Report__c report : [SELECT Report_Name__c, Reg_Body__c FROM SQX_Regulatory_Report__c WHERE Id IN: reportIds]) {
            String uniqueKey = report.Report_Name__c + report.Reg_Body__c;
            if(!uniqueReports.containsKey(uniqueKey)) {
                uniqueReports.put(uniqueKey, new List<SQX_Regulatory_Report__c>());
            }
            uniqueReports.get(uniqueKey).add(report);
            
            reportNamesToLookFor.add(report.Report_Name__c);
            regBodiesToLookFor.add(report.Reg_Body__c);            
        }

       for(CQ_Regulatory_Report_Setting__mdt reportSetting : [SELECT Form_Name__c, Form_Type__c, Regulatory_Body__c, Report_Type__c, Mode_Of_Submission__c
                                                               FROM CQ_Regulatory_Report_Setting__mdt
                                                               WHERE Regulatory_Body__c IN: regBodiesToLookFor
                                                               AND Form_Name__c IN: reportNamesToLookFor
                                                               AND Active__c = true])
       {
           String uniqueKey = reportSetting.Form_Name__c + reportSetting.Regulatory_Body__c;
           for(SQX_Regulatory_Report__c report : uniqueReports.get(uniqueKey)) {
               settingsMap.put(report.Id, reportSetting);
           }
       }

        return settingsMap;
    }
    
    
    public static final String ACTIVITY_CODE_VOID = 'void';

    /**
    * The class that contains code triggered by workflow.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        public Bulkified(){
            
        }
        
        /**
        * The constructor for handling the trigger activities
        * @param newReports the reports that have been newly added/updated
        * @param oldMap the map of old values of the report
        */ 
        public Bulkified(List<SQX_Regulatory_Report__c> newReports, Map<Id, SQX_Regulatory_Report__c> oldMap){
            super(newReports, oldMap);
        }

        /**
         * method to set status based on activity code
         */
        public Bulkified setStatusAndStageBasedOnActivityCode(){

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Regulatory_Report__c.Activity_Code__c, RuleOperator.NotEquals, null);

            this.resetView()
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Regulatory_Report__c> regReportList = (List<SQX_Regulatory_Report__c>) recordsWithActivityCodeRule.evaluationResult;

            if(regReportList.size() > 0){
                
                Map<String, String> statusMap = SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STATUS_MAP;
                for(SQX_Regulatory_Report__c regReport : regReportList){
                    String activityCode = regReport.Activity_Code__c;
                    if(statusMap.containsKey(activityCode)) {
                        regReport.Status__c = statusMap.get(activityCode);
                    }
                }
            }

            return this;
        }

        /**
        *  Method evaluates the record and if the record is not in draft and the activity code is blank,
        *  it sets the activity code to be 'Save'
        */
        public Bulkified evaluateSaveActivity() {

            Rule recordsBeingSaved = new Rule();
            recordsBeingSaved.addRule(SQX_Regulatory_Report__c.Activity_Code__c, RuleOperator.Equals, null);

            this.resetView()
                .removeProcessedRecordsFor(SQX.RegulatoryReport, SQX_Regulatory_Report.RECORD_SAVE_ACTIVITY)
                .applyFilter(recordsBeingSaved, RuleCheckMethod.OnCreateAndEveryEdit);

            for(SQX_Regulatory_Report__c record : (List<SQX_Regulatory_Report__c>)this.view) {
                this.addToProcessedRecordsFor(SQX.RegulatoryReport, SQX_Regulatory_Report.RECORD_SAVE_ACTIVITY, this.view);
                record.Activity_Code__c =  SQX_Supplier_Common_Values.ACTIVITY_CODE_SAVE;
            }
            return this;
        }

        /**
         * method to clear activity code if it is not blank
         */
        public Bulkified clearActivityCode(){

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Regulatory_Report__c.Activity_Code__c, RuleOperator.NotEquals, null);

            this.resetView()
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Regulatory_Report__c> regReportList = (List<SQX_Regulatory_Report__c>) recordsWithActivityCodeRule.evaluationResult;

            if(regReportList.size() > 0){
                
                this.addToProcessedRecordsFor(SQX.RegulatoryReport, RECORD_SAVE_ACTIVITY, regReportList);
                List<SQX_Regulatory_Report__c> regReportsToUpdate = new List<SQX_Regulatory_Report__c>();
                for(SQX_Regulatory_Report__c regReport : regReportList){
                    SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c();
                    report.Activity_Code__c = null;
                    report.Id = regReport.Id;
                    regReportsToUpdate.add(report);
                }

                new SQX_DB().op_update(regReportsToUpdate, new List<Schema.SObjectField>{SQX_Regulatory_Report__c.Activity_Code__c});
            }
            return this;
        }
        
        /**
        * This method sets the default assignee to the current user if it isn't provided and also sets the lock field if the status
        * is complete or void.
        * [Note: It isn't good idea to lump two different logic together, since we are iterating over the same set this has been done for optimization]
        * //TODO: think about 'Premature optimization is the root of all evil -- Donald Knuth'
        */
        public Bulkified setDefaultAssigneeAndLock(){
            this.resetView();
            Map<Id, List<SQX_Regulatory_Report__c>> reportsWithoutAssignee = new Map<Id, List<SQX_Regulatory_Report__c>>();
            
            for(SQX_Regulatory_Report__c report : (List<SQX_Regulatory_Report__c>)this.view){
                
                //store the reports without assignee, we will assign complaint owner as assignee later
                if(report.SQX_User__c == null){
                    List<SQX_Regulatory_Report__c> reports = reportsWithoutAssignee.get(report.SQX_Complaint__c);
                    if(reports == null){
                        reports = new List<SQX_Regulatory_Report__c>();
                        reportsWithoutAssignee.put(report.SQX_Complaint__c, reports);
                    }
                    
                    reports.add(report);
                }
                
                //set the lock field if the report is complete or voided.
                if(report.Status__c == STATUS_COMPLETE || report.Status__c == STATUS_VOID)
                    report.Is_Locked__c = true;
                
            }
            
            //assign the complaint owner as assignee in the reports without assignee
            if(reportsWithoutAssignee.keySet().size() > 0){
                
                //get the complaint owners
                Map<Id, SQX_Complaint__c> complaints = new Map<Id, SQX_Complaint__c>([SELECT Id, OwnerId FROM SQX_Complaint__c
                                                                                      WHERE Id IN : reportsWithoutAssignee.keySet()]);
                
                //Set the assignee of the reports using the complaint
                for(Id complaintId : reportsWithoutAssignee.keySet()){
                    SQX_Complaint__c complaint = complaints.get(complaintId);
                    for(SQX_Regulatory_Report__c report : reportsWithoutAssignee.get(complaintId)){
                        report.SQX_User__c = complaint.OwnerId;
                    }
                }
            }
            
            
            return this; 
        }
        
        /**
         * This method creates a reporting task, the task is created for each regulatory report that is pending.
         */
        public Bulkified createReportingTasks(){
            final String ACTION_NAME = 'createReportingTasks';
            
            Rule reportingOnDecisionTree = new Rule();
            reportingOnDecisionTree.addRule(SQX_Regulatory_Report__c.Task_Id__c, RuleOperator.Equals, null);
            reportingOnDecisionTree.addRule(SQX_Regulatory_Report__c.Status__c, RuleOperator.Equals, STATUS_PENDING);
            
            this.resetView()
                .applyFilter(reportingOnDecisionTree, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(reportingOnDecisionTree.evaluationResult.size() > 0){
                
                //add task for each individual pending regulatory report
                List<SQX_Regulatory_Report__c> reports = (List<SQX_Regulatory_Report__c>)reportingOnDecisionTree.evaluationResult;
                List<Task> tasksToInsert = new List<Task>();
                
                for(SQX_Regulatory_Report__c report : (List<SQX_Regulatory_Report__c>)reportingOnDecisionTree.evaluationResult){
                    Task task = SQX_Task_Template.createTaskFor(report, report.SQX_User__c, null, report.Due_Date__c, SQX_Task_Template.TaskType.RegulatoryReportNeeded);
                    
                    tasksToInsert.add(task);
                }
                
                
                new SQX_DB().op_insert(tasksToInsert, new List<Schema.SObjectField> { Task.WhatId, Task.OwnerId, Task.Subject, Task.ActivityDate, Task.Priority, Task.Status });
                
                //set the id of the task in each individual report.
                for(Integer i = 0; i < reports.size(); i++){
                    SQX_Regulatory_Report__c report = reports.get(i);
                    Task task = tasksToInsert.get(i);
                    report.Task_Id__c = task.Id;
                }
            }
            
            
            return this;
            
            
        }

        
        /**
         * This method closes Salesforce task, related to the regulatory report when the regulatory report is voided or completed.
         */
        public Bulkified closeTaskAssociatedWithReports(){
            final String ACTION_NAME = 'closeTaskAssociatedWithReports';
            
            Rule closedOrVoidedReportRule = new Rule();
            closedOrVoidedReportRule.operator = RuleCombinationOperator.OpOr;
            closedOrVoidedReportRule.addRule(SQX_Regulatory_Report__c.Status__c, RuleOperator.Equals, STATUS_VOID);
            closedOrVoidedReportRule.addRule(SQX_Regulatory_Report__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.RegulatoryReport, ACTION_NAME)
                .applyFilter(closedOrVoidedReportRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(closedOrVoidedReportRule.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.RegulatoryReport, ACTION_NAME, closedOrVoidedReportRule.evaluationResult);
                
                Set<Id> taskIds = this.getIdsForField(SQX_Regulatory_Report__c.Task_Id__c);

                //we are selecting the tasks again because we are recording the id of the task in a text field.
                //so we aren't sure if the task still exists in the Org.
                List<Task> tasksToUpdate = [SELECT Id, Status FROM Task WHERE Id IN: taskIds AND Status != 'Complete' FOR UPDATE];
                for(Task task : tasksToUpdate){
                    task.Status = 'Completed';
                }
                
                /*
                 * WITHOUT SHARING
                 * ---------------
                 * We are using without sharing because the person closing the regulatory report may or may not have necessary permission on the task object
                 * of another user.
                 */
                new SQX_DB().withoutSharing().op_update(tasksToUpdate, new List<SObjectField> { Task.Status});
                
            }
            
            return this;
        }

        /**
         * This method is to prevent deletion of record if parent is locked
         * @author Sanjay Maharjan
         * @date 2019-01-14
         * @story [SQX-7470]
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Regulatory_Report__c> regulatoryReports = new Map<Id, SQX_Regulatory_Report__c>((List<SQX_Regulatory_Report__c>)this.view);
            
            for(SQX_Regulatory_Report__c regulatoryReport : [SELECT Id FROM SQX_Regulatory_Report__c WHERE Id IN : regulatoryReports.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    regulatoryReports.get(regulatoryReport.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }

        /**
         * This method is to prevent deletion of regulatory report record
         */
        public Bulkified preventDeletionOfRegulatoryReports(){
            this.resetView();
            if(this.view.size() > 0){
                for(SQX_Regulatory_Report__c regulatoryReport : (List<SQX_Regulatory_Report__c>) this.view) {
                    regulatoryReport.addError(Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT);
                }
            }
            return this;
        }
    }
}