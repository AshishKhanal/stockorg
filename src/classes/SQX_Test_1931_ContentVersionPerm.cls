/**
* This class checks if the permission util works accordingly. Note no story test can be performed because
* ContentVersion in Unit test can't have Controlled doc set, so no validation kicks in
*/
@IsTest
public class SQX_Test_1931_ContentVersionPerm {
    
    /**
    * This test ensures that the designed util works properly i.e. if a custom permission has been granted to the user
    * then it returns true
    * If it hasn't been granted it returns false
    */
    static testmethod void givenUserIsGrantedPermission_UtilReturnsCorrectValue(){
        // Arrange: create a user with the required custom permissions
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
        	 standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);
        
        
        // from: http://andyinthecloud.com/2015/01/14/creating-assigning-and-checking-custom-permissions/
        PermissionSet ps = new PermissionSet();
        ps.Name = 'Test';
        ps.Label = 'Test';
        insert ps;
        
        SetupEntityAccess sea = new SetupEntityAccess();
        sea.ParentId = ps.Id;
        sea.SetupEntityId = [select Id from CustomPermission where DeveloperName = :SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR][0].Id;
        insert sea;
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = standardUser.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
        
        
        System.runAs(standardUser){
            // Assert: Ensure that the user has custom permission for the edit lock document
            System.assert(SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR), 'Expected the user to have edit lock doc permission but found none');
        }
        
        System.runAs(standardUser2){
            // Assert: Ensure that the user without custom permission for edit lock document gets false
            System.assert(!SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR),
                         'Expected user 2 not have the custom permission');
        }
        
        

    }

}