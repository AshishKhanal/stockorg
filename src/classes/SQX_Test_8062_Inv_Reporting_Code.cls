/**
 * test class for investigation reporting code
 */
@isTest
public class SQX_Test_8062_Inv_Reporting_Code {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * GIVEN : Investigation Reporting Code is created
     * WHEN : Record is saved with incorrect value
     * THEN : Error is thrown
     */
    public testMethod static void givenInvestigationReportingCode_WhenSaveWithIncorrectValue_ErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
            //Arrange: Create complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            Id complaintRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Investigation, 'Complaint_Investigation');                                                                              
            
            //Arrange: Create investigation record
            SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_Complaint__c = complaint.complaint.Id,
                                                       Investigation_Summary__c = 'Test Summary',
                                                       RecordTypeId = complaintRecordTypeId);
            insert investigation;

            SQX_Defect_Code__c defectCode1 = new SQX_Defect_Code__c(Name = 'defectCode',
                                                                    Type__c ='Method Code',
                                                                    Defect_Category__c ='Cat1',
                                                                    Active__c = true,
                                                                    Level__c = 1);
            
            Database.SaveResult result = Database.Insert(defectCode1, false);

            SQX_Defect_Code__c defectCode2 = new SQX_Defect_Code__c(Name = 'defectCode',
                                                Type__c ='Result Code',
                                                Defect_Category__c ='Cat1',
                                                Active__c = true,
                                                Level__c = 1);
            
            result = Database.Insert(defectCode2, false);
            
            
            // ARRANGE : Investigation Reporting Code is created
            SQX_Investigation_Reporting_Code__c reportingCode = new SQX_Investigation_Reporting_Code__c(Code_Type__c = 'Method Code', 
                                                                                                        SQX_Investigation__c = investigation.Id, 
                                                                                                        SQX_Hierarchical_Code__c = defectCode2.Id );
            // ACT : Record is saved with incorrect value
            result = Database.Insert(reportingCode, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Selected value\'s type does not match code type.'), result.getErrors());

            // ACT : Record is saved with correct value
            reportingCode.SQX_Hierarchical_Code__c = defectCode1.Id;
            result = Database.Insert(reportingCode, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
            
            
            // ACT (SQX-8211): update SQX_Investigation_Reporting_Code__c
            update reportingCode;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Investigation_Reporting_Code__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Investigation_Reporting_Code__c.SObjectType);
        }
    }

}