@IsTest
public class SQX_Test_4398_MigrateAuditStatusAndStage {

    final static String RESPONSE_SUBMITTER = 'Std user 1',
                        ADMIN = 'Admin user 1';
    @testsetup
    static void setupData() {
        //Arrange: Create users
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN);
        User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, RESPONSE_SUBMITTER);
    }
    /**
    * helper funtion to setup Audit, Audit Report, Finding, Audit Response, Finding Response,Actions ,Investigations and Response Inclusions 
    * @param adminUser to create Test Audit and ResponseSubmitter for Auditee Contact 
    **/  
    public static List<Id> arrangeAudit(User adminUser, User responseSubmitter){
        /*
        * Arrange Audit as follows
        * Audit 1: Audit with
        *        'Complete' Status
        *        'Open' Finding
        *        'Pending' Audit Response
        * Audit 2: Audit with
        *        'Complete' Status
        *        'Complete' Finding
        *        'Pending' Audit Response
        *        'Open' Actions
        * Audit 3: Audit with
        *        'Complete' Status
        *        'Complete' Finding
        *        'Pending' Audit Response
        *        'Open' Actions
        * Audit 4: Audit with
        *        'Complete' Status
        *        'Open' Finding
        *        'Complete' Audit Response
        * Audit 5: Audit with
        *        'Complete' Status
        *        'Complete' Finding
        *        'Complete' Audit Response
        *        'Complete' Actions
        * Audit 6: Audit with
        *        'Plan' Status
        * Audit 7: Audit with
        *        'Plan Approval' Status
        * Audit 8: Audit with
        *        'Scheduled' Status
        * Audit 9: Audit with
        *        'Confirmed' Status
        * Audit 10: Audit with
        *        'Close' Status
        * Audit 11: Audit with
        *        'In Progress' Status
        * Audit 12: Audit with
        *        'In Progress' Status
        *        'In Approval' Audit Report
        * Audit 13: Audit with
        *        'Plan' Status
        *        'In Approval' Audit Program
        */

        SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
        audit1.audit.Title__c = 'Audit-1';
        audit1.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit1.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit1.setPolicy(false, true, true);
        audit1.audit.Start_Date__c = Date.Today();
        audit1.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit2 = new SQX_Test_Audit(adminUser);
        audit2.audit.Title__c = 'Audit-2';
        audit2.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit2.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit2.setPolicy(false, false, true);
        audit2.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit3 = new SQX_Test_Audit(adminUser);
        audit3.audit.Title__c = 'Audit-3';
        audit3.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit3.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit3.setPolicy(false, false, true);
        audit3.audit.Start_Date__c = Date.Today();
        audit3.audit.End_Date__c = Date.Today().addDays(10);
        
        SQX_Test_Audit audit4 = new SQX_Test_Audit(adminUser);
        audit4.audit.Title__c = 'Audit-4';
        audit4.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit4.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit4.setPolicy(false, true, true);
        audit4.audit.Start_Date__c = Date.Today();
        audit4.audit.End_Date__c = Date.Today().addDays(40);

        SQX_Test_Audit audit5 = new SQX_Test_Audit(adminUser);
        audit5.audit.Title__c = 'Audit-5';
        audit5.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit5.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit5.setPolicy(false, true, true);
        audit5.audit.Start_Date__c = Date.Today();
        audit5.audit.End_Date__c = Date.Today().addDays(50);

        SQX_Test_Audit audit6 = new SQX_Test_Audit(adminUser);
        audit6.audit.Title__c = 'Audit-6';
        audit6.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_PLAN);
        audit6.audit.Start_Date__c = Date.Today();
        audit6.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit7 = new SQX_Test_Audit(adminUser);
        audit7.audit.Title__c = 'Audit-7';
        audit7.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_PLAN_APPROVAL);
        audit7.audit.Start_Date__c = Date.Today();
        audit7.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit8 = new SQX_Test_Audit(adminUser);
        audit8.audit.Title__c = 'Audit-8';
        audit8.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_SCHEDULED);
        audit8.audit.Start_Date__c = Date.Today();
        audit8.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit9 = new SQX_Test_Audit(adminUser);
        audit9.audit.Title__c = 'Audit-9';
        audit9.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_CONFIRMED);
        audit9.audit.Start_Date__c = Date.Today();
        audit9.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit10 = new SQX_Test_Audit(adminUser);
        audit10.audit.Title__c = 'Audit-10';
        audit10.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_CLOSE);
        audit10.audit.Start_Date__c = Date.Today();
        audit10.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit11 = new SQX_Test_Audit(adminUser);
        audit11.audit.Title__c = 'Audit-11';
        audit11.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_IN_PROGRESS);
        audit11.audit.Start_Date__c = Date.Today();
        audit11.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit12 = new SQX_Test_Audit(adminUser);
        audit12.audit.Title__c = 'Audit-12';
        audit12.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_IN_PROGRESS);
        audit12.setStage(SQX_Audit.STAGE_IN_PROGRESS);
        audit12.audit.Start_Date__c = Date.Today();
        audit12.audit.End_Date__c = Date.Today().addDays(10);
        
        SQX_Test_Audit audit13 = new SQX_Test_Audit(adminUser);
        audit13.audit.Title__c = 'Audit-13';
        audit13.setStatus(SQX_7_4_MigrateAuditStatusAndStage.OLD_STATUS_PLAN);
        audit13.audit.Start_Date__c = Date.Today();
        audit13.audit.End_Date__c = Date.Today().addDays(10);
        
        insert new List<SQX_Audit__c> { audit1.audit, audit2.audit, audit3.audit, audit4.audit, audit5.audit, audit6.audit, audit7.audit, audit8.audit, audit9.audit, audit10.audit, audit11.audit, audit12.audit, audit13.audit };

        //Create Audit Report 
        SQX_Audit_Report__c report = new SQX_Audit_Report__c();
        report.Status__c = SQX_Audit_Report.STATUS_IN_APPROVAL;
        report.SQX_Audit__c = audit12.audit.Id;
        insert report; 
        
        //Create Audit Program In Draft Status 
        SQX_Audit_Program__c program = new SQX_Audit_Program__c();
       
        program.Status__c = SQX_Audit_Program.STATUS_DRAFT;
        program.Objective__c = 'This is test';
        program.Title__c = 'Test';
        program.Name = 'AP';
        insert program;
        
        audit13.audit.SQX_Audit_Program__c = program.Id;
        audit13.save();
        
        SQX_Test_Finding auditFinding1 =  audit1.addAuditFinding()
                                                .setRequired(true, true, true, false, false) 
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN);

        SQX_Test_Finding auditFinding2 =  audit2.addAuditFinding()
                                                .setRequired(false, false, false, false, false) 
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        SQX_Test_Finding auditFinding3 =  audit3.addAuditFinding()
                                                .setRequired(false, false, false, false, false) 
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        SQX_Test_Finding auditFinding4 =  audit4.addAuditFinding()
                                                .setRequired(true, true, true, false, false) 
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN);

        SQX_Test_Finding auditFinding5 =  audit5.addAuditFinding()
                                                .setRequired(true, true, true, false, false)
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        insert new List<SQX_Finding__c> {auditFinding1.finding, auditFinding2.finding, auditFinding3.finding, auditFinding4.finding, auditFinding5.finding};

        SQX_Audit_Response__c auditResponse1 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit1.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 1',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_PENDING);

        SQX_Audit_Response__c auditResponse2 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit2.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 2',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        SQX_Audit_Response__c auditResponse3 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit3.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 3',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_PENDING);

        SQX_Audit_Response__c auditResponse4 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit4.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 4',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        SQX_Audit_Response__c auditResponse5 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit5.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 5',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        insert new List<SQX_Audit_Response__c> { auditResponse1, auditResponse2, auditResponse3, auditResponse4, auditResponse5 };

        SQX_Finding_Response__c findingResponse1 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding1.finding.Id,
                                               SQX_Audit_Response__c = auditResponse1.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse2 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding2.finding.Id,
                                               SQX_Audit_Response__c = auditResponse2.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse3 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding3.finding.Id,
                                               SQX_Audit_Response__c = auditResponse3.Id,
                                               Response_Summary__c = 'Response for audit',
                                               RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Response, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING));

        SQX_Finding_Response__c findingResponse4 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding4.finding.Id,
                                               SQX_Audit_Response__c = auditResponse4.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse5 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding5.finding.Id,
                                               SQX_Audit_Response__c = auditResponse5.Id,
                                               Response_Summary__c = 'Response for audit');

        insert new List<SQX_Finding_Response__c> { findingResponse1, findingResponse2, findingResponse3, findingResponse4, findingResponse5 };

        SQX_Investigation__c investigation1 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding1');

        SQX_Investigation__c investigation4 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding4.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding4');

        SQX_Investigation__c investigation5 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding5.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding5');

        insert new List<SQX_Investigation__c> { investigation1, investigation4, investigation5 };

        SQX_Action__c action2 = new SQX_Action__c(SQX_Audit__c = audit2.audit.Id,
                                                    Plan_Number__c = 'PLAN-01',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(10),
                                                    Status__c = SQX_Implementation_Action.STATUS_OPEN);

        SQX_Action__c action3 = new SQX_Action__c(SQX_Audit__c = audit3.audit.Id,
                                                    Plan_Number__c = 'PLAN-03',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(10),
                                                    Status__c = SQX_Implementation_Action.STATUS_OPEN);

        SQX_Action__c action5 = new SQX_Action__c(SQX_Audit__c = audit5.audit.Id,
                                                    Plan_Number__c = 'PLAN-05',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(50),
                                                    Status__c = SQX_Implementation_Action.STATUS_COMPLETE);

        insert new List<SQX_Action__c> { action2, action3, action5};


        SQX_Response_Inclusion__c respInclusion1 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse1.Id,
                                                        SQX_Investigation__c = investigation1.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion3 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Action',
                                                        SQX_Response__c = findingResponse3.Id,
                                                        SQX_Action__c = action3.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion4 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse4.Id,
                                                        SQX_Investigation__c = investigation4.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion5 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse5.Id,
                                                        SQX_Investigation__c = investigation5.Id
                                                    );

        insert new List<SQX_Response_Inclusion__c> { respInclusion1, respInclusion3, respInclusion4, respInclusion5}; 
        
        program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_PLAN_APPROVAL;
        update program;
        
        return new List<Id>{ audit1.audit.Id,
                             audit2.audit.Id,
                             audit3.audit.Id,
                             audit4.audit.Id,
                             audit5.audit.Id,
                             audit6.audit.Id,
                             audit7.audit.Id,
                             audit8.audit.Id,
                             audit9.audit.Id,
                             audit10.audit.Id,
                             audit11.audit.Id,
                             audit12.audit.Id,
                             audit13.audit.Id};
    }

    /**
    * Given: Audit Program with different status
    * When:  SQX_7_4_MigrateAuditStatusAndStage runs
    * Then:  Status are changed into New Status and Stage is set
    * 
    * @author: Sanjay Maharjan
    * @date: 11/30/2017
    * @story: [SQX-4398]
    */
    public static testmethod void whenSQX_7_4_MigrateAuditStatusAndStageClassRuns_ThenStatusChangesToNewStatusAndStageAdded(){
        //Disabled test due to build failure after Summer 18.
        if(true)
            return;
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User responseSubmitter = users.get(RESPONSE_SUBMITTER);
        User adminUser = users.get(ADMIN);
        
        System.runAs(adminUser){
            //Arrange: Create differnt Audit with different status
            List<Id> auditIdList = arrangeAudit(adminUser, responseSubmitter);

            //Give custom permission set of data migration to admin user
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);

            //Act: Run SQX_7_4_MigrateAuditStatusAndStage batch class
            Test.startTest();
            SQX_7_4_MigrateAuditStatusAndStage job = new SQX_7_4_MigrateAuditStatusAndStage();
            ID batchprocessid = Database.executeBatch(job);
            Test.stopTest();

            final Map<String, String[]> auditExpectedStatusAndStage = new Map<String, String[]> {
                'Audit-1' => new String[] { SQX_Audit.STATUS_COMPLETE, SQX_Audit.STAGE_RESPONSE_APPROVAL },
                'Audit-2' => new String[] { SQX_Audit.STATUS_COMPLETE, SQX_Audit.STAGE_IMPLEMENT },
                'Audit-3' => new String[] { SQX_Audit.STATUS_COMPLETE, SQX_Audit.STAGE_IMPLEMENTATION_RESPONSE_APPROVAL },
                'Audit-4' => new String[] { SQX_Audit.STATUS_COMPLETE, SQX_Audit.STAGE_RESPOND },
                'Audit-5' => new String[] { SQX_Audit.STATUS_COMPLETE, SQX_Audit.STAGE_READY_FOR_CLOSURE },
                'Audit-6' => new String[] { SQX_Audit.STATUS_DRAFT, SQX_Audit.STAGE_PLAN },
                'Audit-7' => new String[] { SQX_Audit.STATUS_DRAFT, SQX_Audit.STAGE_PLAN_APPROVAL },
                'Audit-8' => new String[] { SQX_Audit.STATUS_OPEN, SQX_Audit.STAGE_SCHEDULED },
                'Audit-9' => new String[] { SQX_Audit.STATUS_OPEN, SQX_Audit.STAGE_CONFIRMED },
                'Audit-10' => new String[] { SQX_Audit.STATUS_CLOSE, SQX_Audit.STAGE_CLOSED },
                'Audit-11' => new String[] { SQX_Audit.STATUS_OPEN, SQX_Audit.STAGE_IN_PROGRESS },
                'Audit-12' => new String[] { SQX_Audit.STATUS_OPEN, SQX_Audit.STAGE_FINDING_APPROVAL },
                'Audit-13' => new String[] { SQX_Audit.STATUS_DRAFT, SQX_Audit.STAGE_PROGRAM_APPROVAL }
            };
    
            //Assert: Audit Status is changed into New Status and Stage is set
            for ( SQX_Audit__c audit : [SELECT Id, Title__c, Status__c, Stage__c FROM SQX_Audit__c WHERE Id IN: auditIdList ]) {
                System.assertEquals(auditExpectedStatusAndStage.get(audit.Title__c)[0], audit.Status__c, 'Audit\'s status is invalid');
                System.assertEquals(auditExpectedStatusAndStage.get(audit.Title__c)[1], audit.Stage__c, 'Audit\'s stage is invalid');
            }
        }
    }

    /**
    * Given: Audit without Division or Account
    * When:  Audit is inserted with Data Migration Customer Permission
    * Then:  Audit is created without any error else audit will not be created 
    * 
    * @author: Sanjay Maharjan
    * @date: 6/2/2018
    * @story: [SQX-4770]
    */
    public static testmethod void givenAuditWithoutDivisionOrAccount_WhenAuditIsInsertedWithDataMigrationCustomerPermission_ThenAuditIsCreatedElseAuditWillNotBeCreated() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN);
        User standardUser = users.get(RESPONSE_SUBMITTER);

        //Give custom permission set of data migration to admin user
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);

        System.runAs(adminUser) {
            //Arrange: Create Internal Audit without Division and Supplier and Customer Audit without Account 
            SQX_Audit__c internalAudit = new SQX_Audit__c();
            internalAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            internalAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            internalAudit.Title__c = 'Internal Audit Title';
            internalAudit.Audit_Type__c = 'Internal';
            internalAudit.Audit_Category__c = 'ISO';
            internalAudit.SQX_Auditee_Contact__c = adminUser.Id;

            SQX_Audit__c customerAudit = new SQX_Audit__c();
            customerAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            customerAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            customerAudit.Title__c = 'Customer Audit Title';
            customerAudit.Audit_Type__c = 'Customer';
            customerAudit.Audit_Category__c = 'ISO';
            customerAudit.SQX_Auditee_Contact__c = adminUser.Id;

            SQX_Audit__c supplierAudit = new SQX_Audit__c();
            supplierAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            supplierAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            supplierAudit.Title__c = 'Supplier Audit Title';
            supplierAudit.Audit_Type__c = 'Supplier';
            supplierAudit.Audit_Category__c = 'ISO';
            supplierAudit.SQX_Auditee_Contact__c = adminUser.Id;

            //Act: Save audits
            Database.SaveResult[] results = new SQX_DB().op_insert(new List<SQX_Audit__c> {internalAudit, customerAudit, supplierAudit}, new List<Schema.SObjectField>{});

            //Assert : Audit is saved without error
            for(Database.SaveResult result : results) {
                System.assert(result.isSuccess(), 'Error : ' + result.getErrors());
            }
        }

        System.runAs(standardUser) {
            //Arrange: Create Internal Audit without Division and Supplier and Customer Audit without Account 
            SQX_Audit__c internalAudit = new SQX_Audit__c();
            internalAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            internalAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            internalAudit.Title__c = 'Internal Audit Title';
            internalAudit.Audit_Type__c = 'Internal';
            internalAudit.Audit_Category__c = 'ISO';
            internalAudit.SQX_Auditee_Contact__c = adminUser.Id;

            SQX_Audit__c customerAudit = new SQX_Audit__c();
            customerAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            customerAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            customerAudit.Title__c = 'Customer Audit Title';
            customerAudit.Audit_Type__c = 'Customer';
            customerAudit.Audit_Category__c = 'ISO';
            customerAudit.SQX_Auditee_Contact__c = adminUser.Id;

            SQX_Audit__c supplierAudit = new SQX_Audit__c();
            supplierAudit.Status__c = SQX_Audit.STATUS_DRAFT;
            supplierAudit.Stage__c = SQX_Audit.STAGE_PLAN;
            supplierAudit.Title__c = 'Supplier Audit Title';
            supplierAudit.Audit_Type__c = 'Supplier';
            supplierAudit.Audit_Category__c = 'ISO';
            supplierAudit.SQX_Auditee_Contact__c = adminUser.Id;

            //Act: Save audits
            Database.SaveResult[] results = new SQX_DB().continueOnError().op_insert(new List<SQX_Audit__c> {internalAudit, customerAudit, supplierAudit}, new List<Schema.SObjectField>{});

            //Assert : Audit will not be saved
            for(Database.SaveResult result : results) {
                System.assertEquals(false, result.isSuccess(), 'Error : ' + result.getErrors());
            }
        }
    }

    /**
    * Given: given audit record
    * When:  when migrate the audit record
    * Then:  then update audit result type
    */ 
    public static testmethod void givenAudit_WhenMigrate_ThenUpdateAuditResultType(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN);
        User standardUser = users.get(RESPONSE_SUBMITTER);
        
        //Give custom permission set of data migration to admin user
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);
        
        System.runAs(adminUser) {
            //Arrange: Create Audit
            SQX_Test_Audit audit12 = new SQX_Test_Audit(adminUser);
            audit12.audit.Title__c = 'Audit-12';
            audit12.setStatus(SQX_Audit.STATUS_OPEN);
            audit12.setStage(SQX_Audit.STAGE_IN_PROGRESS);
            audit12.audit.Start_Date__c = Date.Today();
            audit12.audit.End_Date__c = Date.Today().addDays(10);
            audit12.audit.Result_Type__c = null;
            audit12.audit.Audit_Result__c = null;
            
            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
            audit1.audit.Title__c = 'Audit-1';
            audit1.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit1.audit.Start_Date__c = Date.Today();
            audit1.audit.End_Date__c = Date.Today().addDays(10);
            audit1.audit.Result_Type__c = null;
            audit1.audit.Audit_Result__c = '';
            
            //Act: Run migration
            SQX_7_4_MigrateAuditStatusAndStage job = new SQX_7_4_MigrateAuditStatusAndStage();
            List<sObject> audLst=new List<sObject>();
            audLst.add(audit12.audit);
            audLst.add(audit1.audit);
            List<SQX_Audit__c> audList = job.SetAuditValuesToUpdate(audLst);
            
            //Assert: Ensured that audut result type updated
            for(SQX_Audit__c audit : audList){
                System.assertEquals(SQX_7_4_MigrateAuditStatusAndStage.RESULT_TYPE_DEFAULT, audit.Result_Type__c);
                System.assertEquals(SQX_7_4_MigrateAuditStatusAndStage.AUDIT_RESULT_DEFAULT, audit.Audit_Result__c);
            }            
        }
    }

    /**
    * Given: given checkout audit 
    * When:  when migrate the audit checkout record
    * Then:  then bypass validation rule
    */ 
    public static testmethod void givenCheckoutAudit_WhenMigrate_ThenBypassValidationRule(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(ADMIN);
        User standardUser = users.get(RESPONSE_SUBMITTER);
        
        //Give custom permission set of data migration to admin user
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);
        
        System.runAs(adminUser) {
            //Arrange: Create Audit
            SQX_Test_Audit audit11 = new SQX_Test_Audit(adminUser);
            audit11.audit.Title__c = 'Audit-Checkout';
            audit11.audit.Start_Date__c = Date.Today();
            audit11.audit.End_Date__c = Date.Today().addDays(10);
            audit11.save();
            // checkout audit
            audit11.audit.Spreadsheet_Id__c ='1234567890';
            audit11.audit.SQX_Checked_Out_By__c = adminUser.Id;
            audit11.save(); 
            
            //Act: Run migration
            Test.startTest();
            SQX_7_4_MigrateAuditStatusAndStage job = new SQX_7_4_MigrateAuditStatusAndStage();
            ID batchprocessid = Database.executeBatch(job);
            Test.stopTest();
            
            //Assert : Ensured that bypass validation rule for checkout audit migration
            List<SQX_Audit__c> audt = [select Batch_Job_Status__c from sqx_audit__c where id =: audit11.audit.Id];
            system.assertEquals(SQX_7_4_MigrateAuditStatusAndStage.MIGRATION_COMPLETED, audt[0].Batch_Job_Status__c);
        }
    }
}