/*
 * unit test for Supplier Escalation record activities
 */

@isTest
public class SQX_Test_Supplier_Esc_Record_Activity {
    public SQX_Supplier_Escalation__c supplierEscalation;
    private static SQX_DB db = new SQX_DB();
    
    public static final String ACTIVITY_CODE_ESCALATE = 'escalate',
        					   ACTIVITY_CODE_DEESCALATE = 'deescalate';
    
    public static final String STAGE_ESCALATED = 'Escalated',
        					   STAGE_DEESCALATED = 'De-escalated';
    
    
    static final String ACCOUNT_NAME = 'SQX_Test_Supplier_Escalation_Account';
    
    @testSetup
    public static void commonSetup(){
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser, adminUser});
        
        System.runAs(adminUser) {
            // create auto number record for supplier escalation
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Escalation',
                Next_Number__c = 1,
                Number_Format__c = 'ESC-{1}',
                Numeric_Format__c = '0000'
            );
        }
        
        System.runAs(standardUser) {
            // create account record
            List<Account> accounts = new List<Account>();
            accounts.add(new Account(
            	Name = ACCOUNT_NAME 
            ));
            accounts.add(new Account(
            	Name = ACCOUNT_NAME 
            ));    
            insert accounts;
            
            //Create contact for Account
            List<Contact> contacts = new List<Contact>();
            contacts.add(new Contact(
            	FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = accounts[0].Id,
                Email = System.now().millisecond() + 'test@test.com'
            ));
            contacts.add(new Contact(
            	FirstName = 'John',
                Lastname = 'Wayne',
                AccountId = accounts[1].Id,
                Email = System.now().millisecond() + 'test@test.com'
            ));
            insert contacts;
        }
    }
    
    /*
     * returns account record created from commonSetup()
     */
    private static List<Account> getAccountAndContactRecords(){
        return [SELECT Id, Name, (SELECT Id, Name FROM Contacts) FROM Account WHERE Name =: ACCOUNT_NAME];
    }
    
    /*
     * Given: Supplier Escalation record
     * When: Record is processed
     * Then: Capture record activity
     */
     
    public static testMethod void givenSupplierEscalation_WhenRecordProcessed_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser, adminUser});
        System.runAs(adminUser){
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', new User(Id = UserInfo.getUserId()), 1);
        }
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            //Act: Submit the record
            supEsc.submit();   
            
            //Arrange: Save the record
            supEsc.supplierEscalation.Activity_Code__c = null;
            supEsc.supplierEscalation.Rationale_for_Escalation__c = 'Test Rationale';
            supEsc.save();
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Arrange: Initiate the record
            supEsc.initiate();
            
            //Arrange: Complete the record
            supEsc.complete();
            
            //Arrange: Escalate the record
            supEsc.escalate('2');
            
            //Assert: Capture submit record activity
            List<SQX_Supplier_Escalation_Record_Activity__c> escRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Escalation_Record_Activity__c WHERE SQX_Supplier_Escalation__c =: supEsc.supplierEscalation.Id Order By CreatedDate Asc];
            System.assertEquals(4, escRecordActivity.Size(), 'Supplier Escalation record activity must be captured.');
            System.assertEquals(Label.SQX_PS_ESCALATION_SUBMITTING_THE_RECORD, escRecordActivity.get(0).Activity__c, 'Expected activity to be properly saved when supplier escalation is submitted.');
            System.assertEquals(Label.SQX_PS_ESCALATION_INITIATING_THE_RECORD, escRecordActivity.get(1).Activity__c, 'Expected activity to be properly saved when supplier escalation is initiated.');
            System.assertEquals(Label.SQX_PS_ESCALATION_COMPLETING_THE_RECORD, escRecordActivity.get(2).Activity__c, 'Expected activity to be properly saved when supplier escalation is completed.');
            System.assertEquals(Label.SQX_PS_ESCALATION_ESCALATING_THE_RECORD, escRecordActivity.get(3).Activity__c, 'Expected activity to be properly saved when supplier escalation is escalated.');                        
        }
    }
    
    /*
     * Given: Supplier Escalation record
     * When: Record is De-escalated
     * Then: Capture record activity
     */
    public static testMethod void givenSupplierEscalation_WhenRecordDeescalated_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser, adminUser});
        System.runAs(adminUser){
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', new User(Id = UserInfo.getUserId()), 1);
        }
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            //Act: Submit the record
            supEsc.submit();   
            
            //Arrange: Save the record
            supEsc.supplierEscalation.Activity_Code__c = null;
            supEsc.supplierEscalation.Rationale_for_Escalation__c = 'Test Rationale';
            supEsc.save(); 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Arrange: Initiate the record
            supEsc.initiate();
            
            //Arrange: Complete the record
            supEsc.complete();
            
            //Arrange: Descalate the record
            supEsc.deescalate('0');

            //Assert: Capture submit record activity
            List<SQX_Supplier_Escalation_Record_Activity__c> escRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Escalation_Record_Activity__c WHERE SQX_Supplier_Escalation__c =: supEsc.supplierEscalation.Id Order By CreatedDate Asc];
            //System.assert(false, escRecordActivity);
            System.assertEquals(4, escRecordActivity.Size(), 'Supplier Escalation record activity must be captured.');
            System.assertEquals(Label.SQX_PS_ESCALATION_DE_ESCALATING_THE_RECORD, escRecordActivity.get(3).Activity__c, 'Expected activity to be properly saved when supplier escalation is de-escalated.');                        
        }
    }

    /*
     * Given: Supplier Escalation record
     * When: Record is restarted
     * Then: Capture record activity
     */
    public static testMethod void givenSupplierEscalation_WhenRecordRestarted_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', new User(Id = UserInfo.getUserId()), 1);
        }
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            //Act: Submit the record
            supEsc.submit();   
            
            //Arrange: Save the record
            supEsc.supplierEscalation.Activity_Code__c = null;
            supEsc.supplierEscalation.Rationale_for_Escalation__c = 'Test Rationale';
            supEsc.save(); 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Arrange: Initiate the record
            supEsc.initiate();
            
            //Arrange: Complete the record
            supEsc.complete();
            
            //Arrange: Restart the record
            supEsc.restart();

            //Assert: Capture restart record activity
            List<SQX_Supplier_Escalation_Record_Activity__c> escRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Escalation_Record_Activity__c WHERE SQX_Supplier_Escalation__c =: supEsc.supplierEscalation.Id Order By CreatedDate Asc];
            System.assertEquals(4, escRecordActivity.Size(), 'Supplier Escalation record activity must be captured.');
            System.assertEquals(Label.SQX_PS_ESCALATION_RESTARTING_THE_RECORD, escRecordActivity.get(3).Activity__c, 'Expected activity to be properly saved when supplier escalation is de-escalated.');                        
        }
    }
}