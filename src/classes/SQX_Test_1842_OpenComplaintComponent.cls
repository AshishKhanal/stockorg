/**
 * test class to ensure that the home page component to display open complaint is working correctly
 * @author: Sagar Shrestha
 * @date: 2016-01-04
 * @story: [SQX-1842] 
 */
@isTest
public class SQX_Test_1842_OpenComplaintComponent{ 
    static boolean runAllTests = true,
                   givenComplaints_OpenorDraftComplaintListed = false;

    /**
     * Scenario 1
     * Action: Complaint is created, with draft status
     * Expected: Created complaint is not listed by open complaint
     *
     * Scenario 2
     * Action: Another complaint is created, and the status is changed to triage
     * Expected: Complaint with triage state should be listed
     *
     * Scenario 3
     * Action: Another complaint is created, and the status is changed to open
     * Expected: Complaint with open state should be listed
     *
     * @author: Sagar Shrestha
     * @date: 2016-01-04
     */
    public testmethod static void givenComplaints_OpenorDraftComplaintListed(){
        if(!runAllTests && !givenComplaints_OpenorDraftComplaintListed){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange1 : Save a complaint
            SQX_Test_Complaint draftComplaint = new SQX_Test_Complaint(adminUser).save();
            SQX_Controller_OpenComplaints openComplaints =  new SQX_Controller_OpenComplaints();

            //Act1 : get all open complaints
            List<SQX_Complaint__c> openOrTriageComplaints = openComplaints.getOpenComplaints();
            //Assert 1 : Expected list to be empty as there is only one complaint in draft state
            System.assertEquals(0, openOrTriageComplaints.size(), 'Expected list to be empty as there is only one complaint in draft state');


            //Arrange2 : Save a complaint with traige status
            SQX_Test_Complaint triageComplaint = new SQX_Test_Complaint(adminUser);
            triageComplaint.complaint.Record_Stage__c = SQX_Complaint.STAGE_TRIAGE;
            triageComplaint.save();

            //Act2 : get all open complaints
            openOrTriageComplaints = openComplaints.getOpenComplaints();

            //Assert 2 : Expected list to have one complaint as there is only one complaint in triage state
            System.assertEquals(1, openOrTriageComplaints.size(), 'Expected list to have one complaint as there is only one complaint in triage state');
            //Get set of Ids in the openOrTriageComplaints Complaints
            Set<Id> openOrTriageComplaintIds = (new Map<Id,SObject>(openOrTriageComplaints)).keySet();
            System.assertEquals(true, openOrTriageComplaintIds.contains(triageComplaint.complaint.Id), 'Expected triage complaint to be in the list');


            //Arrange2 : Save a complaint with traige status
            SQX_Test_Complaint openComplaint = new SQX_Test_Complaint(adminUser);
            openComplaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            openComplaint.save();

            //Act2 : get all open complaints
            openOrTriageComplaints = openComplaints.getOpenComplaints();

            //Assert 2 : Expected list to have two complaint as there is only one complaint in triage state and one in open state
            System.assertEquals(2, openOrTriageComplaints.size(), 'Expected list to have two complaint as there is only one complaint in triage state and one in open state');
            //Get set of Ids in the openOrTriageComplaints Complaints
            openOrTriageComplaintIds = (new Map<Id,SObject>(openOrTriageComplaints)).keySet();
            System.assertEquals(true, openOrTriageComplaintIds.contains(openComplaint.complaint.Id), 'Expected open complaint to be in the list');



        }
    }
}