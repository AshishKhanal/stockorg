/**
* This is the extension used by audit program view/edit overriden page. It contains various
* properties and methods that are required for the page. 
*/
public with sharing class SQX_Extension_Audit_Program extends SQX_Extension_UI{
    
    /**
    * A static list of supported next actions
    */
    public static   String  NXTACT_SAVE = 'save',
                            NXTACT_CLOSE = 'close',
                            NXTACT_VOID = 'void',
                            NXTACT_APPROVE_APPROVAL = 'approveauditprogram',
                            NXTACT_REJECT_APPROVAL = 'rejectauditprogram',
                            NXTACT_APPROVE_REJECT_REQUEST = 'approverejectrequest';
    
    public SQX_Audit_Program__c mainRecord;
    ApexPages.StandardController auditProgramController;
    
    /**
    * Default constructor that intializes the redirector
    */
    public SQX_Extension_Audit_Program(ApexPages.StandardController auditProgramController) {
        
        super(auditProgramController, new List<String> {'Status__c'});
        this.auditProgramController = auditProgramController;
        mainRecord = (SQX_Audit_Program__c)auditProgramController.getRecord();
        
        this.purposeOfSigMap.putAll(new Map<String, String>{
                NXTACT_SAVE => Label.SQX_PS_Audit_Program_Saving_Audit_Program,
                NXTACT_CLOSE => Label.SQX_PS_Audit_Program_Closing_Audit_Program,
                NXTACT_VOID => Label.SQX_PS_Audit_Program_Voiding_Audit_Program,
                NXTACT_APPROVE_APPROVAL => Label.SQX_PS_Audit_Program_Approving_Audit_Program,
                NXTACT_REJECT_APPROVAL => Label.SQX_PS_Audit_Program_Rejecting_Audit_Program,
                NXTACT_APPROVE_REJECT_REQUEST => Label.SQX_PS_Audit_Program_Approving_Rejecting_Audit_Program
            });

        this.addActionSaveAfterDraft();
        //fetch the record type
        this.fetchRecordTypesOf.add(SQX.ControlledDoc);

        //we don't need audit esig validation if audit program is in draft state.
        Boolean isRecInDraftOrNew = !(mainRecord.Id != null && mainRecord.Status__c != SQX_Audit_Program.STATUS_DRAFT);
        addStaticPolicy('save', getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.AuditProgram));
        //We don't need to show separate esig policies for approve, reject. SO,Combine them in one commone policy i.e. approve/reject
        Map<String, SQX_CQ_Esig_Policies__c> allPolicies = getEsigPolicy();
        SQX_CQ_Esig_Policies__c approveRejectPolicy = allPolicies.get('approverejectrequest');
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Boolean esigApproval, esigComment = false;
        if (approveRejectPolicy != null) {
            esigApproval = approveRejectPolicy.Required__c;
            esigComment = approveRejectPolicy.Comment__c;
        } else {
            esigApproval = instance.Enabled__c;
            esigComment = false;
        }
        addStaticPolicy(NXTACT_APPROVE_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
        addStaticPolicy(NXTACT_REJECT_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
    }
    
    /**
    * Defines the list of entities whose data are to be exported along with the main record type
    */
    public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){
        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        return super.getDataLoaderConfig()
                                    .followChild(SQX_Audit__c.SQX_Audit_Program__c)
                                    .followChild(SQX_Audit_Target__c.SQX_Audit__c)
                                    .followChild(SQX_Audit_Program_Scope__c.SQX_Audit_Program__c)
                                    .followChild(SQX_Audit_Program_Record_Activity__c.SQX_Audit_Program__c)
                                    .followChild(SQX_Audit_Team__c.SQX_Audit__c)  

                                    .getFieldsFor(SQX_Controlled_Document__c.getSObjectType(), new List<SObjectField> {SQX_Controlled_Document__c.Name, SQX_Controlled_Document__c.Title__c})
            
                                    /*Approval supported for*/
                                    .followApprovalOf(globalDescribe.get(SQX.AuditProgram))
            
                                    /*Attachments supported for*/
                                    .followAttachmentOf(globalDescribe.get(SQX.AuditProgram))
                                    
                                    /*Notes supported for*/
                                    .followNoteOf(globalDescribe.get(SQX.AuditProgram));
    }
    
    /**
    * Default remote action for the extension to send any data that needs to be persisted or when certain next action is to be performed on the main record
    * @param auditProgramId the Id of the audit program record
    * @param changeset the changes that are to be saved
    * @param objectsToDelete the list of ids that are to be removed from SF
    * @param params the list of extra params such as next action is to be used for processing
    * @param esigData the electronic signature data that must be submitted inorder to process the record
    */
    @RemoteAction
    public static String processChangeSetWithAction(String auditProgramId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData)
    {
        boolean isNewRecord = !SQX_Upserter.isValidID(auditProgramId);
        SQX_Audit_Program__c  auditProgram;
        SQX_Extension_Audit_Program auditProgramExtension;

        if(isNewRecord){
            auditProgram = new SQX_Audit_Program__c();
        }
        else{
            auditProgram = [SELECT Id, Status__c FROM SQX_Audit_Program__c WHERE Id = : auditProgramId];
        }

        //passing expectation because the expectation will store the persisted object in the transaction.
        //this way we can generate the correct content for audit report or send it for approval.
        SQX_Extension_UI.BaseUpsertExpectation expectation = new SQX_Extension_UI.BaseUpsertExpectation(SQX.AuditProgram, auditProgramId);

        auditProgramExtension = new SQX_Extension_Audit_Program(new ApexPages.StandardController(auditProgram));
        SQX_Upserter.SQX_Upserter_Config config =  new SQX_Upserter.SQX_Upserter_Config()
                                                                   .omit(SQX.AuditProgram, SQX_Audit_Program__c.Status__c)
                                                                   .omit(SQX.Audit, SQX_Audit__c.Status__c);
        String mainRecordId =  auditProgramExtension.executeTransaction(auditProgramId, changeset, objectsToDelete, params, esigData, config, null);

        return mainRecordId;
    } 
    
    /**
    * Performs the next action after the save/default action has taken place
    */
    public override void processNextAction(Map<String, String> params){
        SQX_Audit_Program__c currentAuditProgram = (SQX_Audit_Program__c) mainRecord;

        //perform next action
        if(NXTACT_APPROVE_APPROVAL.equalsIgnoreCase(nextAction) || NXTACT_REJECT_APPROVAL.equalsIgnoreCase(nextAction)){
            boolean isApprove = NXTACT_APPROVE_APPROVAL.equalsIgnoreCase(nextAction);
            Id workItemId = params.get('workItemId');
            ProcessInstanceWorkitem item = SQX_Approval_Util.getProcessInstanceWorkitem(workItemId);
            if (item == null) {
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_APPROVAL_REQUEST_ITEM_NOT_AVAILABLE_FOR_APPROVAL);
            } else {
                approveRecord(isApprove, currentAuditProgram.Id, (String)params.get('remark'), item.OriginalActorId);
            }
        }
        else if(NXTACT_CLOSE.equalsIgnoreCase(nextAction)){
            SQX_Audit_Program__c auditProgram = new SQX_Audit_Program__c();
            auditProgram = [SELECT Id, Status__c, Title__c
                        FROM SQX_Audit_Program__c
                        WHERE Id =: currentAuditProgram.Id];
                        
            //update status to close only if not already closed
            if(auditProgram.Status__c != SQX_Audit_Program.STATUS_CLOSED){
                auditProgram.Status__c = SQX_Audit_Program.STATUS_CLOSED;
                 new SQX_DB().op_update(new List<SQX_Audit_Program__c>{auditProgram}, new List<Schema.SObjectField>{
                    Schema.SQX_Audit_Program__c.Status__c
                });
            }
            else{
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_AUDIT_PROGRAM_IS_ALREADY_CLOSED);
            }
        }else if(NXTACT_VOID.equalsIgnoreCase(nextAction)){
            SQX_Audit_Program__c auditProgram = new SQX_Audit_Program__c();
            auditProgram = [SELECT Id, Status__c, Title__c
                        FROM SQX_Audit_Program__c
                        WHERE Id =: currentAuditProgram.Id];
                        
            //update status to void only if not already void
            if(auditProgram.Status__c != SQX_Audit_Program.STATUS_VOID){
                auditProgram.Status__c = SQX_Audit_Program.STATUS_VOID;
                 new SQX_DB().op_update(new List<SQX_Audit_Program__c>{auditProgram}, new List<Schema.SObjectField>{
                    Schema.SQX_Audit_Program__c.Status__c
                });
            }
            else{
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_AUDIT_PROGRAM_IS_ALREADY_VOID);
            }
        }
    } 
}