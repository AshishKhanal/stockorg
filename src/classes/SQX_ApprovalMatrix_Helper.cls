/**
* This class contains the logic for copying approval matrix steps from approval matrix to change order
*/
global with sharing class SQX_ApprovalMatrix_Helper {

    final static Map<SObjectType, SObjectType> RECORD_ACTIVITY_OBJECTTYPE = new Map<SObjectType, SObjectType>{
        SQX_Change_Order__c.SObjectType => SQX_Change_Order_Approval__c.SObjectType,
        SQX_Controlled_Document__c.SObjectType => SQX_Controlled_Document_Approval__c.SObjectType
    };

    /**
     * This method copies the approval matrix steps to the change orders provided in the list.
     * @param changeOrders the list of change orders whose approval matrix step is to be set.
     */
    @InvocableMethod(Label='Copy approval matrix\'s steps to Change Orders')
    global static void copyApprovalMatrixStepsToCO(List<SQX_Change_Order__c> changeOrders){
        copyApprovalMatrixSteps(changeOrders);
    }

    /**
     * this method copies approval matrix step to the related object approval steps
     */
    public static void copyApprovalMatrixSteps(List<SObject> recordsForApproval){
        final Map<SObjectType, List<SObjectField>> approvalObjectFields = new Map<SObjectType, List<SObjectField>> {
            SQX_Controlled_Document__c.SObjectType => new List<SObjectField> { SQX_Controlled_Document_Approval__c.SQX_Controlled_Document__c, 
                                                                                SQX_Controlled_Document_Approval__c.Step__c, 
                                                                                SQX_Controlled_Document_Approval__c.SQX_Job_Function__c, 
                                                                                SQX_Controlled_Document_Approval__c.SQX_User__c },
                                                                            
            SQX_Change_Order__c.SObjectType => new List<SObjectField> {SQX_Change_Order_Approval__c.SQX_Change_Order__c, 
                                                                        SQX_Change_Order_Approval__c.Step__c, 
                                                                        SQX_Change_Order_Approval__c.SQX_Job_Function__c, 
                                                                        SQX_Change_Order_Approval__c.SQX_User__c }
        };
        
        final String APPROVAL_MATRIX_FIELD = 'SQX_Approval_Matrix__c',
                     STEP_FIELD = 'Step__c',
                     JOB_FUNCTION_FIELD = 'SQX_Job_Function__c',
                     USER_FIELD = 'SQX_User__c';
            
            
        if(recordsForApproval == null || recordsForApproval.size() == 0)
            return;

        Set<Id> approvalMatrixIds = new Set<Id>();
        List<SObjectField> fieldSet = new List<SobjectField>();

        Id recordId = recordsForApproval[0].Id;
        SObjectType mainRecordType = recordId.getSObjectType(),
                    recordApprovalType;
        
        String objectTypeName = mainRecordType.getDescribe().getName();
        if(!RECORD_ACTIVITY_OBJECTTYPE.containsKey(mainRecordType)){
            throw new SQX_ApplicationGenericException('Unsupported record type activity for object of type ' + mainRecordType);
        }

        recordApprovalType = RECORD_ACTIVITY_OBJECTTYPE.get(mainRecordType);

        for (SObject recordForApproval : recordsForApproval) {
            if(recordForApproval.get(APPROVAL_MATRIX_FIELD) != null)
                approvalMatrixIds.add((Id) recordForApproval.get(APPROVAL_MATRIX_FIELD));
        }
        
        if(recordsForApproval.size()  > 0 && approvalMatrixIds.size() > 0){
        	// get approval matrix steps and its job functions
            Map<Id, SQX_Approval_Matrix__c> approvalMatrices = new Map<Id, SQX_Approval_Matrix__c>(
                [SELECT Id, (SELECT Id, Name, SQX_Approval_Matrix__c, Step__c, SQX_Job_Function__c,SQX_User__c
                             FROM SQX_Approval_Matrix_Steps__r
                             ORDER BY Step__c, SQX_Job_Function__r.Name)
                 FROM SQX_Approval_Matrix__c
                 WHERE Id = :approvalMatrixIds]
            );
            
            // get all job functions
            Set<Id> jobFunctionIds = new Set<Id>();
            for (Id amId : approvalMatrices.keySet()) {
                for (SQX_Approval_Matrix_Step__c amStep : approvalMatrices.get(amId).SQX_Approval_Matrix_Steps__r) {
                    jobFunctionIds.add(amStep.SQX_Job_Function__c);
                }
            }
            
            // get active users for job functions
            Map<Id, List<User>> jfApprovers = SQX_Job_Function.getActiveUsersForJobFunctions(jobFunctionIds, null, null);
            List<SObject> newItems = new List<SObject>();
            
            for (SObject recordForApproval : recordsForApproval) {
            
                // create new change order approvals for each change order
                for (SQX_Approval_Matrix_Step__c amStep : approvalMatrices.get((Id) recordForApproval.get(APPROVAL_MATRIX_FIELD)).SQX_Approval_Matrix_Steps__r) {
                    Id approverId;
                    SObject approvalStep = recordApprovalType.newSObject();
                    
                    if(amStep.SQX_User__c != null){
                        approverId = amStep.SQX_User__c;
                    }
                    else if (jfApprovers.get(amStep.SQX_Job_Function__c) != null && jfApprovers.get(amStep.SQX_Job_Function__c).size() == 1) {
                        // select the single active user for a job function as approver
                        approverId = jfApprovers.get(amStep.SQX_Job_Function__c).get(0).Id;
                    }

                    if(objectTypeName == SQX.ControlledDoc){
                        approvalStep.put('SQX_Controlled_Document__c', recordForApproval.Id);

                    }else if(objectTypeName == SQX.ChangeOrder){
                        approvalStep.put('SQX_Change_Order__c', recordForApproval.Id);

                    }
                    // create new change order approval with proper values
                    approvalStep.put(STEP_FIELD, amStep.Step__c);
                    approvalStep.put(JOB_FUNCTION_FIELD, amStep.SQX_Job_Function__c);
                    approvalStep.put(USER_FIELD, approverId);

                    // add to new items
                    newItems.add(approvalStep);
                }
            }
            
            if (newItems.size() > 0) {
                //Without sharing has been used to give the approver modify all access to the record so the approver can add or delete approvers 
                new SQX_DB().withoutSharing().op_insert( newItems, approvalObjectFields.get(mainRecordType));
            }
        }
    }
    
    /*
    * returns all approver ids (not null) per approval step for each object ids
    * @param objIds list of object ids (same SObjectType)
    * @return   returns all approver ids (not null) per approval step
    */
    public static Map<Id, Map<Integer, List<Id>>> getAllStepApprovers(List<Id> objIds) {
        Map<Id, Map<Integer, List<Id>>> allStepApprovers = new Map<Id, Map<Integer, List<Id>>>();
        
        SObjectType mainRecordType = objIds[0].getSObjectType();
        
        if (!RECORD_ACTIVITY_OBJECTTYPE.containsKey(mainRecordType)) {
            throw new SQX_ApplicationGenericException('Unsupported record type activity for object of type ' + mainRecordType);
        }
        
        if (mainRecordType == SQX_Change_Order__c.SObjectType) {
            // get all CO approvals with valid step and user values
            Map<Id, SQX_Change_Order__c> coApprovals = new Map<Id, SQX_Change_Order__c>([
                SELECT Id, (SELECT Id, Name, SQX_Change_Order__c, Step__c, SQX_User__c
                            FROM SQX_Change_Order_Approvals__r
                            WHERE Step__c != null AND SQX_User__c != null
                            ORDER BY Step__c, SQX_User__r.Name, SQX_User__c)
                FROM SQX_Change_Order__c
                WHERE Id = :objIds
            ]);
            
            // generate return list for each CO
            for (SQX_Change_Order__c co : coApprovals.values()) {
                Map<Integer, List<Id>> stepApprovers = new Map<Integer, List<Id>>();
                
                // add step with assigned approvers
                for (SQX_Change_Order_Approval__c apl : co.SQX_Change_Order_Approvals__r) {
                    Integer stepNum = (Integer)apl.Step__c;
                    
                    // add approvers to corresponding step
                    if (stepApprovers.containsKey(stepNum)) {
                        stepApprovers.get(stepNum).add(apl.SQX_User__c);
                    }
                    else {
                        stepApprovers.put(stepNum, new List<Id>{ apl.SQX_User__c });
                    }
                }
                
                // add all step approvers for each CO
                allStepApprovers.put(co.Id, stepApprovers);
            }
        }
        else if (mainRecordType == SQX_Controlled_Document__c.SObjectType) {
            // get all controlled document approvals with valid step and user values
            Map<Id, SQX_Controlled_Document__c> docApprovals = new Map<Id, SQX_Controlled_Document__c>([
                SELECT Id, (SELECT Id, Name, SQX_Controlled_Document__c, Step__c, SQX_User__c
                            FROM SQX_Controlled_Document_Approvals__r
                            WHERE Step__c != null AND SQX_User__c != null
                            ORDER BY Step__c, SQX_User__r.Name, SQX_User__c)
                FROM SQX_Controlled_Document__c
                WHERE Id = :objIds
            ]);
            
            // generate return list for each controlled document
            for (SQX_Controlled_Document__c doc : docApprovals.values()) {
                Map<Integer, List<Id>> stepApprovers = new Map<Integer, List<Id>>();
                
                // add step with assigned approvers
                for (SQX_Controlled_Document_Approval__c apl : doc.SQX_Controlled_Document_Approvals__r) {
                    Integer stepNum = (Integer)apl.Step__c;
                    
                    // add approvers to corresponding step
                    if (stepApprovers.containsKey(stepNum)) {
                        stepApprovers.get(stepNum).add(apl.SQX_User__c);
                    }
                    else {
                        stepApprovers.put(stepNum, new List<Id>{ apl.SQX_User__c });
                    }
                }
                
                // add all step approvers for each controlled document
                allStepApprovers.put(doc.Id, stepApprovers);
            }
        }
        
        return allStepApprovers;
    }
    
    /*
    * set all approver fields defined in field set to list the user lookup fields used in approval process of the given record with given approvers
    * if given approver list is empty or null, then clears all approver fields
    * if total number of given approvers is lower than approver fields to set then first approver is used for remaining fields
    * @param obj            record to set step approvers
    * @param approverIds    list of approvers for same step to set in approver fields. note: this list is assumed to have valid ids or to be empty or null
    * @return   returns given object with approver fields set using given approvers
    */
    public static SObject setStepApproverFields(SObject obj, List<Id> approverIds) {
        SObjectType mainRecordType = obj.getSObjectType();
        Schema.FieldSetMember[] stepApproverFields;
        
        if (!RECORD_ACTIVITY_OBJECTTYPE.containsKey(mainRecordType)) {
            throw new SQX_ApplicationGenericException('Unsupported record type activity for object of type ' + mainRecordType);
        }
        
        if (mainRecordType == SQX_Change_Order__c.SObjectType) {
            stepApproverFields = SObjectType.SQX_Change_Order__c.fieldSets.Approval_Matrix_Step_Approver_Fields.fields;
        }
        else if (mainRecordType == SQX_Controlled_Document__c.SObjectType) {
            stepApproverFields = SObjectType.SQX_Controlled_Document__c.fieldSets.Approval_Matrix_Step_Approver_Fields.fields;
        }
        
        for (Integer i = 0; i < stepApproverFields.size(); i++) {
            Id approverId = null;
            if (approverIds != null && approverIds.size() > 0) {
                if (i < approverIds.size()) {
                    approverId = approverIds[i]; // set corresponding approver id
                }
                else {
                    approverId = approverIds[0]; // set first approver when total approvers to set is lower than approver fields
                }
            }
            
            try {
                // set approver field with approver user id
                obj.put(stepApproverFields[i].fieldPath, approverId);
            }
            catch (Exception ex) {
                // ignoring errors as approval fieldset must be setup with proper user lookup fields
                // that is used in active approval processes using approval matrix
            }
        }
        
        return obj;
    }
}