/**
* unit test for supplier deviation record to initiate
*/
@isTest
public class SQX_Test_5760_Initiate_SD {
    final static String STANDARD_USER = 'standardUser',
                        STANDARD_USER1 = 'standardUser1';
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER1);
        
        // add users to queue
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User>{standardUser, standardUser1, adminUser});
    }
    
     /**
     * Given : Supplier deviation record with status 'Draft' and stage 'Triage'
     * When : Supplier deviation is initiated
     * Then : Supplier deviation should successfully initiated and status is changed to 'Open' and stage is changed to 'In Progress'
     */
    public static testMethod void givenSDRecord_WhenInitiateTheRecordStatusIsDraftAndStageIsTriage_ThenInitiate(){        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            // Arrange: Create supplier deviation record
            SQX_Supplier_Deviation__c sd = new SQX_Test_Supplier_Deviation().save().sd;
            
            // Act: Try to initiate Supplier deviation
            sd.Status__c = SQX_Supplier_Deviation.STATUS_OPEN;
            sd.Record_Stage__c = SQX_Supplier_Deviation.STAGE_IN_PROGRESS;
            sd.Activity_Code__c = 'initiate';
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__c> {sd}, new List<SObjectField>{});
            
            // Assert : Ensure nsi is not initiated
            System.assert(result[0].isSuccess() == false, 'Expected : Supplier Record should not be initiated when status is draft and stage is draft. Actual : Supplier Introduction is initiated');
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,result[0].getErrors().get(0).getMessage());

            // Submitting the Supplier deviation record
            sd.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            sd.Record_Stage__c = SQX_Supplier_Deviation.STAGE_TRIAGE;
            sd.Activity_Code__c = 'submit';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__c>{ sd }, new List<SObjectField>{});
            
            // Verify the status and stage is draft and triage respectively
            sd = [SELECT Id, Status__c, Record_Stage__c FROM SQX_Supplier_Deviation__c WHERE Id = :sd.Id];
            System.assertEquals(SQX_Supplier_Deviation.STATUS_DRAFT, sd.Status__c);
            System.assertEquals(SQX_Supplier_Deviation.STAGE_TRIAGE, sd.Record_Stage__c);
            
            // Act: Try to initiate NSI
            sd.Status__c = SQX_Supplier_Deviation.STATUS_OPEN;
            sd.Record_Stage__c = SQX_Supplier_Deviation.STAGE_IN_PROGRESS;
            sd.Activity_Code__c = 'initiate';
            List<Database.SaveResult> result1 = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__c> {sd}, new List<SObjectField>{});
            
            // Assert : Ensure supplier deviation is initiated
            System.assert(result1[0].isSuccess() == true, 'Expected : Supplier Deviation should be initiated as status is draft and stage is triage. Actual : Supplier Deviation is not initiated. Actual error: ' + result1[0].getErrors());
        }
    }
    
    /**
     * GIVEN : Supplier Deviation record
     * WHEN : Record is initiated
     * THEN : Owner of the record is changed to current user
     * @story : [SQX-6087]
     */ 
    public static testmethod void givenSDRecord_WhenRecordIsInitiated_ThenRecordOwnerChangesCurrentUser(){
        
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User adminUser = userMap.get('adminUser');
        User stdUser = userMap.get(STANDARD_USER);
        User assigneeUser = userMap.get(STANDARD_USER1);

        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 1);
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Supplier_Deviation.TASK_TYPE_APPROVAL, assigneeUser, 2);
        }
        
        // ARRANGE : Create Supplier Deviation record
        SQX_Test_Supplier_Deviation supplierDeviation = new SQX_Test_Supplier_Deviation();
        supplierDeviation.save();
        
        // Provide edit permission to another user.
        insert new SQX_Supplier_Deviation__Share(
            ParentId = supplierDeviation.sd.Id,
            UserOrGroupId = stdUser.Id,
            AccessLevel = 'Edit'
        );
        
        // Update the stage of the record
        supplierDeviation.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
        supplierDeviation.save();
        
        // ACT : Initiate the record
        System.runAs(stdUser){
            supplierDeviation.initiate();
        }
        
        // ASSERT : Ensure record owner is changes to current user
        SQX_Supplier_Deviation__c retSD = [SELECT Id, OwnerId FROM SQX_Supplier_Deviation__c WHERE Id = :supplierDeviation.sd.Id].get(0);
        System.assertEquals(stdUser.Id, retSD.OwnerId);
    }
    
    /**
     * GIVEN : Supplier Deviation record is created with two deviation process
     * WHEN : Record is initiated
     * THEN : Only applicable onboarding should have task created
     * @story : [SQX-6478]
     */ 
    public static testmethod void givenSDRecordWithDeviationProcess_WhenRecordIsInitiated_ThenOnlyApplicableStepsShouldBeOpen(){
        
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User adminUser = userMap.get('adminUser');
        User stdUser = userMap.get(STANDARD_USER);
        User assigneeUser = userMap.get(STANDARD_USER1);

        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation.createPolicyTasks(2, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 1);
        }
        
        System.runAs(stdUser){
            // ARRANGE : Create Supplier Deviation record with two deviation process
            SQX_Test_Supplier_Deviation supplierDeviation = new SQX_Test_Supplier_Deviation();
            supplierDeviation.save();
            
            supplierDeviation.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            supplierDeviation.save();

            List<SQX_Deviation_Process__c> dpList = [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: supplierDeviation.sd.Id];
            System.assertEquals(2, dpList.size());

            dpList.get(0).Applicable__c = false;
            update dpList;
            
            // ACT : Initiate the record
            supplierDeviation.initiate();
            
            List<Task> sfTaskList = [SELECT Id, Child_What_Id__c FROM Task WHERE WhatId =: supplierDeviation.sd.Id];

            // ASSERT : Only applicable onboarding should have task created
            System.assertEquals(1, sfTaskList.size());
            System.assertEquals([SELECT Id FROM SQX_Deviation_Process__c WHERE Applicable__c = TRUE].Id, sfTaskList.get(0).Child_What_Id__c);
        }
    }
}