/**
* This is the Homepage CQ actions class. 
* @story   SQX-3159 Homepage UI and Framework Implementation
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-06-03
*/
public with sharing class SQX_Homepage_Item_Action {

    /*
        Holds the name of the action (displayed in the UI)
    */
    public String name { get; set; }

    /*
        Holds the url to perform the action
    */
    public String actionUrl { get; set; }

    /*
        Holds the icon identifier for the action
    */
    public String actionIcon { get; set; }

    /*
        Flag that determines whether the given action supports bulk action
    */
    public Boolean supportsBulk { get; set; }
    
    public String recordId { get; set; }
    
    public Boolean isLightningComponent{ get; set; }

    public String actionId {get; set;}

}