/**
*   @author : Piyush Subedi
*   @date : 2016/12/1
*   @description : Controller class for Close Collaboration page 
*/
global with sharing class SQX_Controller_Close_Collaboration {

    private String grpId;   // holds the id of the collaboration group

    private static final String PARAM_ID = 'Id';

    public static final String  CODE_OF_SIG_CLOSE_COLLABORATION = 'closecollaboration';

    Map<String, String> purposeOfSigMap = new Map<String, String>{
        CODE_OF_SIG_CLOSE_COLLABORATION => Label.SQX_PS_Close_Collaboration
    };

    
    global Map<String, String> newContentInfo { get; set; }  // info of the content stored in the group

    global Boolean hasCheckInPermission { get; set; }   // property holds a boolean value as to whether or not the current user has the permission to check in new content

    global SQX_Controlled_Document__c doc { get; set; } // property holds the controlled doc record of which the group is a part of
   
    global Boolean keepChanges {get; set;}  // property holds the flag for determining whether or not new content is to be checked in upon closure


    global SQX_Controller_Close_Collaboration(){
        this(ApexPages.currentPage().getParameters().get(PARAM_ID));
    }

    /**
    *   Constructor method
    *   @param grpId - Id of the collaboration group
    */
    global SQX_Controller_Close_Collaboration(String grpId){
        try{
            /* 
                getting 18 digit case insensitive id
            */
            this.grpId = (String) Id.valueOf(grpId);

            List<SQX_Controlled_Document__c> docs = [SELECT Id,
                                                            SQX_Checked_Out_by__c,
                                                            Collaboration_Group_Id__c,
                                                            Collaboration_Group_Content_Id__c,
                                                            Content_Reference__c,
                                                            Name,
                                                            Revision__c,
                                                            Title__c FROM SQX_Controlled_Document__c
                                                            WHERE Collaboration_Group_Id__c = :this.grpId ];                 
            if(docs.size() > 0){
                doc = docs.get(0);
                SQX_CollaborationProvider provider = SQX_Controlled_Document.createCollaborationProvider(doc);
                newContentInfo = provider.retrieveGroupContentInfo(doc.Collaboration_Group_Id__c, doc.Collaboration_Group_Content_Id__c);

                hasCheckInPermission = SQX_Controlled_Document_Collaboration.isUserCheckOutUser(doc);
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CQ_UI_Group_Not_Controlled_Document_Collaboration_Group));     
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));     
        }
    }

    /**
    *   Returns the title of the content in the group
    */
    global String getNewContentFileName(){
        if(newContentInfo == null)
            return null;
        return newContentInfo.get(SQX_SF_Collaboration_Provider.KEY_COLLABORATION_CONTENT_FILENAME);
    }

    /**
    *   Returns the primary content's file name stored in SF of the given controlled document
    */
    global String getPrimaryContentName(){

        /* Note : User who doesn't have access to the primary content won't be able to see the content name */
        String pcName;

        List<ContentVersion> cvs = [SELECT Title FROM ContentVersion WHERE ContentDocumentId = :doc.Content_Reference__c AND IsLatest = true];
        if( cvs.size() > 0 ){
            pcName = cvs.get(0).Title;
        }
        return pcName;
    }

    /**
    *   Method to archive collaboration group and check-in document from the group
    *   If, changes are to be kept, the new file is to be replaced as the primary content
    */
    global PageReference closeCollaboration(){
        SavePoint sp = null;
        
        SQX_CollaborationProvider provider = SQX_Controlled_Document.createCollaborationProvider(doc);
        
        try{
            ContentVersion newContent;

            if(keepChanges == true){
                newContent = provider.retrieveGroupContent(grpId, doc.Collaboration_Group_Content_Id__c);
            }
            
            // close collaboration group (possible callout)
            provider.closeCollaboration(grpId);
            sp = Database.setSavepoint();
            if( newContent != null ){
                SQX_Controlled_Document_Collaboration.checkIn(newContent, doc);
            }
            else{
                SQX_Controlled_Document_Collaboration.undoCheckOut(doc, false);
            }

            //finally add this acitivity to record
            saveRecordActivity();

            // info message for user
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.CQ_UI_Collaboration_Group_Closed));

        }catch(Exception ex){
            if(sp != null){
                Database.rollback(sp);
            }
            provider.undoCloseCollaboration(grpId);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , ex.getMessage()));
        }  
        return null;
    }

    /**
    *   Method to save record activity     
    */
    public void saveRecordActivity(){
        SQX_Record_History.insertRecordHistory(String.valueOf(SQX_Controlled_Document__c.SObjectType), (Id)doc.Id, doc.Id, Label.CQ_UI_Collaboration_Group_Closed, purposeOfSigMap.get(CODE_OF_SIG_CLOSE_COLLABORATION), CODE_OF_SIG_CLOSE_COLLABORATION);                              
    }

}