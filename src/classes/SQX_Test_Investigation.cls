@IsTest
public class SQX_Test_Investigation{    
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
   
    /**
     * GIVEN : An investigation
     * WHEN : Clone Investigation invocable method is called
     * THEN : Investigation is cloned
     * @story : [SQX-3611]
     * @date : 2017/06/21
     */
    public static testMethod void giveAnInvestigation_WhenCloned_ItIsCloned(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange : Create required investigation
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
                .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
                .setStatus(SQX_Finding.STATUS_DRAFT)
                .save();
            
            SQX_Investigation__c inv = new SQX_Investigation__c(SQX_Finding__c = finding.finding.Id, Investigation_Summary__c = 'Test');
            Database.insert(inv,false);
            
            // Act : Call clone investigation method
            SQX_Clone_Investigation.cloneInvestigation(new List<SQX_Investigation__c>{inv});
            
            // Assert : Investigation must be cloned
            System.assertEquals(2, [SELECT Id FROM SQX_Investigation__c WHERE SQX_Finding__c =: finding.finding.Id].size());
            
        }
    }
    
    /**
    * this test ensures that our status values are in sync with picklist values
    */
    public static testmethod void givenStatusEnsureItExistsInPickList(){
        String [] allStatuses = new String[] { SQX_Investigation.STATUS_DRAFT,
                                               SQX_Investigation.STATUS_IN_APPROVAL,
                                               SQX_Investigation.STATUS_PUBLISHED};
        
        // validate all statuses exists in picklist
        SQX_Test_Base_Class.validatePickListValuesMatch(allStatuses, SQX.Investigation, SQX.NSPrefix + 'Status__c');
        
        String [] allApprovalStatuses = new String[] { SQX_Investigation.APPROVAL_STATUS_NONE,
                                                        SQX_Investigation.APPROVAL_STATUS_PENDING,
                                                        SQX_Investigation.APPROVAL_STATUS_APPROVED,
                                                        SQX_Investigation.APPROVAL_STATUS_REJECTED };
        
        // validate all approval statuses exists in picklist
        SQX_Test_Base_Class.validatePickListValuesMatch(allApprovalStatuses, SQX.Investigation, SQX.NSPrefix + 'Approval_Status__c');
         
    }
    
    /**
    * this test ensures that given an investigation with action plan, root causes and investigation tools
    * we can clone/revise an investigation
    */
    public static testmethod void givenInvestigationEnsureRevisionWorks(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Investigation invWrapper = null;
    
            SQX_Investigation_Tool_Name__c toolName = new SQX_Investigation_Tool_Name__c(Name = 'Test Tool');
    
            insert toolName;
            
            SQX_Investigation__c investigation = prepareMockObject();
            
            insert investigation;
            
            invWrapper = new SQX_Investigation(investigation);
            SQX_Investigation__c clonedInvestigation = invWrapper.cloneInvestigationAndRelatedList();
            
            clonedInvestigation = [SELECT ID, 
                                   (SELECT ID FROM SQX_Action_Plans__r),
                                   (SELECT ID FROM SQX_Root_Causes__r),
                                   (SELECT ID FROM SQX_Investigation_Tools__r)
                                   FROM SQX_Investigation__c
                                   WHERE ID = : clonedInvestigation.ID LIMIT 1];
            
            System.assert(clonedInvestigation.SQX_Action_Plans__r.size() == 0,
                         'Expected no action plans but found ' + clonedInvestigation.SQX_Action_Plans__r.size() + ' action plans');
            
            System.assert(clonedInvestigation.SQX_Root_Causes__r.size() == 0,
                         'Expected no root causes but found ' + clonedInvestigation.SQX_Root_Causes__r.size() + ' root causes');
    
            System.assert(clonedInvestigation.SQX_Investigation_Tools__r.size() == 0,
                         'Expected no tools but found ' + clonedInvestigation.SQX_Investigation_Tools__r.size() + ' tools');
            
            SQX_Action_Plan__c actionPlan1 = new SQX_Action_Plan__c();
            actionPlan1.Due_Date__c = Date.today();
            actionPlan1.SQX_Investigation__c = investigation.ID;
            
            insert actionPlan1;
            
            SQX_Action_Plan__c actionPlan2 = new SQX_Action_Plan__c();
            actionPlan2.Due_Date__c = Date.today();
            actionPlan2.SQX_Investigation__c = investigation.ID;
            
            insert actionPlan2;
            
            SQX_Root_Cause__c rootCause = new SQX_Root_Cause__c();
            rootCause.SQX_Investigation__c = investigation.ID;
            
            insert rootCause;
            
            SQX_Investigation_Tool__c tools = new SQX_Investigation_Tool__c();
            tools.SQX_Investigation__c = investigation.ID;
            tools.Investigation_Tool_Method_Used__c = toolName.Id;
            
            insert tools;
            
            investigation.Status__c = SQX_Investigation.STATUS_IN_APPROVAL;
            update investigation;
            
            invWrapper = new SQX_Investigation(investigation);
            clonedInvestigation = invWrapper.cloneInvestigationAndRelatedList();
            
            clonedInvestigation = [SELECT ID, Status__c,
                                   (SELECT ID FROM SQX_Action_Plans__r),
                                   (SELECT ID FROM SQX_Root_Causes__r),
                                   (SELECT ID FROM SQX_Investigation_Tools__r)
                                   FROM SQX_Investigation__c
                                   WHERE ID = : clonedInvestigation.ID LIMIT 1];
                                   
           System.assert(clonedInvestigation.Status__c == SQX_Investigation.STATUS_DRAFT,
                   'Expected newly cloned investigation to have draft status but found ' + clonedInvestigation.Status__c);
            
            System.assert(clonedInvestigation.SQX_Action_Plans__r.size() == 2,
                         'Expected 2 action plans but found ' + clonedInvestigation.SQX_Action_Plans__r.size() + ' action plans');
            
            System.assert(clonedInvestigation.SQX_Root_Causes__r.size() == 1,
                         'Expected 1 root causes but found ' + clonedInvestigation.SQX_Root_Causes__r.size() + ' root causes');
    
            System.assert(clonedInvestigation.SQX_Investigation_Tools__r.size() == 1,
                         'Expected 1 tools but found ' + clonedInvestigation.SQX_Investigation_Tools__r.size() + ' tools');
            
            
            // ARRANGE (SQX-8211): add SQX_Evidence__c, SQX_Root_Cause_Code__c, SQX_Approved_Root_Cause__c
            SQX_Evidence__c evd = new SQX_Evidence__c( SQX_Finding__c = investigation.SQX_Finding__c );
            SQX_Root_Cause_Code__c rcCode = new SQX_Root_Cause_Code__c( Name = 'SQX_Test_8211_Root_Cause_Code' );
            SQX_Approved_Root_Cause__c apvdRootCause = new SQX_Approved_Root_Cause__c(
                SQX_Finding__c = investigation.SQX_Finding__c,
                SQX_Root_Cause__c = rootCause.Id
            );
            insert new List<SObject>{ evd, rcCode, apvdRootCause };
            
            // ACT (SQX-8211): update SQX_Root_Cause__c, SQX_Investigation_Tool_Name__c, SQX_Investigation_Tool__c, SQX_Evidence__c, SQX_Root_Cause_Code__c, SQX_Approved_Root_Cause__c
            update new List<SObject>{ rootCause, toolName, tools, evd, rcCode, apvdRootCause };
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Root_Cause__c, SQX_Investigation_Tool_Name__c, SQX_Investigation_Tool__c, SQX_Evidence__c, SQX_Root_Cause_Code__c, SQX_Approved_Root_Cause__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Root_Cause__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Investigation_Tool_Name__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Investigation_Tool__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Evidence__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Root_Cause_Code__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Approved_Root_Cause__c.SObjectType);
         }
        
    }
    
    //creates a investigation with finding with type CAPA
    public static SQX_Investigation__c prepareMockObject(){
        SQX_Investigation__c investigation = new SQX_Investigation__c();
        
        SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                           .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                           .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                           .setStatus(SQX_Finding.STATUS_DRAFT)
                           .save();  

        investigation.SQX_Finding__c= finding.finding.Id;
        
        investigation.Revision__c = 1;
        investigation.Investigation_Summary__c = 'Test investigation';  
        return investigation;
    }

      /*prepares a mock investigation with a finding as parameter
      * @param finding the finding which gets associated with the investigation
      *
      * @author Sagar Shrestha
      * @date 2014/5/12
      */

     public static SQX_Investigation__c prepareMockObject(SObject parent){
         Map<SObjectType, SObjectField> fields = new Map<SObjectType, SObjectField> {
            SQX_CAPA__c.SObjectType => SQX_Investigation__c.SQX_CAPA__c,
            SQX_Nonconformance__c.SObjectType => SQX_Investigation__c.SQX_Nonconformance__c,
            SQX_Complaint__c.SObjectType => SQX_Investigation__c.SQX_Complaint__c
        };
        SQX_Investigation__c investigation = new SQX_Investigation__c();
        investigation.put(fields.get(parent.getSObjectType()), parent.Id);
        investigation.Revision__c = 1;
        investigation.Investigation_Summary__c = 'Test investigation';  
        return investigation;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description adds an investigation with given summary in the response
    */
    public SQX_Test_Investigation addAnActionPlan(String planType, String summary, Date dueDate, boolean isComplete,
    Date completionDate){
    
        SQX_Action_Plan__c actionPlan = new SQX_Action_Plan__c();
        actionPlan.SQX_Investigation__c = this.investigation.ID;
        actionPlan.Description__c = summary;
        actionPlan.Plan_Type__c = planType;
        actionPlan.Due_Date__c = dueDate;
        actionPlan.Completed__c = isComplete;
        actionPlan.Completion_Date__c = completionDate;

        insert actionPlan;


        this.actionPlans.add(actionPlan);
    
        return this;
    }
    
    public SQX_Test_Investigation addAnActionPlan(String planType){
        return this.addAnActionPlan(planType, 'asdadsasd', Date.Today().addDays(10), false, null);
    }
    
    public SQX_Test_Investigation addRootCause(String causeSummary){
        SQX_Root_Cause__c rootCause = new SQX_Root_Cause__c();
        rootCause.Description__c   = causeSummary;
        rootCause.SQX_Investigation__c = this.investigation.Id;
        
        insert rootCause;
        
        return this;
    }
    
    
    public SQX_Investigation__c investigation {get; set;}
    public List<SQX_Action_Plan__c> actionPlans {get; set;} 
    
    
    public SQX_Test_Investigation(SQX_Finding__c finding){
        this(true, finding);
    }

    public SQX_Test_Investigation(Boolean needsFinding, SObject parent) {
        this.investigation = SQX_Test_Investigation.prepareMockObject(parent);
        insert this.investigation;

        this.actionPlans = new List<SQX_Action_Plan__c>();
    }
    
}