/**
* This test class ensures that related SF tasks are completed automatically in completion of investigation, PHR and decision tree run
**/
@IsTest
public class SQX_Test_1986_Complaint_SFTaskCompletion{


    final static String STD_USER_1 = 'Std user 1',
                        ADMIN_USER_1 = 'Admin user 1';

    @testsetup
    static void setupData() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STD_USER_1);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, ADMIN_USER_1);
    }

    /**
    * This test ensures that sf task created for Decision Tree gets completed automatically when the decision tree is run
    *
    * Given : Decision Tree Task is added and Complaint is initaited
    * When :  Decision Tree is run
    * Then : Related SF task created for Decision Tree is completed
    **/                        
    public static testmethod void givenComplaintIsCreated_DecisionTreeRun_TaskCompleted(){

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get(STD_USER_1);
        User adminUser = users.get(ADMIN_USER_1);
        SQX_Test_Task task1;

        System.runAs(adminUser){
            
            task1 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE);
            task1.task.SQX_User__c = standardUser.Id;
            task1.save();
        }

        System.runAs(standardUser){
            
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(null, adminUser).save();
            
            complaint.initiate();

            List<SQX_Complaint_Task__c> complaintTasks = [Select Id, SQX_Complaint__c from SQX_Complaint_Task__c where SQX_Complaint__c = : complaint.complaint.Id and SQX_task__c =:task1.task.Id];
            Set<Id> childWhatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, Schema.SQX_Complaint_Task__c.Id);
            Set<Id> whatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, Schema.SQX_Complaint_Task__c.SQX_Complaint__c);
            List<Task> sfDecisionTreeTasks = [Select Id, Status from Task where Child_What_Id__c in :childWhatIds AND WhatId IN: whatIds AND Status != : SQX_Task.getClosedStatuses()];

            SQX_Decision_Tree__c task1Run1 = complaint.runDecisionTreeTask(task1.task);

            Task decisionTreeTask = [Select Status from Task where Id = :sfDecisionTreeTasks[0].Id];

            //Assert : related investigation SF task is completed
            System.AssertEquals(SQX_Task.STATUS_COMPLETED, decisionTreeTask.Status, 'Expected phr SF task to be completed on phr completion');
        }

    }                 
}