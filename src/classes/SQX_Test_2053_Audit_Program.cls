@isTest
public class SQX_Test_2053_Audit_Program{
    private static Boolean runAllTests = true,
                       run_givenAnAuditProgram_WhenAuditsAreCreatedProgramIsNotCloned = false;  

    /**
     * SCENARIO 1:
     * ACTION: Create an audit program with audit critrion contraint two criterion requirement
     *          An audit is added in audit program and scheduled
     * EXPECTED: All the audit criterion to be copied as audit checklist in audit
     * SCENARIO 2: 
     * ACTION: Create another audit using same program and schedule the audit
     * EXPECTED: Same number of checklist is to be found in both old and new audit using same audit program
     * @author: Anish Shrestha
     * @date: 2015/06/07
     * @story: [SQX-1139]
     */
    public static testmethod void givenAnAuditProgram_WhenAuditsAreCreatedProgramIsNotCloned(){
        if(!runAllTests && !run_givenAnAuditProgram_WhenAuditsAreCreatedProgramIsNotCloned){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(standardUser){

            //Arrange: Setup the program, and set the audit.
            SQX_Test_Audit_Program program1 = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope1 = program1.createScopeWithNewCriteriaDocument(2, adminUser); //refers to a criteria document with single requirement
            
            SQX_Test_Audit audit = new SQX_Test_Audit();
            audit.setStatus(SQX_Audit.STATUS_DRAFT);
            audit.setStage(SQX_Audit.STAGE_PLAN);
            audit.includeInProgram(false, program1.program);
            audit.audit.End_Date__c = Date.Today();
            audit.save();

            // Act 1: Submit for approval and approve the audit program
            program1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_PLAN_APPROVAL;
            program1.save();
            
            program1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program1.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program1.save();

            SQX_Audit__c auditOld = new SQX_Audit__c();
            auditOld = [SELECT Id, Status__c, Stage__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            
            System.assertEquals(SQX_Audit.STAGE_SCHEDULED,  auditOld.Stage__c, 'Expected the status of audit to be scheduled but fount: ' + auditOld.Stage__c);

            List<SQX_Audit_Checklist__c> auditChecklistForAudit1 = new List<SQX_Audit_Checklist__c>();
            auditChecklistForAudit1 = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: auditOld.Id];

            // Assert 1: Expected two audit checklist to be found
            System.assertEquals(2, auditChecklistForAudit1.size(), 'Expected two audit checklist to be found but found: ' + auditChecklistForAudit1.size());

            // Act 2 : Create new audit and refrence old audit program and schedule the audit
            SQX_Test_Audit audit1 = new SQX_Test_Audit();
            audit1.setStage(SQX_Audit.STAGE_SCHEDULED);
            audit1.includeInProgram(false, program1.program);
            audit1.audit.End_Date__c = Date.Today();
            audit1.save();

            SQX_Audit__c auditNew = new SQX_Audit__c();
            auditNew = [SELECT Id, Status__c, Stage__c FROM SQX_Audit__c WHERE Id =: audit1.audit.Id];

            System.assertEquals(SQX_Audit.STAGE_SCHEDULED,  auditNew.Stage__c, 'Expected the status of audit to be scheduled but fount: ' + auditNew.Stage__c);

            List<SQX_Audit_Checklist__c> auditChecklistForAudit2 = new List<SQX_Audit_Checklist__c>();
            auditChecklistForAudit2 = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: auditNew.Id];

            // Assert 2: Expected two audit checklist to be found in new audit
            System.assertEquals(2, auditChecklistForAudit2.size(), 'Expected two audit checklist to be found but found: ' + auditChecklistForAudit2.size());

            auditChecklistForAudit1 = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: auditOld.Id];
            
            // Assert 2: Expected two audit checklist to be found in old audit
            System.assertEquals(2, auditChecklistForAudit1.size(), 'Expected two audit checklist to be found but found: ' + auditChecklistForAudit1.size());

        }
    }
}