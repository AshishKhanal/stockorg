/**
* Unit tests for SQX_Effectiveness_Review
*
* @author Sagar Shrestha
* @date 2014/5/13
* 
*/
@isTest
public class SQX_Test_Effectiveness_Review{

    /**
    *   Setup: Create A CAPA (C1)
    *   Action: Create an Effectiveness Review (ER 1) for C1. as closed effective with rating 5 and closure comments
    *   Expected: C1 should be closed, with 5 and provided closure comment 
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    public static testmethod void addEffReview_CAPAIsClosedwithRatingAndReviewCopied(){ 

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            capa.finding.setRequired(false,false,false,false,false)
                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                    .save();

            capa.CAPA.Status__c = SQX_CAPA.STATUS_COMPLETE;

            capa.save();
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();//add effectiveness review

            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            
            insert review;

            capa.CAPA= [SELECT Status__c, Closure_Comment__c,Rating__c from SQX_CAPA__c where Id = : capa.CAPA.Id] ;

            System.assert(capa.CAPA.Status__c == SQX_CAPA.STATUS_CLOSED, 
                'Expected CAPA to be closed but found it '+capa.CAPA.Status__c);

            System.assert(capa.CAPA.Closure_Comment__c == review.Review__c, 
                'Expected CAPA closure comment to be copied from effectiveness review to be closed but found it '
                +capa.CAPA.Closure_Comment__c +' Effectiveness Review' + review);

            System.assert(String.valueOf(capa.CAPA.Rating__c) == String.valueOf(review.Rating__c), 
                'Expected CAPA closure comment to be copied from effectiveness review to be closed but found it '
                +capa.CAPA.Closure_Comment__c +' Effectiveness Review' + review);
        }
    }

    /**
    *   Setup: Create a CAPA (C1)
    *   Action: Create an Effectiveness Review (ER 1) for C1. void with closure comment 
    *   Expected: C1 should be closed, without rating but with provided closure comment 
    *
    * @author Sagar Shrestha
    * @date 2014/5/14
    * 
    */
    /*public static testmethod void addEffReviewAsVoid_CAPAIsClosed_ReviewCopied(){ 

        SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility()
                                            .save();
        SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();//add effectiveness review

        SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                             Status__c = SQX_Effectiveness_Review.STATUS_VOID,
                                                                             Reviewed_By__c='Sagar',
                                                                             Review_Started_On__c=Date.today(),
                                                                             Review__c='voided',
                                                                             Review_Completed_On__c = Date.today()
                                                                             );
        
        insert review;  

        capa.CAPA= [SELECT Status__c, Closure_Comment__c,Rating__c from SQX_CAPA__c where Id = : capa.CAPA.Id] ;

        System.assert(capa.CAPA.Status__c == SQX_CAPA.STATUS_CLOSED, 
            'Expected CAPA to be closed but found it '+capa.CAPA.Status__c);

        System.assert(capa.CAPA.Closure_Comment__c == review.Review__c, 
            'Expected CAPA closure comment to be copied from effectiveness review to be closed but found it '
            +capa.CAPA.Closure_Comment__c +' Effectiveness Review' + review);

    }*/


    /**
    *   Setup: Create a CAPA (C1)
    *   Action: Create an Effectiveness Review (ER 1) as pending
    *   Expected: ER 1 should have a task created for it 
    *
    * @author Sagar Shrestha
    * @date 2014/5/14
    * 
    */
    public static testmethod void addPendingEffReview_TaskIsCreated(){ 

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility()
                                                .save();
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();//add effectiveness review

            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_PENDING,
                                                                                 Review_Started_On__c=Date.today() 
                                                                                 );
            
            insert review;

            review=[Select Id, Effectiveness_Reviewer__c from SQX_Effectiveness_Review__c where Id=:review.Id];

            boolean taskExistsForEffReview= SQX_Task_Template.taskExistsFor(capa.CAPA, review.Effectiveness_Reviewer__c,SQX_Task_Template.TaskType.EffectivenessReviewToDo);

            System.assert(taskExistsForEffReview, 
                'Expected task to be created for pending Effectiveness review '+ review);

        }
    }

    /**
    *   Setup: Create a CAPA (C1) and an Effectiveness Review (ER1) for it as pending
    *   Action: Complete ER1 but set the status as 'Schedule another review'
    *   Expected: A new effectiveness review (ER2) must be created as open 
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    public static testmethod void addEffReviewAsPending_NewEffReviewCreated(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility()
                                                .save();
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();//add effectiveness review

            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_SCHEDULE_ANOTHER_REVIEW,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today(),
                                                                                 Next_Review_Date__c=Date.today()
                                                                                 );
            
            insert review;

            integer effReviewCount= [SELECT count() FROM  SQX_Effectiveness_Review__c where  SQX_CAPA__c = : capa.CAPA.Id] ;

            System.assert(effReviewCount==2, 
                'Expected another effectiveness review to be created. Count is '+effReviewCount);
        }
        
    }


    /**
    *   Setup: Create a CAPA (C1) and an Effectiveness Review(ER1) as pending, Get ID of Task for ER1
    *   Action: Complete ER1
    *   Expected: The task created for ER1 is now completed
    *
    * @author Sagar Shrestha
    * @date 2014/5/13
    * 
    */
    public static testmethod void addEffReviewAsPending_CompleteEffReview_TaskAlsoCompleted(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            capa.finding.setRequired(false,false,false,false,false)
                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                    .save();

            capa.CAPA.Status__c = SQX_CAPA.STATUS_COMPLETE;

            capa.save();
            
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();//add effectiveness review

            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_PENDING,
                                                                                 Review_Started_On__c=Date.today() 
                                                                                 );
            
            insert review;

            List<Task> effReviewtask= [SELECT Id from Task WHERE whatId=:review.Id];

            System.assert(effReviewtask.size()==1, 
                'Expected task to be created for effectiveness review. '+effReviewtask);

            review.Status__c=SQX_Effectiveness_Review.STATUS_COMPLETED;
            review.Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE;
            review.Rating__c = '3';
            review.Reviewed_By__c='Sagar';
            review.Review__c='effective and complete';
            review.Review_Completed_On__c = Date.today();

            update review;

            Task effRevTask= [SELECT Status from Task WHERE Id=:effReviewtask[0].Id];

            System.assert(effRevTask.Status=='Completed', 
                'Expected task to be completed '+effRevTask);
        }
        
    }

    /**
    * Given:- Create CAPA with Effectiveness Review
    * When :- Insert Effectiveness Review Status as complete and Resolution as void
    * Then :- Changed CAPA Status as closed and Resolution as void
    */
    public static testmethod void givenCapaWithEffReview_WhenAddEffReviewStatusandResolution_ThenChangedCAPAStatusandResolution(){ 

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            // Arrange: Create CAPA with effectiveness review
            capa.finding.setRequired(false,false,false,false,false)
                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                    .save();

            capa.CAPA.Status__c = SQX_CAPA.STATUS_COMPLETE;

            capa.save();
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();

            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_VOID,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='bhuvan',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='void and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            // Act: Insert effectiveness review with status as complete and resolution as void.
            insert review;

            capa.CAPA= [SELECT Status__c, Resolution__c from SQX_CAPA__c where Id = : capa.CAPA.Id];
            
            //Assert:- Ensured that CAPA status as closed and resolution as void
            System.assertEquals(SQX_CAPA.STATUS_CLOSED, capa.CAPA.Status__c);
            System.assertEquals(SQX_CAPA.RESOLUTION_VOID, capa.CAPA.Resolution__c);
        }
    }

    /**
    * Given:- Create CAPA (Open status) with Effectiveness Review
    * When :- Save Effectiveness Review Status as complete and Resolution as Ineffictive
    * Then :- Capa status as closed and resolution as ineffective
    * @story:- SQX-4317
    */
    public static testmethod void givenCapaWithOpenStatus_WhenAddEffReviewStatusandResolution_ThenChangedCAPAStatusandResolution(){ 

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            // Arrange: Create CAPA with effectiveness review
            capa.finding.setRequired(true,true,false,false,false)
                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                    .save();

            capa.save();
            SQX_Effectiveness_Review eff= new SQX_Effectiveness_Review();
            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_INEFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='bhuvan',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='ineffective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            // Act: Insert effectiveness review with status as complete and resolution as ineffective.
            insert review; 

            capa.CAPA= [SELECT Status__c, Resolution__c from SQX_CAPA__c where Id = : capa.CAPA.Id];
            
            //Assert:- Ensured that CAPA status as closed and resolution as ineffective
            System.assertEquals(SQX_CAPA.STATUS_CLOSED, capa.CAPA.Status__c);
            System.assertEquals(SQX_CAPA.RESOLUTION_INEFFECTIVE, capa.CAPA.Resolution__c);
        }
    }

}