@isTest
public class SQX_Test_2080_Change_Order_Owner{

    static boolean runAllTests = true,
                run_givenAUser_WhenCOOwnerIsChanged_ItMustHaveEditAccessToActions = false,
                run_givenAUser_WhenCOIsInRePlan_ActionOwnerMustBeChangedToCOOwner = false,
                run_givenCO_WhenCOIsSubmitted_ChangingOwnerIsLoggedInRecordActivity = false;

    /**
     * Action: Create a Change Order and implementation and save it and change the owner of the action.
     * Expected: New owner must be able to edit implementation
     * @author: Anish Shrestha
     * @date: 2016-03-31
     * @story: [SQX-2080]
     */
    public static testMethod void givenAUser_WhenCOOwnerIsChanged_ItMustHaveEditAccessToActions(){

        if(!runAllTests && !run_givenAUser_WhenCOOwnerIsChanged_ItMustHaveEditAccessToActions){
            return;
        }
        //Arrange: Create a action
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User coOwner = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
        Task task = new Task();
        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, taskAssignee, 'actionDescription'  ,  Date.Today().addDays(15)));

            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });

            SQX_Change_Order__c CO1 = [SELECT Id, OwnerId FROM SQX_Change_Order__c WHERE Id =: changeOrder.changeOrder.Id];

            // Act : Change the owner of the changer order
            CO1.OwnerId = coOwner.Id;

            Database.SaveResult result1 = Database.update(CO1, false);

            System.assertEquals(true, result1.isSuccess(), 'Expected the owner to be changed');
        }

        System.runas(coOwner){
            SQX_Implementation__c imp1 = [SELECT Description__c FROM SQX_Implementation__c WHERE Id =: actions[0].Id];

            // Assert : Edit the implementation, it should be editable
            imp1.Description__c = 'Changed Description';

            Database.SaveResult result2 = Database.update(imp1, false);

            System.assertEquals(true,result2.isSuccess(), 'Expected the implementation to be updated');
        }
    }

    /**
     * Action: Create a Change Order and Implementation and change the owner of the implementation and bring the approval status of CO to replan
     * Expected: Implementation owner must be CO Owner
     * @author: Anish Shrestha
     * @date: 2016-03-31
     * @story: [SQX-2080]
     */
    public static testMethod void givenAUser_WhenCOIsInRePlan_ActionOwnerMustBeChangedToCOOwner(){

        if(!runAllTests && !run_givenAUser_WhenCOIsInRePlan_ActionOwnerMustBeChangedToCOOwner){
            return;
        }
        //Arrange: Create a action
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User coOwner = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
        Task task = new Task();
        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, taskAssignee, 'actionDescription',  Date.Today().addDays(15)));
            actions[0].OwnerId = taskAssignee.Id;

            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });

            SQX_Change_Order__c CO1 = new SQX_Change_Order__c(
                Id = changeOrder.changeOrder.Id,
                Status__c = SQX_Change_Order.STATUS_IMPLEMENTATION,
                Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED
            );

            Database.SaveResult result1 = Database.update(CO1, false);


            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act : Change the approval status of the change order to replan
            CO1.Status__c = SQX_Change_Order.STATUS_OPEN;
            CO1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_REPLAN;

            result1 = Database.update(CO1, false);

            System.assertEquals(true, result1.isSuccess(), 'Expected the owner to be changed' + result1.getErrors());

            // Assert: Expected the owner of the implementation to be owner of the change order
            SQX_Implementation__c imp1 = [SELECT OwnerId FROM SQX_Implementation__c WHERE Id =: actions[0].Id];
            CO1 = [SELECT OwnerId FROM SQX_Change_Order__c WHERE Id =: CO1.Id];

            System.assertEquals(CO1.OwnerId, imp1.OwnerId, 'Expected the owner of the implementation to be the owner of the change order');

        }
    }

    /**
    * Given:A Change Order is created
    * When: Owner is changed
    * Then: "Changing Owner" is supposed to log in record activity
    * @date: 6/23/2016
    * @author: Manoj Thapa
    * @story: SQX-2115
    */
    public static testMethod void givenCO_WhenCOIsSubmitted_ChangingOwnerIsLoggedInRecordActivity(){
        if(!runAllTests && !run_givenCO_WhenCOIsSubmitted_ChangingOwnerIsLoggedInRecordActivity){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);


        System.runas(user){

            //Arrange: A Change Order record is created
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            //Act: The owner of the record is changed
            changeOrder.changeOrder.OwnerId = standardUser.Id;
            changeOrder.save();
           
            List<SQX_Change_Order_Record_Activity__c> coRecordActivityList = [SELECT Id, Purpose_Of_Signature__c FROM SQX_Change_Order_Record_Activity__c WHERE SQX_Change_Order__c =: changeOrder.changeOrder.Id];
            System.assertEquals(1, coRecordActivityList.size(), 'Expected one record activity to be listed');
            Boolean existsChangeOrderRecordActivity = false;
            for (SQX_Change_Order_Record_Activity__c cora : coRecordActivityList){
                if(cora.Purpose_Of_Signature__c == SQX_Record_History.PS_OWNER_CHANGED){
                    existsChangeOrderRecordActivity = true;
                    break;
                }
            }
            //Assert:Expected to contain change owner record activity
            System.assertEquals(true, existsChangeOrderRecordActivity , 'Expected to contain change owner record activity');
     
        }    
     
    }
}