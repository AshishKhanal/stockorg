/**
*   This class contains method to check for CRUD and FLS(for current user) for SObjects and their fields
*   @author Piyush Subedi
*   @date 2017/11/30
*/
public with sharing class SQX_Lightning_CRUD_FLS_Checker{

    public SQX_Lightning_CRUD_FLS_Checker() { }

    /*
        Holds cached accessibility results for sobjectfields
    */
    private static Map<SObjectType, Map<SObjectField, Boolean>> cachedAccessibilityInfoForFields = new Map<SObjectType, Map<SObjectField, Boolean>>();


    /*
        Holds cached accessibility results for sobjecttypes
    */
    private static Map<SObjectType, Boolean> cachedAccessibilityInfoForObjects = new Map<SObjectType, Boolean>();



    /**
    *   Method gets the accessibility permissions of given sobject types
    *   @param objectTypes list of object types to be checked
    */
    public Map<SObjectType, Boolean> getObjectsAccessibility(Set<SObjectType> objectTypes){

        Map<SObjectType, Boolean> objectsAccessibility = new Map<SObjectType, Boolean>();

        for(SObjectType objectType : objectTypes)
        {
            if(cachedAccessibilityInfoForObjects.containsKey(objectType))
            {
                objectsAccessibility.put(objectType, cachedAccessibilityInfoForObjects.get(objectType));
            }
            else
            {
                objectsAccessibility.put(objectType, objectType.getDescribe().isAccessible());
            }
        }

        return objectsAccessibility;
    }



    /*
     * Method checks for the accessibility of the given set of sobject fields associated with the given sobject
     * Throws error(aura handled exception) if ANY ONE of the fields isn't accessible
     * @param objectType sobject type(ex. compliancequest__SQX_Audit__c)
     * @param fields list of fields to be checked
    */
    public void checkFieldsAccessibility(SObjectType objectType, List<SObjectField> fields){

        checkFieldsAccessibility(new Map<SObjectType, List<SObjectField>> { objectType => fields });
    }


    /*
     * Method checks for the accessibility of the given set of sobject fields associated with the given sobject
     * Throws error(aura handled exception) if ANY ONE of the fields isn't accessible
     * @param objFieldsMap map of sobject type(ex. compliancequest__SQX_Audit__c) and the list of fields to be checked
    */
    public void checkFieldsAccessibility(Map<SObjectType, List<SObjectField>> objFieldsMap)
    {
        List<String> exceptionMsgs = new List<String>();

        if(objFieldsMap != null)
        {
            for(SObjectType objectType : objFieldsMap.keySet())
            {
                Map<SObjectField, Boolean> accessibilityMap = new Map<SObjectField, Boolean>();

                if(cachedAccessibilityInfoForFields.containsKey(objectType))
                {
                    accessibilityMap = cachedAccessibilityInfoForFields.get(objectType);
                }
                else
                {
                    cachedAccessibilityInfoForFields.put(objectType, accessibilityMap);
                }

                List<SObjectField> fields = objFieldsMap.get(objectType);

                for(SObjectField field : fields)
                {
                    if(!accessibilityMap.containsKey(field))
                    {
                        accessibilityMap.put(field, field.getDescribe().isAccessible());
                    }

                    if(!accessibilityMap.get(field))
                    {
                        exceptionMsgs.add(String.format(Label.SQX_ERR_MSG_FIELD_NOT_ACCESSIBLE,
                                                        new String[] { field.getDescribe().getName(), objectType.getDescribe().getName() } ));
                    }
                }
            }
        }

        if(exceptionMsgs.size() > 0)
        {
            // amalgate all the error messages and throw the exception(aura handled)
            AuraHandledException ex = new AUraHandledException('');
            ex.setMessage(String.join(exceptionMsgs, ','));

            throw ex;
        }

    }
}