/**
* Test for utility class
*/
@IsTest
public class SQX_Test_Utilities{

    static final String DRAFT_LIB_NAME = 'Test Draft Library123',
                        RELEASE_LIB_NAME = 'Test Release Library123',
                        DISTRIBUTION_LIB_NAME = 'Test Distribution Library123',
                        GROUP_TYPE_QUEUE = 'Queue';

    /**
    * creates number of result types to be used in objects
    * @param numberOfResultTypes the number of result types to be created
    * @param numberOfResultsPerType the number of result type values per types
    */
    public static SQX_Result_Type__c[] setupResultTypes(Integer numberOfResultTypes, Integer numberOfResultsPerType){
         List<SQX_Result_Type__c> resultTypes = new List<SQX_Result_Type__c>();
        List<SQX_Result_Type_Value__c> values = new List<SQX_Result_Type_Value__c>();

        for(Integer i = 0; i < numberOfResultTypes; i++){
            SQX_Result_Type__c type = new SQX_Result_Type__c();
            type.Name = 'Result Type - ' + i;
            type.Description__c = 'Description of ' + i;

            resultTypes.add(type);

        }

        insert resultTypes;

        if(numberOfResultsPerType > 0){
            for(SQX_Result_Type__c type : resultTypes){
                for(Integer k = 0; k < numberOfResultsPerType; k++){
                    SQX_Result_Type_Value__c resValue = new SQX_Result_Type_Value__c();
                    resValue.Name = type.Name + '-Value-' + k;
                    resValue.SQX_Result_Type__c = type.Id;
    
                    values.add(resValue);
                }
            }
            
            insert values;
        }
        return resultTypes;
    }

    /**
     * Method to set user to queue, if no queues are found then it will create a new queue
     * @param usersToAssign list of users that are going to be assigned
     * @param devName developer name of the Queue
     * @param object type
     */
    public static void addUserToQueue(List<User> usersToAssign, String devName, String sObjType) {
        List<Group> gList = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName =: devName];
        Group g1;
        if(gList.size() == 0){
            g1 = new Group(DeveloperName = devName, Name = devName, Type = GROUP_TYPE_QUEUE);
            insert g1;
            
            QueueSObject sobj = new QueueSObject(SobjectType = sObjType, QueueId = g1.Id);
            insert sobj;
        } else {
            g1 = gList.get(0);
        }
        List<GroupMember> members = new List<GroupMember>();
        for(User userToAssign : usersToAssign){
            
            GroupMember member = new GroupMember();
            member.UserOrGroupId = userToAssign.Id;
            member.GroupId = g1.Id;
            members.add(member);
        }
        
        insert members;
    }
    
    /**
     * Method to set custom setting values
     */
    public static SQX_Custom_Settings_Public__c setCustomSettingSpecifiedFieldValue(Map<Schema.SObjectField, Object> fieldToValueMap) {
        SQX_Custom_Settings_Public__c cqSetting = SQX_Custom_Settings_Public__c.getOrgDefaults();
        cqSetting.Community_URL__c = 'fakeURL';
        cqSetting.Org_Base_URL__c = 'fakeURL';
        cqSetting.NC_Analysis_Report__c = 'fakeID';
        if(fieldToValueMap != null) {
            for (Schema.SObjectField field : fieldToValueMap.keySet()) {
                cqSetting.put(field, fieldToValueMap.get(field));
            }
        }   
        Database.insert(cqSetting, true);
        return cqSetting;
    }

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');        
    }

    public testmethod static void getUtilityStuff(){
    
        System.assert(SQX_Utilities.getCurrentUsersRole() == SQX_Utilities.SQX_UserRole.RoleCustomer,
            'Expected current user to be of customer type');
            
        try{
            String url = SQX_Utilities.getDownloadURLFor('RandomID');
            System.assert(url.contains('RandomID'), 'Expected random id');
        }
        catch(Exception ex){
        }
    
    }

    /**
    *   Given: A partner User is created 
    *   When:  contact of the user is partner enabled
    *   Then:  the user is portal enabled
    *
    *   Given: A nonpartner User is created 
    *   When:  contact of the user is not partner enabled
    *   Then:  the user is not portal enabled
    *   @date: 7/12/2016
    *   @story: [SQX-1307]
    *   @authOR: Manoj Thapa
    */
    public testmethod static void checkIfTheUserIsPortalEnabled(){
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User newUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);


        System.runas(newUser) {
            //Arrange: a partner user is created
            Account partnerAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_SUPPLIER);
            Contact partnerContact = SQX_Test_Account_Factory.createContact(partnerAccount);
            User partnerUser = SQX_Test_Account_Factory.createUser(partnerContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
            //Act: contacts of user is made partner enabled
            Set<Id> contacts = new Set<Id>();
            contacts.add(partnerContact.Id);
    
            Map<Id, Boolean> portalEnabledMap = SQX_Utilities.getIsPortalContactsFor(contacts);
            //Assert: The user is portal enabled
            System.assertEquals(true, portalEnabledMap.get(partnerUser.ContactId), 'Expected the user contact to be Portal Enabled');

            
            Set<Id> contacts1 = new Set<Id>();
            contacts1.add(newUser.ContactId);
    
            Map<Id, Boolean> portalEnabledMap1 = SQX_Utilities.getIsPortalContactsFor(contacts1);
    
            System.assertEquals(false, portalEnabledMap1.get(newUser.ContactId), 'Expected the user contact not to be portal enabled');
        }
    }

    /**
    * get current user personnel information 
    */    
    public testmethod static void getCurrentUserPersonnelInfo(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(adminUser){
            // Arrange: create personnel for current user
            SQX_Test_Personnel objPersonnelInfo=new SQX_Test_Personnel(adminUser);
            objPersonnelInfo.save();
            // Act: get current user personel information
            SQX_Personnel__c objPersonnel=SQX_Utilities.getPersonnelInfo();
            // Assert: Ensure that  personnel information
            System.assertNotEquals(null, objPersonnel);
        }
         System.runAs(standardUser){
             // this is  for negative test
            SQX_Personnel__c objPersonnel=SQX_Utilities.getPersonnelInfo();
                System.assertEquals(null,objPersonnel);
        }
    }

    /**
    *   Given: Current Revision number of Controlled Doc (Any pattern of String)
    *   When:  Controlled Doc is revised (ie when string is passed to getNextRev(String rev) method)
    *   Then:  The newly generated revision number is succeeding nuber of previous revision number
    *
    *   @date: 9/15/2016
    *   @story: [SQX-2584]
    *   @authOR: Paras Kumar Bishwakarma
    */
    public static testMethod void givenCurrentRevisionNumberOfControlledDoc_WhenDocIsRevised_NewlyGeneratedRevisionNumberIsSucceedingNumberOfCurrentRevisionNumber(){

        //Arrange: Create diffrent patterns of string
        Map<String,String> revs = new Map<String,String>{
            '0'=>'1','9' => '10',
            '00'=>'01','01'=>'02','09' => '10',
            'A.0' => 'A.1','A.9'=>'B.0','A.09'=>'A.10',
            'A' => 'B', 'B-E'=>'B-F','A.Z'=>'B.A',
                'a' => 'b', 'b-e'=>'b-f','a.Z'=>'b.A',
                'Z.z'=>'AA.a','.999' => '1.000','..'=>'..'
        };
        String newRev;

        //Act : Send the string pattern to getNextRev() method 
        newRev = SQX_Utilities.getNextRev('');
        //Assert: The generated next revision number is as expected
        system.assertEquals(newRev,'0','for blank should return 0');
        //Assert: The generated next revision number is the succeeding number of current revision number
        for (String rev : revs.keySet()){
            newRev = SQX_Utilities.getNextRev(rev);
            system.assertEquals(newRev,revs.get(rev),'failed: '+rev+ ' expected: '+revs.get(rev) +' got:' + newRev);
        }
    }

    /**
    * Scenario : Checks the caching behavior of Utilities.getOrganization method
    * Given Utilities class
    * When Organization is called multiple times
    * Then Cached Result is returned and no SOQL is done
    */
    static testmethod void givenUtils_WhenOrganizationIsQueriedMultipleTimes_ThenCachedResultIsReturned() {
        // Arrange: Fetch the organization for first time
        Integer beforeOrg = Limits.getQueries();
        Organization org1 = SQX_Utilities.getOrganization();
        Integer afterOrg = Limits.getQueries();

        // pre-check: Ensure that query was performed
        System.assertEquals(1, afterOrg - beforeOrg);

        // Act: Fetch organization again
        Organization org2 = SQX_Utilities.getOrganization();
        Integer newAfterOrg = Limits.getQueries();

        // Assert: Ensure that organization is returned and cached result is returned
        System.assertEquals(org1, org2, 'Expected org1 and org2 to refer to same object as they are returned from cache');
        System.assertEquals(0, newAfterOrg - afterOrg, 'Expected no new queries to be fired');
    }
    
    /**
    * ensures getTruncatedText() to truncate only when value is greater than maximum length
    */
    static testmethod void givenGetTruncatedText_ReturnsProperValues() {
        Integer maxLen = 500;
        
        String[] samples = new String[]{ null, 'a'.repeat(maxLen), 'b'.repeat(maxLen + 1), 'c'.repeat(maxLen + 1) };
        String[] expectedResult = new String[]{
            null,
            'a'.repeat(maxLen),
            'b'.repeat(maxLen), // do not add ellipsis
            'c'.repeat(maxLen - 3) + '...'
        };
        Set<Integer> doNotAddEllipsis = new Set<Integer>{ 2 };
        
        for (Integer i = 0; i < samples.size(); i++) {
            // ACT: process description to truncate when string length exceeds maximum length
            String result = SQX_Utilities.getTruncatedText(samples[i], maxLen, !doNotAddEllipsis.contains(i));
            
            System.assertEquals(expectedResult[i], result);
        }
    }

    /**
     * Ensures addErrorMessageInPage() of SQX_Utilities class adds page message if same message is not persent in the current page;
     */
    static testmethod void givenPageHasAlreadyErrorMessage_WhenMethodIsCalled_ItReturnsTrue() {

        // Arrange : set current page 
        Test.setCurrentPage(Page.SQX_Controlled_Document_Approval);
        String exceptionMessage = 'test exception message';
        
        // Act : call addErrorMessageInPage method of SQX_Utilities class
        SQX_Utilities.addErrorMessageInPage(exceptionMessage);
        
        // Assert : page message to be added
        System.assert(ApexPages.getMessages().size() == 1, 'Expected page message count to be 1 but is ' + ApexPages.getMessages().size());
        System.assertEquals(exceptionMessage, ApexPages.getMessages()[0].getDetail());
        
        // Add error message with severity type Info
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Custom error in test method with severity info.'));
        
        // Assert : page message should be added and its count should be 2
        System.assert(ApexPages.getMessages().size() == 2, 'Expected page message count to be 2 but is ' + ApexPages.getMessages().size());

        // Act : call addErrorMessageInPage method of SQX_Utilities class
        exceptionMessage = 'first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, my own exception error message.:[]';
        SQX_Utilities.addErrorMessageInPage(exceptionMessage);

        // Assert : page message should be added and its count should be 3
        System.assert(ApexPages.getMessages().size() == 3, 'Expected page message count to be 3 but is ' + ApexPages.getMessages().size());

        // Arrange : Add another errors with severity type Error
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Custom error in test method with severity Error.'));
        
        // Assert : page message should be added and its count should be 4
        System.assert(ApexPages.getMessages().size() == 4, 'Expected page message count to be 4 but is ' + ApexPages.getMessages().size());
        
        // Act : Call addErrorMessageInPage method of SQX_Utilities class
        exceptionMessage = 'first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Custom error in test method with severity Error.:[]';
        SQX_Utilities.addErrorMessageInPage(exceptionMessage);

        // Assert : page message should not be added thus total page message should be 4
        System.assert(ApexPages.getMessages().size() == 4, 'Expected page message count to be 4 but is ' + ApexPages.getMessages().size());
    }
    
    /**
     * Ensures redirectUrlProtection(String returnURL) of SQX_Utilities class where its check the returnURL contain forward slash or not
     *  if its contain its will replace with '' and add single forward slash for returnURL
     */
    static testmethod void ensureRedirectUrlProtectionMethodWorksCorrectly(){
        String redirectUrl = SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false);
        String returnUrl=SQX_Utilities.redirectUrlProtection(redirectUrl);
        //Assert : whether the String returnUrl is start with / or not 
        System.assertEquals(redirectUrl, returnUrl);
               
        String externalUrl='www.google.com';
        String returnExternalUrl=SQX_Utilities.redirectUrlProtection(externalUrl);
        //Assert : See the returnUrl whether its External link or not 
        System.assertEquals('/' + externalUrl, returnExternalUrl, 'Unauthorized Url Try To access in Org');
        
        String externalUrlhttps='https://www.google.com.np';
        String returnExternalUrlHttps=SQX_Utilities.redirectUrlProtection(externalUrlhttps);
        //Assert : See the returnUrl whether its External link or not
        System.assertEquals('/' + externalUrlhttps, returnExternalUrlHttps, 'Unauthorized Url Try To access in Org');
    }
    
    /**
    * Ensures ensureRedirectUrlValidCorrectly() of SQX_Utilities class where its check the returnURL contain forward slash , http or not
    * if the retURL contain http its check whether its contain force.com or not
    * if the retURL is invalid  than its default url as /home/home.jsp
    */
    static testmethod void ensureRedirectUrlValidCorrectly(){
        String redirectUrl = SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false);
        PageReference  defaulturl=new PageReference(SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));
        String returnUrl= SQX_Utilities.getValidReturnUrl(redirectUrl,null).getURL();
        //Assert : whether the String returnUrl is start with / or not 
        System.assertEquals(redirectUrl, returnUrl);
        
        String externalUrl='https://www.google.com.np/?q=force.com&gws_rd=cr&dcr=0&ei=UmmOWpuUF4HG0AS6uqPoDQ';
        //Assert : See the returnUrl whether its External link 
        System.assertEquals(defaulturl, SQX_Utilities.getValidReturnUrl(externalUrl,defaulturl),'Unauthorized Url Try To access in Org');
        
        String externalUrlhttps='https://www.google.com.np';
        String returnExternalUrlHttps=SQX_Utilities.getValidReturnUrl(externalUrlhttps,null).getURL();
        //Assert : See the returnExternalUrlHttps whether its External link or not
        System.assertEquals(defaulturl.getURL(), returnExternalUrlHttps, 'Unauthorized Url Try To access in Org');
        
    }

    /**
     * Ensures that white listed urls work correctly for all the items
     */
    static testmethod void ensureWhiteListingMechanismWorks() {
        // Arrange: Save custom setting to white list google.com
        SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
            Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
            Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
            Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890',
            Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true,
            Schema.SQX_Custom_Settings_Public__c.WhiteListed_Url__c => 'facebook.com,google.com'
        });

        Map<String, Boolean> returnMap = new Map<String, Boolean> {
            'https://google.com' => true,
            'https://google.com/asdasd' => true,
            'https://a.google.com' => true,
            'https://a.google.com/asdasd' => true,
            'https://google.com.np/aasd' => false,
            'https://google.asdasd.com/aasd' => false,
            'https://google.facebook.com/aasd' => true,
            'https://asdasd.com/facebook.com' => false,
            'http://google.com/facebook.com' => false
        };

        // Act: Get the external redirect url for each case and ensure value matches based on the map
        for(String url : returnMap.keySet()) {
            String retVal = SQX_Utilities.getValidReturnUrl(url, null).getURL();
            if(returnMap.get(url)){
                System.assertEquals(url, retVal, 'Url mismatch');
            } else {
                System.assertNotEquals(url, retVal, 'Url match when it shouldnt');
            }
        }
    }
    /**
    * This method creates three test libraries and add members with permission to access those libraries.
    * @Story : SQX-5145
    */
       
    public static void createLibraries(List<Id> usersToShareWith){
        // create libraries: Test Draft Library123, Test Release Library123 and Test Distribution Library123
        List<ContentWorkspace> libraries = new List<ContentWorkspace>{
            new ContentWorkSpace(Name = DRAFT_LIB_NAME),
            new ContentWorkSpace(Name = RELEASE_LIB_NAME),
            new ContentWorkSpace(Name = DISTRIBUTION_LIB_NAME)};
        insert libraries;
        
        // create content workspace permission
        ContentWorkspacePermission libraryPermission = new ContentWorkspacePermission(
                Name = 'Test',
                PermissionsManageWorkspace = true, 
                PermissionsDeliverContent = true);
        insert libraryPermission;
        
        // add members to library
        List<ContentWorkspaceMember> libraryMembers = new List<ContentWorkspaceMember>();
        for(Id userId: usersToShareWith){
            for(ContentWorkspace library: libraries){                
                ContentWorkspaceMember libraryUser = new ContentWorkspaceMember(
                    ContentWorkspaceId = library.Id, 
                    ContentWorkspacePermissionId = libraryPermission.Id, 
                    MemberId = userId);
                libraryMembers.add(libraryUser);                         
            }            
        }                
        insert libraryMembers;
    }
    
    
    static Map<String, Id> libaryNamesCache;
    /**
    * Returns the library Id for a given type
    * @param libraryType one of the three type Draft, Release, Distribution
    */
    public static Id getLibraryId(String libraryType) {
        if(libaryNamesCache== null) {
            Map<String, String> libraryNames = new Map<String, String> {
                DRAFT_LIB_NAME => 'Draft',
                RELEASE_LIB_NAME => 'Release',
                DISTRIBUTION_LIB_NAME => 'Distribution' };
            libaryNamesCache = new Map<String, Id>();
            for(ContentWorkspace ws : [SELECT Id,Name FROM ContentWorkspace WHERE Name IN : libraryNames.keySet()]) {
                libaryNamesCache.put(libraryNames.get(ws.Name), ws.Id);
            }
        }

        if(!libaryNamesCache.containsKey(libraryType) || libaryNamesCache.get(libraryType) == null) {
            throw new SQX_ApplicationGenericException('Invalid library type requested. Please ensure libraries have been created correctly');
        }
        return libaryNamesCache.get(libraryType); 
    }

    public static void completeSupplierStep(Id taskId, String result, String comment, String stepRecordType){
        completeSupplierStep(taskId, result, comment, stepRecordType, '', null, null);
    }

    public static void completeSupplierStep(Id taskId, String result, String comment, String stepRecordType, String auditNumber){
        completeSupplierStep(taskId, result, comment, stepRecordType, auditNumber, null, null);
    }

    public static void completeSupplierStep(Id taskId, String result, String comment, String stepRecordType, Date documentIssueDate, Date documentExpirationDate){
        completeSupplierStep(taskId, result, comment, stepRecordType, '', documentIssueDate, documentExpirationDate);
    }
    
    public static void completeSupplierStep(Id taskId, String result, String comment, String stepRecordType, String auditNumber, Date documentIssueDate, Date documentExpirationDate){
		
        Task sfTask = [SELECT Id, Child_What_Id__c FROM Task WHERE Id =: taskId];
        Id stepId = (Id) sfTask.Child_What_Id__c;
        SObject step =  stepId.getSobjectType().newSObject();
        step.put('Id', stepId);
        step.put(SQX_Steps_Trigger_Handler.FIELD_STATUS, SQX_Steps_Trigger_Handler.STATUS_COMPLETE);
        step.put(SQX_Steps_Trigger_Handler.FIELD_RESULT, result);
        step.put(SQX_Steps_Trigger_Handler.FIELD_COMMENT, comment);
        step.put(SQX_Steps_Trigger_Handler.FIELD_ISSUE_DATE, documentIssueDate);
        step.put(SQX_Steps_Trigger_Handler.FIELD_EXPIRATION_DATE, documentExpirationDate);
        step.put(SQX_Steps_Trigger_Handler.FIELD_COMPLETION_DATE, Date.today());
        if(!String.isBlank(auditNumber)){
            step.put(SQX_Steps_Trigger_Handler.FIELD_AUDIT_NUMBER, auditNumber);
        }
        
        new SQX_DB().withoutSharing().op_update(new List<SObject>{step}, new List<Schema.SObjectField>{});
    }


    /**
     * Method creates an sobject record for the given sobjecttype
     * It tries to fill all the values for fields of the sobject
     * Avoids reference fields
    */
    public static SObject createRecordFor(SObjectType objType) {
        SObject newRecord = objType.newSObject();

        Map<String, SObjectField> fieldsMap = objType.getDescribe().fields.getMap();

        for(String field : fieldsMap.keySet()) {
            SObjectField sField = fieldsMap.get(field);

            DescribeFieldResult fDes = sField.getDescribe();

            if (!fDes.isAccessible() || !fDes.isCreateable()) {
                continue;
            }

            DisplayType fType = fDes.getType();
            
            if (fType == DisplayType.Reference) {
                continue;
            }

            Object value = null;

            if (fType == DisplayType.Boolean) {
                value = true;
            }

            if (fType == DisplayType.String) {
                String val = 'I am a random string';
                value = val.left(fDes.getLength());
            }

            if (fType == DisplayType.Date) {
                value = Date.today();
            }

            if (fType == DisplayType.DateTime) {
                value = DateTime.now();
            }

            if (fType == DisplayType.Integer) {
                value = Integer.valueOf(Math.random() * Math.pow(10, fDes.getDigits()));
            }

            if (fType == DisplayType.Double) {
                value = Math.random() * Math.pow(10, fDes.getPrecision());
            }

            if (fType == DisplayType.Picklist) {
                Integer randomPicklistValueIndex = Integer.valueOf(Math.random() * fDes.getPicklistValues().size());
                value = fDes.getPicklistValues().get(randomPicklistValueIndex).getValue();
            }

            if (fType == DisplayType.MultiPicklist) {
                String val = '';
                for(PicklistEntry pe : fDes.getPicklistValues()) {
                    val += pe.getValue() + ';'; 
                }
                value = val.subStringBeforeLast(';');
            }

            if (fType == DisplayType.TextArea) {
                String val = 'I am a very long text \r\n I am a very long text \r\n I am a very long text \r\n I am a very long text \r\n I am a very long text';
                value = val.left(fDes.getLength());
            }

            newRecord.put(sField, value);
        }

        return newRecord;
    }


    /**
     * Method ensures that the page name request method in SQX_Utilities is cached
     */
    static testmethod void ensurePageNameRequestIsCached() {

        // ARRANGE : Get full page name for random page
        String pageName = SQX_Utilities.getPageName('', 'SQX_Controlled_Document_Approval');
        Integer queriesSoFar = Limits.getQueries();

        // ACT : Query page name again
        String pageNameNew = SQX_Utilities.getPageName('', 'SQX_Controlled_Document_Approval');

        // ASSERT : No new query is fired and the correct result is returned
        System.assertEquals(pageName, pageNameNew);
        System.assertEquals(queriesSoFar, Limits.getQueries());

        // ACT : Query different page name
        pageNameNew = SQX_Utilities.getPageName('', 'SQX_Controlled_Document_Profile_Page');

        // ASSERT : new query is fired up returning different result
        System.assertNotEquals(pageName, pageNameNew);
        System.assertEquals(queriesSoFar + 1, Limits.getQueries());

    }

    /*
     * Method to test that only Name and Id field is queried from dynamic query for reference object
     */
    static testMethod void ensureIdAndNameFieldQueried(){
        //Arrange: Create case record
        Case comCase = new Case(Status='New',
                                Origin = 'Email',
                                Subject = 'Test');
        insert comCase;
        
        //Arrange: Create complaint record
        SQX_Complaint__c com = new SQX_Complaint__c(Country_of_Origin__c = 'US',
                                                    Company_Name__c = 'Company',
                                                    Complaint_Title__c = 'Test',
                                                    SQX_Source_Case__c  = comCase.Id);
        insert com;
        
        //Act: Query the fields from reference object        
        SQX_Utilities.setReferenceField(com, Schema.SQX_Complaint__c.SQX_Source_Case__c.getDescribe(), new Map<Id, SObject>{}, true);
        
        //Assert: Only Id and Name field should be queried
        System.assertEquals(true,com.SQX_Source_Case__r.Id != null,'Id field of case is queried');
        System.assertEquals(true,com.SQX_Source_Case__r.CaseNumber != null,'Name field of case is queried');

        try{
            System.Debug(com.SQX_Source_Case__r.Status);
        }
        catch(Exception ex){
            System.assertEquals('SObject row was retrieved via SOQL without querying the requested field: Case.Status',ex.getMessage(),'Fields other than Id and Name should not be queried');
        }
    }
}