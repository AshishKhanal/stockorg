/**
 * test class for complaint evaluation for investigation, phr and decision tree
 */
 @isTest
public class SQX_Test_7722_Complaint_Evaluation {
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(adminUser){
            
            // add custom setting to create evaluation records when related complaint task is present
            SQX_Custom_Settings_Public__c customSetting;
            customSetting = new SQX_Custom_Settings_Public__c( Community_URL__c = 'https://login.salesforce.com',
                                                                Org_Base_URL__c = SQX_Utilities.getBaseUrlForUser()+'/',
                                                                NC_Analysis_Report__c = 'abc123def',
                                                                Create_Complaint_Evaluation_Records__c = true);
            insert customSetting;

            // Add four cq task for complaint with different task types
            SQX_Test_Task task1 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task1.task.SQX_User__c = standardUser.Id;
            task1.save();
            
            SQX_Test_Task task2 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_INVESTIGATION).save();
            task2.task.SQX_User__c = standardUser.Id;
            task2.save();

            SQX_Test_Task task3 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_PHR).save();
            task3.task.SQX_User__c = standardUser.Id;
            task3.save();

            SQX_Test_Task task4 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_SAMPLE_REQUEST).save();
            task4.task.SQX_User__c = standardUser.Id;
            task4.save();
        }
    }

    /**
     * GIVEN : Complaint is created and Complaint Create Evaluation record is set to true.
     * WHEN : Complaint is initiated
     * THEN : Related evaluation records are created
     */
    @IsTest
    static void givenComplaint_WhenInitiated_EvaluationRecordsAreCreated(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser) {
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            // ACT : Complaint is initiated
            complaint.initiate();

            // ASSERT : Evaluation records are created
            System.assertEquals(4, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id].size());
            System.assertEquals(4, [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c =: complaint.complaint.Id].size());
            System.assertEquals(1, [SELECT Id FROM SQX_Investigation__C WHERE SQX_Complaint__c =: complaint.complaint.Id].size());

        }
      
    }

    /**
     * GIVEN : Complaint is initated with evaluation records
     * WHEN : Evaluation records are completed
     * THEN : Related complaint tasks are completed
     */
    @IsTest
    static void givenComplaintWithEvaluationRecords_WhenEvaluationRecordsAreCompleted_RelatedTasksAreCompleted(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser) {

            // ARRANGE : Complaint is created and initiated
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();

            System.assertEquals(4, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id].size());
            System.assertEquals(4, [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id].size());

            List<SQX_Complaint_Task__c> complaintTaskList = [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id];
            List<Task> sfTaskList = [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id];
            SQX_Product_History_Review__c phr = [SELECT Id, SQX_Task__c FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            SQX_Investigation__C investigation = [SELECT Id, SQX_Task__c FROM SQX_Investigation__C WHERE SQX_Complaint__c =: complaint.complaint.Id];

            // ACT : Investigation is completed
            Test.startTest();
            investigation.Investigation_Summary__c = 'Investigation Summary';
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            update investigation;

            SQX_Complaint_Task__c complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : investigation.SQX_Task__c];
            Task sfTask = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :complaintTask.Id];

            // ASSERT : Investigation complaint task is completed
            System.assertEquals(SQX_Steps_Trigger_Handler.STATUS_COMPLETE, complaintTask.Status__c);
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);

            // ACT : PHR is completed
            phr.Product_History_Review_Summary__c = 'PHR Summary';
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Completed_By__c = 'std user';
            update phr;

            complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : phr.SQX_Task__c];
            sfTask = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :complaintTask.Id];

            // ASSERT : PHR complaint task is completed
            System.assertEquals(SQX_Steps_Trigger_Handler.STATUS_COMPLETE, complaintTask.Status__c);
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Decision tree is ran
            SQX_Task__c dtTask = [SELECT Id, Name FROM SQX_Task__c WHERE Task_Type__c = : SQX_Task.TYPE_DECISION_TREE LIMIT 1];
            SQX_Decision_Tree__c decisionTree = complaint.runDecisionTreeTask(dtTask);

            decisionTree = [SELECT Id, SQX_Task__c FROM SQX_Decision_Tree__c WHERE SQX_Complaint__c =: complaint.complaint.Id LIMIT 1];

            complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : decisionTree.SQX_Task__c];
            sfTask = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :complaintTask.Id];

            // ASSERT : DT complaint task is completed
            System.assertEquals(SQX_Steps_Trigger_Handler.STATUS_COMPLETE, complaintTask.Status__c);
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);

            SQX_Task__c sampleTask = [SELECT Id FROM SQX_Task__c WHERE Task_Type__c = : SQX_Task.TYPE_SAMPLE_REQUEST LIMIT 1];

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Complaint task is completed
            complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : sampleTask.Id];
            complaintTask.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            update complaintTask;
            
            // ASSERT : SF Task is completed
            sfTask = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :complaintTask.Id];
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);

            System.assertEquals(4, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_COMPLETE].size());
            System.assertEquals(4, [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id AND Status = :SQX_Task.STATUS_COMPLETED].size());
            // System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id].Status__c);

        }
      
    }

    /**
     * GIVEN : Complaint is initiated with evaluation records
     * WHEN : Related complaint tasks are completed without completing evaluation records
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaintComplaintTask_WhenComplaintTaskIsCompletedDirectly_ErrorIsThrown(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser) {
            // ARRANGE : Complaint is initiated with evaluation records
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();

            System.assertEquals(4, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id].size());
            System.assertEquals(4, [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id].size());

            // ACT : Investigation complaint task is completed without completing investigation
            SQX_Investigation__C investigation = [SELECT Id, SQX_Task__c FROM SQX_Investigation__C WHERE SQX_Complaint__c =: complaint.complaint.Id];
            SQX_Complaint_Task__c complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : investigation.SQX_Task__c];
            complaintTask.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            Database.SaveResult result = Database.update(complaintTask, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_Prevent_Complaint_Task_Completion_for_PHR_and_Investigation), result.getErrors());

            // ACT : Investigation complaint task is completed by completing investigation
            investigation.Investigation_Summary__c = 'Investigation Summary';
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            update investigation;

            result = Database.update(complaintTask, false);

            // ASSERT : Save is succesful
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());

            // ACT : DT complaint task is completed without completing investigation
            complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE Task_Type__c = : SQX_Task.TYPE_DECISION_TREE LIMIT 1];
            complaintTask.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            result = Database.update(complaintTask, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_Prevent_Complaint_Task_Completion_for_DT_and_Script), result.getErrors());

            // ACT : Decision tree is ran
            SQX_Task__c dtTask = [SELECT Id, Name FROM SQX_Task__c WHERE Task_Type__c = : SQX_Task.TYPE_DECISION_TREE LIMIT 1];
            SQX_Decision_Tree__c decisionTree = complaint.runDecisionTreeTask(dtTask);

            decisionTree = [SELECT Id, SQX_Task__c FROM SQX_Decision_Tree__c WHERE SQX_Complaint__c =: complaint.complaint.Id LIMIT 1];

            complaintTask = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Task__c = : decisionTree.SQX_Task__c];
            Task sfTask = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :complaintTask.Id];

            // ASSERT : Task is completed
            System.assertEquals(SQX_Steps_Trigger_Handler.STATUS_COMPLETE, complaintTask.Status__c);
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);
        }
      
    }

    /**
     * Given : Open Complaint with one investigation type complint tasks thus one investigation record
     * When : Ivestigation is voided and another complaint task of type investigaton is created thus another investigation is created, ant try to complete the investigtion
     * Then : Investigation should be completed successfully and complaint task and sf task should be completed.
     */
    @IsTest
    static void givenOpenComplaintWithTwoInvestigationComplaintTask_WhenFirstInvestigationIsVoidAndSecondInvestigationIsCompleted_ComplaintTaskShouldBeCompleted(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            // ACT : Complaint is initiated
            complaint.initiate();

            // ASSERT : Evaluation records are created
            System.assertEquals(4, [SELECT Id FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id].size());
            System.assertEquals(4, [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id].size());
            List<SQX_Investigation__C> investigations = [SELECT Id, SQX_Task__c FROM SQX_Investigation__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            System.assertEquals(1, investigations.size());
            
            Test.startTest();
            
            // Act : void the investigation
            investigations[0].Status__c = SQX_Investigation.STATUS_VOID;
            update investigations;

            // Arrange: create another investigation type complaint task
            SQX_Complaint_Task__c task = new SQX_Complaint_Task__c(
                                                Applicable__c = true,
                                                SQX_User__c = standardUser.Id, 
                                                SQX_Complaint__c = complaint.complaint.Id,
                                                Due_Date__c = Date.today(),
                                                Name = 'Test Policy-1',
                                                SQX_Task__c = investigations[0].SQX_Task__c,
                                                RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ComplaintTask, 'Investigation'));
            insert task;
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :task.Id];
            System.assertEquals(1, sfTasks.size());
            investigations = [SELECT Id, SQX_Task__c FROM SQX_Investigation__c WHERE SQX_Complaint__c =: complaint.complaint.Id AND Status__c = :SQX_Investigation.STATUS_DRAFT];
            System.assertEquals(1, investigations.size());
            
            // Act: Complete Investigation
            investigations[0].Status__c = SQX_Investigation.STATUS_PUBLISHED;
            investigations[0].Conclusion__c = 'Confirmed';
            investigations[0].Investigation_Summary__c = 'Completing Investigation';
            update investigations;

            // Assert : Ensure complaint task is completed
            task = [SELECT Id, Status__c FROM SQX_Complaint_Task__c WHERE Id = :task.Id];
            System.assertEquals(SQX_Complaint_Task.STATUS_COMPLETE, task.Status__c);
            sfTasks = [SELECT Id, Status FROM Task WHERE WhatId = :complaint.complaint.Id AND Child_What_Id__c = :task.Id];
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTasks[0].Status);
            Test.stopTest();
        }
    }
}