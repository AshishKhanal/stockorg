/**
* Unit test for SQX-2810 # Inspection: Process Receipt.
* Material Transactions that meet the processing criteria must be processed for Receipts
* Errors during the processing must be captured in the material transaction record.
* @story   SQX-2810 Inspection: Process Receipt
* @author  Anuj Bhandari
* @version 1.0
* @since   2017-01-10
*/

@isTest
public class SQX_Test_2810_Inspection_Mtr_Trans_Batch {
    private static String ERROR_MESSAGE_MTR_PART_OR_PROCESS_REQUIERD = 'Please select either part or process',
                          ERROR_MESSAGE_MTR_PARTREV_FOR_PART_REQUIERD = 'Part rev is required for the part',
                          ERROR_MESSAGE_MTR_STATUS_CANNOT_BE_CHANGED  = 'Status of the successfully processed material transaction cannot be changed',
                          ERROR_MESSAGE_RCT_PART_OR_PROCESS_REQUIERD  = 'Please select at least one part or process',
                          ERROR_MESSAGE_RCT_PARTREV_FOR_PART_REQUIERD  = 'Rev is required for the part';

    /**
    * Test Setup method for material transaction batch processing.
    */
    @testSetup static void materialTransactionTestItems() {
        
        List<SQX_Material_Transaction__c> mtrList = new List<SQX_Material_Transaction__c>();
        SQX_Part__c testPart = SQX_Test_Part.insertPart(null,
                                                       new User(Id = UserInfo.getUserId()), 
                                                       true, '');
        Integer recordsNumber = 12;

        List<PicklistEntry> uoms = SQX_Material_Transaction__c.Unit_of_Measure__c.getDescribe().getPicklistValues();
        integer numberOfUoms = uoms.size();
        for (Integer i = 0; i < recordsNumber; i++) 
        {
          SQX_Material_Transaction__c item = new SQX_Material_Transaction__c();
          item.Inspection_Required__c=true;
          item.Part_Rev__c=string.valueOf(i);
          item.SQX_Part_Number__c= testPart.Id; 
          item.Status__c=SQX_Material_Transaction.STATUS_PENDING;
          item.External_Reference__c ='Test MTR Object';
          item.Transaction_Code__c = (math.mod(i, 2) == 0)? SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT : SQX_Material_Transaction.CODE_TRANSACTION_INSPECTION_CREATED;
          item.Unit_of_Measure__c = uoms.get(math.mod(i, numberOfUoms)).getValue();
          mtrList.add(item);   
        }        
        Database.SaveResult [] updateResults = new SQX_DB().op_insert(mtrlist,
                                                                      new List<Schema.SObjectField>{Schema.SQX_Material_Transaction__c.Status__c,
                                                                                                    Schema.SQX_Material_Transaction__c.Inspection_Required__c,
                                                                                                    Schema.SQX_Material_Transaction__c.Part_Rev__c,
                                                                                                    Schema.SQX_Material_Transaction__c.SQX_Part_Number__c,
                                                                                                    Schema.SQX_Material_Transaction__c.External_Reference__c,
                                                                                                    Schema.SQX_Material_Transaction__c.Transaction_Code__c,
                                                                                                    Schema.SQX_Material_Transaction__c.Unit_of_Measure__c
                                                                        }
                                                                     );
    }
    
    /**
    * Method to fetch the test records to execute the test.
    * @returns List of material transaction records from the test setup.
    */    
    static List<SQX_Material_Transaction__c> getTestMaterialTransactionList(){        
        return new List<SQX_Material_Transaction__c>( [SELECT  
                                                       ID,
                                                       Name,
                                                       Division__c,
                                                       Business_Unit__c,
                                                       Region__c,
                                                       Site__c,
                                                       External_Reference__c,
                                                       Line_Number__c,
                                                       Lot_Number__c,
                                                       Inspection_Required__c,
                                                       SQX_Part_Number__c,    
                                                       Part_Rev__c,
                                                       PO_WO_Number__c,
                                                       SQX_Process__c,
                                                       Quantity__c,
                                                       Supplier__c,
                                                       Comment__c,
                                                       Error_Code__c,
                                                       Error_Description__c,
                                                       Unit_of_Measure__c,
                                                       SQX_Receipt_Number__c,
                                                       SQX_Receipt_Number__r.Unit_of_Measure__c,
                                                       Status__c,
                                                       Transaction_Code__c,
                                                       Part_Number__c,
                                                       Process__c,
                                                       Supplier_Name__c 
                                                       FROM SQX_Material_Transaction__c
                                                       WHERE External_Reference__c ='Test MTR Object'
                                                       AND Transaction_Code__c=:SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT]);
    }
    
    /**
    * GIVEN:Transaction items with Code 'Incoming_Receipt' and  Status 'Pending' 
    * WHEN: Batch processes the given records
    * THEN: Only one receipt is created for a transaction item
    */
    testmethod static void givenWellFormedPendingTransactions_WhenBatchProcessesTheRecords_ThenOnlyOneRecieptIsCreatedForOneTransactionItem(){
        // ARRANGE: Setup 6 properly formed material transaction items
        List<SQX_Material_Transaction__c> testMtrTransactionItems = getTestMaterialTransactionList();
        List<SQX_Receipt__c> testReceiptItemsToAdd = new List<SQX_Receipt__c>();
        
        
        // ACT: Send the transaction to batch
        SQX_Material_Transaction mtrProcessor = new SQX_Material_Transaction(testMtrTransactionItems);
        
        //get all receipt item objects to add to the databse
        for (SQX_Material_Transaction__c transactionItem: testMtrTransactionItems) {
            testReceiptItemsToAdd.add(mtrProcessor.generateReceiptObjectToAdd(SQX_Material_Transaction.MATERIAL_TRANSACTION_TO_RECEIPT_MAP, transactionItem));
        }
        //add receipt objects to database
        Database.SaveResult [] testReceiptAddResult = mtrProcessor.addReceiptRecordsForBatchedMaterialTransactions(testReceiptItemsToAdd);
              
        // ASSERT: 1. No records are created for the failed transaction in receipt table
        //         2. There is one Receipt item created for the given transaction item
        if(testReceiptAddResult.size()>0)
        {
          for(Integer i = 0; i < testReceiptAddResult.size(); i++)
          {
            Database.SaveResult result = testReceiptAddResult[i];                
            Id recId = testReceiptItemsToAdd.get(i).SQX_Transaction_Number__c;
            integer receiptListSize = [SELECT COUNT() FROM SQX_Receipt__c WHERE SQX_Transaction_Number__c=:recId];
            if(!result.isSuccess()){                   
                system.assertEquals(receiptListSize, 0);
            }else{
                system.assertEquals(receiptListSize, 1);
            }
          }
        }
    }
    
    /**
    * GIVEN: Transaction items with Code 'Incoming_Receipt' and  Status 'Pending' 
    * WHEN: Batch processes the given records
    * THEN: Receipts are created
    */
    testmethod static void givenWellFormedPendingTransactions_WhenBatchProcessesTheRecords_ThenRecieptsAreCreated() {
        // ARRANGE: Setup 6 material transaction 
        List<SQX_Material_Transaction__c> testMaterialTransactionList = getTestMaterialTransactionList();

        // ACT: Send the transaction to batch
        SQX_Material_Transaction_Job_Processor mtrProcessor = new SQX_Material_Transaction_Job_Processor();
        mtrProcessor.execute(null,testMaterialTransactionList);     
        testMaterialTransactionList = getTestMaterialTransactionList();   
        
        // ASSERT: Ensure that receipt are created for the transactions
        for (SQX_Material_Transaction__c item : testMaterialTransactionList)
        {
          system.assertNotEquals( item.SQX_Receipt_Number__c, NULL);
          system.assertNotEquals( item.Status__c, 'Pending');
          system.assertEquals(item.Status__c, 'Completed');

          // added after
          // @story - 4154
          system.assertEquals(item.Unit_of_Measure__c, item.SQX_Receipt_Number__r.Unit_of_Measure__c);
        }
    }

    /**
    * GIVEN: Transaction items with Code 'Incoming_Receipt' and  Status 'Pending' and without part and process field
    * WHEN: Inserted into database
    * THEN: transaction item is not created
    */
    testmethod static void GivenTransactionItemWithoutPartAndProcess_WhenAddingRecords_ThenTransactionItemIsNotAdded() {
        // ARRANGE: Setup  material transaction to yield FIELD_CUSTOM_VALIDATION_EXCEPTION 
        List<SQX_Material_Transaction__c> mtrList = new List<SQX_Material_Transaction__c>();
               
        SQX_Material_Transaction__c item = new SQX_Material_Transaction__c();
        item.Inspection_Required__c=true;
        item.Part_Rev__c=null;
        item.SQX_Part_Number__c= null;
        item.SQX_Process__c= null;
        item.Status__c=SQX_Material_Transaction.STATUS_PENDING;
        item.External_Reference__c ='##!Test MTR Validation check Object!##';
        item.Transaction_Code__c =SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT;
        mtrList.add(item);   
      
        // ACT: Save the items
        Database.SaveResult [] dbResults = new SQX_DB().continueOnError().op_insert(mtrlist,
                                                                                    new List<Schema.SObjectField>{Schema.SQX_Material_Transaction__c.Status__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.Inspection_Required__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.Part_Rev__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.SQX_Process__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.SQX_Part_Number__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.External_Reference__c,
                                                                                                                  Schema.SQX_Material_Transaction__c.Transaction_Code__c
                                                                                    }
                                                                                 );
        
        // ASSERT: insert operation fails and material transaction is not created
        for (Database.SaveResult insertResult : dbResults)
        {                                                            
          System.assert(!insertResult.isSuccess());
          System.assert(insertResult.getErrors()[0].getMessage().contains(ERROR_MESSAGE_MTR_PART_OR_PROCESS_REQUIERD));
        }
       
        List<SQX_Material_Transaction__c> testImproperMaterialTransactionList = [SELECT  
                                                                                 ID
                                                                                 FROM SQX_Material_Transaction__c
                                                                                 WHERE External_Reference__c ='##!Test MTR Validation check Object!##'
                                                                                 AND Transaction_Code__c=:SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT];
        system.assertEquals(testImproperMaterialTransactionList.size(), 0);
    }


    /*
    * GIVEN: Transaction items with Code 'Incoming_Receipt' and  Status 'Pending' and without part but with rev field
    * WHEN: Inserted into database
    * THEN: transaction items are not created
    */
    testmethod static void GivenProductTypeTransactionItemWithRevButWithoutPart_WhenAddingRecords_ThenTransactionItemIsNotAdded() {
        // ARRANGE: Setup  material transactions to yield FIELD_CUSTOM_VALIDATION_EXCEPTION due to missing part for the product 
        List<SQX_Material_Transaction__c> mtrList = new List<SQX_Material_Transaction__c>();
        Integer recordsNumber = 5;       
        for (Integer i = 0; i < recordsNumber; i++) 
        {
            SQX_Material_Transaction__c item = new SQX_Material_Transaction__c();
            item.Inspection_Required__c=true;
            item.Part_Rev__c=string.valueOf(i);
            item.SQX_Part_Number__c= null;
            item.Status__c=SQX_Material_Transaction.STATUS_PENDING;
            item.External_Reference__c ='##!Test MTR Parts and Rev Validation check Object!##';
            item.Transaction_Code__c = SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT;
            mtrList.add(item);   
        }   
   
       // ACT: Save the items
       Database.SaveResult [] dbResults = new SQX_DB().continueOnError().op_insert(mtrlist,
                                                                                   new List<Schema.SObjectField>{Schema.SQX_Material_Transaction__c.Status__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.Inspection_Required__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.Part_Rev__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.SQX_Process__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.SQX_Part_Number__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.External_Reference__c,
                                                                                                                 Schema.SQX_Material_Transaction__c.Transaction_Code__c
                                                                   }
                                                                  );
        // ASSERT: Insert operation fails and material transactions are not created
        for (Database.SaveResult insertResult : dbResults)
        {
          System.assert(!insertResult.isSuccess());
          System.assert(insertResult.getErrors()[0].getMessage().contains(ERROR_MESSAGE_MTR_PART_OR_PROCESS_REQUIERD));
        }
       
         
         List<SQX_Material_Transaction__c> testImproperMaterialTransactionList = [SELECT  
                                                                                  ID
                                                                                  FROM SQX_Material_Transaction__c
                                                                                  WHERE External_Reference__c ='##!Test MTR Parts and Rev Validation check Object!##'
                                                                                  AND Transaction_Code__c=:SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT];
         system.assertEquals(testImproperMaterialTransactionList.size(), 0);
    } 
    
    /*
    * GIVEN: Receipt items without part but has rev
    * WHEN: Inserted into database
    * THEN: Receipt items are not created
    */
    testmethod static void GivenProductTypeReceiptItemWithRevButWithoutPart_WhenAddingRecords_ThenReceiptItemIsNotAdded() {
        // ARRANGE: Setup  material transactions to yield FIELD_CUSTOM_VALIDATION_EXCEPTION due to missing part for the product
        List<SQX_Receipt__c> mtrList = new List<SQX_Receipt__c>();
        Integer recordsNumber = 5;       
        for (Integer i = 0; i < recordsNumber; i++) 
        {
          SQX_Receipt__c item = new SQX_Receipt__c();
          item.Part_Revision__c=string.valueOf(i);
          item.SQX_Part_Number__c= null;
          item.External_Reference__c ='##!Test Receipt Parts and Rev Validation check Object!##';
          mtrList.add(item);   
        }   
   
        // ACT: Save the items
        Database.SaveResult [] dbResults = new SQX_DB().continueOnError().op_insert(mtrlist,
                                                                                    new List<Schema.SObjectField>{Schema.SQX_Receipt__c.Part_Revision__c,
                                                                                                                  Schema.SQX_Receipt__c.SQX_Process__c,
                                                                                                                  Schema.SQX_Receipt__c.SQX_Part_Number__c,
                                                                                                                  Schema.SQX_Receipt__c.External_Reference__c
                                                                                    }
                                                                                  );
      
        // ASSERT: DB operation fails and receipt are not created
        for (Database.SaveResult insertResult : dbResults)
        {
          System.assert(!insertResult.isSuccess());
          System.assert(insertResult.getErrors()[0].getMessage().contains(ERROR_MESSAGE_RCT_PART_OR_PROCESS_REQUIERD));
        } 
        
        List<SQX_Receipt__c> testImproperReceiptList = [SELECT  
                                                        ID
                                                        FROM SQX_Receipt__c
                                                        WHERE External_Reference__c ='##!Test Receipt Parts and Rev Validation check Object!##'];
        system.assertEquals(testImproperReceiptList.size(), 0);
    } 
    
    
    /**
    * GIVEN: Receipt items without part and process field values
    * WHEN: Inserted into database
    * THEN: Receipt items are not created
    */
    testmethod static void GivenReceiptItemWithoutPartAndProcess_WhenAddingRecords_ThenReceiptItemIsNotAdded() {
        // ARRANGE: Setup  material transactions to yield FIELD_CUSTOM_VALIDATION_EXCEPTION 
        List<SQX_Receipt__c> mtrList = new List<SQX_Receipt__c>();
        SQX_Receipt__c item = new SQX_Receipt__c();
        item.SQX_Part_Number__c= null;
        item.SQX_Process__c = null;
        item.External_Reference__c ='##!Test Receipt Parts and Rev Validation check Object!##';
        mtrList.add(item);   
             
        // ACT: Save the items      
        Database.SaveResult [] dbResults = new SQX_DB().continueOnError().op_insert(mtrlist,
                                                                                     new List<Schema.SObjectField>{Schema.SQX_Receipt__c.SQX_Process__c,
                                                                                                                   Schema.SQX_Receipt__c.SQX_Part_Number__c,
                                                                                                                   Schema.SQX_Receipt__c.External_Reference__c
                                                                    }
                                                                  );

        // ASSERT: DB operation fails and material transaction is not created
        for (Database.SaveResult insertResult : dbResults)
        {                                                            
           System.assert(!insertResult.isSuccess());
           System.assert(insertResult.getErrors()[0].getMessage().contains(ERROR_MESSAGE_RCT_PART_OR_PROCESS_REQUIERD));
        } 
        List<SQX_Receipt__c> testReceipts = [SELECT  
                                             ID
                                             FROM SQX_Receipt__c
                                             WHERE External_Reference__c ='##!Test Receipt Parts and Rev Validation check Object!##'];
        system.assertEquals(testReceipts.size(), 0);        
    }
    
    /**
    * GIVEN:Transaction items with Status 'Complete' and has corresponding receipt item
    * WHEN: Attempted to change the status 
    * THEN: Status is not changed
    */
    testmethod static void givenCompletedMaterialTransactions_WhenStatusIsChanged_ThenNewStatusWillNotBeApplied() {
        // ARRANGE: create properly formed material transactions and process them to get the corresponding receipt items 
        List<SQX_Material_Transaction__c> testMaterialTransactionList = getTestMaterialTransactionList();
        SQX_Material_Transaction_Job_Processor mtrProcessor = new SQX_Material_Transaction_Job_Processor();
        mtrProcessor.execute(null,testMaterialTransactionList);   
                   
        // ACT: Change the status of the completed material transactions
        testMaterialTransactionList = getTestMaterialTransactionList();
        List<SQX_Material_Transaction__c> updatedMtrList = new list<SQX_Material_Transaction__c>();
        for (SQX_Material_Transaction__c testImproperItem : testMaterialTransactionList)
        {
          testImproperItem.Status__c= SQX_Material_Transaction.STATUS_PENDING;
          updatedMtrList.add(testImproperItem);
        }
        Database.SaveResult [] updateResults = new SQX_DB().continueOnError().op_update(updatedMtrList,
                                                                                        new List<Schema.SObjectField>{Schema.SQX_Material_Transaction__c.Status__c
                                                                                        }
                                                                                      );
        
        // ASSERT: Ensure that the update fails and status remains to Completed.                                                                    
        for (Database.SaveResult updateResult : updateResults)
        {                                                            
           System.assert(!updateResult.isSuccess());
           System.assert(updateResult.getErrors()[0].getMessage().contains(ERROR_MESSAGE_MTR_STATUS_CANNOT_BE_CHANGED));
        }        
        List<SQX_Material_Transaction__c> testImproperMaterialTransactionList = getTestMaterialTransactionList();
        for (SQX_Material_Transaction__c item : testImproperMaterialTransactionList){
            system.assertEquals( item.Status__c, SQX_Material_Transaction.STATUS_SUCCESS);
            system.assertNotEquals( item.Status__c, SQX_Material_Transaction.STATUS_PENDING);
        }
    }    
}