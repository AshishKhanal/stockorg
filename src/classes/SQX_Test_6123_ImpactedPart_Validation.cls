/**
* Test case for either impacted part or impacted part name.
*/
@IsTest
public class SQX_Test_6123_ImpactedPart_Validation {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * GIVEN : given supplier deviation record without impacted part Or name
    * WHEN : save the record 
    * THEN : error is thrown
    */ 
    public static testMethod void giveSDWithoutImpactedPartOrName_WhenSaveTheRecord_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Create SD record
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            SQX_Part__c testPart = SQX_Test_Part.insertPart(null,new User(Id = UserInfo.getUserId()), true, '');
            SQX_Supplier_Deviation__c sd = new SQX_Supplier_Deviation__c(Name = 'TestName', 
                                               Phone__c = '1234567890',
                                               SQX_Account__c = account.Id, 
                                               SQX_External_Contact__c = contact.Id);
            
            //Act: Save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Deviation__c>{sd}, 
                                                                                        new List<SObjectField> {SQX_Supplier_Deviation__c.Name, 
                                                                                            SQX_Supplier_Deviation__c.Phone__c,
                                                                                            SQX_Supplier_Deviation__c.SQX_Account__c, 
                                                                                            SQX_Supplier_Deviation__c.SQX_External_Contact__c});
            
            //Assert: Ensured that throws validation error message
            System.assert(result[0].isSuccess() == false, 'Select either impacted part or impacted part name.');
            System.assertEquals('Please select either impacted part or impacted part name.', result[0].getErrors().get(0).getMessage());
        }
    }
    
    /**
    * GIVEN : given supplier deviation record without impacted part Or name
    * WHEN : save the record 
    * THEN : error is thrown
    */ 
    public static testMethod void giveSDWithImpactedPartOrName_WhenSaveTheRecord_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        //Impacted part name
        System.runAs(adminUser){
            //Arrange: Create SD record
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            SQX_Supplier_Deviation__c sd = new SQX_Supplier_Deviation__c(Name = 'TestName', 
                                               Phone__c = '1234567890',
                                               SQX_Account__c = account.Id, 
                                               SQX_External_Contact__c = contact.Id,
                                               Part_Name__c ='PartName');
            
            //Act: Save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Deviation__c>{sd}, 
                                                                                        new List<SObjectField> {SQX_Supplier_Deviation__c.Name, 
                                                                                            SQX_Supplier_Deviation__c.Phone__c,
                                                                                            SQX_Supplier_Deviation__c.SQX_Account__c, 
                                                                                            SQX_Supplier_Deviation__c.SQX_External_Contact__c});
            
            //Assert: Ensured that save the record
            System.assert(result[0].isSuccess() == true, 'Ssave successfully.');
        }
        //Impacted part
        System.runAs(adminUser){
            //Arrange: Create SD record
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            SQX_Part__c testPart = SQX_Test_Part.insertPart(null,new User(Id = UserInfo.getUserId()), true, '');
            SQX_Supplier_Deviation__c sd = new SQX_Supplier_Deviation__c(Name = 'TestName1', 
                                               Phone__c = '234567890',
                                               SQX_Account__c = account.Id, 
                                               SQX_External_Contact__c = contact.Id,
                                               SQX_Part__c = testPart.Id);
            
            //Act: Save the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Deviation__c>{sd}, 
                                                                                        new List<SObjectField> {SQX_Supplier_Deviation__c.Name, 
                                                                                            SQX_Supplier_Deviation__c.Phone__c,
                                                                                            SQX_Supplier_Deviation__c.SQX_Account__c, 
                                                                                            SQX_Supplier_Deviation__c.SQX_External_Contact__c});
            
            //Assert: Ensured that save the record
            System.assert(result[0].isSuccess() == true, 'Ssave successfully.');
        }
    }
}