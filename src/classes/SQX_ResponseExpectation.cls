/**
* A class that responsible for ensuring that a JSON response submitted is based on the expectation
* that we have. Example: A response might require at least one response object and the response must
* be for the Finding of main record.
*
* This class might become redundant because we are already checking for FLS/CRUD access, however this
* acts as a sanity check for the JSON string that we are sending as a response.
*/
public with sharing class SQX_ResponseExpectation implements SQX_Upserter.SQX_Upserter_Interceptor{

    /** Value that must be passed when Any number of certain components are to be supported.*/
    final public integer ANY_NUMBER = -1;
    
    /**
    * This property is used to set the expectation regarding the number of objects that are
    * permitted based on String SObjectType
    */
    public Map<String, Integer> numberOfObjectsToBeCreated {get; set;}

    /**
    * This property is used to set the expectation of values of field for a particular object type
    * Example: Lets say you want to ensure that Response in the JSON is related to the particular
    * Finding we could send a configuration as
    * valueChecks.put('compliancequest__SQX_Finding_Response__c', new Map&gt;SObjectField, Id&lt;{
    *       SQX_Finding_Response__c.SQX_Finding__c => Finding.Id
    * })
    */
    public Map<String, Map<SObjectField, Id>> valueChecks {get; private set;}

    /**
    * This property is used to set the expected number of objects that have to be set for a particular
    * object type.
    * Example: If an audit response must be present you can send
    * requiredCount.put('compliancequest__SQX_Audit_Response__c', 1);
    */
    public Map<String, Integer> requiredCount {get; set;}

    /**
    * Represents if the request has to be sent for approval or not. This is a suggestive field.
    * If requirements need the request to go through approval, it will still go through.
    */
    public boolean sendForApproval {get; set;}

    /**
    * The type of main response that is being processed currently there are only two main types
    * Audit Response and Response type
    */
    public String responseType {get; private set;}

    /**
    * The main response object that has been processed for the object
    */
    private SObject mainResponse;


    /**
    * The constructor for response expectation, that processes the response.
    * @param mainResponseType the response type that is related to the expectation.
    */
    public SQX_ResponseExpectation(String mainResponseType) {
        valueChecks = new Map<String, Map<SObjectField, Id>>();
        sendForApproval = false;
        responseType = mainResponseType;
    }

    /**
    * The method that is invoked by Upserter to check if the upsert JSON meets the expectation or not.
    * This method checks if the required count if provided matches with the list of objects in the list
    * @param objectsPersisted the list of objects that have been keyed by their type
    * @return returns <code>true</code> if the response is compliant with the expectation else returns <code>false</code>
    */
    public boolean meetsExpectation(Map<String, List<SObject>> objectsPersisted){
        boolean isAsDesired = true;

        if (requiredCount != null && requiredCount.size() > 0) {
            for (String key : requiredCount.keySet()) {
                Integer neededCount = requiredCount.get(key);
                Integer actualCount = objectsPersisted.containsKey(key) ? objectsPersisted.get(key).size() : 0;

                isAsDesired = isAsDesired && actualCount == neededCount;
                if(!isAsDesired) {
                    break;
                }
            }
        }

        return isAsDesired;
    }

    /**
    * This method checks if a particular type of object that is being manipulated can be changed i.e. inserted/updated.
    * In this particular instance it ensures that the object doesn't exceed the required count specified in 'numberOfObjectsToBeCreated'
    * and that particular field in the object matches with the value provided in 'valueChecks'
    * @param sObjectType the type that is being updated
    * @param objectToBeManipulated the object that is being inserted/updated
    * @param isInsertion value indicating whether it is insertion or not
    * @return returns <code>true</code> if the object can be manipulated else returns <code>false</code>
    */
    public boolean canManipulate(String sObjectType, SObject objectToBeManipulated, boolean isInsertion){
        boolean canBeManipulated = numberOfObjectsToBeCreated != null && numberOfObjectsToBeCreated.containsKey(sObjectType);

        if (canBeManipulated) {
            Integer numberOfObjectsCreated = numberOfObjectsToBeCreated.get(sObjectType);
            canBeManipulated = numberOfObjectsCreated > 0 || numberOfObjectsCreated == ANY_NUMBER; 
            if (canBeManipulated) {

                if (numberOfObjectsCreated != ANY_NUMBER) {
                    //don't do anything for ANY_NUMBER because you can create as much as you want until you hit the DML limit ;-)
                    numberOfObjectsCreated--;
                    numberOfObjectsToBeCreated.put(sObjectType, numberOfObjectsCreated);
                }

                if (valueChecks.containsKey(sObjectType)) {
                    Set<String> objChangeMap = objectToBeManipulated.getPopulatedFieldsAsMap().keySet();

                    Map<SObjectField, Id> fieldValue = valueChecks.get(sObjectType);
                    for (SObjectField field : fieldValue.keySet()) {

                        Object val = objectToBeManipulated.get(field);
                        // if it is update then permit missing field value enforcement, we can assume that it hasn't been changed since last insert
                        if (val != fieldValue.get(field) && (isInsertion || (!isInsertion  && objChangeMap.contains(String.valueOf(field))))) {
                            canBeManipulated = false;
                            break;
                        }
                    }
                }
            }            
        }

        return canBeManipulated;
    }

    /**
    * Performs post actions on the response objects once it is persisted. It either publishes/submits a response for approval
    * @param objectsPersisted the map of objects that have been persisted by object type
    * @param setWithTempIdToPersistedObject the map of objects by their temporary key.
    */
    public void successfullyPersisted(Map<String, List<SObject>> objectsPersisted,  Map<String, sObject> setWithTempIdToPersistedObject){
        boolean submitForApproval = sendForApproval;

        // TODO: Both the response type have a lot of similarity we need to use an interface or abstract class
        if (responseType == SQX.AuditResponse) {
            mainResponse = objectsPersisted.get(SQX.AuditResponse)[0];
            SQX_Audit_Response response = new SQX_Audit_Response((SQX_Audit_Response__c)mainResponse);
            submitForApproval = submitForApproval || response.requiresApproval();

            if (submitForApproval) {
                response.submitForApproval();
            }
            else {
                response.publish();
            }
        }
        else {
            mainResponse = objectsPersisted.get(SQX.Response)[0];
            SQX_Finding_Response response = new SQX_Finding_Response(mainResponse.Id);
            submitForApproval = submitForApproval || response.requiresApproval();

            if (submitForApproval) {
                response.submitForApproval();
            }
            else {
                response.publish();
            }
        }
    }

    /**
    * Returns the response that was processed
    */
    public SObject getProcessedRecord(){
        return mainResponse;
    }

    /**
    * Returns the Id of the response record that was processed
    */
    public String getProcessedRecordId(){
        return mainResponse.Id;
    }
}