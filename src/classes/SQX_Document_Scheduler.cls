/**
* This is a common scheduler for all document related tasks
*/
global without sharing class SQX_Document_Scheduler  implements Schedulable{
    String testHelperSuffix = Test.isRunningTest() ? '-Test': '';
    final String DOC_REND_RETR = 'CQ-Document Rendition Retriever' + testHelperSuffix,
        DOC_STATUS_CHECKER = 'CQ-Document Rendition Status Checker' + testHelperSuffix,
        DOC_BATCH_PROCESS = SQX_Document_Batch_Job_Processor.JOB_NAME + testHelperSuffix,
        REQ_ACTIVATION_PROCESS = new SQX_Activated_Requirement_Processor().JOB_NAME + testHelperSuffix,
        REQ_DEACTIVATION_PROCESS = new SQX_Deactivated_Requirement_Processor().JOB_NAME + testHelperSuffix,
        COMPLETED_SESSION_ROSTER_JOB = 'CQ-Completed Session Roster Processing Job' + testHelperSuffix,
        CLOSED_SESSION_ROSTER_JOB = 'CQ-Closed Session Roster Processing Job' + testHelperSuffix;
    public String[] jobNames = new String[] {
        DOC_REND_RETR,
        DOC_STATUS_CHECKER,
        DOC_BATCH_PROCESS,
        REQ_ACTIVATION_PROCESS,
        REQ_DEACTIVATION_PROCESS,
        COMPLETED_SESSION_ROSTER_JOB,
        CLOSED_SESSION_ROSTER_JOB
    };
    global void execute(SchedulableContext sc){
        Set<String> jobStatuses = new Set<String>();
        SQX_Custom_Settings_Public__c cqSetting = SQX_Custom_Settings_Public__c.getInstance();

        List<CronTrigger> jobsToDelete = new List<CronTrigger>();
        for(CronTrigger tr : [SELECT CronJobDetail.Name, Id, NextFireTime
                              FROM CronTrigger 
                              WHERE CronJobDetail.Name IN : jobNames]){

            // if next scheduled date has not been set, clear the job and start it again [SQX-4494]
            if(tr.NextFireTime == null)
            {
                jobsToDelete.add(tr);
            }
            else
            {
                jobStatuses.add(tr.CronJobDetail.Name);
            }

        }

        for(CronTrigger job: jobsToDelete){
            // delete jobs whose next scheduled date have not been set, 
            // try/catch because the job might not exist 
            // if it's being aborted manually or from another execution
            try{
                System.abortJob(job.id);
            } catch (exception e) {
                System.debug('Could not abort job '+job.id+', Error: '+e);
            }
        }

        if(!jobStatuses.contains(DOC_REND_RETR)){
            SQX_RenditionBatchProcessor processor = new SQX_RenditionBatchProcessor();
            processor.Mode = SQX_RenditionBatchProcessor.MODE_SUBMIT_AND_RETREIVE;
            processor.jobName = DOC_REND_RETR;
            processor.batchSize = 1;

            System.scheduleBatch(processor, DOC_REND_RETR, processor.SCHEDULE_AFTER_MIN, 1);
        }

        if(!jobStatuses.contains(DOC_STATUS_CHECKER)){
            SQX_RenditionBatchProcessor processor = new SQX_RenditionBatchProcessor();
            processor.Mode = SQX_RenditionBatchProcessor.MODE_POLL_AND_ABORT;
            processor.jobName = DOC_STATUS_CHECKER;
            processor.batchSize = SQX_RenditionBatchProcessor.POLL_BATCH_SIZE;
            System.scheduleBatch(processor, DOC_STATUS_CHECKER, processor.SCHEDULE_AFTER_MIN, processor.batchSize);
        }

        if(!jobStatuses.contains(DOC_BATCH_PROCESS) && cqSetting.Use_Batch_For_Document_Status_Change__c == true){
            SQX_Document_Batch_Job_Processor processor = new SQX_Document_Batch_Job_Processor();
            
            System.scheduleBatch(processor, DOC_BATCH_PROCESS, processor.SCHEDULE_AFTER_MIN, processor.BATCH_SZE);
        }

        if(!jobStatuses.contains(REQ_ACTIVATION_PROCESS) && cqSetting.Use_Batch_Job_To_Process_REQ_Training__c == true){
            SQX_Activated_Requirement_Processor processor = new SQX_Activated_Requirement_Processor();
            System.scheduleBatch(processor, REQ_ACTIVATION_PROCESS, processor.SCHEDULE_AFTER_MIN, SQX_Activated_Requirement_Processor.BATCH_SIZE);
        }

        if(!jobStatuses.contains(REQ_DEACTIVATION_PROCESS) && cqSetting.Use_Batch_Job_To_Process_REQ_Training__c == true){
            
            SQX_Deactivated_Requirement_Processor processor = new SQX_Deactivated_Requirement_Processor();
            
            System.scheduleBatch(processor, REQ_DEACTIVATION_PROCESS, processor.SCHEDULE_AFTER_MIN, processor.BATCH_SIZE);
        }
        
        if (!jobStatuses.contains(COMPLETED_SESSION_ROSTER_JOB) && cqSetting.Use_Batch_For_Session_Roster_Processing__c == true) {
            SQX_TrainingSessionRoster_Processor processor = new SQX_TrainingSessionRoster_Processor();
            processor.mode = SQX_TrainingSessionRoster_Processor.MODE_DOCUMENT_TRAINING_GENERATION;
            processor.jobName = COMPLETED_SESSION_ROSTER_JOB;
            System.scheduleBatch(processor, processor.jobName, processor.scheduleAfterMin, processor.batchSize);
        }
        
        if (!jobStatuses.contains(CLOSED_SESSION_ROSTER_JOB) && cqSetting.Use_Batch_For_Session_Roster_Processing__c == true) {
            SQX_TrainingSessionRoster_Processor processor = new SQX_TrainingSessionRoster_Processor();
            processor.mode = SQX_TrainingSessionRoster_Processor.MODE_DOCUMENT_TRAINING_COMPLETION;
            processor.jobName = CLOSED_SESSION_ROSTER_JOB;
            System.scheduleBatch(processor, processor.jobName, processor.scheduleAfterMin, processor.batchSize);
        }
    }


}