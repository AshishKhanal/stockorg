/**
* class used for to update audit in supplier steps
*/
global with sharing class SQX_Update_Supplier_Steps {
    /**
    * update audit in supplier steps
    */ 
    @InvocableMethod(Label='Update Supplier Steps')
    global static void updateSupplierSteps(List<SQX_Supplier_Steps> steps){
        try{
            SQX_Supplier_Steps step = steps[0];
            SObjectType recordtype = step.StepId.getSobjectType();
            SObject sobjUpdate = recordtype.newSObject();
            sobjUpdate.put('Id',step.StepId);
            sobjUpdate.put('SQX_Audit_Number__c',step.AuditId); 
            /**
            * WITHOUT SHARING has been used 
            * ---------------------------------
            * Without sharing has been used because the assignee user might not have access to update audit in steps
            * This will cause and error.
            */
            new SQX_DB().withoutSharing().op_update(new List<SObject> { sobjUpdate },new List<SObjectField> { });
            
        }catch(Exception ex){
            AuraHandledException e = new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
            e.setMessage(ex.getMessage() + ex.getStackTraceString());
            throw e;
        }
    }
}