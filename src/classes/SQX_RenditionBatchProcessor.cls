/**
* Rendition Batch Processor processes the pending automatic rendition generation for Controlled Document Objects
*/
global without sharing class SQX_RenditionBatchProcessor implements Database.Batchable <SQX_Controlled_Document__c>, Database.AllowsCallouts{
   
    //default batch size of 50 for the next schedule
    public static Integer POLL_BATCH_SIZE = 50;
    //Instance specific configurable variable for next schedule in minutes
    global Integer SCHEDULE_AFTER_MIN = 1;

    //this flag can be used in tests to disable rescheduling
    //for normal usage this value is always true, for tests it is set to false but can be changed
    public static boolean RESCHEDULE = !Test.isRunningTest();

    public String mode {get; set;}
    public String jobName {get; set;}
    global Integer batchSize {get; set;}

    public static final String MODE_POLL_AND_ABORT = 'PollAndAbort',
                               MODE_SUBMIT_AND_RETREIVE = 'SubmitAndRetrieve';

    global Iterable<SQX_Controlled_Document__c> start(Database.BatchableContext bc){
        return new DocIterable(this.mode);
    }


    /**
    * Default batch method that polls/aborts/submits/retrieves rendition job
    */
    global void execute(Database.BatchableContext bc,List<SQX_Controlled_Document__c> docs){

        if(mode == MODE_POLL_AND_ABORT){
            List<SQX_Controlled_Document__c> docsToAbort = new List<SQX_Controlled_Document__c>();
            List<SQX_Controlled_Document__c> docsToPoll = new List<SQX_Controlled_Document__c>();

            Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__c>(docs);
            Set<Id> docIds = docMap.keySet();

            List<SQX_Rendition_Log__c> staleLogs = SQX_Rendition_Log.getAllStaleLogs();

            Map<Id, SQX_Rendition_Log__c> logs = SQX_Rendition_Log.getLogsFor(docs);

            Set<Id> abortedDueToMaxRetries = new Set<Id>();

            for(SQX_Controlled_Document__c doc : docs){
                if(logs.containsKey(doc.Id)){
                    SQX_Rendition_Log__c log = logs.get(doc.Id);
                    if(log.Status__c == SQX_Rendition_Log.STATUS_ABORT){
                        docsToAbort.add(doc);
                    }
                    else if(log.Status__c == SQX_Rendition_Log.STATUS_ABORT_MAX_RETRIES){
                        docsToAbort.add(doc);
                        abortedDueToMaxRetries.add(doc.Id);
                    }
                    else if(log.Status__c != SQX_Rendition_Log.STATUS_FETCHING){
                        docsToPoll.add(doc);
                    }
                }
            }

            if(docsToAbort.size() > 0 || docsToPoll.size() > 0){
                
                Map<SQX_RenditionRequest, SQX_RenditionResponse> requestResponseMap = SQX_Controlled_Document.pollOrAbortContentGeneration(docsToPoll, docsToAbort, abortedDueToMaxRetries);
                        
                for(SQX_RenditionRequest request : requestResponseMap.keySet()){
                    SQX_RenditionResponse response = requestResponseMap.get(request);
                    if(response.Status == SQX_RenditionResponse.Status.RenditionReady){
                    }
                }

            }

            if(staleLogs.size() > 0){
                new SQX_DB().op_delete(staleLogs);
            }

        }
        else{
            System.assertEquals(1, docs.size(), 'Only one controlled document can be processed while retrieving/submitting');
            SQX_Controlled_Document__c doc = docs.get(0);

            SQX_Controlled_Document.processRequest(doc.SQX_Rendition_Logs__r != null && doc.SQX_Rendition_Logs__r.size() > 0, doc);

        }

    }


    /**
    * Automatically schedules the job once the batch finishes.
    */
    global void finish(Database.BatchableContext bc){
        try{
            if(RESCHEDULE){
                
                System.scheduleBatch(this, this.jobName , SCHEDULE_AFTER_MIN , this.batchSize);
                // + system.now().format('hhmmss',UserInfo.getTimeZone().toString())
            }
        }
        catch(Exception ex){
            // ignore exception while trying to schedule because cron job can respawn in case of conflict
        }
    } 


    /**
    * This class returns the Iterable that returns controlled documents
    */
    public class DocIterable implements Iterable<SQX_Controlled_Document__c>{

        public String mode {get; set;}

        public DocIterable(String mode){
            this.mode = mode;
        }
        
        public Iterator<SQX_Controlled_Document__c> Iterator(){
            return new DocIterator(mode);
        }
    }


    /**
    * Document Iterator gets the list of controlled document that are valid for the current mode
    * If a rendition log exists or has fetching status it will add the doc for retrieval
    * Else if document has rendition log but in other status it will add for polling.
    */
    public class DocIterator implements Iterator<SQX_Controlled_Document__c>{

        private List<SQX_Controlled_Document__c> docs;
        Integer i;

        public DocIterator(String mode){
            this.docs = new List<SQX_Controlled_Document__c>();
            i = 0;

            List<SQX_Controlled_Document__c> validDocs = [SELECT Id, RecordTypeId, Content_Reference__c, Description__c, Title__c, Synchronization_Status__c, Approval_Status__c, Document_Status__c, Secondary_Content_Reference__c, OwnerId, Draft_Vault__c, (SELECT Id, Status__c FROM SQX_Rendition_Logs__r)
                                         FROM SQX_Controlled_Document__c
                                         WHERE Secondary_Content__c = : SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO AND
                                         (
                                            Synchronization_Status__c = : SQX_Controlled_Document.SYNC_STATUS_PENDING
                                            OR (
                                                Synchronization_Status__c = : SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC
                                                AND (Approval_Status__c = : SQX_Controlled_Document.APPROVAL_APPROVED OR Approval_Status__c = : SQX_Controlled_Document.APPROVAL_OBSOLESCENCE_APPROVED OR Approval_Status__c = : SQX_Controlled_Document.APPROVAL_OBSOLESCENCE_APPROVAL)
                                            )
                                         )];

            for(SQX_Controlled_Document__c doc : validDocs){
                Boolean addDoc = false;

                if(doc.SQX_Rendition_Logs__r == null || doc.SQX_Rendition_Logs__r.size() == 0 ){
                    // record hasn't been processed should only be added to docs if it is single record
                    addDoc = mode == MODE_SUBMIT_AND_RETREIVE;
                }
                else{
                    SQX_Rendition_Log__c log = doc.SQX_Rendition_Logs__r.get(0);
                    if(log.Status__c == SQX_Rendition_Log.STATUS_FETCHING){
                        addDoc = mode == MODE_SUBMIT_AND_RETREIVE;
                    }
                    else{
                        addDoc = mode == MODE_POLL_AND_ABORT;
                    }
                }

                if(addDoc){
                    docs.add(doc);
                }
            }
        }

        /**
        * checks if there are any more controlled documents to be processed
        */
        public boolean hasNext(){
            return i < docs.size();
        }

        /**
        * returns the next available controlled
        */
        public SQX_Controlled_Document__c next(){
            SQX_Controlled_Document__c doc = null;

            if(hasNext()){
                doc = docs.get(i);
                i++;
            }

            return doc;
        }
    }



}