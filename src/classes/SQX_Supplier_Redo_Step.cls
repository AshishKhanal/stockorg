/**
 * Invocable class to redo steps in supplier management
 */
global with sharing class SQX_Supplier_Redo_Step {
    
    // action type in the flow
    public static final String ACTION_TYPE_PRE = 'preScreenAction',
        ACTION_TYPE_POST = 'postScreenAction';
    
    // screen type to show in flow
    final static String SCREEN_TYPE_SCREEN_WITHOUT_CONTROLLED_DOCUMENT = 'screenWithoutControlledDocument',
        SCREEN_TYPE_WITH_CONTRLLED_DOCUMENT = 'screenWithControlledDocument';
    
    static String OUTPUT_ERROR_MESSAGE = '',
        OUTPUT_SCREEN_TYPE = '';
        
    
    @InvocableMethod(label='Redo Step')
    global static List<RedoStepResult> redoSteps(List<RedoStepRequest> requests) {
        
        // Since invocable method receives parameters as List but from flow we pass only one record
        // we process only one record from the list and return only one result adding it to the list
        RedoStepRequest request = requests.get(0);
        RedoStepResult result = redoStep(request);
        return new List<RedoStepResult> {result};
    }


     /**
     * method to redo step
     */
    public static RedoStepResult redoStep(RedoStepRequest request) {
        
        String actionType = (String) request.actionType;
        
        Id recordId = (Id) request.recordId;
        
        String objectName = recordId.getSObjectType().getDescribe().getName();
        
        SObject record = Database.query(SQX_Steps_Trigger_Handler.getStepDetailQuery(recordId));
        
        RedoStepResult result;
        if(actionType == ACTION_TYPE_PRE){
            result = preScreenActions(record, request, objectName);
        } else if(actionType == ACTION_TYPE_POST){
            result = postScreenActions(record, request, objectName);
        }
        return result;
    }
    
    /**
     * method to process step before redo screen is thrown
     * @param record step which is to be redone
     * @param request request details of the flow
     * @param objectName step api name
     * @return result from the flow
     */
    private static RedoStepResult preScreenActions(SObject record, RedoStepRequest request, String objectName){
        
        RedoStepResult result = new RedoStepResult();
        
        String recordStatus = String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_STATUS));
        Boolean isRecordArchived = Boolean.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_ARCHIVED));
        
        if(recordStatus == SQX_Steps_Trigger_Handler.STATUS_COMPLETE){
            if(!isRecordArchived){
                Id currentAssigneeId = (Id) String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_ASSIGNEE));
                User currentAssignee = [SELECT Id, Name FROM User WHERE Id = :currentAssigneeId];
                
                Id docId = (Id) String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_CONTROLLED_DOCUMENT));
                Id stepRecordTypeId = (Id) String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_RECORDTYPE_ID));

                Id docRequestRecordTypeId = SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new List<String>{ objectName })
                                                            .get(objectName)
                                                            .get(SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
                
                result.stepName = String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_NAME));
                result.stepDescription = String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_DESCRIPTION));
                result.stepAssigneeId = String.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_ASSIGNEE));
                result.stepAssigneeName = String.valueOf(record.getSObject(SQX_Steps_Trigger_Handler.FIELD_ASSIGNEE_RELATIONSHIP).get(SQX_Steps_Trigger_Handler.FIELD_NAME));
                result.stepNumber = Integer.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_STEP));
                result.stepAllowedDays = Integer.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_ALLOWED_DAYS));
                result.stepDueDate = Date.valueOf(record.get(SQX_Steps_Trigger_Handler.FIELD_DUE_DATE));
                
                if(docId != null && docRequestRecordTypeId == stepRecordTypeId){
                    SObject docFieldsMap = record.getSObject(SQX_Steps_Trigger_Handler.FIELD_CONTROLLED_DOCUMENT_RELATIONSHIP);
                    String documentStatus = String.valueOf(docFieldsMap.get(SQX_Controlled_Document.FIELD_STATUS));
                    String documentNumber = String.valueOf(docFieldsMap.get(SQX_Controlled_Document.FIELD_DOCUMENT_NUMBER));
                    
                    if(documentStatus == SQX_Controlled_Document.STATUS_PRE_RELEASE || documentStatus == SQX_Controlled_Document.STATUS_CURRENT){
                        
                        OUTPUT_SCREEN_TYPE = SCREEN_TYPE_SCREEN_WITHOUT_CONTROLLED_DOCUMENT;
                    } else {
                        List<SQX_Controlled_Document__c> docs = [SELECT Id, Document_Status__c, Document_Number__c 
                                                                 FROM SQX_Controlled_Document__c 
                                                                 WHERE Document_Number__c = :documentNumber 
                                                                 AND (Document_Status__c = :SQX_Controlled_Document.STATUS_PRE_RELEASE OR Document_Status__c = :SQX_Controlled_Document.STATUS_CURRENT)];
                        
                        if(docs.size() > 0){
                            result.stepDocumentNumber = documentNumber;
                            OUTPUT_SCREEN_TYPE = SCREEN_TYPE_WITH_CONTRLLED_DOCUMENT;
                        } else {
                            OUTPUT_ERROR_MESSAGE = Label.CQ_ERR_MSG_CURRENT_DOCUMENT_NOT_AVAILABLE;
                            
                        }
                    }
                } else {
                    OUTPUT_SCREEN_TYPE = SCREEN_TYPE_SCREEN_WITHOUT_CONTROLLED_DOCUMENT;
                }                
            } else {
                OUTPUT_ERROR_MESSAGE = Label.CQ_ERR_MSG_STEP_ALREADY_ARCHIVED;
            }
            
        } else {
            OUTPUT_ERROR_MESSAGE = Label.CQ_ERR_MSG_STEP_CANNOT_BE_REDONE;
        }
        result.errorMessage = OUTPUT_ERROR_MESSAGE;
        result.screenType = OUTPUT_SCREEN_TYPE;
        
        return result;
    }
    
    /**
     * method to process step after redo screen is shown
     * @param record step which is to be redone
     * @param request request details of the flow
     * @param objectName step api name
     * @return result from the flow
     */
    private static RedoStepResult postScreenActions(SObject record, RedoStepRequest request, String objectName){
        Savepoint sp = null;
        RedoStepResult result = new RedoStepResult();
        Id assigneeId = (Id) request.assignee;
        Decimal stepNumber = (Decimal) request.step;
        String stepDescription = (String) request.description;
        String stepName = (String) request.name;
        Id recordId = (Id) request.recordId;
        Decimal  allowedDays =(Decimal) request.allowedDays;
        Date dueDate =(Date) request.dueDate;
        
        SObject docFieldsMap = record.getSObject(SQX_Steps_Trigger_Handler.FIELD_PARENT_RECORD_RELATIONSHIP);
        Decimal parentStepNumber = (Decimal) docFieldsMap.get(SQX_Steps_Trigger_Handler.FIELD_PARENT_RECORD_CURRENT_STEP);
        String parentStepStatus = (String) docFieldsMap.get(SQX_Steps_Trigger_Handler.FIELD_PARENT_RECORD_STATUS);
        
        //System.assert(false, 'stepNumber>' + stepNumber + 'parentStepNumber>' + parentStepNumber);
        if(stepNumber < parentStepNumber && parentStepStatus != SQX_Supplier_Common_Values.STATUS_COMPLETE){
            OUTPUT_ERROR_MESSAGE = Label.CQ_ERR_MSG_ADD_VALID_STEPS;
        } else {
            sp = Database.setSavepoint();
            SObject cloneRecord = record.clone();
            if(request.controlledDocument != ''){
                Id documentId = (Id) request.controlledDocument;
                cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_CONTROLLED_DOCUMENT, documentId);
            }
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_ASSIGNEE, assigneeId);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_STEP, stepNumber);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_DESCRIPTION, stepDescription);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_NAME, stepName);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_PARENT_STEP, recordId);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_STATUS, SQX_Steps_Trigger_Handler.STATUS_DRAFT);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_ALLOWED_DAYS, allowedDays);
            cloneRecord.put(SQX_Steps_Trigger_Handler.FIELD_DUE_DATE, dueDate);
            try {
                new SQX_DB().op_insert(new List<SObject>{cloneRecord}, new List<Schema.SObjectField>{});
                record.put(SQX_Steps_Trigger_Handler.FIELD_ARCHIVED, true);
                record.put(SQX_Steps_Trigger_Handler.FIELD_REDO_POLICY, true);
                record.put('id', recordId);
                new SQX_DB().op_update(new List<SObject>{record}, new List<Schema.SObjectField>{});
            } catch(Exception ex) {
                OUTPUT_ERROR_MESSAGE = ex.getMessage();
                Database.rollback(sp);
            }          
        }
        
        result.errorMessage = OUTPUT_ERROR_MESSAGE;
        
        return result;
    }
    
    /**
     * inner class to fetch parameters from flow
     */
    global with sharing class RedoStepRequest {
        
        @InvocableVariable(required=true)
        global String actionType;
        
        @InvocableVariable(required=true)
        global String recordId;
        
        @InvocableVariable
        global Integer step;
        
        @InvocableVariable
        global String assignee;
        
        @InvocableVariable
        global String controlledDocument;
        
        @InvocableVariable
        global String description;
        
        @InvocableVariable
        global String name;
         
        @InvocableVariable
        global Integer allowedDays;
        
        @InvocableVariable
        global Date dueDate;
    }
    
    /**
     * inner class to set parameters to flow
     */
    global with sharing class RedoStepResult {
        @InvocableVariable
        global String errorMessage;
        
        @InvocableVariable
        global String screenType;
        
        @InvocableVariable
        global String stepName;
        
        @InvocableVariable
        global String stepDescription;
        
        @InvocableVariable
        global Integer stepNumber;
        
        @InvocableVariable
        global String stepDocumentNumber;
        
        @InvocableVariable
        global String stepAssigneeId;
        
        @InvocableVariable
        global String stepAssigneeName;
        
        @InvocableVariable
        global Integer stepAllowedDays;
        
         @InvocableVariable
        global Date stepDueDate;
    }
}