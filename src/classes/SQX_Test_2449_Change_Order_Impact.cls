/**
* Test class for Change Order Impact and its validation rules. 
*/
@isTest
public class SQX_Test_2449_Change_Order_Impact{

    @testSetup
    public static void commonSetup() {
        
        //add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
        
        //Create Change Order
        System.runAs(adminUser){
            SQX_Test_Change_Order chgOrder = new SQX_Test_Change_Order();
            chgOrder.changeOrder.Title__c = 'Change_Order_Test_For_Impact';
            chgOrder.save();
        }
    }

    /**
    * Given: Creat an Impact Of Change Order with Impact Type as Job Function.
    * When: Imact is inserted
    * Then: Insertion is Successfull
    *
    * @date:2016/8/1
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2449]
    */
    public testmethod static void givenImpactWithImpactTypeJobFunction_WhenImpactIsInserted_InsertionIsSuccessful(){

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create Impact of Change Order
            SQX_Change_Order__c chgOrder = [SELECT Id FROM SQX_Change_Order__c WHERE  Title__c = 'Change_Order_Test_For_Impact' LIMIT 1];

            //Act: Create impact without providing required field SQX_Job_Function__c
            SQX_Impact__c impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION),
                                    Description_of_Impact__c = 'Impact Job funtion Test'
                                    );
            List<SQX_Impact__c> impactList = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result = new SQX_DB().continueOnError().op_insert(impactList, new List<Schema.SObjectField>{});
            
            //Assert: Impact should not be inserted.
            System.assert(!result[0].isSuccess(),'Expected impact not to be saved as job function is required field but is saved');
            
            //Act: Create impact with providing required field SQX_Job_Function__c
            SQX_Job_Function__c jobFunction = new SQX_Job_Function__c( Name = 'job function' );
            List<SQX_Job_Function__c> jobFunctionList = new List<SQX_Job_Function__c>{jobFunction};
            List<DataBase.SaveResult> jobFuntionResult = new SQX_DB().continueOnError().op_insert(jobFunctionList,new List<Schema.SObjectField>{});
            
            //Assert : Job funtion is inserted successfully
            System.assert(jobFuntionResult[0].isSuccess(),'Expected job funtion to be inserted');
            impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION),
                                    Description_of_Impact__c = 'Impact Job funtion Test',
                                    SQX_Job_Function__c = jobFunction.Id
                                    );
            List<SQX_Impact__c> impactList1 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result1 = new SQX_DB().continueOnError().op_insert(impactList1, new List<Schema.SObjectField>{});

            //Assert: Insertion is successfull
            System.assert(result1[0].isSuccess(),'Expected impact to be inserted');

            // Act : Void the change order and change the record
            chgOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            chgOrder.Resolution_Code__c='Duplicate';
            List<Database.SaveResult> voidResults = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{chgOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            //Assert:void record successfully
            System.assertEquals(true, voidResults[0].isSuccess(),'Record void Successfully>>' + voidResults[0].getErrors());
            //Arrange: add impact in void change order
            impact = new SQX_Impact__c(
                        SQX_Change_Order__c = chgOrder.Id,
                        RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_JOB_FUNCTION),
                        Description_of_Impact__c = 'Impact Job funtion Test',
                        SQX_Job_Function__c = jobFunction.Id
            );
            //Act: try to add the impact 
            List<SQX_Impact__c> impactList2 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result2 = new SQX_DB().continueOnError().op_insert(impactList2, new List<Schema.SObjectField>{});
            //Assert:Validation error message should display : Parent record is locked.
            System.assert(SQX_Utilities.checkErrorMessage(result2[0].getErrors(), 'Parent record is locked.'), 'Parent record is locked. when user try to add impact  in void change order');
        }
    }

    /**
    * Given: Creat an Impact Of Change Order with Impact Type as Metric.
    * When: Imact is inserted
    * Then: Insertion is Successfull
    *
    * @date:2016/8/1
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2449]
    */
    public testmethod static void givenImpactWithImpactTypeMetric_WhenImpactIsInserted_InsertionIsSuccessful(){

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create Impact of Change Order
            SQX_Change_Order__c chgOrder = [SELECT Id FROM SQX_Change_Order__c WHERE  Title__c = 'Change_Order_Test_For_Impact' LIMIT 1];

            //Act: Create impact without providing required field Metric_Name__c
            SQX_Impact__c impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_METRIC),
                                    Description_of_Impact__c = 'Impact Metric Test'
                                    );
            List<SQX_Impact__c> impactList = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result = new SQX_DB().continueOnError().op_insert(impactList, new List<Schema.SObjectField>{});
    
            //Assert: Impact should not be inserted.
            System.assert(!result[0].isSuccess(),'Expected impact not to be saved as job function is required field but is saved');
            
            //Act: Create impact with providing required field Metric_Name__c
            impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_METRIC),
                                    Description_of_Impact__c = 'Impact Metric Test',
                                    Metric_Name__c = 'Metric 1'
                                    );
            List<SQX_Impact__c> impactList1 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result1 = new SQX_DB().continueOnError().op_insert(impactList1, new List<Schema.SObjectField>{});

            //Assert: Insertion is successfull
            System.assert(result1[0].isSuccess(),'Expected impact to be inserted');
        }
    }

    /**
    * Given: Creat an Impact Of Change Order with Impact Type as Internal.
    * When: Imact is inserted
    * Then: Insertion is Successfull
    *
    * @date:2016/8/1
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2449]
    */
    public testmethod static void givenImpactWithImpactTypeInternal_WhenImpactIsInserted_InsertionIsSuccessful(){

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create Impact of Change Order
            SQX_Change_Order__c chgOrder = [SELECT Id FROM SQX_Change_Order__c WHERE  Title__c = 'Change_Order_Test_For_Impact' LIMIT 1];

            //Act: Create impact without providing required field SQX_Division__c
            SQX_Impact__c impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_INTERNAL),
                                    Description_of_Impact__c = 'Internal Impact Test'
                                    );
            List<SQX_Impact__c> impactList = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result = new SQX_DB().continueOnError().op_insert(impactList, new List<Schema.SObjectField>{});
            
            //Assert: Impact should not be inserted.
            System.assert(!result[0].isSuccess(),'Expected impact not to be saved as division, Business Unit are  required field but is saved');
            
            //Act: Create impact with providing required field SQX_Division__c
            SQX_Division__c division = new SQX_Division__c(Name = 'Division Test');
            List<SQX_Division__c> divisonList = new List<SQX_Division__c>{division};
            List<DataBase.SaveResult> divisonResult = new SQX_DB().continueOnError().op_insert(divisonList,new List<Schema.SObjectField>{});
            
            //Assert : Divison is inserted successfully
            System.assert(divisonResult[0].isSuccess(),'Expected divison to be inserted');
            impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_INTERNAL),
                                    Description_of_Impact__c = 'Internal Impact Test',
                                    SQX_Division__c = division.Id
                                    );
            List<SQX_Impact__c> impactList1 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result1 = new SQX_DB().continueOnError().op_insert(impactList1,new List<Schema.SObjectField>{});

            //Assert: Insertion is successfull
            System.assert(result1[0].isSuccess(),'Expected impact to be inserted');
        }
    }

    /**
    * Given: Creat an Impact Of Change Order with Impact Type as External.
    * When: Imact is inserted
    * Then: Insertion is Successfull
    *
    * @date:2016/8/1
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2449]
    */
    public testmethod static void givenImpactWithImpactTypeExternal_WhenImpactIsInserted_InsertionIsSuccessful(){

        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){

            //Arrange: Create Impact of Change Order
            SQX_Change_Order__c chgOrder = [SELECT Id FROM SQX_Change_Order__c WHERE  Title__c = 'Change_Order_Test_For_Impact' LIMIT 1];

            //Act: Create impact without providing required field External_Identifier__c
            SQX_Impact__c impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_EXTERNAL),
                                    Description_of_Impact__c = 'Impact External Test'
                                    );
            List<SQX_Impact__c> impactList = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result = new SQX_DB().continueOnError().op_insert(impactList, new List<Schema.SObjectField>{});
            
            //Assert: Impact should not be inserted.
            System.assert(!result[0].isSuccess(),'Expected impact not to be saved as external idenfier is required field but is saved');
            
            //Act: Create impact with providing required field External_Identifier__c
            impact = new SQX_Impact__c(
                                    SQX_Change_Order__c = chgOrder.Id,
                                    RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_EXTERNAL),
                                    Description_of_Impact__c = 'Impact External Test',
                                    External_Identifier__c = 'External Idenfier Test'
                                    );
            List<SQX_Impact__c> impactList1 = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result1 = new SQX_DB().continueOnError().op_insert(impactList1, new List<Schema.SObjectField>{});

            //Assert: Insertion is successfull
            System.assert(result1[0].isSuccess(),'Expected impact to be inserted');
        }
    }
}