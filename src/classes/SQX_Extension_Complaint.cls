/**
* @author Anish Shrestha
* @date 2014/12/19
* @description this class is the extension controller for Complaint
*/
public with sharing class SQX_Extension_Complaint extends SQX_Extension_UI {
    
    public static final String TEMPSTORE_TYPE_NAME = 'Extension Complaint Response';

    /*
    * Add default policies
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
        defaultEsigPolicies.put('reopen', this.POLICY_COMMENT_REQUIRED);
        return defaultEsigPolicies;
    }
    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/3
    * @description This is the main entry for the constructor, it copies the sent controller to mainController for further reference
    */
    public SQX_Extension_Complaint (ApexPages.StandardController controller) {
        super(controller, new List<String> {'Status__c'});

        SQX_Complaint__c complaint = (SQX_Complaint__c)controller.getRecord();

        // Since the approval and rejection of the closure review is driven by closure approval esig policy 
        // if esig policy of the closure approval is defined it should take the policy of closure approval 
        // else it should take the default esig policy
        Map<String,SQX_CQ_Esig_Policies__c> allPolicies = getEsigPolicy();
        SQX_CQ_Esig_Policies__c approveRejectPolicy = allPolicies.get('approverejectrequest');
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Boolean closureApproval, closureComment = false;
        if(approveRejectPolicy != null){
            closureApproval = approveRejectPolicy.Required__c;
            closureComment = approveRejectPolicy.Comment__c;
            }
        else{
            closureApproval = instance.Enabled__c;
            closureComment = false;
        }
        //we don't need Complaint esig validation if Complaint is new or is in draft state.
        Boolean isRecInDraftOrNew = !(complaint.Id != null && complaint.Status__c != SQX_Complaint.STATUS_DRAFT);
        addStaticPolicy('save', getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.Complaint));
        addStaticPolicy('approveapproval', new SQX_CQ_Esig_Policies__c(Required__c  = closureApproval, Comment__c =  closureComment));
        addStaticPolicy('rejectapproval', new SQX_CQ_Esig_Policies__c(Required__c  = closureApproval, Comment__c =  closureComment));

        this.purposeOfSigMap.putAll(new Map<String, String>{
                'save' => Label.SQX_PS_Complaint_Saving_the_record,
                'submit' => Label.SQX_PS_Complaint_Submitting_the_record,
                'close' => Label.SQX_PS_Complaint_Closing_the_record,
                'initiate' => Label.SQX_PS_Complaint_Initiating_the_record,
                'takeownershipandinitiate' => Label.SQX_PS_Complaint_Taking_Ownership_and_Initiating,
                'void' => Label.SQX_PS_Complaint_Voiding_the_record,
                'reopen' => Label.SQX_PS_Complaint_Reopening_the_record,
                'createcapa' => Label.SQX_PS_Complaint_Creating_CAPA,
                'approverejectrequest' => Label.SQX_PS_Complaint_Approving_Rejecting_Complaint_Closure,
                'approveapproval' => Label.SQX_PS_Complaint_Approving_Complaint_Closure,
                'rejectapproval' => Label.SQX_PS_Complaint_Rejecting_Complaint_Closure,
                'rundecisiontree' => Label.SQX_PS_Complaint_Running_Decision_Tree,
                'executescript' => Label.SQX_PS_Complaint_Executing_Script
            });
        this.addActionSaveAfterDraft();
    }

    public String getComplaintRecordTypeID(){
        return SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_COMPLAINT);
    }

    public String getCAPARecordTypeID(){
        return SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA);
    }
    
    public override String getChangeSetJSON(){
        SObject mainRecordObj =  mainController.getRecord();
        Id mainRecordObjId = (Id)mainRecordObj.get('Id');

        String changeSet = '[]';


        if(mainRecordObjId != null){
            changeSet = SQX_TempStore.getDeltaObject(mainRecordObjId);

            changeSet = changeSet == '' ? '[]' : changeSet;
        }


        return changeSet;//.escapeHtml4();
    }
    
    transient private String completeJSONData = null;

    /**
    * Returns the configuration that is useful for serializing data related to a Complaint, when
    * displaying.
    * @return returns the object serialization configuration
    */
    public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){

        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        return new SObjectDataLoader.SerializeConfig()
                                    .followChild(SQX_Investigation__c.SQX_Complaint__c)
                                    .followChild(SQX_Root_Cause__c.SQX_Investigation__c)
                                    .followChild(SQX_Root_Cause__c.SQX_Root_Cause_Code__c)
                                    .followChild(SQX_Investigation_Tool__c.SQX_Investigation__c)
                                    .followChild(SQX_Action_Plan__c.SQX_Investigation__c)
                                    .followChild(SQX_Complaint_Defect__c.SQX_Complaint__c)
                                    .followChild(SQX_Complaint_Record_Activity__c.SQX_Complaint__c)
                                    .followChild(SQX_Complaint_Contact__c.SQX_Complaint__c)
                                    .followChild(SQX_Product_History_Review__c.SQX_Complaint__c)
                                    .followChild(SQX_Related_Product__c.SQX_Product_History_Review__c)
                                    .followChild(SQX_Product_History_Answer__c.SQX_Product_History_Review__c)
                                    .followChild(SQX_Cross_Reference_Complaint__c.SQX_Complaint__c)
                                    .followChild(SQX_Cross_Reference_Complaint__c.SQX_Related_Complaint__c)
				                    .followChild(SQX_Complaint_Task__c.SQX_Complaint__c)

                                    .getFieldsFor(SQX_Part__c.getSObjectType(), new List<SObjectField> {SQX_Part__c.Part_Number_and_Name__c, SQX_Part__c.Name, SQX_Part__c.Part_Number__c})
                    
                                    //decision tree related
                                    .followChild(SQX_Decision_Tree__c.SQX_Complaint__c)
                                    .followChild(SQX_DT_Answer__c.SQX_Decision_Tree__c)
                                    .followChild(SQX_Regulatory_Report__c.SQX_Complaint__c)
                                    .followChild(SQX_Submission_History__c.SQX_Complaint__c)

                                    .followAttachmentOf(globalDescribe.get(SQX.Complaint))
                                    .followAttachmentOf(globalDescribe.get(SQX.Investigation))
                                    .followAttachmentOf(globalDescribe.get(SQX.ComplaintDefect))
                                    .followAttachmentOf(globalDescribe.get(SQX.ProductHistoryReview))
                                    .followAttachmentOf(SQX_Submission_History__c.getSObjectType())

                                    .followNoteOf(globalDescribe.get(SQX.Complaint))
                                    .followNoteOf(globalDescribe.get(SQX.Investigation));
    }
    
    
    /*
    * Overide this as appending '_Customer_Script_Layout' would take more than 40 chars which is not allowed by salesforce
    */
    public override pageReference getCustomerScriptPageLayout(){
        return new pagereference(getPageName('_Cu_Script_Layout', getCurrentPageName()));//pagename + layout
    }

    public static SQX_Upserter.SQX_Upserter_Config getUpserterConfig(){
        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config();
        config.omit(SQX.Finding, SQX_Finding__c.SQX_Complaint__c);
        config.omit(SQX.ProductHistoryReview, SQX_Product_History_Review__c.SQX_Complaint__c);
          
        return config;
    }

    
   /*******************************************REMOTE ACTION SECTION**********************************************************/


    @RemoteAction
    public static String processChangeSetWithAction(String complaintId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData) {
        Boolean isNewRecord = !SQX_Upserter.isValidId(complaintId);
        SQX_Complaint__c  complaint;
        SQX_Extension_Complaint complaintExtension;

        if(isNewRecord){
            complaint = new SQX_Complaint__c();
        }
        else{
            complaint = [SELECT Id,
                                Status__c,
                                SQX_Part__c,
                                Resolution__c,
                                SQX_Finding__c,
                                SQX_Department__c,
                                Closure_Review_By__c,
                                Require_Closure_Review__c
                        FROM SQX_Complaint__c WHERE Id = : complaintId];
        }

        complaintExtension = new SQX_Extension_Complaint(new ApexPages.StandardController(complaint));
        SQX_Upserter.SQX_Upserter_Config config = getUpserterConfig();
        config.setUpsertOrder(new List<SObjectType>{ SQX_Product_History_Answer__c.getSObjectType(), SQX_Product_History_Review__c.getSObjectType() });
        return complaintExtension.executeTransaction(complaintId, changeset, objectsToDelete, params, esigData, config, null);
    }

    /**
    * This function processes next action function for page
    */
    public override void processNextAction(Map<String, String> params){
        SQX_Complaint__c complaint = [SELECT Id,
                                Status__c,
                                SQX_Part__c,
                                Age__c,
                                Resolution__c,
                                SQX_Finding__c,
                                SQX_Department__c,
                                Closure_Review_By__c,
                                Require_Closure_Review__c
                        FROM SQX_Complaint__c WHERE Id = : mainRecord.Id];

        if('Submit'.equalsIgnoreCase(nextAction)){
            SQX_Complaint.submitComplaints(new List<SQX_Complaint__c> {complaint}, false);
        }

        else if('initiate'.equalsIgnoreCase(nextAction)){
            SQX_Complaint.initiateComplaints(new List<SQX_Complaint__c> {complaint}, false);
        }

        else if('takeownershipandinitiate'.equalsIgnoreCase(nextAction)){
            SQX_Complaint.takeOwnershipAndInitiateComplaints(new List<SQX_Complaint__c> {complaint}, false);
        }

        else if('void'.equalsIgnoreCase(nextAction) || 'close'.equalsIgnoreCase(nextAction)){
            SQX_Complaint.voidOrCloseComplaints(new List<SQX_Complaint__c> {complaint}, 'void'.equalsIgnoreCase(nextAction), 'true'.equalsIgnoreCase(params.get('SendForApproval')), false);
        }

        else if('reopen'.equalsIgnoreCase(nextAction)){
            SQX_Complaint.reopenComplaints(new List<SQX_Complaint__c> {complaint}, false);
        }

        else if('approveapproval'.equalsIgnoreCase(nextAction) || 'rejectapproval'.equalsIgnoreCase(nextAction)){
            Id recordId = complaint.Id;
            Id workItemId = params.get('workItemId');
            String action = 'approveapproval'.equalsIgnoreCase(nextAction)?SQX_Approval_Util.APPROVE_KEY : SQX_Approval_Util.REJECT_KEY;
            
            processWorkItems(action, recordId, (String)params.get('remark'), workItemId); 
        }
    }

}