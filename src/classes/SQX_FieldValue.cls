global class SQX_FieldValue {
       global string FieldName;
       global object Value;
    
       //use when mapping direct CQ fields
        global SQX_FieldValue(string fieldNameToUse){
           FieldName = SQX.NSPrefix+ fieldNameToUse;
           Value = null;
       }
       //use when sending value
       global SQX_FieldValue(object val){
           FieldName = null;
           Value = val;
       }
        //use to skip NS
       global SQX_FieldValue(string fieldNameToUse,boolean skipNS){
           FieldName = (skipNS ? fieldNameToUse : SQX.NSPrefix+fieldNameToUse);
           Value = null;
       }
}