/**
* Batch processor for migrating all Users to Personnel,
* User Job Function records related to migrating users to Personnel Job Function,
* and Document Training records related migrating users to Personnel Document Training
* Migration user may or may not have the permission on all records while migrating records.
*/
global with sharing class SQX_DocumentTrainingMigration_Processor implements Database.Batchable<sObject> {
    
    public static Set<Id> LimitMigrationToUserIds;
    
    global SQX_DocumentTrainingMigration_Processor() { }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, FirstName, LastName, UserName, Email, ManagerId, IsActive'
            + ' FROM User WHERE Id NOT IN (SELECT SQX_User__c FROM SQX_Personnel__c)';
        if (LimitMigrationToUserIds != null && LimitMigrationToUserIds.size() > 0) {
            query += 'AND Id IN :LimitMigrationToUserIds';
        }
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> objList) {
        System.debug(String.format('SQX_DocumentTrainingMigration_Processor.execute() starting limit, cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                    new String[]{ 
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
        
        if (objList.size() > 0) {
            /***** START: User to Personnel Migration *****/
            List<SQX_Personnel__c> newPsnList = new List<SQX_Personnel__c>();
            Map<Id, SQX_Personnel__c> userPsnMap = new Map<Id, SQX_Personnel__c>();
            SQX_Personnel__c psn;
            
            for (User u : (List<User>)objList) {
                // set user, full name, identification number, email address, and active for new personnel using user
                psn = SQX_Personnel.copyFieldsFromUser(u, new SQX_Personnel__c());
                psn.Active__c = true; // setting personnel record as active even if user is inactive
                if (u.ManagerId != null) {
                    psn.OwnerId = u.ManagerId;
                }
                
                // add psn to insert list
                newPsnList.add(psn);
                
                // map User with Personnel
                userPsnMap.put(psn.SQX_User__c, psn);
            }
            
            /**
            * WITHOUT SHARING has been used 
            * ---------------------------------
            * Without sharing has been used because the running user might not have access to insert migrated personnel records
            * This will cause and error.
            */
            new SQX_DB().withoutSharing().op_insert(newPsnList, new List<SObjectField>{
                    SQX_Personnel__c.fields.Active__c,
                    SQX_Personnel__c.fields.OwnerId
            });
            /***** END: User to Personnel Migration *****/
            
            
            /***** START: Document Training to Personnel Document Training Migration *****/
            // get user's document training list
            List<SQX_Document_Training__c> dtList = [SELECT Id, Name, Completion_Date_Internal__c, Due_Date__c, Level_of_Competency__c,
                                                            Optional__c, SQX_Controlled_Document__c, SQX_Trainer__c, SQX_User__c,
                                                            Status__c, SQX_Training_Approved_By__c, SQX_User_Signed_Off_By__c, Title__c,
                                                            Trainer_SignOff_Comment__c, Trainer_SignOff_Date__c, Trainer_Signature__c,
                                                            Uniqueness_Constraint__c, User_SignOff_Date__c, User_Signature__c,
                                                            User_Signoff_Comment__c, SQX_Controlled_Document__r.Document_Number__c,
                                                            SQX_Controlled_Document__r.Revision__c, SQX_Controlled_Document__r.Document_Status__c
                                                     FROM SQX_Document_Training__c
                                                     WHERE SQX_User__c IN :userPsnMap.keySet()];
            if (dtList.size() > 0) {
                Map<Id, SQX_Personnel_Document_Training__c> newPdtMap = new Map<Id, SQX_Personnel_Document_Training__c>();
                SQX_Personnel_Document_Training__c pdt;
                
                for (SQX_Document_Training__c dt : dtList) {
                    // do not migrate pending document training records of not current controlled document
                    if (dt.Status__c == SQX_Document_Training.STATUS_PENDING
                        && dt.SQX_Controlled_Document__r.Document_Status__c != SQX_Controlled_Document.STATUS_CURRENT)
                        continue;
                    
                    pdt = new SQX_Personnel_Document_Training__c();
                    
                    // set new personnel document training fields using existing document training
                    pdt.Document_Number__c = dt.SQX_Controlled_Document__r.Document_Number__c;
                    pdt.Document_Revision__c = dt.SQX_Controlled_Document__r.Revision__c;
                    pdt.Document_Title__c = dt.Title__c;
                    pdt.Due_Date__c = dt.Due_Date__c;
                    pdt.Level_of_Competency__c = dt.Level_of_Competency__c;
                    pdt.Optional__c = dt.Optional__c;
                    pdt.Personnel_Email__c = userPsnMap.get(dt.SQX_User__c).Email_Address__c;
                    pdt.SQX_Controlled_Document__c = dt.SQX_Controlled_Document__c;
                    pdt.SQX_Personnel__c = userPsnMap.get(dt.SQX_User__c).Id;
                    pdt.SQX_Trainer__c = dt.SQX_Trainer__c;
                    pdt.SQX_Training_Approved_By__c = dt.SQX_Training_Approved_By__c;
                    pdt.SQX_User_Signed_Off_By__c = dt.SQX_User_Signed_Off_By__c;
                    pdt.Status__c = dt.Status__c;
                    pdt.Trainer_SignOff_Comment__c = dt.Trainer_SignOff_Comment__c;
                    pdt.Trainer_SignOff_Date__c = dt.Trainer_SignOff_Date__c;
                    pdt.Trainer_Signature__c = dt.Trainer_Signature__c;
                    pdt.User_SignOff_Date__c = dt.User_SignOff_Date__c;
                    pdt.User_Signature__c = dt.User_Signature__c;
                    pdt.User_Signoff_Comment__c = dt.User_Signoff_Comment__c;
                    if (pdt.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE) {
                        pdt.Completion_Date__c = dt.Completion_Date_Internal__c;
                    }
                    if (pdt.Status__c != SQX_Personnel_Document_Training.STATUS_PENDING) {
                        pdt.Uniqueness_Constraint__c = '' + pdt.SQX_Personnel__c + pdt.SQX_Controlled_Document__c + dt.Id;
                    }
                    pdt.IsMigrated__c = true;
                    
                    // add pdt to insert list
                    newPdtMap.put(dt.Id, pdt);
                }
                
                if (newPdtMap.size() > 0) {

                    /**
                    * WITHOUT SHARING has been used 
                    * ---------------------------------
                    * Without sharing has been used because the running user might not have access to insert migrated personnel document training records
                    * This will cause and error.
                    */
                    new SQX_DB().withoutSharing().op_insert(newPdtMap.values(), new List<SObjectField>{
                    SQX_Personnel_Document_Training__c.fields.Document_Number__c,
                    SQX_Personnel_Document_Training__c.fields.Document_Revision__c,
                    SQX_Personnel_Document_Training__c.fields.Document_Title__c,
                    SQX_Personnel_Document_Training__c.fields.Due_Date__c,
                    SQX_Personnel_Document_Training__c.fields.Level_of_Competency__c,
                    SQX_Personnel_Document_Training__c.fields.Optional__c,
                    SQX_Personnel_Document_Training__c.fields.Personnel_Email__c,
                    SQX_Personnel_Document_Training__c.fields.SQX_Controlled_Document__c,
                    SQX_Personnel_Document_Training__c.fields.SQX_Personnel__c,
                    SQX_Personnel_Document_Training__c.fields.SQX_Trainer__c,
                    SQX_Personnel_Document_Training__c.fields.SQX_Training_Approved_By__c,
                    SQX_Personnel_Document_Training__c.fields.SQX_User_Signed_Off_By__c,
                    SQX_Personnel_Document_Training__c.fields.Status__c,
                    SQX_Personnel_Document_Training__c.fields.Trainer_SignOff_Comment__c,
                    SQX_Personnel_Document_Training__c.fields.Trainer_SignOff_Date__c,
                    SQX_Personnel_Document_Training__c.fields.Trainer_Signature__c,
                    SQX_Personnel_Document_Training__c.fields.User_SignOff_Date__c,
                    SQX_Personnel_Document_Training__c.fields.User_Signature__c,
                    SQX_Personnel_Document_Training__c.fields.User_Signoff_Comment__c,
                    SQX_Personnel_Document_Training__c.fields.Completion_Date__c,
                    SQX_Personnel_Document_Training__c.fields.Uniqueness_Constraint__c,
                    SQX_Personnel_Document_Training__c.fields.IsMigrated__c
                    });
                    
                    // old document training update list
                    List<SQX_Document_Training__c> dtUpdateList = new List<SQX_Document_Training__c>();
                    
                    // link migrated personnel document training to old document training
                    for (SQX_Document_Training__c dt : dtList) {
                        if (newPdtMap.get(dt.Id) != null) {
                            // link personnel document training
                            dt.SQX_Personnel_Document_Training__c = newPdtMap.get(dt.Id).Id;
                            // add to update list
                            dtUpdateList.add(dt);
                        }
                    }
                    
                    /**
                    * WITHOUT SHARING has been used 
                    * ---------------------------------
                    * Without sharing has been used because the running user might not have access to update old document training record with linked personnel document training
                    * This will cause and error.
                    */
                    new SQX_DB().withoutSharing().op_update(dtList, new List<SObjectField>{});
                }
            }
            /***** END: Document Training to Personnel Document Training Migration *****/
            
            
            /***** START: User Job Function to Personnel Job Function Migration *****/
            List<SQX_User_Job_Function__c> ujfList = [SELECT Id, Name, SQX_User__c, SQX_Job_Function__c, Active__c,
                                                             Activation_Date__c, Deactivation_Date__c, Uniqueness_Constraint__c
                                                      FROM SQX_User_Job_Function__c
                                                      WHERE SQX_User__c IN :userPsnMap.keySet()];
            if (ujfList.size() > 0) {
                List<SQX_Personnel_Job_Function__c> newPjfList = new List<SQX_Personnel_Job_Function__c>();
                SQX_Personnel_Job_Function__c pjf;
                
                for (SQX_User_Job_Function__c ujf : ujfList) {
                    pjf = new SQX_Personnel_Job_Function__c();
                    
                    // set new personnel job function fields using existing user job function
                    pjf.SQX_Personnel__c = userPsnMap.get(ujf.SQX_User__c).Id;
                    pjf.SQX_Job_Function__c = ujf.SQX_Job_Function__c;
                    pjf.Active__c = ujf.Active__c;
                    pjf.Activation_Date__c = ujf.Activation_Date__c;
                    pjf.Deactivation_Date__c = ujf.Deactivation_Date__c;
                    if (pjf.Deactivation_Date__c != null) {
                        pjf.Unique_New_Or_Active_Constraint__c = '' + pjf.SQX_Personnel__c + pjf.SQX_Job_Function__c + ujf.Id;
                    }
                    
                    // add pjf to insert list
                    newPjfList.add(pjf);
                }
                
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the running user might not have access to insert migrated personnel job functions
                * This will cause and error.
                */
                new SQX_DB().withoutSharing().op_insert(newPjfList, new List<SObjectField>{
                    SQX_Personnel_Job_Function__c.fields.SQX_Personnel__c,
                    SQX_Personnel_Job_Function__c.fields.SQX_Job_Function__c,
                    SQX_Personnel_Job_Function__c.fields.Active__c,
                    SQX_Personnel_Job_Function__c.fields.Activation_Date__c,
                    SQX_Personnel_Job_Function__c.fields.Deactivation_Date__c,
                    SQX_Personnel_Job_Function__c.fields.Unique_New_Or_Active_Constraint__c
                });
                
                // link migrated personnel job function to old user job function
                for (integer i = 0; i < newPjfList.size(); i++) {
                    SQX_User_Job_Function__c ujf = (SQX_User_Job_Function__c)ujfList[i];
                    ujf.SQX_Personnel_Job_Function__c = newPjfList[i].Id;
                }
                
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the running user might not have access to update old user job function record with linked personnel job function
                * This will cause and error.
                */
                new SQX_DB().withoutSharing().op_update(ujfList, new List<SObjectField>{});
            }
            /***** END: User Job Function to Personnel Job Function Migration *****/
        }
        
        System.debug(String.format('SQX_DocumentTrainingMigration_Processor.execute() ending limit, cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                    new String[]{ 
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
    }
    
    global void finish(Database.BatchableContext bc) { }
}