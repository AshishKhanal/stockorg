/**
 *  Controller class for approval page (SQX_Approval_Document_Information) component
 */
 public with sharing class SQX_Extension_Approval_Doc_Information {
    
    /**
    * This method will approve pending approvals.
    */
    @AuraEnabled
    public static SQX_Action_Response approveRejectDocument(String documentId, String workItemId, String userName, String password, String approveRejectFlag,String comment) {
        SObjectType sobjType=Id.valueOf(documentId).getSObjectType();

        if(Schema.SQX_Controlled_Document__c.SObjectType == sobjType){
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c( Id = documentId);
            SQX_Extension_Controlled_Doc_Approval docApproval = new SQX_Extension_Controlled_Doc_Approval();
            docApproval.recordId = Id.valueOf(documentId);
            docApproval.SigningOffUsername = username;
            docApproval.SigningOffPassword = password;
            
            SQX_Action_Response  response = docApproval.initialize(doc, workItemId);
            if(response.hasMessages()) {
                return response;
            }
            
            return docApproval.approveRejectDocumentAPI(Boolean.valueOf(approveRejectFlag));  
        } else if(Schema.SQX_Change_Order__c.SObjectType == sobjType) {
            return SQX_Change_Order.approveRejectChangeOrder(documentId,workItemId,userName,password,comment,Boolean.valueOf(approveRejectFlag));
        } else if(Schema.SQX_Complaint__c.SObjectType == sobjType) {
            return SQX_Complaint.approveRejectComplaint(documentId,workItemId,userName,password,comment,Boolean.valueOf(approveRejectFlag));
        }
        
        return null;
    }
    /**
    * This method will return the approval process step name.
    */
    @AuraEnabled
    public static String getCurrentApprovalProcessStepName(String docId,String workItemId){
        List<ProcessInstanceNode> nodes = SQX_Approval_Util.getPendingProcessInstanceNode( Id.valueOf(docId) );
        string workId=workItemId!=null?workItemId:getCurrentWorkItemId(docId);
        return nodes.size() > 0 ? nodes[0].ProcessNodeName+'/'+workId: '';
    }
    /**
    * This method will return the workItem id if there is no origin work id is available.
    */
     public Static String getCurrentWorkItemId(String recordId){
         String workId=[SELECT Id, ProcessInstanceId,ProcessInstance.TargetObjectId, ProcessInstance.Status, CreatedDate, OriginalActorId, ActorId
                     FROM ProcessInstanceWorkitem
                     WHERE ProcessInstance.TargetObjectId =:recordId and ActorId=:UserInfo.getUserId() and ProcessInstance.Status='Pending'
                     ].Id;
         return workId;
     }

}