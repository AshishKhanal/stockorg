/**
* Task Template Test
*/
@IsTest
public class SQX_Test_Task_Template{
    
    /**
    * task creation test
    */
    public testmethod static void createTaskTests(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            capa.save();
                        
            Task capaOpenedTask = SQX_Task_Template.createTaskFor(capa.capa, UserInfo.getUserId(), null,
                     Date.Today(), SQX_Task_Template.TaskType.CAPAOpened);
                     
            Task capaClosedTask = SQX_Task_Template.createTaskFor(capa.capa, UserInfo.getUserId(), null,
                     Date.Today(), SQX_Task_Template.TaskType.CAPAClosureToDo);
                     
            insert new Task[]{ capaOpenedTask, capaClosedTask };
            
            System.assert(SQX_Task_Template.taskExistsFor(capa.capa, UserInfo.getUserId(), SQX_Task_Template.TaskType.CAPAOpened)
                        , 'Expected a finding open task but found none') ;
            
            System.assert(SQX_Task_Template.taskExistsFor(capa.capa, UserInfo.getUserId(), SQX_Task_Template.TaskType.CAPAClosureToDo)
                        , 'Expected a finding closure task but found none');
                        
        }
           
    }
    
    /**
    * effectiveness review task test
    */
    public testmethod static void createEffectivenessTaskTests(){
       UserRole role = SQX_Test_Account_Factory.createRole(); 
       User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
       System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();

            capa.finding.setRequired(false,false,false,false,false)
                                    .setStatus(SQX_Finding.STATUS_OPEN)
                                    .save();
                                    
            capa.CAPA.Status__c = SQX_CAPA.STATUS_COMPLETE;

            capa.save();
    
           SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
                                                                                 
           insert review;
           
           Task effectivenessTask = SQX_Task_Template.createTaskFor(review, UserInfo.getUserId(), null,
                     Date.Today(), SQX_Task_Template.TaskType.EffectivenessReviewToDo);
                     
           insert effectivenessTask;
           
           System.assert(SQX_Task_Template.taskExistsFor(capa.capa, UserInfo.getUserId(), SQX_Task_Template.TaskType.EffectivenessReviewToDo)
                        , 'Expected a effectiveness review task but found none');
       }
    }
    
}