/**
*   This class contains test cases for ensuring that NC source is providing homepage items with desired values
*   @author - Anuj Bhandari
*   @date - 12-04-2017
*/
@isTest
public class SQX_Test_Homepage_Source_NC {

    /*
        NC source currently returns items of type :
        1. Open/Draft/Completed/Triage NC Items
    */


    /**
    *   This method creates NC records of Open, Draft, Triage and complete statuses
    *   @author - Anuj Bhandari
    *   @date - 12-04-2017
    */
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        //User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser) {

            /* Creating a Draft NC */
            SQX_Test_NC itmDraft = new SQX_Test_NC();
            itmDraft.setStatus(SQX_NC.STATUS_DRAFT);
            itmDraft.nc.NC_Title__c = 'Test NC';
            itmDraft.save();

            /* Creating an open NC */
            SQX_Test_NC itmOpen = new SQX_Test_NC();
            itmOpen.setStatus(SQX_NC.STATUS_OPEN);
            itmOpen.save();

            /* Creating a triage NC */
            SQX_Test_NC itmTriage = new SQX_Test_NC();
            itmTriage.setStatus(SQX_NC.STATUS_TRIAGE);
            itmTriage.save();

            /* Creating a Completed NC */
            SQX_Test_NC itmCompleted = new SQX_Test_NC();
            itmCompleted.setStatus(SQX_NC.STATUS_COMPLETE);
            itmCompleted.save();
        }
    }


    /**
     *   Given : A draft NC item also a homepage component expecting records from NC Source
     *   When :  NC Record is processed for Homepage items
     *   Then :  Draft NC type item is added to the homepage with desired values
     *   @story -3160
     */
    testmethod static void givenFourNCItemsWithDifferentStatus_WhenNCRecordIsFetchedAndProcessed_ThenCorrepondingNCItemsShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange1: Fetch available NCs - Consist of Open, Draft, Complete and Triage Items
            List <SQX_Nonconformance__c> itms = [SELECT Id,
                                                        Name,
                                                        NC_Title__c,
                                                        Status__c,
                                                        CreatedDate
                                                        FROM SQX_Nonconformance__c
                                                    ];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_NC_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Draft NC has been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size()>= 4, 'Expected at-least four items');

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_NC, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                for (SQX_Nonconformance__c itm: itms) {
                    if (itm.id == hpItem.itemId) {
                        if (itm.Status__c == SQX_NC.STATUS_DRAFT) {

                            validateDraftItem(itm, hpItem);
                            break;

                        } else if (itm.Status__c == SQX_NC.STATUS_OPEN) {

                            validateOpenItem(itm, hpItem);
                            break;

                        } else if (itm.Status__c == SQX_NC.STATUS_TRIAGE) {

                            validateTriageItem(itm, hpItem);
                            break;

                        } else if (itm.Status__c == SQX_NC.STATUS_COMPLETE) {

                            validateCompletedItem(itm, hpItem);
                            break;

                        } else {

                            System.assert(false, 'Unknown action item : ' + hpItem);

                        }
                    }
                }
            }

        }

    }


    /**
     *   Method validates the homepage item of type NC-Draft
     */
    private static void validateDraftItem(SQX_Nonconformance__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);

        System.assertNotEquals(hpItem.actions, null);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

        PageReference pr = new PageReference('/' + itm.Id);
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, new String[] {
            itm.Name, String.isBlank(itm.NC_Title__c) ? '' : itm.NC_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }


    /**
     *   Method validates the homepage item of type NC-Open
     */
    private static void validateOpenItem(SQX_Nonconformance__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS, hpItem.actionType);

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

        PageReference pr = new PageReference('/' + itm.Id);
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, new String[] {
            itm.Name, String.isBlank(itm.NC_Title__c) ? '' : itm.NC_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item of type NC-Triage
     */
    private static void validateTriageItem(SQX_Nonconformance__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_INITIATE, act.name);

        PageReference pr = new PageReference('/' + itm.Id);
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_INITIATE, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, new String[] {
            itm.Name, String.isBlank(itm.NC_Title__c) ? '' : itm.NC_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

        System.assertEquals(false, hpItem.supportsBulk);
    }

    /**
     *   Method validates the homepage item of type NC-Completed
     */
    private static void validateCompletedItem(SQX_Nonconformance__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE, hpItem.actionType);

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_CLOSE, act.name);

        PageReference pr = new PageReference('/' + itm.Id);
        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_CLOSE, act.actionIcon);

        System.assertEquals(false, act.supportsBulk);

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, new String[] {
            itm.Name, String.isBlank(itm.NC_Title__c) ? '' : itm.NC_Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }

    /**
    *   Given : A set of Nc investigation Record 
    *   When : Homepage items are fetched
    *   Then : Desired Investigation items are returned
    */
    @isTest	
    public static void givenNcRecordswithInvestigation_WhenHomepageItemsAreFetched_ThenDesiredNcItemsAreReturned(){
        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            List<SQX_NonConformance__c> nc=[select id,Name,
                                                        NC_Title__c,
                                                        SQX_Investigation_Approver__c,
                                                         Status__c from SQX_NonConformance__c where Status__c=:SQX_NC.STATUS_OPEN] ;
           
            /*added a investigation  for created capa */
            SQX_Investigation__c investigation=new SQX_Investigation__c
                                                (
                                                    SQX_NonConformance__c= nc[0].Id,
                                                    Investigation_Summary__c='Nc investigation',
                                                    Status__c=SQX_Investigation.STATUS_IN_APPROVAL,
                                                    Approval_Status__c=SQX_Investigation.APPROVAL_STATUS_PENDING
                                                );
            
            insert investigation; 

            // Arrange: add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { new SQX_Homepage_Source_NC_Items() };
                
             // Act: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));
            
            List<Object> hpItems = (List<Object>) homepageItems.get('items');
           
            List<Object> errors = (List<Object>) homepageItems.get('errors');
            
            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());
            // retrieve Nc records
            
            SQX_Investigation__c NcInvestigationRecords = [SELECT Id,Name,SQX_NonConformance__r.Title__c,SQX_NonConformance__c,Approval_Status__c,SQX_NonConformance__r.Name,
                                                            Status__c,CreatedDate from SQX_Investigation__c where SQX_Investigation__c.id=:investigation.id ][0];

            // Assert: Items retrieved are as expected
            System.assertNotEquals(null, hpItems);
            System.assert(hpItems.size() == 4, 'Expected exactly 4 items but received ' + hpItems.size() + ' items');
                 
            for(Object hpObj : hpItems) {
                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);
                if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE){
                        validateNcInvestigationItem(NcInvestigationRecords,hpItem);
                }
            }
        }
    }

    /**
    *   Method to verify if the given homepage item is a valid Nc investigation item 
    */
    private static void validateNcInvestigationItem(SQX_Investigation__c inves, SQX_Homepage_Item hpItem){

        System.assertEquals(0, hpItem.overdueDays);

        System.assertEquals(1, hpItem.actions.size());

        SQX_Homepage_Item_Action act = hpItem.actions.get(0);

        System.assertEquals(SQX_Homepage_Constants.ACTION_APPROVE_REJECT, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_NC'));
        pr.getParameters().put('Id',inves.SQX_NonConformance__c);
        pr.getParameters().put('initialTab','responseHistoryTab');

        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT, act.actionIcon);
        
        System.assertEquals(false, act.supportsBulk);

        // asserting feed text
        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE, new String[] { inves.Name,String.isBlank(inves.SQX_NonConformance__r.Title__c) ? '' :  inves.SQX_NonConformance__r.Title__c });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }
}