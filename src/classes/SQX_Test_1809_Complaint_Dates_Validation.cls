/**
 * test class to ensure that the date fields in Complaint (Occurrence Date, Report Date and Aware Date) are validated correctly
 * @author: Sagar Shrestha
 * @date: 2015-12-25
 * @story: [SQX-1809] 
 */
@isTest
public class SQX_Test_1809_Complaint_Dates_Validation{ 
    static boolean runAllTests = true,
                   givenComplaint_OccurrenceDateIsValidated = false,
                   givenComplaint_ReportDateIsValidated= false,
                   givenComplaint_AwareDateIsValidated= false;

    /**
     * Scenario 1
     * Action: Occurrence date is Set to future date
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Occurrence date is Set to past or today's date
     * Expected: Complaint Save should be successful 
     * @author: Sagar Shrestha
     * @date: 2015-12-25
     */
    public testmethod static void givenComplaint_OccurrenceDateIsValidated(){
        if(!runAllTests && !givenComplaint_OccurrenceDateIsValidated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set occurrence date as future date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(1);
            Database.SaveResult futureOccurrenceDateUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for future Occurrence Date
            System.assertEquals(false, futureOccurrenceDateUpdate.isSuccess(), 'Expected save to be unsuccessful for future Occurrence Date');

            //Act2 : Set occurrence date as today's date
            complaint.complaint.Occurrence_Date__c = Date.Today();
            Database.SaveResult todayOccurrenceDateUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be successful for today's Occurrence Date
            System.assertEquals(true, todayOccurrenceDateUpdate.isSuccess(), 'Expected save to be successful for today\'s Occurrence Date');

            //Act3 : Set occurrence date as past date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(-1);
            Database.SaveResult pastOccurrenceDateUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for past Occurrence Date
            System.assertEquals(true, pastOccurrenceDateUpdate.isSuccess(), 'Expected save to be successful for past Occurrence Date');

        }
    }


    /**
     * Scenario 1
     * Action: Report date is Set to future date
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Report Date is set less than occurrence date
     * Expected: Complaint Save should be unsuccessful
     *
     * Scenario 3
     * Action: Report Date is set later than occurrence date
     * Expected: Complaint Save should be successful 
     * @author: Sagar Shrestha
     * @date: 2015-12-25
     */
    public testmethod static void givenComplaint_ReportDateIsValidated(){
        if(!runAllTests && !givenComplaint_ReportDateIsValidated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set Report date as future date
            complaint.complaint.Reported_Date__c = Date.Today().addDays(1);
            Database.SaveResult futureReportDateUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for future Occurrence Date
            System.assertEquals(false, futureReportDateUpdate.isSuccess(), 'Expected save to be unsuccessful for future Report Date');

            //Act2 : Set report date less than occurrence date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(-2);
            complaint.complaint.Reported_Date__c = Date.Today().addDays(-3);
            Database.SaveResult reportDateLessThanOccurrenceDateUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be unsuccessful for report date less than occurrence date
            System.assertEquals(false, reportDateLessThanOccurrenceDateUpdate.isSuccess(), 'Expected save to be unsuccessful for report date less than occurrence date');

            //Act3 : Set report date later than occurrence date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(-3);
            complaint.complaint.Reported_Date__c = Date.Today().addDays(-2);
            complaint.complaint.Aware_Date__c = Date.Today().addDays(-2);
            Database.SaveResult reportDateLaterThanOccurrenceDateUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for Report Date later than Occurrence Date
            System.assertEquals(true, reportDateLaterThanOccurrenceDateUpdate.isSuccess(), 'Expected save to be successful for Report Date later than Occurrence Date');

        }
    }


    /**
     * Scenario 1
     * Action: Aware date is Set to future date
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Aware Date is set later than occurrence date and later than reported date
     * Expected: Complaint Save should be unsuccessful
     *
     * Scenario 3
     * Action: Aware Date is set later than occurrence date less than reported date
     * Expected: Complaint Save should be successful
     * @author: Sagar Shrestha
     * @date: 2015-12-25
     */
    public testmethod static void givenComplaint_AwareDateIsValidated(){
        if(!runAllTests && !givenComplaint_AwareDateIsValidated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set Aware date as future date
            complaint.complaint.Aware_Date__c = Date.Today().addDays(1);
            Database.SaveResult futureAwareDateUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for future Occurrence Date
            System.assertEquals(false, futureAwareDateUpdate.isSuccess(), 'Expected save to be unsuccessful for future Report Date');

            //Act2 : Set Aware date later than occurrence date and reported date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(-5);
            complaint.complaint.Reported_Date__c = Date.Today().addDays(-3);
            complaint.complaint.Aware_Date__c = Date.Today().addDays(-1);
            Database.SaveResult awareDateLaterThanOccurrenceDateAndReportDateUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be unsuccessful for report date less than occurrence date
            System.assertEquals(false, awareDateLaterThanOccurrenceDateAndReportDateUpdate.isSuccess(), 'Expected save to be unsuccessful for report date less than occurrence date');

            //Act3 : Set  Aware Date is set later than occurrence date less than reported date
            complaint.complaint.Occurrence_Date__c = Date.Today().addDays(-5);
            complaint.complaint.Reported_Date__c = Date.Today().addDays(-3);
            complaint.complaint.Aware_Date__c = Date.Today().addDays(-4);
            Database.SaveResult reportDateLaterThanOccurrenceDateLessThanReportDateUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for Aware Date is set later than occurrence date less than reported date
            System.assertEquals(true, reportDateLaterThanOccurrenceDateLessThanReportDateUpdate.isSuccess(), 'Expected save to be successful for Aware Date is set later than occurrence date less than reported date');

        }
    }


}