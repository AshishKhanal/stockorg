/**
* Returns the schema for the current user based on the users
*/
global with sharing class SQX_Schema_Selector {

    /**
    * used by testmethod to provide alternate schema.
    */
    public String customizedSchemaName {get; set;}

    /**
    * used by testmethod to provide alternate language prefix
    */
    public String languagePrefix {get; set;}

    /**
     * static string for param passed in url of page
     */  
    public static final String PARAM_LANGUAGE = 'language',
                               PARAM_RESOURCE_NAME = 'resource',
                               CQ_RESOURCE_CACHE = 'CQ_Resource_Cache';

    /**
     * UI Mode represents whether we are returning kendo ui resource or capa resource.
     */
    private Boolean uimode = false;

    global SQX_Schema_Selector(){
        languagePrefix = ApexPages.currentPage().getParameters().get(PARAM_LANGUAGE);
        customizedSchemaName = 'CQSchema';
        User currentUser = SQX_Utilities.getCurrentUser();
        languagePrefix = String.isBlank(languagePrefix) ? '_' + currentUser.LanguageLocaleKey : '_' + languagePrefix.toLowerCase();
        String resourceType =  ApexPages.currentPage().getParameters().get(PARAM_RESOURCE_NAME);
        uimode = resourceType == 'kendoui';
    }
    
    /**
    * Returns the schema name that is to be used based on user's language and available schema.
    */
    global String getSchemaName(){
        if(uimode == true) {
            return getKendoSchema();
        }

        return getCAPASChema();
    }

    /**
     * Returns CQ_Resource_Cache schema. Non namespace resources have higher priority over named ones.
     */
    private String getKendoSchema() {
        List<StaticResource> resources = [SELECT Id, Name, NamespacePrefix FROM StaticResource WHERE Name = :CQ_RESOURCE_CACHE];
        StaticResource selectedResource = resources.get(0);
        for(StaticResource resource : resources) {
            if(resource.NamespacePrefix == null) {
                selectedResource = resource;
                break;
            }
        }

        return (String.isBlank(selectedResource.NamespacePrefix) ? '' : (selectedResource.NamespacePrefix + '__')) + selectedResource.Name;
    }

    /**
     * returns the capa schema url based on priority
     */
    private String getCAPASChema() {
        final String CQDefaultSchemaName = 'CAPASchema',
                     CQCustomizedSchemaName = customizedSchemaName;


        final Map<String, Integer> schemaNames = new Map<String, Integer> { 
            CQDefaultSchemaName => 1,
            CQCustomizedSchemaName => 2,
            (CQDefaultSchemaName + languagePrefix) => 3,
            (CQCustomizedSchemaName + languagePrefix) => 4
        };

        List<StaticResource> resources = [SELECT Id, Name, NamespacePrefix FROM StaticResource WHERE Name In :schemaNames.keySet() ];
        StaticResource selectedResource = resources.get(0);
        Integer currentPriority = 0;
        for(StaticResource resource : resources){
            if(schemaNames.containsKey(resource.Name) && schemaNames.get(resource.Name) > currentPriority){
                currentPriority = schemaNames.get(resource.Name);
                selectedResource = resource;
            }
        }
        
        return (String.isBlank(selectedResource.NamespacePrefix) ? '' : (selectedResource.NamespacePrefix + '__')) + selectedResource.Name;
    }
}