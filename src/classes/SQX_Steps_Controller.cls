/**
 * controller class for complete steps lightning component
 */
public with sharing class SQX_Steps_Controller {

    public final static String COMPLETE_SF_TASK='Complete Sf task';

    /**
     * method to get map with step sobject type and step id
     * @param recordId SF task id
     * return map with step sobject type and step id
     */
    @AuraEnabled
    public static Map<String, String> resolveObjectName(Id recordId){
        Map<String, String> stepMap = new Map<String, String>();
        Id childWhatId = [SELECT Id, Child_What_Id__c FROM Task WHERE Id = :recordId].Child_What_Id__c;
        stepMap.put('objectType', childWhatId.getSobjectType().getDescribe().getName());
        stepMap.put('stepId', childWhatId);
        CQ_Action__mdt[] actions=getActionForCompleteTask(childWhatId.getSobjectType().getDescribe().getName());
        if(actions!=null){
            for(CQ_Action__mdt cm:actions){
                stepMap.put('componentType',cm.Component_Type__c);
                stepMap.put('componentNamespace',cm.Namespace__c);
                stepMap.put('componentName',cm.Component_Name__c);
                if(cm.Component_Type__c=='VF Page'){
                    String url='/apex/' + cm.Namespace__c + '__' + cm.Component_Name__c+'?recordId='+childWhatId;
                    stepMap.put('returnUrl',url);
                }
            }
        }
        return stepMap;
    }

    /**
     * method to component type and component name that need to be specific action type 
     * @param objectName  define the object name of child id 
     * return custom meta data that action type is action 
     */
    public static CQ_Action__mdt[] getActionForCompleteTask(String objectName){
        SQX_Data_Table_Query_Controller.SQX_ObjectsWithSchema obj = new SQX_Data_Table_Query_Controller.SQX_ObjectsWithSchema();
        return SQX_Lightning_Helper.getActionsFor(objectName, COMPLETE_SF_TASK);
        
    }
}