/**
* removed logic for obsolete document training object
*/
public with sharing class SQX_Document_Training {
    // Note: migration script SQX_DocumentTrainingMigration_Processor and extension SQX_Extension_DTN_To_Sign_Off refers these status variables
    // we need to remove these while doing cleanup for obsolete document training object
    public static final String STATUS_PENDING = 'Pending',
                               STATUS_TRAINER_APPROVAL_PENDING = 'Trainer Approval Pending',
                               STATUS_COMPLETE = 'Complete';
}