/**
* This class stores the common operations in Nonconformance object
* It contains the Status and Resolution of the Nonconformance object
* It also contains the trigger handler for the Nonconformance
*/
public with sharing class SQX_NC{
    
    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_TRIAGE = 'Triage';
    public static final String STATUS_OPEN = 'Open';
    public static final String STATUS_COMPLETE = 'Complete';
    public static final String STATUS_CLOSED = 'Closed';
    
    /* NC Type */
    public static final String TYPE_INTERNAL = 'Internal';
    public static final String TYPE_SUPPLIER = 'Supplier';
    public static final String TYPE_CUSTOMER = 'Customer';

    /* Issue type */
    public static final String ISSUE_TYPE_PRODUCT = 'Product';
    public static final String ISSUE_TYPE_PROCESS = 'Process';


    /* these sets store the NC record that have already been processed for a certain action,
       this is important to prevent reentrancy condition
    */
    //private static Set<Id> processed_updateRelatedFinding = new Set<Id>();
    //TODO: fix issue with test classes that has made reentrant making tests fail


    /**
    * NC Trigger Handler performs actions when NC object is being manipulated, 
    * It primarily assigns default values, and updates related finding.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newNCs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_NonConformance__c> newNCs, Map<Id,SQX_NonConformance__c> oldMap){
        
            super(newNCs, oldMap);
        }

        /*
        * Changes onwer of specified childs on change of owner of this record
        */
        public Bulkified updateRelatedRecordOwner(){
            final String ACTION_NAME = 'updateRelatedRecordOwner';

            //Remove all the previously processed records
            //this.resetView().removeProcessedRecordsFor(SQX.NonConformance, ACTION_NAME);
            Rule hasOwnerChanged = new Rule();
            hasOwnerChanged.addRule(Schema.SQX_Nonconformance__c.OwnerId, RuleOperator.HasChanged);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.NonConformance, ACTION_NAME)
                .applyFilter(hasOwnerChanged, RuleCheckMethod.OnCreateAndEveryEdit);
            List<SQX_Nonconformance__c> ownerChangedNCs= hasOwnerChanged.evaluationResult;

            if(ownerChangedNCs.size()>0){
                this.addToProcessedRecordsFor(SQX.NonConformance, ACTION_NAME, ownerChangedNCs );
                Set<SObjectField> childs = new Set<SObjectField>();
                childs.add(SQX_Finding__c.SQX_Nonconformance__c);
                
                SQX_Utilities.changeOwner(ownerChangedNCs, childs);
            }
            return this;            
        }

        /**
        * This method automatically completes an NC if all NC related policy are met
        */
        public Bulkified completeNCOnAllPolicy() {

            Map<Id, SQX_NonConformance__c> ncsThatMeetCompletionRequirement = new Map<Id, SQX_NonConformance__c>((List<SQX_NonConformance__c>)new SQX_CompletionStrategy(this.resetView().view).matchingStrategy());

            if(ncsThatMeetCompletionRequirement.size() == 0) {
                return this;
            }

            /*
            * finally check if any responses are pending
            */
            List<AggregateResult> incompleteResponses = new AccessAllRecords().getAllIncompleteResponses(ncsThatMeetCompletionRequirement.values());

            /*
            * Iterate through 
            * i. the list of incomplete responses and remove incomplete finding, those with pending responses
            * ii. the list of finding that are ready to be completed and remove all those that have incomplete CAPA.
            */
            for(AggregateResult resp : incompleteResponses) {
                Id ncId = (Id)resp.get('ncId');
                if(ncsThatMeetCompletionRequirement.containsKey(ncId)) {
                    ncsThatMeetCompletionRequirement.remove(ncId);
                }
            }

            Map<Id, SQX_NonConformance__c> completeNCs = new AccessAllRecords().getAllCompleteNCs(ncsThatMeetCompletionRequirement.keySet());

            for(SQX_NonConformance__c nc : ncsThatMeetCompletionRequirement.values()){
                if(!completeNCs.containsKey(nc.Id)){
                    ncsThatMeetCompletionRequirement.remove(nc.Id);
                }
            }

            /*
                i. Create a closure task for finding with no effectiveness review
                ii. Complete all findings that have CAPA associated and is the primary one
            */
            for(SQX_NonConformance__c nc: ncsThatMeetCompletionRequirement.values()){
                nc.Status__c = STATUS_COMPLETE;
            }

            return this;
        }

        /**
        * This method prevents any NC from Closure until all the disposition has been carried out
        * i.e. Closed-Effective, Closed-Non-Effective are not permitted till all the disposition is done.
        */
        public Bulkified preventNCFromClosure(){
            Rule hasNCClosed = new Rule();
            String voidResolution = 'Void';
            boolean trueValue = true;
            hasNCClosed.addRule(Schema.SQX_NonConformance__c.Status__c, RuleOperator.Equals, (Object)STATUS_CLOSED);
            hasNCClosed.addRule(Schema.SQX_Nonconformance__c.Resolution__c, RuleOperator.NotEquals, (Object)voidResolution);
            hasNCClosed.addRule(Schema.SQX_Nonconformance__c.Disposition_Required__c, RuleOperator.Equals, (Object)trueValue);

            this.resetView().applyFilter(hasNCClosed, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            if(hasNCClosed.evaluationResult.size() > 0){
                Map<Id, SQX_NonConformance__c> allValidItems = new Map<Id, SQX_NonConformance__c>((List<SQX_NonConformance__c>)hasNCClosed.evaluationResult);
                Map<Id, SQX_NonConformance__c> completeNCs = new AccessAllRecords().getAllCompleteNCs(allValidItems.keySet());

                for(SQX_NonConformance__c nc : allValidItems.values()){
                    if(!completeNCs.containsKey(nc.Id)){
                        nc.addError('NC requires disposition to be complete before closing');
                    }
                }
            }

            return this;
        }

        /**
         * This method performs all the before updates invoked from the trigger
         * @return   the bulkified object (this) so that method chanining can be used
         */
        public Bulkified performBeforeUpdates() {
            this.resetView();
            for(SQX_Nonconformance__c nc: (List<SQX_Nonconformance__c>) this.view){
                assignDefaultApprovers(nc);
                setCompletionDates(nc);
            }

            return this;
        }

        /**
         * This method will set the completion dates in NC based on the completion date response.
         * TODO: Combine with CAPA to make it generic
         */
        public void setCompletionDates(SQX_Nonconformance__c nc) {
            if(nc.Completion_Date_Response__c == null && nc.Has_Response__c) {
                nc.Completion_Date_Response__c = Date.Today();
            }

            if(nc.Completion_Date_Containment__c == null && nc.Has_Containment__c) {
                nc.Completion_Date_Containment__c = Date.Today();
            }

            if(nc.Completion_Date_Investigation__c == null && nc.Has_Investigation__c) {
                nc.Completion_Date_Investigation__c = Date.Today();
            }
        }

        /**
        * Assigns the default action approver to the nc owner if nothing is provided
        */
        public void assignDefaultApprovers(SQX_Nonconformance__c nc) {

            //assign default disposition approver as owner of NC record
            if(nc.SQX_Disposition_Approver__c == null){
                String objectType = ''+(nc.OwnerID).getSobjectType();

                //if owner is of type user, assign owner as approver, else assign current user as approver. Solves the issue, when owner is queue
                if(objectType.equals('User')){
                    nc.SQX_Disposition_Approver__c = nc.OwnerID;
                }
                else{
                    nc.SQX_Disposition_Approver__c = UserInfo.getUserId();
                }

            }

        }

        /**
         * This method recalled all the responses that are in approval when nc is voided, additionally it also voided draft responses.
         */
        public Bulkified recallResponsesWhenNCIsCompletedThroughNcVoid() {
            Rule ncHasBeenClosed = new Rule();
            ncHasBeenClosed.addRule(Schema.SQX_Nonconformance__c.Status__c, RuleOperator.Equals, STATUS_CLOSED); 
            this.resetView().applyFilter(ncHasBeenClosed, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            List<SQX_Nonconformance__c> ncs = (List<SQX_Nonconformance__c> )ncHasBeenClosed.evaluationResult;

            List<SQX_Finding_Response__c> responsesToBeRecalled = new List<SQX_Finding_Response__c>();
            List<SQX_Finding_Response__c> responsesToVoid = new List<SQX_Finding_Response__c>();

            
            // get all responses that are in approval or in draft
            for(SQX_Finding_Response__c response : [ SELECT Id, Status__c FROM  SQX_Finding_Response__c
                                                        WHERE SQX_Nonconformance__c IN : ncs
                                                        AND (Status__c = :SQX_Finding_Response.STATUS_DRAFT OR
                                                        Status__c = :SQX_Finding_Response.STATUS_IN_APPROVAL)  ]) {

                // find responses that need to be recalled and voided or just voided
                if(response.Status__c == SQX_Finding_Response.STATUS_IN_APPROVAL){
                    responsesToBeRecalled.add(response);
                }

                response.Status__c = SQX_Finding_Response.STATUS_VOID; // void all open responses
                responsesToVoid.add(response);
            }

            //recall all responses that need to be recalled
            SQX_Approval_Util.approveRejectRecallRecords(null, null, responsesToBeRecalled);

            /**
            * Without sharing has been used to cover the following scenario:
            * ------------------------------------------ 
            * All responses that are pending approval (shared by the supplier as read-only) have to be voided
            * 
            * It is not possible to have the sharing settings modified for responses, because allowing this scenario will 
            * allow lots of other unwanted scenarios to occur.
            */
            new SQX_DB().withoutSharing().op_update(responsesToVoid, new List<Schema.SObjectField> { Schema.SQX_Finding_Response__c.Status__c});

            return this;
        }
    }

    public without sharing class AccessAllRecords {
        /**
        * returns all the NCs that are ready to be completed.
        */
        public Map<Id, SQX_NonConformance__c> getAllCompleteNCs(Set<Id> ncs){
            

            Map<Id, SQX_NonConformance__c> allCompleteNCs = new Map<Id, SQX_NonConformance__c>();
            for(SQX_NonConformance__c nc : [SELECT Id, Total_Quantities_Disposed__c , Total_Impacted_Lot_Quantities__c
                ,SQX_Finding__c, Disposition_Required__c , (SELECT Id, Status__c, SQX_Part__c, Lot_Number__c, Disposition_Quantity__c FROM SQX_Dispositions__r)
                        FROM SQX_Nonconformance__c
                        WHERE Id IN : ncs]){

                /*
                * Test 1: if disposition is required, and you haven't provided one, it is not complete
                *        Disposition Required                 (B)                        Has Open Disposition      Is Complete
                *               (A)               (Total Qty Disposed = Total Impacted)        (C)
                *
                *           False                        False                               False                  True
                *           False                        False                               True                   False
                *
                *           False                        True                                False                  True
                *           False                        True                                True                   Can't Arise (there is a bug)
                *
                *           True                         False                               False                  False
                *           True                         False                               True                   False
                *
                *           True                         True                                False                  True
                *           True                         True                                True                   Can't Arise (there is a bug)
                *           
                *
                *  IsComplete = (A'B'C') + (A'BC') + (ABC') 
                *             = C' (A'B' + A' B + A B)
                *             = C' (A'B' + B)
                */

                boolean isComplete = false,
                        hasOpenDisposition = false, //C
                        equalQty = nc.Total_Quantities_Disposed__c == nc.Total_Impacted_Lot_Quantities__c; //B

                //check for C' (No open disposition)
                for(SQX_Disposition__c disposition : nc.SQX_Dispositions__r){
                    
                    if(disposition.Status__c == SQX_Disposition.STATUS_OPEN){
                        hasOpenDisposition = true;
                        break;
                    }
                }

                //check for B (Total Qty Disposed == Total Impacted)
                //NOTE: If disposition is required and both totals are zero, then it is still complete
                //NOTE: if disposition is not required
                //all items may not necessarily be disposed
                //this is subject to argument, we might have to enforce rule that all quantity are disposed.
                            
                        
                isComplete = (!hasOpenDisposition)
                             && (
                                (!nc.Disposition_Required__c && !equalQty)
                                || (equalQty)
                             );


                if(isComplete){
                    allCompleteNCs.put(nc.Id, nc);
                }
            }

            return allCompleteNCs;
        }

        
        /**
        * this function returns the list of all incomplete responses in the given capa
        * @author Pradhanta Bhandari
        * @date 2017/6/23
        * @param ncs this parameter provides a list of all NCs to be matched
        * @return returns a list of finding response along with the NCs for given params.
        */
        public  List<AggregateResult> getAllIncompleteResponses(List<SQX_Nonconformance__c> ncs){
            // all responses in draft with implementation
            // or anything that is in approval

            return [SELECT Count(Id), SQX_Nonconformance__c ncId
            FROM SQX_Finding_Response__c
            WHERE
            Status__c = : SQX_Finding_Response.STATUS_IN_APPROVAL
            AND 
            SQX_Nonconformance__c IN : ncs
            GROUP BY SQX_Nonconformance__c];
        }

    }
}