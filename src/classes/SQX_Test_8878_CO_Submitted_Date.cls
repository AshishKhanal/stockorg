/**
* This test case for should not update submitted date when user submit the change order record.
*/
@isTest
public class SQX_Test_8878_CO_Submitted_Date {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * Given : Change order record with submitted date
    * When : Submitted the CO record 
    * Then : previous submitted date should not override
    */
    static testmethod void givenCORecord_WhenSubmitted_ThenSubmittedDateNotOveride() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Arrange: Create change order record with submitted date
            SQX_Test_Change_Order chgOrder = new SQX_Test_Change_Order();
            Date submittedDate = Date.today()-2;
            chgOrder.changeOrder.Submitted_Date__c = submittedDate;
            chgOrder.save();
            
            //Act: Submit the change order record
            SQX_BulkifiedBase.clearAllProcessedEntities();
            chgOrder.changeOrder.Activity_Code__c = SQX_Change_Order.ACTIVITY_CODE_SUBMIT;
            chgOrder.save();
            
            //Assert: Ensured that submitted date field should not be updated
            SQX_Change_Order__c changeOrder = [SELECT Submitted_Date__c, SQX_Submitted_By__c FROM SQX_Change_Order__c WHERE Id =: chgOrder.changeOrder.Id];
            System.assertEquals(submittedDate, changeOrder.Submitted_Date__c );
            System.assertEquals(adminUser.Id, changeOrder.SQX_Submitted_By__c );
        }
    }
    
    /**
    * Given : Change order record with submitted date null
    * When : Submitted the CO record 
    * Then : set current date
    */
    static testmethod void givenCORecord_WhenSubmittedDateIsNull_ThenSetCurrentDate() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Arrange: Create change order record with submitted date null
            SQX_Change_Order__c changeOrder = new SQX_Change_Order__c(
                Title__c = 'Change Order 2',
                Description__c = 'Description of change order',
                Justification__c  = 'Rationale for the change order',
                Change_Category__c = 'Administrative',
                Submitted_Date__c = null,
                Target_Completion_Date__c = null,
                Status__c = SQX_Change_Order.STATUS_DRAFT
            );
            insert changeOrder;
            //Act: Submit the change order record
            SQX_Change_Order__c submitCO =[SELECT Id, Activity_Code__c FROM SQX_Change_Order__c];
            submitCO.Activity_Code__c = SQX_Change_Order.ACTIVITY_CODE_SUBMIT;
            update submitCO;
            
            //Assert: Ensured that submitted date field updated as current date
            SQX_Change_Order__c changeOrder1 = [SELECT Submitted_Date__c, SQX_Submitted_By__c FROM SQX_Change_Order__c WHERE Id =: submitCO.Id];
            System.assertEquals(Date.today(), changeOrder1.Submitted_Date__c );
            System.assertEquals(adminUser.Id, changeOrder1.SQX_Submitted_By__c );
        }
    }
}