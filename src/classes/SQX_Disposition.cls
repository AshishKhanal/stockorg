/**
* Perform action on SQX_Disposition from trigger or controller.
* @author Pradhanta Bhandari
* @date 2015/5/25
*/
public with sharing class SQX_Disposition {

    /* Readonly statuses that are used in the disposition object's Status field */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_COMPLETE = 'Complete';
    public static final String STATUS_OPEN = 'Open';
    public static final String STATUS_IN_APPROVAL = 'In Approval';
    public static final String APPROVAL_STATUS_APPROVED = 'Approved';
    public static final String APPROVAL_STATUS_NONE = '-';
    public static final String APPROVAL_STATUS_PENDING = 'Pending';



    //this is set to prevent reentrant condition arising if a workflow updates something after the trigger is fired
    public static Set<Id> preventReentrancy_updateimpactedPart = new Set<Id>();


    /**
    * This is the inner class that is called by the trigger to perform various actions on the trigger context.
    * This class has been optimized for bulk operation
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newNCs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Disposition__c> newNCs, Map<Id, SQX_Disposition__c> oldMap){
        
            super(newNCs, oldMap);
        }


        /**
        * this method sets the impacted product lookup field of a disposition based on the 
        * selected part and lot number.
        *
        * it adds an error to the disposition if the product can't be found.
        */
        public Bulkified setTheImpactedProductToCombinationOfPartLot(){
            Rule partOrLotHasChangedRule = new Rule();
            partOrLotHasChangedRule.addRule(Schema.SQX_Disposition__c.SQX_Part__c, RuleOperator.HasChanged);
            partOrLotHasChangedRule.addRule(Schema.SQX_Disposition__c.Lot_Number__c, RuleOperator.HasChanged);
            partOrLotHasChangedRule.addRule(Schema.SQX_Disposition__c.Sublot_Number__c, RuleOperator.HasChanged);
            partOrLotHasChangedRule.operator = RuleCombinationOperator.OpOr;

            this.resetView().applyFilter(partOrLotHasChangedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);



            if(partOrLotHasChangedRule.evaluationResult.size() > 0){


                //if there are disposition with part or lot number changed update them

                /*
                1. Fetch all the impacted part/lot for the combination

                    a. Fetch all findings for the NC mentioned in the disposition
                    b. Fetch all the impacted  part/lot with the defined finding + part + lot combination
                       (Note: The finding + part + lot combination is set in a unique field called uniqueness constraint.
                       We will be using this to perform SOQL query, this will quicken our search)
                */
                Set<Id> ncIds = getIdsForField(partOrLotHasChangedRule.evaluationResult, Schema.SQX_Disposition__c.SQX_Nonconformance__c);
                Map<Id, SQX_NonConformance__c> ncs = new Map<Id, SQX_NonConformance__c>(
                                    [SELECT Id, SQX_Finding__c 
                                    FROM SQX_Nonconformance__c WHERE Id IN : ncIds]
                );


                Map<String, List<SQX_Disposition__c>> uniqueKeys = new Map<String, List<SQX_Disposition__c>>(); //a map to group disposition based on uniqueness constraint (having same finding, impacted part and lot number combination)
                for(SQX_Disposition__c disposition : (List<SQX_Disposition__c>)partOrLotHasChangedRule.evaluationResult){
                    SQX_NonConformance__c nc = ncs.get(disposition.SQX_Nonconformance__c);
                    
                    String uniqueKey = '' + nc.Id + disposition.SQX_Part__c + disposition.Lot_Number__c + disposition.Sublot_Number__c; //generate the key

                    //categorize the disposition based on the key, this is helpful when updating after fetching the data.
                    List<SQX_Disposition__c> categorizedDisposition = uniqueKeys.get(uniqueKey); 
                    if(categorizedDisposition == null){
                        categorizedDisposition = new List<SQX_Disposition__c>();
                        uniqueKeys.put(uniqueKey, categorizedDisposition);
                    }

                    categorizedDisposition.add(disposition);
                }

                //get all the impacted product and update it in the disposition object, set error if not found
                List<SQX_NC_Impacted_Product__c> products = [SELECT Id, Uniqueness_Constraint__c FROM SQX_NC_Impacted_Product__c WHERE Uniqueness_Constraint__c IN : uniqueKeys.keySet()];
                for(SQX_NC_Impacted_Product__c product : products){

                    //get all disposition for the product
                    for(SQX_Disposition__c disposition: uniqueKeys.get(product.Uniqueness_Constraint__c)){
                        disposition.SQX_NC_Impacted_Product__c = product.Id;
                    }

                    uniqueKeys.remove(product.Uniqueness_Constraint__c);

                }


                //for all the remaining disposition in the uniqueness bag add an error
                for(String key : uniqueKeys.keySet()){
                    for(SQX_Disposition__c disposition : uniqueKeys.get(key)){
                        disposition.addError(Label.SQX_ERR_MSG_NO_IMPACTED_PART_FOUND);
                    }
                }
            }

            return this;
        }


        /**
        * Based on SQX-783
        * Update the quantity of items to be disposed in SQX_Impacted_Product Object, when the disposition is marked complete.
        * Update the completion date of SQX_Nonconformance to the last completed Disposition's date.
        * @author Pradhanta Bhandari
        * @date 2015/5/25
        */
        public Bulkified updateDisposedQtyWhenDispositionIsComplete(){

            //1. Fetch all disposition that have been recently completed.
            Rule completeDipositionsRule = new Rule();
            completeDipositionsRule.addRule(Schema.SQX_Disposition__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);

            this.resetView().applyFilter(completeDipositionsRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Disposition__c> completeDispositions = new List<SQX_Disposition__c>();
            //add reentrant check because this method is not idempotent, if this is called twice impacted part lot
            //gets disposed qty added to it twice
            for(SObject obj : completeDipositionsRule.evaluationResult){
                Id objId = (Id) obj.get('Id');
                if(!SQX_Disposition.preventReentrancy_updateimpactedPart.contains(objId)){
                    //if it hasn't already been processed add it to the list of items to be processed.
                    completeDispositions.add((SQX_Disposition__c)obj);
                    SQX_Disposition.preventReentrancy_updateimpactedPart.add(objId); //add this to list of processed records
                }
            }
            

            //for all complete disposition update the nc's dispositioned qty
            if(completeDispositions.size() > 0){

                //get all the related nc for the disposition (Note: FOR Update has been used to lock the record)
                Set<Id> allImpactedPartToFetch = getIdsForField(completeDipositionsRule.evaluationResult, Schema.SQX_Disposition__c.SQX_NC_Impacted_Product__c);
                Map<Id, SQX_NC_Impacted_Product__c> allImpactedPartToUpdate = new Map<Id, SQX_NC_Impacted_Product__c>([SELECT Id, Total_Quantity_Disposed__c
                                                         FROM SQX_NC_Impacted_Product__c
                                                         WHERE Id IN : allImpactedPartToFetch
                                                         FOR UPDATE]);

                //add the disposed quantity to the related nc
                for(SQX_Disposition__c disposition : completeDispositions){
                    SQX_NC_Impacted_Product__c impProd = allImpactedPartToUpdate.get(disposition.SQX_NC_Impacted_Product__c);
                    impProd.Total_Quantity_Disposed__c += disposition.Disposition_Quantity__c;
                }

                /*
                * update all ncs with the new disposition quantity
                * without sharing has been used to update the record because Disposition can be added by anyone
                * even the ones without any read/write rights in NonConformance.
                */
                new SQX_DB().withoutSharing().op_update(allImpactedPartToUpdate.values(), new List<Schema.SObjectField>{
                        Schema.SQX_NC_Impacted_Product__c.Total_Quantity_Disposed__c
                    });


                //additionally update disposition completion date for NC
                Set<Id> allNcsToCheck = getIdsForField(completeDipositionsRule.evaluationResult, Schema.SQX_Disposition__c.SQX_NonConformance__c);
                //get the list of all ncs with disposition qty = dispositioned qty
                Map<Id, SQX_NonConformance__c> ncs = new Map<Id, SQX_NonConformance__c>( [SELECT Id FROM SQX_NonConformance__c WHERE Id IN : allNcsToCheck AND 
                Is_Disposition_Complete__c = true]);

                if(ncs.size() > 0){
                    //set the completion date for all ncs
                    List<SQX_Nonconformance__c> ncsToUpdate = new List<SQX_Nonconformance__c>();

                    List<AggregateResult> maxCompletion = [SELECT SQX_NonConformance__c NonConformance, Max(Completed_On__c) LastCompletionDate
                                             FROM SQX_Disposition__c 
                                             WHERE SQX_NonConformance__c IN : ncs.keySet()
                                             GROUP BY SQX_NonConformance__c ];

                    for(AggregateResult result : maxCompletion){

                        SQX_Nonconformance__c nc = ncs.get((Id)result.get('NonConformance'));
                        nc.Completion_Date_Disposition__c = (Date)result.get('LastCompletionDate');
                        ncsToUpdate.add(nc);
                    }

                    /*
                    *WITHOUT Sharing has been used because NC will be owned by somebody else than creator/submitter of Disposition.
                    */
                    new SQX_DB().withoutSharing().op_update(ncsToUpdate, new List<Schema.SObjectField>{
                            Schema.SQX_NonConformance__c.Completion_Date_Disposition__c
                        });
                }
            }

            return this;
        }

        /**
        * @description: prevent the deletion of disposition if the disposition is completed
        * @story: [SQX-2321]
        */
        public Bulkified preventDeletionOfCompletedDisposition(){

            Rule hasDispositionCompleted = new Rule();
            hasDispositionCompleted.addRule(Schema.SQX_Disposition__c.Status__c, RuleOperator.Equals, SQX_Disposition.STATUS_COMPLETE);

            this.resetView()
                .applyFilter(hasDispositionCompleted, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);



            if(hasDispositionCompleted.evaluationResult.size() > 0){

                for(SQX_Disposition__c disposition : (List<SQX_Disposition__c>) hasDispositionCompleted.evaluationResult){
                    disposition.addError(Label.SQX_ERR_MSG_COMPLETED_DISPOSITION_CANT_BE_DELETED);
                }

            }
            return this;
        }

    }

}