@IsTest
public class SQX_Test_Implementation_Action{
    
    /**
    * this test ensures that our status values are in sync with picklist values
    */
    public static testmethod void givenStatusEnsureItExistsInPickList(){
        String [] allStatuses = new String[] { SQX_Implementation_Action.STATUS_OPEN,
                                               SQX_Implementation_Action.STATUS_COMPLETE,
                                               SQX_Implementation_Action.STATUS_SKIPPED };
        
        // validate all statuses exists in picklist
        SQX_Test_Base_Class.validatePickListValuesMatch(allStatuses, SQX.Action, 'Status__c');
    }
    
    public static SQX_Action__c prepareMockObject(SQX_Capa__c capa){
        SQX_Action__c  action = new SQX_Action__c();
        
        action.Due_Date__c = Date.Today();
        action.Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW;
        action.SQX_CAPA__c = capa.Id;
        
        return action;
    }
}