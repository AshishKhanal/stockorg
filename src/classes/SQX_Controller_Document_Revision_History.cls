/**
* controller for controlled document revision history component
*/
public with sharing class SQX_Controller_Document_Revision_History {
    
    /**
    * main controlled document record
    */
    public SQX_Controlled_Document__c mainRecord { get; set; }
    
    /**
    * flag to toggle show all revisions in revision history
    */
    public Boolean showAllRevisions { get; set; }
    
    /**
    * default constructor
    */
    public SQX_Controller_Document_Revision_History() {
        showAllRevisions = false;
    }
    
    /**
    * returns true when document compare package page is available in the org
    */
    public Boolean getIsDocComparePackagePageAvailable() {
        return SQX_Controller_Document_Compare.isDocComparePackagePageAvailable;
    }
    
    /**
    * returns all released controlled document revision history of the current document (default)
    * and returns all document revisions of the current document number when showAllRevisions is true
    */
    public List<SQX_Controlled_Document__c> getDocRevisions() {
        return SQX_Controlled_Document.getRevisionHistory(mainRecord, showAllRevisions);
    }
   
}