/**
 * Test class to validate Complaint and Complaint Associated Item creation with active and inactive parts
 */ 
@isTest
public class SQX_Test_7582_Complaint_Associated_Item {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
     * GIVEN: An Inactive Part
     * WHEN: Inactive Part Added to Complaint
     * THEN: Complaint record should not be created
     */
    public static testMethod void givenInactivePart_WhenInactivePartAddedToComplaint_ThenComplaintRecordShouldNotBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        User adminUser = allUsers.get(ADMIN_USER);
        
        System.runAs(standardUser){
            // Arrange: Create a Part
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(1);
            
            // Update Part active status to inactive
            SQX_Part__c part = [SELECT Id, Active__c, Part_Family__c FROM SQX_Part__c WHERE Id=:parts.get(0).Id];
            part.Active__c = false;
            update part;
            
            // Act: Create Complaint record with Inactive Part
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = part.Id;
            Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint__c> {compTst.complaint}, new List<Schema.SObjectField>{});
                  
            // Assert: Ensure Complaint record not created with inactive part
            System.assert(!saveResults[0].isSuccess(), 'Complaint record with inactive Part was created');
            
        }
        
    }
    
    /**
     * GIVEN: An Active Part
     * WHEN: Active Part Added to Complaint
     * THEN: Complaint record should not be created
     */
    public static testMethod void givenActivePart_WhenActivePartAddedToComplaint_ThenComplaintRecordShouldBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        User adminUser = allUsers.get(ADMIN_USER);
        
        System.runAs(standardUser){
            // Arrange: Create a Part
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(1);
                        
            // Act: Create Complaint record with active Part
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = parts.get(0).Id;
            Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint__c> {compTst.complaint}, new List<Schema.SObjectField>{});
                
            // Assert: Ensure Complaint record was created with active part
            System.assert(saveResults[0].isSuccess(), 'Complaint record with active Part was not created');
            
        }
        
    }
    
    /**
     * GIVEN: An Inactive Part
     * WHEN: Inactive Part Added to Complaint Associated Item
     * THEN: Complaint Associated Item record should not be created
     */
    public static testMethod void givenInactivePart_WhenInactivePartAddedToComplaintAssociatedItem_ThenComplaintAssociatedItemRecordShouldNotBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        User adminUser = allUsers.get(ADMIN_USER);
        
        System.runAs(standardUser){
            // Arrange: Create a Part and Complaint record
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(2);
            
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = parts.get(0).Id;
            compTst.save();
            
            // Update Part active status to inactive
            SQX_Part__c part = [SELECT Id, Active__c, Part_Family__c FROM SQX_Part__c WHERE Id=:parts.get(1).Id];
            part.Active__c = false;
            update part;
            
            // Act: Create Complaint Associated Item with Inactive Part            
            SQX_Complaint_Associated_Item__c associatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id);
            Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Associated_Item__c> {associatedItem}, new List<Schema.SObjectField>{});
            
            // Assert: Ensure Associated Item record not created with inactive part
            System.assert(!saveResults[0].isSuccess(), 'Associated Item record with inactive Part was created');
            
        }
        
    }
    
    /**
     * GIVEN: An Active Part
     * WHEN: Active Part added to Complaint Associated Item
     * THEN: Complaint Associated Item record should be created
     */
    public static testMethod void givenAnActivePart_WhenActivePartAddedToComplaintAssociatedItem_ThenComplaintAssociatedItemRecordShouldBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        User adminUser = allUsers.get(ADMIN_USER);
        
        System.runAs(standardUser){
            // Arrange: Create a Part and Complaint record
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(1);
            
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = parts.get(0).Id;
            compTst.save();
                        
            // Act: Create Complaint Associated Item with Inactive Part            
            SQX_Complaint_Associated_Item__c associatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = parts.get(0).Part_Family__c, SQX_Part__c = parts.get(0).Id);
            Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Associated_Item__c> {associatedItem}, new List<Schema.SObjectField>{});
            
            // Assert: Ensure Associated Item record not created with inactive part
            System.assert(saveResults[0].isSuccess(), 'Associated Item record with active Part was not created');
            
        }
        
    }
}