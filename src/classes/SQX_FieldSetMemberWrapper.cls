public with sharing class SQX_FieldSetMemberWrapper{
    public Schema.FieldSetMember field {get; set;}
    public boolean isReadonly {get; set;}
}