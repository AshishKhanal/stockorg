/**
* This class contains the logic related to cross reference complaint (related complaint)
*/
public with sharing class SQX_Cross_Reference_Complaint {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        public Bulkified(){
            
        }
        
        /**
        * The constructor for handling the trigger activities
        * @param newCrossReferenceComplaint the related complaints that have been newly added/updated
        * @param oldMap the map of old values of the cross reference complaint
        */ 
        public Bulkified(List<SQX_Cross_Reference_Complaint__c> newCrossReferenceComplaint, Map<Id, SQX_Cross_Reference_Complaint__c> oldMap){
            super(newCrossReferenceComplaint, oldMap);
        }
        
        /**
         * Method to prevent deletion of related complaint if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Cross_Reference_Complaint__c> relatedComplaints = new Map<Id, SQX_Cross_Reference_Complaint__c>((List<SQX_Cross_Reference_Complaint__c>)this.view);
            
            for(SQX_Cross_Reference_Complaint__c relatedComplaint : [SELECT Id FROM SQX_Cross_Reference_Complaint__c WHERE Id IN : relatedComplaints.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    relatedComplaints.get(relatedComplaint.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }
        
    }

}