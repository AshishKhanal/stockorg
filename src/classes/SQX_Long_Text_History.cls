/**
* Long Text History object handler class
*/
public with sharing class SQX_Long_Text_History {
    
    /**
    * generates record index value using combination of fields in order of Parent Record Id, Field Name, Changed By Id
    */
    public static String generateRecordIndex(String parentRecordId, String fldName, String changedById) {
        return parentRecordId + fldName + changedById;
    }
    
    /**
    * bulkified trigger class
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        /**
        * constructor
        */
        public Bulkified(List<SQX_Long_Text_History__c> newObjs, Map<Id, SQX_Long_Text_History__c> oldObjMap) {
            super(newObjs, oldObjMap);
        }
        
        /**
        * sets Record Index field value
        */
        public Bulkified setRecordIndex() {
            resetView();
            
            for (SQX_Long_Text_History__c hist : (List<SQX_Long_Text_History__c>)view) {
                hist.Record_Index__c = generateRecordIndex(hist.Parent_Record_Id__c, hist.Field_Name__c, hist.SQX_Changed_By__c);
            }
            
            return this;
        }
        
    }
    
}