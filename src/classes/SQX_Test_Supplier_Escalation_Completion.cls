/*
 * Unit tests for Supplier Escalation Completion Flow
 */
@isTest
public class SQX_Test_Supplier_Escalation_Completion {

    /*
    *   Following scenarios are to tested
    *   1. If there are no applicable steps, the record should move to Complete/Verification on initiation
    *   2. When all the steps are completed, the record should move to Complete/Verification
    *   3. Given open SE, when the remaining step(s) are rendered unapplicable, the record should move to Complete/Verification
    */

    /**
     *  Test setup
     */
    @testSetup
    public static void commonSetup(){

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');

        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> { standardUser });

        System.runAs(adminUser) {
            // create account record
            List<Account> accounts = new List<Account>();
            accounts.add(new Account(
                Name = SQX_Test_Supplier_Escalation.ACCOUNT_NAME 
            ));

            insert accounts;
            
            //Create contact for Account
            List<Contact> contacts = new List<Contact>();
            contacts.add(new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = accounts[0].Id,
                Email = System.now().millisecond() + 'test@test.com'
            ));
            insert contacts;
        }
    }

    /**
     *  Returns <code>true</code> if the given Supplier Escalation record is complete
     */
    private static Boolean isRecordComplete(SQX_Supplier_Escalation__c se) {
        return se.Status__c == SQX_Supplier_Common_Values.STATUS_COMPLETE &&
                se.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_VERIFICATION &&
                se.Workflow_Status__c == SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED;
    }

    /**
     *  Given : Draft SE record with no applicable steps
     *  When : SE is initiated
     *  Then : Record is complete
     */
    private static testmethod void givenSEWithNoApplicableSteps_WhenTheRecordIsInitiated_ThenTheRecordIsSetAsComplete() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {
            // ARRANGE : A Draft SE record
            SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation();
            se.save();
            se.submit();

            // simply adding an unapplicable step
            SQX_Supplier_Escalation_Step__c unAppliciableStep = new SQX_Supplier_Escalation_Step__c(
                SQX_Parent__c= se.supplierEscalation.Id,
                Name='CustomStep',
                Step__c=1,
                SQX_User__c=UserInfo.getUserId(),
                Due_Date__c=Date.today(),
                RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, SQX_Steps_Trigger_Handler.RT_TASK),
                Applicable__c = false
            );
            insert unAppliciableStep;

            Test.startTest();

            // ACT : initiate the record
            se.initiate();
            se.synchronize();

            Test.stopTest();


            // ASSERT : record should be complete
            System.assert(isRecordComplete(se.supplierEscalation), 'Expected record to be complete but found ' + se.supplierEscalation);
        }

    }


    /**
     *  Given : Open SE record with applicable steps
     *  When : All the steps are completed
     *  Then : Record is complete
     */
    private static testmethod void givenSEWithApplicableSteps_WhenTheStepsAreCompleted_ThenTheRecordIsSetAsComplete() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');

        Integer numberOfPolicyTasks = 2;

        SQX_Test_Supplier_Escalation se;
        List<SQX_Supplier_Escalation_Step__c> steps;

        System.runAs(adminUser) {
            for(Integer i=1; i <= numberOfPolicyTasks; i++) {
                SQX_Test_Supplier_Escalation.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, assigneeUser, i);
            }
        }

        System.runAs(standardUser) {
            // ARRANGE : An Open SE record
            se = new SQX_Test_Supplier_Escalation();
            se.save();
            se.submit();

            // asserting that we have all the policy tasks copied
            steps = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: se.supplierEscalation.Id ORDER BY Step__c];
            System.assertEquals(numberOfPolicyTasks, steps.size(), 'Unexpected number of tasks found');

            se.initiate();

        }

        System.runAs(assigneeUser) {

            // ACT : Complete all the tasks

            Test.startTest();

            List<Task> tasksToComplete;
            Task t;

            tasksToComplete = [SELECT Id FROM Task WHERE WhatId =: se.supplierEscalation.Id AND Child_What_Id__c =: steps.get(0).Id];

            System.assertEquals(1, tasksToComplete.size(), 'Unexpected number of tasks found');

            t = tasksToComplete.get(0);
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();

            tasksToComplete = [SELECT Id FROM Task WHERE WhatId =: se.supplierEscalation.Id AND Child_What_Id__c =: steps.get(1).Id];

            System.assertEquals(1, tasksToComplete.size(), 'Unexpected number of tasks found');

            t = tasksToComplete.get(0);
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            Test.stopTest();

        }

        System.runAs(standardUser) {
            // ASSERT : record should be complete
            se.synchronize();
            System.assert(isRecordComplete(se.supplierEscalation), 'Expected record to be complete but found ' + se.supplierEscalation);
        }

    }


    
    /**
     *  Given : Open SE record with one applicable step
     *  When : the step is rendered unapplicable
     *  Then : Record is complete
     */
    private static testmethod void givenOpenSEApplicableStep_WhenStepIsMadeUnapplicable_ThenTheRecordIsSetAsComplete() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');

        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> { standardUser });

        System.runAs(adminUser) {
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, assigneeUser, 1);
        }

        System.runAs(standardUser) {

            // ARRANGE : An Open SE record
            SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation();
            se.save();
            se.submit();
            se.initiate();
            se.synchronize();

            List<SQX_Supplier_Escalation_Step__c> steps = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: se.supplierEscalation.Id ORDER BY Step__c];
            System.assertEquals(1, steps.size(), 'Unexpected number of steps found');

            System.assertEquals(SQX_Supplier_Common_Values.STATUS_OPEN, se.supplierEscalation.Status__c, 'Expected escalation record to be open after initiation');

            // ACT : Make step unapplicable
            SQX_Supplier_Escalation_Step__c step = steps.get(0);
            step.Applicable__c = false;

            Test.startTest();
            update step;

            se.synchronize();

            Test.stopTest();

            // ASSERT : record should be complete
            System.assert(isRecordComplete(se.supplierEscalation), 'Expected record to be complete but found ' + se.supplierEscalation);
        }

    }


}