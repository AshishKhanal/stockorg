/**
*   Handler class to execute trigger actions
*/


public with sharing virtual class SQX_Trigger_Handler extends SQX_BulkifiedBase {
    /**
    * Action constants
    */
    public static final String ACTION_ERR_KEY_RECORD_ID = '{RECORD_ID}',
        ACTION_ERR_KEY_ERRORS = '{ERRORS}';

    /** Holds the type of object being worked on */
    public SObjectType objType;


    /** Default constructor */
    public SQX_Trigger_Handler() {}


    /**
    * Default constructor for this class, that accepts old and new map from the trigger
    * @param newValues equivalent to trigger.New in salesforce
    * @param oldMap equivalent to trigger.OldMap in salesforce
    */
    public SQX_Trigger_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
        super(newValues, oldMap);

        if(this.view.size() > 0) {
            objType = this.view[0].getSObjectType();
        }
    }


    /**
     *  Method to be overloaded by inherting classes to execute all Before Trigger actions
     */
    protected virtual void onBefore() {}


    /**
     *  Method to be overloaded by inherting classes to execute all After Trigger actions
     */
    protected virtual void onAfter() {}


    /**
     *  Method to execute all actions before any trigger actions of inheriting classes are run
     */
    protected virtual void commonActionsBefore() {}


    /**
     *  Method to execute all actions after all the before trigger actions of inheriting extending classes are run
     */
    protected virtual void commonActionsAfter() {}



    /**
     *  Method to execute all the Trigger actions
     */
    public void execute() {

        if(objType == null) {
            return;
        }

        if(Trigger.isBefore) {
            commonActionsBefore();
            onBefore();
        }

        if(Trigger.isAfter) {
            commonActionsAfter();
            onAfter();
        }
    }


    //// HELPER METHODS ////////

    /**
     *  Method returns the SObjectField object from the given field name
     */
    public SObjectField getSObjectField(String fieldName) {
        if(objType.getDescribe().fields.getMap().containsKey(fieldName)) {
            return objType.getDescribe().fields.getMap().get(fieldName);
        }
        return null;    // throw error?
    }

    /**
     *  Method returns name of the object of the given objtype
     */
    public String getObjName() {
        return objType.getDescribe().getName();
    }
    /**
    * Test only variable that is useful for intercepting actions that are invoked by action
    */
    @testVisible
    private static Action_Interceptor interceptor;
    
    
    /**
    * Method that must be invoked if error message are to be tracked and unit tested.
    * Note: Users can invoke addError directly on the object but this can be a problem while unit testing
    *       since there isn't any direct way of finding which object has an error without resorting heuristics such as PageMessage 
    * @param obj the object in which an error has to be added
    * @param errorMsg the error message that has to be set on the object
    */
    public static void addError(SObject obj, String errorMsg) {
        // TEST-INTERCEPT that triggers notifying the test interceptor of an error being added to the method
        if (Test.isRunningTest() && SQX_Trigger_Handler.interceptor != null) {
            SQX_Trigger_Handler.interceptor.onError(obj, errorMsg);
        }
        obj.addError(errorMsg);
    }
    
    /**
    * method to call an action to manage the DML and handle error considering sharing settings
    */
    public Action getAction(){
        return getAction(false);
    }

    /**
    * method to call an action to manage the DML and handle error without considering sharing settings
    */
    public Action getEscalatedAction(){
        return getAction(true);
    }

    /**
    * A factory method to get an action to manage the DML and handle error.
    * @param withoutSharing flag to indicate where record should be changed without checking security settings
    */
    private Action getAction(Boolean withoutSharing){
        Action action = null;

        action = new Action(withoutSharing);
        
        return action;
    }

    
    
    /**
    * This class will be used to identify a particular action that is being performed by the bulk processor.
    * This class provides common functionality to associate change and with its initiating record
    */
    public with sharing class Action {
        /**
        * delete action error format
        */
        public String deleteErrorFormat = Label.SQX_TRIGGER_ERR_MSG_FOR_FAILED_DELETE_ACTION_ON_RELATED_RECORD;
        
        /**
        * insert action error format
        */
        public String insertErrorFormat = Label.SQX_TRIGGER_ERR_MSG_FOR_FAILED_INSERT_ACTION_ON_RELATED_RECORD;
        
        /**
        * update action error format
        */
        public String updateErrorFormat = Label.SQX_TRIGGER_ERR_MSG_FOR_FAILED_UPDATE_ACTION_ON_RELATED_RECORD;
        
        /**
        * This map that stores object with the list of impactors that had started the insertion/update/deletion of the object (Key)
        */
        @testVisible
        private Map<SObject, List<SObject>> impactorMap;
        
        /**
        * This map that stores the objects that must be updated grouped by their object type
        */
        @testVisible
        private Map<SObjectType, List<SObject>> updateMap;
        
        /**
        * This map stores the objects that must be inserted grouped by their object type 
        */
        @testVisible
        private Map<SObjectType, List<SObject>> insertMap;
        
        /**
        * This map stores the objects that must be deleted grouped by their record type
        */
        @testVisible
        private Map<SObjectType, List<SObject>> deleteMap;

        /**
        * This stores the list of fields whose FLS check must be ignored.
        * This is relevant for cases such as follows
        *
        * Lets say if we have a contact C1 belonging to the Account A1
        * If we query C1 as such [SELECT Id, AccountId, Name FROM Contact WHERE ... FOR Update]
        * 
        * and perform an update on C1.Name = 'New Name';
        * we might want to ignore fls check of Account field when it hasn't been changed and it is non-updateable field
        */
        private Set<SObjectField> ignoredFields;
        
        /**
        * The actual object that performs the DML operations. This object must be used to configure things like withoutSharing and
        * failOnError.
        */
        public SQX_DB db {public get; private set;}
        
        /**
        * Constructor for action name
        */
        public Action(Boolean withoutSharing){
            
            this.impactorMap = new Map<SObject, List<SObject>>();
            this.updateMap = new Map<SObjectType, List<SObject>>();
            this.insertMap = new Map<SObjectType, List<SObject>>();
            this.deleteMap = new Map<SObjectType, List<SObject>>();
            
            this.ignoredFields = new Set<SObjectField>();

            this.db = new SQX_DB();
            this.db.continueOnError();
            
            if(withoutSharing){
                
                this.db.withoutSharing();
            }

        }  

        /**
        * Ignore the fields for a given object. This might be useful if some of the fields that have
        * been populated in the object haven't been modified and are to be ignored.
        * @param sObjectFields the list of fields that are to be ignored for the FLS check
        */
        public void ignoreFields(SObjectField[] sObjectFields) {
            ignoredFields.addAll(sObjectFields);
        }
        
        /**
        * Internal method to store the list of fields that have been modified in the proper store
        * @param sType the sobject type of the entity that is being updated
        * @param mFields the list of fields that are being modified
        * @param store the store that maintains the list
        * @return returns the list of fields that must have been modified
        */
        @testVisible
        private Map<SObjectType, SObjectField[]> getFieldList(Map<SObjectType, List<SObject>> objectMap){
            Map<SObjectType, SObjectField[]> allModifiedFields = new Map<SObjectType, SObjectField[]>();
            final string ID_FIELD = 'Id';

            for (SObjectType objType : objectMap.keySet()) {

                List<SObjectField> modifiedFields = new List<SObjectField>();

                // we are taking first element as a sample
                SObject obj = objectMap.get(objType).get(0);
                Map<String, SObjectField> fieldMap = obj.getSObjectType().getDescribe().fields.getMap();
                

                for (String field : obj.getPopulatedFieldsAsMap().keySet()) {
                    SObjectField fieldToAdd = fieldMap.get(field);
                    // fieldTOAdd can be null in test 
                    // TODO : need to investigate
                    if (fieldToAdd == null || ignoredFields.contains(fieldToAdd) || (Trigger.isUpdate && field == ID_FIELD)) {
                        continue;
                    }
			
                    modifiedFields.add(fieldToAdd);
                }

                allModifiedFields.put(objType, modifiedFields);
            }
            

            return allModifiedFields;
        }
        
        
        /**
        * This method is used to associate the initiator with the affected record in a dml collection
        * @param initiator the object that started action
        * @param affected the object that is being used in the DML
        * @param dmlCollection the collection that is to be updated
        */
        private void addForDML(SObject initiator, SObject affected, Map<SObjectType, List<SObject>> dmlCollection){
            
            SObjectType objType = affected.getSObjectType();

            List<SObject> reln = impactorMap.get(affected);
            if(reln == null){
                reln = new List<SObject>();
                impactorMap.put(affected, reln);
            }

            List<SObject> coll = dmlCollection.get(objType);
            if(coll == null){
                coll = new List<SObject>();
                dmlCollection.put(objType, coll);
            }

            reln.add(initiator);
            coll.add(affected);
        }

        /**
        * Method to associate the Insert action performed in the action
        * @param initiator the object that started the action
        * @param affected the object that is being inserted by the action
        */
        public void op_insert(SObject initiator, SObject affected){
            addForDML(initiator, affected, insertMap);
        }

        /**
        * Method to associate the update on object performed in the action
        * @param initiator the object that started the action
        * @param affected the object that is being updated by the action
        */
        public void op_update(SObject initiator, SObject affected){
            addForDML(initiator, affected, updateMap);
        }

        /**
        * Method to associate deletion of object performed in the action
        * @param initiator the object that started the action
        * @param affected the object that is being deleted by the action
        */
        public void op_delete(SObject initiator, SObject affected){
            addForDML(initiator, affected, deleteMap);
        }


        /**
        * Method to commit the pending DML operations for the action.
        */
        public void op_commit(){
            
            if(insertMap.size() > 0){
                Database.SaveResult[] results = db.op_insert(insertMap, getFieldList(insertMap));
                associateErrorIfAnyAndUpdateParentLink(db.lastDMLObjects, results, true, insertErrorFormat);
            }

            if(updateMap.size() > 0){
                Database.SaveResult[] results = db.op_update(updateMap, getFieldList(updateMap));
                associateErrorIfAnyAndUpdateParentLink(db.lastDMLObjects, results, false, updateErrorFormat);
            }


            if(deleteMap.size() > 0){
                Database.DeleteResult[] results = db.op_delete(deleteMap);
                associateErrorIfAny(db.lastDMLObjects, results, deleteErrorFormat);
            }

        }
        

        /**
        * Internal method to associate error occurred to the causing/initiating object.
        * @param dmlObject the list of objects that were inserted/updated
        * @param saveResults the list of save results. Note the number of DML Object should be same as saveResults.
        * @param updateField true if the parent field has to be updated with newly inserted object field
        * @param errFormat error message format to use
        */
        private void associateErrorIfAnyAndUpdateParentLink(List<SObject> dmlObject, List<Database.SaveResult> saveResults, Boolean updateParentField, String errFormat){
            System.assertEquals(dmlObject.size(), saveResults.size(), 'DML object and result size doesnt match');

            // SF Nuance: the map that was used to the store impact will now have different key. Because Id field will have been set incase of insertion
            //            This will reset the keySet and allow correct selection of the object.
            impactorMap = impactorMap.clone();

            for(Integer i = 0 ; i < saveResults.size(); i++){
                if(!saveResults.get(i).isSuccess()){
                    setErrorOnInitiator(dmlObject.get(i), saveResults[i].getErrors(), errFormat);
                }
            }
        }

        /**
        * Internal method to associate error occurred to the causing/initiating object.
        * @param dmlObject the list of objects that were inserted/updated
        * @param deleteResults the list of save results. Note the number of DML Object should be same as deleteResults.
        * @param errFormat error message format to use
        */
        private void associateErrorIfAny(List<SObject> dmlObject, List<Database.DeleteResult> deleteResults, String errFormat){
            System.assertEquals(dmlObject.size(), deleteResults.size(), 'DML object and result size doesnt match');
            
            for (Integer i = 0 ; i < deleteResults.size(); i++) {
                if (!deleteResults.get(i).isSuccess()) {
                    setErrorOnInitiator(dmlObject.get(i), deleteResults[i].getErrors(), errFormat);
                }
            }
        }

        /**
        * Internal method to set error on the objects initiating the action
        * @param affected the object whose save/update/delete failed
        * @param errors the list of database error that occurred while performing the action
        * @param errFormat error message format to use
        */
        private void setErrorOnInitiator(SObject affected, Database.Error [] errors, String errFormat){
            List<SObject> affectingObjects = impactorMap.get(affected);
            List<String> errMsgs = new List<String>();
            String internalError = String.isBlank(errFormat) ? ACTION_ERR_KEY_ERRORS : errFormat;

            for (Database.Error error : errors) {
                errMsgs.add(error.getMessage());
            }
            
            internalError = internalError.replace(ACTION_ERR_KEY_RECORD_ID, ''+affected.Id).replace(ACTION_ERR_KEY_ERRORS, String.join(errMsgs, '; '));
            
            for(SObject obj : affectingObjects){
                SQX_Trigger_Handler.addError(obj, internalError);
            }
        }

    }
    
    /**
    * This interface will be used for testing purposes and will be invoked by the framework. So, that certain
    * actions can be performed without actually performing Database Insert/Update.
    *
    * This interface can be aptly described as "Test Optimization Interface"
    * @author Pradhanta Ram Bhandari
    */
    public interface Action_Interceptor {
        
        /**
         * Invoked when data is requested for the commit.
         * @param act The action that invoked the commit record.
         * @return return <code>true</code> if actual commit is to be skipped. <code>false</code> if we want to commit
         */
        Boolean onCommit(SQX_Trigger_Handler.Action act);
        
        /**
         * Invoked when an error is being added to the erroneous object. Useful for aggregating the errors
         * occuring in the test.
         * @param obj the object that has caused the error
         * @param errorMessage the message that has to be added to the object
         */
        void onError(SObject obj, String errorMessage);
        

    }

}