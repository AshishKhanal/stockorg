/**
 * test class for the change control action
 * @author: Anish Shrestha
 * @date: 2016-01-27
 * @story: [SQX-1948]
 */
@isTest
public class SQX_Test_1948_Action_Task{

    static boolean runAllTests = true,
                run_givenAUser_WhenActionIsCreated_RelatedSFTaskIsCreated = false,
                run_givenAAction_WhenActionIsCompletedOrSkipped_RelatedSFTaskIsCompleted = false,
                run_givenAAction_WhenActionDueDateAndAssigneeChanged_RelatedSFTaskIsUpdated = false;

    /**
     * Action: Create a action with record type action
     * Expected: When a action is created related sf task should be created
     * @author: Anish Shrestha
     * @date: 2016-01-27
     * @story: [SQX-1948]
     */    
    public testmethod static void givenAUser_WhenActionIsCreated_RelatedSFTaskIsCreated(){

        if(!runAllTests && !run_givenAUser_WhenActionIsCreated_RelatedSFTaskIsCreated){
            return;
        }
        //Arrange: Create two actions
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
        List<Task> tasks = new List<Task>();

        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, taskAssignee, 'Action Description',  Date.Today().addDays(15)));
            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, taskAssignee, 'Action Description',  Date.Today().addDays(15)));

            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });

            // Assert 1: Two actions should be created
            actions = [SELECT Id, Status__c, TaskID__c FROM SQX_Implementation__c];
            
            //System.assert(false, actions);
            System.assertEquals(2, actions.size(), 'Expected two actions to be created but found: ' + actions.size());

            System.assertEquals(SQX_Implementation.STATUS_OPEN, actions[0].Status__c, 'Expected the status to be open but found: ' + actions[0].Status__c);

            // Assert 2: One sf task should be created
            tasks = [SELECT Id FROM Task];
            System.assertEquals(1, tasks.size(), 'Expected one task to be created but found: ' + tasks.size());
        }
    }

    /**
     * Action: Create a action with record type action and complete it
     * Expected: When a action is complete or skipped, sf task should be complete
     * @author: Anish Shrestha
     * @date: 2016-01-27
     * @story: [SQX-1948]
     */
    public testmethod static void givenAAction_WhenActionIsCompletedOrSkipped_RelatedSFTaskIsCompleted(){

        if(!runAllTests && !run_givenAAction_WhenActionIsCompletedOrSkipped_RelatedSFTaskIsCompleted){
            return;
        }
        //Arrange: Create a action
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, taskAssignee, 'Action Description',  Date.Today().addDays(15)));

            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });
            
            //One sf task should be created
            List<Task> tasks = [SELECT Id FROM Task];
            System.assertEquals(1, tasks.size(), 'Expected only one task to be created but found: ' + tasks.size());

            // Act: Complete the action
            SQX_BulkifiedBase.clearAllProcessedEntities();    //clear for trigger handler
            actions.get(0).Status__c = SQX_Implementation.STATUS_COMPLETE;
            new SQX_DB().op_update(actions, new List<SObjectField>{SQX_Implementation__c.fields.Status__c});
            
            SQX_Implementation__c action = [SELECT Id, TaskId__c FROM SQX_Implementation__c WHERE Id = : actions.get(0).Id];
            
            // Assert: Expected the standard sf task to be completed as well
            Task taskCompleted = [SELECT Id, Status FROM Task WHERE Id =: Id.valueOf(action.TaskID__c)];


            System.assertEquals(SQX_Task.STATUS_COMPLETED, taskCompleted.Status, 'Expected the task to be completed but found: ' + taskCompleted.Status);

        }
    }

    /**
     * Action: Create a action with record type action and save and then change due date or assignee
     * Expected: When a action changed, releted SF task should be changed
     * @author: Anish Shrestha
     * @date: 2016-02-16
     * @story: [SQX-1948]
     */
    public testmethod static void givenAAction_WhenActionDueDateAndAssigneeChanged_RelatedSFTaskIsUpdated(){

        if(!runAllTests && !run_givenAAction_WhenActionDueDateAndAssigneeChanged_RelatedSFTaskIsUpdated){
            return;
        }
        //Arrange: Create a action
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, taskAssignee, 'Action Description',  Date.Today().addDays(15)));

            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });
            
            //One sf task should be created
            List<Task> tasks = [SELECT Id FROM Task];
            System.assertEquals(1, tasks.size(), 'Expected only one task to be created but found: ' + tasks.size());

            // Act: Change the action due date and assignee
            SQX_BulkifiedBase.clearAllProcessedEntities();    //clear for trigger handler
            actions.get(0).Due_Date__c = Date.Today().addDays(15);
            actions.get(0).SQX_User__c = taskAssignee2.Id;

            new SQX_DB().op_update(actions, new List<SObjectField>{SQX_Implementation__c.fields.Due_Date__c,
                                                                    SQX_Implementation__c.fields.SQX_User__c});
            
            SQX_Implementation__c actionChanged = [SELECT Id, Due_Date__c,  SQX_User__c, TaskID__c FROM SQX_Implementation__c WHERE Id = : actions.get(0).Id];
            
            // Assert: Expected the standard sf task to be changed as well
            Task taskUpdated = [SELECT Id, OwnerId, ActivityDate FROM Task WHERE Id =: Id.valueOf(actionChanged.TaskId__c)];

            System.assertEquals(actionChanged.Due_Date__c, taskUpdated.ActivityDate , 'Expected the due date to be ' + actionChanged.Due_Date__c +' but found: ' + taskUpdated.ActivityDate);
            System.assertEquals(actionChanged.SQX_User__c, taskUpdated.OwnerId , 'Expected the owner be ' + actionChanged.SQX_User__c +' but found: ' + taskUpdated.OwnerId);

        }
    }
}