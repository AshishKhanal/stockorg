/*
 * Unit tests for Supplier Interaction
 */
@isTest
public class SQX_Test_Supplier_Interaction {
    
    public static final String INTERACTION_QUEUE_DEVELOPER_NAME = 'CQ_Supplier_Interaction_Queue';
    
    public SQX_Supplier_Interaction__c si;
    
    private static SQX_DB db = new SQX_DB();
    
    public static final String ACCOUNT_NAME = 'SQX_Test_Supplier_Interaction_Account';
    
    @testSetup
    public static void commonSetup(){
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(adminUser) {
            // create auto number record for supplier interaction
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Interaction',
                Next_Number__c = 1,
                Number_Format__c = 'INT-{1}',
                Numeric_Format__c = '0000'
            );
        }
        
        System.runAs(standardUser) {
            // Create accounts
            createAccounts();
        }
    }

    /**
     * Create account
     */
    public static void createAccounts() {
        // create account record
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(
            Name = ACCOUNT_NAME 
        ));
        accounts.add(new Account(
            Name = ACCOUNT_NAME 
        ));    
        insert accounts;
        
        //Create contact for Account
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(
            FirstName = 'Bruce',
            Lastname = 'Wayne',
            AccountId = accounts[0].Id,
            Email = System.now().millisecond() + 'test@test.com'
        ));
        contacts.add(new Contact(
            FirstName = 'John',
            Lastname = 'Wayne',
            AccountId = accounts[1].Id,
            Email = System.now().millisecond() + 'test@test.com'
        ));
        insert contacts;
    }
    
    /*
     * returns account record created from commonSetup()
     */
    public static List<Account> getAccountAndContactRecords(){
        return [SELECT Id, Name, (SELECT Id, Name FROM Contacts) FROM Account WHERE Name =: ACCOUNT_NAME];
    }
    
    /*
     * Constructor for creating Supplier Interaction
     * @param: account Account to be set while creating supplier interaction
     */
    public SQX_Test_Supplier_Interaction(Account account){
        this(account, 'New Document Request');
    }
    
    /**
     * Constructor for creating Supplier Interaction
     * @param: account Account to be set while creating supplier interaction.
     * @param: interaction reason
     */
    public SQX_Test_Supplier_Interaction(Account account, String interactionReason){
        si = new SQX_Supplier_Interaction__c();
        si.Interaction_Reason__c = interactionReason;
        si.SQX_Account__c = account.Id;
        si.SQX_Supplier_Contact__c = account.Contacts[0].Id;
        si.Part_Name__c = 'Part 1';
        si.Result__c = SQX_Supplier_Common_Values.RESULT_APPROVED;
    }

    /*
     * Method for saving supplier interaction
     */
    public SQX_Test_Supplier_Interaction save(){
        if (this.si.Id == null) {
            db.op_insert(new List<SQX_Supplier_Interaction__c>{this.si}, new List<Schema.SObjectField>{});
        }
        else {
            db.op_update(new List<SQX_Supplier_Interaction__c>{this.si}, new List<Schema.SObjectField>{});
        }
        return this;
    }
    
    public SQX_Test_Supplier_Interaction() {
        Account account = SQX_Test_Account_Factory.createAccount();
        Contact contact = SQX_Test_Account_Factory.createContact(account);
        si = new SQX_Supplier_Interaction__c(SQX_Account__c = account.Id, 
                                           SQX_Supplier_Contact__c = contact.Id,
                                           Interaction_Reason__c = 'New Document Request',
                                           Part_Name__c = 'Part 1',
                                           Result__c = SQX_Supplier_Common_Values.RESULT_APPROVED);
    }
    
    /**
     * method to set status of supplier interaction
     */
    public SQX_Test_Supplier_Interaction setStatus(String status){
        si.Status__c = status;
        return this;
    }
    
    /**
     * method to set stage of supplier interaction
     */
    public SQX_Test_Supplier_Interaction setStage(String stage){
        si.Record_Stage__c = stage;
        return this;
    }

    /**
     *  Populates the current si record with all the field values
     */
    public SQX_Test_Supplier_Interaction synchronize() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.field = 'Id';
        filter.operator = 'eq';
        filter.value = si.Id;

        si = (SQX_Supplier_Interaction__c)SQX_DynamicQuery.getAllFields(SQX_Supplier_Interaction__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter, new List<SObjectField>(), false).get(0);

        return this;
    }
    
    /**
    * GIVEN : Create supplier interaction record
    * WHEN : save the record
    * THEN : Record Number is set properly using Auto Number setting with name "Supplier Interaction"
    * WHEN : Record Number with auto number value is modified
    * THEN : Error is thrown
    */
    public static testMethod void givenNewSupplierInteraction_WhenSaved_ThenAutoNumberIsSetInRecordNumber(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            // ARRANGE: get account and contact record
            List<Account> accounts = getAccountAndContactRecords();
            
            // ACT: Create supplier document record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction().save();
            
            // ASSERT: Ensured that auto number is reflected
            SQX_Supplier_Interaction__c savedSupInt = [SELECT Name, Prevent_Name_Update__c FROM SQX_Supplier_Interaction__c WHERE Id =: supInt.si.Id];
            System.assertEquals('INT-0001', savedSupInt.Name, 'Record Number should be set as per auto number setting when creating Supplier Interaction record.');
            System.assertEquals(true, savedSupInt.Prevent_Name_Update__c,'Internal field Prevent_Name_Update_Flag__c is expected to be checked when auto number is set by flow.');
            
            // ACT: Modify and save record number
            supInt.si.Name = 'Modified Value 123456';
            Database.SaveResult sr = Database.update(supInt.si, false);
            
            // ASSERT: verify record is not saved and proper error is thrown
            String expErrMsg = 'Supplier Interaction Number cannot be modified.';
            System.assertEquals(false, sr.isSuccess(), 'Modified record number of the supplier Interaction record is expected not to be saved.');
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + sr.getErrors());
        }
    }

    /*
     * Method to sumit supplier interaction
     */
    public void submit() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_SUBMIT;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
     * method to initiate supplier interaction
     */
    public void initiate() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
     * method to complete supplier interaction
     */
    public void complete() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to void supplier interaction
     */
    public void void() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID;
        this.si.Is_Locked__c = true;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * method to redo supplier interaction step
     */
    public void redoStep(SQX_Supplier_Interaction_Step__c step){
        step = [SELECT Id, 
                        Step__c, 
                        SQX_Parent__c, 
                        Name, 
                        SQX_User__c,
                        Description__c, 
                        RecordTypeId,  
                        SQX_Controlled_Document__c 
                        FROM SQX_Supplier_Interaction_Step__c 
                        WHERE Id =: step.Id];

        SQX_Supplier_Redo_Step.RedoStepRequest req = new SQX_Supplier_Redo_Step.RedoStepRequest();
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_PRE;
        req.recordId = step.Id;
        req.step = Integer.valueOf(step.Step__c);
        req.assignee = step.SQX_User__c;
        req.controlledDocument = step.SQX_Controlled_Document__c;
        req.description = step.Name;
        SQX_Supplier_Redo_Step.redoStep(req);
        
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_POST;
        SQX_Supplier_Redo_Step.redoStep(req);
    }
    /**
     *  method to restart supplier interaction
     */
    public void restart() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART;
        this.si.Current_Step__c = 1;
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
     *  method to close supplier interaction
     */
    public void close() {
        this.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
        this.si.Is_Locked__c = true;

        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
    * method to add user to queue
    */
    public static void addUserToQueue(List<User> usersToAssign){
        SQX_Test_Utilities.addUserToQueue(usersToAssign, INTERACTION_QUEUE_DEVELOPER_NAME, SQX.SupplierInteraction);
    }
    
    /*
        This number holds the number of SDs created which shall make the policy task name unique
    */
    static Integer siNumber = 0;
    
    /**
     * method to create policy task of different task types
     */
    public static List<SQX_Task__c> createPolicyTasks(Integer numberOfTasks, String taskType, User assignee, Decimal step){
        List<SQX_Task__c> taskList = new List<SQX_Task__c>();
        for(Integer i = 0; i < numberOfTasks; i++){
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = assignee.Id, 
                                               Description__c = 'Test_DESC', 
                                               Record_Type__c = 'Supplier Interaction', 
                                               Step__c = step,
                                               Active__c = true,
                                               Interaction_Reason__c = 'New Document Request',
                                               Name = 'SI_' + siNumber++ + '_Test_Policy_Task_' + i + 'Step ' + step,
                                               Task_Type__c = taskType,
                                               Document_Name__c = taskType == SQX_Task.TASK_TYPE_DOC_REQUEST ? SQX_Supplier_Common_Values.DOCUMENT_NAME_SAMPLE_DOC : null);
            taskList.add(task);
        }
        
        insert taskList;
        
        return taskList;
    }

    /**
     * Given: given SI record after draft stage
     * When: user try to change the Interaction Reason
     * Then : throws the validation error message
     */
    public static testMethod void ensureInteractionReasonCannotBeChangedAfterDraftStage(){
        final String validationErrMsg = 'Interaction Reason cannot be changed.';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            // Arrange: Get account record
            List<Account> accounts = getAccountAndContactRecords();
            // create policy task so that it will create step later on submitting escalation record
            createPolicyTasks(1, 'Task', standardUser, 1);
            
            // Arrange: Create and Submit Supplier Interaction record
            SQX_Test_Supplier_Interaction supplierInteraction = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            supplierInteraction.submit();
            supplierInteraction.initiate();
            // Act: Try to change interaction reason when record is in stage other than 'Draft'
            supplierInteraction.si.Interaction_Reason__c = 'Component Review';
            List<Database.SaveResult> result = Database.update(new List<SQX_Supplier_Interaction__c> { supplierInteraction.si }, false);
            System.assertEquals(result[0].isSuccess(), false, 'Interaction reason cannot be changed after draft stage');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), validationErrMsg), 'Expected error message : ' + validationErrMsg + ' but got : ' + result[0].getErrors());
        }
    }
    
    /**
     * Given: given SI record
     * When: user try to change the interaction number
     * Then : throws the validation error message
     */
    public static testMethod void ensureInteractionNumberCannotBeChanged(){
        final String validationErrMsg = 'Supplier Interaction Number cannot be modified.';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            // Arrange: Get account record
            List<Account> accounts = getAccountAndContactRecords();
            // create policy task so that it will create step later on submitting escalation record
            createPolicyTasks(1, 'Task', standardUser, 1);
            
            // Arrange: Create and Submit Supplier Interaction record
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            // Act: Try to update record with new Interaction Number
            interaction.si.Name = '123';
            List<Database.SaveResult> result = Database.update(new List<SQX_Supplier_Interaction__c> { interaction.si }, false);
            
            // Assert: Interaction number cannot be updated.
            System.assertEquals(result[0].isSuccess(), false, 'Interaction number cannot be changed');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), validationErrMsg), 'Expected error message : ' + validationErrMsg + ' but got : ' + result[0].getErrors());
        }
    }
   
    /**
     * Given: given SI record with Closed Status
     * When: user try to modify the fieds of record
     * Then : throws the validation error message
     */
    public static testMethod void ensurePreventModificationOfLockedRecord(){
        final String validationErrMsg = 'Record status or Step status does not support the Action Performed.';
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            // Arrange: Get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            // Arrange: Create, Submit, Initiate and Close the Supplier Interaction record
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            interaction.submit();
            interaction.initiate();
            interaction.close();
            // Act: Try to update record
            interaction.si.Scope__c = 'New scope';
            interaction.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_SAVE;
            List<Database.SaveResult> result = Database.update(new List<SQX_Supplier_Interaction__c> { interaction.si }, false);
            
            // Assert: Record is closed so cannot be modified
            System.assertEquals(result[0].isSuccess(), false, 'Cannot modify the locked record');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), validationErrMsg), 'Expected error message : ' + validationErrMsg + ' but got : ' + result[0].getErrors());
        }
    }
    
    /**
     * Given: given SI record on different stages
     * When: user try to change the state of record for different scenarios
     * Then : throws the validation error message
     */
    public static testMethod void ensureInvalidStateTransitions(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            // Arrange: Get account record
            List<Account> accounts = getAccountAndContactRecords();
            // create policy task so that it will create step later on submitting escalation record
            createPolicyTasks(1, 'Task', standardUser, 1);
            
            // Arrange: Create and Submit Supplier Interaction record
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            
             // Act: Try to restart interaction when status is not complete
            validateError(SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART, interaction.si, 'Record in draft stage cannot be restarted');
            
            // Act: Try to complete interaction when stage is draft
            validateError(SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE, interaction.si, 'Record in draft stage cannot be completed');
            
            interaction.si.Scope__c = 'Scope 1';
            interaction.submit();
            // Act: Try to submit interaction record when it is already submitted
            validateError(SQX_Supplier_Common_Values.ACTIVITY_CODE_SUBMIT, interaction.si, 'Already submitted record cannot be submitted');
               
            interaction.si.Scope__c = 'Scope 2';
            interaction.initiate();
            // Act: Try to initiate interaction record when it is already initiated
            validateError(SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE, interaction.si, 'Already initiated record cannot be initiated');
        }
    }
    
    private static void validateError(String activityCode, SQX_Supplier_Interaction__c interaction, String msg) {
        final String validationErrMsg = 'Unable to perform the desired action on the record. Please ensure that the record\'s status/stage permits the action.';
        interaction.Activity_Code__c = activityCode;
        List<Database.SaveResult> result = Database.update(new List<SQX_Supplier_Interaction__c> { interaction }, false);
        // Assert : Ensure validation error is thrown
        System.assert(result.size() ==1 && result[0].isSuccess() == false, msg);
        System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), validationErrMsg), 'Expected error message : ' + validationErrMsg + ' but got : ' + result[0].getErrors());
    }
    
    /*
     * Given: Supplier Interaction record
     * When: Supplier Contact is not related to Account
     * Then: Error message is thrown
     */
    public static testMethod void givenSupplierInteraction_WhenContactIsNotInAccount_ThenShowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Interaction record
            List<SQX_Supplier_Interaction__c> suppInteraction = new List<SQX_Supplier_Interaction__c>();
            SQX_Test_Supplier_Interaction suppInteraction1 = new SQX_Test_Supplier_Interaction(accounts[0]);
            SQX_Test_Supplier_Interaction suppInteraction2 = new SQX_Test_Supplier_Interaction(accounts[0]);
            suppInteraction1.si.SQX_Supplier_Contact__c = accounts[0].Contacts[0].Id;
            suppInteraction.add(suppInteraction1.si);
            suppInteraction2.si.SQX_Supplier_Contact__c = accounts[1].Contacts[0].Id;
            suppInteraction.add(suppInteraction2.si);
            
            //Act : Insert supplier interaction records
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(suppInteraction, new List<Schema.SObjectField>{});
            
            //Assert: verify error message for contact not related to supplier interaction account
            System.assertEquals(true, result[0].isSuccess(), 'Supplier Interaction should be saved. ' + result[0].getErrors());
            System.assertEquals(false, result[1].isSuccess(), 'Supplier Interaction should not be saved. ' + result[1].getErrors());
            String interactionErrMsg = 'Please select contact related to the selected account.';
            System.assert(SQX_Utilities.checkErrorMessage(result[1].getErrors(), interactionErrMsg), 'Expected error: ' + interactionErrMsg + ' Actual Errors: ' + result[1].getErrors());
        }
    }

    /*
     * Given: Supplier Interaction record
     * When: Controlled Document without status Pre-Release and Current is selected
     * Then: Error message is thrown
     */
    public static testMethod void givenSupplierInteraction_WhenDocumentStatusIsNotMatched_ThenShowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: Create controlled document
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            doc1.doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            doc1.save();
            
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save();
            doc2.doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
            doc2.save();
            
            //Arrange: create Supplier Interaction record and set controlled document
            List<SQX_Supplier_Interaction__c> supInt = new List<SQX_Supplier_Interaction__c>();
            SQX_Test_Supplier_Interaction suppInteraction1 = new SQX_Test_Supplier_Interaction(accounts[0]);
            SQX_Test_Supplier_Interaction suppInteraction2 = new SQX_Test_Supplier_Interaction(accounts[0]);
            suppInteraction1.si.SQX_Renew_Controlled_Document__c = doc1.doc.Id;
            supInt.add(suppInteraction1.si);
            suppInteraction2.si.SQX_Renew_Controlled_Document__c = doc2.doc.Id;
            supInt.add(suppInteraction2.si);
            
            //Act : Insert supplier escalation records
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(supInt, new List<Schema.SObjectField>{});
            
            //Assert: verify error message for contact not related to supplier escalation account
            System.assertEquals(true, result[0].isSuccess(), 'Supplier Interaction should be saved. ' + result[0].getErrors());
            System.assertEquals(false, result[1].isSuccess(), 'Supplier Interaction should not be saved. ' + result[1].getErrors());
            String expErrMsg = 'Only Pre-Release or Current Controlled Documents can be used.';
            System.assert(SQX_Utilities.checkErrorMessage(result[1].getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result[1].getErrors());
        }
    }
    /*
     * When: Record is closed with null Result value
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierInteraction_WhenRecordIsClosedWithoutResultValue_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        addUserToQueue(new List<User> {standardUser});        
        System.runAs(standardUser) {
            // Arrange: Get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            // Arrange: Create, Submit, Initiate and Close the Supplier Interaction record
            List<SQX_Supplier_Interaction__c> supplierInteractions = new List<SQX_Supplier_Interaction__c>();
            SQX_Test_Supplier_Interaction interaction1 = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            SQX_Test_Supplier_Interaction interaction2 = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            interaction1.submit();
            interaction1.initiate();
            interaction1.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            supplierInteractions.add(interaction1.si);
            
            interaction2.submit();
            interaction2.initiate();
            interaction2.si.Result__c = null;
            interaction2.si.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE; 
            supplierInteractions.add(interaction2.si);
            
            //Act:Insert the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(supplierInteractions, new List<Schema.SObjectField>{});
            
            // Assert: Validation error
            System.assertEquals(true, result[0].isSuccess(), 'Supplier Interaction should be saved. ' + result[0].getErrors());
            System.assertEquals(false, result[1].isSuccess(), 'Supplier Interaction should not be saved. ' + result[1].getErrors());
            String expErrMsg = 'Supplier Interaction Result is required.';
            System.assert(SQX_Utilities.checkErrorMessage(result[1].getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result[1].getErrors());
        }
    }
}