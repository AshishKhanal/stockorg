public with sharing class SQX_Department {
    /**
    * This is the method that perform bulkified operation on Department object
    * This method is called from the department trigger based on the type of trigger.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified(){
        }


        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newPrograms equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Department__c> newPrograms, Map<Id,SQX_Department__c> oldMap){
        
            super(newPrograms, oldMap);
        }

        //
        public Bulkified ensureQueueIdIsValid(){
                
                Rule hasQueueIdChanged = new Rule();
                hasQueueIdChanged.addRule(Schema.SQX_Department__c.Queue_Id__c, RuleOperator.HasChanged);
                hasQueueIdChanged.addRule(Schema.SQX_Department__c.Queue_Id__c, RuleOperator.NotEquals, null);
                
                this.resetView().applyFilter(hasQueueIdChanged, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

                List<SQX_Department__c> validDepartments = (List<SQX_Department__c>) hasQueueIdChanged.evaluationResult;
                
                set<Id> queueids = new set<Id>();
                List<SQX_Department__c> depts = new List<SQX_Department__c>();
                for(SQX_Department__c program : validDepartments) {
                    Id QueueId;
                    try{
                        /*
                        * to check if the id is valid we use the following test
                        * i. the entered string value is a valid id.
                        * ii. the sobjecttype of the Id is 'QueueId'
                        * 
                        */
                        QueueId = Id.valueOf(program.Queue_Id__c);
                        queueids.add(QueueId);
                        depts.add(program);
                    }
                    catch(Exception ex){
                        //if the id is not valid and an exception is thrown just ignore it.
                    }

                    if (QueueId == null) {
                        program.Queue_Id__c.addError(Label.SQX_ERR_MSG_INVALID_QUEUE_ID);
                    }
                }

                if (queueids.size() > 0) {
                    Map<Id, Group> validQueueIds = new Map<Id, Group>([SELECT Id FROM Group WHERE Id IN :queueids]);
                    
                    for(SQX_Department__c program : depts){
                        Id QueueId = Id.valueOf(program.Queue_Id__c);
                        //if the id is not present add the exception message.
                        if(!validQueueIds.containsKey(QueueId))
                            program.Queue_Id__c.addError(Label.SQX_ERR_MSG_INVALID_QUEUE_ID);
                    }
                }

                return this;
            }

        }

    
}