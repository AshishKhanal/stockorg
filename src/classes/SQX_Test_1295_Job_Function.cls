/**
* Unit tests for Job Function Object
*
* @author Dibya Shrestha
* @date 2015/06/30
* 
*/
@IsTest
public class SQX_Test_1295_Job_Function {

    static Boolean runAllTests = true,
                   run_givenAddJobFunctionName_whenDuplicateFound_JobFunctionNotAdded = false,
                   run_givenAddJobFunctionName_whenDuplicateNotFound_JobFunctionAdded = false,
                   run_givenEditJobFunctionNameForCaseChange_JobFunctionSaved = false,
                   run_givenEditJobFunctionName_whenDuplicateFound_JobFunctionNotSaved = false,
                   run_givenEditJobFunctionName_whenDuplicateNotFound_JobFunctionSaved = false;
    
    /**
    * this test ensures addition of new job function with duplicate name (case-insensitive) to fail
    */
    public testmethod static void givenAddJobFunctionName_whenDuplicateFound_JobFunctionNotAdded(){
        if (!runAllTests && !run_givenAddJobFunctionName_whenDuplicateFound_JobFunctionNotAdded){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            SQX_Job_Function__c jobFunction1 = new SQX_Job_Function__c( Name = 'job function test' );
            Database.SaveResult result1 = Database.insert(jobFunction1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected new job function to be saved.');
            
            SQX_Job_Function__c jobFunction2 = new SQX_Job_Function__c( Name = 'JOB FUNCTION TEST' );
            Database.SaveResult result2 = Database.insert(jobFunction2, false);
            
            System.assert(result2.isSuccess() == false,
                'Expected new duplicate job function not to be saved.');
        }
    }
    
    /**
    * this test ensures addition of new unique job function to pass
    */
    public testmethod static void givenAddJobFunctionName_whenDuplicateNotFound_JobFunctionAdded(){
        if (!runAllTests && !run_givenAddJobFunctionName_whenDuplicateNotFound_JobFunctionAdded){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            SQX_Job_Function__c jobFunction1 = new SQX_Job_Function__c( Name = 'job function test' );
            Database.SaveResult result1 = Database.insert(jobFunction1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected 1st new unique job function to be saved.');
            
            SQX_Job_Function__c jobFunction2 = new SQX_Job_Function__c( Name = 'JOB FUNCTION 2 TEST' );
            Database.SaveResult result2 = Database.insert(jobFunction2, false);
            
            System.assert(result2.isSuccess() == true,
                'Expected 2nd new unique job function to be saved.');
        }
    }
    
    /**
    * this test ensures changing case for job function name to pass
    */
    public testmethod static void givenEditJobFunctionNameForCaseChange_JobFunctionSaved(){
        if (!runAllTests && !run_givenEditJobFunctionNameForCaseChange_JobFunctionSaved){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            SQX_Job_Function__c jobFunction1 = new SQX_Job_Function__c( Name = 'job function test' );
            Database.SaveResult result1 = Database.insert(jobFunction1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected new job function to be saved.');
                
            jobFunction1.Name = 'Job function Test';
            
            Database.SaveResult result2 = Database.update(jobFunction1, false);
            
            System.assert(result2.isSuccess() == true,
                'Expected changing case for job function name to be saved.');
        }
    }
 
    /**
    * this test ensures modification of job function with duplicate name (case-insensitive) to fail
    */
    public testmethod static void givenEditJobFunctionName_whenDuplicateFound_JobFunctionNotSaved(){
        if (!runAllTests && !run_givenEditJobFunctionName_whenDuplicateFound_JobFunctionNotSaved){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            SQX_Job_Function__c jobFunction1 = new SQX_Job_Function__c( Name = 'job function 1 test' );
            Database.SaveResult result1 = Database.insert(jobFunction1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected 1st new job function to be saved.');
            
            SQX_Job_Function__c jobFunction2 = new SQX_Job_Function__c( Name = 'job function 2 test' );
            Database.SaveResult result2 = Database.insert(jobFunction2, false);
            
            System.assert(result2.isSuccess() == true,
                'Expected 2nd new job function to be saved.');
            
            jobFunction2.Name = 'JOB FUNCTION 1 TEST'; // name same as jobFunction1
            Database.SaveResult result3 = Database.update(jobFunction2, false);
            
            System.assert(result3.isSuccess() == false,
                'Expected modified job function with duplicate name not to be saved.');
        }
    }
    
    /**
    * this test ensures modification of job function with unique name to pass
    */
    public testmethod static void givenEditJobFunctionName_whenDuplicateNotFound_JobFunctionSaved(){
        if (!runAllTests && !run_givenEditJobFunctionName_whenDuplicateNotFound_JobFunctionSaved){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser) {
            SQX_Job_Function__c jobFunction1 = new SQX_Job_Function__c( Name = 'job function 1 test' );
            Database.SaveResult result1 = Database.insert(jobFunction1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected 1st new job function to be saved.');
            
            SQX_Job_Function__c jobFunction2 = new SQX_Job_Function__c( Name = 'job function 2 test' );
            Database.SaveResult result2 = Database.insert(jobFunction2, false);
            
            System.assert(result2.isSuccess() == true,
                'Expected 2nd new job function to be saved.');
            
            jobFunction2.Name = 'Job Function 2 Test Edited';
            Database.SaveResult result3 = Database.update(jobFunction2, false);
            
            System.assert(result3.isSuccess() == true,
                'Expected modified job function with unique name to be saved.');
        }
    }

}