/**
*   @author : Piyush Subedi
*   @date : 2016/12/1
*   @description : Class implements functionality when collaboration provider is SF
*/
public with sharing class SQX_SF_Collaboration_Provider implements SQX_CollaborationProvider{
    
    public static final String  COLLABORATION_TYPE_PRIVATE = 'Private',
                                COLLABORATION_TYPE_PUBLIC = 'Public';

    public static final String  FEED_TYPE_CONTENT_POST = 'ContentPost';
    
    public static final String  KEY_COLLABORATION_CONTENT_TITLE = 'TITLE',
                                KEY_COLLABORATION_CONTENT_URL = 'URL',
                                KEY_COLLABORATION_GROUP_ID = 'GROUP_ID',
                                KEY_COLLABORATION_CONTENT_ID = 'CONTENT_ID',
                                KEY_COLLABORATION_CONTENT_FILENAME = 'FILE_NAME';

    public static final String COLLABORATION_MEMBER_ROLE_ADMIN = 'Admin'; 
    private SavePoint sp;                           

    public static final String POST_TITLE_COLLABORATION_GROUP_CLOSED = 'Group Closed';
    /**
    *   Initiates/Creates a collaboration group, adds the default list of members, adds the content
    *   @param collaborationTitle - collaboration name
    *   @param members - list of initial members to be added to the group
    *   @param docId - id of the associated controlled document 
    *   @return Map - map of group info ('GROUP_ID' -> newly formed group's id, 'CONTENT_ID' -> id of the content stored in the new group)
    */
    public Map<String, String> initiateCollaboration(String collaborationTitle, String[] members, String docId){
        
        Map<String,String> grpInfo;

        this.sp = Database.setSavepoint();

        //  creating new group
        String collaborationGroupId = createCollaborationGroup(CollaborationTitle);
        
        if( !String.isBlank(collaborationGroupId) ){

            //  adding content to group
            String contentId = SQX_Controlled_Document_Collaboration.cloneControlledDocumentContent(docId);
            addCollaborationContent(collaborationGroupId, contentId);

            /*
                adding doc to the collaboration group using SF's CollaborationGroupRecord object
                necessary only when provider is SF
            */
            addCollaborationGroupRecord(collaborationGroupId, docId);

            //  adding default list of members to the group
            List<User> users = [SELECT Id FROM User WHERE Id IN :members];
            if(users.size() > 0)
                addMembers(users, collaborationGroupId);

            grpInfo = new Map<String, String>{
                KEY_COLLABORATION_GROUP_ID => collaborationGroupId,
                KEY_COLLABORATION_CONTENT_ID => contentId
            };

        }

        return grpInfo;
    }

    /**
    *   Method to revert collaboration group creation
    *   @param groupId - Id of the collaboration group which is to be deleted
    */
    public void undoCollaborationInitiation(String groupId){
        if(this.sp != null){
            Database.rollback(this.sp);
        }
    }

    /**
    * Create a new collaboration group
    * @param CollaborationTitle - collaboration name
    */
    private String createCollaborationGroup(String CollaborationTitle){

        CollaborationGroup collaborationObj=new CollaborationGroup();
        collaborationObj.Name=CollaborationTitle;
        collaborationObj.OwnerId=UserInfo.getUserId();
        collaborationObj.CollaborationType= COLLABORATION_TYPE_PRIVATE;
        collaborationObj.IsAutoArchiveDisabled =true;
        collaborationObj.Description=Label.CQ_UI_Collaboration_Standard_Description;

        List<Database.SaveResult> results = new SQX_DB().continueOnError().op_insert(new List<CollaborationGroup>{collaborationObj}, new List<Schema.SObjectField>{
                Schema.CollaborationGroup.Name,
                Schema.CollaborationGroup.OwnerId,
                Schema.CollaborationGroup.CollaborationType,
                Schema.CollaborationGroup.IsAutoArchiveDisabled,
                Schema.CollaborationGroup.Description    
        });

        /*
            If the error is because of duplicate group name, throw an error about the same (SF's error message seemed ambiguous)
        */
        Database.SaveResult rs = results.get(0);
        if( !rs.isSuccess() ){
            for(Database.Error err : rs.getErrors() ){
                if( err.getStatusCode() == StatusCode.DUPLICATE_VALUE){
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, Label.SQX_ERR_MSG_DUPLICATE_GROUP_NAME) );
                }else{
                    ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, err.getMessage()) );
                }
            }
        }

        return collaborationObj.Id;
    }
    
    /**
    * Add collaboration group record
    * @param collaborationGroupId - collaboration group id
    * @param recordId - controlled documenet id
    */
    private void addCollaborationGroupRecord(String collaborationGroupId,String recordId){
        CollaborationGroupRecord cgrObj=new CollaborationGroupRecord();
        cgrObj.CollaborationGroupId = collaborationGroupId;
        cgrObj.RecordId = recordId;    
        new SQX_DB().op_insert(new List<CollaborationGroupRecord>{cgrObj}, new List<Schema.SObjectField>{
                Schema.CollaborationGroupRecord.CollaborationGroupId,
                Schema.CollaborationGroupRecord.RecordId 
        });
    }

    /**
    *   Adds content to group
    *   @param grpId - id of the group to which the content is to be added
    *   @param id - content document id of the content to be added to the group
    *   @return String - added content's id
    */
    private void addCollaborationContent(String grpId, String contentId){

        // getting the content version
        List<ContentVersion> cvs = [SELECT Id FROM ContentVersion WHERE ContentDocumentId = :contentId AND IsLatest = true];

        if(cvs.size() > 0){
            // creating feed item
            FeedItem fiObj=new FeedItem();
            fiObj.RelatedRecordId=cvs.get(0).Id;
            fiObj.Type=FEED_TYPE_CONTENT_POST;
            fiObj.ParentId=grpId;
            new SQX_DB().op_insert(new List<FeedItem>{ fiObj }, new List<Schema.SObjectField>{
                    Schema.FeedItem.RelatedRecordId,
                    Schema.FeedItem.Type,
                    Schema.FeedItem.Title,
                    Schema.FeedItem.ParentId
            });
        }else{
            throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_CANNOT_VIEW_CONTROLLED_DOCUMENT_CONTENT);
        }
    }

    /**
    *   Closes/Archives/Deletes collaboration group
    *   @param groupId - Id of the collaboration group to be closed
    */
    public void closeCollaboration(String groupId){

        if( userCanCloseGroup(groupId) ){

            CollaborationGroup cg = [SELECT Name FROM CollaborationGroup WHERE Id = :groupId];
            this.sp = Database.setSavepoint();
            /* Add a feed post such that the members can be notified of the closure */
            postFeedToGroup(cg);

            // archiving the group
            cg.isArchived = true;

            new SQX_DB().op_update(new List<CollaborationGroup> { cg }, new List<sObjectField> { CollaborationGroup.isArchived } );

        }else{
            throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_INSUFFICIENT_PERMISSION_TO_CLOSE_GROUP);
        }
    }

    /**
    *  On close collaboration action, If we get any validation errors, the action should be rollback
    *  @param groupId - Id of the collaboration group to be undo close Collaboration
    */
    public void undoCloseCollaboration(String groupId){
        if (this.sp != null) {
            Database.rollback(this.sp);
        }
    }

    /**
    *   This method posts feed to the given group
    *   @param grp - Group to which the post is to be made
    */
    private void postFeedToGroup(CollaborationGroup grp){

        FeedItem post = new FeedItem();
        post.Title = POST_TITLE_COLLABORATION_GROUP_CLOSED;
        post.Body = Label.CQ_UI_Feed_Collaboration_Group_Closed.replace('{GROUP_NAME}', grp.Name);
        post.ParentId = grp.Id;

        new SQX_DB().op_insert( new List<FeedItem> { post } , new List<sObjectField> {} );
    }

    /**
    *   This method is used to validate the current user on whether he/she can close the collaboration group
    *   @param groupId - id of the group which is to be closed
    *   @return <code>true</code> if the current user has sufficient permission to close the group, else <code>false</code>
    */
    private Boolean userCanCloseGroup(String groupId){

        return [ SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :groupId ].HasEditAccess;

    }

    /**
    *   Retrieves content from the group
    *   @param groupId - Id of the collaboration group whose content is to be retrieved
    *   @param contentId - Id of the content stored in the group which is to be retrieved
    */
    public ContentVersion retrieveGroupContent(String groupId, String contentId){
        ContentVersion cvs;

        Set<Id> allIds = new Set<Id>();
        List<CollaborationGroupFeed> feeds = [SELECT Id, RelatedRecordId FROM CollaborationGroupFeed WHERE ParentId = :groupId AND Type = 'ContentPost'];

        for(CollaborationGroupFeed feed: feeds){
            allIds.add(feed.RelatedRecordId);
        }
        if(allIds.size() > 0){
            List<ContentVersion> cvss = [SELECT Title, ContentDocumentId
                                                    FROM ContentVersion
                                                    WHERE ContentDocumentId =:contentId
                                                    AND Id IN :allIds ORDER BY VersionNumber DESC];
            if(cvss.size() > 0){
                cvs = cvss.get(0);
            }
            else{
                throw new SQX_ApplicationGenericException(Label.CQ_UI_Content_Missing_In_Group);
            }
        }
        return cvs;
    }

    /**
    *   Retrieves info(title, url) of content stored in the collaboration group
    *   @param groupId - Id of the group whose content's info is to be retrieved
    *   @param contentId - Id of the content stored in the group
    *   @return map of info (TITLE -> contentTitle , URL -> contentURL, ...)
    */
    public Map<String,String> retrieveGroupContentInfo(String groupId, String contentId){
        List<ContentVersion> cvs = [SELECT Title, PathOnClient FROM ContentVersion WHERE ContentDocumentId = :contentId AND IsLatest = true];
        Map<String, String> infoMap;
        if(cvs.size() > 0){
            infoMap = new Map<String, String>{
                KEY_COLLABORATION_CONTENT_TITLE => cvs.get(0).Title,
                KEY_COLLABORATION_CONTENT_FILENAME => cvs.get(0).PathOnClient,
                KEY_COLLABORATION_CONTENT_URL => '/' + contentId
            };
        }
        return infoMap;
    }

    /**
    *   Removes content from the group, Most likely to be used in unison with close collaboration
    *   @param groupId - Id of the collaboration group whose content is to be deleted
    *   @param contentId - Id of the content to be deleted
    */
    public void deleteGroupContent(String groupId, String contentId){
        // for future use
    }

    /**
    *   Adds members to collaboration group
    *   @param user - list of SF users to be added to the group
    *   @param grpId - id of the group to which the member is to be added
    */
    public void addMembers(List<User> users, String grpId){

        List<CollaborationGroupMember> memberList=new List<CollaborationGroupMember>();
        for(User user : users){
            CollaborationGroupMember memberObj=new CollaborationGroupMember();
            memberObj.MemberId = user.Id;
            memberObj.CollaborationGroupId = grpId;
            memberObj.CollaborationRole = COLLABORATION_MEMBER_ROLE_ADMIN; //collaborator should be able to view Add/Remove Members option
            memberList.add(memberObj);
        }
        new SQX_DB().op_insert(memberList, new List<Schema.SObjectField>{
                Schema.CollaborationGroupMember.MemberId,
                Schema.CollaborationGroupMember.CollaborationGroupId
        });
    }

    /**
    *   Removes members from the collaboration group    
    *   @param users - list of SF users to be removed from the group
    *   @param grpId - id of the group from which the member is to be removed
    */
    public void removeMembers(List<User> users, String grpId){
        // SF has a standard feature for removing members from groups
    }
}