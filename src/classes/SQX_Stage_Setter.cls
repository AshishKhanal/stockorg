/*
 * class to copy the specification details of the active inspection criteria to the inspection matching the association
 */
global with sharing class SQX_Stage_Setter {
    
    /*
     * method to set Record Stage
     * @param {objectId} list of Object to process
     */
    @InvocableMethod(Label='CQ Set Record\'s Stage')
    global static void setRecordStage(List<Id> objectId ){

        SObjectType objType = objectId[0].getSobjectType();

        if(objType == SQX_Audit__c.getSobjectType()){
            setAuditStage(objectId);
        }
        else{
            throw new SQX_ApplicationGenericException('SObjectType "' + objType + '" is not implemented to set stage of the record.');
        }
    
    }

    /*
     * method to set Audit Stage
     * @param {auditIds} list of Audit Id to process
     */
    public static void setAuditStage(List<Id> auditIds) {
        
        List<SQX_Audit__c> auditsToUpdate = computeAuditStage(auditIds);
        if (auditsToUpdate.size() > 0) {
            /**
            * Without sharing has been used:
            * ------------------------------
            * a. Auditee contact may not have edit permission on audit while submitting responses
            * b. User's completing/closing CAPA (escalted from audit finding) with effective resolution may not enough access on audit record
            */
            new SQX_DB().withoutSharing().op_update(auditsToUpdate, new List<SObjectField>{SQX_Audit__c.Stage__c});
        }
    }

    /**
    * method to compute Stage of Audits
    * @param {auditIds} list of Audit Id to compute stage
    * @return {auditsToUpdate} list of Audit whose stage is set
    */
    public static List<SQX_Audit__c> computeAuditStage(List<Id> auditIds){
        // Set Audit Stage as follows :
        // 1) If Any Response Inclusion In Approval contains Investigation or Finding with CAPA  = > Response Approval  
        // 2) Else If Response Inclusion In Approval contains Implementation => Implementation Response Approval 
        // 3) Else If Any Implemention Open = > Implement 
        // 4) Else If Any Finding Open = > Respond
        // 5) Else => Ready For Closure 
        
        Map<Id, String> auditsWithInApprovalStage = new Map<Id, String>();
        List<SQX_Audit__c> auditsToUpdate = new List<SQX_Audit__c>();
        AccessAllRecords accessAllRecs = new AccessAllRecords();

        // Get In Approval Response Inclusions for audits
        for (AggregateResult ar : accessAllRecs.getApprovalPendingAuditInclusions(auditIds)) {
            Id auditId = (Id)ar.get('auditId');
            String type = (String)ar.get('type');
            
            if (type == SQX_Response_Inclusion.INC_TYPE_FINDING || type == SQX_Response_Inclusion.INC_TYPE_INVESTIGATION) {
                // set response approval when any finding or investigation inclusion is approval pending
                auditsWithInApprovalStage.put(auditId, SQX_Audit.STAGE_RESPONSE_APPROVAL);
            }
            else if (!auditsWithInApprovalStage.containsKey(auditId)) {
                // set implementation response approval when no finding or investigation inclusion is approval pending but any other types are approval pending
                auditsWithInApprovalStage.put(auditId, SQX_Audit.STAGE_IMPLEMENTATION_RESPONSE_APPROVAL);
            }
        }
        
        // set audit stage
        for (SQX_Audit__c audit : accessAllRecs.getAuditsWithOpenChildRecords(auditIds)) {
            if (auditsWithInApprovalStage.containsKey(audit.Id)) {
                audit.Stage__c = auditsWithInApprovalStage.get(audit.Id);
            }
            // set implementation stage if audit has open actions
            else if (audit.SQX_Actions__r.size() > 0) {
                audit.Stage__c = SQX_Audit.STAGE_IMPLEMENT;
            }
            // set Respond stage if audit has open findings 
            else if (audit.SQX_Findings__r.size() > 0) {
                audit.Stage__c = SQX_Audit.STAGE_RESPOND;
            }
            else {
                audit.Stage__c = SQX_Audit.STAGE_READY_FOR_CLOSURE;
            }
            
            auditsToUpdate.add(audit);
        }
        return auditsToUpdate;
    }
    
    
    /**
    * this class is intended to contain any action that requires access to all records irrespective of sharing model.
    */
    private without sharing class AccessAllRecords {
        /**
        * default constructor
        */
        public AccessAllRecords() {
        }
        
        /**
        * this function returns the list of all In Approval response inclusions in the given audit
        * @param auditIds this parameter provides a List of Ids of Audit 
        * @return returns a list of in approval response inclusions
        */
        public List<AggregateResult> getApprovalPendingAuditInclusions(List<Id> auditIds) {
            return [SELECT 
                        SQX_Response__r.SQX_Audit_Response__r.SQX_Audit__c auditId,
                        Type__c type
                    FROM SQX_Response_Inclusion__c
                    WHERE SQX_Response__r.SQX_Audit_Response__r.SQX_Audit__c IN :auditIds
                        AND SQX_Response__r.SQX_Audit_Response__r.Approval_Status__c = :SQX_Audit_Response.APPROVAL_STATUS_PENDING
                    GROUP BY SQX_Response__r.SQX_Audit_Response__r.SQX_Audit__c,
                        Type__c
                    HAVING COUNT(Id) > 0];
        }
        
        /**
        * this function returns the list of audits with open Actions and open Findings or finding in Complete status with stage in Waiting For CAPA 
        * @param auditIds this parameter provides a List of Ids of Audit 
        * @return returns a list of audit with its child records 
        */
        public List<SQX_Audit__c> getAuditsWithOpenChildRecords(List<Id> auditIds) {
            return [SELECT 
                        Id,
                        Stage__c,
                        ( SELECT Id FROM SQX_Actions__r WHERE Status__c = :SQX_Implementation_Action.STATUS_OPEN OR Status__c = null LIMIT 1 ),
                        ( SELECT Id FROM SQX_Findings__r WHERE Status__c = :SQX_Finding.STATUS_OPEN OR (Status__c =: SQX_Finding.STATUS_COMPLETE AND Stage__c =: SQX_Finding.STAGE_WAITING_FOR_CAPA) LIMIT 1 )
                    FROM SQX_Audit__c 
                    WHERE Id IN :auditIds];
        }
    }
}