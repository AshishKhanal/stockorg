global class SQX_Integration_Scheduler  implements Schedulable{
    global void execute(SchedulableContext sc){
//        System.scheduleBatch(batchable, jobName, minutesFromNow, scopeSize)
        System.scheduleBatch(new SQX_NC_Staging_Processor(), 'CQ-Scheduled NC Integration', 1, 2);
    }
}