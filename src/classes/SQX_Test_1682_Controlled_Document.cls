/**
* Unit tests to ensure training duration and/or review interval valeus are copied from template when creating new controlled document
*/
@IsTest
public class SQX_Test_1682_Controlled_Document {

    /**
    * ensures training duration and/or review interval values are copied from template document when not set by user
    */
    public static testmethod void givenCreateDocWithTemplate_TrainingDurationOrReviewIntervalNotSet_CopiedFromTemplate() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required template document
            SQX_Test_Controlled_Document tDoc1 = new SQX_Test_Controlled_Document();
            tDoc1.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            tDoc1.doc.Duration__c = 123;
            tDoc1.doc.Review_Interval__c = 234;
            tDoc1.save(true);
            
            // ACT: create new controlled document with template document and without duration and interval values
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.doc.Document_Type__c = tDoc1.doc.Id;
            cDoc1.save(); // uses SQX_Extension_Controlled_Document extension
            
            // get saved values
            cDoc1.doc = [SELECT Id, Duration__c, Review_Interval__c FROM SQX_Controlled_Document__c WHERE ID = :cDoc1.doc.Id];
            
            System.assertEquals(tDoc1.doc.Duration__c, cDoc1.doc.Duration__c);
            System.assertEquals(tDoc1.doc.Review_Interval__c, cDoc1.doc.Review_Interval__c);
            
            
            // ACT: create new controlled document with template document and some duration value and without interval value
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document();
            cDoc2.doc.Document_Type__c = tDoc1.doc.Id;
            cDoc2.doc.Duration__c = 90;
            cDoc2.save(); // uses SQX_Extension_Controlled_Document extension
            
            // get saved values
            cDoc2.doc = [SELECT Id, Duration__c, Review_Interval__c FROM SQX_Controlled_Document__c WHERE ID = :cDoc2.doc.Id];
            
            System.assertEquals(90, cDoc2.doc.Duration__c);
            System.assertEquals(tDoc1.doc.Review_Interval__c, cDoc2.doc.Review_Interval__c);
            
            
            // ACT: create new controlled document with template document and some interval value and without duration value
            SQX_Test_Controlled_Document cDoc3 = new SQX_Test_Controlled_Document();
            cDoc3.doc.Document_Type__c = tDoc1.doc.Id;
            cDoc3.doc.Review_Interval__c = 80;
            cDoc3.save(); // uses SQX_Extension_Controlled_Document extension
            
            // get saved values
            cDoc3.doc = [SELECT Id, Duration__c, Review_Interval__c FROM SQX_Controlled_Document__c WHERE ID = :cDoc3.doc.Id];
            
            System.assertEquals(tDoc1.doc.Duration__c, cDoc3.doc.Duration__c);
            System.assertEquals(80, cDoc3.doc.Review_Interval__c);
            
            
            // ACT: create new controlled document with template document and some duration and interval values
            SQX_Test_Controlled_Document cDoc4 = new SQX_Test_Controlled_Document();
            cDoc4.doc.Document_Type__c = tDoc1.doc.Id;
            cDoc4.doc.Duration__c = 90;
            cDoc4.doc.Review_Interval__c = 80;
            cDoc4.save(); // uses SQX_Extension_Controlled_Document extension
            
            // get saved values
            cDoc4.doc = [SELECT Id, Duration__c, Review_Interval__c FROM SQX_Controlled_Document__c WHERE ID = :cDoc4.doc.Id];
            
            System.assertEquals(90, cDoc4.doc.Duration__c);
            System.assertEquals(80, cDoc4.doc.Review_Interval__c);
        }
    }
}