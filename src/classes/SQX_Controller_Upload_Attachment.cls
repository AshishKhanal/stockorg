/**
* Controller to upload attachments for objects in salesforce
* @author Pradhanta Bhandari
* @date 2014/4/18
*/
public with sharing class SQX_Controller_Upload_Attachment{

    public static final string PARAM_PARENT_ID  = 'parentId';
    public static final string PARAM_UUID = 'uid';
    public static final string PARAM_ATTACH_TO_PARENT = 'attachToParent';
    public static final string PARAM_RECORD_ID = 'recordid';
    
    public SQX_Controller_Upload_Attachment(){
        string updateAttachmentWithID = ApexPages.currentPage().getParameters().get(PARAM_RECORD_ID );
        
        if(i_attachment == null){
            i_attachment = new Attachment(); //always initialize if nothing
        }
        
        if(updateAttachmentWithID != null){
            try{
                Id attachmentId = Id.valueOf(updateAttachmentWithID);
                Attachment attachment = [SELECT ID,ParentId, Name, Description FROM Attachment WHERE ID = : attachmentId ];
                
                i_attachment.Name = attachment.Name;
                i_attachment.Description = attachment.Description;
                //i_attachment.ParentId = attachment.parentId;
                i_attachment.Id = attachment.Id;
                
                lastUploadFileName = attachment.Name;
                lastUploadFileSize = 100; //any number would do
            }
            catch(Exception ex){
                //ignore any exception
            }
            
        }

    }


    //main attachment object
    private Attachment i_attachment;
    public Attachment attachment{
        get {
            
            return i_attachment;
        }
        set {
            i_attachment = value;
        }
    }

    //last uploaded file name stored
    public String lastUploadFileName{
        get;
        set;
    }

    //last uploaded file size
    public Integer lastUploadFileSize{
        get;
        set;
    }

    //store the error if any, transient because don't want it to persist more than one session
    private transient string internalError;

    public String ErrorMessage{
        get { return internalError; }
    }

    //the attachment Id
    private transient string internalAttachmentID;

    public String AttachmentId{
        get { return internalAttachmentID; }
    }

    //returns a JSON representing the last uploaded file
    public String getLastUpload(){
        if(lastUploadFileName == null)
            return '[]';
            
            
        List<Map<String, Object>> files = new List<Map<String, Object>>();
        Map<String, Object> retVal = new Map<String, Object>();
        
        retVal.put('name', 'Random.doc');
        retVal.put('size', 300);
        retVal.put('extension', '.doc');
        
        files.add(retVal); 
        
        return JSON.serialize(files);//'[{ name: "' + (lastUploadFileName) + '", size: ' + lastUploadFileSize + '}]';
    }


    /**
* action to upload the attachment for the parent id
*              Accepts following get parameters
*              'parentId' -> the Id of the parent Object against which the attachment is to be related
*              'uid' -> the unique id to represent this attachments parent, used when attachment has not been stored (temp objects)
*              'attachToParent' -> if  this flag is set attaches directly to the parentId, else stores as temp attachment
* @author Pradhanta Bhandari
* @date 2014/4/18 -refactored to add attach to parent
*/
    public PageReference upload() {

        String parentID = ApexPages.currentPage().getParameters().get(PARAM_PARENT_ID);
        String uniqueID = ApexPages.currentPage().getParameters().get(PARAM_UUID );
        String attachToParent = ApexPages.currentPage().getParameters().get(PARAM_ATTACH_TO_PARENT);  
        
        try {
            //check if parent is accessible
            if(attachment.Id == null && attachment.Body == null){
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_ATTACHMENT_FILE_NOT_UPLOADED);
            }
              
            if(attachToParent != null){
                Id parsedID = Id.valueOf(parentID);
                Schema.DescribeSObjectResult objectDesc = parsedID.getSObjectType().getDescribe();
                if(!objectDesc.isQueryable() || !objectDesc.isAccessible()){
                    throw new SQX_ApplicationGenericException(objectDesc.getName() + ' is not accessible ');
                }
                String dynamicSOQL = 'SELECT ID FROM ' + objectDesc.getName() + ' WHERE ID = :parsedID ';
                
                SObject parentObject = Database.query(dynamicSOQL);
                
                        
         
                //if attachToParent is set and id is null meaning its a new attachment
                SQX_DB.DBOperation operation = SQX_DB.DBOperation.Modify;
                List<SObjectField> fieldsToSet = new List<SObjectField>{ Schema.Attachment.fields.Name, Schema.Attachment.fields.Body };

                if(attachment.Id == null){
                    attachment.ParentId = parsedId;
                    operation = SQX_DB.DBOperation.Create;
                    fieldsToSet.add(Schema.Attachment.fields.ParentId);
                }

                    
                SQX_DB db = new SQX_DB();

                if(db.isCUDCompliant(attachment.getSObjectType(), operation) 
                    && db.IsFLSCompliant(fieldsToSet, operation) ){

                    Database.upsert(attachment);
                }
            }
            else{
                //else store as temporary attachment
                if(attachment.Id == null){
                    SQX_TempStore.storeTemporaryAttachment(parentID, uniqueID, attachment);
                }
                else{
                    new SQX_DB().op_update(new List<Attachment>{attachment}, new List<SObjectField>{ Schema.Attachment.fields.Name, Schema.Attachment.fields.Body });
                }
            }
            
            internalAttachmentID = attachment.ID;
            lastUploadFileName = attachment.Name;
            lastUploadFileSize = attachment.Body == null ? 0 : attachment.Body.size();
            
        }
        catch (Exception e) {
             internalError = '{message: "' + Label.SQX_ERR_MSG_ERR_UPLOADING_FILE  +'", internalError: "' + e.getMessage() + '"}'; 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, internalError)); 
        }
        finally {
            
            i_attachment.body = null; // clears the viewstate
        }
        
        return null;

    }



}