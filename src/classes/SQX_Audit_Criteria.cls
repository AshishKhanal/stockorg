/**
* removed logic for obsolete document training object
*/
public with sharing class SQX_Audit_Criteria{
    // we need to remove these while doing cleanup for obsolete Audit Criteria object
    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_IN_APPROVAL = 'In Approval';
    public static final String STATUS_ACTIVE = 'Active';
    public static final String STATUS_REJECTED = 'Rejected';
    public static final String STATUS_OBSOLETED = 'Obsolete';

}