/**
* Extension class for Controlled Document homepage component for items to approve layout 
* 
* @author Shailesh Maharjan
* @date 2015/08/10
* 
*/
public with sharing class SQX_Controller_Approval_Component {
    
    public List<ItemsToApprove> itemsToApprove { get; set; }
    

    /**
    * List of items that are displayed in the approval component
    */
    final List<String> fetchApprovalsOf = new List<String> { SQX.ControlledDoc, SQX.Audit, SQX.AuditReport, SQX.AuditCriteria, SQX.AuditProgram, SQX.ChangeOrder, SQX.Complaint};

    /**
    * The approval component initializer
    */
    public SQX_Controller_Approval_Component() {
        Set<ID> queue_current_user_IDs= SQX_Utilities.getQueueIdsForUser(UserInfo.getUserId());
            
        queue_current_user_IDs.add(UserInfo.getUserId());

        itemsToApprove = new List<ItemsToApprove>();
    
        //select the list of work items of types that are to be displayed for the current user.
        List<ProcessInstanceWorkItem> workItems = 
            [SELECT Id, CreatedDate, processinstance.targetobjectid, processinstance.targetobject.name, processinstance.targetobject.Type,
            processinstance.LastActorId, processinstance.SubmittedById, ActorId
            FROM processinstanceworkitem
            WHERE processinstance.targetobject.Type IN : fetchApprovalsOf
            AND ActorId in : queue_current_user_IDs
            ORDER BY CreatedDate DESC];
            
        //add it to the list of items to approve
        if(workItems.size() > 0){
            for(ProcessInstanceWorkItem wi : workItems){
                ItemsToApprove approval = new ItemsToApprove(wi, null);
                
                itemsToApprove.add(approval);
            }
        
        }
    }


    //used to cache the name of the object for the sObjectType. Used to reduce describe calls, eventhough limits on describe have been removed.
    static Map<SObjectType, String> objectToName = new Map<SObjectType, String>();

    /**
    * Wrapper class used to provide the workitem and its redirect url
    */
    public with sharing class ItemsToApprove {
        //the wrapped work item
        public ProcessInstanceWorkItem workItem {get; set;}

        public SObject relatedItem {get; set;}
        public String  objectType {get; set;}
        public SObjectType targetType;
        private Id targetObjectId, workitemId;



        public ItemsToApprove(){

        }

        public ItemsToApprove(ProcessInstanceWorkItem workItem, SObject relatedItem){
            this.workitemId = workItem.Id;
            this.relatedItem = relatedItem;
			this.workItem = workItem;

            this.targetObjectId = workItem.processInstance.TargetObjectId;
            this.targetType = this.targetObjectId.getSObjectType();
            if(objectToName.containsKey(this.targetType)){
                objectType = objectToName.get(this.targetType);
            }
            else{
                objectType = this.targetType.getDescribe().getLabel();
                objectToName.put(this.targetType, objectType);
            }
        }

        /**
        * The redirect url for a particular type.
        */
        public String getApprovalURL(){
            String approvalUrl;
            PageReference approvalRef;


            if(targetType == SQX_Controlled_Document__c.getSObjectType()){
                approvalRef =  new PageReference(SQX_Utilities.getPageName('', 'SQX_Controlled_Document_Approval'));
                approvalRef.getParameters().put('id', targetObjectId);
                approvalRef.getParameters().put('reqId', workitemId);
                approvalRef.getParameters().put('retURL', SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));
            }
            else if(targetType == SQX_Audit__c.getSObjectType()){
                approvalRef = Page.SQX_Audit;
                approvalRef.getParameters().put('id', targetObjectId);

                approvalUrl = approvalRef.getUrl() + '#sqx_audit_approval'; //add fragment identifier to display the correct div
            }
            else if(targetType == SQX_Audit_Report__c.getSObjectType()){
                approvalRef = Page.SQX_Audit_Report;
                approvalRef.getParameters().put('id', targetObjectId);
            }
            else if(targetType == SQX_Change_Order__c.getSObjectType()){
                approvalRef = Page.SQX_Change;
                approvalRef.getParameters().put('id', targetObjectId);
                approvalRef.getParameters().put('initialTab', 'actionTab');
            }
            else if(targetType == SQX_Complaint__c.getSObjectType()){
                approvalRef = Page.SQX_Complaint_Main;
                approvalRef.getParameters().put('id', targetObjectId);
                approvalRef.getParameters().put('initialTab', 'workflowTab');
            }
            else if(targetType == SQX_Audit_Program__c.getSObjectType()){
                approvalRef = new ApexPages.StandardController(new SQX_Audit_Program__c(Id = targetObjectId)).view();             
            }
            else{
                //redirect to default approval screen if no override has been specified
                approvalRef = new PageReference('/p/process/ProcessInstanceWorkitemWizardStageManager');
                approvalRef.getParameters().put('id', workitemId);

            }

            
            approvalUrl =  approvalUrl == null ? approvalRef.getUrl() : approvalUrl;

            return approvalUrl;
        }
    }

}