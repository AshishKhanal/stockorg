/**
* Extension for medwatch container
*/
global with sharing class SQX_Extension_Medwatch {
    global SQX_Medwatch_Null_Value__c NullValues {get; set;}
    global SQX_MedWatch_Suspect_Product__c[] Suspects {get; set;}
    global SQX_MedWatch_Suspect_Product__c[] toDelete {get; set;}
    global SQX_Medwatch_D11_Medical_Product__c[] D11MedicalProducts {get; set;}
    global SQX_Medwatch_D11_Medical_Product__c[] toDeleteD11MedicalProducts {get; set;}
    global SQX_Medwatch_F10_Device_Code__c[] F10DeviceCodes {get; set;}
    global SQX_Medwatch_F10_Device_Code__c[] toDeleteF10DeviceCodes {get; set;}
    global SQX_Medwatch_F10_Patient_Code__c[] F10PatientCodes {get; set;}
    global SQX_Medwatch_F10_Patient_Code__c[] toDeleteF10PatientCodes {get; set;}
    global SQX_Medwatch_H6_Device_Code__c[] H6DeviceCodes {get; set;}
    global SQX_Medwatch_H6_Device_Code__c[] toDeleteH6DeviceCodes {get; set;}
    global SQX_Medwatch_H6_Patient_Code__c[] H6PatientCodes {get; set;}
    global SQX_Medwatch_H6_Patient_Code__c[] toDeleteH6PatientCodes {get; set;}
    global SQX_Medwatch_H6_Method__c[] H6MethodCodes {get; set;}
    global SQX_Medwatch_H6_Method__c[] toDeleteH6MethodCodes {get; set;}
    global SQX_Medwatch_H6_Result__c[] H6ResultCodes {get; set;}
    global SQX_Medwatch_H6_Result__c[] toDeleteH6ResultCodes {get; set;}
    global SQX_Medwatch_H6_Conclusion__c[] H6ConclusionCodes {get; set;}
    global SQX_Medwatch_H6_Conclusion__c[] toDeleteH6ConclusionCodes {get; set;}
    global SQX_MedWatch__c Medwatch {get; set;}

    private ApexPages.StandardController controller;

    /**
     *	Validation error message (holds list of error messages delimited by ';;;' )
    */
    global String message { get; set; }

    global SelectOption[] NullOptions {get; set;}
    /**
     * Constructor for VF Page
     * This constructor will retrieve/create null value record for Medwatch record.
     * Retrieves/Add all the related child object's records (eg: C SUSPECT PRODUCT(S), F10 Event Problem Codes, H6 Event Problem and Evaluation Codes, etc )
     */
    global SQX_Extension_Medwatch(ApexPages.StandardController controller) {
        this.controller = controller;
        Medwatch = (SQX_MedWatch__c) controller.getRecord();
        if(Medwatch.SQX_Medwatch_Null_Value__c == null ) {
            NullValues = new SQX_Medwatch_Null_Value__c();
        } else {
            NullValues = (SQX_Medwatch_Null_Value__c) SQX_DynamicQuery.getSObjectWithId(Medwatch.SQX_Medwatch_Null_Value__c);
        }

        NullOptions = new List<SelectOption>();
        for(Schema.PicklistEntry entry : SQX_Medwatch_Null_Value__c.A2_Age__c.getDescribe().getPicklistValues()){
            NullOptions.add(new SelectOption(entry.getValue(), entry.getLabel()));
        }

        if(Medwatch.Id != null) {
            reloadSuspects(Medwatch.Id);
            reloadD11MedicalProducts(Medwatch.Id);
            reloadF10DeviceCodes(Medwatch.Id);
            reloadF10PatientCodes(Medwatch.Id);
            reloadH6DeviceCodes(Medwatch.Id);
            reloadH6PatientCodes(Medwatch.Id);
            reloadH6MethodCodes(Medwatch.Id);
            reloadH6ResultCodes(Medwatch.Id);
            reloadH6ConclusionCodes(Medwatch.Id);

        }

        // add one Suspect Product item by default
        if(Suspects == null || Suspects.isEmpty()) {
            Suspects = new List<SQX_MedWatch_Suspect_Product__c>();
            AddSuspect();
        }

        toDelete = new List<SQX_MedWatch_Suspect_Product__c>();
        
        // add one D11 Concomitant Medical Product and Therapy Date item by default
        if(D11MedicalProducts == null || D11MedicalProducts.isEmpty()) {
            D11MedicalProducts = new List<SQX_Medwatch_D11_Medical_Product__c>();
            AddD11MedicalProducts();
        }

        toDeleteD11MedicalProducts = new List<SQX_Medwatch_D11_Medical_Product__c>();

        // add one F10 Device Code by default
        if(F10DeviceCodes == null || F10DeviceCodes.isEmpty()){
            F10DeviceCodes = new List<SQX_Medwatch_F10_Device_Code__c>();
            AddF10DeviceCodes();
        }
        toDeleteF10DeviceCodes = new List<SQX_Medwatch_F10_Device_Code__c>();
        
        // add one F10 Device Code by default
        if(F10PatientCodes == null || F10PatientCodes.isEmpty()){
            F10PatientCodes = new List<SQX_Medwatch_F10_Patient_Code__c>();
            AddF10PatientCodes();
        }
        toDeleteF10PatientCodes = new List<SQX_Medwatch_F10_Patient_Code__c>();

        // add one H6 Device Code by default
        if(H6DeviceCodes == null || H6DeviceCodes.isEmpty()){
            H6DeviceCodes = new List<SQX_Medwatch_H6_Device_Code__c>();
            AddH6DeviceCodes();
        }
        toDeleteH6DeviceCodes = new List<SQX_Medwatch_H6_Device_Code__c>();        
        
        // add one H6 Patient Code by default
        if(H6PatientCodes == null || H6PatientCodes.isEmpty()){
            H6PatientCodes = new List<SQX_Medwatch_H6_Patient_Code__c>();
            AddH6PatientCodes();
        }
        toDeleteH6PatientCodes = new List<SQX_Medwatch_H6_Patient_Code__c>();
                
        // add one H6 Method Code by default
        if(H6MethodCodes == null || H6MethodCodes.isEmpty()){
            H6MethodCodes = new List<SQX_Medwatch_H6_Method__c>();
            AddH6MethodCodes();
        }
        toDeleteH6MethodCodes = new List<SQX_Medwatch_H6_Method__c>();
        
        // add one H6 Result Code by default
        if(H6ResultCodes == null || H6ResultCodes.isEmpty()){
            H6ResultCodes = new List<SQX_Medwatch_H6_Result__c>();
            AddH6ResultCodes();
        }
        toDeleteH6ResultCodes = new List<SQX_Medwatch_H6_Result__c>();
        
        // add one H6 Result Code by default
        if(H6ConclusionCodes == null || H6ConclusionCodes.isEmpty()){
            H6ConclusionCodes = new List<SQX_Medwatch_H6_Conclusion__c>();
            AddH6ConclusionCodes();
        }
        toDeleteH6ConclusionCodes = new List<SQX_Medwatch_H6_Conclusion__c>();
    
    }
	/**
	 * This method retrieves the Suspect Products and its null values related to Medwatch record
	 */ 
    private void reloadSuspects(Id medwatchId) {
        Suspects = [
            SELECT Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, SQX_Medwatch__c, 
            	C1_Drug_Type__c, C1_If_IND_Give_Protocol_Number__c, C1_Approval_Number__c,
                C1_NDC_Number_Unique_Id__c, C1_Lot_Number__c, C3_Dose__c, C3_Number_of_separate_dosages__c, C3_Frequency__c, C3_Frequency_Unit__c, C3_Route_Used__c,
                C4_Therapy_Start_Date__c, C4_Therapy_Stop_Date__c, C5_Diagnosis_For_Use__c, C9_Event_Abated__c, C6_Is_the_Product_Compounded__c, C7_Is_the_Product_Over_the_Counter__c,
                C10_Event_Reappeared__c, C8_Expiration_Date__c, C1_Name_and_Strength__c,
                C1_Manufacturer_or_Compounder__c, C3_Dose_Unit__c, SQX_Suspect_Product_Null_Value__c,
                SQX_Suspect_Product_Null_Value__r.C1_Drug_Type__c, SQX_Suspect_Product_Null_Value__r.C1_Approval_Number__c, C2_Concomitant_Medical_Products__c,
                SQX_Suspect_Product_Null_Value__r.C1_If_IND_Give_Protocol_Number__c, 
                SQX_Suspect_Product_Null_Value__r.C10_Event_Reappeared__c, SQX_Suspect_Product_Null_Value__r.C1_Lot_Number__c, 
                SQX_Suspect_Product_Null_Value__r.C1_Manufacturer_or_Compounder__c, SQX_Suspect_Product_Null_Value__r.C1_NDC_Number_Unique_Id__c,
                SQX_Suspect_Product_Null_Value__r.C1_Name_and_Strength__c, SQX_Suspect_Product_Null_Value__r.C2_Therapy_Date__c,
                SQX_Suspect_Product_Null_Value__r.C3_Dose_Unit__c, SQX_Suspect_Product_Null_Value__r.C3_Dose__c, SQX_Suspect_Product_Null_Value__r.C3_Number_of_separate_dosages__c, 
            	SQX_Suspect_Product_Null_Value__r.C3_Frequency_Unit__c, SQX_Suspect_Product_Null_Value__r.C3_Frequency__c, SQX_Suspect_Product_Null_Value__r.C3_Route_Used__c,                
            	SQX_Suspect_Product_Null_Value__r.C4_Therapy_Start_Date__c, SQX_Suspect_Product_Null_Value__r.C4_Therapy_Stop_Date__c,
                SQX_Suspect_Product_Null_Value__r.C5_Diagnosis_For_Use__c, SQX_Suspect_Product_Null_Value__r.C6_Is_the_Product_Compounded__c, SQX_Suspect_Product_Null_Value__r.C7_Is_the_Product_Over_the_Counter__c,
                SQX_Suspect_Product_Null_Value__r.C8_Expiration_Date__c, SQX_Suspect_Product_Null_Value__r.C9_Event_Abated__c
            FROM SQX_Medwatch_Suspect_Product__c
            WHERE SQX_MedWatch__c =: medwatchId
        ];
    }
	/**
	 * Method to add a new Suspect Product
	 */ 
    global void AddSuspect() {
        SQX_Medwatch_Suspect_Product_Null_Value__c suspectNull = new SQX_Medwatch_Suspect_Product_Null_Value__c();
        Suspects.add(new SQX_MedWatch_Suspect_Product__c(SQX_Suspect_Product_Null_Value__r = suspectNull));
    }
	/**
	 * Method to remove Suspect Product
	 */ 
    global void RemoveSuspect() {
        if(!Suspects.isEmpty()) {
            SQX_MedWatch_Suspect_Product__c suspect = Suspects.remove(Suspects.size() - 1);

            if(suspect.Id != null) {
                toDelete.add(suspect);
            }
        }
    }

    /**
     * This method retrieves all the D11 Medical Products and Therapy Dates related to Medwatch record
     */
    private void reloadD11MedicalProducts(Id medwatchId) {
        
        D11MedicalProducts = [
            SELECT Id, Name, D11_Medical_Product__c, D11_Therapy_Date__c, SQX_D11_Medical_Product_Null_Value__r.D11_Medical_Product__c, SQX_D11_Medical_Product_Null_Value__r.D11_Therapy_Date__c
            FROM SQX_Medwatch_D11_Medical_Product__c
            WHERE SQX_MedWatch__c =: medwatchId            
        ];                                                       
    }
    /**
     * Method to add D11 Medical Products and Therapy Dates
     */ 
    global void AddD11MedicalProducts() {
        SQX_Medwatch_D11_Medical_Prod_Null_Value__c d11medicalProdNull = new SQX_Medwatch_D11_Medical_Prod_Null_Value__c();
        D11MedicalProducts.add(new SQX_Medwatch_D11_Medical_Product__c(SQX_D11_Medical_Product_Null_Value__r = d11medicalProdNull));
    }
	/**
	 * Method to remove D11 Medical Products and Therapy Dates
	 */ 
    global void RemoveD11MedicalProducts() {
        if(!D11MedicalProducts.isEmpty()) {
            SQX_Medwatch_D11_Medical_Product__c d11MedicalProduct = D11MedicalProducts.remove(D11MedicalProducts.size() - 1);

            if(d11MedicalProduct.Id != null) {
                toDeleteD11MedicalProducts.add(d11MedicalProduct);
            }
        }
    }
    
    /**
     * This method reloads all the F10 Device Codes related to current medwatch record
     */
    private void reloadF10DeviceCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_F10_Device_Code__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        F10DeviceCodes = (List<SQX_Medwatch_F10_Device_Code__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_F10_Device_Code__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_F10_Device_Code__c.Name }, true);
    }
    /**
     * Method to add new F10 Device Codes
     */ 
    global void AddF10DeviceCodes() {
        F10DeviceCodes.add(new SQX_Medwatch_F10_Device_Code__c());
    }
    /**
     * Method to remove F10 Device Codes related to medwatch record
     */ 
    global void RemoveF10DeviceCodes() {
        if(!F10DeviceCodes.isEmpty()) {
            SQX_Medwatch_F10_Device_Code__c f10DeviceCodes = F10DeviceCodes.remove(F10DeviceCodes.size() - 1);

            if(f10DeviceCodes.Id != null) {
                toDeleteF10DeviceCodes.add(f10DeviceCodes);
            }
        }
    }

    
   /**
    * This method reloads all the F10 Patient Code related to Medwatch record
    */  
    private void reloadF10PatientCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_F10_Patient_Code__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        F10PatientCodes = (List<SQX_Medwatch_F10_Patient_Code__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_F10_Patient_Code__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_F10_Patient_Code__c.Name }, true);
    }
    /**
     * Method to add new F10 Patient Code
     */ 
    global void AddF10PatientCodes() {
        F10PatientCodes.add(new SQX_Medwatch_F10_Patient_Code__c());
    }
    /**
     * Method to remove F10 Patient Code
     */ 
    global void RemoveF10PatientCodes() {
        if(!F10PatientCodes.isEmpty()) {
            SQX_Medwatch_F10_Patient_Code__c f10PatientCodes = F10PatientCodes.remove(F10PatientCodes.size() - 1);

            if(f10PatientCodes.Id != null) {
                toDeleteF10PatientCodes.add(f10PatientCodes);
            }
        }
    }
    
    /**
     * This method reloads all the H6 Device Codes related to Medwatch record
     */
    private void reloadH6DeviceCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_H6_Device_Code__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        H6DeviceCodes = (List<SQX_Medwatch_H6_Device_Code__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_H6_Device_Code__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_H6_Device_Code__c.Name }, true);
    }
    /**
     * Method to add new H6 Device Code
     */ 
    global void AddH6DeviceCodes() {
        H6DeviceCodes.add(new SQX_Medwatch_H6_Device_Code__c());
    }
    /**
     * Method to remove H6 Device Code
     */ 
    global void RemoveH6DeviceCodes() {
        if(!H6DeviceCodes.isEmpty()) {
            SQX_Medwatch_H6_Device_Code__c h6DeviceCodes = H6DeviceCodes.remove(H6DeviceCodes.size() - 1);

            if(h6DeviceCodes.Id != null) {
                toDeleteH6DeviceCodes.add(h6DeviceCodes);
            }
        }
    }
    
    /**
     * This method retrieves the H6 Patient Code related to Medwatch record
     */ 
    private void reloadH6PatientCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_H6_Patient_Code__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        H6PatientCodes = (List<SQX_Medwatch_H6_Patient_Code__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_H6_Patient_Code__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_H6_Patient_Code__c.Name }, true);
    }
    /**
     * Method to add new H6 Patient Code
     */ 
    global void AddH6PatientCodes() {
        H6PatientCodes.add(new SQX_Medwatch_H6_Patient_Code__c());
    }
    /**
     * Method to remove H6 Patient Code
     */ 
    global void RemoveH6PatientCodes() {
        if(!H6PatientCodes.isEmpty()) {
            SQX_Medwatch_H6_Patient_Code__c h6PatientCodes = H6PatientCodes.remove(H6PatientCodes.size() - 1);

            if(h6PatientCodes.Id != null) {
                toDeleteH6PatientCodes.add(h6PatientCodes);
            }
        }
    }
    
    /**
     * This method retrieves H6 Method Code related to Medwatch record
     */
    private void reloadH6MethodCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_H6_Method__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        H6MethodCodes = (List<SQX_Medwatch_H6_Method__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_H6_Method__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_H6_Method__c.Name }, true);
    }
    /**
     * Method to add a new H6 Method Code
     */ 
    global void AddH6MethodCodes() {
        H6MethodCodes.add(new SQX_Medwatch_H6_Method__c());
    }
    /**
     * Method to remove H6 Method Code
     */ 
    global void RemoveH6MethodCodes() {
        if(!H6MethodCodes.isEmpty()) {
            SQX_Medwatch_H6_Method__c h6MethodCodes = H6MethodCodes.remove(H6MethodCodes.size() - 1);

            if(h6MethodCodes.Id != null) {
                toDeleteH6MethodCodes.add(h6MethodCodes);
            }
        }
    }
    
    /**
     * This method retrieves H6 Result Code related to medwatch record
     */    
    private void reloadH6ResultCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_H6_Result__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        H6ResultCodes = (List<SQX_Medwatch_H6_Result__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_H6_Result__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_H6_Result__c.Name }, true);
    }
    /**
     * Method to add new H6 Result Code
     */ 
    global void AddH6ResultCodes() {
        H6ResultCodes.add(new SQX_Medwatch_H6_Result__c());
    }
    /**
     * Method to remove H6 Result Code
     */ 
    global void RemoveH6ResultCodes() {
        if(!H6ResultCodes.isEmpty()) {
            SQX_Medwatch_H6_Result__c h6ResultCodes = H6ResultCodes.remove(H6ResultCodes.size() - 1);

            if(h6ResultCodes.Id != null) {
                toDeleteH6ResultCodes.add(h6ResultCodes);
            }
        }
    }
    
    /**
     * This method retrieves H6 Conclusion Codes related to Medwatch record
     */
    private void reloadH6ConclusionCodes(Id medwatchId) {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.value = medwatchId;
        filter.field = SQX_Medwatch_H6_Conclusion__c.SQX_MedWatch__c.getDescribe().getName();
        filter.operator = 'eq';
        H6ConclusionCodes = (List<SQX_Medwatch_H6_Conclusion__c>)SQX_DynamicQuery.getallFields(SQX_Medwatch_H6_Conclusion__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter,
                                                                                                     new List<SObjectField> { SQX_Medwatch_H6_Conclusion__c.Name }, true);
    }
    /**
     * Method to add H6 Conclusion Codes
     */ 
    global void AddH6ConclusionCodes() {
        H6ConclusionCodes.add(new SQX_Medwatch_H6_Conclusion__c());
    }
	/**
	 * Method to remove H6 Conclusion codes
	 */ 
    global void RemoveH6ConclusionCodes() {
        if(!H6ResultCodes.isEmpty()) {
            SQX_Medwatch_H6_Conclusion__c h6ConclusionCodes = H6ConclusionCodes.remove(H6ConclusionCodes.size() - 1);

            if(h6ConclusionCodes.Id != null) {
                toDeleteH6ConclusionCodes.add(h6ConclusionCodes);
            }
        }
    }         
   
    /**
     * This method creates/updates/deletes Medwatch record along with its related child object's record
     */ 
    global PageReference Save() {
        Medwatch.F8_Date_of_This_Report__c = Medwatch.F11_Date_Report_Sent_To_FDA__c != null ? Medwatch.F11_Date_Report_Sent_To_FDA__c:Medwatch.F13_Date_Sent_to_Manufacturer__c; 
        SavePoint sp = Database.setSavepoint();
        PageReference returnTo = null;
        try {
            returnTo = controller.save();
            Id mainRecordId = controller.getId();
            Medwatch.Id = mainRecordId;

            if(NullValues.Id == null) {
                // VF's input field will auto enforce FLS hence we are not checking it here.
                new SQX_DB().op_insert(new SQX_Medwatch_Null_Value__c[] { NullValues},
                                       new List<SObjectField> {});
            } else {
                new SQX_DB().op_update(new SQX_Medwatch_Null_Value__c[] { NullValues},
                                       new List<SObjectField> {});
            }
            if(MedWatch.SQX_Medwatch_Null_Value__c == null) {
                new SQX_DB().op_update(
                    new Map<SObjectType, List<SObject>> {
                        SQX_Medwatch__c.SObjectType => new SQX_MedWatch__c[] { new SQX_MedWatch__c(Id = mainRecordId, SQX_Medwatch_Null_Value__c = NullValues.Id) },
                        SQX_Medwatch_Null_Value__c.SObjectType => new SQX_Medwatch_Null_Value__c[] { new SQX_Medwatch_Null_Value__c(Id = NullValues.Id, SQX_Medwatch__c = mainRecordId) }
                    },                   
                    new Map<SObjectType, List<SObjectField>> {
                        SQX_Medwatch__c.SObjectType => new List<SObjectField> { SQX_MedWatch__c.SQX_Medwatch_Null_Value__c },
                        SQX_Medwatch_Null_Value__c.SObjectType => new List<SObjectField> { SQX_Medwatch_Null_Value__c.SQX_MedWatch__c }
                    }
                );
            }
            
            // Suspect Product null value records to be created or updated
            List<SQX_Medwatch_Suspect_Product_Null_Value__c> toInsertSuspNull = new List<SQX_Medwatch_Suspect_Product_Null_Value__c>();
            List<SQX_Medwatch_Suspect_Product_Null_Value__c> toUpdateSuspNull = new List<SQX_Medwatch_Suspect_Product_Null_Value__c>();

            for(SQX_MedWatch_Suspect_Product__c suspect:Suspects){
                if(suspect.SQX_Suspect_Product_Null_Value__r.Id == null){
                    toInsertSuspNull.add(suspect.SQX_Suspect_Product_Null_Value__r);
                }else{
                    toUpdateSuspNull.add(suspect.SQX_Suspect_Product_Null_Value__r);
                }
            }
            // create/update Suspect Product null value records
            new SQX_DB().op_insert(toInsertSuspNull, new List<SObjectField> {});
            new SQX_DB().op_update( toUpdateSuspNull, new List<SObjectField> {});
            
            // Suspect Product to be created/updated
            List<SQX_MedWatch_Suspect_Product__c> toInsert = new List<SQX_MedWatch_Suspect_Product__c>();
            List<SQX_MedWatch_Suspect_Product__c> toUpdate = new List<SQX_MedWatch_Suspect_Product__c>();

            for(SQX_MedWatch_Suspect_Product__c suspect: Suspects) {
                if(suspect.Id == null) {
                    suspect.SQX_MedWatch__c = mainRecordId;
                    suspect.SQX_Suspect_Product_Null_Value__c = suspect.SQX_Suspect_Product_Null_Value__r.Id;
                    toInsert.add(suspect);
                } else {
                    toUpdate.add(suspect);
                }
            }
            
            // D11 Concomitant Medical Product and Therapy Date null value records to be created or updated
            List<SQX_Medwatch_D11_Medical_Prod_Null_Value__c> toInsertD11Null = new List<SQX_Medwatch_D11_Medical_Prod_Null_Value__c>();
            List<SQX_Medwatch_D11_Medical_Prod_Null_Value__c> toUpdateD11Null = new List<SQX_Medwatch_D11_Medical_Prod_Null_Value__c>();

            for(SQX_Medwatch_D11_Medical_Product__c d11MedicalProd:D11MedicalProducts){
                if(d11MedicalProd.SQX_D11_Medical_Product_Null_Value__r.Id == null){
                    toInsertD11Null.add(d11MedicalProd.SQX_D11_Medical_Product_Null_Value__r);
                }else{
                    toUpdateD11Null.add(d11MedicalProd.SQX_D11_Medical_Product_Null_Value__r);
                }
            }
            // create/update Suspect Product null value records
            new SQX_DB().op_insert(toInsertD11Null, new List<SObjectField> {});
            new SQX_DB().op_update(toUpdateD11Null, new List<SObjectField> {});
           
            // D11 Concomitant Medical Products and Therapy Dates records to be created/updated
            List<SQX_Medwatch_D11_Medical_Product__c> toInsertD11MedicalProducts = new List<SQX_Medwatch_D11_Medical_Product__c>();
            List<SQX_Medwatch_D11_Medical_Product__c> toUpdateD11MedicalProducts = new List<SQX_Medwatch_D11_Medical_Product__c>();
            
            for(SQX_Medwatch_D11_Medical_Product__c d11MedicalProduct: D11MedicalProducts) {
                if(d11MedicalProduct.Id == null) {
                    d11MedicalProduct.SQX_MedWatch__c = mainRecordId;
                    d11MedicalProduct.SQX_D11_Medical_Product_Null_Value__c = d11MedicalProduct.SQX_D11_Medical_Product_Null_Value__r.Id;
                    toInsertD11MedicalProducts.add(d11MedicalProduct);
                } else {
                    toUpdateD11MedicalProducts.add(d11MedicalProduct);
                }
            }
            
            // F10 Device Code record to be created/updated
            List<SQX_Medwatch_F10_Device_Code__c> toInsertF10DeviceCode = new List<SQX_Medwatch_F10_Device_Code__c>();
            List<SQX_Medwatch_F10_Device_Code__c> toUpdateF10DeviceCode = new List<SQX_Medwatch_F10_Device_Code__c>();

            for(SQX_Medwatch_F10_Device_Code__c f10DeviceCodes: F10DeviceCodes) {
                if(f10DeviceCodes.Id == null) {
                    f10DeviceCodes.SQX_MedWatch__c = mainRecordId;
                    toInsertF10DeviceCode.add(f10DeviceCodes);
                } else {
                    toUpdateF10DeviceCode.add(f10DeviceCodes);
                }
            }
            
            // F10 Patient Code record to be created/updated
            List<SQX_Medwatch_F10_Patient_Code__c> toInsertF10PatientCode = new List<SQX_Medwatch_F10_Patient_Code__c>();
            List<SQX_Medwatch_F10_Patient_Code__c> toUpdateF10PatientCode = new List<SQX_Medwatch_F10_Patient_Code__c>();
            for(SQX_Medwatch_F10_Patient_Code__c f10PatientCode: F10PatientCodes) {
                if(f10PatientCode.Id == null) {
                    f10PatientCode.SQX_MedWatch__c = mainRecordId;
                    toInsertF10PatientCode.add(f10PatientCode);
                } else {
                    toUpdateF10PatientCode.add(f10PatientCode);
                }
            }
            
            // H6 Device Code to created/updated
            List<SQX_Medwatch_H6_Device_Code__c> toInsertH6DeviceCode = new List<SQX_Medwatch_H6_Device_Code__c>();
            List<SQX_Medwatch_H6_Device_Code__c> toUpdateH6DeviceCode = new List<SQX_Medwatch_H6_Device_Code__c>();
            for(SQX_Medwatch_H6_Device_Code__c h6DeviceCode: H6DeviceCodes) {
                if(h6DeviceCode.Id == null) {
                    h6DeviceCode.SQX_MedWatch__c = mainRecordId;
                    toInsertH6DeviceCode.add(h6DeviceCode);
                } else {
                    toUpdateH6DeviceCode.add(h6DeviceCode);
                }
            }
            
            // H6 Patient Code record to be created/updated
            List<SQX_Medwatch_H6_Patient_Code__c> toInsertH6PatientCode = new List<SQX_Medwatch_H6_Patient_Code__c>();
            List<SQX_Medwatch_H6_Patient_Code__c> toUpdateH6PatientCode = new List<SQX_Medwatch_H6_Patient_Code__c>();
            for(SQX_Medwatch_H6_Patient_Code__c h6PatientCode: H6PatientCodes) {
                if(h6PatientCode.Id == null) {
                    h6PatientCode.SQX_MedWatch__c = mainRecordId;
                    toInsertH6PatientCode.add(h6PatientCode);
                } else {
                    toUpdateH6PatientCode.add(h6PatientCode);
                }
            }
            
            // H6 Method Code record to be created/updated
            List<SQX_Medwatch_H6_Method__c> toInsertH6MethodCode = new List<SQX_Medwatch_H6_Method__c>();
            List<SQX_Medwatch_H6_Method__c> toUpdateH6MethodCode = new List<SQX_Medwatch_H6_Method__c>();
            for(SQX_Medwatch_H6_Method__c h6MethodCode: H6MethodCodes) {
                if(h6MethodCode.Id == null) {
                    h6MethodCode.SQX_MedWatch__c = mainRecordId;
                    toInsertH6MethodCode.add(h6MethodCode);
                } else {
                    toUpdateH6MethodCode.add(h6MethodCode);
                }
            }
            
            // H6 Result Code record to be created/updated
            List<SQX_Medwatch_H6_Result__c> toInsertH6ResultCode = new List<SQX_Medwatch_H6_Result__c>();
            List<SQX_Medwatch_H6_Result__c> toUpdateH6ResultCode = new List<SQX_Medwatch_H6_Result__c>();
            for(SQX_Medwatch_H6_Result__c h6ResultCode: H6ResultCodes) {
                if(h6ResultCode.Id == null) {
                    h6ResultCode.SQX_MedWatch__c = mainRecordId;
                    toInsertH6ResultCode.add(h6ResultCode);
                } else {
                    toUpdateH6ResultCode.add(h6ResultCode);
                }
            }
            
            // H6 Conclusion Code record to be created/updated
            List<SQX_Medwatch_H6_Conclusion__c> toInsertH6ConclusionCode = new List<SQX_Medwatch_H6_Conclusion__c>();
            List<SQX_Medwatch_H6_Conclusion__c> toUpdateH6ConclusionCode = new List<SQX_Medwatch_H6_Conclusion__c>();
            for(SQX_Medwatch_H6_Conclusion__c h6ConclusionCode: H6ConclusionCodes) {
                if(h6ConclusionCode.Id == null) {
                    h6ConclusionCode.SQX_MedWatch__c = mainRecordId;
                    toInsertH6ConclusionCode.add(h6ConclusionCode);
                } else {
                    toUpdateH6ConclusionCode.add(h6ConclusionCode);
                }
            }
            // Updates all child records related to Medwatch record
            new SQX_DB().op_update(new Map<SObjectType, SObject[]> {
                SQX_MedWatch_Suspect_Product__c.SObjectType => toUpdate,
                    SQX_Medwatch_D11_Medical_Product__c.SObjectType => toUpdateD11MedicalProducts,
                    SQX_Medwatch_F10_Device_Code__c.SObjectType => toUpdateF10DeviceCode,
                    SQX_Medwatch_F10_Patient_Code__c.SObjectType => toUpdateF10PatientCode,                    
                    SQX_Medwatch_H6_Device_Code__c.SObjectType => toUpdateH6DeviceCode,
                    SQX_Medwatch_H6_Patient_Code__c.SObjectType => toUpdateH6PatientCode,                    
                    SQX_Medwatch_H6_Method__c.SObjectType => toUpdateH6MethodCode,
                    SQX_Medwatch_H6_Result__c.SObjectType => toUpdateH6ResultCode,
                    SQX_Medwatch_H6_Conclusion__c.SObjectType => toUpdateH6ConclusionCode},
                                   new Map<SObjectType, SObjectField[]> {
                                       SQX_MedWatch_Suspect_Product__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_D11_Medical_Product__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_F10_Device_Code__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_F10_Patient_Code__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_H6_Device_Code__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_H6_Patient_Code__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_H6_Method__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_H6_Result__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_H6_Conclusion__c.SObjectType => new List<SObjectField> ()});
            
            // Saves all of the Child Objects record related to Medwatch
            new SQX_DB().op_insert(new Map<SObjectType, SObject[]> {
                SQX_MedWatch_Suspect_Product__c.SObjectType => toInsert,
                    SQX_Medwatch_D11_Medical_Product__c.SObjectType => toInsertD11MedicalProducts,
                    SQX_Medwatch_F10_Device_Code__c.SObjectType => toInsertF10DeviceCode,
                    SQX_Medwatch_F10_Patient_Code__c.SObjectType => toInsertF10PatientCode,                    
                    SQX_Medwatch_H6_Device_Code__c.SObjectType => toInsertH6DeviceCode,
                    SQX_Medwatch_H6_Patient_Code__c.SObjectType => toInsertH6PatientCode,                    
                    SQX_Medwatch_H6_Method__c.SObjectType => toInsertH6MethodCode,
                    SQX_Medwatch_H6_Result__c.SObjectType => toInsertH6ResultCode,
                    SQX_Medwatch_H6_Conclusion__c.SObjectType => toInsertH6ConclusionCode},
                                   new Map<SObjectType, SObjectField[]> {
                                       SQX_MedWatch_Suspect_Product__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_D11_Medical_Product__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_F10_Device_Code__c.SObjectType => new List<SObjectField> (),
                                           SQX_Medwatch_F10_Patient_Code__c.SObjectType => new List<SObjectField> (), 
                                           SQX_Medwatch_H6_Device_Code__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_H6_Patient_Code__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_H6_Method__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_H6_Result__c.SObjectType => new List<SObjectField>(),
                                           SQX_Medwatch_H6_Conclusion__c.SObjectType => new List<SObjectField>()});

            // Delete all the requested child object record
            new SQX_DB().op_delete(new Map<SObjectType, SObject[]>{
                SQX_Medwatch_Suspect_Product__c.SObjectType => toDelete,
                    SQX_Medwatch_D11_Medical_Product__c.SObjectType => toDeleteD11MedicalProducts,
                    SQX_Medwatch_F10_Device_Code__c.SObjectType => toDeleteF10DeviceCodes,
                    SQX_Medwatch_F10_Patient_Code__c.SObjectType => toDeleteF10PatientCodes,                    
                    SQX_Medwatch_H6_Device_Code__c.SObjectType => toDeleteH6DeviceCodes,
                    SQX_Medwatch_H6_Patient_Code__c.SObjectType => toDeleteH6PatientCodes,                    
                    SQX_Medwatch_H6_Method__c.SObjectType => toDeleteH6MethodCodes,
                    SQX_Medwatch_H6_Result__c.SObjectType => toDeleteH6ResultCodes,
                    SQX_Medwatch_H6_Conclusion__c.SObjectType => toDeleteH6ConclusionCodes});

            // update null values to link with the corresponding record
            toUpdateSuspNull = new List<SQX_Medwatch_Suspect_Product_Null_Value__c>();
            for(SQX_Medwatch_Suspect_Product__c suspect : toInsert) {
                toUpdateSuspNull.add(new SQX_Medwatch_Suspect_Product_Null_Value__c(Id = suspect.SQX_Suspect_Product_Null_Value__c, SQX_Medwatch_Suspect_Product__c = suspect.Id));
            }

            toUpdateD11Null = new List<SQX_Medwatch_D11_Medical_Prod_Null_Value__c>();
            for(SQX_Medwatch_D11_Medical_Product__c d11MedicalProd : toInsertD11MedicalProducts) {
                toUpdateD11Null.add(new SQX_Medwatch_D11_Medical_Prod_Null_Value__c(Id = d11MedicalProd.SQX_D11_Medical_Product_Null_Value__c, SQX_Medwatch_D11_Medical_Product__c = d11MedicalProd.Id));
            }

            new SQX_DB().op_update(
                new Map<SObjectType, List<SObject>>{
                    SQX_Medwatch_Suspect_Product_Null_Value__c.SObjectType => toUpdateSuspNull,
                    SQX_Medwatch_D11_Medical_Prod_Null_Value__c.SObjectType => toUpdateD11Null
                },
                new Map<SObjectType, List<SObjectField>> {
                    SQX_Medwatch_Suspect_Product_Null_Value__c.SObjectType => new List<SObjectField> { SQX_Medwatch_Suspect_Product_Null_Value__c.SQX_Medwatch_Suspect_Product__c },
                    SQX_Medwatch_D11_Medical_Prod_Null_Value__c.SObjectType => new List<SObjectField> { SQX_Medwatch_D11_Medical_Prod_Null_Value__c.SQX_Medwatch_D11_Medical_Product__c }
                }
            );

        } catch (Exception ex) {
            Database.rollback(sp);
            SQX_Utilities.addErrorMessageInPage(ex.getMessage());
            returnTo = null;
        }

        return returnTo;
    }


    /**
     * Method saves the Medwatch Report without having the page redirect (i.e redirects to null) such that validation can be performed after save
    */
    global PageReference saveAndReturn() {
        save();
        return null;
    }

    /**
     * Method to add validation error messages to the page
    */
    global void addErrorMessages() {
        if(!String.isBlank(message)) {
            for(String message : message.split(';;;')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            }
        }
    }
}