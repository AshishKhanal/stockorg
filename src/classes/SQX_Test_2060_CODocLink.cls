/**
* The following scenario has been tested by the test code
* a. Given a Change Order's action refers to a Controlled Document as revised/new, then Controlled Doc's Change Order is updated when it is blank
* b. Given a Change Order's action refers to a Controlled Document as revised/new, then Controlled Doc's Change Order is not updated when it is not blank
* c. Given a Change Order's action previously refers to a Controlled Document as revised/new, then Controlled Doc's Change Order is updated to blank
*/
@IsTest
public class SQX_Test_2060_CODocLink{
    
    /**
    * This test ensures that all setup datas are inserted
    **/
    public testmethod static void givenChangeOrdersActionRefersControlledDoc_ControlledDocsCOLinkIsConsistent() {
		UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User stdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
                
        System.runAs(stdUser){
            // Arrange: Create a change order in which we will add three actions two new doc one revise.
            // 			a. A1 -> New Document Task
            // 			b. A2 -> Revise Document Task -> Doc To Revise : DOC1
            // 			c. A3 -> New Document Task
            // 		    Create a controlled document DOC1 to revise
            
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save(),
                				  changeOrder2 = new SQX_Test_Change_Order().save();
            
            SQX_Test_Controlled_Document controlledDocument = new SQX_Test_Controlled_Document().save();
            controlledDocument.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Implementation__c a1 = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, stdUser, null, null),
                				  a2 = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, stdUser, null, null),
                				  a3 = changeOrder2.addAction(SQX_Implementation.RECORD_TYPE_ACTION, stdUser, null, null);
	       		
            a1.Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            a2.Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            a2.SQX_Controlled_Document__c = controlledDocument.doc.Id;
            a3.Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            
            insert new List<SObject> {a1, a2, a3};
            
            // Act: Create a doc, Revise one doc and then update all three actions [a]
            SQX_Test_Controlled_Document revisedDoc = controlledDocument.revise();
            SQX_Test_Controlled_Document newDocument = new SQX_Test_Controlled_Document().save();
            
            a1.SQX_Controlled_Document_New__c = newDocument.doc.Id;
            a2.SQX_Controlled_Document_New__c = revisedDoc.doc.Id;
            
            Database.SaveResult [] saveResults = Database.update(new List<SObject> {a1, a2 }, false);
            
            // Assert: ensure that a1 and a2 were saved
            System.assert(saveResults[0].isSuccess(), 'Expected a1 to save but failed ' + saveResults[0]);
            System.assert(saveResults[1].isSuccess(), 'Expected a2 to save but failed ' + saveResults[1]);
            
            // Assert: ensure that the new doc and revised doc have change order set
            Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>([SELECT SQX_Change_Order__c
                                                                                                FROM SQX_Controlled_Document__c
                                                                                                WHERE ID = : newDocument.doc.Id OR ID = : revisedDoc.doc.Id]);
            System.assertEquals(changeOrder.Id, docs.get(newDocument.doc.Id).SQX_Change_Order__c,
                               'Expected change order id to be updated in new document but found ' + docs.get(newDocument.doc.Id) + newDocument.doc.Id);
            
            System.assertEquals(changeOrder.Id, docs.get(revisedDoc.doc.Id).SQX_Change_Order__c,
                               'Expected change order id to be updated in new document but found ' + docs.get(revisedDoc.doc.Id));
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act: update a3 with newdocument id
            a3.SQX_Controlled_Document_New__c = newDocument.doc.Id;
            saveResults = Database.update(new List<SObject> {a3 }, false);
            
            // Assert: A3 wasn't saved and an error was thrown
            System.assert(!saveResults[0].isSuccess(), 'Expected a3 to not save but saved ');
            Boolean alreadyLinkedErrorWasThrown = false;
            for(Database.Error error : saveResults[0].getErrors()){
                if(error.getMessage().contains(Label.SQX_ERR_MSG_CONTROLLED_DOC_HAS_CHANGE_ORDER)){
                    alreadyLinkedErrorWasThrown = true;
                    break;
                }
            }
            
            System.assert(alreadyLinkedErrorWasThrown, 'Expected a3 to contain the error message saying doc already has another change order but found ' + saveResults[0]);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act: Update a1 with blank new document id [d] detach case
            a1.SQX_Controlled_Document_New__c = null;
            
            saveResults = Database.update(new List<SObject> { a1 }, false);
            
            // Assert: ensure that a1 was saved
            System.assert(saveResults[0].isSuccess(), 'Expected a1 to save but failed ' + saveResults[0]);
            
            // Assert: ensure that new doc's change order is blank
            docs = new Map<Id, SQX_Controlled_Document__c>([SELECT SQX_Change_Order__c
                                                            FROM SQX_Controlled_Document__c
                                                            WHERE ID = : newDocument.doc.Id]);
            
            System.assertEquals(null, docs.get(newDocument.doc.Id).SQX_Change_Order__c,
                               'Expected change order id to be cleared in new document but found ' + docs.get(newDocument.doc.Id));
            
        }
        
    }

}