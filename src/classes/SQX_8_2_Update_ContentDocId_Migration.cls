/**
* Batch processor to migrate all old record(s) of supplier management steps
* Added in version  : 8.2
* Used for versions : less than 8.2
*/
global with sharing class SQX_8_2_Update_ContentDocId_Migration implements Database.Batchable<SObject>{

    List<SObjectType> ObjectTypesToMigrate = new List<SObjectType>{
                Schema.SQX_Onboarding_Step__c.SObjectType,
                Schema.SQX_Deviation_Process__c.SObjectType,
                Schema.SQX_Supplier_Interaction_Step__c.SObjectType,
                Schema.SQX_Supplier_Escalation_Step__c.SObjectType
            };

    private Integer processObjectTypeIndex = 0;

    /**
     * Global constructor for instantiating the migration job and starting the work
     */
    global SQX_8_2_Update_ContentDocId_Migration(){
        this(0);
    }

    /**
     * Constructor for instantiating with different steps
     */
    public SQX_8_2_Update_ContentDocId_Migration(Integer objectTypeIndex){
        processObjectTypeIndex = objectTypeIndex;
    }

    /**
     * Returns the query locator for each different type of record
     */
    global Database.QueryLocator start(Database.BatchableContext bc){
        Id recordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(String.valueOf(ObjectTypesToMigrate.get(processObjectTypeIndex)), SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
        Database.QueryLocator sobjecRecordList= Database.getQueryLocator('SELECT Id, SQX_Parent__r.OwnerId, ContentDocumentId__c, ContentDocId__c, Status__c, SQX_Parent__c FROM '+ ObjectTypesToMigrate.get(processObjectTypeIndex) + ' WHERE RecordTypeId = \''+ recordTypeId +'\' AND SQX_Controlled_Document__c != null');
        return sobjecRecordList;
    }

    /**
     * Removes content document links added between doc and content in < 8.2.0 and also ensures that the records
     * are consistent with 8.2.0 step status
     */
    global void execute(Database.BatchableContext bc, List<SObject> objectList){
        SQX_ContentDocumentLink.disableForMigration = true;
        if(objectList.size() > 0){
            SQX_ContentVersion_Clone reproducer = new SQX_ContentVersion_Clone(true);
            Id cqContentRecordType = SQX_ContentVersion.getCQContentRecordTypeId();
            List<SObject> processedRecords = new List<SObject>();
            Map<Id, Set<Id>> deleteContentDocLinks = new Map<Id, Set<Id>>();
            Set<Id> parentIds = new Set<Id>();
            SObjectType parentType = null;
            for(SObject step: objectList) {
                Id parentId = (Id)step.get(SQX_Steps_Trigger_Handler.PARENT_OBJECT_ID_FIELD);
                parentIds.add(parentId);
                if(parentType == null) {
                    parentType = parentId.getSObjectType();
                }
            }

            String query = 'SELECT ' + SQX_Steps_Trigger_Handler.PARENT_FIELD_STATUS + ' FROM ' +  parentType.getDescribe().getName()
                + ' WHERE Id IN : parentIds';
            Map<Id, SObject> parents = new Map<Id, SObject>(Database.query(query));

            for(SObject step: objectList) {
                Id oldContentId = (Id) step.get(SQX_Steps_Trigger_Handler.FIELD_CONTENTDOCUMENT_ID);
                Id contentId = (Id) step.get(SQX_Steps_Trigger_Handler.FIELD_STEP_CONTENTDOCUMENT_ID);
                String stepStatus = (String) step.get(SQX_Steps_Trigger_Handler.FIELD_STATUS);
                Id parentId = (Id)step.get(SQX_Steps_Trigger_Handler.PARENT_OBJECT_ID_FIELD);
                String parentStatus = (String)parents.get(parentId).get(SQX_Steps_Trigger_Handler.PARENT_FIELD_STATUS);
                Set<Id> linkedEntities = deleteContentDocLinks.get(oldContentId);
                if(linkedEntities == null) {
                    linkedEntities = new Set<Id>();
                    deleteContentDocLinks.put(oldContentId, linkedEntities);
                }
                linkedEntities.add(step.Id);

                Id ownerId = (Id) step.getSObject(SQX_Steps_Trigger_Handler.FIELD_PARENT_RECORD_RELATIONSHIP).get('OwnerId');

                if(parents.get(parentId).get(SQX_Steps_Trigger_Handler.PARENT_FIELD_STATUS) == SQX_Steps_Trigger_Handler.PARENT_STATUS_OPEN &&
                step.get(SQX_Steps_Trigger_Handler.FIELD_STATUS) == SQX_Steps_Trigger_Handler.STATUS_DRAFT && String.isEmpty((String)step.get(SQX_Steps_Trigger_Handler.FIELD_STEP_CONTENTDOCUMENT_ID))) {
                    SObject stepToUpdate = step.clone(true);
                    reproducer.cloneContentFor(stepToUpdate, oldContentId, new ContentVersion(OwnerId = ownerId));
                    processedRecords.add(stepToUpdate);
                }
            }

            if(!deleteContentDocLinks.isEmpty()){
                cleanUpPreviousRecords(deleteContentDocLinks);
            }

            if(!processedRecords.isEmpty()) {
                reproducer.cloneRecords();
                reproducer.linkDocumentsToInitiators(SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED);
                List<SObject> objectsToUpdate = reproducer.updateDocumentIdInInitiators(SQX_Steps_Trigger_Handler.FIELD_STEP_CONTENTDOCUMENT_ID, true);
                new SQX_DB().op_update(objectsToUpdate, new SObjectField[] {});
            }
        }
    }

    /**
     * Removes content document links that had been created prior to 8.2 between Controlled Document's Content and Content
     * @param  deleteContentDocLinks The map containing set of steps for each content id
     */
    private void cleanUpPreviousRecords(Map<Id, Set<Id>> deleteContentDocLinks){
        Set<Id> linkedEntities = new Set<Id>();
        List<ContentDocumentLink> linksToDelete = new List<ContentDocumentLink>();

        for(Id contentId : deleteContentDocLinks.keySet()) {
            linkedEntities.addAll(deleteContentDocLinks.get(contentId));
        }

        for(ContentDocumentLink link : [SELECT Id, LinkedEntityId, ContentDocumentID FROM ContentDocumentLink
                                             WHERE ContentDocumentID IN : deleteContentDocLinks.keySet() AND LinkedEntityId IN : linkedEntities]) {
            //
            if(deleteContentDocLinks.containsKey(link.ContentDocumentID) && deleteContentDocLinks.get(link.ContentDocumentID).contains(link.LinkedEntityId)) {
                linksToDelete.add(link);
            }
        }
        /*
         * Without SHARING used
         * -------------
         * Content Document Link can be added by any user hence, escalation is done while removing it.
         */
        new SQX_DB().withoutSharing().op_delete(linksToDelete);
    }

    /**
     * Finishes the batch job when all types of records have been processed.
     */
    global void finish(Database.BatchableContext bc){
        processObjectTypeIndex++;
        if (!Test.isRunningTest() && processObjectTypeIndex < ObjectTypesToMigrate.size()) {
            SQX_8_2_Update_ContentDocId_Migration processor = new SQX_8_2_Update_ContentDocId_Migration(processObjectTypeIndex);
            Database.executeBatch(processor, 1);
        }
    }
}