/**
 * This test ensures that field set value is properly set while revising document.
 */
@isTest
public class SQX_Test_3664_Configure_Title_Duplicate {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * Test to ensure that different value of field available in fieldset are properly set while revising document.
     * @story : SQX-3664
     */
    public static testmethod void ensureValueOfFieldAvailableInFieldSetArebeingCopiedInRevisedDocument() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){

            // Arrange : Create doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.save(true);
            
            // get the field set member list for 'New_Document' fieldset, this field set is being used to test whether the fields of the fieldset is being copied or not
            List<Schema.FieldSetMember> fieldSetMembers = Schema.SObjectType.SQX_Controlled_Document__c.fieldSets.New_Document.getFields();
            
            // create controller and instantiate corresponding extension
            ApexPages.StandardController controller = new ApexPages.StandardController(doc.doc);
            SQX_Extension_Controlled_Document docExt = new SQX_Extension_Controlled_Document(controller, fieldSetMembers);
            
            // Assert : Ensure all the fields in the field set are being copied in revisionDoc
            for(Schema.FieldSetMember m : fieldSetMembers ) {
                System.assertEquals( doc.doc.get( m.getFieldPath() ), docExt.revisionDoc.get( m.getFieldPath() ) );
            }
            
            // Act : Change title of document being revised and revise doc
            docExt.revisionDoc.Title__c = 'Revised_Doc_Title_Test';
            
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            doc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            doc.save();
            
            // Assert : document should be revised properly
            PageReference pr = docExt.Revise();
            System.assert(pr != null, 'Expected doc to be revised');
            
            // Assert : revised document should have new title value as set above.
            List<SQX_Controlled_Document__c> revDoc = [SELECT Id, Title__c, Revision__c FROM SQX_Controlled_Document__c WHERE Document_Number__c = :doc.doc.Document_Number__c AND Title__c = :docExt.revisionDoc.Title__c ];
            System.assert(revDoc.size() > 0,'Expected revised document should have new changed title');
        }
    }

    /**
     * Test to ensure that different value of field available in fieldset are properly set while revising document through document review.
     * @story : SQX-3664
     */
    public static testmethod void testReview() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            
            // Arrange : Create current doc
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save(true);
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save(true);
            
            // document review create page
            PageReference docReviewCreatePage = Page.SQX_Document_Review_Editor;
            docReviewCreatePage.getParameters().put('controlleddocid', cDoc.doc.Id);
            
            Test.setCurrentPage(docReviewCreatePage);
            SQX_Document_Review__c rev1 = new SQX_Document_Review__c( Controlled_Document__c = cDoc.doc.Id );
            ApexPages.StandardController controller = new ApexPages.StandardController(rev1);
            SQX_Extension_Document_Review ext = new SQX_Extension_Document_Review( controller, Schema.SObjectType.SQX_Controlled_Document__c.fieldSets.New_Document.getFields() );
            rev1.Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_REVISE;
            rev1.Performed_By__c = UserInfo.getUserId();
            rev1.Performed_Date__c = System.today();
            
            // Act : Change title of document being revised and revise doc
            ext.revisionDoc.Title__c = 'Revised_Doc_Title_Test';
            
            // ACT: create new document review
            PageReference pr = ext.SubmitReview();

            // Assert : document should be revised properly
            System.assert(pr != null, 'Expected the review to be submitted successfully');

            // Assert : revised document should have new title value as set above.
            List<SQX_Controlled_Document__c> revDoc = [SELECT Id, Title__c, Revision__c FROM SQX_Controlled_Document__c WHERE Document_Number__c = :cDoc.doc.Document_Number__c AND Title__c = :ext.revisionDoc.Title__c ];
            System.assert(revDoc.size() > 0,'Expected revised document should have new changed title');
        }
    }
}