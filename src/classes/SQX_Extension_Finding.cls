/**
* This the extension used by finding view/edit overriden page. It contains various
* properties and methods that are required for the page. 
*/
public with sharing class SQX_Extension_Finding extends SQX_Extension_UI {
    
    /**
    * A static list of supported next actions
    */
    public static   String  NXTACT_SAVE = 'save',
                            NXTACT_SUBMIT_RESPONSE = 'submitresponse',
                            NXTACT_APPROVE_RESPONSE = 'approveresponse',
                            NXTACT_REJECT_RESPONSE = 'rejectresponse',
                            NXTACT_RECALL_RESPONSE = 'recallresponse',
                            NXTACT_TAKE_OWNERSHIP = 'takeownership',
                            NXTACT_TAKE_OWNERSHIP_AND_INITIATE = 'takeownershipandinitiate',
                            NXTACT_INITIATE = 'initiate',
                            NXTACT_ESCALATE_TO_CAPA = 'escalatetocapa',
                            NXTACT_CLOSE = 'close';
    
    /*
    * Returns customer script page layout
    */
    public override pageReference getCustomerScriptPageLayout(){
        return new pagereference(getPageName('_Cu_Script_Layout', ''));
    }
    
    /**
    * Constructor for the apex page that inits the static esig policies, purpose of sig map, security matrix and other tasks
    */
    public SQX_Extension_Finding(ApexPages.StandardController controller){
        super(controller, new List<String> {'Status__c', 'Create_New_CAPA__c '});
        
        SQX_Finding__c finding = (SQX_Finding__c)controller.getRecord();
        
        
        //we do not need Change esig validation if Finding is new or is in draft state.
        Boolean isRecInDraftOrNew = !(finding.Id != null && finding.Status__c != SQX_Finding.STATUS_DRAFT);
        addStaticPolicy(NXTACT_SAVE, getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.Finding));
        
        Map<String, SQX_CQ_Esig_Policies__c> allPolicies = getEsigPolicy();
        this.purposeOfSigMap.putAll(new Map<String, String>{
                NXTACT_SAVE => Label.SQX_PS_Finding_Saving_the_record,
                NXTACT_SUBMIT_RESPONSE => Label.SQX_PS_Finding_Submitting_Response,
                NXTACT_APPROVE_RESPONSE => Label.SQX_PS_Finding_Approving_Response,
                NXTACT_REJECT_RESPONSE => Label.SQX_PS_Finding_Rejecting_Response,
                NXTACT_RECALL_RESPONSE => Label.SQX_PS_Finding_Recalling_Response,
                NXTACT_TAKE_OWNERSHIP => Label.SQX_PS_Finding_Taking_Ownership,
                NXTACT_TAKE_OWNERSHIP_AND_INITIATE => Label.SQX_PS_Finding_Taking_Ownership_and_Initiating_the_record,
                NXTACT_INITIATE => Label.SQX_PS_Finding_Initiating_the_record,
                NXTACT_CLOSE => Label.SQX_PS_Finding_Closing_the_record,
                NXTACT_ESCALATE_TO_CAPA => Label.SQX_PS_Finding_Escalate_To_CAPA
            });
         this.addActionSaveAfterDraft();
    }
    
    /**
    * Add default policies for the various next actions
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
        defaultEsigPolicies.put(NXTACT_CLOSE, this.POLICY_COMMENT_REQUIRED);
        return defaultEsigPolicies;
    }
    
    /**
    * Defines the list of entities whose data are to be exported along with the main record type
    */
    public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){
        return super.getDataLoaderConfig()

                    /* include parent records */
                    .follow(SQX_Finding__c.SQX_Audit__c)
                    .follow(SQX_Response_Inclusion__c.SQX_Containment__c)
                    .follow(SQX_Response_Inclusion__c.SQX_Investigation__c)
                    
                    /* include child records */
                    .followChild(SQX_Finding_CAPA__c.SQX_Finding__c)
                    .followChild(SQX_Finding_Record_Activity__c.SQX_Finding__c)
                    .followChild(SQX_Finding_Response__c.SQX_Finding__c)
                    .followChild(SQX_Containment__c.SQX_Finding__c)
                    .followChild(SQX_Investigation__c.SQX_Finding__c)
                    .followChild(SQX_Approved_Investigation__c.SQX_Finding__c)
                    .followChild(SQX_Response_Inclusion__c.SQX_Response__c)
                    .followChild(SQX_Root_Cause__c.SQX_Investigation__c)
                    .followChild(SQX_Root_Cause__c.SQX_Root_Cause_Code__c)
                    .followChild(SQX_Investigation_Tool__c.SQX_Investigation__c)
                    .followChild(SQX_Action_Plan__c.SQX_Investigation__c)
                    .followChild(SQX_Audit_Response__c.SQX_Audit__c)
                    .followChild(SQX_Finding_Response__c.SQX_Audit_Response__c)
                    .followChild(SQX_Resp_Inclusion_Approval__c.SQX_Response_Inclusion__c)
                    .followChild(SQX_Supplier_Deviation_Finding__c.SQX_Finding__c)
                    .followChild(SQX_Escalation_Reference__c.SQX_Related_Finding__c)
                    
                    /* include approval history */
                    .followApprovalOf(SQX_Finding_Response__c.sObjectType)
                    
                    /* include attachments */
                    .followAttachmentOf(SQX_Finding__c.sObjectType)
                    .followAttachmentOf(SQX_Containment__c.sObjectType)
                    .followAttachmentOf(SQX_Investigation__c.sObjectType)
                    
                    /* include notes */
                    .followNoteOf(SQX_Finding__c.sObjectType)
                    
                    /* include additional fields of parent records */
                    .getFieldsFor(SQX_Part__c.sObjectType, new List<SObjectField> {SQX_Part__c.Part_Number_and_Name__c, SQX_Part__c.Name, SQX_Part__c.Part_Number__c});
    }
    

    /**
    * Performs the next action after the save/default action has taken place
    */
    public override void processNextAction(Map<String, String> params){
        SQX_Finding__c finding = (SQX_Finding__c) mainRecord;

        //declaring a list at the top helps us pass the finding to extension without newing every time
        List<SQX_Finding__c> findings = new List<SQX_Finding__c> { finding }; 

        if(NXTACT_TAKE_OWNERSHIP.equalsIgnoreCase(nextAction)){
            SQX_Finding.takeOwnership(findings);
        }
        else if(NXTACT_APPROVE_RESPONSE.equalsIgnoreCase(nextAction) || NXTACT_REJECT_RESPONSE.equalsIgnoreCase(nextAction) || NXTACT_RECALL_RESPONSE.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Finding_Response__c WHERE Id = :responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId');
            
            if (NXTACT_APPROVE_RESPONSE.equalsIgnoreCase(nextAction) || NXTACT_REJECT_RESPONSE.equalsIgnoreCase(nextAction)) {
                approveRecord(NXTACT_APPROVE_RESPONSE.equalsIgnoreCase(nextAction), responseID, (String)params.get('remark'), originalApproverId);
            }
            else if (NXTACT_RECALL_RESPONSE.equalsIgnoreCase(nextAction)) {
                recallRecord(true, responseID, (String) params.get('remark'), originalApproverId);
            }
        }
        else if(NXTACT_TAKE_OWNERSHIP_AND_INITIATE.equalsIgnoreCase(nextAction)){
        	SQX_Finding.takeOwnershipAndInitiate(findings);
        }
        else if(NXTACT_INITIATE.equalsIgnoreCase(nextAction)){
        	SQX_Finding.initiate(findings);
        }
        else if(NXTACT_CLOSE.equalsIgnoreCase(nextAction)){
            SQX_Finding.close(findings);
        }
        else if(NXTACT_ESCALATE_TO_CAPA.equalsIgnoreCase(nextAction)){
            String comment = params.get('comment');

            List<SQX_Finding__c> findingList = [SELECT Id, 
                                                       Create_New_CAPA__c, 
                                                       Name, 
                                                       Title__c, 
                                                       Description__c, 
                                                       Stage__c, 
                                                       SQX_CAPA__c,
                                                       Org_Business_Unit__c,
                                                       Org_Division__c,
                                                       Org_Region__c,
                                                       Org_Site__c,
                                                       Operation__c
                                                FROM SQX_Finding__c WHERE Id IN: findings];
            nextActionProcessedId = SQX_Finding.escalateToCAPA(findingList, comment).get(findingList[0].Id);

        }
    }
    
    /**
    * Returns the configuration that is to be used in response upsert. It returns that configuration with the
    * fields that are to be ignored for a particular type.
    */
    public override SQX_Upserter.SQX_Upserter_Config getResponseConfig() {
        return new SQX_Upserter.SQX_Upserter_Config()
                               .omit(SQX.Finding, SQX_Finding__c.Stage__c)
                               .omit(SQX.Finding, SQX_Finding__c.Status__c);
    }
    
    /**
    * Returns the expectation of the response i.e. what makes up a response 
    */
    public override SQX_Upserter.SQX_Upserter_Interceptor getResponseExpectation(boolean sendForApproval) {
        SQX_ResponseExpectation expectation = new SQX_ResponseExpectation(SQX.Response);
        final Integer ANY_NUMBER = expectation.ANY_NUMBER;
        
        Map<String, Integer> objectsCount = new Map<String, Integer>();
        objectsCount.put(SQX.Response, 1);
        objectsCount.put(SQX.Containment, ANY_NUMBER);
        objectsCount.put(SQX.Investigation, ANY_NUMBER);
        objectsCount.put(SQX.InvestigationTool , ANY_NUMBER);
        objectsCount.put(SQX.RootCause, ANY_NUMBER);
        objectsCount.put(SQX.ActionPlan, ANY_NUMBER);
        objectsCount.put(SQX.ResponseInclusion, ANY_NUMBER);

        expectation.sendForApproval = sendForApproval;
        expectation.numberOfObjectsToBeCreated = objectsCount;
        expectation.requiredCount = new Map<String, Integer> { SQX.Response => 1 };
        
        return expectation;
    }
    
    /**
    * Default remote action for the extension to send any data that needs to be persisted or when certain next action is to be performed on the main record
    * @param findingId the Id of the finding order record
    * @param changeSet the findings that are to be saved
    * @param objectsToDelete the list of ids that are to be removed from SF
    * @param params the list of extra params such as next action is to be used for processing
    * @param esigData the electronic signature data that must be submitted inorder to process the record
    */
    @RemoteAction
    public static String processChangeSetWithAction(String findingId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData) {
        return processChangeSetWithAction(findingId, changeSet, objectsToDelete, params, esigData, false);
    }

    /**
     * Internal method for processing the change set with action given action
     * @param findingId the Id of the finding order record
     * @param changeSet the findings that are to be saved
     * @param objectsToDelete the list of ids that are to be removed from SF
     * @param params the list of extra params such as next action is to be used for processing
     * @param esigData the electronic signature data that must be submitted inorder to process the record
     * @param apiIntegration <code>true</code> if this is api integration (Used by NC Staging) else <code>false</code>
     */
    public static String processChangeSetWithAction(String findingId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData, Boolean apiIntegration)
    {

        Boolean isNewRecord = !SQX_Upserter.isValidId(findingId);
        SQX_Finding__c  finding;

        if(isNewRecord){
            finding = new SQX_Finding__c();
        }
        else{
            finding = [SELECT    Id, 
                                Status__c
                        FROM SQX_Finding__c WHERE Id = : findingId];
        }

        //passing expectation because the expectation will store the persisted object in the transaction.
        SQX_Extension_UI.BaseUpsertExpectation expectation = new SQX_Extension_UI.BaseUpsertExpectation(SQX.Finding, findingId);

        SQX_Extension_Finding findingExtension = new SQX_Extension_Finding(new ApexPages.StandardController(finding));

        SQX_Upserter.SQX_Upserter_Config config =  new SQX_Upserter.SQX_Upserter_Config()
                                                                   .omit(SQX.Finding, SQX_Finding__c.Stage__c)
                                                                   .omit(SQX.Finding, SQX_Finding__c.Status__c);

        return findingExtension.executeTransaction(findingId, changeset, objectsToDelete, params, esigData, config, expectation, apiIntegration);
    }

}