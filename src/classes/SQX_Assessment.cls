/**
 * This class will contain the common methods and static strings required for the Assessment object
 */
public  with sharing class SQX_Assessment {
    public static final String  STATUS_DRAFT = 'Draft',
                                STATUS_PRE_RELEASE = 'Pre-Release',
                                STATUS_CURRENT = 'Current',
                                STATUS_PRE_EXPIRE='Pre-Expire',
                                STATUS_OBSOLETE='Obsolete';

    public static final String  APPROVAL_IN_APPROVAL = 'In Approval',
                                APPROVAL_APPROVED = 'Approved',
                                APPROVAL_REJECTED = 'Rejected';

    public static final String  ASSESSMENT_RESULT_SATISFACTORY = 'Satisfactory',
                                ASSESSMENT_RESULT_UNSATISFACTORY = 'Unsatisfactory';

    public static final String  RT_SCORM_ASSESMENT_API_NAME = 'SCORM_Assessment',
                                RT_CQ_ASSESSMENT_API_NAME = 'CQ_Assessment';

    /**
     * Assessment Trigger Handler
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        Bulkified(){}

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newAssessment equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Assessment__c> newAssessment, Map<Id, SQX_Assessment__c> oldMap){
            super(newAssessment, oldMap);
        }

        /**
         * This is a validation that will prevent assessment from going obsolete when this assessment is being used by controlled doc and requirement
         */
        public Bulkified preventAssessmentFromBeingObsoleted() {
            Rule obsoleteRule = new Rule();
            obsoleteRule.addRule(SQX_Assessment__c.Status__c, RuleOperator.Equals, STATUS_OBSOLETE);

            this.resetView()
                .applyFilter(obsoleteRule, RuleCheckMethod.OnCreateAndEveryEdit);

            if(obsoleteRule.evaluationResult.size() > 0) {

                Map<Id, SQX_Assessment__c> assessments = new Map<Id, SQX_Assessment__c>( (List<SQX_Assessment__c>) obsoleteRule.evaluationResult );

                // if there are any documents or any active requirements refereing assessment then add error
                List<SQX_Assessment__c> allAssessments = [ SELECT Id,
                                                                (SELECT Id FROM SQX_Controlled_Documents_Init_Assessment__r WHERE Document_Status__c IN:  SQX_Controlled_Document.DOCUMENT_STATUSES_PREVENTING_ASSESSMENT_OBSOLESCENCE),
                                                                (SELECT Id FROM SQX_Controlled_Documents_Rev_Assessment__r WHERE Document_Status__c  IN:  SQX_Controlled_Document.DOCUMENT_STATUSES_PREVENTING_ASSESSMENT_OBSOLESCENCE),
                                                                (SELECT Id FROM SQX_Controlled_Documents_Ref_Assessment__r WHERE Document_Status__c  IN:  SQX_Controlled_Document.DOCUMENT_STATUSES_PREVENTING_ASSESSMENT_OBSOLESCENCE),
                                                                (SELECT Id FROM SQX_Requirements_Init_Assessment__r WHERE Active__c = true),
                                                                (SELECT Id FROM SQX_Requirements_Ref_Assessment__r WHERE Active__c = true),
                                                                (SELECT Id FROM SQX_Requirements_Rev_Assessment__r WHERE Active__c = true)
                                                            FROM SQX_Assessment__c WHERE Id IN : assessments.keySet() ];

                for (SQX_Assessment__c assessment : allAssessments) {
                    SQX_Assessment__c obj = assessments.get(assessment.Id);
                    if (assessment.SQX_Controlled_Documents_Init_Assessment__r.size() > 0
                        || assessment.SQX_Controlled_Documents_Rev_Assessment__r.size() > 0
                        || assessment.SQX_Controlled_Documents_Ref_Assessment__r.size() > 0) {
                        obj.addError(Label.SQX_ERR_MSG_CAN_NOT_OBSOLETE_ASSESSMENT_BEING_REFERRED);

                    } else if (assessment.SQX_Requirements_Init_Assessment__r.size() > 0
                        || assessment.SQX_Requirements_Ref_Assessment__r.size() > 0
                        || assessment.SQX_Requirements_Rev_Assessment__r.size() > 0) {
                        obj.addError(Label.SQX_ERR_MSG_CAN_NOT_OBSOLETE_ASSESSMENT_BEING_REFERRED_BY_ACTIVE_REQUIREMENT);
                    }
                }
            }
            return this;
        }
    }

    private static Id scormAssessmentRecordTypeId;

    /**
    * Returns the id of the templated doc type
    */
    public static Id getScormAssessmentRecordTypeId(){
        if(scormAssessmentRecordTypeId == null){
            scormAssessmentRecordTypeId = Id.valueOf(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Assessment, RT_SCORM_ASSESMENT_API_NAME));
        }

        return scormAssessmentRecordTypeId;
    }


    /**
     *  Method to associate content version with the given assessment record and
     *  also set a record type
     *  @param assessment - assessment record into which the content is to be checked in
     *  @param cv - version to be checked in
    */
    public static void checkInContent(SQX_Assessment__c assessment, ContentVersion cv){

        ContentVersion cvs = new ContentVersion(Id = cv.Id);
        cvs.RecordTypeId = SQX_ContentVersion.getScormContentRecordTypeId();
        cvs.Assessment__c = assessment.Id;

        new SQX_DB().op_update(new List<ContentVersion> { cvs }, new List<SObjectField> { ContentVersion.RecordTypeId, ContentVersion.Assessment__c });

        // adding content document link such that the content is shared when the assessment record is shared
        addContentDocumentLink(assessment, [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: cvs.Id].ContentDocumentId);
    }

    /**
    *   This method adds the ContentDocumentLink of the assessment content with the assessment record
    *   @param assessment - record to be linked with the content
    *   @param contentDocId - contentdocument id of the assessment's content
    */
    private static void addContentDocumentLink(SQX_Assessment__c assessment, Id contentDocId){

        ContentDocumentLink link = new ContentDocumentLink(
                                        ContentDocumentId = contentDocId,
                                        LinkedEntityId = assessment.Id,
                                        ShareType = 'I');

        new SQX_DB().op_insert(new List<ContentDocumentLink> { link }, new List<SObjectField> {
                                            ContentDocumentLink.ContentDocumentId,
                                            ContentDocumentLink.LinkedEntityId,
                                            ContentDocumentLink.ShareType
                              });

    }

    /**
     *  Method to evaluate if the contents of the given assessments can be updated
     *  @param assessments - list of assessments to be evaluated
    */
    public static Map<Id, Boolean> canUpdateContent(List<SQX_Assessment__c> assessments){

        Map<Id, Boolean> canUpdateContentMap = new Map<Id, Boolean>();
        for(SQX_Assessment__c asmt : assessments){
            canUpdateContentMap.put(asmt.Id, !asmt.Is_Locked__c);
        }

        return canUpdateContentMap;
    }
}