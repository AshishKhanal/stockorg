/**
* class for submitting doument for retire approval except for unit test
*/
public with sharing class SQX_Submit_Doc_For_Retire_Approval {
    
    /**
     * method for submitting doument for retire approval except for unit test
     * @param docs controlled document to be submitted for approval
     */
    @InvocableMethod(label='Submit Document for Retire Approval')
    public static void submitDocForRetireApproval(List<SQX_Controlled_Document__c> docs){
        
        if(!Test.isRunningTest()){
            List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
            for(SQX_Controlled_Document__c doc : docs){
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                req.setObjectId(doc.id);
                requests.add(req);
            }
            
            Approval.process(requests);
        }
    }
}