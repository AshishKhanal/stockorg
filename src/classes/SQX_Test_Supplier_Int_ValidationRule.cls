/**
* Class to test the validation rules of Supplier Interaction
**/
@isTest
public class SQX_Test_Supplier_Int_ValidationRule {
	@testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        System.runAs(standardUser) {

            // create account record
            Account account = new Account(
                Name = SQX_Test_Supplier_Interaction.ACCOUNT_NAME 
            );
            insert account;
            
            //Create contact for Account
            insert new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = account.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
        }
    }
    
    /*
     * GIVEN: Supplier Interaction record in Draft status
     * WHEN: Account is changed
     * THEN: Error is thrown
     */
    static testmethod void whenSupplierIsChangedAfterDraftStage_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser});

        System.runAs(standardUser){
            // Arrange: Create a new Supplier interaction in draft stage
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(SQX_Test_Supplier_Interaction.getAccountAndContactRecords()[0]).save();
            
            Id existingAccountId = interaction.si.SQX_Account__c;
            
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            
            // Act: Change the account
            interaction.si.SQX_Account__c = account.Id;
            interaction.si.SQX_Supplier_Contact__c = contact.Id;
            
            // Assert: No error is thrown
			Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction__c>{interaction.si}, new List<SObjectField>{})[0];  
            System.assertEquals(true, result.isSuccess(), 'No Error should be thrown while changing supplier on draft stage but has error. '+ result.getErrors());
            
            // Act: submit the record and change account
            interaction.submit();
            
            interaction.si.SQX_Account__c = existingAccountId;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Supplier cannot be changed.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while changing Supplier after draft stage');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    
    
    /*
     * GIVEN: New Supplier interaction
     * WHEN: Contact is left blank
     * THEN: Error is thrown
     */
    static testmethod void whenContactIsLeftBlank_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: New Supplier interaction
            SQX_Test_Supplier_interaction interaction = new SQX_Test_Supplier_interaction(SQX_Test_Supplier_Interaction.getAccountAndContactRecords()[0]).save();
            
            // Act: Set the contact to null
            interaction.si.SQX_Supplier_Contact__c = null;
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Supplier Contact is required.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while removing Supplier Contact');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: New Supplier interaction
     * WHEN: Part is left blank
     * THEN: Error is thrown
     */
    static testmethod void whenPartOrServiceIsLeftBlank_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: Create new Supplier interaction
            SQX_Test_Supplier_interaction interaction = new SQX_Test_Supplier_interaction(SQX_Test_Supplier_Interaction.getAccountAndContactRecords()[0]);
            
            // Act: Set the part numbers as blank
            interaction.si.Part_Name__c = null;
            Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Product or Service is required.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved without part or service');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
            
            //Act: Set the part number and save
            interaction.si.Part_Name__c = 'Part1, Part2';
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Assert: No error is thrown
            System.assertEquals(true, result.isSuccess(), 'Record should saved while adding part but has error. ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: New Supplier interaction in open status
     * WHEN: Part is changed
     * THEN: Error is thrown
     */
    static testmethod void whenPartOrServiceIsChangedAfterInitiation_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: Create new supplier interaction in open status
            SQX_Test_Supplier_interaction interaction = new SQX_Test_Supplier_interaction(SQX_Test_Supplier_Interaction.getAccountAndContactRecords()[0]);
            interaction.si.Part_Name__c = 'Part1, Part2';
            interaction.si.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            interaction.si.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_IN_PROGRESS;
            Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Act: Change the part number field
            interaction.si.Part_Name__c = 'Part1, Part2, Part3';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_interaction__c>{interaction.si}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Product and Service cannot be changed.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while initaiting without part or service');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    /**
     * Given : Closed Supplier Interaction
     * When : File is added to Supplier Interaction or Supplier Interaction Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenClosedSupplierInteractionAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a Supplier Interaction Record 
            SQX_Test_Supplier_Interaction sd = new SQX_Test_Supplier_Interaction().save();
           
            // Submitting the Supplier Interaction record
            sd.submit();
            sd.initiate();

            //close  the Supplier Interaction record
            sd.close();

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(sd.si.Id);
        }
    }

    /**
     * Given : Void Supplier Interaction
     * When : File is added to Supplier Interaction or Supplier Interaction Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenVoidSupplierInteractionAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a Supplier Interaction Record 
            SQX_Test_Supplier_Interaction sd = new SQX_Test_Supplier_Interaction().save();
           
            // Submitting the Supplier Interaction record
            sd.submit();

            // void  the Supplier Interaction record
            sd.void();

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(sd.si.Id);
            
        }
    }
    
    /**
     * This common internal private method is used to validate common error message while adding files to locked Supplier Interaction/step records.
     */
    private static void validateErrorWhileAddingFilesToLockedRecords(Id supplierInteractionId) {
        // Act : add file
        ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
        insert cv;
        
        // query contentdocumentid
        Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
        // insert ContentDocumentLink for Supplier Interaction
        Database.SaveResult result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = supplierInteractionId, ShareType = 'V'), false);
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for locked Supplier Interaction record.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());

        // Act : add files (i.e. ContentDocumentLink) for Onboarding Step
        SQX_Supplier_Interaction_Step__c interactionStep = [SELECT Id FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =:supplierInteractionId AND Status__c =: SQX_Supplier_Common_Values.STATUS_DRAFT LIMIT 1];
        result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId, LinkedEntityId = interactionStep.Id, ShareType = 'V'), false);
        
        // Assert: Ensure the error is thrown
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for Supplier Interaction Step whose Supplier Interaction is locked.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());
    }

    /**
     * Given : Supplier Interaction and Supplier Interaction record, File(ContentDocumentLink) is added to Supplier Interaction and Step and Supplier Interaction is locked(closed or voided)
     * When : ContentDocumentLink is tried to delete
     * Then : Error is thrown
     */
    @isTest
    public static void givenLockedSupplierInteractionAndStepWithFile_WhenContentDocumentLinkIsDelted_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange : Create a Supplier Interaction Record 
            SQX_Test_Supplier_Interaction sd = new SQX_Test_Supplier_Interaction().save();

            // add file
            ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
            insert cv;

            // query contentdocumentid
            Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
            // insert ContentDocumentLink for Supplier Interaction
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = sd.si.Id, ShareType = 'V');
            Database.SaveResult result = Database.insert(documentLink, false);
            
            System.assert(result.isSuccess() && documentLink.Id != null, 'Expected file/ContentDocumentLink to be added in Supplier Interaction. Actual Error : ' + result.getErrors());

            // Submitting the Supplier Interaction record
            sd.submit();
            sd.initiate();

            // add files to Supplier Interaction too
            SQX_Supplier_Interaction_Step__c interactionStep = [SELECT Id FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =:sd.si.Id LIMIT 1];
            ContentDocumentLink documentLink_dp = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = interactionStep.Id, ShareType = 'V');
            result = Database.insert(documentLink_dp, false);
            System.assert(result.isSuccess() && documentLink_dp.Id != null, 'Expected file/ContentDocumentLink to be added in Supplier Interaction. Actual Error : ' + result.getErrors());

            // lock record by closeing the Supplier Interaction record
            sd.close();

            // Act : Try to delete ContentDocumentLink for Supplier Interaction by calling common method
            validateDeletionOfContentDocumentLink(documentLink, 'Supplier Interaction');

            // Act : Try to delete ContentDocumentLink for Escalation Step by calling common method
            validateDeletionOfContentDocumentLink(documentLink_dp, 'Escalation Step');
        }
    }

    /**
     * Common method to ensure that the deletion of ContentDocumentLink locked Supplier Interaction and Escalation Step is being prevented
     */
    public static void validateDeletionOfContentDocumentLink(ContentDocumentLink cdLink, String objName) {

        // Act : try to delete ContentDocumentLink
        Database.DeleteResult deleteResult = Database.delete(cdLink, false);
        
        // Assert : Ensure ContentDocumentLink is not deleted and system throws expected validation error.
        System.assert(deleteResult.isSuccess() == false, 'Expected ContentDocumentLink not to be deleted from' + objName + ' but is deleted.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, deleteResult.getErrors().get(0).getMessage());
    }
}