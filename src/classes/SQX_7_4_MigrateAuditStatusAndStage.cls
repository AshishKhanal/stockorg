/**
* Batch processor to change Audit (SQX_Audit__c) Status and add Stage
* Added in version  : 7.4.0
* Used for versions : all earlier versions
*/
global with sharing class SQX_7_4_MigrateAuditStatusAndStage implements Database.Batchable<sObject> {

    public static final String MIGRATION_FAILED = '7.4 Migration Failed';
    public static final String MIGRATION_COMPLETED = '7.4 Migration Completed';

    public static final String AUDIT_RESULT_DEFAULT = 'NA',
                               RESULT_TYPE_DEFAULT = 'Audit Result';

    //Deprecated Old Status
    public static final String OLD_STATUS_PLAN = 'Plan',
                               OLD_STATUS_PLAN_APPROVAL = 'Plan Approval',
                               OLD_STATUS_SCHEDULED = 'Scheduled',
                               OLD_STATUS_CONFIRMED = 'Confirmed',
                               OLD_STATUS_IN_PROGRESS = 'In Progress',
                               OLD_STATUS_COMPLETE = 'Complete',
                               OLD_STATUS_CLOSE = 'Closed',
                               OLD_STATUS_VOID = 'Void';

    //Map to set Old Status to New Status
    public static final Map<String, String> OLD_STATUS_TO_NEW_STATUS_MAP = new Map<String, String>{
        OLD_STATUS_PLAN => SQX_Audit.STATUS_DRAFT,
        OLD_STATUS_PLAN_APPROVAL => SQX_Audit.STATUS_DRAFT,
        OLD_STATUS_SCHEDULED => SQX_Audit.STATUS_OPEN,
        OLD_STATUS_CONFIRMED => SQX_Audit.STATUS_OPEN,
        OLD_STATUS_IN_PROGRESS => SQX_Audit.STATUS_OPEN
    };

    //Map to set Stage according to Old Status
    public static final Map<String, String> STAGE_FROM_OLD_STATUS_MAP = new Map<String, String>{
        OLD_STATUS_PLAN_APPROVAL => SQX_Audit.STAGE_PLAN_APPROVAL,
        OLD_STATUS_SCHEDULED => SQX_Audit.STAGE_SCHEDULED,
        OLD_STATUS_CONFIRMED => SQX_Audit.STAGE_CONFIRMED,
        OLD_STATUS_CLOSE => SQX_Audit.STAGE_CLOSED
    };

    /**
    * Gets invoked when the batch job starts and selects all audit whose CQ Version is not 7.4
    * @param bc reference to the Database.BatchableContext object
    * @return Database.QueryLocator object that contains audits passed to the job
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Status of all Audit whose Migration Status is Failed
        // we are not migrating voided records
        return Database.getQueryLocator([SELECT Id, 
                                                Status__c, 
                                                SQX_Audit_Program__c,
                                                SQX_Audit_Program__r.Approval_Status__c,Audit_Result__c,
                                                (SELECT Id FROM SQX_Audit_Reports__r WHERE Status__c =: SQX_Audit_Report.STATUS_IN_APPROVAL LIMIT 1 )
                                         FROM SQX_Audit__c
                                         WHERE Batch_Job_Status__c !=: MIGRATION_COMPLETED
                                            AND Status__c != :OLD_STATUS_VOID
        ]);
    }

    /** 
    * Execute the method to set new status and stage in audit according to old status
    * @param bc reference to the Database.BatchableContext object
    * @param scope list of sObjects containing audits
    */
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        if(scope.size()>0){

            //List of Audits
            List<SQX_Audit__c> audits = SetAuditValuesToUpdate(scope);

            //If any Audit exist update Audit
            if(!audits.isEmpty()){
                //List for audits which failed in migration
                List<SQX_Audit__c> failedAudits = new List<SQX_Audit__c>();

                /**
                * Without Sharing is used because migration should affect all data
                * Update continues even when error occurs during batch update
                */
                Database.SaveResult[] saveResults = new SQX_DB().withoutSharing().continueOnError().op_update(addMigrationStatus(audits), new List<SObjectField>{SQX_Audit__c.Status__c, SQX_Audit__c.Stage__c, SQX_Audit__c.Batch_Job_Status__c});
                for(Integer i = 0; i < saveResults.size(); i++){
                    Database.SaveResult saveResult = saveResults[i];
                    SQX_Audit__c audit = audits[i];

                    //If any Audit is failed to migrate then add status of Migration Failed
                    if(!saveResult.isSuccess()){
                        SQX_Audit__c failedAudit = new SQX_Audit__c();
                        failedAudit.Id = audit.Id;
                        failedAudit.Batch_Job_Status__c = MIGRATION_FAILED;
                        failedAudit.Batch_Job_Error__c = SQX_Utilities.getFormattedErrorMessage(saveResult.getErrors());
                        failedAudits.add(failedAudit);
                    }
                }

                //If any Audit failed to migrate exist update Audit
                if(!failedAudits.isEmpty()){

                    /**
                    * Without Sharing is used because migration should affect all data
                    * Update is halted and reverted if error occurs during batch update
                    */
                    new SQX_DB().withoutSharing().op_update(failedAudits, new List<SObjectField>{SQX_Audit__c.Batch_Job_Status__c, SQX_Audit__c.Batch_Job_Error__c});
                }
            }
        }
    }

    /**
    * Used to add Migration Status in all audits
    * @param {audits} is the list of audit which contains all audits in which migration status is to be added
    * @return {auditWithMigrationStatus} is the list of audit with migration status set
    */
    public List<SQX_Audit__c> addMigrationStatus(List<SQX_Audit__c> audits){
        List<SQX_Audit__c> auditWithMigrationStatus = new List<SQX_Audit__c>();
        for(SQX_Audit__c audit : audits){
            audit.Batch_Job_Status__c = MIGRATION_COMPLETED;
            auditWithMigrationStatus.add(audit);
        }
        return auditWithMigrationStatus;
    }

    /**
    * check old status and stage then set values to update
    * @param 'scope' list of ausit record(s)
    */ 
    public List<SQX_Audit__c> SetAuditValuesToUpdate(List<sObject> scope){

        List<SQX_Audit__c> audits = new List<SQX_Audit__c>();
        //List of Audit Ids for status complete
        List<Id> completeAuditIds = new List<Id>();
        
        //Loop through each Audits and set new Status and Stage
        for(SQX_Audit__c audit: (List<SQX_Audit__c>)scope){
            String oldStatus = audit.Status__c;
            
            //Check if status is complete or not
            if(oldStatus == OLD_STATUS_COMPLETE){
                completeAuditIds.add(audit.Id);
            } else{

                if (String.isBlank(audit.Audit_Result__c)) {
                    audit.Audit_Result__c = AUDIT_RESULT_DEFAULT;
                }

                //set default value as 'Audit Result'
                audit.Result_Type__c = RESULT_TYPE_DEFAULT;

                //Set stage if old status of Audit is Draft
                if(oldStatus == OLD_STATUS_PLAN){
                    if(audit.SQX_Audit_Program__r.Approval_Status__c == SQX_Audit_Program.APPROVAL_STATUS_PLAN_APPROVAL){
                        audit.Stage__c = SQX_Audit.STAGE_PROGRAM_APPROVAL;
                    }
                    else{
                        audit.Stage__c = SQX_Audit.STAGE_PLAN;
                    }
                }
                
                //Set stage if old status of Audit is In Progress
                if(oldStatus == OLD_STATUS_IN_PROGRESS){
                    if(audit.SQX_Audit_Reports__r.size() > 0){
                        audit.Stage__c = SQX_Audit.STAGE_FINDING_APPROVAL;
                    }
                    else{
                        audit.Stage__c = SQX_Audit.STAGE_IN_PROGRESS;
                    }
                }
                
                //Set new status by comparing with old status
                if(OLD_STATUS_TO_NEW_STATUS_MAP.containsKey(oldStatus)){
                    audit.Status__c = OLD_STATUS_TO_NEW_STATUS_MAP.get(oldStatus);
                }
                
                //Set stage by comparing with old status except for status Draft, In Progress, Complete and Void
                if(STAGE_FROM_OLD_STATUS_MAP.containsKey(oldStatus)){
                    audit.Stage__c = STAGE_FROM_OLD_STATUS_MAP.get(oldStatus);
                }
                audits.add(audit);
            }
        }
        
        //If Audit with status Complete is not empty 
        if(!completeAuditIds.isEmpty()){
            List<SQX_Audit__c> computeAuditList = SQX_Stage_Setter.computeAuditStage(completeAuditIds);
            for(SQX_Audit__C audit : computeAuditList){
                if(String.isBlank(audit.Audit_Result__c)) {
                    audit.Audit_Result__c = AUDIT_RESULT_DEFAULT;
                }
                audit.Result_Type__c = RESULT_TYPE_DEFAULT;
                audits.add(audit);
            }
        }
        return audits;
    }
    
    /** 
    * Used to execute post-processing operations and is called once after all batches are processed
    * @param bc reference to the Database.BatchableContext object
    */
    global void finish(Database.BatchableContext bc){}

}