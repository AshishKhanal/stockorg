/**
 * Test class for creating regulatory report
 */
@IsTest
public class SQX_Test_7283_Create_Regulatory_Report {
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
    }
    
    /**
     * Given : Reportable flag is true for Answer Option Attribute Record
     * When : Try to insert Answer Option Attribute 
     * Then : Due_In_Days__c, Report_Name__c and Reg_Body__c fields are required.
     */
    static testmethod void ensureThatReportableFlagHasNeededFieldsWhileCreatingAnswerOptionAttributeRecords() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Create a CQ Task
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task.task.SQX_User__c = standardUser.Id;
            task.save();
            
            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();
            
            // ARRANGE : Create Answer Option
            SQX_Answer_Option__c answerOption = task.addAnswer(question, null, false);
            
            // ARRANGE : Create Answer Option Attributes with reportable flag checked and all required fields.
            SQX_Answer_Option_Attribute__c answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true);
            // Assert : Ensure the record is saved without any error.
            Database.SaveResult result = Database.insert(answerOptionAttribute, false);
            System.assert(result.isSuccess(), 'Expected Answer Option Attribute record to be saved but is failed with error ' + result.getErrors());

            // Act : try to insert record without due report name but reportable flag checked
            answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Reportable__c = true);
            
            // Asssert : ensure validation error occurs with required validation error.
            result = Database.insert(answerOptionAttribute, false);
            System.assert(result.isSuccess() == false, 'Expected Answer Option Attribute record not to be saved but is being saved');
            final String errorMsg = 'Report Name, Regulatory body and Due in days must be provided for a reportable answer option attribute.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), errorMsg), 'Expected ' + errorMsg + ' got ' + result.getErrors());        
        }
    }

    /**
     * GIVEN : Decision Tree with Answer Option Attribute having reportable flag checked.
     * WHEN : Create Regulatory Report is invoked
     * THEN  :  Regulatory Reports is created as per number of Answer Option Attribute
    */
    static testmethod void givenDecisionTreeWithAnswerOptionAttribute_WhenCreateRegulatoryReportIsInvoked_ThenUniqueRegulatoryReportsAreCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Create a CQ Task
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task.task.SQX_User__c = standardUser.Id;
            task.save();
            
            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();
            
            // ARRANGE : Create Answer Option
            SQX_Answer_Option__c answerOption = task.addAnswer(question, null, false);
            
            // ARRANGE : Create Answer Option Attributes without specifying country
            SQX_Answer_Option_Attribute__c answerOptionAttribute1 = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true,
                                                                                                      Report_Type__c = 'Initial report');
            
            // create answer option attribute with country different than country or complaint
            SQX_Answer_Option_Attribute__c answerOptionAttribute2 = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true,
                                                                                                      Country__c = 'AF');
            
            // ARRANGE : Create Answer Option Attributes with specifying country
            SQX_Answer_Option_Attribute__c answerOptionAttribute3 = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                       Attribute_Type__c = 'Sample Attribute Type',
                                                                       Value__c = 'Test Value 1',
                                                                       Due_In_Days__c = 1,
                                                                       Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                       Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                       Reportable__c = true,
                                                                       Country__c = 'US');
            
            // ARRANGE : Create another Answer Option Attributes having same attribute type and value and with specifying country
            SQX_Answer_Option_Attribute__c answerOptionAttribute4 = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                       Attribute_Type__c = 'Sample Attribute Type',
                                                                       Value__c = 'Test Value 1',
                                                                       Due_In_Days__c = 1,
                                                                       Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_EMA,
                                                                       Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_2_DAY_EMA,
                                                                       Reportable__c = true,
                                                                       Country__c = 'US');

            // Create another Answer Option Attributes having reportable false
            SQX_Answer_Option_Attribute__c answerOptionAttribute5 = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                       Attribute_Type__c = 'Sample Attribute Type',
                                                                       Value__c = 'Test Value 1',
                                                                       Due_In_Days__c = 1,
                                                                       Reportable__c = false,
                                                                       Country__c = 'US');
            // insert answer option attribute
            insert new List<SQX_Answer_Option_Attribute__c> { answerOptionAttribute1, 
                                                              answerOptionAttribute2, 
                                                              answerOptionAttribute3, 
                                                              answerOptionAttribute4,
                                                              answerOptionAttribute5
                                                          };
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Aware_Date__c = System.today();
            complaint.save();
            SQX_Complaint__c comp = [SELECT Id, OwnerId FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            
            // ARRANGE : Create Decision Tree
            SQX_Decision_Tree__c decisionTree = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint.complaint.Id,
                                                                         SQX_Task__c = task.task.Id);
            insert decisionTree;
            
            // ARRANGE : Create Decision Tree Answer
            SQX_DT_Answer__c decisionTreeAnswer = new SQX_DT_Answer__c(SQX_Decision_Tree__c = decisionTree.Id, 
                                                                       SQX_Answer_Option__c = answerOption.Id);
            insert decisionTreeAnswer;
            
            // ACT : Invoke Create Regulatory Report method
            SQX_Create_Regulatory_Report.createRegulatoryReportBasedOnAnswerOptionAttribute(new List<SQX_Decision_Tree__c>{decisionTree});
            
            List<SQX_Regulatory_Report__c> regulatoryReport = [SELECT Id, SQX_Complaint__c, SQX_Decision_Tree__c, Due_Date__c, Name, Reg_Body__c, SQX_User__c, Report_Type__c FROM SQX_Regulatory_Report__c ORDER BY Name ASC]; 
            
            // ASSERT : Two regulatory reports should be created as there are 4 reportable attributes and among 4,
            // one attribute is unique with attribute type and value,
            // two attributes are duplicate with same attribute type and value whic results 1 report,
            // one has different country than complaint which results no report
            System.assertEquals(2, regulatoryReport.size());
            System.assertEquals(SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA, regulatoryReport[0].Name, 'Expected Name of Regulatory Report to be EU:2-Day Reportable but got ' +  regulatoryReport[0].Name);
            System.assertEquals(complaint.complaint.Id, regulatoryReport[0].SQX_Complaint__c, 'Expected Id of Complaint of Regulatory Report to be ' + complaint.complaint.Id + ' but got ' +  regulatoryReport[0].SQX_Complaint__c);
            System.assertEquals(decisionTree.Id, regulatoryReport[0].SQX_Decision_Tree__c, 'Expected id SQX_Decision_Tree__c of Regulatory Report to be ' + decisionTree.Id + ' but got ' +  regulatoryReport[0].SQX_Decision_Tree__c);
            System.assertEquals(comp.OwnerId, regulatoryReport[0].SQX_User__c, 'Expected Assignee User Id of Regulatory Report to be ' +comp.OwnerId + ' but got ' +  regulatoryReport[0].SQX_User__c);
            System.assertEquals(SQX_REgulatory_Report.REGULATORY_BODY_EMA, regulatoryReport[0].Reg_Body__c, 'Expected Regulatory Body of Regulatory Report to be EMA but got ' +  regulatoryReport[0].Name);
            
            System.assertEquals(SQX_Regulatory_Report.REPORT_NAME_30_DAY_MDR, regulatoryReport[1].Name, 'Expected Name of Regulatory Report to be US:30-Day Reportable but got ' +  regulatoryReport[1].Name);
            System.assertEquals(complaint.complaint.Id, regulatoryReport[1].SQX_Complaint__c, 'Expected Id of Complaint of Regulatory Report to be ' + complaint.complaint.Id + ' but got ' +  regulatoryReport[1].SQX_Complaint__c);
            System.assertEquals(decisionTree.Id, regulatoryReport[1].SQX_Decision_Tree__c, 'Expected id SQX_Decision_Tree__c of Regulatory Report to be ' + decisionTree.Id + ' but got ' +  regulatoryReport[1].SQX_Decision_Tree__c);
            System.assertEquals(comp.OwnerId, regulatoryReport[1].SQX_User__c, 'Expected Assignee User Id of Regulatory Report to be ' +comp.OwnerId + ' but got ' +  regulatoryReport[1].SQX_User__c);
            System.assertEquals(SQX_REgulatory_Report.REGULATORY_BODY_FDA, regulatoryReport[1].Reg_Body__c, 'Expected Regulatory Body of Regulatory Report to be FDA but got ' +  regulatoryReport[1].Reg_Body__c);
            System.assertEquals(answerOptionAttribute1.Report_Type__c, regulatoryReport[1].Report_Type__c, 'Expected Type of Regulatory Report to be :'+answerOptionAttribute1.Report_Type__c+' but got:' +  regulatoryReport[1].Report_Type__c);

        }
    }
    
    /**
     * Given : Script Execution Record
     * When : User tries to update Responses_Saved__c or Complaint field of Script Execution
     * Then : record is updated successfully.
     * 
     * When : User tries to update other fields of Script Execution
     * Then : Validation error occurs
     */
    static testmethod void ensureStandardUserCanUpdateScriptExecutionRecordThroughInvocableMethod() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ARRANGE : Create a Script Execution record
            SQX_Script_Execution__c scriptExecution = new SQX_Script_Execution__c(Name = 'Sample Execution');
            insert scriptExecution; 

            // Act : call privilege escalated invocable method to update the record
            scriptExecution.Responses_Saved__c = true;
            scriptExecution.SQX_Complaint__c = complaint.complaint.Id;
            
            Database.SaveResult result = Database.update(scriptExecution,false);
            System.assert(result.isSuccess(), 'Expected Responses_Saved__c to be updated but is not updated');

            // Assert : Ensure that the record updated successfully
            SQX_Script_Execution__c scriptExecutionUpdated = [SELECT Id, Responses_Saved__c, SQX_Complaint__c FROM SQX_Script_Execution__c WHERE Id = :scriptExecution.Id];
            System.assert(scriptExecutionUpdated.Responses_Saved__c == true, 'Expected Responses_Saved__c flag to be true but got ' + scriptExecutionUpdated.Responses_Saved__c);
            System.assertEquals(complaint.complaint.Id,scriptExecutionUpdated.SQX_Complaint__c,'Complaint should be updated in Execution Script');
            
            // Act : tries to update other field
            scriptExecutionUpdated.Comment__c = 'Sample Comment__c';
            result = Database.update(scriptExecutionUpdated,false);
            System.assert(result.isSuccess() == false, 'ExpectedScript Execution not to be updated but is being updated');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Script Execution record cannot be updated once it has been created.'), 'Expected Script Execution record cannot be updated once it has been created. got ' + result.getErrors());
            
            // Act : tries to update complaint field
            SQX_Test_Complaint complaint2 = new SQX_Test_Complaint(standardUser).save();
            scriptExecutionUpdated.SQX_Complaint__c = complaint2.complaint.Id;
            result = Database.update(scriptExecutionUpdated,false);
            System.assert(result.isSuccess() == false, 'ExpectedScript Execution not to be updated but is being updated');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Script Execution record cannot be updated once it has been created.'), 'Expected Script Execution record cannot be updated once it has been created. got ' + result.getErrors());
        }
    }
    
    /**
     * Given : Decision Tree Record
     * When : User tries to update DT_Answers_Saved__c field of Decision Tree
     * Then : record is updated successfully.
     *
     * When : User tries to update other fields of Decision Tree
     * Then : Validation error occurs.
     */
    static testmethod void ensureStandardUserCanUpdateDTAnswerRecordThroughInvocableMethod() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {

            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Aware_Date__c = System.today();
            complaint.save();
            
            // ARRANGE : Create Decision Tree
            SQX_Decision_Tree__c decisionTree = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint.complaint.Id);
            insert decisionTree;

            // Act : call privilege escalated invocable method to update the record
            decisionTree.DT_Answers_Saved__c = true;
            Database.SaveResult result = Database.update(decisionTree,false);
            System.assert(result.isSuccess(), 'Expected DT_Answers_Saved__c to be updated but is not updated');

            // Assert : Ensure that the record updated successfully
            SQX_Decision_Tree__c decisionTreeUpdated = [SELECT Id, DT_Answers_Saved__c FROM SQX_Decision_Tree__c WHERE Id = :decisionTree.Id];
            System.assert(decisionTreeUpdated.DT_Answers_Saved__c == true, 'Expected DT_Answers_Saved__c flag to be true but got ' + decisionTreeUpdated.DT_Answers_Saved__c);

            // Act : tries to update other fields
            decisionTree.Comment__c = 'Sample Comment__c';
            result = Database.update(decisionTree,false);
            System.assert(result.isSuccess() == false, 'Expected Decision Tree not to be updated but is being updated');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Decision Tree record cannot be updated once it has been created.'), 'Expected Decision Tree record cannot be updated once it has been created. got ' + result.getErrors());
        }
    }
    
    /**
     * Given : Complaint is created
     * When : Regulatory Report is deleted created from Decision Tree
     * Then : Error is thrown
     */
    static testmethod void givenComplaint_WhenFormNameIsChangedCreatedFromDT_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // ARRANGE : Create a CQ Task
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task.task.SQX_User__c = standardUser.Id;
            task.save();
            
            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();
            
            // ARRANGE : Create Answer Option
            SQX_Answer_Option__c answerOption = task.addAnswer(question, null, false);
            
            // ARRANGE : Create Answer Option Attributes with reportable flag checked and all required fields.
            SQX_Answer_Option_Attribute__c answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 1,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true);
            Database.insert(answerOptionAttribute, false);
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.Aware_Date__c = System.today();
            complaint.save();
            
            // ARRANGE : Create Decision Tree
            SQX_Decision_Tree__c decisionTree = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint.complaint.Id,
                                                                         SQX_Task__c = task.task.Id);
            insert decisionTree;
            
            // ARRANGE : Create Decision Tree Answer
            SQX_DT_Answer__c decisionTreeAnswer = new SQX_DT_Answer__c(SQX_Decision_Tree__c = decisionTree.Id, 
                                                                       SQX_Answer_Option__c = answerOption.Id);
            insert decisionTreeAnswer;
            
            SQX_Create_Regulatory_Report.createRegulatoryReportBasedOnAnswerOptionAttribute(new List<SQX_Decision_Tree__c>{decisionTree});

            // ACT : Form name of regulatory information is changed created from Decision Tree
            SQX_Regulatory_Report__c reportWithDT = [SELECT Id FROM SQX_Regulatory_Report__c WHERE SQX_Decision_Tree__c =: decisionTree.Id];

            // ACT : Regulatory report is deleted linked with decision tree
            Database.DeleteResult deleteResult = Database.delete(reportWithDT, false);

            // ASSERT : Error is thrown
            System.assertEquals(deleteResult.isSuccess(), false, 'Delete should not be successful.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(deleteResult.getErrors(), Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT));

            // ACT : Form name of regulatory information is changed created
            SQX_Regulatory_Report__c reportWithoutDT = new SQX_Regulatory_Report__c(Name = 'Report without DT', SQX_Complaint__c = complaint.complaint.Id, Due_Date__c = Date.today().addDays(10));
            Database.insert(reportWithoutDT, false);

            // ACT : Regulatory report without DT (manually created) should not be deleted
            deleteResult = Database.delete(reportWithoutDT, false);

            // ASSERT : Regulatory report should not be deleted
            System.assertEquals(false, deleteResult.isSuccess(), 'Regulatory Reports cannot be deleted.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(deleteResult.getErrors(), Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT), 'Expected error: ' + Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT);
            
            
            // ACT (SQX-8211): update SQX_DT_Answer__c
            update decisionTreeAnswer;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_DT_Answer__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_DT_Answer__c.SObjectType);
        }
    }
}