/*
* Apex class to clone records
*/
global with sharing class SQX_Clone_Records {
    public static final String DEFAULT_FIELD_INCLUSION_FIELDSET = 'compliancequest__CQ_Clone_Inclusion_FieldSet';
    public static final String DEFAULT_FIELD_EXCLUSION_FIELDSET= 'compliancequest__CQ_Clone_Exclusion_FieldSet';


    private final String FIELD_ID = 'Id';

    /**
    * Clone records by providing a  configuration
   */
    @InvocableMethod(label='Clone Record' description='This method clones record based upon the information present in configuration')
    global static List<Id> cloneRecord(List<SQX_Clone_Configuration> configurations) {

        SQX_Clone_Records cloner = new SQX_Clone_Records();

        SQX_Clone_Configuration configuration = cloner.flattenConfigurations(configurations);

        List<SObject> clonedRecords = cloner.getClonedRecords(configuration);

        new SQX_DB().op_insert(clonedRecords, new List<SObjectField>());

        return new List<Id>(new Map<Id, sObject>(clonedRecords).keyset());
    }

    /**
    * Clone records based on the configuration provided
   */
    public List<SObject> getClonedRecords(SQX_Clone_Configuration configuration) {

        List<Id> idsToClone = configuration.ids;

        if(idsToClone.size() == 0){
            throw new SQX_ApplicationGenericException('At least one id is required to proceed further.');
        }

        SObjectType objType = idsToClone[0].getSObjectType();
        DescribeSObjectResult objDescribe = objType.getDescribe();

        return getClonedRecords(configuration, objDescribe, FIELD_ID);
    }


    public List<SObject> getClonedRecords(SQX_Clone_Configuration configuration, DescribeSObjectResult objDescribe, String filterField) {

        List<Id> idsToClone = configuration.ids;

        Integer pageSize = filterField == FIELD_ID ? idsToClone.size() : 100;
        Integer offset = 0;

        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_IN;
        filter.value = 'idsToClone';
        filter.field = filterField;

        String query='';
        String fieldsetName='';

        if(configuration.useInclusionFieldset){

            fieldsetName =  String.isEmpty(configuration.fieldSetName) ? DEFAULT_FIELD_INCLUSION_FIELDSET : configuration.fieldSetName;
            query =  SQX_DynamicQuery.prepareQuery(offset,
                                                   pageSize,
                                                   objDescribe,
                                                   new List<SObjectField>(SQX_Utilities.getFieldsFromFieldSet(objDescribe, fieldsetName)),
                                                   filter,
                                                   new List<SObjectField>(),
                                                   false);
        }else{

            fieldsetName =  String.isEmpty(configuration.fieldSetName) ? DEFAULT_FIELD_EXCLUSION_FIELDSET : configuration.fieldSetName;
            query =  SQX_DynamicQuery.prepareAllFieldsQuery(offset,
                                                            pageSize,
                                                            objDescribe,
                                                            SQX_Utilities.getFieldsFromFieldSet(objDescribe, fieldsetName),
                                                            filter,
                                                            new List<SObjectField>(),
                                                            false);
        }

        List<SObject> clonedRecords = cloneRecords(Database.query(query), configuration);

        mergeRecords(clonedRecords, configuration, objDescribe);

        return clonedRecords;
    }


    /**
    * Method to merge multiple configuration to single configuration
    */
    private SQX_Clone_Configuration flattenConfigurations(List<SQX_Clone_Configuration> configurations) {
        SQX_Clone_Configuration flattenedConfig = new SQX_Clone_Configuration();
        Set<Id> idsToClone = new Set<Id>();

        for(SQX_Clone_Configuration configuration : configurations) {
            if((flattenedConfig.relationalField != configuration.relationalField && !String.isEmpty(flattenedConfig.relationalField))
               ||
               (flattenedConfig.fieldSetName != configuration.fieldSetName && !String.isEmpty(flattenedConfig.fieldSetName))
               ||
               (flattenedConfig.jsonDefaults != configuration.jsonDefaults && !String.isEmpty(flattenedConfig.jsonDefaults))
               ||
               (flattenedConfig.useInclusionFieldset != configuration.useInclusionFieldset && flattenedConfig.useInclusionFieldset!=null)
              ) {
                  throw new SQX_ApplicationGenericException('Multiple configurations aren\'t supported.');
              }

            flattenedConfig.relationalField = configuration.relationalField;
            flattenedConfig.fieldSetName = configuration.fieldSetName;
            flattenedConfig.jsonDefaults = configuration.jsonDefaults;

            if(configuration.useInclusionFieldset==null){
                flattenedConfig.useInclusionFieldset=false;
            }else{
                flattenedConfig.useInclusionFieldset=configuration.useInclusionFieldset;
            }

            idsToClone.addAll((List<Id>)configuration.ids);
        }

        flattenedConfig.ids = new List<Id>(idsToClone);

        return flattenedConfig;

    }


    /**
    * Method to merge clonedRecords and deserialized jsonValue records
    */
    private List<SObject> mergeRecords(List<SObject> clonedRecords, SQX_Clone_Configuration configuration, DescribeSObjectResult objDescribe) {
        Map<String, Object> deSerializedMap = (Map<String, Object>) JSON.deserializeUntyped(configuration.jsonDefaults);
        Map<String, SObjectField> allFields = objDescribe.fields.getMap();

        for(SObject clonedRecord : clonedRecords) {
            for(String fieldName : deSerializedMap.keySet()) {
                Object fieldValue = deSerializedMap.get(fieldName);
                if(allFields.get(fieldName).getDescribe().getType() == Schema.DisplayType.DATE) {
                    fieldValue = Date.valueOf(fieldValue.toString());
                } else if(allFields.get(fieldName).getDescribe().getType() == Schema.DisplayType.BOOLEAN) {
                    fieldValue = Boolean.valueOf(fieldValue.toString());
                }else if(allFields.get(fieldName).getDescribe().getType() == Schema.DisplayType.DOUBLE){
                    fieldValue = Double.valueOf(fieldValue);
                }
                clonedRecord.put(fieldName, fieldValue);
            }
        }

        return clonedRecords;
    }


    /**
    * Method to clone a list of records
    */
    @TestVisible
    private List<SObject> cloneRecords(List<SObject> originals, SQX_Clone_Configuration configuration) {
        List<SObject> clonedRecords = new List<SObject>();
        for(SObject original : originals) {
            SObject clone = original.clone(false, false, false, false);
            if(!String.isEmpty(configuration.relationalField)) {
                clone.put(configuration.relationalField, original.Id);
            }
            clonedRecords.add(clone);
        }
        return clonedRecords;
    }
}