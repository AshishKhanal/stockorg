/**
* used to render editable related list of the various sobject
*/
public with sharing class SQX_ObjectWrapper{
    public integer serialNumber { get; set; }
    public boolean isSelected { get; set; }
    public sObject baseObject { get; set; }
    public boolean markForDeletion { get; set; }
    public boolean isNewObject { get; set; }
    
    public SQX_ObjectWrapper(){
        isNewObject = false;
    }
}