/*
 * Test class to check SQX_7_0_PdjfGeneration batch class functionality
 */
@IsTest
public class SQX_Test_2883_PdjfGeneration {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
            
    // array of level of competency
    private static final String[] locList = new String[]{SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                            SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                            SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT};
            
    // array of training status                                
    private static final String[] pdtStatusList = new String[]{SQX_Personnel_Document_Training.STATUS_PENDING,
                                                                SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                                                                SQX_Personnel_Document_Training.STATUS_COMPLETE};
    
    /**
    *   Given : active requirements and job functions 
    *   When  : SQX_7_0_PdjfGeneration batch class executes
    *   Then  : Personnel Document Job Function is generated
    *   @author - Sajal Joshi
    *   @date - 04/07/2017
    *   @story - SQX-2883
    */
    public testmethod static void whenSQX_7_0_PdjfGenerationBatchClassExecutes_ThenPdjfIsGenerated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_DB db = new SQX_DB();
            
            // Arrange: Add required pjf, requirement, pdt, jf and document
            List<SQX_Personnel__c> psnList = new List<SQX_Personnel__c>();
            List<SQX_Job_Function__c> jfList = new List<SQX_Job_Function__c>();
            List<SQX_Controlled_Document__c> docList = new List<SQX_Controlled_Document__c>();
            
            // bypass pdt creation when requirement and personnel job function are inserted
            SQX_Personnel_Document_Job_Function.bypassPdjfCreationForMigrationUnitTest = true;
            
            for(Integer i=0; i<3; i++){
                // Add required job functions
                SQX_Job_Function__c jf = new SQX_Job_Function__c(Name = 'JF', Active__c = true);
                jfList.add(jf);
                
                // Add required personnels
                SQX_Test_Personnel psn= new SQX_Test_Personnel();
                psnList.add(psn.mainRecord);
                
                // Add required controlled document
                SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
                doc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
                docList.add(doc.doc);
            }
            db.op_insert( jfList , new List<Schema.SObjectField>{});
            db.op_insert( psnList , new List<Schema.SObjectField>{});
            db.op_insert( docList , new List<Schema.SObjectField>{});
            
            // Add required personnel job function
            List<SQX_Personnel_Job_Function__c> pjfList = new List<SQX_Personnel_Job_Function__c>();
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c();
            pjf1.Active__c = true;
            pjf1.SQX_Job_Function__c = jfList[0].id;
            pjf1.SQX_Personnel__c = psnList[0].id;
            pjfList.add(pjf1);
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c();
            pjf2.Active__c = true;
            pjf2.SQX_Job_Function__c = jfList[1].id;
            pjf2.SQX_Personnel__c = psnList[0].id;
            pjfList.add(pjf2);
            SQX_Personnel_Job_Function__c pjf3 = new SQX_Personnel_Job_Function__c();
            pjf3.Active__c = true;
            pjf3.SQX_Job_Function__c = jfList[1].id;
            pjf3.SQX_Personnel__c = psnList[1].id;
            pjfList.add(pjf3);
            SQX_Personnel_Job_Function__c pjf4 = new SQX_Personnel_Job_Function__c();
            pjf4.Active__c = true;
            pjf4.SQX_Job_Function__c = jfList[0].id;
            pjf4.SQX_Personnel__c = psnList[2].id;
            pjfList.add(pjf4);
            SQX_Personnel_Job_Function__c pjf5 = new SQX_Personnel_Job_Function__c();
            pjf5.Active__c = true;
            pjf5.SQX_Job_Function__c = jfList[2].id;
            pjf5.SQX_Personnel__c = psnList[2].id;
            pjfList.add(pjf5);
            db.op_insert( pjfList , new List<Schema.SObjectField>{});
            
            // Add required requirements
            List<SQX_Requirement__c> reqList = new List<SQX_Requirement__c>();
            SQX_Requirement__c req1 = new SQX_Requirement__c();
            req1.SQX_Controlled_Document__c = docList[0].id;
            req1.SQX_Job_Function__c = jfList[0].id;
            req1.Level_Of_Competency__c = locList[0];
            req1.Optional__c = true;
            req1.Active__c = true;
            reqList.add(req1);
            SQX_Requirement__c req2 = new SQX_Requirement__c();
            req2.SQX_Controlled_Document__c = docList[0].id;
            req2.SQX_Job_Function__c = jfList[1].id;
            req2.Level_Of_Competency__c = locList[1];
            req2.Optional__c = false;
            req2.Active__c = true;
            reqList.add(req2);
            SQX_Requirement__c req3 = new SQX_Requirement__c();
            req3.SQX_Controlled_Document__c = docList[0].id;
            req3.SQX_Job_Function__c = jfList[2].id;
            req3.Level_Of_Competency__c = locList[2];
            req3.Optional__c = true;
            req3.Active__c = true;
            reqList.add(req3); 
            SQX_Requirement__c req4 = new SQX_Requirement__c();
            req4.SQX_Controlled_Document__c = docList[1].id;
            req4.SQX_Job_Function__c = jfList[0].id;
            req4.Level_Of_Competency__c = locList[0];
            req4.Optional__c = true;
            req4.Active__c = true;
            reqList.add(req4);
            SQX_Requirement__c req5 = new SQX_Requirement__c();
            req5.SQX_Controlled_Document__c = docList[1].id;
            req5.SQX_Job_Function__c = jfList[2].id;
            req5.Level_Of_Competency__c = locList[2];
            req5.Active__c = true;
            req5.Optional__c = true;
            reqList.add(req5);
            SQX_Requirement__c req6 = new SQX_Requirement__c();
            req6.SQX_Controlled_Document__c = docList[1].id;
            req6.SQX_Job_Function__c = jfList[1].id;
            req6.Level_Of_Competency__c = locList[1];
            req6.Active__c = false;
            reqList.add(req6);
            db.op_insert( reqList , new List<Schema.SObjectField>{});
            
            // Add required personnel document training
            List<SQX_Personnel_Document_Training__c> pdtList = new List<SQX_Personnel_Document_Training__c>();
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c();
            pdt1.SQX_Controlled_Document__c = docList[0].id;
            pdt1.Level_Of_Competency__c = locList[1];
            pdt1.Status__c = pdtStatusList[0];
            pdt1.SQX_Personnel__c = psnList[0].id;
            pdt1.Due_Date__c = Date.today();
            pdt1.Completion_Date__c = Date.today();
            pdtList.add(pdt1);
            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c();
            pdt2.SQX_Controlled_Document__c = docList[1].id;
            pdt2.Level_Of_Competency__c = locList[1];
            pdt2.Status__c = pdtStatusList[2];
            pdt2.SQX_Personnel__c = psnList[0].id;
            pdt2.Due_Date__c = Date.today();
            pdt2.Completion_Date__c = Date.today();
            pdtList.add(pdt2);
            SQX_Personnel_Document_Training__c pdt3 = new SQX_Personnel_Document_Training__c();
            pdt3.SQX_Controlled_Document__c = docList[0].id;
            pdt3.Level_Of_Competency__c = locList[0];
            pdt3.Status__c = pdtStatusList[2];
            pdt3.SQX_Personnel__c = psnList[1].id;
            pdt3.Due_Date__c = Date.today();
            pdt3.Completion_Date__c = Date.today();
            pdtList.add(pdt3);
            SQX_Personnel_Document_Training__c pdt4 = new SQX_Personnel_Document_Training__c();
            pdt4.SQX_Controlled_Document__c = docList[0].id;
            pdt4.Level_Of_Competency__c = locList[1];
            pdt4.Status__c = pdtStatusList[1];
            pdt4.SQX_Personnel__c = psnList[1].id;
            pdt4.Due_Date__c = Date.today();
            pdt4.Completion_Date__c = Date.today();
            pdtList.add(pdt4);
            SQX_Personnel_Document_Training__c pdt5 = new SQX_Personnel_Document_Training__c();
            pdt5.SQX_Controlled_Document__c = docList[0].id;
            pdt5.Level_Of_Competency__c = locList[0];
            pdt5.Status__c = pdtStatusList[1];
            pdt5.SQX_Personnel__c = psnList[2].id;
            pdt5.Due_Date__c = Date.today();
            pdt5.Completion_Date__c = Date.today();
            pdtList.add(pdt5);
            SQX_Personnel_Document_Training__c pdt6 = new SQX_Personnel_Document_Training__c();
            pdt6.SQX_Controlled_Document__c = docList[0].id;
            pdt6.Level_Of_Competency__c = locList[2];
            pdt6.Status__c = pdtStatusList[2];
            pdt6.SQX_Personnel__c = psnList[2].id;
            pdt6.Due_Date__c = Date.today();
            pdt6.Completion_Date__c = Date.today();
            pdtList.add(pdt6);
            SQX_Personnel_Document_Training__c pdt7 = new SQX_Personnel_Document_Training__c();
            pdt7.SQX_Controlled_Document__c = docList[1].id;
            pdt7.Level_Of_Competency__c = locList[2];
            pdt7.Status__c = pdtStatusList[0];
            pdt7.SQX_Personnel__c = psnList[2].id;
            pdt7.Due_Date__c = Date.today();
            pdt7.Completion_Date__c = Date.today();
            pdtList.add(pdt7);
            
            db.op_insert( pdtList , new List<Schema.SObjectField>{});
            
            // Act: execute the batch class SQX_7_0_PdjfGeneration
            Test.startTest(); 
            SQX_7_0_PdjfGeneration pdjfGen = new SQX_7_0_PdjfGeneration();
            Database.executeBatch(pdjfGen);
            Test.stopTest();
            
            // Query throught the newly created pjf
            Map<Id, List<SQX_Personnel_Document_Job_Function__c>> pjfPdjfMap = new Map<Id, List<SQX_Personnel_Document_Job_Function__c>>();
            for(SQX_Personnel_Document_Job_Function__c pdjf : [SELECT SQX_Personnel_Document_Training__c, SQX_Requirement__c,
                                                                      SQX_Personnel_Job_Function__c
                                                               FROM SQX_Personnel_Document_Job_Function__c]){
                                                                   if(pjfPdjfMap.containsKey(pdjf.SQX_Personnel_Job_Function__c)){
                                                                       pjfPdjfMap.get(pdjf.SQX_Personnel_Job_Function__c).add(pdjf);
                                                                   }else{
                                                                       pjfPdjfMap.put(pdjf.SQX_Personnel_Job_Function__c, new List<SQX_Personnel_Document_Job_Function__c>{pdjf});
                                                                   }
                                                               }
            // Asserts: pdjfs should created for the active pjfs and requirements
            // 1) 2 pdjf should be created for pjf1
            System.assertEquals(2, pjfPdjfMap.get(pjf1.id).size(), 'pjf1 should generate two pdjf since it has 2 active document requirements(req1 & req2)');
            // 2) 1 pdjf should be created for pjf2
            System.assertEquals(1, pjfPdjfMap.get(pjf2.id).size(), 'pjf2 should generate a pdjf since it has 1 active document requirements(req1)');
            // 3) 1 pdjf should be created for pjf2
            System.assertEquals(1, pjfPdjfMap.get(pjf3.id).size(), 'pjf1 should generate a pdjf since it has 1 active document requirements(req1)');
            // 4) 1 pdjf should be created for pjf2
            System.assertEquals(2, pjfPdjfMap.get(pjf4.id).size(), 'pjf1 should generate two pdjf since it has 2 active document requirements(req1 & req2)');
            // 5) 1 pdjf should be created for pjf2
            System.assertEquals(2, pjfPdjfMap.get(pjf5.id).size(), 'pjf1 should generate two pdjf since it has 2 active document requirements(req1 & req2)');
            
            // Query through all the pending personnel document training
            List<SQX_Personnel_Document_Training__c> newPdtList = [SELECT Overall_Optional__c, Overall_Competency__c 
                                                                    FROM SQX_Personnel_Document_Training__c 
                                                                    WHERE id IN: pdtList AND Status__c =: pdtStatusList[0]];
            
            // Overall competency and optional should be populated for pending training
            System.assertEquals(false, newPdtList[0].Overall_Optional__c, 'Overall option should be false since it has a non optional pdt');
            System.assertEquals(locList[1], newPdtList[0].Overall_Competency__c, 'Overall Competency is set to L2 of highter competency then L1');
            System.assertEquals(true, newPdtList[1].Overall_Optional__c, 'Overall option should be true since it has all optional pdt');
            System.assertEquals(locList[2], newPdtList[1].Overall_Competency__c, 'Overall Competency is set to L3 of highter competency then L1');
            
            // Is_PDJF_Generated__c should be selected for all active pjf after batch run
            System.assertEquals(pjfPdjfMap.keySet().size(), [SELECT Id FROM SQX_Personnel_Job_Function__c WHERE Is_PDJF_Generated__c =: true].size());
        }
    }
    
    /**
     *   Given : PJFS:
     *              Personnel 1 -> JF 1
     *              Personnel 2 -> JF 2
     * 
     *          Doc-1
     *              Requirement 1: (Active Requirement for generating Training 2 - JF 2)
     *
     *              Training 1: Personnel 1 (Complete/Pending without any requirements)
     *              Training 2: Personnel 2 (Complete/Pending)
     *
     *           Doc-2
     *              Requirement 2: (Active Requirement for generating Training 4 - JF 2)
     *              Requirement 3: (Active Requirement for generating Training 3 - JF 1)
     *
     *              Training 3: Personnel 1 (Complete/Pending)
     *              Training 4: Personnel 2 (Complete/Pending) 
     *          
     *   When  : SQX_7_0_PdjfGeneration batch class executes
     *   Then  : Three PDJFs will be generated correctly for three training records (2,3 and 4) as training-1 has no active requirements.
     *   @author : Paras BK
     *   @date : 09/25/2017
     *   @story : SQX-3896
     */
    public testmethod static void ensureOnlyValidDocumentTrainingsAreProcessed(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_DB db = new SQX_DB();
            
            // Arrange: Add required pjf, requirement, pdt, jf and document
            List<SQX_Personnel__c> psnList = new List<SQX_Personnel__c>();
            List<SQX_Job_Function__c> jfList = new List<SQX_Job_Function__c>();
            List<SQX_Controlled_Document__c> docList = new List<SQX_Controlled_Document__c>();
            
            // bypass pdt creation when requirement and personnel job function are inserted
            SQX_Personnel_Document_Job_Function.bypassPdjfCreationForMigrationUnitTest = true;
            
            for(Integer i=0; i<2; i++){
                // Add required job functions
                SQX_Job_Function__c jf = new SQX_Job_Function__c(Name = 'JF', Active__c = true);
                jfList.add(jf);
                
                // Add required personnels
                SQX_Test_Personnel psn= new SQX_Test_Personnel();
                psnList.add(psn.mainRecord);
                
                // Add required controlled document
                SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
                doc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
                docList.add(doc.doc);
            }
            db.op_insert( jfList , new List<Schema.SObjectField>{});
            db.op_insert( psnList , new List<Schema.SObjectField>{});
            db.op_insert( docList , new List<Schema.SObjectField>{});
            
            // Add required personnel job function
            List<SQX_Personnel_Job_Function__c> pjfList = new List<SQX_Personnel_Job_Function__c>();
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c();
            pjf1.Active__c = true;
            pjf1.SQX_Job_Function__c = jfList[0].id;
            pjf1.SQX_Personnel__c = psnList[0].id;
            pjfList.add(pjf1);

            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c();
            pjf2.Active__c = true;
            pjf2.SQX_Job_Function__c = jfList[1].id;
            pjf2.SQX_Personnel__c = psnList[1].id;
            pjfList.add(pjf2);

            // insert 
            db.op_insert( pjfList , new List<Schema.SObjectField>{});
            
            // Add required requirements
            List<SQX_Requirement__c> reqList = new List<SQX_Requirement__c>();

            SQX_Requirement__c req1 = new SQX_Requirement__c();
            req1.SQX_Controlled_Document__c = docList[0].id;
            req1.SQX_Job_Function__c = jfList[1].id;
            req1.Level_Of_Competency__c = locList[0];
            req1.Optional__c = true;
            req1.Active__c = true;
            reqList.add(req1);

            SQX_Requirement__c req2 = new SQX_Requirement__c();
            req2.SQX_Controlled_Document__c = docList[1].id;
            req2.SQX_Job_Function__c = jfList[1].id;
            req2.Level_Of_Competency__c = locList[1];
            req2.Optional__c = false;
            req2.Active__c = true;
            reqList.add(req2);

            SQX_Requirement__c req3 = new SQX_Requirement__c();
            req3.SQX_Controlled_Document__c = docList[1].id;
            req3.SQX_Job_Function__c = jfList[0].id;
            req3.Level_Of_Competency__c = locList[2];
            req3.Optional__c = true;
            req3.Active__c = true;
            reqList.add(req3);

            // insert
            db.op_insert( reqList , new List<Schema.SObjectField>{});
            
            // Add required personnel document training
            List<SQX_Personnel_Document_Training__c> pdtList = new List<SQX_Personnel_Document_Training__c>();
            
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c();
            pdt1.SQX_Controlled_Document__c = docList[0].id;
            pdt1.Level_Of_Competency__c = locList[1];
            pdt1.Status__c = pdtStatusList[0];
            pdt1.SQX_Personnel__c = psnList[0].id;
            pdt1.Due_Date__c = Date.today();
            pdt1.Completion_Date__c = Date.today();
            pdtList.add(pdt1);

            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c();
            pdt2.SQX_Controlled_Document__c = docList[0].id;
            pdt2.Level_Of_Competency__c = locList[1];
            pdt2.Status__c = pdtStatusList[2];
            pdt2.SQX_Personnel__c = psnList[1].id;
            pdt2.Due_Date__c = Date.today();
            pdt2.Completion_Date__c = Date.today();
            pdtList.add(pdt2);

            SQX_Personnel_Document_Training__c pdt3 = new SQX_Personnel_Document_Training__c();
            pdt3.SQX_Controlled_Document__c = docList[1].id;
            pdt3.Level_Of_Competency__c = locList[0];
            pdt3.Status__c = pdtStatusList[2];
            pdt3.SQX_Personnel__c = psnList[0].id;
            pdt3.Due_Date__c = Date.today();
            pdt3.Completion_Date__c = Date.today();
            pdtList.add(pdt3);

            SQX_Personnel_Document_Training__c pdt4 = new SQX_Personnel_Document_Training__c();
            pdt4.SQX_Controlled_Document__c = docList[1].id;
            pdt4.Level_Of_Competency__c = locList[1];
            pdt4.Status__c = pdtStatusList[1];
            pdt4.SQX_Personnel__c = psnList[1].id;
            pdt4.Due_Date__c = Date.today();
            pdt4.Completion_Date__c = Date.today();
            pdtList.add(pdt4);
            
            db.op_insert( pdtList , new List<Schema.SObjectField>{});
            
            // Act: execute the batch class SQX_7_0_PdjfGeneration
            Test.startTest(); 
            SQX_7_0_PdjfGeneration pdjfGen = new SQX_7_0_PdjfGeneration();
            Database.executeBatch(pdjfGen);
            Test.stopTest();
            
            // Query throught the newly created pjf
            Map<Id, List<SQX_Personnel_Document_Job_Function__c>> pjfPdjfMap = new Map<Id, List<SQX_Personnel_Document_Job_Function__c>>();
            for(SQX_Personnel_Document_Job_Function__c pdjf : [SELECT SQX_Personnel_Document_Training__c, SQX_Requirement__c,
                                                                      SQX_Personnel_Job_Function__c
                                                               FROM SQX_Personnel_Document_Job_Function__c]){
                                                                   if(pjfPdjfMap.containsKey(pdjf.SQX_Personnel_Job_Function__c)){
                                                                       pjfPdjfMap.get(pdjf.SQX_Personnel_Job_Function__c).add(pdjf);
                                                                   }else{
                                                                       pjfPdjfMap.put(pdjf.SQX_Personnel_Job_Function__c, new List<SQX_Personnel_Document_Job_Function__c>{pdjf});
                                                                   }
                                                               }
                                                               
            // Asserts: pdjfs should created for the active pjfs and requirements
            // 1) 1 pdjf should be created for pjf1
            System.assertEquals(1, pjfPdjfMap.get(pjf1.id).size(), 'pjf1 should generate one pdjf since it has 1 active document requirements(req1)');
            // 2) 2 pdjf should be created for pjf2
            System.assertEquals(2, pjfPdjfMap.get(pjf2.id).size(), 'pjf2 should generate a pdjf since it has 2 active document requirements(req1 & req2)');
        }
    }

     /**
     *   Given : PJFS:
     *              Personnel 1 -> JF 1
     *              Personnel 2 -> JF 2
     * 
     *          Doc-1
     *              Requirement 1: (Active Requirement for generating Training 2 - JF 2)
     *
     *              Training 1: Personnel 1 (Complete/Pending without any requirements)
     *              Training 2: Personnel 2 (Complete/Pending)
     *
     *           Doc-2
     *              Requirement 2: (Active Requirement for generating Training 4 - JF 2)
     *              Requirement 3: (Active Requirement for generating Training 3 - JF 1)
     *
     *              Training 3: Personnel 1 (Complete/Pending)
     *              Training 4: Personnel 2 (Complete/Pending) 
     *          
     *   When  : SQX_7_0_PdjfGeneration batch class executes
     *   Then  : No Duplicate PDJF Records are generated
     *   @story : SQX-5073
     */
    public testmethod static void ensureOnlyValidDocumentTrainingsAreGenerated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_DB db = new SQX_DB();
            
            // Arrange: Add required pjf, requirement, pdt, jf and document
            List<SQX_Personnel__c> psnList = new List<SQX_Personnel__c>();
            List<SQX_Job_Function__c> jfList = new List<SQX_Job_Function__c>();
            List<SQX_Controlled_Document__c> docList = new List<SQX_Controlled_Document__c>();
            
            for(Integer i=0; i<2; i++){
                // Add required job functions
                SQX_Job_Function__c jf = new SQX_Job_Function__c(Name = 'JF', Active__c = true);
                jfList.add(jf);
                
                // Add required personnels
                SQX_Test_Personnel psn= new SQX_Test_Personnel();
                psnList.add(psn.mainRecord);
                
                // Add required controlled document
                SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
                doc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
                docList.add(doc.doc);
            }
            db.op_insert( jfList , new List<Schema.SObjectField>{});
            db.op_insert( psnList , new List<Schema.SObjectField>{});
            db.op_insert( docList , new List<Schema.SObjectField>{});
            
            // Add required personnel job function
            List<SQX_Personnel_Job_Function__c> pjfList = new List<SQX_Personnel_Job_Function__c>();
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c();
            pjf1.Active__c = true;
            pjf1.SQX_Job_Function__c = jfList[0].id;
            pjf1.SQX_Personnel__c = psnList[0].id;
            pjfList.add(pjf1);
            
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c();
            pjf2.Active__c = true;
            pjf2.SQX_Job_Function__c = jfList[1].id;
            pjf2.SQX_Personnel__c = psnList[1].id;
            pjfList.add(pjf2);
            
            // insert 
            db.op_insert( pjfList , new List<Schema.SObjectField>{});
            
            // Add required requirements
            List<SQX_Requirement__c> reqList = new List<SQX_Requirement__c>();
            
            SQX_Requirement__c req1 = new SQX_Requirement__c();
            req1.SQX_Controlled_Document__c = docList[0].id;
            req1.SQX_Job_Function__c = jfList[1].id;
            req1.Level_Of_Competency__c = locList[0];
            req1.Optional__c = true;
            req1.Active__c = true;
            reqList.add(req1);
            
            SQX_Requirement__c req2 = new SQX_Requirement__c();
            req2.SQX_Controlled_Document__c = docList[1].id;
            req2.SQX_Job_Function__c = jfList[1].id;
            req2.Level_Of_Competency__c = locList[1];
            req2.Optional__c = false;
            req2.Active__c = true;
            reqList.add(req2);
            
            SQX_Requirement__c req3 = new SQX_Requirement__c();
            req3.SQX_Controlled_Document__c = docList[1].id;
            req3.SQX_Job_Function__c = jfList[0].id;
            req3.Level_Of_Competency__c = locList[2];
            req3.Optional__c = true;
            req3.Active__c = true;
            reqList.add(req3);
            
            // insert
            db.op_insert( reqList , new List<Schema.SObjectField>{});
            
            // Add required personnel document training
            List<SQX_Personnel_Document_Training__c> pdtList = new List<SQX_Personnel_Document_Training__c>();
            
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c();
            pdt1.SQX_Controlled_Document__c = docList[0].id;
            pdt1.Level_Of_Competency__c = locList[1];
            pdt1.Status__c = pdtStatusList[0];
            pdt1.SQX_Personnel__c = psnList[0].id;
            pdt1.Due_Date__c = Date.today();
            pdt1.Completion_Date__c = Date.today();
            pdtList.add(pdt1);
            
            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c();
            pdt2.SQX_Controlled_Document__c = docList[0].id;
            pdt2.Level_Of_Competency__c = locList[1];
            pdt2.Status__c = pdtStatusList[2];
            pdt2.SQX_Personnel__c = psnList[1].id;
            pdt2.Due_Date__c = Date.today();
            pdt2.Completion_Date__c = Date.today();
            pdtList.add(pdt2);
            
            SQX_Personnel_Document_Training__c pdt3 = new SQX_Personnel_Document_Training__c();
            pdt3.SQX_Controlled_Document__c = docList[1].id;
            pdt3.Level_Of_Competency__c = locList[0];
            pdt3.Status__c = pdtStatusList[2];
            pdt3.SQX_Personnel__c = psnList[0].id;
            pdt3.Due_Date__c = Date.today();
            pdt3.Completion_Date__c = Date.today();
            pdtList.add(pdt3);
            
            SQX_Personnel_Document_Training__c pdt4 = new SQX_Personnel_Document_Training__c();
            pdt4.SQX_Controlled_Document__c = docList[1].id;
            pdt4.Level_Of_Competency__c = locList[1];
            pdt4.Status__c = pdtStatusList[1];
            pdt4.SQX_Personnel__c = psnList[1].id;
            pdt4.Due_Date__c = Date.today();
            pdt4.Completion_Date__c = Date.today();
            pdtList.add(pdt4);
            
            db.op_insert( pdtList , new List<Schema.SObjectField>{});
            
            // Act: execute the batch class SQX_7_0_PdjfGeneration
            Test.startTest(); 
            SQX_7_0_PdjfGeneration pdjfGen = new SQX_7_0_PdjfGeneration();
            Database.executeBatch(pdjfGen);
            Test.stopTest();
            
            // Query throught the newly created pjf
            Map<Id, List<SQX_Personnel_Document_Job_Function__c>> pjfPdjfMap = new Map<Id, List<SQX_Personnel_Document_Job_Function__c>>();
            for(SQX_Personnel_Document_Job_Function__c pdjf : [SELECT SQX_Personnel_Document_Training__c, SQX_Requirement__c,
                                                               SQX_Personnel_Job_Function__c
                                                               FROM SQX_Personnel_Document_Job_Function__c]){
                                                                   if(pjfPdjfMap.containsKey(pdjf.SQX_Personnel_Job_Function__c)){
                                                                       pjfPdjfMap.get(pdjf.SQX_Personnel_Job_Function__c).add(pdjf);
                                                                   }else{
                                                                       pjfPdjfMap.put(pdjf.SQX_Personnel_Job_Function__c, new List<SQX_Personnel_Document_Job_Function__c>{pdjf});
                                                                   }
                                                               }
            
            // Asserts: pdjfs should created for the active pjfs and requirements
            // 1) 1 pdjf should be created for pjf1
            System.assertEquals(1, pjfPdjfMap.get(pjf1.id).size(), 'pjf1 should generate one pdjf since it has 1 active document requirements(req1)');
            // 2) 2 pdjf should be created for pjf2
            System.assertEquals(2, pjfPdjfMap.get(pjf2.id).size(), 'pjf2 should generate a pdjf since it has 2 active document requirements(req1 & req2)');
        }
    }
}