/**
* Test Class for preventing H1 field to be required if Manufacturer is selected..
*/
@isTest
public class SQX_Test_7766_eMDR_Template {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
    * Given: A Medwatch Record (that is not followup) with a Regulatory Default record with Reporting Site as Manufacturing Site.
    * When: it is inserted
    * Then: an error is thrown saying that H1 field is required.
    */    
    static testmethod void givenNonFollowupMedwatchRecordWithH1FieldEmpty_whenValidatedOrSubmitted_ErrorShouldBethrownIndicatingThatH1IsRequired(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            
            //Arrange : create all required records.
            SQX_Reporting_Default__c reporting = new SQX_Reporting_Default__c();
            reporting.Reporting_Site__c = 'Manufacturing Site';
            insert reporting;
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'US:30-Day MDR Test2018812';
            regulatoryReport.Report_Name__c = 'US:30-Day MDR';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.Reg_Body__c = 'FDA';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_Reporting_Default__c = reporting.Id;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            
            insert regulatoryReport;
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.compliancequest__B5_Describe_event_or_problem__c = 'asdasd';
            medwatch.compliancequest__B7_Other_Relevant_History__c= 'asdasd';
            medwatch.compliancequest__D1_Brand_Name__c='asdas';
            medwatch.compliancequest__D2_a_Common_Device_Name__c = 'asdas';
            medwatch.compliancequest__D2_b_Procode__c = 'as';
            medwatch.SQX_Regulatory_Report__c =  regulatoryReport.Id;
            medwatch.compliancequest__Activity_Code__c= 'Validate';
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            medwatchList.add(medwatch);
            //Act : insert the record;
            Database.SaveResult[] saveResults = Database.insert(medwatchList, false);
            
            //Assert : Ensure that the record wasn't saved
            System.assertEquals(false, saveResults[0].isSuccess());
            
            
            // Arrange: Create Report Submission
            SQX_Submission_History__c regSubmission = new SQX_Submission_History__c();
            regSubmission.Status__c='Complete';
            regSubmission.SQX_Complaint__c=complaint.complaint.Id;
            regSubmission.Submitted_By__c='Submitter';
            insert regSubmission;
            
            // Arrange: Complete the regulatory report so that it can be followed up.
            regulatoryReport.Status__c='Complete';
            regulatoryReport.SQX_Submission_History__c=regSubmission.Id;
            update regulatoryReport;
            
            // Arrange: Followup the regulatory report.
            SQX_Regulatory_Report__c regReportFollowup = new SQX_Regulatory_Report__c( 
                Name = 'US:30-Day MDR Test2018812',
                Report_Name__c = 'US:30-Day MDR',
                Due_Date__c =  Date.today() + 21,
                Reg_Body__c = 'FDA',
                Report_Type__c = 'Initial report',
                SQX_Reporting_Default__c = reporting.Id,
                SQX_Complaint__c = complaint.complaint.Id,
                SQX_Follow_Up_Of__c = regulatoryReport.Id
            );
            
            insert regReportFollowup;
            
            // Arrange: Create followup medwatch report with H1 field missing.
            SQX_Medwatch__c medwatchFollowUp = new SQX_Medwatch__c(
                B5_Describe_event_or_problem__c = 'event description',
                B7_Other_Relevant_History__c= 'Other relevant history',
                D1_Brand_Name__c='brand name',
                D2_a_Common_Device_Name__c = 'common device name',
                D2_b_Procode__c = 'COD',
                SQX_Regulatory_Report__c =  regReportFollowup.Id,
                Activity_Code__c= 'Submit'
            );
           
             //Act : insert the followup medwatch report with missing H1.
            Database.SaveResult[] followupMedwatchSaveResult = Database.insert(new List<SQX_Medwatch__c>{medwatchFollowUp}, false);
            
            //Assert : Ensure that the followup record with missing H1 was saved successfully.
            System.assertEquals(true, followupMedwatchSaveResult[0].isSuccess());
        }
        
    }

    /**
    * Given: A Medwatch Record with a Regulatory Default record with Reporting Site as Manufacturing Site.
    * When: it is inserted
    * Then: an error is thrown saying that H1 field is required.
    */    
    static testmethod void selectedManufacturerInReportingSite_H1IsGivenData_NoError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            
            //Arrange : create all required records.
            SQX_Reporting_Default__c reporting = new SQX_Reporting_Default__c();
            reporting.Reporting_Site__c = 'Manufacturing Site';
            insert reporting;
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'US:30-Day MDR Test20';
            regulatoryReport.Report_Name__c = 'US:30-Day MDR';
            regulatoryReport.Due_Date__c =  Date.today() + 11;
            regulatoryReport.Reg_Body__c = 'FDA';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_Reporting_Default__c = reporting.Id;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            insert regulatoryReport;
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.compliancequest__B5_Describe_event_or_problem__c = 'asdasd';
            medwatch.compliancequest__B7_Other_Relevant_History__c= 'asdasd';
            medwatch.compliancequest__D1_Brand_Name__c='asdas';
            medwatch.compliancequest__D2_a_Common_Device_Name__c = 'asdas';
            medwatch.compliancequest__D2_b_Procode__c = 'as';
            medwatch.compliancequest__H1_Type_of_Reportable_Event__c = 'C25745';
            medwatch.SQX_Regulatory_Report__c =  regulatoryReport.Id;
            medwatch.compliancequest__Activity_Code__c= 'Validate';
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            medwatchList.add(medwatch);
            //Act : insert the record;
            Database.SaveResult[] saveResults = Database.insert(medwatchList, false);
            
            //Assert : Ensure that the record was saved
            System.assertEquals(true, saveResults[0].isSuccess());
        }
        
    }
   /**
    * Given: A Medwatch Record with H1 field not empty and a Regulatory Default record with Reporting Site as User Facility/Importer Site.
    * When: it is inserted
    * Then: record isnot inserted.
    */ 
    static testmethod void selectedUserFacilityInReportingSite_H1ContainData_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            
            //Arrange : create all required records.
            SQX_Reporting_Default__c reporting = new SQX_Reporting_Default__c();
            reporting.Reporting_Site__c = 'User Facility/Importer Site';
            insert reporting;
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'US:30-Day MDR Test2018812';
            regulatoryReport.Report_Name__c = 'US:30-Day MDR';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.Reg_Body__c = 'FDA';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_Reporting_Default__c = reporting.Id;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            insert regulatoryReport;
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.compliancequest__B5_Describe_event_or_problem__c = 'asdasd';
            medwatch.compliancequest__B7_Other_Relevant_History__c= 'asdasd';
            medwatch.compliancequest__D1_Brand_Name__c='asdas';
            medwatch.compliancequest__D2_a_Common_Device_Name__c = 'asdas';
            medwatch.compliancequest__D2_b_Procode__c = 'as';
            medwatch.compliancequest__H1_Type_of_Reportable_Event__c = 'C28554';
            medwatch.compliancequest__F1_For_Use_By__c = 'C53617';
            medwatch.SQX_Regulatory_Report__c =  regulatoryReport.Id;
            medwatch.compliancequest__Activity_Code__c= 'Validate';
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            medwatchList.add(medwatch);
            //Act : insert the record;
            Database.SaveResult[] saveResults = Database.insert(medwatchList, false);
            
            //Assert : Ensure that the record wasn't saved
            System.assertEquals(false, saveResults[0].isSuccess());
        }
        
    }
    /**
    * Given: A Medwatch Record with H1 field not empty and a Regulatory Default record with Reporting Site as User Facility/Importer Site.
    * When: it is inserted
    * Then: record isnot inserted.
    */ 
    static testmethod void selectedUserFacilityInReportingSite_H1DoNotContainData_ErrorIsNotThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            
            //Arrange : create all required records.
            SQX_Reporting_Default__c reporting = new SQX_Reporting_Default__c();
            reporting.Reporting_Site__c = 'User Facility/Importer Site';
            insert reporting;
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'US:30-Day MDR Test2018812';
            regulatoryReport.Report_Name__c = 'US:30-Day MDR';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.Reg_Body__c = 'FDA';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_Reporting_Default__c = reporting.Id;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            insert regulatoryReport;
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.compliancequest__B5_Describe_event_or_problem__c = 'asdasd';
            medwatch.compliancequest__B7_Other_Relevant_History__c= 'asdasd';
            medwatch.compliancequest__D1_Brand_Name__c='asdas';
            medwatch.compliancequest__D2_a_Common_Device_Name__c = 'asdas';
            medwatch.compliancequest__D2_b_Procode__c = 'as';
            medwatch.compliancequest__F1_For_Use_By__c = 'C53617';
            medwatch.SQX_Regulatory_Report__c =  regulatoryReport.Id;
            medwatch.compliancequest__Activity_Code__c= 'Validate';
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            medwatchList.add(medwatch);
            //Act : insert the record;
            Database.SaveResult[] saveResults = Database.insert(medwatchList, false);
            //Assert : Ensure that the record was saved
            System.assertEquals(true, saveResults[0].isSuccess());
        }
        
    }
    
}