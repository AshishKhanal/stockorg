/**
 * test class for SF tasks of supplier management
 */
@isTest
public class SQX_Test_Supplier_SF_Task {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
    }
    
    /**
     * GIVEN : A NSI is created with open onboarding step
     * WHEN : Onboarding step owner is changed
     * THEN : Related SF task owner is changed
     * @story : [SQX-6480]
     */
    public static testMethod void givenOpenNSIWithOpenTask_WhenOnboardingStepOwnerIsChanged_TaskOwnerIsChanged(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 1);
        }
        System.runAs(standardUser){
            // ARRANGE : A NSI is created with open onboarding step
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();
            
            nsi.initiate();
            
            Task sfTasksDesc = [SELECT Id, OwnerId  FROM Task WHERE WhatId =: nsi.nsi.Id ];
            System.assertEquals(assigneeUser.Id,sfTasksDesc.OwnerId);
            
            // ACT : Onboarding step owner is changed
            SQX_OnBoarding_Step__c os = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            os.SQX_User__c = standardUser.Id;
            update os;
            
            // ASSERT :  Related SF task owner is changed
            sfTasksDesc = [SELECT Id, OwnerId  FROM Task WHERE WhatId =: nsi.nsi.Id ];
            System.assertEquals(standardUser.Id,sfTasksDesc.OwnerId);
            
        }   
    }

    /**
     * GIVEN : Supplier Deviation record is created with open deviation process
     * WHEN : Deviation Process owner is changed
     * THEN : Related SF task owner is changed
     * @story : [SQX-6480]
     */ 
    public static testmethod void givenOpenSDWithOpenTask_WhenDeviationProcessOwnerIsChanged_TaskOwnerIsChanged(){
        
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        User assigneeUser = userMap.get('assigneeUser');
        User adminUser = userMap.get('adminUser');
        
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});

        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 1);
        }
        
        System.runAs(standardUser){
            
            // ARRANGE : Supplier Deviation record is created with open deviation process
            SQX_Test_Supplier_Deviation supplierDeviation = new SQX_Test_Supplier_Deviation();
            supplierDeviation.save();
            
            supplierDeviation.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            supplierDeviation.save();
            
            supplierDeviation.initiate();
            
            Task sfTasksDesc = [SELECT Id, OwnerId  FROM Task WHERE WhatId =: supplierDeviation.sd.Id ];
            System.assertEquals(assigneeUser.Id,sfTasksDesc.OwnerId);
            
            // ACT : Deviation Process owner is changed
            SQX_Deviation_Process__c dp = [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: supplierDeviation.sd.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
			dp.SQX_User__c = standardUser.Id;
            update dp;
            
            // ASSERT : Related SF task owner is changed
            sfTasksDesc = [SELECT Id, OwnerId  FROM Task WHERE WhatId =: supplierDeviation.sd.Id ];
            System.assertEquals(standardUser.Id,sfTasksDesc.OwnerId);
        }
        
    }
}