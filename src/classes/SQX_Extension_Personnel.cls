/**
* extension class for personnel object
*/
public with sharing class SQX_Extension_Personnel {
    ApexPages.StandardController stdController;
    SQX_Personnel__c mainRecord;
    public String createActionToTake { get; set; }
    public boolean includeDeactivatedJobFunctions { get; set; }
    private boolean oldIncludeDeactivatedJobFunction = includeDeactivatedJobFunctions;

    /**
    * The instance of standard set controller useful for paginating accross records
    */
    private ApexPages.StandardSetController personnelJobFunctionController,
                                            completedPersonnelDocumentTrainingController,
                                            pendingPersonnelDocumentTrainingController,
                                            personnelAssessmentListController;

    /**
    * The default size of the page that is being used for pagination.
    */
    private final Integer DEFAULT_PAGE_SIZE = 10;
    
    /**
    * returns create action options
    */
    public List<SelectOption> getCreateActionOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(SQX_Personnel.CREATE_ACTION_NON_USER, Label.CQ_UI_CREATE_ACTION_NON_USER));
        options.add(new SelectOption(SQX_Personnel.CREATE_ACTION_IMPORT_USER, Label.CQ_UI_CREATE_ACTION_IMPORT_USER));
        options.add(new SelectOption(SQX_Personnel.CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS, Label.CQ_UI_CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS));
        return options;
    }
    
    public SQX_Extension_Personnel(ApexPages.StandardController controller) {
        stdController = controller;
        mainRecord = (SQX_Personnel__c)controller.getRecord();
        includeDeactivatedJobFunctions = false;
        
        if (mainRecord.Id == null) {
            // setting default create action
            createActionToTake = SQX_Personnel.CREATE_ACTION_NON_USER;
            
            if (mainRecord.OwnerId == null) {
                // set default training manager / owner
                mainRecord.OwnerId = UserInfo.getUserId();
            }
        }
    }
    
    private PageReference create(boolean isCreateAndNew) {
        PageReference retURL = null;
        
        try {
            if (String.isBlank(createActionToTake)) {
                // Action not selected
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action is required.'));
                
            } else if (createActionToTake == SQX_Personnel.CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS) {
                // import active users
                List<Database.SaveResult> results = SQX_Personnel.importAllUsers();
                
                Integer newCount = 0;
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        newCount++;
                    } else {
                        // display save errors
                        for (Database.Error error : result.getErrors()) {
                            String msg = SQX_Personnel.getCustomizedErrorMessage(error.getMessage());
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
                        }
                    }
                }
                
                if (results.size() == 0) {
                    // display no records to import
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There are no new active users to import.'));
                } else if (newCount > 0) {
                    // display number of successful imports
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, newCount + ' records have been imported successfully.'));
                }
            } else {
                DataBase.SaveResult result = null;
                
                if (createActionToTake == SQX_Personnel.CREATE_ACTION_IMPORT_USER) {
                    // import user
                    result = SQX_Personnel.importUser(mainRecord.SQX_User__c);
                    
                } else if (createActionToTake == SQX_Personnel.CREATE_ACTION_NON_USER) {
                    // create new user
                    result = new SQX_DB().continueOnError().op_insert(new List<SQX_Personnel__c> { mainRecord }, new List<Schema.SObjectField>{ }).get(0);
                }
                
                if (result.isSuccess()) {
                    if (isCreateAndNew) {
                        retURL = Page.SQX_Personnel_Create;
                        retURL.setRedirect(true);
                    } else {
                        retURL = new ApexPages.StandardController(new SQX_Personnel__c(Id = result.getId())).View();
                    }
                } else {
                    // display save errors
                    for (Database.Error error : result.getErrors()) {
                        String msg = SQX_Personnel.getCustomizedErrorMessage(error.getMessage());
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
                    }
                }
            }
        } catch (Exception ex) {
            // display unknown exceptions
            String msg = SQX_Personnel.getCustomizedErrorMessage(ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
        }
        
        return retURL;
    }
    
    public PageReference create() {
        return create(false);
    }
    
    public PageReference createAndNew() {
        return create(true);
    }
    
    /**
    * returns completed personnel document training list
    * TODO: Find a way to combine the query in respective classes with this.
    *       Can be replaced with
    *       <code>SQX_Personnel.getPersonnelDocumentTrainingList(new Set<Id>{ mainRecord.Id },
    *        new Set<String>{SQX_Personnel_Document_Training.STATUS_COMPLETE}
    *    );</code>
    *       returning QueryLocator instead of result
    */
    public ApexPages.StandardSetController getCompletedPersonnelDocumentTrainingController() {
        if(completedPersonnelDocumentTrainingController == null) {
            Set<Id> personnelIds = new Set<Id>{ mainRecord.Id };
            Set<String> trainingStatuses = new Set<String>{SQX_Personnel_Document_Training.STATUS_COMPLETE };

            completedPersonnelDocumentTrainingController = new ApexPages.StandardSetController(
                Database.getQueryLocator([SELECT Id, Name, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, Title__c,
                       Document_Number__c, Document_Revision__c, Document_Title__c, Document_Number_Rev__c,
                       SQX_Personnel__c, Personnel_Name__c, SQX_Trainer__c, SQX_Trainer__r.Name, Level_Of_Competency__c, Optional__c,
                       Trainer_Approval_Needed__c, Due_Date__c, Status__c, CreatedDate, Completion_Date__c,
                       SQX_User_Signed_Off_By__c, SQX_User_Signed_Off_By__r.Name, User_SignOff_Date__c,
                       SQX_Training_Approved_By__c, SQX_Training_Approved_By__r.Name, Trainer_SignOff_Date__c,
                       Can_Retrain__c, Is_Retrain__c, Retrain_Comment__c, SQX_Assessment__c, Assessment_Name__c, Can_View_And_Sign_Off__c,Can_Take_Assessment__c, SQX_Personnel_Assessment__c
                FROM SQX_Personnel_Document_Training__c
                WHERE SQX_Personnel__c IN :personnelIds AND Status__c IN :trainingStatuses
                ORDER BY Name])
            );


            completedPersonnelDocumentTrainingController.setPageSize(DEFAULT_PAGE_SIZE);
        }

        return completedPersonnelDocumentTrainingController;
    }

    
    /**
    * Returns the controller capable of returning pending personnel document training list
    * TODO: Find a way to combine the query in respective classes with this.
    *       Can be replaced with
    *       <code>SQX_Personnel.getPersonnelDocumentTrainingList(new Set<Id>{ mainRecord.Id },
    *        new Set<String>{SQX_Personnel_Document_Training.STATUS_PENDING, SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING }
    *    );</code>
    *       returning QueryLocator instead of result
    */
    public ApexPages.StandardSetController getpendingPersonnelDocumentTrainingController() { //PendingPersonnelDocumentTrainingList() {
        if(pendingPersonnelDocumentTrainingController == null){
            Set<Id> personnelIds = new Set<Id>{ mainRecord.Id };
            Set<String> trainingStatuses = new Set<String>{SQX_Personnel_Document_Training.STATUS_PENDING, SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING };

            pendingPersonnelDocumentTrainingController = new ApexPages.StandardSetController(
                Database.getQueryLocator([SELECT Id, Name, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, Title__c,
                       Document_Number__c, Document_Revision__c, Document_Title__c, Document_Number_Rev__c, SQX_Training_Session__c,
                       SQX_Personnel__c, Personnel_Name__c, SQX_Trainer__c, SQX_Trainer__r.Name, Level_Of_Competency__c, Optional__c,
                       Trainer_Approval_Needed__c, Due_Date__c, Status__c, CreatedDate, Completion_Date__c,
                       SQX_User_Signed_Off_By__c, SQX_User_Signed_Off_By__r.Name, User_SignOff_Date__c, Is_Training_Session_Complete__c,
                       SQX_Training_Approved_By__c, SQX_Training_Approved_By__r.Name, Trainer_SignOff_Date__c,
                       Can_Retrain__c, Is_Retrain__c, Retrain_Comment__c, SQX_Assessment__c, Assessment_Name__c, Can_View_And_Sign_Off__c,Can_Take_Assessment__c, SQX_Personnel_Assessment__c
                FROM SQX_Personnel_Document_Training__c
                WHERE SQX_Personnel__c IN :personnelIds AND Status__c IN :trainingStatuses
                ORDER BY Name])
            );

            pendingPersonnelDocumentTrainingController.setPageSize(DEFAULT_PAGE_SIZE);
        }

        return pendingPersonnelDocumentTrainingController;
    }

    /**
    * Returns the set controller responsible for iterating over job functions for the personnel
    * TODO: Find a way to combine the query in respective classes with this.
    *       Can be replaced with <code>SQX_Personnel_Job_Function.getPersonnelJobFunctionList(mainRecord.Id, showNewOrActiveOnly)</code>
    *       returning QueryLocator instead of result
    */
    public ApexPages.StandardSetController getPersonnelJobFunctionController(){
        if(personnelJobFunctionController == null || includeDeactivatedJobFunctions != oldIncludeDeactivatedJobFunction){
            if(!includeDeactivatedJobFunctions) {
                personnelJobFunctionController = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID,Name,SQX_Job_Function__c,Activation_Date__c,Active__c,Deactivation_Date__c, Training_Status__c, Current_Training_Program_Step__c, SQX_Job_Function__r.Name
                    FROM SQX_Personnel_Job_Function__c
                    WHERE SQX_Personnel__c = :mainRecord.Id AND Deactivation_Date__c = null
                    ORDER BY Personnel_Name__c]));
            }
            else {
                personnelJobFunctionController = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID,Name,SQX_Job_Function__c,Activation_Date__c,Active__c,Deactivation_Date__c, Training_Status__c, Current_Training_Program_Step__c, SQX_Job_Function__r.Name
                    FROM SQX_Personnel_Job_Function__c
                    WHERE SQX_Personnel__c = :mainRecord.Id
                    ORDER BY Personnel_Name__c, Activation_Date__c DESC]));
            }

            personnelJobFunctionController.setPageSize(DEFAULT_PAGE_SIZE);
            oldIncludeDeactivatedJobFunction = includeDeactivatedJobFunctions;
        }

        return personnelJobFunctionController;
    }


    /**
     * Returns the controller that can return list of records of personnel Assessment.
     */
    public ApexPages.StandardSetController getpersonnelAssessmentListController() {
        if(personnelAssessmentListController == null){
            personnelAssessmentListController =  new ApexPages.StandardSetController(Database.getQueryLocator([SELECT  Id, 
                        Name, 
                        SQX_Assessment__c, 
                        Assessment_Name__c,
                        SQX_Last_Assessment_Attempt__c, 
                        Result__c,
                        SQX_Last_Assessment_Attempt__r.Name,
                        SQX_Last_Assessment_Attempt__r.Submitted_Date__c,
                        Can_Take_or_Retake_Assessment__c,
                        Can_Retake__c,
                        SQX_Personnel__c
                    FROM SQX_Personnel_Assessment__c WHERE SQX_Personnel__c =: mainRecord.Id 
                    ORDER BY SQX_Last_Assessment_Attempt__c DESC NULLS FIRST]));

            personnelAssessmentListController.setPageSize(DEFAULT_PAGE_SIZE);
        }

        return personnelAssessmentListController;
    }
}