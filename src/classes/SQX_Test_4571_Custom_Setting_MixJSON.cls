@isTest
public class SQX_Test_4571_Custom_Setting_MixJSON {
    
    /**
    * When: when generate easyPDF with custom setting
    * Then: then existing property is overriden
    */ 
    testmethod static void WhenGenerateEasyPdf_ThenExistingPropertyIsOverriden() {
        // Arrange: Create mock original json and new custom json
        String originalJSON ='{"Word": {"ConvertBookmarks": "PRN_BOOKMARKS_NONE","ConversionMethod": "EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED"},'+
            '"Excel": {"PrintAllSheets": true,"DocumentPassword": "","Scaling": "EPWF_EXCEL_SCALING_FIT_TO_PAGE","ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT"},'+
            '"PowerPoint": {"FrameSlides": false,"HandoutOrder": "PRN_PP_HANDOUT_HORIZONTAL_FIRST","OutputType": "PRN_PP_OUTTYPE_SLIDES","PrintColorType": "PRN_PP_PRINT_COLOR","ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT"}}';
        String newJson ='{"Word": {"ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT_TEST"},'+
            '"PowerPoint": {"ConversionMethod": "EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED_TEST"},'+
            '"Excel": {"Scaling": "EPWF_EXCEL_SCALING_FIT_TO_PAGE_TEST"}}';
        
        // Act : mix both json
        SQX_PDFWorkflowGenerator obj=new SQX_PDFWorkflowGenerator();
        Map<String, Object> orgJson = (Map<String, Object>) JSON.deserializeUntyped(originalJSON);
        Map<String, Object> nJson = (Map<String, Object>) JSON.deserializeUntyped(newJson);
        obj.mixJSON(orgJson, nJson);
        
        //Assert: ensured that existing property is overriden
        Map<String, Object> excel = (Map<String, Object>)orgJson.get('Excel');
        system.assertEquals('EPWF_EXCEL_SCALING_FIT_TO_PAGE_TEST', excel.get('Scaling'));
        
        Map<String, Object> word = (Map<String, Object>) orgJson.get('Word');
        system.assertEquals('EPWF_CONVERSION_METHOD_DEFAULT_TEST', word.get('ConversionMethod'));
        
        Map<String, Object> powerPoint = (Map<String, Object>)orgJson.get('PowerPoint');
        system.assertEquals('EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED_TEST', powerPoint.get('ConversionMethod'));
    }
    
    /**
    * When: when generate easyPDF with custom setting
    * Then: then missing property is added top level
    */ 
    testmethod static void WhenGenerateEasyPdf_ThenMissingPropertyIsAdded() {
        // Arrange: Create mock original json and new custom json
        String originalJSON ='{"Word": {"ConvertBookmarks": "PRN_BOOKMARKS_NONE","ConversionMethod": "EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED"}}';
        String newJson ='{"Visio": {"ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT"}}';
        
        // Act : mix both json
        SQX_PDFWorkflowGenerator obj=new SQX_PDFWorkflowGenerator();
        Map<String, Object> orgJson = (Map<String, Object>) JSON.deserializeUntyped(originalJSON);
        Map<String, Object> nJson = (Map<String, Object>) JSON.deserializeUntyped(newJson);
        obj.mixJSON(orgJson, nJson);
        
        //Assert: ensured that missing property is added top level
        Map<String, Object> visio = (Map<String, Object>)orgJson.get('Visio');
        system.assertEquals('EPWF_CONVERSION_METHOD_DEFAULT', visio.get('ConversionMethod'));
        
        Map<String, Object> word = (Map<String, Object>)orgJson.get('Word');
        system.assertEquals('EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED', word.get('ConversionMethod'));
    }
    
    /**
    * When: when generate easyPDF with custom setting
    * Then: then complex properties are added
    */ 
    testmethod static void WhenGenerateEasyPdf_ThenComplexPropertiesAdded() {
        // Arrange: Create mock original json and new custom json
        String originalJSON ='{"Word": {"ConvertHyperlinks": true,"ConvertBookmarks": "PRN_BOOKMARKS_NONE","ConversionMethod": "EPWF_CONVERSION_METHOD_OFFICE_OPTIMIZED"}}';
        String newJson ='{"Word": {"ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT_TEST", "Paper": {"Orientation": "PRN_PAPER_ORIENT_DEFAULT","Size": "PRN_MSO_PAPER_DEFAULT","Width": 8.5,"Height": 11}}}';
        
        // Act : mix both json
        SQX_PDFWorkflowGenerator obj=new SQX_PDFWorkflowGenerator();
        Map<String, Object> orgJson = (Map<String, Object>) JSON.deserializeUntyped(originalJSON);
        Map<String, Object> nJson = (Map<String, Object>) JSON.deserializeUntyped(newJson);
        obj.mixJSON(orgJson, nJson);
        
        //Assert: ensured that complex property is added
        Map<String, Object> word = (Map<String, Object>)orgJson.get('Word');
        system.assertEquals(true, word.get('ConvertHyperlinks'));
        Map<String,Object> innerProperties =(Map<String, Object>)word.get('Paper');
        system.assertEquals('PRN_PAPER_ORIENT_DEFAULT', innerProperties.get('Orientation'));
        system.assertEquals('PRN_MSO_PAPER_DEFAULT', innerProperties.get('Size'));
    }
    
    /**
    * When: when generate easyPDF with custom setting
    * Then: then deep complex properties are added
    */ 
    testmethod static void WhenGenerateEasyPdf_ThenDeepComplexPropertiesAdded() {
        // Arrange: Create mock original json and new custom json
        String originalJSON ='{"Excel": {"PrintAllSheets": true,"DocumentPassword": "","Scaling": "EPWF_EXCEL_SCALING_FIT_TO_PAGE","ConversionMethod": "EPWF_CONVERSION_METHOD_DEFAULT"}}';
        String newJson ='{"Excel": {"Scaling": "EPWF_EXCEL_SCALING_FIT_TO_PAGE_TEST","Paper": {"Orientation": "PRN_PAPER_ORIENT_LANDSCAPE","Size": "PRN_MSO_PAPER_DEFAULT","Test" : {"A" : 1, "B" : 2}}}}';
        
        // Act : mix both json
        SQX_PDFWorkflowGenerator obj=new SQX_PDFWorkflowGenerator();
        Map<String, Object> orgJson = (Map<String, Object>) JSON.deserializeUntyped(originalJSON);
        Map<String, Object> nJson = (Map<String, Object>) JSON.deserializeUntyped(newJson);
        obj.mixJSON(orgJson, nJson);
        
        //Assert: ensured that deep complex property is added 
        Map<String, Object> excel = (Map<String, Object>)orgJson.get('Excel');
        Map<String,Object> innerProperties =(Map<String, Object>)excel.get('Paper');
        system.assertEquals('PRN_PAPER_ORIENT_LANDSCAPE', innerProperties.get('Orientation'));
        system.assertEquals('PRN_MSO_PAPER_DEFAULT', innerProperties.get('Size'));
        Map<String,Object> deepProperties =(Map<String, Object>)innerProperties.get('Test');
        system.assertEquals(1, deepProperties.get('A'));
        system.assertEquals(2, deepProperties.get('B'));
    }
}