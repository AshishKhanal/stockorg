/**
* Unit tests for User Job Function Object
*
* @author Dibya Shrestha
* @date 2015/07/02
* 
*/
@IsTest
public class SQX_Test_1296_User_Job_Function {
    
    static Boolean runAllTests = true,
                   run_givenAddUserJobFunction = false,
                   run_givenDeleteUserJobFunction = false,
                   run_givenEditUserJobFunction = false;
    
    /**
    * This test ensures addition of new / duplicate user job function to work as expected.
    * UJF is duplicate when another inactive and not deactivated UJF exists with same job function and user,
    *   and can have many deactivated UJFs with same job function and user.
    */
    public testmethod static void givenAddUserJobFunction(){
        if (!runAllTests && !run_givenAddUserJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        // creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for ujf test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);
            
            SQX_User_Job_Function__c ujf1 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_User__c = testUser1.Id,
                Active__c = false
            );
            
            // insert ujf1
            Database.SaveResult result1 = Database.insert(ujf1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected user job function to be saved.');
            
            SQX_User_Job_Function__c ujf2 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_User__c = testUser1.Id,
                Active__c = true
            );
            
            // insert ujf2
            Database.SaveResult result2 = Database.insert(ujf2, false);
            
            System.assert(result2.isSuccess() == false,
                'Expected duplicate user job function not to be saved.');
            
            // activating ujf1
            ujf1.Active__c = true;
            Database.SaveResult result3 = Database.update(ujf1, false);
            
            // insert ujf2
            Database.SaveResult result4 = Database.insert(ujf2, false);
            
            System.assert(result4.isSuccess() == false,
                'Expected duplicate user job function not to be saved.');
            
            // deactivating ujf1
            ujf1.Active__c = false;
            Database.SaveResult result5 = Database.update(ujf1, false);
            
            // insert ujf2
            Database.SaveResult result6 = Database.insert(ujf2, false);
            
            System.assert(result6.isSuccess() == true,
                'Expected user job function with existing deactivated user job functions to be saved.');
            
            // deactivating ujf2
            ujf2.Active__c = false;
            Database.SaveResult result7 = Database.update(ujf2, false);
            
            SQX_User_Job_Function__c ujf3 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_User__c = testUser1.Id,
                Active__c = false
            );
            
            // insert ujf3
            Database.SaveResult result8 = Database.insert(ujf3, false);
            
            System.assert(result8.isSuccess() == true,
                'Expected user job function with existing deactivated user job functions to be saved.');
        }
    }
    
    /**
    * This test ensures deletion of a user job function to work as expected.
    * Once activated UJF cannot be deleted, otherwise it can be deleted.
    */
    public testmethod static void givenDeleteUserJobFunction(){
        if (!runAllTests && !run_givenDeleteUserJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        // creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for ujf test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);
            
            SQX_User_Job_Function__c ujf1 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_User__c = testUser1.Id,
                Active__c = false
            );
            
            // insert ujf1
            Database.SaveResult saveResult1 = Database.insert(ujf1, false);
            
            // delete ujf1
            Database.DeleteResult delResult1 = Database.delete(ujf1, false);
            
            System.assert(delResult1.isSuccess() == true,
                'Expected not activated user job function to be deleted.');
            
            SQX_User_Job_Function__c ujf2 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_User__c = testUser1.Id,
                Active__c = true
            );
            
            // insert ujf2
            Database.SaveResult saveResult2 = Database.insert(ujf2, false);
            
            // delete ujf2
            Database.DeleteResult delResult2 = Database.delete(ujf1, false);
            
            System.assert(delResult2.isSuccess() == false,
                'Expected once activated user job function not to be deleted.');
        }
    }
    
    /**
    * This test ensures modification of fields of a user job function to work as expected.
    * Cannot edit job function and/or user of a once activated UJF.
    * Cannot set active for a deactivated UJF.
    */
    public testmethod static void givenEditUserJobFunction(){
        if (!runAllTests && !run_givenEditUserJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        // creating required test users
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User testUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(adminUser) {
            // creating required test job functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1 for ujf test' );
            Database.SaveResult jf1Result = Database.insert(jf1, false);
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for ujf test' );
            Database.SaveResult jf2Result = Database.insert(jf2, false);
            
            SQX_User_Job_Function__c ujf1 = new SQX_User_Job_Function__c(
                SQX_Job_Function__c = jf2.Id,
                SQX_User__c = testUser2.Id,
                Active__c = false
            );
            
            // insert ujf1
            Database.SaveResult r1 = Database.insert(ujf1, false);
            
            // change job function and user
            ujf1.SQX_Job_Function__c = jf1.Id;
            ujf1.SQX_User__c = testUser1.Id;
            Database.SaveResult r2 = Database.update(ujf1, false);
            
            System.assert(r2.isSuccess() == true,
                'Expected modification job function and user fields of not activated user job function to be saved.');
                
            // activate ujf1
            ujf1.Active__c = true;
            Database.SaveResult r3 = Database.update(ujf1, false);
            
            // fetch activation date
            ujf1 = [SELECT Id, Activation_Date__c FROM SQX_User_Job_Function__c WHERE Id = :ujf1.Id];
            
            System.assert(ujf1.Activation_Date__c != null,
                'Expected activation date to be set when activating a user job function.');
            
            // change job function and user
            ujf1.SQX_Job_Function__c = jf2.Id;
            ujf1.SQX_User__c = testUser2.Id;
            Database.SaveResult r4 = Database.update(ujf1, false);
            
            System.assert(r4.isSuccess() == false,
                'Expected modification job function and user fields of once activated user job function not to be saved.');
                
            // deactivate ujf1
            ujf1 = new SQX_User_Job_Function__c( Id = ujf1.Id, Active__c = false);
            Database.SaveResult r5 = Database.update(ujf1, false);
            
            // fetch deactivation date
            ujf1 = [SELECT Id, Deactivation_Date__c FROM SQX_User_Job_Function__c WHERE Id = :ujf1.Id];
            
            System.assert(ujf1.Deactivation_Date__c != null,
                'Expected deactivation date to be set when deactivating a user job function.');
            
            // reactivate ujf1
            ujf1.Active__c = true;
            Database.SaveResult r6 = Database.update(ujf1, false);
            
            System.assert(r6.isSuccess() == false,
                'Expected reactivation of deactivated user job function not to be saved.');
        }
    }

}