/**
* Used for creating product history answer records according to the task defined in product history review object.
* @author Anish Shretha 
* @date 2018-02-04
* @story [SQX-915]
*/
public with sharing class SQX_Product_History_Review{    

    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_COMPLETE = 'Completed';

    // relevant observation values
    public static final String RELEVANT_OBSERVATION_YES = 'Yes',
                                RELEVANT_OBSERVATION_NO = 'No';

    /**
    * PHR Trigger Handler performs actions when PHR object is being manipulated
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newPHRs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Product_History_Review__c> newPHRs, Map<Id,SQX_Product_History_Review__c> oldMap){
        
            super(newPHRs, oldMap);
        }

        /**
         * method to copy product history answer from question in CQ Task
         */
        public Bulkified addTaskQuestionToProductHistoryAnswer(){
            final String ACTION_NAME = 'addProductHistoryReviewAnswer';

            Rule hasPHRCreated = new Rule();
            hasPHRCreated.addRule(Schema.SQX_Product_History_Review__c.Status__c, RuleOperator.HasChanged);

            this.resetView()
                .removeProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME)//Remove all the previously processed records
                .applyFilter(hasPHRCreated, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(hasPHRCreated.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME, hasPHRCreated.evaluationResult);

                List<SQX_Product_History_Review__c> productHistoryReviews = (List<SQX_Product_History_Review__c>) hasPHRCreated.evaluationResult;

                Set<Id> cqTaskIds  = this.getIdsForField(productHistoryReviews,Schema.SQX_Product_History_Review__c.SQX_Task__c);

                Map<Id, SQX_Task__c> taskMap = new Map<Id, SQX_Task__c>([SELECT Id, 
                                                                    (SELECT Id, QuestionText__c FROM SQX_Task_Question__r)
                                                                  FROM SQX_Task__c WHERE Id = : cqTaskIds]);

                List<SQX_Product_History_Answer__c> productHistoryAnswerToBeUpdated = new List<SQX_Product_History_Answer__c>();
                for(SQX_Product_History_Review__c phr : productHistoryReviews){
                    if(taskMap.containsKey(phr.SQX_Task__c)){
                        for(SQX_Task_Question__c question : taskMap.get(phr.SQX_Task__c).SQX_Task_Question__r){
                            SQX_Product_History_Answer__c productHistoryAnswer = new SQX_Product_History_Answer__c();
                            productHistoryAnswer.Question__c = question.QuestionText__c;
                            productHistoryAnswer.SQX_Product_History_Review__c = phr.Id;
                            productHistoryAnswer.SQX_Question_With_Task__c = question.Id;
                            productHistoryAnswerToBeUpdated.add(productHistoryAnswer);
                        }
                    }
                }
                
                if(productHistoryAnswerToBeUpdated.size() > 0){
                    new SQX_DB().op_insert(productHistoryAnswerToBeUpdated, new List<Schema.SObjectField>{
                        SQX_Product_History_Answer__c.fields.Question__c,
                        SQX_Product_History_Answer__c.fields.SQX_Question_With_Task__c,
                        SQX_Product_History_Answer__c.fields.SQX_Product_History_Review__c
                    });
                }  
            }
            return this;
        }
 
        /**
        * This method updates the completed date by on PHR of complaint.
        * @author Anish Shrestha
        * @date 2016-01-04
        * @story SQX-915
        */
        public Bulkified setCompletionDateOnPHRCompletion(){

            final String ACTION_NAME = 'setCompletionDateOnPHRCompletion';
            //get all published approved responses in this transaction
            Rule completedPHRRule = new Rule();
            completedPHRRule.addRule(Schema.SQX_Product_History_Review__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Product_History_Review.STATUS_COMPLETE);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME)//Remove all the previously processed records
                .applyFilter(completedPHRRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(completedPHRRule.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME, completedPHRRule.evaluationResult);

                List<SQX_Product_History_Review__c> completedPHRs = completedPHRRule.evaluationResult;

                for(SQX_Product_History_Review__c phr : completedPHRs){
                    phr.Completed_On__c = Date.Today();
                }
            }
            return this;
        }

        /**
        * Method to complte complaint task when PHR is complete
        */
        public Bulkified completeComplaintTask(){
            final String ACTION_NAME = 'completeComplaintTask';

            Rule completedPHRRule = new Rule();
            completedPHRRule.addRule(Schema.SQX_Product_History_Review__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Product_History_Review.STATUS_COMPLETE);

            this.resetView()
                .removeProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME)//Remove all the previously processed records
                .applyFilter(completedPHRRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Product_History_Review__c> productHistoryReviews = completedPHRRule.evaluationResult;

            if(productHistoryReviews.size() > 0){
                this.addToProcessedRecordsFor(SQX.ProductHistoryReview, ACTION_NAME, productHistoryReviews);

                Set<Id> complaintIds  = this.getIdsForField(productHistoryReviews,Schema.SQX_Product_History_Review__c.SQX_Complaint__c); 
                Set<Id> cqTaskIds  = this.getIdsForField(productHistoryReviews,Schema.SQX_Product_History_Review__c.SQX_Task__c); 

                //complete related complaint tasks
                SQX_Complaint_Task.completeComplaintTasks(complaintIds, cqTaskIds);
            }

            return this;
        }
        
        /**
		 * This method checks whether complaint related to PHR to be deleted is locked or not
		 * @author Sanjay Maharjan
		 * @date 2019-01-23
		 * @story [SQX-7470]
		 */
        public Bulkified preventDeletionOfPHROfLockedRecord() {
            
            this.resetView();

            if(this.view.size() >0) {
             		
                Map<Id, SQX_Product_History_Review__c> productHistoryReviewMap = new Map<Id, SQX_Product_History_Review__c>((List<SQX_Product_History_Review__c>) this.view);
            
                for(SQX_Product_History_Review__c phr : [SELECT Id FROM SQX_Product_History_Review__c WHERE Id IN : productHistoryReviewMap.keySet() AND SQX_Complaint__r.Is_Locked__c = true]) {
                    productHistoryReviewMap.get(phr.Id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
                }
                
            }
            
            return this;
            
        }
    }
        
}