/**
* This class is a wrapper for SQX Applications throwing any generic errors
* usage: new SQXApplicationGenericException(String message)
*/
public with sharing class SQX_ApplicationGenericException extends Exception{

}