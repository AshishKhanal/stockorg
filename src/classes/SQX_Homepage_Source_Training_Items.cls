/**
 * This class acts as an item source for Training related items
 * @author  Anuj Bhandari
 * @date    13-04-2017
 */
public with sharing class SQX_Homepage_Source_Training_Items extends SQX_Homepage_ItemSource {

    /*
        Training Items: User Signoff/Trainer Signoff
    */

    private String module = SQX_Homepage_Constants.MODULE_TYPE_DOCUMENT_TRAINING;

    private User loggedInUser;
    
    // holds true if only pending training is needed.
    public Boolean needingOnlyPendingTrainings {get;set;}

    /*
        This is the boundary value that separates urgency for an action on this item from high to critical
        e.x. if the item's due date is less than or equal to LIMIT_FOR_CRITICALITY away from today's date, the urgency is set to High
    */
    public static final Integer LIMIT_FOR_CRITICALITY_TRAINING = 3;


    /**
     *   Constructor method
     */
    public SQX_Homepage_Source_Training_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Controlled_Document__c.SObjectType => new List<SObjectField>
            {
                SQX_Controlled_Document__c.Document_Status__c,
                SQX_Controlled_Document__c.Is_Scorm_Content__c
            },
            SQX_Personnel__c.SObjectType => new List<SObjectField>
            {
                SQX_Personnel__c.SQX_User__c
            },
            SQX_Personnel_Document_Training__c.SObjectType => new List<SObjectField> 
            {
                SQX_Personnel_Document_Training__c.Can_Take_Assessment__c,
                SQX_Personnel_Document_Training__c.CreatedDate,
                SQX_Personnel_Document_Training__c.Due_Date__c,
                SQX_Personnel_Document_Training__c.Is_Refresher__c,
                SQX_Personnel_Document_Training__c.Is_Training_Session_Complete__c,
                SQX_Personnel_Document_Training__c.Name,
                SQX_Personnel_Document_Training__c.Status__c,
                SQX_Personnel_Document_Training__c.SQX_Assessment__c,
                SQX_Personnel_Document_Training__c.SQX_Controlled_Document__c,
                SQX_Personnel_Document_Training__c.SQX_Personnel__c,
                SQX_Personnel_Document_Training__c.SQX_Trainer__c,
                SQX_Personnel_Document_Training__c.SQX_Training_Session__c,
                SQX_Personnel_Document_Training__c.Title__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
     * returns a list of homepage items for the current user related to Training Module
     * Also returns pending trainig records for personnel if being called from personnel view. [SQX-5316] 
     * @author : Piyush Subedi
     * @story  : [SQX-5316],[SQX-7241]
     */
    protected override List<SObject> getUserRecords() {
        Set<Id> queueIds = SQX_Homepage_Helper.getAllIdsForLoggedInUser();
        List<SQX_Personnel_Document_Training__c> items = new List<SQX_Personnel_Document_Training__c>();
        if(this.needingOnlyPendingTrainings != true) {
            items = [
                SELECT Id,
                Name,
                CreatedDate,
                Title__c,
                Due_Date__c,
                Status__c,
                Is_Refresher__c,
                SQX_Assessment__c,
                SQX_Personnel__c,
                Can_Take_Assessment__c,
                SQX_Training_Session__c,
                Is_Training_Session_Complete__c,
                SQX_Controlled_Document__c,
                SQX_Controlled_Document__r.Is_Scorm_Content__c,
                Document_Title__c,Document_Number_Rev__c
                FROM SQX_Personnel_Document_Training__c
                WHERE
                (
                    SQX_Controlled_Document__r.Document_Status__c IN: SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING
                    AND
                    (
                        (
                            SQX_Personnel__r.SQX_User__c IN: queueIds
                            AND
                            Status__c =: SQX_Personnel_Document_Training.STATUS_PENDING
                        ) 
                        OR
                        (
                            SQX_Trainer__c IN: queueIds
                            AND
                            Status__c = : SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING
                        )
                    )
                )
                ORDER BY CreatedDate DESC
            ];
            
        }
        else{
            items = [ SELECT Id,
                     Name,
                     CreatedDate,
                     Title__c,
                     Due_Date__c,
                     Status__c,
                     Is_Refresher__c,
                     SQX_Assessment__c,
                     SQX_Personnel__c,
                     Can_Take_Assessment__c,
                     SQX_Training_Session__c,
                     Is_Training_Session_Complete__c,
                     SQX_Controlled_Document__c,
                     Document_Title__c,
                     Document_Number_Rev__c,
                     SQX_Controlled_Document__r.Is_Scorm_Content__c
                     FROM SQX_Personnel_Document_Training__c
                     WHERE SQX_Controlled_Document__r.Document_Status__c IN: SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING
                     AND SQX_Personnel__r.SQX_User__c IN: queueIds
                     AND Status__c =: SQX_Personnel_Document_Training.STATUS_PENDING
                     ORDER BY CreatedDate DESC];
        }
        return items;
    }

    /**
     *   Method returns a SQX_Homepage_Item type item from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        return getTrainingItem((SQX_Personnel_Document_Training__c) item);

    }

    /**
     *   Method returns a SQX_Homepage_Item with correct values for item of different trainig signoff types from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getTrainingItem(SQX_Personnel_Document_Training__c item) {

        SQX_Homepage_Item tItem = new SQX_Homepage_Item();

        tItem.itemId = item.Id;

        tItem.dueDate = Date.valueOf(item.Due_Date__c);

        tItem.urgency = this.getItemUrgency(tItem.dueDate, LIMIT_FOR_CRITICALITY_TRAINING);

        // set action type
        tItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

        tItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        tItem.moduleType = this.module;

        // set related record
        tItem.relatedRecord = new SQX_Controlled_Document__c(Id=item.SQX_Controlled_Document__c,Title__c=item.Document_Title__c,Name=item.Document_Number_Rev__c);
       
        if (item.Status__c == SQX_Personnel_Document_Training.STATUS_PENDING) {
            // user sign-off

            // set actions for the current item
            tItem.actions = getActionsForUserSignoff(item);

            // set feed text for the current item
            tItem.feedText = getUserSignoffFeedText(item);

        } else {

            // set actions for the current item
            tItem.actions = getActionsForTrainerSignoff(item);

            // set feed text for the current item
            tItem.feedText = getTrainerSignoffFeedText(item);

        }

        tItem.creator = loggedInUser;

        return tItem;

    }


    /**
     *   Method returns a list of actions associated with the User Signoff pending item
     *   @param item - document training record under consideration
     */
    private List<SQX_Homepage_Item_Action> getActionsForUserSignoff(SQX_Personnel_Document_Training__c item) {

        List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

        // Action for content view
        if(item.SQX_Controlled_Document__r.Is_Scorm_Content__c)
        {
            // show launch
            SQX_Homepage_Item_Action launchAction = new SQX_Homepage_Item_Action();
            launchAction.name = SQX_Homepage_Constants.ACTION_LAUNCH_CONTENT;
            launchAction.actionUrl = SQX_Homepage_Constants.SF_BASE_URL + Page.SQX_eContent.getURL() + '?id=' + item.Id;
            launchAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_TOUCH_ACTION;
            launchAction.supportsBulk = false;

            actions.add(launchAction);
        }
        else
        {
            // add view action
            actions.add(getViewAction(item.SQX_Controlled_Document__c));
        }

        // Add actions for sign-off
        if(String.isBlank(item.SQX_Training_Session__c) ||
           item.Is_Training_Session_Complete__c)
        {

            if(item.Can_Take_Assessment__c)
            {
                // show Take Assessment link
                SQX_Homepage_Item_Action takeAssessmentAction = new SQX_Homepage_Item_Action();
                takeAssessmentAction.name = SQX_Homepage_Constants.ACTION_TAKE_ASSESSMENT;
                takeAssessmentAction.actionUrl =  SQX_Homepage_Constants.SF_BASE_URL + Page.SQX_Assessment_Start.getURL() + '?astId=' + item.SQX_Assessment__c + '&psnId=' + item.SQX_Personnel__c
                                                    + '&retURL=' + SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true);
                takeAssessmentAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_CREATE_RECORD;
                takeAssessmentAction.supportsBulk = false;

                actions.add(takeAssessmentAction);
            }
            else if(!item.SQX_Controlled_Document__r.Is_Scorm_Content__c)
            {
                // show user sign-off action
                SQX_Homepage_Item_Action signoffAction = new SQX_Homepage_Item_Action();
                signoffAction.name = SQX_Homepage_Constants.ACTION_USER_SIGNOFF;
                signoffAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_QA;
                signoffAction.supportsBulk = true;
                signoffAction.recordId = item.Id;
                signoffAction.isLightningComponent = UserInfo.getUiThemeDisplayed() == SQX_Utilities.SF_LIGHTING_DISPLAYED_THEME;
                signoffAction.actionId = 'usersignoff';
                if(signoffAction.isLightningComponent) {
                    signoffAction.actionUrl =  SQX.NSPrefix + 'SQX_Training_Sign_Off_Records';
                } else {
                    signoffAction.actionUrl =  SQX_Homepage_Constants.SF_BASE_URL + Page.SQX_PersonnelDocTraining_User_SignOff.getURL() +
                                            '?retUrl=' + SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true) + '&dtnId=' + item.Id;
                }
                actions.add(signoffAction);
            }

        }

        return actions;
    }

    /**
     *   Method returns a list of actions associated with the Trainer Signoff pending item
     *   @param item - document training record record under consideration
     */
    private List<SQX_Homepage_Item_Action> getActionsForTrainerSignoff(SQX_Personnel_Document_Training__c item) {

        List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

        // Action for content view
        actions.add(getViewAction(item.SQX_Controlled_Document__c));

        // Add action for sign-off
        if(String.isBlank(item.SQX_Training_Session__c) ||
           item.Is_Training_Session_Complete__c)
        {
            // show trainer sign-off action
            SQX_Homepage_Item_Action signoffAction = new SQX_Homepage_Item_Action();
            signoffAction.name = SQX_Homepage_Constants.ACTION_TRAINER_SIGNOFF;
            signoffAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_PEOPLE;
            signoffAction.supportsBulk = true;
            signoffAction.recordId = item.Id;
            signoffAction.isLightningComponent = UserInfo.getUiThemeDisplayed() == SQX_Utilities.SF_LIGHTING_DISPLAYED_THEME;
            signoffAction.actionId = 'trainersignoff';
            if(signoffAction.isLightningComponent) {
                signoffAction.actionUrl =  SQX.NSPrefix + 'SQX_Training_Sign_Off_Records';
            } else {
                signoffAction.actionUrl =  SQX_Homepage_Constants.SF_BASE_URL + Page.SQX_PersonnelDocTraining_Trainer_SignOff.getURL() + '?retUrl=' + SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true) + '&dtnId=' + item.Id;
            }

            actions.add(signoffAction);
        }

        return actions;
    }

    /**
     *   Returns the view action for Doc Training
     *   @param itemId - id of the record for which action is to be set
     */
    private SQX_Homepage_Item_Action getViewAction(string itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW_CONTENT;

        PageReference pr = Page.SQX_View_Controlled_Document;
        pr.getParameters().put('Id', itemId);

        action.actionUrl = pr.getUrl();

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return action;
    }


    /**
     *   Method returns the user signoff feed text for the given item
     */
    private String getUserSignoffFeedText(SQX_Personnel_Document_Training__c item) {

        String template = item.Is_Refresher__c ? SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_USER_REFRESHER : SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_USER;
        return String.format(template, new String[] {
            item.Name,
            item.Title__c,
            item.Document_Number_Rev__c
        });
    }

    /**
     *   Method returns Trainer signoff feed text for the given item
     */
    private String getTrainerSignoffFeedText(SQX_Personnel_Document_Training__c item) {

        String template = item.Is_Refresher__c ? SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER_REFRESHER : SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER;

        return String.format(template, new String[] {
            item.Name,
            item.Title__c,
            item.Document_Number_Rev__c
        });
    }
}