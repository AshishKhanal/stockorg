/**
* Batch processor to migrate all old record activities of the following objects
* NC,CAPA,Audit,Audit Program,Equipment,Complaint,Electronic Signature and Change Order
* Added in version  : 7.0
* Used for versions : less than 7.0
*/
global with sharing class SQX_7_0_Record_Activity_Migration implements Database.Batchable<SObject> {
    //record activity objects
    Map<SObjectType, SObjectType> ObjectTypes = SQX_Record_History.RECORD_ACTIVITY_OBJECTTYPE;
    
    //record activity objects to migrate
    List<SObjectType> ObjectTypesToMigrate = new List<SObjectType>();
    
    //declare map for each objects
    final Map<SObjectType, Map<String, String>> purposeOfSigsMap;
    
    // default index of process objectType should be 0
    private Integer processObjectTypeIndex = 0;
    

    global SQX_7_0_Record_Activity_Migration(){
        this(0);
    }
    
    private SQX_7_0_Record_Activity_Migration(Integer objectTypeIndex){
        processObjectTypeIndex=objectTypeIndex;
        
        purposeOfSigsMap = new Map<SObjectType, Map<String, String>> {
            SQX_NC_Record_Activity__c.SObjectType => new SQX_Extension_NC(new ApexPages.StandardController(new SQX_Nonconformance__c())).getPurposeOfSig(),
            SQX_CAPA_Record_Activity__c.SObjectType => new SQX_Extension_CAPA(new ApexPages.StandardController(new SQX_CAPA__c())).getPurposeOfSig(),
            SQX_Audit_Record_Activity__c.SObjectType => new SQX_Extension_Audit(new ApexPages.StandardController(new SQX_Audit__c())).getPurposeOfSig(),
            SQX_Audit_Program_Record_Activity__c.SObjectType => new SQX_Extension_Audit_Program(new ApexPages.StandardController(new SQX_Audit_Program__c())).getPurposeOfSig(),
            SQX_Complaint_Record_Activity__c.SObjectType => new SQX_Extension_Complaint(new ApexPages.StandardController(new SQX_Complaint__c())).getPurposeOfSig(),
            SQX_Electronic_Signature_Record_Activity__c.SObjectType => SQX_Controller_ES_Setting.purposeOfSigMap,
            SQX_Equipment_Record_Activity__c.SObjectType => SQX_Equipment.purposeOfSigMap, 
            SQX_Change_Order_Record_Activity__c.SObjectType => new SQX_Extension_Change_Order(new ApexPages.StandardController(new SQX_Change_Order__c())).getPurposeOfSig()
        };
        
        //add record activity object name to migrate list variable
        for (SObjectType sobjname: ObjectTypes.keySet()) {
            ObjectTypesToMigrate.add(ObjectTypes.get(sobjname));
        }   
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        Database.QueryLocator sobjecRecordList= Database.getQueryLocator('SELECT Activity_Code__c,Purpose_Of_Signature__c FROM '+ ObjectTypesToMigrate.get(processObjectTypeIndex) + ' WHERE Activity_Code__c = null');
        return sobjecRecordList;
    }
    
    global void execute(Database.BatchableContext ctx, List<SObject> lstsObject) {
        if (lstsObject.size() > 0) {
            SObjectType listType = lstsObject.get(0).getSObjectType();
            Map<String, SObjectField> fields = listType.getDescribe().fields.getMap();
            final SObjectField PURPOSE_FIELD = fields.get(SQX.getNSNameFor('Purpose_Of_Signature__c')),
                               CODE_FIELD = fields.get(SQX.getNSNameFor('Activity_Code__c'));
            
            Map<String, String> purposeOfSig = purposeOfSigsMap.get(listType);
            Map<String, String> invertedMap = new Map<String, String>();
            for(String key : purposeOfSig.keySet()) {
                invertedMap.put(purposeOfSig.get(key), key);
            }
            
            for(SObject obj : lstsObject) {
                String purpose = (String)obj.get(PURPOSE_FIELD);
                if(invertedMap.containsKey(purpose)) {
                    obj.put(CODE_FIELD, invertedMap.get(purpose));
                }
            }
            Database.update(lstsObject, false);
        }
    }
 
    global void finish(Database.BatchableContext bc){
        processObjectTypeIndex++;
        if (processObjectTypeIndex < ObjectTypesToMigrate.size()) {
            SQX_7_0_Record_Activity_Migration processor = new SQX_7_0_Record_Activity_Migration(processObjectTypeIndex);
            Database.executeBatch(processor);   
        }
    }
}