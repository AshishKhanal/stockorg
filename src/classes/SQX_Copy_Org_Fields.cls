/**
 * class to copy org fields from personnel to record with owner matching with personnel
 */
public with sharing class SQX_Copy_Org_Fields {
    
    /**
     * method to copy org fields from personnel to record with owner matching with personnel
     * @param recordIds record ids in which org fields are to be copied
     */
    @InvocableMethod(Label='CQ Copy Org Fields')
    public static void copyOrgFieldsFromPersonnel(List<Id> recordIds){
        copyOrgFields(recordIds);
    }
    
    /**
     * method to copy org fields from personnel to records
     * @param records record ids of the record in which org fields are to be copied
     */
    public static void copyOrgFields(List<Id> records){
        
        Id recordId = records[0];
        
        SObjectType recordtype = recordId.getSobjectType();
        String sObjName = recordtype.getDescribe().getName();

        String query = 'SELECT Id, OwnerId, Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c FROM ';
        query = query + sObjName;
        query = query + ' WHERE Id =: records' ;
        List<sObject> sobjList = Database.query(query);
        
        Set<Id> userIds = new Set<Id>();
        for(SObject record : sobjList){
            userIds.add((Id) record.get('OwnerId'));
        }
        
        List<SQX_Personnel__c> personnels = new List<SQX_Personnel__c>();
        Map<Id, SQX_Personnel__c> personnelMap = new Map<Id, SQX_Personnel__c>();
        personnels =[SELECT Id, Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c, SQX_User__c  FROM SQX_Personnel__c WHERE SQX_User__c =: userIds];
        for(SQX_Personnel__c personnel : personnels){
            personnelMap.put(personnel.SQX_User__c, personnel);
        }
        
        if(personnels.size() > 0){
            List<SObject> objectsToUpdate = new List<SObject>();
            for(SObject record : sobjList){
                Id recordOwner = (ID) record.get('OwnerId');
                if(personnelMap.containsKey(recordOwner)){
                    SQX_Personnel__c personnel = personnelMap.get(recordOwner);
                    SObject objectToUpdate = recordtype.newSObject();
                    objectToUpdate.put('Id', (ID) record.get('Id'));
                    if(String.isBlank((String) record.get('Org_Division__c')) &&
                      	String.isBlank((String) record.get('Org_Business_Unit__c')) &&
                      	String.isBlank((String) record.get('Org_Region__c')) &&
                      	String.isBlank((String) record.get('Org_Site__c'))) {
                            
                        objectToUpdate.put('Org_Division__c', personnel.Org_Division__c);
                        objectToUpdate.put('Org_Business_Unit__c', personnel.Org_Business_Unit__c);
                        objectToUpdate.put('Org_Region__c', personnel.Org_Region__c);
                        objectToUpdate.put('Org_Site__c', personnel.Org_Site__c);
                        objectsToUpdate.add(objectToUpdate);
                    }
                }
            }
            
            
            if(objectsToUpdate.size() > 0){
                Map<String, SObjectField> fieldsMap = new Map<String, SObjectField>();
                fieldsMap = recordtype.getDescribe().fields.getMap();
                new SQX_DB().op_update(objectsToUpdate, new List<Schema.SObjectField> {
                    fieldsMap.get(SQX.NSPrefix + 'Org_Division__c'),
                    fieldsMap.get(SQX.NSPrefix + 'Org_Business_Unit__c'),
                    fieldsMap.get(SQX.NSPrefix + 'Org_Region__c'),
                    fieldsMap.get(SQX.NSPrefix + 'Org_Site__c')
                });
            }
            
        }
    }
}