/**
 * Test class to ensure SQX_RegulatoryReport_EmailBatchProcessor batch job works as expected.
 */
@isTest
public class SQX_Test_8080_RegulatoryEmailBatchProces {
    @testSetup
    static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');

        System.runAs(admin1){
            Account acc = SQX_Test_Account_Factory.createAccount();
            Contact con = SQX_Test_Account_Factory.createContact(acc);
            con.email = 'someone@ambarkaar.com';
            update con;
        }
    }

    /**
     * Given : Regulatory Report, Reporting Default and Submission History record
     * When : Batch Job started
     * Then : Email is sent to CA Contact (SQX_Reporting_Site_Contact__c) of Reporting Default record,
        and if every thing is fine then Status of Submission History is updated to Complete
        else if any error occurs then Status will be set to Transmission Failure
     *
     */
    public static testMethod void ensureEmailBatchProcessorWorksCorrectly() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User admin = users.get('admin1');
        System.runas(admin) {

            // Arrange : Create Complaint, Regulatory Report,Submission History with files and Reporting Default record
            SQX_Complaint__c complaint = new SQX_Complaint__c();
            complaint.Country_of_Origin__c = 'AF';
            insert complaint;

            SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c();
            report.SQX_Complaint__c = complaint.Id;
            report.Due_Date__c = Date.today();
            insert report;

            SQX_Submission_History__c sh = new SQX_Submission_History__c();
            sh.SQX_Complaint__c = complaint.Id;
            sh.Submitted_By__c = admin.Email;
            sh.SQX_Submitted_By__c = admin.Id;
            sh.Status__c = 'Pending';
            sh.SQX_Regulatory_Report__c = report.Id;
            sh.Delivery_Type__c = 'Email';
            insert sh;

            // insert files
            ContentVersion cv=new Contentversion();
            cv.title='ABC';
            cv.PathOnClient ='test';
            cv.versiondata=Blob.valueOf('Sample.txt');
            insert cv;

            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            System.assert(documents.size() > 0, 'Expected one content document but got ' + documents.size());
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=sh.id;
            contentlink.ShareType= 'V';
            contentlink.ContentDocumentId=documents[0].Id;
            contentlink.Visibility = 'AllUsers';
            insert contentlink;

            // get contact
            Contact reportingContact = [SELECT Id FROM Contact LIMIT 1];
            // insert reporting default
            SQX_Reporting_Default__c reportingDefault = new SQX_Reporting_Default__c();
            reportingDefault.SQX_CA_Contact__c = reportingContact.Id;
            insert reportingDefault;

            update new SQX_Regulatory_Report__c(Id = report.Id, SQX_Reporting_Default__c = reportingDefault.Id);

            // Act : Execute batch
            SQX_RegulatoryReport_EmailBatchProcessor emailBatchProcessor = new SQX_RegulatoryReport_EmailBatchProcessor();

            Test.startTest();
            Database.executeBatch(emailBatchProcessor, 1);
            Test.stopTest();

            // Assert : Ensure Status of Submission History is Complete
            sh = [SELECT Id, Status__c FROM SQX_Submission_History__c WHERE Id = :sh.Id];
            System.assertEquals(SQX_Submission_History.STATUS_COMPLETE, sh.Status__c);
        }
    }


    /**
     * Given : Regulatory Report, Reporting Default and Submission History record
     * When : Batch Job started
     * Then : Email is sent to CA Contact (SQX_Reporting_Site_Contact__c) of Reporting Default record,
        and if every thing is fine then Status of Submission History is updated to Complete
        else if any error occurs then Status will be set to Transmission Failure
     *
     */
    public static testMethod void ensureEmailBatchProcessorFailsWhenExpected() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User admin = users.get('admin1');
        System.runas(admin) {

            // Arrange : Create Complaint, Regulatory Report,Submission History with files and Reporting Default record
            SQX_Complaint__c complaint = new SQX_Complaint__c();
            complaint.Country_of_Origin__c = 'AF';
            insert complaint;

            SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c();
            report.SQX_Complaint__c = complaint.Id;
            report.Due_Date__c = Date.today();
            insert report;

            SQX_Submission_History__c sh = new SQX_Submission_History__c();
            sh.SQX_Complaint__c = complaint.Id;
            sh.Submitted_By__c = admin.Email;
            sh.SQX_Submitted_By__c = admin.Id;
            sh.Status__c = 'Pending';
            sh.SQX_Regulatory_Report__c = report.Id;
            sh.Delivery_Type__c = 'Email';
            insert sh;

            // insert files
            ContentVersion cv=new Contentversion();
            cv.title='ABC';
            cv.PathOnClient ='test';
            cv.versiondata=Blob.valueOf('Sample.txt');
            insert cv;

            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            System.assert(documents.size() > 0, 'Expected one content document but got ' + documents.size());
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=sh.id;
            contentlink.ShareType= 'V';
            contentlink.ContentDocumentId=documents[0].Id;
            contentlink.Visibility = 'AllUsers';
            insert contentlink;

            // get contact
            Contact reportingContact = [SELECT Id FROM Contact LIMIT 1];
            // insert reporting default
            SQX_Reporting_Default__c reportingDefault = new SQX_Reporting_Default__c();
            reportingDefault.SQX_CA_Contact__c = reportingContact.Id;
            insert reportingDefault;

            update new SQX_Regulatory_Report__c(Id = report.Id, SQX_Reporting_Default__c = reportingDefault.Id);

            // remove email from contact, this should fail the batch job processor
            Contact ct = [SELECT Id FROM Contact];
            ct.email = null;
            update ct;

            // Act : Execute batch
            SQX_RegulatoryReport_EmailBatchProcessor emailBatchProcessor = new SQX_RegulatoryReport_EmailBatchProcessor();

            Test.startTest();
            Database.executeBatch(emailBatchProcessor, 1);
            Test.stopTest();

            // Assert : Ensure Status of Submission History denotes failure
            sh = [SELECT Id, Status__c, Batch_Job_Error__c FROM SQX_Submission_History__c WHERE Id = :sh.Id];
            System.assertEquals(SQX_Submission_History.STATUS_TRANSMISSION_FAILURE, sh.Status__c);
            System.assertNotEquals(null, sh.Batch_Job_Error__c, 'Expected batch job error to be set in case of failure');
        }
    }
}