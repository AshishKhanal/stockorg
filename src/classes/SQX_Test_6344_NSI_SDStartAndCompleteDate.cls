/*
 * Unit test for adding start date and completion date to NSI onboarding steps and Supplier deviation - deviation process steps
 */ 
@isTest
public class SQX_Test_6344_NSI_SDStartAndCompleteDate {
    
    @testSetup
    public static void commonSetup(){
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
             
    }
    
    public static void commonSetupForNSI() {
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser});
        SQX_Test_NSI nsiRecord;
        
        System.runAs(standardUser) {
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 1);  
            
            nsiRecord = new SQX_Test_NSI().save();
            nsiRecord.setStage(SQX_NSI.STAGE_TRIAGE);
            nsiRecord.save();
            nsiRecord.initiate();
            //Assert: Make sure newly created opened record start date is filled with the date when it was opened.
            SQX_Onboarding_Step__c onboardingRecord = [Select id, start_date__c, completion_date__c from SQX_Onboarding_Step__c];
            System.assertEquals(String.valueOf(onboardingRecord.start_date__c), String.valueOf(Date.today()));
        }
    }
    
    /*
    * Given: Onboarding Step Record in open status
    * When : Onboarding step related sf task is completed 
    * Then : Onboarding step completion date field be should be filled with that date
    */
    @isTest
    public static void whenOnboardingIsComplete_ThenCompletionDateIsFilled() {
        commonSetupForNSI();
        List<SQX_New_Supplier_Introduction__c> nsiRecord = [Select id, name from SQX_New_Supplier_Introduction__c];
        
        System.runAs(SQX_Test_Account_Factory.getUsers().get('assigneeUser')) {
            //Act: Complete onboarding record
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord[0].Id];
            Task sfTask = sfTasks.get(0);
            
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            
            //Assert: Make sure completed record date is filled with the date whenthe task was completed.
            SQX_Onboarding_Step__c onboardingStep = [Select id, start_date__c, completion_date__c from SQX_Onboarding_Step__c];
            System.assertEquals(true, onboardingStep.completion_date__c != null);
            System.assertEquals(String.valueOf(onboardingStep.completion_date__c), String.valueOf(Date.today()));
        }    
    }
    
    /*
    * Given: Onboarding Step Record in open status
    * When : Onboarding step is completed and redone
    * Then : New onboarding step start date should be filled as it will be in open status.
    */ 
    @isTest
    public static void whenCompletedOnboardingIsRedone_ThenStartDateIsFilledForNewOnboardingStep() {
        commonSetupForNSI();
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        SQX_New_Supplier_Introduction__c nsiRecord = [Select id, name from SQX_New_Supplier_Introduction__c];
        
        //Arrange: Get onboarding step record with complete no go result.
        System.runAs(assigneeUser){
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord.Id];
            Task sfTask = sfTasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
        }
        System.runAs(standardUser) {
             //Act: Redo the record
            SQX_Onboarding_Step__c onBoardingStepRecord =  [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsiRecord.Id 
                                                            AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                                            AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
            
            SQX_Test_NSI os = new SQX_Test_NSI();
            os.redoOnboardingStep(onBoardingStepRecord);
            
            SQX_Onboarding_Step__c openOnboardingStepRecord =  [SELECT Id, start_date__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsiRecord.Id 
                                                                AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            
            //Assert: Make sure new onboarding record created from redone have start date filled.
            System.assertEquals(2, [SELECT Id FROM Task WHERE WhatId =: nsiRecord.Id].size(), 'Expected 2 sf tasks');
            System.assertEquals(String.valueOf(openOnboardingStepRecord.start_date__c), String.valueOf(Date.today()));
        }
        
    }
    
    public static void commonSetupForSupplierDeviation() {
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, assigneeUser});
        SQX_Test_Supplier_Deviation sdRecord;
        
        System.runAs(standardUser) {
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, 'Task', assigneeUser, 1);  
            
            sdRecord = new SQX_Test_Supplier_Deviation().save();
            sdRecord.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            sdRecord.save();
            sdRecord.initiate();
            //Assert: Make sure newly created opened record start date is filled with the date when it was opened.
            SQX_Deviation_Process__c deviationRecord = [Select id, start_date__c from SQX_Deviation_Process__c];
            System.assertEquals(String.valueOf(deviationRecord.start_date__c), String.valueOf(Date.today()));
        }
    }
    
    /*
    * Given: Deviation Step Record in open status
    * When : Deviation step related sf task is completed 
    * Then : Deviation step completion date field be should be filled with that date
    */
    @isTest
    public static void whenDeviationStepIsComplete_ThenCompletionDateIsFilled() {
        commonSetupForSupplierDeviation();
        List<SQX_Supplier_Deviation__c> sdRecord = [Select id, name from SQX_Supplier_Deviation__c];
        
        System.runAs(SQX_Test_Account_Factory.getUsers().get('assigneeUser')) {
            //Act: Complete Deviation record
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord[0].Id];
            Task sfTask = sfTasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            //Assert: Make sure completed record date is filled with the date whenthe task was completed.
            SQX_Deviation_Process__c deviationStep = [Select id, start_date__c, completion_date__c from SQX_Deviation_Process__c];
            System.assertEquals(true, deviationStep.completion_date__c != null);
            System.assertEquals(String.valueOf(deviationStep.completion_date__c), String.valueOf(Date.today()));
        }    
    }
    
    /*
    * Given: Deviation Step Record in open status
    * When : Deviation step is completed and redone
    * Then : New Deviation step start date should be filled as it will be in open status.
    */ 
    @isTest
    public static void whenCompletedDeviationStepIsRedone_ThenStartDateIsFilledForDeviationStep() {
        commonSetupForSupplierDeviation();
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        SQX_Supplier_Deviation__c sdRecord = [Select id, name from SQX_Supplier_Deviation__c];
        
        //Arrange: Get deviation process step record with complete no go result.
        System.runAs(assigneeUser){
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.Id];
            
            Task sfTask = sfTasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
        }
        System.runAs(standardUser) {
             //Act: Redo the record
            SQX_Deviation_Process__c deviationStepRecord =  [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sdRecord.Id 
                                                            AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                                            AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
            
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation();
            sd.redoDeviationProcess(deviationStepRecord);
            
            SQX_Deviation_Process__c openDeviationStepRecord =  [SELECT Id, start_date__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sdRecord.Id 
                                                                AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            
            //Assert: Make sure new deviation process step record created from redone and have start date filled.
            System.assertEquals(2, [SELECT Id FROM Task WHERE WhatId =: sdRecord.Id].size(), 'Expected 2 sf tasks');
            System.assertEquals(String.valueOf(openDeviationStepRecord.start_date__c), String.valueOf(Date.today()));
        }
        
    }
    
}