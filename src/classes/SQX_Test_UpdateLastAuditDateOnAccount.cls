/**
* test to ensure that audit info is rolled up to account
*/
@istest
public class SQX_Test_UpdateLastAuditDateOnAccount{    
    
      /**
      * ensures that last audit date is copied to Account if it is the max one.
      */
      static testmethod void testtrigger1(){
            
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
          Account account = new Account();
          account.name = 'Test Account' + Math.random();
          account.AccountNumber = '_' + Math.random();

          account.Sic = '123123123';
          
          insert account;
          
          Contact C1 = new Contact();
          C1.Salutation = 'Mr';
          C1.FirstName = 'User';
          C1.lastname = 'Dummy';    
          C1.AccountID = account.id; 
          
          insert c1;
          
          User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
          
          integer randomNumber = (integer)(Math.random() * 1000000);
          SQX_Department__c department = new SQX_Department__c();
          SQX_Division__c division = new SQX_Division__c();

          System.runas(adminUser){

          department.Name = 'Department' + randomNumber;

          new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                  Schema.SQX_Department__c.Name
              } ) ;

          division.Name = 'Division' + randomNumber;

          new SQX_DB().op_insert(new List<SQX_Division__c> {division}, new List<Schema.SObjectField>{
                  Schema.SQX_Division__c.Name
              } ) ;
          }
          
          SQX_Audit__C Audit1 = new SQX_Audit__C();
          Audit1.Account__c = account.id;
          Audit1.Number_of_Findings__c = 2;
          Audit1.Audit_Result__c = 'Pass';          
          Audit1.Audited_On__c = date.parse('12/27/2009');
          Audit1.Lead_Auditor__c = 'Hemu';
          Audit1.Audit_Type__c = 'Internal';
          Audit1.Title__c = 'Audit Title';
          Audit1.Start_Date__c = Date.Today();
          Audit1.Audit_Category__c = 'ISO';
          Audit1.SQX_Department__c = department.Id;
          Audit1.SQX_Division__c = division.Id;
          Audit1.Org_Division__c = 'Sample Division';
          Audit1.SQX_Auditee_Contact__c = adminUser.Id;

          Audit1.Primary_Contact_Name__c = c1.id;
          
          insert Audit1;
          
          account = [SELECT Id,Last_Audit_Date__c 
                     From Account 
                     where id = :account.id limit 1];
          
          system.assert(Audit1.Audited_On__c == account.Last_Audit_Date__c, 
                        'Last Update Date on Account not Updated from Audit Object ON CREATE');
          
          Audit1.Audited_On__c =  date.parse('12/12/2012');
          update Audit1;
          
          account = [SELECT Id,Last_Audit_Date__c From account where id = :account.id limit 1];
          system.assert(Audit1.Audited_On__c ==account.Last_Audit_Date__c, 
                        'Last Update Date on Account not Updated from Audit Object OON UPDATE');

        }
          
      }
}