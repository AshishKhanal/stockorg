/**
 * Extension method to perform various action on Assessment
 */
global with sharing class SQX_Extension_Assessment {
    
    //The Assessment that is currently being edited/created
    private SQX_Assessment__c assessment;

    // Holds the original assessment which does not get modified ie clone of assessment.
    private SQX_Assessment__c orignalAssessment;

    //reference to the standard controller
    ApexPages.StandardController ctrller;
    
    //questions to be deleted
    List<SQX_Assessment_Question__c> removedQuestions = new List<SQX_Assessment_Question__c>();
    
    //questions that is being created or edited in the assessment
    global List<SQX_Assessment_Question__c> questions { get; set; }

    /*
    *   Property determines whether the logged in user has edit access on the assessment record
    */
    global Boolean HasEditAccess { get; private set; }

    /**
     * This method sets the Uniqueness_Constraint__c field in each index of the question list. this will be helpful while
     * removing question by passing corresponding  Uniqueness_Constraint__c value.(see removeQuestion method for detail)
     */
    private void setUniqueQuestionIndex() {
        for (Integer i = 0; i < questions.size(); i++) {
            questions[i].Uniqueness_Constraint__c = String.valueOf(i);
        }
    }
    
    /**
     * Constructor for the apex page
     */
    global SQX_Extension_Assessment(ApexPages.StandardController controller) {
        ctrller = controller;
        try {
            ctrller.addFields(new List<String>(SQX_Assessment__c.SObjectType.getDescribe().fields.getMap().keySet()));
        } catch (Exception e) {
            //ignore add fields error
        }
        assessment = (SQX_Assessment__c)ctrller.getRecord();
        orignalAssessment = assessment.clone();
        
        //get the assessment questions all fields dynamically
        if (assessment.Id != null) {
            SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
            filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            filter.field = '' + SQX_Assessment_Question__c.SQX_Assessment__c;
            filter.value = assessment.Id;
            questions = (List<SQX_Assessment_Question__c>) SQX_DynamicQuery.getallFields(SQX_Assessment_Question__c.SObjectType.getDescribe(), new Set<Schema.SObjectField>{},
                filter, new List<Schema.SObjectField>{ SQX_Assessment_Question__c.Question_Number__c }, true);
        } else {
            //make the empty List if the id of of assessment is null ie if we are creating new assessment
            questions = new List<SQX_Assessment_Question__c>{};
        }

        setUniqueQuestionIndex();

        HasEditAccess = SQX_Approval_Util.getUserRecordAccess(new Set<Id> { assessment.Id }, UserInfo.getUserId(), true).get(assessment.Id).HasEditAccess;
    }

    private Boolean IsScormType;
    global Boolean getIsScormType(){

        if(IsScormType == null){
            IsScormType = (assessment.RecordTypeId == null ? false : assessment.RecordTypeId == SQX_Assessment.getScormAssessmentRecordTypeId());
        }
        return IsScormType;
    }


    private String pathOnClient;

    /**
     * This method returns the PathOnClient(fileName) for the latest version of the given contentDocument
     */
    global String getAssessmentTitle(){

        if(pathOnClient == null){
            pathOnClient = [SELECT pathOnClient FROM ContentVersion WHERE ContentDocumentId =: assessment.SCORM_Content_Reference__c AND IsLatest=true].pathOnClient;
        }
        return pathOnClient;
    }

    /**
    * This method was used to synchronize content in SF and in the cloud but since cloud is in no longer use this method returns only null.
    */
    global PageReference syncContent() {
        return null;
    }

    /**
     * This method saves the assessment , assessment questions(if any)
     */
     global pageReference save() {
        PageReference returnTo = null;
        List<Schema.SObjectField> fieldListOfAssessment = new List<Schema.SObjectField>{
                                                        SQX_Assessment__c.Total_Questions_To_Ask__c,
                                                        SQX_Assessment__c.Randomize__c,
                                                        SQX_Assessment__c.Passing_Percentage__c,
                                                        SQX_Assessment__c.Show_Correct_Answers_On_Completion__c,
                                                        SQX_Assessment__c.Description__c,
                                                        SQX_Assessment__c.Status__c,
                                                        SQX_Assessment__c.Number_Of_Retakes_Allowed__c,
                                                        SQX_Assessment__c.Days_between_Retake__c 
                                                    };
        if(getIsScormType()){
            returnTo = saveScormAssessment(fieldListOfAssessment);
        }
        else{
            returnTo = saveCQAssessment(fieldListOfAssessment);
        }
        return returnTo;
    }

    /**
     * This method clones the assessment
     *  If SCORM type - content is cloned
     *  If CQ type - questions are cloned
     */
    global PageReference cloneAssessment() {
        PageReference returnTo = null;
        
        assessment.Id = null; // clear id
        assessment.Status__c = SQX_Assessment.STATUS_DRAFT; // cloned assessment's status is always draft
        assessment.OwnerId = UserInfo.getUserId(); // change the owner id (issue : SQX-3451)

        Savepoint sp = null;
        try{

            if(getIsScormType()){

                // clone the content
                ContentVersion cv = [SELECT Id, PathOnClient, Title, VersionData FROM ContentVersion WHERE ContentDocumentId =: assessment.SCORM_Content_Reference__c AND IsLatest = true ];

                ContentVersion clonedCv = new ContentVersion();
                clonedCv.VersionData = cv.VersionData;
                clonedCv.Title = cv.Title;
                clonedCv.PathOnClient = cv.PathOnClient;

                sp = Database.setSavepoint();
                
                new SQX_DB().op_insert(new List<ContentVersion> { clonedCv }, new List<Schema.SObjectField> { ContentVersion.PathOnClient, ContentVersion.Title, ContentVersion.VersionData });

                assessment.SCORM_Content_Reference__c = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: clonedCv.Id].ContentDocumentId;
            }
            else{
                
                for(SQX_Assessment_Question__c question : questions) {
                    question.Id = null;
                    question.SQX_Assessment__c = null;
                }
            }

            // save the cloned assessment
            returnTo = save();

        }catch(Exception ex){
            if(sp != null){
                Database.rollback(sp);
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }

        return returnTo;
    }

    
    /**
     * This method is used to add the questions temporarily not persisting in database.
     */
    global void addQuestion() {
        Integer lastNumber = questions.size();
        SQX_Assessment_Question__c q = new SQX_Assessment_Question__c();
        q.SQX_Assessment__c = assessment.id;
        q.Question_Number__c = lastNumber + 1;
        q.Uniqueness_Constraint__c = String.valueOf(lastNumber);
        questions.add(q);
    }
    
    /**
     * This method is used to remove the added questions in the table
     */
    global void removeQuestion() {

        //index value is set in Uniqueness_Constraint__c field of Question in the constructor level and when removing question
        Integer indexVal = Integer.valueof(Apexpages.currentPage().getparameters().get('index'));
        
        //If the question is an existing question then add it to the List to delete from the databse
        if (questions[indexVal].Id != null) {
            removedQuestions.add(questions[indexVal]);
        }
        
        //Remove the question from the table
        questions.remove(indexVal);

        setUniqueQuestionIndex();
    }
    
    /**
     * get detail layout
     */
    public pageReference getDetailPageLayout() {
        return new pagereference(SQX_Utilities.getPageName('_Detail_Layout', '')); //pagename + layout
    }

    /**
    *   Method returns the url to preview uploaded scorm content
    */
    public String getScormPreviewUrl(){

        return SQX_Controller_eContent.getPreviewUrl(assessment.Id);
    }

    /**
     * Method to save the scorm assessment.
     */
    private PageReference saveScormAssessment(List<Schema.SObjectField> fieldListOfAssessment){
        SavePoint sp = Database.setSavepoint();
        SQX_DB sqxDb = new SQX_DB();
        boolean isNewRecord = false;
        try{
            if(assessment.Id == null){
                isNewRecord = true;
                // make sure content has been uploaded
                if(String.isBlank(assessment.SCORM_Content_Reference__c)) { throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_NO_ASSESSMENT_SELECTED); }
                sqxDb.op_insert(new List<SQX_Assessment__c>{ assessment }, fieldListOfAssessment);
                // associate content with assessment
                SQX_Assessment.checkInContent(assessment, [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: assessment.SCORM_Content_Reference__c AND IsLatest = true]);
            }
            else
            {
                sqxDb.op_update(new List<SQX_Assessment__c>{ assessment }, fieldListOfAssessment );
            }
            return new ApexPages.StandardController(assessment).view();
        }catch(Exception e){
            if(isNewRecord) {
                assessment.Id = null;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            Database.rollBack(sp); 
            return null;
        }
    }

    /**
     * Method to save the cq assessment.
     */
    private PageReference saveCQAssessment(List<Schema.SObjectField> fieldListOfAssessment){
        SavePoint sp = Database.setSavepoint();
        SQX_DB sqxDb = new SQX_DB();
        List<SQX_Assessment_Question__c> changedQuestions = new List<SQX_Assessment_Question__c>();
        List<SQX_Assessment_Question__c> addedQuestions = new List<SQX_Assessment_Question__c>();
        boolean isNewRecord = false;
        Decimal tempTotalQnToAsk;
        String tempStatus;
        List<Schema.SObjectField> fieldListOfQuestion = new List<Schema.SObjectField>{
                                                        SQX_Assessment_Question__c.Navigation__c,
                                                        SQX_Assessment_Question__c.Question_Long__c,
                                                        SQX_Assessment_Question__c.Question_Number__c,
                                                        SQX_Assessment_Question__c.Answer_Options_Long__c,
                                                        SQX_Assessment_Question__c.Type_In_Answer__c,
                                                        SQX_Assessment_Question__c.Correct_Answer__c 
                                                    };
        try{
            tempTotalQnToAsk = assessment.Total_Questions_To_Ask__c; //store the value temporarily
            tempStatus = assessment.Status__c;
            // total question to ask is only inserted or updated with zero if the original status of assessment is draft or if the assessment is new. since in other scenario assessment is locked
            // note : the reason of saving total question to ask with value zero is because assessment gets saved first then question is saved, but while saving assessment we don't 
            //        have questions saved already so there is validation total question to aske should be less or equal to total question.
            //        similarly reason of saving status with draft is because in in higher status assessment is locked and hence validation error will stop to insert/update
            if(assessment.Id == null || orignalAssessment.Status__c == SQX_Assessment.STATUS_DRAFT) {
                assessment.Total_Questions_To_Ask__c = 0;
                assessment.Status__c = SQX_Assessment.STATUS_DRAFT;
            }
            
            List<SQX_Assessment__c> assmentList= new List<SQX_Assessment__c>{ assessment };
            if (assessment.Id == null) {
                isNewRecord = true;
                sqxDb.op_insert( assmentList, fieldListOfAssessment );
            } else {
                sqxDb.op_update( assmentList, fieldListOfAssessment );
            }
            for (SQX_Assessment_Question__c q : questions) {
                if (q.id != null) {
                    changedQuestions.add(q);
                } else {
                    q.SQX_Assessment__c = assessment.Id;
                    addedQuestions.add(q);
                }
            }
            
            //delete the questions(if any)
            sqxDb.op_delete(removedQuestions);
            
            //update the changed question
            sqxDb.op_update( changedQuestions, fieldListOfQuestion );
            
            //insert the newly added questions
            sqxDb.op_insert( addedQuestions, fieldListOfQuestion );
            
            //update the assessment with the temporarily stored value of total question to ask
            assessment.Total_Questions_To_Ask__c = tempTotalQnToAsk;
            assessment.Status__c = tempStatus;
            sqxDb.op_update( new List<SQX_Assessment__c>{ assessment },
                            new List<Schema.SObjectField>{ 
                                SQX_Assessment__c.Total_Questions_To_Ask__c });
            return new ApexPages.StandardController(assessment).view();
        }catch(Exception e){
            //reset the total question to ask value with temporarily stored value
            assessment.Total_Questions_To_Ask__c = tempTotalQnToAsk;
            assessment.Status__c = tempStatus;
            //clear the ids of newly created assessments and questions
            if(isNewRecord) {
                assessment.Id = null;
            }
            for(SQX_Assessment_Question__c qn : addedQuestions){
                qn.Id = null;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            Database.rollBack(sp); 
            return null;
        }
    }
}