/**
 * Unit tests for multiple records at My Task in homepage component
 *
 * @author Shailesh Maharjan
 * @date 2015/07/01
 * 
 */
@
IsTest
public class SQX_Test_579_SingleTaskForCapaInitiation {

    static boolean runAllTests = true,
        run_givenCAPAInitiated_SingleTaskCreated = false;

    /**
     *   Setup: Create New CAPA and Initiate
     *   Action: To check the Multi-records in My task
     *   Expected: Single task should be created for initiated CAPA
     *
     * @author Shailesh Maharjan
     * @date 2015/07/01
     * 
     */
    public static testmethod void givenCAPAInitiated_SingleTaskCreated() {

        if (!runAllTests && !run_givenCAPAInitiated_SingleTaskCreated) {
            return;

        }

        /*Creating CAPA*/
        UserRole role = SQX_Test_Account_Factory.createRole();
        User newUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);

        System.runas(newUser) {
            //Arrange: Create supplierAccount, contact and user to which the CAPA will be associated.
            //         The capa will be initiated as soon as it is created
            Account supplierAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_SUPPLIER);
            Contact supplierContact = SQX_Test_Account_Factory.createContact(supplierAccount);
            User supplierUser = SQX_Test_Account_Factory.createUser(supplierContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);

            SQX_CAPA__c capa = new SQX_CAPA__c(
                Needs_Effectiveness_Review__c = false,
                Effectiveness_Monitoring_Days__c = 90,
                Issued_Date__c = Date.Today(),
                Target_Due_Date__c = Date.Today().addDays(15),
                SQX_Account__c = supplierAccount.Id,
                SQX_External_Contact__c = supplierContact.Id,
                Containment_Required__c = true,
                Investigation_Required__c = true,
                Corrective_Action_Required__c = true,
                Preventive_Action_Required__c = true);


            Map < String, Object > submissionSet = new Map < String, Object > ();
            List < Map < String, Object >> changeSet = new List < Map < String, Object >> ();

            Map < String, Object > capaMap = (Map < String, Object > ) JSON.deserializeUntyped(JSON.serialize(capa));

            capaMap.put('Id', 'CAPA-1');


            changeSet.add(capaMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet = JSON.serialize(submissionSet);
            Map < String, String > params = new Map < String, String > ();

            //Act: Initiate the CAPA so that the task relevant to the supplier user is created.
            params.put('nextAction', 'initiate');

            Id result = SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null, params);
            System.assertEquals(SQX.Capa, string.valueOf(result.getsObjectType()),
                'Expected to return Capa Id' + changeSet);


            //Assert: Ensure that only one task has been created for the External Contact(Supplier Contact), related to the opening of CAPA
            List < Task > tasksforCAPA = [Select Id, Subject from Task WHERE WhatId = : result AND OwnerId = : supplierUser.Id AND Subject = : SQX_Task.TASK_SUBJECT_OPEN_CAPA];
            System.assertEquals(1, tasksforCAPA.size(),
                'Expected Single task to be Created ' + tasksforCAPA);
        }
    }
}