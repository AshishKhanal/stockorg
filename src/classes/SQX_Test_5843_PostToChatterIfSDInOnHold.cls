/*
 * Unit test to check if FeedItem is posted on chatter or not under supplier deviation record.
 */
@isTest
public class SQX_Test_5843_PostToChatterIfSDInOnHold {
    final static String WORKFLOW_STATUS_ON_HOLD = 'On Hold';
    final static String chatterMsg = 'Supplier Deviation {0} is OnHold due to the following comment and needs your attention.';
	
    @testSetup
     public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
         
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        SQX_Part__c part;
        SQX_Test_Supplier_Deviation sdRecord;
        System.runAs(adminUser) {
            //Arrange: Create tasks and sqx_part
            SQX_Test_Supplier_Deviation.createPolicyTasks(2, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 1);
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 2);
            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');

            SQX_Custom_Settings_Public__c customSetting;
            customSetting = new SQX_Custom_Settings_Public__c( Community_URL__c = 'https://login.salesforce.com',
                                                                Org_Base_URL__c = SQX_Utilities.getBaseUrlForUser()+'/',
                                                                NC_Analysis_Report__c = 'abc123def');
            insert customSetting;
         }
		
        System.runAs(standardUser) {
            //Act: SD Record initiated and ownership taken
            sdRecord = new SQX_Test_Supplier_Deviation().save();
            sdRecord.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            sdRecord.sd.SQX_Part__c = part.id;
            sdRecord.save();
            sdRecord.initiate();
            
            //Assert: Make sure that SD record is in 'Inprogress' Stage
            System.assertEquals(SQX_Supplier_Deviation.STAGE_IN_PROGRESS, [Select Record_Stage__c from sqx_supplier_deviation__c where id = :sdRecord.sd.id].Record_Stage__c);
            System.assertEquals(2, [SELECT Id, Step__c FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sdRecord.sd.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
        }
         
         System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.sd.Id];
            Task sfTask = sfTasks.get(0);
            
            //Act: any one of the task is updated where status is 'completed' and result is 'No Go' and notification is sent to chatter
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

        }

     }
    
    /*
     * Given: A SD Record
     * When: Any SD is in onHold by setting deviation process step to complete-nogo
     * Then: a chatter notification is added in chatter feeds
     */ 
    @isTest
    public static void  givenSDRecord_WhenSDIsinOnHold_ThenPostMsgOnChatter(){
        //Arrange: Get sdRecord, under which chatter msg has been posted
        SQX_Supplier_Deviation__c sdRecord = [Select id, name from SQX_Supplier_Deviation__c limit 1];
        
        //Act: Get the feed item 
        FeedItem feed = [SELECT parentid, body FROM FeedItem WHERE parentId = :sdRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST limit 1];
            
        //Assert: Make sure msg is posted on chatter
        System.assertEquals(true, feed != null, 'Expected, a feedMessage to be present in chatter');
        System.assertEquals(true, feed.body.contains(String.format(chatterMsg, new String[]{sdRecord.name})));
    }
    /*
     * Given: A SD Record already in onHold
     * When:  Any deviation process step  is completed with result nogo
     * Then:   Chatter notification is added in chatter feeds
     */ 
    @isTest
    public static void  givenSDRecord_WhenSDAlreadyInOnHoldAndDeviationStepIsCompleteNoGo_ThenMsgIsPostedOnChatter(){
        //Get users
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        SQX_Supplier_Deviation__c sdRecord = [Select id, name from SQX_Supplier_Deviation__c limit 1];

        System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.Id];
			
            //Act: Task2 of step1 status is set to complete and result to no-go
            Task sfTask2 = sfTasks.get(1);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask2.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            
            //Assert: Make sure that 2 onboarding policy are  completed and in no-go
            System.assertEquals(2, [Select id from SQX_Deviation_Process__c where status__c = :SQX_Steps_Trigger_Handler.STATUS_COMPLETE and result__c = :SQX_Steps_Trigger_Handler.RESULT_NO_GO].size());			
            System.assertEquals(WORKFLOW_STATUS_ON_HOLD, [SELECT Record_Stage__c FROM sqx_supplier_deviation__c WHERE id = :sdRecord.id].Record_Stage__c);
        }
        //Assert: Make sure 2 message is posted on chatter.
          System.assertEquals(2, [SELECT parentid, body FROM FeedItem WHERE parentId = :sdRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());
    }
    
    /*
     * Given: A Supplier Deviation Record
     * When:  SupplierDeviation is in onHold multiple times for different deviation process step
     * Then:  chatter notification is sent for each different step.
     */
    
     @isTest
    public static void  givenSDRecord_WhenSDInOnHoldAndDeviationStepIsCompleteNoGoForDifferentStep_ThenMultipleMsgIsPostedOnChatter(){
        //Get users
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        sqx_supplier_deviation__c sdRecord = [Select id, name, Record_Stage__c from sqx_supplier_deviation__c];
        
        System.runAs(standardUser){
            SQX_Deviation_Process__c deviationProcess =  [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sdRecord.Id 
                                               			AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                               			AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
            //Act: Redo the task
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation();
            sd.redoDeviationProcess(deviationProcess);

            SQX_BulkifiedBase.clearAllProcessedEntities();
        }

        Test.startTest();
        System.runAs(assigneeUser) {
            //Act: Complete-Go remaining SF tasks
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.Id  AND Status != :SQX_Task.STATUS_COMPLETED];
            Task sfTask1 = sfTasks.get(0);
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask1.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            SQX_BulkifiedBase.clearAllProcessedEntities();

            Task sfTask2 = sfTasks.get(1);
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask2.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
        }
        //Assert: Make sure than only one chatter feed has been posted till now.
        System.assertEquals(1, [SELECT parentid, body FROM FeedItem WHERE parentId = :sdRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());
        	
        System.runAs(assigneeUser) {
            //Arrange: Get step2 tasks
            List<Task> step2Tasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.Id and Status != :SQX_Task.STATUS_COMPLETED];
            System.assertEquals(1, step2Tasks.size(), 'Expected, only one remaining task');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act: Update the SD stage to inprogress
            Task step2Task = step2Tasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(step2Task.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

            //Assert: SD stage should now be in onHold.
            System.assertEquals(WORKFLOW_STATUS_ON_HOLD, [Select Record_Stage__c from sqx_supplier_deviation__c where id = :sdRecord.id].Record_Stage__c);
        }
         //Assert: Make sure 2 chatter feeds have been posted in FeedItem.
          System.assertEquals(2, [SELECT parentid, body FROM FeedItem WHERE parentId = :sdRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());
    }
}