/**
* This unit test class used to check all old record activities of the following objects
* NC,CAPA,Audit,Audit Program,Equipment,Complaint,Electronic Signature and Change Order
* Added in version  : 7.0
* Used for versions : less than 7.0
*/
@isTest
public class SQX_Test_2976_RecordActivities_Migration {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
   /**
   * Given: given all old record activities object data (NC,CAPA,Audit,Audit Program,Equipment,Complaint,Electronic Signature and Change Order)
   * When : when all old record activities to migrate 
   * Then : update activity code
   */
   testmethod static void whenRecordActivities_Migrate_ThenUpdateActivityCode() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // Esig configuration Map
            Map<String, SQX_Extension_UI> extensions = new Map<String, SQX_Extension_UI>{
                SQX.Nonconformance =>new SQX_Extension_NC(new ApexPages.StandardController(new SQX_Nonconformance__c())),
                SQX.CAPA => new SQX_Extension_CAPA(new ApexPages.StandardController(new SQX_CAPA__c())),
                SQX.Audit => new SQX_Extension_Audit(new ApexPages.StandardController(new SQX_Audit__c())),
                SQX.AuditProgram => new SQX_Extension_Audit_Program(new ApexPages.StandardController(new SQX_Audit_Program__c())),
                SQX.Complaint => new SQX_Extension_Complaint(new ApexPages.StandardController(new SQX_Complaint__c())),
                SQX.ChangeOrder => new SQX_Extension_Change_Order(new ApexPages.StandardController(new SQX_Change_Order__c()))
            };
                
            Map<String, String> NCpurposeOfSigMap = new Map<String, String>();
            Map<String, String> CAPApurposeOfSigMap = new Map<String, String>();
            Map<String, String> AuditpurposeOfSigMap = new Map<String, String>();
            Map<String, String> AuditPrgmpurposeOfSigMap = new Map<String, String>();
            Map<String, String> ComplaintpurposeOfSigMap = new Map<String, String>();
            Map<String, String> ChangeOrderpurposeOfSigMap = new Map<String, String>();
            
            for(String objectName : extensions.keySet()){
                SQX_Extension_UI ext = extensions.get(objectName);
                if(objectName.contains('SQX_NonConformance__c')){
                    NCpurposeOfSigMap = ext.getPurposeOfSig();
                }else if(objectName.contains('SQX_CAPA__c')){
                    CAPApurposeOfSigMap = ext.getPurposeOfSig(); 
                }else if(objectName.contains('SQX_Audit__c')){
                    AuditpurposeOfSigMap = ext.getPurposeOfSig();
                }else if(objectName.contains('SQX_Audit_Program__c')){
                    AuditPrgmpurposeOfSigMap = ext.getPurposeOfSig();
                }else if(objectName.contains('SQX_Complaint__c')){
                    ComplaintpurposeOfSigMap = ext.getPurposeOfSig();
                }else if(objectName.contains('SQX_Change_Order__c')){
                    ChangeOrderpurposeOfSigMap = ext.getPurposeOfSig();
                }
            }
            SQX_DB sqxDb = new SQX_DB();
            // Arrange: create a NC record and activities
            SQX_Test_NC nc = new SQX_Test_NC().save();
            List<SQX_NC_Record_Activity__c> ncActivities = new List<SQX_NC_Record_Activity__c>();
            for(String act : NCpurposeOfSigMap.keySet()){
                SQX_NC_Record_Activity__c Ncactivity = new SQX_NC_Record_Activity__c();
                Ncactivity.SQX_Nonconformance__c = nc.nc.Id;
                Ncactivity.Purpose_Of_Signature__c = NCpurposeOfSigMap.get(act);
                ncActivities.add(Ncactivity);
            }
            sqxDb.op_insert(ncActivities,new List<Schema.SObjectField>{
                    Schema.SQX_NC_Record_Activity__c.SQX_Nonconformance__c,
                    Schema.SQX_NC_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a CAPA record and activities
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility().save();
            List<SQX_CAPA_Record_Activity__c> capaActivities = new List<SQX_CAPA_Record_Activity__c>();
            for(String act : CAPApurposeOfSigMap.keySet()){
                SQX_CAPA_Record_Activity__c CAPAactivity = new SQX_CAPA_Record_Activity__c();
                CAPAactivity.SQX_CAPA__c = capa.capa.Id;
                CAPAactivity.Purpose_Of_Signature__c = CAPApurposeOfSigMap.get(act);
                capaActivities.add(CAPAactivity);
            }
            sqxDb.op_insert(capaActivities,new List<Schema.SObjectField>{
                    Schema.SQX_CAPA_Record_Activity__c.SQX_CAPA__c,
                    Schema.SQX_CAPA_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a Audit record and activities
            SQX_Test_Audit audit = new SQX_Test_Audit().save();
            List<SQX_Audit_Record_Activity__c> auditActivities = new List<SQX_Audit_Record_Activity__c>();
            for(String act : AuditpurposeOfSigMap.keySet()){
                SQX_Audit_Record_Activity__c auditActivity = new SQX_Audit_Record_Activity__c();
                auditActivity.SQX_Audit__c = audit.audit.Id;
                auditActivity.Purpose_Of_Signature__c = AuditpurposeOfSigMap.get(act);
                auditActivities.add(auditActivity);
            }
            sqxDb.op_insert(auditActivities, new List<Schema.SObjectField>{
                Schema.SQX_Audit_Record_Activity__c.SQX_Audit__c,
                Schema.SQX_Audit_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a Audit Program record and activities
            SQX_Test_Audit_Program auditProgram = new SQX_Test_Audit_Program().save();
            List<SQX_Audit_Program_Record_Activity__c> auditProgramActivities = new List<SQX_Audit_Program_Record_Activity__c>();
            for(String act : AuditPrgmpurposeOfSigMap.keySet()){
                SQX_Audit_Program_Record_Activity__c auditProgramActivity = new SQX_Audit_Program_Record_Activity__c();
                auditProgramActivity.SQX_Audit_Program__c = auditProgram.program.Id;
                auditProgramActivity.Purpose_Of_Signature__c = AuditPrgmpurposeOfSigMap.get(act);
                auditProgramActivities.add(auditProgramActivity);
            }
            sqxDb.op_insert(auditProgramActivities, new List<Schema.SObjectField>{
                Schema.SQX_Audit_Program_Record_Activity__c.SQX_Audit_Program__c,
                Schema.SQX_Audit_Program_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a equipment record and activities
            SQX_Equipment__c Eqp = new SQX_Equipment__c(
                Name = '1A',
                Equipment_Description__c = 'Test',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            sqxDb.op_insert(new List<SQX_Equipment__c>{Eqp}, new List<Schema.SObjectField>{});

            List<SQX_Equipment_Record_Activity__c> equipmentActivities = new List<SQX_Equipment_Record_Activity__c>();
            for(String act : SQX_Equipment.purposeOfSigMap.keySet()){
                SQX_Equipment_Record_Activity__c equipmentProgramActivity = new SQX_Equipment_Record_Activity__c();
                equipmentProgramActivity.SQX_Equipment__c = Eqp.Id;
                equipmentProgramActivity.Purpose_Of_Signature__c = SQX_Equipment.purposeOfSigMap.get(act);
                equipmentActivities.add(equipmentProgramActivity);
            }
            sqxDb.op_insert(equipmentActivities, new List<Schema.SObjectField>{
                Schema.SQX_Equipment_Record_Activity__c.SQX_Equipment__c,
                Schema.SQX_Equipment_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a complaint record and activities
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            List<SQX_Complaint_Record_Activity__c> complaintActivities = new List<SQX_Complaint_Record_Activity__c>();
            for(String act : ComplaintpurposeOfSigMap.keySet()){
                SQX_Complaint_Record_Activity__c complaintActivity = new SQX_Complaint_Record_Activity__c();
                complaintActivity.SQX_Complaint__c = complaint.complaint.Id;
                complaintActivity.Purpose_Of_Signature__c = ComplaintpurposeOfSigMap.get(act);
                complaintActivities.add(complaintActivity);
             }
            sqxDb.op_insert(complaintActivities, new List<Schema.SObjectField>{
                Schema.SQX_Complaint_Record_Activity__c.SQX_Complaint__c,
                Schema.SQX_Complaint_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a electronic signature activities
            List<SQX_Electronic_Signature_Record_Activity__c> electronicActivities = new List<SQX_Electronic_Signature_Record_Activity__c>();
            for(String act : SQX_Controller_ES_Setting.purposeOfSigMap.keySet()){
                SQX_Electronic_Signature_Record_Activity__c electronicProgramActivity = new SQX_Electronic_Signature_Record_Activity__c();
                electronicProgramActivity.Purpose_Of_Signature__c = SQX_Controller_ES_Setting.purposeOfSigMap.get(act);
                electronicActivities.add(electronicProgramActivity);
            }
            sqxDb.op_insert(electronicActivities, new List<Schema.SObjectField>{
                Schema.SQX_Electronic_Signature_Record_Activity__c.Purpose_Of_Signature__c
            });
            
            // Arrange: create a change order record and activities
            SQX_Test_Change_Order changeorder = new SQX_Test_Change_Order().save();
            List<SQX_Change_Order_Record_Activity__c> changeOrderActivities = new List<SQX_Change_Order_Record_Activity__c>();
            for(String act : ChangeOrderpurposeOfSigMap.keySet()){
                SQX_Change_Order_Record_Activity__c changeOrderActivity = new SQX_Change_Order_Record_Activity__c();
                changeOrderActivity.SQX_Change_Order__c = changeorder.changeorder.Id;
                changeOrderActivity.Purpose_Of_Signature__c = ChangeOrderpurposeOfSigMap.get(act);
                changeOrderActivities.add(changeOrderActivity);
            }
            sqxDb.op_insert(changeOrderActivities, new List<Schema.SObjectField>{
                Schema.SQX_Change_Order_Record_Activity__c.SQX_Change_Order__c,
                Schema.SQX_Change_Order_Record_Activity__c.Purpose_Of_Signature__c
            });
            Test.startTest();
            
            // Act: Start migration
            SQX_PostInstall_Processor processor = new SQX_PostInstall_Processor();
            Test.testInstall(processor, new Version(6,4));
            Test.stopTest();

            // Assert: NC Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_NC_Record_Activity__c> ncList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_NC_Record_Activity__c];
            for(SQX_NC_Record_Activity__c ncRecord: ncList){
                system.assertNotEquals(null, ncRecord.Activity_Code__c);
                //ignore save and draft
                if(ncRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(ncRecord.Purpose_Of_Signature__c, ncRecord.Activity__c);
                }
            }
            
            // Assert: CAPA Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_CAPA_Record_Activity__c> capaList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_CAPA_Record_Activity__c];
            for(SQX_CAPA_Record_Activity__c capaRecord: capaList){
                system.assertNotEquals(null, capaRecord.Activity_Code__c);
                if(capaRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(capaRecord.Purpose_Of_Signature__c, capaRecord.Activity__c);
                }
            }
            
            // Assert: Audit Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Audit_Record_Activity__c> auditList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Audit_Record_Activity__c];
            for(SQX_Audit_Record_Activity__c auditRecord: auditList){
                system.assertNotEquals(null, auditRecord.Activity_Code__c);
                if(auditRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(auditRecord.Purpose_Of_Signature__c, auditRecord.Activity__C);
                }
            }
            
            // Assert: Audit Program Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Audit_Program_Record_Activity__c> auditProgramList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Audit_Program_Record_Activity__c];
            for(SQX_Audit_Program_Record_Activity__c audiitProgramRecord: auditProgramList){
                system.assertNotEquals(null, audiitProgramRecord.Activity_Code__c);
                if(audiitProgramRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(audiitProgramRecord.Purpose_Of_Signature__c, audiitProgramRecord.Activity__c);
                }
            }
            
            // Assert: Eqipment Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Equipment_Record_Activity__c> equuipmentList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Equipment_Record_Activity__c];
            for(SQX_Equipment_Record_Activity__c equuipmentRecord: equuipmentList){
                system.assertNotEquals(null, equuipmentRecord.Activity_Code__c);
                system.assertEquals(equuipmentRecord.Purpose_Of_Signature__c, equuipmentRecord.Activity__c);
            }
            
            // Assert: Complaint Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Complaint_Record_Activity__c> complaintList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Complaint_Record_Activity__c];
            for(SQX_Complaint_Record_Activity__c complaintRecord: complaintList){
                system.assertNotEquals(null, complaintRecord.Activity_Code__c);
                if(complaintRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(complaintRecord.Purpose_Of_Signature__c, complaintRecord.Activity__c);
                }
            }
            
            // Assert: Electronic Signature Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Electronic_Signature_Record_Activity__c> elecSigList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Electronic_Signature_Record_Activity__c];
            for(SQX_Electronic_Signature_Record_Activity__c elecSigRecord: elecSigList){
                system.assertNotEquals(null, elecSigRecord.Activity_Code__c);
                system.assertEquals(elecSigRecord.Purpose_Of_Signature__c, elecSigRecord.Activity__C);
            }
            
            // Assert: Change Order Record Activity Activity_Code__c field is updated and purpose of signature and activity shoule be same
            List<SQX_Change_Order_Record_Activity__c> changeOrderList=[SELECT Purpose_Of_Signature__c, Activity__c, Activity_Code__c FROM SQX_Change_Order_Record_Activity__c];
            for(SQX_Change_Order_Record_Activity__c changeOrderRecord: changeOrderList){
                system.assertNotEquals(null, changeOrderRecord.Activity_Code__c);
                if(changeOrderRecord.Activity__c != 'saveafterdraft') {
                    system.assertEquals(changeOrderRecord.Purpose_Of_Signature__c, changeOrderRecord.Activity__c);
                }
            }
        }
   }
}