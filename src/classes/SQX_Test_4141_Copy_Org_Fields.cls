/**
 * test class for validating if org fields are copied from personnel or not
 */
@isTest
public class SQX_Test_4141_Copy_Org_Fields {
	
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole, 'AdminUser1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'StdUser1');
    }
    
    /**
     * GIVEN : Add personnel record for standardUser with division, bu, site and region and an inspection is created without any inspection details
     * WHEN : An inspection is created without any inspection details
     * THEN : Org fields are copied from personnel to inspection
     * @story : SQX-4141
     * @date : 2017/12/12
     */
    public static testMethod void givenAnUserWithPersonnel_WhenInspectionIsCreatedByUser_OrgFieldsShouldBeCopied(){
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        User adminUser = users.get('AdminUser1');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        SQX_Test_Inspection inspection;
        SQX_Inspection__c inspect;
        
        System.runAs(adminUser){
            // Arrange : Create Part family and Part as Admin User 
            partFamily = SQX_Test_Inspection.newPartFamily('PF');
            part = SQX_Test_Inspection.newPart(partFamily, 'P1');
        }
        
        System.runas(standardUser) {
            
            // Arrange : add personnel record for standardUser with division, bu, site and region
            // An inspection is created without any inspection details
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(standardUser);
            Database.SaveResult sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            p1.mainRecord.Org_Division__c = 'Sample Division';
            p1.mainRecord.Org_Business_Unit__c = 'Sample Business Unit';
            p1.mainRecord.Org_Region__c = 'Sample Region';
            p1.mainRecord.Org_Site__c = 'Sample Site';
            p1.save();
            
            // ACT : Inspection is saved
            inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();     
            
            SQX_Copy_Org_Fields.copyOrgFieldsFromPersonnel(new List<Id>{inspection.inspection.Id});
            
            SQX_Inspection__c ins = [SELECT Id, Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // ASSERT : Org fields are copied from personnel to inspection
            System.assertEquals('Sample Division', ins.Org_Division__c);
            System.assertEquals('Sample Business Unit', ins.Org_Business_Unit__c);
            System.assertEquals('Sample Region', ins.Org_Region__c);
            System.assertEquals('Sample Site', ins.Org_Site__c);
        }
    }
    
    /**
     * GIVEN : An inspection is created without any inspection details
     * WHEN : Save inspection
     * THEN : Org fields of inspection are blank
     * @story : SQX-4141
     * @date : 2017/12/12
     */
    public static testMethod void givenAnUserWithoutPersonnel_WhenInspectionIsCreatedByUser_OrgFieldsShouldBeBlank(){
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        User adminUser = users.get('AdminUser1');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        SQX_Test_Inspection inspection;
        SQX_Inspection__c inspect;
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily('PF');
            part = SQX_Test_Inspection.newPart(partFamily, 'P1');
        }
        
        System.runas(standardUser) {
            
            // Arrange : An inspection is created without any inspection details
            inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            
            // ACT : Save inspection
            inspection.save();     
            
            SQX_Copy_Org_Fields.copyOrgFieldsFromPersonnel(new List<Id>{inspection.inspection.ID});
            
            SQX_Inspection__c ins = [SELECT Id, Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // ASSERT : Org fields of inspection are blank
            System.assertEquals(null, ins.Org_Division__c);
            System.assertEquals(null, ins.Org_Business_Unit__c);
            System.assertEquals(null, ins.Org_Region__c);
            System.assertEquals(null, ins.Org_Site__c);
        }
    }
    
    /**
     * GIVEN : Add personnel record for standardUser with division, bu, site and region and an inspection is created with division
     * WHEN : An inspection is saved with division
     * THEN : Org fields are not copied from personnel to inspection
     * @story : SQX-4141
     * @date : 2017/12/12
     */
    public static testMethod void givenAnUserWithPersonnel_WhenInspectionIsCreatedByUserWithOrgFieldsPresent_OrgFieldsShouldNotBeCopied(){
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('StdUser1');
        User adminUser = users.get('AdminUser1');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        SQX_Test_Inspection inspection;
        SQX_Inspection__c inspect;
        
        System.runAs(adminUser){
            // Arrange : Create Part family and Part as Admin User 
            partFamily = SQX_Test_Inspection.newPartFamily('PF');
            part = SQX_Test_Inspection.newPart(partFamily, 'P1');
        }
        
        System.runas(standardUser) {
            
            // Arrange : add personnel record for standardUser with division, bu, site and region
            // An inspection is created with division 
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(standardUser);
            Database.SaveResult sr = p1.save();
            System.assert(sr.isSuccess(), 'Required personnel record is expected to be saved.');
            
            p1.mainRecord.Org_Division__c = 'Sample Division';
            p1.mainRecord.Org_Business_Unit__c = 'Sample Business Unit';
            p1.mainRecord.Org_Region__c = 'Sample Region';
            p1.mainRecord.Org_Site__c = 'Sample Site';
            p1.save();
            
            // ACT : Inspection is saved
            inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.inspection.Org_Division__c = 'Sample Division';
            inspection.save();     
            
            SQX_Copy_Org_Fields.copyOrgFieldsFromPersonnel(new List<Id>{inspection.inspection.Id});
            
            SQX_Inspection__c ins = [SELECT Id, Org_Division__c, Org_Business_Unit__c, Org_Region__c, Org_Site__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            
            // ASSERT : Org fields are not copied from personnel to inspection
            System.assertEquals('Sample Division', ins.Org_Division__c);
            System.assertEquals(null, ins.Org_Business_Unit__c);
            System.assertEquals(null, ins.Org_Region__c);
            System.assertEquals(null, ins.Org_Site__c);
        }
    }
}