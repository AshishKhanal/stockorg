/**
* Unit tests for SQX_RootCauseCodeShouldbeUnique
*
* @author Shailesh Maharjan
* @date 2015/06/03
* 
*/
@IsTest
public class SQX_Test_1116_RootCauseShouldbeUnique{

    static boolean runAllTests = true,
                    run_rootCauseAdded_DuplicateNotAllowed = true;


    /**
    *   Setup: Create A Root Cause Code
    *   Action: To check the Root Cause Name is Unique
    *   Expected: Second insert should be failed with its error msg
    *
    * @author Shailesh Maharjan
    * @date 2015/06/03
    * 
    */

     public static testmethod void rootCauseAdded_DuplicateNotAllowed(){
        

        if(!runAllTests && !run_rootCauseAdded_DuplicateNotAllowed){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
     
        System.runas(adminUser){
             SQX_Root_Cause_Code__c rootCauseCode1 = new SQX_Root_Cause_Code__c(Name='RootCausetest',Description__c='RootCauseCat');
             Database.SaveResult result1 = Database.Insert(rootCauseCode1 ,false);

             System.assert(result1.isSuccess() == true,
                 'Expected Root Cause code save to be success ');
                 
             SQX_Root_Cause_Code__c rootCauseCode2 = new SQX_Root_Cause_Code__c(Name='RootCausetest',Description__c='RootCauseCat');
             Database.SaveResult result2 = Database.Insert(rootCauseCode2 ,false);

             System.assert(result2.isSuccess() == false,
                 'Expected Root Cause code save to be Unsuccess ');   
        } 
     }

}