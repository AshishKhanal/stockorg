/**
* ensures owner of the primary and secondary content documents are synchronized when the controlled document owner is changed
*/
@isTest
public class SQX_Test_2238_ChangeDocumentOwner {
    @testSetup
    public static void commonSetup() {
        // add required users
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');
    }
    
    /**
    * ensures owner of the primary and secondary content documents are synchronized when the controlled document owner is changed
    */
    public static testmethod void givenChangeControlledDocumentOwner_RelatedContentDocumentsUpdated() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1'),
            user2 = users.get('user2');
        System.assert(user1 != null);
        System.assert(user2 != null);
        
        SQX_Test_Controlled_Document cDoc1;
        
        System.runAs(user1) {
            // add required controlled document
            cDoc1 = new SQX_Test_Controlled_Document()
                .updateContent(true, 'Primary.txt')
                .updateContent(false, 'Secondary.txt')
                .save();
            
            // ACT: change controlled document owner
            cDoc1.doc.OwnerId = user2.Id;
            cDoc1.save();
            
            // ASSERTS: ensure primary and secondary content documents are assigned to user2
            List<ContentDocument> primaryContent = [SELECT Id FROM ContentDocument WHERE Id = :cDoc1.doc.Content_Reference__c AND OwnerId = :user2.Id];
            
            System.assert(primaryContent.size() == 1, 'Owner of the primary content document is expected to be the owner of controlled document.');
            
            List<ContentDocument> secondaryContent = [SELECT Id FROM ContentDocument WHERE Id = :cDoc1.doc.Secondary_Content_Reference__c AND OwnerId = :user2.Id];
            
            System.assert(secondaryContent.size() == 1, 'Owner of the secondary content document is expected to be the owner of controlled document.');
        }
    }
}