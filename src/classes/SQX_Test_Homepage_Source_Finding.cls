/**
*   This class contains test cases for ensuring that Finding source is providing homepage items with desired values
*   @author - Sanjay Maharjan
*   @date - 16-04-2018
*   Finding source currently returns items of type :
*    1. Open/Draft/Completed
*/
@isTest
public class SQX_Test_Homepage_Source_Finding {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     *   Given : Finding With Draft Status
     *   When :  Finding Records are fetched and processed
     *   Then :  Draft Finding item should be added to Homepage Component
     *   @story - SQX-4569
     */
    testmethod static void givenFindingWithDraftStatus_WhenFindingRecordsAreFetchedAndProcessed_ThenDraftFindingItemShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange :  Create a Draft Finding 
            SQX_Test_Finding itemDraft = new SQX_Test_Finding();
            itemDraft.setStatus(SQX_Finding.STATUS_DRAFT);
            itemDraft.save();

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Finding_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Draft Finding has been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size()>= 1, 'Expected at-least one item');

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_FINDING, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                SQX_Homepage_Item_Action act = hpItem.actions.get(0);

                System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

                System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

                System.assertEquals(false, act.supportsBulk);
                
            }

        }

    }

    /**
     *   Given : Finding With Open Status
     *   When :  Finding Records are fetched and processed
     *   Then :  Open Finding item should be added to Homepage Component
     *   @story - SQX-4569
     */
    testmethod static void givenFindingWithOpenStatus_WhenFindingRecordsAreFetchedAndProcessed_ThenOpenFindingItemShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange :  Create a Open Finding 
            SQX_Test_Finding itemOpen = new SQX_Test_Finding();
            itemOpen.setStatus(SQX_Finding.STATUS_OPEN);
            itemOpen.save();

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Finding_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Open Finding has been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size()>= 1, 'Expected at-least one item');

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_FINDING, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS, hpItem.actionType);

                SQX_Homepage_Item_Action act = hpItem.actions.get(0);

                System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

                System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

                System.assertEquals(false, act.supportsBulk);
                
            }

        }

    }

    /**
     *   Given : Finding With Open Status and Waiting For CAPA Stage
     *   When :  Finding Records are fetched and processed
     *   Then :  Open Finding item should not be added to Homepage Component
     *   @story - SQX-6208
     */
    testmethod static void givenFindingWithOpenStatusAndWaitingForCapaStage_WhenFindingRecordsAreFetchedAndProcessed_ThenOpenFindingItemShouldNotBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange :  Create a Open Finding with Waiting for Capa stage
            SQX_Test_Finding itemOpen = new SQX_Test_Finding();
            itemOpen.setStatus(SQX_Finding.STATUS_OPEN);
            itemOpen.setStage(SQX_Finding.STAGE_WAITING_FOR_CAPA);
            itemOpen.save();

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Finding_Items()
            };

            // Act: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            System.assert(hpItems.size()==0, 'Expected no items.');

        }

    }

    /**
     *   Given : Finding With Complete Status
     *   When :  Finding Records are fetched and processed
     *   Then :  Complete Finding item should be added to Homepage Component
     *   @story - SQX-4569
     */
    testmethod static void givenFindingWithCompleteStatus_WhenFindingRecordsAreFetchedAndProcessed_ThenCompleteFindingItemShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange :  Create a Complete Finding 
            SQX_Test_Finding itemCompleted = new SQX_Test_Finding();
            itemCompleted.setStatus(SQX_Finding.STATUS_COMPLETE);
            itemCompleted.save();

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Finding_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Complete Finding has been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size()>= 1, 'Expected at-least one item');

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_FINDING, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE, hpItem.actionType);

                SQX_Homepage_Item_Action act = hpItem.actions.get(0);

                System.assertEquals(SQX_Homepage_Constants.ACTION_CLOSE, act.name);

                System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_CLOSE, act.actionIcon);

                System.assertEquals(false, act.supportsBulk);
                
            }

        }

    }

   /**
    *   Given : A set of finding investigation Record 
    *   When : Homepage items are fetched
    *   Then : Desired Investigation items are returned
    */
    @isTest
    public static void givenFindingRecords_WhenHomepageItemsAreFetched_ThenDesiredFindingItemsAreReturned(){
        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            SQX_Test_Finding itmOpen = new SQX_Test_Finding();
            itmOpen.setStatus(SQX_Finding.STATUS_OPEN);
            itmOpen.finding.Investigation_Required__c=true;
            itmOpen.finding.Due_Date_Investigation__c= date.parse('6/13/2018');
            itmOpen.finding.SQX_Investigation_Approver__c=UserInfo.getUserId();
            itmOpen.save();
            
            List<SQX_Finding__c> finding=[select id,SQX_Investigation_Approver__c from SQX_Finding__c];
            /*added a investigation  for created capa */
            SQX_Investigation__c investigation= new SQX_Investigation__c
                                                (
                                                    SQX_Finding__c= finding[0].Id,
                                                    Investigation_Summary__c='finding investigation',
                                                    Status__c=SQX_Investigation.STATUS_IN_APPROVAL,
                                                    Approval_Status__c=SQX_Investigation.APPROVAL_STATUS_PENDING
                                                );

            insert investigation; 

            // Arrange: add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { new SQX_Homepage_Source_Finding_Items() };

            // Act: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());
            // retrieve finding records
            SQX_Investigation__c investigationItem = [SELECT Id,Name,SQX_Finding__r.Title__c,SQX_Finding__c,Approval_Status__c,SQX_Finding__r.Name,
                                                            Status__c,CreatedDate from SQX_Investigation__c where SQX_Investigation__c.id=:investigation.id ][0];

            // Assert: Items retrieved are as expected
            System.assertNotEquals(null, hpItems);

            System.assert(hpItems.size() == 2, 'Expected exactly 2 items but received ' + hpItems.size() + ' items');
            for(Object hpObj : hpItems){
                    SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);
                    if(hpItem.actionType == SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE){
                                validateFindingInvestigationItem(investigationItem,hpItem);
                    }
            }
        }
    }

    /**
    *   Method to verify if the given homepage item is a valid finding investigation item 
    */
    private static void validateFindingInvestigationItem(SQX_Investigation__c investigation, SQX_Homepage_Item hpItem){
        
        System.assertEquals(0, hpItem.overdueDays);
        
        System.assertEquals(1, hpItem.actions.size());
        
        SQX_Homepage_Item_Action act = hpItem.actions.get(0);
        
        System.assertEquals(SQX_Homepage_Constants.ACTION_APPROVE_REJECT, act.name);

        PageReference pr = new PageReference(SQX_Utilities.getPageName('', 'SQX_Finding'));
        pr.getParameters().put('Id',investigation.SQX_Finding__c);
        pr.getParameters().put('initialTab','responseHistoryTab');

        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);
                
        System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT, act.actionIcon);
        
        System.assertEquals(false, act.supportsBulk);

        // asserting feed text
        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE, new String[] { investigation.Name,String.isBlank(investigation.SQX_Finding__r.Title__c) ? '' :  investigation.SQX_Finding__r.Title__c });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }
}