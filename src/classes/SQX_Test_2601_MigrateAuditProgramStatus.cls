@IsTest
public class SQX_Test_2601_MigrateAuditProgramStatus {
    /**
    * Given: Audit Program with different status
    * When:  SQX_6_3_MigrateAuditProgramStatus runs
    * Then:  Status are changed into Status and Approval Status
    * 
    * @author: Sajal Joshi
    * @date: 09/22/2016
    * @story: [SQX-2601]
    */ 
    public static testmethod void whenSQX_6_3_MigrateAuditProgramStatusClassRuns_ThenStatusChangesToStatusNApprovalStatus(){
        
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);
        
        System.runAs(user){
            //Arrange: Create differnt Audit Program with different status
            List<SQX_Audit_Program__c> auditPrograms = new List<SQX_Audit_Program__c>();
            List<String> programStatus = new List<String>();
            Map<String, String[]> oldStatus2NewMap = SQX_6_3_MigrateAuditProgramStatus.OLD_STATUS_TO_NEW_STATUS_MAP;
            for(String status: oldStatus2NewMap.keySet()){
                SQX_Test_Audit_Program program1 = new SQX_Test_Audit_Program();
                program1.setStatus(status).save();
                programStatus.add(status);
                auditPrograms.add(program1.program);
            }
            //Act: Run SQX_6_3_MigrateAuditProgramStatus batch class
            Test.startTest();
            SQX_6_3_MigrateAuditProgramStatus job = new SQX_6_3_MigrateAuditProgramStatus();
            ID batchprocessid = Database.executeBatch(job);
            Test.stopTest();
                
            //Assert: Audit Program Status is changed into Status and Approval Status 
            Integer count = 0;
            List<SQX_Audit_Program__c> newAuditPrograms = [SELECT Status__c, Approval_Status__c FROM SQX_Audit_Program__c WHERE id IN : auditPrograms];
            for(SQX_Audit_Program__c auditProgram: newAuditPrograms){
                System.assertEquals(oldStatus2NewMap.get(programStatus[count])[0], auditProgram.Status__c);
                System.assertEquals(oldStatus2NewMap.get(programStatus[count])[1], auditProgram.Approval_Status__c);
                count++;
            }              
        }
    }
}