/**
*   @author : Piyush Subedi
*   @date : 2016/12/1
*   @description : Test class to test GroupTrigger
*/
@isTest
public class SQX_Test_Group_Trigger{
    
    static Boolean runAllTests = true,
                    run_givenACollaborationGroupAssociatedWithAControlledDocument_WhenUserTriesToDeleteTheGroup_ThenErrorMessageIsThrown = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users       
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(standardUser){
            // creating a group
            CollaborationGroup grp = new CollaborationGroup();
            grp.Name = 'Test Collab Group';
            grp.OwnerId = standardUser.Id;
            grp.CollaborationType = SQX_SF_Collaboration_Provider.COLLABORATION_TYPE_PRIVATE;
            insert grp;

            // adding group id to a controlled record
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document().save();
            tstObj.doc.Collaboration_Group_Id__c = grp.Id;
            tstObj.save();            
        }
    }
    
    /**
    *   Given a collaboration group associated with a controlled document record
    *   When user tries to delete the group
    *   Then error message stating the group cannot be deleted, should be thrown
    */
    static testMethod void givenACollaborationGroupAssociatedWithAControlledDocument_WhenUserTriesToDeleteTheGroup_ThenErrorMessageIsThrown(){
        
        if(!runAllTests && !run_givenACollaborationGroupAssociatedWithAControlledDocument_WhenUserTriesToDeleteTheGroup_ThenErrorMessageIsThrown)
            return;
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            /** Arrange */

            CollaborationGroup grp = [SELECT Id FROM CollaborationGroup LIMIT 1];
          
            /** Act */
            // deleting the group
            try{
                delete grp;
            }catch(Exception ex){
                /** Assert */
                System.assert(ex.getMessage().contains(Label.CQ_UI_Cannot_Delete_Controlled_Doc_Collab_Group), 
                    'Expected error message : ' + Label.CQ_UI_Cannot_Delete_Controlled_Doc_Collab_Group + ' Actual Error Message : ' + ex.getMessage());
            }      
        }

    }
}