/**
* Tests for SQX_PostInstall_Processor class
*/
@IsTest
public class SQX_Test_PostInstall_Processor {
    static boolean runAllTests = true,
                run_givenVerifyDocumentTrainingMigration= false;
    /*
    * ensures post install migration of User, User Job Function, and Document Training
    * using old package version as 3.0
    */
    public testmethod static void givenVerifyDocumentTrainingMigration() {
        if(!runAllTests && !run_givenVerifyDocumentTrainingMigration){
            return;
        }
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User sysAdmin = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole);
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(sysAdmin) {
            // set manager for user1
            user1.ManagerId = sysAdmin.Id;
            Database.update(user1, false);
            
            // add required job functions
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(2, true);
            insert jfs;

            SQX_Job_Function__c jf1 = jfs[0];
            SQX_Job_Function__c jf2 = jfs[1];
            
            // add required controlled documents
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document doc2 = new SQX_Test_Controlled_Document().save();
            doc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document doc3 = new SQX_Test_Controlled_Document().save();
            doc3.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required document trainings
            SQX_Document_Training__c dt1 = new SQX_Document_Training__c();
            dt1.SQX_User__c = user1.Id;
            dt1.SQX_Controlled_Document__c = doc1.doc.Id;
            dt1.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY;
            dt1.Due_Date__c = System.today();
            dt1.Optional__c = true;
            dt1.SQX_Trainer__c = sysAdmin.Id;
            dt1.Status__c = SQX_Document_Training.STATUS_COMPLETE;
            dt1.SQX_User_Signed_Off_By__c = user2.Id;
            dt1.User_Signature__c = user2.Username;
            dt1.User_SignOff_Date__c = DateTime.now();
            dt1.User_Signoff_Comment__c = 'user signoff for dt1';
            dt1.SQX_Training_Approved_By__c = user2.Id;
            dt1.Trainer_Signature__c = user2.Username;
            dt1.Trainer_SignOff_Date__c = DateTime.now();
            dt1.Trainer_SignOff_Comment__c = 'trainer signoff for dt1';
            dt1.Uniqueness_Constraint__c = 'dt1' + user2.Id + doc1.doc.Id;
            
            SQX_Document_Training__c dt2 = new SQX_Document_Training__c();
            dt2.SQX_User__c = user1.Id;
            dt2.SQX_Controlled_Document__c = doc1.doc.Id;
            dt2.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            dt2.Due_Date__c = System.today();
            dt2.Optional__c = false;
            dt2.SQX_Trainer__c = sysAdmin.Id;
            dt2.Status__c = SQX_Document_Training.STATUS_TRAINER_APPROVAL_PENDING;
            dt2.SQX_User_Signed_Off_By__c = user2.Id;
            dt2.User_Signature__c = user2.Username;
            dt2.User_SignOff_Date__c = DateTime.now();
            dt2.User_Signoff_Comment__c = 'user signoff for dt2';
            dt2.Uniqueness_Constraint__c = 'dt2' + user2.Id + doc1.doc.Id;
            
            SQX_Document_Training__c dt3 = new SQX_Document_Training__c();
            dt3.SQX_User__c = user2.Id;
            dt3.SQX_Controlled_Document__c = doc2.doc.Id;
            dt3.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            dt3.Due_Date__c = System.today();
            dt3.Optional__c = true;
            dt3.Status__c = SQX_Document_Training.STATUS_PENDING;
            
            SQX_Document_Training__c dt4 = new SQX_Document_Training__c();
            dt4.SQX_User__c = user2.Id;
            dt4.SQX_Controlled_Document__c = doc3.doc.Id;
            dt4.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND;
            dt4.Due_Date__c = System.today();
            dt4.Optional__c = true;
            dt4.Status__c = SQX_Document_Training.STATUS_COMPLETE;
            dt4.SQX_User_Signed_Off_By__c = user2.Id;
            dt4.User_Signature__c = user2.Username;
            dt4.User_SignOff_Date__c = DateTime.now();
            dt4.User_Signoff_Comment__c = 'user signoff for dt4';
            dt4.Uniqueness_Constraint__c = 'dt4' + user2.Id + doc3.doc.Id;
            
            SQX_Document_Training__c dt5 = new SQX_Document_Training__c();
            dt5.SQX_User__c = user2.Id;
            dt5.SQX_Controlled_Document__c = doc3.doc.Id;
            dt5.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY;
            dt5.Due_Date__c = System.today();
            dt5.Optional__c = false;
            dt5.SQX_Trainer__c = sysAdmin.Id;
            dt5.Status__c = SQX_Document_Training.STATUS_TRAINER_APPROVAL_PENDING;
            dt5.SQX_User_Signed_Off_By__c = user2.Id;
            dt5.User_Signature__c = user2.Username;
            dt5.User_SignOff_Date__c = DateTime.now();
            dt5.User_Signoff_Comment__c = 'user signoff for dt5';
            dt5.Uniqueness_Constraint__c = 'dt5' + user2.Id + doc3.doc.Id;
            
            SQX_Document_Training__c dt6 = new SQX_Document_Training__c();
            dt6.SQX_User__c = user2.Id;
            dt6.SQX_Controlled_Document__c = doc3.doc.Id;
            dt6.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            dt6.Due_Date__c = System.today();
            dt6.Optional__c = true;
            dt6.Status__c = SQX_Document_Training.STATUS_PENDING;
            
            Database.insert(new List<SQX_Document_Training__c>{ dt1, dt2, dt3, dt4, dt6, dt5 }, false);
            
            // make doc3 obsolete
            doc3.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // add required requirements
            SQX_Requirement__c req1 = new SQX_Requirement__c();
            req1.SQX_Controlled_Document__c = doc1.doc.Id;
            req1.SQX_Job_Function__c = jf1.Id;
            req1.Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT;
            req1.Optional__c = true;
            req1.Active__c = true;
            Database.insert(req1, false);
            
            // add required user job functions
            SQX_User_Job_Function__c ujf1 = new SQX_User_Job_Function__c();
            ujf1.SQX_User__c = user1.Id;
            ujf1.SQX_Job_Function__c = jf1.Id;
            ujf1.Active__c = false;
            ujf1.Activation_Date__c = System.today();
            ujf1.Deactivation_Date__c = System.today();
            ujf1.Uniqueness_Constraint__c = 'Inactive1' + user1.Id + jf1.Id;
            
            SQX_User_Job_Function__c ujf2 = new SQX_User_Job_Function__c();
            ujf2.SQX_User__c = user1.Id;
            ujf2.SQX_Job_Function__c = jf1.Id;
            ujf2.Active__c = true;
            
            SQX_User_Job_Function__c ujf3 = new SQX_User_Job_Function__c();
            ujf3.SQX_User__c = user2.Id;
            ujf3.SQX_Job_Function__c = jf2.Id;
            ujf3.Active__c = false;
            
            Database.insert(new List<SQX_User_Job_Function__c>{ ujf1, ujf2, ujf3}, false);
            
            
            // process migration for limited users
            // note: it is recommended to limit users for testing
            boolean includeSysAdmin = true;
            boolean includeUser1 = true;
            boolean includeUser2 = true;
            
            Set<Id> processUserIds = new Set<Id>();
            if (includeUser1)
                processUserIds.add(user1.Id);
            if (includeUser2)
                processUserIds.add(user2.Id);
            if (includeSysAdmin)
                processUserIds.add(sysAdmin.Id);
            
            if (processUserIds == null || processUserIds.size() == 0) {
                // if no limit is set, migration processor process all users
                includeUser1 = true;
                includeUser2 = true;
                includeSysAdmin = true;
            }
            
            System.debug(String.format('SQX_PostInstall_Processor starting limit before Test.startTest(), cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                        new String[]{ 
                                            '' + Limits.getCpuTime(), 
                                            '' + Limits.getQueries(),
                                            '' + Limits.getLimitQueries(),
                                            '' + Limits.getDMLStatements(),
                                            '' + Limits.getLimitDMLStatements()
                                        })
            );
            
            Test.startTest();
            
            System.debug('Starting post install migration.');
            
            
            // limit document training migration to test users only
            SQX_DocumentTrainingMigration_Processor.LimitMigrationToUserIds = processUserIds;

            // ACT: start post install migration
            SQX_PostInstall_Processor processor = new SQX_PostInstall_Processor();
            Test.testInstall(processor, new Version(3,0));
            
            System.debug('Post install migration finished.');
            
            Test.stopTest();
            
            System.debug(String.format('SQX_PostInstall_Processor ending limit after Test.stopTest(), cpu time: {0}, soql: {1}/{2}, dml: {3}/{4}', 
                                        new String[]{ 
                                            '' + Limits.getCpuTime(), 
                                            '' + Limits.getQueries(),
                                            '' + Limits.getLimitQueries(),
                                            '' + Limits.getDMLStatements(),
                                            '' + Limits.getLimitDMLStatements()
                                        })
            );
            
            
            // get migrated personnel records
            SQX_Personnel__c p1;
            SQX_Personnel__c p2;
            SQX_Personnel__c p3;
            List<SQX_Personnel__c> psns = [SELECT Id, SQX_User__c, Full_Name__c, Identification_Number__c, Email_Address__c, Active__c, Unique_User_Constraint__c
                                           FROM SQX_Personnel__c WHERE SQX_User__c IN :processUserIds];
            
            integer expectedListSize = 0;
            if (includeUser1)
                expectedListSize++;
            if (includeUser2)
                expectedListSize++;
            if (includeSysAdmin)
                expectedListSize++;
            System.assertEquals(expectedListSize, psns.size(), 'Expected ' + expectedListSize + ' personnel records to be created.');
            
            for (SQX_Personnel__c p : psns) {
                if (p.SQX_User__c == user1.Id)
                    p1 = p;
                if (p.SQX_User__c == user2.Id)
                    p2 = p;
                if (p.SQX_User__c == sysAdmin.Id)
                    p3 = p;
            }
            
            Set<Id> migratedPersonnelIds = new Set<Id>();
            if (includeUser1) {
                System.assert(p1 != null, 'Expected user to be migrated to personnel.');
                verifyMigratedUser(user1, p1);
                migratedPersonnelIds.add(p1.Id);
            }
            if (includeUser2) {
                System.assert(p2 != null, 'Expected user to be migrated to personnel.');
                verifyMigratedUser(user2, p2);
                migratedPersonnelIds.add(p2.Id);
            }
            if (includeSysAdmin) {
                System.assert(p3 != null, 'Expected user to be migrated to personnel.');
                verifyMigratedUser(sysAdmin, p3);
                migratedPersonnelIds.add(p3.Id);
            }
            
            // get updated user job function records
            Set<Id> listIds = new Set<Id>{ ujf1.Id, ujf2.Id, ujf3.Id };
            List<SQX_User_Job_Function__c> ujfs = [SELECT Id, Name, SQX_User__c, SQX_Job_Function__c, Active__c, Activation_Date__c, Deactivation_Date__c,
                                                          Uniqueness_Constraint__c, SQX_Personnel_Job_Function__c
                                                   FROM SQX_User_Job_Function__c WHERE Id IN :listIds];
            for (SQX_User_Job_Function__c ujf : ujfs) {
                if (ujf1.Id == ujf.Id)
                    ujf1 = ujf;
                if (ujf2.Id == ujf.Id)
                    ujf2 = ujf;
                if (ujf3.Id == ujf.Id)
                    ujf3 = ujf;
            }
            
            if (includeUser1) {
                System.assert(ujf1.SQX_Personnel_Job_Function__c != null, 'Expected personnel job function to be linked.');
                System.assert(ujf2.SQX_Personnel_Job_Function__c != null, 'Expected personnel job function to be linked.');
            }
            if (includeUser2) {
                System.assert(ujf3.SQX_Personnel_Job_Function__c != null, 'Expected personnel job function to be linked.');
            }
            
            // get migrated personnel job function records
            List<SQX_Personnel_Job_Function__c> pjfs = [SELECT Id, Name, SQX_Personnel__c, SQX_Job_Function__c, Active__c, Activation_Date__c, Deactivation_Date__c,
                                                               Unique_New_Or_Active_Constraint__c
                                                        FROM SQX_Personnel_Job_Function__c WHERE SQX_Personnel__c IN :migratedPersonnelIds];
            
            expectedListSize = 0;
            if (includeUser1)
                expectedListSize += 2;
            if (includeUser2)
                expectedListSize += 1;
            System.assertEquals(expectedListSize, pjfs.size(), 'Expected ' + expectedListSize + ' personnel job function records to be created.');
            
            SQX_Personnel_Job_Function__c pjf1;
            SQX_Personnel_Job_Function__c pjf2;
            SQX_Personnel_Job_Function__c pjf3;
            for (SQX_Personnel_Job_Function__c pjf : pjfs) {
                if (ujf1.SQX_Personnel_Job_Function__c == pjf.Id)
                    pjf1 = pjf;
                if (ujf2.SQX_Personnel_Job_Function__c == pjf.Id)
                    pjf2 = pjf;
                if (ujf3.SQX_Personnel_Job_Function__c == pjf.Id)
                    pjf3 = pjf;
            }
            
            if (includeUser1) {
                System.assert(pjf1 != null, 'Expected user job function to be migrated to personnel job function.');
                verifyMigratedUserJobFunction(p1, ujf1, pjf1);
                System.assert(pjf2 != null, 'Expected user job function to be migrated to personnel job function.');
                verifyMigratedUserJobFunction(p1, ujf2, pjf2);
            }
            if (includeUser2) {
                System.assert(pjf3 != null, 'Expected user job function to be migrated to personnel job function.');
                verifyMigratedUserJobFunction(p2, ujf3, pjf3);
            }
            
            // get updated document training records
            listIds = new Set<Id>{ dt1.Id, dt2.Id, dt3.Id, dt4.Id, dt5.Id, dt6.Id };
            List<SQX_Document_Training__c> dts = [SELECT Id, Name, Completion_Date_Internal__c, Due_Date__c, Level_of_Competency__c, Optional__c,
                                                         SQX_Controlled_Document__c, SQX_Trainer__c, SQX_Training_Approved_By__c, SQX_User_Signed_Off_By__c,
                                                         SQX_User__c, Status__c, Title__c, Trainer_SignOff_Comment__c, Trainer_SignOff_Date__c, Trainer_Signature__c,
                                                         Uniqueness_Constraint__c, User_SignOff_Date__c, User_Signature__c, User_Signoff_Comment__c,
                                                         SQX_Controlled_Document__r.Document_Number__c, SQX_Controlled_Document__r.Revision__c, SQX_Personnel_Document_Training__c
                                                  FROM SQX_Document_Training__c WHERE Id IN :listIds];
            for (SQX_Document_Training__c dt : dts) {
                if (dt1.Id == dt.Id)
                    dt1 = dt;
                if (dt2.Id == dt.Id)
                    dt2 = dt;
                if (dt3.Id == dt.Id)
                    dt3 = dt;
                if (dt4.Id == dt.Id)
                    dt4 = dt;
                if (dt5.Id == dt.Id)
                    dt5 = dt;
                if (dt6.Id == dt.Id)
                    dt6 = dt;
            }
            
            if (includeUser1) {
                System.assert(dt1.SQX_Personnel_Document_Training__c != null, 'Expected personnel document training to be linked.');
                System.assert(dt2.SQX_Personnel_Document_Training__c != null, 'Expected personnel document training to be linked.');
            }
            if (includeUser2) {
                System.assert(dt3.SQX_Personnel_Document_Training__c != null, 'Expected personnel document training to be linked.');
                System.assert(dt4.SQX_Personnel_Document_Training__c != null, 'Expected personnel document training to be linked.');
                System.assert(dt5.SQX_Personnel_Document_Training__c != null, 'Expected personnel document training to be linked.');
                System.assert(dt6.SQX_Personnel_Document_Training__c == null,
                    'Expected pending document training of obsolete controlled document not to be migrated to personnel document training.');
            }
            
            // get migrated personnel document training records
            List<SQX_Personnel_Document_Training__c> pdts = [SELECT Id, Completion_Date__c, Document_Number__c, Document_Revision__c, Document_Title__c, Due_Date__c,
                                                                    Level_of_Competency__c, Optional__c, Personnel_Email__c, SQX_Controlled_Document__c, SQX_Personnel__c,
                                                                    SQX_Trainer__c, SQX_Training_Approved_By__c, SQX_User_Signed_Off_By__c, Status__c,
                                                                    Trainer_SignOff_Comment__c, Trainer_SignOff_Date__c, Trainer_Signature__c,
                                                                    User_SignOff_Date__c, User_Signature__c, User_Signoff_Comment__c, Uniqueness_Constraint__c
                                                             FROM SQX_Personnel_Document_Training__c WHERE SQX_Personnel__c IN :migratedPersonnelIds];
            
            expectedListSize = 0;
            if (includeUser1)
                expectedListSize += 2;
            if (includeUser2)
                expectedListSize += 3;
            System.assertEquals(expectedListSize, pdts.size(), 'Expected ' + expectedListSize + ' personnel document training records to be created.');
            
            SQX_Personnel_Document_Training__c pdt1;
            SQX_Personnel_Document_Training__c pdt2;
            SQX_Personnel_Document_Training__c pdt3;
            SQX_Personnel_Document_Training__c pdt4;
            SQX_Personnel_Document_Training__c pdt5;
            for (SQX_Personnel_Document_Training__c pdt : pdts) {
                if (dt1.SQX_Personnel_Document_Training__c == pdt.Id)
                    pdt1 = pdt;
                if (dt2.SQX_Personnel_Document_Training__c == pdt.Id)
                    pdt2 = pdt;
                if (dt3.SQX_Personnel_Document_Training__c == pdt.Id)
                    pdt3 = pdt;
                if (dt4.SQX_Personnel_Document_Training__c == pdt.Id)
                    pdt4 = pdt;
                if (dt5.SQX_Personnel_Document_Training__c == pdt.Id)
                    pdt5 = pdt;
            }
            
            if (includeUser1) {
                System.assert(pdt1 != null, 'Expected document training to be migrated to personnel document training.');
                verifyMigratedDocumentTraining(p1, dt1, pdt1);
                System.assert(pdt2 != null, 'Expected document training to be migrated to personnel document training.');
                verifyMigratedDocumentTraining(p1, dt2, pdt2);
            }
            if (includeUser2) {
                System.assert(pdt3 != null, 'Expected document training to be migrated to personnel document training.');
                verifyMigratedDocumentTraining(p2, dt3, pdt3);
                System.assert(pdt4 != null, 'Expected document training to be migrated to personnel document training.');
                verifyMigratedDocumentTraining(p2, dt4, pdt4);
                System.assert(pdt5 != null, 'Expected document training to be migrated to personnel document training.');
                verifyMigratedDocumentTraining(p2, dt5, pdt5);
            }
        }
    }
    
    public static void verifyMigratedUser(User u, SQX_Personnel__c p) {
        System.assertEquals(u.Id, p.SQX_User__c,
            'Expected personnel to be have same user value.');
        System.assertEquals(u.FirstName + ' ' + u.LastName, p.Full_Name__c,
            'Expected migrated personnel to be have proper full name value.');
        System.assertEquals(u.Username, p.Identification_Number__c,
            'Expected migrated personnel to be have proper identification number value.');
        System.assertEquals(u.Email, p.Email_Address__c,
            'Expected migrated personnel to be have proper email address value.');
        System.assertEquals(true, p.Active__c,
            'Expected migrated personnel to be have active value as true.');
        System.assert(p.Unique_User_Constraint__c != null,
            'Expected migrated personnel to be have uniqueness constraint value.');
    }
    
    public static void verifyMigratedUserJobFunction(SQX_Personnel__c p, SQX_User_Job_Function__c ujf, SQX_Personnel_Job_Function__c pjf) {
        System.assertEquals(ujf.SQX_User__c, p.SQX_User__c,
            'Expected personnel to be compared with same user of obsolete user job function.');
        System.assertEquals(ujf.SQX_Personnel_Job_Function__c, pjf.Id,
            'Expected personnel job function to be compared with migrated record of obsolete user job function.');
        System.assertEquals(p.Id, pjf.SQX_Personnel__c,
            'Expected migrated personnel job function to be have proper personnel value');
        System.assertEquals(ujf.SQX_Job_Function__c, pjf.SQX_Job_Function__c,
            'Expected migrated personnel job function to be have proper job function value.');
        System.assertEquals(ujf.Active__c, pjf.Active__c,
            'Expected migrated personnel job function to be have proper active value.');
        System.assertEquals(ujf.Activation_Date__c, pjf.Activation_Date__c,
            'Expected migrated personnel job function to be have proper activation date value.');
        System.assertEquals(ujf.Deactivation_Date__c, pjf.Deactivation_Date__c,
            'Expected migrated personnel job function to be have proper deactivation date value.');
        System.assert(pjf.Unique_New_Or_Active_Constraint__c != null,
            'Expected migrated personnel job function to be have uniqueness constraint value.');
    }
    
    public static void verifyMigratedDocumentTraining(SQX_Personnel__c p, SQX_Document_Training__c dt, SQX_Personnel_Document_Training__c pdt) {
        System.assertEquals(dt.SQX_User__c, p.SQX_User__c,
            'Expected personnel to be compared with same user of obsolete document training.');
        System.assertEquals(dt.SQX_Personnel_Document_Training__c, pdt.Id,
            'Expected personnel document training to be compared with migrated record of obsolete document training.');
        System.assertEquals(p.Id, pdt.SQX_Personnel__c,
            'Expected migrated personnel document training to be have proper personnel value.');
        System.assertEquals(dt.SQX_Controlled_Document__c, pdt.SQX_Controlled_Document__c,
            'Expected migrated personnel document training to be have proper controlled document value.');
        System.assertEquals(dt.Completion_Date_Internal__c, pdt.Completion_Date__c,
            'Expected migrated personnel document training to be have proper completion date value.');
        System.assertEquals(dt.Due_Date__c, pdt.Due_Date__c,
            'Expected migrated personnel document training to be have proper due date value.');
        System.assertEquals(dt.Level_of_Competency__c, pdt.Level_of_Competency__c,
            'Expected migrated personnel document training to be have proper level of competency value.');
        System.assertEquals(dt.Optional__c, pdt.Optional__c,
            'Expected migrated personnel document training to be have proper optional value.');
        if (dt.SQX_Trainer__c != null || dt.Status__c != SQX_Document_Training.STATUS_PENDING) {
            System.assertEquals(dt.SQX_Trainer__c, pdt.SQX_Trainer__c,
                'Expected migrated personnel document training to be have proper trainer value.');
        }
        System.assertEquals(dt.Status__c, pdt.Status__c,
            'Expected migrated personnel document training to be have proper status value.');
        System.assertEquals(dt.SQX_Training_Approved_By__c, pdt.SQX_Training_Approved_By__c,
            'Expected migrated personnel document training to be have proper training approved by value.');
        System.assertEquals(dt.SQX_User_Signed_Off_By__c, pdt.SQX_User_Signed_Off_By__c,
            'Expected migrated personnel document training to be have proper user signed off by value.');
        System.assertEquals(dt.Trainer_SignOff_Comment__c, pdt.Trainer_SignOff_Comment__c,
            'Expected migrated personnel document training to be have proper trainer signoff comment value.');
        System.assertEquals(dt.Trainer_SignOff_Date__c, pdt.Trainer_SignOff_Date__c,
            'Expected migrated personnel document training to be have proper trainer signoff date value.');
        System.assertEquals(dt.Trainer_Signature__c, pdt.Trainer_Signature__c,
            'Expected migrated personnel document training to be have proper trainer signature value.');
        System.assertEquals(dt.User_SignOff_Date__c, pdt.User_SignOff_Date__c,
            'Expected migrated personnel document training to be have proper user signoff date value.');
        System.assertEquals(dt.User_Signature__c, pdt.User_Signature__c,
            'Expected migrated personnel document training to be have proper user signature value.');
        System.assertEquals(dt.User_Signoff_Comment__c, pdt.User_Signoff_Comment__c,
            'Expected migrated personnel document training to be have proper user signoff comment value.');
        System.assertEquals(dt.SQX_Controlled_Document__r.Document_Number__c, pdt.Document_Number__c,
            'Expected migrated personnel document training to be have proper document number value.');
        System.assertEquals(dt.SQX_Controlled_Document__r.Revision__c, pdt.Document_Revision__c,
            'Expected migrated personnel document training to be have proper document revision value.');
        System.assertEquals(dt.Title__c, pdt.Document_Title__c,
            'Expected migrated personnel document training to be have proper document title value.');
        System.assert(pdt.Uniqueness_Constraint__c != null,
            'Expected migrated personnel document training to be have uniqueness constraint value');
    }


}