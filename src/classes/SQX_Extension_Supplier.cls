public with sharing class SQX_Extension_Supplier{

    private List<SQX_ObjectWrapper> allContacts;
    private List<SQX_ObjectWrapper> allSupplierParts;
    private List<SQX_ObjectWrapper> allSupplierServices;

    private Account currentAccount;
    private ApexPages.StandardController mainController;
    
    public Account getCurrentAccount(){
        return currentAccount;
    }
    
    private transient Schema.DescribeSObjectResult i_ContactCRUD = null;
    /**
    * this method returns the contact object create
    */
    public boolean getContactCreateable(){
        if(i_ContactCRUD == null){
            i_ContactCRUD = new Contact().getSObjectType().getDescribe();
        }
        
        return i_ContactCRUD.isCreateable();
    }
    
    public boolean getContactDeletable(){
        if(i_ContactCRUD == null){
            i_ContactCRUD = new Contact().getSObjectType().getDescribe();
        }
        
        return i_ContactCRUD.isDeletable();
    }

    private transient Schema.DescribeSObjectResult i_SupplierPartCRUD = null;
    /**
    * this method returns the supplier part object create
    */
    public boolean getSupplierPartCreateable(){
        if(i_SupplierPartCRUD == null){
            i_SupplierPartCRUD = new SQX_Supplier_Part__c().getSObjectType().getDescribe();
        }
        
        return i_SupplierPartCRUD.isCreateable();
    }
    
    public boolean getSupplierPartDeletable(){
        if(i_SupplierPartCRUD == null){
            i_SupplierPartCRUD = new SQX_Supplier_Part__c().getSObjectType().getDescribe();
        }
        
        return i_SupplierPartCRUD.isDeletable();
    }

    private transient Schema.DescribeSObjectResult i_SupplierServiceCRUD = null;
    /**
    * this method returns the contact object create
    */
    public boolean getSupplierServiceCreateable(){
        if(i_SupplierServiceCRUD == null){
            i_SupplierServiceCRUD = new SQX_Supplier_Service__c().getSObjectType().getDescribe();
        }
        
        return i_SupplierServiceCRUD.isCreateable();
    }
    
    public boolean getSupplierServiceDeletable(){
        if(i_SupplierServiceCRUD == null){
            i_SupplierServiceCRUD = new SQX_Supplier_Service__c().getSObjectType().getDescribe();
        }
        
        return i_SupplierServiceCRUD.isDeletable();
    }

    
    public SQX_Extension_Supplier(ApexPages.StandardController controller){
        currentAccount = (Account) controller.getRecord();
        mainController = controller;
        if(allContacts == null){
            
            //fetch all contacts returns a blank list if nothing is found
            allContacts = getObjectsFor(Contact.getSObjectType(), 
                            Schema.SObjectType.Contact.FieldSets.Basic_Information.getFields(), 'AccountId');
            
            
            allSupplierParts = getObjectsFor(SQX_Supplier_Part__c.getSObjectType(), 
                                Schema.SObjectType.SQX_Supplier_Part__c.FieldSets.Basic_Information.getFields(), 'Account__c');
            
            
            allSupplierServices = getObjectsFor(SQX_Supplier_Service__c.getSObjectType(), 
                                Schema.SObjectType.SQX_Supplier_Service__c.FieldSets.Basic_Information.getFields(), 'Account__c');
        
            if(currentAccount.Id == null){
                //this is new account
                //add one contact, supplier part and supplier service by default
                
                addNewContact();
                addNewSupplierPart();
                addNewSupplierService();
            }
        }

    }
    
    /**
    * this action redirects the user to supplier page if logged in user is a supplier.
    */
    public PageReference redirectIfSupplier(){
        PageReference redirectTo = null;
        
        if(SQX_Utilities.getCurrentUsersRole() == SQX_Utilities.SQX_UserRole.RoleSupplier){
            redirectTo = Page.SQX_Supplier_Admin;
            redirectTo.getParameters().put('id', currentAccount.ID);
            redirectTo.getParameters().put('retURL', '/' + currentAccount.ID);
            redirectTo.getParameters().put('saveURL', '/' + currentAccount.ID);
        }
        
        return redirectTo;
    }
    
    /**
    * adds a new blank contact to the user record
    */
    public void addNewContact(){
        if(allContacts == null )
            return;
            
        
        
        integer serialNumber = allContacts.size() == 0 ? 1 : (allContacts.get(allContacts.size() - 1).serialNumber + 1);
        SQX_ObjectWrapper newObject = new SQX_ObjectWrapper();
        Contact contact = new Contact();
        contact.AccountID = currentAccount.ID;
        newObject.baseObject = contact;
        newObject.isSelected = false;
        newObject.serialNumber = serialNumber;
        newObject.markForDeletion  = false;
        newObject.isNewObject = true;
        
        allContacts.add(newObject);
        
    }
    
    /**
    * adds a new supplier part to account
    */
    public void addNewSupplierPart(){
        if(allSupplierParts == null )
            return;
        
        integer serialNumber = allSupplierParts.size() == 0 ? 1 : (allSupplierParts.get(allSupplierParts.size() - 1).serialNumber + 1);   
        SQX_ObjectWrapper newObject = new SQX_ObjectWrapper();
        SQX_Supplier_Part__c supplierPart = new SQX_Supplier_Part__c();
        supplierPart.Account__c = currentAccount.ID;
        newObject.baseObject = supplierPart;
        newObject.isSelected = false;
        newObject.serialNumber = serialNumber;
        newObject.markForDeletion  = false;
        newObject.isNewObject = true;
        
        allSupplierParts.add(newObject);
    }
    
    public void addNewSupplierService(){
        if(allSupplierServices == null  )
            return;
        
        integer serialNumber = allSupplierServices.size() == 0 ? 1 : (allSupplierServices.get(allSupplierServices.size() - 1).serialNumber + 1);
        SQX_ObjectWrapper newObject = new SQX_ObjectWrapper();
        SQX_Supplier_Service__c supplierService = new SQX_Supplier_Service__c();
        supplierService.Account__c = currentAccount.ID;
        newObject.baseObject = supplierService;
        newObject.isSelected = false;
        newObject.serialNumber = serialNumber;
        newObject.markForDeletion  = false;
        newObject.isNewObject = true;
        
        allSupplierServices.add(newObject);
    }

    public List<SQX_ObjectWrapper> getAllContacts() {

        return this.allContacts;
    }
    
    public List<SQX_ObjectWrapper> getAllSupplierParts(){

        
        return this.allSupplierParts;
    }
    
    public List<SQX_ObjectWrapper> getAllSupplierServices(){

        
        return this.allSupplierServices;
    }
    
    
    private List<SQX_ObjectWrapper> getObjectsFor(Schema.SObjectType objectType, List<Schema.FieldSetMember> fields, String accountField){
        Schema.DescribeSObjectResult objectDesc = objectType.getDescribe();
        if(this.CurrentAccount.ID == null || !objectDesc.isQueryable() || !objectDesc.isAccessible()){
            return new List<SQX_ObjectWrapper>();
        }
        
        String query = 'SELECT ';
        boolean namePresent = false;
        boolean accountFieldPresent = false;
        
        for(Schema.FieldSetMember f : fields) {
            query += f.getFieldPath() + ', ';
            if(f.getFieldPath() == 'Name')
                namePresent = true;
                
            if(f.getFieldPath() == accountField)
                accountFieldPresent = true;
        }
        
        if(namePresent == false)
            query += 'Name, ';
            
        if(accountFieldPresent == false)
            query += accountField + ',';
            
        query += 'Id FROM ' + objectType;
        query += ' WHERE ' + accountField + ' = \'' + currentAccount.ID + '\'';
        query += ' ORDER BY Name ';
        List<sObject> queryResult = Database.query(query);
        
        List<SQX_ObjectWrapper> returnList = new List<SQX_ObjectWrapper>();
        integer count = 1;
        for(sObject baseObject : queryResult){
            SQX_ObjectWrapper wrapper = new SQX_ObjectWrapper();
            wrapper.baseObject = baseObject;
            wrapper.isSelected = false;
            wrapper.serialNumber = count;
            wrapper.markForDeletion  = false;
            returnList.add(wrapper);
            
           
            
            count++;
        }
        
        return returnList;
    }
    
    public String getCurrentAccountID(){
        return currentAccount.ID;
    }
    
    
    public PageReference saveAllChanges(){
        Id oldAccountId = null;
        PageReference returnReference = null;
        SavePoint nothingSaved = Database.setSavePoint();
        
        List<Schema.FieldSetMember> contactFields = Schema.SObjectType.Contact.FieldSets.Basic_Information.getFields();
        List<SQX_ObjectWrapper> originalContacts = getObjectsFor(Contact.getSObjectType(), 
                            contactFields, 'AccountId');
                            
        
        List<Schema.FieldSetMember> supplierPartsFields = Schema.SObjectType.SQX_Supplier_Part__c.FieldSets.Basic_Information.getFields();
        List<SQX_ObjectWrapper> originalSupplierParts = getObjectsFor(SQX_Supplier_Part__c.getSObjectType(), 
                                supplierPartsFields, 'Account__c');

        List<Schema.FieldSetMember> supplierServicesFields = Schema.SObjectType.SQX_Supplier_Service__c.FieldSets.Basic_Information.getFields();
        List<SQX_ObjectWrapper> originalSupplierServices = getObjectsFor(SQX_Supplier_Service__c.getSObjectType(), 
                                supplierServicesFields, 'Account__c');
        
        try{
            
            currentAccount = (Account)mainController.getRecord();
            oldAccountId = currentAccount.Id;

            Database.SaveResult saveResult = null; 
            if(currentAccount.Id == null){
                if(currentAccount.getSObjectType().getDescribe().isCreateable() == false)
                    throw new SQX_ApplicationGenericException('Account is not createable');

                saveResult = Database.Insert(currentAccount, false);
                if(saveResult.isSuccess() == false){
                    currentAccount.Id = null;
                }
            }
            else{
                if(currentAccount.getSObjectType().getDescribe().isUpdateable() == false)
                    throw new SQX_ApplicationGenericException('Account is not updateable');
                
                saveResult = Database.Update(currentAccount, false);
            }

            


            if(saveResult.isSuccess()){
                returnReference = new PageReference('/' + currentAccount.Id);
                boolean hasErrors = false;

                boolean persistChangesError = !persistChanges(allContacts,'AccountID',originalContacts , contactFields );
                hasErrors = hasErrors || persistChangesError;
                System.debug('Has error : ' + hasErrors + ' after persisting contacts ' + persistChangesError);
                
                
                persistChangesError = !persistChanges(allSupplierParts,'Account__c', originalSupplierParts , supplierPartsFields );
                hasErrors = hasErrors || persistChangesError;
                System.debug('Has error : ' + hasErrors + ' after persisting supplier parts ' + persistChangesError);
                
                persistChangesError = !persistChanges(allSupplierServices,'Account__c',originalSupplierServices , supplierServicesFields );
                hasErrors = hasErrors || persistChangesError;
                System.debug('Has error : ' + hasErrors + ' after persisting supplier services ' + persistChangesError);
                
                
                if(hasErrors == true){
                    currentAccount.Id = oldAccountId;

                    returnReference = null;
                    Database.rollback(nothingSaved);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The save operation could not be completed because of some errors.'));
                    
                    
                    //revert all newObjects id to null
                    List<List<SQX_ObjectWrapper>> allObjects = new List<List<SQX_ObjectWrapper>>{allContacts, allSupplierParts, allSupplierServices };
                    for(List<SQX_ObjectWrapper> objectList : allObjects){
                        if(objectList == null || objectList.size() == 0)
                            continue;
                             
                        for(SQX_ObjectWrapper wrappedObject : objectList){
                            if(wrappedObject == null)
                                continue;
                            System.assert(wrappedObject != null, 'Expected non null value but found null values');
                            if(wrappedObject.isNewObject){
                                wrappedObject.baseObject.put('Id',null);
                            }
                        }
                    }
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Error occurred while saving account ' 
                    + saveResult.getErrors().get(0).getMessage()));

            }
        }
        catch(Exception ex){
            returnReference = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage() + ex.getStackTraceString()));
        }
        
        return returnReference;
            
    }
    
    /**
    * @author Pradhanta Bhandari
    * @description this method saves the list of object of given type
    * @param objectList list of objects to be persisted
    * @param accountField the field in the object which relates to corresponding Account object. 
    * @return <code>true</code> if the object was saved successfully,
    *         <code>false</code> if there was any error 
    */
    private boolean persistChanges(List<SQX_ObjectWrapper> objectList, String accountField,
     List<SQX_ObjectWrapper> originals, List<Schema.FieldSetMember> fields){
        if(objectList == null)
            return true;


    
        boolean hasError = false;
        List<sObject> objectsToDelete = new List<sObject>();
        List<sObject> objectsToUpdate = new List<sObject>();
        List<sObject> objectsToInsert = new List<sObject>();
        
        for(SQX_ObjectWrapper wrappedObject : objectList){
            if(wrappedObject.baseObject.get(accountField) == null || 
                wrappedObject.baseObject.get(accountField) != currentAccount.ID){
                wrappedObject.baseObject.put(accountField, currentAccount.ID);
            }
            
        
            if(wrappedObject.markForDeletion == true){
                System.assert(wrappedObject.baseObject.ID != null ,
                    'Expected the ID to be non null but found null ID for ' + wrappedObject.baseObject); 
                
                System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isDeletable() , wrappedObject.baseObject + ' is not deletable.');
                objectsToDelete.add(wrappedObject.baseObject);
                
            }
            else{
                if(wrappedObject.baseObject.ID == null){
                    System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isCreateable() , wrappedObject.baseObject + ' is not createable.'); //fls is enforced by ui
                    objectsToInsert.add(wrappedObject.baseObject);
                }
                else{
                    if(hasChanged(wrappedObject.baseObject, originals, fields)){
                        System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isUpdateable() , wrappedObject.baseObject + ' is not updatable.');
                        objectsToUpdate.add(wrappedObject.baseObject);
                    }
                }
            }
        }
        
        List<Database.DeleteResult> deleteResults = new SQX_DB().continueOnError().op_delete(objectsToDelete);

        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            if(wrappedObject.markForDeletion == true){
                for(Database.DeleteResult deleteResult : deleteResults){
                    if(deleteResult.getID() == wrappedObject.baseObject.ID){
                        if(deleteResult.isSuccess() == false){
                            wrappedObject.markForDeletion = false;
                            wrappedObject.baseObject.addError(deleteResult.getErrors().get(0).getMessage() );
                            hasError = true;
                            
                            break;
                        }
                    }
                }
            }
        }
        
        List<Database.SaveResult> updateResults = Database.update(objectsToUpdate, false);
        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            
            for(Integer index = objectsToUpdate.size() - 1; index >= 0; index--){
                if(wrappedObject.baseObject == objectsToUpdate.get(index)){
                    Database.SaveResult dmlSaveResult = updateResults.get(index);
                
                    if(dmlSaveResult.isSuccess() == false){
                        wrappedObject.baseObject.addError(dmlSaveResult.getErrors().get(0).getMessage());
                        hasError = true;
                        objectsToUpdate.remove(index); //just reducing number of iterations for next match
                        break;
                    }
                
                }
            }
        }
        
        List<Database.SaveResult> insertResults = Database.insert(objectsToInsert, false);
        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            
            for(Integer index = 0; index < objectsToInsert.size(); index++){
                if(wrappedObject.baseObject == objectsToInsert.get(index)){
                    Database.SaveResult dmlSaveResult = insertResults.get(index);
                
                    if(dmlSaveResult.isSuccess() == false){
                        wrappedObject.baseObject.addError(dmlSaveResult.getErrors().get(0).getMessage());
                        wrappedObject.baseObject.put(accountField, null);
                        wrappedObject.baseObject.put('Id', null);

                        hasError = true;
                        objectsToInsert.remove(index); //just reducing number of iterations for next match
                        break;
                    }
                
                }
            }
        }
        
        return !hasError; //return success value i.e. no error
        
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/4
    * @description checks whether a object has been modified or not
    * @
    */
    private boolean hasChanged(sObject objToValidate, List<SQX_ObjectWrapper> originals, List<Schema.FieldSetMember> fields){
        boolean hasChanged = false;
        
        for(SQX_ObjectWrapper original : originals){
            if(original.baseObject.get('Id') == objToValidate.get('Id')){        
                for(Schema.FieldSetMember field : fields){
                    if(original.baseObject.get(field.getFieldPath()) != 
                    objToValidate.get(field.getFieldPath())){
                        hasChanged = true;
                        break;
                    }
                }
                
                break;
            }
        }
        
        return hasChanged;
    }
    /*
    private boolean associateError(List<sObject> objects, List<Database.UpsertResult> results){
        boolean hasError = false;
        
        System.assert(objects.size() == results.size(), 
        'Expected result (' + results.size() + ') and object size (' + objects.size() + ') to be same but found different');
        
        
        for(sObject objectToCheck : objects){
            integer index = 0;
            for(Database.UpsertResult result : results){
                if(result.getID() == objectToCheck.ID){
                    if(result.isSuccess() == false){
                        objectToCheck.addError('Error while saving');
                        hasError = true;
                    }
                }
            }
            
            results.remove(index);
        }
        
        return hasError;
    } 
    
    private List<sObject> unwrapObject(List<SQX_ObjectWrapper> wrappedSObjects, String accountFieldName, boolean onlySelected){
        List<sObject> objects = new List<sObject>();
        
        for(SQX_ObjectWrapper wrappedSObject : wrappedSObjects){
            if(onlySelected){
                if(wrappedSObject.isSelected == false)
                    continue;
            }
            
            if(wrappedSObject.baseObject.get(accountFieldName) == null){
                System.assert(currentAccount.ID != null, 'Expected the account ID to be set but found it null');
                wrappedSObject.baseObject.put(accountFieldName, currentAccount.ID);
            }
            
            objects.add(wrappedSObject.baseObject);
        }
        
        return objects;
    }
    
    
    /******** delete actions ********* /    
    public void deleteSelectedContact(){
        deleteSelectedObjects(allContacts, 'AccountID');
    }
    
    public void deleteSelectedSupplierParts(){
        deleteSelectedObjects(allSupplierParts, 'Account__c');
    }
    
    public void deleteSelectedSupplierServices(){
        deleteSelectedObjects(allSupplierServices, 'Account__c');
    }
    
    
    private void deleteSelectedObjects(List<SQX_ObjectWrapper> wrappedObjects, String accountFieldName ){
           
        List<sObject> selectedObjects = unwrapObject(wrappedObjects, accountFieldName, true);

        //remove any non inserted objects
        for(integer index = selectedObjects.size() - 1; index > 0 ; index--){
            sObject baseObject = selectedObjects.get(index);
            if(baseObject.ID == null){
                //remove from main list
                for(integer count = 0; count < wrappedObjects.size(); count++){
                    if(baseObject == wrappedObjects.get(count).baseObject){
                        wrappedObjects.remove(count);
                        selectedObjects.remove(index);
                        break;
                    }
                }
            }
            
            
        }
        
        List<Database.DeleteResult> results = Database.delete(selectedObjects, false);
        
        
        for(Database.DeleteResult dr : results) {
            Boolean deleteObjectFromList = false;
            integer index = 0;
            for(SQX_ObjectWrapper wrappedObject : wrappedObjects){
                if(wrappedObject.baseObject.ID == dr.getID()){
                    if(dr.isSuccess()){
                        //remove from list
                        deleteObjectFromList = true;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully deleted: ' 
                        + wrappedObject.baseObject.get('Name')));
                    }
                    else{
                        //associate the error
                        wrappedObject.baseObject.addError(dr.getErrors().get(0).getMessage());

                    }
                    break;
                }
                index++;
            }
            
            if (deleteObjectFromList) {
                wrappedObjects.remove(index);
                
            }
            
        }
    } */
    
    public void removeTemporaryPart(){
        removeTemporaryObject('supplierPartIndex', allSupplierParts);
    }
    
    public void removeTemporaryContact(){
        removeTemporaryObject('contactIndex', allContacts);
    }
    
    public void removeTemporaryService(){
        
        removeTemporaryObject('supplierServiceIndex', allSupplierServices);
    }
    
    public void removeTemporaryObject(String paramName, List<SQX_ObjectWrapper> objectList){        
        
        integer objectSerialNumber = -1;
        String index = ApexPages.currentPage().getParameters().get(paramName);
        
        if(index == null)
            return;
        
        objectSerialNumber = integer.valueOf(index);
        
        if(objectSerialNumber < 1)
            return;
            
        integer indexOfObject  = -1;
        integer count = 0;
        for(SQX_ObjectWrapper wrappedObject : objectList){
            if(wrappedObject.serialNumber == objectSerialNumber){
                
                indexOfObject = count;
                break;
            }
            count++;
        }
        
        if(indexOfObject != -1){
            SQX_ObjectWrapper wrappedObject = objectList.get(indexOfObject);
            wrappedObject.markForDeletion = !wrappedObject.markForDeletion;
            
            if(wrappedObject.baseObject.ID == null){
                objectList.remove(indexOfObject);
            }
        }

    }
    

    
}