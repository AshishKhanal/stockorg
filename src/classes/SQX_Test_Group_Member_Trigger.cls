/**
*   @author : Piyush Subedi
*   @date : 2016/11/23
*   @description : Test class to test GroupMemberTrigger
*/
@isTest
public class SQX_Test_Group_Member_Trigger{
    
    static Boolean runAllTests = true,
                    run_givenACollaborationGroup_WhenNewMemberIsAdded_ThenAGroupFeedStatingTheAdditionOfMemberShouldBeGenerated = false,
                    run_givenACollaborationGroup_WhenAMemberIsRemoved_ThenAGroupFeedStatingTheRemovalOfMemberShouldBeGenerated = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users       
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');
        
        System.runAs(standardUser1){
            // creating a group
            CollaborationGroup grp = new CollaborationGroup();
            grp.Name = 'Test Collab Group';
            grp.OwnerId = standardUser1.Id;
            grp.CollaborationType = SQX_SF_Collaboration_Provider.COLLABORATION_TYPE_PRIVATE;
            insert grp;

            // adding group id to a controlled record
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document().save();
            tstObj.doc.Collaboration_Group_Id__c = grp.Id;
            tstObj.save();            
        }
    }
    
    /**
    *   Given a collaboration group associated with a controlled document record
    *   When a new member is added to the group
    *   Then a feed should be generated in the group regarding the addition of the member
    */
    static testMethod void givenACollaborationGroup_WhenNewMemberIsAdded_ThenAGroupFeedStatingTheAdditionOfMemberShouldBeGenerated(){
        
        if(!runAllTests && !run_givenACollaborationGroup_WhenNewMemberIsAdded_ThenAGroupFeedStatingTheAdditionOfMemberShouldBeGenerated)
            return;

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser1 = users.get('standardUser1');
        System.runAs(standardUser1){
            
            /** Arrange */

            CollaborationGroup grp = [SELECT Id FROM CollaborationGroup LIMIT 1];
          
            User standardUser2 = users.get('standardUser2');

            /** Act */
            // adding member to group
            CollaborationGroupMember grpMember = new CollaborationGroupMember();
            grpMember.CollaborationGroupId = grp.Id;
            grpMember.memberId = standardUser2.Id;
            insert grpMember;

            /** Assert */
            List<CollaborationGroupFeed> grpFeeds = [SELECT Body, Title FROM CollaborationGroupFeed WHERE ParentId = :grp.Id];
            // Asserting if feed has been created
            System.Assert(grpFeeds.size() > 0, 'Expected a feed item in group, found none');
            
            CollaborationGroupFeed feed = grpFeeds.get(0);
            // Asserting feed title is valid
            System.assert(feed.Title == Label.CQ_UI_New_Member_Added_In_Group_Title, 'Expected title : ' + Label.CQ_UI_New_Member_Added_In_Group_Title +' Actual Title : ' + feed.Title);
            
            String memberName = standardUser2.Name;
            // Asserting feed body is valid
            System.assert(feed.Body == Label.CQ_UI_New_Member_Added_In_Group.replace('{NAME}', memberName), 'Expected body : ' + Label.CQ_UI_New_Member_Added_In_Group.replace('{NAME}', memberName) +
                                                                                                                    'Actual body : ' + feed.Body);
        }

    }

    /**
    *   Given a collaboration group associated with a controlled document record
    *   When a member is removed from the group
    *   Then a feed should be generated in the group regarding the removal of the member
    */
    static testMethod void givenACollaborationGroup_WhenAMemberIsRemoved_ThenAGroupFeedStatingTheRemovalOfMemberShouldBeGenerated(){
        
        if(!runAllTests && !run_givenACollaborationGroup_WhenAMemberIsRemoved_ThenAGroupFeedStatingTheRemovalOfMemberShouldBeGenerated)
            return;
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser1 = users.get('standardUser1');
        System.runAs(standardUser1){
            
            /** Arrange */
            CollaborationGroup grp = [SELECT Id FROM CollaborationGroup LIMIT 1];
            
            // adding a new member to the group
            User standardUser2 = users.get('standardUser2');
            CollaborationGroupMember grpMember = new CollaborationGroupMember();
            grpMember.CollaborationGroupId = grp.Id;
            grpMember.memberId = standardUser2.Id;
            insert grpMember;

            /** NOTE : By adding a new member, a feed is already created */

            /** Act */
            // removing member from the group           
            delete grpMember;

            /** Assert */
            List<CollaborationGroupFeed> grpFeeds = [SELECT Body, Title FROM CollaborationGroupFeed WHERE ParentId = :grp.Id ORDER BY Title ASC];
            // Asserting if feed has been created
            System.Assert(grpFeeds.size() > 1, 'Expected two feeds item in group, found none');
            
            CollaborationGroupFeed feed = grpFeeds.get(0);  
            // Asserting feed title is valid
            System.assert(feed.Title == Label.CQ_UI_Member_Removed_From_Group_Title, 'Expected title : ' + Label.CQ_UI_Member_Removed_From_Group_Title +' Actual Title : ' + feed.Title);
            
            String memberName = standardUser2.Name;
            // Asserting feed body is valid
            System.assert(feed.Body == Label.CQ_UI_Member_Removed_From_Group.replace('{NAME}', memberName), 
                          'Expected body : ' + Label.CQ_UI_Member_Removed_From_Group.replace('{NAME}', memberName) +
                          'Actual body : ' + feed.Body);
        }
    }

}