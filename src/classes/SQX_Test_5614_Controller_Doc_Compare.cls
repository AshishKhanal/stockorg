/**
* unit tests for SQX_Controller_Document_Compare controller
*/
@isTest
public class SQX_Test_5614_Controller_Doc_Compare {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensure SQX_Controller_Document_Compare constructor sets internally stored values properly
    */
    testmethod static void givenInstantiateController_ValuesAreSetProperly() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('user1')) {
            // ARRANGE: set document compare with base doc id and cmpare with doc id params
            Test.setCurrentPage(Page.SQX_Document_Compare);
            String  baseDocId = 'baseDocId',
                    compareWithDocId = 'compareWithDocId';
            ApexPages.currentPage().getParameters().put(SQX_Controller_Document_Compare.PARAM_BASE_DOC_ID, baseDocId);
            ApexPages.currentPage().getParameters().put(SQX_Controller_Document_Compare.PARAM_COMPARE_WITH_DOC_ID, compareWithDocId);
            
            // ACT: initialize SQX_Controller_Document_Compare controller
            SQX_Controller_Document_Compare cntlr = new SQX_Controller_Document_Compare();
            
            // ASSERT: controller params values
            System.assertEquals(baseDocId, cntlr.prmBaseDocId, 'Page param for base doc id is expected to be set.');
            System.assertEquals(compareWithDocId, cntlr.prmCompareWithDocId, 'Page param for compare with doc id is expected to be set.');
            System.assertNotEquals(null, SQX_Controller_Document_Compare.isDocComparePackagePageAvailable,
                'Expected SQX_Controller_Document_Compare.isDocComparePackagePageAvailable value to be set.');
        }
    }
    
    /**
    * ensure SQX_Controller_Document_Compare.redirectToAddonPage() return proper page url with params
    */
    testmethod static void givenRedirectToAddonPageMethodCalled_ProperPageUrlIsReturned() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('user1')) {
            // ARRANGE: set doc id params values of the controlled with isDocComparePackagePageAvailable value as false
            SQX_Controller_Document_Compare cntlr = new SQX_Controller_Document_Compare();
            SQX_Controller_Document_Compare.isDocComparePackagePageAvailable = false;
            cntlr.prmBaseDocId = 'doc1Id';
            cntlr.prmCompareWithDocId = 'doc2Id';
            
            // ACT: execute redirectToAddonPage() when doc compare addon page is not available
            PageReference pr = cntlr.redirectToAddonPage();
            
            // ASSERT: verify no page url is returned
            System.assertEquals(null, pr, 'Expected SQX_Controller_Document_Compare.redirectToAddonPage() to return null when document compare package is not available.');
            
            // ARRANGE: setting isDocComparePackagePageAvailable to true
            SQX_Controller_Document_Compare.isDocComparePackagePageAvailable = true;
            
            // ACT: execute redirectToAddonPage() when document compare package page is available
            pr = cntlr.redirectToAddonPage();
            
            // ASSERT: verify valid page url values
            String expectedPage = SQX_Controller_Document_Compare.DOCUMENT_COMPARE_PACKAGE_NAMESPACE
                                + '__' + SQX_Controller_Document_Compare.DOCUMENT_COMPARE_PACKAGE_COMPARE_PAGENAME;
            System.assertNotEquals(null, pr, 'Expected SQX_Controller_Document_Compare.redirectToAddonPage() to return some url when document compare package is available.');
            System.assert(pr.getUrl().contains(expectedPage),
                'Expected SQX_Controller_Document_Compare.redirectToAddonPage() to return valid page when document compare package is available.');
            System.assert(pr.getUrl().contains(SQX_Controller_Document_Compare.PARAM_BASE_DOC_ID + '=doc1Id'),
                'Expected SQX_Controller_Document_Compare.redirectToAddonPage() to return url with valid base doc id param when document compare package is available.');
            System.assert(pr.getUrl().contains(SQX_Controller_Document_Compare.PARAM_COMPARE_WITH_DOC_ID + '=doc2Id'),
                'Expected SQX_Controller_Document_Compare.redirectToAddonPage() to return url with valid compare with doc id param when document compare package is available.');
        }
    }
    
    /**
    * ensure SQX_Controller_Document_Revision_History.getIsDocComparePackagePageAvailable() is in sync with SQX_Controller_Document_Compare.isDocComparePackagePageAvailable
    * ensure SQX_Extension_PDT_SignOff_Base.getIsDocComparePackagePageAvailable() is in sync with SQX_Controller_Document_Compare.isDocComparePackagePageAvailable
    * ensure SQX_Extension_UI.getIsDocComparePackagePageAvailable() is in sync with SQX_Controller_Document_Compare.isDocComparePackagePageAvailable
    */
    testmethod static void givenGetIsDocComparePackagePageAvailableIsCalled_ValueIsInSync() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('user1')) {
            // ARRANGE: setting SQX_Controller_Document_Compare.isDocComparePackagePageAvailable to false
            SQX_Controller_Document_Compare.isDocComparePackagePageAvailable = false;
            
            // ARRANGE: create instances of SQX_Controller_Document_Revision_History class,
            //          SQX_Extension_PDT_User_SignOff (extended class of SQX_Extension_PDT_SignOff_Base) class,
            //          SQX_Extension_Change_Order (extended class of SQX_Extension_UI) class
            SQX_Controller_Document_Revision_History revCntlr = new SQX_Controller_Document_Revision_History();
            SQX_Extension_PDT_User_SignOff signOffCntlr = new SQX_Extension_PDT_User_SignOff(new ApexPages.StandardSetController(new List<SQX_Personnel_Document_Training__c>()));
            SQX_Extension_Change_Order extChangeOrder = new SQX_Extension_Change_Order(new ApexPages.StandardController(new SQX_Change_Order__c()));
            
            // ACT: verify getIsDocComparePackagePageAvailable() value
            System.assertEquals(false, revCntlr.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Controller_Document_Revision_History.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
             System.assertEquals(false, signOffCntlr.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Extension_PDT_SignOff_Base.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
             System.assertEquals(false, extChangeOrder.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Extension_UI.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
           
            // ARRANGE: setting SQX_Controller_Document_Compare.isDocComparePackagePageAvailable to true
            SQX_Controller_Document_Compare.isDocComparePackagePageAvailable = true;
            
            // ACT: verify getIsDocComparePackagePageAvailable() value
            System.assertEquals(true, revCntlr.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Controller_Document_Revision_History.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
            System.assertEquals(true, signOffCntlr.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Extension_PDT_SignOff_Base.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
            System.assertEquals(true, extChangeOrder.getIsDocComparePackagePageAvailable(),
                'Expected SQX_Extension_UI.getIsDocComparePackagePageAvailable() to reflect value of SQX_Controller_Document_Compare.isDocComparePackagePageAvailable');
        }
    }
}