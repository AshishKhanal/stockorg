public virtual class SQX_Test_Base_Class{
    public static void validatePickListValuesMatch(String[] allStatuses, String className, String fieldName){
        Set<String> pickListValues = SQX_Utilities.getPicklistValues(className, SQX.getNSNameFor(fieldName));
        
        System.assert(pickListValues != null && pickListValues.size() > 0, 
                     'Expected the picklist values to be returned for ' + fieldName + ' in ' +
                      className + 'but failed to retrieve any valid values');
        
        for(String status: allStatuses){
            System.assert(pickListValues.contains(status), 
                          'Expected \'' + status + '\' to be in pick list of \'' + className + '\' in field \'' +
                          fieldName + '\', but found it missing');
        }
    }
}