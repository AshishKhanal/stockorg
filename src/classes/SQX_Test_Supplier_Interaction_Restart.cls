/*
 * Unit tests for Supplier Interaction Restart Flow
 */
@isTest
public class SQX_Test_Supplier_Interaction_Restart {

    /*
    *   Following scenarios are to tested
    *   1. If there are no applicable steps added after completion, when the record is restarted, the record should move to Complete/Verification
    *   2. If there are applicable steps added after completed, when the record is restarted, the record should move to Open/In Progress with Step number equal to added step's number
    */

    /**
     *  Test setup
     */
    @testSetup
    public static void commonSetup(){

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');

        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> { standardUser });

        System.runAs(adminUser) {
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, standardUser, 1);
        }

        System.runAs(standardUser) {
            // Create a new completed SI record
            SQX_Test_Supplier_Interaction si = new SQX_Test_Supplier_Interaction();
            si.save();
            si.submit();
            si.initiate();

            Task t = [SELECT Id FROM Task WHERE WhatId =: si.si.Id];
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(t.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

        }
    }

    /**
     *  Returns <code>true</code> if the given Supplier Interaction record is open
     */
    private static Boolean isRecordOpen(SQX_Supplier_Interaction__c si) {
        return si.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN &&
                si.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_IN_PROGRESS &&
                si.Workflow_Status__c == SQX_Supplier_Common_Values.WORKFLOW_STATUS_IN_PROGRESS;
    }

    /**
     * Method returns a SQX_Test_Supplier_Interaction object holding a completed SI record
     */
    private static SQX_Test_Supplier_Interaction getCompletedSupplierInteractionTestObject() {
        SQX_Test_Supplier_Interaction si = new SQX_Test_Supplier_Interaction();
        si.si = [SELECT Id FROM SQX_Supplier_Interaction__c LIMIT 1];

        return si.synchronize();
    }


    /**
     *  Given : Completed SI record with no applicable steps added after completion
     *  When : SI is restarted
     *  Then : Record is complete
     */
    private static testmethod void givenCompletedSIWithNoApplicableStepsRemaining_WhenTheRecordIsRestarted_ThenTheRecordIsSetAsComplete() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {

            // ARRANGE : A completed SI record
            SQX_Test_Supplier_Interaction si = getCompletedSupplierInteractionTestObject();

            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, si.si.Status__c, 'Expected interaction record to be complete');

            // ACT : Restarting the record
            Test.startTest();
            si.restart();
            Test.stopTest();

            // ASSERT : record should be complete
            si.synchronize();
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, si.si.Status__c, 'Expected interaction record to automatically complete after restart');
        }

    }


    /**
     *  Given : Completed SI record and an added Step
     *  When : Record is restarted
     *  Then : Record is Open
     */
    private static testmethod void givenCompletedSIWithApplicableStepsAdded_WhenTheRecordIsRestarted_ThenTheRecordIsSetAsOpen() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser) {

            // ARRANGE : A Completed SI record
            SQX_Test_Supplier_Interaction si = getCompletedSupplierInteractionTestObject();

            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, si.si.Status__c, 'Expected interaction record to be complete');

            // adding an applicable step
            SQX_Supplier_Interaction_Step__c step = new SQX_Supplier_Interaction_Step__c(
                SQX_Parent__c= si.si.Id,
                Name='CustomStep',
                Step__c=2,
                SQX_User__c=UserInfo.getUserId(),
                Due_Date__c=Date.today(),
                RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierInteractionStep, SQX_Steps_Trigger_Handler.RT_TASK)
            );
            insert step;

            // ACT : Restarting the record
            Test.startTest();
            si.restart();
            Test.stopTest();

            // ASSERT : record should be open with correct step number
            si.synchronize();
            System.assert(isRecordOpen(si.si), 'Expected record to be open but found ' + si.si);
            System.assertEquals(1, [SELECT Id FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c =: si.si.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size(), 'Expected exactly 1 step to be opened');
            System.assertEquals(2, si.si.Current_Step__c, 'Invalid step number in record after restart');
        }

    }

}