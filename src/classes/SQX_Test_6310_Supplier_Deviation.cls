/**
 * This test class ensures linking of Finding and Nonconformance
 * to a Supplier Deviation records are unique
 * @Author: Dil Bahadur Thapa
 * @Date: 9/7/2018
 */
@isTest
public class SQX_Test_6310_Supplier_Deviation {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * GIVEN: Supplier Deviation with Finding linked
     * WHEN: Same Finding record is attempted to link
     * THEN: Finding record should not be linked
     * @Story: [SQX-6310]
     */
    
    public static testmethod void givenSupplierDeviationWithFindingLinked_WhenSameFindingIsAttemptedToLink_ThenFindingShouldNotBeLinked(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            
            // Create Supplier Deviation
            SQX_Test_Supplier_Deviation supplierDeviation = new SQX_Test_Supplier_Deviation();
            supplierDeviation.save();
            
            SQX_Test_Supplier_Deviation supplierDeviation2 = new SQX_Test_Supplier_Deviation();
            supplierDeviation2.save();
            
            // Create Finding
            SQX_Test_Finding finding = new SQX_Test_Finding();
            finding.save();
            
            // ARRANGE: Link the created Supplier Deviation and Finding
            SQX_Supplier_Deviation_Finding__c sdFinding = new SQX_Supplier_Deviation_Finding__c();
            sdFinding.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdFinding.SQX_Finding__c = finding.finding.Id;
            
            Database.SaveResult insertResult = Database.insert(sdFinding);
            
            // ACT: Link same Supplier Deviation and Finding
            SQX_Supplier_Deviation_Finding__c sdFinding2 = new SQX_Supplier_Deviation_Finding__c();
            sdFinding2.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdFinding2.SQX_Finding__c = finding.finding.Id;
            Database.SaveResult duplicateInsertResult = Database.insert(sdFinding2, false);
            
            // ASSERT: Ensure duplicate link is not created
            System.assert(!duplicateInsertResult.isSuccess(), 'Expected update action to fail when Supplier Deviation and Finding combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(duplicateInsertResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + duplicateInsertResult.getErrors());
            
            // ARRANGE: Link the created Supplier Deviation and Finding
            SQX_Supplier_Deviation_Finding__c sdFindingUpdate = new SQX_Supplier_Deviation_Finding__c();
            sdFindingUpdate.SQX_Supplier_Deviation__c = supplierDeviation2.sd.Id;
            sdFindingUpdate.SQX_Finding__c = finding.finding.Id;
            Database.SaveResult result2 = Database.insert(sdFindingUpdate);
            
            // ACT: Update the Supplier Deviation and Finding Combo with existing Combo values
            sdFindingUpdate.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdFindingUpdate.SQX_Finding__c = finding.finding.Id;
            Database.SaveResult updateResult = Database.update(sdFindingUpdate, false);            
            
            // ASSERT: Ensure duplicate link is not created on update
            System.assert(!updateResult.isSuccess(), 'Expected update action to fail when Supplier Deviation and Finding combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(updateResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + updateResult.getErrors());
            
        }
    }
    
    /**
     * GIVEN: Supplier Deviation with NC linked
     * WHEN: Same NC record is attempted to link
     * THEN: NC record should not be linked
     * @Story: [SQX-6310]
     */
    
    public static testmethod void givenSupplierDeviationWithNCLinked_WhenSameNCIsAttemptedToLink_ThenNCShouldNotBeLinked(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            
            // Create Supplier Deviation
            SQX_Test_Supplier_Deviation supplierDeviation = new SQX_Test_Supplier_Deviation();
            supplierDeviation.save();
            
            SQX_Test_Supplier_Deviation supplierDeviation2 = new SQX_Test_Supplier_Deviation();
            supplierDeviation2.save();
            
            // Create NC
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.save();
            
            // ARRANGE: Link the created Supplier Deviation and NC
            SQX_Supplier_Deviation_NC__c sdNC = new SQX_Supplier_Deviation_NC__c();
            sdNC.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdNC.SQX_Nonconformance__c = nc.nc.Id;
            
            Database.SaveResult result = Database.insert(sdNC);
            
            // ACT: Link same Supplier Deviation and NC
            SQX_Supplier_Deviation_NC__c sdNC2 = new SQX_Supplier_Deviation_NC__c();
            sdNC2.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdNC2.SQX_Nonconformance__c = nc.nc.Id;
            Database.SaveResult duplicateInsertResult = Database.insert(sdNC2, false);
            
            // ASSERT: Ensure duplicate link is not created
            System.assert(!duplicateInsertResult.isSuccess(), 'Expected save action to fail when Supplier Deviation and NC combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(duplicateInsertResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + duplicateInsertResult.getErrors());
            
            // ARRANGE: Link the created Supplier Deviation and NC
            SQX_Supplier_Deviation_NC__c sdNCUpdate = new SQX_Supplier_Deviation_NC__c();
            sdNCUpdate.SQX_Supplier_Deviation__c = supplierDeviation2.sd.Id;
            sdNCUpdate.SQX_Nonconformance__c = nc.nc.Id;
            Database.SaveResult result2 = Database.insert(sdNCUpdate);
            
            // ACT: Update the Supplier Deviation and NC Combo with existing Combo values
            sdNCUpdate.SQX_Supplier_Deviation__c = supplierDeviation.sd.Id;
            sdNCUpdate.SQX_Nonconformance__c = nc.nc.Id;
            Database.SaveResult updateResult = Database.update(sdNCUpdate, false);
            
            // ASSERT: Ensure duplicate link is not created on update
            System.assert(!updateResult.isSuccess(), 'Expected update action to fail when Supplier Deviation and NC combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(updateResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + updateResult.getErrors());
        }
    }
}