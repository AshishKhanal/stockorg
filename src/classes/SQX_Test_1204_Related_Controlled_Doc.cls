/*
 * test for in related controlled document
 * author: Anish Shrestha
 * date: 2015/06/09
 * story: SQX-1204
 **/
@isTest
public class SQX_Test_1204_Related_Controlled_Doc{

    /*
    * A1 = Controlled Document 1
    * A2 = Controlled Doc 2
    * B1 = Referred Control Doc 1
    * B2 = Referred Control Doc 2
    */
    static boolean runAllTests = false,
                    run_givenUser_WhenControlledDocIsRevised_RelatedDocShouldBeCloned = true,
                    run_givenUser_WhenReferredDocIsRevisedAndSpecificRevIsFalse_RelatedDocShouldBeCloned = true,
                    run_givenUser_WhenReferredDocIsRevisedAndSpecificRevIsTrue_RelatedDocShouldNotBeCloned = true;
    /*
    Helper methods to fill in required field in controlled doc and get it ready for insertion
    */
    private static void setupControlledDoc(SQX_Controlled_Document__c doc){

        integer randomNumber = (Integer)( Math.random() * 1000000 );
        doc.Document_Number__c = 'Doc' + randomNumber;
        doc.Revision__c = 'REV-1';
        doc.Title__c = 'Document Title ' + randomNumber;
        doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
        doc.Document_Status__c = SQX_Controlled_Document.STATUS_DRAFT;
    }

    /*
    * helper method to insert related doc in the controlled doc
    */
    private static void setupRelatedDoc(Id docId1, Id docId2, Boolean specificRev){
        SQX_Related_Document__c relatedDoc = new SQX_Related_Document__c(
                                                    Controlled_Document__c = docId1,
                                                    Referenced_Document__c = docId2,
                                                    Specific_Rev__c = specificRev,
                                                    Comment__c = 'Random_Comments');
        new SQX_DB().op_insert(new List<SQX_Related_Document__c> { relatedDoc },  new List<Schema.SObjectField>{});
    }

    /**
    * When the revise the contolled doc with related document, related document should be copied.
    * @author Anish Shrestha
    * @date 2015/06/09
    * @story [SQX-1204]
    */ 
    public testMethod static void givenUser_WhenControlledDocIsRevised_RelatedDocShouldBeCloned(){

        if(!runAllTests && !run_givenUser_WhenControlledDocIsRevised_RelatedDocShouldBeCloned){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){ 
            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();

            SQX_Controlled_Document__c referredDoc = new SQX_Controlled_Document__c();
            ApexPages.StandardController referredcontroller = new ApexPages.StandardController(referredDoc);
            SQX_Extension_Controlled_Document referredextension = new SQX_Extension_Controlled_Document(referredcontroller);

            referredextension.file = Blob.valueOf('Hello Referred World');
            referredextension.fileName = 'HelloReferred.txt';
            setupControlledDoc(referredDoc);
            referredextension.create();
            
            setupRelatedDoc(doc.Id, referredDoc.Id, false);

            extension.Revise();

            List<SQX_Related_Document__c> newRelatedDoc = new List<SQX_Related_Document__c>();
            newRelatedDoc = [SELECT Id FROM SQX_Related_Document__c WHERE Controlled_Document__c =: doc.Id];

            System.assert(newRelatedDoc.size() == 1, 'When revising the controlled doc, related document should be copied');
        }
    }

    /**
    * When the revise the referred doc with related document and specific revision set true, related document should be added to thecontrolling doc.
    * @author Anish Shrestha
    * @date 2015/06/09
    * @story [SQX-1204]
    */ 
    public testMethod static void givenUser_WhenReferredDocIsRevisedAndSpecificRevIsFalse_RelatedDocShouldBeCloned(){

        if(!runAllTests && !run_givenUser_WhenReferredDocIsRevisedAndSpecificRevIsFalse_RelatedDocShouldBeCloned){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){ 
            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();

            SQX_Controlled_Document__c referredDoc = new SQX_Controlled_Document__c();
            ApexPages.StandardController referredcontroller = new ApexPages.StandardController(referredDoc);
            SQX_Extension_Controlled_Document referredextension = new SQX_Extension_Controlled_Document(referredcontroller);

            referredextension.file = Blob.valueOf('Hello Referred World');
            referredextension.fileName = 'HelloReferred.txt';
            setupControlledDoc(referredDoc);
            referredextension.create();
            
            setupRelatedDoc(doc.Id, referredDoc.Id, false);

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            referredDoc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            update referredDoc;

            referredextension.Revise();

            List<SQX_Related_Document__c> newRelatedDoc = new List<SQX_Related_Document__c>();
            newRelatedDoc = [SELECT Id FROM SQX_Related_Document__c WHERE Controlled_Document__c =: doc.Id];

            System.assert(newRelatedDoc.size() == 2, 'When revising the referring doc if the specific revision is false, related document should be copied to the controlling doc');
        }
    }

    /**
    * When the revise the referred doc with related document and specific revision set true, related document should be added to thecontrolling doc.
    * @author Anish Shrestha
    * @date 2015/06/09
    * @story [SQX-1204]
    */ 
    public testMethod static void givenUser_WhenReferredDocIsRevisedAndSpecificRevIsTrue_RelatedDocShouldNotBeCloned(){

        if(!runAllTests && !run_givenUser_WhenReferredDocIsRevisedAndSpecificRevIsTrue_RelatedDocShouldNotBeCloned){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){ 
            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();

            SQX_Controlled_Document__c referredDoc = new SQX_Controlled_Document__c();
            ApexPages.StandardController referredcontroller = new ApexPages.StandardController(referredDoc);
            SQX_Extension_Controlled_Document referredextension = new SQX_Extension_Controlled_Document(referredcontroller);

            referredextension.file = Blob.valueOf('Hello Referred World');
            referredextension.fileName = 'HelloReferred.txt';
            setupControlledDoc(referredDoc);
            referredextension.create();
            
            setupRelatedDoc(doc.Id, referredDoc.Id, true);

            referredextension.Revise();

            List<SQX_Related_Document__c> newRelatedDoc = new List<SQX_Related_Document__c>();
            newRelatedDoc = [SELECT Id FROM SQX_Related_Document__c WHERE Controlled_Document__c =: doc.Id];

            System.assert(newRelatedDoc.size() == 1, 'When revising the referring doc if the specific revision is true, related document should be copied to the controlling doc');
        }
    }



}