/**
 * Unit test for Supplier Escalation Submit and Initiate
 */
@isTest
public class SQX_Test_Sup_Escalation_Submit_Initiate {
    @testSetup
    public static void commonSetup() {
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(adminUser) {
            // create auto number record for supplier escalation
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Escalation',
                Next_Number__c = 1,
                Number_Format__c = 'ESC-{1}',
                Numeric_Format__c = '0000'
            );
        }
        System.runAs(standardUser) {
            // create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
       }
    }
       
    /**
     * Given : Supplier Escalation Record with two policy tasks of type Document Request and Task
     * When : Record is submitted 
     * Then : Two Supplier Escalation Step record with recor type Document Request and Task respectively are generated.
     * 
     * When : Record is initiated
     * Then : Record is open and Task for first step is generated
     */
    public static testMethod void ensureSubmitOfEscalationCreatesEscalationStepsAndInitiateOfEscalationCreatesTaskForOpenSteps(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser){
            //Arrange : Create current doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            doc.save();
            
            //Arrange: Get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            //Create policy task so that it will create step later on submitting escalation record
            List<SQX_Task__c> policyTasks = SQX_Test_Supplier_Escalation.createPolicyTasks(1, SQX_Task.TASK_TYPE_DOC_REQUEST, standardUser, 1);
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, standardUser, 2);
            policyTasks[0].SQX_Controlled_Document__c = doc.doc.Id;
            update policyTasks;
            
            //Arrange: Create Supplier Escalation record
            SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            //Act: submit the record
            supEsc.submit();
            List<SQX_Supplier_Escalation_Step__c> steps = [SELECT
                                                           Id,
                                                           Status__c,
                                                           SQX_Controlled_Document__c,
                                                           RecordType.DeveloperName,
                                                           Allowed_Days__c
                                                           FROM
                                                           SQX_Supplier_Escalation_Step__c
                                                           WHERE
                                                           SQX_Parent__c =: supEsc.supplierEscalation.Id];
            
            //Assert: Ensure two steps are created when record is submitted.
            System.assert(steps.size() == 2, 'Expected two steps to be created while submitting escalation as there are two policy tasks.');
            System.assertEquals(policyTasks.get(0).Allowed_Days__c, steps.get(0).Allowed_Days__c, 'Expected Allowed Days to be same but is different');
            Map<String, SQX_Supplier_Escalation_Step__c> recordTypeToStep = new Map<String, SQX_Supplier_Escalation_Step__c>();
            for (SQX_Supplier_Escalation_Step__c step : steps) {
                recordTypeToStep.put(step.RecordType.DeveloperName, step);
            }
            //Assert: Ensure Document Request type step should contain Controlled Document and should be Task type as null.
            System.assert(recordTypeToStep.get('Document_Request') != null && recordTypeToStep.get('Document_Request').SQX_Controlled_Document__c == doc.doc.Id, 'Expected Document Request type step should contain Controlled Document but is ' + recordTypeToStep.get('Document_Request').SQX_Controlled_Document__c);
            System.assert(recordTypeToStep.get('Task') != null, 'Expected Task type step to be created but is null');
            
            //Act: Initiate the record
            supEsc.initiate();
            
            steps = [SELECT
                     Status__c
                     FROM
                     SQX_Supplier_Escalation_Step__c
                     WHERE
                     SQX_Parent__c =: supEsc.supplierEscalation.Id];
            //Assert: Ensure Task Status should be changed to Open when record is initiated.
            System.assertEquals(steps[0].Status__c, 'Open', 'Task status doesn\'t changed to Open when Supplier Escalation is initiated');
        }
    }
}