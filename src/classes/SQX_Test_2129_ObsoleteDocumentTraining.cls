/**
* ensures pending document trainings are set to obsolete when a controlled document is retired
* ensures obsolete document training cannot be modified (including deletion)
*/
@isTest
public class SQX_Test_2129_ObsoleteDocumentTraining {
    @testSetup
    public static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        
        SQX_Job_Function__c jf1;
        SQX_Personnel__c p1, p2;
        
        System.runAs(admin1) {
            // add required job function
            jf1 = new SQX_Job_Function__c( Name = 'JF1');
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            jf.save();
        }
        
        System.runAs(user1) {
            // add required personnels
            p1 = new SQX_Test_Personnel().mainRecord;
            p2 = new SQX_Test_Personnel().mainRecord;
            p1.Full_Name__c = 'P1';
            p2.Full_Name__c = 'P2';
            p1.Active__c = true;
            p2.Active__c = true;
            insert new List<SQX_Personnel__c> { p1, p2 };
            
            // add required active personnel job function for p1
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true );
            insert pjf1;
        }
    }
    
    static Map<String, SQX_Personnel__c> getPersonnels() {
        List<SQX_Personnel__c> psns = [SELECT Id, Name, Full_Name__c, Identification_Number__c, SQX_User__c, Active__c FROM SQX_Personnel__c];
        
        Map<String, SQX_Personnel__c> psnMap = new Map<String, SQX_Personnel__c>();
        for (SQX_Personnel__c psn : psns) {
            psnMap.put(psn.Full_Name__c, psn);
        }
        
        return psnMap;
    }
    
    /**
    * ensures only pending document trainings are set to obsolete when a controlled document is retired
    */
    public static testmethod void givenRetireDocumentWithPendingTrainings_TrainingsStatusObsolete() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1'),
                            p2 = psns.get('P2');
        
        SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = 'JF1'];
            
        System.runas(user1) {
            // add required released controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            cDoc1.save();
            
            // add required requirement
            SQX_Requirement__c req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            insert req1;
            
            // get system generated training for p1
            SQX_Personnel_Document_Training__c dt1 = [SELECT Id, Status__c FROM SQX_Personnel_Document_Training__c
                                                      WHERE SQX_Personnel__c = :p1.Id AND SQX_Controlled_Document__c = :cDoc1.doc.Id];
            
            // add required pending document training for p2
            SQX_Personnel_Document_Training__c dt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p2.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = System.today()
            );
            // add required trainer approval pending document training for p2
            SQX_Personnel_Document_Training__c dt3 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p2.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING
            );
            // add required completed document training for p2
            SQX_Personnel_Document_Training__c dt4 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p2.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE
            );
            insert new List<SQX_Personnel_Document_Training__c> { dt2, dt3, dt4 };
            
            // ACT: retire document
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // get document trainings with Obsolete status
            Map<Id, SQX_Personnel_Document_Training__c> dts = new Map<Id, SQX_Personnel_Document_Training__c>(
                [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_OBSOLETE]
            );
            
            // expected only pending trainings to be obsolete
            System.assertEquals(2, dts.size());
            System.assertEquals(true, dts.containsKey(dt1.Id));
            System.assertEquals(true, dts.containsKey(dt2.Id));
        }
    }
    
    /**
    * ensures obsolete document training cannot be modified (including deletion)
    */
    public static testmethod void givenModifyObsoleteTraining_NotSavedOrDeleted() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');
            
        System.runas(user1) {
            // add required released controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            cDoc1.save();
            
            // add required pending document training
            SQX_Personnel_Document_Training__c dt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today()
            );
            insert dt1;
            
            // retire document
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // ACT: change obsolete training value
            dt1 = new SQX_Personnel_Document_Training__c(
                Id = dt1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY
            );
            Database.SaveResult sr1 = Database.update(dt1, false);
            
            String errMsg = 'You cannot modify completed or obsolete document training record.'; // validation rule Prevent_Modification_After_Completion error message
            System.assertEquals(false, sr1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(sr1.getErrors(), errMsg));
            
            
            // ACT: change obsolete training value
            dt1 = new SQX_Personnel_Document_Training__c(
                Id = dt1.Id,
                Due_Date__c = System.today() + 30
            );
            sr1 = Database.update(dt1, false);
            
            System.assertEquals(false, sr1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(sr1.getErrors(), errMsg));
            
            
            // ACT: change obsolete training value
            dt1 = new SQX_Personnel_Document_Training__c(
                Id = dt1.Id,
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE
            );
            sr1 = Database.update(dt1, false);
            
            System.assertEquals(false, sr1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(sr1.getErrors(), errMsg));
            
            
            // ACT: delete obsolete training
            Database.DeleteResult dr1 = Database.delete(dt1, false);
            
            System.assertEquals(false, dr1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(dr1.getErrors(), Label.SQX_PDT_ERR_MSG_CANNOT_DELETE_SIGNED_OFF_RECORD));
        }
    }
}