/**
 * Class provides specific details required for submission of a Canada Report
*/

public with sharing class SQX_Canada_Report_eSubmitter extends SQX_Regulatory_Report_eSubmitter {

    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * the desired actions(PDF generation and Insertion) for the given Canada Report Record
    */
    protected override String constructPayloadForSubmission(Id recordId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act, successAct;
        SQX_External_Service_Payload.Job job;
        SQX_External_Service_Payload.HTTP_Request_Detail httpRequestDetail;
        SQX_External_Service_Payload.JWTPrincipal principal;

        Export_To_PDF_Job_Detail pdfJobDetail;

        String recordJSON;

        String baseUrl, versionDataInsertionPath;

        payload = new SQX_External_Service_Payload();

        SObjectDataLoader.SerializeConfig configToUse = getSerializationConfig().translateLabels(); // we need labels in PDF not the values

        recordJSON = this.getRecordJSONFor(recordId, configToUse);

        // first action : PDF generation
        act = payload.createAction();

        // now adding additional data required for XML generation which is the record json
        pdfJobDetail = new Export_To_PDF_Job_Detail(recordJSON, getPDFTemplateName(recordId), getRecordName(recordId));

        job = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_PDF, null, pdfJobDetail);

        // second action : Add request to insert content to SF
        successAct = act.createSuccessAction();

        baseUrl = Url.getSalesforceBaseUrl().toExternalForm();

        principal = payload.getDefaultPrincipalForCurrentUser();

        ContentVersion cv = new ContentVersion(
            PathOnClient = getRecordName(recordId) + EXTENSION_PDF,
            CQ_Actions__c = String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { getRegReportIdFor(recordId) }),
            RecordTypeId = SQX_ContentVersion.getRegulatoryContentRecordType()
        );

        versionDataInsertionPath = baseUrl + '/services/data/v44.0/sobjects/ContentVersion/';

        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(versionDataInsertionPath, SQX_External_Service_Payload.HttpMethod.POST, JSON.serialize(cv), principal);

        successAct.addJob(SQX_External_Service_Payload.JobType.INSERT_CONTENT_TO_SF, new Integer[] { job.getIdentifier() }, httpRequestDetail);

        return payload.convertToJSONString();
    }


    private SQX_Canada_Report__c recordDetailCache = null;

    /**
     * Canada Report record query
    */
    protected override SObject getRecordDetail(String recordId) {
        if(recordDetailCache == null) {
            recordDetailCache = [SELECT Name, SQX_Regulatory_Report__c FROM SQX_Canada_Report__c WHERE Id =: recordId];
        }
        return recordDetailCache;
    }


    /**
     * Return true if the reg record supports XML Validation else return false
    */
    public override Boolean supportsXMLValidation() {
        return false;
    }




    /**
     * Trigger handler for Canada Report trigger actions
    */
    public with sharing class SQX_Canada_Report_Trigger_Handler extends SQX_Trigger_Handler {

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newValues equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public SQX_Canada_Report_Trigger_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
            super(newValues, oldMap);
        }

        /**
        * Trigger methods:
        * a. After Update [setSubmissionDateOnSubmit]: Method sets the submission date on Canada Report when the report is submitted
        */
        protected override void onBefore() {
            setSubmissionDateOnSubmit();
        }


        /**
        * Trigger methods:
        * a. After Insert/Update [clearActivityCode]: clears out activity code field value
        */
        protected override void onAfter() {
            clearActivityCode();
        }


        /**
         * Method sets the submission date on Canada Report when the report is submitted
         */
        private void setSubmissionDateOnSubmit() {

            Rule recordsBeingSubmitted = new Rule();
            recordsBeingSubmitted.addRule(SQX_Canada_Report__c.Activity_Code__c, RuleOperator.NotEquals, SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_SUBMIT);

            this.resetView()
                .applyFilter(recordsBeingSubmitted, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(this.view.size() > 0){
                for(SQX_Canada_Report__c report : (List<SQX_Canada_Report__c>) this.view) {
                    report.Date_Submitted__c = Date.today();
                }
            }
        }


        /**
         * Method clears out activity code field value
        */
        private void clearActivityCode() {

            final String ACTION_NAME = 'clearActivityCode';

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_Canada_Report__c.Activity_Code__c, RuleOperator.NotEquals, null);

            String objName = this.getObjName();

            this.resetView()
                .removeProcessedRecordsFor(objName, ACTION_NAME)
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Canada_Report__c> recordsToUpdate = new List<SQX_Canada_Report__c>();

            if(this.view.size() > 0){
                this.addToProcessedRecordsFor(objName, ACTION_NAME, this.view);

                for(SQX_Canada_Report__c record : (List<SQX_Canada_Report__c>) this.view) {
                    SQX_Canada_Report__c recordToUpdate = new SQX_Canada_Report__c(Id = record.Id);
                    recordToUpdate.Activity_Code__c = null;
                    recordsToUpdate.add(recordToUpdate);
                }

                new SQX_DB().op_update(recordsToUpdate, new List<SObjectField> { SQX_Canada_Report__c.Activity_Code__c });
            }

        }
    }

}