/**
 *  Implementation class for General Report Submitter type
*/

public with sharing class SQX_General_Report_eSubmitter extends SQX_Regulatory_Report_eSubmitter {

    /**
     * NOT SUPPORTED FOR GENERAL(MANUAL SUBMISSION) REPORT TYPES
    */
    protected override String constructPayloadForSubmission(Id recordId) {
        throw new SQX_ApplicationGenericException('Submission Not Supported');
    }

    private SQX_General_Report__c recordDetailCache = null;

    /**
     * General Report record query
    */
    protected override SObject getRecordDetail(String recordId) {
        if(recordDetailCache == null) {
            recordDetailCache = [SELECT Name, SQX_Regulatory_Report__c FROM SQX_General_Report__c WHERE Id =: recordId];
        }
        return recordDetailCache;
    }
}