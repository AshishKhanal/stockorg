public with sharing class SQX_PostInstall_Processor implements InstallHandler {
    public void onInstall(InstallContext context) {
        integer batchSize;
        Id batchProcessId;
        
        if (context.isUpgrade()) {
            // execute scripts while upgrading previous versions between 3.0 and 5.0
            if (context.previousVersion().compareTo(new Version(3, 0)) >= 0
                && context.previousVersion().compareTo(new Version(5, 0)) < 0) {
                
                // personnel document migration
                batchSize = 10;
                SQX_DocumentTrainingMigration_Processor processor = new SQX_DocumentTrainingMigration_Processor();
                batchProcessId = Database.executeBatch(processor, batchSize);
                System.debug('SQX_DocumentTrainingMigration_Processor migration batch process ID: ' + batchProcessId);
            }
            
            // execute scripts while upgrading previous versions between 5.0 and 6.1
            if (context.previousVersion().compareTo(new Version(5, 0)) >= 0
                && context.previousVersion().compareTo(new Version(6, 1)) < 0) {
                
                // process approval pending inclusion items of approval pending responses
                batchSize = 200;
                SQX_6_1_MigrateObsoleteDocumentTrainings obsoleteDocTrainingsProcessor = new SQX_6_1_MigrateObsoleteDocumentTrainings();
                batchProcessId = Database.executeBatch(obsoleteDocTrainingsProcessor, batchSize);
                System.debug('SQX_6_1_MigrateObsoleteDocumentTrainings migration batch process ID: ' + batchProcessId);
            }
            
            // execute scripts while upgrading previous versions earlier than 6.1
            if (context.previousVersion().compareTo(new Version(6, 1)) < 0) {
                
                // process approval pending inclusion items of approval pending responses
                batchSize = 100;
                SQX_6_1_MigrateInApprovalResponseItems inclusionsProcessor = new SQX_6_1_MigrateInApprovalResponseItems();
                inclusionsProcessor.setRecallComment('Post upgrade process using SQX_6_1_MigrateInApprovalResponseItems');
                batchProcessId = Database.executeBatch(inclusionsProcessor, batchSize);
                System.debug('SQX_6_1_MigrateInApprovalResponseItems migration batch process ID: ' + batchProcessId);
                
                // process action items to sync owner with asignee
                SQX_6_1_MigrateActions actionsProcessor = new SQX_6_1_MigrateActions();
                batchProcessId = Database.executeBatch(actionsProcessor, batchSize);
                System.debug('SQX_6_1_MigrateActions migration batch process ID: ' + batchProcessId);
            }

            // execute scripts while upgrading previous versions earlier than 6.2
            if (context.previousVersion().compareTo(new Version(6,2)) < 0) {
                
                // process all NC CAPA and set their uniqueness constraint correctly
                batchSize = 200;
                SQX_6_2_MigrateRelatedCapasOfNC relatedCapaProcessor = new SQX_6_2_MigrateRelatedCapasOfNC();
                batchProcessId = Database.executeBatch(relatedCapaProcessor,batchSize);
                System.debug('SQX_6_2_MigrateRelatedCapasOfNC migration batch process ID: ' + batchProcessId);
            }

            // execute scripts while upgrading previous versions earlier than 6.3
            if(context.previousVersion().compareTo(new Version(6,3)) < 0){

                // process all pre-release controlled document and set their training link correctly.
                batchSize = 5;
                SQX_6_3_CopyTrainingLinkIdToDocument trainingLinkProcessor = new SQX_6_3_CopyTrainingLinkIdToDocument();
                batchProcessId = Database.executeBatch(trainingLinkProcessor, batchSize);
                System.debug('SQX_6_3_CopyTrainingLinkIdToDocument migration batch process ID: ' + batchProcessId);
            }

            // execute scripts while upgrading previous versions earlier than 6.4
            if (context.previousVersion().compareTo(new Version(6,4)) < 0) {
                
                // process all Audit criteria and insert the equivalent controlled document of record type 'audit_criteria'
                batchSize = 10;
                SQX_6_4_Criteria_Document_Migration criteriaProcessor = new SQX_6_4_Criteria_Document_Migration();
                batchProcessId = Database.executeBatch(criteriaProcessor,batchSize);
                System.debug('SQX_6_4_Criteria_Document_Migration migration batch process ID: ' + batchProcessId);
            }

            // execute scripts while upgrading previous versions earlier than 7.0
            if (context.previousVersion().compareTo(new Version(7,0)) < 0) {
                batchSize = 200;
                SQX_7_0_Record_Activity_Migration recordActivityProcessor = new SQX_7_0_Record_Activity_Migration();
                batchProcessId = Database.executeBatch(recordActivityProcessor,batchSize);
                System.debug('SQX_7_0_Record_Activity_Migration migration batch process ID: ' + batchProcessId);
            }

            // execute scripts while upgrading previous versions earlier than 9.0
            if (context.previousVersion().compareTo(new Version(9,0)) < 0) {
                batchSize = 200;
                SQX_9_0_Active_CQ_Task_Migration cqTaskProcessor = new SQX_9_0_Active_CQ_Task_Migration();
                batchProcessId = Database.executeBatch(cqTaskProcessor,batchSize);
                System.debug('SQX_9_0_Active_CQ_Task_Migration migration batch process ID: ' + batchProcessId);
            }
            
            // execute scripts while upgrading previous versions earlier than 8.2
            if (context.previousVersion().compareTo(new Version(8,2)) < 0) {
                batchSize = 200;
                SQX_8_2_Update_ContentDocId_Migration updateStepsContentDocID = new SQX_8_2_Update_ContentDocId_Migration();
                batchProcessId = Database.executeBatch(updateStepsContentDocID,batchSize);
                System.debug('SQX_8_2_Update_ContentDocId_Migration migration batch process ID: ' + batchProcessId);
            }
        }
    }
}