/**
 *  Test class for SCORM features
*/
@isTest
public class SQX_Test_3681_Scorm {

    @testSetup
    public static void commonSetup(){
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser){

            /******************************* Creating Training record ***********************************/

            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.mainRecord.SQX_User__c = standardUser.Id;
            personnel.save();

            
            SQX_Test_Controlled_Document tDoc1 = new SQX_Test_Controlled_Document();
            tDoc1.doc.Is_Scorm_Content__c = true;
            tDoc1.doc.Document_Number__c = 'CDOC1';
            tDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Test_Controlled_Document tDoc2 = new SQX_Test_Controlled_Document();
            tDoc2.doc.Is_Scorm_Content__c = true;
            tDoc2.doc.Document_Number__c = 'CDOC2';
            tDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = tDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Optional__c = false
            );
            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = tDoc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Optional__c = false
            );
            new SQX_DB().op_insert(new List<SObject> { pdt1, pdt2 }, new List<Schema.SObjectField>
                                                                {   Schema.SQX_Personnel_Document_Training__c.SQX_Personnel__c,
                                                                    Schema.SQX_Personnel_Document_Training__c.SQX_Controlled_Document__c,
                                                                    Schema.SQX_Personnel_Document_Training__c.Level_of_Competency__c,
                                                                    Schema.SQX_Personnel_Document_Training__c.Due_Date__c,
                                                                    Schema.SQX_Personnel_Document_Training__c.Completion_Date__c,
                                                                    Schema.SQX_Personnel_Document_Training__c.Optional__c
                                                                });
        }
    }

    /* helper method to get controlled document of the given doc number*/
    static SQX_Test_Controlled_Document getDocument(String docNum){
        SQX_Test_Controlled_Document tDoc = new SQX_Test_Controlled_Document();
        tDoc.doc = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Number__c =: docNum];
        return tDoc.synchronize();
    }

    /* helper method to get pdt of the given doc number*/
    static SQX_Personnel_Document_Training__c getPDT(String docNum){
        return [SELECT Id, Name, Personnel_Name__c, Document_Title__c FROM SQX_Personnel_Document_Training__c WHERE Document_Number__c =: docNum LIMIT 1];
    }

    /**
    *   PREVIEW SERVICE
    *   Given : Controlled Document With Scorm Content
    *   When : Content Preview is requested
    *   Then : Desired url is returned
    */
    static testmethod void givenControlledDocumentWithScormContent_WhenContentPreviewIsRequested_ThenDesiredUrlIsRetrieved(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')){

            // Arrange : create a controlled document with scorm content
            SQX_Test_Controlled_Document tDoc = getDocument('CDOC2');

            PageReference expectedPage = Page.SQX_eContent;
            expectedPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, tDoc.doc.Id);

            // Act : request preview url
            Test.startTest();

            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(tDoc.doc));

            PageReference pr = ext.getContentPage();

            Test.stopTest();

            // Assert : Preview url is same
            System.assertEquals(expectedPage.getURL(), pr.getURL());
        }
    }


    
    /**
     * GIVEN : personnel and control doc 
     *       i)without scrom content 
     *       ii)With scrom content and 
     *       PDT with or without scrom content and its level of Competency as 'Read and Exhibit Competency'	
     * WHEN : User try to trainner signoff 
     * THEN : IF user have scrom content and its pending than courseid and other variable is set other wise its set to null
     * @story : [SQX-8048]
     */
    @isTest
    public static void givenControlledDocumentWithOrWithOutScormContent_WhenUserTrainerSignOff_ThenEnsureCourserOrContentIdIsSet(){
  
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runas(standardUser) {
            //Arrange: create a personnel 
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.mainRecord.SQX_User__c = standardUser.Id;
            personnel.save();
            
            //Creating Controlled Doc1
            SQX_Test_Controlled_Document tDoc1 = new SQX_Test_Controlled_Document();
            tDoc1.doc.Is_Scorm_Content__c = false;
            tDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            //Creating Controlled Doc2
            SQX_Test_Controlled_Document tDoc2 = new SQX_Test_Controlled_Document();
            tDoc2.doc.Is_Scorm_Content__c = true;
            tDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            //Creating  pdt1
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = tDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Optional__c = false
            );
            Database.insert(pdt1, false);
            //Creating  pdt1
            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = tDoc2.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Due_Date__c = System.today(),
                Completion_Date__c = System.today(),
                Optional__c = false
            );
            Database.insert(pdt2, false);

            //gone through the controller content without scrom content 
            SQX_Controller_eContent con=new SQX_Controller_eContent(pdt1.id);
            //Assert:couser id and content id is not set as per scrom content is not define
            System.assertEquals(null,con.CourseId,'course  id is null per scrom content is not define');
            System.assertEquals(null,con.ContentId,'ContentId  id is null as scrom content is not define');

            SQX_Controller_eContent con1=new SQX_Controller_eContent(pdt2.id);
            //Assert: course id and record id is set when scrom content is define
            System.assertEquals(pdt2.Id,con1.CourseId,'course  id is  set as per scrom content is true');
            System.assertEquals(pdt2.Id,con1.recordId,'recordId  id is  set as per scrom content is true');
       }
    }

    /**
    *   LAUNCH SERVICE
    *   Given : Document Training On SCORM Content
    *   When : Content is launched and completed
    *   Then : Training is completed
    */
    static testmethod void givenDocumentTrainingWithScormContent_WhenContentIsLaunchedAndCompleted_SignOffIsCompleted(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('standardUser')){

            // Arrange : create a document training on scorm content
            SQX_Personnel_Document_Training__c pdt = getPDT('CDOC2');

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pdt.Id);

            Test.setCurrentPage(launchPage);

            // Act : content is completed
            Test.startTest();

            SQX_Controller_eContent ct = new SQX_Controller_eContent();

            ct.signOff();

            Test.stopTest();
            
            // Assert : Training is signed off
            pdt = [SELECT Status__c FROM SQX_Personnel_Document_Training__c WHERE Id =: pdt.Id];

            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdt.Status__c);
        }
    }

    /**
    *   Given : Document Training On SCORM Content for User A
    *   When : User B tries to launch content
    *   Then : Error is thrown
    */
    static testmethod void givenDocumentTrainingWithScormContent_WhenSomeOtherUserTriesToLaunchTheContent_ErrorIsThrown(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){

            // Arrange : create a document training on scorm content
            SQX_Personnel_Document_Training__c pdt = getPDT('CDOC2');

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pdt.Id);

            // Act : Get page
            Test.setCurrentPage(launchPage);
     
            SQX_Controller_eContent ct = new SQX_Controller_eContent();
 
            // Assert : Expected invalid access error
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD), 'Expected error message : ' + Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD);
        }
    }

    /**
     * This testmethod makes use of esig policies and validates sign off
     * @story [SQX-6188]
     */
    static testmethod void givenVariousEsigPolicies_TheSignOffWorksAsExpected(){

        SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
        cqES.Consumer_Key__c = 'Mock';
        cqES.Consumer_Secret__c = 'Mock';
        cqES.Enabled__c = true;
        cqES.Ask_Username__c = true;
        Database.insert(cqES, true);


        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');

        System.runAs(standardUser) {

            // Arrange : create a document training on scorm content
            SQX_Personnel_Document_Training__c pdt = getPDT('CDOC2');

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pdt.Id);

            Test.setCurrentPage(launchPage);

            Test.setMock(HttpCalloutMock.class, new MockAuthClass());

            Test.startTest();

            // Act : Sign off with correct password
            SQX_Controller_eContent ct = new SQX_Controller_eContent();
            ct.SigningOffUsername = standardUser.username;
            ct.SigningOffPassword = 'rightPassword';

            ct.signOff();

            pdt = [SELECT Status__c FROM SQX_Personnel_Document_Training__c WHERE Id =: pdt.Id];

            // Assert : sign off should be successful
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdt.Status__c);

            Test.stopTest();
        }
    }

    /**
     * Mock class to handle esig validation
     */
    public class MockAuthClass implements HttpCalloutMock {
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(200);
            
            String url = req.getEndpoint();
            if(url.contains('rightPassword')){
                response.setBody('{"id":"allcorrect"}');
            }
            else{
                response.setBody('{"error":"incorrect password"}');
            }
            return response;
        }
    }

}