/**
* class to perform callout to the google spreadsheet and drive and return corresponding response from the google api
*/
public class SQX_GSheet {
    
    /***************************** Varible Defination for Audit Spreadsheet *************************/
    final static String GSHEET_HOST = 'callout:' + SQX_Custom_Settings_Public__c.getInstance().Google_Sheet_Named_Credentials__c,
                        GDRIVE_HOST = 'callout:' + SQX_Custom_Settings_Public__c.getInstance().Google_Drive_Named_Credentials__c,
                        SHEET_MIMETYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        HTTP_GET = 'GET', 
                        HTTP_POST = 'POST',
                        HTTP_PUT = 'PUT',
                        BATCH_SUFFIX = ':batchUpdate',
                        VALUES_SUFFIX = 'values:batchUpdate',
                        DRIVE_COPY_PREFIX = '/v3/files/',
                        DRIVE_COPY_SUFFIX = '/copy',
                        GET_SUFFIX = 'values:batchGet?',
                        POST_SUFFIX = 'values:batchUpdate?';

    
    public final static Integer HTTP_ERROR_STATUS = 404, 
                                HTTP_CONTINUE_STATUS = 202,
                                HTTP_OK_STATUS = 200,
                                HTTP_EMPTY_RESPONSE_STATUS = 204;

    private final static Integer REQ_POST_COPY_SPREADSHEET = 10,
                          REQ_GET_NAMED_RANGES = 20,
                          REQ_GET_NAMED_RANGES_VALUES = 30,
                          REQ_POST_VALUES_TO_SHEET = 40,
                          REQ_CLONE_TEMPLATE = 50,
                          REQ_POST_BATCH_VALUES_TO_SHEET = 60,
                          REQ_GET_VALUES = 70,
                          REQ_DELETE_SPREADSHEET = 80,
                          REQ_DOWNLOAD_SPREADSHEET = 90,
                          REQ_UPLOAD_SPREADSHEET = 100,
                          REQ_UPLOAD_METADATA_SPREADSHEET = 110;
        
    public String confinedRange = null,
                    session_uri = null,
                    sBody = null;
 
    public Blob blBody = null;

    public String sheetId { get; set; }   
    public String templateId { get; set; }

    public String [] i_ranges;


    /**
    * @description: constructer to set the tempate id of the audit spreadsheet
    */
    public SQX_GSheet(){
        SQX_Custom_Settings_Public__c customSettings = SQX_Custom_Settings_Public__c.getInstance();
        String templateId = customSettings.Audit_Template_Spreadsheet_Id__c;

        this.templateId = templateId;
    }

    /**
    * @description: constructer to set sheet id and template id of the spreadsheet
    */
    public SQX_GSheet(String sheetId, String templateId){
        this.sheetId = sheetId;
        this.templateId = templateId;
    }

    /**
    * @description: get the namedranges of the template spreadsheet from sheets
    */
    public GSheet_Response getTemplateRanges(){
        if(String.isBlank(this.templateId)){
            throw new SQX_ApplicationGenericException('Template Id hasn\'t been set');
        }
        return new RequestBuilder(this,REQ_GET_NAMED_RANGES, this.templateId).execute();
    }

    /**
    * @description: gets the namedranges of the copied spreadsheet containing values
    */
    public GSheet_Response getSheetRanges(){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Sheet Id hasn\'t been set');
        }
        return new RequestBuilder(this,REQ_GET_NAMED_RANGES, this.sheetId).execute();
    }

    /**
    * @description: gets the values from the sheets
    */
    public GSheet_Response getSheetValues(String[] arrayOfNamedRangesForSheetValues){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Spreadsheet has not been created');
        }
        i_ranges = arrayOfNamedRangesForSheetValues;
        return new RequestBuilder(this,REQ_GET_NAMED_RANGES_VALUES, this.sheetId).execute();
    }

    /**
    * @description: clones the tempalate spreadsheet to new sheet
    */
    public GSheet_Response cloneTemplate(String name){
        if(String.isBlank(this.templateId)){
            throw new SQX_ApplicationGenericException('Template Id hasn\'t been set');
        }
        Map<String,Object> copyRequest = new Map<String,Object>{
            'name' => name
        };
        sBody = JSON.serialize(copyRequest);
        return new RequestBuilder(this,REQ_CLONE_TEMPLATE, this.templateId).execute();
    }

    /**
    * @description: copies values from salesforce to google spreadsheet
    */
    public GSheet_Response postSheetValues(String jsonBody){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Spreadsheet has not been created');
        }
        sBody = jsonBody;
        return new RequestBuilder(this,REQ_POST_VALUES_TO_SHEET, this.sheetId).execute();
    }

    /**
    * @description: copies mass values from salesforce to google spreadsheet
    */
    public GSheet_Response postRequest(String jsonBody){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Spreadsheet has not been created');
        }
        sBody = jsonBody;
        return new RequestBuilder(this,REQ_POST_BATCH_VALUES_TO_SHEET, this.sheetId).execute();
    }
    
    /**
    * @description: deletes spreadsheet of the given id
    */
    public GSheet_Response deleteSpreadSheet(){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Spreadsheet has not been created');
        }        
        return new RequestBuilder(this,REQ_DELETE_SPREADSHEET, this.sheetId).execute();
    }

   /**
    *   @description : downloads sheet from google drive
    *   @param {String} fileId - spreadsheet id 
    *   @return {GSheet_Resonse} 
    */
    public GSheet_Response download(){
        if(String.isBlank(this.sheetId)){
            throw new SQX_ApplicationGenericException('Spreadsheet has not been created');
        }
        return new RequestBuilder(this,REQ_DOWNLOAD_SPREADSHEET, this.sheetId).execute();           
    }
    
    /**
     *  @description : uploads the file to Google drive
    */
    public GSheet_Response upload(Blob fileBody){
        
        // first callout to send the metadata
        // creating metadata
        Map<String, Object> sBodyMap = new Map<String,Object>{
            'name' => 'TEMP',
            'mimeType' => 'application/vnd.google-apps.spreadsheet'
        };
        sBody = JSON.serialize(sBodyMap);  
        HttpResponse response = new RequestBuilder(this,REQ_UPLOAD_METADATA_SPREADSHEET,'').executeMetaDataRequest();
        session_uri = response.getHeader('Location');
        blBody = fileBody;
        return new RequestBuilder(this,REQ_UPLOAD_SPREADSHEET,'').execute();
    }
    /**
     * @description: class for creating the request to send to google api
     */
    private class RequestBuilder {

        private HttpRequest request {get; private set;}
        private Boolean response_isBlob = false,
                        request_isBlob = false;
        SQX_GSheet outerClass;
        /**
         * @description: constructor to create the request to send to google api
         */
        private RequestBuilder(SQX_GSheet outerClass, Integer type, String sheetId){
            this.request = new HttpRequest();
            this.outerClass = outerClass;
            String url, 
                    method, 
                    contentType = 'application/json', 
                    majorDimension = null,
                    ranges = null,
                    valueRenderOption = null;

            if(type == SQX_GSheet.REQ_GET_NAMED_RANGES){
                url = new PageReference(GSHEET_HOST + '/' + sheetId).getUrl();
                method = HTTP_GET;
            }
            else if(type == SQX_GSheet.REQ_GET_NAMED_RANGES_VALUES){
                PageReference pagRef = new PageReference(GSHEET_HOST + '/' + sheetId + '/' + GET_SUFFIX);
                pagRef.getParameters().put('majorDimension', 'ROWS');
                pagRef.getParameters().put('valueRenderOption', 'FORMATTED_VALUE');
                String rangeURL = '';
                for(String range : outerClass.i_ranges){
                    rangeURL = rangeURL + '&ranges=' + EncodingUtil.urlEncode(range, 'UTF-8');
                }
                url = pagRef.getUrl() + rangeURL;
                method = HTTP_GET;
                //System.assert(false, url);
            }
            else if(type == SQX_GSheet.REQ_CLONE_TEMPLATE)
            {
                url = GDRIVE_HOST + '/drive' + DRIVE_COPY_PREFIX + sheetId + DRIVE_COPY_SUFFIX;
                method = HTTP_POST; 
            }
            else if(type == SQX_GSheet.REQ_DELETE_SPREADSHEET)
            {
                url = GDRIVE_HOST + '/drive' + DRIVE_COPY_PREFIX +  sheetId;
                method = 'DELETE';
            }
            else if(type == SQX_GSheet.REQ_GET_VALUES)
            {
                PageReference pagRef = new PageReference(GDRIVE_HOST + '/drive/' + sheetId + '/' + GET_SUFFIX); 
                pagRef.getParameters().put('valueRenderOption', 'FORMULA');
                ranges = '&ranges=' + EncodingUtil.urlEncode(outerClass.confinedRange, 'UTF-8');
                url = pagRef.getURL() + ranges;
                method = HTTP_GET;
            }
            else if(type == SQX_GSheet.REQ_POST_VALUES_TO_SHEET)
            {
                url = new PageReference(GSHEET_HOST + '/' + sheetId + '/' + POST_SUFFIX).getUrl();
                method = HTTP_POST;
            }
            else if(type == SQX_GSheet.REQ_POST_BATCH_VALUES_TO_SHEET)
            {
                url = GSHEET_HOST + '/' + sheetId + BATCH_SUFFIX;
                method = HTTP_POST;
            }
            else if(type == SQX_GSheet.REQ_DOWNLOAD_SPREADSHEET)
            {
                PageReference pagRef = new PageReference(GDRIVE_HOST + '/drive/v3/files/' + sheetId + '/export');                 
                pagRef.getParameters().put('mimeType', SHEET_MIMETYPE);
                url = pagRef.getUrl();                         
                method = HTTP_GET;
                contentType = null;    
                response_isBlob = true;
            }
            else if(type == SQX_GSheet.REQ_UPLOAD_METADATA_SPREADSHEET)
            {
                url = GDRIVE_HOST + '/upload/drive/v3/files?uploadType=resumable';
                request.setHeader('X-Upload-Content-Type',SHEET_MIMETYPE);
                method = HTTP_POST;
            }
            else if(type == SQX_GSheet.REQ_UPLOAD_SPREADSHEET)
            {
                url = outerClass.session_uri;
                contentType = SHEET_MIMETYPE;
                method = HTTP_PUT;
                request_isBlob = true;
            }
            else{
                throw new SQX_ApplicationGenericException('Unsupported request type');
            }

            request.setMethod(method);
            request.setEndpoint(url);
            if(contentType != null)
                request.setHeader('Content-type', contentType);
            if(outerClass.sBody != null || outerClass.blBody != null) {
                if(request_isBlob)
                    request.setBodyasBlob(outerClass.blBody);
                else    
                    request.setBody(outerClass.sBody);
            }
        }
        
        /*
        *   @description : executes the meta-data upload request and returns the httpresponse
        */
        private HttpResponse executeMetaDataRequest(){
            Http http = new Http();
            HttpResponse response = http.send(request);
            if(response.getStatusCode() != HTTP_ERROR_STATUS)
                return response;
            return null;
        }

        /**
         * @description: executes the request build from the request builder
         */
        private GSheet_Response execute(){
            Http http = new Http();
            HttpResponse response = http.send(request);

            if(response_isBlob)
                return new GSheet_Response().setResponse(request.getEndPoint(), response.getStatusCode(), response.getBodyAsBlob());    
            else
                return new GSheet_Response().setResponse(request.getEndPoint(), response.getStatusCode(), response.getBody());
        }
    }   
    
    /**
     * @description: class to create response received for the google api
     */
    public class GSheet_Response {
        public String Url {get; set;}
        public Integer Code {get; set;}
        public String rBody {get; set;}
        public Blob bBody {get; set;}
        public Boolean hasError {get  { return this.Code == HTTP_ERROR_STATUS; } }
        
        public GSheet_Response setResponse(String url, Integer code, String body){
            this.Url = url;
            this.Code = code;
            this.rBody = body;
            return this;
        }
        public GSheet_Response setResponse(String url, Integer code, Blob body){
            this.Url = url;
            this.Code = code;
            this.bBody = body;
            return this;            
        }
        
    } 
}