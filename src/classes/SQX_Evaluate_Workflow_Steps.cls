/**
 * class to evaluate steps for workflow policy
 */
global with sharing class SQX_Evaluate_Workflow_Steps {
    
    /**
     * method to evaluate steps and workflow status of the parent record
     * @param parentIds main records for which parent steps and workflow status is to be evaulated 
     */
    @InvocableMethod(Label='CQ Evaluate Workflow Steps')
    global static void evaluateWorkflowSteps(List<Id> parentIds){
        
        SQX_Step_Evaluation_Engine stepEvaluationEngine = new SQX_Step_Evaluation_Engine();
        stepEvaluationEngine.evaluateSteps(parentIds);
    }
}