/**
* this test case for assignee user want to complete the task which added by the other user
*/
@isTest
public class SQX_Test_6924_Cross_Privilege {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
    }
    
    /**
    * GIVEN : Onboarding step record
    * WHEN : assignee complete the task(step)
    * THEN : successfully completed
    */
    public static testMethod void givenOnboardingStep_WhenAssigneeCompleteTheTask_ThenCompleteTheStep(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        
        SQX_Test_NSI nsi;
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, SQX_Task.TASK_TYPE_PERFORM_AUDIT, assigneeUser, 1);
        }
        System.runAs(standardUser){

            // ARRANGE : NSI is created
            nsi = new SQX_Test_NSI().save();
            
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();
            
            nsi.initiate();
            
            System.assertEquals(SQX_NSI.WORKFLOW_STATUS_IN_PROGRESS, [SELECT Id, WOrkflow_Status__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id].Workflow_Status__c);

        }
        System.runAs(assigneeUser){
            // ACT : NSI On boarding steps are completed
            SQX_Onboarding_Step__c wfPolicy = new SQX_Onboarding_Step__c();
            wfPolicy = [SELECT Id, Status__c, Step__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id];
            
            SQX_Test_Audit audit = new SQX_Test_Audit().save();
            audit.audit.SQX_Auditee_Contact__c= assigneeUser.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            
            wfPolicy.SQX_Audit_Number__c = audit.audit.Id;
            wfPolicy.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            wfPolicy.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            wfPolicy.Comment__c = 'Test_Comment';
           
            //Assert: Ensured that task was successfully completed
            List<Database.SaveResult> result = new SQX_DB().withoutSharing().continueOnError().op_update(new List<SQX_Onboarding_Step__c> {wfPolicy}, new List<SObjectField>{});
            System.assertEquals(true,result[0].isSuccess());
        } 
    }
}