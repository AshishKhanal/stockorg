/**
 * This class provides invocable method which is invoked from process builder to create Script Attribute.
 */
public with sharing class SQX_Create_Script_Attribute {
    
    /**
     * This method creates regulatory report for Decision Trees which have been run.
     * @param scriptExecutions collection of Script Execution for which Script Attributes need to be created.
     */
    @InvocableMethod(label='Create Script Attribute')
    public static void createScriptAttributeRecordBasedOnAnswerOptionAttribute(List<SQX_Script_Execution__c> scriptExecutions) {
        
        Set<Id> scriptExecutionIds = new Set<Id>();
        List<Id> answerOptionIds = new List<Id>();
        Map<String, SQX_Script_Attribute__c> scriptAttributeMap = new Map<String, SQX_Script_Attribute__c>();
        
        for(SQX_Script_Execution__c scriptExecution : scriptExecutions) {
            scriptExecutionIds.add(scriptExecution.Id);
        }

        List<SQX_Script_Execution__c> scriptExecutionsToProcess = [SELECT   Id,
                                                                            SQX_Complaint__c, 
                                                                            (SELECT Id, 
                                                                                    SQX_Answer_Option__c 
                                                                                FROM SQX_Script_Responses__r ORDER BY Response_Sequence__c ASC) 
                                                                    FROM SQX_Script_Execution__c 
                                                                    WHERE Id IN : scriptExecutionIds];
        for (SQX_Script_Execution__c scriptExecution : scriptExecutionsToProcess) {
            for (SQX_Script_Response__c scriptResponse : scriptExecution.SQX_Script_Responses__r) {
                answerOptionIds.add(scriptResponse.SQX_Answer_Option__c);
            }
        }
        
        Map<Id, SQX_Answer_Option__c> answerOptionsMap = new Map<Id, SQX_Answer_Option__c>([SELECT Id, 
                                                                                                    (SELECT Id,Name,
                                                                                                        Attribute_Type__c,
                                                                                                        Due_In_Days__c,
                                                                                                        Report_Name__c,
                                                                                                        Reportable__c,
                                                                                                        Reg_Body__c,
                                                                                                        SQX_Answer_Option__c,
                                                                                                        Value__c
                                                                                                     FROM SQX_Answer_Option_Attributes__r) 
                                                                                            FROM SQX_Answer_Option__c WHERE Id IN : answerOptionIds]);

        for (SQX_Script_Execution__c scriptExecution : scriptExecutionsToProcess) {
            for (SQX_Script_Response__c scriptResponse : scriptExecution.SQX_Script_Responses__r) {
                // for answer type input text or input date/time, answer option is null
                if (answerOptionsMap.get(scriptResponse.SQX_Answer_Option__c) != null) {
                    for (SQX_Answer_Option_Attribute__c answerOptionAttribute : answerOptionsMap.get(scriptResponse.SQX_Answer_Option__c).SQX_Answer_Option_Attributes__r) {
                        SQX_Script_Attribute__c scriptAttribute = new SQX_Script_Attribute__c(
                                                                        Name = answerOptionAttribute.Name,
                                                                        Value__c = answerOptionAttribute.Value__c,
                                                                        SQX_Script_Execution__c = scriptExecution.Id,
                                                                        SQX_Script_Response__c = scriptResponse.Id,
                                                                        SQX_Answer_Option_Attribute__c = answerOptionAttribute.Id);

                        // this key makes unique scriptAttribute and in case of any duplicate records found then it keeps the last record as per the story [SQX-7544]
                        String key = '' + scriptExecution.Id + answerOptionAttribute.Attribute_Type__c + answerOptionAttribute.Value__c;
                        scriptAttributeMap.put(key,scriptAttribute);
                    }
                }
            }
        }
        
        // save scriptAttribute.
        new SQX_DB().op_insert(scriptAttributeMap.values(), new List<Schema.SObjectField> {Schema.SQX_Script_Attribute__c.Name,
                                                                                  Schema.SQX_Script_Attribute__c.Value__c,
                                                                                  Schema.SQX_Script_Attribute__c.SQX_Script_Execution__c,
                                                                                  Schema.SQX_Script_Attribute__c.SQX_Script_Response__c,
                                                                                  Schema.SQX_Script_Attribute__c.SQX_Answer_Option_Attribute__c
                                                                                });
    }
}