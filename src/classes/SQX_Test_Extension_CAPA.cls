/**
* Unit tests for SQX_Extension_CAPA
*
* @author Sagar Shrestha
* @date 2014/5/22
* 
*/
@isTest
public class SQX_Test_Extension_CAPA{
    /*
    *   Setup: create CAPA, save 
    *   Action: Call processChangeSet()
    *   Expected: should either save or throw exception
    */
  public static testmethod void createCAPA(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            Account supplierAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_SUPPLIER);
            Contact supplierContact = SQX_Test_Account_Factory.createContact(supplierAccount);
            User supplierUser = SQX_Test_Account_Factory.createUser(supplierContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER  );

            SQX_CAPA__c capa = new SQX_CAPA__c(
                                    Needs_Effectiveness_Review__c = false,
                                    Effectiveness_Monitoring_Days__c = 90,
                                    Issued_Date__c = Date.Today(),
                                    Target_Due_Date__c = Date.Today().addDays(15),
                                    SQX_Account__c = supplierAccount.Id,
                                    SQX_External_Contact__c = supplierContact.Id);

            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));

            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(capa));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            capaMap.put('Id', 'CAPA-1');
            capaMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
            
            changeSet.add(capaMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            Id result= SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null,params);

            System.assert(string.valueOf(result.getsObjectType()) == SQX.Capa, 
                'Expected to process changeset'+ changeSet);

        }
    }

    /*
    *   Setup: create CAPA without partner not enabled account, save 
    *   Action: Call processChangeSet()
    *   Expected: should either save or throw exception
    */
  public static testmethod void createCAPAWithoutPartnerNotEnabledContact(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            Account supplierAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_CUSTOMER);
            Contact supplierContact = SQX_Test_Account_Factory.createContact(supplierAccount);
            User supplierUser = SQX_Test_Account_Factory.createUser(supplierContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER  );

            SQX_CAPA__c capa = new SQX_CAPA__c(
                                    Needs_Effectiveness_Review__c = false,
                                    Effectiveness_Monitoring_Days__c = 90,
                                    Issued_Date__c = Date.Today(),
                                    Target_Due_Date__c = Date.Today().addDays(15),
                                    SQX_Account__c = supplierAccount.Id,
                                    SQX_External_Contact__c = supplierContact.Id);

            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));

            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(capa));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            capaMap.put('Id', 'CAPA-1');
            capaMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
            
            changeSet.add(capaMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            Id result= SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null,params);

            System.assert(string.valueOf(result.getsObjectType()) == SQX.Capa, 
                'Expected to process changeset'+ changeSet);

        }
    }

    /**
    *   Setup: create CAPA, save 
    *   Action: Call processChangeSet()
    *   Expected: should either save or throw exception 
    *
    * @author Sagar Shrestha
    * @date 2014/5/22
    * 
    */
    public static testmethod void saveCAPA(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save();   



            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name, Status__c, SQX_Finding__c FROM SQX_Capa__c WHERE Id=:capa.capa.Id]);

            SQX_Extension_CAPA capaExtension = new SQX_Extension_CAPA(controller);

            Map<String, SQX_CQ_Esig_Policies__c> esigPolicies = ( Map<String, SQX_CQ_Esig_Policies__c>)JSON.deserialize(capaExtension.getEsigPoliciesJSON(), Map<String, SQX_CQ_Esig_Policies__c>.class);

            String controllerName= capaExtension.getControllerName();
            System.assert(controllerName != SQX.Capa, 
                'Incorrect Controller'+ controllerName);


            String JSONData= capaExtension.getCompleteDataJSON();

            System.assert(JSONData != null, 
                'Expected to return JSONData'+ JSONData);


            SQX_CAPA__c changedCapa = new SQX_CAPA__c(Id = capa.capa.Id, Effectiveness_Monitoring_Days__c = 99);
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedCapa));
            Map<String, Object> originalValues = new Map<String, Object>();

            originalValues.put(SQX.getNSNameFor('Effectiveness_Monitoring_Days__c'), 90);

            changeSet.add(capaMap);

            submissionSet.put('changeSet', changeSet);

            capaMap.put('originalValues', originalValues);
           

            String stringChangeSet= JSON.serialize(submissionSet);
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            
            String result= SQX_Extension_CAPA.processChangeSetWithAction(capa.CAPA.Id, stringChangeSet, null,params);

            System.assert(result != null, 
                'Expected to process changeset'+ changeSet);
        }
    }

  public static testmethod void saveCAPAWithOnlyFindingChange(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save();   


            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name, Status__c, SQX_Finding__c FROM SQX_Capa__c WHERE Id=:capa.capa.Id]);

            SQX_Extension_CAPA capaExtension = new SQX_Extension_CAPA(controller);

            String controllerName= capaExtension.getControllerName();
            System.assert(controllerName != 'SQX_CAPA__c', 
                'Incorrect Controller'+ controllerName);


            String JSONData= capaExtension.getCompleteDataJSON();

            System.assert(JSONData != null, 
                'Expected to return JSONData'+ JSONData);

            SQX_Finding__c changedFinding = new SQX_Finding__c(Title__c = 'TT change', ID = capa.CAPA.SQX_Finding__c);

            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedFinding));
            Map<String, Object> originalValues = new Map<String, Object>();

            originalValues.put(SQX.getNSNameFor('Title__c'), null);

            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            findingMap.put('originalValues', originalValues);
           

            String stringChangeSet= JSON.serialize(submissionSet);

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            
            id result= SQX_Extension_CAPA.processChangeSetWithAction(capa.CAPA.Id, stringChangeSet, null,params);

            System.assert(result == capa.CAPA.Id, 
                'Expected to get same CAPA Id back although CAPA not changed'+ result + capa.CAPA.id);
        }
    }

    /**
    *   Setup: create CAPA, save 
    *   Action: Call processChangeSetWithAction()
    *   Expected: should either save or throw exception 
    *
    * @author Sagar Shrestha
    * @date 2014/5/22
    * 
    */
    public static testmethod void saveCAPAWithAction(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save();   
                                      
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Initiate');

            String result= SQX_Extension_CAPA.processChangeSetWithAction(capa.CAPA.Id,null, null, params);

            System.assert(result != null, 
                'Expected to process changeset');
        }
    }

    /**
    *   Setup: create CAPA, save 
    *   Action: Void CAPA
    *   Expected: should have status void 
    *
    * @author Sagar Shrestha
    * @date 2014/5/22
    * 
    */
    public static testmethod void voidCAPA(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save(); 

            SQX_CAPA__c changedCapa = new SQX_CAPA__c(Id = capa.capa.Id, Closure_Comment__c = 'voided');
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedCapa));
            Map<String, Object> originalValues = new Map<String, Object>();

            originalValues.put(SQX.getNSNameFor('Closure_Comment__c'), null);

            changeSet.add(capaMap);

            submissionSet.put('changeSet', changeSet);

            capaMap.put('originalValues', originalValues);
           

            String stringChangeSet= JSON.serialize(submissionSet);                      
            
            //void
            Map<String, String> paramsVoid = new Map<String, String>();
            paramsVoid.put('nextAction', 'Void');
            paramsVoid.put('recordID', capa.CAPA.Id);
            paramsVoid.put('purposeOfSignature', 'voiding CAPA');
            paramsVoid.put('comment', 'voiding CAPA');

            String result= SQX_Extension_CAPA.processChangeSetWithAction(capa.CAPA.Id,stringChangeSet, null, paramsVoid);

            SQX_CAPA__c voidedCAPA=[Select Status__c, Resolution__c from SQX_CAPA__c where Id= :capa.CAPA.Id]; 

            //call for test coverage
            SQX_Extension_UI.initWipItem();

            System.assert(voidedCAPA.Status__c=='Closed', 
                'Expected to void CAPA but is '+voidedCAPA.Status__c);

            System.assert(voidedCAPA.Resolution__c==SQX_Finding.RESOLUTION_VOID, 
                'Expected to void CAPA but is '+voidedCAPA.Resolution__c);
        }
    }

    /**
    *   Setup: create CAPA, save
    *   Action: Call processChangeSet()  after deleting a defect
    *   Expected: should either save or throw exception 
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void saveCAPAwithDeletedItems(){ 
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility()
                                                .save();
                                      
            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name,Status__c, SQX_Finding__c FROM SQX_Capa__c WHERE Id=:capa.capa.Id]);

            SQX_Extension_CAPA capaExtension = new SQX_Extension_CAPA(controller);

            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name='Hardware');
            insert partFamily;

            SQX_Part__c part= new SQX_Part__c(Name='Chip', 
                                            Part_Number__c='123',
                                            Part_Family__c=partFamily.Id,
                                            Part_Risk_Level__c=5,
                                            Active__c=true);
            insert part;

            SQX_Defect_Code__c defectCode= new SQX_Defect_Code__c(Name='Scratch', Description__c='new defect code',Defect_Category__c='Defect Cat');

            insert  defectCode;

            //List<SQX_Part__c> part= [SELECT Id FROM SQX_Part__c WHERE Active__c = true limit 1];
            //List<SQX_Defect_Code__c> defectCode= [SELECT Id FROM SQX_Defect_Code__c limit 1];

            SQX_Defect__c defect= new SQX_Defect__c(SQX_Finding__c= capa.finding.finding.Id,
                                                    SQX_Part__c= part.Id,
                                                    SQX_Defect_Code__c=defectCode.Id,
                                                    Defective_Quantity__c=300,
                                                    Defect_Category__c = 'Category One',
                                                    Defect_Code__c = 'Defect Code',
                                                    Defect_Description__c = 'Le defect',
                                                    Lot_Ser_Number__c='123',
                                                    Number_of_defects__c=1
                                        );

            insert defect;

            List<SQX_Defect__c> addedDefect= [SELECT Id FROM SQX_Defect__c WHERE SQX_Finding__c =:capa.finding.finding.Id];

            SQX_CAPA__c changedCapa = new SQX_CAPA__c(Id = capa.capa.Id, Effectiveness_Monitoring_Days__c = 99);
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(changedCapa));
            Map<String, Object> originalValues = new Map<String, Object>();

            originalValues.put(SQX.getNSNameFor('Effectiveness_Monitoring_Days__c'), 90);

            changeSet.add(capaMap);

            submissionSet.put('changeSet', changeSet);

            capaMap.put('originalValues', originalValues);
           

            String stringChangeSet= JSON.serialize(submissionSet);
            
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');

            String result= SQX_Extension_CAPA.processChangeSetWithAction(capa.CAPA.id,stringChangeSet, addedDefect, params);       

            addedDefect= [SELECT Id FROM SQX_Defect__c WHERE SQX_Finding__c =:capa.finding.finding.Id];

            System.assert(addedDefect.size() == 0, 
                'Expected defect to be deleted'+ defect);
        }
        
    }

    /**
    * Scenario: Tests whether security matrix serializes correctly or not
    * Given CAPA extension
    * When Security Matrix is fetched
    * Then Matrix contains security rules for required objects
    */
    static testmethod void givenExtension_WhenSecurityMatrixIsFetched_ThenAccessRecordIsReturned() {

        // Arrange: Create an extension for capa object
        SQX_Extension_CAPA ext = new SQX_Extension_CAPA(new ApexPages.StandardController(new SQX_CAPA__c()));

        //Act: Get json rules and deserialize it
        String jsonRules = ext.getSecurityMatrix();
        Map<String, Object> rules = (Map<String, Object>) JSON.deserializeUntyped(jsonRules);


        // Assert: Ensure that json rules for the following object types in CAPA are serialized
        String[] objectTypes = new String[] {SQX.CAPA, SQX.RootCause,
                             SQX.Investigation, SQX.Action,
                             SQX.getNSNameFor('SQX_CAPA_Impacted_Site__c'),
                             SQX.getNSNameFor('SQX_CAPA_Record_Activity__c'),
                             SQX.getNSNameFor('SQX_Resp_Inclusion_Approval__c'),
                             'Note', 'Attachment'};

        for (String objType : objectTypes) {
            System.assert(rules.containsKey(objType), 'Expected ' + objType + ' in matrix but found none');
            Map<String, Object> rule = (Map<String, Object>)rules.get(objType);
            System.assertEquals(3, rule.size(), 'Expected the map to contain three rules');
            System.assert(rule.containsKey(ext.IS_CREATEABLE), 'Expected map to contain create able');
            System.assert(rule.containsKey(ext.IS_UPDATEABLE), 'Expected map to contain update able');
            System.assert(rule.containsKey(ext.IS_DELETEABLE), 'Expected map to contain delete able');
        }
    }

}