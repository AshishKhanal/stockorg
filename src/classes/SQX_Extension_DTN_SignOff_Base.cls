/*
* author: Dibya Shrestha
* date : 2015/07/29
* description: Base extension class used for SignOff process for User or Trainer
*/
public with sharing  abstract class SQX_Extension_DTN_SignOff_Base {
    
    ApexPages.StandardSetController standardSetController;
    
    public Boolean isTrainerSignOff { get; set; }
    public PageReference redirectUrl { get; set; }
    
    /**
    * SQX_Extension_DTN_SignOff_Base constructor
    */
    public SQX_Extension_DTN_SignOff_Base(ApexPages.StandardSetController controller, String urlKey, Boolean isTrainerSignOff) {
        // initialize required values
        this.standardSetController = controller;
        this.isTrainerSignOff = isTrainerSignOff;
        
        // redirect for single record only, which is sent through email
        Id oldId = null;
        String strUrlKeyValue = ApexPages.currentPage().getParameters().get(urlKey);
        
        if (String.isNotBlank(strUrlKeyValue)) {
            try {
                oldId = strUrlKeyValue;
            } catch (Exception e) { }
        }
        
        if (oldId != null) {
            // get migrated record id
            List<SQX_Document_Training__c> dtList = [SELECT Id, Name, SQX_Personnel_Document_Training__c FROM SQX_Document_Training__c
                                                     WHERE Id = :oldId AND SQX_Personnel_Document_Training__c != null];
            
            if (dtList.size() == 1) {
                String migratedId = dtList[0].SQX_Personnel_Document_Training__c;
                String urlKeyForNewPage = '';
                
                if (isTrainerSignOff) {
                    redirectUrl = Page.SQX_PersonnelDocTraining_Trainer_SignOff;
                    urlKeyForNewPage = SQX_Extension_PDT_Trainer_SignOff.URL_KEY_FOR_ID;
                } else {
                    redirectUrl = Page.SQX_PersonnelDocTraining_User_SignOff;
                    urlKeyForNewPage = SQX_Extension_PDT_User_SignOff.URL_KEY_FOR_ID;
                }
                
                redirectUrl.getParameters().put(urlKeyForNewPage, migratedId);
            }
        }
    }
    
    public PageReference redirectToNewPage() {
        return redirectUrl;
    }
}