/**
* unit test for supplier deviation record submit only draft status
*/
@isTest
public class SQX_Test_5758_Submit_Access_Only_Draft {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, adminUser, assigneeUser});
    }
    
    /**
    * GIVEN : given initiate supplier deviation record 
    * WHEN : submit the record
    * THEN : throws validation error message
    */
    public static testMethod void givenAnInitiateSD_WhenSubmitted_ThenAnErrorIsPresented(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, adminUser});
        System.runAs(adminUser){
            // Arrange: create supplier deviation record
            SQX_Supplier_Deviation__c sd = new SQX_Test_Supplier_Deviation().save().sd;
            
            //Submit the SD record
            sd.Status__c = SQX_NSI.STATUS_DRAFT;
            sd.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            sd.Activity_Code__c = 'submit';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__C>{ sd }, new List<SObjectField>{});
            
            // initiate the record
            sd.Status__c = SQX_NSI.STATUS_OPEN;
            sd.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            sd.Activity_Code__c = 'initiate';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__C>{ sd }, new List<SObjectField>{});
            
            // Act: Try to Submit supplier deviation
            sd.Status__c = SQX_NSI.STATUS_DRAFT;
            sd.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            sd.Activity_Code__c = 'submit';
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__C> {sd}, new List<SObjectField>{});
            
            // Assert : Ensure supplier deviation is not Submitted
            System.assert(result[0].isSuccess() == false, 'Expected : SD should not be submitted when status is open and inProgress . Actual : SD is initiated');
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,result[0].getErrors().get(0).getMessage());
        }
    }
    
    /**
    * GIVEN : given supplier deviation record 
    * WHEN : submit the record multiple times
    * THEN : throws validation error message
    */
    public static testMethod void givenSupplierDeviation_WhenSubmitTheRecordMultipleTimes_ThenThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, adminUser});
        System.runAs(adminUser){
            // Arrange: create supplier deviation record
            SQX_Supplier_Deviation__c sd = new SQX_Test_Supplier_Deviation().save().sd;
            
            //Submit the SD record
            sd.Status__c = SQX_NSI.STATUS_DRAFT;
            sd.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            sd.Activity_Code__c = 'submit';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__C>{ sd }, new List<SObjectField>{});
            
            //Act: Submit the record which already submitted
            sd.Status__c = SQX_NSI.STATUS_DRAFT;
            sd.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            sd.Activity_Code__c = 'submit';
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__C> {sd}, new List<SObjectField>{});
            
            // Assert : Ensure supplier deviation is not Submitted
            System.assert(result[0].isSuccess() == false, 'Once record is submitted user should not able to submit the record again');
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,result[0].getErrors().get(0).getMessage());
        }
    }
}