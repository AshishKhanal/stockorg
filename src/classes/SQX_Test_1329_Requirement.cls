/**
* Unit tests for Requirement Object
*
* @author Dibya Shrestha
* @date 2015/07/17
* 
*/
@IsTest
public class SQX_Test_1329_Requirement {
    
    static Boolean runAllTests = true,
                   run_givenAddRequirementForDocumentWithoutEditPermission_NotAdded = false,
                   run_givenApproveAutoReleaseRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated = false,
                   run_givenReleaseApprovedRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated = false;
    
    static Boolean hasError(String errorMessage, Database.SaveResult saveResult) {
        for (Database.Error err : saveResult.getErrors()) {
            if (err.getMessage().equals(errorMessage)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * Tests prevention of addition of requirement for documents with edit permission.
    */
    public static testmethod void givenAddRequirementForDocumentWithoutEditPermission_NotAdded() {
        if (!runAllTests && !run_givenAddRequirementForDocumentWithoutEditPermission_NotAdded) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User stdUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User stdUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        SQX_Job_Function__c jf1, jf2;
        SQX_Test_Controlled_Document cDoc1;
        Database.SaveResult saveResult1;
        
        System.runas(stdUser1) {
            // creating required job functions
            jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            saveResult1 = Database.insert(jf1, false);

            jf2 = new SQX_Job_Function__c( Name = 'job function 2 for test' );
            saveResult1 = Database.insert(jf2, false);
            
            // creating required controlled doc with draft status
            cDoc1 = new SQX_Test_Controlled_Document().save();
        }
        
        System.runas(stdUser2) {
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            
            // ACT: add requirement on document created by user 1
            saveResult1 = Database.insert(Req1, false);
            
            System.assert(saveResult1.isSuccess() == false,
                'Expected requirement not to be added for document without edit permission.');
            
            System.assert(hasError(Label.SQX_ERR_MSG_CANNOT_ADD_REQUIREMENT_FOR_DOCUMENT_WITH_INSUFFICIENT_PRIVILEGES, saveResult1),
                'Expected error message "' + Label.SQX_ERR_MSG_CANNOT_ADD_REQUIREMENT_FOR_DOCUMENT_WITH_INSUFFICIENT_PRIVILEGES + '"');
        }
    }
    
    /**
    * Tests approval process for a revised document with auto-release on by another user.
    */
    public static testmethod void givenApproveAutoReleaseRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated() {
        if (!runAllTests && !run_givenApproveAutoReleaseRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User stdUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User stdUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        SQX_Job_Function__c jf1;
        SQX_Test_Controlled_Document revisedDoc;
        Database.SaveResult saveResult1;
        
        System.runas(stdUser1) {
            // creating required job functions
            jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            saveResult1 = jf.save();
            
            // creating required controlled doc with draft status
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            
            // inserting active requirement for current document
            saveResult1 = Database.insert(Req1, false);
            
            revisedDoc = cDoc1.revise().save();
        }
        
        System.runas(stdUser2) {
            //ACT: approve controlled doc with auto-release on
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            revisedDoc.doc.Effective_Date__c = Date.Today();
            revisedDoc.doc.Date_Issued__c = Date.Today();
            new SQX_DB().withoutSharing().op_update(new List<SQX_Controlled_Document__c> { revisedDoc.doc }, new List<Schema.SObjectField> { Schema.SQX_Controlled_Document__c.Document_Status__c,
                                                                                                                                             Schema.SQX_Controlled_Document__c.Effective_Date__c });


            // fetch activated requirement of newly released revised document
            SQX_Requirement__c Req2 = [SELECT Id, Active__c, Activation_Date__c FROM SQX_Requirement__c
                                       WHERE SQX_Controlled_Document__c = :revisedDoc.doc.Id
                                       AND SQX_Job_Function__c = :jf1.Id];

            System.assert(Req2.Active__c == true,
                'Expected Active to be checked for requirement of newly released revised document.');

            System.assert(Req2.Activation_Date__c != null,
                'Expected activation date to be set for requirement of newly released revised document.');
        }
    }
    
    /**
    * Tests releasing process of an approved revised document with auto-release off by another user.
    */
    public static testmethod void givenReleaseApprovedRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated() {
        if (!runAllTests && !run_givenReleaseApprovedRevisedDocumentUsingAnotherUser_DocumentReleasedWithRequirementActivated) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User stdUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User stdUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        SQX_Job_Function__c jf1;
        SQX_Test_Controlled_Document revisedDoc;
        Database.SaveResult saveResult1;
        
        System.runas(stdUser1) {
            // creating required job functions
            jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            saveResult1 = jf.save();
            
            // creating required controlled doc with draft status
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = true
            );
            
            // inserting active requirement for current document
            saveResult1 = Database.insert(Req1, false);
            
            // creating revised draft document with auto-release off
            revisedDoc = cDoc1.revise();
            revisedDoc.doc.Auto_Release__c = false;
            revisedDoc.save();
            
            // approve revised document
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();
        }
        
        System.runas(stdUser2) {
            //ACT: release approved revised document
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            revisedDoc.doc.Effective_Date__c = Date.Today();
            revisedDoc.doc.Date_Issued__c = Date.Today();
            new SQX_DB().withoutSharing().op_update(new List<SQX_Controlled_Document__c> { revisedDoc.doc }, new List<Schema.SObjectField> { Schema.SQX_Controlled_Document__c.Document_Status__c,
                                                                                                                                             Schema.SQX_Controlled_Document__c.Effective_Date__c });


            // fetch activated requirement of newly released revised document
            SQX_Requirement__c Req2 = [SELECT Id, Active__c, Activation_Date__c FROM SQX_Requirement__c
                                       WHERE SQX_Controlled_Document__c = :revisedDoc.doc.Id
                                       AND SQX_Job_Function__c = :jf1.Id];

            System.assert(Req2.Active__c == true,
                'Expected Active to be checked for requirement of newly released revised document.');

            System.assert(Req2.Activation_Date__c != null,
                'Expected activation date to be set for requirement of newly released revised document.');
        }
    }
}