public with sharing class SQX_Extension_Supplier_Admin {
    private List<SQX_ObjectWrapper> allContacts;
    private List<SQX_ObjectWrapper> allSupplierParts;
    private List<SQX_ObjectWrapper> allSupplierServices;
    
    private Account CurrentAccount;
    private ApexPages.StandardController mainController;

    

    public SQX_Extension_Supplier_Admin(ApexPages.StandardController controller) {
        currentAccount = (Account) controller.getRecord();
        mainController = controller;
        
        if(allContacts == null){
            //fetch all contacts returns a blank list if nothing is found
            allContacts = getObjectsFor(Contact.getSObjectType(), 
                            Schema.SObjectType.Contact.FieldSets.Basic_Information.getFields(), 'AccountId');
            
            
            allSupplierParts = getObjectsFor(SQX_Supplier_Part__c.getSObjectType(), 
                                Schema.SObjectType.SQX_Supplier_Part__c.FieldSets.Supplier_Admin_Basic_Information.getFields(), 'Account__c');
            
            
            allSupplierServices = getObjectsFor(SQX_Supplier_Service__c.getSObjectType(), 
                                Schema.SObjectType.SQX_Supplier_Service__c.FieldSets.Supplier_Admin_Basic_Information.getFields(), 'Account__c');
        }
    }
    
    public PageReference redirectBackToAccountIfNoID(){
        if(currentAccount.ID == null){
            return new PageReference('/001');
        }
        

        return null;
    }
    
    public void addNewContact(){
        if(allContacts == null )
            return;
            
        
        
        integer serialNumber = allContacts.size() == 0 ? 1 : (allContacts.get(allContacts.size() - 1).serialNumber + 1);
        SQX_ObjectWrapper newObject = new SQX_ObjectWrapper();
        Contact contact = new Contact();
        contact.AccountID = currentAccount.ID;
        newObject.baseObject = contact;
        newObject.isSelected = false;
        newObject.serialNumber = serialNumber;
        newObject.markForDeletion  = false;
        
        allContacts.add(newObject);
        
    }
    
    public List<SQX_ObjectWrapper> getAllContacts() {
      
        return this.allContacts;
    }
    
    public List<SQX_ObjectWrapper> getAllSupplierParts(){
 
        return this.allSupplierParts;
    }
    
    public List<SQX_ObjectWrapper> getAllSupplierServices(){

        return this.allSupplierServices;
    }
    
    private List<SQX_ObjectWrapper> getObjectsFor(Schema.SObjectType objectType, List<Schema.FieldSetMember> fields, String accountField){
        Schema.DescribeSObjectResult objectDesc = objectType.getDescribe();
        if(this.CurrentAccount.ID == null || !objectDesc.isQueryable() || !objectDesc.isAccessible()){
            return new List<SQX_ObjectWrapper>();
        }
        
        String query = 'SELECT ';
        boolean namePresent = false;
        boolean accountFieldPresent = false;
        
        for(Schema.FieldSetMember f : fields) {
            query += f.getFieldPath() + ', ';
            if(f.getFieldPath() == 'Name')
                namePresent = true;
                
            if(f.getFieldPath() == accountField)
                accountFieldPresent = true;
        }
        
        if(namePresent == false)
            query += 'Name, ';
            
        if(accountFieldPresent == false)
            query += accountField + ',';
            
        query += 'Id FROM ' + objectType;
        query += ' WHERE ' + accountField + ' = \'' + currentAccount.ID + '\'';
        query += ' ORDER BY Name ';
        List<sObject> queryResult = Database.query(query);
        
        List<SQX_ObjectWrapper> returnList = new List<SQX_ObjectWrapper>();
        integer count = 1;
        for(sObject baseObject : queryResult){
            SQX_ObjectWrapper wrapper = new SQX_ObjectWrapper();
            wrapper.baseObject = baseObject;
            wrapper.isSelected = false;
            wrapper.serialNumber = count;
            wrapper.markForDeletion  = false;
            returnList.add(wrapper);
            
            count++;
        }
        
        return returnList;
    }
    
    public void removeTemporaryContact(){
        removeTemporaryObject('contactIndex', allContacts);
    }
    
    public void removeTemporaryObject(String paramName, List<SQX_ObjectWrapper> objectList){        
        
        integer objectSerialNumber = -1;
        String index = ApexPages.currentPage().getParameters().get(paramName);
        
        if(index == null)
            return;
        
        objectSerialNumber = integer.valueOf(index);
        
        if(objectSerialNumber < 1)
            return;
            
        integer indexOfObject  = -1;
        integer count = 0;
        for(SQX_ObjectWrapper wrappedObject : objectList){
            if(wrappedObject.serialNumber == objectSerialNumber){
                
                indexOfObject = count;
                break;
            }
            count++;
        }
        
        if(indexOfObject != -1){
            SQX_ObjectWrapper wrappedObject = objectList.get(indexOfObject);
            wrappedObject.markForDeletion = !wrappedObject.markForDeletion;
            
            if(wrappedObject.baseObject.ID == null){
                objectList.remove(indexOfObject);
            }
        }

    }


    
    public PageReference saveAllChanges(){
        
        return new PrivilegeEscalator().saveAllChanges(mainController, allContacts, allSupplierServices, allSupplierParts);
            
    }
    
    
    
    
    public List<SQX_FieldSetMemberWrapper> getSupplierPartsFields(){
        return getFieldSet(Schema.SObjectType.SQX_Supplier_Part__c.fieldSets.Supplier_Admin_Basic_Information.getFields(),
                Schema.SObjectType.SQX_Supplier_Part__c.fieldSets.Supplier_Admin_Editable_Fields.getFields());
    }
    
    public List<SQX_FieldSetMemberWrapper> getSupplierServicesFields(){
        return getFieldSet(Schema.SObjectType.SQX_Supplier_Service__c.fieldSets.Supplier_Admin_Basic_Information.getFields(),
                Schema.SObjectType.SQX_Supplier_Service__c.fieldSets.Supplier_Admin_Editable_Fields.getFields());
    }
    
    private List<SQX_FieldSetMemberWrapper> getFieldSet(List<Schema.FieldSetMember> allMembers, 
            List<Schema.FieldSetMember> readwriteMembers){
        List<SQX_FieldSetMemberWrapper> allFields = new List<SQX_FieldSetMemberWrapper>();
        
        for(Schema.FieldSetMember member : allMembers){
            SQX_FieldSetMemberWrapper wrapper = new SQX_FieldSetMemberWrapper();
            wrapper.field = member;
            wrapper.isReadOnly = true;
            
            for(Schema.FieldSetMember readWriteMember : readwriteMembers){
                
                if(readWriteMember.getFieldPath() == member.getFieldPath()){
                    wrapper.isReadOnly = false;
                    break;
                }
            }
            
            allFields.add(wrapper);
        }
        
        return allFields;
    }


    /**
    * Here privilege has been escalated i.e. sharing rules are being violated because the supplier with read permission on the account, supplier parts, supplier services and contacts needs
    * to be able to modify them.
    */
    public without sharing class PrivilegeEscalator{
        private Account currentAccount;
        public PageReference saveAllChanges(ApexPages.StandardController mainController, List<SQX_ObjectWrapper> allContacts,
            List<SQX_ObjectWrapper> allSupplierServices, List<SQX_ObjectWrapper> allSupplierParts){

            PageReference returnReference = null;
            SavePoint nothingSaved = Database.setSavePoint();
            
            try{

                currentAccount = (Account)mainController.getRecord();
                update currentAccount;
                
                returnReference = new PageReference('/' + currentAccount.ID);
                
                boolean hasErrors = true;
                if(persistChanges(allContacts)){
                    if(persistChanges(allSupplierParts)){
                        if(persistChanges(allSupplierServices)){
                            hasErrors = false;
                        }
                    }
                }
                
                if(hasErrors == true){
                    
                    returnReference = null;
                    Database.rollback(nothingSaved);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The save operation could not be completed because of some errors.'));
                }
                
            }
            catch(Exception ex){
                returnReference = null;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage() + ex.getStackTraceString()));
            }
        
            return returnReference;

        }

        private boolean persistChanges(List<SQX_ObjectWrapper> objectList){
        boolean hasError = false;
        List<sObject> objectsToDelete = new List<sObject>();
        List<sObject> objectsToUpdate = new List<sObject>();
        List<sObject> objectsToInsert = new List<sObject>();
        
        
        for(SQX_ObjectWrapper wrappedObject : objectList){
            if(wrappedObject.markForDeletion == true){
                System.assert(wrappedObject.baseObject.ID != null ,
                    'Expected the ID to be non null but found null ID for ' + wrappedObject.baseObject);

                System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isDeletable() , wrappedObject.baseObject + ' is not deletable.'); 
                objectsToDelete.add(wrappedObject.baseObject);
            }
            else{
                if(wrappedObject.baseObject.ID == null){
                    System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isCreateable() , wrappedObject.baseObject + ' is not createable.'); //fls is enforced by ui
                    objectsToInsert.add(wrappedObject.baseObject);
                }
                else{
                    System.assert(wrappedObject.baseObject.getSObjectType().getDescribe().isUpdateable() , wrappedObject.baseObject + ' is not updatable.');
                    objectsToUpdate.add(wrappedObject.baseObject);
                }
            }
        }
        List<Database.DeleteResult> deleteResults = new SQX_DB().continueOnError().op_delete(objectsToDelete);

        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            if(wrappedObject.markForDeletion == true){
                for(Database.DeleteResult deleteResult : deleteResults){
                    if(deleteResult.getID() == wrappedObject.baseObject.ID){
                        if(deleteResult.isSuccess() == false){
                            wrappedObject.markForDeletion = false;
                            wrappedObject.baseObject.addError(deleteResult.getErrors().get(0).getMessage());
                            hasError = true;
                            
                            break;
                        }
                    }
                }
            }
        }
        
        List<Database.SaveResult> updateResults = new SQX_DB().continueOnError().op_update(objectsToUpdate, new List<Schema.SObjectField>{} );
        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            
            for(Integer index = 0; index < objectsToUpdate.size(); index++){
                if(wrappedObject.baseObject == objectsToUpdate.get(index)){
                    Database.SaveResult dmlSaveResult = updateResults.get(index);
                
                    if(dmlSaveResult.isSuccess() == false){
                        wrappedObject.baseObject.addError(dmlSaveResult.getErrors().get(0).getMessage() + wrappedObject.baseObject);
                        hasError = true;
                        objectsToUpdate.remove(index); //just reducing number of iterations for next match
                        break;
                    }
                
                }
            }
        }
        
        List<Database.SaveResult> insertResults = new SQX_DB().continueOnError().op_insert(objectsToInsert, new List<Schema.SObjectField>{});
        //associate the error
        for(SQX_ObjectWrapper wrappedObject : objectList){
            //check if wrappedObject has delete error
            
            for(Integer index = 0; index < objectsToInsert.size(); index++){
                if(wrappedObject.baseObject == objectsToInsert.get(index)){
                    Database.SaveResult dmlSaveResult = insertResults.get(index);
                
                    if(dmlSaveResult.isSuccess() == false){
                        wrappedObject.baseObject.addError(dmlSaveResult.getErrors().get(0).getMessage());
                        hasError = true;
                        objectsToInsert.remove(index); //just reducing number of iterations for next match
                        break;
                    }
                
                }
            }
        }
        
        return !hasError; //return success value i.e. no error
        
    }
    }
    
    
   
}