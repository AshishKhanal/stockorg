/**
* This class contains the logic related to submission history
*/
public with sharing class SQX_Submission_History {

    public final static String STATUS_COMPLETE = 'Complete',
                        STATUS_PENDING = 'Pending',
                        STATUS_QUEUED = 'Queued',
                        STATUS_SUBMISSION_FAILURE = 'Submission Failure',
                        STATUS_TRANSMISSION_FAILURE = 'Transmission Failure',
                        STATUS_GENERATING_ADDITIONAL_FILES = 'Generating Additional Files';

    public final static String ACKNOWLEDGEMENT_STATUS_PASSED = 'Passed',
                        ACKNOWLEDGEMENT_STATUS_FAILED = 'Failed';

    public final static String DELIVERY_TYPE_MANUAL = 'Manual',
                                DELIVERY_TYPE_EMAIL = 'Email',
                                DELIVERY_TYPE_EXTERNAL_SYSTEM = 'External System';

    /**
     * Method to create a submission record for Manual Submission type reports
    */
    @AuraEnabled
    public static String createSubmissionRecord(String recordId) {

        try {

            SQX_General_Report__c gReport = [SELECT SQX_Regulatory_Report__c, SQX_Regulatory_Report__r.Due_Date__c, SQX_Regulatory_Report__r.Follow_Up_Number__c,
                                             SQX_Regulatory_Report__r.SQX_Follow_Up_Of__c, SQX_Regulatory_Report__r.SQX_Complaint__c, SQX_Regulatory_Report__r.Reg_Body__c, SQX_Regulatory_Report__r.Name FROM SQX_General_Report__c WHERE Id =: recordId];

            CQ_Regulatory_Report_Setting__mdt settings = new SQX_Regulatory_Report().getSubmissionSettingsForReport(gReport.SQX_Regulatory_Report__c);

            String deliveryType = settings == null ? DELIVERY_TYPE_MANUAL : settings.Mode_Of_Submission__c;

            SQX_Submission_History__c submissionHistory = new SQX_Submission_History__c(
                Status__c = (deliveryType == DELIVERY_TYPE_MANUAL ? STATUS_COMPLETE : STATUS_PENDING),
                SQX_Complaint__c = gReport.SQX_Regulatory_Report__r.SQX_Complaint__c,
                Submitted_By__c = UserInfo.getName(),
                SQX_Submitted_By__c = UserInfo.getUserId(),
                Reg_Body__c = gReport.SQX_Regulatory_Report__r.Reg_Body__c,
                Submitted_Date__c = System.today(),
                Name = gReport.SQX_Regulatory_Report__r.Name,
                Due_Date__c = gReport.SQX_Regulatory_Report__r.Due_Date__c,
                SQX_General_Report__c = gReport.Id,
                SQX_Regulatory_Report__c = gReport.SQX_Regulatory_Report__c,
                Delivery_Type__c = deliveryType,
                Report_Number__c = gReport.SQX_Regulatory_Report__r.Follow_Up_Number__c,
                Is_Follow_Up__c = gReport.SQX_Regulatory_Report__r.SQX_Follow_Up_Of__c != null? true:false
            );

            new SQX_DB().op_insert(new List<SQX_Submission_History__c>  { submissionHistory }, new List<SObjectField> {
                    SQX_Submission_History__c.Status__c,
                    SQX_Submission_History__c.SQX_Complaint__c,
                    SQX_Submission_History__c.Submitted_By__c,
                    SQX_Submission_History__c.Reg_Body__c,
                    SQX_Submission_History__c.Submitted_Date__c,
                    SQX_Submission_History__c.Name,
                    SQX_Submission_History__c.SQX_General_Report__c
                    });
            
            // Query the attachment files of the general report. The file is represented here by ContentDocumentId
            List<ContentDocumentLink> cDocLinks = [SELECT Id,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =: gReport.Id];
            
            List<ContentDocumentLink> linksToInsert = new List<ContentDocumentLink>();
            
            // Create content document link that links the file with submission record.
            for(ContentDocumentLink cDocLink: cDocLinks){
                
                ContentDocumentLink cdl = new ContentDocumentLink();
            	cdl.ContentDocumentId = cDocLink.ContentDocumentId;
            	cdl.LinkedEntityId = submissionHistory.Id;
            	cdl.ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED;
                linksToInsert.add(cdl);
                
            }
            
            // WithoutSharing added to allow user other than the creator of general report to add attachments without submission failing. 
            new SQX_DB().withoutSharing().op_insert( linksToInsert , new List<SObjectField> {});

            if(submissionHistory.Status__c == STATUS_PENDING) {
                update new SQX_Regulatory_Report__c(Id = submissionHistory.SQX_Regulatory_Report__c, SQX_Submission_History__c = submissionHistory.Id);
            }

            return submissionHistory.Id;

        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }

    }


    /**
     * Method to set the record type of the Manually submitted Regulatory content
    */
    @AuraEnabled
    public static void setRecordTypeForRegulatoryReport(Id contentDocumentId) {

        try {

            Id regContentRTId = SQX_ContentVersion.getRegulatoryContentRecordType();

            List<ContentVersion> cvsToUpdate = new List<ContentVersion>();
            for(ContentVersion cv : [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: contentDocumentId AND IsLatest = true]) {
                cv.RecordTypeId = regContentRTId;
                cvsToUpdate.add(cv);
            }

            new SQX_DB().op_update(cvsToUpdate, new List<SObjectField> { ContentVersion.RecordTypeId });

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }


    /**
     * Method creates a new submission record for the regulatory record
     * and links the newly inserted Regulatory Record content(file) to the submission record created above
     * @param regRecordContentMap map whose key is the Regulatory Record(Medwatch/MedDev etc) Id and whose value is the ContentDocumentId
    */
    public static void createSubmissionAndLinkContentFor(Map<Id, ContentVersion> reportAndContentMap) {

        Set<Id> reportIds = reportAndContentMap.keySet();

        SQX_Submission_History__c submissionRecord;

        Map<Id, SQX_Submission_History__c> contentAndSubmissionMap = new Map<Id, SQX_Submission_History__c>();

        List<SQX_Submission_History__c> submissionRecordsToInsert = new List<SQX_Submission_History__c>();

        List<ContentDocumentLink> linksToInsert = new List<ContentDocumentLink>();

        List<SQX_Submission_History__c> submissionRecordsToUpdate = new List<SQX_Submission_History__c>();
        Map<Id, SQX_Regulatory_Report__c> reportsToAddLinkTo = new Map<Id, SQX_Regulatory_Report__c>();

        Map<Id, CQ_Regulatory_Report_Setting__mdt> settingsMap = new SQX_Regulatory_Report().getSubmissionSettingsForReports(reportIds);

        Map<String,String> regReportByReport = new Map<String,String> ();
        Map<String,String> submissionHistoryByReport = new Map<String,String> ();
        
        for(SQX_Regulatory_Report__c report : [SELECT Report_Id__c,Follow_Up_Number__c, SQX_Follow_Up_Of__c, SQX_Submission_History__c, Due_Date__c, Reg_Body__c, SQX_Complaint__c, Name from SQX_Regulatory_Report__c where Id in: reportIds]) {

            ContentVersion cv = reportAndContentMap.get(report.Id);

            String action = cv.CQ_Actions__c.substringBeforeLast('_');

            if(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION.startsWith(action) || SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_INITIAL.startsWith(action)) {
				
                regReportByReport.put(report.Id, report.Report_Id__c);
                String deliveryType = settingsMap.containsKey(report.Id) ? settingsMap.get(report.Id).Mode_Of_Submission__c : DELIVERY_TYPE_MANUAL;

                // insert new submission history, link content, and add link to report
                submissionRecord = new SQX_Submission_History__c(
                    Status__c = SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION.startsWith(action) ? (deliveryType == DELIVERY_TYPE_MANUAL ? STATUS_COMPLETE : STATUS_PENDING) : STATUS_GENERATING_ADDITIONAL_FILES,
                    SQX_Complaint__c = report.SQX_Complaint__c,
                    Submitted_By__c= UserInfo.getName(),
                    SQX_Submitted_By__c = UserInfo.getUserId(),
                    Reg_Body__c = report.Reg_Body__c,
                    Submitted_Date__c=System.today(),
                    Name = report.Name,
                    SQX_Regulatory_Report__c = report.Id,
                    Due_Date__c = report.Due_Date__c,
                    Delivery_Type__c = deliveryType,
                    Report_Number__c = report.Follow_Up_Number__c,
                    Is_Follow_Up__c = report.SQX_Follow_Up_Of__c != null? true:false
                );

                // add link to the reg record(i.e Medwatch, MedDev, etc)
                SObjectType regRecordType = Id.valueOf(report.Report_Id__c).getSObjectType();
                submissionRecord.put(getRelatingField(regRecordType, SQX_Submission_History__c.SObjectType), report.Report_Id__c);

                submissionRecordsToInsert.add(submissionRecord);

                if(submissionRecord.Status__c != STATUS_COMPLETE) {
                    // if the submission record is already complete, the trigger will automatically link and complete the regulatory report
                    reportsToAddLinkTo.put(report.Id, new SQX_Regulatory_Report__c(Id = report.Id));
                }


                contentAndSubmissionMap.put(cv.ContentDocumentId, submissionRecord);

                // if the report already contains a submission record, remove the link from submission record as it has to be a submission failure
                if(!String.isBlank(report.SQX_Submission_History__c)) {
                    submissionRecordsToUpdate.add(new SQX_Submission_History__c(Id = report.SQX_Submission_History__c, SQX_Regulatory_Report__c = null));
                }

            } else if(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_ADDITIONAL.startsWith(action)) {

                // just link content
                contentAndSubmissionMap.put(cv.ContentDocumentId, new SQX_Submission_History__c(Id = report.SQX_Submission_History__c, SQX_Regulatory_Report__c = report.Id));

            } else if(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_FINAL.startsWith(action)) {

                // link content, update status to pending
                contentAndSubmissionMap.put(cv.ContentDocumentId, new SQX_Submission_History__c(Id = report.SQX_Submission_History__c, SQX_Regulatory_Report__c = report.Id));
                submissionRecordsToUpdate.add(new SQX_Submission_History__c(Id = report.SQX_Submission_History__c, Status__c = STATUS_PENDING));
            }

        }

        List<Database.SaveResult> srs = new SQX_DB().continueOnError().op_insert(submissionRecordsToInsert, new List<SObjectField> {
            SQX_Submission_History__c.Status__c,
            SQX_Submission_History__c.SQX_Complaint__c,
            SQX_Submission_History__c.Submitted_By__c,
            SQX_Submission_History__c.Reg_Body__c,
            SQX_Submission_History__c.Submitted_Date__c,
            SQX_Submission_History__c.Due_Date__c,
            SQX_Submission_History__c.Name
        });   

        integer i=0;
        for(Database.SaveResult sr : srs) {

            SQX_Submission_History__c subRecord = submissionRecordsToInsert.get(i++);

            if(!sr.isSuccess()) {
                List<String> errors = new List<String>();

                for(Database.Error error : sr.getErrors()) {
                    errors.add(error.getMessage());
                }

                reportAndContentMap.get(subRecord.SQX_Regulatory_Report__c).addError(String.join(errors, '\n'));

            }
        }

        // Map regulatoryReport.Report__Id__c with submission historyId
        for(SQX_Submission_History__c history: submissionRecordsToInsert){      
            if(history.Id != null){
                submissionHistoryByReport.put(regReportByReport.get(history.SQX_Regulatory_Report__c), history.Id);
            } 
        }
        
        if(!regReportByReport.isEmpty()){
            
            List<String> reportIdList = regReportByReport.values();
            
            List<ContentDocumentLink> cDocLinks = [SELECT Id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink 
                                                   WHERE LinkedEntityId IN: reportIdList];
            
            Map<String,String> contentDocByReport = new Map<String,String>();
            for(ContentDocumentLink cDocLink : cDocLinks){
                
                if(submissionHistoryByReport.containsKey(cDocLink.LinkedEntityId)){
                    contentAndSubmissionMap.put(cDocLink.ContentDocumentId, 
                                                new SQX_Submission_History__c(Id = submissionHistoryByReport.get(cDocLink.LinkedEntityId),SQX_Regulatory_Report__c = regReportByReport.get(cDocLink.LinkedEntityId)) ); 
                }
                
            }
        }   

        for(Id cDocId : contentAndSubmissionMap.keySet()) {

            SQX_Submission_History__c subHistoryRecord = contentAndSubmissionMap.get(cDocId);

            if(subHistoryRecord.Id != null) {
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cDocId;
                cdl.LinkedEntityId = subHistoryRecord.Id;
                cdl.ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED;

                linksToInsert.add(cdl);

                // now that the new submission history has been inserted, attach it with the regulatory report
                if(reportsToAddLinkTo.containsKey(subHistoryRecord.SQX_Regulatory_Report__c)) {
                    reportsToAddLinkTo.get(subHistoryRecord.SQX_Regulatory_Report__c).SQX_Submission_History__c = subHistoryRecord.Id;
                }
            } else {
                reportsToAddLinkTo.remove(subHistoryRecord.SQX_Regulatory_Report__c);
            }

        }

        // WithoutSharing added to allow user other than the creator of (Medwatch/Meddev) to add attachments without submission failing.
        new SQX_DB().withoutSharing().op_insert(linksToInsert, new List<SObjectField> {
            ContentDocumentLink.ContentDocumentId,ContentDocumentLink.LinkedEntityId,ContentDocumentLink.ShareType
                });

        new SQX_DB().op_update(new Map<SObjectType, List<SObject>> {
            SQX_Regulatory_Report__c.SObjectType => reportsToAddLinkTo.values(),
            SQX_Submission_History__c.SObjectType => submissionRecordsToUpdate
        }, new Map<SObjectType, List<SObjectField>> {
            SQX_Regulatory_Report__c.SObjectType => new List<SObjectField> { SQX_Regulatory_Report__c.SQX_Submission_History__c },
            SQX_Submission_History__c.SObjectType => new List<SObjectField> { SQX_Submission_History__c.Status__c }
        });
    }


    /**
     * Method returns the field pointing to the given parent object from the given child object
    */
    private static SObjectField getRelatingField(SObjectType parentObjType, SObjectType childObjType) {
        SObjectField relatingField = null;

        DescribeSObjectResult sObjDesc = parentObjType.getDescribe();

        for(ChildRelationship childRel : sObjDesc.getChildRelationships()) {
            if(childRel.getChildSObject() == childObjType) {
                relatingField = childRel.getField();
                break;
            }
        }

        return relatingField;
    }

    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified(){

        }

        /**
        * The constructor for handling the trigger activities
        * @param newSubmissionHistories the submission histories that have been newly added/updated
        * @param oldMap the map of old values of the submission history
        */
        public Bulkified(List<SQX_Submission_History__c> newSubmissionHistories, Map<Id, SQX_Submission_History__c> oldMap){
            super(newSubmissionHistories, oldMap);
        }

        /**
         * Method to prevent deletion of submission history if parent complaint is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){

            this.resetView();

            Map<Id, SQX_Submission_History__c> submissionHistories = new Map<Id, SQX_Submission_History__c>((List<SQX_Submission_History__c>)this.view);

            for(SQX_Submission_History__c submissionHistory : [SELECT Id FROM SQX_Submission_History__c WHERE Id IN : submissionHistories.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    submissionHistories.get(submissionHistory.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }

            return this;

        }


        /**
         * Update Report Submission Status based on the Acknowledgement from Regulatory Body
         * Valid only for eSubmission
        */
        public Bulkified updateSubmissionStatusWhenAcknowledgementIsReceived() {

            this.resetView();

            for(SQX_Submission_History__c submission : (List<SQX_Submission_History__c>) this.view) {
                SQX_Submission_History__c oldSubmission = (SQX_Submission_History__c) this.oldValues.get(submission.Id);

                if((oldSubmission == null || oldSubmission.ACK1_Status__c != submission.ACK1_Status__c) &&
                   submission.ACK1_Status__c == ACKNOWLEDGEMENT_STATUS_FAILED)
                {
                       submission.Status__c = STATUS_TRANSMISSION_FAILURE;
                } else if((oldSubmission == null || oldSubmission.ACK2_Status__c != submission.ACK2_Status__c) &&
                   submission.ACK2_Status__c == ACKNOWLEDGEMENT_STATUS_FAILED)
                {
                    submission.Status__c = STATUS_TRANSMISSION_FAILURE;
                } else if(oldSubmission == null || oldSubmission.ACK3_Status__c != submission.ACK3_Status__c)
                {
                    if(submission.ACK3_Status__c == ACKNOWLEDGEMENT_STATUS_PASSED) {
                        submission.Status__c = STATUS_COMPLETE;
                        submission.Is_Locked__c = true;
                    } else if(submission.ACK3_Status__c == ACKNOWLEDGEMENT_STATUS_FAILED) {
                        submission.Status__c = STATUS_SUBMISSION_FAILURE;
                        submission.Is_Locked__c = true;
                    }
                }
            }

            return this;
        }


       /**
         *	Method to complete Regulatory Report on completion of its Submission
        */
        public Bulkified completeReportOnSubmissionCompletion() {

            Rule unlinkedSubmissions = new Rule();
            unlinkedSubmissions.addRule(SQX_Submission_History__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);
            unlinkedSubmissions.addRule(SQX_Submission_History__c.SQX_Regulatory_Report__c, RuleOperator.NotEquals, null);

            this.resetView()
                .applyFilter(unlinkedSubmissions, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(this.view.size() > 0) {

                List<SQX_Regulatory_Report__c> reportsToComplete = new List<SQX_Regulatory_Report__c>();
                for(SQX_Submission_History__c submission : (List<SQX_Submission_History__c>) this.view) {
                    reportsToComplete.add(new SQX_Regulatory_Report__c(Id = submission.SQX_Regulatory_Report__c, Status__c = SQX_Regulatory_Report.STATUS_COMPLETE, SQX_Submission_History__c = submission.Id, Submitted_Date__c = Date.today()));
                }

                new SQX_DB().op_update(reportsToComplete, new List<SObjectField> { SQX_Regulatory_Report__c.Status__c, SQX_Regulatory_Report__c.SQX_Submission_History__c });
            }

            return this;
        }

    }

}