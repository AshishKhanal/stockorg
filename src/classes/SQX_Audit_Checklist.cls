/**
* SQX_Audit_Checklist objects class handles trigger
*/
public with sharing class SQX_Audit_Checklist{
    public with sharing class Bulkified extends SQX_BulkifiedBase{
            
            public Bulkified(List<SQX_Audit_Checklist__c> newAuditChecklist, Map<Id, SQX_Audit_Checklist__c> oldAuditChecklist){
                super(newAuditChecklist, oldAuditChecklist);
            }

            /**
            * @author Sudeep Maharjan
            * @date 2016/09/09
            * @description Transfers all the audit target belonging to audit to audid checklist target
            */
            public Bulkified addTargetToAuditChecklist(){     
                final String ACTION_NAME = 'addTargetToAudit';
            
                Rule uninitializedChecklists = new Rule();

                Boolean falseValue = false;
                uninitializedChecklists.addRule(SQX_Audit_Checklist__c.Target_Intiailized__c, RuleOperator.Equals,(Object) falseValue);
                
                this.resetView()
                    .removeProcessedRecordsFor(SQX.AuditChecklist, ACTION_NAME)
                    .applyFilter(uninitializedChecklists, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

                if(uninitializedChecklists.evaluationResult.size() > 0){

                    addToProcessedRecordsFor(SQX.AuditChecklist, ACTION_NAME, uninitializedChecklists.evaluationResult);
                    //get all the audit checklists whose targets haven't been initialized
                    List<SQX_Audit_Checklist__c> auditChecklists = (List<SQX_Audit_Checklist__c>) uninitializedChecklists.evaluationResult;
                    
                    //get audits belonging to audit checklist
                    Set<Id> auditId = getIdsForField(auditChecklists, SQX_Audit_Checklist__c.SQX_Audit__c);
                    Map<Id,SQX_Audit__c> audits = new  Map<Id,SQX_Audit__c>([SELECT Id, (SELECT Id FROM SQX_Audit_Targets__r) 
                                                                            FROM SQX_Audit__c WHERE Id IN : auditId]);
                    
                    List<SQX_Checklist_Target__c> targets = new List<SQX_Checklist_Target__c>(); 

                    for ( SQX_Audit_Checklist__c chk :  auditChecklists ){
                        
                        SQX_Audit__c audit = audits.get(chk.SQX_Audit__c);
                        if(audit.SQX_Audit_Targets__r != null){
                            for(SQX_Audit_Target__c target : audit.SQX_Audit_Targets__r){
                               SQX_Checklist_Target__c chkTarget = new SQX_Checklist_Target__c( SQX_Audit_Checklist__c = chk.Id, SQX_Audit_Target__c = target.Id);
                               targets.add(chkTarget);
                            }
                        }
                    }


                    /**
                    * WITHOUT SHARING has been used 
                    * ---------------------------------
                    * Without sharing has been used because when the audit is approved by the approver, checklist target will be copied to the audit checklist but the approver has no access to the record.
                    * This will cause and error.
                    */
                    new SQX_DB().withoutSharing().op_insert(targets, new List<SObjectField> {
                       SQX_Checklist_Target__c.SQX_Audit_Checklist__c,
                        SQX_Checklist_Target__c.SQX_Audit_Target__c
                    });
                
                }

                return this;
            }
    }
}