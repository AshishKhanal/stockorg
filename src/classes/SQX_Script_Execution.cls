/**
* This class encapsulates the script execution logics
*/
public with sharing class SQX_Script_Execution {
    
    /**
    * The code that will be called from the trigger.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        /**
        * Default constructor
        */
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newScriptExecutions equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Script_Execution__c> newScriptExecutions, Map<Id,SQX_Script_Execution__c> oldMap){
        
            super(newScriptExecutions, oldMap);
        }

        /**
        * Method to complete complaint task when script execution is created referring to the cq task.
        */
        public Bulkified completeComplaintTask(){
            final String ACTION_NAME = 'updateRelatedSFtask';

            this.resetView()
                .removeProcessedRecordsFor(SQX.ScriptExecution, ACTION_NAME);//Remove all the previously processed records

            List<SQX_Script_Execution__c> scriptExecutions = (List<SQX_Script_Execution__c>)this.view;

            if(scriptExecutions.size() > 0){
                this.addToProcessedRecordsFor(SQX.ScriptExecution, ACTION_NAME, scriptExecutions);

                Set<Id> cqTaskIds =  this.getIdsForField(scriptExecutions, SQX_Script_Execution__c.SQX_CQ_Task__c);
                Set<Id> complaintIds = this.getIdsForField(scriptExecutions, SQX_Script_Execution__c.SQX_Complaint__c);

                //complete related complaint tasks
                SQX_Complaint_Task.completeComplaintTasks(complaintIds, cqTaskIds);
            }
            return this;
        }
        
        /**
         * This method is to prevent deletion of record if parent is locked
         */
        public Bulkified preventDeletionOfLockedRecords(){
            
            this.resetView();
            
            Map<Id, SQX_Script_Execution__c> scriptExecutions = new Map<Id, SQX_Script_Execution__c>((List<SQX_Script_Execution__c>)this.view);
            
            for(SQX_Script_Execution__c scriptExecution : [SELECT Id FROM SQX_Script_Execution__c WHERE Id IN : scriptExecutions.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    scriptExecutions.get(scriptExecution.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }
   }
}