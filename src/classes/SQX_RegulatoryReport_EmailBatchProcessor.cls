/**
 * Batch to send email to reporting contact with regulatory report.
 */
global without sharing class SQX_RegulatoryReport_EmailBatchProcessor implements Database.Batchable <sObject> {

    //Instance specific configurable variable for batch size
    global Integer BATCH_SIZE = 1;

    //Instance specific configurable variable for next schedule in minutes
    global Integer SCHEDULE_AFTER_MIN = 2;

    static String JOB_NAME = 'CQ-Regulatory Report Email Batch Job Processing';

    final static String TemplateDeveloperName = 'CQ_Regulatory_Report_Email_Template';

    //this flag can be used in tests to disable rescheduling
    //for normal usage this value is always true, for tests it is set to false but can be changed
    boolean RESCHEDULE = !Test.isRunningTest();

    /**
     * Returns the query that will fetch all the pending Submission History records.
     */
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            [SELECT Id, Submitted_By__c, SQX_Submitted_By__c, SQX_Submitted_By__r.Email, SQX_Regulatory_Report__c, SQX_Regulatory_Report__r.SQX_Reporting_Default__r.SQX_CA_Contact__c
                FROM SQX_Submission_History__c
                WHERE Status__c =: SQX_Submission_History.STATUS_PENDING
                AND Delivery_Type__c =: SQX_Submission_History.DELIVERY_TYPE_EMAIL]);
    }

    /**
     * batch method that processes batch items
     */
    global void execute(Database.BatchableContext bc,List<sObject> scope) {
        if (scope.size() > 0) {
            processBatch((SQX_Submission_History__c)scope.get(0));
        }
    }

    /**
     * process batch item to send email to Reporting Contact and update status of Sbmission History accordingly.
     */
    public void processBatch(SQX_Submission_History__c sh) {
        List<Id> shIds = new List<Id>();
        List<Id> reportIds = new List<Id>();
        List<Id> ContentDocumentIds = new List<Id>();
        Map<Id, List<ContentDocumentLink>> ContentDocumentIdsMap = new Map<Id, List<ContentDocumentLink>>();
        Map<Id, ContentVersion> cVersionMap = new Map<Id, ContentVersion>();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

        for(ContentDocumentLink CDLink : [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :sh.Id]) {
            ContentDocumentIds.add(CDLink.ContentDocumentId);
            List<ContentDocumentLink> cdLinks = ContentDocumentIdsMap.get(CDLink.LinkedEntityId);
            if (cdLinks == null) {
                cdLinks = new List<ContentDocumentLink>();
                ContentDocumentIdsMap.put(CDLink.LinkedEntityId, cdLinks);
            }
            cdLinks.add(CDLink);
        }

        for ( ContentVersion cversion : [SELECT PathOnClient,
                                                VersionData,
                                                ContentDocumentId
                                                FROM ContentVersion
                                                WHERE ContentDocumentId IN :ContentDocumentIds 
                                                AND IsLatest = true]) {
            cVersionMap.put(cversion.ContentDocumentId, cversion);
        }

        Id emailTemplateId = [Select id, name FROM EmailTemplate WHERE DeveloperName = :TemplateDeveloperName].Id;

        List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
        if (ContentDocumentIdsMap.containsKey(sh.Id)) {
            for (ContentDocumentLink cdLink : ContentDocumentIdsMap.get(sh.Id)) {
                ContentVersion cVersion = cVersionMap.get(cdLink.ContentDocumentId);
                if (cVersion != null) {
                    // Create the email attachment
                    Blob fileAttachment = cVersion.VersionData;
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cVersion.PathOnClient);
                    efa.setBody(fileAttachment);
                    fileAttachments.add(efa);
                }
            }
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(emailTemplateId);
        mail.setTargetObjectId(sh.SQX_Regulatory_Report__r.SQX_Reporting_Default__r.SQX_CA_Contact__c);
        mail.setSaveAsActivity(false);

        if(!String.isBlank(sh.Submitted_By__c)) {
            mail.setSenderDisplayName(sh.Submitted_By__c);
        }

        if(!String.isBlank(sh.SQX_Submitted_By__c)) {
            mail.setReplyTo(sh.SQX_Submitted_By__r.Email);
        }

        mail.setFileAttachments(fileAttachments);

        Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail}, false);
        if (results.size() > 0) {
            if (results[0].isSuccess()) {
                sh.Status__c =  SQX_Submission_History.STATUS_COMPLETE;
                System.debug('Successfully processed record.');
                sh.Batch_Job_Error__c = '';
            } else {
                sh.Status__c =  SQX_Submission_History.STATUS_TRANSMISSION_FAILURE;
                sh.Batch_Job_Error__c =  results[0].getErrors().get(0).getMessage();
                System.debug(results[0].getErrors().get(0).getMessage());
            }

            // update submission history
            new SQX_DB().continueOnError().op_update(new List<SQX_Submission_History__c>{sh}, new List<Schema.SObjectField>{});
        }

    }

    /**
     * method is called by the batch executor to perform cleanups. In case of CQ this method reschedules the batch job processing staging.
     */
    global void finish(Database.BatchableContext bc){
        //schedule next loop.
        if (RESCHEDULE ){
            System.scheduleBatch(new SQX_RegulatoryReport_EmailBatchProcessor(), JOB_NAME, SCHEDULE_AFTER_MIN , BATCH_SIZE);
        }
    }
}