/**
 * test class for complaint create
 * @author: Anish Shrestha 
 * @date: 2016-01-15
 * @story: [SQX-937] 
 */
@isTest
public class SQX_Test_937_Complaint_Create{

    static boolean runAllTests = true,
                    run_givenAComplaint_ComplaintCanBeSavedForProductReceived = false;

    /**
     * Scenario : 1
     * Action: Complaint is saved by making product received required but not entering Return Received Date, Actual Return Quantity.
     * Expected: Complaint should not be saved and must throw error
     *
     * Scenario : 2
     * Action: Complaint is saved by making product received required and entering Return Received Date, Actual Return Quantity.
     * Expected: Complaint should be saved.
     * @date: 2016-01-15
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAComplaint_ComplaintCanBeSavedForProductReceived(){

        if(!runAllTests && !run_givenAComplaint_ComplaintCanBeSavedForProductReceived){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

            SQX_Test_Complaint complaint;
            SQX_Complaint__c complaintMain;

        System.runas(standardUser){
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, 
                                    Status__c, 
                                    Product_Received__c, 
                                    Return_Received_Date__c, 
                                    Actual_Return_Quantity__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];


            //Act 1: Make the product received required
            complaintMain.Product_Received__c = true;

            // Assert 1: Should not be able to save complaint and must throw error
            try{
                new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Product_Received__c});
                System.assert(false, 'Save should not be successful but complaint is saved.');
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Returned Received Date and Actual Return Quantity must be entered when Product Received is checked.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 

            // Act 2: Make product received required and enter Return Received Date, Actual Return Quantity.
            complaintMain.Product_Received__c = true;
            complaintMain.Return_Received_Date__c = Date.Today().addDays(-10);
            complaintMain.Actual_Return_Quantity__c = 10.00;

            // Assert 2: Save should be successful
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Product_Received__c,
                                                                                            SQX_Complaint__c.fields.Return_Received_Date__c,
                                                                                            SQX_Complaint__c.fields.Actual_Return_Quantity__c});

            complaintMain = [SELECT Id, 
                                    Status__c, 
                                    Product_Received__c, 
                                    Return_Received_Date__c, 
                                    Actual_Return_Quantity__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            System.assertEquals(10.00, complaintMain.Actual_Return_Quantity__c ,'Expected the value to be saved');

        }
    }

    /**
     * Action: Complaint is updated with blank country of origin.
     * Expected: Complaint should not be saved.
     * @date: 2018-10-25
     * @author: Kishor Bikram Oli 
     */
    public testmethod static void ensureCountryOfOriginIsRequired(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        
        System.runas(standardUser){
            //Arrange: Create a complaint record
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            complaintMain = [SELECT Id FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            //Act:  Country of origin is set to empty
            complaintMain.Country_of_Origin__c = '';
            
            Database.SaveResult result = Database.update(complaintMain, false);
            
            //Assert 1: Update should not be successful with blank country of origin.
            System.assertEquals(false, result.isSuccess(), 'Complaint record should not be saved with empty county of origin.');
            
            //Assert 2: Validation rule error message should match.
            System.assertEquals('Country of origin is required.', result.getErrors()[0].getMessage(), 'Country of origin should be entered while saving Complaint record.');
        }
    }
    
    /**
     * GIVEN : A complaint is created
     * WHEN : Occurence Date is Greater than reported date
     * THEN : Error is thrown
     */
    public testmethod static void givenAComplaint_WhenReportedDateIsLessThanOccurenceDate_ErrorIsThrown(){
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);


        System.runas(standardUser){
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Occurence Date Greater than reported date is saved
            SQX_Complaint__c comp = new SQX_Complaint__c(Id = complaint.complaint.Id);
            comp.Occurrence_Date__c = Date.today();
            comp.Reported_Date__c = Date.today().addDays(-1);
            Database.SaveResult result = Database.update(comp, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Reported Date cannot be less than Occurrence Date'), result.getErrors());

            // ACT : Occurence Date equals to reported date is saved
            comp.Reported_Date__c = Date.today();
            result = Database.update(comp, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());
        }
    }
}