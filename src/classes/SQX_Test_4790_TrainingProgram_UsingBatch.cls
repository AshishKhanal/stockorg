/**
 * This test class ensures that the step wise training generation in training program works correctly when batch job is on.
 */
@isTest
public class SQX_Test_4790_TrainingProgram_UsingBatch {
    @testSetup
    static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');
        
        SQX_Job_Function__c jf = new SQX_Job_Function__c( Name = 'Test_Training_Program' );
        SQX_Test_Job_Function jft = new SQX_Test_Job_Function(jf, false); // inactive jf        
        System.runas(admin1) {
            // add required job functions
            jft.save();
        }
        
        System.runas(user1) {
            // add required personnels
            List<SQX_Personnel__c> psns = SQX_Test_3474_RequirementProcessingBatch.createPersonnels('PSN', 1);
            insert psns;
            
            // add required active pjfs
            List<SQX_Personnel_Job_Function__c> pjfs = SQX_Test_3474_RequirementProcessingBatch.createActivePersonnelJobFunctions(psns, jf);
            pjfs[0].Active__c = false;
            insert pjfs;
        }
        
        System.runas(user2) {

            // add required documents
            SQX_Controlled_Document__c document1 = new SQX_Controlled_Document__c();
            document1.Document_Number__c = 'Doc1';
            document1.Revision__c = 'REV-1';
            document1.Title__c = 'DOC_1_TITLE';
            document1.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
            document1.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            document1.Effective_Date__c = Date.Today();

            SQX_Controlled_Document__c document2 = new SQX_Controlled_Document__c();
            document2.Document_Number__c = 'Doc2';
            document2.Revision__c = 'REV-1';
            document2.Title__c = 'DOC_2_TITLE';
            document2.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
            document2.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            document2.Effective_Date__c = Date.Today();

            SQX_Controlled_Document__c document3 = new SQX_Controlled_Document__c();
            document3.Document_Number__c = 'Doc3';
            document3.Revision__c = 'REV-1';
            document3.Title__c = 'DOC_3_TITLE';
            document3.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
            document3.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            document3.Effective_Date__c = Date.Today();
            new SQX_DB().op_insert(new List<SQX_Controlled_Document__c> { document1, document2, document3 },  new List<Schema.SObjectField>{});
            
            // add required doc requirement
            List<SQX_Requirement__c> requirements = new List<SQX_Requirement__c> {
                new SQX_Requirement__c(
                    SQX_Controlled_Document__c = document1.Id,
                    SQX_Job_Function__c = jf.Id,
                    Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                    Training_Program_Step__c = null,
                    Active__c = false
                ),
                new SQX_Requirement__c(
                    SQX_Controlled_Document__c = document2.Id,
                    SQX_Job_Function__c = jf.Id,
                    Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                    Training_Program_Step__c = 1,
                    Active__c = false
                ),
                new SQX_Requirement__c(
                    SQX_Controlled_Document__c = document3.Id,
                    SQX_Job_Function__c = jf.Id,
                    Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                    Training_Program_Step__c = 2,
                    Active__c = false
                )

            };
            insert requirements;
        }
    }
    
    /**
     * Given : Batch job is onn and 3 requirements having first requiremtnt's step null, second's step 1 and third's step 2, also step of pjf is null
     * When : Job function is activated
     * Then : Step of pjf should be update to 0.
     */
    static testmethod void ensureStepOfPJFIsUpdatedWithZeroWhenFirstStepOfRequirementIsBlank() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        System.runas(users.get('admin1')) {
            // enable use batch job custom setting to process training
            SQX_Test_3474_RequirementProcessingBatch.setTrainingBatchJobCustomSetting(true);
            
            // Act : activate jf
            List<SQX_Job_Function__c> jf = [SELECT Id FROM SQX_Job_Function__c WHERE Name = 'Test_Training_Program'];
            jf[0].Active__c = true;
            update jf;
            List<SQX_Controlled_Document__c> docs = [ SELECT Id, 
                                                             Document_Status__c, 
                                                             (SELECT Id FROM SQX_Requirements__r)
                                                      FROM SQX_Controlled_Document__c ORDER BY Document_Number__c];
            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[0].SQX_Requirements__r[0].Id));
            // Assert : step of pjf to be zero as step of requirement is blank
            List<SQX_Personnel_Job_Function__c> pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 0, 'Expected step of pjf to be zero as step of requirement is blank but got ' + pjfs[0].Current_Training_Program_Step__c);

            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[1].SQX_Requirements__r[0].Id));

            // Assert : step of pjf to be zero as step of requirement is blank
            pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 0, 'Expected step of pjf to be zero as step of requirement is blank but got ' + pjfs[0].Current_Training_Program_Step__c);

            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[2].SQX_Requirements__r[0].Id));

            // Assert : step of pjf to be zero as step of requirement is blank
            pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 0, 'Expected step of pjf to be zero as step of requirement is blank but got ' + pjfs[0].Current_Training_Program_Step__c);
            
            // Assert : Ensure one training is created as there is one requirements with step blank
            List<SQX_Personnel_Document_Training__c> pdt = [SELECT Id FROM SQX_Personnel_Document_Training__c];
            System.assert(pdt.size() == 1, 'Expected one trainings but got ' + pdt.size());
            
            // Assert : Ensure no pdjf is created for step 1 and 2
            List<SQX_Personnel_Document_Job_Function__c> pdjf = [SELECT Id FROM SQX_Personnel_Document_Job_Function__c WHERE SQX_Requirement__c IN : new List<Id> {docs[2].SQX_Requirements__r[0].Id, docs[1].SQX_Requirements__r[0].Id}];
            System.assert(pdjf.size() == 0, 'Expected no pdjf for requirement with step 2 but got ' + pdjf.size() + ' pdjf');
        }
    }
    
    /**
     * Given : Batch job is onn and 3 requirements having first requiremtnt's step 1, second's step 1 and third's step 2, also step of pjf is null
     * When : Job function is activated
     * Then : Step of pjf should be update to 1.
     */
    static testmethod void ensureStepOfPJFIsCorrectlyUpdatedWhenStepOfRequirementIsNotBlank() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        System.runas(users.get('admin1')) {
            // Arrange : enable use batch job custom setting to process training
            SQX_Test_3474_RequirementProcessingBatch.setTrainingBatchJobCustomSetting(true);
            
            List<SQX_Controlled_Document__c> docs = [ SELECT Id, 
                                                             Document_Status__c, 
                                                             (SELECT Id FROM SQX_Requirements__r)
                                                      FROM SQX_Controlled_Document__c ORDER BY Document_Number__c];
            
            new SQX_DB().op_update(new List<SQX_Requirement__c> {new SQX_Requirement__c(Id=docs[0].SQX_Requirements__r[0].Id, Training_Program_Step__c = 1 )}, new List<Schema.SObjectField> {});
            
            // Act : activate jf
            List<SQX_Job_Function__c> jf = [SELECT Id FROM SQX_Job_Function__c WHERE Name = 'Test_Training_Program'];
            jf[0].Active__c = true;
            new SQX_DB().op_update(jf, new List<Schema.SObjectField> {});
            
            // pass the requirement to process
            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[0].SQX_Requirements__r[0].Id));
            // Assert : step of pjf to be 1 as step of requirement is 1
            List<SQX_Personnel_Job_Function__c> pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 1, 'Expected step of pjf to be 1 as step of requirement is 1 but got ' + pjfs[0].Current_Training_Program_Step__c);

            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[1].SQX_Requirements__r[0].Id));

            // Assert : step of pjf to be 1 as step of requirement is 1
            pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 1, 'Expected step of pjf to be 1 as step of requirement is 1 but got ' + pjfs[0].Current_Training_Program_Step__c);

            SQX_Personnel_Document_Job_Function.processActivatedRequirementUsingBatch(SQX_Test_3474_RequirementProcessingBatch.getRequirement(docs[2].SQX_Requirements__r[0].Id));

            // Assert : step of pjf to be 1 as step of requirement is 1
            pjfs = [SELECT Current_Training_Program_Step__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf[0].Id];
            System.assert(pjfs[0].Current_Training_Program_Step__c == 1, 'Expected step of pjf to be 1 as step of requirement is 1 but got ' + pjfs[0].Current_Training_Program_Step__c);
            
            // Assert : Ensure two trainings are created as there are two requirements with step 1
            List<SQX_Personnel_Document_Training__c> pdt = [SELECT Id FROM SQX_Personnel_Document_Training__c];
            System.assert(pdt.size() == 2, 'Expected two trainings but got ' + pdt.size());
            
            // Assert : Ensure no pdjf is created for step 2
            List<SQX_Personnel_Document_Job_Function__c> pdjf = [SELECT Id FROM SQX_Personnel_Document_Job_Function__c WHERE SQX_Requirement__c = : docs[2].SQX_Requirements__r[0].Id];
            System.assert(pdjf.size() == 0, 'Expected no pdjf for requirement with step 2 but got ' + pdjf.size() + ' pdjf');
        }
    }
}