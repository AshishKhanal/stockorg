/**
* As a Customer
* I want see the list of Supplier who are Approved to provide Parts and Services 
* So that I can see the complete list of Approved Suppliers or Filter the Approved Suppliers by parts, service provided by location.
* If Supplier is not Approved, All parts/Service are not approved
* Part/Service is Active only when it is Approved
* InActive Part/Service can be Approved or Not Approved
*
* @author Pradhanta Bhandari
* @date 2014/2/4
* 
*/
@IsTest
public class SQX_Test_Account {

    /**
    * @param description adds a supplier service to the account as active and approved
    * @param supplierService the supplier service to be added, if it is null then a new supplier service is added
    * @param supplier the account/supplier for which the supplier service is to be added
    * @param active flag to set whether the supplier service is active or not
    * @param approved flag to set whether the supplier service is approved or not
    * @param return returns the supplier service that has been inserted.
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    */
    private static SQX_Supplier_Service__c addSupplierService(SQX_Supplier_Service__c supplierService, Account supplier, boolean active, boolean approved){
        SQX_Supplier_Service__c addedSupplierService = null;
        
        if(supplierService == null){
            integer randomNumber = (integer)(Math.random() * 1000000);
            SQX_Standard_Service__c standardService = new SQX_Standard_Service__c();
            standardService.Name = 'Service-' + randomNumber;

            insert standardService;

            addedSupplierService = new SQX_Supplier_Service__c(Account__c = supplier.Id, Standard_Service__c = standardService.Id, Active__c = active, Approved__c = approved);


        }
        else{
            addedSupplierService = supplierService;
        }

        insert addedSupplierService;
       
        return addedSupplierService;
    }


    /**
    * @param description adds a supplier part to the account as active, since the part is active and approved
    * @param supplierPart the supplier part to be added, if it is null then a new supplier part is added
    * @param supplier the account/supplier for which the supplier part is to be added
    * @param active flag to set whether the supplier part is active or not
    * @param approved flag to set whether the supplier part is approved or not    
    * @param return returns the supplier part that has been inserted, if it is new supplier part it contains the reference to the part in Part__r attribute
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5    
    */
    private static SQX_Supplier_Part__c addSupplierPart(SQX_Supplier_Part__c supplierPart, Account supplier, boolean active, boolean approved ){
        SQX_Supplier_Part__c addedSupplierPart = null;
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, 'Standard User Profile', mockRole);

        System.runas(adminUser){
            if(supplierPart == null){
                SQX_Part__c part = SQX_Test_Part.insertPart(null, adminUser, true, 'Expected parts to be inserted successfully');
                addedSupplierPart = new SQX_Supplier_Part__c(Part__c = part.Id, Account__c = supplier.Id, Active__c = active, Approved__c = approved );
            }
            else{
                addedSupplierPart = supplierPart;
            }

            insert addedSupplierPart;
        }
        

        return addedSupplierPart;
    }

    /**
    *  Given that Supplier is Approved and Active
    *  When Supplier is now DisApproved
    *  Then Supplier becomes Inactive, all the parts and Service become DisApproved and Inactive
    *  And We cannot Activate the Supplier and Cannot Active or Approve any Part and Service for the Supplier
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5    
    */
    public static testmethod void givenApprovedActiveSupplier_DisapprovalPropagates(){

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, 'Standard User Profile', mockRole);
        System.runas(adminUser){
            //setup the given conditinons
            Account supplier = SQX_Test_Account_Factory.createAccount();
            supplier.Status__c = SQX_Account.STATUS_ACTIVE;
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_APPROVED ;

            update supplier; //approved and active account

            SQX_Supplier_Part__c sp1 = addSupplierPart(null, supplier, true, true); //active and approved
            SQX_Supplier_Service__c ss1 = addSupplierService(null, supplier, true, true);

            //now disapprove the account
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_DISAPPROVED ;
            Database.SaveResult result = Database.Update(supplier, false);

            System.assert(result.isSuccess() == true, 'Expected the disapproval to be successfull, but failed');

            //supplier becomes inactive
            supplier = [SELECT Id, Status__c, Approval_Status__c, Active__c FROM Account WHERE Id = : supplier.Id];

            System.assert(supplier.Status__c == SQX_Account.STATUS_INACTIVE , 'Expected the supplier to be inactive, but found it ' + supplier.Status__c );

            //ensure all supplier service and supplier parts are disapproved and inactive
            sp1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Part__c WHERE Id = : sp1.Id LIMIT 1];
            ss1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Service__c WHERE Id = : ss1.Id LIMIT 1];

            System.assert(sp1.Active__c == false && sp1.Approved__c == false, 
                    'Expected supplier part to be inactive and disapproved, but found it Active: ' + sp1.Active__c + ' and Approved: ' + sp1.Approved__c);

            System.assert(ss1.Active__c == false && ss1.Approved__c == false, 
                    'Expected supplier service to be inactive and disapproved, but found it Active: ' + ss1.Active__c + ' and Approved: ' + ss1.Approved__c);


            //check if we can activate the supplier
            supplier.Status__c = SQX_Account.STATUS_ACTIVE;

            result = Database.update(supplier, false);
            System.assert(result.isSuccess() == false || [SELECT Status__c FROM Account WHERE Id = : supplier.Id LIMIT 1].Status__c == SQX_Account.STATUS_INACTIVE ,
             'Expected the supplier to be inactive  because supplier is disapproved.');


            sp1.Active__c = true;
            result = Database.update(sp1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier part  activation to fail but found it succeeding');

            ss1.Active__c = true;
            result = Database.update(ss1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier service  activation to fail but found it succeeding');

            sp1.Active__c = false;
            sp1.Approved__c = true;
            result = Database.update(sp1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier part approval to fail but found it succeeding');


            ss1.Active__c = false;
            ss1.Approved__c = true;
            result = Database.update(ss1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier service  activation to fail but found it succeeding');
        }
        


    }

    /**
    * Given that Supplier is Approved and Active
    * When Supplier is InActivated
    * Then Approved Parts and Service remains Approved, but are all InActivated and cannot be Activated
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5 
    */
    public static testmethod void givenApprovedActiveSupplier_InactivationPropagates(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, 'Standard User Profile', mockRole);
        System.runas(adminUser){
            //setup the given conditinons
            Account supplier = SQX_Test_Account_Factory.createAccount();
            supplier.Status__c = SQX_Account.STATUS_ACTIVE;
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_APPROVED ;

            update supplier; //approved and active account

            SQX_Supplier_Part__c sp1 = addSupplierPart(null, supplier, true, true); //active and approved
            SQX_Supplier_Service__c ss1 = addSupplierService(null, supplier, true, true);

            //now inactive the account
            supplier.Status__c = SQX_Account.STATUS_INACTIVE ;
            update supplier;

            //assert
            sp1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Part__c WHERE Id = : sp1.Id LIMIT 1];
            ss1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Service__c WHERE Id = : ss1.Id LIMIT 1];

            System.assert(sp1.Active__c == false && sp1.Approved__c == true, 
                    'Expected supplier part to be inactive and approved, but found it Active: ' + sp1.Active__c + ' and Approved: ' + sp1.Approved__c);

            System.assert(ss1.Active__c == false && ss1.Approved__c == true, 
                    'Expected supplier service to be inactive and approved, but found it Active: ' + ss1.Active__c + ' and Approved: ' + ss1.Approved__c);

            //activation should fail
            sp1.Active__c = true;
            Database.SaveResult result = Database.update(sp1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier part  activation to fail but found it succeeding');

            ss1.Active__c = true;
            result = Database.update(ss1, false);
            System.assert(result.isSuccess() == false, 'Expected the supplier service  activation to fail but found it succeeding');
        }

    }


    /**
    * Given that Supplier is InActive and Approved
    * When the Supplier is Activated
    * Then the status of Parts and Service are not changed, but can be changed
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    */
    public static testmethod void givenInactiveApprovedSupplier_ActivationDoesntChangeRelatedObjects(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, 'Standard User Profile', mockRole);
        System.runas(adminUser){
            //setup the given conditinons
            Account supplier = SQX_Test_Account_Factory.createAccount();
            supplier.Status__c = SQX_Account.STATUS_INACTIVE ;
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_APPROVED ;

            update supplier; //approved and active account

            SQX_Supplier_Part__c sp1 = addSupplierPart(null, supplier, false, true); //active and approved
            SQX_Supplier_Service__c ss1 = addSupplierService(null, supplier, false, true);



            sp1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Part__c WHERE Id = : sp1.Id LIMIT 1];
            ss1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Service__c WHERE Id = : ss1.Id LIMIT 1];

            //check current status
            System.assert(sp1.Active__c == false && sp1.Approved__c == true, 'Expected supplier part to be inactive');
            System.assert(ss1.Active__c == false && sp1.Approved__c == true, 'Expected supplier service to be inactive');


            //change the supplier status to active and ensure that supplier part and service are not changed
            supplier.Status__c = SQX_Account.STATUS_ACTIVE;
            update supplier;

            sp1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Part__c WHERE Id = : sp1.Id LIMIT 1]; // same tests and query because we expect the same output
            ss1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Service__c WHERE Id = : ss1.Id LIMIT 1];

            //ensure the status has not changed
            System.assert(sp1.Active__c == false && sp1.Approved__c == true, 'Expected supplier part to be inactive');
            System.assert(ss1.Active__c == false && sp1.Approved__c == true, 'Expected supplier service to be inactive');


            sp1.Active__c = true;
            Database.SaveResult result = Database.Update(sp1, false);
            System.assert(result.isSuccess() == true, 'Expected the activation of supplier part to go through, but found it failing');

            ss1.Active__c = true;
            result = Database.Update(ss1, false);
            System.assert(result.isSuccess() == true, 'Expected the activation of supplier service to go through, but found it failing');
        }
    }


    /**
    * Given that Suppleir is InActive and DisApproved
    * When Supplier is Approved
    * Then Supplier can be Activated, Parts and Suppleir can be Activated as well, but can only be Approved when Supplier is Approved
    *
    * @author Pradhanta Bhandari
    * @date 2014/2/5
    */
    public static testmethod void givenInactiveDisApprovedSupplierIsApproved_ActivationsCanBeDone(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, 'Standard User Profile', mockRole);
        System.runas(adminUser){
            //setup the given conditions
            Account supplier = SQX_Test_Account_Factory.createAccount();
            supplier.Status__c = SQX_Account.STATUS_INACTIVE ;
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_DISAPPROVED ;

            update supplier; //approved and active account

            SQX_Supplier_Part__c sp1 = addSupplierPart(null, supplier, false, false); //active and approved
            SQX_Supplier_Service__c ss1 = addSupplierService(null, supplier, false, false);


            //now approve the supplier
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_APPROVED ;
            Database.SaveResult result = Database.Update(supplier, false);

            System.assert(result.isSuccess() == true, 'Expected supplier approval to go through');


            //try activating the supplier
            supplier.Status__c = SQX_Account.STATUS_ACTIVE;
            result = Database.Update(supplier, false);

            System.assert(result.isSuccess() == true, 'Expected supplier activation to go through');

            Account currentSupplier = [SELECT Id, Status__c, Approval_Status__c FROM Account WHERE Id = : supplier.Id LIMIT 1];
            System.assert(currentSupplier.Status__c == SQX_Account.STATUS_ACTIVE && currentSupplier.Approval_Status__c == SQX_Account.APPROVAL_STATUS_APPROVED ,
                    'Expected supplier to be active and approved but found ' + currentSupplier);

            //now try activating the service and part note that activation of service requires approval
            sp1.Approved__c = sp1.Active__c = true;
            ss1.Approved__c = ss1.Active__c = true;

            result = Database.update(sp1, false);
            System.assert(result.isSuccess() == true, 'Expected supplier part activation and approval to go through');

            result = Database.update(ss1, false);
            System.assert(result.isSuccess() == true, 'Expected supplier service activation and approval to go through');

            sp1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Part__c WHERE Id = : sp1.Id LIMIT 1]; // same tests and query because we expect the same output
            ss1 = [SELECT Active__c, Approved__c FROM SQX_Supplier_Service__c WHERE Id = : ss1.Id LIMIT 1];

            System.assert(sp1.Active__c == true && sp1.Approved__c == true, 'Expected supplier part to be activated and approved');
            System.assert(ss1.Active__c == true && ss1.Approved__c == true, 'Expected supplier service to be activated and approved');

            //now try disapproving the account for the last part of test
            supplier.Approval_Status__c = SQX_Account.APPROVAL_STATUS_DISAPPROVED ;
            result = Database.Update(supplier, false);

            sp1.Approved__c = false;
            ss1.Approved__c = false;

            result = Database.update(sp1, false);
            System.assert(result.isSuccess() == false, 'Expected supplier part approval to fail when supplier is not approved');

            result = Database.update(ss1, false);
            System.assert(result.isSuccess() == false, 'Expected supplier service approval to fail when supplier is not approved');
        }

    }


}