/**
* @author Pradhanta Bhandari
* @date 2014/1/16
* @description unit test for SQX upserter
*/
@IsTest
public class SQX_Test_Upserter {

    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/16
    * @description primitive unit test to check a default record can be upserted
    */
    public static testmethod void  givenSimpleRecordUpsertIt(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","AccountId":"mockAccount1","Id":"mockContact1"},{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/a0P9000000EXB8LEAX"},"Type":"Jibberish", "Name":"I am an account","Account__c":"1231","' + SQX.getNSNameFor('Sic')  + '":"123123123","Id":"mockAccount1"},{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Jane","AccountId":"mockAccount1","Id":"mockContact2"}]}';
        

        SQX_Upserter.UpsertRecords(validJSON);
        

        
        //assert if the records are indeed created
        Account acc = [SELECT ID, Name, Type
                                  FROM Account
                                  WHERE Sic = '123123123' LIMIT 1];
                                  
        System.assertEquals([SELECT ID FROM Contact WHERE accountID = : acc.Id].size(), 2);
        
        System.assert(acc.Name == 'I am an account',
                     'Expected referenced name to be I am an account but found it ' +
                     acc.Name);
                     
        System.assert(acc.Type == 'Jibberish',
             'Expected account type to be saved but found it failing');
                     

        try{
            string changedJSON = '{"changeSet":[{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"' + SQX.getNSNameFor('Sic')  + '":"gvc-changed",'
            +' "Id":"' + acc.ID + '","originalValues":{"' + SQX.getNSNameFor('Sic')  + '":"123123121"}}]}';
            
            SQX_Upserter.UpsertRecords(changedJSON );
            
            System.assert(false, 'Expected an concurrency exception to occur but failed to occur.');
        }
        catch(SQX_Upserter.UpserterException ex){
            //exception should occur
        }
        
        string changedJSON = '{"changeSet":[{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"' + SQX.getNSNameFor('Sic')  + '":"gvc-changed",'
            +' "Id":"' + acc.ID + '","originalValues":{"' + SQX.getNSNameFor('Sic')  + '":"123123123"}}]}';
            
            
        SQX_Upserter.UpsertRecords(changedJSON );
        
        acc = [SELECT ID, Name, Sic 
                  FROM Account
                  WHERE ID = :acc.ID LIMIT 1];
        
        System.assert(acc.Sic == 'gvc-changed',
                     'Expected government id to be gvc-changed but found it ' +
                     acc.Sic);
        
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description this test ensures that the omit field functionality works
    */
    public static testmethod void givenAFieldIsOmitted_ItIsntPersisted(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","AccountId":"mockAccount1","Id":"mockContact1"},{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/a0P9000000EXB8LEAX"},"Type":"Jibberish", "Name":"I am an account","Account__c":"1231","' + SQX.getNSNameFor('Sic')  + '":"123123123","Id":"mockAccount1"},{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Jane","AccountId":"mockAccount1","Id":"mockContact2"}]}';
        
        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config()
                                                      .omit('Account', Account.Type);

        SQX_Upserter.UpsertRecords(validJSON, config);

        //assert if the records are indeed created
        Account acc = [SELECT ID, Name, Type
                                  FROM Account
                                  WHERE Sic = '123123123' LIMIT 1];
        
        System.assert(acc.Type != 'Jibberish',
                     'Expected account type to be ignored, but found it persisted ' +
                     acc);

    }
    


    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description this test makes sure that when expectations are failed nothing is persisted
    */
    public static testmethod void givenAExpectationIsFailed_ItIsPersisted(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","AccountId":"mockAccount1","Id":"mockContact1"},{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/a0P9000000EXB8LEAX"},"Name":"I am an account","Account__c":"1231","' + SQX.getNSNameFor('sic')  + '":"123123123","Id":"mockAccount1"},{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Jane","AccountId":"mockAccount1","Id":"mockContact2", "Type":"Jibberish"}]}';
        
        Expectation expectationChecker = new Expectation();
        expectationChecker.expectationResult = false;
        expectationChecker.canManipulateDataResult = true;
        

        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config()
                                                      .setInterceptor(expectationChecker)
                                                      .omit('Account', Account.Type);

        SQX_Upserter.UpsertRecords(validJSON, config);

        //assert if the records are indeed created
        List<Account> acc = [SELECT ID, Name, Type
                                  FROM Account
                                  WHERE sic = '123123123'];
        
        System.assert(acc.size() == 0,
                     'Expected data to not be persisted but found it persisted ');

    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description this test checks whether default ignored fields are ignored
    */
    public static testmethod void givenADefaultIgnoredField_ItIsNotUsed(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","Id":"mockContact1", "OwnerId": "123123123"}]}';
        
        //there is an invalid OwnerID, by default an exception would occur but since this field is ignored nothing happens 
        SQX_Upserter.UpsertRecords(validJSON);

        //assert if the records are indeed created
        List<Contact> acc = [SELECT ID, Name
                                  FROM Contact
                                  WHERE LastName = 'Doe'];
        System.assert(acc.size() == 1,
                     'Expected data to be persisted but found it not persisted ');

    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description this test makes sure that when expectations are met it is persisted
    */
    public static testmethod void givenAExpectationIsMet_ItIsPersisted(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","AccountId":"mockAccount1","Id":"mockContact1"},{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/a0P9000000EXB8LEAX"},"Name":"I am an account","Account__c":"1231","' + SQX.getNSNameFor('sic')  + '":"123123123","Id":"mockAccount1"},{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Jane","AccountId":"mockAccount1","Id":"mockContact2", "Account_Type__c":"Jibberish"}]}';
        
        Expectation expectationChecker = new Expectation();
        expectationChecker.expectationResult = true;
        expectationChecker.canManipulateDataResult = true;
        

        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config()
                                                      .setInterceptor(expectationChecker)
                                                      .omit('Account', Account.Type);

        SQX_Upserter.UpsertRecords(validJSON, config);

        //assert if the records are indeed created
        List<Account> acc = [SELECT ID, Name, Type
                                  FROM Account
                                  WHERE sic = '123123123'];
        
        System.assert(acc.size() == 1,
                     'Expected data to be persisted but found it not persisted ');

    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description this test makes sure that when expectations are met, but can not manipulate data it is not persisted
    */
    public static testmethod void givenAManipulationRuleIsNotMet_ItIsNotPersisted(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","Id":"mockContact1"}]}';
        
        Expectation expectationChecker = new Expectation();
        expectationChecker.expectationResult = true;
        expectationChecker.canManipulateDataResult = false;
        

        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config()
                                                      .setInterceptor(expectationChecker);

        SQX_Upserter.UpsertRecords(validJSON, config);

        //assert if the records are indeed created
        List<Contact> contact = [SELECT ID, LastName
                                  FROM Contact
                                  WHERE LastName = 'Doe'];
        
        System.assert(contact.size() == 0,
                     'Expected data to be not persisted but found it persisted ');

    }

    /*
    * @author Pradhanta Bhandari
    * @date 2014/3/13
    * @description This test ensures that a changeset can be cleared of a persisted set
    * 
    * test commented out because removal of persisted entities in change set has been left to the JS UI and functionality is no longer present
    public static testmethod void givenAChangeSetHasBeenPersisted_RemoveIt(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","Id":"mockContact1"}]}';
        
        Expectation expectationChecker = new Expectation();
        expectationChecker.expectationResult = true;
        expectationChecker.canManipulateDataResult = true;
        

        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config()
                                                      .setInterceptor(expectationChecker);

        SQX_Upserter.UpsertRecords(validJSON, config);


        String actualChangeSet = '{"changeSet":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Doe","Id":"mockContact1"}'+
                                  ',{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/a0P9000000EXB8LEAX"},"LastName":"Jane","Id":"mockContact2"}]}';
        
        
        String newChangeSet = SQX_Upserter.getNewChangeSetAfterRemovingPersistedOnes(actualChangeSet, expectationChecker.objectsPersisted, expectationChecker.setWithTempIdToPersistedObject);

        System.assert(newChangeSet.contains('mockContact1') == false,
          'Expected mockContact1 to be removed but found ' + newChangeSet);

        List<Map<String, Object>> newChangeSetDeserialized = SQX_Upserter.getChangeSet(newChangeSet); //ensures that JSON is valid

        System.assert(newChangeSetDeserialized.size() == 1,
          'Expected single element to be in the new change set but found ' + newChangeSetDeserialized);

        System.assert(newChangeSetDeserialized.get(0).get('Id') == 'mockContact2',
          'Expected to find mockContact2 but could not find it');


    }*/

    public class Expectation implements SQX_Upserter.SQX_Upserter_Interceptor{
        public boolean expectationResult {get; set;}
        public boolean canManipulateDataResult {get; set;}
        public List<sObject> objectsPersisted {get; set;}
        public Map<String, sObject> setWithTempIdToPersistedObject {get; set;}

        public boolean meetsExpectation(Map<String, List<sObject>> objectsPersisted){
          return expectationResult;
        }
        
        public void successfullyPersisted(Map<String, List<sObject>> objectsPersisted,  Map<String, sObject> setWithTempIdToPersistedObject){
            //do nothing on this one
          this.objectsPersisted = new List<sObject>();
            for(List<sObject> objPersisted : objectsPersisted.values()){
              this.objectsPersisted.addAll( objPersisted);
            }
            this.setWithTempIdToPersistedObject = setWithTempIdToPersistedObject;
        }

        public boolean canManipulate(String sObjectType, sObject objectToBeManipulated, boolean isInsertion){

          return canManipulateDataResult;
        }
        
        public String getProcessedRecordId(){
            return null;
        }
    }


    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/13
    * @description primitive unit test to insert a single record and update it
    */
    public static testmethod void  givenSingleRecordCreateAndUpdateIt(){
        String validJSON = '{"changeSet":[{"attributes":{"type":"Contact"}, "FirstName": "Ramesh232", "LastName":"Doe101","Id":"mockContact1"}]}';
        
        SQX_Upserter.UpsertRecords(validJSON);
        
        //assert if the records are indeed created
        List<Contact> contacts = [SELECT ID, FirstName, LastName
                                  FROM Contact
                                  WHERE FirstName = 'Ramesh232'];
        
        System.assert(contacts.size() == 1, 'Expected to find the saved contact but found it missing');
                     
                     
        String updateJSON = '{"changeSet":[{"attributes":{"type":"Contact"}, "FirstName": "Ramesh234", "Id":"' + contacts.get(0).Id + '", "originalValues": {"FirstName" : "Ramesh232"}}]}';
        
        SQX_Upserter.UpsertRecords(updateJSON);

        Contact contactUpdated = [SELECT ID, FirstName, LastName
                                  FROM Contact
                                  WHERE Id = : contacts.get(0).Id];
        
        System.assert(contactUpdated.FirstName == 'Ramesh234', 'Expected the contact to be updated to Ramesh234 but found ' + contactUpdated);
        
    }

    /**
     * test for upserter order
     * ACTION: Initally complete complaint and then answer all product history checklist and add it in changeset
     * EXPECTED: All the reocord are saved without any error
     * @author: Anish Shrestha
     * @date: 2016-01-11
     */
    // public static testmethod void givenComplaint_ProductHistoryMustBeAnsweredToCompletePHR(){
        /**
         *Arrange: Create a task with task type phr,
         *         Create the complaint and
         *          add the task to the complaint policy related to that comlaint   
         *          initiate the complaint
        */
        /*UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_Task_Library task1, task2;
        System.runAs(adminUser){
            task1 = new SQX_Test_Task_Library();
            task1.task.Task_Type__c = SQX_Task.TYPE_PHR ;
            task1.task.Record_Type__c = SQX_Task.RTYPE_COMPLAINT;
            task1.task.Allowed_Days__c = 30;
            task1.save();

            task2 = new SQX_Test_Task_Library();
            task2.task.Task_Type__c = SQX_Task.TYPE_PHR ;
            task2.task.Record_Type__c = SQX_Task.RTYPE_COMPLAINT;
            task2.task.Allowed_Days__c = 30;
            task2.save();

            //insert two task of type phr
            List<SQX_Task__c> tasksToBeInserted =  new List<SQX_Task__c>();
            List<SQX_Task_Question__c> questionToBeInserted = new List<SQX_Task_Question__c>();

            tasksToBeInserted = [SELECT Id FROM SQX_Task__c];

            for(SQX_Task__c task : tasksToBeInserted){

                SQX_Task_Question__c taskQuestion = new SQX_Task_Question__c(
                                                    QuestionText__c =  'Question ' + task.Id,
                                                    SQX_Task__c = task.Id);

                questionToBeInserted.add(taskQuestion);
            }

            insert questionToBeInserted;
        }

        System.runas(standardUser){

            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            List<SQX_Complaint_Task__c> complaintTasks = [SELECT Id, SQX_Task__c FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c =: complaint.complaint.Id];

            System.assertEquals(2, complaintTasks.size(), 'Expected two complaint tasks to be created but found: ' + complaintTasks.size());

            // Act: Initiate the complaint and complete the PHR and answer all phr checklist
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            SQX_Product_History_Review__c phr = new SQX_Product_History_Review__c();
            phr = [SELECT Id, Product_History_Review_Summary__c, Status__c FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c =: complaintMain.Id];

            // Assert 1: Expected one phr to be created after initiating the complaint
            System.assert(phr.Id != null, 'Expected one phr to be created after initiating the complaint');

            // complete all phr checklist
            List<SQX_Product_History_Answer__c> phrAnswer = new List<SQX_Product_History_Answer__c>();
            phrAnswer = [SELECT Id, Question__c, Answer__c FROM SQX_Product_History_Answer__c WHERE SQX_Product_History_Review__c =: phr.Id];

            phr.Product_History_Review_Summary__c = 'Random Summary';
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Completed_By__c = 'Sagar';
            phrAnswer[0].Answer__c = 'Yes';

            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

            changeSet.addChanged(phr)
                     .addOriginalValue(SQX_Product_History_Review__c.Product_History_Review_Summary__c, null)
                     .addOriginalValue(SQX_Product_History_Review__c.Status__c, SQX_Product_History_Review.STATUS_DRAFT)
                     .addOriginalValue(SQX_Product_History_Review__c.Completed_By__c, null);


            changeSet.addChanged(phrAnswer[0])
                     .addOriginalValue(SQX_Product_History_Answer__c.Answer__c, null)
                     .addOriginalValue(SQX_Product_History_Answer__c.Question__c, phrAnswer[0].Question__c);


            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'save');
            params.put('comment', 'Mock comment');

            // Save the complaint
            System.assertEquals(complaint.complaint.Id,
                SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, changeSet.toJSON(), null , params, null ), 'Expected to successfully submit reponse');

            phr = [SELECT Id, Status__c FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c =: complaintMain.Id];

            // Assert: Expected the PHR to be complete
            System.assertEquals(SQX_Product_History_Review.STATUS_COMPLETE, phr.Status__c, 'Expected the PHR to be complete but found: ' + phr.Status__c);
        }

    } */


    /**
    * a. Given a temporary object i.e. one with temp Id the attachment is saved to the new object [Similar to saving when object hasn't been saved]
    * b. Given a main object is saved but attachment is saved in temp store it is saved to the new object [Similar to saving when object has been saved. But attachment is in temp store response]
    * c. Given both main object and attachment have been saved then attachment is saved [Similar to editing when main object has been saved]
    */
    static testmethod void givenAttachmentInObjectItWorksProperly(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);


        System.runAs(standardUser){
          // Arrange: Create a temp store where the attachment will be saved
          SQX_TempStore__c store = SQX_TempStore.storeObject('User', standardUser.Id, 'Test object', '', '');

          Attachment att = new Attachment();
          att.Body = Blob.valueOf('This is an test attachment');
          att.Name = 'Test.txt';

          // upload the attachment
          att = SQX_TempStore.storeTemporaryAttachment(standardUser.Id, 'Temp-1', att);

          // Act: save both the temporary object and the attachment
          SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
          changeset.addChanged('Temp-1', new SQX_Test_Change_Order().changeOrder);
          changeset.addChanged(att).addRelation(Attachment.ParentId, 'Temp-1');

          SQX_Upserter.UpsertRecords(changeSet.toJSON());

          // Assert: a. ensure that the attachment is deleted
          //         b. Change Order is saved
          //         c. New change Order has the attachment
          System.assertEquals(0, [SELECT Id FROM Attachment WHERE Id = : att.Id].size(), 'Expected the temp attachment to be deleted but found it');
          SQX_Change_Order__c co = [SELECT Id FROM SQX_Change_Order__c]; // since we have only created one change order fetching it is like asserting
          List<Attachment> fetchedAtt = [SELECT Id, Body, Name FROM Attachment WHERE ParentId = : co.Id];
          System.assertEquals(att.Body.toString(), fetchedAtt.get(0).Body.toString(), 'Body should be same');
          System.assertEquals(att.Name, fetchedAtt.get(0).Name, 'Name should be same');



          // Act: save the attachment in temp store
          att.Id = null;
          att.Name = 'Test-2.txt';
          att = SQX_TempStore.storeTemporaryAttachment(standardUser.Id, co.Id, att);

          changeSet = new SQX_Test_ChangeSet();
          changeset.addChanged(new Attachment(Id = att.Id, ParentId = co.Id, Name = att.Name));
          

          SQX_Upserter.UpsertRecords(changeSet.toJSON());

          // Assert: a. ensure that attachment is deleted
          //         b. Change Order has new attachment
          System.assertEquals(0, [SELECT Id FROM Attachment WHERE Id = : att.Id].size(), 'Expected the temp attachment to be deleted but found it');
          fetchedAtt = [SELECT Id, Name FROM Attachment WHERE ParentId = : co.Id AND Name = :att.Name];
          System.assertEquals(1, fetchedAtt.size(), 'Couldn\'t find matching attachment' + fetchedAtt);


          // Act: update the saved attachment and send it through change set
          att = new Attachment();
          att.Id = fetchedAtt.get(0).Id;
          att.Name = 'Renamed.txt';

          update att;

          changeSet = new SQX_Test_ChangeSet();
          changeset.addChanged(att);

          SQX_Upserter.UpsertRecords(changeSet.toJSON());


          // Assert: a. Ensure that attachment is still present and no changes occured
          fetchedAtt = [SELECT Id, Name FROM Attachment WHERE ParentId = : co.Id AND Name = : att.Name];
          System.assertEquals(1, fetchedAtt.size(), 'Couldn\'t find matching attachment' + fetchedAtt);
          System.assertEquals(att.Name, fetchedAtt.get(0).Name );

            
        }
    }

}