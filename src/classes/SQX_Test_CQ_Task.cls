/**
 * test class for CQ task
 */
@isTest
public class SQX_Test_CQ_Task {

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
    }
    
    /**
     * GIVEN : CQ Task with record type Supplier Introduction
     * WHEN : Record is saved without task type
     * THEN : Error is thrown
     */
    public static testMethod void givenCQTaskWithRecordTypeOfSupplier_WhenTaskIsSavedWithoutTaskType_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser){
            
            // ARRANGE : Create a CQ Task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_SUPPLIER_INTRODUCTION,
                                               Step__c = 1,
                                               Name = 'NSI_1');
            
            // ACT : Save record without Task Type
            Database.SaveResult result = Database.insert(task, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Task is not saved');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Task Type is required.'));
            
            // ACT : Task Type is added
            task.Task_Type__c = SQX_Task.TASK_TYPE_TASK;
            result = Database.insert(task, false);
            
            // ASSERT : CQ Task is saved
            System.assert(result.isSuccess(), 'Task is saved');
        }
    }
    
    /**
     * GIVEN : CQ Task with record type Supplier Introduction and task type document request
     * WHEN : Record is saved without document name
     * THEN : Error is thrown
     */
    public static testMethod void givenCQTaskWithRecordTypeOfSupplierAndTaskTypeDocumentRequest_WhenTaskIsSavedWithoutDocumentName_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser){
            
            // ARRANGE : Create a CQ Task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_SUPPLIER_INTRODUCTION,
                                               Task_Type__c = SQX_Task.TASK_TYPE_DOC_REQUEST,
                                               Step__c = 1,
                                               Name = 'NSI_1');
            
            // ACT : Save record without Document Name
            Database.SaveResult result = Database.insert(task, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Task is not saved');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Document Name is required for Document Request record type.'));
            
            // ACT : Document Name is added
            task.Document_Name__c = 'Sample Document Name';
            result = Database.insert(task, false);
            
            // ASSERT : CQ Task is saved
            System.assert(result.isSuccess(), 'Task is saved');
        }
    }
}