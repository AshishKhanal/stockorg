/**
 * This class encapsulates the logic that is required for signing off training records
 * Rationale: Since the extension for sign-off required apex standard set controller, which
 *            isn't valid in lightning context the logic was split
 */
public with sharing virtual class  SQX_Training_Signoff {
    
    protected List<SQX_Personnel_Document_Training__c> documentTrainingList;
    
    // active content version
    protected ContentVersion acv { get; set; }
    
    // primary content version
    protected ContentVersion cv { get; set; }
    
    // secondary content version
    protected ContentVersion scv { get; set; }
    
    
    public Boolean isTrainerSignOff { get; set; }
    public Boolean CQ_Electronic_Signature_Enabled { get; set; }
    public Boolean CQ_Electronic_Signature_Ask_Username { get; set; }
    public String SigningOffUsername { get; set; }
    public String SigningOffPassword { get; set; }
    public String SigningOffComment { get; set; }
    public Boolean IsInSync {get; set;}
    public Boolean DocHasContent {get; protected set;}
    
    public SQX_Training_Signoff() {
        setElectronicSignatureSettings();
    }

    public SQX_Training_Signoff(Boolean isTrainerSignOff) {
        this.isTrainerSignOff = isTrainerSignOff;
        setElectronicSignatureSettings();
    }

    
    /**
    *   This property is used to determine if all the user sign off items are SCORM contents
    */
    public Boolean HasAllScormContents { get; protected set; }

    public Set<String> statusesForSigningOffItems {
        get {
            // allow trainer can sign off user sign off pending records.
            Set<String> statuses = new Set<String>{ SQX_Personnel_Document_Training.STATUS_PENDING };
            
            if (isTrainerSignOff) {
                statuses.add(SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING);
            }
            
            return statuses;
        }
    }
    

    public void setElectronicSignatureSettings() {
        SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getInstance();
        CQ_Electronic_Signature_Enabled = cqES.Enabled__c;
        CQ_Electronic_Signature_Ask_Username = (cqES.Enabled__c && cqES.Ask_Username__c);
    }
    
    public List<SQX_Personnel_Document_Training__c> getListItems() {
        return documentTrainingList;
    }
        
    public Boolean getHasSecondary(){ 
        return acv == scv;
    }
    
    public void setListItems(List<SQX_Personnel_Document_Training__c> items) {
        documentTrainingList = items;
    }
    
    /*
    * returns salesfoce content server url path
    */
    protected String getContentServer() {
        return SQX_Utilities.getContentServer();
    }
    
    /*
    * returns salesforce content server native url to download the primary document content revision
    */
    public String getContentServerNativeLink() {
        return (cv == null ? '' : getContentServer() + 'download/' + cv.Id + '?asPdf=false');
    }
    
    /**
     */
    public String getDocId() {
        return (scv == null ? '' : scv.Controlled_Document__c);        
    }
    
    /*
    * returns content server url to view the document content revision as pdf
    */
    public String getContentServerPdfLink() {
        return (cv == null ? '' : getContentServer() + 'renditionDownload?versionId=' + cv.Id + '&operationContext=CONTENT&rendition=PDF');
    }

    /*
    * returns flash vars while previewing the document content revision
    */
    public String getFlashVars() {
        return (acv == null ? '' : 'shepherd_prefix=/sfc/servlet.shepherd&v=' + acv.Id + '&mode=details');
    }
    
    
    public List<SQX_Personnel_Document_Training__c> getDocumentTrainingsToSignOff() {
        List<SQX_Personnel_Document_Training__c> result = new List<SQX_Personnel_Document_Training__c>();

        Boolean allScormContents = true;
        for (SQX_Personnel_Document_Training__c dt : documentTrainingList) {
            // a. trainings needing assessment to complete cannot be signed off
            // b. trainings linked to incomplete training session roster cannot be signed off
            if ( statusesForSigningOffItems.contains( dt.Status__c )
                && ( dt.SQX_Assessment__c == null || dt.SQX_Personnel_Assessment__c != null )
                && ( dt.SQX_Training_Session__c == null || dt.Is_Training_Session_Complete__c == true ) ) {

                    if(!isTrainerSignOff && !dt.SQX_Controlled_Document__r.Is_Scorm_Content__c){
                        allScormContents=false;
                    }

                    result.add(dt);
            }
        }

        if(result.size() > 0 && allScormContents){
            HasAllScormContents = true;
        }

        return result;
    }
    
    public List<SQX_Personnel_Document_Training__c> getDocumentTrainingsNotRequiringSignOff() {
        List<SQX_Personnel_Document_Training__c> result = new List<SQX_Personnel_Document_Training__c>();
        
        for (SQX_Personnel_Document_Training__c dt : documentTrainingList) {
            if ( !statusesForSigningOffItems.contains(dt.Status__c)
                || ( dt.SQX_Training_Session__c != null && dt.Is_Training_Session_Complete__c != true ) ) {
                result.add(dt);
            }
        }
        
        return result;
    }

    /**
     * This method returns the pending training that has the assessment
     */
    public List<SQX_Personnel_Document_Training__c> getDocumentTrrainingsRequiringAssessmentToPass() {
        List<SQX_Personnel_Document_Training__c> result = new List<SQX_Personnel_Document_Training__c>();
        
        for (SQX_Personnel_Document_Training__c dt : documentTrainingList) {
            if (dt.Status__c == SQX_Personnel_Document_Training.STATUS_PENDING && dt.SQX_Assessment__c != null && dt.SQX_Personnel_Assessment__c == null
                && (dt.SQX_Training_Session__c == null || dt.Is_Training_Session_Complete__c == true)) {
                result.add(dt);
            }
        }
        return result;
    }
    
    public virtual String getCurrentUserSignatureData() {
        return SQX_Extension_UI.getUserDetailsForRecordActivity();
    }
    
    public virtual Boolean doVerify() {
        if (CQ_Electronic_Signature_Enabled) {
            //if esignature is enabled, then only validate
            if(!CQ_Electronic_Signature_Ask_Username){
                //if username is not asked for, then user current user's username else use the username provided in username input
                SigningOffUsername = UserInfo.getUserName();
            }
            
            return new SQX_OauthEsignatureValidation().validateCredentials(SigningOffUsername, SigningOffPassword);
        }
        
        return true;
    }


    public SQX_Action_Response signOffUsingApi(Boolean allowScormSignOff) {
        SQX_Action_Response response = new SQX_Action_Response();
        List<SQX_Personnel_Document_Training__c> documentTrainingsToUpdate;
        List<Schema.SObjectField> fieldsToSet;
        
        if(doVerify()){
            Id currentUserID = UserInfo.getUserId();
            String currentUserSignatureData = getCurrentUserSignatureData();
            if (String.isBlank(currentUserSignatureData)) {
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_PDT_SIGNOFF_ERR_MSG_EMPTY_SIGNATURE_DATA));
                response.errorMessages.add(Label.SQX_PDT_SIGNOFF_ERR_MSG_EMPTY_SIGNATURE_DATA);
            } else {
                List<SQX_Personnel_Document_Training__c> documentTrainingsToSignOff = getDocumentTrainingsToSignOff();
                if (isTrainerSignOff) {
                    documentTrainingsToUpdate = SQX_Personnel_Document_Training.setFieldValuesForTrainerSignOff(documentTrainingsToSignOff, SigningOffComment);
                    
                    // fields for trainer sign off
                    fieldsToSet = new List<Schema.SObjectField> {
                                      Schema.SQX_Personnel_Document_Training__c.SQX_Training_Approved_By__c,
                                      Schema.SQX_Personnel_Document_Training__c.Trainer_Signature__c,
                                      Schema.SQX_Personnel_Document_Training__c.Trainer_SignOff_Date__c,
                                      Schema.SQX_Personnel_Document_Training__c.Trainer_Signoff_Comment__c,
                                      Schema.SQX_Personnel_Document_Training__c.Status__c,
                                      Schema.SQX_Personnel_Document_Training__c.Completion_Date__c };
                } else {

                    documentTrainingsToUpdate = new List<SQX_Personnel_Document_Training__c>();

                    // exclude scorm contents from sign off
                    for(SQX_Personnel_Document_Training__c dts : documentTrainingsToSignOff){
                        if(allowScormSignOff || !dts.SQX_Controlled_Document__r.Is_Scorm_Content__c){
                            documentTrainingsToUpdate.add(SQX_Personnel_Document_Training.setFieldValuesForUserSignOff(new List<SQX_Personnel_Document_Training__c> { dts }, SigningOffComment).get(0)); 
                        }
                    }

                    // fields for user sign off
                    fieldsToSet = new List<Schema.SObjectField> {
                                      Schema.SQX_Personnel_Document_Training__c.SQX_Trainer__c,
                                      Schema.SQX_Personnel_Document_Training__c.SQX_User_Signed_Off_By__c,
                                      Schema.SQX_Personnel_Document_Training__c.User_Signature__c,
                                      Schema.SQX_Personnel_Document_Training__c.User_SignOff_Date__c,
                                      Schema.SQX_Personnel_Document_Training__c.User_Signoff_Comment__c,
                                      Schema.SQX_Personnel_Document_Training__c.Status__c,
                                      Schema.SQX_Personnel_Document_Training__c.Completion_Date__c };
                }

                try {
                    // personnel user (with read access on personnel provided by system) can only sign off as trainee and is validated using validation rule
                    // any user can sign off as trainer who has at least read access on personnel record
                    // TODO: show errors for failed training records only and allow successfully signed off records
                    new SQX_DB().withoutSharing().op_update(documentTrainingsToUpdate, fieldsToSet);
                } catch (Exception ex) {
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                    response.errorMessages.add(ex.getMessage());
                }
            }
        } else {
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
           response.errorMessages.add(Label.SQX_INVALID_USERNAME_PASSWORD);
        }

        return response;
    }

    public SQX_Action_Response initializeDocumentTrainings(Set<Id> documentTrainingIds) {
        // populate documentTrainingList
        SQX_Action_Response response = new SQX_Action_Response();

        documentTrainingList = SQX_Personnel_Document_Training.getPDTList(documentTrainingIds);

        if (documentTrainingList.size() == 1) {
            SQX_Controlled_Document__c doc = [SELECT Id, RecordTypeId, Content_Reference__c, Secondary_Content_Reference__c, Valid_Content_Reference__c, Synchronization_Status__c, Secondary_Content__c FROM SQX_Controlled_Document__c WHERE Id=:documentTrainingList[0].SQX_Controlled_Document__c ];

            Boolean isAutoAndNotInSync =  (doc.Synchronization_Status__c != SQX_Controlled_Document.SYNC_STATUS_IN_SYNC) &&
                                            (doc.Secondary_Content__c == SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO);

            IsInSync = (!isAutoAndNotInSync);
            DocHasContent = !SQX_Controlled_Document.DEFAULT_CONTENT_LINK_SUPPORTING_RECORDTYPE.contains(doc.RecordTypeId);

            List<Id> contentIds = new List<Id>();
            contentIds.add(doc.Content_Reference__c);
            if(doc.Content_Reference__c != doc.Valid_Content_Reference__c) {
                contentIds.add(doc.Valid_Content_Reference__c);
            }
            
            Map<Id, ContentDocument> cDocs = new Map<Id, ContentDocument>([SELECT Id, LatestPublishedVersion.Id,  LatestPublishedVersion.Controlled_Document__c  FROM ContentDocument WHERE Id IN : contentIds]);
            
            if (cDocs.size() > 0) {
                cv = cDocs.containsKey(doc.Content_Reference__c) ? cDocs.get(doc.Content_Reference__c).LatestPublishedVersion : null;
                scv = cDocs.containsKey(doc.Secondary_Content_Reference__c) ? cDocs.get(doc.Secondary_Content_Reference__c).LatestPublishedVersion : null;
                acv = cDocs.containsKey(doc.Valid_Content_Reference__c) ? cDocs.get(doc.Valid_Content_Reference__c).LatestPublishedVersion : null;
            }
            
        }
        
        // show errors for not available data with valid ids
        if (documentTrainingList.size() != documentTrainingIds.size()) {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Some or all ' + Schema.SObjectType.SQX_Personnel_Document_Training__c.getLabelPlural().toLowerCase() + ' you were trying to sign off could not be found.'));
            response.errorMessages.add('Some or all ' + Schema.SObjectType.SQX_Personnel_Document_Training__c.getLabelPlural().toLowerCase() + ' you were trying to sign off could not be found.');
        }

        return response;
    }

}
