/**
* Unit tests for SQX_Extension_UI
*
* @author Sagar Shrestha
* @date 2014/5/23
* 
*/
@isTest
public class SQX_Test_Extension_UI{

    /**
    * Count for the object type. Note: That a single counter is applied to all object types
    */
    static Integer count = 1;

    /**
     * Useful for generating fake ID for objects without actually saving it
     * Refer to : http://salesforce.stackexchange.com/a/21297
     *            https://foobarforce.com/2013/11/25/df13return/
     */
    public static String getFakeId(SObjectType objType) {
        String result = String.valueOf(count++);

        return objType.getDescribe().getKeyPrefix() +
            '0'.repeat(12-result.length()) + result;
    }

    final static String STD_USER_1 = 'STD_USER_1',
                        ADMIN_USER_1 = 'ADMIN_USER_1';

    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STD_USER_1);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER_1);
    }

    /**
    *  Tests for Remote Action getparts()
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void requestDataFromRemoteActionForPart(){ 

        SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name='Hardware');
        insert partFamily;

        SQX_Part__c part= new SQX_Part__c(Name='Chip', 
                                        Part_Number__c='123',
                                        Part_Family__c=partFamily.Id,
                                        Part_Risk_Level__c=5,
                                        Active__c=true);
        insert part;

        List<SQX_Part__c> parts= SQX_Extension_UI.getParts(1,null);

        System.assert(parts.size() == 1, 
            'Expected Part to be added'+ parts);
    }

    /**
    *  Tests for Remote Action getAccounts()
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void requestDataFromRemoteActionForAccount(){ 

        Account account = SQX_Test_Account_Factory.createAccount();

        List<Account> accounts= SQX_Extension_UI.getAccounts(1,null);

        System.assert(accounts.size() == 1, 
            'Expected account to be added'+ accounts);
    }

    /**
    *  Tests for Remote Action getContacts(), getUsers()
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void requestDataFromRemoteActionForContacts(){ 
    
        
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){

            Account account = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(account);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER );
    
    
            List<Contact> contacts= SQX_Extension_UI.getContacts(1,null);
    
            System.assert(contacts.size() == 1, 
                'Expected contact to be added'+ contacts);
    
            List<User> users= SQX_Extension_UI.getUsers(1,null);
    
            System.assert(users.size() >= 1, 
                'Expected user to be added'+ users);
        }

    }

    /**
    *  Tests for Remote Action getStandardServices()
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void requestDataFromRemoteActionForStandardService(){ 

        SQX_Standard_Service__c standardService= new SQX_Standard_Service__c(Name='Hardware Maintenence',
                                                                            Description__c='sahi service');

        insert standardService;

        List<SQX_Standard_Service__c> standardServices= SQX_Extension_UI.getStandardServices(1,null);

        System.assert(standardServices.size() == 1, 
            'Expected account to be added'+ standardServices);
    }

    /**
    *  Tests for Remote Action getDefectCodes()
    *
    * @author Sagar Shrestha
    * @date 2014/5/23
    * 
    */
    public static testmethod void requestDataFromRemoteActionForDefectCode(){ 

        SQX_Defect_Code__c defectCode= new SQX_Defect_Code__c(Name='Scratch', Description__c='new defect code', Active__c = false,
                                                                Defect_Category__c='Human Fault');

        insert  defectCode;

        List<SQX_Defect_Code__c> defectCodes= SQX_Extension_UI.getDefectCodes(1,null,'');

        System.assert(defectCodes.size() == 0, 
            'Expected defect codes not to be added'+ defectCodes);

        defectCode.Active__c = true;
        update defectCode;
        
        defectCodes= SQX_Extension_UI.getDefectCodes(1,null,'');

        System.assert(defectCodes.size() == 1, 
            'Expected account to be added'+ defectCodes);
    }


    /**
    * This method ensures that CAPASchema is selected by default and other schema when present.
    * Note: Since we don't have multiple schemas we are using SetupData as alternate schema 
    */
    static testmethod void whenSchemaNameIsRequested_ThenNameIsReturnedBasedOnPriority(){
        SQX_Schema_Selector selector = new SQX_Schema_Selector();

        //Act: Get Schema name
        selector.customizedSchemaName = 'InvalidSchemaXYZ'; // this is necessary for orgs where CQSchema is already present
        String schema = selector.getSchemaName();
        
        // Assert: Default schema CAPASchema is selected
        System.assert(schema.contains('CAPASchema'), 'CAPASchema was expected but found ' + schema);

        //Act: Provide higher priority schema and get schema
        selector.customizedSchemaName = 'SetupData';
        schema = selector.getSchemaName();

        // Assert: Setupdata is selected because it customized schema if present should be returned
        System.assert(schema.contains(selector.customizedSchemaName), 'Expected SetupData schema but found ' + schema);

        //Act: Provider higher priority schema with language
        selector.languagePrefix = '_6_4';
        schema = selector.getSchemaName();

        // Assert: Setupdata_6_4 should be selected since it has highest priority
        System.assert(schema.contains(selector.languagePrefix), 'Expected Setupdata_6_4 but found ' + schema);
        
        //Act: Provider higher priority schema with language
        selector.languagePrefix = '_8_0';
        schema = selector.getSchemaName();

        // Assert: Setupdata_8_0 should be selected since it has highest priority
        System.assert(schema.contains(selector.languagePrefix), 'Expected Setupdata_8_0 but found ' + schema);

    }

    /**
    * This method ensures that CQ Resource Cache is returned when kendoui param is set
    */
    static testmethod void whenKendoUIParamIsSet_ReturnCQUIResourceCache(){
        PageReference ref = new PageReference('/');
        ref.getParameters().put(SQX_Schema_Selector.PARAM_RESOURCE_NAME, 'kendoui');
        Test.setCurrentPage(ref);
        SQX_Schema_Selector selector = new SQX_Schema_Selector();

        //Act: Get Schema name
        String schema = selector.getSchemaName();

        // Assert: Default schema CAPASchema is selected
        System.assert(schema.contains('CQ_Resource_Cache'), 'CQ Resource Cache was expected but found ' + schema);

    }

    /**
    * Scenario: Test for paging using 'getObjects' method
    * Given contacts (2 * PAGESIZE)
    * When queried for
    *      a) just contacts of 1st page
    *      b) second page of contacts
    *      c) when queried for 3rd Page, higher page
    * Then it returns
    *      a) PAGESIZE number of contacts
    *      b) Returned results contain PAGESIZE records
    *      c) Returns an error
    */
    static testmethod void givenContacts_WhenQueriedPageByPage_ThenResultsAreReturned() {

        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){
            // Arrange: Create required number of contacts
            List<Contact> contactsToInsert = new List<Contact>();
            for(Integer i = 0; i < 2 * SQX_Extension_UI.PAGE_SIZE; i++) {
                contactsToInsert.add(new Contact(LastName = 'Last Name-' + i ));
            }

            insert contactsToInsert;

            Integer[] resultSize = new Integer[] {SQX_Extension_UI.PAGE_SIZE, SQX_Extension_UI.PAGE_SIZE, 0};

            for (Integer i = 0; i < resultSize.size(); i++) {
                // Act: Query for task page 'i+1'
                List<Contact> contacts = SQX_Extension_UI.getObjects('Contact', new List<String> { 'LastName' }, i + 1, null);

                // Assert: Ensure that PAGE_SIZE number of records are returned
                System.assertEquals(resultSize[i], contacts.size(), 'Expected returned contact to be equal to page size for page ' + i);
            }


        }
    }

    /**
    * Scenario: Test for querying objects without 'Name' field
    * Given tasks in system
    * When queried for task
    * Then it returns tasks sorted by id field
    */
    static testmethod void givenTasks_WhenQueried_ThenResultsAreReturned() {
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){

            // Arrange: Create required number of tasks
            List<Task> tasksToInsert = new List<Task>();
            for(Integer i = 0; i < SQX_Extension_UI.PAGE_SIZE; i++) {
                tasksToInsert.add(new Task(Subject = 'Subject -' + i ));
            }

            insert tasksToInsert;

            // Act: Query for tasks page 1
            List<Task> tasks = SQX_Extension_UI.getObjects('Task', new List<String> { }, 1, null);

            // Assert: Ensure that PAGE_SIZE number of records are returned
            System.assertEquals(SQX_Extension_UI.PAGE_SIZE, tasks.size(), 'Expected returned task to be equal to page size.');


        }
    }

    /**
    * Scenario: Test for ensuring that objects with name field such as User return name by default
    * Given users in system
    * When queried for users
    * Then returned users have name field
    */
    static testmethod void givenObjectWithNameField_WhenQueried_ThenNameFieldIsReturned() {
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){

            // Act: Query for tasks page 1
            List<User> users = SQX_Extension_UI.getObjects('User', new List<String> { }, 1, null);

            // Assert: Ensure that user has name field returned
            System.assertNotEquals(0, users.size() , 'Expected users to be returned but found none.');
            System.assertNotEquals(null, users.get(0).Name, 'Expected user name to not be null but found');
        }
    }


    /**
    * Scenario: Test for checking when change set is synchronized/auto-saved, deletion is performed
    * Given We have 3 tasks in NC
    * When change set is synchronized with deletion request for 2 tasks
    * Then only two tasks are deleted and third is still present
    */
    static testmethod void givenTasks_WhenAutoSavedWithDeletion_ThenRecordsAreDeletedCorrectly() {
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){

            // Arrange: Create 3 tasks and initialize temp store
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.save();

            Task [] tasks = new Task[] { new Task(Subject = 'Task 1', WhatId = nc.nc.Id),
                                         new Task(Subject = 'Task 2', WhatId = nc.nc.Id),
                                         new Task(Subject = 'Task 3', WhatId = nc.nc.Id) };

            insert tasks;
            new SQX_Extension_NC(new ApexPages.StandardController(nc.nc)).initializeTemporaryStorage();

            // Act: Auto-save change with 2 task deleted
            String message = SQX_Extension_UI.autoSaveChangeSet(nc.nc.Id, new Task[] { tasks[0], tasks[1] }, '', 'Title');

            // Assert: Ensure that 2 tasks are deleted while 3 is as is.
            Map<Id, Task> taskMap = new Map<Id, Task>([SELECT Id, IsDeleted FROM Task WHERE Id = : tasks ALL ROWS]);
            System.assert(taskMap.get(tasks[0].Id).IsDeleted, 'Expected task 1 to be deleted');
            System.assert(taskMap.get(tasks[1].Id).IsDeleted, 'Expected task 2 to be deleted');
            System.assert(!taskMap.get(tasks[2].Id).IsDeleted, 'Expected task 3 to remain as is but found it otherwise');
            System.assertEquals(SQX_Extension_UI.OK_STATUS, message, 'Expected everything to go through');



            // Act: Reattempt deletion of task 1 [Failure Scenario]
            message = SQX_Extension_UI.autoSaveChangeSet(nc.nc.Id, new Task[] { tasks[0] }, '', 'Title');

            System.assertNotEquals(SQX_Extension_UI.OK_STATUS, message, 'Expected the request to fail with proper message');


        }
    }

    /**
    * This is a reusable method that is being used by two tests providing different ids
    */
    static void ensureCorrectExtensionIsReturned(Map<String, String> idMap) {
        final String ncId = idMap.get(SQX.NonConformance),
                     capaId = idMap.get(SQX.Capa),
                     auditId = idMap.get(SQX.Audit),
                     complaintId = idMap.get(SQX.Complaint),
                     changeOrderId = idMap.get(SQX.ChangeOrder),
                     auditProgramId = idMap.get(SQX.AuditProgram),
                     unsupportedId = idMap.get(SQX.Implementation);

        Boolean responseMode = true, normalMode = false;

        SQX_Extension_UI result;


        // Act: Get the extension for CAPA in normal mode
        result = SQX_Extension_UI.getExtension(capaId, normalMode);

        // Assert: Ensure that the returned extension is an instance of Extension NC.
        System.assert(result instanceof SQX_Extension_CAPA, 'Expected to get CAPA extension for non response mode');
        System.assert(!(result instanceof SQX_Extension_CAPA_Supplier), 'Didn\'t expect to get CAPA supplier extension for non response mode');

        // Act: Get the extension for CAPA in response mode
        result = SQX_Extension_UI.getExtension(capaId, responseMode);

        // Assert: Ensure that the returned extension is an instance of Extension NC.
        System.assert(result instanceof SQX_Extension_CAPA_Supplier, 'Expected to get CAPA Supplier extension for response mode');

        // following types should have same extension for both mode
        for(Boolean mode : new Boolean[] {responseMode, normalMode}) {
            String modeString = (mode ? 'response' : 'normal');

            // Act: Get the extension for NC in both mode
            result = SQX_Extension_UI.getExtension(ncId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension NC.
            System.assert(result instanceof SQX_Extension_NC, 'Expected to get NC extension for ' + modeString  + ' mode');

            // Act: Get the extension for Audit in both mode
            result = SQX_Extension_UI.getExtension(auditId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension Audit in both mode
            System.assert(result instanceof SQX_Extension_Audit, 'Expected to get Audit extension for ' + modeString + ' mode');


            // Act: Get the extension for Complaint in both mode
            result = SQX_Extension_UI.getExtension(complaintId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension Complaint in both mode
            System.assert(result instanceof SQX_Extension_Complaint, 'Expected to get Complaint extension for ' + modeString + ' mode');


            // Act: Get the extension for Change Order in both mode
            result = SQX_Extension_UI.getExtension(changeOrderId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension Change Order in both mode
            System.assert(result instanceof SQX_Extension_Change_Order, 'Expected to get Change Order extension for ' + modeString + ' mode');


            // Act: Get the extension for Complaint in both mode
            result = SQX_Extension_UI.getExtension(changeOrderId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension Complaint in both mode
            System.assert(result instanceof SQX_Extension_Change_Order, 'Expected to get Change Order extension for ' + modeString + ' mode');


            // Act: Get the extension for Audit Program in both mode
            result = SQX_Extension_UI.getExtension(auditProgramId, mode);

            // Assert: Ensure that the returned extension is an instance of Extension Audit Program in both mode
            System.assert(result instanceof SQX_Extension_Audit_Program, 
                'Expected to get Audit Program extension for ' + modeString  + ' mode');
        }

        // Act: Get the extension for unsupported UI
        Boolean exceptionWasThrown = false;
        try{
            result = SQX_Extension_UI.getExtension(unsupportedId, responseMode);
        }
        catch (SQX_ApplicationGenericException ex) {
            exceptionWasThrown = true;
        }

        // Assert: Ensure that exception was thrown
        System.assert(exceptionWasThrown, 'Expected an exception to be thrown for unsupported type but no exception was thrown');
    }

    /**
    * Scenario: New Record without ID invoking get Remote Action method instantiate correct extension
    * Given New records (NC, CAPA, Audit, Complaint, Change Order, Audit Program)
    * When Get Extension is called
    * Then Appropriate concrete extension is returned for the object type
    */
    static testmethod void givenNewRecord_WhenGetExtensionIsCalled_ThenCorrectExtensionIsReturned() {

        ensureCorrectExtensionIsReturned(new Map<String, String> {
            SQX.NonConformance => SQX.NonConformance + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.Capa => SQX.Capa + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.Audit => SQX.Audit + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.Complaint => SQX.Complaint + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.ChangeOrder => SQX.ChangeOrder + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.AuditProgram => SQX.AuditProgram + '-' + '32_chars_long_id_gen_by_kendo_ui',
            SQX.Implementation => SQX.Implementation + '-' + '32_chars_long_id_gen_by_kendo_ui'
        });

    }


    /**
    * Scenario: Persisted record invokes remote action defined in Extension UI, get Extension instantiates correct concrete class
    * Given Persisted records of types (NC, CAPA, Audit, Complaint, Change Order, Audit Program and Unsupported type)
    * When Get Extension is called
    * Then Appropriate concrete extension is returned for the object type
    */
    static testmethod void givenExistingRecord_WhenGetExtensionIsCalled_ThenCorrectExtensionIsReturned() {

        ensureCorrectExtensionIsReturned(new Map<String, String> {
            SQX.NonConformance => getFakeId(SQX_Nonconformance__c.SObjectType),
            SQX.Capa => getFakeId(SQX_CAPA__c.SObjectType),
            SQX.Audit => getFakeId(SQX_Audit__c.SObjectType),
            SQX.Complaint => getFakeId(SQX_Complaint__c.SObjectType),
            SQX.ChangeOrder => getFakeId(SQX_Change_Order__c.SObjectType),
            SQX.AuditProgram => getFakeId(SQX_Audit_Program__c.SObjectType),
            SQX.Implementation => getFakeId(SQX_Implementation__c.SObjectType)
        });

    }

    /**
    * Scenario: Record type fetch is done correctly for an extension, additionally fetching is optimized to a single query.
    * Given An extension UI
    * When Record types list is fetched twice
    * Then Initial fetch returns list with query and subsequent returns result without querying (cached)
    */
    static testmethod void givenExtension_WhenRecordTypesAreFetched_ThenTheyAreFetchedCorrectly() {

        // Arrange: create a capa extension. Note: any other concrete class would have worked too
        SQX_Extension_UI ext = SQX_Extension_UI.getExtension(getFakeId(SQX_CAPA__c.SObjectType), false);

        // Act: Fetch the record types that have been added to the list
        Integer soqlLimitBefore = Limits.getQueries();
        Map<String, Map<String, Id>> recordTypes = ext.RecordTypes;
        Integer soqlLimitAfter = Limits.getQueries();

        // Assert: Ensure that finding type added by CAPA extension is fetched and it uses a single query
        System.assert(recordTypes.containsKey(SQX.Finding), 'Finding objects record type are fetched');
        System.assertEquals(0, soqlLimitAfter - soqlLimitBefore, 'Expected only single query to be performed when fetching record types');

        // Act: Fetch the record type again
        soqlLimitBefore = Limits.getQueries();
        recordTypes = ext.RecordTypes;
        soqlLimitAfter = Limits.getQueries();

        // Assert: Ensure that finding type is still present but no query has been used i.e. cached result has been returned.
        System.assert(recordTypes.containsKey(SQX.Finding), 'Finding objects record type are fetched');
        System.assertEquals(0, soqlLimitAfter - soqlLimitBefore, 'Expected only single query to be performed when fetching record types');
    }


    /**
    * Scenario: Ensures that discarding of changes from extension clears the data
    * Given an extension
    * When changes are discarded
    * Then change is cleared
    */
    static testmethod void givenExtension_WhenChangesAreDiscarded_ThenDataIsCleared() {

        // Arrange: Create temp store with some value
        final String CHANGE = 'Hello World';
        Id capaId = getFakeId(SQX_CAPA__c.SObjectType);
        SQX_Extension_UI ext = SQX_Extension_UI.getExtension(capaId, false);
        ext.initializeTemporaryStorage();
        SQX_Extension_UI.autoSaveChangeSet(capaId, null, CHANGE, 'Hello');

        // pre-check: ensure that change set is saved
        System.assertEquals(CHANGE, SQX_Extension_UI.getStoredChangeSetData(capaId), 'Expected delta in the object');

        // Act: Remove the change/delta
        SQX_Extension_UI.discardChangeSet(capaId);

        // Assert: Ensure that change set has been cleared.
        System.assertEquals('', SQX_Extension_UI.getStoredChangeSetData(capaId));
    }

    /**
    * Scenario: Ensures that sharing of changes works properly in methods
    * Given we have an extension (concrete implementation of Extension UI), with initialized temporary store
    * When we share changes and make changes to new set
    * Then accessing page with 'Access Key' returns the shared change.
    */
    static testmethod void givenExtension_WhenChangesAreShared_ThenSharedDataIsFetched() {
        // Arrange: Create an instance of extension ui and setup temp store
        final String CHANGE = 'Hello World';
        Id capaId = getFakeId(SQX_CAPA__c.SObjectType);
        SQX_Extension_UI ext = SQX_Extension_UI.getExtension(capaId, false);
        ext.initializeTemporaryStorage();

        // Act: share changes and make changes to old set
        String key = SQX_Extension_UI.shareChanges(capaId, CHANGE);
        SQX_Extension_UI.autoSaveChangeSet(capaId, null, CHANGE + '-changed', 'Hello');

        // Assert: Ensure that shared change set access using key returns shared change set not the new one.
        PageReference ref = Page.SQX_CAPA;
        ref.getParameters().put(SQX_Extension_UI.PARAM_ACCESS_KEY, key);
        Test.setCurrentPage(ref);
        System.assertEquals(CHANGE, ext.getChangeSetJSON(), 'Shared extension should still be old value');

    }

    /**
     * Given: Create a new audit
     * When: SObject and Field Name is passed
     * Then: Field value is returned if available else blank
     * @author: Sajal Joshi
     * @date: 5/24/2017
     * @story: SQX-3494
     */ 
    static testmethod void whenSObjectNFieldNameIsPasses_ThenFieldValueIsReturnedIfAvailable(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(STD_USER_1)){
            // Arrange: Create a audit
            String testTitle = 'Test Audit';
            SQX_Test_Audit audit = new SQX_Test_Audit();
            audit.audit.Title__c = testTitle;
            
            // Instantiate the default extension with audit's extension
            SQX_Extension_UI extUI = new SQX_Extension_Audit(new ApexPages.StandardController(audit.audit));
            
            // Assert: Required field value should be returned when passing sobject and field name
            System.assertEquals(testTitle, extUI.getFieldValue(audit.audit, 'Title__c'));
            
            // Assert: Blank value should be returned when passing undefined field
            System.assertEquals('', extUI.getFieldValue(audit.audit, 'Test__c'));
            
            // Assert: hasSObjectField method should return field name if valid field is passed
            System.assert(String.isNotBlank(extUI.getSObjectField(audit.audit, SQX.NSPrefix+'Title__c')));
            
            // Assert: hasSObjectField method should return null if invalid field is passed
            System.assert(String.isBlank(extUI.getSObjectField(audit.audit, SQX.NSPrefix+'Test__c')));
        }
    } 
    
    /**
     * Given: List of Users in Database.
     * When: When queried.
     * Then: Only the active users need to be returned.
     * @author: Pramithas Dhakal
     * @date: 7/23/2018
     * @story: SQX-6148
     */ 
    static testmethod void givenUsersInDb_WhenQueried_ThenOnlyActiveUserShouldBeReturned(){
        
        // create an user with permission set 'CQ_Standard_User' and make it inactive.
        User testUser=SQX_Test_Account_Factory.getUser(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        testUser.isActive=false;
        testUser.FirstName='testFirst';
        testUser.LastName='testLast';
        update testUser;
       
        //Arrange: assign pageNumber
        Integer pageNumber=1;
        
        // create filter for querying the above created new user
        SQX_DynamicQuery.Filter filter= new SQX_DynamicQuery.Filter();
        filter.logic = SQX_DynamicQuery.FILTER_LOGIC_AND;
        filter.filters.add(new SQX_DynamicQuery.Filter('FirstName',SQX_DynamicQuery.FILTER_OPERATOR_EQUALS,'testFirst'));
        filter.filters.add(new SQX_DynamicQuery.Filter('LastName',SQX_DynamicQuery.FILTER_OPERATOR_EQUALS,'testLast'));

                
        //Act: query database for users
        List<SObject> users= SQX_Extension_UI.getUsers(pageNumber,filter);
        

        //Assert: all the users returned should be active (The newly created user above should not be returned)
        for(SObject user: users){
            System.assertEquals(true, user.get('IsActive'));
        }
    }

    
    /**
     * Given: Department and Account records
     * When: User searches for 'ware' keyword in department 
     *       User searches for 'Hard keyword in control document
     * Then: Return department records which has 'ware' in department name.
     *       Return department records which has 'Hard' in control document name.
     */
    @isTest
    public static void givenDepartmentRecords_WhenUserSearchesForDepartments_ThenReturnFilteredDepartmentRecords(){
        //Arrange: Arrange Department records
        List<SQX_Department__c> departmentList = new List<SQX_Department__c>();
        departmentList.add(new SQX_Department__c(Name = 'Software'));
        departmentList.add(new SQX_Department__c(Name = 'Hardware'));
        departmentList.add(new SQX_Department__c(Name = 'Manufacture'));
        departmentList.add(new SQX_Department__c(Name = 'Inspection'));
        
        insert departmentList;
        //Act: Search for ware keyword in department
        SQX_Department__c[] departments = SQX_Extension_UI.searchObjects('ware', 'compliancequest__SQX_Department__c', new String[] { 'Id', 'Name' }, null, 1, new String[]{'Name'}, true);
        //Assert: Make sure there is multiple department record that has ware in it.
        System.assertEquals(2, departments.size());
        
        //Arrange: Arrange Document records
        List<SQX_Controlled_Document__c> documentList = new List<SQX_Controlled_Document__c>();
        documentList.add(new SQX_Controlled_Document__c(name = 'SoftwareDocument', document_number__c='SoftwareDocument', title__c='SoftwareDocument'));
        documentList.add(new SQX_Controlled_Document__c(name = 'HardwareDocument', document_number__c='HardwareDocument', title__c='HardwareDocument'));
        documentList.add(new SQX_Controlled_Document__c(name = 'ManufactureDocument', document_number__c='ManufactureDocument', title__c='ManufactureDocument'));
        documentList.add(new SQX_Controlled_Document__c(name = 'InspectionDocument', document_number__c='InspectionDocument', title__c='InspectionDocument'));
        insert documentList;
        
        Test.setFixedSearchResults(new Id[]{ documentList[1].Id });
        //Act: Search for Hard keyword in department
        SQX_Controlled_Document__c[] documents = (SQX_Controlled_Document__c[])SQX_Extension_UI.searchObjects('Hard', 'compliancequest__SQX_Controlled_Document__c', new String[] { 'Id', 'Name' }, null, 1, new String[]{'Name'}, true);
         //Assert: Make sure there is one document record that has Hard in it.
        System.assertEquals(1, documents.size());
    }   

}