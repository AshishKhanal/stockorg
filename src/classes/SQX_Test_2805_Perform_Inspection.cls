/**
 * test class to test actions while performing inspection
 */
@isTest
public class SQX_Test_2805_Perform_Inspection {
    
    /**
     * 1. Add inspection result in open inspection through inspection detail page [givenAnOpenInspection_WhenInspectionResultIsAddedFrom_ResultPage_ItIsSaved]
     * 2. Prevent change of transferred inspection detail [givenAnOpenInspection_WhenCopiedInspectionDetailIsChanged_ErrorIsThrown]
     * 3. Ensure required fields are entered while adding new inspection detail in open inspection [givenAnOpenInspection_WhenAddedInspectionDetail_RequiredFieldsMustBeEntered]
     * 4. When equipment is added to the inspection detail of the open inspection, equipment calibration is calculated [givenAnOpenInspection_WhenEquipmentIsAddedToInspectionDetail_EqipmentCalibrationIsCalculated]
     * 5. When inspection detail is deleted which is copied from the inspection criteria, error is thrown [givenAnOpenInspectionWithInspectionDetails_WhenCopiedInspectionDetailIsDeleted_ErrorIsThrown]
     * 5. When inspection detail is deleted which is created in inspection, inspection detail is deleted [givenAnOpenInspectionWithInspectionDetails_WhenNewlyCreatedInspectionDetailIsDeleted_ItIsDeleted]
     */
    public static Boolean runAllTests = true,
                            run_givenAnOpenInspection_WhenInspectionResultIsAddedFrom_ResultPage_ItIsSaved = false,
                            run_givenAnOpenInspection_WhenCopiedInspectionDetailIsChanged_ErrorIsThrown = false,
                            run_givenAnOpenInspection_WhenAddedInspectionDetail_RequiredFieldsMustBeEntered = false,
                            run_givenAnOpenInspection_WhenEquipmentIsAddedToInspectionDetail_EqipmentCalibrationIsCalculated = false,
                            run_givenAnOpenInspectionWithInspectionDetails_WhenCopiedInspectionDetailIsDeleted_ErrorIsThrown = false,
                            run_givenAnOpenInspectionWithInspectionDetails_WhenNewlyCreatedInspectionDetailIsDeleted_ItIsDeleted = false;
    
    public static String PART_FAMILY_1 = 'PART_FAMILY_1',
                            PART_1 = 'PART_1',
                            PART_2 = 'PART_2',
                            PROCESS_1 = 'PROCESS_1';
    
    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /*
     * Given: An open inspection with two inspection details
     * When: Inspection detail is completed from inspection detail page
     * Then: Inspection detail is completed
     * @date: 2017-03-03
     * @story: [SQX-3135]
     */
    public static testMethod void givenAnOpenInspection_WhenInspectionResultIsAddedFrom_ResultPage_ItIsSaved(){
        
        if(!runAllTests && !run_givenAnOpenInspection_WhenInspectionResultIsAddedFrom_ResultPage_ItIsSaved){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created with open status containing two inspection details
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});
            System.assertEquals(2, [SELECT ID FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id].size());   

            Test.setCurrentPageReference(Page.SQX_Inspection_Detail); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                (SELECT Id, Result__c FROM SQX_Inspection_Details__r)
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);

            // Act : Inspection detail result is added from inspection detail page
            List<SQX_Inspection_Detail__c> inspectionDetailList = inspect.SQX_Inspection_Details__r;
            inspectionDetailList[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetailList[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            extension.saveInspectionDetails();
            
            // Assert : Inspection detail is saved
            System.assertEquals(2, [SELECT Id, Passed_Characteristics__c FROM SQX_Inspection__c WHERE Id =: inspection.Inspection.Id].Passed_Characteristics__c);
        }
    }   
    
    /*
     * Given: An open inspection with two inspection details
     * When: Transferred inspection detail data is changed
     * Then: Error is thrown
     * @date: 2017-03-03
     * @story: [SQX-3135]
     */
    public static testMethod void givenAnOpenInspection_WhenCopiedInspectionDetailIsChanged_ErrorIsThrown(){
        
        if(!runAllTests && !run_givenAnOpenInspection_WhenCopiedInspectionDetailIsChanged_ErrorIsThrown){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created with open status containing two inspection details
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});
            System.assertEquals(2, [SELECT ID FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id].size());   

            // Act : Transferred inspection detail information is changed
            List<SQX_Inspection_Detail__c> inspectionDetailList = [SELECT Id,Characteristics__c, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            inspectionDetailList[0].Characteristics__c = 'Changed';
            
            Database.SaveResult result = Database.update(inspectionDetailList[0], false);
            
            // Assert : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to be unsuccessful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Inspection Detail copied from matching inspection criteria\'s specification cannot be changed.'), result.getErrors());
        }
    }   
    
    /*
     * Given: An open inspection with two inspection details
     * When: Inspection detail is added without all required fields
     * Then: Error is thrown
     * 
     * When: Inspection detail is added with all required fields
     * Then: Inspection detail is saved
     * @date: 2017-03-03
     * @story: [SQX-3135]
     */
    public static testMethod void givenAnOpenInspection_WhenAddedInspectionDetail_RequiredFieldsMustBeEntered(){
        
        if(!runAllTests && !run_givenAnOpenInspection_WhenAddedInspectionDetail_RequiredFieldsMustBeEntered){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created with open status containing two inspection details
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});
            System.assertEquals(2, [SELECT ID FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id].size()); 
            
            // Act 1: Inspection detail is added without all required fields
            SQX_Inspection_Detail__c inspectionDetail = new SQX_Inspection_Detail__c(Impact__c = SQX_Inspection_Detail.INSPECTION_DETAIL_IMPACT_HIGH, SQX_Inspection__c =inspection.inspection.Id );
            
            Database.SaveResult result = Database.insert(inspectionDetail, false);
            
            // Assert 1 : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to be unsuccessful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Characteristics is required.'), result.getErrors());
            
            inspectionDetail = new SQX_Inspection_Detail__c(Characteristics__c = 'Characteristics', SQX_Inspection__c =inspection.inspection.Id );
            
            result = Database.insert(inspectionDetail, false);
            
            // Assert 2 : Error is thrown
            System.assert(!result.isSuccess(), 'Expected save to be unsuccessful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Impact is required.'), result.getErrors());

            // Act 2 : Inspection detail is saved with all required fields
            inspectionDetail = new SQX_Inspection_Detail__c(Impact__c = SQX_Inspection_Detail.INSPECTION_DETAIL_IMPACT_HIGH, Characteristics__c = 'Characteristics', SQX_Inspection__c =inspection.inspection.Id);
            
            result = Database.insert(inspectionDetail, false);
            
            // Assert 3 : Save is successful
            System.assert(result.isSuccess(), 'Expected save to be successful');
        }
    }  
    
    /*
     * Given: An open inspection with two inspection details
     * When: Equipment is added to inspection detail
     * Then: Equipment calibration is calculated
     * @date: 2017-03-16
     * @story: [SQX-3197]
     */
    public static testMethod void givenAnOpenInspection_WhenEquipmentIsAddedToInspectionDetail_EqipmentCalibrationIsCalculated(){
        
        if(!runAllTests && !run_givenAnOpenInspection_WhenEquipmentIsAddedToInspectionDetail_EqipmentCalibrationIsCalculated){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        
        System.runas(standardUser){
            // Arrange: Create an inspection INS1 with two details inspectionDetail1, inspectionDetail2.
            //          Create two equipment equipment1(Calibrated) and equipment2 (Not-calibrated)
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            SQX_Equipment__c equipment1 = new SQX_Equipment__c(
                Name = 'equipment 1',
                Equipment_Description__c = 'Test 1',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false,
                Last_Calibration_Date__c = Date.Today().addDays(-5),
                Next_Calibration_Date__c = Date.Today().addDays(5)
            );
            
            Database.SaveResult result1 = Database.insert(equipment1, false); 
            System.assert(result1.isSuccess(), 'Expected Equipment to be Created');
            
            SQX_Equipment__c equipment2 = new SQX_Equipment__c(
                Name = 'equipment 2',
                Equipment_Description__c = 'Test 2',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false,
                Last_Calibration_Date__c = Date.Today().addDays(-5),
                Next_Calibration_Date__c = Date.Today().addDays(-2)
            );
            
            result1 = Database.insert(equipment2, false); 
            System.assert(result1.isSuccess(), 'Expected Equipment to be Created'+ result1.getErrors());

            // Act: Update inspectionDetail1 with equipment1 and inspectionDetail2 with equipment2
            SQX_Inspection_Detail__c inspectionDetail1 = new SQX_Inspection_Detail__c(Characteristics__c = 'Inspection Criteria Characteristics 1', 
                                                                                     Impact__c = SQX_Specification.IMPACT_HIGH, 
                                                                                     Measurement_Standard__c = 'SI',
                                                                                     SQX_Equipment__c = equipment1.Id,
                                                                                    SQX_Inspection__c = inspection.inspection.Id);
            
            result1 = Database.insert(inspectionDetail1, false);
            System.assert(result1.isSuccess(), 'Expected inspection detail to be Created');

            SQX_Inspection_Detail__c inspectionDetail2 = new SQX_Inspection_Detail__c(Characteristics__c = 'Inspection Criteria Characteristics 2', 
                                                                                     Impact__c = SQX_Specification.IMPACT_HIGH, 
                                                                                     Measurement_Standard__c = 'SI',
                                                                                     SQX_Equipment__c = equipment2.Id,
                                                                                    SQX_Inspection__c = inspection.inspection.Id);
            
            result1 = Database.insert(inspectionDetail2, false);
            System.assert(result1.isSuccess(), 'Expected inspection detail to be Created');

            // Assert: Ensure that inspectionDetail1's Equipment Calibrated flag is set because equipment1 was calibrated between last calibration date and next calibration date
            //         Ensure that inspectionDetail2's Equipment Calibrated flag is not set because equipment2 wasn't calibrated between last calibration date and next calibration date
            System.assert([SELECT Id, Equipment_Calibrated__c FROM SQX_Inspection_Detail__c WHERE Id =: inspectionDetail1.Id].Equipment_Calibrated__c, 'Expected equipment to be calibrated');
            System.assert(![SELECT Id, Equipment_Calibrated__c FROM SQX_Inspection_Detail__c WHERE Id =: inspectionDetail2.Id].Equipment_Calibrated__c, 'Expected equipment not to be calibrated');
        }
    }   
    
    /*
     * Given: An open inspection with two inspection details copied from inspection criteria
     * When: Inspection detail is deleted
     * Then: Error is thrown
     * @date: 2017-03-27
     * @story: [SQX-3143]
     */
    public static testMethod void givenAnOpenInspectionWithInspectionDetails_WhenCopiedInspectionDetailIsDeleted_ErrorIsThrown(){
        
        if(!runAllTests && !run_givenAnOpenInspectionWithInspectionDetails_WhenCopiedInspectionDetailIsDeleted_ErrorIsThrown){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created with two inspection details copied from inspection criteria
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});

            List<SQX_Inspection_Detail__c> detailList = [SELECT ID FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, detailList.size());

            // Act : Inspection detail is deleted

            Database.DeleteResult result = Database.delete(detailList[0], false);

            // Assert : Error is thrown
            System.assert(!result.isSuccess(), 'Expected delete to fail');

            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_INSPECTION_DETAIL_CANNOT_BE_DELETED));
        }
    }   
    
    /*
     * Given: An open inspection with newly created inspection detail
     * When: Inspection detail is deleted
     * Then: It is deleted
     * @date: 2017-03-27
     * @story: [SQX-3143]
     */
    public static testMethod void givenAnOpenInspectionWithInspectionDetails_WhenNewlyCreatedInspectionDetailIsDeleted_ItIsDeleted(){
        
        if(!runAllTests && !run_givenAnOpenInspectionWithInspectionDetails_WhenNewlyCreatedInspectionDetailIsDeleted_ItIsDeleted){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created with new inspection detail added
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();

            SQX_Inspection_Detail__c inspectionDetail = new SQX_Inspection_Detail__c(Characteristics__c = 'Characteristics', 
                                                                                        Impact__c = SQX_Specification.IMPACT_LOW,
                                                                                        SQX_Inspection__c = inspection.inspection.Id);

            Database.SaveResult saveDetail = Database.insert(inspectionDetail, false);
            System.assert(saveDetail.isSuccess(), 'Expected save to be successful');

            // Act : Inspection detail is deleted
            Database.DeleteResult result = Database.delete(inspectionDetail, false);

            // Assert : Deletion is successful
            System.assert(result.isSuccess(), 'Expected delete to be successful');
        }
    }  
}