/***
* test for audit checkin and checkout when provider type is excel
*/
@isTest
public class SQX_Test_2580_Audit_Checkin_Checkout {
    public class MockHttpClass implements HttpCalloutMock{
        public HttpResponse respond(HttpRequest request){            
            HttpResponse response = new HttpResponse();
            if(request.getEndPoint().contains('export')){
                Blob body = Blob.valueOf('BLANK');
                response.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                response.setBodyAsBlob(body);
                response.setStatusCode(200);
            }
            else if(request.getEndPoint().contains('uploadType=resumable')){
                response.setBody('');
                response.setHeader('Location','Random session uri');
                response.setStatusCode(200);
            }
            else if(request.getMethod() == 'PUT'){
                response.setBody('{"id" : "Random"}');
                response.setStatusCode(200);
            }
            else 
            {
                response.setBody('');
                response.setStatusCode(200);
            }
            return response;
        }
    }    
    public static Boolean runAllTests = false,
                    run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut = false,
                    run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn = true;
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'testUser1');
    }


    /**
    *   Given : Audit whose status is InProgress
    *   Action: Audit is checked out with provider type set as Excel
    *   Expected: A spreadsheet file should be created in SF and the file Id is set as spreadsheet id for the given audit
    *   @date: 2016-09-28
    *   @story: SQX-2580
    */
    public static testmethod void givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut(){
        if(!runAllTests && !run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            // Arrange: Create an audit and move its status to in progress
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                                            .setStatus(SQX_Audit.STATUS_DRAFT)
                                                            .setStage(SQX_Audit.STAGE_PLAN)
                                                            .save();

            audit.audit.End_Date__c = Date.Today().addDays(10);      
            audit.audit.Spreadsheet_Id__c = 'Random Id';      
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                    .save();
            
            // custom setting to define template
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890',
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => 'Microsoft Excel'
            });


            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkoutaudit');
            params.put('comment', 'Mock comment');
            
            List<Map<String, String>> requestList = new List<Map<String, String>>();
            Map<String, String> request = new Map<String, String>();
            request.put('VALUES_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            request.put('BATCH_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            requestList.add(request);

            // Assumed audit is check out and spreadsheet id has been set to audit
            Test.startTest();
            String docId = SQX_Extension_Audit.addFileToSF(audit.audit.Id);
            Test.stopTest();

            SQX_Audit__c currentAudit = [SELECT Id, Name, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert: File id should be set as Spreadsheet Id
            System.assertEquals(docId, currentAudit.Spreadsheet_Id__c, 'Expected document id to be set as spreadsheet id');
            
            ContentVersion ctd = [SELECT ContentDocumentId, Title, FileExtension FROM ContentVersion WHERE ContentDocumentId =: currentAudit.Spreadsheet_Id__c];
            // Assert: Spreadsheet Id should be set when checked out
            System.assertEquals(ctd.ContentDocumentId, currentAudit.Spreadsheet_Id__c, 'Expected file to be created');

            // Assert: The file uploaded in SF should have extension xlsx
            System.assertEquals(ctd.FileExtension, 'xlsx', 'Expected file to be of type xlsx but got ' +ctd.FileExtension);
            
            // Assert: The file uploaded in SF should have name equal to corresponding Audit name
            System.assertEquals(ctd.Title, currentAudit.Name, 'Expected file name to be '+ currentAudit.Name + ' but got ' + ctd.Title);
        }       
    }

    /**
    *   Given : Checked out Audit
    *   Action: Audit is checked in with provider type set as Excel
    *   Expected: The file(present in SF) data  should be checked in
    *   @date: 2016-09-28
    *   @story: SQX-2580
    */
    public static testmethod void givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn(){
        if(!runAllTests && !run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn){
            return;
        }
       
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            // Arrange : Create a audit, move its status to in progress and check out audit
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());
            
            ContentVersion ctvs = new ContentVersion();
            ctvs.versionData = Blob.valueOf('Blank');
            ctvs.title = 'MOCK';
            ctvs.pathOnClient = ctvs.title + '.xlsx';
            insert ctvs;

            ContentVersion ctv = [SELECT ContentDocumentId FROM ContentVersion where Id =:ctvs.Id LIMIT 1];
            
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();
                        
            audit.audit.Spreadsheet_Id__c = ctv.ContentDocumentId;   // NOTE: Valid File Id
            audit.audit.SQX_Checked_Out_By__c = standardUser.Id;
            audit.save(); 

            // custom setting to define template
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890',
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => 'Microsoft Excel'
            });
            
            // create temp store
            SQX_TempStore__c tempObj = new SQX_TempStore__c();
            tempObj.Related_Object_ID__c = standardUser.Id;
            tempObj.Type__c ='Audit';
            new SQX_DB().op_insert(new List<SQX_TempStore__c>{ tempObj }, new List<SObjectField>{});
            
            //create attachment
            Attachment attObj =new Attachment();
            attObj.ContentType ='application/json';
            attObj.ParentId = tempObj.Id;
            attObj.Body = Blob.valueOf('test test');
            attObj.Name = 'attachment';
            new SQX_DB().op_insert(new List<Attachment>{ attObj }, new List<SObjectField>{});

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkinaudit');
            params.put('recordID', audit.audit.Id);

            // Act : Check in audit
            Test.startTest();
            String result= SQX_Extension_Audit.getSheetNamedRanges(audit.audit.Id, attObj.Id);
            Test.stopTest();

            Map<String,Object> parsedResult = (Map<String,Object>)System.JSON.deserializeUnTyped(result);
            String shId = (String)parsedResult.get('SHEET_ID');
            String response = (String)parsedResult.get('RESPONSE');
            Blob body =[SELECT Body FROM Attachment WHERE Id =: attObj.Id AND ContentType ='application/json' LIMIT 1].Body;
            
            // Assert: Ensured that sheetid and attachmentid should be same
            System.assertEquals(shId, attObj.Id); 
            
            // Assert: Ensured that body should be same
            System.assertEquals(Blob.valueOf(response), body);                      
        }             
    }

    /**
     * GIVEN : Draft Audit with Criteria Document
     * WHEN : Audit is opened/initiated
     * THEN : New content derived from Criteria Document is associated with the Audit record
     * @story [SQX-5814]
     */
    static testmethod void givenAuditWithACriteriaDocument_WhenAnAuditIsInitiated_ThenCriteriaDocumentContentIsTransferredToAudit() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){

            // ARRANGE : Audit Criteria Document
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME)
                                                .setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                                                .save(true);

            // ARRANGE : Draft Audit linked with the criteria document
            SQX_Test_Audit ta = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN);
            ta.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            ta.save();

            // ACT : Initiate Audit
            Test.startTest();
            ta.audit.End_Date__c = Date.Today().addDays(10);
            ta.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();
            Test.stopTest();

            // ASSERT : New content/template excel is linked with Audit
            Id spreadsheetId = [SELECT Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: ta.audit.Id].Spreadsheet_Id__c;
            System.assertNotEquals(null, spreadsheetId, 'Expected spreadsheet id to be set in Audit but was not the case');
            System.assert(spreadsheetId.getSObjectType() == ContentDocument.SObjectType, 'Expected spreadsheet id to be content document id');

            // ASSERT : Content document link has been established
            List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: ta.audit.Id AND ContentDocumentId =: spreadsheetId];
            System.assertEquals(1, links.size(), 'Expected contentdocumentlink to be established');
        }
    }

    /**
     * NOTE : This testcase is irrevelant when new Audit record is created post this story
     * However, this testcase ensures that pre-existing Audit records work as desired
     * GIVEN : Open Audit refering obsolete Criteria Document
     * WHEN : Audit is checked out
     * THEN : Error is thrown stating that the criteria is obsolete
     * @story [SQX-5814]
     */
    static testmethod void givenOpenAuditReferingObsoleteCriteria_WhenAuditIsCheckedOut_ThenErrorIsThrown() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){

            // ARRANGE : Microsoft Excel as Audit Offline Provider
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => 'Microsoft Excel'
            });

            // ARRANGE : Audit Criteria Document
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME)
                                                .setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                                                .save();

            // ARRANGE : Draft Audit linked with the criteria document
            SQX_Test_Audit ta = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_IN_PROGRESS);
            ta.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            ta.save();

            // obsolete the criteria
            tcd.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();

            // clear spreadsheet id
            ta.audit.Spreadsheet_Id__c = '';
            ta.save();

            // ACT : Initiate Audit
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkoutaudit');
            params.put('recordID', ta.audit.Id);

            String errMsg;
            Test.startTest();
            try {
                SQX_Extension_Audit.checkOut(ta.audit.Id, '{"changeSet":[]}', null, params, null, null);
            } catch(SQX_ApplicationGenericException ex) {
                errMsg = ex.getMessage();
            }

            Test.stopTest();

            // ASSERT : Expected error message is thrown
            System.assertNotEquals(null, errMsg, 'Expected error message to be thrown when checking out obsolete criteria doc refering Audit');
            System.assertEquals(Label.SQX_ERR_MSG_CANNOT_CHECKOUT_OBSOLETE_DOCUMENT, errMsg, 'Unexpected error message');
        }
    }

    /**
     * GIVEN : Audit without criteria document and Provider is Excel
     * WHEN : Audit is checked out
     * THEN : Error is thrown
     */
    static testmethod void givenAuditWithoutCriteriaDoc_WhenAuditIsCheckedOut_ThenErrorIsThrown() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){

            // ARRANGE : Microsoft Excel as Audit Offline Provider
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => 'Microsoft Excel'
            });

            // ARRANGE : Audit without criteria document
            SQX_Test_Audit ta = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_IN_PROGRESS)
                                        .save();

            // ACT : Checkout Audit
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkoutaudit');
            params.put('recordID', ta.audit.Id);

            String errMsg;
            Test.startTest();
            try {
                SQX_Extension_Audit.checkOut(ta.audit.Id, '{"changeSet":[]}', null, params, null, null);
            } catch(SQX_ApplicationGenericException ex) {
                errMsg = ex.getMessage();
            }

            Test.stopTest();

            // ASSERT : Error is thrown
            System.assertEquals(Label.SQX_ERR_MSG_Select_Audit_Criteria_Document, errMsg, 'Unexpected error message ' + errMsg);
        }
    }


    /**
     * Given: Audit with spreadsheet id (content) and a unreferred content
     * When: Contents are deleted
     * Then: Content that is not referred is deleted and one in audit is prevented
     */
    static testmethod void givenAuditWithSpreadsheetId_WhenContentIsDeleted_ItIsPrevented() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){
            // Arrange: Create an audit and refer to a content (Version). Create another content (free content) not referred by any data
            SQX_Test_Audit audit = new SQX_Test_Audit().save();
            ContentVersion version = new ContentVersion(
                PathOnClient = 'Asd.txt',
                VersionData = Blob.valueOf('asdasdasd')
            );
            ContentVersion freeContent = version.clone();

            insert new ContentVersion[] {version, freeContent};

            Map<Id, Id> contentDocByVersion = new Map<Id, Id>();
            for(ContentVersion ver : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: new ContentVersion[] {version, freeContent}]) {
                contentDocByVersion.put(ver.Id, ver.ContentDocumentId);
            }


            ContentDocumentLink link = new ContentDocumentLink(ShareType = 'V', ContentDocumentId = contentDocByVersion.get(version.Id), LinkedEntityId = audit.audit.Id);
            insert link;
            audit.audit.Spreadsheet_Id__c = contentDocByVersion.get(version.Id);
            audit.save();

            // Act: Try deleting the content
            Database.DeleteResult [] deletions = Database.delete(new ContentDocument[] {
                new ContentDocument(Id = contentDocByVersion.get(version.Id) ),
                new ContentDocument(Id = contentDocByVersion.get(freeContent.Id) )
            }, false);

            // Assert: Ensure free content gets deleted but not linked content
            System.assert(!deletions.get(0).isSuccess(), 'Content linked to audit shouldnt get deleted');
            System.assert(deletions.get(1).isSuccess(), 'Content not linked to audit should get deleted' + deletions.get(1));

            // Act: Try deleting the content document link
            Database.DeleteResult[] deleteResult = Database.delete(new ContentDocumentLink[] { link }, false);

            // Assert: Ensure deletion is prevented
            System.assert(!deleteResult.get(0).isSuccess(), 'Content link cannot be deleted');
        }
    }
}