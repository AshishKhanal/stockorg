/**
* This class contains the functionalities for change order impacts
*/
public with sharing class SQX_Impact{

    /* Record Type Strings */
    /* Impact record type's name */
    public static final String RECORD_TYPE_JOB_FUNCTION = 'Job_Function';
    public static final String RECORD_TYPE_METRIC = 'Metric';
    public static final String RECORD_TYPE_INTERNAL = 'Internal';
    public static final String RECORD_TYPE_EXTERNAL = 'External';

    
    /**
    * Bulkified method called from trigger to perform various operation.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newImpacts equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Impact__c> newImpacts, Map<Id,SQX_Impact__c> oldMap){
            super(newImpacts, oldMap);
        }
        
        /**
        * Insert document requirements when Jobfunction requirement is added to change order.
        * Additionally, if the JF impact is being modified adds the requirement as well.
        * @story SQX-2451
        */ 
       public Bulkified addRequirementToCORelatedDocuments(){
            final String ACTION_NAME = 'addRequirementToCORelatedDocuments';

            Id impactedJFId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, RECORD_TYPE_JOB_FUNCTION);

            Rule impactedJobFunction = new Rule();
            impactedJobFunction.addRule(SQX_Impact__c.SQX_Job_Function__c, RuleOperator.HasChanged);
            impactedJobFunction.addRule(SQX_Impact__c.SQX_Job_Function__c, RuleOperator.NotEquals, null);
            impactedJobFunction.addRule(SQX_Impact__c.RecordTypeId, RuleOperator.Equals, impactedJFId);

            this.resetView()
                .removeProcessedRecordsFor(SQX.Impact, ACTION_NAME)
                .applyFilter(impactedJobFunction, RuleCheckMethod.OnCreateAndEveryEdit);

            if(impactedJobFunction.evaluationResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.Impact, ACTION_NAME, impactedJobFunction.evaluationResult);

                Map<Id, Set<Id>> jobFunctionsByCO = new Map<Id, Set<Id>>();
                for(SQX_Impact__c impact : (List<SQX_Impact__c>) impactedJobFunction.evaluationResult){
                    Set<Id> jfIds = jobFunctionsByCO.get(impact.SQX_Change_Order__c);
                    if(jfIds == null) {
                        jfIds = new Set<Id>();
                        jobFunctionsByCO.put(impact.SQX_Change_Order__c, jfIds);
                    }

                    jfIds.add(impact.SQX_Job_Function__c);
                }

                SQX_Change_Order.synchronizeRequirementsWithDocument(jobFunctionsByCO.keySet(), jobFunctionsByCO, null);
            }

            return this;
        }

        /**
        *  Method prevent deletion of impact when change order record is void or closed
        */
        public Bulkified preventDeletionOnClosureAndVoid(){
            this.resetView();
            List<SQX_Impact__c> impactList = (List<SQX_Impact__c>) this.view;
            if(impactList.size() > 0){
                for(SQX_Impact__c record : impactList) {
                   if((Boolean) record.Is_Parent_Locked__c || (Boolean) record.Is_Locked__c) {
                        record.addError(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE);
                    }
                }
            } 
            return this;
        }
        
        
    }
}