@isTest
public class SQX_Test_Doc_Criterion_Requirement {
    static final String ADMIN_USER_1 = 'adminUser1';

    @testSetup
    static void commonSetup() {
        // add required users
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, ADMIN_USER_1);
    }
    
    /*
    * When the parent audit criteria is in locked, audit doc criterion requirement cannot be deleted
    * @author Sudeep Maharjan
    * @date 2015/09/12
    * @story [SQX-2566]
    */ 
    public testMethod static void  givenUser_DocCriteriaRequirementCannotBeDeletedWhenParentAuditCriteriaIsLocked(){
        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        System.runas(adminUser){
            /*
            * Arrange: 
            * Create new audit criteria
            * Create new result type
            * Create new audit criteria requirement
            */ 
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document('Audit_Criteria');
            auditCriteria.save(false);
            List<SQX_Result_Type__c> result_Types = SQX_Test_Utilities.setupResultTypes(1, 0);
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirements = auditCriteria.addCriterionRequirements(1, result_Types[0].Id);
            
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();
           
            //Act1: Change the status of the audit criteria to Current and try deleting audit criteria document requirement 
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            List<Database.DeleteResult> result1 = Database.delete(docCriteriaRequirements, false);

            /*Assert1: 
            * Doc criterion requirement should not to be deleted when Parent Audit Criteria Document is Locked
            * Should throw an exception 'Document Criterion Requirement cannot be deleted when parent Audit Criteria Document is Locked' 
            */
            System.assertEquals(false, result1[0].success, 'Expected doc criterion requirement not to be deleted when Parent Audit Criteria Document is Locked');
            for (Database.Error err : result1[0].getErrors()) {
                Boolean expectedExceptionThrown =  err.getMessage().contains('Document Criterion Requirement cannot be deleted when parent Audit Criteria Document is Locked') ? true : false;
                System.assertEquals(expectedExceptionThrown,true);     
            }
            
            //Act2: Change the status of the audit criteria to current and Approval status to approved and try deleting audit criteria document requirement 
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            auditCriteria.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            auditCriteria.save();
            List<Database.DeleteResult> result2 = Database.delete(docCriteriaRequirements, false);
            //Assert2 : Doc criterion requirement should not to be deleted when Parent Audit Criteria Document is Locked
            
            System.assertEquals(false, result2[0].success, 'Expected doc criterion requirement not to be deleted when Parent Audit Criteria Document is Locked');
            
            //Act3: Change the status of the audit criteria to draft and approval status to release approval and try deleting audit criteria document requirement 
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();
            auditCriteria.doc.First_Approver__c = adminUser.Id;
            auditCriteria.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            auditCriteria.save();
            List<Database.DeleteResult> result3 = Database.delete(docCriteriaRequirements, false);

            //Assert3: Doc criterion requirement should not to be deleted when Parent Audit Criteria Document is Locked
            System.assertEquals(false, result3[0].success, 'Expected doc criterion requirement not to be deleted when Parent Audit Criteria Document is Locked');

            //Act4: Change the status of the audit criteria to draft and approval status to in change approval and try deleting audit criteria document requirement 
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();
            auditCriteria.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            auditCriteria.save();
            List<Database.DeleteResult> result4 = Database.delete(docCriteriaRequirements, false);

            //Assert4: Doc criterion requirement should not to be deleted when Parent Audit Criteria Document is Locked
            System.assertEquals(false, result4[0].success, 'Expected doc criterion requirement not to be deleted when Parent Audit Criteria Document is Locked');
            
            //Act5: Change the status of the audit criteria to draft and try deleting criteria document requirement
            auditCriteria.save();
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();
            auditCriteria.doc.Approval_Status__c = '';
            auditCriteria.save();
            List<Database.DeleteResult> result5 = Database.delete(docCriteriaRequirements, false);

            //Assert3: Doc criterion requirement should not to be deleted when Parent Audit Criteria Document is Locked
            System.assertEquals(true, result5[0].success, 'Expected doc criterion requirement not to be deleted when Parent Audit Criteria Document is Locked');
        }
    }
    
    /*
    * Ensures any one of the result type fields of SQX_Doc_Criterion_Requirement__c object are set
    * Given
    *   a. both result type fields are empty
    *   b. Result_Type__c field is set
    *   c. SQX_Result_Type__c field is set
    * When record is saved
    * Then
    *   a. validation error occurs
    *   b. record is saved
    *   c. record is saved
    * @story [SQX-3652]
    */
    static testmethod void getCreateUpdateRecord_ResultTypeFieldsEmpty_SaveFailed() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            // Read SQX_Doc_Criterion_Requirement__c.Result_Type__c picklist values
            List<String> auditResultTypes = new List<String>(SQX_Utilities.getPicklistValues(SQX.DocCriteriaRequirement, SQX.getNSNameFor('Result_Type__c')));
            system.assert(auditResultTypes.size() > 0, 'Expected Result_Type__c field in SQX_Doc_Criterion_Requirement__c object to have at least 1 value.');
            
            // Add result type records
            List<SQX_Result_Type__c> result_Types = SQX_Test_Utilities.setupResultTypes(1, 0);
            
            // create audit criteria doc
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document('Audit_Criteria');
            auditCriteria.save(false);
            
            // add criterion reqs
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirements = new List<SQX_Doc_Criterion_Requirement__c>();
            // a. both result type fields are empty
            docCriteriaRequirements.add(new SQX_Doc_Criterion_Requirement__c(
                SQX_Controlled_Document__c = auditCriteria.doc.Id,
                Objective__c = 'Objective 1',
                Section__c = 'Section 1',
                Standard__c = 'Standard 1',
                Sub_Section__c = 'Sub Section 1',
                Topic__c = 'Topic 1',
                Result_Type__c = null,
                SQX_Result_Type__c = null
            ));
            // b. Result_Type__c field is set
            docCriteriaRequirements.add(new SQX_Doc_Criterion_Requirement__c(
                SQX_Controlled_Document__c = auditCriteria.doc.Id,
                Objective__c = 'Objective 2',
                Section__c = 'Section 2',
                Standard__c = 'Standard 2',
                Sub_Section__c = 'Sub Section 2',
                Topic__c = 'Topic 2',
                Result_Type__c = auditResultTypes[0],
                SQX_Result_Type__c = null
            ));
            // c. SQX_Result_Type__c field is set
            docCriteriaRequirements.add(new SQX_Doc_Criterion_Requirement__c(
                SQX_Controlled_Document__c = auditCriteria.doc.Id,
                Objective__c = 'Objective 3',
                Section__c = 'Section 3',
                Standard__c = 'Standard 3',
                Sub_Section__c = 'Sub Section 3',
                Topic__c = 'Topic 3',
                Result_Type__c = null,
                SQX_Result_Type__c = result_Types[0].Id
            ));
            
            // ACT: save criterion requirements
            Database.SaveResult[] results = new SQX_DB().continueOnError().op_insert(docCriteriaRequirements, new List<Schema.SObjectField>{ });
            
            // ASSERT: validation error occurs for record with both result type fields empty
            System.assertEquals(false, results[0].isSuccess(), 'Expected saving doc criterion requirement with both result type fields empty to fail');
            String expectedErrMsg = 'Result Type is required';
            System.assert(SQX_Utilities.checkErrorMessage(results[0].getErrors(), expectedErrMsg),
                'Expected error message: ' + expectedErrMsg + '; Errors:' + results[0].getErrors());
            System.assertEquals(true, results[1].isSuccess(), 'Expected saving doc criterion requirement with any result type field to succeed');
            System.assertEquals(true, results[2].isSuccess(), 'Expected saving doc criterion requirement with any result type field to succeed');
        }
    }
}