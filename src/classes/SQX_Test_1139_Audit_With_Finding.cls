/*
 * description: test for audit finding when the team members are added
 */
@IsTest
public class SQX_Test_1139_Audit_With_Finding{

    private static Boolean runAllTests = true,
                           run_givenAuditWhenTeamMemberIsAdded_EnsureAuditFindingCanBeEdited = false,
                           run_givenAuditWhenFindngIsAdded_EnsureItIsEditableByTeamMembers = false,
                           run_givenAuditWhenTeamMemberIsDeleted_EnsureSharingIsDeletedFromFinding = false,
                           run_givenAuditWhichHasAuditChecklistFromApprovedCriteria_ItCannotBeAltered = false;  

    /*
     * description: when the team member is added in the audit,
     *              added team member should be able to edit existing findings
     * @author: Anish Shrestha
     * @date: 2015/07/01
     * @story: [SQX-1139]
     */
    public static testmethod void givenAuditWhenTeamMemberIsAdded_EnsureAuditFindingCanBeEdited(){
        if(!runAllTests && !run_givenAuditWhenTeamMemberIsAdded_EnsureAuditFindingCanBeEdited){
            return;
        }

        /*
         * Arrange:
         * Setup two users User1 and User2
         * Create audit and finding
         */
        User stdUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User stdUser2 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        SQX_Test_Audit audit = new SQX_Test_Audit();
        SQX_Finding__c finding = new SQX_Finding__c();
        SQX_Audit_Team__c auditTeam = new SQX_Audit_Team__c();

        /*
         * Act 1:
         * Create audit and finding by user1
         */
        System.runas(stdUser1){

            integer randomNumber = (integer)(Math.random() * 1000000);

            audit.setStage(SQX_Audit.STAGE_SCHEDULED)
                 .save();

             finding.Title__c = 'SQX_Finding' + randomNumber;
             finding.Description__c = 'Random Description';
             finding.SQX_Audit__c = audit.audit.Id;
             finding.Investigation_Required__c = false;
             finding.Corrective_Action_Required__c = false;
             finding.Preventive_Action_Required__c = false;
             finding.Containment_Required__c = false;
             finding.Due_Date_Response__c = Date.Today();
             finding.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Finding, SQX_Finding.AUDIT_RECORD_TYPE_DEVELOPER_NAME);
             insert finding;


        }

        /*
         * Assert:
         * Check if user 2 can update findings
         * but user 2 should not be able to update findings
         */
        System.runas(stdUser2){
            finding = [SELECT Description__c FROM SQX_Finding__c WHERE Id =: finding.Id];

            finding.Description__c = 'Updated_Description';

            try{
                update finding;
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') ? true : false;
                //System.assert(false, e.getMessage());
                system.assertEquals(expectedExceptionThrown,true);       
            }            

        }

        /*
         * Act 2:
         * Add user2 in the team member of the audit
         */
        System.runas(stdUser1){
             auditTeam.SQX_User__c = stdUser2.Id;
             auditTeam.SQX_Audit__c = audit.audit.Id;

             insert auditTeam;

             List<SQX_Audit_Team__c> auditTeamList = [SELECT Id FROM SQX_Audit_Team__c WHERE SQX_Audit__c =: audit.audit.Id];
             System.assert(auditTeamList.size() == 2, 'Must have two audit team members but found: ' + auditTeamList.size());
        }

        /*
         * Assert: User 2 should be able to update findings
         */
        System.runas(stdUser2){
            finding = [SELECT Description__c FROM SQX_Finding__c WHERE Id =: finding.Id];

            finding.Description__c = 'Updated_Description';

            update finding;
            finding = [SELECT Description__c FROM SQX_Finding__c WHERE Id =: finding.Id];

            System.assert(finding.Description__c == 'Updated_Description','Expecting the description to be updated');     

        }

    }

    /*
     * description: when the finding is added in the audit,
     *              ensure that all the team members can edit it.
     * @author: Anish Shrestha
     * @date: 2015/07/01
     * @story: [SQX-1139]
     */
    public static testmethod void givenAuditWhenFindngIsAdded_EnsureItIsEditableByTeamMembers(){
        if(!runAllTests && !run_givenAuditWhenFindngIsAdded_EnsureItIsEditableByTeamMembers){
            return;
        }

        /*
         * Arrange:
         * Setup two users User1 and User2
         * Create audit and add user to the audit team
         */
        User stdUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User stdUser2 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        SQX_Test_Audit audit = new SQX_Test_Audit();
        SQX_Finding__c finding = new SQX_Finding__c();
        SQX_Audit_Team__c auditTeam = new SQX_Audit_Team__c();

        /*
         * Act 1:
         * Create audit and 
         * add user in the team member
         * add finding to the audit
         */
        System.runas(stdUser1){

            integer randomNumber = (integer)(Math.random() * 1000000);

            audit.setStage(SQX_Audit.STAGE_SCHEDULED)
                 .save();

             auditTeam.SQX_User__c = stdUser2.Id;
             auditTeam.SQX_Audit__c = audit.audit.Id;

             insert auditTeam;

             List<SQX_Audit_Team__c> auditTeamList = [SELECT Id FROM SQX_Audit_Team__c WHERE SQX_Audit__c =: audit.audit.Id];
             System.assert(auditTeamList.size() == 2, 'Must have two audit team members but found: ' + auditTeamList.size());

             finding.Title__c = 'SQX_Finding_1';
             finding.Description__c = 'Random Description';
             finding.SQX_Audit__c = audit.audit.Id;
             finding.Investigation_Required__c = false;
             finding.Preventive_Action_Required__c = false;
             finding.Corrective_Action_Required__c = false;
             finding.Containment_Required__c = false;
             finding.Due_Date_Response__c = Date.Today();
             finding.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Finding, SQX_Finding.AUDIT_RECORD_TYPE_DEVELOPER_NAME);
             insert finding;

        }

        /*
         * Assert:
         * Check if finding is updateable by the second user
         */
        System.runas(stdUser2){
            finding = [SELECT Id, Description__c FROM SQX_Finding__c WHERE Id = : finding.Id];

            finding.Description__c = 'Updated_Description';

            update finding;

            System.assert(finding.Description__c == 'Updated_Description', 'Description should be updated');

        }


    }

    /*
     * description: when the team member is deleted from the audit,
     *              ensure that all the team members cannot edit finding.
     * @author: Anish Shrestha
     * @date: 2015/07/01
     * @story: [SQX-1139]
     */
    public static testmethod void givenAuditWhenTeamMemberIsDeleted_EnsureSharingIsDeletedFromFinding(){
        if(!runAllTests && !run_givenAuditWhenTeamMemberIsDeleted_EnsureSharingIsDeletedFromFinding){
            return;
        }

        /*
         * Arrange:
         * Setup two users User1 and User2
         * Create audit, finding and add user to the audit team
         */
        User stdUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User stdUser2 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        SQX_Test_Audit audit = new SQX_Test_Audit();
        SQX_Finding__c finding = new SQX_Finding__c();
        SQX_Audit_Team__c auditTeam = new SQX_Audit_Team__c();

        /*
         * Act 1:
         * Create audit and 
         * add finding to the audit
         * add user in the team member
         */
        System.runas(stdUser1){

            integer randomNumber = (integer)(Math.random() * 1000000);

            audit.setStage(SQX_Audit.STAGE_SCHEDULED)
                 .save();

             finding.Title__c = 'SQX_Finding_1';
             finding.Description__c = 'Random Description';
             finding.SQX_Audit__c = audit.audit.Id;
             finding.Investigation_Required__c = false;
             finding.Corrective_Action_Required__c = false;
             finding.Preventive_Action_Required__c = false;
             finding.Containment_Required__c = false;
             finding.Due_Date_Response__c = Date.Today();
             finding.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Finding, SQX_Finding.AUDIT_RECORD_TYPE_DEVELOPER_NAME);
             insert finding;

             auditTeam.SQX_User__c = stdUser2.Id;
             auditTeam.SQX_Audit__c = audit.audit.Id;
             insert auditTeam;

             List<SQX_Audit_Team__c> auditTeamList = [SELECT Id FROM SQX_Audit_Team__c WHERE SQX_Audit__c =: audit.audit.Id];
             System.assert(auditTeamList.size() == 2, 'Must have two audit team members but found: ' + auditTeamList.size());


        }

        /*
         * Assert:
         * Check if finding is updateable by the second user
         */
        System.runas(stdUser2){
            finding = [SELECT Id, Description__c FROM SQX_Finding__c WHERE Id = : finding.Id];

            finding.Description__c = 'Updated_Description';

            update finding;

            System.assert(finding.Description__c == 'Updated_Description', 'Description should be updated');

        }

        /*
         * Act 2: Delete second user from audit
         */

        System.runAs(stdUser1){
            SQX_Audit_Team__c memberToDelete = [SELECT Id FROM SQX_Audit_Team__c WHERE SQX_User__c =: stdUser2.Id];

            delete memberToDelete;
        }

        /*
         * Assert:
         * Std user 2 should not be able to edit finding
         */

        System.runAs(stdUser2){
            finding = [SELECT Description__c FROM SQX_Finding__c WHERE Id =: finding.Id];
            finding.Description__c =  'Check if finding is deleteable';

            try{
                update finding;
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') ? true : false;
                //System.assert(false, e.getMessage());
                system.assertEquals(expectedExceptionThrown,true);       
            }  

        }


    } 

    /**
     * description: Requirements that come from approved criteria may not be altered. They may be removed or marked as skipped 
     * @author: Anish Shrestha
     * @date: 2015/06/07
     * @story: [SQX-1139]
     */
    public static testmethod void givenAuditWhichHasAuditChecklistFromApprovedCriteria_ItCannotBeAltered(){
        if(!runAllTests && !run_givenAuditWhichHasAuditChecklistFromApprovedCriteria_ItCannotBeAltered){
            return;
        }

        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        System.runas(user){

            //Arrange: Setup the program, and set the audit as active and approved.
                 
            SQX_Test_Audit_Program program1 = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope1 = program1.createScopeWithNewCriteriaDocument(1,adminUser); //refers to a criteria document with single requirement
            
            program1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program1.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program1.save();

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            //Act 1: Add the audit program in audit and progarm is added to audit
            audit.includeInProgram(false, program1.program).save();

            audit.setStage(SQX_Audit.STAGE_SCHEDULED).save();

            List<SQX_Audit_Checklist__c> auditChecklist = new List<SQX_Audit_Checklist__c>();
            auditChecklist = [SELECT Id
                                FROM SQX_Audit_Checklist__c
                                WHERE SQX_Audit__c =: audit.audit.Id];

            System.assert(auditChecklist.size() == 1, 'Must have one audit checkist but found : ' + auditChecklist.size());

            auditChecklist[0].Objective__c = 'Updated_Objective';
            Database.SaveResult res = Database.update(auditChecklist[0], false);
            
            System.assertEquals(false, res.isSuccess());
            
            List<String> auditResultTypes = new List<String>(SQX_Utilities.getPicklistValues(SQX.DocCriteriaRequirement, SQX.getNSNameFor('Result_Type__c')));
            System.assert(auditResultTypes.size() > 0, 'Expected Result_Type__c field in SQX_Doc_Criterion_Requirement__c object to have at least 1 value.');
            
            // ACT: change audit checklist Result_Type__c field value
            res = Database.update(new SQX_Audit_Checklist__c( Id = auditChecklist[0].Id, Result_Type__c = auditResultTypes[0] ), false);
            
            System.assertEquals(false, res.isSuccess());
            
            SQX_Audit_Checklist__c auditChecklist1 = new SQX_Audit_Checklist__c();
            auditChecklist1 = [SELECT Rationale__c FROM SQX_Audit_Checklist__c WHERE Id =: auditChecklist[0].Id];
            auditChecklist1.Rationale__c = 'Updated_Rationale';
            update auditChecklist1;

            auditChecklist1 = [SELECT Rationale__c FROM SQX_Audit_Checklist__c WHERE Id =: auditChecklist1.Id];

            System.assert(auditChecklist1.Rationale__c == 'Updated_Rationale', 'Expected the rationale field to be updated but is not');

        }
    }      
}