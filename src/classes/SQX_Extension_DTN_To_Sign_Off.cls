/**
* Extension class for SQX_Document_Training__c object that handles document training user/trainer sign off list for current user
* 
* @author Dibya Shrestha
* @date 2015/07/25
* 
*/
public with sharing class SQX_Extension_DTN_To_Sign_Off {
    
    List<SQX_Document_Training__c> documentTrainingsToSignOff;
    
    public static String trainerSignOffUrlKey { get; set; }
    public static String userSignOffUrlKey { get; set; }
    
    public SQX_Extension_DTN_To_Sign_Off(ApexPages.StandardController controller) {
        trainerSignOffUrlKey = SQX_Extension_DTN_Trainer_SignOff.URL_KEY_FOR_ID;
        userSignOffUrlKey = SQX_Extension_DTN_User_SignOff.URL_KEY_FOR_ID;
        
        Id userId = userInfo.getUserId(); // current user
        documentTrainingsToSignOff = [SELECT Id, Name, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, SQX_User__c, SQX_User__r.Name,
                                             SQX_Trainer__c, SQX_Trainer__r.Name, Level_Of_Competency__c, Optional__c, Due_Date__c, Status__c, Trainer_Approval_Needed__c,
                                             SQX_User_Signed_Off_By__c, User_SignOff_Date__c, SQX_Training_Approved_By__c, Trainer_SignOff_Date__c
                                      FROM SQX_Document_Training__c
                                      WHERE (
                                              (SQX_User__c = :userId AND Status__c = :SQX_Document_Training.STATUS_PENDING)
                                              OR
                                              (SQX_Trainer__c = :userId AND Status__c = :SQX_Document_Training.STATUS_TRAINER_APPROVAL_PENDING)
                                            )];
    }
    
    /**
    * Determines if an item in the sign off list requires user sign off
    */
    public Boolean getRequireUserSignOff() {
        for (SQX_Document_Training__c dt : documentTrainingsToSignOff) {
            if (dt.Status__c == SQX_Document_Training.STATUS_PENDING) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * Determines if an item in the sign off list requires trainer sign off
    */
    public Boolean getRequireTrainerSignOff() {
        for (SQX_Document_Training__c dt : documentTrainingsToSignOff) {
            if (dt.Status__c == SQX_Document_Training.STATUS_TRAINER_APPROVAL_PENDING) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
    * Returns sign off list for current user.
    */
    public List<SQX_Document_Training__c> getSignOffList() {
        if (documentTrainingsToSignOff == null) {
            documentTrainingsToSignOff = new List<SQX_Document_Training__c>();
        }
        
        return documentTrainingsToSignOff;
    }
}