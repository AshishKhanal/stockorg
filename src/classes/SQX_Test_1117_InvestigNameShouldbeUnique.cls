/**
* Unit tests for SQX_Investigation_Tool_NameShouldbeUnique
*
* @author Shailesh Maharjan
* @date 2015/06/03
* 
*/
@IsTest
public class SQX_Test_1117_InvestigNameShouldbeUnique{

    static boolean runAllTests = true,
                    run_investigationToolNameAdded_DuplicateNotAllowed = true;


    /**
    *   Setup: Create A Root Cause Code
    *   Action: To check the Investigation Tool Name is Unique
    *   Expected: Second insert should be failed with its error msg
    *
    * @author Shailesh Maharjan
    * @date 2015/06/03
    * 
    */

     public static testmethod void investigationToolNameAdded_DuplicateNotAllowed(){
       

        if(!runAllTests && !run_investigationToolNameAdded_DuplicateNotAllowed){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
     
        System.runas(adminUser){
             SQX_Investigation_Tool_Name__c investigationName1 = new SQX_Investigation_Tool_Name__c(Name='InvestigationTest');
             Database.SaveResult result1 = Database.Insert(investigationName1 ,false);

             System.assert(result1.isSuccess() == true,
                 'Expected Investigation Tool Name save to be success ');
                 
             SQX_Investigation_Tool_Name__c investigationName2 = new SQX_Investigation_Tool_Name__c(Name='InvestigationTest');
             Database.SaveResult result2 = Database.Insert(investigationName2 ,false);

             System.assert(result2.isSuccess() == false,
                 'Expected Investigation Tool Name save to be Unsuccess ');  
        }
     }

}