@isTest
public class SQX_Test_2251_Distribution_Lib_Support{

    static final String USER_STD_1 = 'StdUser-1';

    static final String DRAFT_LIB_TYPE = 'Draft',
                        RELEASE_LIB_TYPE = 'Release',
                        DISTRIBUTION_LIB_TYPE = 'Distribution';

    @testSetup
    static void setupTest(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, USER_STD_1);

        List<Id> userIdList = new List<Id>();
         userIdList.add(standardUser.Id);
         
        SQX_Test_Utilities.createLibraries(userIdList);
    }

    static Boolean runAllTests = true,
                    run_givenAReleasedDoc_WhenLibraryIsChanged_ContentDocumentParentIdIsChangedVersionIsNotUpdated = false,
                    run_giveAReleasedDoc_WhenDistributionLibrarySetToNull_GivesAnError = false,
                    run_givenADoc_WhenDocIsRevisedFromDocWithDistributionLibraryAndDocIsRelasedWithDistributionLibraryNull_ContentDocumentIsArchieved = false,
                    run_givenADoc_WhenDocIsRevisedFromDocWithEmptyDistributionLibraryAndRelasedBySettingDistributionLibrary_ContetnDocumentIsSaved = false;


    /*
     * SETUP: Create a controlled doc and release the doc
     * SCENARIO 1 :
     * ACTION: Change the library in the distribution library
     * EXPECTED: ContentDocument ParentId is change
     * SCENARIO 2 :
     * ACTION: Change the library in the distribution library
     * EXPECTED: ContentVersion is not updated
     * @date: 2016-05-11
     * @story: [SQX-2251]
     */
    static testmethod void givenAReleasedDoc_WhenLibraryIsChanged_ContentDocumentParentIdIsChangedVersionIsNotUpdated(){
        if (!runAllTests && !run_givenAReleasedDoc_WhenLibraryIsChanged_ContentDocumentParentIdIsChangedVersionIsNotUpdated){
            return;
        }
        
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrage: Create a controlled doc and release the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = null;
            cDoc1.doc.Release_Vault__c = null;
            cDoc1.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Distribution_Vault__c, Release_Vault__c, Draft_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act: release the documents
            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.save();

            List<SQX_Controlled_Document_Release__c> releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: cDoc1.doc.Id];

            System.assertEquals(1, releasedDoc.size(), 'Expected one controlled doc release record to be found but found: ' + releasedDoc.size());

            ContentDocument cdoc = [SELECT ParentId, Title, Description, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];

            System.assertEquals(SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE), cdoc.ParentId, 'Expect to be distribution library');

            List<ContentVersion> versionCount = [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: cdoc.Id];
            System.assertEquals(1, versionCount.size(), 'Expected one content version to be found');

            SQX_BulkifiedBase.clearAllProcessedEntities();
            ApexPages.StandardController controller = new ApexPages.StandardController(controlledDoc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            // Act : Change the distribution vault
            controlledDoc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            extension.saveVault();

            releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: cDoc1.doc.Id];

            cdoc = [SELECT ParentId, Title, Description, Id  FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];
            //System.assert(false, releasedDoc + '||||' + cdoc);

            // Assert 1 : Expected the parent id of the content document to be changed
            System.assertEquals(SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE), cdoc.ParentId, 'Expect to be release library');

            // Asseert 2 : Expected the content version not be updated
            System.assertEquals(1, versionCount.size(), 'Expected one content version to be found');
        }
    }

    /*
     * SETUP: Create a controlled doc and release the doc
     * ACTION: Change the library in the distribution library to none
     * EXPECTED: Save should throw an error
     * @date: 2016-05-11
     * @story: [SQX-2251]
     */
    static testmethod void giveAReleasedDoc_WhenDistributionLibrarySetToNull_GivesAnError(){
        if (!runAllTests && !run_giveAReleasedDoc_WhenDistributionLibrarySetToNull_GivesAnError){
            return;
        }
        
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrage: Create a controlled doc and release the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            cDoc1.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            SQX_BulkifiedBase.clearAllProcessedEntities();

            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.save();

            List<SQX_Controlled_Document_Release__c> releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: cDoc1.doc.Id];

            System.assertEquals(1, releasedDoc.size(), 'Expected one controlled doc release record to be found but found: ' + releasedDoc.size());
            //System.assert(false, releasedDoc);
            ContentDocument cdoc = [SELECT ParentId, Title, Description, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];

            System.assertEquals(SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE), cdoc.ParentId, 'Expect to be distribution library');
            controlledDoc = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act: Change the distribution vault to none
            controlledDoc.Distribution_Vault__c = null;

            Database.SaveResult result = Database.update(controlledDoc, false);

            // Assert: Expect update to be not successful
            System.assert(!result.isSuccess(), 'Expected update to be failed');

            System.assertEquals(Label.SQX_ERR_CONTROLLED_DOCUMENT_DISTRIBUTION_LIBRARY_CANNOT_BE_NULL, result.getErrors()[0].getMessage(), 'Expected to get correct error msg');
        }
    }

    /*
     * SETUP: Create a controlled doc and release the doc and revise the doc and set the distribution library to null
     * ACTION: Release the revised doc
     * EXPECTED: ContentDocument is archieved
     * @date: 2016-05-16
     * @story: [SQX-2251]
     */
    static testmethod void givenADoc_WhenDocIsRevisedFromDocWithDistributionLibraryAndDocIsRelasedWithDistributionLibraryNull_ContentDocumentIsArchieved(){
        if (!runAllTests && !run_givenADoc_WhenDocIsRevisedFromDocWithDistributionLibraryAndDocIsRelasedWithDistributionLibraryNull_ContentDocumentIsArchieved){
            return;
        }
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrage: Create a controlled doc and release the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = null;
            cDoc1.doc.Release_Vault__c = null;
            cDoc1.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act: release the documents
            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.save();

            List<SQX_Controlled_Document_Release__c> releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: cDoc1.doc.Id];

            System.assertEquals(1, releasedDoc.size(), 'Expected one controlled doc release record to be found but found: ' + releasedDoc.size());

            ContentDocument cdoc = [SELECT ParentId, Title, Description, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];

            System.assertEquals(SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE), cdoc.ParentId, 'Expect to be distribution library');

            SQX_Test_Controlled_Document revisedDoc = cDoc1.revise();

            SQX_Controlled_Document__c revisedControlledDoc = [SELECT Id, Release_Vault__c, Draft_Vault__c, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: revisedDoc.doc.Id];

            ApexPages.StandardController controller = new ApexPages.StandardController(revisedControlledDoc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            // Act : Change the distribution vault
            revisedControlledDoc.Distribution_Vault__c = null;
            extension.saveVault();

            SQX_BulkifiedBase.clearAllProcessedEntities();
            // Act: release the documents
            revisedDoc.synchronize();
            revisedDoc.doc.Effective_Date__c = Date.Today();
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            revisedDoc.save();

            releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: revisedDoc.doc.Id];

            cdoc = [SELECT ParentId, IsArchived, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c ALL ROWS];

            // Assert : Expected the parent id of the content document to be changed
            System.assertEquals(true, cdoc.IsArchived, 'Expect to be true');
        }
    }

    /*
     * SETUP: Create a controlled doc and release the doc and revise the doc and set the distribution library
     * ACTION: Release the revised doc
     * EXPECTED: ContentDocument is saved
     * @date: 2016-05-20
     * @story: [SQX-2251]
     */
    static testmethod void givenADoc_WhenDocIsRevisedFromDocWithEmptyDistributionLibraryAndRelasedBySettingDistributionLibrary_ContetnDocumentIsSaved(){
        if (!runAllTests && !run_givenADoc_WhenDocIsRevisedFromDocWithEmptyDistributionLibraryAndRelasedBySettingDistributionLibrary_ContetnDocumentIsSaved){
            return;
        }
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrage: Create a controlled doc and release the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = null;
            cDoc1.doc.Release_Vault__c = null;
            cDoc1.doc.Distribution_Vault__c = null;
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act: release the documents
            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.save();
            SQX_Test_Controlled_Document revisedDoc = cDoc1.revise();

            SQX_Controlled_Document__c revisedControlledDoc = [SELECT Id, Distribution_Vault__c, Release_Vault__c, Draft_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: revisedDoc.doc.Id];

            ApexPages.StandardController controller = new ApexPages.StandardController(revisedControlledDoc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            // Act : Change the distribution vault
            revisedControlledDoc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            extension.saveVault();

            SQX_BulkifiedBase.clearAllProcessedEntities();
            // Act: release the documents
            revisedDoc.doc.Effective_Date__c = Date.Today();
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            revisedDoc.save();

            List<SQX_Controlled_Document_Release__c> releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: revisedDoc.doc.Id];
            
            System.assertEquals(1, releasedDoc.size(), 'Expected one controlled doc release record to be found but found: ' + releasedDoc.size());

            //System.assert(false, releasedDoc + '||||' +[SELECT ParentId, Title, Description, Id FROM ContentDocument]);
            ContentDocument cdoc = [SELECT ParentId, Title, Description, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];

            System.assertEquals(SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE), cdoc.ParentId, 'Expect to be distribution library');
        }
    }

    /*
     * GIVEN : An released controlled document with secondary content disabled
     * WHEN :  Content is updated in release library
     * THEN : Content in distribution library is also update
     * @date : 2018/04/17
     * @story : [SQX-2748]
     */
    static testmethod void giveAReleasedDoc_WhenReleaseDocIsUpdated_DistributionDocIsUpdated(){
        
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR);
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrage: Create a controlled doc and release the doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                   .updateContent(true, 'Doc-1-Primary.txt');

            cDoc1.doc.Title__c = 'Document - 1';
            cDoc1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            cDoc1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
            cDoc1.doc.Distribution_Vault__c = SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE);
            cDoc1.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_DISABLED;
            cDoc1.save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Valid_Content_Reference__c, Distribution_Vault__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            SQX_BulkifiedBase.clearAllProcessedEntities();

            cDoc1.doc.Effective_Date__c = Date.Today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc1.save();

            List<SQX_Controlled_Document_Release__c> releasedDoc = [SELECT ID, Content_Document_Id__c FROM SQX_Controlled_Document_Release__c WHERE SQX_Controlled_Document__c =: cDoc1.doc.Id];

            System.assertEquals(1, releasedDoc.size(), 'Expected one controlled doc release record to be found but found: ' + releasedDoc.size());
            ContentDocument cdoc = [SELECT ParentId, Title, Description, Id FROM ContentDocument WHERE Id =: releasedDoc.get(0).Content_Document_Id__c];

            System.assertEquals(SQX_Test_Utilities.getLibraryId(DISTRIBUTION_LIB_TYPE), cdoc.ParentId, 'Expect to be distribution library');

            // Act : Content is updated
            ContentVersion cv = new ContentVersion();
            cv.VersionData = Blob.valueOf('SampleContentUpdated-2748');
            cv.PathOnClient = 'Test.txt';
            cv.ContentDocumentId = controlledDoc.Valid_Content_Reference__c;
            insert cv;

            // Assert : Content is updated in distribution library
            List<ContentVersion> cvList = new List<ContentVersion>();
            cvList = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =: releasedDoc.get(0).Content_Document_Id__c ORDER BY CreatedDate ASC];
            System.assertEquals(2, cvList.size());
            System.assertEquals(cv.VersionData.toString(), cvList[1].VersionData.toString());

        }
    }
}