/**
 * This test class verifies that each method returns values as expected.
 * @author : Piyush Subedi
 * @story  : [SQX-5316],[SQX-7241]
 */
@IsTest
public class SQX_Test_Controller_PSN_Highlight_Panel {
    
    @testSetup
    static void commonSetup() {
        // add required users
        User admin = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin');
        User stdUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'StdUser1');
        System.runAs(stdUser1) {

            // Arrange : Create job function, personnel, pjf and certificates
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>{
                                                new SQX_Job_Function__c( Name = 'Training Program 1', Active__c = true ), 
                                                new SQX_Job_Function__c( Name = 'Training Program 2', Active__c = true )
                                             };
            SQX_Personnel__c psn = new SQX_Test_Personnel('PSN-1').mainRecord;
            psn.SQX_User__c = admin.Id;
            SQX_Personnel__c psn1 = new SQX_Test_Personnel('PSN-2').mainRecord;
            
            List<SObject> sobjs = new List<SObject>();
            sobjs.add(psn);
            sobjs.add(psn1);
            sobjs.addAll(jfs);
            insert sobjs;
            
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c> {
                                                            new SQX_Personnel_Job_Function__c( SQX_Job_Function__c = jfs[0].Id, SQX_Personnel__c = psn.Id, Active__c = true ),
                                                            new SQX_Personnel_Job_Function__c( SQX_Job_Function__c = jfs[1].Id, SQX_Personnel__c = psn.Id, Active__c = true )
                                                        };
            insert pjfs;
            
            List<SQX_Training_Certificate__c> tcfs = new List<SQX_Training_Certificate__c> { 
                                                        new SQX_Training_Certificate__c(
                                                            SQX_Personnel_Job_Function__c = pjfs[0].Id,
                                                            SQX_Personnel__c = psn.Id,
                                                            SQX_Job_Function__c = jfs[0].Id,
                                                            Title__c = jfs[0].Name,
                                                            Enrollment_Date__c = System.today(),
                                                            Completion_Date__c = System.today() 
                                                        ),
                                                        new SQX_Training_Certificate__c(
                                                            SQX_Personnel_Job_Function__c = pjfs[1].Id,
                                                            SQX_Personnel__c = psn.Id,
                                                            SQX_Job_Function__c = jfs[1].Id,
                                                            Title__c = jfs[1].Name,
                                                            Enrollment_Date__c = System.today(),
                                                            Completion_Date__c = System.today() 
                                                        )
                                                    };
            insert tcfs;
        }
    }
    
    /**
     * Given : Personnel record id.
     * When : Controller method 'getPersonnelData()' is called to get personnel related data like jf, certificates and user.
     * Then : Method should return all the valid job functions, training certificates and user for the given personnel. 
     * @story : SQX-5322
     * @author : Paras BK
     */
    public testmethod static void ensureJobFunctionsAreFetchedProperlyForGivenPersonnel() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('StdUser1')) {
            
            // Act : call controller method to get personnel related datas i.e. personnel wrapper
            // Assert : For PSN-1, 2 js and 2 certificate should be available. Also there should be only one personnel with user and that user should be the admin user.
            SQX_Controller_Personnel_Highlight_Panel.PersonnelWrapper personneWrapper = SQX_Controller_Personnel_Highlight_Panel.getPersonnelData([SELECT Id FROM SQX_Personnel__c ORDER BY Full_Name__c][0].Id);
            System.assertEquals(2, personneWrapper.certificates.size());
            System.assertEquals(2, personneWrapper.jfs.size());
            Object userWrapper= personneWrapper.userWrapper;
            System.assert(userWrapper != null, 'Expected user for personnel with full name PSN-1 but is ' + userWrapper);
            
            // Assert : No jfs, certificates and user should be for PSN-2
            personneWrapper = SQX_Controller_Personnel_Highlight_Panel.getPersonnelData([SELECT Id FROM SQX_Personnel__c ORDER BY Full_Name__c][1].Id);
            System.assertEquals(0, personneWrapper.certificates.size());
            System.assertEquals(0, personneWrapper.jfs.size());
            userWrapper= personneWrapper.userWrapper;
            System.assert(userWrapper == null, 'Expected user to be null for personnel with full name PSN-2 but is ' + userWrapper);
        }
    }
}