/**
* This text cases for to prevent delete when complaint record is closed or void.
*/
@isTest
public class SQX_Test_8169_Prevent_Delete_Record {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Task__c task = new SQX_Task__c();
            task.Name = 'DT';
            task.Record_Type__c = 'Complaint';
            task.Task_Type__c = 'Decision Tree';
            task.Description__c = 'DT Description';
            task.SQX_User__c = standardUser.Id;
            task.Allowed_Days__c = 10;
            
            insert task;
        }
    }
    /**
    * Given : closed complaint record with policy
    * When : delete policy
    * Then : Thrown error message
    */
    static testmethod void giveClosedComplaintRecord_WhenDeletePolicy_ThenThrowErrorMessage() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Arrange : Create complaint with policy
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.complaint.Aware_Date__c = Date.today();
            complaint.save();
            
            SQX_Task__c cqTask = [SELECT Id, Name, Description__c, Allowed_Days__c, SQX_User__c FROM SQX_Task__c LIMIT 1];
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = cqTask.Id;
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.SQX_User__c = adminUser.Id;
            insert complaintTask;
            complaint.close();
            
            //Act: delete policy
            List<SQX_Complaint_Task__c> ctList =[SELECT Id FROM SQX_Complaint_Task__c ];
            DataBase.DeleteResult result = new SQX_DB().continueOnError().op_delete(ctList)[0];

            //Assert: Ensured that error message thrown
            System.assertEquals(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE , result.getErrors().get(0).getMessage());
        }
    }
    
    /**
    * Given : closed complaint record with invertigation
    * When : delete invertigation
    * Then : Thrown error message
    */
    static testmethod void giveClosedComplaintRecord_WhenDeleteInvertigation_ThenThrowErrorMessage1() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Arrange : Create complaint with invertigation
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.complaint.Aware_Date__c = Date.today();
            complaint.save();
            
            SQX_Task__c cqTask = [SELECT Id, Name, Description__c, Allowed_Days__c, SQX_User__c FROM SQX_Task__c LIMIT 1];
            String complaintInvestigationRecordId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Investigation, SQX_Investigation.RECORD_TYPE_COMPLAINT_INVESTIGATION);
            
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.Status__c = SQX_Investigation.STATUS_DRAFT;
            investigation.OwnerId = adminUser.Id;
            investigation.SQX_Task__c = cqTask.Id;
            investigation.RecordTypeId = complaintInvestigationRecordId;
            insert investigation;
            complaint.close();
            
            //Act: delete investigation
            List<SQX_Linked_Investigation__c> linkedInvList = [SELECT Id FROM SQX_Linked_Investigation__c WHERE SQX_Investigation__c =: investigation.Id];
            delete linkedInvList;
            
            List<SQX_Investigation__c> invList = [SELECT Id FROM SQX_Investigation__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            DataBase.DeleteResult result = new SQX_DB().continueOnError().op_delete(invList)[0];
            
            //Assert: Ensured that error message thrown
            System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked , result.getErrors().get(0).getMessage());
        }
    }
}