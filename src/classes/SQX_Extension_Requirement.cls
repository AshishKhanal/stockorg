/**
* @author Sudeep Maharjan
* extension class for Requirement object
*/
public with sharing class SQX_Extension_Requirement {
    
    ApexPages.StandardController standardController;
    SQX_Requirement__c currentReq;
    PageReference saveAndNewReturnURL;

    private static final String PARAM_JF = 'jobfunction';

    /**
    * this method generates customized messages that are proper to display to users
    */
    private String getCustomizedErrorMessage(String message) {
        String msg = message;

        if (msg.contains('Uniqueness_Constraint__c')) {
            String fieldLabel = Schema.SObjectType.SQX_Requirement__c.fields.Uniqueness_Constraint__c.getLabel();
            
            SQX_Requirement__c dupRec = [SELECT Id, Name FROM SQX_Requirement__c
                                               WHERE SQX_Job_Function__c = :currentReq.SQX_Job_Function__c
                                               AND SQX_Controlled_Document__c = :currentReq.SQX_Controlled_Document__c
                                               AND Deactivation_Date__c = NULL];
            
            PageReference viewRef = new ApexPages.StandardController(dupRec).view();

            msg = 'Duplicate value on record: <a target="_blank" href="' + viewRef.getUrl() + '">' + dupRec.Name + '</a> (' + fieldLabel + ')';
        }

        return msg;
    }
    
    /**
     * Internal method that sets Save and New Return URL
     */
    void setSaveAndNewReturnURL() {
        // return url to SQX_Requirement_Create page with original query params
        saveAndNewReturnURL = Page.SQX_Requirement_Create;
    
        for (String param : ApexPages.currentPage().getParameters().keySet()) {
            saveAndNewReturnURL.getParameters().put(param, ApexPages.currentPage().getParameters().get(param));
        }
    }

    /**
     * Internal method that returns redirect URL
     * @param isSaveAndNew boolean value to denote is save and new 
     */
    PageReference getReturnURL(boolean isSaveAndNew) {
        PageReference retUrl;
        
        if (isSaveAndNew) {
            retUrl = saveAndNewReturnURL;
        } else {
            // return url to view page of saved record
            retUrl = new ApexPages.StandardController(currentReq).view();
        } 
        
        retUrl.setRedirect(true);
        return retUrl;
    }

    public SQX_Extension_Requirement(ApexPages.StandardController controller) {
        this.standardController = controller;
        this.currentReq = (SQX_Requirement__c)controller.getRecord();
        setSaveAndNewReturnURL();

        // set passed Job Function values for new record
        if (currentReq.Id == null) {

            String jobfunctionQueryParam = Apexpages.currentPage().getParameters().get(PARAM_JF);

            if (String.isNotBlank(jobfunctionQueryParam)) {
                
                // set job function value
                setJobFunction((Id)jobfunctionQueryParam);
            }
        }
    }

    /**
     * Internal method that sets the Job_function for requirement
     * @param recordId id of the record that is to be queried
     */
    private void setJobFunction(Id recordId) {
        List<SQX_Job_Function__c> jfs = [SELECT Id FROM SQX_Job_Function__c WHERE Id = :recordId LIMIT 1];      
        if (jfs.size() == 1) {
            // set Job Function field value when passed Job Function id value is valid
            currentReq.SQX_Job_Function__c = jfs[0].Id;
        }
    }
    
   
    /**
    * saves current new record
    * @return returns the saveresult of the current new Requirement object
    */
    PageReference saveRecord(boolean isSaveAndNew) {
        PageReference returnTo = null;
        
        Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Requirement__c> { currentReq }, new List<Schema.SObjectField>{ }).get(0);
        
        if (result.isSuccess()) {
            returnTo = getReturnURL(isSaveAndNew);
        } else {
            // customize error messages, if required, and return error messages to same page
            for (Database.Error error : result.getErrors()) {
                String msg = getCustomizedErrorMessage(error.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
            }
        }

        return returnTo;
    }
    
    /**
    * save action for custom vf page
    */
    public PageReference save() {
        return saveRecord(false);
    }
    
    /**
    * save and new action for custom vf page
    */
    public PageReference saveAndNew() {
        return saveRecord(true);
    }
}