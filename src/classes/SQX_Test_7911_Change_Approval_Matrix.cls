/**
* This test case for to set approval matrix when initiate the change order record (draft status)
*/ 
@isTest
public class SQX_Test_7911_Change_Approval_Matrix {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
     /**
     * Given : Change order draft status record
     * When : Initiate the change order record 
     * Then : update approval matrix field.
     */
    static testmethod void givenChangeOrderDraftRecord_WhenInitiateTheRecord_ThenSetApprovalMatrix() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'Change Order Default Approval' );
            insert am1;
            //Arrange: Create change order draft record
            SQX_Test_Change_Order chgOrder = new SQX_Test_Change_Order();
            chgOrder.save();
            
            //Act: Initiate the change order record
            SQX_BulkifiedBase.clearAllProcessedEntities();
            chgOrder.changeOrder.Activity_Code__c = SQX_Change_Order.ACTIVITY_CODE_INITIATE;
            chgOrder.save();
            
            //Assert: Ensured that approval matrix field should be updated
            String aprovalMatrix = [SELECT Name FROM SQX_Approval_Matrix__c WHERE Name = 'Change Order Default Approval'].Name;
            SQX_Change_Order__c changeOrder = [SELECT SQX_Approval_Matrix__r.Name FROM SQX_Change_Order__c WHERE Id =: chgOrder.changeOrder.Id];
            System.assertEquals(aprovalMatrix, changeOrder.SQX_Approval_Matrix__r.Name );

        }
    }
}