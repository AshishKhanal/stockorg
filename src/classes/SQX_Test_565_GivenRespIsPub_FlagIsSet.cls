/**
 * This class ensures that when a response is published [(published,-), (published,approved), (published,rejected)], Has Acknowledgement flag in 
 * a Finding is set.
 * Issue Description: 
 * If a response is submitted, acknowledgement is considered complete (regardless of approval/rejection). 
 * Acknowledge means respondent indicates that they are aware of the CAPA, a response is sufficient to qualify as acknowledgement. 
 * So I'd say set the flag on first response submission
 * 
 * [Please note that SQX-565 is a bug and changes have been requested in the comment]
 * 
 * @story SQX-565
 * @author Pradhanta Bhandari
 * @date 2014/8/19
 */
@isTest
private class SQX_Test_565_GivenRespIsPub_FlagIsSet {

	/* test class basic config, allows to selectively run a unit test */
    static boolean runAllTests = true,
               run_givenResponseIsPublished_HasAckIsChecked = false,
               run_givenResponseIsApproved_HasAckIsChecked = false,
               run_givenResponseIsRejected_HasAckIsChecked = false;

    /**
    * given a response is published (published, -), has ack is set
    */
    static testMethod void givenResponseIsPublished_HasAckIsChecked() {
        if(!runAllTests && !run_givenResponseIsPublished_HasAckIsChecked)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User findingOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        
        System.runas(findingOwner){
            //create a finding
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();
                            
            //submit a response as primary contact user
            System.runas(finding.primaryContactUser){
                Test.startTest();
                
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c, false); //assert that initially the flag is not set
                
                SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(finding.finding)
                                                     .save();
                                                     
                finding.synchronize();                                     
                System.assertEquals(finding.finding.Has_Response__c, false); //assert that just adding a response doesn't set the flag 
                                                     
             	response.publishResponse();
                                                     
                                                     
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c,true); //assert that the flag has been set.
                
                Test.stopTest();
            }
        }
    }
    
    
    /**
    * given a response is published and approved (published, approved), has ack is set
    */
    static testMethod void givenResponseIsApproved_HasAckIsChecked() {
        if(!runAllTests && !run_givenResponseIsApproved_HasAckIsChecked)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User findingOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        
        System.runas(findingOwner){
            //create a finding
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();
                            
            //submit a response as primary contact user
            System.runas(finding.primaryContactUser){
                Test.startTest();
                
                SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(finding.finding)
                                                     .save()
                                                     .submitForApproval();
                                                     
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c, false); //assert that initially the flag is not set
                
                SQX_BulkifiedBase.clearAllProcessedEntities();
                response.performApproval(true); //approve the response
                                                     
                                                     
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c,true); //assert that the flag has been set.
                
                Test.stopTest();
            }
        }
    }
    
    
    /**
    * given a response is published and rejected (published, rejected), has ack is set
    */
    static testMethod void givenResponseIsRejected_HasAckIsChecked() {
        if(!runAllTests && !run_givenResponseIsRejected_HasAckIsChecked)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User findingOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        
        System.runas(findingOwner){
            //create a finding
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                            .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_OPEN)
                            .save();
                            
            //submit a response as primary contact user
            System.runas(finding.primaryContactUser){
                Test.startTest();
                
                SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(finding.finding)
                                                     .save()
                                                     .submitForApproval();
                
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c, false); //assert that initially the flag is not set                                    
                SQX_BulkifiedBase.clearAllProcessedEntities();
                response.performApproval(false); //approve the response
                                                     
                                                     
                finding.synchronize();
                System.assertEquals(finding.finding.Has_Response__c,true); //assert that the flag has been set.
                
                Test.stopTest();
            }
        }
    }
}