/**
 * This class acts as an item source for "Items To Approve"
 * @story   SQX-3164
 * @author  Anuj Bhandari
 * @date    2017-24-03
 */
public with sharing class SQX_Homepage_Source_ItemsToApprove extends SQX_Homepage_ItemSource {

    private User loggedInUser;
    private String pageName = '';

    /**
     *   Constructor method
     *   @param filter - instance of SQX_Homepage_Filter class that defines the filters to be applied across records
    */
    public SQX_Homepage_Source_ItemsToApprove() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
        pageName = SQX_Utilities.getPageName('', 'SQX_Controlled_Document_Approval');
        isDynamic = true;
        dynamicQueryMap = new Map<String, String>
        {
            SQX.Audit => 'Title__c',
            SQX.AuditProgram => 'Title__c',
            SQX.AuditReport => 'SQX_Audit__r.Title__c',
            SQX.AuditResponse => 'SQX_Audit__c, SQX_Audit__r.Name, SQX_Audit__r.Title__c',
            SQX.ChangeOrder => 'Title__c',
            SQX.Complaint => 'Complaint_Title__c',
            SQX.ControlledDoc => 'Title__c',
            SQX.Response => 'SQX_Finding__c, SQX_Finding__r.Name, SQX_Finding__r.Title__c,' +
                            'SQX_NonConformance__c, SQX_NonConformance__r.Name, SQX_NonConformance__r.NC_Title__c,' +
                            'SQX_CAPA__c, SQX_CAPA__r.Name, SQX_CAPA__r.Title__c'
        };
    }


    /**
     *  This source returns a list of objects when requested for user records
     *  Hence, setting to true
    */
    protected override Boolean returnsObjects(){
        return true;
    }


    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Audit__c.SObjectType => new List<SObjectField>
            {
                SQX_Audit__c.Name,
                SQX_Audit__c.Title__c
            },
            SQX_Audit_Program__c.SObjectType => new List<SObjectField>
            {
                SQX_Audit_Program__c.Title__c
            },
            SQX_Audit_Report__c.SObjectType => new List<SObjectField>
            {
                SQX_Audit_Report__c.Id
            },
            SQX_Audit_Response__c.SObjectType => new List<SObjectField>
            {
                SQX_Audit_Response__c.SQX_Audit__c
            },
            SQX_Change_Order__c.SObjectType => new List<SObjectField>
            {
                SQX_Change_Order__c.Title__c
            },
            SQX_Complaint__c.SObjectType => new List<SObjectField>
            {
                SQX_Complaint__c.Complaint_Title__c
            },
            SQX_Controlled_Document__c.SObjectType => new List<SObjectField>
            {
                SQX_Controlled_Document__c.Title__c
            },
            SQX_Finding_Response__c.SObjectType => new List<SObjectField> 
            {
                SQX_Finding_Response__c.SQX_CAPA__c,
                SQX_Finding_Response__c.SQX_Finding__c,
                SQX_Finding_Response__c.SQX_NonConformance__c
            },
            SQX_Finding__c.SObjectType => new List<SObjectField>
            {
                SQX_Finding__c.Name,
                SQX_Finding__c.Title__c
            },
            SQX_NonConformance__c.SObjectType => new List<SObjectField>
            {
                SQX_NonConformance__c.Name,
                SQX_NonConformance__c.NC_Title__c
            },
            SQX_CAPA__c.SObjectType => new List<SObjectField>
            {
                SQX_CAPA__c.Name,
                SQX_CAPA__c.Title__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            },
            SQX_Investigation__c.SObjectType=>new List<SObjectField>
            {
                SQX_Investigation__c.Name,
                SQX_Investigation__c.SQX_CAPA__c,
                SQX_Investigation__c.SQX_Nonconformance__c,
                SQX_Investigation__c.SQX_Finding__c
            }
        };
    }


    /**
     *   returns a list of objects containing information regarding
     *   approval items for current logged in user
     */
    protected override List<Object> getUserObjects() {
        if(dynamicQueryMap.size() == 0) {
            return new List<Object>();
        }

        List<ProcessInstanceWorkItemExtended> approvalItems = new List<ProcessInstanceWorkItemExtended>();

        //select the list of work items of types that are to be displayed for the current user.
        List<ProcessInstanceWorkItem> workItems = [SELECT 
                                                        Id,
                                                        CreatedDate,
                                                        ProcessInstance.TargetObjectId,
                                                        ProcessInstance.TargetObject.Name,
                                                        ProcessInstance.Targetobject.Type
                                                    FROM ProcessInstanceWorkitem
                                                    WHERE
                                                    Processinstance.TargetObject.Type IN: dynamicQueryMap.keySet()
                                                    AND
                                                    ActorId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ORDER BY CreatedDate DESC];

        Map<String, Set<Id>> moduleWiseRecordIds = new Map<String, Set<Id>>();

        for(ProcessInstanceWorkitem workItem : workItems)
        {
            String module = workItem.Processinstance.TargetObject.Type;

            if(moduleWiseRecordIds.containsKey(module))
            {
                moduleWiseRecordIds.get(module).add(workItem.ProcessInstance.TargetObjectId);
            }
            else
            {
                moduleWiseRecordIds.put(module, new Set<Id> { workItem.ProcessInstance.TargetObjectId });
            }
        }

        // fetch title from each record
        Map<Id, SObject> recordsMap = new Map<Id, SObject>();
        for(String module : moduleWiseRecordIds.keySet())
        {
            Set<Id> recordIds = moduleWiseRecordIds.get(module);

            if(!String.isBlank(dynamicQueryMap.get(module)))
            {
                String query = 'SELECT Id, ' + dynamicQueryMap.get(module) + ' FROM ' + module + ' WHERE Id IN: recordIds';

                if(module == SQX.Response)
                {
                    query += ' AND SQX_Finding__r.Status__c != \'' + SQX_Finding.STATUS_CLOSED + '\'' +
                             ' AND SQX_CAPA__r.Status__c != \'' + SQX_CAPA.STATUS_CLOSED + '\'' +
                             ' AND SQX_NonConformance__r.Status__c != \'' + SQX_NC.STATUS_CLOSED + '\'';
                }

                recordsMap.putAll(Database.query(query));
            }
            else
            {
                // generate a blank sobject record
                for(Id recordId : recordIds)
                {
                    recordsMap.put(recordId, recordId.getSObjectType().newSObject(recordId));
                }
            }
        }

        for(ProcessInstanceWorkitem workItem : workItems)
        {
            if(recordsMap.containsKey(workItem.ProcessInstance.TargetObjectId))
            {
                ProcessInstanceWorkItemExtended item = new ProcessInstanceWorkItemExtended();
                item.workItem = workItem;
                item.relatedRecord = recordsMap.get(workItem.ProcessInstance.TargetObjectId);

                approvalItems.add(item);
            }
        }

        return approvalItems;
    }

    /**
    *   Method returns a SQX_Homepage_Item type item from the given object
    *   @param object - Contains information necessary for creating an 'Item to Approve' homepage item
    */
    public override SQX_Homepage_Item getHomePageItemFromObject(Object obj) {

        SQX_Homepage_Item approvalItem = new SQX_Homepage_Item();
        
        ProcessInstanceWorkItemExtended item = (ProcessInstanceWorkItemExtended) obj;
         
        approvalItem.itemId = item.workItem.Processinstance.TargetObjectId;

        approvalItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE;

        approvalItem.createdDate = Date.valueOf(item.workItem.CreatedDate);

        // module based info
        SObjectType targetType = item.relatedRecord.getSObjectType();

        Boolean setReassignLink = targetType != SQX_Finding_Response__c.SObjectType && targetType != SQX_Audit_Response__c.SObjectType;

        PageReference approvalRef = null;
        String actionUrl = '', itemTitle = '', itemName = '';
		
        if(targetType == SQX_Finding_Response__c.SObjectType)
        {
            String recordId = '';
            if(item.relatedRecord.get('SQX_CAPA__c') != null)
            {
                // CAPA response
                recordId = (String) item.relatedRecord.get('SQX_CAPA__c');

                approvalRef = Page.SQX_CAPA;

                itemName = (String) item.relatedRecord.getSObject('SQX_CAPA__r').get('Name');
                itemTitle = (String) item.relatedRecord.getSObject('SQX_CAPA__r').get('Title__c');
                approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_CAPA;
            }
            else if(item.relatedRecord.get('SQX_NonConformance__c') != null)
            {
                // NC response
                recordId = (String) item.relatedRecord.get('SQX_NonConformance__c');

                approvalRef = Page.SQX_NC;
                
                itemName = (String) item.relatedRecord.getSObject('SQX_NonConformance__r').get('Name');
                itemTitle = (String) item.relatedRecord.getSObject('SQX_NonConformance__r').get('NC_Title__c');

                approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_NC;
            }
            else
            {
                // Finding Response
                recordId = (String) item.relatedRecord.get('SQX_Finding__c');

                approvalRef = Page.SQX_Finding;
                
                itemName = (String) item.relatedRecord.getSObject('SQX_Finding__r').get('Name');
                itemTitle = (String) item.relatedRecord.getSObject('SQX_Finding__r').get('Title__c');

                approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_FINDING;
            }

            approvalRef.getParameters().put('id', recordId);
            approvalRef.getParameters().put('initialTab','responseHistoryTab');
            approvalRef.getParameters().put('expandRecord', approvalItem.itemId);
            approvalRef.getParameters().put('expandRecordType', SQX.Response);
            actionUrl = approvalRef.getURL();

        }
        else if(targetType == SQX_Audit_Response__c.SObjectType)
        {
            String recordId = (String) item.relatedRecord.get('SQX_Audit__c');

            approvalRef = Page.SQX_Audit;
            approvalRef.getParameters().put('id', recordId);
            approvalRef.getParameters().put('initialTab','responseHistoryTab');
            approvalRef.getParameters().put('expandRecord', approvalItem.itemId);
            approvalRef.getParameters().put('expandRecordType', SQX.AuditResponse);

            itemName = (String) item.relatedRecord.getSObject('SQX_Audit__r').get('Name');
            itemTitle = (String) item.relatedRecord.getSObject('SQX_Audit__r').get('Title__c');
            actionUrl = approvalRef.getURL();
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_AUDIT;
        }
        else if(targetType == SQX_Controlled_Document__c.SObjectType)
        {
            approvalRef = new Pagereference(pageName);
            approvalRef.getParameters().put('id', approvalItem.itemId);
            approvalRef.getParameters().put('reqId', item.workItem.Id);
            approvalRef.getParameters().put('retURL', SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));

            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.get('Title__c');
            actionUrl = approvalRef.getUrl();
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_DOCUMENT_MANAGEMENT;
        }
        else if(targetType == SQX_Audit__c.SObjectType)
        {
            approvalRef = Page.SQX_Audit;
            approvalRef.getParameters().put('id', approvalItem.itemId);

            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.get('Title__c');
            actionUrl = approvalRef.getUrl();
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_AUDIT;
        }
        else if(targetType == SQX_Audit_Report__c.SObjectType)
        {
            approvalRef = Page.SQX_Audit_Report;
            approvalRef.getParameters().put('id', approvalItem.itemId);

            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.getSObject('SQX_Audit__r').get('Title__c');
            actionUrl = approvalRef.getUrl();
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_AUDIT;
        }
        else if(targetType == SQX_Change_Order__c.SObjectType)
        {
            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.get('Title__c');
            actionUrl = '/'+approvalItem.itemId;
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_CHANGE_ORDER;
        }
        else if(targetType == SQX_Complaint__c.SObjectType){
            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.get('Complaint_Title__c');
            actionUrl = '/'+approvalItem.itemId;
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_COMPLAINT;
        }
        else if(targetType == SQX_Audit_Program__c.SObjectType)
        {
            approvalRef = new ApexPages.StandardController(new SQX_Audit_Program__c(Id = approvalItem.itemId)).view();

            itemName = item.workItem.ProcessInstance.TargetObject.Name;
            itemTitle = (String) item.relatedRecord.get('Title__c');
            actionUrl = approvalRef.getUrl();
            approvalItem.moduleType = SQX_Homepage_Constants.MODULE_TYPE_AUDIT;
        }

        // set actions for the current item
        approvalItem.actions = getApprovalItemActions(approvalItem.itemId, actionUrl, item.workItem.Id, setReassignLink);

        // set feed text for the current item
        approvalItem.feedText = getFeedText(itemName, itemTitle);

        // set user details of the user related to this item
        approvalItem.creator = loggedInUser;

        return approvalItem;
    }


    /**
     *   Method returns a list of actions associated with the Response items
     *   @param actionUrl - action url to be set for each action
     *   @param itemId - process instance item id
     *   @param setReassignLink - boolean to check if reassign is to be displayed or not
     */
    private List<SQX_Homepage_Item_Action> getApprovalItemActions(String recordId, String actionUrl, String itemId, Boolean setReassignLink) {

        // adding action- Approve or Reject
        SQX_Homepage_Item_Action approveOrReject = new SQX_Homepage_Item_Action();

        approveOrReject.name = SQX_Homepage_Constants.ACTION_APPROVE_REJECT;
        approveOrReject.isLightningComponent =false;
        Id mainRecordId = (Id) recordId;
        CQ_Action__mdt[] actions=SQX_Lightning_Helper.getActionsFor(mainRecordId.getSobjectType().getDescribe().getName(),'approval');
        if(actions!=null && actions.size() > 0){
            for(CQ_Action__mdt cm:actions){
                if(cm.compliancequest__Component_Type__c=='Lightning Component'){
                    approveOrReject.actionUrl = cm.compliancequest__Namespace__c +'__'+ cm.compliancequest__Component_Name__c;
                    approveOrReject.actionId = itemId;
                    approveOrReject.recordId = recordId;
                    approveOrReject.isLightningComponent =true;
                }
            }
        }else{
            approveOrReject.actionUrl = SQX_Homepage_Constants.SF_BASE_URL + actionUrl;
        }
        
        approveOrReject.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT;

        approveOrReject.supportsBulk = false;


        if(setReassignLink) {
            // adding action - Reassign
            SQX_Homepage_Item_Action reassign = new SQX_Homepage_Item_Action();

            reassign.name = SQX_Homepage_Constants.ACTION_REASSIGN;

            reassign.actionUrl = SQX_Homepage_Constants.SF_BASE_URL + '/' + itemId + '/e?et=REASSIGN&retURL=' + SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true);

            reassign.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_REASSIGN;

            reassign.supportsBulk = false;

            return new List <SQX_Homepage_Item_Action> { approveOrReject, reassign };
        }
        
        return new List <SQX_Homepage_Item_Action> { approveOrReject };
        
    }


    /**
     *   Method to return feed text to be displayed for Open Audit - Needing Response type
     */
    private String getFeedText(string itemName, string itemTitle) {

        String feed = '';

        if(String.isBlank(itemTitle))
        {
            String template = SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITHOUT_TITLE;
            feed = String.format(template, new String[] {
                itemName
            });
        }
        else
        {
            String template = SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE;
            return String.format(template, new String[] {
                itemName,
                itemTitle
            });
        }

        return feed;
    }


    /**
        Inner class to hold extra information about the associated record
        of a given process instance workitem
    */
    public class ProcessInstanceWorkItemExtended
    {
        public ProcessInstanceWorkItem workItem;

        public SObject relatedRecord;

        public ProcessInstanceWorkItemExtended() {}
    }

}