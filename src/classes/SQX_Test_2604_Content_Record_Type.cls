/**
* test class for the content record type
*/
@isTest
public class SQX_Test_2604_Content_Record_Type{

    static Boolean runAllTests = true,
                   run_givenATemplateDoc_WhenADocIsSaved_TemplateContentVersionIsCreated = false,
                   run_givenAControlledDoc_WhenADocIsSaved_ControlledDocContentVersionIsCreated = false;

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    
    /**
    * Given : Controlled doc is saved with record type template document
    * When  : Document is saved
    * Then : Content Document with template document record type is saved
    * @date: 2016-10-03
    * @story: [SQX-2604]
    */
    public static testMethod void givenATemplateDoc_WhenADocIsSaved_TemplateContentVersionIsCreated(){

        if(!runAllTests && !run_givenATemplateDoc_WhenADocIsSaved_TemplateContentVersionIsCreated){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_TEMPLATE_API_NAME).save(true);
            Id contentVersionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_TEMPLATE_DOCUMENT);
            ContentVersion cv = [SELECT Id, RecordTypeId FROM ContentVersion WHERE Controlled_Document__c =: cDoc.doc.Id];

            System.assertEquals(contentVersionRecordTypeId, cv.RecordTypeId, 'Expected template document record type to be found in content version');

        }
    }
    
    /**
    * Given : Controlled doc is saved with record type Controlled document
    * When  : Document is saved
    * Then : Content Document with Controlled document record type is saved
    * @date: 2016-10-03
    * @story: [SQX-2604]
    */
    public static testMethod void givenAControlledDoc_WhenADocIsSaved_ControlledDocContentVersionIsCreated(){

        if(!runAllTests && !run_givenAControlledDoc_WhenADocIsSaved_ControlledDocContentVersionIsCreated){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_CNTRLLED_API_NAME).save(true);
            Id contentVersionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CONTROLLED_DOCUMENT);
            ContentVersion cv = [SELECT Id, RecordTypeId FROM ContentVersion WHERE Controlled_Document__c =: cDoc.doc.Id];

            System.assertEquals(contentVersionRecordTypeId, cv.RecordTypeId, 'Expected controlled document record type to be found in content version');

        }
    }
}