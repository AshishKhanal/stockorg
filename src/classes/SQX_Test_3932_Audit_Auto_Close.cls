/*
 * description: test for Auto Close Audit
 */
@IsTest
public class SQX_Test_3932_Audit_Auto_Close{
    final static String RESPONSE_SUBMITTER = 'Std user 1',
                        AUDITEE_CONTACT = 'Std user 2',
                        ADMIN = 'Admin user 1';
    @testsetup
    static void setupData() {
        //Arrange: Create users
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN);
        User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, RESPONSE_SUBMITTER);
        User auditeeContact =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, AUDITEE_CONTACT);
    }

    /**
    * helper funtion to setup Audit, Finding 
    * @param adminUser to create Test Audit and ResponseSubmitter for Auditee Contact 
    * @param responseSubmitter to assign Auditee contact as response submitter
    * @return  returns list of Ids of newly created audits
    **/  
    public static List<Id> arrangeAudit(User adminUser, User responseSubmitter){
        /*
        * Arrange Audit as follows
        * Audit 1: Audit with
        *        'Open' Finding
        *         'Auto Close' field is unchecked
        * Audit 2: Audit with
        *        'Complete' Finding
        *        'Open' Actions
        *        'Auto Close' field is checked
        */

        SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
        audit1.audit.Title__c = 'Audit-1';
        audit1.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit1.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit1.setStage(SQX_Audit.STAGE_RESPOND);
        audit1.setPolicy(false, true, true);
        audit1.audit.Start_Date__c = Date.Today();
        audit1.audit.End_Date__c = Date.Today().addDays(10);


        SQX_Test_Audit audit2 = new SQX_Test_Audit(adminUser);
        audit2.audit.Title__c = 'Audit-2';
        audit2.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit2.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit2.setStage(SQX_Audit.STAGE_RESPOND);
        audit2.setPolicy(false, true, true);
        audit2.audit.Start_Date__c = Date.Today();
        audit2.audit.End_Date__c = Date.Today().addDays(20);
        audit2.audit.Auto_Close__c = true;


        insert new List<SQX_Audit__c> { audit1.audit,  audit2.audit};

        SQX_Test_Finding auditFinding1 =  audit1.addAuditFinding()
                                                .setRequired(false, false, false, false, false)
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);
        auditFinding1.finding.SQX_Assignee__c = responseSubmitter.Id;

        SQX_Test_Finding auditFinding2 =  audit2.addAuditFinding()
                                                .setRequired(false, false, false, false, false)
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);
        auditFinding2.finding.SQX_Assignee__c = responseSubmitter.Id;

        insert new List<SQX_Finding__c> {auditFinding1.finding,auditFinding2.finding };
        
        return new List<Id>{  audit1.audit.Id, audit2.audit.Id};
    }

    /**
    * helper funtion to setup Audit Response, Finding Response,Actions ,Investigations and Response Inclusions 
    * @param List of audit ids for which Audit response is to be created
    * @return  returns List of audits
    **/  
    public static List<Id> arrangeAuditwithResponse(List<Id> auditId){

        //Get Findings of Audits
        List<SQX_Finding__c> findingList = [SELECT Id, SQX_Audit__c FROM SQX_Finding__c WHERE SQX_Audit__c IN: auditId];

        /* Arrange: 
        *  Create Audit Response auditResponse1 for Audit 1  and auditResponse2 for Audit 2
        *  Create Finding response findingResponse1 for Finding 1 and findingResponse2 for Finding 2
        *  Create Investigation investigation1 for Finding 1 and investigation2 for Finding 2 
        *  Create Implementation action1 for Audit1 and action2 for Audit2
        *  Create Response Inclusion respInclusion1 for findingResponse1 and respInclusion2 for findingResponse2
        **/
        SQX_Audit_Response__c auditResponse1 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= auditId[0],
                                               Audit_Response_Summary__c ='Audit Response for finding 1',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        SQX_Audit_Response__c auditResponse2 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= auditId[1],
                                               Audit_Response_Summary__c ='Audit Response for finding 2',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        insert new List<SQX_Audit_Response__c> {auditResponse1,  auditResponse2};
        
        SQX_Finding_Response__c findingResponse1 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = findingList[0].Id,
                                               SQX_Audit_Response__c = auditResponse1.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse2 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = findingList[1].Id,
                                               SQX_Audit_Response__c = auditResponse2.Id,
                                               Response_Summary__c = 'Response for audit');


        insert new List<SQX_Finding_Response__c> { findingResponse1, findingResponse2 };


        SQX_Investigation__c investigation1 = new SQX_Investigation__c(
                                                SQX_Finding__c = findingList[0].Id,
                                                Investigation_Summary__c = 'Investigation for finding1');

        SQX_Investigation__c investigation2 = new SQX_Investigation__c(
                                                SQX_Finding__c = findingList[1].Id,
                                                Investigation_Summary__c = 'Investigation for finding2');

        insert new List<SQX_Investigation__c> {  investigation1, investigation2 };

        SQX_Action__c action1 = new SQX_Action__c(SQX_Audit__c = auditId[0],
                                                    Plan_Number__c = 'PLAN-01',
                                                    Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(10),
                                                    Status__c = SQX_Implementation_Action.STATUS_COMPLETE);

        SQX_Action__c action2 = new SQX_Action__c(SQX_Audit__c = auditId[1],
                                                    Plan_Number__c = 'PLAN-02',
                                                    Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(20),
                                                    Status__c = SQX_Implementation_Action.STATUS_COMPLETE);

        insert new List<SQX_Action__c> { action1, action2};

        SQX_Response_Inclusion__c respInclusion1 = new SQX_Response_Inclusion__c(
                                                        Type__c = SQX_Response_Inclusion.INC_TYPE_INVESTIGATION,
                                                        SQX_Response__c = findingResponse1.Id,
                                                        SQX_Investigation__c = investigation1.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion2 = new SQX_Response_Inclusion__c(
                                                        Type__c = SQX_Response_Inclusion.INC_TYPE_INVESTIGATION,
                                                        SQX_Response__c = findingResponse2.Id,
                                                        SQX_Investigation__c = investigation2.Id
                                                    );

        insert new List<SQX_Response_Inclusion__c> { respInclusion1, respInclusion2 }; 

        return auditId;
    }
   
    /*
    *   test for checking whether Audit Stage is set appropriately 
    */
    public static testmethod void givenAudit_thenAppropriateStageIsSetBasedOnAuditState(){

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User responseSubmitter = users.get(RESPONSE_SUBMITTER);
        User adminUser = users.get(ADMIN);
        List<Id> auditIdList = new List<Id>();
        System.runas(adminUser){
            /*
            * Arange : Add Audit, Finding,
            */
            auditIdList = arrangeAudit(adminUser, responseSubmitter);
        }
        System.runas(responseSubmitter){
            /*
            * Arange : Add Audit Response, Finding Response,Actions ,Investigations and Response Inclusions as Response submitter
            */
            auditIdList = arrangeAuditwithResponse(auditIdList);

            // Act : Set Audit Stage 
            SQX_Stage_Setter.setRecordStage(auditIdList);
            
            final Map<String, String> auditExpectedStage = new Map<String, String> {
               
                'Audit-1' => SQX_Audit.STAGE_READY_FOR_CLOSURE,
                'Audit-2' => SQX_Audit.STAGE_CLOSED
            };

            //Assert : Expected Audit 1 to be in 'Ready for Closure' Stage whereas Audit 2 is Auto Closed( Since Auto Close was checked). 
            for ( SQX_Audit__c audit : [SELECT Id, Title__c, Stage__c FROM SQX_Audit__c WHERE Id IN: auditIdList ]) {
                System.assertEquals(auditExpectedStage.get(audit.Title__c), audit.Stage__c, 'Audit\'s stage is invalid');
            }
        }
        
    }


    /*
    *   test for validating SQX-4687(Bug Preventing Auditee Contact to Approve Audit Report)
    */
    public static testmethod void givenAuditwithAutoCloseEnabled_WhenAuditReportIsEnabled_AuditIsClosed(){

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User auditeeContact = users.get(AUDITEE_CONTACT);
        User stdUser = users.get(RESPONSE_SUBMITTER);
        User adminUser = users.get(ADMIN);
        List<Id> auditIdList = new List<Id>();

        SQX_Test_Audit audit1;
        SQX_Audit_Report__c auditReport1;

        System.runas(stdUser){

            /*
            * Arange : Add Audit
            */
            audit1 = new SQX_Test_Audit(adminUser);
            audit1.audit.Title__c = 'Audit-1';
            audit1.audit.SQX_Auditee_Contact__c= auditeeContact.Id;
            audit1.setStatus(SQX_Audit.STATUS_OPEN);
            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS);
            audit1.setPolicy(false, false, false);
            audit1.audit.Start_Date__c = Date.Today();
            audit1.audit.End_Date__c = Date.Today().addDays(10);
            audit1.audit.Auto_Close__c = true;
            audit1.save();

            //Arrange: Generate audit report and send for approval
            auditReport1 = audit1.generateReport(true);
        }
        System.runas(auditeeContact){
            //Act: Approve Audit Report
            auditReport1.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            auditReport1.Approval_Status__c = 'Approved';

            List<Database.SaveResult> result = new SQX_DB().withoutSharing().continueOnError().op_update(new List<SQX_Audit_Report__c> { auditReport1 },  new List<Schema.SObjectField>{});
            
            //Assert: Save did go through and the audit was approved.
            System.assertEquals(true, result[0].isSuccess());
            
            //Assert: Since Auto Close is enabled, Audit is closed.
            System.assertEquals(SQX_Audit.STAGE_CLOSED, [SELECT Stage__c FROM SQX_Audit__c WHERE Id = : audit1.audit.Id].Stage__c);
        }  
    }
}