/**
* This class stores the common operations in Account object
* It contains the Status and Approval Status of the Account object
* It also contains the trigger handler for the Account
*
* @author Pradhanta Bhandari
* @date 2014/2/13
*/
public with sharing class SQX_Account{
    public static final String APPROVAL_STATUS_NONE = '';
    public static final String APPROVAL_STATUS_APPROVED = 'Approved';
    public static final String APPROVAL_STATUS_DISAPPROVED = 'Disapproved';
    
    public static final String STATUS_INACTIVE = 'Inactive';
    public static final String STATUS_ACTIVE = 'Active';

    /**
    * This a trigger handler class for Account, so it contains all the actions that are performed
    * based on Account's field update
    *
    */
    public with sharing class Bulkified {
        /**
        * This trigger action ensures that all disapproved account are inactive.
        * This is in line with SQX-99
        * [Given that Supplier is Approved and Active 
        *        When Supplier is now DisApproved
        *        Then Supplier becomes Inactive, all the parts and Service become DisApproved and Inactive]
        *
        * @author Pradhanta Bhandari
        * @date 2014/2/5
        * @requirement SQX-99
        */
        public void ensureDisapprovedAccountAreInactive(List<Account> newAccounts){
            for(Account account : newAccounts){
                if(account.Approval_Status__c == SQX_Account.APPROVAL_STATUS_DISAPPROVED && account.Status__c != SQX_Account.STATUS_INACTIVE ){
                    account.Status__c = SQX_Account.STATUS_INACTIVE ;
                }
            }
            
        }

        /**
        * This trigger action will propagate approval status, basically once the account gets disapproved
        * all the related supplier parts(account-parts) and supplier services (account-services) are automatically
        * deactivated or disapproved accordingly.
        * If account has been disapproved then the related item is deactived and disapproved
        * On the other hand if the account is deactivated then the related item is only deactivated
        * This is also related to SQX-99.
        *
        * @author Pradhanta Bhandari
        * @date 2014/2/4
        * @requirement SQX-99
        */
        public void propagateApprovalStatus(List<Account> newAccounts, List<Account> oldAccounts){
            Set<Id> deactivatedAccounts = new Set<Id>();
            Set<Id> disapprovedAccounts = new Set<Id>();
            
            
            
            integer index = 0;
            for(Account account : newAccounts){
                Account oldAccount = oldAccounts.get(index);
                
                if(oldAccount.Status__c != account.Status__c ||
                    oldAccount.Approval_Status__c != account.Approval_Status__c){
                    //there is some change to the active state or approval status
                    

                    
                    if(oldAccount.Approval_Status__c != account.Approval_Status__c && account.Approval_Status__c == SQX_Account.APPROVAL_STATUS_DISAPPROVED){
                        disapprovedAccounts.add(account.Id);
                        
                    }
                    
                    if(oldAccount.Status__c != account.Status__c && account.Status__c == SQX_Account.STATUS_INACTIVE ){
                        deactivatedAccounts.add(account.Id);
                    }
                    
                }
            
                index++;
            }
            
            Map<Schema.SObjectType, List<sObject>> allObjects = new Map<Schema.SObjectType, List<sObject>>();
            Map<Schema.SObjectType, List<Schema.SObjectField>> allModifiedFields = new Map<Schema.SObjectType, List<Schema.SObjectField>>();

            Map<Id, sObject> objectsToUpdate = new Map<Id, sObject>();
            //deactivate and disapprove supplier parts
            for(List<SQX_Supplier_Part__c> supplierParts : [SELECT Id, Active__c, Approved__c, Account__c FROM SQX_Supplier_Part__c
                                            WHERE Account__c IN : deactivatedAccounts OR
                                            Account__c IN : disapprovedAccounts]){
                                            
                for(SQX_Supplier_Part__c supplierPart : supplierParts){
                    if(deactivatedAccounts.contains(supplierPart.Account__c)){
                        //if account has been deactivated
                        //    Given that Supplier is Approved and Active 
                        //    When Supplier is InActivated 
                        //    Then Approved Parts and Service remains Approved, but are all InActivated and cannot be Activated, The 
                        
                        supplierPart.Active__c = false;
                        objectsToUpdate.put(supplierPart.Id, supplierPart);
                    }
                    
                    if(disapprovedAccounts.contains(supplierPart.Account__c)){
                        //if account has been disapproved
                        //    Given that Supplier is Approved and Active 
                        //    When Supplier is now DisApproved 
                        //    Then Supplier becomes Inactive, all the parts and Service become DisApproved and Inactive 
                        //    And We cannot Activate the Supplier and Cannot Active or Approve any Part and Service for the Supplier 
                        
                        supplierPart.Approved__c = false;
                        objectsToUpdate.put(supplierPart.Id, supplierPart);
                    }
                }
            }
            
            if(objectsToUpdate.values().size() > 0){
                Schema.SObjectType supplierPartType = objectsToUpdate.values().get(0).getSObjectType();
                allModifiedFields.put(supplierPartType, new List<Schema.SObjectField>{ SQX_Supplier_Part__c.Active__c, SQX_Supplier_Part__c.Approved__c});
                allObjects.put(supplierPartType, objectsToUpdate.values());
            }
            
            
            
            /* Similarly change supplier service*/
            
            objectsToUpdate = new Map<Id, sObject>();
            //deactivate and disapprove supplier parts
            for(List<SQX_Supplier_Service__c> supplierServices : [SELECT Id, Active__c, Approved__c, Account__c FROM SQX_Supplier_Service__c
                                            WHERE Account__c IN : deactivatedAccounts OR
                                            Account__c IN : disapprovedAccounts]){
                                            
                for(SQX_Supplier_Service__c supplierService : supplierServices){
                    if(deactivatedAccounts.contains(supplierService.Account__c)){
                        //if account has been deactivated
                        //    Given that Supplier is Approved and Active 
                        //    When Supplier is InActivated 
                        //    Then Approved Parts and Service remains Approved, but are all InActivated and cannot be Activated, The 
                        
                        supplierService.Active__c = false;
                        objectsToUpdate.put(supplierService.Id, supplierService);
                    }
                    
                    if(disapprovedAccounts.contains(supplierService.Account__c)){
                        //if account has been disapproved
                        //    Given that Supplier is Approved and Active 
                        //    When Supplier is now DisApproved 
                        //    Then Supplier becomes Inactive, all the parts and Service become DisApproved and Inactive 
                        //    And We cannot Activate the Supplier and Cannot Active or Approve any Part and Service for the Supplier 
                        
                        supplierService.Approved__c = false;
                        objectsToUpdate.put(supplierService.Id, supplierService);
                    }
                }
            }
            
            if(objectsToUpdate.values().size() > 0){
                Schema.SObjectType supplierServiceType = objectsToUpdate.values().get(0).getSObjectType();
                allModifiedFields.put(supplierServiceType, new List<Schema.SObjectField>{ SQX_Supplier_Service__c.Active__c, SQX_Supplier_Service__c.Approved__c});
                allObjects.put(supplierServiceType, objectsToUpdate.values());
            }

            new SQX_DB().op_update(allObjects, allModifiedFields);
            
        }
    }
}