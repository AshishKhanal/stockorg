/**
* This is controller for sign off UI(user and trainer) and provide basic information.
*/
public with sharing class SQX_Extension_Document_Training_Sign_Off {


    public with sharing class TrainingRecords {
        @AuraEnabled
        public List<SQX_Personnel_Document_Training__c> trainingsToSignOff {get; set;}

        @AuraEnabled
        public List<SQX_Personnel_Document_Training__c> trainingsThatCantBeSignedOff {get; set;}

        @AuraEnabled
        public List<SQX_Personnel_Document_Training__c> trainingsNeedingAssessment {get; set;}
    }

    /**
    * This method use to return personnel document training information
    */
    @AuraEnabled
    public static TrainingRecords getPersonnelDocumentTrainings(List<Id> recordIds, String purpose) {
        TrainingRecords records = new TrainingRecords();

        SQX_Training_Signoff ext = new SQX_Training_Signoff(purpose != 'usersignoff');
        ext.initializeDocumentTrainings(new Set<Id>(recordIds));
        records.trainingsToSignOff = ext.getDocumentTrainingsToSignOff();
        records.trainingsThatCantBeSignedOff = ext.getDocumentTrainingsNotRequiringSignOff();
        records.trainingsNeedingAssessment = ext.getDocumentTrrainingsRequiringAssessmentToPass();

        return records;
    }

    /**
    * This method used to sign off personnel document trainings
    * @param 'recordIds' list of personnel document trainings
    * @param 'purpose' sign off mode (user or trainer sign off)
    */
    @AuraEnabled
    public static SQX_Action_Response performSignOff(List<Id> recordIds, List<SQX_Personnel_Document_Training__c> trainingsWithModifications, String purpose, String username, String password) {
        SQX_Training_Signoff ext = new SQX_Training_Signoff(purpose != 'usersignoff');
        ext.initializeDocumentTrainings(new Set<Id>(recordIds));
        if(trainingsWithModifications != null && !trainingsWithModifications.isEmpty()) {
            Map<Id, SQX_Personnel_Document_Training__c> trainings = new Map<Id, SQX_Personnel_Document_Training__c>(ext.getDocumentTrainingsToSignOff());
            for(SQX_Personnel_Document_Training__c training : trainingsWithModifications) {
                if(trainings.containsKey(training.Id)) {
                    SQX_Personnel_Document_Training__c tr = trainings.get(training.Id);
                    for(String field : training.getPopulatedFieldsAsMap().keySet()) {
                        tr.put(field, training.get(field));
                    }
                }
            }
        }
        ext.SigningOffPassword = password;
        ext.SigningOffUsername = username;

        SQX_Action_Response response = ext.signOffUsingApi(false);

        return response;
    }
}