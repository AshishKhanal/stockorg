/**
 * even though this class is not decorated as test it is a test helper class
 * The purpose of this class is to make the tests pass in an environment where there is no approval process.
 * It mimics the behaviour of approval process,
 * Method process -> handles approval submission request
 * Method processApproval ->  handles approve/reject for approval requests
 * 
 * WITHOUT SHARING used
 * -------------
 * Salesforce provides update access to objects in approval process. We mimic this behaviour using without sharing.
 */
public without sharing class SQX_Test_Mock_Approval_Util{
    public static Set<Id> objectsInApproval = new Set<Id>();
    
    
    /**
    * This is the main method that takes the necessary action for when submitting for approval i.e. perform field updates
    * as would be done by approval process.
    * @param submissionRequests 
    */
    public static List<SQX_Approval_Util.ResultWrapper> process(List<Approval.ProcessSubmitRequest> submissionRequests){

        //The list of approval results to be returned
        List<SQX_Approval_Util.ResultWrapper> results = new List<SQX_Approval_Util.ResultWrapper>();
        
        //grouping approval requests based on the type of approval we are interested in.
        Map<Id, SQX_Approval_Util.ResultWrapper> 
            responseApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            auditResponseApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            containmentApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            investigationApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            auditReportApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            complaintApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            controlledDocumentApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>();
        
        //iterate over the approval requests and group them for further processing
        for(Approval.ProcessSubmitRequest submissionRequest: submissionRequests){
            SQX_Approval_Util.ResultWrapper result = new SQX_Approval_Util.ResultWrapper();
            result.i_entityID = submissionRequest.getObjectID();
            
            //Grouping the objects based on the SObjectType
            //Note: We only group the objects we are interested in for other types we simply
            //set the approval request as successful.
            SObjectType objectType = result.i_entityID.getSObjectType();
            if(objectType == SQX_Finding_Response__c.getSObjectType()){
                responseApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Audit_Response__c.getSObjectType()){
                auditResponseApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Investigation__c.getSObjectType()){
                investigationApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Containment__c.getSObjectType()){
                containmentApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Audit_Report__c.getSObjectType()){
                auditReportApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Complaint__c.getSObjectType()){
                complaintApprovals.put(result.i_entityID, result);
            }
            else if(objectType == SQX_Controlled_Document__c.getSObjectType()){
                controlledDocumentApprovals.put(result.i_entityID, result);
            }
            else{
                result.i_isSuccess = true;
                objectsInApproval.add(result.i_entityID);
            }
            
            results.add(result);
        }
        

        //perform field updates on each type of action
        List<SObject> objectsToUpdate = new List<SObject>();

        if(responseApprovals.values().size() > 0){
            //Note: i am ignoring initial submitter
            for(SQX_Finding_Response__c response : [SELECT Id, Status__c, Approval_Status__c
                                                   FROM SQX_Finding_Response__c
                                                    WHERE Id IN : responseApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = responseApprovals.get(response.Id);

                if(response.Status__c == SQX_Finding_Response.STATUS_DRAFT){
                    response.Status__c = SQX_Finding_Response.STATUS_IN_APPROVAL;
                    response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PENDING;
                    objectsToUpdate.add(response);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(auditResponseApprovals.values().size() > 0){
            //Note: i am ignoring initial submitter
            for(SQX_Audit_Response__c auditResponse : [SELECT Id, Status__c, Approval_Status__c
                                                   FROM SQX_Audit_Response__c
                                                    WHERE Id IN : auditResponseApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = auditResponseApprovals.get(auditResponse.Id);

                if(auditResponse.Status__c == SQX_Audit_Response.STATUS_DRAFT){
                    auditResponse.Status__c = SQX_Audit_Response.STATUS_IN_APPROVAL;
                    auditResponse.Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_PENDING;
                    objectsToUpdate.add(auditResponse);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(containmentApprovals.values().size() > 0){
            for(SQX_Containment__c containment : [SELECT Id, Status__c
                                                   FROM SQX_Containment__c
                                                    WHERE Id IN : containmentApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = containmentApprovals.get(containment.Id);

                if(containment.Status__c == SQX_Investigation.STATUS_DRAFT){
                    containment.Status__c = SQX_Investigation.STATUS_IN_APPROVAL;

                    objectsToUpdate.add(containment);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(investigationApprovals.values().size() > 0){
            for(SQX_Investigation__c investigation : [SELECT Id, Status__c
                                                   FROM SQX_Investigation__c
                                                    WHERE Id IN : investigationApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = investigationApprovals.get(investigation.Id);

                if(investigation.Status__c == SQX_Investigation.STATUS_DRAFT){
                    investigation.Status__c = SQX_Investigation.STATUS_IN_APPROVAL;
                    investigation.Approval_Status__c = SQX_Investigation.APPROVAL_STATUS_PENDING;

                    objectsToUpdate.add(investigation);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(auditReportApprovals.values().size() > 0){
            for(SQX_Audit_Report__c auditReport : [SELECT Id, Status__c, Approval_Status__c
                                                   FROM SQX_Audit_Report__c
                                                    WHERE Id IN : auditReportApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = auditReportApprovals.get(auditReport.Id);

                if(auditReport.Status__c == SQX_Audit_Report.STATUS_DRAFT){
                    auditReport.Status__c = SQX_Audit_Report.STATUS_IN_APPROVAL;
                    auditReport.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_NONE;
                    objectsToUpdate.add(auditReport);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(complaintApprovals.values().size() > 0){
            //Note: i am ignoring initial submitter
            for(SQX_Complaint__c complaint : [SELECT Id, Record_Stage__c, Status__c, Require_Closure_Review__c, Is_Recalled__c
                                                   FROM SQX_Complaint__c
                                                    WHERE Id IN : complaintApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = complaintApprovals.get(complaint.Id);

                if(complaint.Require_Closure_Review__c){
                    complaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSURE_REVIEW;
                    objectsToUpdate.add(complaint);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(controlledDocumentApprovals.values().size() > 0){
            for(SQX_Controlled_Document__c doc : [SELECT Id, Document_Status__c
                                                    FROM SQX_Controlled_Document__c
                                                    WHERE Id IN : controlledDocumentApprovals.keySet()]){

                SQX_Approval_Util.ResultWrapper result = controlledDocumentApprovals.get(doc.Id);

                if(doc.Document_Status__c == SQX_Controlled_Document.STATUS_DRAFT){
                    doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;

                    objectsToUpdate.add(doc);
                    result.i_isSuccess = true;
                    objectsInApproval.add(result.i_entityID);
                }
                else{
                    result.i_errors = new List<Database.Error>();
                    //don't add any errors in this because Database.Error wan't allow.
                    result.i_isSuccess = false;
                }
            }
        }

        if(objectsToUpdate.size() > 0)
            update objectsToUpdate;
        
        return results;
    }

    /**
    * This is the method that performs approval or rejection field updates for test classes.
    * @param objects the list of objects to approve/reject
    * @param action the type of action that is to be performed i.e. approve or reject
    */
    public static List<SQX_Approval_Util.ResultWrapper> processApproval(List<SObject> objects, String action){
        List<SQX_Approval_Util.ResultWrapper> results = new List<SQX_Approval_Util.ResultWrapper>();
        
        Map<Id, SQX_Approval_Util.ResultWrapper> responseApprovals = 
            new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            auditResponseApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            containmentApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            investigationApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            auditReportApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            complaintApprovals = new Map<Id, SQX_Approval_Util.ResultWrapper>(),
            allMap = new Map<Id, SQX_Approval_Util.ResultWrapper>();

        //categorize the approvals based on type
        for(SObject obj : objects){
            SQX_Approval_Util.ResultWrapper result = new SQX_Approval_Util.ResultWrapper();
            result.i_entityID = (Id)obj.get('Id');
            if(objectsInApproval.contains(result.i_entityID)){
                SObjectType entityType = result.i_entityID.getSObjectType();
                if(entityType == SQX_Finding_Response__c.getSObjectType()){
                    responseApprovals.put(result.i_entityID, result);
                }
                else if(entityType == SQX_Audit_Response__c.getSObjectType()){
                    auditResponseApprovals.put(result.i_entityID, result);
                }
                else if(entityType == SQX_Investigation__c.getSObjectType()){
                    investigationApprovals.put(result.i_entityID, result);
                }
                else if(entityType == SQX_Containment__c.getSObjectType()){
                    containmentApprovals.put(result.i_entityID, result);
                }
                else if(entityType == SQX_Audit_Report__c.getSObjectType()){
                    auditReportApprovals.put(result.i_entityID, result);
                }
                else if(entityType == SQX_Complaint__c.getSObjectType()){
                    complaintApprovals.put(result.i_entityID, result);
                }
                else{
                    result.i_isSuccess = true;
                }

                objectsInApproval.remove(result.i_entityID); //remove from this 
            }
            else{
                result.i_isSuccess = false;
                result.i_errors = new List<Database.Error>();
            }

            allMap.put(result.i_entityID, result);
            
            results.add(result);
        }

        List<SObject> objectsToUpdate = new List<SObject>();

        //perform field updates on various objects
        if(responseApprovals.size() > 0){
            for(Id responseID : responseApprovals.keySet()){
                SQX_Finding_Response__c response = new SQX_Finding_Response__c(Id = responseID);

                if(action == SQX_Approval_Util.APPROVE_KEY){
                    response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
                    response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_APPROVED;
                }
                else if(action == SQX_Approval_Util.RECALL_KEY){
                    response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
                    response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_RECALLED;
                }
                else if(action == SQX_Approval_Util.REJECT_KEY){
                    response.Status__c = SQX_Finding_Response.STATUS_PUBLISHED;
                    response.Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_REJECTED;
                }

                responseApprovals.get(responseID).i_isSuccess = true;

                objectsToUpdate.add(response);
            }
        }

        if(auditResponseApprovals.size() > 0){
            for(Id auditResponseID : auditResponseApprovals.keySet()){
                SQX_Audit_Response__c auditResponse = new SQX_Audit_Response__c(Id = auditResponseID);

                if(action == SQX_Approval_Util.APPROVE_KEY){
                    auditResponse.Status__c = SQX_Audit_Response.STATUS_COMPLETED;
                    auditResponse.Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_APPROVED;
                }
                else if(action == SQX_Approval_Util.RECALL_KEY){
                    auditResponse.Status__c = SQX_Audit_Response.STATUS_COMPLETED;
                    auditResponse.Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_RECALLED;
                }
                else if(action == SQX_Approval_Util.REJECT_KEY){
                    auditResponse.Status__c = SQX_Audit_Response.STATUS_COMPLETED;
                    auditResponse.Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_REJECTED;
                }

                auditResponseApprovals.get(auditResponseID).i_isSuccess = true;

                objectsToUpdate.add(auditResponse);
            }
        }

        if(containmentApprovals.size() > 0){
            for(Id containmentID : containmentApprovals.keySet()){
                SQX_Containment__c containment = new SQX_Containment__c(Id = containmentID);

                if(action == SQX_Approval_Util.RECALL_KEY){
                    containment.Status__c = SQX_Investigation.STATUS_DRAFT;
                }

                containmentApprovals.get(containmentID).i_isSuccess = true;
                objectsToUpdate.add(containment);
            }
        }

        if(investigationApprovals.size() > 0){
            for(Id investigationID : investigationApprovals.keySet()){
                SQX_Investigation__c investigation = new SQX_Investigation__c(Id = investigationID);
                if(action == SQX_Approval_Util.RECALL_KEY){
                    investigation.Status__c = SQX_Investigation.STATUS_DRAFT;
                    investigation.Approval_Status__c = SQX_Investigation.APPROVAL_STATUS_NONE;
                }

                investigationApprovals.get(investigationID).i_isSuccess = true;
                objectsToUpdate.add(investigation);
            }
        }

        if(auditReportApprovals.size() > 0){
            for(Id reportId : auditReportApprovals.keySet()){
                SQX_Audit_Report__c report = new SQX_Audit_Report__c(Id = reportId);

                if(action == SQX_Approval_Util.APPROVE_KEY){
                    report.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
                    report.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_APPROVED;
                }
                else if(action == SQX_Approval_Util.RECALL_KEY){
                    report.Status__c = SQX_Audit_Report.STATUS_DRAFT;
                    report.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_NONE;
                }
                else if(action == SQX_Approval_Util.REJECT_KEY){
                    report.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
                    report.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_REJECTED;
                }

                auditReportApprovals.get(reportId).i_isSuccess = true;

                objectsToUpdate.add(report);
            }
        }
        if(complaintApprovals.size() > 0){
            for(Id complaintID : complaintApprovals.keySet()){
                SQX_Complaint__c complaint = new SQX_Complaint__c(Id = complaintID);

                if(action == SQX_Approval_Util.APPROVE_KEY){
                    complaint.Status__c = SQX_Complaint.STATUS_CLOSED;
                }
                else if(action == SQX_Approval_Util.RECALL_KEY){
                    complaint.Is_Recalled__c = true;
                }
                else if(action == SQX_Approval_Util.REJECT_KEY){
                    complaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSURE_REJECTED;
                }

                complaintApprovals.get(complaintID).i_isSuccess = true;

                objectsToUpdate.add(complaint);
            }
        }


        if(objectsToUpdate.size() > 0){
            List<Database.SaveResult> updResults = Database.update(objectsToUpdate, false);
            integer index = 0;
            for(Database.SaveResult updResult : updResults){
                if(updResult.isSuccess() == false){
                    Id erroneousObjID = (Id)objectsToUpdate.get(index).get('Id');
                    SQX_Approval_Util.ResultWrapper result = allMap.get(erroneousObjID);
                    System.assert(result != null, updResult + ' ' + objectsToUpdate); 
                    result.i_isSuccess = false;
                    result.i_errors = new List<Database.Error>();
                }
                
                index++;
            }
        }
        
        return results;
    }
    

}