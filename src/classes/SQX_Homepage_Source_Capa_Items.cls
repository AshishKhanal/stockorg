/**
* This class acts as an item source for CAPA related items
* @author  Anuj Bhandari
* @date    05-04-2017
*/
public with sharing class SQX_Homepage_Source_Capa_Items extends SQX_Homepage_ItemSource {


    /*
        CAPA Items : 
            1. Open CAPAs
            2. Draft CAPAs
            3. Open Actions
    */
    
    private final String module = SQX_Homepage_Constants.MODULE_TYPE_CAPA;
    String capaResponsePage;  //getPageName of capa response
    String capaPage;         // getPageName of capa main page

    /*
        This is the boundary value that separates urgency for an action on this item from high to critical
        e.x. if the item's due date is less than or equal to LIMIT_FOR_CRITICALITY away from today's date, the urgency is set to High
    */
    public static final Integer LIMIT_FOR_CRITICALITY_OPENACTION = 3;

    private User loggedInUser;

    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_Capa_Items(){
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
        capaResponsePage=SQX_Utilities.getPageName('', 'SQX_CAPA_Response');
        capaPage=SQX_Utilities.getPageName('', 'SQX_CAPA');
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_CAPA__c.SObjectType => new List<SObjectField> 
            {
                SQX_CAPA__c.Name,
                SQX_CAPA__c.Title__c,
                SQX_CAPA__c.SQX_CAPA_Coordinator__c,
                SQX_CAPA__c.CreatedDate,
                SQX_CAPA__c.Status__c
            },
            SQX_Action__c.SObjectType => new List<SObjectField>
            {
                SQX_Action__c.Name,
                SQX_Action__c.CreatedDate,
                SQX_Action__c.Due_Date__c,
                SQX_Action__c.Description__c,
                SQX_Action__c.SQX_CAPA__c,
                SQX_Action__c.Status__c
            },
            SQX_Investigation__c.SObjectType => new List<SObjectField>    
            {
                SQX_Investigation__c.Name,
                SQX_Investigation__c.CreatedDate,
                SQX_Investigation__c.SQX_CAPA__c,
                SQX_Investigation__c.Status__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
    *   returns a list of homepage items for the current user related to CAPA module
    */
    protected override List<SObject> getUserRecords(){

        List<SObject> allItems = new List<SObject>();

        List<SQX_CAPA__c> capaItems =   [SELECT Id,
                                                Name,
                                                SQX_CAPA_Coordinator__c,
                                                CAPA_Title__c,
                                                CreatedDate,
                                                Status__c,
                                                OwnerId
                                            FROM SQX_CAPA__c
                                            WHERE
                                            (
                                                (
                                                    Status__c =: SQX_CAPA.STATUS_OPEN
                                                            AND
                                                    SQX_CAPA_Coordinator__c =: loggedInUser.Id
                                                )
                                                OR
                                                (
                                                    Status__c =: SQX_CAPA.STATUS_DRAFT
                                                            AND
                                                    OwnerId =: loggedInUser.Id
                                                )
                                            )
                                            AND
                                            Partner_enabled__c = false
                                        ];

        List<SQX_Action__c> capaActions = [SELECT Id,
                                                Description__c,
                                                SQX_CAPA__c,
                                                SQX_CAPA__r.Name,
                                                SQX_CAPA__r.CAPA_Title__c,
                                                Due_Date__c,
                                                CreatedDate
                                            FROM SQX_Action__c
                                            WHERE
                                            SQX_CAPA__c != null
                                            AND
                                            SQX_User__c =: loggedInUser.Id
                                            AND
                                            Status__c =: SQX_Implementation_Action.STATUS_OPEN];

        List<SQX_Investigation__c> investigationAction=    [SELECT  Id,
                                                                    Name,
                                                                    SQX_CAPA__c,
                                                                    SQX_CAPA__r.Name,
                                                                    SQX_CAPA__r.Title__c,
                                                                    CreatedDate , 
                                                                    Approval_Status__c ,
                                                                    Status__c 
                                                                FROM SQX_Investigation__c
                                                                WHERE Approval_Status__c=:SQX_Investigation.APPROVAL_STATUS_PENDING
                                                                AND Id NOT IN( SELECT SQX_Investigation__c 
                                                                               FROM SQX_Response_Inclusion__c 
                                                                               WHERE SQX_Investigation__c !=null
                                                                            )
                                                                AND SQX_CAPA__r.SQX_Investigation_Approver__c=: loggedInUser.Id 
                                                            ];
        allItems.addAll((List<SObject>) capaItems);
        allItems.addAll((List<SObject>) capaActions);
        allItems.addAll((List<SObject>) investigationAction);
        return allItems;
    }




    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item){

        SQX_Homepage_Item capaItem;

        if(item instanceOf SQX_CAPA__c){
        
            capaItem = getCapaItem((SQX_CAPA__c) item);

        }else if(item instanceOf SQX_Action__c){

            capaItem = getOpenActionItem((SQX_Action__c) item);

        }else if(item instanceOf SQX_Investigation__c ){

            capaItem = getInvestigationItem((SQX_Investigation__c) item);

        }

        return capaItem;
    }

    /**
    *   Method returns a corresponding homepage item for the given CAPA record/item
    *   @param item - CAPA record
    */
    private SQX_Homepage_Item getCapaItem(SQX_CAPA__c item){

        // flag to check if item is Open CAPA item or Draft CAPA item
        Boolean isOpenCapa = item.Status__c == SQX_CAPA.STATUS_OPEN;

        SQX_Homepage_Item capaItem = new SQX_Homepage_Item();

        capaItem.itemId = item.Id;
        
        // set action type
        capaItem.actionType = isOpenCapa ? SQX_Homepage_Constants.ACTION_TYPE_NEEDING_RESPONSE : SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;

        capaItem.createdDate = Date.valueOf(item.CreatedDate);

        // set module type
        capaItem.moduleType = this.module;

        // set actions for the current item
        capaItem.actions = isOpenCapa ? getOpenItemActions(capaItem.itemId) : getDraftCAPAActions(capaItem.itemId);

        // set feed text for the current item
        capaItem.feedText = getCapaItemFeedText(item, isOpenCapa);

        // set user details of the user related to this item
        capaItem.creator = loggedInUser;

        return capaItem;

    }

    /**
    *   Method returns a corresponding homepage item for the given (open)Action record/item for CAPAs
    *   @param item - CAPA record
    */
    private SQX_Homepage_Item getOpenActionItem(SQX_Action__c item){

        SQX_Homepage_Item openActionItem = new SQX_Homepage_Item();

        openActionItem.itemId = item.Id;
        
        // set action badge
        openActionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_COMPLETE;

        openActionItem.createdDate = Date.valueOf(item.CreatedDate);

        // set due date
        openActionItem.dueDate = item.Due_Date__c;

        // set urgency        
        openActionItem.urgency = this.getItemUrgency(openActionItem.dueDate, LIMIT_FOR_CRITICALITY_OPENACTION);

        // set module type
        openActionItem.moduleType = this.module;
       
        // set actions for the current item
        String recordId = item.SQX_CAPA__c;
        openActionItem.actions = getOpenItemActions(recordId);

        // set feed text for the current item
        openActionItem.feedText = getOpenActionFeedText(item);

        // set user details of the user related to this item
        openActionItem.creator = loggedInUser;

        return openActionItem;
    }


    /**
    *   Returns a list of actions for open item type record (open CAPA or open Actions)
    *   @param id - id of the record for which action is to be set
    *   @param capaResponsePage -capaResponsePage is the capa response page to redirect when click for action
    */
    private List<SQX_Homepage_Item_Action> getOpenItemActions(String id){

        // adding action - Respond
        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_RESPOND;
        
        PageReference pr = new PageReference(capaResponsePage);
        pr.getParameters().put('Id',id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_REPLY;

        action.supportsBulk = false;

        return new List<SQX_Homepage_Item_Action> { action };
    }


    /**
    *   Returns a list of actions for draft item type record
    *   @param id - id of the record for which action is to be set
    */
    private List<SQX_Homepage_Item_Action> getDraftCAPAActions(String id){

        // adding action - View
        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + id);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List<SQX_Homepage_Item_Action> { action };
    }

    /**
    *   Method returns the feed text for CAPA items(Open CAPA / Draft CAPA)
    *   @param item - CAPA record
    *   @param isOpenCAPA - flag to determine if the record is of type Open CAPA or Draft CAPA
    */
    private String getCapaItemFeedText(SQX_CAPA__c item, Boolean isOpenCapa){
        String template = isOpenCapa ? SQX_Homepage_Constants.FEED_TEMPLATE_RESPOND :
                                       SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS;
        return String.format(template, new String[] { item.Name, getCAPATitle(item) } );
    }

    /**
    *   Method returns the open action type feed text for the given capa item
    *   @param item - CAPA record
    */
    private String getOpenActionFeedText(SQX_Action__c item){

        String feedText;

        String shortDesc = this.getShortenedDescription(item.Description__c);

        if(String.isBlank(shortDesc)){

            feedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITHOUT_SUBJECT,
                                 new String[] {
                                     item.SQX_CAPA__r.Name,
                                     getCAPATitle(item.SQX_CAPA__r)
                                 }
                                );

        }else{

            feedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITH_SUBJECT,
                                 new String[] {
                                     shortDesc,
                                     item.SQX_CAPA__r.Name,
                                     getCAPATitle(item.SQX_CAPA__r)
                                 }
                                );
        }
        return feedText;
    }

    /*
        Method returns capa title
        returns blank(instead of null) if capa title is empty
    */
    private String getCAPATitle(SQX_CAPA__c capa)
    {
        return String.isBlank(capa.CAPA_Title__c) ? '' : capa.CAPA_Title__c; 
    }

    /**
    *   Method returns a corresponding homepage item for the given record item for  Investigation
    *   @param item - SQX_Investigation__c record
    */
    private  SQX_Homepage_Item getInvestigationItem(SQX_Investigation__c item){

        // flag to check if item is draft of CAPA investigation
        Boolean isApprovalInvestigation = item.Status__c == SQX_Investigation.STATUS_IN_APPROVAL;
        SQX_Homepage_Item InvestigationItem = new SQX_Homepage_Item();
        InvestigationItem.itemId = item.Id;
        // set action type
        InvestigationItem.actionType =SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE;
        InvestigationItem.createdDate = Date.valueOf(item.CreatedDate);
        // set module type
        InvestigationItem.moduleType =this.module;
        // set actions for the current item
        String recordId = item.SQX_CAPA__c;
        InvestigationItem.actions = getInvestigationItemActions(recordId,item);
        // set feed text for the current item
        InvestigationItem.feedText = getInvestigationFeedText(item);
        // set user details of the user related to this item
        InvestigationItem.creator = loggedInUser;
        return InvestigationItem;
    }

    /**
    *   Returns a list of actions for in Approval of capa's investigation
    *   @param id - id of the record for which action is to be set
    *   @param item - SQX_Investigation__c record
    */
    private List<SQX_Homepage_Item_Action> getInvestigationItemActions(String id,SQX_Investigation__c item){
        SQX_Homepage_Item_Action investigationAction = new SQX_Homepage_Item_Action();
        investigationAction.name = SQX_Homepage_Constants.ACTION_APPROVE_REJECT;
        PageReference pr = new PageReference(capaPage);
        pr.getParameters().put('Id',id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        investigationAction.actionUrl = getHomePageItemActionUrl(pr);

        investigationAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT;
        investigationAction.supportsBulk = false;
        return new List<SQX_Homepage_Item_Action> { investigationAction };
     }

    /**
    *   Method returns the feed text for  Investigation items
    *   @param item -  Investigation record
    */
    private String getInvestigationFeedText(SQX_Investigation__c item){
        String template =SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE;
        return String.format(template, new String[] { 
                                item.Name, (String.isBlank(item.SQX_CAPA__r.Title__c) ? '' : item.SQX_CAPA__r.Title__c)
                            });
    }
}