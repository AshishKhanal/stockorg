/**
 * This object contains the strategy that is used to check if a finding or nc or capa is complete
 */
public with sharing class SQX_CompletionStrategy {

    /**
     * This flag is used to hold off completion of records until transaction is completed
     */
    public static Boolean preventCompletion = false;

    private List<SObject> objects;

    /**
     * This list will contain the result of records that match
     */
    public List<SObject> matchingRecords {get; private set;}

    /**
     * Default constructor accepting the list of items to check
     */
    public SQX_CompletionStrategy(List<SObject> objectToCheckCompletion) {
        objects = objectToCheckCompletion;
    }

    /**
     * Classifies records that are ready for completion and returns the matching records.
     * @return returns the list of record that are ready to be completed.
     */
    public List<SObject> matchingStrategy() {
        matchingRecords = new List<SObject>();

        if(preventCompletion) {
            System.debug( LoggingLevel.WARN, 'SQX_Completion_Strategy: Completion has been disabled.');
            return matchingRecords;
        }

        final String OpenStatus = SQX_Finding.STATUS_OPEN;
        System.assertEquals(SQX_Finding.STATUS_OPEN, SQX_NC.STATUS_OPEN, 'Developer exception open value mismatched');
        System.assertEquals(SQX_Finding.STATUS_OPEN, SQX_CAPA.STATUS_OPEN, 'Developer exception open value mismatched');

        final String statusField = SQX.getNSNameFor('Status__c'),
                     containmentRequiredField = SQX.getNSNameFor('Containment_Required__c'),
                     responseRequiredField = SQX.getNSNameFor('Response_Required__c'),
                     invesitigationRequiredField = SQX.getNSNameFor('Investigation_Required__c'),
                     approvedInvRequiredField = SQX.getNSNameFor('Investigation_Approval__c'),
                     caRequiredField = SQX.getNSNameFor('Corrective_Action_Required__c'),
                     paRequiredField = SQX.getNSNameFor('Preventive_Action_Required__c'),
                     hasContainmentField = SQX.getNSNameFor('Has_Containment__c'),
                     hasResponseField = SQX.getNSNameFor('Has_Response__c'),
                     hasCAField = SQX.getNSNameFor('Has_Corrective_Action__c'),
                     hasPAField = SQX.getNSNameFor('Has_Preventive_Action__c'),
                     hasInvestigationField = SQX.getNSNameFor('Has_Investigation__c'),
                     hasApprovedInvestigationField = SQX.getNSNameFor('Has_Approved_Investigation__c'),
                     hasDoneRespondingField = SQX.getNSNameFor('Done_Responding__c');

            
        for(SObject obj: objects){
            
            Boolean containmentRequired = (Boolean) obj.get(containmentRequiredField),
                    responseRequired = (Boolean) obj.get(responseRequiredField),
                    investigationRequired = (Boolean) obj.get(invesitigationRequiredField),
                    approvedInvRequired = (Boolean) obj.get(approvedInvRequiredField),
                    caRequired = (Boolean) obj.get(caRequiredField),
                    paRequired = (Boolean) obj.get(paRequiredField),
                    hasContainment = (Boolean) obj.get(hasContainmentField),
                    hasResponseVal = (Boolean) obj.get(hasResponseField),
                    hasCA = (Boolean) obj.get(hasCAField),
                    hasPA = (Boolean) obj.get(hasPAField),
                    hasInvestigation = (Boolean) obj.get(hasInvestigationField),
                    hasApprovedInvestigation = (Boolean) obj.get(hasApprovedInvestigationField),
                    hasDoneResponding = (Boolean) obj.get(hasDoneRespondingField),
                    isntOpen = (String)obj.get(statusField) != OpenStatus,
                    capaRequired = false;

            if (obj.getSObjectType() == SQX_Finding__c.getSObjectType()) {
                capaRequired = (Boolean) obj.get(SQX.getNSNameFor('CAPA_Required__c'));
            }

            if(isntOpen){
                continue;
            }

            boolean meetsRequirements = true;

            /*
                the following boolean logic is used to check if requirements are met or not
                Example: containment to check if requirement is met

                Containment Reqd.  |    Has containment  |  Result
                T               |         T           |    T
                T               |         F           |    F
                F               |         T           |    T
                F               |         F           |    T

                Simplified logical expression: OR(NOT(Containment Reqd) , AND(Containment Reqd, Has Containment))
            */


            //containment requirement
            meetsRequirements = meetsRequirements && ( (!containmentRequired) || (hasContainment && containmentRequired));


            //response requirement
            boolean hasResponse = (hasResponseVal || hasContainment || hasCA || hasPA || hasInvestigation );
            
            meetsRequirements = meetsRequirements && ( (!responseRequired) || (hasResponse && responseRequired));

            // Let CAPA drive Finding Status if CAPA is required
            meetsRequirements = meetsRequirements && (!capaRequired);


            //corrective action requirement
            meetsRequirements = meetsRequirements && ( (!caRequired) || (hasCA && caRequired));

            //preventive action requirement
            meetsRequirements = meetsRequirements && ( (!paRequired) || (hasPA && paRequired));


            //investigation requirement
            meetsRequirements = meetsRequirements && ( (!investigationRequired) || (hasInvestigation && investigationRequired));
            

            //check if approved investigation exist
            boolean requiresApprovedInvestigation = approvedInvRequired && investigationRequired;
            meetsRequirements = meetsRequirements && ( (! requiresApprovedInvestigation) || (hasApprovedInvestigation && requiresApprovedInvestigation));

            //check if additional response exists or not
            //if meetsRequirements is true till here but additional response still exists then 
            //meetsRequirements will be false and hence finding , nc , capa will still be open 
            //else finding, nc , capa will be complete
            meetsRequirements = meetsRequirements && hasDoneResponding;

            //if they meet requirement then send the for effectiveness review or closure accordingly
            if(meetsRequirements){

                matchingRecords.add(obj);
            }
        }
        
        return matchingRecords;
    }
}