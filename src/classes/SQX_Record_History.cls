/*
 * @author: Anish Shrestha
 * @date: 2014/12/04
 * @description: To record the record activity of different components
 */

public with sharing class SQX_Record_History{

    public static final String PS_OWNER_CHANGED = Label.SQX_PS_Changing_Owner;
    public static final String RECORDHISTORY_COMMENT_OWNER_CHANGED = Label.SQX_RECORDHISTORY_COMMENT_OWNER_CHANGED;
    public static final String TYPE_EDIT = 'Edit';
    public static final String TYPE_CREATE = 'Create';
    public final static Map<SObjectType, SObjectType> RECORD_ACTIVITY_OBJECTTYPE = new Map<SObjectType, SObjectType>{
            SQX_NonConformance__c.SObjectType => Schema.SQX_NC_Record_Activity__c.SObjectType,
            SQX_CAPA__c.SObjectType => Schema.SQX_CAPA_Record_Activity__c.SObjectType,
            SQX_Audit__c.SObjectType => Schema.SQX_Audit_Record_Activity__c.SObjectType,
            SQX_Audit_Program__c.SObjectType => Schema.SQX_Audit_Program_Record_Activity__c.SObjectType,
            SQX_Inspection__c.SObjectType => Schema.SQX_Inspection_Record_Activity__c.SObjectType,
            SQX_Complaint__c.SObjectType => Schema.SQX_Complaint_Record_Activity__c.SObjectType,
            SQX_CQ_Electronic_Signature__c.SObjectType => Schema.SQX_Electronic_Signature_Record_Activity__c.SObjectType,
            SQX_Equipment__c.SObjectType => Schema.SQX_Equipment_Record_Activity__c.SObjectType,
            SQX_Change_Order__c.SObjectType => Schema.SQX_Change_Order_Record_Activity__c.SObjectType,
            SQX_Controlled_Document__c.SObjectType => Schema.SQX_Controlled_Doc_Record_Activity__c.SObjectType,
            SQX_Training_Session__c.SObjectType => Schema.SQX_Training_Session_Record_Activity__c.SObjectType,
            SQX_Finding__c.SObjectType => Schema.SQX_Finding_Record_Activity__c.SObjectType,
            SQX_Supplier_Deviation__c.SObjectType => Schema.SQX_Supplier_Deviation_Record_Activity__c.SObjectType,
            SQX_New_Supplier_Introduction__c.SObjectType => Schema.SQX_NSI_Record_Activity__c.SObjectType,
            SQX_Supplier_Interaction__c.SObjectType => Schema.SQX_Supplier_Interaction_Record_Activity__c.SObjectType,
            SQX_Supplier_Escalation__c.SObjectType => Schema.SQX_Supplier_Escalation_Record_Activity__c.SObjectType
        };

    /**
    * Inserts the record history for a given record id.
    * @param sObjectType The type of record for which the record is to be created.
    * @param recordId The id of the record whose record history is to be inserted
    * @param objectId The virtual id of the object which is being inserted or edited. This determines if the record is being edited or created.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param activityCode purpose of signature translation label code
    */
    public static void insertRecordHistory(String sObjectType, Id recordId, String objectId,  String comment,  String purposeOfSignaure, String activityCode){
        insertRecordHistory( sObjectType,  recordId,  objectId,   comment,   purposeOfSignaure, true, activityCode);
    }

    /**
    * Inserts the record history for a given record id.
    * @param sObjectType The type of record for which the record is to be created.
    * @param recordId The id of the record whose record history is to be inserted
    * @param objectId The virtual id of the object which is being inserted or edited. This determines if the record is being edited or created.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    */
    public static void insertRecordHistory(String sObjectType, Id recordId, String objectId,  String comment,  String purposeOfSignaure, Boolean setParentField, String activityCode){
        insertRecordHistory(sObjectType, recordId, objectId,  comment,  purposeOfSignaure, setParentField, activityCode, false); 
    }

    /**
    * Inserts the record history for a given record id.
    * @param sObjectType The type of record for which the record is to be created.
    * @param recordId The id of the record whose record history is to be inserted
    * @param objectId The virtual id of the object which is being inserted or edited. This determines if the record is being edited or created.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    * @param escalatePrivilege flag to check whether to escalate the permission of user or not
    */
    public static void insertRecordHistory(String sObjectType, Id recordId, String objectId,  String comment,  String purposeOfSignaure, Boolean setParentField, String activityCode, Boolean escalatePrivilege){
        String newOrEditRecord = SQX_Upserter.isValidID(objectId) ? TYPE_EDIT : TYPE_CREATE;  
        insertMassRecordHistory(new List<Id>{ recordId },newOrEditRecord,comment,purposeOfSignaure,setParentField,activityCode, escalatePrivilege, null, null); 
    }

    /**
    * Inserts the record history for a given record id.
    * @param sObjectType The type of record for which the record is to be created.
    * @param recordId The id of the record whose record history is to be inserted
    * @param objectId The virtual id of the object which is being inserted or edited. This determines if the record is being edited or created.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    * @param escalatePrivilege flag to check whether to escalate the permission of user or not
    * @param oldStage previous value of stage
    * @param newStage current value of stage
    */
    public static void insertRecordHistory(String sObjectType, Id recordId, String objectId,  String comment,  String purposeOfSignaure, Boolean setParentField, String activityCode, Boolean escalatePrivilege, String oldStage, String newStage){
        String newOrEditRecord = SQX_Upserter.isValidID(objectId) ? TYPE_EDIT : TYPE_CREATE;  
        insertMassRecordHistory(new List<Id>{ recordId },newOrEditRecord,comment,purposeOfSignaure,setParentField,activityCode, escalatePrivilege, oldStage, newStage); 
    }
     

    /**
    * This function inserts record history for a list of sobjects with a same comment and purpose of signature for all records.
    * Used in cases where all record is changed with the same purpose
    * @param record history is to be inserted.
    * @param newOrEditRecord type of activity is to be inserted.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    */
    public static void insertMassRecordHistory(List<Id> records, String newOrEditRecord,  String comment,  String purposeOfSignaure,Boolean setParentField, String activityCode){
        insertMassRecordHistory(records, newOrEditRecord,  comment,  purposeOfSignaure, setParentField, activityCode, false);
    }
     

    /**
    * This function inserts record history for a list of sobjects with a same comment and purpose of signature for all records.
    * Used in cases where all record is changed with the same purpose
    * @param record history is to be inserted.
    * @param newOrEditRecord type of activity is to be inserted.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    * @param escalatePrivilege flag to check whether to escalate the permission of user or not
    */
    public static void insertMassRecordHistory(List<Id> records, String newOrEditRecord,  String comment,  String purposeOfSignaure,Boolean setParentField, String activityCode, Boolean escalatePrivilege){
        insertMassRecordHistory(records, newOrEditRecord,  comment,  purposeOfSignaure, setParentField, activityCode, escalatePrivilege, null, null);
    }

    /**
    * This function inserts record history for a list of sobjects with a same comment and purpose of signature for all records.
    * Used in cases where all record is changed with the same purpose
    * @param record history is to be inserted.
    * @param newOrEditRecord type of activity is to be inserted.
    * @param comment The comment that is to be stored for the record activity
    * @param purposeOfSignature The reason esig was provided for the data.
    * @param setParentField set if the record history object has parent object
    * @param activityCode purpose of signature translation label code
    * @param escalatePrivilege flag to check whether to escalate the permission of user or not
    * @param oldStage previous value of stage
    * @param newStage current value of stage
    */
    public static void insertMassRecordHistory(List<Id> records, String newOrEditRecord,  String comment,  String purposeOfSignaure,Boolean setParentField, String activityCode, Boolean escalatePrivilege, String oldStage, String newStage){
        String parentFieldName;
        SObjectField NStypeOfActivity,
                NScomment,
                NSpurposeOfSignature,
                NSmodifiedBy,
                NSActivityCode,
                NSOldStage,
                NSNewStage;

        List<SObject> recordActivities= new List<SObject>();
        List<SObjectField> fieldSet= new List<SObjectField>();

        SObjectType mainRecordType = records[0].getSObjectType(),
                    recordActivityType;
        
        if(!RECORD_ACTIVITY_OBJECTTYPE.containsKey(mainRecordType)){
            throw new SQX_ApplicationGenericException('Unsupported record type activity for object of type ' + mainRecordType);
        }

        recordActivityType = RECORD_ACTIVITY_OBJECTTYPE.get(mainRecordType);
        
        Map<String, Schema.SObjectField> allFields = recordActivityType.getDescribe().fields.getMap();
        for(Id record : records){
            SObject recordActivity = recordActivityType.newSObject();

            NStypeOfActivity = allFields.get(SQX.getNSNameFor('Type_of_Activity__c'));
            NScomment = allFields.get(SQX.getNSNameFor('Comment_Long__c'));
            if(NSComment == null) {
                NSComment = allFields.get(SQX.getNSNameFor('Comment__c'));
            }
            NSpurposeOfSignature = allFields.get(SQX.getNSNameFor('Purpose_Of_Signature__c'));
            NSmodifiedBy = allFields.get(SQX.getNSNameFor('Modified_By__c'));
            NSActivityCode = allFields.get(SQX.getNSNameFor('Activity_Code__c'));
            NSOldStage = allFields.get(SQX.getNSNameFor('Old_Stage__c'));
            NSNewStage = allFields.get(SQX.getNSNameFor('New_Stage__c'));

            final Integer MAX_COMMENT_LENGTH = NSComment.getDescribe().getLength();

            recordActivity.put(NStypeOfActivity, newOrEditRecord);
            recordActivity.put(NScomment, SQX_Utilities.getTruncatedText(comment, MAX_COMMENT_LENGTH, true));
            recordActivity.put(NSpurposeOfSignature, purposeOfSignaure);
            recordActivity.put(NSmodifiedBy, SQX_Utilities.getUserDetailsForRecordActivity());
            recordActivity.put(NSActivityCode, activityCode);
            if( oldStage != null){
            recordActivity.put(NSOldStage, oldStage);
            }
            if( newStage != null){
            recordActivity.put(NSNewStage, newStage);
            }

            if(setParentField){
                parentFieldName = mainRecordType.getDescribe().getName(); //CONVENTION: Parent Field API name is same as parent record Name
                recordActivity.put(parentFieldName, record);
            }
            recordActivities.add(recordActivity);
        }
        if(setParentField){
            fieldSet.add(allFields.get(parentFieldName));
        }
        fieldSet.addAll(new SObjectField[] { NStypeOfActivity, NScomment, NSpurposeOfSignature, NSmodifiedBy, NSActivityCode, NSOldStage, NSNewStage});

        SQX_DB db = new SQX_DB();
        if(escalatePrivilege){
            db.withoutSharing();
        }
        
        db.op_insert(recordActivities, fieldSet);
        
    }
}