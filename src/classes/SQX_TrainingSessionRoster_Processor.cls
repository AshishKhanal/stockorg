/**
* batch job processor to process rosters of a completed and/closed training session
*/
global without sharing class SQX_TrainingSessionRoster_Processor implements Database.Batchable<SObject> {
    
    /**
    * instance specific variables for batch job
    */
    global String mode;
    global String jobName;
    global Integer batchSize = 100;
    global Integer scheduleAfterMin = 2;
    global Boolean reschedule = !Test.isRunningTest();

    /**
    * batch job modes
    */
    public static final String MODE_DOCUMENT_TRAINING_GENERATION = SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_GENERATION,
                               MODE_DOCUMENT_TRAINING_COMPLETION = SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_COMPLETION;
    
    /**
    * roster batch job status of respective batch job modes
    */
    private static Map<String, String> modeQueuedStatusMap = new Map<String, String>{
        MODE_DOCUMENT_TRAINING_GENERATION => SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_QUEUED,
        MODE_DOCUMENT_TRAINING_COMPLETION => SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED
    };
    
    /**
    * roster batch job error status of respective batch job modes
    */
    private static Map<String, String> modeErrorStatusMap = new Map<String, String>{
        MODE_DOCUMENT_TRAINING_GENERATION => SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_ERROR,
        MODE_DOCUMENT_TRAINING_COMPLETION => SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_ERROR
    };
    
    /**
    * batch start method to return rosters query
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if (!modeQueuedStatusMap.containsKey(mode))
            throw new SQX_ApplicationGenericException('Batch job mode "' + mode + '" is not supported.');
        
        // rosters of sessions queued for completed or closed session processing as per provided mode
        return Database.getQueryLocator([
            SELECT SQX_Training_Session__c,
                SQX_Training_Session__r.Status__c,
                SQX_Training_Session__r.Roster_Processing_Queued_On__c
            FROM SQX_Training_Session_Roster__c
            WHERE SQX_Training_Session__r.Roster_Batch_Job_Status__c = :modeQueuedStatusMap.get(mode)
                AND Batch_Processed__c = false
            ORDER BY SQX_Training_Session__r.LastModifiedDate, SQX_Training_Session__c, Result__c, Name
        ]);
    }
    
    /**
    * batch execute method to process rosters
    */
    global void execute(Database.BatchableContext bc, List<SQX_Training_Session_Roster__c> rosters) {
        Set<Id> sessionsWithProcessedJobStatus = new Set<Id>();
        Map<Id, SQX_Training_Session__c> sessionsToUpdate = new Map<Id, SQX_Training_Session__c>();
        List<SQX_Training_Session_Roster__c> rostersToUpdate = new List<SQX_Training_Session_Roster__c>();
        DateTime nextJobQueuedOn = system.now();
        
        // update flag field of rosters as per batch job mode
        for (SQX_Training_Session_Roster__c roster : rosters) {
            SQX_Training_Session_Roster__c r = roster.clone(true);
            Id tsId = r.SQX_Training_Session__c;
            String sessionRosterBatchStatus = ''; // empty string as completed batch job status
            
            r.Batch_Job_Error__c = '';
            if (mode == MODE_DOCUMENT_TRAINING_GENERATION) {
                r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_DOCUMENT_TRAINING_GENERATION, r.SQX_Training_Session__r.Roster_Processing_Queued_On__c);
                
                if (r.SQX_Training_Session__r.Status__c == SQX_Training_Session.STATUS_CLOSED) {
                    // set session roster batch job status to queue for closed session processing when session is already closed
                    sessionRosterBatchStatus = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED;
                }
            }
            else if (mode == MODE_DOCUMENT_TRAINING_COMPLETION) {
                r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_DOCUMENT_TRAINING_COMPLETION, r.SQX_Training_Session__r.Roster_Processing_Queued_On__c);
            }
            
            rostersToUpdate.add(r);
            
            if (!sessionsToUpdate.containsKey(tsId)) {
                // initially assume all rosters will be processed for training sessions
                sessionsWithProcessedJobStatus.add(tsId);
                sessionsToUpdate.put(tsId, new SQX_Training_Session__c(
                    Id = tsId,
                    Roster_Batch_Job_Status__c = sessionRosterBatchStatus,
                    Roster_Processing_Queued_On__c = (sessionRosterBatchStatus == '' ? null : nextJobQueuedOn)
                ));
            }
        }
        
        // CRUD is checked but FLS not checked for flag fields of roster obejct in batch job operation
        Database.SaveResult[] srs = new SQX_DB().continueOnError().op_update(rostersToUpdate, new List<SObjectField>());
        
        SObjectType sessionObjType = SQX_Training_Session__c.SObjectType,
            rosterObjType = SQX_Training_Session_Roster__c.SObjectType;
        Map<SObjectType, List<SObject>> objsToUpdate = new Map<SObjectType, List<SObject>>();
        List<SObject> errRosters = new List<SObject>();
        
        // set error message in roster object and error status in session object
        for (Integer i = 0; i < srs.size(); i++) {
            if (!srs[i].isSuccess()) {
                // set error messages in roster object
                errRosters.add(new SQX_Training_Session_Roster__c(
                    Id = rostersToUpdate[i].Id,
                    Batch_Job_Error__c = SQX_Utilities.getFormattedErrorMessage(srs[i].getErrors())
                ));
                
                // set error roster batch job status in respective training session
                Id tsId = rosters[i].SQX_Training_Session__c;
                sessionsToUpdate.put(tsId, new SQX_Training_Session__c( Id = tsId, Roster_Batch_Job_Status__c = modeErrorStatusMap.get(mode) ));
                sessionsWithProcessedJobStatus.remove(tsId);
            }
        }
        
        if (!errRosters.isEmpty()) {
            // add rosters with error messages to update list
            objsToUpdate.put(rosterObjType, errRosters);
        }
        
        if (!sessionsWithProcessedJobStatus.isEmpty()) {
            // check whether there are pending rosters to be processed for current mode
            for (SQX_Training_Session__c ts : [ SELECT Id, (SELECT Id FROM SQX_Training_Session_Rosters__r WHERE Batch_Processed__c = false LIMIT 1)
                                                FROM SQX_Training_Session__c WHERE Id IN :sessionsWithProcessedJobStatus ]) {
                if (ts.SQX_Training_Session_Rosters__r.size() > 0) {
                    // remove sessions that have pending rosters to be processed
                    sessionsToUpdate.remove(ts.Id);
                }
            }
        }
        
        // add modified sessions to update list
        if (!sessionsToUpdate.isEmpty()) {
            objsToUpdate.put(sessionObjType, sessionsToUpdate.values());
        }
        
        // CRUD is checked but FLS not checked for batch job fields in batch job operation
        new SQX_DB().op_update(objsToUpdate, new Map<SObjectType, List<SObjectField>>{
            sessionObjType => new List<SObjectField>(),
            rosterObjType => new List<SObjectField>()
        });
    }
    
    /**
    * schedules the job once the batch finishes.
    */
    global void finish(Database.BatchableContext bc){
        try{
            if (reschedule) {
                System.scheduleBatch(this, jobName, scheduleAfterMin, batchSize);
            }
        }
        catch(Exception ex){
            // ignore exception while trying to schedule because cron job can respawn in case of conflict
        }
    }
}