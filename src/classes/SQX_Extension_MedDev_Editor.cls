/**
 * Extension class for MedDev Report Editor page
*/


global with sharing class SQX_Extension_MedDev_Editor {

    private ApexPages.StandardController controller;

    /**
     *	Validation error message (holds list of error messages delimited by ';;;' )
    */
    global String message { get; set; }


    /**
     * Constructor takes in standard controller
    */
    global SQX_Extension_MedDev_Editor(ApexPages.StandardController controller) {
        this.controller = controller;
    }

    /**
     * Method saves the MedDev Report without having the page redirect (i.e redirects to null) such that validation can be performed after save
    */
    global PageReference saveAndReturn() {
        this.controller.save();
        return null;
    }

    /**
     * Method to add validation error messages to the page
    */
    global void addErrorMessages() {
        if(!String.isBlank(message)) {
            for(String message : message.split(';;;')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            }
        }
    }

}