/**
* Unit tests for creating new Personnel Job Function Object
*
* @author Shailesh Maharjan
* @date 2015/12/02
* 
*/
@IsTest
public class SQX_Test_1772_Personnel_Job_Function {
    static Boolean runAllTests = true,
                   run_givenAddPersonnelJobFunction = false,
                   run_givenDeletePersonnelJobFunction = false,
                   run_givenEditPersonnelJobFunction = false,
                   run_givenPreventModifyPersonnelJobFunctioninHierarchy = false;

    /**
    * This test ensures addition of new / duplicate Personnel job function to work as expected.
    * PJF is duplicate when another inactive and not deactivated PJF exists with same job function and Personnel,
    *   and can have many deactivated PJFs with same job function and Personnel.
    */
    public testmethod static void givenAddPersonnelJobFunction(){
        if (!runAllTests && !run_givenAddPersonnelJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for PJF test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();

            //Create required Personnel for PFJ
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();

            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf1
            Database.SaveResult result1 = Database.insert(pjf1, false);
            
            System.assert(result1.isSuccess() == true,
                'Expected Personnel job function to be saved.');
            
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = true
            );
            
            // insert pjf2
            Database.SaveResult result2 = Database.insert(pjf2, false);
            
            System.assert(result2.isSuccess() == false,
                'Expected duplicate Personnel job function not to be saved.');
            
            // activating pjf1
            pjf1.Active__c = true;
            Database.SaveResult result3 = Database.update(pjf1, false);
            
            // insert pjf2
            Database.SaveResult result4 = Database.insert(pjf2, false);
            
            System.assert(result4.isSuccess() == false,
                'Expected duplicate Personnel job function not to be saved.');
            
            // deactivating pjf1
            pjf1.Active__c = false;
            Database.SaveResult result5 = Database.update(pjf1, false);
            
            // insert pjf2
            Database.SaveResult result6 = Database.insert(pjf2, false);
            
            System.assert(result6.isSuccess() == true,
                'Expected Personnel job function with existing deactivated Personnel job functions to be saved.');
            
            // deactivating pjf2
            pjf2.Active__c = false;
            Database.SaveResult result7 = Database.update(pjf2, false);
            
            SQX_Personnel_Job_Function__c pjf3 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf3
            Database.SaveResult result8 = Database.insert(pjf3, false);
            
            System.assert(result8.isSuccess() == true,
                'Expected Personnel job function with existing deactivated Personnel job functions to be saved.');
        }
    }

    /**
    * This test ensures deletion of a Personnel job function to work as expected.
    * Once activated PJF cannot be deleted, otherwise it can be deleted.
    */
    public testmethod static void givenDeletePersonnelJobFunction(){
        if (!runAllTests && !run_givenDeletePersonnelJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for pjf test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();

            //Create required Personnel for PFJ
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();
            
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf1
            Database.SaveResult saveResult1 = Database.insert(pjf1, false);
            
            // delete pjf1
            Database.DeleteResult delResult1 = Database.delete(pjf1, false);
            
            System.assert(delResult1.isSuccess() == true,
                'Expected not activated Personnel job function to be deleted.');
            
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = true
            );
            
            // insert pjf2
            Database.SaveResult saveResult2 = Database.insert(pjf2, false);
            
            // delete pjf2
            Database.DeleteResult delResult2 = Database.delete(pjf1, false);
            
            System.assert(delResult2.isSuccess() == false,
                'Expected once activated Personnel job function not to be deleted.');
        }
    }

     /**
    * This test ensures modification of fields of a personnel job function to work as expected.
    * Cannot edit job function and/or user of a once activated PJF.
    * Cannot set active for a deactivated PJF.
    */
    public testmethod static void givenEditPersonnelJobFunction(){
        if (!runAllTests && !run_givenEditPersonnelJobFunction){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // creating required test job functions
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function 1 for pjf test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function 2 for pjf test' );
            jf = new SQX_Test_Job_Function(jf2);
            Database.SaveResult jf2Result = jf.save();
            
            //Create required Personnel for PFJ
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save(); 
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            p1.save();
            
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf2.Id,
                SQX_Personnel__c = p1.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf1
            Database.SaveResult r1 = Database.insert(pjf1, false);
            
            // change job function
            pjf1.SQX_Job_Function__c = jf1.Id;
            Database.SaveResult r2 = Database.update(pjf1, false);
            
            System.assert(r2.isSuccess() == true,
                'Expected modification job function of not activated Personnel job function to be saved.');
                
            // activate pjf1
            pjf1.Active__c = true;
            Database.SaveResult r3 = Database.update(pjf1, false);
            
            // fetch activation date
            pjf1 = [SELECT Id, Activation_Date__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf1.Id];
            
            System.assert(pjf1.Activation_Date__c != null,
                'Expected activation date to be set when activating a personnel job function.');
            
            // change job function
            pjf1.SQX_Job_Function__c = jf2.Id;
            Database.SaveResult r4 = Database.update(pjf1, false);
            
            System.assert(r4.isSuccess() == false,
                'Expected modification job function of once activated personnel job function not to be saved.');
                
            // deactivate pjf1
            pjf1 = new SQX_Personnel_Job_Function__c( Id = pjf1.Id, Active__c = false);
            Database.SaveResult r5 = Database.update(pjf1, false);
            
            // fetch deactivation date
            pjf1 = [SELECT Id, Deactivation_Date__c FROM SQX_Personnel_Job_Function__c WHERE Id = :pjf1.Id];
            
            System.assert(pjf1.Deactivation_Date__c != null,
                'Expected deactivation date to be set when deactivating a personnel job function.');
            
            // reactivate pjf1
            pjf1.Active__c = true;
            Database.SaveResult r6 = Database.update(pjf1, false);
            
            System.assert(r6.isSuccess() == false,
                'Expected reactivation of deactivated personnel job function not to be saved.');
        }
    }

    /**
    * This test ensures to Modify Personnel job function in hierarchy to work as expected.
    * 
    */
    public testmethod static void givenPreventModifyPersonnelJobFunctioninHierarchy(){
        if (!runAllTests && !run_givenPreventModifyPersonnelJobFunctioninHierarchy){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        
        // creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User testUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Test_Personnel p;
        SQX_Job_Function__c jf1;
        
        System.runas(testUser1) {
            //Create required Personnel for PFJ
            p = new SQX_Test_Personnel();
            p.save();
            
            // creating required job function
            jf1 = new SQX_Job_Function__c( Name = 'job function for pjf test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();
        }
        
        System.runas(testUser2) {
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p.mainRecord.Id,
                Active__c = true
            );
            
            // insert pjf1
            Database.SaveResult saveResult1 = Database.insert(pjf1, false);
            System.assert(saveResult1.isSuccess() == false,
                'Expected Adding Personnel job function of other user personnel job function not to be saved.');
        }
    }
}