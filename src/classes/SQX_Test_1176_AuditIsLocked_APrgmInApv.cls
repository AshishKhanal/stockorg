/**
*/
@IsTest
public class SQX_Test_1176_AuditIsLocked_APrgmInApv {
    
    private static boolean run_allTests = true,
    					   run_givenProgramIsInApproval_AuditCantBeModifiedAddedOrDeleted = false;
    /**
    * This test ensures that once an audit program is in approval
    * a) Audit in that program can't be added
    * b) Audit in that program can't be modified
    * c) Audit in that program can't be deleted
    */
    public testmethod static void givenProgramIsInApproval_AuditCantBeModifiedAddedOrDeleted() {

        if(!run_allTests && !run_givenProgramIsInApproval_AuditCantBeModifiedAddedOrDeleted)
            return;

        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        System.runas(user){
            //Arrange: Create audit program and include two audits in it. Then send the audit program in to approval.

            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser),
                           audit2 = new SQX_Test_Audit(adminUser);


            audit1.includeInProgram(true, null); //add audit in a program
            audit2.includeInProgram(false, audit1.program);

            audit1.save();
            audit2.save();

            audit1.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_PLAN_APPROVAL;

            new SQX_DB().op_update(new List<SQX_Audit_Program__c>{ audit1.program }, new List<Schema.SObjectField>{
                Schema.SQX_Audit_Program__c.Status__c
            });


            //Act 1: try deleting the audit1
            try{
                SQX_Audit__c audit = new SQX_Audit__c( Id = audit1.audit.Id);
                new SQX_DB().op_delete(new List<SQX_Audit__c> { audit } );
                System.assert(false, 'An exception should have been thrown preventing deletion of the object');
            }
            catch(DMLException ex){
                //Assert 1: an exception should be raised
                System.assert(ex.getMessage().contains(Label.SQX_ERR_MSG_AUDIT_IN_AUDIT_PRGM_CANT_BE_DELETED));
            }

            //Act 2: try updating the audit2
            try{
                audit2.audit = [SELECT Status__c, Stage__c, Audit_Program_Approval_Status__c, SQX_Audit_Program__c, SQX_Audit_Program__r.Approval_Status__c 
                                FROM  SQX_Audit__c WHERE id=: audit2.audit.Id];
                audit2.audit.Title__c = 'asd';
                audit2.Save();
                //Assert 2: An exception gets raised an save fails
                System.assert(false, 'Exception should have occurred but didn\'t');
            }
            catch(DmlException ex){
                
                System.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            }

            //Act 3: Adding a new update
            try{
                SQX_Test_Audit audit3  = new SQX_Test_Audit();

                audit3.includeInProgram(false, audit1.program);
                audit3.save();
                System.assert(false, 'An exception should have been thrown preventing insertion of new audit');

            }
            catch(DmlException ex){
                System.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            }


            //Act 4: Try changing audit program
            try{
                audit2.includeInProgram(true, null);
                audit2.Save();
                
                //Assert 4: An exception gets raised an save fails
                System.assert(false, 'Exception should have occurred but didn\'t');
            }
            catch(DmlException ex){

                System.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            }

        }
    }
}