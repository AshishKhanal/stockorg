/**
* Unit tests for:
* i.    settign of text value of step field in "step as text" field of change order approval [givenSetStepAsText]
* ii.   validation of step number not allowing negative or zero value [givenNegativeOrZeroStepNumberValidation]
*/
@IsTest
public class SQX_Test_1944_Change_Order_Approval {
    
    /*
    * ensures step number is stored as text for change order approval
    * related method: SQX_Change_Order_Approval.setStepAsTextValue()
    */
    public static testmethod void givenSetStepAsText() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required draft change order with last approval status
            SQX_Change_Order__c co = new SQX_Change_Order__c(
                Title__c = 'Change Order ' + System.now(),
                Description__c = 'Description',
                Justification__c  = 'Justification',
                Change_Category__c = 'Administrative',
                Submitted_Date__c = Date.today(),
                Target_Completion_Date__c = Date.today().addDays(20),
                Status__c = SQX_Change_Order.STATUS_DRAFT
            );
            insert co;
            
            // ACT: add change order approval
            SQX_Change_Order_Approval__c coa = new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co.Id, Step__c = 2 );
            insert coa;
            
            String expectedVal = String.valueOf(2);
            coa = [SELECT Id, SQX_Change_Order__c, Step__c, Step_As_Text__c FROM SQX_Change_Order_Approval__c WHERE Id = :coa.Id];
            System.assertEquals(expectedVal, coa.Step_As_Text__c, 'Step is expected to be stored in Step As Text field.');
            
            
            // ACT: update step number
            coa.Step__c = 5;
            update coa;
            
            expectedVal = String.valueOf(5);
            coa = [SELECT Id, SQX_Change_Order__c, Step__c, Step_As_Text__c FROM SQX_Change_Order_Approval__c WHERE Id = :coa.Id];
            System.assertEquals(expectedVal, coa.Step_As_Text__c, 'Step is expected to be stored in Step As Text field.');
        }
    }
    
    /*
    * ensures step number not to have zero or negative value
    * validation rule: Prevent_Negative_Or_Zero_Step_Number
    */
    public static testmethod void givenNegativeOrZeroStepNumberValidation() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required draft change order with last approval status
            SQX_Change_Order__c co = new SQX_Change_Order__c(
                Title__c = 'Change Order ' + System.now(),
                Description__c = 'Description',
                Justification__c  = 'Justification',
                Change_Category__c = 'Administrative',
                Submitted_Date__c = Date.today(),
                Target_Completion_Date__c = Date.today().addDays(20),
                Status__c = SQX_Change_Order.STATUS_DRAFT
            );
            insert co;
            
            // ACT: add change order approval with zero step number
            SQX_Change_Order_Approval__c coa = new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co.Id, Step__c = 0 );
            Database.SaveResult result1 = Database.insert(coa, false);
            
            final String expectedErrMsg = 'Step cannot be negative or zero number.';
            System.assert(result1.isSuccess() == false, 'Change order approval with zero step number is not expected to be saved.');
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg), 'Error message for negative or zero step number is expected.');
            
            
            // ACT: add change order approval with negative step number
            coa = new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co.Id, Step__c = -10 );
            result1 = Database.insert(coa, false);
            
            System.assert(result1.isSuccess() == false, 'Change order approval with negative step number is not expected to be saved.');
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg), 'Error message for negative or zero step number is expected.');
            
            
            // add required change order approval
            coa = new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co.Id, Step__c = 10 );
            insert coa;
            
            // ACT: update change order approval with zero step number
            coa.Step__c = 0;
            result1 = Database.update(coa, false);
            
            System.assert(result1.isSuccess() == false, 'Change order approval with zero step number is not expected to be saved.');
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg), 'Error message for negative or zero step number is expected.');
            
            
            // ACT: update change order approval with negative step number
            coa.Step__c = -1;
            result1 = Database.update(coa, false);
            
            System.assert(result1.isSuccess() == false, 'Change order approval with negative step number is not expected to be saved.');
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg), 'Error message for negative or zero step number is expected.');
        }
    }
}