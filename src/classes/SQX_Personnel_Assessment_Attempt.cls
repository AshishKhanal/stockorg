/**
 *  Class contains common methods for Personnel Assessment Attemp object
*/
public with sharing class SQX_Personnel_Assessment_Attempt {


    public static SQX_Personnel_Assessment_Attempt__c signOffPersonnelAssessmentAttempt(SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt, Boolean isSatisfactory, String signOffComment){
        return signOffPersonnelAssessmentAttempt(personnelAssessmentAttempt, isSatisfactory, signOffComment, UserInfo.getUserId());
    }

    public static SQX_Personnel_Assessment_Attempt__c signOffPersonnelAssessmentAttempt(SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt, Boolean isSatisfactory, String signOffComment, String signedOffById){
        return signOffPersonnelAssessmentAttempt(personnelAssessmentAttempt, isSatisfactory, signOffComment, signedOffById, true);
    }

    /**
    *   Method to complete personnel assessment attempt
    *   @param personnelAssessmentAttempt record to be signed off
    *   @param isSatisfactory flag to determine if the attempt has been completed satisfactorily or not
    *   @param signOffComment comment associated with the signoff
    *   @param signedOffById id of the user who is signing off the record
    *   @param escalatePrivilege flag to determine if sharing rules are to be bypassed or not, <code>true</code> if rules are to be bypassed
    *   @return signed off record
    */
    public static SQX_Personnel_Assessment_Attempt__c signOffPersonnelAssessmentAttempt(SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt, Boolean isSatisfactory, String signOffComment, String signedOffById, Boolean escalatePrivilege){

        SQX_Personnel_Assessment_Attempt__c attemptToSignOff = personnelAssessmentAttempt.clone(true);

        if(isSatisfactory){
            attemptToSignOff.Result__c = SQX_Assessment.ASSESSMENT_RESULT_SATISFACTORY;
        }
        else
        {
            attemptToSignOff.Result__c = SQX_Assessment.ASSESSMENT_RESULT_UNSATISFACTORY;
        }

        attemptToSignOff.User_Comment__c = signOffComment;
        attemptToSignOff.SQX_Submitted_By__c = signedOffById;
        attemptToSignOff.Submitted_Date__c = System.today();

        SQX_DB db = new SQX_DB();
        if(escalatePrivilege){
            db = db.withoutSharing();
        }

        db.op_update(new List<SQX_Personnel_Assessment_Attempt__c> { attemptToSignOff },
                     new List<Schema.SObjectField> { 
                                SQX_Personnel_Assessment_Attempt__c.Result__c,
                                SQX_Personnel_Assessment_Attempt__c.User_Comment__c,
                                SQX_Personnel_Assessment_Attempt__c.SQX_Submitted_By__c,
                                SQX_Personnel_Assessment_Attempt__c.Submitted_Date__c
                             });

        return attemptToSignOff;
    }

    /**
    * Bulkified class for personnel assessment attempt
    * without sharing is required to query/update personnel assesment and controlled documents
    */
    public without sharing class Bulkified extends SQX_BulkifiedBase {


        public Bulkified(List<SQX_Personnel_Assessment_Attempt__c> newList , Map<Id, SQX_Personnel_Assessment_Attempt__c> oldMap) {
            super(newList, oldMap);
        }

        /*
         * Method filters out SCORM type assessments which have been sucessfully completed
         * and associates the personnel assessment record with its training record
        */
        public Bulkified associatePersonnelAssessmentWithTrainingRecordOnSuccessfulCompletion(){

            final String ACTION_NAME = 'associatePersonnelAssessmentWithTrainingRecordOnSuccessfulCompletion';

            Rule successfulAttempts = new Rule();
            successfulAttempts.addRule(SQX_Personnel_Assessment_Attempt__c.Result__c, RuleOperator.Equals, SQX_Assessment.ASSESSMENT_RESULT_SATISFACTORY);
            successfulAttempts.addRule(SQX_Personnel_Assessment_Attempt__c.Submitted_Date__c, RuleOperator.NotEquals, null);


            this.resetView()
                .removeProcessedRecordsFor(SQX.PersonnelAssessmentAttempt, ACTION_NAME)
                .applyFilter(successfulAttempts, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Personnel_Assessment_Attempt__c> successfulPersonnelAssessmentAttempts = (List<SQX_Personnel_Assessment_Attempt__c>) successfulAttempts.evaluationResult;

            if(successfulPersonnelAssessmentAttempts.size() > 0){
                this.addToProcessedRecordsFor(SQX.PersonnelAssessmentAttempt, ACTION_NAME, successfulPersonnelAssessmentAttempts);

                Set<Id> personnelAssessmentIds = this.getIdsForField(SQX_Personnel_Assessment_Attempt__c.SQX_Personnel_Assessment__c);

                // this will hold a map of personnel and its associated scorm assessment records
                Map<Id, Set<Id>> personnelAndAssessmentsMap = new Map<Id, Set<Id>>();

                Map<String, SQX_Personnel_Assessment__c> personnelAssessmentMap = new Map<String, SQX_Personnel_Assessment__c>();

                for(SQX_Personnel_Assessment__c personnelAssessment : [SELECT Id, SQX_Personnel__c, SQX_Assessment__c FROM SQX_Personnel_Assessment__c WHERE 
                                                                       Id IN: personnelAssessmentIds])
                {
                    Set<Id> assessmentIds = new Set<Id>();
                    if(personnelAndAssessmentsMap.containsKey(personnelAssessment.SQX_Personnel__c)){
                        assessmentIds = personnelAndAssessmentsMap.get(personnelAssessment.SQX_Personnel__c);
                    }
                    assessmentIds.add(personnelAssessment.SQX_Assessment__c);

                    personnelAndAssessmentsMap.put(personnelAssessment.SQX_Personnel__c, assessmentIds);

                    // creating unique keys using personnel and assessment id to locate personnel assessment record
                    personnelAssessmentMap.put('' + personnelAssessment.SQX_Personnel__c + personnelAssessment.SQX_Assessment__c, personnelAssessment);
                }

                List<SQX_Personnel_Document_Training__c> pdtsToUpdate = new List<SQX_Personnel_Document_Training__c>();
                if(personnelAndAssessmentsMap.keySet().size() > 0){

                    // select pdts for the given set of personnels which have not yet been linked with any personnel assessment
                    for(SQX_Personnel_Document_Training__c pdt : [SELECT Id, SQX_Personnel__c, SQX_Assessment__c FROM SQX_Personnel_Document_Training__c
                                                                  WHERE SQX_Personnel__c IN: personnelAndAssessmentsMap.keySet() AND SQX_Personnel_Assessment__c = null])
                    {
                        if(personnelAndAssessmentsMap.get(pdt.SQX_Personnel__c).contains(pdt.SQX_Assessment__c)){
                            SQX_Personnel_Assessment__c personnelAssessment = personnelAssessmentMap.get('' + pdt.SQX_Personnel__c + pdt.SQX_Assessment__c);
                            
                            pdt.SQX_Personnel_Assessment__c = personnelAssessment.Id;
                            pdtsToUpdate.add(pdt);
                        }
                    }
                }

                if(pdtsToUpdate.size() > 0){
                    /*  WITHOUT SHARING USED 
                    *   ---------------------
                    *   Site user does not have required permissions to update PDTs. Hence, the escalation.
                    */
                    new SQX_DB().withoutSharing().op_update(pdtsToUpdate, new List<Schema.SObjectField> { SQX_Personnel_Document_Training__c.SQX_Personnel_Assessment__c });
                }
            }
            return this;
        }
    }
}