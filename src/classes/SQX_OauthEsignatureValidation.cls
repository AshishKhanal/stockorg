/**
* @author Sagar Shrestha
* @date 2014/11/18
* @description This class contains function to implement OAuth verification method to validate password of a user
*/
public with sharing class SQX_OauthEsignatureValidation{

    public String userName {get; set;}
    public String password {get; set;}

    public Boolean isProvidedCredentialsInvalid { 
      get ; private set;
    }

    public SQX_OauthEsignatureValidation(){
      
    }

    public SQX_OauthEsignatureValidation(String userName, String password){
      this.userName = userName;
      this.password = password;
    }

    public boolean validateCredentials(){
        return validateCredentials(this.username, this.password);
    }

    
    /**
    * @author Sagar Shrestha
    * @date 2014/11/18
    * @description validate password of a salesforce user
    * @param username username of salesforce user
    * @param password password of salesforce user
    */
    public boolean validateCredentials(String username, String password){
      SQX_CQ_Electronic_Signature__c esig= SQX_CQ_Electronic_Signature__c.getInstance();
      return validateCredentials(username, password, esig.Consumer_Key__c, esig.Consumer_Secret__c);
    }

    /**
    * @author Sagar Shrestha
    * @date 2014/11/18
    * @description validate password of a salesforce user
    * @param username username of salesforce user
    * @param password password of salesforce user
    * @param consumerKey Consumer key of connected app
    * @param consumerSecret Consumer Secret of connected app
    */
    public boolean validateCredentials(String username, String password, String consumerKey, String consumerSecret){
      this.isProvidedCredentialsInvalid = false;

      //validate username with current username
      if(username!=UserInfo.getUserName()){
        this.isProvidedCredentialsInvalid = true;
        return false;
      }
      //handle login validation for portal users
      if(SQX_Utilities.getCurrentUsersRole() == SQX_Utilities.SQX_UserRole.RoleSupplier)
      {
          PageReference ref = Site.Login(username, password, null);
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '' + ref));
            if(!ApexPages.hasMessages(ApexPages.Severity.ERROR)){
                return true;
            }
            else{
                return false;
            }            
      }

      Organization currentOrganization= [SELECT IsSandBox FROM Organization LIMIT 1 ];
      boolean isSandBoxEnvironment= currentOrganization.IsSandBox;
      boolean verificationResult = false; 
      String loginURL= 'https://login.salesforce.com';

      If(isSandBoxEnvironment)
      {
          loginURL= 'https://test.salesforce.com';
      }

      Http h = new Http();
      HttpRequest req = new HttpRequest();

      //build a request, contains consumer key and consumer secret of connected app SQX_CQ_Electronic_Signature, and contains username and password
      //SQX-2182 : Fix to escape special chars in endpoint url
      PageReference ref = new PageReference(loginURL + '/services/oauth2/token');
      ref.getParameters().put('grant_type', 'password');
      ref.getParameters().put('client_id', consumerKey);
      ref.getParameters().put('client_secret', consumerSecret);
      ref.getParameters().put('username', username);
      ref.getParameters().put('password', password);
      
      req.setEndpoint(ref.getUrl());
      req.setMethod('POST');
      HttpResponse res = h.send(req);
      String response = res.getBody();
      System.debug('response: '+response);

      //if the response doesnt contain error and statuscode is equals to 200 which means it is valid request, then it is a valid username and password
      if((res.getStatusCode() == 200) && !response.contains('error'))
        {
            verificationResult = true;
        }
        else{
            verificationResult = false;
            this.isProvidedCredentialsInvalid = true;
        }


      return verificationResult;
    }


    
}