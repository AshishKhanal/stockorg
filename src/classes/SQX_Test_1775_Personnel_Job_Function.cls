/**
* Unit tests for creating new Personnel Job Function Object using SQX_New_Personnel_Job_Function VF page and extension SQX_Extension_Personnel_Job_Function
*
* @author Shailesh Maharjan 
* @date 2015/12/007
* 
*/
@IsTest
public class SQX_Test_1775_Personnel_Job_Function {
    
    static final String USER_ADMIN1 = '1775_admin1',
                        USER_STD1 = '1775_user1';
    
    @testSetup
    public static void commonSetup() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, USER_ADMIN1);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, USER_STD1);
    }
    
    static Boolean runAllTests = true,
                   run_givenCallSaveAndNewWithPersonnelParam_NoDuplicate_SavedWithNewURLWithSamePersonnelParam = false,
                   run_givenCallSaveWithPersonnelParam_NoDuplicate_SavedWithViewURL = false,
                   run_givenDisplayListofPersonnelJobFunction = false;
    
    /**
    * tests saveAndNew method of SQX_Extension_Personnel_Job_Function extension
    */
    public testmethod static void givenCallSaveAndNewWithPersonnelParam_NoDuplicate_SavedWithNewURLWithSamePersonnelParam() {
        if (!runAllTests && !run_givenCallSaveAndNewWithPersonnelParam_NoDuplicate_SavedWithNewURLWithSamePersonnelParam) {
            return;
        }
        
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_ADMIN1)) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for ujf test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();

            //Create required Personnel for PFJ
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();
            
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            
            String testPersonnel1Id = p.mainRecord.Id;
            
            Test.setCurrentPage(Page.SQX_New_Personnel_Job_Function);
            ApexPages.currentPage().getParameters().put('personnel', testPersonnel1Id );
            ApexPages.StandardController pjfStandardController = new ApexPages.StandardController(pjf1);
            SQX_Extension_Personnel_Job_Function pjfExtension = new SQX_Extension_Personnel_Job_Function(pjfStandardController);
            
            // call saveAndNew method using extension
            PageReference pr = pjfExtension.saveAndNew();
            
            System.assert(pr != null,
                'Expected Personnel job function to be saved using passed Personnel value.');
            
            System.assert(pr.getURL().contains(Page.SQX_New_Personnel_Job_Function.getURL()),
                'Expected current page to be the custom VF page for creating new Personnel Job Function.');
            
            System.assertEquals(testPersonnel1Id , pr.getParameters().get('Personnel'),
                'Expected Personnel to be same as original link for save and new method.');
        }
    }

    /**
    * tests save method of SQX_Extension_Personnel_Job_Function extension with user value passed
    * ensures customized error message is displayed in VF page when user does not have access on the personnel
    */
    public testmethod static void givenCallSaveWithPersonnelParam_NoDuplicate_SavedWithViewURL() {
        if (!runAllTests && !run_givenCallSaveWithPersonnelParam_NoDuplicate_SavedWithViewURL) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get(USER_ADMIN1);
        User stdUser = users.get(USER_STD1);
        
        SQX_Personnel_Job_Function__c pjf1;
        SQX_Extension_Personnel_Job_Function pjfExtension;
        
        System.runas(adminUser) {
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for pjf test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();
            
            //Create required Personnel for PFJ
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();

            pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            
            String testPersonnel1Id = p.mainRecord.Id;
            
            Test.setCurrentPage(Page.SQX_New_Personnel_Job_Function);
            ApexPages.currentPage().getParameters().put('personnel', testPersonnel1Id);
            ApexPages.StandardController pjfStandardController = new ApexPages.StandardController(pjf1);
            pjfExtension = new SQX_Extension_Personnel_Job_Function(pjfStandardController);
        }
        
        System.runas(stdUser) {
            // ACT: call save method using extension with no access on personnel
            pjfExtension.save();
            
            System.assert(SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), 'You do not have sufficient access on the personnel'),
                          'Expected customized error on VF page when user has no access on personnel');
        }
        
        System.runas(adminUser) {
            // ACT: call save method using extension with access on personnel
            PageReference pr = pjfExtension.save();
            
            System.assert(pr != null,
                'Expected Personnel job function to be saved using passed Personnel value.');
            
            System.assertEquals(new ApexPages.StandardController(pjf1).view().getURL(), pr.getURL(),
                'Expected current page to be the standard view page of Personnel Job Function.');
        }
    }

    /**
    * tests to view list of Personnel_Job_Function
    */
    public testmethod static void givenDisplayListofPersonnelJobFunction() {
        if (!runAllTests && !run_givenDisplayListofPersonnelJobFunction) {
            return;
        }
        
        SQX_Test_Personnel p1;

        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_ADMIN1)) {
            // Creating New Personnel Job Function 
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for pjf1 test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jfResult = jf.save();

            //Creating required Personnel for PFJ
            p1 = new SQX_Test_Personnel();
            p1.save();
            
            //Creating Personnel Job Function 
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                SQX_Personnel__c = p1.mainRecord.Id,
                Active__c = false
            );
            
            // insert pjf1
            Database.SaveResult saveResult1 = Database.insert(pjf1, false);

            // Creating Personnel Job Function with Active status
            // creating required job function
            SQX_Job_Function__c jf2 = new SQX_Job_Function__c( Name = 'job function for pjf2 test' );
            jf = new SQX_Test_Job_Function(jf2);
            Database.SaveResult jfResult2 = jf.save();
            
            //Creating Personnel Job Function 
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf2.Id,
                SQX_Personnel__c = p1.mainRecord.Id,
                Active__c = true
            );
            
            // insert pjf2
            Database.SaveResult saveResult2 = Database.insert(pjf2, false);

            // Creating Personnel Job Function with De-Activated status
            // creating required job function
            SQX_Job_Function__c jf3 = new SQX_Job_Function__c( Name = 'job function for pjf3 test' );
            jf = new SQX_Test_Job_Function(jf3);
            Database.SaveResult jfResult3 = jf.save();
            
            //Creating Personnel Job Function 
            SQX_Personnel_Job_Function__c pjf3 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf3.Id,
                SQX_Personnel__c = p1.mainRecord.Id,
                Active__c = true
            );
            
            // insert pjf3
            Database.SaveResult saveResult3 = Database.insert(pjf3, false);

            // deactivate pjf3
            pjf3 = new SQX_Personnel_Job_Function__c( Id = pjf3.Id, Active__c = false);
            Database.SaveResult saveResult4 = Database.update(pjf3, false);

            // ACT: get default list
            ApexPages.StandardController pjfStandardController = new ApexPages.StandardController(p1.mainRecord);
            SQX_Extension_Personnel pjfExtension = new SQX_Extension_Personnel(pjfStandardController);
            pjfExtension.getPersonnelJobFunctionController().setPageSize(200);
            Map<Id, SQX_Personnel_Job_Function__c> pjfList = new Map<Id, SQX_Personnel_Job_Function__c>((List<SQX_Personnel_Job_Function__c>)pjfExtension.getPersonnelJobFunctionController().getRecords());

            System.assertEquals(2, pjfList.size(),
                'Expected 2 Personnel job function list to be displayed.');

            System.assert(pjfList.containsKey(pjf1.Id),
                'Expected new Personnel job function to be listed.');

            System.assert(pjfList.containsKey(pjf2.Id),
                'Expected active Personnel job function to be listed.');

            // ACT: get all personnel job function list including deactivated
            ApexPages.StandardController pjfStandardController1 = new ApexPages.StandardController(p1.mainRecord);
            SQX_Extension_Personnel pjfExtension1 = new SQX_Extension_Personnel(pjfStandardController1);
            pjfExtension1.includeDeactivatedJobFunctions = true;
            pjfExtension1.getPersonnelJobFunctionController().setPageSize(200);
            Map<Id, SQX_Personnel_Job_Function__c> pjfList1 = new Map<Id, SQX_Personnel_Job_Function__c>((List<SQX_Personnel_Job_Function__c>)pjfExtension1.getPersonnelJobFunctionController().getRecords());
            
            System.assertEquals(3, pjfList1.size(),
                'Expected 3 Personnel job function list to be displayed.');

            System.assert(pjfList1.containsKey(pjf3.Id),
                'Expected deactivated Personnel job function not to be listed.');
        }
    }
}