/**
* @author Pradhanta Bhandari
* @date 2014/3/23
* @issueNumber SQX-990 [CAPA | Effectiveness Review : Effectiveness Reviewer unable to edit and save Effectiveness Review]
* @description
* The issue is caused when an effectiveness reviewer doesn't have R/W access on the CAPA
* The sharing setting of Eff.Review object mandates that R/W access should be held on CAPA to make any modification.
* When eff.reviewer tries to edit the eff.review he/she doesn't have the R/W access by default.
*
* Fix: The fix for the solution is to provide the required R/W access to the CAPA when the Eff.Review is created.
*
* Note: An alternate of fixing with changing CAPA Owner was tried, but changing the CAPA Owner resets all existing 
* sharing rules. So, a supplier with read access now doesn't have the required privileges. Hence, this alternate was
* not used.
* Also, adding R/W access is more inline with the idea of sharing the record with the CAPA team, so that they can
* freely collaborate.
*/
@IsTest
private class SQX_Test_990_CAPAOwnershipWhenEffective {

	private static boolean 	runAllTests = true,
							run_givenCAPAGetsCompleted_OwnershipIsTransferredToEffReviewer = false,
                            run_givenCAPAGetsCompletedBySupplier_EffReviewerCanReview = false;
	
	/**
	* @description This test checks if the effectiveness review of the CAPA can be closed when a CAPA object is completed.
	* The CAPA's R/W access is given to the corresponding effectiveness reviewer.
	*/
	@IsTest
	public static void givenCAPAGetsCompleted_EffReviewerCanReview() {

		if(!runAllTests && !run_givenCAPAGetsCompleted_OwnershipIsTransferredToEffReviewer)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User effectivenessReviewer = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER , null);
        DateTime startTime = DateTime.now();

        SQX_CAPA__c capa = null;

        System.runas(capaOwner){
            SQX_Test_CAPA_Utility capaUtil = new SQX_Test_CAPA_Utility(true, capaOwner, capaOwner);
            capaUtil.CAPA.Needs_Effectiveness_Review__c = true;
            capaUtil.CAPA.SQX_Effectiveness_Reviewer__c = effectivenessReviewer.Id;
            capaUtil.CAPA.Status__c = SQX_Finding.STATUS_DRAFT;
            
            capaUtil.save();
            
            capaUtil.CAPA.Status__c = SQX_Finding.STATUS_COMPLETE;
            capaUtil.save();
            capa = capaUtil.CAPA;
        }


        System.runAs(effectivenessReviewer){
        	SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            
            insert review;


            capa = [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id = :capa.Id];

            System.assert(capa.Status__c == SQX_Finding.STATUS_CLOSED, 
            	'Expected the CAPA to be closed but found it ' + capa.Status__c);
        }


	}



    /**
    * @description This test checks if the effectiveness review of the CAPA is transferred when a CAPA object is created and 
    * and can be closed when the CAPA is completed by a supplier 
    */
    @IsTest
    public static void givenCAPAGetsCompletedBySupplier_EffReviewerCanReview() {

        if(!runAllTests && !run_givenCAPAGetsCompletedBySupplier_EffReviewerCanReview)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User effectivenessReviewer = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER , null);
        SQX_CAPA__c capa;
        
        System.runas(newUser){
            
            SQX_Test_CAPA_Utility capaUtil = new SQX_Test_CAPA_Utility(true, newUser, newUser);
            capaUtil.CAPA.Needs_Effectiveness_Review__c = true;
            capaUtil.CAPA.SQX_Effectiveness_Reviewer__c = effectivenessReviewer.Id;
            capaUtil.CAPA.Status__c = SQX_Finding.STATUS_DRAFT;
            
            capaUtil.save();
            
            capaUtil.CAPA.Status__c = SQX_Finding.STATUS_COMPLETE;
            capaUtil.save();
            capa = capaUtil.CAPA;
            
            Test.setCurrentPage(Page.SQX_CAPA_Response);
            ApexPages.StandardController sc = new ApexPages.StandardController(capa);
            SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
            controller.initializeTemporaryStorage();
            
            
            /* construct a response */
            SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.Id,
                                                                           Response_Summary__c = 'Response');
            
            Map<String,Object>  changeSet = new Map<String, Object>(),
                                responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response));
            
            //set virtual IDSs
            responseUT.put('Id', 'Response-1');
            
            List<Object> changes = new List<Object>();
            
            changes.add(responseUT);
            
            changeSet.put('changeSet', changes);

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'submitresponse');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            
            
            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
            
            System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.Id].size());
        }

        System.runAs(effectivenessReviewer){
            SQX_Effectiveness_Review__c review = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            
            insert review;


            capa = [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id = :capa.Id];

            System.assert(capa.Status__c == SQX_Finding.STATUS_CLOSED, 
                'Expected the CAPA to be closed but found it ' + capa.Status__c);
        }


    }
	
	
	
}