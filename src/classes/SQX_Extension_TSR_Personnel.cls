/**
* Extension to list and insert personnel to training session roster
* 
* @author Sajal Joshi
*/
public with sharing class SQX_Extension_TSR_Personnel {
    
    private final SQX_Training_Session__c session;
    Map<Id, List<SQX_Personnel_Document_Training__c>> uniquePersonnelMap;
    //list of personnel to be displayed
    public List<SQX_Training_Session_Roster_Wrapper> rosters {get; set;}
    
    /** 
    * Get list of personnel to be displayed in page
    */
    public SQX_Extension_TSR_Personnel(ApexPages.StandardController  controller) {
        session = (SQX_Training_Session__c)controller.getRecord();
        List<SQX_Training_Session_Roster__c> existingRosters = [SELECT SQX_Personnel__c FROM SQX_Training_Session_Roster__c WHERE SQX_Training_Session__c =: session.id];
        Set<Id> existingPersonnels = new SQX_BulkifiedBase().getIdsForField(existingRosters, SQX_Training_Session_Roster__c.SQX_Personnel__c);
        
        uniquePersonnelMap = new Map<Id, List<SQX_Personnel_Document_Training__c>>();
        if(session.SQX_Controlled_Document__c != null){
            for(SQX_Personnel_Document_Training__c pdt : [SELECT Id, SQX_Personnel__c, SQX_Personnel__r.Name, SQX_Personnel__r.Full_Name__c FROM SQX_Personnel_Document_Training__c 
                                                          WHERE SQX_Controlled_Document__c =: session.SQX_Controlled_Document__c 
                                                          AND Status__c =: SQX_Personnel_Document_Training.PENDING_STATUSES 
                                                          AND SQX_Training_Session__c =: null AND SQX_Personnel__c NOT IN: existingPersonnels]) {
                if(uniquePersonnelMap.containsKey(pdt.SQX_Personnel__c)) {
                    uniquePersonnelMap.get(pdt.SQX_Personnel__c).add(pdt);
                } else {
                    uniquePersonnelMap.put(pdt.SQX_Personnel__c, new List<SQX_Personnel_Document_Training__c> { pdt });
                }
            }
        }
        rosters = new List<SQX_Training_Session_Roster_Wrapper>();
        if(uniquePersonnelMap.size() > 0){
            for(Id id: uniquePersonnelMap.keySet()){
                SQX_Training_Session_Roster_Wrapper roster = new SQX_Training_Session_Roster_Wrapper(session, uniquePersonnelMap.get(id)[0].SQX_Personnel__r);
                rosters.add(roster);
            }
        }
    }
    
    /**
    * Added the selected personnel to training session roster
    * @return url to redirect to training session main page or throws error
    */
    public PageReference addToRoster(){
        PageReference ref = null;
        Database.SaveResult result = null;
        Savepoint sp = null;
        List<SQX_Training_Session_Roster__c> selectedRoster = new List<SQX_Training_Session_Roster__c>();
        List<SQX_Personnel_Document_Training__c> selectedTraining = new List<SQX_Personnel_Document_Training__c>();
        
        for(SQX_Training_Session_Roster_Wrapper rosterWrapper : rosters){
            if(rosterWrapper.selected == true){
                selectedRoster.add(rosterWrapper.roster);
                
                if (uniquePersonnelMap.containsKey(rosterWrapper.personnel.Id)) {
                    for(SQX_Personnel_Document_Training__c pdt: uniquePersonnelMap.get(rosterWrapper.personnel.Id)){
                        pdt.SQX_Training_Session__c = session.id;
                        selectedTraining.add(pdt);
                    }
                }
            }
        }
        
        if(selectedRoster.size() > 0){
            sp = Database.setSavepoint();
            
            try{
                new SQX_DB().op_insert(selectedRoster, new List<Schema.SObjectField>{
                                                            SQX_Training_Session_Roster__c.SQX_Training_Session__c,
                                                            SQX_Training_Session_Roster__c.SQX_Personnel__c
                                                        });
                
                if (selectedTraining.size() > 0) {
                    /*
                    * WITHOUT SHARING used 
                    * --------------------
                    * Escalate users/trainers with read only access on personnel records and their child records to update training session link on document training
                    */
                    new SQX_DB().withoutSharing().op_update(selectedTraining, new List<Schema.SObjectField>{
                                                                SQX_Personnel_Document_Training__c.SQX_Training_Session__c
                                                            });
                }
                
                ref = new ApexPages.StandardController(session).view();
            }catch(Exception ex){
                if(sp != null){
                    Database.rollback(sp);
                }
                
                SQX_Utilities.addErrorMessageInPage(ex.getMessage());
            }
            
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_NO_PERSONNEL_SELECTED));
        }
        return ref; 
    }

    /**
    * Wrapper class to get the selected personnel value
    */
    public with sharing class SQX_Training_Session_Roster_Wrapper {
        public Boolean selected {get; set;}
        public SQX_Training_Session_Roster__c roster {get; set;}
        public SQX_Personnel__c personnel {get; set;}
        
        public SQX_Training_Session_Roster_Wrapper(SQX_Training_Session__c session, SQX_Personnel__c psn){
            personnel = psn;
            roster = new SQX_Training_Session_Roster__c(
                SQX_Training_Session__c = session.Id,
                SQX_Personnel__c = psn.Id
            );
        }
    }
}