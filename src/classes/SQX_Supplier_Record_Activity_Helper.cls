/**
 * Helper class for Supplier Management Module to record activities
 */
public with sharing class SQX_Supplier_Record_Activity_Helper {

    private final String CHILD_FIELD_ACTIVITY_CODE = SQX.NSPrefix + 'Activity_Code__c',
                         CHILD_FIELD_ACTIVITY_COMMENT = SQX.NSPrefix + 'Comment_Long__c',
                         CHILD_FIELD_TYPE_OF_ACTIVITY = SQX.NSPrefix + 'Type_of_Activity__c',
                         CHILD_FIELD_MODIFIED_BY = SQX.NSPrefix + 'Modified_By__c';

    /*
        Map holds the default activity comment for specific actions
    */
    private final Map<String, String> defaultCommentMap = new Map<String, String>
    {
        SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE => Label.CQ_UI_Completing_the_record,
        SQX_Supplier_Common_Values.ACTIVITY_CODE_SAVE => Label.CQ_UI_Saving_The_Record
    };

    /*
        SObjectField entity in record activity child object that links to the parent object
    */
    private SObjectField parentSObjectField;

    /*
        Type of record activity object associated with parent object
    */
    private SObjectType recordActivitySObjectType;

    /*
        Default activity type to be set with each record activity
    */
    private String defaultActivityType;

    /*
        Default modified by field
    */
    private String modifiedBy;

    /*
        All SObjectFields of the Record Activity object that get updated when inserting the activity
    */
    private List<Schema.SObjectField> updateFieldSet;

    /*
        Instance of SQX_DB to be used to perform DMLs throughout this class
    */
    private SQX_DB db;

    /**
     * Constructor class that sets in default values based on the record type
     * @param objType SObjecttype of the record on which the activity has been performed on
    */
    public SQX_Supplier_Record_Activity_Helper(SObjectType objType){
        this(objType, true);
    }

    /**
     * Constructor class that sets in default values based on the records
     * @param records List of records whose activity is to be recorded
     * @param ignoreBlankActivities flag that determines whether to ignore records with blank activity code or not
     * @param escalatePrivilege set <code>True</code> if sharing rules are to be bypassed
    */
    public SQX_Supplier_Record_Activity_Helper(SObjectType objType, Boolean escalatePrivilege){

        this.db = new SQX_DB();
        if(escalatePrivilege) {
            this.db.withoutSharing();
        }

        recordActivitySObjectType = SQX_Record_History.RECORD_ACTIVITY_OBJECTTYPE.get(objType);

        Map<String, SObjectField> allFields = recordActivitySObjectType.getDescribe().fields.getMap();

        parentSObjectField = allFields.get(objType + '');

        updateFieldSet = new List<Schema.SObjectField> {
            allFields.get(CHILD_FIELD_ACTIVITY_CODE),
            allFields.get(CHILD_FIELD_ACTIVITY_COMMENT),
            allFields.get(CHILD_FIELD_TYPE_OF_ACTIVITY),
            allFields.get(CHILD_FIELD_MODIFIED_BY),
            parentSObjectField
        };

        defaultActivityType = SQX_Record_History.TYPE_EDIT; // Record Activity is only being inserted as part of an update
        modifiedBy = SQX_Utilities.getUserDetailsForRecordActivity();
    }

    /**
     * Method records the activities of the records
     * In addition, the method clears out the Activity Code after insertion
     */
    public void insertRecordActivity(List<SObject> records) {

        if(records.size() > 0) {
            List<SObject> recordActivitiesToBeInserted = new List<SObject>();

            for(SObject record : records) {
                if(!String.isBlank(getActivityCodeFor(record))){
                    recordActivitiesToBeInserted.add(getRecordActivityFor(record));
                }
            }

            db.op_insert(recordActivitiesToBeInserted, updateFieldSet);
        }

    }

    /**
     * Method returns a record activity object filled in with appropriate values from the given parent record
     * @param record sobject record whose action is to be recorded
     * @returns record activity sobject, null if the record doesn't have an activity code set
     */
    private SObject getRecordActivityFor(SObject record) {

        SObject recordActivity = recordActivitySObjectType.newSObject();
        recordActivity.put(parentSObjectField, (String) record.get('Id'));   // add parent record link

        String activityCode = getActivityCodeFor(record);
        recordActivity.put(CHILD_FIELD_ACTIVITY_CODE, activityCode);
        recordActivity.put(CHILD_FIELD_ACTIVITY_COMMENT, getCommentFor(record, activityCode));
        recordActivity.put(CHILD_FIELD_TYPE_OF_ACTIVITY, defaultActivityType);
        recordActivity.put(CHILD_FIELD_MODIFIED_BY, modifiedBy);

        return recordActivity;
    }


    /**
     * Method returns the activity code set in the given record
     * If no activity code was set in the record, it assumses the activity to be 'Save'
     * based on the value of ignoreBlankActivities
     */
    private String getActivityCodeFor(SObject record) {
        String activityCode = (String) record.get(SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_CODE);
        if(String.isBlank(activityCode)) {
            activityCode = SQX_Supplier_Common_Values.ACTIVITY_CODE_SAVE;
        }
        return activityCode;
    }

    /**
     * Method returns the comment supplied by the user when performing the action
     * If no comment was supplied and the given activity has a default comment associated
     * the method sets the default comment and returns it
     */
    private String getCommentFor(SObject record, String activityCode) {
        String comment = (String) record.get(SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_COMMENT);

        if(String.isBlank(comment) && defaultCommentMap.containsKey(activityCode)) {
            comment = defaultCommentMap.get(activityCode);
        }

        return comment;
    }

}