/*
* This class used for controlled document checkout, checkin and undo checkout
*/
global with sharing class SQX_Extension_Controlled_Doc_Checkout extends SQX_Controller_Sign_Off{

    private SQX_Controlled_Document__c mainRecord=null;

    public static final String  CODE_OF_SIG_CHECKOUT = 'checkout',
                                CODE_OF_SIG_CHECKIN = 'checkin',
                                CODE_OF_SIG_UNDOCHECKOUT = 'undocheckout';

    /*
        This property holds the parameter name passed to the page that signifies what operation ( checkout, undocheckout, checkin) is being performed
    */
    public static final String PARAM_MODE = 'mode';

    private final Map<String, String> paramMap = new Map<String,String>{
        'in' => CODE_OF_SIG_CHECKIN,
        'out' => CODE_OF_SIG_CHECKOUT,
        'undo' => CODE_OF_SIG_UNDOCHECKOUT
    };
    
    global Boolean KeepCheckedOut{get;set;}

    /*
    * Add default policies
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            CODE_OF_SIG_CHECKOUT => this.POLICY_NO_COMMENT_REQUIRED,
            CODE_OF_SIG_UNDOCHECKOUT => this.POLICY_NO_COMMENT_REQUIRED
        };
        return defaultEsigPolicies;
    }

    global SQX_Extension_Controlled_Doc_Checkout(ApexPages.StandardController controller) {

        super(controller);

        mainRecord= (SQX_Controlled_Document__c)controller.getRecord();

        this.recordId = mainRecord.Id;

        this.purposeOfSigMap.putAll( new Map<String, String>{
                                        CODE_OF_SIG_CHECKOUT => Label.SQX_PS_Controlled_Doc_Checkout,
                                        CODE_OF_SIG_CHECKIN => Label.SQX_PS_Controlled_Doc_Checkin,
                                        CODE_OF_SIG_UNDOCHECKOUT => Label.SQX_PS_Controlled_Doc_UndoCheckout
                                    });

        String action = 'in';   //checkin
        if(ApexPages.currentPage() != null){
            if(ApexPages.currentPage().getParameters().containsKey(PARAM_MODE)){
                action = ApexPages.currentPage().getParameters().get(PARAM_MODE);
            }
            
            if(paramMap.containsKey(action)){
                this.actionName = paramMap.get(action);
                this.purposeOfSignature = this.purposeOfSigMap.get(actionName);
            }
        }
    }

    /*
    * This method used to checkout a controlled document & track record activity
    */ 
    global PageReference checkoutDocument(){
        SavePoint sp = null;
        if(!SQX_Controlled_Document_Collaboration.isControlledDocCheckout(mainRecord.Id)){
            if (this.hasValidCredentials()) {
                try{
                    sp = Database.setSavePoint();
                    
                    SQX_Controlled_Document_Collaboration.checkOut(mainRecord);
                    SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Controlled_Document__c.SObjectType), mainRecord.Id, string.valueOf(mainRecord.Id), this.RecordActivityComment, purposeOfSigMap.get(CODE_OF_SIG_CHECKOUT), CODE_OF_SIG_CHECKOUT);
                }catch(Exception ex){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                    
                    if (sp != null) {
                        Database.rollBack(sp);
                    }
                }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.SQX_ERR_MSG_Already_Checked_Out)); 
        }
        return null;
    }
    
    /*
    * This method used to checkin a controlled document & track record activity
    */ 
    global PageReference checkinDocument(){
        SavePoint sp = null;
        if (this.hasValidCredentials()) {
            try{
                sp = Database.setSavePoint();
                
                if(!KeepCheckedOut){
                    SQX_Controlled_Document_Collaboration.undoCheckOut(mainRecord);
                }
                SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Controlled_Document__c.SObjectType), mainRecord.Id, string.valueOf(mainRecord.Id), this.RecordActivityComment, purposeOfSigMap.get(CODE_OF_SIG_CHECKIN), CODE_OF_SIG_CHECKIN);
            }catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                
                if (sp != null) {
                    Database.rollBack(sp);
                }
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        return null;
    }
    
    /*
    * This method used to undo checkout & track record activity
    */
    global PageReference undoCheckoutDocument() {
        SavePoint sp = null;
        if (this.hasValidCredentials()) {
            try{
                sp = Database.setSavePoint();
                
                SQX_Controlled_Document_Collaboration.undoCheckOut(mainRecord);
                SQX_Record_History.insertRecordHistory(string.valueOf(SQX_Controlled_Document__c.SObjectType), mainRecord.Id, string.valueOf(mainRecord.Id), this.RecordActivityComment, purposeOfSigMap.get(CODE_OF_SIG_UNDOCHECKOUT), CODE_OF_SIG_UNDOCHECKOUT);
            }catch(Exception ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                
                if (sp != null) {
                    Database.rollBack(sp);
                }
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        return null;
    }
    
    /*
    * returns current user signature data
    */
    global String getCurrentUserSignatureData() {
        return SQX_Utilities.getUserDetailsForRecordActivity();
    }

    /*
    * returns current user is a controlled document checkout user or not
    */ 
    global Boolean getIsUserCheckOutUser(){
        return SQX_Controlled_Document_Collaboration.isUserCheckOutUser(mainRecord);
    }

    /*
    * Returns current user has document supervisory permission
    */
    global Boolean getIsSupervisorUser(){
        return SQX_Controlled_Document_Collaboration.userHasSupervisoryPermission();
    }
}