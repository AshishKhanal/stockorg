/**
* This class will contain the common methods and static strings required for the Escalation Reference object
**/
public with sharing class SQX_Escalation_Reference {
        
    public static final List<String> CLOSED_ESCALATION_STATUSES = new String[]{SQX_Supplier_Common_Values.STATUS_CLOSED, SQX_Supplier_Common_Values.STATUS_VOID};
        
	/** 
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param oldReference equivalent to trigger.Old in salesforce
        * @param newMap equivalent to trigger.NewMap in salesforce
        */
        public Bulkified(List<SQX_Escalation_Reference__c> oldReference, Map<Id, SQX_Escalation_Reference__c> newMap){
            
            super(oldReference, newMap);
        }
        
        /**
        * This handler ensures that audit owner can't be removed from the audit team
        */
        public Bulkified preventEscalationReferenceCannotBeDeletedAfterEscalationIsVoidOrClosed(){
            Map<Id, SQX_Escalation_Reference__c> escalationReferenceMap = new Map<Id, SQX_Escalation_Reference__c>((List<SQX_Escalation_Reference__c>)this.resetView().view);
            for(SQX_Escalation_Reference__c reference : [SELECT Id FROM SQX_Escalation_Reference__c 
                                                         WHERE Id IN: escalationReferenceMap.keySet() 
                                                         AND SQX_Supplier_Escalation__r.Status__c IN: CLOSED_ESCALATION_STATUSES]){
                                                             escalationReferenceMap.get(reference.Id).addError(Label.SQX_ERR_REFERENCE_CANNOT_BE_DELETED);
            }
            
            return this;
        }
    }
}