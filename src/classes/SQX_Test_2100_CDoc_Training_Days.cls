@isTest
public class SQX_Test_2100_CDoc_Training_Days{
    /**
     * Scenario 1:
     * ACTION: Save the doc with days to complete training as negative
     * EXPECTED: Doc should not be saved and should throw an error.
     *
     * Scenario 2:
     * ACTION:Save the doc with days to complete training as positive
     * EXPECTED: Doc should be saved.
     * @date: 2016-04-07
     * @story: [SQX-2100]
    */
    public testMethod static void givenADoc_DocShouldOnlyBeSaved_WhenValidDaysForTrainingIsEntered(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_DRAFT).save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, Duration__c  FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            controlledDoc.Duration__c = -5;

            // Act 1: set days to complete training as negative and save controlled doc
            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            // Assert 2: Expected the doc not to be saved
            System.assertEquals(false, docStatus.isSuccess(), 'Expected the doc not to be saved');

            // Act 2: Set days to complete training as positive and save controlled doc
            controlledDoc.Duration__c = 5;

            docStatus =  Database.update(controlledDoc, false);

            // Assert 2: Expected doc to be saved.
            System.assertEquals(true, docStatus.isSuccess(), 'Expected the doc to be saved but found errors: ' + docStatus.getErrors());

        }
    }
}