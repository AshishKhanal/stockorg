/*
 * test for in Controlled Document class and trigger
 * author: Anish Shrestha
 * date: 2015/06/04
 * story: SQX-1166
 **/
@isTest
public class SQX_Test_1166_Controlled_Doc_Approval{

    static boolean runAllTests = false,
                    run_givenUser_WhenControlledDocApproved_MovesToReleaseLibrary = false,
                    run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsTrue_StatusIsCurrent = true,
                    run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalse_StatusIsApproved = true,
                    run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalseAndReleased_StatusIsCurrent = true;

 /*
  Helper methods to fill in required field in controlled doc and get it ready for insertion
  */
  private static void setupControlledDoc(SQX_Controlled_Document__c doc){
    integer randomNumber = (Integer)( Math.random() * 1000000 );
    doc.Document_Number__c = 'Doc' + randomNumber;
    doc.Revision__c = 'REV-1';
    doc.Title__c = 'Document Title ' + randomNumber;
    doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
  }
    // THIS UNIT TEST FAILS BECAUSE THE LIBRARY VAULT CANNOT BE USED IN THE UNIT TEST
    /**
    * When the user creates controlled doc and it gets approved, then the doc is moved to release library
    * @author Anish Shrestha
    * @date 2015/06/04
    * @story [SQX-1166]
    */ 
    public testMethod static void givenUser_WhenControlledDocApproved_MovesToReleaseLibrary(){

        if(!runAllTests && !run_givenUser_WhenControlledDocApproved_MovesToReleaseLibrary){
            return;
        }

        List<ContentWorkspace> libraries = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Test Release Vault' OR Name = 'Test Draft Vault'];

        System.assertEquals(2, libraries.size(), 'Libraries must be set up before running this test for the current user');
        /*
        * Instruction on how to setup the library:
        *
        * 1. Create 2 libraries named 'Test Release Vault' and 'Test Draft Vault'
        * 2. Provide at least 'Author' role in both the vaults
        */

        ContentWorkspace releaseVault, draftVault;

        releaseVault = libraries.get(0).Name.equals('Test Release Vault') ? libraries.get(0) : libraries.get(1);
        draftVault = libraries.get(0).Name.equals('Test Draft Vault') ? libraries.get(0) : libraries.get(1);

        //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
        Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
        SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(doc);
        SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

        extension.file = Blob.valueOf('Hello World');
        extension.fileName = 'Hello.txt';
        setupControlledDoc(doc);

        doc.Release_Vault__c = releaseVault.Id;
        doc.Draft_Vault__c = draftVault.Id;

        extension.create();
        //Act: Update the document status to current
        doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
        new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc}, new List<Schema.SObjectField>{
                Schema.SQX_Controlled_Document__c.Document_Status__c
        });
        
        doc =  [SELECT Content_Reference__c, Release_Vault__c
                                                 FROM SQX_Controlled_Document__c
                                                 WHERE Id = : doc.Id];

        //Assert: Ensure that the referenced document is now in release vault

        //System.assert(false, ''+doc.Content_Reference__c);
        ContentDocument conDoc = new ContentDocument(); 
        conDoc = [SELECT Id, ParentId FROM ContentDocument WHERE Id =: doc.Content_Reference__c];

        System.assert(conDoc.ParentId == doc.Release_Vault__c,'Expected the document to be in release vault');

    }

    /**
    * When the user creates controlled doc and it gets approved and auto release is on, then the doc status becomes current
    * @author Anish Shrestha
    * @date 2015/06/04
    * @story [SQX-1166]
    */ 
    public testMethod static void givenUser_WhenControlledDocApproved_AndAutoReleaseIsTrue_StatusIsCurrent(){

        if(!runAllTests && !run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsTrue_StatusIsCurrent){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){

            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();
            
            doc =  [SELECT Auto_Release__c, Document_Status__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];


            //Act: Update the document status to current
            doc.Auto_Release__c = true;
            doc.Document_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc}, new List<Schema.SObjectField>{
                    Schema.SQX_Controlled_Document__c.Auto_Release__c,
                    Schema.SQX_Controlled_Document__c.Document_Status__c
            });
            doc =  [SELECT Document_Status__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];

            //Assert: Ensure that the referenced document is now in release vault

            System.assert(doc.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_RELEASE, 'Expected the document to be in Pre Release State');
        }

    }

    /**
    * When the user creates controlled doc and it gets approved and auto release is on, then the doc status becomes current
    * @author Anish Shrestha
    * @date 2015/06/04
    * @story [SQX-1166]
    */ 
    public testMethod static void givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalse_StatusIsApproved(){

        if(!runAllTests && !run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalse_StatusIsApproved){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){

            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();
            
            doc =  [SELECT Auto_Release__c, Document_Status__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];

            //Act: Update the document status to current
            doc.Auto_Release__c = false;
            doc.Document_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc}, new List<Schema.SObjectField>{
                    Schema.SQX_Controlled_Document__c.Auto_Release__c,
                    Schema.SQX_Controlled_Document__c.Document_Status__c
            });
            
            doc =  [SELECT Document_Status__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];

            //Assert: Ensure that the referenced document is now in release vault

            System.assert(doc.Document_Status__c == SQX_Controlled_Document.STATUS_APPROVED, 'Expected the document to be in approved state');
        }

    }

    /**
    * When the user creates controlled doc and it gets approved and auto release is on, then the doc status becomes current
    * @author Anish Shrestha
    * @date 2015/06/04
    * @story [SQX-1166]
    */ 
    public testMethod static void givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalseAndReleased_StatusIsCurrent(){

        if(!runAllTests && !run_givenUser_WhenControlledDocApproved_AndAutoReleaseIsFalseAndReleased_StatusIsCurrent){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){

            //Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();
            
            doc =  [SELECT Auto_Release__c, Document_Status__c, Effective_Date__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];

            //Act: Update the document status to current
            doc.Auto_Release__c = false;
            doc.Document_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
            doc.Effective_Date__c = Date.Today().addDays(15);
            new SQX_DB().op_update(new List<SQX_Controlled_Document__c>{doc}, new List<Schema.SObjectField>{
                    Schema.SQX_Controlled_Document__c.Auto_Release__c,
                    Schema.SQX_Controlled_Document__c.Document_Status__c,
                    Schema.SQX_Controlled_Document__c.Effective_Date__c
            });


            extension.Release();
            
            doc =  [SELECT Document_Status__c FROM SQX_Controlled_Document__c
                                                WHERE Id = : doc.Id];




            //Assert: Ensure that the referenced document is now in release vault

            System.assert(doc.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_RELEASE, 'Expected the document to be in pre-release state after doc is released but found: ' + doc.Document_Status__c);
        }

    }

}