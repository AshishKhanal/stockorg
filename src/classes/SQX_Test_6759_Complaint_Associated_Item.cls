/**
 * Test class to validate Complaint Association Item generation
 */
@isTest
public class SQX_Test_6759_Complaint_Associated_Item {

    @testSetup
    static void commonSetup() {

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'adminUser');

        System.runAs(adminUser){

            // creating random parts
            List<SQX_Part__c> parts = SQX_Test_Part.insertParts(2);
        }
    }

    /**
     * GIVEN : A complaint is saved with associated item
     * WHEN : Primary Associated Item is updated
     * THEN : Complaint is updated
     */
    public static testMethod void givenComplaintWithAssociatedItem_WhenPrimaryAssociatedItemIsChanged_ComplaintIsUpdated(){

        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Generate a new complaint record instance associated with a random part
            SQX_Part__c part_1 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(1);
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = part_1.Id;

            compTst.save();

            // ACT : New associated item is made primary
            SQX_Complaint_Associated_Item__c newAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = part_2.Part_Family__c, SQX_Part__c = part_2.Id, Make_Primary__c = true);
            insert newAssociatedItem;

            newAssociatedItem = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, Is_Primary__c, Make_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :newAssociatedItem.Id];
            SQX_Complaint__c comp = [SELECT Id, SQX_Part__c, SQX_Part_Family__c,  SQX_Primary_Associated_Item__c FROM SQX_Complaint__c WHERE Id = :compTst.complaint.Id];

            // ASSERT : Complaint is updated with new associated item details
            System.assertEquals(newAssociatedItem.SQX_Part__c, comp.SQX_Part__c);
            System.assertEquals(newAssociatedItem.SQX_Part_Family__c, comp.SQX_Part_Family__c);
            System.assertEquals(newAssociatedItem.Id, comp.SQX_Primary_Associated_Item__c);
            System.assertEquals(true, newAssociatedItem.Is_Primary__c);
            System.assertEquals(false, newAssociatedItem.Make_Primary__c);
        }
    }

    /**
     * GIVEN : A complaint is saved with associated item
     * WHEN : Associated Item is updated
     * THEN : Complaint is updated
     */
    public static testMethod void givenComplaintWithAssociatedItem_WhenAssociatedItemIsUpdated_ComplaintIsUpdated(){

        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Generate a new complaint record instance associated with a random part
            SQX_Part__c part_1 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(1);
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = part_1.Id;

            compTst.save();

            SQX_Complaint_Associated_Item__c associatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = part_2.Part_Family__c, SQX_Part__c = part_2.Id, Make_Primary__c = true);
            insert associatedItem;

            // ACT : Associated item is updated
            associatedItem.Lot_Number__c = 'TEST_LOT';
            associatedItem.Manufacturing_Date__c = Date.today();
            associatedItem.Expiration_Date__c = Date.today().addDays(10);

            update associatedItem;

            SQX_Complaint__c comp = [SELECT Id, Lot_Number__c, Manufacturing_Date__c, Expiration_Date__c FROM SQX_Complaint__c WHERE Id = : compTst.complaint.Id];

            // ASSERT : Complaint is updated
            System.assertEquals(associatedItem.Lot_Number__c, comp.Lot_Number__c);
            System.assertEquals(associatedItem.Manufacturing_Date__c, comp.Manufacturing_Date__c);
            System.assertEquals(associatedItem.Expiration_Date__c, comp.Expiration_Date__c);
        }
    }

    /**
     * GIVEN : A complaint is saved with associated item
     * WHEN : Primary Associated Item is deleted
     * THEN : Error is thrown
     */
    public static testMethod void givenComplaintWithAssociatedItem_WhenPrimaryAssociatedItemIsDeleted_ErrorIsThrown(){

        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Generate a new complaint record instance associated with a random part
            SQX_Part__c part_1 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(1);
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.complaint.SQX_Part__c = part_1.Id;

            compTst.save();

            // ACT : Primary Association Item is added
            SQX_Complaint_Associated_Item__c primaryAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = part_2.Part_Family__c, SQX_Part__c = part_2.Id, Make_Primary__c = true);
            insert primaryAssociatedItem;

            primaryAssociatedItem = [SELECT Id, SQX_Part__c, Is_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :primaryAssociatedItem.Id];
            System.assertEquals(true, primaryAssociatedItem.Is_Primary__c);

            // ACT : Association Item is added
            SQX_Complaint_Associated_Item__c associatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, SQX_Part_Family__c = part_2.Part_Family__c, SQX_Part__c = part_2.Id);
            insert associatedItem;

            associatedItem = [SELECT Id, SQX_Part__c, Is_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :associatedItem.Id];
            System.assertEquals(false, associatedItem.Is_Primary__c);

            Database.DeleteResult result = Database.delete(associatedItem, false);

            // ASSERT : Non primary association item is deleted
            System.assert(result.isSuccess(), 'Delete is not successful');

            result = Database.delete(primaryAssociatedItem, false);

            // ASSERT : Error is thrown when primary association item is deleted
            System.assert(!result.isSuccess(), 'Delete is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.SQX_ERR_MSG_Primary_Associated_Item_cannot_be_deleted));

        }
    }

    /**
     * GIVEN : Complaint is created
     * WHEN : Primary associated item is added without part
     * THEN : Error is thrown
     */
    @IsTest
    static void givenComplaintWithAssociatedItem_WhenAssociatedItemIsSavedWithoutPart_ErrorIsThrown(){
      
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        test.startTest();
        system.RunAs(standardUser){
            // ARRANGE : Complaint is created
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            // ACT : Primary associated item is added without part
            SQX_Complaint_Associated_Item__c associatedItem = new SQX_Complaint_Associated_Item__c();
            associatedItem.SQX_Complaint__c = complaint.complaint.Id;
            associatedItem.Make_Primary__c = true;
            associatedItem.SQX_Part_Family__c = [SELECT Id FROM SQX_Part_Family__c LIMIT 1].Id;
            Database.SaveResult result = Database.insert(associatedItem, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is not successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Part is required for primary associated item'), result.getErrors());

            // ACT : Associated item is added with part
            associatedItem.SQX_Part__c = [SELECT Id FROM SQX_Part__c LIMIT 1].Id;
            result = Database.insert(associatedItem, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
        }
        test.stopTest();
      
    }

    /**
     * GIVEN : A complaint is saved with primary associated item
     * WHEN : Another Primary Associated Item is added
     * THEN : Complaint is synchronized with latest primary associated item
     */
    public static testMethod void givenComplaintWithPrimaryAssociatedItem_WhenPrimaryAssociatedItemIsChanged_ComplaintIsUpdated(){

        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Generate a new complaint record instance associated with a random part
            SQX_Part__c part_1 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(1);
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.save();

            // ACT : New associated item is made primary
            SQX_Complaint_Associated_Item__c newAssociatedItem1 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, 
                                                                                                        SQX_Part_Family__c = part_1.Part_Family__c, 
                                                                                                        SQX_Part__c = part_1.Id, 
                                                                                                        Make_Primary__c = true);
            insert newAssociatedItem1;

            newAssociatedItem1 = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, Is_Primary__c, Make_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :newAssociatedItem1.Id];
            SQX_Complaint__c comp = [SELECT Id, SQX_Part__c, SQX_Part_Family__c,  SQX_Primary_Associated_Item__c FROM SQX_Complaint__c WHERE Id = :compTst.complaint.Id];

            // ASSERT : Complaint is updated with new associated item details
            System.assertEquals(newAssociatedItem1.SQX_Part__c, comp.SQX_Part__c);
            System.assertEquals(newAssociatedItem1.SQX_Part_Family__c, comp.SQX_Part_Family__c);
            System.assertEquals(newAssociatedItem1.Id, comp.SQX_Primary_Associated_Item__c);
            System.assertEquals(true, newAssociatedItem1.Is_Primary__c);
            System.assertEquals(false, newAssociatedItem1.Make_Primary__c);

            // ACT : New associated item is made primary
            SQX_Complaint_Associated_Item__c newAssociatedItem2 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, 
                                                                                                        SQX_Part_Family__c = part_2.Part_Family__c, 
                                                                                                        SQX_Part__c = part_2.Id, 
                                                                                                        Make_Primary__c = true);
            insert newAssociatedItem2;

            newAssociatedItem2 = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, Is_Primary__c, Make_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :newAssociatedItem2.Id];
            comp = [SELECT Id, SQX_Part__c, SQX_Part_Family__c,  SQX_Primary_Associated_Item__c FROM SQX_Complaint__c WHERE Id = :compTst.complaint.Id];

            // ASSERT : Complaint is updated with new associated item details
            System.assertEquals(newAssociatedItem2.SQX_Part__c, comp.SQX_Part__c);
            System.assertEquals(newAssociatedItem2.SQX_Part_Family__c, comp.SQX_Part_Family__c);
            System.assertEquals(newAssociatedItem2.Id, comp.SQX_Primary_Associated_Item__c);
            System.assertEquals(true, newAssociatedItem2.Is_Primary__c);
            System.assertEquals(false, newAssociatedItem2.Make_Primary__c);
        }
    }

    /**
     * GIVEN : A complaint is saved with primary associated item
     * WHEN : Existing associated item is promoted as primary
     * THEN : Complaint is synchronized with latest primary associated item
     */
    public static testMethod void givenComplaintWithPrimaryAssociatedItem_WhenExistingAssociatedItemIsPromotedAsPrimary_ComplaintIsUpdated(){

        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser) {
            // ARRANGE : Generate a new complaint record instance associated with a random part
            SQX_Part__c part_1 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);
            SQX_Part__c part_2 = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(1);
            SQX_Test_Complaint compTst = new SQX_Test_Complaint(adminUser);
            compTst.save();

            // ACT : New associated item is made primary
            SQX_Complaint_Associated_Item__c newAssociatedItem1 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, 
                                                                                                        SQX_Part_Family__c = part_1.Part_Family__c, 
                                                                                                        SQX_Part__c = part_1.Id, 
                                                                                                        Make_Primary__c = true);
            insert newAssociatedItem1;

            SQX_Complaint_Associated_Item__c newAssociatedItem2 = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = compTst.complaint.Id, 
                                                                                                        SQX_Part_Family__c = part_2.Part_Family__c, 
                                                                                                        SQX_Part__c = part_2.Id, 
                                                                                                        Make_Primary__c = false);
            insert newAssociatedItem2;

            newAssociatedItem1 = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, Is_Primary__c, Make_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :newAssociatedItem1.Id];
            SQX_Complaint__c comp = [SELECT Id, SQX_Part__c, SQX_Part_Family__c,  SQX_Primary_Associated_Item__c FROM SQX_Complaint__c WHERE Id = :compTst.complaint.Id];

            // ASSERT : Complaint is updated with new associated item details
            System.assertEquals(newAssociatedItem1.SQX_Part__c, comp.SQX_Part__c);
            System.assertEquals(newAssociatedItem1.SQX_Part_Family__c, comp.SQX_Part_Family__c);
            System.assertEquals(newAssociatedItem1.Id, comp.SQX_Primary_Associated_Item__c);
            System.assertEquals(true, newAssociatedItem1.Is_Primary__c);
            System.assertEquals(false, newAssociatedItem1.Make_Primary__c);

            // ACT : Existing associated item is promoted as primary
            newAssociatedItem2.Make_Primary__c = true;
            update newAssociatedItem2;

            newAssociatedItem2 = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, Is_Primary__c, Make_Primary__c FROM SQX_Complaint_Associated_Item__c WHERE Id = :newAssociatedItem2.Id];
            comp = [SELECT Id, SQX_Part__c, SQX_Part_Family__c,  SQX_Primary_Associated_Item__c FROM SQX_Complaint__c WHERE Id = :compTst.complaint.Id];

            // ASSERT : Complaint is updated with new associated item details
            System.assertEquals(newAssociatedItem2.SQX_Part__c, comp.SQX_Part__c);
            System.assertEquals(newAssociatedItem2.SQX_Part_Family__c, comp.SQX_Part_Family__c);
            System.assertEquals(newAssociatedItem2.Id, comp.SQX_Primary_Associated_Item__c);
            System.assertEquals(true, newAssociatedItem2.Is_Primary__c);
            System.assertEquals(false, newAssociatedItem2.Make_Primary__c);
        }
    }
}