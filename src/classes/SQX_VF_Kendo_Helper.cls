/**
 * Helper class for working with Kendo SLDS VF pages
 */
public with sharing class SQX_VF_Kendo_Helper extends SQX_Extension_UI {
    final static String SAVE_ACTION = 'save',
                 KENDO_UI_PARAM = 'kendoui',
                 STATUS_FIELD = 'Status__c',
                 RECORD_TYPE_FIELD = 'RecordTypeId',
                 MASTER_RECORD_TYPE = 'Master';

    final static String CQ_LOOKUP_FIELDSET_NAME = 'CQ_Lookup_Fieldset';

    /**
     * Assigned by initializing component dynamically
     */
    public String action {get; set;}

    /**
     * Contains the values passed through url parameter
     */
    private Set<String> urlPassedParams = new Set<String>();


    /**
     * Initializes the kendo helper extension. It also adds status and stage field to the record.
     * If the fields don't exist no error is thrown.
     */
    public SQX_VF_Kendo_Helper(ApexPages.StandardController controller) {
        super(controller, new List<String> {STATUS_FIELD});
        skipRecordHistoryActions.add(SAVE_ACTION);
        // esig always turned off
        addStaticPolicy(SAVE_ACTION, new SQX_CQ_Esig_Policies__c(Required__c  = false, Comment__c = false));
        this.customerScriptLayout = '_CuScript_Layout';
    }


    /**
     * This method returns the location of the CQ UI Resource cache static resource.
     */
    public String getLayoutResourcePath() {
        return getResourcePath(KENDO_UI_PARAM);
    }

    /**
     * This method returns the current VF Extension so that it can be passed to the component and used
     */
    public SQX_VF_Kendo_Helper getVFExtension() {
        return this;
    }

    /**
     * Used for caching the page title
     */
    private String pageTitle;

    /**
     * Returns the applicable page title for the page
     */
    public String getPageTitle() {
        if(pageTitle == null) {

            SObject sobj = getMainRecord();
            String recordType = getObjRecordType();
            DescribeSObjectResult descr = sobj.getSObjectType().getDescribe();
            String objLabel = descr.getLabel();
            String title = '';
            if(sobj.Id == null) {
                title = Label.CQ_UI_New_SObject.replace('{0}', objLabel);
            } else {
                title = Label.CQ_UI_Edit_SObject.replace('{0}', (String)sobj.get('Name'));
            }

            if(recordType != MASTER_RECORD_TYPE) {
                title += ' - ' + descr.getRecordTypeInfosByDeveloperName().get(recordType).getName();
            }
            pageTitle = title;
        }
        return pageTitle;
    }

    /**
     * Returns the name of the object's record type
     * @return   return description
     */
    public String getObjRecordType() {
        SObject sobj = getMainRecord();
        String recordType = MASTER_RECORD_TYPE;
        if(sobj.getSObjectType().getDescribe().fields.getMap().containsKey(RECORD_TYPE_FIELD)) {
            Id recordTypeId = (Id)sobj.get(RECORD_TYPE_FIELD);
            String objType = getMainRecordType();

            Map<String, Id> recordTypes = SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new String[] { objType }).get(objType);
            for(String key : recordTypes.keySet()) {
                if(recordTypeId == recordTypes.get(key)) {
                    recordType = key;
                    break;
                }
            }
        }


        return recordType;
    }

    /**
     * Iterate over the references present in the object and return the list of fields that must be fetched for them
     * @return   return JSON string representing the object type and additional field to be queried in the UI
     *
     * TODO: Add unit test after a component is added in managed package
     */
    public String getReferencesMap() {
        Map<SObjectType, List<Map<String, String>>> refFields = new Map<SObjectType, List<Map<String, String>>>();
        Map<String, SObjectField> fields = mainRecord.getSObjectType().getDescribe().fields.getMap();
        for(SObjectField field : fields.values()) {
            DescribeFieldResult fieldDescribe = field.getDescribe();
            if((fieldDescribe.isCreateable() || fieldDescribe.isUpdateable()) && fieldDescribe.getType() == DisplayType.Reference) {
                SObjectType ref = fieldDescribe.getReferenceTo().get(0);
                if(!refFields.containsKey(ref)) {
                    List<Map<String, String>> fieldsToShow = getFieldsFromFieldSet(ref.getDescribe());
                    if(!fieldsToShow.isEmpty()) {
                        refFields.put(ref, fieldsToShow);
                    }
                }
            }
        }

        return JSON.serialize(refFields);
    }

    /**
     * Gets the values from field set if the field set is present in either managed or unmanaged pacakge
     */
    public List<Map<String, String>> getFieldsFromFieldSet(DescribeSObjectResult sobjDesc) {
        final String PROPERTY_FIELD = 'Field', PROPERTY_LABEL = 'Label';
        List<Map<String, String>> fields = new List<Map<String, String>>();
        Map<String, FieldSet> fieldSets = sobjDesc.fieldSets.getMap();
        if(fieldSets.containsKey(CQ_LOOKUP_FIELDSET_NAME) || fieldSets.containsKey(SQX.NSPrefix + CQ_LOOKUP_FIELDSET_NAME)) {
            String fieldSetName = CQ_LOOKUP_FIELDSET_NAME;
            fieldSetName = fieldSets.containsKey(fieldSetName) ? fieldSetName : (SQX.NSPrefix + fieldSetName);
            for(Schema.FieldSetMember field : fieldSets.get(fieldSetName).getFields()) {
                Map<String, String> fieldMap = new Map<String, String>();
                fieldMap.put(PROPERTY_FIELD, field.getFieldPath());
                fieldMap.put(PROPERTY_LABEL, field.getLabel());
                fields.add(fieldMap);
            }
        }

        return fields;
    }

    /**
     * This method gets the details for the record being edited/created. Details would include information about record types,
     * default values, evaluation expression (dynamic expression that guides UI's behaviour) etc.
     */
    public String getDetails() {

        SObjectType mainRecordType = mainRecord.getSObjectType();
        Map<String, SObjectField> fields = mainRecordType.getDescribe().fields.getMap();

        // set the values of passed params in url first
        setPassedParams(fields);

        String objectType = mainRecordType.getDescribe().getName();
        String uiDefaults = JSON.serialize(getMainRecord());

        SQX_Lightning_Helper.SQX_Save_UI_Detail detail = new SQX_Lightning_Helper.SQX_Save_UI_Detail(objectType, uiDefaults, action, mainRecord.Id);

        Map<Id, SObject> objectCache = new Map<Id, SObject>();
        for(SQX_Lightning_Helper.SQX_Save_UI_RecordType val : detail.RecordTypes) {
            SObject rec = val.DefaultValue;
            for(String fieldName : rec.getPopulatedFieldsAsMap().keySet()) {
                if(fields.containsKey(fieldName)) {
                    DescribeFieldResult fieldDescribe = fields.get(fieldName).getDescribe();
                    if(fieldDescribe.getType() == DisplayType.Reference) {
                        SQX_Utilities.setReferenceField(rec, fieldDescribe, objectCache, true);
                    }
                }
            }
        }

        return JSON.serialize(detail);
    }

    /**
     * Supports setting of values in main record using url. The parameter have to be prefixed with cq_
     * @param fields - fields present in the main record. To be used to validate passed param
     */
    private void setPassedParams(Map<String, SObjectField> fields) {
        if(ApexPages.currentPage() != null) {
            final Pattern CQ_VALUE = Pattern.compile('cq_(\\w+)');
            Map<String, String> parameters = ApexPages.currentPage().getParameters();
            for(String paramKey : parameters.keySet()) {
                Matcher cqmatcher = CQ_VALUE.matcher(paramKey);
                if(cqmatcher.matches() && String.isNotBlank(parameters.get(paramKey))) {
                    String fName = cqmatcher.group(1);
                    if(fields.containsKey(fName)) {
                        SObjectField sField = fields.get(fName);
                        Object valueToSet = parameters.get(paramKey);
                        if(sField.getDescribe().getType() == DisplayType.Boolean) {
                            valueToSet = valueToSet.toString() == 'true';
                        }
                        tryToSetField(fName, valueToSet);
                    }
                }
            }
        }
    }

    /**
     * Attempts to set a field sent from the params
     */
    private void tryToSetField(String fieldName, Object value) {
        try {
            mainRecord.put(fieldName, value);
            urlPassedParams.add(fieldName);
        } catch (Exception ex) {
            // ignore exception while setting value
        }
    }

    /**
	* Default remote action for the extension to send any data that needs to be persisted or when certain next action is to be performed on the main record
	* @param changeId the Id of the change order record
	* @param changeSet the changes that are to be saved
	* @param objectsToDelete the list of ids that are to be removed from SF
	* @param params the list of extra params such as next action is to be used for processing
	* @param esigData the electronic signature data that must be submitted inorder to process the record
    * @param sobjectType the type of object that is related to the request
	*/
	@RemoteAction
    public static String processChangeSetWithAction(String objectId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData, String sobjectType) {
        Boolean isNewRecord = !SQX_Upserter.isValidId(objectId);
        SObject mainRecord = Schema.getGlobalDescribe().get(sobjectType).newSObject();
        if(!isNewRecord) {
            mainRecord = SQX_DynamicQuery.getSObjectWithId(objectId);
        }

        SQX_Extension_UI ext;

        ext = new SQX_VF_Kendo_Helper(new ApexPages.StandardController(mainRecord));
        SQX_Upserter.SQX_Upserter_Config config =  new SQX_Upserter.SQX_Upserter_Config();
        config.captureLastError = true;

        return ext.executeTransaction(objectId, changeset, objectsToDelete, params, esigData, config, null);
    }

    /**
     * Method useful to invoke the flow with given namespace and name. The 'obj' param will contain
     * JSON value for the initiating object and will be passed to the flow.
     * @return returns the JSON object containing all the values of the fields.
     */
    @RemoteAction
    public static String invokeFlow(String namespace, String flowName, String obj){
        return JSON.serialize(SQX_Lightning_Helper.invokeFlow(namespace, flowName, obj));
    }
}