public with sharing class SQX_Implementation_Action{

    /** picklist values ***/
    public static final string STATUS_OPEN = 'Open';
    public static final string STATUS_COMPLETE = 'Complete';
    public static final string STATUS_SKIPPED = 'Skipped';
    
    public static final string TARGET_ACTION_NEW = 'New';
    public static final string TARGET_ACTION_SKIP = 'Skip';
    public static final string TARGET_ACTION_COMPLETE = 'Complete';
    
    public static string getFinalStatusFor(String targetAction){
        if(targetAction == TARGET_ACTION_NEW ){
            return STATUS_OPEN;
        }
        else if(targetAction == TARGET_ACTION_SKIP ){
            return STATUS_SKIPPED;
        }
        else if(targetAction == TARGET_ACTION_COMPLETE ){
            return STATUS_COMPLETE;
        }
        else{
            System.assert(false, 'Unknown target action received');
        }
        
        return null;
    }
    
    /**
    * This class handles the trigger events related to implementation object and performs various actiosn
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {
        /**
        * Default constructor
        */
        public Bulkified(List<SQX_Action__c> newList, Map<Id, SQX_Action__c> oldMap) {
            super(newList, oldMap);
        }
        
        /**
        * sets assignee as the owner of the record
        */
        public Bulkified syncRecordOwnerWithAssignee() {
            this.resetView();
            
            if (this.view.size() > 0) {
                for (SQX_Action__c action : (List<SQX_Action__c>)this.view) {
                    if (action.Status__c == STATUS_OPEN && action.SQX_User__c != null) {
                        // set assignee as owner of the record
                        action.OwnerId = action.SQX_User__c;
                    }
                }
            }
            
            return this;
        }
        
        /**
        * add the permission to edit the action to the main record owner when the assignee is other than main record owner
        */
        public Bulkified shareRecordWithMainRecordOwner() {
            final String ACTION_NAME = 'shareRecordWithMainRecordOwner';
            
            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Action__c.OwnerId, RuleOperator.HasChanged);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Action, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (filter.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.Action, ACTION_NAME, filter.evaluationResult);
                
                List<SQX_Action__Share> objectsToInsert = new List<SQX_Action__Share>();
                
                for (SQX_Action__c action : (List<SQX_Action__c>)filter.evaluationResult) {
                    if (action.OwnerId != action.Main_Record_Owner_Id__c) {
                        // if main record owner is not same as action owner, grant edit i.e. read/write access to main record owner
                        SQX_Action__Share share = new SQX_Action__Share(
                            AccessLevel = 'edit',
                            ParentId = action.Id,
                            UserOrGroupId = action.Main_Record_Owner_Id__c,
                            RowCause = Schema.SQX_Action__Share.RowCause.SQX_Shared__c
                        );
                        objectsToInsert.add(share);
                    }
                }
                
                if (objectsToInsert.size() > 0) {
                    // action can be completed by a user with Read only access
                    // error while adding sharing rule is not critical and can always be added manually
                    new SQX_DB().withoutSharing().continueOnError().op_insert(objectsToInsert, new List<SObjectField>{});
                }
            }
            
            return this;
        }

        /**
        * Assigns auditee as the owner of the implementation while creating implementation if assignee field is left empty in Audit.
        * Assigns coordinator as the owner of the implementation while creating implementation if assignee field is left empty in Cpa
        */
        public Bulkified assignAuditeeAndCoordinatorAsActionOwner(){
            final String ACTION_NAME = 'assignAuditeeAndCoordinatorAsActionOwner';

            Rule actionWithEmptyAssignee = new Rule();
            actionWithEmptyAssignee.addRule(Schema.SQX_Action__c.SQX_User__c, RuleOperator.Equals, null);
            
            this.resetView()
                .applyFilter(actionWithEmptyAssignee, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(actionWithEmptyAssignee.evaluationResult.size() > 0){

                List<SQX_Action__c> actionList = actionWithEmptyAssignee.evaluationResult;

                Set<Id> auditIds = getIdsForField(actionWithEmptyAssignee.evaluationResult, SQX_Action__c.SQX_Audit__c);
                Set<ID> capaIds = getIdsForField(actionWithEmptyAssignee.evaluationResult, SQX_Action__c.SQX_CAPA__c);
                                
                if(auditIds.size()>0 || capaIds.size()>0){
                    Map<Id, SQX_Audit__c> auditMap;
                    Map<Id, SQX_CAPA__c> capaMap;
                    if(auditIds.size()>0){
                        auditMap = new Map<Id, SQX_Audit__c>([SELECT Id, SQX_Auditee_Contact__c FROM SQX_Audit__c WHERE ID IN: auditIds]);
                    }
                    if(capaIds.size()>0){
                        capaMap = new Map<Id, SQX_CAPA__c>([SELECT Id, SQX_CAPA_Coordinator__c FROM SQX_CAPA__c WHERE ID IN: capaIds]);
                    }
                

                    for(SQX_Action__c action : actionList){
                        if(action.SQX_Audit__c != null){
                            action.SQX_User__c = auditMap.get(action.SQX_Audit__c).SQX_Auditee_Contact__c;
                        }
                        else if(action.SQX_CAPA__c != null){
                            action.SQX_User__c = capaMap.get(action.SQX_CAPA__c).SQX_CAPA_Coordinator__c;
                        }
                        

                    }
                }
            }
            
            return this;
        }

        /**
        * @Descrption : This method shares audit action with the members of audit team.
        * @Author: Paras Kumar Bishwakarma
        * @Story: [SQX-2370]
        * @Date: 09/23/2016
        */
        public Bulkified shareAuditActionWithAuditTeamMembers(){
            final String ACTION_NAME = 'shareAuditActionWithAuditTeamMembers';
            
            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Action__c.OwnerId, RuleOperator.HasChanged);
            filter.addRule(Schema.SQX_Action__c.SQX_Audit__c, RuleOperator.NotEquals,null);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.Action, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Action__c> newActions = (List<SQX_Action__c>)filter.evaluationResult;

            if (newActions.size() > 0) {
                //add the records in the list of processed records, to prevent duplicate evaluation in this same transaction
                this.addToProcessedRecordsFor(SQX.Action, ACTION_NAME, newActions);
                SQX_Audit_Team.shareRecordsWithAuditTeamMembers(newActions, SQX_Action__c.SQX_Audit__c, Schema.SQX_Action__Share.RowCause.SQX_Shared__c, false);
            }
            return this;
        }
    }
}