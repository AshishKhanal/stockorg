/**
 * This test ensures that the activation and deactivation of job function (training program) works correctly.
 */
@isTest
public class SQX_Test_4428_Job_Function_Activation {
    
    @testSetup
    public static void commonSetup() {

        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        
        System.runAs(adminUser) {

            // Create job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'JF_TrainingProgram', Active__c =  false );
            insert(new List<SQX_Job_Function__c> {jf1});
            
            // Assert : Verify job function has been inserted properly.
            System.assert(jf1.Id != null, 'Excpected job function jf1 to be inserted');
            
            // Create personnels
            SQX_Personnel__c p1 = new SQX_Test_Personnel('PersonnelName1').mainRecord;
            SQX_Personnel__c p2 = new SQX_Test_Personnel('PersonnelName2').mainRecord;
            
            insert(new List<SQX_Personnel__c>{ p1, p2});

            // Assert : Verify personnels have been inserted correctly
            System.assert(p1.Id != null, 'Excpected personnel p1 to be inserted');
            System.assert(p2.Id != null, 'Excpected personnel p2 to be inserted');
            
            // Create required PJF records
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = false );
            SQX_Personnel_Job_Function__c pjf2 = new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf1.Id, Active__c = false );
            
            insert(new List<SQX_Personnel_Job_Function__c>{ pjf1, pjf2});
            
            // Assert : Verify that pjfs have been inserted correctly.
            System.assert(pjf1.Id != null, 'Excpected personnel jobfunction pjf1 to be inserted');
            System.assert(pjf2.Id != null, 'Excpected personnel jobfunction pjf1 to be inserted');
            
            // Create Controlled documents that are reqiured for generating training.
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // Assert : Verify documents have been inserted correctly
            System.assert(cDoc1.doc.Id != null, 'Excpected document cDoc1 to be inserted');
            System.assert(cDoc2.doc.Id != null, 'Excpected document cDoc2 to be inserted');

            // Create requirements
            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND
            );
            
            SQX_Requirement__c Req2 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc2.doc.Id, 
                SQX_Job_Function__c = jf1.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND
            );
            
            insert (new List<SQX_Requirement__c> {Req1, Req2});

            // Assert : Verify requirements have been inserted correctly
            System.assert(Req1.Id != null, 'Excpected requirement Req1 to be inserted');
            System.assert(Req2.Id != null, 'Excpected requirement Req2 to be inserted');
        }
    }
    
    /**
     * Internal method that returns all pjf
     * @param personnels personnels for which the pjf has been created
     * @param jfs job functions for pjf has been created
     */
    private static List<SQX_Personnel_Job_Function__c> getPJFs(List<SQX_Personnel__c> personnels, List<SQX_Job_Function__c> jfs) {
        return [SELECT Id, Active__c, Activation_Date__c, Deactivation_Date__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Personnel__c IN : personnels AND SQX_Job_Function__c IN : jfs];
    }
    
    /**
     * Internal method that returns all job functions 
     * @param jfNames list of names of job functions, required to query job functions.
     */
    private static List<SQX_Job_Function__c> getJobFunctionByNames(List<String> jfNames) {
		return [SELECT Id, Active__c, Activation_Date__c, Deactivation_Date__c FROM SQX_Job_Function__c WHERE Name IN :jfNames];
    }
    
    /**
     * Internal method that returns all requirements 
     * @param jfs list of job function used to query requirements
     * @param docs list of controlled documents used to query requirements
     */
    private static List<SQX_Requirement__c> getRequirementsByJFs(List<SQX_Job_Function__c> jfs, List<SQX_Controlled_Document__c> docs) {
        return [SELECT Id, Active__c, Activation_Date__c, Deactivation_Date__c, Training_Job_Status__c FROM SQX_Requirement__c WHERE SQX_Job_Function__c IN :jfs AND SQX_Controlled_Document__c IN : docs];
    }
    
    /**
     * Given : Create inactive training program (i.e. job function say jfs), personnel job function say pjfs, requirements say reqs, documents say docs, personnels say personells
     * When : Requirement and Personnel Job Function are tried to activated
     * Then : Error should be thrown as job function is still inactive
     */
    public static testmethod void givenInactiveJobFunction_WhenRequirementAndPjfAreTriedToActivated_ErrorIsThrown() {

        if(true)
            return;
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            SQX_DB sqxDB = new SQX_DB();

            // Arrange : Create jf,pjf and requirement
            List<SQX_Job_Function__c> jfs = getJobFunctionByNames(new List<String> {'JF_TrainingProgram'});
            
            List<SQX_Personnel__c> personnels = [SELECT Id FROM SQX_Personnel__c WHERE Full_Name__c IN :new List<String> {'PersonnelName1', 'PersonnelName2'}];
            List<SQX_Controlled_Document__c> docs = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Status__c = :SQX_Controlled_Document.STATUS_CURRENT];
            List<SQX_Personnel_Job_Function__c> pjfs = getPJFs(personnels, jfs);
            List<SQX_Requirement__c> reqs = getRequirementsByJFs(jfs, docs);
            
            for (SQX_Personnel_Job_Function__c pjf : pjfs) {
                pjf.Active__c = true;
            }
            
            List<Database.SaveResult> sr1 = sqxDB.continueOnError().op_update(pjfs, new List<Schema.SObjectField> {SQX_Personnel_Job_Function__c.Active__c});
            System.assert(!sr1[0].isSuccess(), 'Personnel job functions cannot be activated for inactive job function');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(sr1[0].getErrors(), 'Personnel Job Function cannot be active for an inactive Job Function.'), 'Expected validation message should be as expected ' + sr1[0].getErrors());
            
            for (SQX_Requirement__c req : reqs) {
                req.Active__c = true;
            }
            
            List<Database.SaveResult> sr2 = sqxDB.continueOnError().op_update(reqs, new List<Schema.SObjectField> {SQX_Requirement__c.Active__c});
            System.assert(!sr2[0].isSuccess(), 'Personnel Job Function cannot be activated for inactive job function');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(sr2[0].getErrors(), 'Requirement cannot be active for an inactive Job Function.'), 'Expected validation message should be as expected ' + sr2[0].getErrors());
        }
    }
    
    /**
     * Given : Create training program (i.e. job function say jfs), personnel job function say pjfs, requirements say reqs, documents say docs, personnels say personells
     * When : Training Program is activated
     * Then : All pjfs and requirements are activated and thus corresponding pending trainings should be generated
     *
     * When : Training Program is deactivated
     * Then : All pjfs and requirements are deactivated and thus corresponding trainings should be obsolete
     */
    public static testmethod void ensureTrainigProgramActivationDeactivationWorks() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            SQX_DB sqxDB = new SQX_DB();

            Test.startTest();
            // Arrange : Create jf,pjf and requirement
            List<SQX_Job_Function__c> jfs = getJobFunctionByNames(new List<String> {'JF_TrainingProgram'});
            
            List<SQX_Personnel__c> personnels = [SELECT Id FROM SQX_Personnel__c WHERE Full_Name__c IN :new List<String> {'PersonnelName1', 'PersonnelName2'}];
            List<SQX_Controlled_Document__c> docs = [SELECT Id FROM SQX_Controlled_Document__c WHERE Document_Status__c = :SQX_Controlled_Document.STATUS_CURRENT];
            List<SQX_Personnel_Job_Function__c> pjfs = getPJFs(personnels, jfs);
            List<SQX_Requirement__c> reqs = getRequirementsByJFs(jfs, docs);
            
            // Assert : initially pjfs and requirements should be inactive
            for (SQX_Personnel_Job_Function__c pjf : pjfs) {
                System.assert(pjf.Active__c == false && pjf.Activation_Date__c == null, 'Expected personnel job function to be inactive initially');
            }
            
            for (SQX_Requirement__c req : reqs) {
               System.assert(req.Active__c == false && req.Activation_Date__c == null, 'Expected requirement to be inactive initially');
            }
        
            // Act : Activate Training Program (job function)
            jfs[0].Active__c = true;
            sqxDB.op_update(jfs, new List<Schema.SObjectField> {SQX_Job_Function__c.Active__c});
            
            // Assert : personnel job function, requirements should be active and hence 4 training records should be generated
            pjfs = getPJFs(personnels, jfs);
            reqs = getRequirementsByJFs(jfs, docs);
            
            // Assert : pjfs and requirements should be active
            for (SQX_Personnel_Job_Function__c pjf : pjfs) {
                System.assert(pjf.Active__c == true && pjf.Activation_Date__c != null, 'Expected personnel job function to be inactive initially');
            }
            
            for (SQX_Requirement__c req : reqs) {
               System.assert(req.Active__c == true && req.Activation_Date__c != null, 'Expected requirement to be inactive initially');
            }
            
            List<SQX_Personnel_Document_Training__c> dts = [SELECT Id, Status__c FROM SQX_Personnel_Document_Training__c 
                                                            WHERE SQX_Personnel__c IN :personnels 
                                                            AND SQX_Controlled_Document__c IN :docs];
            System.assert(dts.size() == 4, 'Expected 4 training records to be generated but is ' + dts.size());
            for (SQX_Personnel_Document_Training__c dt : dts) {
                System.assertEquals(dt.Status__c, SQX_Personnel_Document_Training.STATUS_PENDING, 'Expected pending documents to be generated');
            }
            
            // Act : Deactivate the Training Program
            jfs[0].Active__c = false;
            sqxDB.op_update(jfs, new List<Schema.SObjectField> {SQX_Job_Function__c.Active__c});
            
            // Assert : personnel job function, requirements should be deactivated
            pjfs = getPJFs(personnels, jfs);
            reqs = getRequirementsByJFs(jfs, docs);
            
            Test.stopTest();

            // Assert : pjfs and requirements should be deactivated
            for (SQX_Personnel_Job_Function__c pjf : pjfs) {
                System.assert(pjf.Active__c == false && pjf.Deactivation_Date__c != null, 'Expected personnel job function to be deactivated');
            }
            
            for (SQX_Requirement__c req : reqs) {
               System.assert(req.Active__c == false && req.Deactivation_Date__c != null, 'Expected requirement to be deactivated');
            }
            
            // trainings should be obsolete
            dts = [SELECT Id, Status__c FROM SQX_Personnel_Document_Training__c 
                                                            WHERE SQX_Personnel__c IN :personnels 
                                                            AND SQX_Controlled_Document__c IN :docs];
            for (SQX_Personnel_Document_Training__c dt : dts) {
                System.assertEquals(dt.Status__c, SQX_Personnel_Document_Training.STATUS_OBSOLETE, 'Expected training to be obsolete');
            }
        }
    }

    /**
     * Given : Job function record
     * When : activation and deactivation dates are set through setJobFunctionFieldsValues method of SQX_Finding class
     * Then : activation and deactivation dates should be set
     */
    public static testmethod void ensureActivationAndDeactivationDatesAreSetProperly() {

        // Arrange : Create active job function
        SQX_Job_Function__c jf = new SQX_Job_Function__c(Active__c = true);

        // Act : call setJobFunctionFieldsValues method
        SQX_Job_Function.onJFActivation(jf);

        // Assert : Ensure activation date not to be null and deactivation date should be null
        System.assert(jf.Activation_Date__c != null && jf.Activation_Date__c == System.today(), 'Expected activation date to be set but is ' + jf.Activation_Date__c);
        System.assert(jf.Deactivation_Date__c == null, 'Expected deactivation date to be null but is ' + jf.Deactivation_Date__c);

        // Act : deactivate jf
        jf.Active__c = false;
        SQX_Job_Function.onJFDeactivation(jf);
        
        // Assert : Ensure deactivation date set properly
        System.assert(jf.Activation_Date__c != null && jf.Activation_Date__c == System.today(), 'Expected activation date not to be null but is ' + jf.Activation_Date__c);
        System.assert(jf.Deactivation_Date__c != null && jf.Deactivation_Date__c == System.today(), 'Expected deactivation date to be set but is ' + jf.Deactivation_Date__c);
    }
}