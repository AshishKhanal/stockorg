/**
* Controller for Training Gap Analysis Report
* Uses Personnel_Wrapper and JobFunction_Wrapper inner classes to group first by personnel and then by job function
*/
public with sharing class SQX_Controller_TrainingGapAnalysisReport {
    /**
    * Personnel Job Function object as filter to get Personnel and Job Function filter values from user
    */
    public SQX_Personnel_Job_Function__c filterBy { get; set; }
    
    /**
    * Report data for Training Gap Analysis
    */
    public List<Personnel_Wrapper> reportData { get; set; }
    
    /**
    * SQX_Controller_TrainingGapAnalysisReport Constructor
    */
    public SQX_Controller_TrainingGapAnalysisReport() {
        filterBy = new SQX_Personnel_Job_Function__c();
        reportData = new List<Personnel_Wrapper>();
    }
    
    /**
    * Blank job function to map personnel document training records that are not mapped with any job function in DB for report purpose.
    * Personnel document training is mapped to a job function when that job function is active for both personnel and controlled document of the document training.
    */
    public SQX_Job_Function__c blankJobFunction = new SQX_Job_Function__c( Name = '' );
    
    /**
    * generates report data using filters
    */
    public void generateReport() {
        boolean filterByPersonnel = !String.isBlank(filterBy.SQX_Personnel__c);
        boolean filterByJobFunction = !String.isBlank(filterBy.SQX_Job_Function__c);
        
        // clear reportData
        reportData = new List<Personnel_Wrapper>();

        if (filterByPersonnel || filterByJobFunction) {
            // generate report data when any filter is provided
            
            // variable for filters to be used in dynamic query
            Id personnelIdFilter = null;
            Id jobFunctionIdFilter = null;

            if (filterByPersonnel)
                personnelIdFilter = filterBy.SQX_Personnel__c;
            if (filterByJobFunction)
                jobFunctionIdFilter = filterBy.SQX_Job_Function__c;
            
            // Dynamic query to get active pdjfs
            String pdjfQuery = 'SELECT '
                                + ' Id, Personnel_Id__c, Controlled_Document_Id__c, Job_Function_Id__c, SQX_Personnel_Job_Function__c, SQX_Personnel_Document_Training__c, '
                                + ' Controlled_Document_Number__c, Controlled_Document_Status__c, '
                                + ' Job_Function_Name__c, SQX_Personnel_Job_Function__r.Name, SQX_Personnel_Job_Function__r.Training_Status__c '
                                + ' FROM SQX_Personnel_Document_Job_Function__c '
                                + ' WHERE Is_Archived__c = false'
                                + ' AND SQX_Personnel_Document_Training__c != null' // PDT is null for queued refrehser that are not activated yet
                                + (filterByPersonnel ? ' AND Personnel_Id__c = :personnelIdFilter ' : '')
                                + (filterByJobFunction ? ' AND Job_Function_Id__c = :jobFunctionIdFilter ' : '')
                                + ' ORDER BY Personnel_Id__c, Job_Function_Name__c, SQX_Personnel_Job_Function__r.Training_Status__c DESC, '
                                + ' SQX_Personnel_Document_Training__r.Status__c, Controlled_Document_Name__c, '
                                + ' SQX_Personnel_Document_Training__r.Completion_Date__c DESC, Name';
            
            // get pdjf records from DB using dynamic query
            List<SQX_Personnel_Document_Job_Function__c> pdjfs = (List<SQX_Personnel_Document_Job_Function__c>)Database.query(pdjfQuery);
            
            if (pdjfs.size() > 0) {
                Set<Id> pdtIds = new Set<Id>();
                Set<Id> psnIds = new Set<Id>();
                Map<Id, List<SQX_Personnel_Document_Job_Function__c>> psnPdjfsMap = new Map<Id, List<SQX_Personnel_Document_Job_Function__c>>();
                Map<Id, Map<Id, JobFunction_Wrapper>> psnJfWrapperMap = new Map<Id, Map<Id, JobFunction_Wrapper>>();
                
                for (SQX_Personnel_Document_Job_Function__c pdjf : pdjfs) {
                    pdtIds.add(pdjf.SQX_Personnel_Document_Training__c);
                    psnIds.add(pdjf.Personnel_Id__c);
                    
                    if (!psnPdjfsMap.containsKey(pdjf.Personnel_Id__c)) {
                        psnPdjfsMap.put(pdjf.Personnel_Id__c, new List<SQX_Personnel_Document_Job_Function__c>());
                        psnJfWrapperMap.put(pdjf.Personnel_Id__c, new Map<Id, JobFunction_Wrapper>());
                    }
                    psnPdjfsMap.get(pdjf.Personnel_Id__c).add(pdjf);
                    
                    // create pjf wrapper for grouping records at job function level
                    if (!psnJfWrapperMap.get(pdjf.Personnel_Id__c).containsKey(pdjf.Job_Function_Id__c)) {
                        SQX_Job_Function__c jf = new SQX_Job_Function__c(
                            Id = pdjf.Job_Function_Id__c,
                            Name = pdjf.Job_Function_Name__c
                        );
                        JobFunction_Wrapper jfWrapper = new JobFunction_Wrapper(jf, pdjf.SQX_Personnel_Job_Function__r);
                        psnJfWrapperMap.get(pdjf.Personnel_Id__c).put(pdjf.Job_Function_Id__c, jfWrapper);
                    }
                }
                
                // get all document trainings
                Map<Id, SQX_Personnel_Document_Training__c> pdtMap = new Map<Id, SQX_Personnel_Document_Training__c>([
                    SELECT Id, Name, SQX_Personnel__c, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, Level_of_Competency__c,
                        Optional__c, Due_Date__c, Trainer_Approval_Needed__c, Status__c, Title__c, Document_Number__c, Document_Revision__c,
                        Document_Title__c, Completion_Date__c, User_SignOff_Date__c, Trainer_SignOff_Date__c,
                        SQX_Controlled_Document__r.Document_Status__c
                    FROM SQX_Personnel_Document_Training__c
                    WHERE Id IN :pdtIds
                ]);
                
                // get all personnels
                for (SQX_Personnel__c psn :[SELECT Id, Name, Full_Name__c
                                            FROM SQX_Personnel__c
                                            WHERE Id IN :psnIds
                                            ORDER BY Full_Name__c, Name]) {
                    // list document trainings as per personnel job function
                    for (SQX_Personnel_Document_Job_Function__c pdjf : psnPdjfsMap.get(psn.Id)) {
                        JobFunction_Wrapper jfWrapper = psnJfWrapperMap.get(pdjf.Personnel_Id__c).get(pdjf.Job_Function_Id__c);
                        jfWrapper.documentTrainings.add(pdtMap.get(pdjf.SQX_Personnel_Document_Training__c));
                    }
                    
                    // personnel wrapper for grouping records at personnel level
                    Personnel_Wrapper psnWrapper = new Personnel_Wrapper(psn);
                    psnWrapper.jobFunctions = psnJfWrapperMap.get(psn.Id).values();
                    
                    // add to report data
                    reportData.add(psnWrapper);
                }
            }
        } else {
            // throw error if any filter is not used
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                SQX_Personnel_Job_Function__c.SQX_Personnel__c.getDescribe().getLabel() + ' or '
                + SQX_Personnel_Job_Function__c.SQX_Job_Function__c.getDescribe().getLabel() + ' filter value is required to generate report.'));
        }
    }
    
    /**
    * wrapper class to list document training list per job function of a personnel
    */
    public with sharing class Personnel_Wrapper {
        public SQX_Personnel__c personnel { get; set; }
        public List<JobFunction_Wrapper> jobFunctions { get; set; }
        
        public Personnel_Wrapper(SQX_Personnel__c psn) {
            personnel = psn;
            jobFunctions = new List<JobFunction_Wrapper>();
        }
    }
    
    /**
    * wrapper class to list document training list for a job function
    */
    public with sharing class JobFunction_Wrapper {
        public SQX_Job_Function__c jobFunction { get; set; }
        public SQX_Personnel_Job_Function__c personnelJobFunction { get; set; }
        public List<SQX_Personnel_Document_Training__c> documentTrainings { get; set; }
        
        public JobFunction_Wrapper(SQX_Job_Function__c jf, SQX_Personnel_Job_Function__c pjf) {
            jobFunction = jf;
            personnelJobFunction = pjf;
            documentTrainings = new List<SQX_Personnel_Document_Training__c>();
        }
    }
}