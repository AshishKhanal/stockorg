/**
* Unit tests for validation rule Prevent_Update_On_User_And_Document of Document Training object
*
* @author Dibya Shrestha
* @date 2015/08/04
* 
*/
@IsTest
public class SQX_Test_1324_DTN_ModifyUserOrDocument {

    static Boolean runAllTests = false,
        run_givenModifyUserAndDocumentValues_DocumentTrainingNotSaved = false;
    
    /**
    * This test ensures prevention of modification of user and document fields after creation of a document training
    */
     public static testmethod void givenModifyUserAndDocumentValues_DocumentTrainingNotSaved() {
        if (!runAllTests && !run_givenModifyUserAndDocumentValues_DocumentTrainingNotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser1) {
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().save();
            cDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add document training for standardUser2 and cDoc1
            SQX_Document_Training__c dt1 = new SQX_Document_Training__c(
                SQX_User__c = standardUser2.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.SaveResult saveResult1 = Database.insert(dt1, false);
            
            // change user for document training
            dt1.SQX_User__c = standardUser1.Id;
            
            // ACT: save document training with modified user value
            saveResult1 = Database.update(dt1, false);
            
            //Assert : ensuring document training not to be saved for modified user value
            System.assert(saveResult1.isSuccess() == false,
                'Expected modified user value not to be saved for existing document training.');
            
            // reset user to previous value
            dt1.SQX_User__c = standardUser2.Id;
            
            // change document for document training
            dt1.SQX_Controlled_Document__c = cDoc2.doc.Id;
            
            // ACT: save document training with modified document value
            saveResult1 = Database.update(dt1, false);
            
            //Assert : ensuring document training not to be saved for modified document value
            System.assert(saveResult1.isSuccess() == false,
                'Expected modified document value not to be saved for existing document training.');
        }
    }
}