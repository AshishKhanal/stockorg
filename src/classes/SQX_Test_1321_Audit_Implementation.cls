/*
 * description: test for audit response with action
 */
@IsTest
public class SQX_Test_1321_Audit_Implementation{

    private static Boolean runAllTests = true,
                            run_auditResponseSubmitted_ResponseProcessed=false,
                            run_auditResponseSubmitted_SentForApproval = false,
                            run_auditResponse_ApprovalRequired_Approved = false,
                            run_auditResponse_ApprovalRequired_Rejected = false,
                            run_allAuditActionComplete_AuditCanBeClosed = false,
                            run_allAuditActionNotComplete_AuditCannotBeClosed = false,
                            run_auditResponseSubmitted_WithFindingRequiredTypeResponseRecordType = false,
                            run_assignAuditeeAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation = false,
                            run_assignCAPACoordinatorAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation = false;

    
    /**
    * helper funtion to setup Audit, finding and finding response
    * @param actionApproval set whether action approval is required or not
    **/                      
    public static SQX_Test_Audit arrangeForAuditResponse(User adminUser, boolean implementationApproval, user responseSubmitter){
            /*
            * Arrange : Create Audit with investigation apprval required
            *           Add a finding with investigation and containment required
            *           Add another finding with containment required
            *           Create audit response with investigation and containment for first finding and containment for second finding
            */
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.setPolicy(false, implementationApproval, implementationApproval);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            SQX_Test_Finding auditFinding1 =  audit.addAuditFinding()
                                                .setRequired(true, true, true, true, true) //response, containment, and investigation required
                                                .setApprovals(false, implementationApproval, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();
            SQX_Test_Finding auditFinding2 =  audit.addAuditFinding()
                                                .setRequired(true, true, false, false, false)//response, and containment required
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();

            System.runas(responseSubmitter) {
                SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

                // added audit response
                changeSet.addChanged('AuditResponse-1', 
                                         new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 1'));

                // added finding response 1
                changeSet.addChanged('responseForFinding1-1', 
                                        new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding1.finding.Id,
                                               Response_Summary__c = 'Response for audit')
                                    )
                            .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');

                // added finding response 2
                changeSet.addChanged('responseForFinding2-1',
                                        new SQX_Finding_Response__c( 
                                                SQX_Finding__c = auditFinding2.finding.Id,
                                                Response_Summary__c = 'Response for audit'))
                            .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');

                // added containment in finding 1
                changeSet.addChanged('Containment-1',
                                        new SQX_Containment__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                 Containment_Summary__c = 'Containment for finding1',
                                                 Completion_Date__c = Date.today()) );

                //added  containment for finding 2
                changeSet.addChanged('Containment-2',
                                        new SQX_Containment__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                 Containment_Summary__c = 'Containment for finding2',
                                                 Completion_Date__c = Date.today()) );

                //added  investigation for finding 1
                changeSet.addChanged('Investigation-1',
                                        new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding1'));

                
                //added corrective action plan for investigation in finding 1
                changeSet.addChanged('CorrectiveActionPlan-1',
                                        new SQX_Action_Plan__c(
                                                Plan_Number__c = 'PLAN-01',
                                                Plan_Type__c = 'Corrective',
                                                Description__c = 'Corrective Action Plan Description',
                                                Due_Date__c = Date.Today().addDays(10)) )
                            .addRelation(SQX_Action_Plan__c.SQX_Investigation__c,'Investigation-1');

                //added preventive action plan for investigation in finding 1
                changeSet.addChanged('PreventiveActionPlan-1',
                                        new SQX_Action_Plan__c(
                                                Plan_Number__c = 'PLAN-02',
                                                Plan_Type__c = 'Preventive',
                                                Description__c = 'Preventive Action Plan Description',
                                                Due_Date__c = Date.Today().addDays(10)) )
                            .addRelation(SQX_Action_Plan__c.SQX_Investigation__c,'Investigation-1');

                //added response inlusion of type containment
                changeSet.addChanged('Inclusion-1',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'Containment-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

                //added response inlusion of type investigation
                changeSet.addChanged('Inclusion-2',
                                        new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'Investigation-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');


                //added response inlusion of type containment
                changeSet.addChanged('Inclusion-3',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'Containment-2')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding2-1');

                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'submitresponse');
                params.put('comment', 'Mock comment');
                params.put('purposeOfSignature', 'Mock purposeOfSignature');

                /*
                * Act : Submit audit response for audit
                */
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit reponse');
            }                                   
            

            return audit;
    }

    /**
    * helper funtion to respond audit with the corrective and preventive action
    * @param audit with corrective and preventive action
    * @param user who submits response
    **/  
    public static SQX_Test_Audit arrangeAuditResponseWithAction(SQX_Test_Audit audit, User responseSubmitter, String recordType){  

        System.runas(responseSubmitter) {

            List<SQX_Action__c> actionList = [SELECT Id FROM SQX_Action__c WHERE SQX_Audit__c =: audit.audit.Id];
            System.assertEquals(2, actionList.size(), 'Expectedt to have two actions');

            SQX_Action__c correctiveAction = [SELECT Id, Status__c, Remark__c, Completion_Date__c FROM SQX_Action__c WHERE SQX_Audit__c =: audit.audit.Id AND Plan_Type__c = 'Corrective'];
            SQX_Action__c preventiveAction = [SELECT Id, Status__c, Remark__c, Completion_Date__c FROM SQX_Action__c WHERE SQX_Audit__c =: audit.audit.Id AND Plan_Type__c = 'Preventive'];
            
            correctiveAction.Record_Action__c = SQX_Implementation_Action.STATUS_COMPLETE;
            correctiveAction.Remark__c = 'Corrective Action Completed';
            correctiveAction.Completion_Date__c = Date.Today().addDays(-5);

            preventiveAction.Record_Action__c = SQX_Implementation_Action.STATUS_COMPLETE;
            preventiveAction.Remark__c = 'Preventive Action Completed';
            preventiveAction.Completion_Date__c = Date.Today().addDays(-5);

            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

            // added audit response
            changeSet.addChanged('AuditResponse-2', 
                                     new SQX_Audit_Response__c(
                                           SQX_Audit__c= audit.audit.Id,
                                           Audit_Response_Summary__c ='Audit Response for finding 1'));

            // added finding response 1
            changeSet.addChanged('responseForFinding3-1', 
                                    new SQX_Finding_Response__c(
                                                        Response_Summary__c = 'Response for audit',
                                                        RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Response, recordType))
                                )
                        .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-2');

            changeSet.addChanged(correctiveAction)
                     .addOriginalValue(SQX_Action__c.Record_Action__c, null)
                     .addOriginalValue(SQX_Action__c.Remark__c, null)
                     .addOriginalValue(SQX_Action__c.Completion_Date__c, null);

            changeSet.addChanged(preventiveAction)
                     .addOriginalValue(SQX_Action__c.Record_Action__c, null)
                     .addOriginalValue(SQX_Action__c.Remark__c, null)
                     .addOriginalValue(SQX_Action__c.Completion_Date__c, null);

            //added response inlusion of type action corrective
            changeSet.addChanged('Inclusion-4',
                                    new SQX_Response_Inclusion__c(Type__c = 'Action'))
                        .addRelation(SQX_Response_Inclusion__c.SQX_Action__c,correctiveAction.Id)
                        .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding3-1');

            //added response inlusion of type action preventive
            changeSet.addChanged('Inclusion-5',
                                    new SQX_Response_Inclusion__c(Type__c = 'Action'))
                        .addRelation(SQX_Response_Inclusion__c.SQX_Action__c, preventiveAction.Id)
                        .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding3-1');


            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'submitresponse');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            /*
            * Act : Submit audit response for audit
            */
            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit reponse');

        }

        return audit;

    }
    
    /*
    *   test for actions to be performed on audit response submission when aproval is not required
    */
    public static testmethod void auditResponseSubmitted_ResponseProcessed(){
            if(!runAllTests && !run_auditResponseSubmitted_ResponseProcessed){
                return;
            }
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange and act 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, false, responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                List<SQX_Action__c> actionList = [SELECT Id FROM SQX_Action__c WHERE SQX_Audit__c =: audit.audit.Id];
                System.assertEquals(2, actionList.size(), 'Expectedt to have two actions');

                
            }
    }

    /*
    *   test for actions to be performed on audit response submission when action approval is required
    */
    public static testmethod void auditResponseSubmitted_SentForApproval(){
            if(!runAllTests && !run_auditResponseSubmitted_SentForApproval){
                return;
            }           
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arange : add audit, add findings, and submit response and action to complete all tasks and submit for approval
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//action approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                audit = arrangeAuditResponseWithAction(audit, responseSubmitter, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING);
                
                List<SQX_Audit_Response__c> auditResponse = [Select Id, Submitted_On__c, Submitted_By__c, Response_Approver__c, Approval_Status__c, Status__c from SQX_Audit_Response__c 
                                                        where SQX_Audit__c= :audit.audit.Id];

                /*
                * Approval is required for action, so the audit reponse should go for approval
                */
                System.assertEquals(SQX_Audit_Response.STATUS_IN_APPROVAL, auditResponse[1].Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_IN_APPROVAL+ ' but found ' +auditResponse[1].Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_PENDING, auditResponse[1].Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_PENDING+ ' but found ' +auditResponse[1].Approval_Status__c);

                SQX_Finding_Response__c findingResponses = [SELECT Approval_Status__c, Status__c FROM SQX_Finding_Response__c where 
                                                                                SQX_Audit_Response__c =:auditResponse[1].Id ];

              

                /*
                * Test that finding response status and approval status should be synced with audit response 
                * when changes in these field are detected in audit response
                */
                System.assertEquals(findingResponses.Status__c, auditResponse[1].Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses.Status__c + ' and audit response status is '+ auditResponse[1].Status__c);
                System.assertEquals(findingResponses.Approval_Status__c, auditResponse[1].Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses.Approval_Status__c + ' and audit response status is '+ auditResponse[1].Approval_Status__c);

            }
    }

    /*
    *   test for status check when audit response is approved
    */
    public static testmethod void auditResponse_ApprovalRequired_Approved(){

            if(!runAllTests && !run_auditResponse_ApprovalRequired_Approved){
                return;
            }  
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);


            
            System.runas(stdUser){
                /*
                * Arange : add audit, add findings, and submit response and action to complete all tasks and approve audit response
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true,responseSubmitter);//action approval not required

                /*
                * Assert: Must have only one audit response
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                audit = arrangeAuditResponseWithAction(audit, responseSubmitter, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING);
                System.runas(adminUser){
                    List<SQX_Audit_Response__c> auditResponse = [Select Id from SQX_Audit_Response__c 
                                                                    where SQX_Audit__c= :audit.audit.Id];

                    /*
                    * Act : approve the audit response 
                    */
                    Map<String, String> params = new Map<String, String>();
                    params.put('nextAction', 'Approve');
                    params.put('recordID', auditResponse[1].Id);
                    Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                    System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                    'Expected to successfully Approve '+ auditId);

                    auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                    /*
                    * Assert : Response should be approved 
                    */
                    System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse[1].Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse[1].Status__c);
                    System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_APPROVED, auditResponse[1].Approval_Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_APPROVED+ ' but found ' +auditResponse[1].Approval_Status__c);

                }

                
            }
    }

    /*
    *   test for status check when audit response is rejected
    */
    public static testmethod void auditResponse_ApprovalRequired_Rejected(){
            if(!runAllTests && !run_auditResponse_ApprovalRequired_Rejected){
                return;
            }  
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arange : add audit, add findings, and submit response and action to complete all tasks and reject audit response
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//action approval not required

                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                audit = arrangeAuditResponseWithAction(audit, responseSubmitter, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING);

                List<SQX_Audit_Response__c> auditResponse = [Select Id from SQX_Audit_Response__c 
                                                                where SQX_Audit__c= :audit.audit.Id];
                 /*
                * Act : Reject the response
                */
                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'Reject');
                params.put('recordID', auditResponse[1].Id);
                Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                'Expected to successfully Reject '+ auditId);

                auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                /*
                * Assert: Response should be approved 
                */
                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse[1].Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse[1].Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_REJECTED, auditResponse[1].Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_REJECTED+ ' but found ' +auditResponse[1].Approval_Status__c);

            }
    }


    /*
    *   test for audit closure when all related action are not complete
    */
    public static testmethod void allAuditActionNotComplete_AuditCannotBeClosed(){
            if(!runAllTests && !run_allAuditActionNotComplete_AuditCannotBeClosed){
                return;
            }  
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            
            System.runas(adminUser){
                /*
                * Arrange : add audit, add findings, and submit response and action but do not approve action
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//action approval not required

                List<SQX_Finding__c> auditFindings = [SELECT Status__c from SQX_Finding__c Where SQX_Audit__c= :audit.audit.Id];

                System.assertEquals(2, auditFindings.size(), 'Expected 2 findings added in Audit');

                audit = arrangeAuditResponseWithAction(audit, responseSubmitter, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING);
                /*
                * Act :  set audit to completion
                */
                audit.setStatus(SQX_Audit.STATUS_CLOSE);

                /*
                * Assert :  closure should be unsuccessful
                */
                Database.SaveResult auditClosureResult = Database.update(audit.audit, false);

                System.assertEquals(false, auditClosureResult.isSuccess(), 'Expected closure to be failed');

            }

    }

    /*
    *   test for when the response type is with record type finding required finding should be provided
    */
    public static testmethod void auditResponseSubmitted_WithFindingRequiredTypeResponseRecordType(){
            if(!runAllTests && !run_auditResponseSubmitted_WithFindingRequiredTypeResponseRecordType){
                return;
            }           
            /* Arrange: Add the respone without finding */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            
            System.runas(adminUser){
                SQX_Finding_Response__c response = new SQX_Finding_Response__c();
                response.Response_Summary__c = 'Response Summary';
                response.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Response , SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITH_FINDING);

                //Act: Save response without finding
                try{
                   insert response;

                } 
                catch(Exception e){
                    //Assert: Response should not be saved
                    Boolean expectedExceptionThrown =  e.getMessage().contains('Only findings that are open can be responded to') ? true : false;
                    system.assertEquals(expectedExceptionThrown,true);       
                } 
               
            }
    }

    /**
     * Given: An audit is created
     * When: Imlementation is created with assignee
     * Then: Implementation owner to be creator of implementation
     *
     * Given: An audit is created
     * When: Implementation is created without assignee
     * Then: Auditee Contact of the audit to be owner of the implementation
     * @date: 2016-06-08
     * @story: [SQX-2327]
     * @author: manoj thapa
    */
    public static testmethod void assignAuditeeAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation(){
        if (!runAllTests && !run_assignAuditeeAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation) {
            return;
            
        }
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User stdUser1 = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User stdUser2 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);

        System.runas(stdUser1){
            // Arrange : an audit is created 

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();

            audit.audit.SQX_Auditee_Contact__c = stdUser2.Id;
            audit.save();

            // Act 1:Implementation is created with assignee
            SQX_Action__c implementationWithAssignee = new SQX_Action__c(
                                                                Plan_Number__c = 'PLN-000',
                                                                Plan_Type__c = 'Corrective',
                                                                SQX_User__c =  stdUser2.Id,
                                                                SQX_Audit__c = audit.audit.Id,
                                                                Description__c = 'Implementation with assignee',
                                                                Due_Date__c = Date.Today().addDays(10),
                                                                Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW);

            insert implementationWithAssignee;

            implementationWithAssignee = [SELECT Id, SQX_User__c FROM SQX_Action__c WHERE Id =: implementationWithAssignee.Id];

            // Assert 1:Implementation owner to be assignee of implementation
            System.assertEquals(stdUser2.Id, implementationWithAssignee.SQX_User__c, 'Expected the creator of implementation to be the owner');

            // Act 2: Implementation is created without assignee
            SQX_Action__c implementationWithOutAssignee = new SQX_Action__c(
                                                                Plan_Number__c = 'PLN-000',
                                                                Plan_Type__c = 'Corrective',
                                                                SQX_Audit__c = audit.audit.Id,
                                                                Description__c = 'Implementation with assignee',
                                                                Due_Date__c = Date.Today().addDays(10),
                                                                Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW);

            insert implementationWithOutAssignee;

            implementationWithOutAssignee = [SELECT Id, SQX_User__c FROM SQX_Action__c WHERE Id =: implementationWithOutAssignee.Id];

            // Assert 2: Auditee Contact of the audit to be owner of the implementation
     
            System.assertEquals(audit.audit.SQX_Auditee_Contact__c, implementationWithOutAssignee.SQX_User__c, 'Expected the auditee contact of audit to be the owner');


        }

    }   

    /**
     * Given: A CAPA is created
     * When: Imlementation is created with assignee
     * Then: Implementation owner to be creator of implementation
     *
     * Given: A CAPA is created
     * When: Implementation is created without assignee
     * Then: CAPA Coordinator of the CAPA to be owner of the implementation
     * @date: 2016-06-20
     * @story: [SQX-2327]
     * @author: manoj thapa
    */
    public static testmethod void assignCAPACoordinatorAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation(){
        if (!runAllTests && !run_assignCAPACoordinatorAsActionOwnerWhenAssigneeIsEmpty_WhileCreatingImplementation) {
            return;
            
        }
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User stdUser1 = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User stdUser2 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);

        System.runas(stdUser1){
            // Arrange : a CAPA is created 

            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.CAPA.SQX_CAPA_Coordinator__c = stdUser2.Id;
            capa.save();
            //capa.save();

            // Act 1:Implementation is created with assignee
            SQX_Action__c implementationWithAssignee = new SQX_Action__c(
                                                                Plan_Number__c = 'PLN-000',
                                                                Plan_Type__c = 'Corrective',
                                                                SQX_User__c =  stdUser2.Id,
                                                                SQX_CAPA__c = capa.CAPA.Id,
                                                                Description__c = 'Implementation with assignee',
                                                                Due_Date__c = Date.Today().addDays(10),
                                                                Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW);

            insert implementationWithAssignee;

            implementationWithAssignee = [SELECT Id, SQX_User__c FROM SQX_Action__c WHERE Id =: implementationWithAssignee.Id];

            // Assert 1:Assignee to be the owner  of implementation
            System.assertEquals(stdUser2.Id, implementationWithAssignee.SQX_User__c, 'Expected the creator of implementation to be the owner');

             // Act 2: Implementation is created without assignee
            SQX_Action__c implementationWithOutAssignee = new SQX_Action__c(
                                                                Plan_Number__c = 'PLN-000',
                                                                Plan_Type__c = 'Corrective',
                                                                SQX_CAPA__c = capa.CAPA.Id,
                                                                Description__c = 'Implementation without assignee',
                                                                Due_Date__c = Date.Today().addDays(10),
                                                                Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW);

            insert implementationWithOutAssignee;

            implementationWithOutAssignee = [SELECT Id, SQX_User__c FROM SQX_Action__c WHERE Id =: implementationWithOutAssignee.Id];

            // Assert 2: CAPA Coordinator to be owner of the implementation
     
            System.assertEquals(capa.CAPA.SQX_CAPA_Coordinator__c, implementationWithOutAssignee.SQX_User__c, 'Expected CAPA Coordinator to be the asignee');
        }  
    }  

  
}