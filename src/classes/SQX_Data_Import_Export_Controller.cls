/**
* Class contains methods to perform data import/export operations
*/
public with sharing class SQX_Data_Import_Export_Controller {

    // This map will ensure that when the records are being imported/deserialized, existing records are updated and new ones are inserted
    // Key => sobjecttype
    // Value => Comma separated sobjectfields that will act as a combo for uniquely identifying the sobject record
    final static Map<String, String> sobjectUniqueFieldsMap = new Map<String, String>
    {
        'compliancequest__SQX_Task__c' => 'Name',
        'compliancequest__SQX_Task_Question__c' => 'Name, compliancequest__SQX_Task__c',
        'compliancequest__SQX_Answer_Option__c' => 'Name, compliancequest__Question__c',
        'compliancequest__SQX_Answer_Option_Attribute__c' => 'Name, compliancequest__SQX_Answer_Option__c'
    };


    /**
     * Method to export/serialize given set of records with the relationships specified
     * @param recordIds record ids of parent object
     * @param relationsipsToFollow list of child relationships to follow when retrieving the record
     * @return Serialized set of records (JSON string based on SObjectDataLoader.RecordsBundle type)
     */
    @AuraEnabled
    public static String export(List<Id> recordIds, List<String> relationshipsToFollow) {
        try {

            SObjectDataLoader.SerializeConfig cfg = new SObjectDataLoader.SerializeConfig();
            if(relationshipsToFollow.size() > 0) {

                Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

                for(String relationship : relationshipsToFollow) {
                    if(relationship.contains('.')) {
                        String childSObject = relationship.split('\\.')[0];
                        String relField = relationship.split('\\.')[1];

                        if(globalDescribe.containsKey(childSObject)) {
                            cfg.followChild(globalDescribe.get(childSObject).getDescribe().fields.getMap().get(relField));
                        }
                    }
                }
            }

            return SObjectDataLoader.serialize(new Set<Id>(recordIds), cfg);
        }
        catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * Method to import/deserialize records into the current org
     * @param jsonData serialized records (JSON string based on SObjectDataLoader.RecordsBundle type)
     * @return Comma-separated list of records inserted
     */
    @AuraEnabled
    public static String import(String jsonData) {
        try {
            return String.join(new List<Id> (SObjectDataLoader.deserialize(jsonData, new SQX_RelationshipResolver(), sobjectUniqueFieldsMap)), ',');
        } catch(Exception ex) {
            AuraHandledException auraEx = new AuraHandledException(ex.getMessage());
            auraEx.setMessage(ex.getMessage());
            throw auraEx;
        }
    }

    // cache holds records by sobjecttype
    // the inner map is of Record name and the corresponding list of records with that name
    private static Map<SObjectType, Map<String, List<SObject>>> recordsCache = new Map<SObjectType, Map<String, List<SObject>>>();

    /**
     *  This class will be used to resolve dependencies(relationships) that could not be resolved by SObjectDataLoader
     *  It basically tries to resolve them by searching for records of the same name in the org
     *  Throws error if no such dependencies are found
     */
    private class SQX_RelationshipResolver implements SObjectDataLoader.IDeserializeCallback {

        /**
        * Used to attempt to resolve references not resolved but required to insert records
        **/
        public void unresolvedReferences(Schema.SObjectType sObjectType, List<SObjectDataLoader.UnresolvedReferences> unresolvedReferences) {

            Map<SObjectType, Set<String>> namesToLookForByObjType = new Map<SObjectType, Set<String>>();

            Map<SObject, List<SObjectField>> unResolvedFieldsBySObject = new Map<SObject, List<SObjectField>>();

            // first get all the unresolved fields and group them by record for bulkified query
            for (SObjectDataLoader.UnresolvedReferences unresolvedReference : unresolvedReferences) {

                SObject newRecord = unresolvedReference.Record;

                List<SObjectField> unResolvedFields = new List<SObjectField>();

                for(Schema.SObjectField sObjectField : unresolvedReference.References) {

                    // we do not care about not being able to resolve standard fields like 'Owner', 'CreatedBy', 'LastModifiedBy', etc
                    // these fields are anyways not included in the record being inserted
                    if(sObjectField.getDescribe().isCustom()) {

                        String relField = sObjectField.getDescribe().getRelationshipName();

                        SObject relatedObject = newRecord.getSObject(relField);

                        if(relatedObject != null) {

                            SObjectType relatedObjectType = relatedObject.getSObjectType();

                            DescribeSObjectResult relatedObjectDesc = relatedObject.getSObjectType().getDescribe();

                            if(relatedObjectDesc.isQueryable()) {

                                String relatedRecordName = (String) relatedObject.get('Name');

                                if(!String.isBlank(relatedRecordName)) {

                                    if(!namesToLookForByObjType.containsKey(relatedObjectType)) {
                                        namesToLookForByObjType.put(relatedObjectType, new Set<String>());
                                    }

                                    namesToLookForByObjType.get(relatedObjectType).add(relatedRecordName);

                                    unResolvedFields.add(sObjectField);
                                }

                            }
                        }
                    }

                }

                unResolvedFieldsBySObject.put(newRecord, unResolvedFields);
            }

            // look for matching records in the org
            for(SObjectType relatedObjType : namesToLookForByObjType.keySet()) {
                
                Set<String> namesToLookFor = namesToLookForByObjType.get(relatedObjType);
              
                if(recordsCache.containsKey(relatedObjType)) {
                    Map<String, List<SObject>> sobjectsByName = recordsCache.get(relatedObjType);
                    namesToLookFor.removeAll(sobjectsByName.keySet());
                }

                if(namesToLookFor.size() > 0) {
                    String query = 'SELECT Id, Name FROM ' + relatedObjType.getDescribe().getName() + ' WHERE Name IN: namesToLookFor';

                    for(SObject recordInOrg : Database.query(query)) {
                        String recordName = (String) recordInOrg.get('Name');

                        Map<String, List<SObject>> sobjectsByName = recordsCache.get(relatedObjType);

                        if(sobjectsByName == null) {
                            sobjectsByName = new Map<String, List<SObject>>();
                        }

                        if(!sobjectsByName.containsKey(recordName)) {
                            sobjectsByName.put(recordName, new List<SObject>());
                        }

                        sobjectsByName.get(recordName).add(recordInOrg);
                        
                        recordsCache.put(relatedObjType, sobjectsByName);

                    }
                }
            }

            // update the id of the unresolved references with the id of the matching record
            for(SObject newRecord : unResolvedFieldsBySObject.keySet()) {

                for(SObjectField unResolvedField : unResolvedFieldsBySObject.get(newRecord)) {

                    String relField = unResolvedField.getDescribe().getRelationshipName();

                    SObject relatedObject = newRecord.getSObject(relField);

                    SObjectType relatedObjectType = relatedObject.getSObjectType();

                    String relatedRecordName = (String) relatedObject.get('Name');

                    List<SObject> matchingRecordsInOrg = new List<SObject>();
                    if(recordsCache.containsKey(relatedObjectType)) {
                        Map<String, List<SObject>> sobjectsByName = recordsCache.get(relatedObjectType);
                        if(sobjectsByName.containsKey(relatedRecordName)) {
                            matchingRecordsInOrg = sobjectsByName.get(relatedRecordName);
                        }
                    }

                    if(matchingRecordsInOrg.size() == 1) {
                        newRecord.put(unResolvedField, matchingRecordsInOrg.get(0).get('Id'));
                        newRecord.putSObject(relField, matchingRecordsInOrg.get(0));
                    } else {
                        throw new SQX_ApplicationGenericException(
                                            String.format(Label.CQ_ERR_MSG_DATA_IMPORT_RESOLVE_FAILED,
                                            new List<String> { relField, newRecord.getSObjectType().getDescribe().getName(), String.valueOf(matchingRecordsInOrg.size()), relatedRecordName }));
                    }
                }

            }
        }
    }

}