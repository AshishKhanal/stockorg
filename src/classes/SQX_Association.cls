/***
* class to hold the methods for the actions performed on trigger
*/
public with sharing class SQX_Association{

    /**
     * This method computes the unique key that can be used to identify the active association
     * @param partId the part which is related to the association
     * @param revision the revision which is related to the association
     * @param processId the Id of the process/service related to the association
     * @param partFamilyId the Id of the part family related to the association
     */
    public static String getActiveAssociationKey(Id partId, String revision, Id processId, Id partFamilyId) {
        String key = (partId  != null ? String.valueOf(partId ) : '')
                    + (revision != null ? String.valueOf(revision) : '')
                    + (partFamilyId  != null ? String.valueOf(partFamilyId) : '')
                    + (processId  != null ? String.valueOf(processId) : '');

        return key;
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
      * Default constructor for this class, that accepts old and new map from the trigger
      * @param newAssociations equivalent to trigger.New in salesforce
      * @param oldMap equivalent to trigger.OldMap in salesforce
      */
        public Bulkified(List<SQX_Association__c> newAssociations, Map<Id,SQX_Association__c> oldMap){
            
            super(newAssociations, oldMap);
        }
        
        /**
        * ensures that the combination in the association is unique
        * for doc with current and pre-expire status, combination is the sum of part + rev + part family + process
        * for doc with other status, combination is the sum of inspection criteria + part + rev + part family + process
        */
        public Bulkified ensureControlledDocHaveUniqueAssociation(){
            
            this.resetView();
            List<SQX_Association__c> associationList = (List<SQX_Association__c>) this.view;
            
            if(associationList.size() > 0){
                combineAllAssociationFields(associationList);
            }
            return this;
        }

        /**
         * prevent modification of association when its related inspection criteria is in approval
         */
        public Bulkified preventModificationOfAssociationOnApproval(){
            
            this.resetView();
            List<SQX_Association__c> associationList = (List<SQX_Association__c>) this.view;

            if(associationList.size() > 0){
                Set<Id> docIds = getIdsForField(associationList, Schema.SQX_Association__c.SQX_Inspection_Criteria__c);
                Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Approval_Status__c FROM SQX_Controlled_Document__c WHERE Id IN: docIds]);

                for(SQX_Association__c association : associationList){
                    if((docMap.get(association.SQX_Inspection_Criteria__c).Approval_Status__c == SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL ||
                        docMap.get(association.SQX_Inspection_Criteria__c).Approval_Status__c == SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL) && 
                        !SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR)){
                        association.addError(Label.SQX_ERR_MSG_PREVENT_ASSOCIATION_MODIFICATION);
                    }
                }
            }
            return this;
        }
    }

    /*
     * method to combine all the fields of the association to get unique string used for checking uniqueness of association
     */
    public static void combineAllAssociationFields(List<SQX_Association__c> associationList){
        Set<Id> docIds = new SQX_BulkifiedBase().getIdsForField(associationList, Schema.SQX_Association__c.SQX_Inspection_Criteria__c);
        Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id,SQX_Controlled_Document__c>([SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c WHERE Id IN: docIds]);
        
        for(SQX_Association__c association : associationList){
            association.Uniqueness_Constraint__c = '';
            if(!(docMap.get(association.SQX_Inspection_Criteria__c).Document_Status__c == SQX_Controlled_Document.STATUS_CURRENT ||
               docMap.get(association.SQX_Inspection_Criteria__c).Document_Status__c == SQX_Controlled_Document.STATUS_PRE_EXPIRE)){
                association.Uniqueness_Constraint__c = association.SQX_Inspection_Criteria__c != null ? String.valueOf(association.SQX_Inspection_Criteria__c) : '';
               }

            association.Uniqueness_Constraint__c =  association.Uniqueness_Constraint__c +
                getActiveAssociationKey(association.SQX_Part__c, association.Rev__c, association.SQX_Process__c, association.SQX_Part_Family__c);
        }
    }
}