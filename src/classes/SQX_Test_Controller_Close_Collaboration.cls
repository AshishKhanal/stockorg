/**
*   @description : Test class for SQX_Controller_Close_Collaboration
*   @author : Piyush Subedi
*   @date : 2016/11/29
*   @story : 2650
*/
@isTest
public class SQX_Test_Controller_Close_Collaboration{

    static Boolean runAllTests = true,
                    run_givenCollaborationGroupReadyToBeClosedWithKeepChangesFlagSetTrue_WhenUserClicksCloseCollaboration_ThenGroupShouldbeArchivedAndContentShouldBeCheckedIntoControlledDocument = true;
    
    @testSetup
    public static void commonSetup() {
        // add required users       
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(standardUser){

            // creating primary content for controlled document record
            ContentVersion cv = new ContentVersion();
            cv.Title = 'Cv#1';
            cv.PathOnClient = 'MOCK PATH';
            cv.VersionData = Blob.valueOf('MOCK CONTENT');            
            insert cv;

            // creating a controlled document record
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document().save();
            tstObj.doc.Content_Reference__c = [SELECT ContentDocumentId FROM ContentVersion LIMIT 1].ContentDocumentId;
                                    
            // creating a group
            CollaborationGroup grp = new CollaborationGroup();
            grp.Name = 'Test Collab Group';
            grp.OwnerId = standardUser.Id;
            grp.CollaborationType = SQX_SF_Collaboration_Provider.COLLABORATION_TYPE_PRIVATE;
            insert grp;

            //creating a group member
            CollaborationGroupMember grpMember = new CollaborationGroupMember();
            grpMember.MemberId = adminUser.Id;
            grpMember.CollaborationGroupId = grp.Id;
            grpMember.CollaborationRole = SQX_SF_Collaboration_Provider.COLLABORATION_MEMBER_ROLE_ADMIN;
            insert grpMember;

            // creating a clone of the primary content, which will be added in the group
            ContentVersion cv1 = new ContentVersion();
            cv1.Title = 'Cv#1_Clone';
            cv1.PathOnClient = 'MOCK PATH';
            cv1.VersionData = cv.VersionData;
            cv1.Origin = 'H';
            insert cv1;

            // adding content to group
            FeedItem feed = new FeedItem();
            feed.RelatedRecordId = cv1.Id;
            feed.Type = 'ContentPost';
            feed.Title = cv1.Title;
            feed.ParentId = grp.Id;
            insert feed;
                        
            // adding group id to a controlled record
            tstObj.doc.Collaboration_Group_Id__c = grp.Id;
            tstObj.doc.Collaboration_Group_Content_Id__c = [SELECT ContentDocumentId FROM ContentVersion WHERE Title = 'Cv#1_Clone' LIMIT 1].ContentDocumentId;
            tstObj.doc.Checked_Out__c = true;
            tstObj.doc.SQX_Checked_Out_By__c = standardUser.Id;
            tstObj.doc.is_Locked__c = true;
            tstObj.doc.Checked_Out_On__c = Date.today();
            tstObj.save();

        }
    }


    /**
    *   Given a collaboration group ready to be closed with changes to be checked in
    *   When user clicks 'Close Collaboration' button
    *   Then group should be archived and the new content must be uploaded as a new version to the existing primary content of the record
    */
    static testmethod void givenCollaborationGroupReadyToBeClosedWithKeepChangesFlagSetTrue_WhenUserClicksCloseCollaboration_ThenGroupShouldbeArchivedAndContentShouldBeCheckedIntoControlledDocument(){
        
        if(!runAllTests && !run_givenCollaborationGroupReadyToBeClosedWithKeepChangesFlagSetTrue_WhenUserClicksCloseCollaboration_ThenGroupShouldbeArchivedAndContentShouldBeCheckedIntoControlledDocument){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : get a controlled document which has been checked out for collaboration
            
            SQX_Controlled_Document__c doc = [SELECT Collaboration_Group_Id__c FROM SQX_Controlled_Document__c LIMIT 1];                      
            CollaborationGroup grp = [SELECT Id, Name, OwnerId, isArchived FROM CollaborationGroup WHERE Id = :doc.Collaboration_Group_Id__c];

            SQX_Controller_Close_Collaboration ext = new SQX_Controller_Close_Collaboration(grp.Id);
            ext.KeepChanges = true;
            
            // Act : close collaboration group

            Test.startTest();

            PageReference pr = ext.closeCollaboration();                        

            Test.stopTest();

            // Assert : group should be closed with content checked in

            doc = [SELECT Checked_Out__c, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = :doc.Id];                      
            // assert if checked out field is cleared in controlled doc
            System.assert(!doc.Checked_Out__c, 'Expected checked out field to be false but found ' + doc.Checked_Out__c);
          

            List<ContentVersion> cv = [SELECT ContentDocumentId FROM ContentVersion WHERE ContentDocumentId = :doc.Content_Reference__c];            
            // assert if primary content has been checked in
            System.assert(cv.size() > 1, 'Expected new content to be checked in but no new version found');
            
            // assert is feed post has been generated regarding the closure of the group
            CollaborationGroupFeed lastFeed = [SELECT Body FROM CollaborationGroupFeed WHERE ParentId = :grp.Id ORDER BY Title DESC LIMIT 1];
            System.assertEquals( Label.CQ_UI_Feed_Collaboration_Group_Closed.replace('{GROUP_NAME}', grp.Name), lastFeed.Body );

            // asssert if group is archived
            grp = [SELECT isArchived FROM CollaborationGroup WHERE Id = :grp.Id];
            System.assert(grp.isArchived, 'Expected group to be archived but the group has not been archived');
            
        }
    }

    /**
    *   Given a collaboration group ready to be closed with changes to be checked in
    *   When user (not who checked out a document) clicks 'Close Collaboration' button
    *   Then group should not be archived
    */
    static testmethod void givenCollaborationGroupReadyToBeClosedWithKeepChangesFlagSetTrue_WhenUserClicksCloseCollaboration_ThenGroupShouldNotBeArchived(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){

            // Arrange : get a controlled document which has been checked out for collaboration
            
            SQX_Controlled_Document__c doc = [SELECT Collaboration_Group_Id__c FROM SQX_Controlled_Document__c LIMIT 1];                      
            CollaborationGroup grp = [SELECT Id, Name, OwnerId, isArchived FROM CollaborationGroup WHERE Id = :doc.Collaboration_Group_Id__c];

            SQX_Controller_Close_Collaboration ext = new SQX_Controller_Close_Collaboration(grp.Id);
            ext.KeepChanges = true;
            
            // Act : close collaboration group

            Test.startTest();

            PageReference pr = ext.closeCollaboration();

            Test.stopTest();


            // Assert: group is unarchived (negative test)
            grp = [SELECT isArchived FROM CollaborationGroup WHERE Id = :grp.Id];
            System.assert(!grp.isArchived, 'Expected group should not be archived, when we get validation errors.');
            
        }
    }

}