/**
*/
@IsTest
public class SQX_Test_1136_AuditProgram {
    static boolean run_AllTests = true,
                   run_givenAuditProgramHasAuditsInApproval_WhenAPIsVoided_AuditIsRecalled = false,
                   run_givenAuditProgramIsClosed_NoNewAuditsCanBeAdded = false,
                   run_givenAuditProgramIsDraft_NewAuditsCanBeAdded = false,
                   run_givenAuditProgramIsVoid_NewAuditCannotBeAdded = false;
        
    /**
    * Given Audit Program Has Audits In Approval, It can't be voided 
    * [SQX-1350]
    */         
    public testmethod static void givenAuditProgramHasAuditsInApproval_WhenAPIsVoided_AuditIsRecalled(){
        if(!run_AllTests && !run_givenAuditProgramHasAuditsInApproval_WhenAPIsVoided_AuditIsRecalled){
            return;
        }

        User stdUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        System.runAs(stdUser1){
            //Arrange: Create an audit program with audit in approval
            SQX_Test_Audit_Program program = new SQX_Test_Audit_Program().save();

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.setStage(SQX_Audit.STAGE_PLAN_APPROVAL);
            audit.includeInProgram(false, program.program);
            audit.audit.End_Date__c = Date.Today();
            audit.save();

            //Act: Try voiding the audit program
            program.setStatus(SQX_Audit_Program.STATUS_VOID);
            Database.SaveResult saveResult = Database.update(program.program, false);
            //Assert: Save did go through and the audit was also voided.
            System.assertEquals(true, saveResult.isSuccess());

            System.assertEquals(SQX_Audit.STATUS_VOID, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit.audit.Id].Status__c);
        }
    }

    // when Audit Program's status is Closed
    public static testmethod void givenAuditProgramIsClosed_NoNewAuditsCanBeAdded(){
        if(!run_AllTests && !run_givenAuditProgramIsClosed_NoNewAuditsCanBeAdded){
            return;
        }


        User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            
        System.runas(stdUser){
    
            //Arrange: Setup the program, and set the audit as closed.
            SQX_Audit_Program__c program1 = new SQX_Audit_Program__c();
            program1.Unique_Name__c = 'uniq-01'; 
            program1.Status__c = SQX_Audit_Program.STATUS_CLOSED;
            program1.Objective__c = 'This is test with Audit program status closed';
            program1.Title__c = 'Test';
    
            insert program1;

            // Act: Attempt to Insert a Audit, with Audit Program as program1.Id whose status is closed.  
            integer randomNumber = (integer)(Math.random() * 1000000);

            SQX_Department__c department = new SQX_Department__c();
            SQX_Division__c division = new SQX_Division__c();

            System.runas(user){

                department.Name = 'Department' + randomNumber;

                new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                        Schema.SQX_Department__c.Name
                    } ) ;

                division.Name = 'Division' + randomNumber;

                new SQX_DB().op_insert(new List<SQX_Division__c> {division}, new List<Schema.SObjectField>{
                        Schema.SQX_Division__c.Name
                    } ) ;
            }

            SQX_Audit__c audit = new SQX_Audit__c();
            audit.Status__c = SQX_Audit.STATUS_DRAFT;
            audit.Stage__c = SQX_Audit.STAGE_PLAN;
            audit.Lead_Auditor__c = 'Auditor-1';
            audit.Title__c = 'RandomTitle';
            audit.Audit_Type__c = 'Internal';
            audit.Audit_Category__c = 'ISO';
            audit.Start_Date__c = Date.Today();
            audit.SQX_Division__c = division.Id;
            audit.SQX_Department__c = department.Id;
            audit.SQX_Audit_Program__c = program1.Id;

            Database.SaveResult result1 = Database.insert(audit, false);

            // Assert: The save was not successful because Audit Program's status is Closed 
            System.assertEquals(false, result1.isSuccess());
        }

    }


    // when Audit Program's status is Draft
    public static testmethod void givenAuditProgramIsDraft_NewAuditsCanBeAdded(){
        if(!run_AllTests && !run_givenAuditProgramIsDraft_NewAuditsCanBeAdded){
            return;
        }

        User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            
        
        System.runas(stdUser){

            //Arrange: Setup the program, and set the audit as plan.
            SQX_Audit_Program__c program2 = new SQX_Audit_Program__c();
            program2.Unique_Name__c = 'uniq-02';
            program2.Status__c = SQX_Audit_Program.STATUS_DRAFT;
            program2.Objective__c = 'This is test with Audit program status not closed';
            program2.Title__c = 'Test';

            insert program2;

            // Act: Attempt to Insert a Audit, with Audit Program as program2.Id whose status is plan(i.e not closed). 
            integer randomNumber = (integer)(Math.random() * 1000000);

            SQX_Department__c department = new SQX_Department__c();
            SQX_Division__c division = new SQX_Division__c();
            System.runas(user){

                department.Name = 'Department' + randomNumber;

                new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                        Schema.SQX_Department__c.Name
                    } ) ;

                division.Name = 'Division' + randomNumber;

                new SQX_DB().op_insert(new List<SQX_Division__c> {division}, new List<Schema.SObjectField>{
                        Schema.SQX_Division__c.Name
                    } ) ;
            }

            SQX_Audit__c audit = new SQX_Audit__c();
            audit.Status__c = SQX_Audit.STATUS_DRAFT;
            audit.Stage__c = SQX_Audit.STAGE_PLAN;
            audit.Lead_Auditor__c = 'Auditor-1';
            audit.Title__c = 'RandomTitle';
            audit.Audit_Type__c = 'Internal';
            audit.Audit_Category__c = 'ISO';
            audit.Start_Date__c = Date.Today();
            audit.SQX_Division__c = division.Id;
            audit.Org_Division__c = 'Sample Division';
            audit.SQX_Department__c = department.Id;
            audit.SQX_Auditee_Contact__c = stdUser.Id;

            Database.SaveResult result2 = Database.insert(audit, false);

            // Assert: The save was successful because Audit Program's status is draft 
            System.assertEquals(true, result2.isSuccess());
        }
    }

    /**
    * @description: Given Audit Program is voided, then audit cannot be added
    * @author: Anish Shrestha
    * @date: 2015/07/22 
    * @sqx-story: [SQX-1350]
    */         
    public testmethod static void givenAuditProgramIsVoid_NewAuditCannotBeAdded(){
        if(!run_AllTests && !run_givenAuditProgramIsVoid_NewAuditCannotBeAdded){
            return;
        }

        User stdUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        System.runAs(stdUser1){
            //Arrange: Create an audit program  and audit which is in draft
            SQX_Test_Audit_Program program = new SQX_Test_Audit_Program().setStatus(SQX_Audit_Program.STATUS_DRAFT).save();

            //Act 1: Save audit with the audit program is in draft
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).setStatus(SQX_Audit.STATUS_DRAFT)
                                                       .setStage(SQX_Audit.STAGE_PLAN)
                                                       .includeInProgram(false, program.program)
                                                       .save();

            List<SQX_Audit__c> newAudit =  [SELECT Id FROM SQX_Audit__c WHERE SQX_Audit_Program__c =: program.program.Id];

            //Assert 1: Audit must be saved
            System.assertEquals(1, newAudit.size(), 'Expected the audit to be saved');
            
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStatus(SQX_Audit.STATUS_VOID);
            audit.setStage(SQX_Audit.STAGE_CLOSED);
            audit.save();
            //Arrange: Void the created audit program
            program.setStatus(SQX_Audit_Program.STATUS_VOID)
                    .save();

            //Act 2: Save audit with audit program which is voided
            try{
                SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser).setStatus(SQX_Audit.STATUS_DRAFT)
                                                           .setStage(SQX_Audit.STAGE_PLAN)
                                                           .includeInProgram(false, program.program)
                                                           .save();


            } catch(Exception e){
                //System.assert(false, e.getMessage());
                Boolean expectedExceptionThrown =  e.getMessage().contains('Audit Program is already voided, no new audit can be included in it.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);  
            }
        }
    }

}