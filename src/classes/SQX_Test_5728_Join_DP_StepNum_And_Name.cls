/*
 * Unit test for concatinating deviation process step number and name fields.
 */
@isTest
public class SQX_Test_5728_Join_DP_StepNum_And_Name{
    @testSetup
    public static void commonSetup(){
        
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        System.runAs(adminUser) {
            SQX_Test_Supplier_Deviation  sRecord = new SQX_Test_Supplier_Deviation().save();
            SQX_Supplier_Deviation__c supplierDeviation = [SELECT Status__c,Record_Stage__c FROM SQX_Supplier_Deviation__c LIMIT 1];
            System.assertEquals('Draft', supplierDeviation.Status__c);
            System.assertEquals('Draft', supplierDeviation.Record_Stage__c);
            
            //Arrange: Create Deviation Process Record
            SQX_Deviation_Process__c deviationProcess = new SQX_Deviation_Process__c();
            deviationProcess.SQX_Parent__c = sRecord.sd.Id;
            deviationProcess.Name = 'TestName';
            deviationProcess.Status__c = 'Draft';
            deviationProcess.step__c = 1;
            deviationProcess.Due_Date__c = Date.today();
            deviationProcess.SQX_User__c = adminUser.Id;
            new SQX_DB().op_insert(new List<SQX_Deviation_Process__c> { deviationProcess },  new List<Schema.SObjectField>{});
            
        }
    }
    
   /**
    * GIVEN : Deviation Process record 
    * WHEN  : user creates a new record
    * THEN  : Deviation Process name is prepended by step_number__c field with the  deviation process name field, 
    *         Both fields are separated using a dash and shown in the format (step_number__c - name)	
    */
    @isTest
    public static void givenDeviationProcessRecord_WhenSaved_ThenPrepentStepNumberAndName(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){          
            //Act
            SQX_Deviation_Process__c deviationProcess = [SELECT name,step__c FROM SQX_Deviation_Process__c LIMIT 1];
            //Assert: Ensure that Step number and deviation process is concatinated separated by '-'
            System.assertEquals('TestName', deviationProcess.name);            
        }
    }
    
    /**
    * GIVEN : Deviation Process record 
    * WHEN  : user updates step number of a old deviation record
    * THEN  : Deviation Process name is prepended by new step_number__c field with the deviation process name field.
    */
    @isTest
    public static void givenDeviationProcessRecord_WhenUserUpdatesOldStepNumber_ThenAppendNewStepNumberAndName(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){           
            
            //Arrange
            SQX_Deviation_Process__c deviationProcess = [SELECT name,step__c FROM SQX_Deviation_Process__c LIMIT 1];
           
            //Act: Change only step number
            deviationProcess.step__c = 4;
            new SQX_DB().op_update(new List<SQX_Deviation_Process__c> { deviationProcess },  new List<Schema.SObjectField>{});
            
            //Assert: If a step number is changed, step number is replaced
            System.assertEquals('TestName', [Select name FROM SQX_Deviation_Process__c where id = :deviationProcess.id].name);
            
        }
    }
    
    /**
    * GIVEN : Deviation Process record 
    * WHEN  : user updates name of  old deviation record where old deviation record starts with step number
    * THEN  : Deviation Process name is updated by prepending old step number with the new deviation name  	
    */
    @isTest
    public static void givenDeviationProcessRecord_WhenUserUpdatesNameField_ThenPrependStepNumberWithNewName(){
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){           
            
            //Arrange
            SQX_Deviation_Process__c deviationProcess = [SELECT name,step__c FROM SQX_Deviation_Process__c LIMIT 1];
            
            //Act: Change name of deviation process
            deviationProcess.name = 'updatedName';
            new SQX_DB().op_update(new List<SQX_Deviation_Process__c> { deviationProcess },  new List<Schema.SObjectField>{});
            
            //Assert: If a name is changed, step number and name both are concatinated.
            System.assertEquals('updatedName', [Select name FROM SQX_Deviation_Process__c where id = :deviationProcess.id].name);

        }
    }
}