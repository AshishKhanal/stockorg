/**
* ensures review decision to default to retire and change order to be defaulted to change order of the implementation parameter
* ensures New Document Review page lists proper change orders in dropdown
* tests for error message using SQX_ERR_MSG_CHANGE_ORDER_HAS_NO_ACTIONS_TO_RETIRE_THE_CONTROLLED_DOCUMENT when no obsolete document actions found
* tests for error message using SQX_ERR_MSG_CHANGE_ORDER_WITH_INVALID_STATUS_TO_RETIRE_THE_CONTROLLED_DOCUMENT when change order is completed or voided
* ensures document review is saved with valid change order
*/
@isTest
public class SQX_Test_2151_DocReview_ObsoleteDocCO {
    @testSetup
    public static void commonSetup() {
        // add required users
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensures review decision to default to retire and change order to be defaulted to change order of the implementation parameter
    */
    public static testmethod void givenOpenObsoleteDocumentCOLink_RetireReviewDecisionAndChangeOrderSet() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required change order
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            
            // add required obsolete document action
            SQX_Implementation__c imp1 = co1.addAction(SQX_Implementation.RECORD_TYPE_ACTION, user1, 'Obsolete Doc Description', null);
            imp1.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            insert imp1;
            
            // ACT: open SQX_Document_Review_Editor page with implementation param
            SQX_Document_Review__c review = new SQX_Document_Review__c();
            Test.setCurrentPage(Page.SQX_Document_Review_Editor);
            ApexPages.currentPage().getParameters().put('controlleddocid', cDoc1.doc.Id);
            ApexPages.currentPage().getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPLEMENTATION, imp1.Id);
            ApexPages.StandardController sc = new ApexPages.standardController(review);
            SQX_Extension_Document_Review ext = new SQX_Extension_Document_Review(sc);
            
            System.assertEquals(SQX_Document_Review.REVIEW_DECISION_RETIRE, review.Review_Decision__c);
            System.assertEquals(co1.Id, review.SQX_Obsolete_Document_Change_Order__c);
        }
    }
    
    /**
    * ensures SQX_Extension_Document_Review.getChangeOrdersToRetireDocument() used for listing change orders in New Document Review page to list proper values
    */
    public static testmethod void givenProperChangeOrderDropDownList() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required change orders
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order().save();
            SQX_Test_Change_Order co2 = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_IMPLEMENTATION).save();
            SQX_Test_Change_Order co3 = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_COMPLETE).save();
            
            // add required obsolete document implementations
            SQX_Implementation__c imp1 = co1.addAction(SQX_Implementation.RECORD_TYPE_PLAN, user1, 'Obsolete Doc Description', null);
            SQX_Implementation__c imp2 = co2.addAction(SQX_Implementation.RECORD_TYPE_ACTION, user1, 'Obsolete Doc Description', null);
            SQX_Implementation__c imp3 = co3.addAction(SQX_Implementation.RECORD_TYPE_ACTION, user1, 'Obsolete Doc Description', null);
            imp1.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp2.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp3.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            imp2.SQX_Controlled_Document__c = cDoc1.doc.Id;
            imp3.SQX_Controlled_Document__c = cDoc1.doc.Id;
            insert new List<SQX_Implementation__c>{ imp1, imp2, imp3 };
            
            // ACT: call SQX_Extension_Document_Review.getChangeOrdersToRetireDocument()
            SQX_Document_Review__c review = new SQX_Document_Review__c();
            review.Controlled_Document__c = cDoc1.doc.Id;
            ApexPages.StandardController sc = new ApexPages.standardController(review);
            SQX_Extension_Document_Review ext = new SQX_Extension_Document_Review(sc);
            List<SelectOption> changeOrderList = ext.getChangeOrdersToRetireDocument();
            
            Set<Id> listedIds = new Set<Id>();
            for (SelectOption o : changeOrderList) {
                if (o.getValue() != '') {
                    listedIds.add(Id.valueOf(o.getValue()));
                }
            }
            
            System.assertEquals(2, listedIds.size());
            System.assertEquals(true, listedIds.contains(co1.changeOrder.Id));
            System.assertEquals(true, listedIds.contains(co2.changeOrder.Id));
        }
    }
    
    /**
    * tests for error message using SQX_ERR_MSG_CHANGE_ORDER_HAS_NO_ACTIONS_TO_RETIRE_THE_CONTROLLED_DOCUMENT
    * tests for error message using SQX_ERR_MSG_CHANGE_ORDER_WITH_INVALID_STATUS_TO_RETIRE_THE_CONTROLLED_DOCUMENT
    */
    public static testmethod void givenInvalidChangeOrder_ErrorShown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document().save();
            
            // add required change orders
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order().save();
            SQX_Test_Change_Order co2 = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_IMPLEMENTATION).save();
            SQX_Test_Change_Order co3 = new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_COMPLETE).save();
            
            // add required obsolete document implementations
            SQX_Implementation__c imp1 = co2.addAction(SQX_Implementation.RECORD_TYPE_PLAN, user1, 'Obsolete Doc Description', null);
            SQX_Implementation__c imp2 = co3.addAction(SQX_Implementation.RECORD_TYPE_PLAN, user1, 'Obsolete Doc Description', null);
            imp1.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp2.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp1.SQX_Controlled_Document__c = cDoc2.doc.Id;
            imp2.SQX_Controlled_Document__c = cDoc1.doc.Id;
            insert new List<SQX_Implementation__c>{ imp1, imp2 };
            
            // ACT: add document reviews for cDoc1
            SQX_Document_Review__c review1 = new SQX_Document_Review__c(
                Controlled_Document__c = cDoc1.doc.Id,
                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                SQX_Obsolete_Document_Change_Order__c = co1.changeOrder.Id
            );
            SQX_Document_Review__c review2 = new SQX_Document_Review__c(
                Controlled_Document__c = cDoc1.doc.Id,
                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                SQX_Obsolete_Document_Change_Order__c = co2.changeOrder.Id
            );
            SQX_Document_Review__c review3 = new SQX_Document_Review__c(
                Controlled_Document__c = cDoc1.doc.Id,
                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                SQX_Obsolete_Document_Change_Order__c = co3.changeOrder.Id
            );
            List<SQX_Document_Review__c> reviews = new List<SQX_Document_Review__c>{ review1, review2, review3 };
            Database.SaveResult[] results = Database.insert(reviews, false);
            
            System.assertEquals(false, results[0].isSuccess());
            System.assertEquals(false, results[1].isSuccess());
            System.assertEquals(false, results[2].isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(results[0].getErrors(), Label.SQX_ERR_MSG_CHANGE_ORDER_HAS_NO_ACTIONS_TO_RETIRE_THE_CONTROLLED_DOCUMENT));
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(results[1].getErrors(), Label.SQX_ERR_MSG_CHANGE_ORDER_HAS_NO_ACTIONS_TO_RETIRE_THE_CONTROLLED_DOCUMENT));
            
            String expectedErrorMsg = Label.SQX_ERR_MSG_CHANGE_ORDER_WITH_INVALID_STATUS_TO_RETIRE_THE_CONTROLLED_DOCUMENT;
            expectedErrorMsg = expectedErrorMsg.replace('{ChangeOrderStatus}', SQX_Change_Order.STATUS_COMPLETE);
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(results[2].getErrors(), expectedErrorMsg));
        }
    }
    
    /**
    * ensures document review is saved with valid change order
    */
    public static testmethod void givenValidChangeOrder_ReviewSaved() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {
            // add required released controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            // add required change orders
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order().save();
            
            // add required obsolete document implementations
            SQX_Implementation__c imp1 = co1.addAction(SQX_Implementation.RECORD_TYPE_PLAN, user1, 'Obsolete Doc Description', null);
            imp1.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            imp1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            insert imp1;
            
            // ACT: add document review
            SQX_Document_Review__c review1 = new SQX_Document_Review__c(
                Controlled_Document__c = cDoc1.doc.Id,
                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                SQX_Obsolete_Document_Change_Order__c = co1.changeOrder.Id,
                Expiration_Date__c = System.today() + 30
            );
            Database.SaveResult res1 = Database.insert(review1, false);
            
            System.assertEquals(true, res1.isSuccess(), res1.getErrors());
        }
    }

    /**
     * Given : A Controlled Document already sent for obsolescence approval
     * When : Review is added with retire decision
     * Then : Error is thrown as the document is already in obsolescence approval.
     */
    public static testmethod void givenDocumentInObsolsecenceApproval_WhenDocIsRetired_ErrorIsThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
            
        System.runas(user1) {

            // add required released controlled doc
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document().save();
            cDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_OBSOLESCENCE_APPROVAL;
            cDoc.save();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // ACT: add document review
            SQX_Document_Review__c review = new SQX_Document_Review__c(
                Controlled_Document__c = cDoc.doc.Id,
                Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_RETIRE,
                Expiration_Date__c = System.today() + 30
            );
            Database.SaveResult res = Database.insert(review, false);

            // Assert : Expected validation message should be matched with the acutal validation error message thrown by system.
            String errorMessage = 'Controlled Document has already been submitted for Obsolescence approval process.';
            System.assert(SQX_Utilities.checkErrorMessage(res.getErrors(), errorMessage), 'Expected error : ' + errorMessage);
        }
    }
}