@IsTest
public class SQX_Test_Approved_Investigation{
    
    public testmethod static void givenApprovedInvestigationExists_ItCantBeDuplicated(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
        
            //setup create finding and investigation
            SQX_Test_Finding findingContainer = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
            .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
            .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
            .setStatus(SQX_Finding.STATUS_DRAFT)
            .save();
            
            SQX_Investigation__c investigation = new SQX_Investigation__c(Status__c = SQX_Investigation.STATUS_PUBLISHED,
                                                                          Investigation_Summary__c = '',
                                                                         SQX_Finding__c = findingContainer.finding.Id);
            
            insert investigation;
            
            //add an approved investigation
            //
            SQX_Approved_Investigation__c approvedInvestigation = new SQX_Approved_Investigation__c(
                SQX_Investigation__c = investigation.Id,
                SQX_Finding__c = findingContainer.finding.Id
            );
            
            insert approvedInvestigation;
            
            //lets try inserting an approved investigation again
            approvedInvestigation.Id = null;
            
            Database.SaveResult result = Database.insert(approvedInvestigation, false);
            System.assert(result.isSuccess() == false, 'Expected duplicate to be prevented');
        }
            
    }
}