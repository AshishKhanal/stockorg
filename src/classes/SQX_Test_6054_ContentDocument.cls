/*
 * Test class for validating update content document in controlled document when status of content document is in Approval
 */

@isTest
public class SQX_Test_6054_ContentDocument {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /*
     * Method to assert validation for content document
     * @param contentDocumentReference of the Controlled Document to be set
     */ 
    public static void updateContentVersionAndValidate(Id contentDocumentReference){
        //update content document version
        ContentVersion cv = new ContentVersion();
        
        cv.VersionData = Blob.valueOf('SampleContent');
        cv.PathOnClient = 'Test.txt';
        
        cv.ContentDocumentId = contentDocumentReference;
        List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<ContentVersion>{ cv }, new List<SObjectField>{});
        
        //Assert: can update the Content Document since Controlled Document Approval Status is Release Approval
        System.assertEquals(true, result[0].isSuccess(),'Content Document is expected to be updated');
    } 
    
    /*
     * Given : Controlled Document record with content document
     * When : Status of Controlled Document is in Approval
     * Then : Allow update content document
     * @SQX-6054 prevented content updated but was overriden by SQX-7184
     */
    public static testMethod void givenControlledDocument_WhenInApproval_AllowContentDocumentUpdate(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            //Arrange: Add document supervisor custom permission to admin user
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR);

            //Arrange: create controlled document record with Approval Status in Release Approval
            SQX_Test_Controlled_Document contDoc = new SQX_Test_Controlled_Document().save(true);
            
            //Act: Set approval status to release approval
            contDoc.doc.First_Approver__c = adminUser.Id;
            contDoc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_DRAFT;
            contDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            contDoc.save().synchronize();
            
            //Assert: ensure validation for update content document
            updateContentVersionAndValidate(contDoc.doc.Content_Reference__c);
            
            //Arrange: create controlled document record with Approval Status in In Change Approval
            SQX_Test_Controlled_Document contDoc2 = new SQX_Test_Controlled_Document().save(true);
            contDoc2.doc.Document_Status__c = SQX_Controlled_Document.STATUS_DRAFT;
            contDoc2.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            contDoc2.save().synchronize();
            
            //Assert: ensure validation for update content document
            updateContentVersionAndValidate(contDoc2.doc.Content_Reference__c);
            
            //Arrange: create controlled document record with Approval Status in Obsolescence Approval
            SQX_Test_Controlled_Document contDoc3 = new SQX_Test_Controlled_Document().save(true);
            contDoc3.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            contDoc3.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_OBSOLESCENCE_APPROVAL;
            contDoc3.save().synchronize();
            
            //Assert: ensure validation for update content document
            updateContentVersionAndValidate(contDoc3.doc.Content_Reference__c);
        }
    }
}