/**
* This class is used to return all the Complete(i.e Open) Audit for which the user is Auditee Contact.
*/
public with sharing class SQX_Controller_Complete_Audits{
    
    /**
    * returns the list of complete Audit for the currently logged in user's account 
    */
    public List<SQX_Audit__c> getCompleteAudits() {
    
        List<SQX_Audit__c> completeAudit = [                    
                        SELECT Id, Name, Title__c, Start_Date__c, End_Date__c, Status__c, SQX_Department__c, Audit_Type__c, Number_of_Findings__c, OwnerId, Owner.Name,
                        (SELECT Status__c FROM SQX_Findings__r)FROM SQX_Audit__c
                        WHERE Status__c = :SQX_Audit.STATUS_COMPLETE AND SQX_Auditee_Contact__c = : UserInfo.getUserId()
                        ORDER BY Name DESC
                        ];
        List<SQX_Audit__c> auditsNeedingResponse = new List<SQX_Audit__c>();

        //add only those audit which has related finding status open ie needs response
        for(SQX_Audit__c audit : completeAudit){
            for(SQX_Finding__c auditFinding : audit.SQX_Findings__r){
                if(auditFinding.Status__c== SQX_Finding.STATUS_OPEN){
                    auditsNeedingResponse.add(audit);
                    break;
                }
                
            }
        }

        return auditsNeedingResponse;
        
    }

}