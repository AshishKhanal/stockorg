/*
 * Unit test of Validation Rules for New Supplier Introduction object
 * Story: SQX-5957
 */ 
@isTest
public class SQX_Test_5957_Prevent_Edit_Of_NSI_Number {
@testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /*
     * Given: Any New Supplier Introduction Record
     * When: User tries to edit NSI number 
     * Then: Validation Error msg, 'NSI Number cannot be modified.' is thrown
     */
    @isTest
    public static void givenSDRecord_whenUserEditsNSINumber_ThenValidationErrorIsThrown(){
        
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')) {
            //Arrange: Create NSI Record
            SQX_Test_NSI nsiRecord = new SQX_Test_NSI().save();
            System.assert(nsiRecord.nsi.Id != null, 'Expected, NSI record to be created');
            
            //Act: Edit NSI Number and update the record
            nsiRecord.nsi.Name = 'NSI-ModifiedNameOfNSI';
            Database.SaveResult result = Database.update(nsiRecord.nsi, false);

            //Assert: Validation Error message 'NSI Number cannot be modified.' is thrown.
            System.assertEquals('NSI Number cannot be modified.',result.getErrors()[0].getMessage(), 'Expected, A validation error to be thrown when user modifies NSI number');
            //Assert: Make sure that Prevent_Name_Update_Flag__c field is set to true by Process Builder
            System.assertEquals(true, [SELECT Prevent_Name_Update_Flag__c FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsiRecord.nsi.Id].Prevent_Name_Update_Flag__c);

        }
    }
}