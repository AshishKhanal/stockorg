/**
 * test class to ensure that the complaint quantity cannot be negative value
 * @author: Sagar Shrestha
 * @date: 2015-12-31
 * @story: [SQX-1822] 
 */
@isTest
public class SQX_Test_1822_ComplaintQtyNotNegative{ 
    static boolean runAllTests = true,
                   givenComplaint_ComplaintQuantityCannotBeNegative = false,
                   givenComplaint_ActualReturnQuantityCannotBeNegative = false,
                   givenComplaint_ExpectedReturnQuantityCannotBeNegative = false;

    /**
     * Scenario 1
     * Action: Set negative complaint quantity
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Set 0 complaint quantity
     * Expected: Complaint Save should be successful 
     *
     * Scenario 3
     * Action: Set positive complaint quantity
     * Expected: Complaint Save should be successful
     * 
     * @author: Sagar Shrestha
     * @date: 2015-12-31
    */
    public testmethod static void givenComplaint_ComplaintQuantityCannotBeNegative(){
        if(!runAllTests && !givenComplaint_ComplaintQuantityCannotBeNegative){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set negative complaint quantity
            complaint.complaint.Complaint_Quantity__c = -5.00;
            Database.SaveResult negativeComplaintQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for negative compalint quantity
            System.assertEquals(false, negativeComplaintQuantityUpdate.isSuccess(), 'Expected save to be unsuccessful for negative compalint quantity');

            //Act2 : Set zero complaint quantity
            complaint.complaint.Complaint_Quantity__c = 0.00;
            Database.SaveResult zeroComplaintQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be successful for zero complaint quantity
            System.assertEquals(true, zeroComplaintQuantityUpdate.isSuccess(), 'Expected save to be successful for zero complaint quantity');

            //Act3 : Set positive complaint quantity
            complaint.complaint.Complaint_Quantity__c = 5.00;
            Database.SaveResult positiveComplaintQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for positive complaint quantity
            System.assertEquals(true, positiveComplaintQuantityUpdate.isSuccess(), 'Expected save to be successful for positive complaint quantity');

        }
    }

    /**
     * Scenario 1
     * Action: Set negative Actual Return Quantity
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Set 0 Actual Return Quantity
     * Expected: Complaint Save should be successful 
     *
     * Scenario 3
     * Action: Set positive Actual Return Quantity
     * Expected: Complaint Save should be successful
     * 
     * @author: Shailesh Maharjan
     * @date: 2016-01-05
    */
    public testmethod static void givenComplaint_ActualReturnQuantityCannotBeNegative(){
        if(!runAllTests && !givenComplaint_ActualReturnQuantityCannotBeNegative){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set negative Actual Return Quantity
            complaint.complaint.Actual_Return_Quantity__c = -5.00;
            Database.SaveResult negativeActualReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for negative Actual Return Quantity
            System.assertEquals(false, negativeActualReturnQuantityUpdate.isSuccess(), 'Expected save to be unsuccessful for negative Actual Return Quantity');

            //Act2 : Set zero Actual Return Quantity
            complaint.complaint.Actual_Return_Quantity__c = 0.00;
            Database.SaveResult zeroActualReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be successful for zero Actual Return Quantity
            System.assertEquals(true, zeroActualReturnQuantityUpdate.isSuccess(), 'Expected save to be successful for zero Actual Return Quantity');

            //Act3 : Set positive Actual Return Quantity
            complaint.complaint.Actual_Return_Quantity__c = 5.00;
            Database.SaveResult positiveActualReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for positive Actual Return Quantity
            System.assertEquals(true, positiveActualReturnQuantityUpdate.isSuccess(), 'Expected save to be successful for positive Actual Return Quantity');

        }
    }

    /**
     * Scenario 1
     * Action: Set negative Expected Return Quantity
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Set 0 Expected Return Quantity
     * Expected: Complaint Save should be successful 
     *
     * Scenario 3
     * Action: Set positive Expected Return Quantity
     * Expected: Complaint Save should be successful
     * 
     * @author: Shailesh Maharjan
     * @date: 2016-01-05
    */
    public testmethod static void givenComplaint_ExpectedReturnQuantityCannotBeNegative(){
        if(!runAllTests && !givenComplaint_ExpectedReturnQuantityCannotBeNegative){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set negative Expected Return Quantity
            complaint.complaint.Expected_Return_Quantity__c = -5.00;
            Database.SaveResult negativeExpectedReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for negative Expected Return Quantity
            System.assertEquals(false, negativeExpectedReturnQuantityUpdate.isSuccess(), 'Expected save to be unsuccessful for negative Expected Return Quantity');

            //Act2 : Set zero Expected Return Quantity
            complaint.complaint.Expected_Return_Quantity__c = 0.00;
            Database.SaveResult zeroExpectedReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 2 : Expected save to be successful for zero Expected Return Quantity
            System.assertEquals(true, zeroExpectedReturnQuantityUpdate.isSuccess(), 'Expected save to be successful for zero Expected Return Quantity');

            //Act3 : Set positive Expected Return Quantity
            complaint.complaint.Expected_Return_Quantity__c = 5.00;
            Database.SaveResult positiveExpectedReturnQuantityUpdate = Database.update(complaint.complaint, false);

            //Assert 3 : Expected save to be successful for positive Actual Return Quantity
            System.assertEquals(true, positiveExpectedReturnQuantityUpdate.isSuccess(), 'Expected save to be successful for positive Expected Return Quantity');

        }
    }
}