/*
 * description: test for audit response
 */
@IsTest
public class SQX_Test_1293_Audit_Response{

    private static Boolean runAllTests = true,
    						run_auditResponseSubmitted_ResponseProcessed=false,
    						run_auditResponseSubmitted_SentForApproval=false,
    						run_auditResponse_ApprovalRequired_Approved=false,
    						run_auditResponse_ApprovalRequired_Rejected=false,
    						run_allAuditFIndingComplete_AuditCanBeClosed=false,
    						run_allAuditFIndingCompleteOrDraftOrClosed_AuditCanBeClosed=false,
    						run_allAuditFindingNotComplete_AuditCannotBeClosed=false,
                            run_givenAnAuditIsClosed_ItCantBeChanged = false;
    
    /**
    * helper funtion to setup Audit, finding and finding response
    * @param investigationApproval set whether investigation approval is required or not
    **/                      
    public static SQX_Test_Audit arrangeForAuditResponse(User adminUser, boolean investigationApproval, user responseSubmitter){
            /*
            * Arrange : Create Audit with investigation apprval required
            *           Add a finding with investigation and containment required
            *           Add another finding with containment required
            *           Create audit response with investigation and containment for first finding and containment for second finding
            */
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.setPolicy(investigationApproval, false, false);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            SQX_Test_Finding auditFinding1 =  audit.addAuditFinding()
                                                .setRequired(true, true, true, false, false) //response, containment, and investigation required
                                                .setApprovals(investigationApproval, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();
            SQX_Test_Finding auditFinding2 =  audit.addAuditFinding()
                                                .setRequired(true, true, false, false, false)//response, and containment required
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();

            System.runas(responseSubmitter) {
            	SQX_Audit_Response__c auditResponse = new SQX_Audit_Response__c(SQX_Audit__c= audit.audit.Id,
                                                                            Audit_Response_Summary__c ='Audit Response for finding 1');

	            SQX_Finding_Response__c responseForFinding1 = new SQX_Finding_Response__c( 
	                                                                            SQX_Finding__c = auditFinding1.finding.Id,
	                                                                            Response_Summary__c = 'Response for audit');

	            SQX_Finding_Response__c responseForFinding2 = new SQX_Finding_Response__c( 
	                                                                            SQX_Finding__c = auditFinding2.finding.Id,
	                                                                            Response_Summary__c = 'Response for audit');
	            
	            SQX_Containment__c containmentForFinding1 = new SQX_Containment__c(SQX_Finding__c = auditFinding1.finding.Id,
	                                                                                     Containment_Summary__c = 'Containment for finding1',
	                                                                                     Completion_Date__c = Date.today());

	            SQX_Containment__c containmentForFinding2 = new SQX_Containment__c(SQX_Finding__c = auditFinding2.finding.Id,
	                                                                                     Containment_Summary__c = 'Containment for finding2',
	                                                                                     Completion_Date__c = Date.today());
	            
	            SQX_Investigation__c investigationForFinding1 = new SQX_Investigation__c(SQX_Finding__c = auditFinding1.finding.Id,
	                                                                         Investigation_Summary__c = 'Investigation for finding1');
	            
	            SQX_Response_Inclusion__c inclusion1 = new SQX_Response_Inclusion__c(Type__c = 'Containment');
	            SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');

	            Map<String,Object>  changeSet = new Map<String, Object>(),
	                                auditResponseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(auditResponse)),
	                                responseForFinding1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(responseForFinding1)),
	                                responseForFinding2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(responseForFinding2)),
	                                containmentForFinding1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(containmentForFinding1)),
	                                containmentForFinding2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(containmentForFinding2)),
	                                investigationForFinding1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigationForFinding1)),
	                                inclusion1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion1)),
	                                inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)),
	                                inclusion3UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion1)); // UT: untyped
	            
	            //set virtual IDSs
	            auditResponseUT.put('Id', 'AuditResponse-1');
	            responseForFinding1UT.put('Id', 'responseForFinding1-1');
	            responseForFinding2UT.put('Id', 'responseForFinding2-1');
	            containmentForFinding1UT.put('Id', 'Containment-1');
	            containmentForFinding2UT.put('Id', 'Containment-2');
	            investigationForFinding1UT.put('Id', 'Investigation-1');
	            inclusion1UT.put('Id', 'Inclusion-1');
	            inclusion2UT.put('Id', 'Inclusion-2');
	            inclusion3UT.put('Id', 'Inclusion-3');

	            //put audit response id in finding responses
	            responseForFinding1UT.put(SQX.getNSNameFor('SQX_Audit_Response__c'), 'AuditResponse-1');
	            responseForFinding2UT.put(SQX.getNSNameFor('SQX_Audit_Response__c'), 'AuditResponse-1');

	            inclusion1UT.put(SQX.getNSNameFor('SQX_Containment__c'), 'Containment-1');
	            inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
	            inclusion3UT.put(SQX.getNSNameFor('SQX_Containment__c'), 'Containment-2');

	            //add containment and investigation in finding response for finding1
	            inclusion1UT.put(SQX.getNSNameFor('SQX_Response__c'), 'responseForFinding1-1');
	            inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'responseForFinding1-1');

	            //add containment in finding response for finding2
	            inclusion3UT.put(SQX.getNSNameFor('SQX_Response__c'), 'responseForFinding2-1');


	            List<Object> changes = new List<Object>();
	            
	            changes.add(auditResponseUT);
	            changes.add(responseForFinding1UT);
	            changes.add(responseForFinding2UT);
	            changes.add(containmentForFinding1UT);
	            changes.add(containmentForFinding2UT);
	            changes.add(investigationForFinding1UT);
	            changes.add(inclusion1UT);
	            changes.add(inclusion2UT);
	            changes.add(inclusion3UT);

	            changeSet.put('changeSet', changes);
	            Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'submitresponse');
	            params.put('comment', 'Mock comment');
	            params.put('purposeOfSignature', 'Mock purposeOfSignature');

	            /*
	            * Act : Submit audit response for audit
	            */
	            System.assertEquals(SQX_Extension_UI.OK_STATUS,
	                SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', JSON.serialize(changeSet), false , null, params ), 'Expected to successfully submit reponse');
            }                                   
            

            return audit;
    }
    
    /*
    *   test for actions to be performed on audit response submission when aproval is not required
    */
    public static testmethod void auditResponseSubmitted_ResponseProcessed(){
    		if(!runAllTests && !run_auditResponseSubmitted_ResponseProcessed){
	            return;
	        }
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange and act 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, false, responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id, Submitted_On__c, Submitted_By__c, Response_Approver__c, Approval_Status__c, Status__c from SQX_Audit_Response__c 
                                                        where SQX_Audit__c= :audit.audit.Id];

                /**
                * Test for response once published should not be allowed to be deleted
                **/
                Database.DeleteResult deleteAuditResponseResult = Database.Delete(auditResponse,false);
                System.assertEquals(false, deleteAuditResponseResult.isSuccess(), 'Expected audit cannot be deleted once published');

                //Assert the Submitted on and submitted by are filled 
                System.assertEquals([Select Name from User where Id= :responseSubmitter.Id ].Name, auditResponse.Submitted_By__c,  'Expected current UserName to be set in submitted by');
                System.assertEquals(Date.Today(), ((DateTime)auditResponse.Submitted_On__c).date(),  'Expected today date to be set in submitted on');

                //Assert that audit response approver is set to audit owner
                System.assertEquals([Select OwnerId from SQX_Audit__c where Id= :audit.audit.Id].OwnerId, auditResponse.Response_Approver__c,  'Expected current UserName to be set in submitted by');
                
                /*
                * Approval is not required, so the audit reponse should be approved
                */
                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_NONE, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_NONE+ ' but found ' +auditResponse.Approval_Status__c);

                List<SQX_Finding_Response__c> findingResponses = [SELECT Approval_Status__c, Status__c FROM SQX_Finding_Response__c where 
                                                                                SQX_Audit_Response__c =:auditResponse.Id ];

                System.assertEquals(2, findingResponses.size(),  
                    'Expected 2 finding responses to be associated with Audit Response but found '+ findingResponses.size());

                /*
                * Test that finding response status and approval status should be synced with audit response 
                * when changes in these field are detected in audit response
                */
                System.assertEquals(findingResponses[0].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[0].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                System.assertEquals(findingResponses[1].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[1].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                
            }
    }

    /*
    *   test for actions to be performed on audit response submission when iinvestigation aproval is required
    */
    public static testmethod void auditResponseSubmitted_SentForApproval(){
    		if(!runAllTests && !run_auditResponseSubmitted_SentForApproval){
	            return;
	        }    		
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange and act 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id, Submitted_On__c, Submitted_By__c, Response_Approver__c, Approval_Status__c, Status__c from SQX_Audit_Response__c 
                                                        where SQX_Audit__c= :audit.audit.Id];
                
                //Assert that audit response approver is set to audit owner
                System.assertEquals([Select OwnerId from SQX_Audit__c where Id= :audit.audit.Id].OwnerId, auditResponse.Response_Approver__c,  'Expected current UserName to be set in submitted by');
                
                /*
                * Approval is required in fining 1 for investigation, so the audit reponse should go for approval
                */
                System.assertEquals(SQX_Audit_Response.STATUS_IN_APPROVAL, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_IN_APPROVAL+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_PENDING, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_PENDING+ ' but found ' +auditResponse.Approval_Status__c);

                List<SQX_Finding_Response__c> findingResponses = [SELECT Approval_Status__c, Status__c FROM SQX_Finding_Response__c where 
                                                                                SQX_Audit_Response__c =:auditResponse.Id ];

                System.assertEquals(2, findingResponses.size(),  
                    'Expected 2 finding responses to be associated with Audit Response but found '+ findingResponses.size());

                /*
                * Test that finding response status and approval status should be synced with audit response 
                * when changes in these field are detected in audit response
                */
                System.assertEquals(findingResponses[0].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[0].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                System.assertEquals(findingResponses[1].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[1].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                
            }
    }

    /*
    *   test for status check when audit response is approved
    */
    public static testmethod void auditResponse_ApprovalRequired_Approved(){
    		if(!runAllTests && !run_auditResponse_ApprovalRequired_Approved){
	            return;
	        }  
        /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);


            
            System.runas(stdUser){
                /*
                * Arrange 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true,responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                System.runas(adminUser){
                	SQX_Audit_Response__c auditResponse = [Select Id from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id]; 

	                /*
	                * Act : approve the audit response 
	                */
	                Map<String, String> params = new Map<String, String>();
	                params.put('nextAction', 'Approve');
	                params.put('recordID', auditResponse.Id);
	                Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

	                System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
	                'Expected to successfully Approve '+ auditId);

	                auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

	                /*
	                * Assert : Response should be approved 
	                */
	                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
	                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
	                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_APPROVED, auditResponse.Approval_Status__c,  
	                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_APPROVED+ ' but found ' +auditResponse.Approval_Status__c);

                }

                
            }
    }

    /*
    *   test for status check when audit response is rejected
    */
    public static testmethod void auditResponse_ApprovalRequired_Rejected(){
    		if(!runAllTests && !run_auditResponse_ApprovalRequired_Rejected){
	            return;
	        }  
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//investigation approval not required

                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id]; 

                 /*
                * Act : Reject the response
                */
                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'Reject');
                params.put('recordID', auditResponse.Id);
                Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                'Expected to successfully Reject '+ auditId);

                auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                /*
                * Assert: Response should be approved 
                */
                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_REJECTED, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_REJECTED+ ' but found ' +auditResponse.Approval_Status__c);

            }
    }



    /*
    *   test for audit closure when all related findings are not complete
    */
    public static testmethod void allAuditFindingNotComplete_AuditCannotBeClosed(){
    		if(!runAllTests && !run_allAuditFindingNotComplete_AuditCannotBeClosed){
	            return;
	        }  
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            
            System.runas(adminUser){
                /*
                * Arrange : add audit, add findings, and submit response but findings are not moved to completion
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//investigation approval not required

                List<SQX_Finding__c> auditFindings = [SELECT Status__c from SQX_Finding__c Where SQX_Audit__c= :audit.audit.Id];

                System.assertEquals(2, auditFindings.size(), 'Expected 2 findings added in Audit');

                System.assertEquals(SQX_Finding.STATUS_OPEN, auditFindings[0].Status__c , 'Expected finding to remain open');
                System.assertEquals(SQX_Finding.STATUS_OPEN, auditFindings[1].Status__c , 'Expected finding to remain open');

                /*
                * Act :  set audit to completion
                */
                audit.setStatus(SQX_Audit.STATUS_CLOSE);

                /*
                * Assert :  closure should be unsuccessful
                */
                Database.SaveResult auditClosureResult = Database.update(audit.audit, false);

                System.assertEquals(false, auditClosureResult.isSuccess(), 'Expected closure to be failed');

            }

    }

    /**
    * This test ensures that once audit is closed it can't be edited
    * SQX-1331 bug raised
    */
    public static testmethod void givenAnAuditIsClosed_ItCantBeChanged(){
        if(!runAllTests && !run_givenAnAuditIsClosed_ItCantBeChanged){
                return;
        }

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(adminUser){
            /*
            * Arrange : add audit in closed status
            */
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).setStatus(SQX_Audit.STATUS_CLOSE);
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);


            audit.save();

            //Act: try changing the audit
            Boolean auditChangeSucceeded = true;
            try{
                audit.Save();
            }
            catch(DMLException ex){
                auditChangeSucceeded = false; //any exception would imply that the transaction failed
            }


            //Assert: Ensure that the audit wasn't modified
            System.assertEquals(false, auditChangeSucceeded);

        }
    }
}