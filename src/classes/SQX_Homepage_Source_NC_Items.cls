/**
* This class acts as an item source for NC related items
* @author  Anuj Bhandari
* @date    06-04-2017
*/
public with sharing class SQX_Homepage_Source_NC_Items extends SQX_Homepage_ItemSource {


    /*
        NC Items : Draft/Open/Triage/Completed items
    */

    private final String module = SQX_Homepage_Constants.MODULE_TYPE_NC;

    private User loggedInUser;

    String ncPage;  // getPageName of nc main page
    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_NC_Items() {
        super();
        loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
        ncPage=SQX_Utilities.getPageName('', 'SQX_NC');
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Nonconformance__c.SObjectType => new List<SObjectField> 
            {
                SQX_Nonconformance__c.Name,
                SQX_Nonconformance__c.NC_Title__c,
                SQX_Nonconformance__c.CreatedDate,
                SQX_Nonconformance__c.Status__c,
                SQX_Nonconformance__c.OwnerId
            },
            SQX_Investigation__c.SObjectType => new List<SObjectField>    
            {
                SQX_Investigation__c.Name,
                SQX_Investigation__c.CreatedDate,
                SQX_Investigation__c.SQX_Nonconformance__c,
                SQX_Investigation__c.Status__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
    *   returns a list of homepage items for the current user related to NC module
    */
    protected override List <SObject> getUserRecords() {

        List<SObject> allItems = new List<SObject>();
        List <String> ncStatuses = new list <String>
        {
            SQX_NC.STATUS_DRAFT,
            SQX_NC.STATUS_OPEN,
            SQX_NC.STATUS_TRIAGE,
            SQX_NC.STATUS_COMPLETE
        };

        List <SQX_Nonconformance__c> ncItems = [SELECT  Id,
                                                        Name,
                                                        NC_Title__c,
                                                        Status__c,
                                                        CreatedDate
                                                        FROM SQX_Nonconformance__c
                                                        WHERE
                                                        Status__c IN: ncStatuses
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];
        
        List<SQX_Investigation__c> investigationAction=   [SELECT   Id,
                                                                    Name,
                                                                    CreatedDate, 
                                                                    SQX_Nonconformance__c, 
                                                                    SQX_Nonconformance__r.Name,
                                                                    SQX_Nonconformance__r.Title__c,
                                                                    Approval_Status__c ,
                                                                    Status__c 
                                                                FROM SQX_Investigation__c
                                                                WHERE Approval_Status__c=:SQX_Investigation.APPROVAL_STATUS_PENDING
                                                                AND Id NOT IN( SELECT SQX_Investigation__c 
                                                                               FROM SQX_Response_Inclusion__c 
                                                                               WHERE SQX_Investigation__c !=null
                                                                             )
                                                                AND SQX_Nonconformance__r.SQX_Investigation_Approver__c=: loggedInUser.Id
                                                            ];
        allItems.addAll((List<SObject>) ncItems);
        allItems.addAll((List<SObject>) investigationAction);
        return allItems;
    }



    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        SQX_Homepage_Item ncItem;
        if(item instanceOf SQX_Nonconformance__c){
            ncItem = getNcItem((SQX_Nonconformance__c) item);
        }else if(item instanceOf SQX_Investigation__c ){
            ncItem = getInvestigationItem((SQX_Investigation__c) item);
        }
        return ncItem;
    }

    /**
    *   Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getNcItem(SQX_Nonconformance__c item) {

        SQX_Homepage_Item ncItem = new SQX_Homepage_Item();

        ncItem.itemId = item.Id;

        ncItem.createdDate = Date.valueOf(item.CreatedDate);

        ncItem.moduleType = this.module;

        ncItem.creator = loggedInUser;

        if (item.Status__c == SQX_NC.STATUS_OPEN) {

            ncItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS;
            ncItem.actions = getActionsForOpenNc(item.Id);
            ncItem.feedText = getOpenNcFeedText(item);

        } else if (item.Status__c == SQX_NC.STATUS_DRAFT) {

            ncItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            ncItem.actions = getDraftNcActions(item.Id);
            ncItem.feedText = getDraftNcFeedText(item);

        } else if (item.Status__c == SQX_NC.STATUS_TRIAGE) {

            ncItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
            ncItem.actions = getTriageActionsForNc(item.Id);
            ncItem.feedText = getTriageNcFeedText(item);

        } else if (item.Status__c == SQX_NC.STATUS_COMPLETE) {

            ncItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            ncItem.actions = getCompletedNcActions(item.Id);
            ncItem.feedText = getCompletedNcFeedText(item);

        }

        return ncItem;
    }

    /**
    *   Method returns a list of actions associated triage NC
    *   @param itemId - id of the record under consideration
    */
    private List <SQX_Homepage_Item_Action> getTriageActionsForNc(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_INITIATE;

        PageReference pr = new PageReference('/' + itemId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_INITIATE;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    *   Returns the respond action type for NC
    *   @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getActionsForOpenNc(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    *   Returns the view action type for Draft NC item
    *   @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getDraftNcActions(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    *   Returns the action for completed NC
    *   @param id - id of the record for which action is to be set
    */
    private List <SQX_Homepage_Item_Action> getCompletedNcActions(String itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_CLOSE;

        PageReference pr = new PageReference('/' + itemId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_CLOSE;

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    *    Method returns the open NC type feed text for the given item
    *    @param item - NC record
    */
    private String getOpenNcFeedText(SQX_Nonconformance__c item) {
        return getNCFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);
    }

    /**
    *   Method returns the draft NC type feed text for the given item
    *   @param item - NC record
    */
    private String getDraftNcFeedText(SQX_Nonconformance__c item) {
        return getNCFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);
    }

    /**
    *   Method returns the Completed NC type feed text for the given item
    *   @param item - NC record
    */
    private String getCompletedNcFeedText(SQX_Nonconformance__c item) {
        return getNCFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);
    }

    /**
    *   Method returns the Triage NC type feed text for the given item
    *   @param item - NC record
    */
    private String getTriageNcFeedText(SQX_Nonconformance__c item) {
        return getNCFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, item);
    }

    /**
    *   Method replaces the given NC feed template with NC's info and returns the string
    */
    private String getNCFeedText(String template, SQX_Nonconformance__c item)
    {
        return String.format(template, new String[] {
            item.Name,
            String.isBlank(item.NC_Title__c) ? '' : item.NC_Title__c
        });
    }

    /**
    *   Method returns a corresponding homepage item for the given record item for Investigation
    *   @param item - SQX_Investigation__c record
    */
    private  SQX_Homepage_Item getInvestigationItem(SQX_Investigation__c item) {

        // flag to check if item is draft of Nc investigation
        Boolean isApprovalInvestigation = item.Status__c == SQX_Investigation.STATUS_IN_APPROVAL;
        SQX_Homepage_Item InvestigationItem = new SQX_Homepage_Item();
        InvestigationItem.itemId = item.Id;
        // set action type
        InvestigationItem.actionType =SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE;

        InvestigationItem.createdDate = Date.valueOf(item.CreatedDate);
        // set module type
        InvestigationItem.moduleType =this.module;
      
        // set actions for the current item
        String recordId =item.SQX_Nonconformance__C;
        InvestigationItem.actions = getInvestigationItemActions(recordId,item);
        // set feed text for the current item
        InvestigationItem.feedText = getInvestigationFeedText(item);
        // set user details of the user related to this item
        InvestigationItem.creator = loggedInUser;
        return InvestigationItem;
    }

    /**
    *   Returns a list of actions for in Approval of NC investigation
    *   @param id - id of the record for which action is to be set
    *   @param item - SQX_Investigation__c record
    */
    private List<SQX_Homepage_Item_Action> getInvestigationItemActions(String id,SQX_Investigation__c item){
        SQX_Homepage_Item_Action investigationAction = new SQX_Homepage_Item_Action();
        investigationAction.name = SQX_Homepage_Constants.ACTION_APPROVE_REJECT;
        PageReference pr = new PageReference(ncPage);
        pr.getParameters().put('Id',id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        investigationAction.actionUrl = getHomePageItemActionUrl(pr);
        investigationAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT;
        investigationAction.supportsBulk = false;
        return new List<SQX_Homepage_Item_Action> { investigationAction };
     }
    
    /**
    *   Method returns the feed text for Investigation items
    *   @param item -  Investigation record
    */
    private String getInvestigationFeedText(SQX_Investigation__c item){
        String template =SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE;
        return String.format(template, new String[] { 
                                item.Name,(String.isBlank(item.SQX_Nonconformance__r.Title__c) ? '' : item.SQX_Nonconformance__r.Title__c)
                            });
    }

}