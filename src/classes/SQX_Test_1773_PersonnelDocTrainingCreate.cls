/**
* Unit tests for setting personnel and document values for new personnel document training
* using SQX_Extension_PersonnelDocumentTraining extension and SQX_Personnel_Document_Training_Create VF page.
*/
@IsTest
public class SQX_Test_1773_PersonnelDocTrainingCreate {
    
    static Boolean runAllTests = true,
                   run_givenCallSaveAndNewWithPersonnelAndDocumentValues_PDTSavedAndReturnCreateURLWithSamePersonnelAndDocumentValues = false,
                   run_givenCallSaveWithPersonnelAndDocumentValues_PDTSavedAndReturnViewURL = false;
    
    /**
    * tests saveAndNew method of SQX_Extension_PersonnelDocumentTraining extension
    */
    public testmethod static void givenCallSaveAndNewWithPersonnelAndDocumentValues_PDTSavedAndReturnCreateURLWithSamePersonnelAndDocumentValues() {
        if (!runAllTests && !run_givenCallSaveAndNewWithPersonnelAndDocumentValues_PDTSavedAndReturnCreateURLWithSamePersonnelAndDocumentValues) {
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for ujf test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);
            
            // add required personnel
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();
            
            // add required controlled document
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document().save();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            
            String personnelId = p.mainRecord.Id;
            String documentId = cDoc.doc.Id;
            
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false
            );
            
            Test.setCurrentPage(Page.SQX_Personnel_Document_Training_Create);
            ApexPages.currentPage().getParameters().put('personnel', personnelId );
            ApexPages.currentPage().getParameters().put('document', documentId );
            ApexPages.StandardController stdController = new ApexPages.StandardController(pdt1);
            SQX_Extension_PersonnelDocumentTraining ext = new SQX_Extension_PersonnelDocumentTraining(stdController);
            
            // ACT: call saveAndNew method using extension
            PageReference pr = ext.saveAndNew();
            
            List<SQX_Personnel_Document_Training__c> newPDT = [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE SQX_Personnel__c = :personnelId AND SQX_Controlled_Document__c = :documentId];
            
            System.assertEquals(1, newPDT.size(), 'Expected 1 personnel document training to be created with passed parameters.');
            
            System.assert(pr != null,
                'Expected personnel document training to be saved using passed parameters.');
            
            System.assert(pr.getURL().contains(Page.SQX_Personnel_Document_Training_Create.getURL()),
                'Expected SQX_Personnel_Document_Training_Create VF page URL to be returned.');
            
            System.assertEquals(personnelId , pr.getParameters().get('personnel'),
                'Expected personnel parameter value to be same as original link for save and new method.');
            
            System.assertEquals(documentId , pr.getParameters().get('document'),
                'Expected controlled document parameter value to be same as original link for save and new method.');
        }
    }

    /**
    * tests save method of SQX_Extension_PersonnelDocumentTraining extension with user value passed
    */
    public testmethod static void givenCallSaveWithPersonnelAndDocumentValues_PDTSavedAndReturnViewURL() {
        if (!runAllTests && !run_givenCallSaveWithPersonnelAndDocumentValues_PDTSavedAndReturnViewURL) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        // creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(adminUser) {
            // add required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for ujf test' );
            Database.SaveResult jfResult = Database.insert(jf1, false);
            
            // add required personnel
            SQX_Test_Personnel p = new SQX_Test_Personnel();
            p.save();
            
            // add required controlled document
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document().save();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Job_Function__c = jf1.Id,
                Active__c = false
            );
            
            String personnelId = p.mainRecord.Id;
            String documentId = cDoc.doc.Id;
            
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false
            );
            
            Test.setCurrentPage(Page.SQX_Personnel_Document_Training_Create);
            ApexPages.currentPage().getParameters().put('personnel', personnelId );
            ApexPages.currentPage().getParameters().put('document', documentId );
            ApexPages.StandardController stdController = new ApexPages.StandardController(pdt1);
            SQX_Extension_PersonnelDocumentTraining ext = new SQX_Extension_PersonnelDocumentTraining(stdController);
            
            // ACT: call saveAndNew method using extension
            PageReference pr = ext.save();
            
            List<SQX_Personnel_Document_Training__c> newPDT = [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE SQX_Personnel__c = :personnelId AND SQX_Controlled_Document__c = :documentId];
            
            System.assertEquals(1, newPDT.size(), 'Expected 1 personnel document training to be created with passed parameters.');
            
            System.assert(pr != null,
                'Expected personnel document training to be saved using passed parameters.');
            
            System.assertEquals(new ApexPages.StandardController(pdt1).view().getURL(), pr.getURL(),
                'Expected new created personnel document training record view url to be returned.');
        }
    }
}