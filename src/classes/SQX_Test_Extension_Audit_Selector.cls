/**
* Unit test for audit selector based on [SQX-1484]
* Basic test includes
* i. Creating a new audit -> Redirects to custom audit
* ii. Creating a new audit in audit program -> Redirects to audit standard layout
* iii. Editing Audit Program' audit in plan state -> Redirects to audit standard layout
* iv. Editing Audit Program' audit in approval state -> Redirects to audit standard layout
* v. Editing Audit Program' audit not in plan/approval state -> Redirects to custom audit
* vi. Editing Audit not in program in approval or plan or any other state -> Redirects to custom audit
* vii. Viewing Audit not in audit program in any state -> Redirects to custom audit
* viii. Viewing Audit Program's audit in plan or approval state -> Redirects to audit standard layout
* ix. Viewing Audit Program's audit not in plan or approval state -> Redirects to custom audit
* x. invalid audit program id throws exception
*/
@IsTest
public class SQX_Test_Extension_Audit_Selector{

    /**
    * this method checks if two url's path are same i.e. ignores the query strings
    */
    private static boolean checkIfPageReferencePathMatch(PageReference pgRef1, PageReference pgRef2){
        String  url1 = pgRef1.getUrl(),
                url2 = pgRef2.getUrl();
        Integer extractToForUrl1 = url1.indexOf('?'),
                extractToForUrl2 = url2.indexOf('?');


        url1 = url1.subString(0,  (extractToForUrl1 != -1 ? extractToForUrl1 : url1.length()));
        url2 = url2.subString(0,  (extractToForUrl2 != -1 ? extractToForUrl2 : url2.length()));

        return url1 == url2;
    }

    /**
    * Covers test scenario i and ii.
    */
    public testmethod static void givenNewAudit_RedirectionIsHandledProperly(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(user){
            //Arrange: create a program to be referenced in the test, create blank audit object and extension
            //set page to Audit Selector
            SQX_Test_Audit_Program program = new SQX_Test_Audit_Program().save();
            SQX_Audit__c  audit = new SQX_Audit__c();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit));


            // Case i.
            // Act: get the page ref from the url
            Test.setCurrentPage(Page.SQX_Audit_Editor);
            PageReference redirectTo = selector.editorURL();

            //Assert: ensure that the user was redirected to custom page
            System.assert(checkIfPageReferencePathMatch(selector.CUSTOM_AUDIT_UI, redirectTo), 'URL do not match ' + selector.CUSTOM_AUDIT_UI + ' ' + redirectTo);

            //Case ii.
            //Act: create a page ref similar to how it would be for when creating from program
            PageReference ref = Page.SQX_Audit_Editor;
            ref.getParameters().put('program_lkid', program.program.Id); // program_lkid hack used to pass the id of the program
            Test.setCurrentPage(ref);
            redirectTo = selector.editorURL();

            //Assert: ensure that the user was redirected to standar page
            System.assert(checkIfPageReferencePathMatch(redirectTo, new PageReference( '/' + SQX_Audit__c.SObjectType.getDescribe().getKeyPrefix() + '/e' )), 'URL do not match ' + redirectTo);

            System.assertEquals('1', redirectTo.getParameters().get('nooverride')); //prevent override

            
             
        }
    }


    /**
    * Covers test scenario iii, iv and v.
    */
    public testmethod static void givenEditingAuditProgramAudit_RedirectionIsHandledProperly(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        System.runas(user){
            //Arrange: create an audit belonging to audit program in plan state
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).includeInProgram(true, null)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit.audit));
            PageReference standardPage = new PageReference( '/' + audit.audit.Id + '/e' );


            // Case iii.
            // Act: get the page ref from the url
            Test.setCurrentPage(Page.SQX_Audit_Editor);
            PageReference redirectTo = selector.editorURL();

            //Assert: ensure that the user was redirected to standard page
            System.assert(checkIfPageReferencePathMatch(standardPage, redirectTo), 'URL do not match ' + selector.CUSTOM_AUDIT_UI + ' ' + redirectTo);

            //Case iv.
            //Act: set the status to in approval and get the url
            audit.setStage(SQX_Audit.STAGE_PLAN_APPROVAL);
            redirectTo = selector.editorURL();

            //Assert: ensure that the user was redirected to standard page
            System.assert(checkIfPageReferencePathMatch(standardPage, redirectTo), 'URL do not match ' + redirectTo);



            //Case iv.
            //Act: Change the stage to scheduled...void 
            String [] stagesThatRedirectToCustom = new String[] {
                SQX_Audit.STAGE_SCHEDULED,
                SQX_Audit.STAGE_CONFIRMED,
                SQX_Audit.STAGE_IN_PROGRESS,
                SQX_Audit.STAGE_CLOSED
            };

            for(String stage : stagesThatRedirectToCustom){
                audit.setStage(stage);
                redirectTo = selector.editorURL();

                //Assert: ensure that the user was redirected to custom page
                System.assert(checkIfPageReferencePathMatch(selector.CUSTOM_AUDIT_UI, redirectTo), 'URL do not match ' + redirectTo);
            }

        }
    }

    /**
    * Covers test scenario vi
    */
    public testmethod static void givenEditingAuditNotInProgram_RedirectionIsHandledProperly(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        
        System.runas(user){
            //Arrange: create an ad-hoc audit
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).setStatus(SQX_Audit.STATUS_DRAFT)
                                                                .setStage(SQX_Audit.STAGE_PLAN)
                                                                .save();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit.audit));
            
            // Case vi
            //Act: set the audit's status and get the editor redirect url
            Test.setCurrentPage(Page.SQX_Audit_Editor);
            PageReference redirectTo;

            String [] stagesThatRedirectToCustom = new String[] {
                SQX_Audit.STAGE_SCHEDULED,
                SQX_Audit.STAGE_CONFIRMED,
                SQX_Audit.STAGE_IN_PROGRESS,
                SQX_Audit.STAGE_CLOSED
            };

            for(String stage : stagesThatRedirectToCustom){
                audit.setStage(stage);
                redirectTo = selector.editorURL();

                //Assert: ensure that the user was redirected to custom page
                System.assert(checkIfPageReferencePathMatch(selector.CUSTOM_AUDIT_UI, redirectTo), 'URL do not match ' + redirectTo);
            }

        }
    }


    /**
    * Covers test scenario viii and ix
    */
    public testmethod static void givenViewingAuditProgramAudit_RedirectionIsHandledProperly(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        
        System.runas(user){
            //Arrange: create and audit i.e included in an audit program.
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser).includeInProgram(true, null)
                                                                .setStatus(SQX_Audit.STATUS_DRAFT)
                                                                .setStage(SQX_Audit.STAGE_PLAN)
                                                                .save();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit.audit));
            PageReference standardPage = new PageReference( '/' + audit.audit.Id );


            // Case viii.
            // Act: Get the redirection url for the audit
            Test.setCurrentPage(Page.SQX_Audit_Selector);
            PageReference redirectTo = selector.url();

            //Assert: ensure that the user was redirected to standard page
            System.assert(checkIfPageReferencePathMatch(standardPage, redirectTo));

            //Case viii.
            //Act: Set the status to in approval and get the view url
            audit.setStatus(SQX_Audit.STAGE_PLAN_APPROVAL);
            redirectTo = selector.url();

            //Assert: ensure that the user was redirected to standard page
            System.assert(checkIfPageReferencePathMatch(standardPage, redirectTo));



            //Case ix.
            //Act: Set stage to value other than plan and in approval and get the redirect url
            String [] stagesThatRedirectToCustom = new String[] {
                SQX_Audit.STAGE_SCHEDULED,
                SQX_Audit.STAGE_CONFIRMED,
                SQX_Audit.STAGE_IN_PROGRESS,
                SQX_Audit.STAGE_CLOSED
            };

            for(String stage : stagesThatRedirectToCustom){
                audit.setStage(stage);
                redirectTo = selector.url();

                //Assert: ensure that the user was redirected to custom page
                System.assert(checkIfPageReferencePathMatch(selector.CUSTOM_AUDIT_UI, redirectTo), 'URL do not match ' + redirectTo);
            }

        }
    }

    /**
    * Covers test scenario vii
    */
    public testmethod static void givenViewingAuditNotInProgram_RedirectionIsHandledProperly(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        
        
        System.runas(user){
            //Arrange: create an ad-hoc audit.
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                            .setStatus(SQX_Audit.STATUS_DRAFT)
                                            .setStage(SQX_Audit.STAGE_PLAN)
                                            .save();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit.audit));
            

            //Case vii.
            //Act: Set the status of the audit and get the view url
            Test.setCurrentPage(Page.SQX_Audit_Selector);
            String [] statusesThatRedirectToCustom = new String[] {
                SQX_Audit.STATUS_DRAFT,
                SQX_Audit.STATUS_COMPLETE,
                SQX_Audit.STATUS_CLOSE,
                SQX_Audit.STATUS_VOID
            };

            for(String status : statusesThatRedirectToCustom){
                audit.setStatus(status);
                PageReference redirectTo = selector.url();

                //Assert: ensure that the user was redirected to custom page
                System.assert(checkIfPageReferencePathMatch(selector.CUSTOM_AUDIT_UI, redirectTo), 'URL do not match ' + redirectTo);
            }

        }
    }


    /**
    * test scenario x smoke test
    */
    public testmethod static void givenInvalidProgramId_ErrorIsThrown(){
        User user =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(user){
            //Arrange: create an audit and an extension
            //set page to Audit Selector
            SQX_Audit__c  audit = new SQX_Audit__c();
            SQX_Extension_Audit_Selector selector = new SQX_Extension_Audit_Selector(new ApexPages.StandardController(audit));


            //Act: Set the program lookup id to invalid id and get the redirect url
            PageReference ref = Page.SQX_Audit_Editor;
            ref.getParameters().put('program_lkid', 'Invalid ID'); // program_lkid hack used to pass the id of the program
            Test.setCurrentPage(ref);
            Boolean exceptionThrown = false;
            try{
                PageReference redirectTo = selector.editorURL();
            }
            catch(Exception ex){
                exceptionThrown = true;
            }

            //Assert: ensure that an exception was thrown
            System.assertEquals(true, exceptionThrown);

        }
        
    }
    
}