/**
 * This test class ensures that the constructed dependency relationship for div to bu and bu to site for various cq custom objects are valid.
 */
@isTest
public class SQX_Test_Org_Dependency_Synchronizer {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
    }
    
     /**
      * This test method ensures that the custom value of global value set for division, business unit and site, and value set for business unit and site are valid.
      * @story SQX-3464, SQX-3462
      * @author Paras Kumar Bishwakarma
      * @date 06/19/2017
      */
    public testmethod static void ensureConstructedValueSetForPickListDependenciesAreValid(){
        
        // Act : create controller instance which will create list of all object which need to be synchronized dependency.
        SQX_Controller_OrgDependencySynchronizer controllerObj = new SQX_Controller_OrgDependencySynchronizer();
        
        // get the serialized string of list of objects
        String allObjectsToProcess = controllerObj.getObjectToProcess();

        // get the created select option for object api name
        List<SelectOption> objectNameList = controllerObj.objectNameList;

        // Assert : SelectOption should have both label and value
        for (SelectOption so : objectNameList) {
            System.assert(so.getLabel() != null && so.getValue() != null, 'Expected SelectOption list should have label and its corresponding value but label is ' + so.getLabel() + 'and value is ' + so.getValue());
        }

        // Assert : json string should be in proper json format
        try {
            List<Object> allAvailableObjectsToProcess = (List<Object>) JSON.deserializeUntyped(allObjectsToProcess);
            System.assert(allAvailableObjectsToProcess != null && allAvailableObjectsToProcess.size() > 0, 'Expected all availble objects list should not be null and empty but is '+allAvailableObjectsToProcess);
            
        } catch (Exception ex) {
            System.assert(false, 'could not deserialize json string for allObjectsToProcess. Error message : '+ex.getMessage());
        }
    }
}