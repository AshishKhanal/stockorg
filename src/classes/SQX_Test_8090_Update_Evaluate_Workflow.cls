/**
 * This unit test cases for to update evaluate workflow when complaint, SI, SD, ST and SE status is open amd stage is in progress.
 */
@isTest
public class SQX_Test_8090_Update_Evaluate_Workflow {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    /**
     * Given: given complaint record
     * When: record status is open and stage is in progress
     * Then: update evaluate workflow
     */
    testmethod static void givenComplaintRecord_WhenStatusIsOpenAndStageIsInProgress_ThenUpdateEvaluateWorkflow() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            SQX_Defect_Code__c complaintConclusion =  new SQX_Defect_Code__c(Name = 'Test Defect Code 3', 
                                                                             Active__c = true,
                                                                             Defect_Category__C = 'Test_Category3',
                                                                             Description__c = 'Test Description',
                                                                             Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
            
            //Arrange: Create complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_DRAFT);
            complaint.setStage(SQX_Complaint.STAGE_TRIAGE);
            complaint.complaint.Complaint_Title__c = 'Test Complaint';
            complaint.save();
            
            //Act: open complaint record
            complaint.setStatus(SQX_Complaint.STATUS_OPEN);
            complaint.setStage(SQX_Complaint.STAGE_INPROGRESS);
            complaint.save();
            
            //Assert: Ensured that complaint evaluate workflow should be set as true.
            complaint.complaint = [SELECT Id, Evaluate_Workflow__c FROM SQX_Complaint__c];
            System.assertEquals(true, complaint.complaint.Evaluate_Workflow__c);
            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.close();
            //Act:When complaint record is reopen
            complaint.complaint.Reason_For_Reopen__c = 'Data Correction';
            complaint.reopen();
            //Assert: Ensured that complaint evaluate workflow should be set as false.
            System.assertEquals(false, [SELECT Evaluate_Workflow__c FROM SQX_Complaint__c WHERE Id=:complaint.complaint.Id].Evaluate_Workflow__c);
        }
    }
    
    /**
     * Given: given nsi record
     * When: record status is open and stage is in progress
     * Then: update evaluate workflow
     */
    testmethod static void givenNSIRecord_WhenStatusIsOpenAndStageIsInProgress_ThenUpdateEvaluateWorkflow() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            //Arrange: Create NSI record
            SQX_Test_NSI nsi = new SQX_Test_NSI();
            nsi.setStatus(SQX_NSI.STATUS_DRAFT);
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();
            
            //Act: open NSI record
            nsi.setStatus(SQX_NSI.STATUS_OPEN);
            nsi.setStage(SQX_NSI.STAGE_IN_PROGRESS);
            nsi.save();
            
            //Assert: Ensured that NSI evaluate workflow should be set as true.
            nsi.nsi = [SELECT Evaluate_Workflow__c FROM SQX_New_Supplier_Introduction__c];
            System.assertEquals(true, nsi.nsi.Evaluate_Workflow__c);
        }
    }
    
    /**
     * Given: given SD record
     * When: record status is open and stage is in progress
     * Then: update evaluate workflow
     */
    testmethod static void givenSDRecord_WhenStatusIsOpenAndStageIsInProgress_ThenUpdateEvaluateWorkflow() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { adminUser});
        System.runAs(adminUser) {
            //Arrange: Create SD record
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation();
            sd.setStatus(SQX_Supplier_Deviation.STATUS_DRAFT);
            sd.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            sd.save();
            
            //Act: open SD record
            sd.setStatus(SQX_Supplier_Deviation.STATUS_OPEN);
            sd.setStage(SQX_Supplier_Deviation.STAGE_IN_PROGRESS);
            sd.save();
            
            //Assert: Ensured that SD evaluate workflow should be set as true.
            sd.sd = [SELECT Evaluate_Workflow__c FROM SQX_Supplier_Deviation__c];
            System.assertEquals(true, sd.sd.Evaluate_Workflow__c);
        }
    }
    
    /**
     * Given: given SE record
     * When: record status is open and stage is in progress
     * Then: update evaluate workflow
     */
    testmethod static void givenSERecord_WhenStatusIsOpenAndStageIsInProgress_ThenUpdateEvaluateWorkflow() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            // create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
            
            // Get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // Create Supplier Escalation
            SQX_Test_Supplier_Escalation se = new SQX_Test_Supplier_Escalation(accounts[0]);
            se.setStatus(SQX_Supplier_Common_Values.STATUS_DRAFT);
            se.setStage(SQX_Supplier_Common_Values.STAGE_TRIAGE);
            se.save();
            
            //Act: open SE record
            se.setStatus(SQX_Supplier_Common_Values.STATUS_OPEN);
            se.setStage(SQX_Supplier_Common_Values.STAGE_IN_PROGRESS);
            se.save();
            
            //Assert: Ensured that SE evaluate workflow should be set as true.
            se.supplierEscalation = [SELECT Evaluate_Workflow__c FROM SQX_Supplier_Escalation__c];
            System.assertEquals(true, se.supplierEscalation.Evaluate_Workflow__c);
        }
    }
    
    /**
     * Given: given ST record
     * When: record status is open and stage is in progress
     * Then: update evaluate workflow
     */
    testmethod static void givenSTRecord_WhenStatusIsOpenAndStageIsInProgress_ThenUpdateEvaluateWorkflow() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            //Arrange: Create ST record
            SQX_Test_Supplier_Interaction st = new SQX_Test_Supplier_Interaction();
            st.setStatus(SQX_Supplier_Common_Values.STATUS_DRAFT);
            st.setStage(SQX_Supplier_Common_Values.STAGE_TRIAGE);
            st.save();
            
            //Act: open ST record
            st.setStatus(SQX_Supplier_Common_Values.STATUS_OPEN);
            st.setStage(SQX_Supplier_Common_Values.STAGE_IN_PROGRESS);
            st.save();
            
            //Assert: Ensured that ST evaluate workflow should be set as true.
            st.si = [SELECT Evaluate_Workflow__c FROM SQX_Supplier_Interaction__c];
            System.assertEquals(true, st.si.Evaluate_Workflow__c);
        }
    }
}