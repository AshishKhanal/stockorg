/**
* This extension allows user to manage records related to controlled document.
*/
global with sharing class SQX_Extension_Requirement_MultiEdit {
    
    /**
    * The Id of the controlled document whose requirement is being edited
    */
    public Id recordId;

    /**
    * The pageable controller used for paging through requirements
    */
    global ApexPages.StandardSetController pagedContainer {get; set;}

    /**
    * The requirement object that will contain default values.
    */
    global SQX_Requirement__c defaultValue {get; set;}

    /**
    * The requirement object that will contain default values when refresher is updated.
    */
    global SQX_Requirement__c refresherDefaultValue {get; set;}

    /**
     * Indicates whether or not a record has been saved previously. It is useful for switching
     * the label of the 'Cancel' button
     */
    global Boolean requirementSaved {get; set;}

    /**
    * Map of requirements to save in the document
    */
    public Map<Id,WrappedRequirement> requirementsToSave = new Map<Id,WrappedRequirement>();


    /**
    * Map of requirements by job function id.
    */
    private Map<Id, SQX_Requirement__c> requirements;
    
    /**
    * List of items that is being currently worked on
    */
    global List<WrappedRequirement> dataSet {get; set;}

    /**
    * Initializes the extension of the document.
    */
    global SQX_Extension_Requirement_MultiEdit(ApexPages.StandardController controller) {
        recordId = controller.getRecord().Id;
        if(recordId != null) {
            SQX_Controlled_Document__c doc = [SELECT Duration__c FROM SQX_Controlled_Document__c WHERE Id = : recordId];
            defaultValue = new SQX_Requirement__c(Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND);
            refresherDefaultValue = new SQX_Requirement__c(Refresher_Interval__c = 365, Days_in_Advance_to_Start_Refresher__c = 30,
                                                           Days_to_Complete_Refresher__c = doc.Duration__c,
                                                           Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND);
            pagedContainer = new ApexPages.StandardSetController(Database.getQueryLocator([
                SELECT Id, Name  FROM SQX_Job_Function__c ORDER By Name
            ]));
            loadRequirements();
            buildDataset();
        }
    }
    
    /**
    * Action to move to next page of records
    */
    global void next() {
        storeChanges();
        pagedContainer.next();
        buildDataset();
    }
    
    /**
    * Action to move to previous page of records
    */
    global void previous() {
        storeChanges();
        pagedContainer.previous();
        buildDataset();
    }
    
    /**
    * Constructs the current working data set with list of job function in the current page.
    */
    private void buildDataset() {
        dataSet = new List<WrappedRequirement>();
        for(SQX_Job_Function__c jf : (List<SQX_Job_Function__c>) pagedContainer.getRecords()) {

            WrappedRequirement req;
            
            if(requirementsToSave.containsKey(jf.Id)) {
                req = requirementsToSave.get(jf.Id);
            }else {
                req = new WrappedRequirement();
                req.JobFunctionId = jf.Id;
                req.Name = jf.Name;
                req.Requirement = requirements.get(jf.Id);
                req.EnableRequirement = requirements.containsKey(jf.Id);
                req.EnableRefresher = requirements.containsKey(jf.Id) && requirements.get(jf.Id).Require_Refresher__c;
            }
            dataSet.add(req);
        }
    }
    
    /**
     * Internal method to copy fields from source to destination in null
     * @param source object to get values from
     * @param dest object to set values to
     * @param db object to check for existing values 
     */
    private void copyValuesFrom(SObject source, SObject dest, SObject db) {
        for(String field : source.getPopulatedFieldsAsMap().keyset()) {
            if(db == null || db.get(field) == null) {
                dest.put(field, source.get(field));
            }
        }
    }

    /**
    * Internal method to create a requirement that will be saved in the db.
    * @param wreq the wrapped requirement which will be used to construct the requirement
    */
    private SQX_Requirement__c createRequirement(WrappedRequirement wreq) {
        SQX_Requirement__c req = new SQX_Requirement__c();
        req.Id = wreq.Requirement != null ? wreq.Requirement.Id : null;
        if(req.Id == null){
            req.SQX_Controlled_Document__c = recordId;
            req.SQX_Job_Function__c = wreq.JobFunctionId;
            copyValuesFrom(defaultValue, req, null);
        }
        req.Require_Refresher__c = wreq.EnableRefresher;

        if(wreq.EnableRefresher) {
            copyValuesFrom(refresherDefaultValue, req, wreq.Requirement);
        }

        return req;
    }

    
    /**
    * Stores the changes in the requirement so that it can be done even when paging. 
    */
    private void storeChanges() {
        for(WrappedRequirement wreq : dataSet){
            if(requirements.containsKey(wreq.JobFunctionId)) {
                //either delete or update
                if(wreq.EnableRequirement == false ||
                 wreq.EnableRefresher != wreq.Requirement.Require_Refresher__c){
                    requirementsToSave.put(wreq.JobFunctionId, wreq);
                }
                else {
                    requirementsToSave.remove(wreq.JobFunctionId);
                }
            }
            else {
                if(wreq.EnableRequirement == true) {
                    requirementsToSave.put(wreq.JobFunctionId, wreq);
                }
                else {
                    requirementsToSave.remove(wreq.JobFunctionId);
                }
            }
        }
    }
    
    /**
     * Saves all the changes made in the transaction and displays the output.
     */
    global PageReference save() {
        requirementSaved = true;
        storeChanges();
        SQX_DB db = new SQX_DB();
        SavePoint sp = Database.setSavePoint(); 
        try {
            SQX_Requirement__c[] reqsToAdd = new List<SQX_Requirement__c>();
            SQX_Requirement__c[] reqsToUpdate = new List<SQX_Requirement__c>();
            SQX_Requirement__c[] reqsToDelete = new List<SQX_Requirement__c>();

            for(Id jfId : requirementsToSave.keySet()){
                WrappedRequirement req = requirementsToSave.get(jfId);
                if(req.EnableRequirement) {
                    if(req.Requirement == null){
                        reqsToAdd.add(createRequirement(req));
                    }
                    else if(req.EnableRefresher != req.Requirement.Require_Refresher__c){
                        reqsToUpdate.add(createRequirement(req));
                    }
                }
                else {
                    reqsToDelete.add(req.Requirement);
                }
            }
            db.op_insert(reqsToAdd, new List<SObjectField> {});
            db.op_update(reqsToUpdate, new List<SObjectField> {});
            db.op_delete(reqsToDelete);
            
            
            String message = Label.CQ_UI_DML_Summary.replace('{inserted}', String.valueOf(reqsToAdd.size()));
            message = message.replace('{updated}', String.valueOf(reqsToUpdate.size()));
            message = message.replace('{deleted}', String.valueOf(reqsToDelete.size()));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, message));

            requirementsToSave.clear();
            loadRequirements();
            buildDataset();
        }
        catch(Exception ex) {
            Database.rollBack(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        } 
        
        return null;
    }

    /**
    * This method loads the requirements present in the document.
    */
    private void loadRequirements() {
        requirements = new Map<Id, SQX_Requirement__c>();
            
        for(SQX_Requirement__c req: [SELECT Id, SQX_Job_Function__c, Level_Of_Competency__c, Refresher_Competency__c, 
                                            Require_Refresher__c, Refresher_Interval__c,
                                            Active__c, Days_to_Complete_Refresher__c, Days_in_Advance_to_Start_Refresher__c
                                    FROM SQX_Requirement__c 
                                    WHERE SQX_Controlled_Document__c = : recordId
                                    AND Deactivation_Date__c = null ]){

            requirements.put(req.SQX_Job_Function__c, req);
        }
    }

    /**
    * The class that wraps both requirement and job function name
    */
    global class WrappedRequirement {
        global Id JobFunctionId {get; set;}
        global String Name {get; set;}
        global SQX_Requirement__c Requirement {get; set;}
        global Boolean EnableRequirement {get; set;}
        global Boolean EnableRefresher {get; set;}
    }

}