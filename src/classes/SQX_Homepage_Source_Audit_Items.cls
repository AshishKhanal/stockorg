/**
* This class acts as an item source for Audit related items
* @author  Piyush Subedi
* @date    03-04-2017
*/
public with sharing class SQX_Homepage_Source_Audit_Items extends SQX_Homepage_ItemSource {

    
    /*
        Audit Items : 
            1. Open Audits Needing Response
            2. Open Actions
    */

    private final String module = SQX_Homepage_Constants.MODULE_TYPE_AUDIT;
    String auditResponsePage;  //getPageName of Audit response
    String auditPage;         // getPageName of Audit main page

    private final User loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();
    
    /*
        This is the boundary value that separates urgency for an action on this item from high to critical
        e.x. if the item's due date is less than or equal to LIMIT_FOR_CRITICALITY away from today's date, the urgency is set to High
    */
    public static final Integer LIMIT_FOR_CRITICALITY_OPENAUDITNR = 3,
                                LIMIT_FOR_CRITICALITY_OPENACTION = 3;


    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_Audit_Items(){
        super();
        auditResponsePage=SQX_Utilities.getPageName('', 'SQX_Audit_Response');
        auditPage=SQX_Utilities.getPageName('', 'SQX_Audit');
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Audit__c.SObjectType => new List<SObjectField> 
            {
                SQX_Audit__c.Name,
                SQX_Audit__c.Title__c,
                SQX_Audit__c.Status__c,
                SQX_Audit__c.CreatedDate,
                SQX_Audit__c.End_Date__c,
                SQX_Audit__c.SQX_Auditee_Contact__c,
                SQX_Audit__c.Stage__c,
                SQX_Audit__c.OwnerId
            },
            SQX_Action__c.SObjectType => new List<SObjectField>
            {
                SQX_Action__c.Name,
                SQX_Action__c.CreatedDate,
                SQX_Action__c.Due_Date__c,
                SQX_Action__c.Description__c,
                SQX_Action__c.SQX_Audit__c,
                SQX_Action__c.Status__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }


    /**
    *   returns a list of homepage items for the current user related to Audit module
    */
    protected override List<SObject> getUserRecords(){

        List<SObject> allItems = new List<SObject>();

        List<SQX_Audit__c> auditItems = [ SELECT Id, Name, Title__c, Stage__c, Status__c, CreatedDate, Start_Date__c, End_Date__c, SQX_Auditee_Contact__c,
                                                 OwnerId,
                                                (SELECT Status__c FROM SQX_Findings__r WHERE Status__c = : SQX_Finding.STATUS_OPEN LIMIT 1),

                                                (SELECT Name, CreatedDate, Due_Date__c, Description__c, SQX_Audit__r.Name, SQX_Audit__r.Title__c, SQX_Audit__c
                                                        FROM SQX_Actions__r
                                                        WHERE (SQX_User__c =: loggedInUser.Id AND Status__c =: SQX_Implementation_Action.STATUS_OPEN)
                                                        ORDER BY CreatedDate DESC)

                                            FROM SQX_Audit__c
                                            WHERE Status__c =: SQX_Audit.STATUS_COMPLETE
                                            ORDER BY CreatedDate DESC
                                        ];

        for( SQX_Audit__c adt : auditItems ){

            if( adt.SQX_Auditee_Contact__c == loggedInUser.Id &&
                adt.SQX_Findings__r.size() > 0){

                allItems.add(adt);

            }
            else if(adt.OwnerId == loggedInUser.Id &&
                    adt.Stage__c == SQX_Audit.STAGE_READY_FOR_CLOSURE)
            {
                allItems.add(adt);
            }

            if(adt.SQX_Actions__r.size() > 0){
                allItems.addAll( (List<SObject>) adt.SQX_Actions__r );
            }

        }

        return allItems;
    }




    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item){

        SQX_Homepage_Item auditItem;

        if(item instanceOf SQX_Audit__c){
            
            SQX_Audit__c audit = (SQX_Audit__c) item;

            if( audit.Stage__c == SQX_Audit.STAGE_READY_FOR_CLOSURE){

                auditItem = getAuditToBeClosedItem(audit);

            } else {

                auditItem = getOpenAuditNRItem(audit);

            }
            
        }else if(item instanceOf SQX_Action__c){

            auditItem = getOpenActionItem( (SQX_Action__c) item);

        }else{
            System.assert(false, 'Unknown item :' + item);
        }

        return auditItem;
    }




    /**
    *   Method returns a Open Audit- NR type SQX_Homepage_Item item from the given audit record
    *   @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getOpenAuditNRItem(SQX_Audit__c item){

        SQX_Homepage_Item openAuditNRItem = new SQX_Homepage_Item();

        openAuditNRItem.itemId = item.Id;

        // set action badge
        openAuditNRItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDING_RESPONSE;

        openAuditNRItem.createdDate = Date.valueOf(item.CreatedDate);

        // set due date
        openAuditNRItem.dueDate = item.End_Date__c;

        // set urgency
        openAuditNRItem.urgency = this.getItemUrgency(openAuditNRItem.dueDate, LIMIT_FOR_CRITICALITY_OPENAUDITNR);

        // set module badge for the current item
        openAuditNRItem.moduleType = this.module;

        // set actions for the current item
        openAuditNRItem.actions = getActionsForOpenAuditsNR(openAuditNRItem.itemId);

        // set feed text for the current item
        openAuditNRItem.feedText = getOpenAuditNRFeedText(item);

        // set user details of the user related to this item
        openAuditNRItem.creator = loggedInUser;

        return openAuditNRItem;
    }

    /**
    *   Method returns an Audit to be Closed type SQX_Homepage_Item item from the given audit record
    *   @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getAuditToBeClosedItem(SQX_Audit__c item){

        SQX_Homepage_Item auditToBeClosedItem = new SQX_Homepage_Item();

        auditToBeClosedItem.itemId = item.Id;

        // set action badge
        auditToBeClosedItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;

        auditToBeClosedItem.createdDate = Date.valueOf(item.CreatedDate);

         // set module badge for the current item
        auditToBeClosedItem.moduleType = this.module;

        // set actions for the current item
        auditToBeClosedItem.actions = getActionsForAuditToBeClosed(auditToBeClosedItem.itemId);

        // set feed text for the current item
        auditToBeClosedItem.feedText = getAuditToBeClosedFeedText(item);

        // set user details of the user related to this item
        auditToBeClosedItem.creator = loggedInUser;

        return auditToBeClosedItem;

    }


    /**
    *   Method returns a Open Action-Audit type SQX_Homepage_Item item from the given Action record
    *   @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getOpenActionItem(SQX_Action__c item){

        SQX_Homepage_Item openActionItem = new SQX_Homepage_Item();

        openActionItem.itemId = item.Id;

        // set action type
        openActionItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_COMPLETE;

        openActionItem.createdDate = Date.valueOf(item.CreatedDate);

        // set due date
        openActionItem.dueDate = item.Due_Date__c;

        // set urgency
        openActionItem.urgency = this.getItemUrgency(openActionItem.dueDate, LIMIT_FOR_CRITICALITY_OPENACTION);

        // set module type
        openActionItem.moduleType = this.module;

        // set actions for the current item
        String recordId = (String) item.SQX_Audit__c;
        openActionItem.actions = getActionsForOpenActionItems(recordId);


        // set feed text for the current item
        openActionItem.feedText = getOpenActionFeedText(item);

        // set user details of the user related to this item
        openActionItem.creator = loggedInUser;

        return openActionItem;
    }


    /**
    *   Method returns a list of actions associated with the open audits needing response item type    
    *   @param itemId - id of the record under consideration
    */
    private List<SQX_Homepage_Item_Action> getActionsForOpenAuditsNR(String itemId){

        List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

        actions.add(getRespondActionForAudit(itemId));

        return actions;
    }


    /**
    *   Method returns a list of actions associated with the open actions item type for audit
    *   @param itemId - id of the record under consideration
    */
    private List<SQX_Homepage_Item_Action> getActionsForOpenActionItems(String itemId){

        List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

        actions.add(getRespondActionForAudit(itemId));

        return actions;
    }

    /**
    *   Method returns a list of actions associated with audit to be closed
    *   @param itemId - id of the record under consideration
    */
    private List<SQX_Homepage_Item_Action> getActionsForAuditToBeClosed(String itemId){

        List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

        actions.add(getCloseActionForAudit(itemId));

        return actions;
    }

    /**
    *   Returns the respond action type for audit
    *   @param id - id of the record for which action is to be set
    *   @param urlSuffix - adding URL string to be added to the action url for Audit response
    */
    private SQX_Homepage_Item_Action getRespondActionForAudit(String id){

        // adding action - Respond
        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_RESPOND;

        PageReference pr = new PageReference(auditResponsePage);
        pr.getParameters().put('Id',id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_REPLY;

        action.supportsBulk = false;

        return action;
    }

    /**
    *   Returns the respond action type for audit
    *   @param id - id of the record for which action is to be set
    */
    private SQX_Homepage_Item_Action getCloseActionForAudit(String id){

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_CLOSE;

        PageReference pr = new PageReference('/' + id);
        pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_CLOSE;

        action.supportsBulk = false;

        return action;
    }

    /**
    *   Method returns the open audit needing response type feed text for the given item
    */
    private String getOpenAuditNRFeedText(SQX_Audit__c item){

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_RESPOND;
        return String.format(template, new String[] { item.Name, item.Title__c } );
    }

    /**
    *   Method returns the open action type feed text for the given item
    */
    private String getOpenActionFeedText(SQX_Action__c item){

        String feed;

        String shortDesc = this.getShortenedDescription(item.Description__c);

        if(String.isBlank(shortDesc)){

            feed = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITHOUT_SUBJECT,
                                 new String[] {
                                     item.SQX_Audit__r.Name,
                                     item.SQX_Audit__r.Title__c   
                                 }
                                );

        }else{

            feed = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_ACTION_WITH_SUBJECT,
                                 new String[] {
                                     shortDesc,
                                     item.SQX_Audit__r.Name,
                                     item.SQX_Audit__r.Title__c   
                                 }
                                );
        }

        return feed;
    }

    /**
    *   Method returns the ready for closure audit needing close type feed text for the given item
    */
    private String getAuditToBeClosedFeedText(SQX_Audit__c item){
        
        String template = SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS;
        return String.format(template, new String[] { item.Name, item.Title__c } );
    }

}