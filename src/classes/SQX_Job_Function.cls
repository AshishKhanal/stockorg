/**
* Extension for Job Function object
*/
public with sharing class SQX_Job_Function {

    // Batch Job Statuses
    public static final String  BATCH_JOB_STATUS_ACTIVATION_MIGRATION_COMPLETED = '7.4 Migration Completed',
                                BATCH_JOB_STATUS_ACTIVATION_MIGRATION_FAILED = '7.4 Migration Failed',
                                BATCH_JOB_STATUS_DEFAULT = 'Created 7.4 Onwards';

    /**
    * Used to by pass trigger when Record is migrated
    */
    private static Boolean value_BypassBulkifiedTrigger = false;
    public static Boolean bypassBulkifiedTrigger {
        get { return value_BypassBulkifiedTrigger; }
        set { value_BypassBulkifiedTrigger = value; }
    }

    /**
    * returns active users related to job functions using active personnel job functions
    * @param  jobFunctionIds job function ids
    * @param  filter         filter to apply when querying records
    * @param  excludeFields  fields to exclude from filter
    * @return                returns active users for each job function ids
    */
    public static Map<Id, List<User>> getActiveUsersForJobFunctions(Set<Id> jobFunctionIds, SQX_DynamicQuery.Filter filter, Set<String> excludeFields) {
        
        List<SQX_Personnel_Job_Function__c> pjfs = new PrivilegeEscalator().getUsersForJobFunctions(jobFunctionIds, filter, excludeFields);

        // map to list active users for each job function
        Map<Id, List<User>> result = new Map<Id, List<User>>();
        
        // return all job function ids
        for (Id jfId : jobFunctionIds) {
            result.put(jfId, new List<User>());
        }
        
        // populate active users for each job function
        for (SQX_Personnel_Job_Function__c pjf : pjfs) {
            result.get(pjf.SQX_Job_Function__c).add(pjf.SQX_Personnel__r.SQX_User__r);
        }
        
        return result;
    }
    
    /**
    * returns active users related to a job function
    * @param  jobFunctionId job function id
    * @param  filter        filter to apply when querying records
    * @param  excludeFields fields to exclude from filter
    * @return               returns active users if job function id is <code>null</code>, else returns active users related to the job function
    */
    public static List<SQX_JobFunctionUser_Wrapper> getActiveUsers(Id jobFunctionId, SQX_DynamicQuery.Filter filter) {
        List<SQX_JobFunctionUser_Wrapper> result = new List<SQX_JobFunctionUser_Wrapper>();
        List<User> activeUsers;
        Set<String> excludeFields = new Set<String>{ 'JobFunctionId' }; // JobFunctionId property of SQX_JobFunctionUser_Wrapper
        
        if (jobFunctionId == null) {
            // dynamic query to list active users
            String soqlQuery = 'SELECT Id, Name FROM User WHERE IsActive = true ';
            if (filter != null) {
                String filterString = filter.toString(User.getSObjectType().getDescribe().fields.getMap(), '', excludeFields);
                if (!String.isBlank(filterString))
                    soqlQuery += 'AND ' + filterString;
            }
            soqlQuery += ' ORDER BY Name LIMIT 200';
            System.debug('Dynamic query for null jobFunctionId: ' + soqlQuery);
            
            // get active users
            activeUsers = Database.query(soqlQuery);
        } else {
            // get active users
            activeUsers = getActiveUsersForJobFunctions(new Set<Id>{ jobFunctionId }, filter, excludeFields).values().get(0);
        }
        
        for (User au : activeUsers) {
            SQX_JobFunctionUser_Wrapper uw = new SQX_JobFunctionUser_Wrapper();
            uw.Id = au.Id;
            uw.Name = au.Name;
            uw.JobFunctionId = jobFunctionId;
            result.add(uw);
        }
        
        return result;
    }

    /**
     * This method sets activation and deactivation dates for gieven active jf
     * @param jobFunction record in which field value is to be set
     */
    public static void onJFActivation(SQX_Job_Function__c jobFunction) {
        jobFunction.Activation_Date__c = System.today();
        jobFunction.Deactivation_Date__c = null;
    }

    /**
     * This method sets deactivation date for given inactive jf
     * @param jobFunction record in which field value is to be set
     */
    public static void onJFDeactivation(SQX_Job_Function__c jobFunction) {
        jobFunction.Deactivation_Date__c = System.today();
    }
    
    /**
    * Wrapper class to relate user linked with a job function
    */
    public with sharing class SQX_JobFunctionUser_Wrapper {
        /**
        * User Id
        */
        public Id Id { get; set; }
        /**
        * User Name
        */
        public String Name { get; set; }
        /**
        * Job Function Id
        */
        public Id JobFunctionId { get; set; }
        /**
        * default constructor
        */
        public SQX_JobFunctionUser_Wrapper() { }
    }

    /**
     * Allows user to get select reviewers with particular personnel job function, if it is private.
     * WITHOUT Sharing Used
     * --------------------
     * Use case : Personnel object is kept private to keep data related to personnel such as certifications private
     *            However, public data related to personnel such as job function also becomes inaccessible.
     *            Escalation is required to access the object
     */
    private without sharing class PrivilegeEscalator {
        public List<SQX_Personnel_Job_Function__c> getUsersForJobFunctions(Set<Id> jobFunctionIds, SQX_DynamicQuery.Filter filter, Set<String> excludeFields) {
            // dynamic query to list active users for job functions
            String soqlQuery = 'SELECT Id, SQX_Job_Function__c, SQX_Personnel__r.SQX_User__r.Id, SQX_Personnel__r.SQX_User__r.Name '
                            + 'FROM SQX_Personnel_Job_Function__c '
                            + 'WHERE SQX_Job_Function__c = :jobFunctionIds AND Active__c = true AND SQX_Personnel__r.SQX_User__r.IsActive = true ';
            if (filter != null) {
                String filterString = filter.toString(User.getSObjectType().getDescribe().fields.getMap(), 'SQX_Personnel__r.SQX_User__r', excludeFields);
                if (!String.isBlank(filterString))
                    soqlQuery += 'AND ' + filterString;
            }
            soqlQuery += ' ORDER BY SQX_Job_Function__r.Name, SQX_Personnel__r.SQX_User__r.Name';
            System.debug('Dynamic query for jobFunctionIds: ' + soqlQuery);

            // get all active users related to job fucntions with active personnel job functions
            return Database.query(soqlQuery);
        }
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified() {
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newReq equivalent to trigger.new in salesforce
        * @param oldMap equivalent to trigger.oldMap in salesforce
        */
        public Bulkified(List<SQX_Job_Function__c> newReq, Map<Id, SQX_Job_Function__c> oldMap) {
            super(newReq, oldMap);
        }
        
        /**
         * This method updates the activation date of job function when job function is activated
         */
        public Bulkified updateActivationAndDeactivationDate() {

            // By pass trigger in case of Migration
            if(bypassBulkifiedTrigger){
                return this;
            }

            this.resetView();
            
            for (SQX_Job_Function__c jf : (List<SQX_Job_Function__c>)this.view) {
                SQX_Job_Function__c jfOld = (SQX_Job_Function__c)this.oldValues.get(jf.Id);
                
                if (jf.Active__c == true && (jfOld == null || jf.Active__c != jfOld.Active__c)) {
                    
                    // set dates when new active JF is created or existing JF is activated
                    SQX_Job_Function.onJFActivation(jf);

                } else if (jf.Active__c == false && jfOld != null && jf.Active__c != jfOld.Active__c) {
                    
                    // set deactivation dates when active JF is deactivated
                    SQX_Job_Function.onJFDeactivation(jf);
                }
            }
            
            return this;
        }

        /**
         * This method activates pjf and requirements when corresponding job functions are activated
         */
        public Bulkified activatePJFsAndRequirements() {
            
            // By pass trigger in case of Migration
            if(bypassBulkifiedTrigger){
                return this;
            }

            final string ACTION_NAME = 'activatePJFsAndRequirements';
            Rule activeJFRule = new Rule();
            activeJFRule.addRule(Schema.SQX_Job_Function__c.Active__c, RuleOperator.Equals, true);
            this.resetView()
                .removeProcessedRecordsFor(SQX.JobFunction, ACTION_NAME)
                .applyFilter(activeJFRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            List<SQX_Job_Function__c> activeJFs = activeJFRule.evaluationResult;
            if (activeJFs.size() > 0) {
                this.addToProcessedRecordsFor(SQX.JobFunction, ACTION_NAME, activeJFs);

                List<SQX_Job_Function__c> jfList = [ SELECT Id, 
                                                        ( 
                                                            SELECT Id 
                                                            FROM SQX_Personnel_Job_Functions__r 
                                                            WHERE Active__c = false 
                                                                AND Deactivation_Date__c = null 
                                                                AND SQX_Personnel__r.Active__c = true
                                                        ), 
                                                        ( 
                                                            SELECT Id 
                                                            FROM SQX_Requirements__r 
                                                            WHERE Active__c = false 
                                                                AND Deactivation_Date__c = NULL 
                                                                AND SQX_Controlled_Document__r.Document_Status__c IN :SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING
                                                            ORDER BY Training_Program_Step_Internal__c ASC
                                                        ) 
                                                    FROM SQX_Job_Function__c WHERE Id IN : activeJFs];


                // update pjfs and djfs of jfList
                updateRecordsInCluster(jfList, true);
            }
            
            return this;
        }
        
        /**
         * This method deactivates pjfs and requirements when corresponding job functions are deactivated
         */
        public Bulkified deactivatePJFsAndRequirements() {
            final string ACTION_NAME = 'deactivatePJFsAndRequirements';
            
            Rule deactiveJFRule = new Rule();
            deactiveJFRule.addRule(Schema.SQX_Job_Function__c.Active__c, RuleOperator.Equals, false);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.JobFunction, ACTION_NAME)
                .applyFilter(deactiveJFRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            List<SQX_Job_Function__c> inactiveJFs = deactiveJFRule.evaluationResult;
            if (inactiveJFs.size() > 0) {
                this.addToProcessedRecordsFor(SQX.JobFunction, ACTION_NAME, inactiveJFs);

                List<SQX_Job_Function__c> jfList = [ SELECT Id, 
                                                        ( 
                                                            SELECT Id 
                                                            FROM SQX_Personnel_Job_Functions__r 
                                                            WHERE Active__c = true 
                                                                AND Activation_Date__c != null 
                                                                AND Deactivation_Date__c = null
                                                        ), 
                                                        ( 
                                                            SELECT Id 
                                                            FROM SQX_Requirements__r 
                                                            WHERE Active__c = true 
                                                                AND Activation_Date__c != null 
                                                                AND Deactivation_Date__c = null
                                                        ) 
                                                    FROM SQX_Job_Function__c WHERE Id IN : inactiveJFs];

                
                // update pjfs and djfs of jfList
                updateRecordsInCluster(jfList, false);
            }
            
            return this;
        }

        /**
         * Internal common method to update pjf and requirements in cluster
         * @param jfList list of jobfunction whose child pjfs and djfs are to be updated
         * @param val Boolean value that needs to be set to pjfs and djfs
         */
        private void updateRecordsInCluster(List<SQX_Job_Function__c> jfList, Boolean val) {

            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            List<SQX_Requirement__c> requirements = new List<SQX_Requirement__c>();

            for (SQX_Job_Function__c jf : jfList) {
               for (SQX_Personnel_Job_Function__c pjf : jf.SQX_Personnel_Job_Functions__r) {
                    pjf.Active__c = val;
                } 

                for (SQX_Requirement__c req : jf.SQX_Requirements__r) {
                    req.Active__c = val;
                }

                pjfs.addAll(jf.SQX_Personnel_Job_Functions__r);
                requirements.addAll(jf.SQX_Requirements__r);
            }

            Map<SObjectType, List<SObject>> objsToUpdate = new Map<SObjectType, List<sObject>>();
            Map<SObjectType, List<Schema.SObjectField>> fieldsUpdated = new Map<SObjectType, List<Schema.SObjectField>>();
            if (pjfs.size() > 0) {
                SObjectType objType = pjfs[0].getSObjectType();
                objsToUpdate.put(objType, pjfs);
                fieldsUpdated.put(objType, new List<Schema.SObjectField> {SQX_Personnel_Job_Function__c.Active__c});
            }
            if (requirements.size() > 0) {
                SObjectType objType = requirements[0].getSObjectType();
                objsToUpdate.put(objType, requirements);
                fieldsUpdated.put(objType, new List<Schema.SObjectField> {SQX_Requirement__c.Active__c}); 
            }                
            
            // update objects using clustered object
            new SQX_DB().op_update(objsToUpdate, fieldsUpdated);
        }
    }
}