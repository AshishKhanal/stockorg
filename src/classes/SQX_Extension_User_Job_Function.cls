public with sharing class SQX_Extension_User_Job_Function {
    
    public SQX_Extension_User_Job_Function(ApexPages.StandardController controller) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'User Job Function object is obsolete. Please use Personnel and Personnel Job Function.'));
    }
    
    /**
    * logic removed for obsolete object
    */
    public PageReference saveNewRecord(Boolean isSaveAndNew) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'User Job Function object is obsolete. Please use Personnel and Personnel Job Function.'));
        
        return null;
    }
    
    /**
    * logic removed for obsolete object
    */
    public PageReference save() {
        return saveNewRecord(false);
    }
    
    /**
    * logic removed for obsolete object
    */
    public PageReference saveAndNew() {
        return saveNewRecord(true);
    }
    
}