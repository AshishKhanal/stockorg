/**
* Unit tests for long text history
* Note: Finding object is used for this unit test methods
*/
@IsTest
public class SQX_Test_8211_View_Long_Text_History {
    
    /**
    * constants
    */
    final static String FINDING_HISTORY_SOBJ_TYPE_NAME = 'compliancequest__SQX_Finding__History',
        FINDING_DESCRIPTION_FIELD_NAME = 'compliancequest__Description__c';
    
    /**
    * common setup for all test methods
    */
    @testSetup
    static void commonSetup() {
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'adminUser');
        User stdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'stdUser');
    }
    
    /**
    * ensures changed by field is required in long text history object
    * ensures record index field is auto populated when long text history object is created
    */
    @isTest
    static void givenCreateLongTextHistoryWithoutChangedBy_ErrorThrown() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('stdUser')) {
            // ARRANGE: create long text history object without setting Changed By field
            List<SQX_Long_Text_History__c> objs = new List<SQX_Long_Text_History__c>();
            objs.add(new SQX_Long_Text_History__c(
                Parent_Record_Id__c = 'fakeId',
                Field_Name__c = 'fakeField',
                Changed_On__c = System.now()
            ));
            
            // ACT: save record
            Database.SaveResult sr = new SQX_DB().continueOnError().op_insert(objs, new List<SObjectField>()).get(0);
            
            // ASSERT: validation error occurred
            String expErrMsg = 'Changed By is required';
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg);
            
            
            // ARRANGE: Changed By field is set
            objs[0].SQX_Changed_By__c = UserInfo.getUserId();
            
            // ACT: save record
            sr = new SQX_DB().continueOnError().op_insert(objs, new List<SObjectField>()).get(0);
            
            // ASSERT: record is saved without error
            System.assert(sr.isSuccess(), 'Expected long text history record to be saved.');
            // ASSERT: Record Index field is auto populated
            System.assert(String.isNotBlank([SELECT Record_Index__c FROM SQX_Long_Text_History__c WHERE Id = :objs[0].Id].Record_Index__c),
                'Expected Record_Index__c field is auto populated when record is created.');
        }
    }
    
    /**
    * ensures existing long text history object cannot be modified
    */
    @isTest
    static void givenCreateLongTextHistory_WhenValuesAreModified_ErrorThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        
        System.runas(users.get('adminUser')) {
            // ARRANGE: create long text history object without setting Changed By field
            List<SQX_Long_Text_History__c> objs = new List<SQX_Long_Text_History__c>();
            objs.add(new SQX_Long_Text_History__c(
                Parent_Record_Id__c = 'fakeId',
                Field_Name__c = 'fakeField',
                Changed_On__c = System.now(),
                SQX_Changed_By__c = UserInfo.getUserId(),
                Old_Value__c = 'oldVal'
            ));
            
            // ACT: save record
            Database.SaveResult sr = new SQX_DB().continueOnError().op_insert(objs, new List<SObjectField>()).get(0);
            
            
            // ARRANGE: values modified
            SQX_Long_Text_History__c objToUpdate = new SQX_Long_Text_History__c(
                Id = objs[0].Id,
                Parent_Record_Id__c = 'anotherId',
                Field_Name__c = 'anotherField',
                Changed_On__c = System.now().addHours(1),
                SQX_Changed_By__c = users.get('stdUser').Id,
                Old_Value__c = 'anotherVal'
            );
            
            // ACT: save record
            sr = Database.update(objToUpdate, false);
            
            // ASSERT: validation error occurred
            String expErrMsg = 'Modification of history record is not allowed';
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg);
        }
    }
    
    /**
    * Given:
    *   long text field history tracking is enabled properly
    *   a finding record is created with a description by a user
    * When:
    *   the finding description is update by another user (only case changed)
    * Then:
    *   Long Text History is captured
    *   View Long Text History loads proper detail for the provided audit trail record
    *   and the page shows proper error messages when invalid params are passed or no audit trail record/long text history record found
    */
    @isTest
    static void givenObjectFieldHistoryIsEnabled_ModifyALongTextField_LongTextHistoryIsCaptured() {
        // enable field tracking query for this test case
        SQX_BulkifiedBase.skipFieldTrackingQueryForUnitTests = false;
        
        if (!SQX_BulkifiedBase.getTrackedLongTextFields(SQX_Finding__c.SObjectType.getDescribe().getName()).contains(SQX_Finding__c.Description__c.getDescribe().getName())) {
            // bypass this test since track history is not enabled for finding description field
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User stdUser = users.get('stdUser');
        User adminUser = users.get('adminUser');
        
        final String INITIAL_DESC = 'initial description';
        SQX_Finding__c finding = new SQX_Test_Finding().finding;
        
        System.runas(stdUser) {
            // ARRANGE: create new finding with initial description
            finding.Description__c = INITIAL_DESC;
            insert finding;
            
            System.runas(adminUser) {
                // ARRANGE: Update finding description (only chaning case to upper) by another user
                finding.Description__c = INITIAL_DESC.toUpperCase();
                update finding;
            }
            
            // ARRANGE: mock finding history object
            // Note: history object are not created in unit tests
            String historyJson = '{"attributes":{"type":"' + FINDING_HISTORY_SOBJ_TYPE_NAME + '"},"ParentId":"' + finding.Id
                                 + '","Field":"' + FINDING_DESCRIPTION_FIELD_NAME + '","CreatedById":"' + adminUser.Id
                                 + '","CreatedDate":' + JSON.serialize(System.now()) + ',"Id":"000000000000000AAA"}';
            SObject fakeHistoryObj = (SObject)JSON.deserialize(historyJson, SObject.class);
            
            // ARRANGE: set view long text history page as current and add required url params values
            Test.setCurrentPage(Page.SQX_AuditTrail_ViewLongTextHistory);
            ApexPages.currentPage().getParameters().put(SQX_Controller_AuditTrailLongTextHistory.URL_PARAM_HISTORY_ID, fakeHistoryObj.Id);
            ApexPages.currentPage().getParameters().put(SQX_Controller_AuditTrailLongTextHistory.URL_PARAM_OBJECT_TYPE, FINDING_HISTORY_SOBJ_TYPE_NAME);
            
            // ACT: instantiate view long text history page controller when user does not have view audit trail permission
            SQX_Controller_AuditTrailLongTextHistory cntlr = new SQX_Controller_AuditTrailLongTextHistory();
            
            // ASSERT: check error message for insufficient permission
            System.assertEquals(true, SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_USER_DOES_NOT_HAVE_VIEW_AUDIT_TRAIL_PERMISSION),
                'Expected page error message: "' + Label.SQX_ERR_MSG_USER_DOES_NOT_HAVE_VIEW_AUDIT_TRAIL_PERMISSION);
            
            
            // ARRANGE: bypass permission check for unit test
            cntlr.userHasViewAuditTrailPermission = true;
            
            // ACT: execute methods to load url params
            cntlr.initHistoryObjectFromUrlParams();
            
            // ASSERT: check valid history object type
            System.assertEquals(FINDING_HISTORY_SOBJ_TYPE_NAME, cntlr.historyObjectTypeName, 'Expected cntlr.historyObjectTypeName is set properly');
            // ASSERT: check error message for fake history id
            System.assertEquals(true, SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_AUDIT_TRAIL_RECORD_NOT_AVAILABLE),
                'Expected page error message: "' + Label.SQX_ERR_MSG_AUDIT_TRAIL_RECORD_NOT_AVAILABLE);
            
            
            // ARRANGE: set fake history object in the controller
            cntlr.historyObj = fakeHistoryObj;
            
            // ACT: execute method to load long text history
            cntlr.initLongTextHistoryRecords();
            
            // ASSERT: check parent record object type is identified
            System.assertEquals(SQX_Finding__c.SObjectType.getDescribe().getLabel(), cntlr.parentObjectTypeLabel, 'Expected cntlr.parentObjectTypeLabel is set properly');
            // ASSERT: long text history object is created
            System.assertNotEquals(null, cntlr.longTextHistoryObj, 'Expected long text history is created and cntlr.longTextHistoryObj is set');
            // ASSERT: modified date is captured in long text history object
            System.assertNotEquals(null, cntlr.longTextHistoryObj.Changed_On__c, 'Expected changed on value is captured in long text history object');
            // ASSERT: changed by is captured in long text history object
            System.assertEquals(adminUser.Id, cntlr.longTextHistoryObj.SQX_Changed_By__c, 'Expected changed by is properly captured in long text history object');
            // ASSERT: old value is captured in long text history object
            System.assertEquals(INITIAL_DESC, cntlr.longTextHistoryObj.Old_Value__c, 'Expected old value to be properly captured in long text history object');
            
            
            // ARRANGE: mock and set finding history object for which no long text history has been captured
            historyJson = '{"attributes":{"type":"' + FINDING_HISTORY_SOBJ_TYPE_NAME + '"},"ParentId":"' + finding.Id
                          + '","Field":"' + FINDING_DESCRIPTION_FIELD_NAME + '","CreatedById":"' + stdUser.Id
                          + '","CreatedDate":' + JSON.serialize(System.now()) + ',"Id":"000000000000000AAA"}';
            cntlr.historyObj = (SObject)JSON.deserialize(historyJson, SObject.class);
            
            
            // ACT: execute method to load long text history
            cntlr.initLongTextHistoryRecords();
            
            // ASSERT: check error message for not matched long text history
            System.assertEquals(true, SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_AUDIT_TRAIL_LONG_TEXT_HISTORY_NOT_AVAILABLE),
                'Expected page error message: "' + Label.SQX_ERR_MSG_AUDIT_TRAIL_LONG_TEXT_HISTORY_NOT_AVAILABLE);
        }
    }
}