/* Test for NC When Part is entered lot quantity and lot number is required */
@isTest
public class SQX_Test_1565_NC_Part{
        static boolean runAllTests = true,
                    run_givenAUser_NCPartIsGiven_NCCanBeSavedWithLotQtyAndLotNo = false;
    /*                
    * this test ensures when the NC has part
    * and if part has lot number and lot quantity 
    * theb NC can be saved
    */               
    public testmethod static void givenAUser_NCPartIsGiven_NCCanBeSavedWithLotQtyAndLotNo(){

        if(!runAllTests && !run_givenAUser_NCPartIsGiven_NCCanBeSavedWithLotQtyAndLotNo){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){

            departmentResearch = new SQX_Department__c(
                                         Name='Research1'
                                        );
            insert departmentResearch;
        }

        System.runas(standardUser){

            SQX_Nonconformance__c nonConformance = new SQX_Nonconformance__c (
                                            Type__c = SQX_NC.TYPE_INTERNAL,
                                            Type_Of_Issue__c = SQX_NC.ISSUE_TYPE_PRODUCT,
                                            Disposition_Required__c = true,
                                            Due_Date_Disposition__c = Date.today(),
                                            Occurrence_Date__c = Date.Today(),
                                            SQX_Department__c = departmentResearch.Id,
                                            Status__c = SQX_NC.STATUS_DRAFT);
            insert nonConformance;

            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(
                                                    Name = 'Random Part Family');

            insert partFamily;

            SQX_Part__c part = new SQX_Part__c(
                                Name = 'Random part',
                                Part_Number__c = 'PART-01',
                                Part_Risk_Level__c = 1,
                                Part_Family__c = partFamily.Id);

            insert part;

            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(
                                                                SQX_Impacted_Part__c = part.Id,
                                                                Lot_Number__c = 'LOT-12345',
                                                                Lot_Quantity__c = 10,
                                                                SQX_Nonconformance__c = nonConformance.Id);

            Database.SaveResult impactedProductSave = Database.insert(impactedProduct, false);

            System.assert(impactedProductSave.isSuccess(), 'Expected the impacted part to be saved' + impactedProductSave);
            

            SQX_NC_Impacted_Product__c impactedProduct1 = new SQX_NC_Impacted_Product__c(
                                                                SQX_Impacted_Part__c = part.Id,
                                                                Lot_Number__c = 'LOT-12345',
                                                                Lot_Quantity__c = 10,
                                                                SQX_Nonconformance__c = nonConformance.Id);

            Database.SaveResult impactedProductSave1 = Database.insert(impactedProduct1, false);

            System.assert(!impactedProductSave1.isSuccess(), 'Expected the impacted part not to be saved');

        }
    }
}