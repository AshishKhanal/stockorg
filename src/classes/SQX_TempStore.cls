/**
* @author Pradhanta Bhandari
* @date 2014/1/28
* @description used to store and retrieve temporary values from the data store 
*/
public with sharing class SQX_TempStore{

    public String baseObject { get; set; }
    public String deltaObject { get; set; }

    private static final String JSON_MIME_TYPE = 'application/json';

    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description used to save the temporary value of an object to data store. If the value doesn't 
    * exists one is created else overwritten 
    * @param objectType the type of object for which the store is created
    * @param relatedObjectID the parent object for which this temporary data is being created
    * @param valueToStore the JSON data to store
    */
    public static SQX_TempStore__c  storeObject(String objectType, String relatedObjectID, String title, String baseValueToStore, String deltaValueToStore){
        //once access token is set the record is not modified.
        List<SQX_TempStore__c> entry = [SELECT ID, Base_Object_Attachment_ID__c, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID AND
                                  Access_Key__c =: null AND Type__c = : objectType];
        SQX_TempStore__c  Data = null;
        
        if(entry.size() == 0){
            //insert new record
            
            data = new SQX_TempStore__c();
            data.Related_Object_ID__c = relatedObjectID;
            data.Base_Object_Attachment_ID__c = data.Delta_Attachment_Id__c = null;
            data.Title__c = title;
            data.Type__c = objectType; 
            
            new SQX_DB().op_insert(new List<SObject>{data}, new List<SObjectField>{
                SQX_TempStore__c.fields.Related_Object_ID__c,
                SQX_TempStore__c.fields.Base_Object_Attachment_ID__c,
                SQX_TempStore__c.fields.Delta_Attachment_Id__c,
                SQX_TempStore__c.fields.Title__c,
                SQX_TempStore__c.fields.Type__c
            });
        }
        else{
            data = entry.get(0);
        }
        
        if(baseValueToStore != null){
            data.Base_Object_Attachment_ID__c = upsertAttachment(data.Id, data.Base_Object_Attachment_ID__c, baseValueToStore);
        }
        
        if(deltaValueToStore != null){
            System.assert(data.Base_Object_Attachment_ID__c != null,
                'Expected base object to be set before delta but found it missing');
            data.Delta_Attachment_Id__c = upsertAttachment(data.Id, data.Delta_Attachment_Id__c, deltaValueToStore);
        }

        new SQX_DB().op_update(new List<SObject>{data}, new List<SObjectField>{
            SQX_TempStore__c.fields.Base_Object_Attachment_ID__c,
            SQX_TempStore__c.fields.Delta_Attachment_Id__c
        });
        
        return data;
    }

    /**
    * stores/upserts a temp store object for a related object, with base value and delta value without title
    */
    public static SQX_TempStore__c  storeObject(String objectType, String relatedObjectID, String baseValueToStore, String deltaValueToStore){
        return storeObject(objectType, relatedObjectID, '', baseValueToStore, deltaValueToStore);
    }

    public static SQX_TempStore__c getStoreFor(String relatedObjectID){
        
        List<SQX_TempStore__c> stores = [SELECT Related_Object_ID__c, Title__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = :relatedObjectID AND
                                  Access_Key__c =: null];
                                  
        return stores.size() == 0 ? null : stores.get(0);
    }
    
    /**
    * returns all the stores that match the given related object id like for a user
    * the array has relaetd_object_id and title only, because that is what should be used to load
    * any data
    * @param objectType type of object for which the store was created
    */
    public static List<SQX_TempStore__c> getStoresOfType(String objectType){
        
        return [SELECT Related_Object_ID__c, Title__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Type__c = :objectType AND
                                  Access_Key__c =: null];
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description updates/inserts an attachment for parent with parent ID with valueToStore
    * @param parentID the id of parent for which this attachment is to be created
    * @param attachmentId the id of the attachment if it is to be updated else null, which will result in an insertion
    * @param valueToStore the value of the data to store
    * @return always returns the Id of the attachment either update or inserted
    */
    private static Id upsertAttachment(Id parentID, String attachmentId, String valueToStore){
        Attachment att = null;
        SQX_DB.DBOperation operation = null;
        List<SObjectField> fieldsSet = null;

        if(attachmentId != null){
            //update the record
            Id attachID = Id.valueOf(attachmentId);
            att = [SELECT Name, Body FROM Attachment WHERE ID = : attachID LIMIT 1 FOR UPDATE];
            
            att.Body = Blob.valueOf(valueToStore);
            operation = SQX_DB.DBOperation.Modify;
            fieldsSet = new List<SObjectField>{Attachment.fields.Body};
        }
        else{
            //insert record
            att = new Attachment();
            att.Name = 'Temporary Data';
            att.ParentID = parentID;
            att.Body = Blob.valueOf(valueToStore);
            att.ContentType = JSON_MIME_TYPE;
            operation = SQX_DB.DBOperation.Create;
            fieldsSet = new List<SObjectField>{
                Attachment.fields.Name,
                Attachment.fields.ParentID,
                Attachment.fields.Body,
                Attachment.fields.ContentType
            };
        }
        

        SQX_DB db = new SQX_DB();
        if(db.isCUDCompliant(att.getSObjectType(), operation)
                && db.IsFLSCompliant(fieldsSet, operation)){

                    Database.upsert(att);
        }

        return att.Id;
    }   
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description used to retrieve the base value of an object in data store. 
    * @param relatedObjectID the parent object for which this temporary data is being created
    * @return returns empty string if nothing is found, else returns the stored value
    */
    public static String getBaseObject(String relatedObjectID){
        List<SQX_TempStore__c> entry = [SELECT ID, Base_Object_Attachment_ID__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID AND
                                  Access_Key__c =: null
                                  ORDER BY LastModifiedDate DESC];
                                  
        if(entry.size() > 0){
            
            //System.assert(entry.size() == 1, 'Expected only one temporary entry to exist in draft stage but found multiple');
            
            if(entry.get(0).Base_Object_Attachment_ID__c  != null){
                return getAttachmentContent(entry.get(0).Base_Object_Attachment_ID__c);
            }
            
        }
        
        return '';
                                  
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/10
    * @description checks if there is a delta object, if so returns true
    * @param relatedObjectID the object for which the temp object is meant
    * @return <code>true</code> if a delta object has been stored
    *         <code>false</code> if a delta object has not been stored
    */
    public static boolean hasDeltaObject(String relatedObjectID){
        List<SQX_TempStore__c> entry = [SELECT ID, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID AND
                                  Access_Key__c =: null];

        boolean hasDelta = false;

        if(entry.size() > 0){
            hasDelta = entry.get(0).Delta_Attachment_Id__c != null;
        }


        return hasDelta;
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description used to retrieve the base value of an object in data store. 
    * @param relatedObjectID the parent object for which this temporary data is being created
    * @return returns empty string if nothing is found, else returns the stored value
    */
    public static String getDeltaObject(String relatedObjectID){
        List<SQX_TempStore__c> entry = [SELECT ID, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID AND
                                  Access_Key__c =: null
                                  ORDER BY LastModifiedDate DESC];
                                  
        if(entry.size() > 0){
            
            //System.assert(entry.size() == 1, 'Expected only one temporary entry to exist in draft stage but found multiple');
            
            if(entry.get(0).Delta_Attachment_Id__c != null){
                return getAttachmentContent(entry.get(0).Delta_Attachment_Id__c);
            }
        }
        
        return '';                    
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description fetch the blob of attachment with id
    * @param attachmentId the attachment whose blob is to be fetched 
    * @return the blob(text content) value as string
    */
    private static String getAttachmentContent(String attachmentId){
        if(attachmentId != null){
            List<Attachment> att = [SELECT Body FROM Attachment WHERE ID = : attachmentId];

            if(att.size() == 1)
                return att[0].Body.toString();
        }
        
        return '';
    }
    
    /**
    * @authoer Pradhanta Bhandari
    * @date 2014/1/28
    * @description fetches a temporarily stored object with the help of an access key
    * @param key the access key which is to be used to fetch the data
    * @return a SQX_TempStore object which contains both base and delta value
    */
    public static SQX_TempStore getTempObjectWithAccessKey(String key){
         List<SQX_TempStore__c> entry = [SELECT ID, Base_Object_Attachment_ID__c, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE Access_Key__c =: key];
         if(entry.size() > 0){
             SQX_TempStore__c data = entry.get(0);
             SQX_TempStore retValue = new SQX_TempStore();
             
             retValue.baseObject = getAttachmentContent(data.Base_Object_Attachment_ID__c);
             retValue.deltaObject = getAttachmentContent(data.Delta_Attachment_Id__c);
             
             
             return retValue;
             
         }
         
         return null;
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @description used to freeze/lock a temp object at the state, essentially clones the object
    * @param relatedObjectID the parent object for which this temporary data is to be frozen
    * @return returns empty string if nothing is found, else returns the access key
    */
    public static String freezeTempObject(String relatedObjectID){
        List<SQX_TempStore__c> entry = [SELECT ID, Type__c, Title__c, Base_Object_Attachment_ID__c, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID AND
                                  Access_Key__c =: null];
        
        if(entry.size() > 0){
            SQX_TempStore__c data = entry.get(0);
            if(data.Base_Object_Attachment_ID__c != null && data.Delta_Attachment_Id__c != null){
                SQX_TempStore__c clonedData = data.clone();
                clonedData.Access_Key__c = String.valueOf(Crypto.getRandomLong());
                clonedData.Base_Object_Attachment_ID__c = clonedData.Delta_Attachment_Id__c = null;

                new SQX_DB().op_insert(new List<SObject>{clonedData}, new List<SObjectField>{
                    SQX_TempStore__c.fields.Access_Key__c,
                    SQX_TempStore__c.fields.Base_Object_Attachment_ID__c,
                    SQX_TempStore__c.fields.Delta_Attachment_Id__c
                });
                
                
                clonedData.Base_Object_Attachment_ID__c = upsertAttachment(clonedData.Id, null, getAttachmentContent(data.Base_Object_Attachment_ID__c));
                clonedData.Delta_Attachment_Id__c  = upsertAttachment(clonedData.Id, null, getAttachmentContent(data.Delta_Attachment_Id__c ));
                
                new SQX_DB().op_update(new List<SObject>{clonedData}, new List<SObjectField>{
                    SQX_TempStore__c.fields.Base_Object_Attachment_ID__c,
                    SQX_TempStore__c.fields.Delta_Attachment_Id__c
                });
                
                return clonedData.Access_Key__c;
            }
        }
        
        return '';  
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/28
    * @description used to remove an existing the temporary value of an object in data store. 
    * @relatedObjectID the related object ID
    */
    public static void removeObject(String relatedObjectID){
        List<SQX_TempStore__c> entry = [SELECT ID, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID];
                                  
        if(entry.size() > 0){
            new SQX_DB().op_delete(entry);
            Database.emptyRecycleBin(entry); //permanently delete the attachment
        }
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/29
    * @description used to remove an delta/change from temporary data store. 
    * @relatedObjectID the related object ID
    */
    public static void removeDelta(String relatedObjectID){
        SQX_DB db = new SQX_DB();
        List<SQX_TempStore__c> entry = [SELECT ID, Related_Object_ID__c, Delta_Attachment_Id__c 
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedObjectID];
                                  
        if(entry.size() > 0){
            SQX_TempStore__c data = entry.get(0);
            if(data.Delta_Attachment_Id__c  != null){
                Attachment att = new Attachment();
                att.Id = data.Delta_Attachment_Id__c;

                db.op_delete(new List<SObject>{att});
                
                //remove all temp attachment
                List<SQX_Temp_Store_Attachment__c> tempAttachments = [SELECT Id FROM SQX_Temp_Store_Attachment__c WHERE SQX_Temp_Store__c =:data.Id];
                db.op_delete(tempAttachments);
                
                data.Delta_Attachment_Id__c  = null;

                db.op_update(new List<SObject>{data}, new List<SObjectField>{SQX_TempStore__c.fields.Delta_Attachment_Id__c});
            }
        }
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/31
    * @description used to add attachment to the temporary object. These attachments could be used latter. Calling this function multiple times will add
    *               the attachment multiple times.
    * @relatedObjectID the related object ID for which the temp store is to be created could be any sobject
    * @uniqueID the Id of the object
    * @att the attachment to add
    * @return returns the inserted attachment
    */
    public static Attachment storeTemporaryAttachment(String relatedId, String uniqueID, Attachment att){
        SQX_DB db = new SQX_DB();
        List<SQX_TempStore__c> entry = [SELECT ID, Related_Object_ID__c, Delta_Attachment_Id__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedId AND
                                  Access_Key__c = null];
                                  
        if(entry.size() > 0){
            SQX_TempStore__c store = entry.get(0);
            
            //get the temp store attachment Id
            List<SQX_Temp_Store_Attachment__c> tempAttachments = [SELECT ID, Name, SQX_Temp_Store__c
                                           FROM SQX_Temp_Store_Attachment__c
                                           WHERE Name = :uniqueId AND SQX_Temp_Store__c = : store.Id];
            
            SQX_Temp_Store_Attachment__c tempAttachment = null;
            
            //if there is no temp attachment add one                 
            if(tempAttachments.size() == 0){
                tempAttachment = new SQX_Temp_Store_Attachment__c();
                tempAttachment.Name = uniqueId;
                tempAttachment.SQX_Temp_Store__c = store.Id;
                
                db.op_insert(new List<SObject>{tempAttachment}, new List<SObjectField>{
                    SQX_Temp_Store_Attachment__c.fields.Name,
                    SQX_Temp_Store_Attachment__c.fields.SQX_Temp_Store__c
                });
            }
            else{
                //if there is one just assign it to the base
                tempAttachment = tempAttachments.get(0);
            }
            
            
            //set the new attachments parent id to temp store attachment entity
            att.parentId = tempAttachment.Id;
            
            //final dml
            db.op_insert(new List<SObject>{att},new List<SObjectField>{
                Attachment.fields.Name,
                Attachment.fields.Body,
                Attachment.fields.Description
            });
        }
        
        
        return att;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/13
    * @description this method gets the list of all attachments for a related id's based on the uniqueId 
    * @param relatedId the parent Id for which the temp store is meant
    * @param uniqueId the unique Id of the object to be stored. This allows a single related Id to have multiple attachments
    * @return null when no temp store can be found else returns the list of attachment for 
    */
    public static List<Attachment> getAttachments(Id relatedId, String uniqueId){
        List<SQX_TempStore__c> entry = [SELECT ID, Related_Object_ID__c, Delta_Attachment_Id__c
                                  FROM SQX_TempStore__c
                                  WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedId AND
                                  Access_Key__c = null];
                                  
        if(entry.size() > 0){
            SQX_TempStore__c store = entry.get(0);
            
            //get the parent temp store attachment object for the unique id
            //note: two SOQL queries are being used instead of
            // using a single SOQL query to because Querying attachment's body in subquery is not permitted
            Map<Id, SQX_Temp_Store_Attachment__c> tempAttachments = new Map<Id, SQX_Temp_Store_Attachment__c>([SELECT ID, Name, SQX_Temp_Store__c
                                           FROM SQX_Temp_Store_Attachment__c
                                           WHERE Name = :uniqueId AND SQX_Temp_Store__c = : store.Id]);

            //return the list of all objects
            return [SELECT Id, ParentId, Body, Name FROM Attachment WHERE ParentId IN : tempAttachments.keySet() ];
            
        }
        
        
        return null;
    }

    /*
    * @author Pradhanta Bhandari
    * @date 2014/3/12
    * @description This method updates the changeset to remove the persisted objects from changeset and reparent attachment to correct parent 
    * @param relatedId the id of related object
    * @param setWithTempIdToPersistedObject the temporary objects that were mapped to permanent objects
    * @param delta the new delta value to be updated to, if null delta is passed it is not updated
    * @throws an exception no temporary object is found
    *
    * reparenting logic moved to upserter to handle how Attachment is updated, added etc.
    public static void reparentAttachmentAndUpdateDelta(String relatedId, Map<String, sObject> setWithTempIdToPersistedObject, String delta){
        
        List<SQX_TempStore__c> entry = [SELECT ID, Base_Object_Attachment_ID__c, Delta_Attachment_Id__c, Related_Object_ID__c
                                  FROM SQX_TempStore__c
                                  WHERE Access_Key__c = null AND OwnerID = : UserInfo.getUserID()
                                  AND Related_Object_ID__c = :relatedId];
        if(entry.size() > 0){
            SQX_TempStore__c data = entry.get(0);

            //move attachments
            Map<String, SQX_Temp_Store_Attachment__c> attachments = new Map<String, SQX_Temp_Store_Attachment__c>( [SELECT Id, Name, Has_Valid_Parent__c 
                                                            FROM SQX_Temp_Store_Attachment__c
                                                            WHERE SQX_Temp_Store__c = : data.Id 
                                                            AND (Name IN : setWithTempIdToPersistedObject.keySet()
                                                            OR Has_Valid_Parent__c = true)]);

            
            List<SObject> reparentedAttachment = new List<SObject>(); //list of reparented attachment
            List<Attachment> toInsert = new List<Attachment>(); //entity to insert


            for(Attachment att: [SELECT Id,Name,Description,Body, BodyLength, ParentId FROM Attachment
                                                    WHERE ParentId IN : attachments.keySet() ]){

                
                reparentedAttachment.add(new Attachment(Id = att.Id ) );
                Id parentId = att.ParentID; 
                Attachment newAttachment = att;
                newAttachment.Id = null; //this is equivalent to cloning and doing this should allow us to reduce heap size
                        
                //if parent is already a valid one i.e. Temp Store Attachment name is not logical
                //reparent to that parent
                if(attachments.get(parentId).Has_Valid_Parent__c ){
                    newAttachment.ParentID = attachments.get(parentId).Name;
                }
                else{
                        //else get the actual parent id from the map containing logical id(non sf id) to sf id
                        sObject newObject = setWithTempIdToPersistedObject.get(attachments.get(parentId).Name);
                        if(newObject == null)
                            continue;
                            
                        Id newObjectId = (Id) newObject.get('Id');
                    
                        newAttachment.ParentID = newObjectId;
                }

                toInsert.add(newAttachment);

            }
            
            
            
            //insert the records
            List<Database.SaveResult> results = new SQX_DB().op_insert(toInsert, new List<SObjectField>{
                Attachment.fields.Name,
                Attachment.fields.Description,
                Attachment.fields.ParentId,
                Attachment.fields.Body
            });


            
            //remove attachment from changeset                                    
            if(reparentedAttachment.size() > 0){
                //delta = SQX_Upserter.getNewChangeSetAfterRemovingPersistedOnes(delta, reparentedAttachment, null);
            }
            
            Attachment deltaAttachment = null;
            //check if there is a new delta if yes update
            if(data.Delta_Attachment_Id__c != null && delta != null){
                deltaAttachment = new Attachment();
                deltaAttachment.Body = Blob.valueOf(delta);
                deltaAttachment.Id = data.Delta_Attachment_Id__c;
                
                new SQX_DB().op_update(new List<SObject>{deltaAttachment}, new List<SObjectField>{
                    Attachment.Body
                });
            }

            if(reparentedAttachment.size() > 0){
                List<Database.DeleteResult> result = new SQX_DB().op_delete(reparentedAttachment);//lets remove the attachments from salesforce
                if(result.get(0).isSuccess())
                    Database.emptyRecycleBin(reparentedAttachment);
            }
            
            System.debug('After reparenting - ' + Limits.getDMLStatements());
        
        }
            
        return;

    }*/


    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/3
    * @description removes an attachment from the related id with a uniqueId
    * @param relatedId the Id of the parent
    * @param uniqueId the Id that is uniquely used to identify this finding
    * @param attachmentId the Id of the attachment to be deleted
    * @return <code>true</code> if attachment was deleted successfully, <code>false</code> if the attachment was not deleted
    */
    public static boolean removeTemporaryAttachment(Id relatedId, String uniqueId, String attachmentId){
        boolean deleted = false;

        try{
            List<SQX_TempStore__c> entry = [SELECT ID, Related_Object_ID__c, Delta_Attachment_Id__c
                                      FROM SQX_TempStore__c
                                      WHERE OwnerID =: UserInfo.getUserID() AND Related_Object_ID__c = : relatedId AND
                                      Access_Key__c = null];
                                      
            if(entry.size() > 0){
                SQX_TempStore__c store = entry.get(0);
                
                //get the parent temp store attachment object for the unique id
                //note: two SOQL queries are being used instead of
                // using a single SOQL query to because Querying attachment's body in subquery is not permitted
                Map<Id, SQX_Temp_Store_Attachment__c> tempAttachments = new Map<Id, SQX_Temp_Store_Attachment__c>([SELECT ID, Name, SQX_Temp_Store__c
                                               FROM SQX_Temp_Store_Attachment__c
                                               WHERE Name = :uniqueId AND SQX_Temp_Store__c = : store.Id]);

                if(tempAttachments.values().size() != 0){
                    //check if the attachment is indeed present
                    Attachment attachment  = [SELECT Id, Name FROM Attachment WHERE ParentID IN : tempAttachments.keySet() AND Id = :attachmentId];
                    new SQX_DB().op_delete(new List<SObject>{attachment});


                    Database.emptyRecycleBin(new ID[]{attachment.Id}); //permanently delete the attachment

                    deleted = true;
                }


                
            }
        }
        catch(Exception ex){
            //ignore exceptions cause all that means is it failed deletion test.
        }
        
        
        return deleted;
    }

    


}