/**
* This class stores the common operations in Finding CAPA object
* It also contains the trigger handler for the Finding CAPA
*/
public with sharing class SQX_Finding_Capa {
     public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(List<SQX_Finding_Capa__c> newFindingCapas, Map<Id, SQX_Finding_Capa__c> oldFindingCapas){
            super(newFindingCapas, oldFindingCapas);
        }

        /*
        * links Finding with Capa when CAPA Resolves Finding 
        */
        public Bulkified linkFindingWithCapaWhenResolvesByCapaIsChecked() {

            final String ACTION_NAME = 'linkFindingWithCapaWhenResolvesByCapaIsChecked'; 

            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Finding_Capa__c.Resolves_by_CAPA__c, RuleOperator.Equals, true);

            this.resetView()
                .removeProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!filter.evaluationResult.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME, filter.evaluationResult);

                List<SQX_Finding__c> findingsToUpdate = new List<SQX_Finding__c>();

                for (SQX_Finding_Capa__c findingCAPA : (List<SQX_Finding_Capa__c>) this.view){
                    SQX_Finding__c finding = new SQX_Finding__c();
                    finding.Id = findingCAPA.SQX_Finding__c;
                    finding.SQX_Capa__c = findingCAPA.SQX_Capa__c;
                    findingsToUpdate.add(finding);
                }
                
                /**
                * Without sharing has been used to cover the following scenario:
                * ------------------------------------------ 
                * Without sharing has been used as CAPA owner may not have necessary access to update Finding fields if FindingCAPA Reference is altered 
                */
                if (findingsToUpdate.size() > 0) {
                    new SQX_DB().withoutSharing().op_update(findingsToUpdate , 
                            new List<Schema.SObjectField>{ 
                                SQX_Finding__c.fields.SQX_CAPA__c 
                                                        });
                }
            }

            return this;
        }

        /*
        * unlinks Finding with Capa when Solved By Capa is Unchecked 
        */
        public Bulkified unlinkFindingWithCapaWhenResolvesByCapaIsUnchecked() {

            final String ACTION_NAME = 'unlinkFindingWithCapaWhenResolvesByCapaIsUnchecked'; 

            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Finding_Capa__c.Resolves_by_CAPA__c, RuleOperator.Equals, false);

            this.resetView()
                .removeProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (!filter.evaluationResult.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME, filter.evaluationResult);

                reEvaluateFinding((List<SQX_Finding_CAPA__c>)filter.evaluationResult);
            }
            
            return this;
        }

        /*
        * clear Finding CAPA field When Reference Is Deleted
        */
        public Bulkified clearFindingCAPAFieldWhenReferenceIsDeleted() {

            final String ACTION_NAME = 'clearFindingCAPAFieldWhenReferenceIsDeleted'; 

            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Finding_Capa__c.Resolves_by_CAPA__c, RuleOperator.Equals, true);

            this.resetView()
                .removeProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEveryEdit);
            
            if (!filter.evaluationResult.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME, filter.evaluationResult);

                reEvaluateFinding((List<SQX_Finding_CAPA__c>)filter.evaluationResult);

            }
            return this;
        } 

        /*
        * Prevent Finding CAPA Reference deletion if finding is in CAPA Link Approval Stageve
        */
        public Bulkified preventFindingCAPADeletionIfFindingIsInCAPALinkApprovalStage() {

            final String ACTION_NAME = 'preventFindingCAPADeletionIfFindingIsInCAPALinkApprovalStage'; 

            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Finding_Capa__c.Resolves_by_CAPA__c, RuleOperator.Equals, true);

            this.resetView()
                .removeProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME)
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEveryEdit);
            
            if (!filter.evaluationResult.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.FindingCAPA, ACTION_NAME, filter.evaluationResult);

                for (SQX_Finding_Capa__c findingCAPA : (List<SQX_Finding_Capa__c>) filter.evaluationResult) {
                    if (findingCAPA.Finding_Stage__c == SQX_Finding.STAGE_CAPA_LINK_APPROVAL){
                        // Finding CAPA Reference cannot be removed if Finding is in CAPA Link Approval Stage
                        // throw exception to CAPA kendo pages
                        throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_CAPA_Finding_Reference_can_t_be_Removed);
                    }
                }
            }
            return this;
        }
    }
    /**
    * this function evaluates finding from Finding CAPA Reference as follows:
    * Finding is reopened if Finding is 'Complete' and finding CAPA reference is removed
    * @param findingCAPARefList this parameter provides a List of finding CAPA reference list  
    * @return returns a list of in approval response inclusions
    */
    public static void reEvaluateFinding(List<SQX_Finding_CAPA__c> findingCAPARefList){

        if(findingCAPARefList.size() > 0){
            
            List<SQX_Finding__c> findingsToUpdate = new List<SQX_Finding__c>();

            for (SQX_Finding_Capa__c findingCAPA : findingCAPARefList) {

                SQX_Finding__c finding = new SQX_Finding__c();
                finding.Id = findingCAPA.SQX_Finding__c;
                
                if(findingCAPA.Finding_Status__c == SQX_Finding.STATUS_COMPLETE && findingCAPA.Finding_Stage__c == SQX_Finding.STAGE_WAITING_FOR_CAPA){ 
                    //Reopen Finding if Finding CAPA Reference is removed
                    finding.Status__c = SQX_Finding.STATUS_OPEN;
                    finding.Update_Audit_Stage__c = true;
                }

                // Remove CAPA as reference and clear Stage and CAPA field
                finding.Stage__c = null;
                finding.SQX_Capa__c = null;
                findingsToUpdate.add(finding);
            }
  
            /**
            * Without sharing has been used to cover the following scenario:
            * ------------------------------------------ 
            * Without sharing has been used as CAPA owner may not have necessary access to update Finding fields if FindingCAPA Reference is altered 
            */
            if(findingsToUpdate.size() > 0){
                new SQX_DB().withoutSharing().op_update(findingsToUpdate , 
                    new List<Schema.SObjectField>{ 
                        SQX_Finding__c.fields.SQX_CAPA__c,
                        SQX_Finding__c.fields.Stage__c,
                        SQX_Finding__C.fields.Status__c,
                        SQX_Finding__C.fields.Update_Audit_Stage__c
                                                });
            }
        }
    }
}