/*
 * test class for CAPA Impacted Documents.
 */
@isTest
public class SQX_Test_3934_Impacted_Document {

    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * ARRANGE : Create a CAPA with all policies required false, and controlled doc in draft, add impacted document with status open
     * ACT : Change Assignee 
     * ASSERT : Task should be reassigned
     * @date : 2017/10/12
     * @story : SQX-3934
     */
    public static testMethod void givenACAPA_WhenImpactedDocumentAdded_TaskShouldBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser2 = allUsers.get('standardUser2');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            // Add controlled doc in draft
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_OPEN,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));
            
            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            Task docTask = [SELECT Id FROM Task WHERE Child_What_Id__c =: impactedDocument.Id ]; 
            System.assert(docTask.Id != null, 'Task should be creatd');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: Change Assignee
            impactedDocument.SQX_User__c = standardUser2.Id;
            new SQX_DB().op_update(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{SQX_Impacted_Document__c.SQX_User__c});
            
            List<Task> docTasks = [SELECT Id, Status, Subject FROM Task WHERE Child_What_Id__c =: impactedDocument.Id  ORDER BY CreatedDate ASC];
            
            // Assert : New Task is created, old task is completed
            System.assertEquals(2, docTasks.size(), docTasks);
            System.assertEquals(1, [SELECT Id, Status FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Status =: SQX_Task.STATUS_COMPLETED].size());
            System.assertEquals(1, [SELECT Id, Status FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Status =: SQX_Task.STATUS_NOT_STARTED].size());
            
            
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Complete impacted document by moving document is current status
            cDoc.doc.Effective_Date__c = Date.today();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc.save();
            
            List<SQX_Impacted_Document__c> impactedDocuments = [SELECT Id, Status__c, Completion_Date__c FROM SQX_Impacted_Document__c WHERE Id =: impactedDOcument.Id];
            // ASSERT : Impacted document is complete
            System.assertEquals(SQX_Impacted_Document.STATUS_COMPLETE, impactedDocuments[0].Status__c);
            //ASSERT: Completion Date should be set
            System.assertEquals(System.today(), impactedDocuments[0].Completion_Date__c);
            
            // ASSERT : All tasks are complete
            System.assertEquals(2,[SELECT Id, Status FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Status =: SQX_Task.STATUS_COMPLETED].size());  
            
            // ASSERT : CAPA is complete
            System.assertEquals(SQX_CAPA.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id].Status__c);          
        }
    }  
    
    /**
     * ARRANGE : Create a CAPA and controlled doc in draft, add impacted document with status open
     * ACT : Delete impacted document
     * ASSERT : Task should be deleted and capa should be completed
     * @date : 2017/11/14
     * @story : SQX-4258
     */
    public static testMethod void givenACAPAWithImpactedDoc_WhenImpactedDocIsDeleted_TaskShouldBeDeleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser2 = allUsers.get('standardUser2');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            // Add controlled doc in draft
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_OPEN,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));

            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            String iDocId = impactedDocument.Id;
            Task docTask = [SELECT Id FROM Task WHERE Child_What_Id__c =: impactedDocument.Id ]; 
            System.assert(docTask.Id != null, 'Task should be creatd');
            
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // ACT : Delete impacted document
            new SQX_DB().op_delete(new List<SQX_Impacted_Document__c>{impactedDocument});

            // ASSERT : Impacted document related task is deleted
            System.assertEquals(0, [SELECT Id FROM Task WHERE Child_What_Id__c =: iDocId].size());

            // ASSERT : CAPA is complete
            System.assertEquals(SQX_CAPA.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id].Status__c);          
        }
    }    

    /**
     * ARRANGE : Create a CAPA with all policies required false, and controlled doc in draft, add impacted document with status open
     * ACT : Complete impacted document
     * ASSERT : CAPA is completed
     * @date : 2017/10/12
     * @story : SQX-3934
     */
    public static testMethod void givenACAPA_WhenImpactedDocumentIsCompleted_CapaShouldBeCompleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            //  Add controlled doc in current status
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            cDoc.doc.Effective_Date__c = Date.today();
            cDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Complete document
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_OPEN,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));
            
            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            
            System.assertEquals(SQX_Impacted_Document.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Impacted_Document__c WHERE Id =: impactedDOcument.Id].Status__c);
            
            // ASSERT : CAPA should be complete
            System.assertEquals(SQX_CAPA.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id].Status__c);          
        }
        
    }

    /**
     * ARRANGE : Create a CAPA with all policies required false, and controlled doc in draft, add impacted document with status open
     * ACT : Try completing impacted document without making document current 
     * ASSERT : Save is unsuccessful
     * @date : 2017/10/12
     * @story : SQX-3934
     */
    public static testMethod void givenAnCAPA_WhenImpactedDocumentIsCompletedWithoutReleasingRelatedDoc_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false, 
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            // Add controlled doc in draft
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            

            // add impacted document with status open
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_OPEN,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));
            
            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            
            System.assertEquals(SQX_Impacted_Document.STATUS_OPEN, [SELECT Id, Status__c FROM SQX_Impacted_Document__c WHERE Id =: impactedDOcument.Id].Status__c);
            System.assertEquals(SQX_CAPA.STATUS_OPEN, [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id].Status__c);          
        
            impactedDocument = [SELECT Id, Status__c FROM SQX_Impacted_Document__c WHERE Id =: impactedDocument.Id];

            // ACT : Complete the document
            impactedDocument.Status__c = SQX_Impacted_Document.STATUS_COMPLETE;

            Database.SaveResult result = Database.update(impactedDocument, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Expected to be unsuccessful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Impacted Document without document status current or pre-expire cannot be completed.'));
        }
        
        
    }
    
    /**
     * ARRANGE : Create a CAPA with all policies required false, and controlled doc in draft, add impacted document with status draft
     * ACT : Change status to open 
     * ASSERT : Task should be created
     * @date : 2017/11/17
     * @story : SQX-4297
     */
    public static testMethod void givenACAPAWithImpactedDocumentInDraft_WhenImpactedDocumentIsOpen_TaskShouldBeCreated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser2 = allUsers.get('standardUser2');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            // Add controlled doc in draft
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_DRAFT,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));
            
            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            List<Task> docTask = new List<Task>(); 
            docTask = [SELECT Id FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Child_What_Id__c =: impactedDocument.Id]; 
            System.assertEquals(0, docTask.size(), 'Task should not be creatd');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: Open Impacted Document
            impactedDocument.Status__c = SQX_Impacted_Document.STATUS_OPEN;
            new SQX_DB().op_update(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{SQX_Impacted_Document__c.Status__c});
            
            // ASSERT : Task should be created
            docTask = [SELECT Id FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Child_What_Id__c =: impactedDocument.Id]; 
            System.assertEquals(1, docTask.size(), 'Task should be creatd');         
        }
    }  
    
    /**
     * ARRANGE : Create a CAPA with all policies required false, and controlled doc in draft, add impacted document with status open
     * ACT : Skip Impacted Document
     * ASSERT : Task and CAPA is completed
     * @date : 2017/11/22
     * @story : SQX-4343
     */
    public static testMethod void givenACAPAWithImpactedDocument_WhenDocIsSkipped_TaskAndCAPAisCompleted(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser2 = allUsers.get('standardUser2');
        User adminUser = allUsers.get('adminUser');
        System.runAs(standardUser){

            // ARRANGE : Create a CAPA with all policies required false
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            capa.prepareMockCAPA(standardUser, null, standardUser.Id);
            capa.save();
            
            capa.capa.Investigation_Required__c = false;
            capa.capa.Response_Required__c = false;
            capa.capa.Containment_Required__c = false;
            capa.capa.Done_Responding__c = true;
            capa.capa.Preventive_Action_Required__c =  false;
            capa.capa.Corrective_Action_Required__c = false;
            capa.save();
            

            // Add controlled doc in draft
            SQX_Test_Controlled_Document cDoc = new SQX_Test_Controlled_Document();
            cDoc.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            SQX_Impacted_Document__c impactedDocument = new SQX_Impacted_Document__c(
                                                                SQX_CAPA__c = capa.capa.Id,
                                                                SQX_Controlled_Document__c = cDoc.doc.Id,
                                                                Status__c = SQX_Impacted_Document.STATUS_OPEN,
                                                                SQX_User__c = standardUser.Id,
                                                                Due_Date__c = Date.today().addDays(30));
            
            new SQX_DB().op_insert(new List<SQX_Impacted_Document__c>{impactedDocument}, new List<Schema.SObjectField>{});
            
            Task docTask = [SELECT Id FROM Task WHERE Child_What_Id__c =: impactedDocument.Id ]; 
            System.assert(docTask.Id != null, 'Task should be creatd');

            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Skip Impacted Document
            impactedDocument.Status__c = SQX_Impacted_Document.STATUS_SKIPPED;
            update impactedDocument;
            
            // ASSERT : All tasks are complete
            System.assertEquals(1,[SELECT Id, Status FROM Task WHERE Child_What_Id__c =: impactedDocument.Id AND Status =: SQX_Task.STATUS_COMPLETED].size());  
            
            // ASSERT : CAPA is complete
            System.assertEquals(SQX_CAPA.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id].Status__c);          
        }
    }  
}