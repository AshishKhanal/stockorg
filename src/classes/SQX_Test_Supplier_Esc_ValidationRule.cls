/**
 * Test class for validating all Supplier Escalation related validation rules
 **/
@isTest
public class SQX_Test_Supplier_Esc_ValidationRule {
	@testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
        User anotherStandardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'anotherStandardUser'); 
        
        System.runAs(standardUser) {

            // create account record
            Account account = new Account(
                Name = SQX_Test_Supplier_Escalation.ACCOUNT_NAME 
            );
            insert account;
            
            //Create contact for Account
            insert new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = account.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
        }
    }
    
    /*
     * GIVEN: Supplier Escalation record in Draft status
     * WHEN: Account is changed
     * THEN: Error is thrown
     */
    static testmethod void whenSupplierIsChangedAfterDraftStage_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});

        System.runAs(standardUser){
            // Arrange: Create a new Supplier escalation in draft stage
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation().save();
            
            Id existingAccountId = escalation.supplierEscalation.SQX_Account__c;
            
            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            
            // Act: Change the account
            escalation.supplierEscalation.SQX_Account__c = account.Id;
            escalation.supplierEscalation.SQX_Supplier_Contact__c = contact.Id;

            // Assert: No error is thrown
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];  
            System.assertEquals(true, result.isSuccess(), 'No Error should be thrown while changing supplier on draft stage but has error. '+ result.getErrors());
            
            // Act: submit the record and change account
            escalation.submit();
            
            escalation.supplierEscalation.SQX_Account__c = existingAccountId;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Supplier cannot be changed.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while changing Supplier after draft stage');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: New Supplier Escalation
     * WHEN: Contact is left blank
     * THEN: Error is thrown
     */
    static testmethod void whenContactIsLeftBlank_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: New Supplier Escalation
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation().save();
            
            // Act: Set the contact to null
            escalation.supplierEscalation.SQX_Supplier_Contact__c = null;
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Supplier Contact is required.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while removing Supplier Contact');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: New Supplier Escalation
     * WHEN: Part is left blank
     * THEN: Error is thrown
     */
    static testmethod void whenPartOrServiceIsLeftBlank_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: Create new Supplier Escalation
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            
            // Act: Set the part numbers as blank
            escalation.supplierEscalation.compliancequest__Related_Part_Numbers__c = null;
            Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Product or Service is required.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved without part or service');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
            
            //Act: Set the part number and save
            escalation.supplierEscalation.compliancequest__Related_Part_Numbers__c = 'Part1, Part2';
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Assert: No error is thrown
            System.assertEquals(true, result.isSuccess(), 'Record should saved while adding part but has error. ' + result.getErrors());
        }
    }
    
    /*
     * GIVEN: New Supplier Escalation in open status
     * WHEN: Part is changed
     * THEN: Error is thrown
     */
    static testmethod void whenPartOrServiceIsChangedAfterInitiation_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: Create new supplier escalation in open status
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation();
            escalation.supplierEscalation.compliancequest__Related_Part_Numbers__c = 'Part1, Part2';
            escalation.supplierEscalation.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            escalation.supplierEscalation.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_IN_PROGRESS;
            Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Act: Change the part number field
            escalation.supplierEscalation.compliancequest__Related_Part_Numbers__c = 'Part1, Part2, Part3';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation__c>{escalation.supplierEscalation}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Product and Service cannot be changed.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while initaiting without part or service');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }
    /**
      * Given: supplier escalation record in open status,
      * When: escalataion fields is edited 
      * Then: Validation error msg, 'Supplier Escalation step in open status cannot be edited.' is thrown.	
      */
     @isTest
    public static void givenEscalationStep_WhenUserEditRecord_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {adminUser, standardUser});
        System.runAs(adminUser){
             //Arrange: Create policy task
             SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
         }
         
         
         System.runAs(standardUser){  
             // Act: Create supplier escalation document record
             SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation().save();
             
             //Act: Submit the record
             supEsc.submit();
             supEsc.initiate();
 
             SQX_Supplier_Escalation_Step__c escalationStep = [Select id, name, status__c, result__c , comment__c
                                                      from SQX_Supplier_Escalation_Step__c where status__c =  :SQX_Steps_Trigger_Handler.STATUS_OPEN and
                                                      SQX_Parent__c = :supEsc.supplierEscalation.id limit 1];
             //Act: Any allowed field is edited
             escalationStep.Comment__c = 'Just a random comment.';
             List<Database.SaveResult> resultAgain =  new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation_Step__c> {escalationStep}, 
                                                                                               new List<Schema.SObjectField>{});
             //Assert: Make sure that fields are updated
             System.assertEquals(true, resultAgain[0].isSuccess());
             
             //Act: try updating field that is not allowed to be modified
             escalationStep.allowed_days__c = 5;
             List<Database.SaveResult> result =  new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation_Step__c> {escalationStep}, new List<Schema.SObjectField>{});
             
             //Assert: Validation error message is thrown
             System.assertEquals('Supplier Escalation step in open status cannot be edited.',result[0].getErrors()[0].getMessage());  
             
         } 
     }
    
    /**
     * GIVEN : given SupplierEscalation Step record
     * WHEN : when save the record with issue date greater than current date
     * THEN : throw validation error message
     */  
     public static testMethod void givenEscalationStep_WhenSaveRecordWithIssueDateGreaterThanCurrentDate_ThenThrowValidationErrorMessage(){
         Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
         User adminUser = allUsers.get('adminUser');
         User standardUser = allUsers.get('standardUser');
         SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {adminUser, standardUser});
         System.runAs(adminUser){
             //Arrange: Create policy task
             SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
         }
         
         
         System.runAs(standardUser){
             
              // Act: Create supplier escalation task record
             SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation().save();
             
             //Act: Submit the record
             supEsc.submit();
             supEsc.initiate();
 
           
             //Arrange: retrive workflow policy
             SQX_Supplier_Escalation_Step__c escStep = [SELECT Id, Expiration_Date__c, Issue_Date__c FROM SQX_Supplier_Escalation_Step__c LIMIT 1];
             escStep.Issue_Date__c = Date.today() + 2;	
             
             //Act: save the record
             List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation_Step__c> { escStep },  new List<Schema.SObjectField>{ SQX_Supplier_Escalation_Step__c.Expiration_Date__c });
             System.assert(result[0].isSuccess() == false, 'Expected Issue date to be smaller than current date');
             System.assertEquals('Issue Date cannot be greater than Current Date.', result[0].getErrors().get(0).getMessage());
         }
     }
    
    /**
     * Given: Supplier Escalation with policy task 
     * When: Obsolete controlled document is referred to task
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierEscalation_WhenObsoleteDocumentReferred_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {adminUser, standardUser});
        System.runAs(adminUser){
            //Arrange: Create policy task
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
        }
        
        System.runAs(standardUser){    
            //Arrange: Create controlled document
            SQX_Test_Controlled_Document testControlledDocument = new SQX_Test_Controlled_Document();
            testControlledDocument.doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
            testControlledDocument.save();
        
            // Act: Create supplier document record
            SQX_Test_Supplier_Escalation supEsc = new SQX_Test_Supplier_Escalation().save();
            
            //Act: Submit the record
            supEsc.submit();
            
            //Act: Set obsolete document in step
            SQX_Supplier_Escalation_Step__c siSteps = [SELECT Id, Status__c, SQX_Controlled_Document__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: supEsc.supplierEscalation.Id];
            siSteps.Status__c = SQX_Supplier_Common_Values.STATUS_OPEN;
            siSteps.SQX_Controlled_Document__c = testControlledDocument.doc.Id;
            Database.SaveResult result = Database.update(siSteps, false);
            
            //Assert: Obsolete controlled document should not be set in step
            String ExpErrMsg = 'Only Pre-Release and Current Controlled Documents can be referred.';
            System.assertEquals(ExpErrMsg, result.getErrors().get(0).getMessage());
        }
    }

    /**
     * Given : Closed Supplier Escalation
     * When : File is added to Supplier Escalation or Supplier Escalation Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenClosedSupplierEscalationAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a Supplier Escalation Record 
            SQX_Test_Supplier_Escalation sd = new SQX_Test_Supplier_Escalation().save();
           
            // Submitting the Supplier Escalation record
            sd.submit();
            sd.initiate();

            //close  the Supplier Escalation record
            sd.escalate('2');

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(sd.supplierEscalation.Id);
        }
    }

    /**
     * Given : Void Supplier Escalation
     * When : File is added to Supplier Escalation or Supplier Escalation Step
     * Then : Error is thrown
     */
    @isTest
    public static void givenVoidSupplierEscalationAndSteps_WhenFileIsAddedToIt_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange :Create a Supplier Escalation Record 
            SQX_Test_Supplier_Escalation sd = new SQX_Test_Supplier_Escalation().save();
           
            // Submitting the Supplier Escalation record
            sd.submit();

            // void  the Supplier Escalation record
            sd.voidEscalation();

            // call common method to validate error messages
            validateErrorWhileAddingFilesToLockedRecords(sd.supplierEscalation.Id);
            
        }
    }
    
    /**
     * This common internal private method is used to validate common error message while adding files to locked Supplier Escalation/step records.
     */
    private static void validateErrorWhileAddingFilesToLockedRecords(Id supplierEscalationId) {
        // Act : add file
        ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
        insert cv;
        
        // query contentdocumentid
        Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
        // insert ContentDocumentLink for Supplier Escalation
        Database.SaveResult result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = supplierEscalationId, ShareType = 'V'), false);
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for locked Supplier Escalation record.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());

        // Act : add files (i.e. ContentDocumentLink) for Onboarding Step
        SQX_Supplier_Escalation_Step__c escalationStep = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =:supplierEscalationId AND Status__c =: SQX_Supplier_Common_Values.STATUS_DRAFT LIMIT 1];
        result = Database.insert(new ContentDocumentLink(ContentDocumentId = contentId, LinkedEntityId = escalationStep.Id, ShareType = 'V'), false);
        
        // Assert: Ensure the error is thrown
        System.assert(result.isSuccess() == false, 'Expected ContentDocumentLink not to be added for Supplier Escalation Step whose Supplier Escalation is locked.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, result.getErrors().get(0).getMessage());
    }

    /**
     * Given : Supplier Escalation and Supplier Escalation record, File(ContentDocumentLink) is added to Supplier Escalation and Step and Supplier Escalation is locked(closed or voided)
     * When : ContentDocumentLink is tried to delete
     * Then : Error is thrown
     */
    @isTest
    public static void givenLockedSupplierEscalationAndStepWithFile_WhenContentDocumentLinkIsDelted_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            
            //Arrange : Create a policy task 
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', adminUser, 1);
           
            // Arrange : Create a Supplier Escalation Record 
            SQX_Test_Supplier_Escalation sd = new SQX_Test_Supplier_Escalation().save();

            // add file
            ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('Sample File'), PathOnClient = 'Sample.txt');
            insert cv;

            // query contentdocumentid
            Id contentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id][0].ContentDocumentId;
        
            // insert ContentDocumentLink for Supplier Escalation
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = sd.supplierEscalation.Id, ShareType = 'V');
            Database.SaveResult result = Database.insert(documentLink, false);
            
            System.assert(result.isSuccess() && documentLink.Id != null, 'Expected file/ContentDocumentLink to be added in Supplier Escalation. Actual Error : ' + result.getErrors());

            // Submitting the Supplier Escalation record
            sd.submit();
            sd.initiate();

            // add files to Supplier Escalation too
            SQX_Supplier_Escalation_Step__c escalationStep = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =:sd.supplierEscalation.Id LIMIT 1];
            ContentDocumentLink documentLink_dp = new ContentDocumentLink(ContentDocumentId = contentId,LinkedEntityId = escalationStep.Id, ShareType = 'V');
            result = Database.insert(documentLink_dp, false);
            System.assert(result.isSuccess() && documentLink_dp.Id != null, 'Expected file/ContentDocumentLink to be added in Supplier Escalation. Actual Error : ' + result.getErrors());

            // lock record by closeing the Supplier Escalation record
            sd.escalate('2');

            // Act : Try to delete ContentDocumentLink for Supplier Escalation by calling common method
            validateDeletionOfContentDocumentLink(documentLink, 'Supplier Escalation');

            // Act : Try to delete ContentDocumentLink for Escalation Step by calling common method
            validateDeletionOfContentDocumentLink(documentLink_dp, 'Escalation Step');
        }
    }

    /**
     * Common method to ensure that the deletion of ContentDocumentLink locked Supplier Escalation and Escalation Step is being prevented
     */
    public static void validateDeletionOfContentDocumentLink(ContentDocumentLink cdLink, String objName) {

        // Act : try to delete ContentDocumentLink
        Database.DeleteResult deleteResult = Database.delete(cdLink, false);
        
        // Assert : Ensure ContentDocumentLink is not deleted and system throws expected validation error.
        System.assert(deleteResult.isSuccess() == false, 'Expected ContentDocumentLink not to be deleted from' + objName + ' but is deleted.');
        System.assertEquals(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked, deleteResult.getErrors().get(0).getMessage());
    }
}