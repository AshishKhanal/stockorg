/**
 * test class to check due date of action when the action plan has due date or days required
 */
@isTest
public class SQX_Test_3455_Action_Plan_Effort {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    /**
     * GIVEN : 2 action plans - one with due date and other with days required
     * WHEN: Due Date is calculated
     * THEN : Actions have correct due date
     *          1. Action created from action plan with due date will have due date copied
     *          2. Action created from action plan with days required will have due date with days required added to the days from which action plan is approved
     */
    public static testMethod void givenAnActionPlan_WhenApproved_ActionAreCreatedAsPerActionPlanDueDateOrEffort(){
        
        List<SQX_Action_Plan__c> actionPlans = new List<SQX_Action_Plan__c>();
        SQX_Action_Plan__c actionPlan1 = new SQX_Action_Plan__c (Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                                Description__c = 'Corrective Action Plan Description',
                                                                Due_Date__c = Date.today().addDays(10));
        
        SQX_Action_Plan__c actionPlan2 = new SQX_Action_Plan__c (Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                                Description__c = 'Corrective Action Plan Description',
                                                                Days_Required__c = 10.0);

        Date dueDate = Date.today().addDays(10);

        // Act : Calculate due date
        // Assert : Actions have correct due date
        // 1. Action created from action plan with due date will have due date copied
        // 2. Action created from action plan with days required will have due date with days required added to the days from which action plan is approved
        System.assertEquals(dueDate, SQX_Investigation.calculateActionDueDate(actionPlan1));
        System.assertEquals(dueDate, SQX_Investigation.calculateActionDueDate(actionPlan2));
            
            
    }
   
    /**
     * GIVEN : An investigation with action plan
     * WHEN : Action plan is inserted with negative days required
     * THEN : Error is thrown
     * @story : [SQX-3455]
     * @date : 2017/06/26
     */
    public static testMethod void giveAnInvestigation_WhenActionPlanInsertedWithIncorrectDaysRequired_ErrorIsThrown(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange : Create required investigation
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
                .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
                .setStatus(SQX_Finding.STATUS_DRAFT)
                .save();
            
            SQX_Investigation__c inv = new SQX_Investigation__c(SQX_Finding__c = finding.finding.Id, Investigation_Summary__c = 'Test');
            Database.insert(inv,false);
            
            // Act : Create action plan with negative days required
            SQX_Action_Plan__c actionPlan1 = new SQX_Action_Plan__c (SQX_Investigation__c = inv.Id,
                                                                    Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                                                    Description__c = 'Corrective Action Plan Description',
                                                                    Days_Required__c = -10.0);
            
            Database.SaveResult result = Database.insert(actionPlan1, false);

            // Assert : Insertion unsuccessful
            System.assert(!result.isSuccess(), 'Expect to fail');

            // Act : Add action plan with positive days required
            actionPlan1.Days_Required__c = 10.0;
            result = Database.insert(actionPlan1, false);

            // Assert : Insertion successful
            System.assert(result.isSuccess(), 'Expect to pass');


        }
    }
}