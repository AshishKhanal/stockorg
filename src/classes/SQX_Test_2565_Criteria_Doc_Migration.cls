/**
* Test to ensure migration script SQX_6_4_Criteria_Document_Migration (using SQX_PostInstall_Processor) migrates all old audit criteria to equivalent controlled doc of record type audit criteria
*/
@IsTest
public class SQX_Test_2565_Criteria_Doc_Migration{

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole, 'adminUser');
        User adminUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole, 'adminUser1');
    }

}