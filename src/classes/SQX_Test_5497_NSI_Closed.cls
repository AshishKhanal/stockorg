/**
* unit test for when nsi record closed then account, contact, supplier document request and task information transfer.
*/
@isTest
public class SQX_Test_5497_NSI_Closed {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        
        System.runAs(adminUser){
            SQX_Part__c part = SQX_Test_Part.insertPart(null, adminUser, true, '');
            SQX_Standard_Service__c stdServiceName = new SQX_Standard_Service__c(name = 'StandardService');
            insert stdServiceName;
            SQX_Test_NSI nsi = new SQX_Test_NSI();
            nsi.nsi.SQX_Part__c = part.Id;
            nsi.nsi.SQX_Standard_Service__c = stdServiceName.Id;
            nsi.nsi.Description__c = 'Test Desc';
            nsi.save();
            SQX_New_Supplier_Introduction__c nsiObject=[SELECT Status__c, Record_Stage__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            
            //Create workflow policy(on-boarding step)
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Onboarding_Step__c wp1 = new SQX_Onboarding_Step__c();
            wp1.SQX_Parent__c =nsi.nsi.Id;
            wp1.Name = 'TestName1';
            wp1.Status__c = SQX_NSI.STATUS_DRAFT;
            wp1.Step__c =1;
            wp1.Due_Date__c = Date.today();
            wp1.SQX_User__c = adminUser.Id;
            wp1.Document_Name__c='Sample Document Name';
            wp1.Issue_Date__c = Date.today();
            wp1.Needs_Periodic_Update__c=true;
            wp1.Notify_Before_Expiration__c=10;
            wp1.Expiration_Date__c = Date.today()+1;
            wp1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            wp1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{});
        }
    }
    
    /**
    * GIVEN : given nsi record
    * WHEN : when closed with result approved
    * THEN : transfer information
    */  
    public static testMethod void givenNSI_WhenClosedWithResultApproved_ThenTransferInformation(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close NSI record with result approved
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id, Status__c, Record_Stage__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            // Submitting the NSI record
            nsi.Status__c = SQX_NSI.STATUS_DRAFT;
            nsi.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            nsi.Activity_Code__c = 'submit';
            nsi.Location__c = 'LalitPur';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Initiate the NSI record
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            nsi.Activity_Code__c = 'initiate';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi.Activity_Code__c = 'close';
            nsi.Location__c = 'LalitPur';
            nsi.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Assert: Ensured that account, contact and task information was transfered
            assertWhenNSIClosed(SQX_NSI.NSI_RESULT_APPROVED);
        }
    }
    
    /**
    * GIVEN : given nsi record
    * WHEN : when closed with result conditionally approved
    * THEN : transfer information
    */
    public static testMethod void givenNSI_WhenClosedWithResultConditionallyApproved_ThenTransferInformation(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close NSI record with result conditionally approved
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id, Status__c, Record_Stage__c, Result__c, Task_Name__c, 
                                                     SQX_Task_Assignee__c, Due_Date__c, Activity_Comment__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            
            // Submitting the NSI record
            nsi.Status__c = SQX_NSI.STATUS_DRAFT;
            nsi.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            nsi.Activity_Code__c = 'submit';
            nsi.Location__c = 'LalitPur';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Act: Saving the record (capture NSI status not equal to 'Draft')
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            nsi.Activity_Code__c = 'initiate';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi.Activity_Code__c = 'close';
            nsi.Result__c = SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED;
            nsi.Task_Name__c = 'TaskName';
            nsi.SQX_Task_Assignee__c = adminUser.Id;
            nsi.Due_Date__c = Date.today();
            nsi.Activity_Comment__c = 'Comments';
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Assert: Ensured that account, contact and task information was transfered
            assertWhenNSIClosed(SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED);
        }
    }

    /**
    * GIVEN : given nsi record
    * WHEN : when closed with result conditionally approved
    * THEN : name and assignee fields required
    */ 
    public static testMethod void givenNSI_WhenClosedWithResultConditionallyApproved_ThenNameAndAssigneeRequired(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: retrieve NSI record
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Id, Status__c, Record_Stage__c, Result__c, Task_Name__c, 
                                                     SQX_Task_Assignee__c, Due_Date__c, Activity_Comment__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
                                                     
            //Act: close the NSI record
            nsi1.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi1.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi1.Result__c = SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED;
            nsi1.Due_Date__c = Date.today();
            nsi1.Activity_Comment__c = 'Comments';
        
            List<Database.SaveResult> result =new SQX_DB().continueOnError().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi1 }, new List<SObjectField>{});
            
            //Assert: Ensured that name and assignee fields required
            System.assert(result[0].isSuccess() == false, 'Name and Assignee fields required when supplier introduction record closed with result conditionally approved.');
            System.assertEquals('Please fill Name and Assignee fields.', result[0].getErrors().get(0).getMessage());
        }  
    }
    
    /**
    * GIVEN : given nsi record with linked account
    * WHEN : when closed 
    * THEN : updated existing account approval status
    */
    public static testMethod void givenNSIWithLinkedAccount_WhenClosed_ThenUpdateAccountApprovalStatus(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close NSI record with result conditionally approved
            Account account = SQX_Test_Account_Factory.createAccount();
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id, Status__c, SQX_Account__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.SQX_Account__c = account.Id;
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Id, Status__c, SQX_Account__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi1.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi1.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi1.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            nsi1.Location__c = 'Lalitpur';
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi1 }, new List<SObjectField>{});
            
            //Assert: Ensured that account approval status is updated
            Account acct =[SELECT Approval_Status__c, Site FROM Account WHERE Id =: account.Id];
            System.assertEquals(SQX_NSI.NSI_RESULT_APPROVED, acct.Approval_Status__c);
            System.assertEquals(true, nsi1.Location__c != acct.Site, 'Expected Account.Site, not to equal NSI.Location when Account not created from NSI.');
        }
    }
    
    /**
    * GIVEN : given nsi record
    * WHEN : when closed with result rejected
    * THEN : contact informaton transfer
    */  
    public static testMethod void givenNSI_WhenClosedWithResultRejected_ThenContactInfoNotTransfer(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close NSI record with result rejected
            Account account = SQX_Test_Account_Factory.createAccount();
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id, Status__c, SQX_Account__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.SQX_Account__c = account.Id;
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Id, Status__c, SQX_Account__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi1.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi1.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi1.Result__c = SQX_NSI.NSI_RESULT_REJECTED;
            nsi1.Location__c = 'Lalitpur';
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi1 }, new List<SObjectField>{});
            
            //Assert: Ensured that contact should not be transfered
            assertWhenNSIClosed(SQX_NSI.NSI_RESULT_REJECTED);
        }
    }
    
    /**
    * Ensured that when nsi record is closed
    * @param result nsi result value
    */
    private static void assertWhenNSIClosed(String result){
        SQX_New_Supplier_Introduction__c nsi1 = [SELECT SQX_Account__c, Company_Name__c, Supplier_Number__c, First_Name__c, Last_Name__c, Phone__c, Location__c
                                                     , Task_Name__c, SQX_Task_Assignee__c, Due_Date__c, Activity_Comment__c, Part_Name__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
        if(result == SQX_NSI.NSI_RESULT_REJECTED){
            List<Contact> contact1 = [SELECT FirstName, LastName, Phone FROM Contact WHERE AccountId=: nsi1.SQX_Account__c];
            //Assert: Ensured that contact was not created
            System.assertEquals(0, contact1.size());
        } else{
            List<Account> account = [SELECT Id, Name, AccountNumber, Site FROM Account WHERE Name = 'TEST_ACCOUNT'];
            //Assert: Ensured that account was created
            System.assertEquals(1, account.size());
            System.assertEquals(nsi1.Company_Name__c, account[0].Name);
            System.assertEquals(nsi1.Supplier_Number__c, account[0].AccountNumber);
            System.assertEquals(nsi1.location__c, account[0].Site, 'Expected NSI.location and Account.Site to be same, when Account is created from NSI only with Approved Result');
            
            List<Contact> contact = [SELECT FirstName, LastName, Phone FROM Contact WHERE AccountId=: account[0].Id];
            System.assertEquals(1, contact.size());
            //Assert: Ensured that contact was created
            System.assertEquals(nsi1.First_Name__c, contact[0].FirstName);
            System.assertEquals(nsi1.Last_Name__c, contact[0].LastName);
            System.assertEquals(nsi1.Phone__c, contact[0].Phone);

            SQX_Onboarding_Step__c wp = [SELECT Issue_Date__c, Expiration_Date__c,Document_Name__c,Notify_Before_Expiration__c,Needs_Periodic_Update__c,
                                         SQX_Parent__r.Part_Name__c, SQX_Parent__r.SQX_Part__c, SQX_Parent__r.SQX_Standard_Service__c, SQX_Parent__r.Description__c FROM SQX_Onboarding_Step__c WHERE RecordTypeId=:SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST)];
            List<SQX_Certification__c> sdr = [SELECT Issue_Date__c, Expire_Date__c,Document_Name__c,Notify_Before_Expiration__c,Needs_Periodic_Update__c,Part_Name__c,
                                              SQX_Part__c, SQX_Standard_Service__C, Description__c
                                              FROM SQX_Certification__c LIMIT 1];

            //Assert: Ensured that supplier document request was created
            System.assertEquals(1, sdr.size());
            System.assertEquals(wp.Issue_Date__c, sdr[0].Issue_Date__c);
            System.assertEquals(wp.Expiration_Date__c, sdr[0].Expire_Date__c);
            System.assertEquals(wp.Document_Name__c, sdr[0].Document_Name__c);
            System.assertEquals(wp.Notify_Before_Expiration__c, sdr[0].Notify_Before_Expiration__c);
            System.assertEquals(wp.Needs_Periodic_Update__c, sdr[0].Needs_Periodic_Update__c);
            System.assertEquals(wp.SQX_Parent__r.Part_Name__c, sdr[0].Part_Name__c);
            System.assertEquals(wp.SQX_Parent__r.SQX_Part__c, sdr[0].SQX_Part__c);
            System.assertEquals(wp.SQX_Parent__r.SQX_Standard_Service__c, sdr[0].SQX_Standard_Service__c);
            System.assertEquals(wp.SQX_Parent__r.Description__c, sdr[0].Description__c);

            
            if(result == SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED){
                List<Task> task = [SELECT Subject, OwnerId, ActivityDate, Description FROM Task];
                
                //Assert: Ensured that task was created
                System.assertEquals(1, task.size());
                System.assertEquals(nsi1.Task_Name__c, task[0].Subject);
                System.assertEquals(nsi1.SQX_Task_Assignee__c, task[0].OwnerId);
                System.assertEquals(nsi1.Due_Date__c, task[0].ActivityDate);
                System.assertEquals(nsi1.Activity_Comment__c, task[0].Description);
                System.assertEquals(nsi1.location__c, account[0].Site, 'Expected NSI.location and Account.Site to be same, when Account is created from NSI with Conditionally Approved Result');
            }
        }
    }

    /**
    * GIVEN : given nsi record with linked account
    * WHEN : when closed with result rejected
    * THEN : updated existing account approval status
    */
    public static testMethod void givenNSIWithLinkedAccount_WhenClosedWithResultRejected_ThenUpdateAccountApprovalStatus(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close NSI record with result conditionally approved
            Account account = SQX_Test_Account_Factory.createAccount();
            SQX_New_Supplier_Introduction__c nsi = [SELECT Id, Status__c, SQX_Account__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi.SQX_Account__c = account.Id;
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Id, Status__c, SQX_Account__c, Company_Name__c FROM SQX_New_Supplier_Introduction__c LIMIT 1];
            nsi1.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi1.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi1.Result__c = SQX_NSI.NSI_RESULT_REJECTED;
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi1 }, new List<SObjectField>{});
            
            //Assert: Ensured that account approval status is updated and account name not overrided
            Account acct =[SELECT Approval_Status__c FROM Account WHERE Id =: account.Id];
            System.assertEquals(SQX_NSI.NSI_RESULT_REJECTED, acct.Approval_Status__c);
            System.assertNotEquals(account.Name, nsi1.Company_Name__c);
        }
    }

    /**
    * GIVEN: given nsi record 
    *             i)closed the record
    *             ii)void the record
    * WHEN: when user try to add onboarding step 
    * THEN: user gets validation error message 'Cannot modify locked On-boarding Step record. Either record is locked or parent Supplier Introduction record is locked.'
    */
    @isTest
    public static void givenNsiRecordVoidOrClose_WhenUserTryToAddOnBoardingStep_ThenUserGetErrorMessage(){
        final String EXPECTED_VALIDATION_ERROR_MSG = 'Record status or Step status does not support the action performed.';

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            
            // Arrange:create a nsi record
            SQX_Test_NSI nsiforClosed= new SQX_Test_NSI().save();
            nsiforClosed.submit();
            nsiforClosed.initiate();
            // Close the nsi record
            nsiforClosed.close();
            
            List<SQX_Onboarding_Step__c> onboardingStep = new SQX_Onboarding_Step__c[]{
                                                          new SQX_Onboarding_Step__c (SQX_Parent__c=nsiforClosed.nsi.id,
                                                                                      Step__c=1,
                                                                                      SQX_User__c=UserInfo.getUserId(),
                                                                                      Due_Date__c=Date.today()
                                                                                   )};
            // Act :Try To insert the OnBoarding Step
            List<Database.SaveResult> resultforClosed = new SQX_DB().continueOnError().op_insert(onboardingStep, new List<Schema.SObjectField>{});

            // Assert: Verify the  Validation Rule Error message when user try to add the onbarding step 
            System.assert(!resultforClosed[0].isSuccess(), 'Expected Onboarding Step record not to be inserted for closed NSI rercord but is being inserted.');
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, resultforClosed[0].getErrors()[0].getMessage());

            // Arrange: create a nsi record
            SQX_Test_NSI nsiforVoid= new SQX_Test_NSI().save();
            
            // void  the nsi record
            nsiforVoid.void();
                        
            onboardingStep = new SQX_Onboarding_Step__c[]{
                             new SQX_Onboarding_Step__c (SQX_Parent__c=nsiforVoid.nsi.id,
                                                         Step__c=1,
                                                         SQX_User__c=UserInfo.getUserId(),
                                                         Due_Date__c=Date.today()
                                                       )};
            // Act :Try To insert the OnBoarding Step
            List<Database.SaveResult> resultforVoid = new SQX_DB().continueOnError().op_insert(onboardingStep, new List<Schema.SObjectField>{});

            // Assert: Verify the  Validation Rule Error message when user try to add the onbarding step 
            System.assert(!resultforVoid[0].isSuccess(), 'Expected Onboarding Step record not to be inserted for voided NSI rercord but is being inserted. ');
            System.assertEquals(EXPECTED_VALIDATION_ERROR_MSG, resultforVoid[0].getErrors()[0].getMessage());
        }
    }

    /*
     * Given: Supplier Introduction record
     * When: Record is closed with null Result value
     * Then: Throw validation error
     */
    public static testMethod void givenSupplierIntroduction_WhenRecordIsClosedWithoutResultValue_ThenThrowError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser });        
        System.runAs(standardUser) {
            
            // Arrange: Create, Submit, Initiate and Close the Supplier Deviation record
            List<SQX_New_Supplier_Introduction__c> supplierIntroductions = new List<SQX_New_Supplier_Introduction__c>();
            SQX_Test_NSI si1 = new SQX_Test_NSI().save();
            SQX_Test_NSI si2 = new SQX_Test_NSI().save();
            
            si1.submit();
            si1.initiate();
            si1.nsi.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            supplierIntroductions.add(si1.nsi);
            
            si2.submit();
            si2.initiate();
            si2.nsi.Result__c = null;
            si2.nsi.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE; 
            supplierIntroductions.add(si2.nsi);
            
            //Act:Insert the record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(supplierIntroductions, new List<Schema.SObjectField>{});
            
            // Assert: Validation error
            System.assertEquals(true, result[0].isSuccess(), 'Supplier Introduction should be saved. ' + result[0].getErrors());
            System.assertEquals(false, result[1].isSuccess(), 'Supplier Introduction should not be saved. ' + result[1].getErrors());
            String expErrMsg = 'Supplier Introduction Result is required.';
            System.assert(SQX_Utilities.checkErrorMessage(result[1].getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result[1].getErrors());
        }
    }

    /*
     * Given:2 nsi record  with same part , service and account
     * When: nsi Record is closed 
     * Then: nsi should closed successfully
     */
    @isTest
    public static void givenNSIPartOrService_WhenUserClosedNsi_ThenNSIShouldClosedSuccessfully(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        SQX_Test_NSI.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser) {
            //Arrange : create part
            SQX_Part__c part=SQX_Test_3103_Inbound_MT_Processing.createPart();
            part.Active__c=true;
            update part;
            // service
            SQX_Standard_Service__c stdService=SQX_Test_3103_Inbound_MT_Processing.createStandardService();

            //Arrange:add account in nsi 
            Account account = SQX_Test_Account_Factory.createAccount();
            Test.startTest();
            //Arrange: create nsi first record
            SQX_Test_NSI nsi1 = new SQX_Test_NSI().save();
            nsi1.nsi.SQX_Account__c = account.Id;
            nsi1.nsi.SQX_Part__c=part.id;
            nsi1.nsi.SQX_Standard_Service__c=stdService.id;
            nsi1.save();
            nsi1.submit();
            nsi1.initiate();
            //Act: Close NSI record
            nsi1.nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi1.nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi1.nsi.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            nsi1.nsi.Location__c = 'Lalitpur';
            nsi1.nsi.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            List<Database.SaveResult> result =  new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi1.nsi }, new List<SObjectField>{});
            //Assert: NSI record save successfuly
            System.assertEquals(true, result[0].isSuccess(), 'nsi record get closed successfully');
            
            //Arrange: Create 2nd nsi record with same account, part and service used in forst nsi record
            SQX_Test_NSI nsi2 = new SQX_Test_NSI().save();
            nsi2.nsi.SQX_Account__c = account.Id;
            nsi2.nsi.SQX_Part__c=part.id;
            nsi2.nsi.SQX_Standard_Service__c=stdService.id;
            nsi2.nsi.First_Name__c = 'FName';
            nsi2.nsi.Last_Name__c = 'LName';
            nsi2.save();
            nsi2.submit();
            nsi2.initiate();
            //Act: Close NSI record
            nsi2.nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi2.nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi2.nsi.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            nsi2.nsi.Location__c = 'Lalitpur';
            nsi2.nsi.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            List<Database.SaveResult> result1 =  new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi2.nsi }, new List<SObjectField>{});
            //Assert: Ensured that nsi record save successfully.
            System.assertEquals(true, result1[0].isSuccess(), 'nsi record get closed successfully');
            System.assertEquals(nsi1.nsi.SQX_Part__c, nsi2.nsi.SQX_Part__c);
            System.assertEquals(nsi1.nsi.SQX_Standard_Service__c, nsi2.nsi.SQX_Standard_Service__c);
            Test.stopTest();

            //Inactivate supplier part and service
            SQX_Supplier_Service__c ss =[SELECT Id, Active__c FROM SQX_Supplier_Service__c WHERE Standard_Service__c =: nsi2.nsi.SQX_Standard_Service__c];
            ss.Active__c = false;
            update ss;
            SQX_Supplier_Part__c sp =[SELECT Id, Active__c FROM SQX_Supplier_Part__c WHERE Part__c =: nsi2.nsi.SQX_Part__c];
            sp.Active__c = false;
            update sp;
            
            //Arrange: Create 3rd nsi record with same account, part and service used in first nsi record
            SQX_Test_NSI nsi3 = new SQX_Test_NSI().save();
            nsi3.nsi.SQX_Account__c = account.Id;
            nsi3.nsi.SQX_Part__c=part.id;
            nsi3.nsi.SQX_Standard_Service__c=stdService.id;
            nsi3.nsi.First_Name__c = 'FName1';
            nsi3.nsi.Last_Name__c = 'LName1';
            nsi3.nsi.Phone__c ='988888';
            nsi3.save();
            nsi3.submit();
            nsi3.initiate();
            //Act: close nsi record
            nsi3.nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi3.nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi3.nsi.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            nsi3.nsi.Location__c = 'Lalitpur_1';
            nsi3.nsi.Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE;
            List<Database.SaveResult> result2 =  new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi3.nsi }, new List<SObjectField>{});
           //Assert: Ensured that inactive service and part activated and save successfully
            System.assertEquals(true, result2[0].isSuccess(), 'nsi record get closed successfully');
            SQX_Supplier_Service__c ss1 =[SELECT Id, Active__c FROM SQX_Supplier_Service__c WHERE Standard_Service__c =: nsi3.nsi.SQX_Standard_Service__c];
            system.assertEquals(true, ss1.Active__c);
            SQX_Supplier_Part__c sp1 =[SELECT Id, Active__c FROM SQX_Supplier_Part__c WHERE Part__c =: nsi3.nsi.SQX_Part__c];
            system.assertEquals(true, sp1.Active__c);
        }
    } 

    /*
     * Given: Supplier Introduction record with disapproved account
     * When: Record is closed with Approved Result value
     * Then: successfully closed nsi record
     */
    public static testMethod void givenNSIWithDisapprovedAccount_WhenNSIClosed_ThenClosedSuccessfully(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_Part__c part1 = SQX_Test_Part.insertPart(null, adminUser, true, '');
            //Arrange: Create Disapproved account record
            Account account = SQX_Test_Account_Factory.createAccount();
            account.Approval_Status__c = 'Disapproved';
            update account;
            //create nsi record with disapproved account
            SQX_New_Supplier_Introduction__c  nsi = new SQX_New_Supplier_Introduction__c(Name = 'AUTO', 
                                                   Company_Name__c = 'TEST_ACCOUNT', 
                                                   Supplier_Number__c = 'SUP_0001', 
                                                   First_Name__c = 'Test', 
                                                   Last_Name__c = 'User', 
                                                   Phone__c = '1234567890',
                                                   SQX_Part__c = part1.Id,
                                                   SQX_Account__c  = account.Id);
            insert nsi;
            // Submitting the NSI record
            nsi.Status__c = SQX_NSI.STATUS_DRAFT;
            nsi.Record_Stage__c = SQX_NSI.STAGE_TRIAGE;
            nsi.Activity_Code__c = 'submit';
            nsi.Location__c = 'LalitPur';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Initiate the NSI record
            nsi.Status__c = SQX_NSI.STATUS_OPEN;
            nsi.Record_Stage__c = SQX_NSI.STAGE_IN_PROGRESS;
            nsi.Activity_Code__c = 'initiate';
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            nsi.Status__c =SQX_NSI.STATUS_CLOSED;
            nsi.Record_Stage__c = SQX_NSI.STAGE_CLOSED;
            nsi.Activity_Code__c = 'close';
            nsi.Location__c = 'LalitPur';
            nsi.Result__c = SQX_NSI.NSI_RESULT_APPROVED;
            
            //Act: close the NSI record
            new SQX_DB().op_update(new List<SQX_New_Supplier_Introduction__c>{ nsi }, new List<SObjectField>{});
            
            //Assert: Ensured that account approval status is updated
            Account acct =[SELECT Approval_Status__c, Site FROM Account WHERE Id =: account.Id];
            System.assertEquals(SQX_NSI.NSI_RESULT_APPROVED, acct.Approval_Status__c);
            
        }
    }
}