/**
 * test for follow-up audit
 */

@isTest
public class SQX_Test_1476_Followup_Audit{

    private static Boolean runAllTests = true,
                            run_auditIsClosedAnfFollowupAuditIsRequired_FollowupAuditIsCreated = false,
                            run_auditIsClosedAnfFollowupAuditIsNotRequired_FollowupAuditIsNotCreated = false;

    /*
    *   test for audit when the follow-up audit is required
    */
    public static testmethod void auditIsClosedAnfFollowupAuditIsRequired_FollowupAuditIsCreated(){
        if(!runAllTests && !run_auditIsClosedAnfFollowupAuditIsRequired_FollowupAuditIsCreated){
            return;
        }         
        //Arrange: Save an audit
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User standardUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runAs(standardUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);

            // Act: Close the audit with follow up audit required and set followup aduit date
            audit.audit.Follow_up_Required__c = true;
            audit.audit.Follow_up_Date__c = Date.Today().addDays(150);
            audit.audit.End_Date__c = Date.Today().addDays(50);
            audit.setStatus(SQX_Audit.STATUS_COMPLETE); // Audit should be complete for this test case
            audit.setStage(SQX_Audit.STAGE_RESPOND);
            audit.save();

            Map<String, String> params = new Map<String,String>();
            params.put('nextAction', 'close');
            SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, null, null,params, null);
            
            List<SQX_Audit__c> auditList = new List<SQX_Audit__c>();
            auditList = [SELECT Id FROM SQX_Audit__c];

            SQX_Audit__c followupAudit =  new SQX_Audit__c();
            followupAudit = [SELECT Id, Follow_up_Required__c, Follow_up_Date__c, 
                                Audit_Category__c, Audit_Type__c, Title__c, Start_Date__c,
                                SQX_Auditee_Contact__c, SQX_Department__c, SQX_Division__c, 
                                Account__c, Primary_Contact_Name__c,
                                SQX_Business_Unit__c, SQX_Site__c
                            FROM SQX_Audit__c
                            WHERE SQX_FollowUp_Audit__c =: audit.audit.Id];

            //Assert: Follow up audit must be created and all the required fields and auditee fields must be copied
            System.assertEquals(2, auditList.size(), 'Expected two audits to be created ie one main audit and other is followup audit');


            System.assertEquals(followupAudit.Audit_Category__c, audit.audit.Audit_Category__c);
            System.assertEquals(followupAudit.Audit_Type__c, audit.audit.Audit_Type__c);
            System.assertEquals(followupAudit.Start_Date__c, audit.audit.Follow_up_Date__c);
            System.assertEquals(followupAudit.Title__c, audit.audit.Title__c);
            System.assertEquals(followupAudit.SQX_Auditee_Contact__c, audit.audit.SQX_Auditee_Contact__c);
            System.assertEquals(followupAudit.SQX_Department__c, audit.audit.SQX_Department__c);
            System.assertEquals(followupAudit.SQX_Division__c, audit.audit.SQX_Division__c);
            System.assertEquals(followupAudit.Account__c, audit.audit.Account__c);
            System.assertEquals(followupAudit.Primary_Contact_Name__c, audit.audit.Primary_Contact_Name__c);
            System.assertEquals(followupAudit.SQX_Business_Unit__c, audit.audit.SQX_Business_Unit__c);
            System.assertEquals(followupAudit.SQX_Site__c, audit.audit.SQX_Site__c);

        }
    }  
    /*
    *   test for audit when the follow-up audit is not required
    */
    public static testmethod void auditIsClosedAnfFollowupAuditIsNotRequired_FollowupAuditIsNotCreated(){
        if(!runAllTests && !run_auditIsClosedAnfFollowupAuditIsNotRequired_FollowupAuditIsNotCreated){
            return;
        }         
        //Arrange: Save an audit
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User standardUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runAs(standardUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);

            // Act: Close the audit with follow up audit not required 
            audit.audit.Follow_up_Required__c = false;
            //audit.audit.Follow_up_Date__c = Date.Today().addDays(150);
            audit.audit.End_Date__c = Date.Today().addDays(50);
            audit.setStatus(SQX_Audit.STATUS_COMPLETE); // Audit should be complete for this test case
            audit.setStage(SQX_Audit.STAGE_RESPOND);
            audit.save();

            Map<String, String> params = new Map<String,String>();
            params.put('nextAction', 'close');
            SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, null, null,params, null);
            
            List<SQX_Audit__c> auditList = new List<SQX_Audit__c>();
            auditList = [SELECT Id FROM SQX_Audit__c];

            //Assert: Follow up audit must not be created
            System.assertEquals(1, auditList.size(), 'Expected to be only one audit as followup audit is not required');


        }
    }   
}