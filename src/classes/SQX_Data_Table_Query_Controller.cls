/**
 * Action handler invoked when getting data from the sobject table controller
 */
public with sharing class SQX_Data_Table_Query_Controller {

    // Standard fields to be omitted
    private static Set<String> fieldsToOmit =
        new Set<String>
            {
                'LastViewedDate',
                'LastReferencedDate'
            };

    /**
     * Deletes a given record for the user
     */
    @AuraEnabled
    public static void deleteRecord(Id recordToDelete) {

        SObject obj = recordToDelete.getSObjectType().newSObject();
        obj.Id = recordToDelete;
        Database.DeleteResult result = new SQX_DB().continueOnError().op_delete(new SObject[] { obj })[0];

        if(result.isSuccess() == false) {
            throw new AuraHandledException('Error occurred while deleting record. Actual Error: ' + result.getErrors().get(0).getMessage());
        }
    }

    /**
     * Saves the records
     * @param recordsList list of records to update
	*/
    @AuraEnabled
    public static void saveRecords(List<SObject> recordsList){

        try {
            new SQX_DB().op_update(recordsList, new List<SObjectField>{});
        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * This method returns the objects and the field description for generating columns.
     * @param  objectName    the name of the object that is to be retrievedfetched
     * @param  fieldList     the list of fields for the query that are to be fetched. Comma separated list
     */
    @AuraEnabled
    public static SQX_ObjectsWithSchema getFieldSchemaForObject(String objectName, String fieldList) {
        SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        SQX_ObjectsWithSchema res = new SQX_ObjectsWithSchema();

        if(objType == null) {
            throw new AuraHandledException('Object type ' + objectName + 'not found');
        }
        initializeFields(fieldList, objType.getDescribe().fields.getMap(), true, res, false);
        return res;
    }

    /**
     * Queries SF Repository and returns matching record along with field description to be shown in the UI
     * @param  mainRecordId  The Id of the main record, which is the context for the current record value
     * @param  objectName    the name of the object that is to be retrieved
     * @param  fieldList     the list of fields for the query that are to be fetched. Comma separated list
     * @param  whereClause   the filter clause that matches SQX_DynamicQuery.Filter (kendo filters) structure
     * @param  altFilter     the alternate filter that has to be ANDed to the where clause.
     * @param  orderbyFields the list of fields by which the result is to be ordered
     * @param  sortAscending true if ascending, else false
     * @param  pageNumber    the page that needs to be fetched
     * @param  pageSize      the size of the page
     * @return               returns the objects and the field description for generating columns and displaying records
     */
    @AuraEnabled
    public static SQX_ObjectsWithSchema getObjects(String mainRecordId, String objectName, String fieldList, String whereClause, String altFilter, String orderbyFields, Boolean sortAscending, Integer pageNumber, Integer pageSize, String purpose) {
        SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        SQX_ObjectsWithSchema res = new SQX_ObjectsWithSchema();

        if(objType == null) {
            throw new AuraHandledException('Object type ' + objectName + 'not found');
        }

        if(objType == ProcessInstanceWorkItem.SObjectType) {
            return getApprovalOf(mainRecordId, purpose);
        }
        
        res.actions = getActionsFor(objectName, purpose);

        final DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String,SObjectField> fieldMap = objDescribe.fields.getMap();

        List<SObjectField> fields = initializeFields(fieldList, fieldMap, true, res, null);
        List<SObjectField> orderBy = initializeFields(orderByFields, fieldMap, false, null, null);

        res.recordTypes = new SQX_Lightning_Helper.SQX_Save_UI_Detail(objectName, null, null, null).RecordTypes;
        SQX_DynamicQuery.Filter filter = null;
        if(!String.isBlank(whereClause)) {
            filter = ( SQX_DynamicQuery.Filter) JSON.deserializeStrict(whereClause, SQX_DynamicQuery.Filter.class);
        }

        // combine alternate filter if present
        if(!String.isBlank(altFilter)) {

            SQX_DynamicQuery.Filter altFilterObj =  ( SQX_DynamicQuery.Filter) JSON.deserializeStrict(altFilter, SQX_DynamicQuery.Filter.class);
            if(filter == null) {
                filter = altFilterObj;
            } else {
                SQX_DynamicQuery.Filter mainFilter = filter;
                filter = new SQX_DynamicQuery.Filter();
                filter.logic = SQX_DynamicQuery.FILTER_LOGIC_AND;
                filter.filters = new SQX_DynamicQuery.Filter[] {
                    altFilterObj, mainFilter
                };
            }
        }

        res.sobjects = SQX_DynamicQuery.evaluateWithCurrentRecord(getMainRecord(mainRecordId), Integer.valueOf(pageNumber), Integer.valueOf(pageSize), objDescribe , fields, filter, orderBy, sortAscending);
        res.mainRecordName = objDescribe.getLabelPlural();

        return res;
    }
    
    /**
     * This method returns the applicable actions for the given purpose
     * @param  recordType the object whose applicable actions are to be returned
     * @param  purpose    the purpose for which actions are to be identified
     */
    private static CQ_Action__mdt[] getActionsFor(String recordType, String purpose){
        return SQX_Lightning_Helper.getActionsFor(recordType, purpose);
    }

    /**
     * Constructs a SObjectField list from comma separated list
     * @param  fieldList       the comma separated list of fields
     * @param  fieldMap        the map of fields that are valid
     * @param  followReference true if lookup/reference fields name are to be followed
     * @param  res             the schema object where fields are to be set
     * @param  makeFieldEditable flag to make field editable/uneditable externally.
     * @return returns the list of fields that were identified
     */
    private static List<SObjectField> initializeFields(String fieldList, Map<String, SObjectField> fieldMap, Boolean followReference, SQX_ObjectsWithSchema res, Boolean makeFieldEditable) {
        List<SObjectField> fields = new List<SObjectField>();

        if(!String.isBlank(fieldList)) {
            for(String field : fieldList.split(',')) {
                field = field.trim();
                if(fieldMap.containsKey(field)) {
                    SObjectField sfield = fieldMap.get(field);
                    DescribeFieldResult fieldDesc = sfield.getDescribe();
                    if(res != null) {
                        SQX_Field f = new SQX_Field();
                        f.label = fieldDesc.getLabel();
                        f.editable = makeFieldEditable == null ? fieldDesc.isUpdateable() : makeFieldEditable;
                        f.sortable = fieldDesc.isSortable();
                        f.setFieldTypeEnum(fieldDesc.getType());

                        if(followReference && fieldDesc.getType() == DisplayType.Reference){
                            f.fieldName = fieldDesc.getRelationshipName() + '.Name';
                        }
                        else {
                            if(!fieldsToOmit.contains(fieldDesc.getName())){
                                f.fieldName = fieldDesc.getName();
                            }
                        }

                        res.fields.add(f);

                    }
                    fields.add(fieldMap.get(field));
                }
                else {
                    throw new AuraHandledException('Invalid field ' + field);
                }
            }
        }

        return fields;
    }

    /**
     * Queries the object with given id.
     * @param  mainRecordId the id of the record whose fields are to be fetched
     * @return returns the sobject with all the fields in it.
     */
    private static SObject getMainRecord(Id mainRecordId) {
        //TODO: move to dynamic query and support this pattern there are lots of places where
        //      similar logic is being used
        SQX_DynamicQuery.Filter recordFilter = new SQX_DynamicQuery.Filter();
        recordFilter.field = 'Id';
        recordFilter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
        recordFilter.value = mainRecordId;
        Id mr = Id.valueOf(mainRecordId);
        return SQX_DynamicQuery.getallFields(mr.getSObjectType().getDescribe(), new Set<SObjectField>(), recordFilter, new List<SObjectField>(), true).get(0);

    }

    /**
     * Returns the approval data for the object. Note no pagination is done.
     */
    private static SQX_ObjectsWithSchema getApprovalOf(Id mainRecordID, String purpose) {
        SQX_ObjectsWithSchema obj = new SQX_ObjectsWithSchema();
        obj.mainRecordName = Label.CQ_UI_Controlled_Document_Approval_History;
        if(purpose == 'approval'){
            String objName = mainRecordID.getSObjectType().getDescribe().getName();
            obj.actions = getActionsFor(objName, purpose);
            obj.currentUser = SQX_Utilities.getCurrentUser();
        }
        obj.sobjects = [SELECT Id, TargetObjectId,(SELECT ActorId, StepStatus, toLabel(StepStatus) StepStatusLabel, Actor.Name, OriginalActorId, CreatedDate, OriginalActor.Name,ProcessNode.Name, IsPending FROM StepsAndWorkItems ORDER BY CreatedDate DESC) FROM ProcessInstance WHERE TargetObjectId = : mainRecordId Order By CreatedDate DESC];
        return obj;
    }

    /**
     * Encapsulates the result with main record name, fields and results
     */
    public class SQX_ObjectsWithSchema {
        @AuraEnabled
        public String mainRecordName {get; set;}

        @AuraEnabled
        public User currentUser {get; set;}

        @AuraEnabled
        public SQX_Field[] fields {get; set;}

        @AuraEnabled
        public SObject[] sobjects {get; set;}

        @AuraEnabled
        public CQ_Action__mdt[] actions {get; set;}

        @AuraEnabled
        public SQX_Lightning_Helper.SQX_Save_UI_RecordType[] recordTypes {get; set;}

        public SQX_ObjectsWithSchema() {
            fields = new List<SQX_Field>();
        }
    }

    /**
     * The field/column in each query
     */
    public class SQX_Field {
        @AuraEnabled
        public String label {get; set;}

        @AuraEnabled
        public String fieldType {get; set;}

        @AuraEnabled
        public Boolean editable {get; set;}

        @AuraEnabled
        public String fieldName {get; set;}
        
        @AuraEnabled
        public Boolean sortable {get; set;}

        public void setFieldTypeEnum(DisplayType display) {
            switch on display {
                when Date,DateTime {
                    fieldType = 'date';
                }
                when Double {
                    fieldType = 'number';
                }
                when else {
                    fieldType = String.valueOf(display).toLowerCase();
                }
            }
        }
    }
}