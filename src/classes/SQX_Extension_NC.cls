/**
* this controller class is responsible providing various data to the NC form.
*/
public with sharing class SQX_Extension_NC extends SQX_Extension_UI {

    /**
    * Default constructor for the extension.
    * @param controller the standard controller related to the object
    */
    public SQX_Extension_NC(ApexPages.StandardController controller) {
        super(controller, new List<String> {'Status__c'});

        SQX_Nonconformance__c nc = (SQX_Nonconformance__c)controller.getRecord();


        //we don't need NC esig validation if NC is new or is in draft state.
        Boolean isRecInDraftOrNew = !(nc.Id != null && nc.Status__c != SQX_NC.STATUS_DRAFT);
        addStaticPolicy('save', getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.Nonconformance));
        
        this.purposeOfSigMap.putAll(new Map<String, String>{
                'save' => Label.SQX_PS_NC_Saving_the_record,
                'submit' => Label.SQX_PS_NC_Submitting_the_record,
                'takeownershipandinitiate' => Label.SQX_PS_NC_Taking_Ownership_And_Initiating_the_record,
                'close' =>  Label.SQX_PS_NC_Closing_the_record,
                'void' => Label.SQX_PS_NC_Voiding_the_record,
                'approve' => Label.SQX_PS_NC_Approving_Response,
                'reject' => Label.SQX_PS_NC_Rejecting_Response,
                'recall' => Label.SQX_PS_NC_Recalling_Response,
                'submitresponse' => Label.SQX_PS_NC_Submitting_Response,
                'reopen' => Label.SQX_PS_NC_Reopening_the_record
            });

        this.addActionSaveAfterDraft();
        fetchRecordTypesOf.add(SQX.Finding);
  
    }
    

    /**
    * Returns the configuration used for serializing the data. This configuration object
    * guides which object should be followed and how
    * @return returns the serialization configuration
    */    
    public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){

        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        
        return new SObjectDataLoader.SerializeConfig()
                    .followChild(SQX_Investigation__c.SQX_NonConformance__c)
                    .followChild(SQX_Finding_Response__c.SQX_NonConformance__c)
                    .followChild(SQX_Containment__c.SQX_NonConformance__c)
                    .follow(SQX_Response_Inclusion__c.SQX_Investigation__c)
                    .followChild(SQX_Response_Inclusion__c.SQX_Response__c)
                    .followChild(SQX_Root_Cause__c.SQX_Investigation__c)
                    .followChild(SQX_Root_Cause__c.SQX_Root_Cause_Code__c)
                    .followChild(SQX_Investigation_Tool__c.SQX_Investigation__c)
                    .followChild(SQX_Action_Plan__c.SQX_Investigation__c)
                    .followChild(SQX_NC_Defect__c.SQX_NonConformance__c)
                    .followChild(SQX_Resp_Inclusion_Approval__c.SQX_Response_Inclusion__c)
                    .followChild(SQX_Disposition__c.SQX_Nonconformance__c)
                    .followChild(SQX_NC_Impacted_Product__c.SQX_Nonconformance__c)
                    .followChild(SQX_NC_CAPA__c.SQX_NonConformance__c)
                    .followChild(SQX_NC_Record_Activity__c.SQX_NonConformance__c)
                    .followChild(SQX_Supplier_Deviation_NC__c.SQX_Nonconformance__c)
                    .followChild(SQX_Escalation_Reference__c.SQX_Related_Nonconformance__c)
                    
                    .getFieldsFor(SQX_Part__c.getSObjectType(), new List<SObjectField> {SQX_Part__c.Part_Number_and_Name__c, SQX_Part__c.Name, SQX_Part__c.Part_Number__c})
                    
                    /*attachments supported for*/
                    .followAttachmentOf(globalDescribe.get(SQX.Containment))
                    .followAttachmentOf(globalDescribe.get(SQX.Investigation))
                    .followAttachmentOf(globalDescribe.get(SQX.NonConformance))
                    .followAttachmentOf(globalDescribe.get(SQX.Evidence))
                    .followAttachmentOf(globalDescribe.get(SQX.Defect))
                    .followAttachmentOf(globalDescribe.get(SQX.Disposition))
                    .followAttachmentOf(globalDescribe.get(SQX.ImpactedProduct))
                    
                    .followApprovalOf(globalDescribe.get(SQX.Response))

                    .followNoteOf(globalDescribe.get(SQX.NonConformance))
                    .followNoteOf(globalDescribe.get(SQX.Defect));
    }
    
    
    /**
    * Returns the configuration that is to be used while upserting.
    * @return returns the configuration that is used while upserting a record
    */
    public static SQX_Upserter.SQX_Upserter_Config getUpserterConfig(){
        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config();
        config.omit(SQX.Finding, SQX_Finding__c.SQX_Nonconformance__c);
          
        return config;
    }
    




    /**
    * @author Sagar Shrestha
    * @date 2014/11/18
    * @description override processChangeSetWithAction without electronicSignatureData only to be used by legacy test
    */
    public static String processChangeSetWithAction(String ncID, String changeset, sObject[] objectsToDelete, Map<String, String> params){
        return processChangeSetWithAction(ncID, changeset, objectsToDelete, params, null);
    }


   /*******************************************REMOTE ACTION SECTION**********************************************************/




    /**
    * @author AR
    * @date 2014/4/23
    * @description Saves a NC and executes next action
    * @apiIntegration is set to false
    */
    @RemoteAction
    public static String processChangeSetWithAction(String ncID, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData)
    {
        return processChangeSetWithAction(ncID, changeset, objectsToDelete, params,  esigData, false);
    }

    public static String processChangeSetWithAction(String ncID, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData, Boolean apiIntegration)
    {

        String  processedNCId = null;
        Boolean isNewRecord = !SQX_Upserter.isValidId(ncID);
        SQX_Nonconformance__c  nc;
        SQX_Extension_NC ncExtension;

        if(isNewRecord){
            nc = new SQX_Nonconformance__c();
        }
        else{
            nc = [SELECT Id, Status__c, SQX_Finding__c, Department__c, SQX_Department__c FROM SQX_Nonconformance__c WHERE Id = : ncID];
        }

        //passing expectation because the expectation will store the persisted object in the transaction.
        //this way we can generate the correct content for audit report or send it for approval.
        SQX_Extension_UI.BaseUpsertExpectation expectation = new SQX_Extension_UI.BaseUpsertExpectation(SQX.NonConformance, ncID);

        ncExtension = new SQX_Extension_NC(new ApexPages.StandardController(nc));
        ncExtension.params = params;
        ncExtension.expectation = expectation;
        return ncExtension.executeTransaction(ncID, changeset, objectsToDelete, params, esigData, getUpserterConfig(), expectation, apiIntegration);
    }


    SQX_Extension_UI.BaseUpsertExpectation expectation = null;
    Map<String, String> params = null;


    public override void processNextAction(Map<String, String> params){
        SQX_Nonconformance__c nonConformance = (SQX_Nonconformance__c) mainRecord;

        if('submit'.equalsIgnoreCase(nextAction)){
            nonConformance.Status__c =  SQX_NC.STATUS_TRIAGE;
            
            if(nonConformance.SQX_Department__c != null){
                SQX_Department__c newOwner= [Select Queue_Id__c, Name from SQX_Department__c where Id=:nonConformance.SQX_Department__c];
                
                if(newOwner != null && newOwner.Queue_Id__c != null){
                    nonConformance.OwnerId = newOwner.Queue_Id__c;
                    nonConformance.Department__c= newOwner.Name; //copy new department values to old department field value. This would make old reports work
                }
            }

            Map<Schema.SObjectType, List<SObject>> allObjects = new Map<Schema.SObjectType, List<SObject>>();
            Map<Schema.SObjectType, List<Schema.SObjectField>> allFields = new Map<Schema.SObjectType, List<Schema.SObjectField>>();
            
            allObjects.put(nonConformance.getSObjectType(), new List<SObject>{nonConformance});
            allFields.put(nonConformance.getSObjectType(), new List<SObjectField>{SQX_NonConformance__c.fields.Status__c, SQX_NonConformance__c.fields.OwnerId});
            
            new SQX_DB().op_update(allObjects, allFields);
        }
        else if('takeownershipandinitiate'.equalsIgnoreCase(nextAction)){
            nonConformance.OwnerId = UserInfo.getUserId();
            nonConformance.Status__c = SQX_NC.STATUS_OPEN;
            
            Map<Schema.SObjectType, List<SObject>> allObjects = new Map<Schema.SObjectType, List<SObject>>();
            Map<Schema.SObjectType, List<Schema.SObjectField>> allFields = new Map<Schema.SObjectType, List<Schema.SObjectField>>();
            
            allObjects.put(nonConformance.getSObjectType(), new List<SObject>{nonConformance});
            allFields.put(nonConformance.getSObjectType(), new List<SObjectField>{SQX_NonConformance__c.fields.OwnerId, SQX_NonConformance__c.fields.Status__c});
            
            new SQX_DB().op_update(allObjects, allFields);
        }
        else if('void'.equalsIgnoreCase(nextAction)|| 'close'.equalsIgnoreCase(nextAction)){
            Id voidNCID = nonConformance.Id;
            SQX_NonConformance__c nc = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_NonConformance__c WHERE Id = : voidNCID]; //just ensuring record access is valid
            
            if(nc.Status__c != SQX_Finding.STATUS_CLOSED){
                nc.Status__c = SQX_Finding.STATUS_CLOSED;
                
                if ('void'.equalsIgnoreCase(nextAction)) 
                    nc.Resolution__c = SQX_Finding.RESOLUTION_VOID;
                
                new SQX_DB().op_update(new List<SObject>{nc}, new List<SObjectField>{SQX_NonConformance__c.fields.Status__c});
            }
            else{
                throw new SQX_ApplicationGenericException('Non Conformance is alread closed');
            }
        }
        else if('reopen'.equalsIgnoreCase(nextAction)){
            Id reopenNCID = nonConformance.Id;
            SQX_NonConformance__c nc = [SELECT Id, SQX_Finding__c, Close_Date__c, Status__c, Age__c FROM SQX_NonConformance__c WHERE Id = : reopenNCID]; //just ensuring record access is valid
            if(nc.Status__c == SQX_NC.STATUS_CLOSED){
                nc.Status__c = SQX_NC.STATUS_COMPLETE;
                nc.Reopen_Date__c = Date.today();
                nc.Close_Date__c = null;
                nc.Age_Before_Reopen__c = nc.Age__c;
                
                new SQX_DB().op_update(new List<SObject>{nc}, new List<SObjectField>{SQX_NonConformance__c.fields.Status__c,
                                                                                        SQX_NonConformance__c.fields.Reopen_Date__c,
                                                                                        SQX_NonConformance__c.fields.Close_Date__c,
                                                                                        SQX_NonConformance__c.fields.Age_Before_Reopen__c});
            }
            else{
                throw new SQX_ApplicationGenericException('Non Conformance is still open');
            }
        }
        else if('approve'.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 
            
            approveRecord(true, responseID, (String)params.get('remark'), originalApproverId); 
        }
        else if('reject'.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 
            
            approveRecord(false, responseID, (String) params.get('remark'), originalApproverId);
        }
        else if('recall'.equalsIgnoreCase(nextAction)){            
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 

            recallRecord(true, responseID, (String) params.get('remark'), originalApproverId);
        }

    }


    /**
    * Returns the configuration necessary for upserting a response.
    */
    public override SQX_Upserter.SQX_Upserter_Config getResponseConfig() {
        return new SQX_Upserter.SQX_Upserter_Config()
                                .omit(SQX.Response, SQX_Finding_Response__c.Status__c)
                                .omit(SQX.Response, SQX_Finding_Response__c.Approval_Status__c)
                                .omit(SQX.Response, SQX_Finding_Response__c.Published_Date__c)
                                .omit(SQX.Investigation, SQX_Investigation__c.Approval_Status__c)
                                .omit(SQX.Investigation, SQX_Investigation__c.Status__c)
                                .omit(SQX.Investigation, SQX_Investigation__c.SQX_Primary_Contact__c)
                                .omit(SQX.Investigation, SQX_Investigation__c.SQX_Account__c)
                                .omit(SQX.Containment, SQX_Containment__c.Status__c);
    }

    /**
    * Returns the the expectation for a response in nc response screen.
    */
    public override SQX_Upserter.SQX_Upserter_Interceptor getResponseExpectation(boolean sendForApproval) {

        SQX_ResponseExpectation expectation = new SQX_ResponseExpectation(SQX.Response);
        final Integer ANY_NUMBER = expectation.ANY_NUMBER;
        
        Map<String, Integer> objectsCount = new Map<String, Integer>();
        objectsCount.put(SQX.Response, 1);
        objectsCount.put(SQX.Containment, ANY_NUMBER);
        objectsCount.put(SQX.Investigation, ANY_NUMBER);
        objectsCount.put(SQX.InvestigationTool , ANY_NUMBER);
        objectsCount.put(SQX.RootCause, ANY_NUMBER);
        objectsCount.put(SQX.ActionPlan, ANY_NUMBER);
        objectsCount.put(SQX.Disposition, ANY_NUMBER);
        objectsCount.put(SQX.ResponseInclusion, ANY_NUMBER);

        SQX_Nonconformance__c nc = [SELECT Id, SQX_Finding__c FROM SQX_Nonconformance__c WHERE Id =: mainRecord.Id];

        expectation.valueChecks.put(SQX.Response, new Map<SObjectField, Id> {
          SQX_Finding_Response__c.SQX_Nonconformance__c => nc.Id
        });


        expectation.sendForApproval = sendForApproval;
        expectation.numberOfObjectsToBeCreated = objectsCount;

        return expectation;
    }




}