/**
* Tests to ensure that all setup datas are inserted correctly 
*/
@IsTest
public class SQX_Test_1984_SetupData_Processor {
    static boolean runAllTests = true,
                run_givenFreshInstall_SetupDatas_Inserted= false;    
    
    @testSetup
    static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'adminUser');
    }
    
    /**
    * This test ensures that all setup datas are inserted
    **/
    public testmethod static void givenFreshInstall_SetupDatas_Inserted() {
        if(!runAllTests && !run_givenFreshInstall_SetupDatas_Inserted){
            return;
        }

        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(adminUser){
            //Act : Fresh Install
            SQX_SetupData_Processor.execute();

            //Assert 1 : Expected 14 picklist values should be inserted
            List<SQX_Picklist_Value__c> pickListValues = [Select Id from SQX_Picklist_Value__c];
            System.assertEquals(14, pickListValues.size(), 'Expected 15 picklist values to be inserted');


            //Assert 2 : Expected 26 defect Codes to be inserted 
            List<SQX_Defect_Code__c> defectCodes = [Select Id from SQX_Defect_Code__c];
            System.assertEquals(26, defectCodes.size(), 'Expected 27  defect codes values to be inserted');

            //Assert 3 : Expected 11 tasks to be inserted
            List<SQX_Task__c> sqxTasks = [Select Id from SQX_Task__c];
            System.assertEquals(11, sqxTasks.size(), 'Expected 11 tasks to be inserted');

            //Assert 4 : Expected 57 task questions to be inserted
            List<SQX_Task_Question__c> taskQuestions = [Select Id from SQX_Task_Question__c];
            System.assertEquals(56, taskQuestions.size(), 'Expected 56 task questions to be inserted');

            //Assert 5 : Expected 106 answer options to be inserted
            List<SQX_Answer_Option__c> answerOptions = [Select Id from SQX_Answer_Option__c];
            System.assertEquals(105, answerOptions.size(), 'Expected 106 answer options to be inserted');

            //Assert 6 : Expected 4 result types to be inserted
            List<SQX_Result_Type__c> resultTypes = [Select Id from SQX_Result_Type__c];
            System.assertEquals(4, resultTypes.size(), 'Expected 4 result types to be inserted');

            //Assert 7 : Expected 4 result types to be inserted
            List<SQX_Result_Type_Value__c> resultTypeValues = [Select Id from SQX_Result_Type_Value__c];
            System.assertEquals(8, resultTypeValues.size(), 'Expected 4 result types to be inserted');

            //Assert 8 : Expected 11 auto numbers to be inserted
            List<SQX_Auto_Number__c> autoNumbers = [SELECT Id FROM SQX_Auto_Number__c];
            System.assertEquals(11, autoNumbers.size(), 'Expected 11 auto number schemes to be inserted');
        }
        

    }
    
    /**
    * This test ensures only specific setup data are inserted
    * @story SQX-2610
    */
    public testmethod static void givenInstallVersionSpecificSetupData_Inserted() {
        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){
            //Act : Fresh Install
            SQX_SetupData_Processor.execute('SetupData_6_4');
            
            List<SQX_Picklist_Value__c> pickListValues = [Select Id from SQX_Picklist_Value__c];
            System.assertEquals(0, pickListValues.size(), 'Expected 0 picklist values to be inserted');
            
            List<SQX_Defect_Code__c> defectCodes = [Select Id from SQX_Defect_Code__c];
            System.assertEquals(0, defectCodes.size(), 'Expected 0  defect codes values to be inserted');
            
            List<SQX_Auto_Number__c> autoNumbers = [SELECT Id FROM SQX_Auto_Number__c];
            System.assertEquals(4, autoNumbers.size(), 'Expected 4 auto numbers to be inserted');
        }
    }
    
    /**
    * This test ensures only specific setup data are inserted
    * @story SQX-5855
    */
    public testmethod static void givenInstallVersion_8_0_SpecificSetupData_Inserted() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){
            //Act : Fresh Install
            SQX_SetupData_Processor.execute('SetupData_8_0');          
            List<SQX_Auto_Number__c> autoNumbers = [SELECT Id FROM SQX_Auto_Number__c];
            //Assert: Ensured that auto numbers added
            System.assertEquals(5, autoNumbers.size(), 'Expected 5 auto numbers to be inserted');
        }
    }

    /**
    * This test ensuers only specific setup data are inserted
    * @story SQX-7561
    */
    public testmethod static void givenInstallVersion_9_0_SpecificSetupData_Inserted() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){
            //Act : Fresh Install
            SQX_SetupData_Processor.execute('SetupData_9_0');          
            List<SQX_Reporting_Default__c> reportingDefaults = [SELECT Id FROM SQX_Reporting_Default__c];
            //Assert: Ensured that reporting default is inserted
            System.assertEquals(1, reportingDefaults.size(), 'Expected 1 reporting default to be inserted');
        }
    }
}