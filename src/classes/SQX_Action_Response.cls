/**
 * This class contains the responses that must be presented after an action is performed
 */
public with sharing class SQX_Action_Response {
    
    @AuraEnabled
    public List<String> errorMessages = new List<String>();
    @AuraEnabled
    public List<String> infoMessages = new List<String>();
    @AuraEnabled
    public List<String> confirmMessages = new List<String>();
    
    /**
     * Adds messages to page, so that existing VF page aren't impacted
     */
    public void addMessages() {
        if(ApexPages.currentPage() != null) {
            for(String errorMsg : errorMessages) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, errorMsg));
            }
            
            for(String infoMessage : infoMessages) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, infoMessage));
            }
            
            for(String confirmMessage : confirmMessages) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, confirmMessage));
            }
        }
       
    }
    
    /**
     * Check if SQX_Action_Response has any message.
     */
    public boolean hasMessages(){
        return (errorMessages.size() > 0 || confirmMessages.size() > 0 || infoMessages.size() > 0);
    }

    /**
     * Merge response of another SQX_Action_Response with the response object that called this method and return merged response.
     */
    public SQX_Action_Response mergeResponse(SQX_Action_Response response) {
        this.errorMessages.addAll(response.errorMessages);
        this.confirmMessages.addAll(response.confirmMessages);
        this.infoMessages.addAll(response.infoMessages);
        return this;
    }
    
}