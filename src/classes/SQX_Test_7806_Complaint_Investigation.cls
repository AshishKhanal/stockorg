/**
 * This test class ensures the functionality to create, link and detach of Investigation from Complaint works as expected.
 */
@isTest
public class SQX_Test_7806_Complaint_Investigation {
    static final Id complaintInvestigationRecordTypeId = Schema.SObjectType.SQX_Investigation__c.getRecordTypeInfosByName().get('Complaint Investigation').getRecordTypeId();
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        System.runAs(adminUser){
            List<SQX_Defect_Code__c> defects = new List<SQX_Defect_Code__c>();
            SQX_Defect_Code__c complaintConclusion = new SQX_Defect_Code__c(Name = 'Defect1', 
                                                         Active__c = true,
                                                         Defect_Category__c = 'Test_Category_Complaint_Conclusion',
                                                         Description__c = 'Test Description Complaint Conclusion',
                                                         Type__c = 'Complaint Conclusion');
            
            SQX_Defect_Code__c complaintConclusion1 = new SQX_Defect_Code__c(Name = 'Defect2', 
                                                         Active__c = true,
                                                         Defect_Category__c = 'Test_Category_Complaint_Code',
                                                         Description__c = 'Test Description Complaint Code',
                                                         Type__c = 'Complaint Code');
            defects.add(complaintConclusion);
            defects.add(complaintConclusion1);
            insert defects;
            SQX_Test_Part.insertPart(null, adminUser, true, '');
        }   
    }
    
    public List<SQX_Defect_Code__c> getInvestigations() {
        return [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
    }
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is neither Open nor Void
     * When : Create Investigation
     * Then : Investigation References Complaint as Source of Investigation and Adds Complaint/Investigation to Linked Investigation.
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation
     */
    public testMethod static void givenDraftComplaintWithoutAI_WhenInevstigationIsCreated_LinkedInvestigationIsCreatedSuccessfully() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.save();
            
            // Act : Create Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation;
            
            // Assert : Ensure Linked Investigation is created
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation to be created');
            System.assert(insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == null, 'Expected Primary Investigation in Complaint to be null but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Void Investigation
            investigation.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation;
            
            // Assert : Ensure linked investigation is deleted
            List<SQX_Linked_Investigation__c> linkedInvestigations = [SELECT Id FROM SQX_Linked_Investigation__c WHERE SQX_Investigation__c = :investigation.Id];
            System.assert(linkedInvestigations.size() == 0, 'Expected linked investigation to be deleted as investigation is voided but is not deleted.');
        }
    }
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is Open and No Primary Investigation is identified for the complaint and Scope matches
     * When : Create Investigation
     * Then : Complaint References  Investigation as Primary. Adds Complaint/Investigation to Linked Investigation and shows it as "Primary"
     * 
     * When : Investigation is close
     * Then : Updates Complaint Diagnostic Code (Complaint Conclusion or Complaint Code ? To ask with AR) with Investigation Diagnostic Code
     * 
     * When : Investigation is void
     * Then : Removes Clears reference from Complaint to Primary Investigation. Removes link from Complaint/Investigation
     */
    public testMethod static void givenOpenComplaintWithoutAI_WhenCreateLinkedInvestigationAndScopeMatches_ComplaintIsUpdatedWithPrimaryInvestigation() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create open complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            // Act : Create Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation;
                        
            // Assert : Ensure created Linked Investigation is primary and Its Complaint has Primary Investigation
            SQX_Investigation__c inv = [SELECT Id, SQX_Complaint__c, SQX_Part__c, SQX_Part_Family__c, SQX_Defect_Code__c FROM SQX_Investigation__c WHERE Id = :investigation.Id];
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation to be created');
            System.assert(insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == investigation.Id, 'Expected Primary Investigation in Complaint to be ' + investigation.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Void Investigation
            investigation.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation;
            
            // Assert : Ensure linked investigation is deleted
            insertedLinkedInvestigations = [SELECT Id FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id];
            System.assert(insertedLinkedInvestigations.size() == 0, 'Expected linked investigation to be deleted as primary investigation is voided.');

            // Assert : Ensure Primary investigation is cleared from Complaint
            SQX_Complaint__c com = [SELECT Id, SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            System.assert(com.SQX_Primary_Investigation__c == null, 'Expected primary investigation to be cleared from Complaint but is ' + com.SQX_Primary_Investigation__c);
        }
    }
    
     /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is Open and Primary Investigation is identified for the complaint
     * When : Investigation is close
     * Then : Updates Complaint Diagnostic Code (Complaint Conclusion or Complaint Code ? To ask with AR) with Investigation Diagnostic Code
     */
    public testMethod static void givenComplaintWithoutAndWithPrimaryInvestigation_WhenInvestigationIsClosed_ComplaintConclustionCodeIsUpdatedWithDefectCodeOfInvestigation() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create open complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            // Act : Create Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation;
            
            // Assert : Ensure created Linked Investigation is primary and Its Complaint has Primary Investigation
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation to be created');
            System.assert(insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == investigation.Id, 'Expected Primary Investigation in Complaint to be ' + investigation.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Close Investigation
            investigation.Status__c = SQX_Investigation.STATUS_PUBLISHED;
            investigation.investigation_Summary__c = 'Closing Investigation';
            update investigation;
            
            // Assert : Ensure Complaint's Conclusion Code (Diagnostic Code) should be equal to Primary Diagnostic Code of Primary Investigation.
            SQX_Complaint__c com = [SELECT Id, SQX_Primary_Investigation__r.SQX_Primary_Diagnostic__c, SQX_Conclusion_Code__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            System.assert(com.SQX_Conclusion_Code__c == com.SQX_Primary_Investigation__r.SQX_Primary_Diagnostic__c, 'Expected Conclusion Code of Complaint to be match with Defect Code its Primary Investigation, but they are found different.');
        }
    }
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is Open and Primary Investigation is already identified for the complaint
     * When : Create Investigation
     * Then : Investigation References Complaint as Source of Investigation and Adds Complaint/Investigation to Linked Investigation
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation
     */
    public testMethod static void givenOpenComplaintWithoutAIHavingPrimaryInvestigation_WhenCreatingNewInvestigationAndScopeMatches_NoChangeInPrimaryInvestigationForAIAndComplaint() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create open complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            // Act : Create two Investigations, both match scope and first will create Primary LI 
            SQX_Investigation__c investigation1 = new SQX_Investigation__c();
            investigation1.SQX_Complaint__c = complaint.complaint.Id;
            investigation1.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation1.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation1;
            
            SQX_Investigation__c investigation2 = new SQX_Investigation__c();
            investigation2.SQX_Complaint__c = complaint.complaint.Id;
            investigation2.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation2.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation2;
            
            // Assert : Ensure firstly created Linked Investigation is primary and secondly created LI is not primary as complaint has already primary investigation.
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND Is_Primary__c = true];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one primary linked investigation');
            Boolean p = insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c != investigation2.Id && insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == investigation1.Id;
            System.assert(p , 'Expected Primary Investigation in Complaint to be ' + investigation1.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Void second Investigation i.e. investigation2 (which is not primary)
            investigation2.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation2;
            
            // Assert : Only linked investigation is deleted and no impact in the primary investigation of Complaint
            insertedLinkedInvestigations = [SELECT Id,Is_Primary__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation as another has been voided but got ' + insertedLinkedInvestigations.size());
            System.assert(insertedLinkedInvestigations[0].Is_Primary__c == true, 'Expected primary Linked Investigation but found non primary');
            SQX_Complaint__c com = [SELECT Id, SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            System.assert(com.SQX_Primary_Investigation__c != null, 'Expected primary investigation no to be cleared from Complaint but is being cleared');
            
            // detach primary investigation
            delete insertedLinkedInvestigations;
            SQX_Complaint__c complaint1 = [SELECT Id, SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id =:complaint.complaint.Id];
            System.assert(complaint1.SQX_Primary_Investigation__c == null, 'Expected Primary Investigation to be cleared from Complaint as a result of detach but is ' + complaint1.SQX_Primary_Investigation__c);
        }
    }
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is Open and No Primary Investigation is identified for the complaint and Scope matches
     * When : Link Investigation
     * Then : Complaint References  Investigation as Primary. Adds Complaint/Investigation to Linked Investigation and shows it as "Primary" if scope matches
     * 
     * When : Investigation is close
     * Then : Updates Complaint Diagnostic Code with Investigation Diagnostic Code
     * 
     * When : Investigation is void
     * Then : Clears reference from Complaint to Primary Investigation. Removes link from Complaint/Investigation
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is Open and Primary Investigation is already identified for the complaint
     * When : Link Investigation
     * Then : Adds Complaint/Investigation to Linked Investigation
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    /**
     * CASE Complaint Without Associated Item (AI)
     * Given : Complaint is neither Open nor Void
     * When : Detach Investigation
     * Then : Complaint Reference  to primary Investigation is cleared. Complaint/Investigation from Linked Investigation is removed
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    ///////// WITH AI
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is neither Open nor Void
     * When : Create Investigation
     * Then : Investigation References Complaint as Source of Investigation and Adds Complaint, AI, Investigation to Linked Investigation
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation
     */
    public testMethod static void givenDraftComplaintWithAI_WhenInevstigationIsCreated_LinkedInvestigationIsCreatedSuccessfully() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.save();
            
            // ARRANGE : Create new associated item and is primary
            SQX_Complaint_Associated_Item__c newAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id, Make_Primary__c = true);
            insert newAssociatedItem;
            
            // Act : Create Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.SQX_Associated_Item__c = newAssociatedItem.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation;
           
            // Assert : Ensure Linked Investigation is created
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id AND SQX_Associated_Item__c = :newAssociatedItem.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation to be created');
            System.assert(insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == null, 'Expected Primary Investigation in Complaint to be null but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Void Investigation
            investigation.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation;
            
            // Assert : Ensure linked investigation is deleted
            List<SQX_Linked_Investigation__c> linkedInvestigations = [SELECT Id FROM SQX_Linked_Investigation__c WHERE SQX_Investigation__c = :investigation.Id];
            System.assert(linkedInvestigations.size() == 0, 'Expected linked investigation to be deleted as investigation is voided but is not deleted.');
        }
    }
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and No Primary Investigation is identified for the associated Item and Scope matches AI
     * When : Create Investigation
     * Then : Investigation References Complaint as Source, AI References  Investigation as Primary. Adds Complaint/AI/Investigation to Linked Investigation. If AI is primary then updates Complaint Primary Investigation
     * 
     * When : Investigation is close
     * Then : Updates Complaint Diagnostic Code with Investigation Diagnostic Code if Investigation is Primary Investigation for the complaint
     * 
     * When : Investigation is void
     * Then : Clears reference from AI. Removes link from Complaint/Investigation/AI. If AI is primary Complaint Primary Investigation is cleared
     */
    public testMethod static void givenOpenComplaintWithAI_WhenCreateLinkedInvestigationAndScopeMatches_AIAndComplaintIsUpdatedWithPrimaryInvestigation() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create open complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            // ARRANGE : New associated item and is primary
            SQX_Complaint_Associated_Item__c newAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id, Make_Primary__c = true);
            insert newAssociatedItem;
            
            // Act : Create Investigation
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.SQX_Associated_Item__c = newAssociatedItem.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            Test.startTest();
            insert investigation;
            
            // Assert : Ensure created Linked Investigation is primary and Its Associated Item has Primary Investigation, also since this AI is primary to Complaint so Complaint should also have Primary Investigation
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c, SQX_Associated_Item__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND SQX_Investigation__c = :investigation.Id];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one linked investigation to be created');
            System.assert(insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == investigation.Id, 'Expected Primary Investigation in Complaint to be ' + investigation.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            System.assert(insertedLinkedInvestigations[0].SQX_Associated_Item__r.SQX_Primary_Investigation__c == investigation.Id, 'Expected Primary Investigation in Associated Item to be ' + investigation.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Associated_Item__r.SQX_Primary_Investigation__c);
            
            // Act : Void Investigation
            investigation.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation;
            Test.stopTest();
            
            // Assert : Ensure Primary investigation is cleared from AI and since this AI is primary to Complaint so Primary Investigation is also cleared from Complaint.
            SQX_Complaint__c com = [SELECT Id, SQX_Primary_Investigation__c, SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            System.assert(com.SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c == null, 'Expected primary investigation to be cleared from Associated Item but is ' + com.SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c);
            System.assert(com.SQX_Primary_Investigation__c == null, 'Expected primary investigation to be cleared from Complaint but is ' + com.SQX_Primary_Investigation__c);
        }
    }
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and Primary Investigation is already identified for the AI
     * When : Create Investigation
     * Then : Investigation References Complaint as Source of Investigation. Adds Complaint/Investigation to Linked Investigation
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/Investigation/AI
     */
    public testMethod static void givenOpenComplaintWithAIAndHavingPrimaryInvestigation_WhenCreatingNewInvestigationAndScopeMatches_NoChangeInPrimaryInvestigationForAIAndComplaint() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c ORDER BY Name];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
            // ARRANGE : Create open complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[1].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            // ARRANGE : New associated item and is primary
            SQX_Complaint_Associated_Item__c newAssociatedItem = new SQX_Complaint_Associated_Item__c(SQX_Complaint__c = complaint.complaint.Id, SQX_Part_Family__c = part.Part_Family__c, SQX_Part__c = part.Id, Make_Primary__c = true);
            insert newAssociatedItem;
            
            // Act : Create two Investigations both matche scope and first will create primary LI
            SQX_Investigation__c investigation1 = new SQX_Investigation__c();
            investigation1.SQX_Complaint__c = complaint.complaint.Id;
            investigation1.SQX_Associated_Item__c = newAssociatedItem.Id;
            investigation1.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation1.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation1;
            
            SQX_Investigation__c investigation2 = new SQX_Investigation__c();
            investigation2.SQX_Complaint__c = complaint.complaint.Id;
            investigation2.SQX_Associated_Item__c = newAssociatedItem.Id;
            investigation2.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation2.RecordTypeId = complaintInvestigationRecordTypeId;
            insert investigation2;
            
            // Assert : Ensure created Linked Investigation is not primary as complaint has already primary investigation
            List<SQX_Linked_Investigation__c> insertedLinkedInvestigations = [SELECT Id, SQX_Complaint__r.SQX_Primary_Investigation__c, SQX_Associated_Item__r.SQX_Primary_Investigation__c FROM SQX_Linked_Investigation__c WHERE SQX_Complaint__c = :complaint.complaint.Id AND Is_Primary__c = true];
            System.assert(insertedLinkedInvestigations.size() == 1, 'Expected one primary linked investigation');
            Boolean p = insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c != investigation2.Id && insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c == investigation1.Id;
            Boolean p_withai = insertedLinkedInvestigations[0].SQX_Associated_Item__r.SQX_Primary_Investigation__c != investigation2.Id && insertedLinkedInvestigations[0].SQX_Associated_Item__r.SQX_Primary_Investigation__c == investigation1.Id;
            System.assert(p, 'Expected Primary Investigation in Complaint to be ' + investigation2.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            System.assert(p_withai, 'Expected Primary Investigation in Associated Item to be ' + investigation2.Id +' but is ' + insertedLinkedInvestigations[0].SQX_Complaint__r.SQX_Primary_Investigation__c);
            
            // Act : Void second Investigation i.e. investigation1 (which is not primary)
            investigation2.Status__c = SQX_Investigation.STATUS_VOID;
            update investigation2;
            
            // Assert : Only linked investigation is deleted and no impact in the primary investigation of Complaint and Associated Item
            SQX_Complaint__c com = [SELECT Id, SQX_Primary_Investigation__c, SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];
            System.assert(com.SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c != null, 'Expected primary investigation no to be cleared from Associated Item but is being cleared');
            System.assert(com.SQX_Primary_Investigation__c != null, 'Expected primary investigation no to be cleared from Complaint but is being cleared');
            Test.startTest();
            // detach primary investigation 
            delete insertedLinkedInvestigations;
            
            // Assert : Primary Investigatin should be cleared in both Associated Item and Complaint
            SQX_Complaint__c complaint1 = [SELECT Id, SQX_Primary_Investigation__c, SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE Id =:complaint.complaint.Id];
            System.assert(complaint1.SQX_Primary_Investigation__c == null, 'Expected Primary Investigation to be cleared from Complaint as a result of detach but is ' + complaint1.SQX_Primary_Investigation__c);
            System.assert(complaint1.SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c == null, 'Expected Primary Investigation to be cleared from Associated Item as a result of detach but is ' + complaint1.SQX_Primary_Associated_Item__r.SQX_Primary_Investigation__c);
            Test.stopTest();
        }

    }

    /**
     * Given: Open Complaint record with investigation.
     * When: Part or Part Family or Defect code is updated.
     * Then: Error msg is thrown.
     */
    public testMethod static void givenOpenComplainWithInvestigation_WhenPartOrDefectCodeIsUpdated_ThenErrorMsgIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser) {
            
            // Get the defect and Part
            List<SQX_Defect_Code__c> defects = [SELECT Id FROM SQX_Defect_Code__c where Type__c = 'Complaint Code' and active__c = true];
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c LIMIT 1];
            
                        
            // ARRANGE : Create complaint with investigatio 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            complaint.complaint.SQX_Complaint_Code__c = defects[0].Id;
            complaint.complaint.SQX_Part__c = part.Id;
            complaint.complaint.SQX_Part_Family__c = part.Part_Family__c;
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            complaint.save();
            
            SQX_Investigation__c investigation = new SQX_Investigation__c();
            investigation.SQX_Complaint__c = complaint.complaint.Id;
            investigation.Type_of_Investigation__c = SQX_Investigation.TYPE_OF_INVESTIGATION_RETAINED_SAMPLE;
            investigation.RecordTypeId = complaintInvestigationRecordTypeId;
            investigation.SQX_Defect_Code__c = defects[0].Id;
            investigation.SQX_Part__c = part.id;
            investigation.SQX_Part_Family__c = part.Part_Family__c;
            insert investigation;
            
            //Act: Update part or partfamily or defect code
            investigation.SQX_Defect_Code__c = null;
            investigation.SQX_Part__c = null;
            investigation.SQX_Part_Family__c = null;
            
            SQX_Investigation__c investigationRecord = [Select id, Name, SQX_Complaint__r.Name from SQX_Investigation__c where id = :investigation.Id];
            
            Database.SaveResult result = Database.update(investigation, false);
           	System.assertEquals(false, result.isSuccess(), 'Expected investigation update to fail as part/part-family or defect code cannot be updated');
            System.assertEquals(Label.SQX_ERR_MSG_Prevent_Update_Of_Primary_Investigation.replace('{investigationName}', investigationRecord.Name).replace('{complaintName}', investigationRecord.SQX_Complaint__r.Name), result.getErrors()[0].getMessage(), 'Expected to get error msg saying primary investigation with update in part or defect code is not allowed.');
            
        }
    }
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and No Primary Investigation is identified for the associated Item and Scope matches AI
     * When : Link Investigation
     * Then : AI References Investigation as Primary. Adds Complaint/AI/Investigation to Linked Investigation. If AI is primary then updates Complaint Primary Investigation
     * 
     * When : Investigation is close
     * Then : Updates Complaint Diagnostic Code with Inspection Diagnostic Code if Investigation is Primary Investigation
     * 
     * When : Investigation is void
     * Then : Clears reference from AI to Investigation. Clears reference to Primary Investigation from Complaint if it was previously set.  Removes link from Complaint/Investigation
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and Primary Investigation is already identified for the complaint
     * When : Link Investigation
     * Then : Adds Complaint/AI/Investigation to Linked Investigation
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : Removes link from Complaint/AI/Investigation
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and Selected AI/Investigation is primary
     * When : Detach Investigation
     * Then : Complaint Reference  to primary Investigation is cleared. Complaint/Investigation from Linked
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : No Impact
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
    /**
     * CASE Complaint With Associated Item (AI)
     * Given : Complaint is Open and Selected AI/Investigation is not primary
     * When : Detach Investigation
     * Then : Complaint AI Investigation is cleared. Complaint/AI/Investigation from Linked Investigation is removed
     * 
     * When : Investigation is close
     * Then : No Impact
     * 
     * When : Investigation is void
     * Then : No Impact
     * 
     * Note : These test cases have already  been covered in above tests. It just to keep the test scenarios
     */
    
}