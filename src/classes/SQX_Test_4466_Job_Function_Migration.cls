/**
 * test class for validating Job Function active state after migration 
 */
@IsTest
public class SQX_Test_4466_Job_Function_Migration {
    @testSetup
    public static void commonSetup() {

        // add required users
        User admin = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin');
        SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);
        
        SQX_Job_Function__c jf1, jf2, jf3;
        SQX_Personnel__c p1, p2, p3;
        
        System.runAs(admin) {
            // add required job functions
            jf1 = new SQX_Job_Function__c( Name = 'JF1', Batch_Job_Status__c = null);
            jf2 = new SQX_Job_Function__c( Name = 'JF2');
            jf3 = new SQX_Job_Function__c( Name = 'JF3', Active__c = true);
            insert new List<SQX_Job_Function__c> { jf1, jf2, jf3};
    
            // add required personnels
            p1 = new SQX_Test_Personnel().mainRecord;
            p2 = new SQX_Test_Personnel().mainRecord;
            p3 = new SQX_Test_Personnel().mainRecord;

            p1.Full_Name__c = 'P1';
            p2.Full_Name__c = 'P2';
            p3.Full_Name__c = 'P3';

            p1.Active__c = true;
            p2.Active__c = true;
            p3.Active__c = true;

            insert new List<SQX_Personnel__c> { p1, p2, p3 };
            
            // add required active personnel job functions
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = false));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf1.Id, Active__c = true));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p3.Id, SQX_Job_Function__c = jf1.Id, Active__c = false, Activation_Date__c = System.today(), Deactivation_Date__c = System.today()));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf2.Id, Active__c = false));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p3.Id, SQX_Job_Function__c = jf3.Id, Active__c = false));
            insert pjfs;
            
        }
    }

    /*
    * This returns Map of Job Function 
    */
    static Map<String, SQX_Job_Function__c> getJobFunctions() {
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name, Batch_Job_Status__c FROM SQX_Job_Function__c];
        
        Map<String, SQX_Job_Function__c> jfMap = new Map<String, SQX_Job_Function__c>();
        for (SQX_Job_Function__c jf : jfs) {
            jfMap.put(jf.Name, jf);
        }
        
        return jfMap;
    }
    
    /*
    * This returns Map of Personnels
    */
    static Map<String, SQX_Personnel__c> getPersonnels() {
        List<SQX_Personnel__c> psns = [SELECT Id, Name, Full_Name__c, Identification_Number__c, SQX_User__c, Active__c FROM SQX_Personnel__c];
        
        Map<String, SQX_Personnel__c> psnMap = new Map<String, SQX_Personnel__c>();
        for (SQX_Personnel__c psn : psns) {
            psnMap.put(psn.Full_Name__c, psn);
        }
        
        return psnMap;
    }

    /*
    * This returns Map of Personnel Job Functions
    */
    static Map<String, SQX_Personnel_Job_Function__c> getPersonnelJobFunctions() {
        List<SQX_Personnel_Job_Function__c> pjfs = [SELECT Id, SQX_Personnel__c, SQX_Job_Function__c, Active__c, Activation_Date__c, Deactivation_Date__c FROM SQX_Personnel_Job_Function__c];
        
        Map<String, SQX_Personnel_Job_Function__c> pjfMap = new Map<String, SQX_Personnel_Job_Function__c>();
        
        for (SQX_Personnel_Job_Function__c pjf : pjfs) {
            String psnJFId = '' + pjf.SQX_Personnel__c + pjf.SQX_Job_Function__c;
            pjfMap.put(psnJFId, pjf);
        }
        
        return pjfMap;
    }

    /**
     * GIVEN : Job Function in various State and Does Not Require Migration in different state.
     *          1) JF1 => Active = false, Batch_Job_Status__c = null (Old Job Function that requires migration)
     *          2) JF2 => Active = false, Batch_Job_Status__c = 'Created 7.4 Onwards' (Inactive New Job Function that does not require migration)
     *          2) JF2 => Active = true, Batch_Job_Status__c = 'Created 7.4 Onwards' (Active New Job Function that does not require migration)
     * WHEN : Job Function is migrated
     * THEN : JF1 is migrated and Requirements and PJFs (Child records) are not affected by it.
     *      : JF2, JF3 is not migrated and child values are not affected
     * @story : SQX-4466
     */
    public testmethod static void givenJobFunctionInInactiveState_whenJobFunctionIsMigrated_thenAllJobFunctionsAreActivated(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User admin = users.get('admin');

        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');
        SQX_Job_Function__c jf2 = jfs.get('JF2');
        SQX_Job_Function__c jf3 = jfs.get('JF3');
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');
        SQX_Personnel__c    p2 = psns.get('P2');
        SQX_Personnel__c    p3 = psns.get('P3');

        Map<String, SQX_Personnel_Job_Function__c> pjfs = getPersonnelJobFunctions();
        SQX_Personnel_Job_Function__c    pjf1 = pjfs.get('' + p1.Id + jf1.Id);
        SQX_Personnel_Job_Function__c    pjf2 = pjfs.get('' + p2.Id + jf1.Id);
        SQX_Personnel_Job_Function__c    pjf3 = pjfs.get('' + p3.Id + jf1.Id);
        SQX_Personnel_Job_Function__c    pjf4 = pjfs.get('' + p2.Id + jf2.Id);
        SQX_Personnel_Job_Function__c    pjf5 = pjfs.get('' + p3.Id + jf3.Id);

        System.runas(admin) {
            
            //Arrange1: Create Control Documents
            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document document2 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Controlled_Document document3 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            //Act1: Requirement is added to Controlled Document 
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 2,
                                                             Active__c = false);

            SQX_Requirement__c req2 = new SQX_Requirement__c(SQX_Controlled_Document__c = document2.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = true);

            SQX_Requirement__c req3 = new SQX_Requirement__c(SQX_Controlled_Document__c = document3.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = false);

            SQX_Requirement__c req4 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf2.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = false);

            SQX_Requirement__c req5 = new SQX_Requirement__c(SQX_Controlled_Document__c = document2.doc.Id, 
                                                             SQX_Job_Function__c = jf3.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 2, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 1,
                                                             Days_in_Advance_To_Start_Refresher__c = 1,
                                                             Training_Program_Step__c = 1,
                                                             Active__c = false);

            new SQX_DB().op_insert(new List<SObject>{req1, req2, req3, req4, req5}, new List<SObjectField>{});

            // set expected state of requirement and Personnel Job Functions of JF1
            Map<Id, Boolean> expectedPJFState = new Map<Id,Boolean>();
            Map<Id, Boolean> expectedRequirementState = new Map<Id,Boolean>(); 
            
            for(SQX_Personnel_Job_Function__c pjf : [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf1.Id]){
                expectedPJFState.put(pjf.Id,pjf.Active__c);
            }   
            for(SQX_Requirement__c req : [SELECT Id, Active__c FROM SQX_Requirement__c WHERE SQX_Job_Function__c =: jf1.Id]){
                expectedRequirementState.put(req.Id,req.Active__c);
            }

            //Act: Run SQX_7_4_JobFunctionActivationMigration batch class
            Test.startTest();
            SQX_7_4_JobFunctionActivationMigration job = new SQX_7_4_JobFunctionActivationMigration(new List<String>()); // empty statuses passed for code coverage
            ID batchprocessid = Database.executeBatch(job);
            Test.stopTest();

            //Assert: Job function(JF1) is activated
            //        Activation date is set 
            //        Does Not Require Migration field of job function is set
            jf1 = [SELECT Id, 
                          Active__c, 
                          Activation_Date__c, 
                          Deactivation_Date__c, 
                          Batch_Job_Status__c 
                    FROM SQX_Job_Function__c WHERE Id =: jf1.Id];
            System.assertEquals( true, jf1.Active__c, 'Expected Job Function to be Activated after Migration');
            System.assertNotEquals( null, jf1.Activation_Date__c, 'Expected Activation_Date__c to be set');
            System.assertEquals( SQX_Job_Function.BATCH_JOB_STATUS_ACTIVATION_MIGRATION_COMPLETED, jf1.Batch_Job_Status__c, 'Expected Batch Job Status to be 7.4 Migration Completed');
            
            //Assert: JF2 and JF3 are not affected by migration
            jf2 = [SELECT Id, 
                          Active__c, 
                          Activation_Date__c, 
                          Deactivation_Date__c, 
                          Batch_Job_Status__c 
                    FROM SQX_Job_Function__c WHERE Id =: jf2.Id];
            System.assertEquals( false, jf2.Active__c, 'Expected Job Function created 7.4 onwards not to be affected by migration');
            System.assertEquals( SQX_Job_Function.BATCH_JOB_STATUS_DEFAULT, jf2.Batch_Job_Status__c, 'Expected Batch Job Status to be Created 7.4 Onwards');

            jf3 = [SELECT Id, 
                          Active__c, 
                          Activation_Date__c, 
                          Deactivation_Date__c, 
                          Batch_Job_Status__c 
                    FROM SQX_Job_Function__c WHERE Id =: jf3.Id];
            System.assertEquals( true, jf3.Active__c, 'Expected Job Function created 7.4 onwards not to be affected by migration');
            System.assertEquals( SQX_Job_Function.BATCH_JOB_STATUS_DEFAULT, jf3.Batch_Job_Status__c, 'Expected Batch Job Status to be Created 7.4 Onwards');

            //Assert: Migration should not affect JF1's Requirement's state
            for(SQX_Requirement__c req : [SELECT Id, Active__c FROM SQX_Requirement__c WHERE SQX_Job_Function__c =: jf1.Id]){
                System.assertEquals(expectedRequirementState.get(req.Id), req.Active__c);
            }
            
            //Assert: Migration should not affect JF1's Personnel Job Function's state
            for(SQX_Personnel_Job_Function__c pjf : [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c WHERE SQX_Job_Function__c =: jf1.Id]){
                System.assertEquals(expectedPJFState.get(pjf.Id), pjf.Active__c);
            }
            
        }
    }
}