/**
* @author Pradhanta Bhandari
* @date 2014/2/5
* @description contains test helper method to insert new part 
*/
@IsTest
public class SQX_Test_Part{
    private static SQX_Part_Family__c partFamily;
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/4
    * @description setup the part family, which is required for the test
    */
    private static SQX_Part_Family__c setupPartFamily(){
        if(partFamily == null){
            integer randomNumber = (Integer)(Math.random() * 1000000);
            partFamily = new SQX_Part_Family__c();
            partFamily.Name = 'dummyFamily-' + randomNumber;
            
            insert partFamily;
        }
        
        return partFamily;
    }

    /**
     *  Returns a new part object setup with a random part family set
     */
    private static SQX_Part__c getNewPart() {

        SQX_Part__c newPart;

        Integer randomNumber = (Integer)(Math.random() * 1000000);

        SQX_Part_Family__c family = setupPartFamily();

        newPart = new SQX_Part__c();
        newPart.Name = 'part' + randomNumber;
        newPart.Part_Number__c = 'PR-' + randomNumber;
        newPart.Part_Risk_Level__c = 3;
        newPart.Active__c = true;
        newPart.Part_Family__c = family.Id;

        return newPart;
    }


    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/4
    * @description this is a common reusable method to insert a part as a certain user
    * @param part the part  to insert, pass null to create a new part, the part family associated with the part is got from setup
    * @param user the user whose context the test is to run
    * @param shouldSucceed true/false based on whether the insert is expected to succeed or fail
    * @param expectationMessage error message to display when expectation/assertion fails
    */
    public static SQX_Part__c insertPart(SQX_Part__c part, User user,  boolean shouldSucceed, string expectationMessage){
        SQX_Part__c  newPart = null;
        
        System.runAs(user){
            if(newPart == null){
                newPart = getNewPart();
            }
            else{
                newPart = part;
            }
            
            Database.SaveResult result = Database.Insert(newPart, false);
            
            System.assert(result.isSuccess() == shouldSucceed, expectationMessage);
            
            
        }
        
        return newPart;
    }

    /**
     *  Method inserts a set of new parts
     *  @param numberOfParts number of parts to be inserted
     */
    public static List<SQX_Part__c> insertParts(Integer numberOfParts) {

        List<SQX_Part__c> newParts = new List<SQX_Part__c>();

        for(Integer i=0; i < numberOfParts; i++) {
            newParts.add(getNewPart());
        }

        insert newParts;

        return newParts;
    }
}