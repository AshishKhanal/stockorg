/**
 * This test class is to check if follow up medwatch has F7 and G7 field set
 */
@isTest
public class SQX_Test_8443_Update_Medwatch {
    
    @testSetup
    public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * GIVEN : Reporting Default with Reporting Site User Facility/Importer Site and Manufacturer Site.
     * WHEN : Follow up Medwatch is created.
     * THEN : F7 and G7 fields are set to followup.
     */
    public static testmethod void givenReportingDefault_WhenFollowUpMedwatchIsCreated_ThenF7AndG7FieldIsSetToFollowUp() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        SQX_Reporting_Default__c reportingDefaultImporter, reportingDefaultManufacturer;

        System.runAs(adminUser) {
            List<SQX_Reporting_Default__c> reportingDefaultList = new List<SQX_Reporting_Default__c>();

            // ARRANGE : Create Reporting Default with reporting site importer
            reportingDefaultImporter = new SQX_Reporting_Default__c(
                Reporting_Site__c = 'User Facility/Importer Site'
            );
            reportingDefaultList.add(reportingDefaultImporter);

            // ARRANGE : Create Reporting Default with reporting site manufacturer
            reportingDefaultManufacturer = new SQX_Reporting_Default__c(
                Reporting_Site__c = 'Manufacturing Site'
            );
            reportingDefaultList.add(reportingDefaultManufacturer);

            new SQX_DB().op_insert(reportingDefaultList, new List<SObjectField> {});

        }
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();

            List<SQX_Regulatory_Report__c> regulatoryReportList = new List<SQX_Regulatory_Report__c>();
            List<SQX_Regulatory_Report__c> regulatoryReportListToUpdate = new List<SQX_Regulatory_Report__c>();
            List<SQX_Medwatch__c> medwatchList = new List<SQX_Medwatch__c>();
            List<SQX_Submission_History__c> submissionHistoryList = new List<SQX_Submission_History__c>();

            reportingDefaultImporter = [SELECT Id FROM SQX_Reporting_Default__c WHERE Reporting_Site__c = 'User Facility/Importer Site'];
            reportingDefaultManufacturer = [SELECT Id FROM SQX_Reporting_Default__c WHERE Reporting_Site__c = 'Manufacturing Site'];

            // ARRANGE : Create Regulatory Reports
            SQX_Regulatory_Report__c regulatoryReportImporter = new SQX_Regulatory_Report__c(
                SQX_User__c = standardUser.Id,
                SQX_Complaint__c = complaint.complaint.Id,
                SQX_Reporting_Default__c = reportingDefaultImporter.Id,
                Due_Date__c = Date.today(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                Status__c=SQX_Regulatory_Report.STATUS_PENDING
            );
            regulatoryReportList.add(regulatoryReportImporter);

            SQX_Regulatory_Report__c regulatoryReportManufacturer = new SQX_Regulatory_Report__c(
                SQX_User__c = standardUser.Id,
                SQX_Complaint__c = complaint.complaint.Id,
                SQX_Reporting_Default__c = reportingDefaultManufacturer.Id,
                Due_Date__c = Date.today(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                Status__c=SQX_Regulatory_Report.STATUS_PENDING
            );
            regulatoryReportList.add(regulatoryReportManufacturer);

            new SQX_DB().op_insert(regulatoryReportList, new List<SObjectField> {});

            // ARRANGE : Create Medwatch records
            SQX_Medwatch__c medwatchImporter = new SQX_Medwatch__c(
                SQX_Regulatory_Report__c = regulatoryReportImporter.Id
            );
            medwatchList.add(medwatchImporter);
            
            SQX_Medwatch__c medwatchManufacturer = new SQX_Medwatch__c(
                SQX_Regulatory_Report__c = regulatoryReportManufacturer.Id
            );
            medwatchList.add(medwatchManufacturer);

            new SQX_DB().op_insert(medwatchList, new List<SObjectField> {});

            // ARRANGE : Update report id in Regulatory Report
            regulatoryReportImporter.Report_Id__c = medwatchImporter.Id;
            regulatoryReportManufacturer.Report_Id__c = medwatchManufacturer.Id;
            regulatoryReportListToUpdate.add(regulatoryReportImporter);
            regulatoryReportListToUpdate.add(regulatoryReportManufacturer);

            new SQX_DB().op_update(regulatoryReportListToUpdate, new List<SObjectField> {});

            // ARRANGE : Create Report Submission records
            SQX_Submission_History__c submissionHistoryImporter = new SQX_Submission_History__c(
                Status__c='Complete',
                SQX_Complaint__c=complaint.complaint.Id,
                Submitted_By__c='Submitter',
                SQX_Regulatory_Report__c = regulatoryReportImporter.Id,
                SQX_Medwatch__c = medwatchImporter.Id
            );
            submissionHistoryList.add(submissionHistoryImporter);

            SQX_Submission_History__c submissionHistoryManufacturer = new SQX_Submission_History__c(
                Status__c='Complete',
                SQX_Complaint__c=complaint.complaint.Id,
                Submitted_By__c='Submitter',
                SQX_Regulatory_Report__c = regulatoryReportManufacturer.Id,
                SQX_Medwatch__c = medwatchManufacturer.Id
            );
            submissionHistoryList.add(submissionHistoryManufacturer);

            new SQX_DB().op_insert(submissionHistoryList, new List<SObjectField> {});

            Test.startTest();

            // ACT : Create a followup report
            List<Id> medwatchImporterIds = SQX_Create_Followup.createFollowup(new List<Id>{regulatoryReportImporter.Id});
            List<Id> medwatchManufacturerIds = SQX_Create_Followup.createFollowup(new List<Id>{regulatoryReportManufacturer.Id});
            

            // ASSERT : Fields are mapped correctly
            SQX_Medwatch__c medwatch = [SELECT Id, F7_Type_Of_Report__c, G7_Type_Of_Report__c FROM SQX_Medwatch__c WHERE Id IN : medwatchImporterIds AND SQX_Regulatory_Report__r.SQX_Reporting_Default__r.Reporting_Site__c = 'User Facility/Importer Site'];
            System.assertEquals('F', medwatch.F7_Type_Of_Report__c);
            System.assertEquals(null, medwatch.G7_Type_Of_Report__c);

            medwatch = [SELECT Id, F7_Type_Of_Report__c, G7_Type_Of_Report__c FROM SQX_Medwatch__c WHERE Id IN : medwatchManufacturerIds AND SQX_Regulatory_Report__r.SQX_Reporting_Default__r.Reporting_Site__c = 'Manufacturing Site'];
            System.assertEquals(null, medwatch.F7_Type_Of_Report__c);
            System.assertEquals('C53579', medwatch.G7_Type_Of_Report__c);
              
            Test.stopTest();

        }
        
    }
    
}