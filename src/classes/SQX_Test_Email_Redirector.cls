/**
 * test class for email redirector controller
 */
@isTest
public class SQX_Test_Email_Redirector {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');	
    }
    /**
     * GIVEN : An user with lightning preference enabled if org has lightning feature
     * WHEN : Relative URL is given
     * THEN : User will be redirected to salesforce app 
     * @date : 2018/01/05
     * @story : [SQX-3528]
     */
    public static testMethod void givenAnUser_WhenLightningPreferenceIsEnabled_URLContainsAloha(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        Boolean isLightningPreferred = standardUser.getSObjectType().getDescribe().fields.getMap().containsKey('UserPreferencesLightningExperiencePreferred');
        if(isLightningPreferred){
            if(isLightningPreferred){
                standardUser.UserPreferencesLightningExperiencePreferred = true;
                update standardUser;
            }
            System.runas(standardUser){ 
                
                // ARRANGE : An audit is created
                SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                    .setStatus(SQX_Audit.STATUS_DRAFT)
                    .setStage(SQX_Audit.STAGE_PLAN)
                    .save();
                PageReference myVfPage = Page.SQX_Email_Redirector;
                myVFPage.getParameters().put('redirectTo', '%2Fapex%2FSQX_Audit%3Fid%3'+ audit.audit.Id +'%26initialTab%3DfindingTab');
                Test.setCurrentPage(myVfPage);
                
                // ACT : Redirect URL is retrieved
                SQX_Email_Redirector_Controller redirectController = new SQX_Email_Redirector_Controller();
                String resultUrl = redirectController.getRedirectToRecord();
                
                // ASSERT : User will be redirected to salesforce app
                System.assert(resultUrl.contains('alohaRedirect'), 'Must be redirected to lightning page');
                System.assertEquals(true, SQX_Utilities.getUserPreferenceForLightning());
            }
        }
    }
    /**
     * GIVEN : An user with lightning preference not enabled
     * WHEN : Relative URL is given
     * THEN : User will be redirected to standard url
     * @date : 2018/01/05
     * @story : [SQX-3528]
     */
    public static testMethod void givenAnUser_WhenLightningPreferenceIsNotEnabled_URLDoesNotContainsAloha(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        Boolean isLightningPreferred = standardUser.getSObjectType().getDescribe().fields.getMap().containsKey('UserPreferencesLightningExperiencePreferred');
        if(isLightningPreferred){
            standardUser.UserPreferencesLightningExperiencePreferred = false;
            update standardUser;
        }
        System.runas(standardUser){ 
			
            // ARRANGE : An audit is created
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
            PageReference myVfPage = Page.SQX_Email_Redirector;
            myVFPage.getParameters().put('redirectTo', '%2Fapex%2FSQX_Audit%3Fid%3'+ audit.audit.Id +'%26initialTab%3DfindingTab');
            Test.setCurrentPage(myVfPage);
            
            // ACT : Redirect URL is retrieved
            SQX_Email_Redirector_Controller redirectController = new SQX_Email_Redirector_Controller();
            String resultUrl = redirectController.getRedirectToRecord();
            // ASSERT : User will be redirected to standard url
            System.assert(!resultUrl.contains('alohaRedirect'), 'Must not be redirected to lightning page');
        }
    }
}