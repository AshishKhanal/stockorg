/**
* This class performs the following tests in the regulatory report
* a. If a pending regulatory report is added then a SF task is created [givenRegulatoryReportIsAdded_ATaskIsAdded]
* b. If a complete/voided report is added then a SF task isn't created
* c. If a task is completed or voided then the associated SF task is closed.
* d. If a task is deleted before completing the task, no error is thrown.
* e. If a complaint regulatory report is assigned to some one else, the report should be completable/voidable by the complaint owner [givenRegulatoryReportIsAssignedToOther_ComplaintOwnerCanCompleteIt]
* f. A Regulatory report can't be completed without submission history.
* g. A Regulatory report can be voided without submission history.
*
* ------ Not related to main story but implementation
* h. The common remoting action SQX_Extension_UI.getObjects returns the correct set of objects if the user has permission
* i. The common remoting action SQX_Extension_UI.getObjects returns the correct set of objects with required fields if the user has permission
* j. If user doesn't have read access on the object no data is returned [Check for Read Access Compliance]
* 
* ------ Regressions
* k. SQX-1876 - Test to ensure that if two regulatory report are added at once, then both have their task Ids set properly. [run_givenTwoReports_TwoTasksAreCreatedAndLinked]
*/
@IsTest
public class SQX_Test_1763_Regulatory_Report {
    public static boolean   runAllTests = true,
                            run_givenRegulatoryReportIsAdded_ATaskIsAdded = false,
                            run_givenRegulatoryReportIsAssignedToOther_ComplaintOwnerCanCompleteIt = false,
                            run_givenRegReportIsCompleted_SubmissionIsRequired = false,
                            run_givenObjectsAreRetrievedUsingExtension_TheyAreCorrectlyReturned = false,
                            run_givenTwoReports_TwoTasksAreCreatedAndLinked = false,
                            run_givenTwoReportsWithSameName_TwoTasksAreCreatedAndLinked = false;
    
    
    /**
    * This method performs the following tests in the regulatory report
    * a. If a pending regulatory report is added then a SF task is created [givenRegulatoryReportIsAdded_ATaskIsAdded]
    * b. If a complete/voided report is added then a SF task isn't created
    * c. If a task is completed or voided then the associated SF task is closed.
    * d. If a task is deleted before completing the task, no error is thrown
    */
    public static testmethod void givenRegulatoryReportIsAdded_ATaskIsAdded(){
        
        if(!runAllTests && !run_givenRegulatoryReportIsAdded_ATaskIsAdded){
            return;
        }
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            //Act: Add a pending regulatory report [a]
            SQX_Regulatory_Report__c regReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING);
            
            //Assert: Ensure that a regulatory report has a task associated with it.
            SQX_Regulatory_Report__c report = [SELECT Id, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : regReport.Id];
            System.assertNotEquals(null, report.Task_Id__c, 'Expected the regulatory report to be complete');
            System.assertEquals(1, [SELECT Id FROM Task WHERE Id = : report.Task_Id__c].size(), 'Expected a single task to be created for the report');
            
            
            //Act: Add a completed and a void regulatory report [b,c]
            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            SQX_Regulatory_Report__c completedReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_COMPLETE, null, null, submissionHistory);
            SQX_Regulatory_Report__c voidedReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_VOID);
            
            //Assert: Ensure that a regulatory report has a task associated with it.
            System.assertEquals(null, [SELECT Id, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : completedReport.Id].Task_Id__c, 'Expected no task to be created for the regulatory report');
            System.assertEquals(null, [SELECT Id, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : voidedReport.Id].Task_Id__c, 'Expected no task to be created for the regulatory report');
            
            
            //Act: Close the pending regulatory report [d]
            regReport.Status__c = SQX_Regulatory_Report.STATUS_VOID;
            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c> { regReport }, new List<SObjectField> { SQX_Regulatory_Report__c.Status__c });
            
            //Assert: Ensure that a task associated with the regulatory report has been closed
            System.assertEquals('Completed', [SELECT Id, Status FROM Task WHERE Id = : report.Task_Id__c].Status, 'Expected a report task to be created for the report');


            //Arrange: Add a pending regulatory report, and delete its associated task
            regReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING);
            report = [SELECT Id, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : regReport.Id];
            delete new Task(Id = report.Task_Id__c);

            //Act: 
            Boolean hasError = false;
            try{
                regReport.Status__c = SQX_Regulatory_Report.STATUS_COMPLETE;
                regReport.SQX_Submission_History__c = submissionHistory.Id;
                new SQX_DB().op_update(new List<SQX_Regulatory_Report__c> { regReport }, new List<SObjectField> { SQX_Regulatory_Report__c.Status__c });
                    
            }
            catch(Exception ex){
                hasError = true;
            }


            System.assertEquals(false, hasError, 'Expected no error to occur while completeing the task');
        }
    }


    /**
    * This method ensures that 
    * e. If a complaint regulatory report is assigned to some one else, the report should be completable/voidable by the complaint owner 
    */
    public static testmethod void givenRegulatoryReportIsAssignedToOther_ComplaintOwnerCanCompleteIt(){
        if(!runAllTests && !run_givenRegulatoryReportIsAssignedToOther_ComplaintOwnerCanCompleteIt){
            return;
        }
        
        //Arrange: Create an open complaint and add a pending regulatory report assigned to standard user 2
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runAs(standardUser){
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Regulatory_Report__c regReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING, Date.today().addDays(10), standardUser2);

            //Act: Complete the pending regulatory report task assigned to standard user 2 [e]
            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            submissionHistory.Status__c = SQX_Submission_History.STATUS_COMPLETE;
            submissionHistory.SQX_Regulatory_Report__c = regReport.Id;
            new SQX_DB().op_update(new List<SQX_Submission_History__c> {submissionHistory}, new List<SObjectField>{});

            //Assert: Ensure that the task associated with the report was closed
            regReport = [SELECT Id, Status__c, Task_Id__c, Submitted_Date__c FROM SQX_Regulatory_Report__c WHERE Id = : regReport.Id];
            Task regReportTask = [SELECT Id, Status FROM Task WHERE Id = : regReport.Task_Id__c];

            System.assertEquals(SQX_Regulatory_Report.STATUS_COMPLETE, regReport.Status__c);
            System.assertEquals(Date.today(), regReport.Submitted_Date__c);
            System.assertEquals('Completed', regReportTask.Status, 'Expected the task to be completed');

        }
    }
    
    
    /**
     * This method ensures the following
     * f. A Regulatory report can't be completed without submission history.
     * g. A Regulatory report can be voided without submission history.
     */
    public static testmethod void givenRegReportIsCompleted_SubmissionIsRequired(){
        if(!runAllTests && !run_givenRegReportIsCompleted_SubmissionIsRequired){
            return;
        }
        
        //Arrange: Create an open complaint and add a pending regulatory report
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(adminUser){
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Regulatory_Report__c regReport = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING),
                                     regReport2 = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING);

            //Act: Complete the pending regulatory report without submission history [f]
            regReport.Status__c = SQX_Regulatory_Report.STATUS_COMPLETE;
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Regulatory_Report__c> {regReport}, new List<SObjectField>{}).get(0);
            
            //Assert: Ensure that the save failed
            System.assertEquals(false, result.isSuccess(), 'Expected the save to not succeed but was successful');
            
            
            //Act: void the pending regulatory report without submission history [g]
            regReport.Status__c = SQX_Regulatory_Report.STATUS_VOID;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Regulatory_Report__c> {regReport}, new List<SObjectField>{}).get(0);
            
            //Assert: Ensure that the save succeeded
            System.assertEquals(true, result.isSuccess(), 'Expected the save to not succeed but it failed with error ' + result);
            

            //Act: Complete regulatory report 2 with submission history [f']
            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            regReport2.SQX_Submission_History__c = submissionHistory.Id;
            regReport2.Status__c = SQX_Regulatory_Report.STATUS_COMPLETE;
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Regulatory_Report__c> {regReport2}, new List<SObjectField>{}).get(0); 
           
            //Assert: Ensure that the save succeeded
            System.assertEquals(true, result.isSuccess(), 'Expected the save to succeed but it failed with error ' + result);
            
        }
    }
    
    
    /**
     * This test method tests SQX_Extension_UI's getObjects method and ensures that it works. It checks for the following scenario
     * 
     * h. The common remoting action SQX_Extension_UI.getObjects returns the correct set of objects if the user has permission
     * i. The common remoting action SQX_Extension_UI.getObjects returns the correct set of objects with required fields if the user has permission
     * j. If user doesn't have read access on the object no data is returned [Check for Read Access Compliance]
     */
    public static testmethod void givenObjectsAreRetrievedUsingExtension_TheyAreCorrectlyReturned(){
        if(!runAllTests && !run_givenObjectsAreRetrievedUsingExtension_TheyAreCorrectlyReturned){
            return;
        }
        
        //Arrange: Create two users one with read permission on task object (Custom task object of CQ). Add three tasks
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User nonAdminUser = SQX_Test_Account_Factory.createUser(null, 'Random', null); //HACK: sending random value to profile selects standard user profile but no permission set
        SQX_Test_Task_Library task1, task2, task3;
        final String TASK_OBJECT_TYPE = SQX_Task__c.getSObjectType() + '';
        
        System.runAs(adminUser){
            task1 = new SQX_Test_Task_Library().save();
            task2 = new SQX_Test_Task_Library().save();
            task3 = new SQX_Test_Task_Library().save();
            
            //Act: Query for task object using extension UI [h]
            List<SObject> objects = SQX_Extension_UI.getObjects(TASK_OBJECT_TYPE, null, 1, null );
            
            //Assert: Ensure that 3 task objects were returned
            System.assertEquals(3, objects.size(), 'Expected 3 task objects found ' + objects);
            System.assertEquals(SQX_Task__c.getSObjectType(), objects.get(0).getSObjectType(), 'Expected object to be of task object type but found ' + objects.get(0).getSObjectType());
            
            
            //Act: Query for task object and two fields [i]
            List<String> fields = new List<String> { SQX_Task__c.Record_Type__c + '', SQX_Task__c.Description__c + ''};
            Map<Id, SObject> objectMap = new Map<Id, SObject> (SQX_Extension_UI.getObjects(TASK_OBJECT_TYPE, fields, 1, null));
            
            //Assert: Ensure that Record type field was fetched
            SQX_Task__c task1Retrieved = (SQX_Task__c) objectMap.get(task1.task.Id);
            System.assertEquals(task1.task.Record_Type__c, task1Retrieved.Record_Type__c, 'Expected the retreived task1 to contain same value but found ' + task1 + ' ' + task1Retrieved);
            System.assertEquals(task1.task.Description__c, task1Retrieved.Description__c);
            
            //Assert: Ensure fields not in the list haven't been serialized such as Name and throws an error.
            Boolean hasError = false;
            try{
                System.debug(task1Retrieved.Name);
            }
            catch(Exception ex){
                hasError = true;
            }
            System.assertEquals(true, hasError, 'Expected exception to occur when querying for field not queried.');
            
            
        }
        
        System.runAs(nonAdminUser){
            Boolean throwsError = false;
            try{
                //Act: Query for task object using extension UI [j]
                List<SObject> objects = SQX_Extension_UI.getObjects(TASK_OBJECT_TYPE, null, 1, null );
            }
            catch(SQX_ApplicationGenericException ex){
                throwsError = true;
            }
            
            //Assert: Ensure that 0 task objects were returned because this user doesn't have read access in task object
            System.assertEquals(true,  throwsError, 'Expected an exception to be raised');
            
        }
    }
    
    
    /**
     * This test method ensures that 
     * k. SQX-1876 - Test to ensure that if two regulatory report are added at once, then both have their task Ids set properly.
     *
     * An issue was observed where if two reportables where added and later voided by decision tree run. SF tasks created for the decision tree weren't
     * closed.
     *
     * Root Cause: The issue was caused because single task Id was stored for all reports created in a single batch, due to a semantical bug in the code
     * Fix: Ensured that each report had their corresponding task id set correctly.
     *
     * This test ensures that the fix works correctly, by inserting two regulatory reports in a single batch and ensuring that the task reference in the
     * object are updated correctly and the tasks are created correctly.
     */
    public static testmethod void givenTwoReports_TwoTasksAreCreatedAndLinked(){
        if(!runAllTests && !run_givenTwoReports_TwoTasksAreCreatedAndLinked){
            return;
        }
        
        //Arrange: Create an open complaint and add a regulatory report.
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runAs(standardUser){
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Regulatory_Report__c regReport1 = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING, Date.today().addDays(10), standardUser2);

            //Act: Clone the regulatory report and create two regulatory reports and save it
            SQX_Regulatory_Report__c regReport2 = regReport1.clone(),
                                     regReport3 = regReport2.clone();



            regReport3.SQX_User__c = standardUser.Id;
            regReport3.Due_Date__c = Date.today();


            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c> {regReport2, regReport3}, new List<SObjectField> {});


            //Assert: Ensure that tasks have been correctly created for both the report items
            Map<Id, SQX_Regulatory_Report__c> reports = new Map<Id, SQX_Regulatory_Report__c>([SELECT Id, Task_Id__c, Name, SQX_User__c
                                                                FROM SQX_Regulatory_Report__c
                                                                WHERE Id = : regReport2.Id OR Id = : regReport3.Id ]);

            Id  report2sTaskId = reports.get(regReport2.Id).Task_Id__c,
                report3sTaskId = reports.get(regReport3.Id).Task_Id__c;

            System.assertNotEquals(report2sTaskId, report3sTaskId, 'Expected the two task ids to be different for two tasks');

            Map<Id, Task> tasks = new Map<Id, Task>([SELECT Id, Subject, WhatId, OwnerId, ActivityDate FROM Task WHERE Id =: report2sTaskId OR Id = : report3sTaskId]);
            Task report2sTask = tasks.get(report2sTaskId);
            Task report3sTask = tasks.get(report3sTaskId);

            //assert tasks are correctly assigned
            System.assertEquals(regReport2.SQX_User__c, report2sTask.OwnerId); 
            System.assertEquals(regReport3.SQX_User__c, report3sTask.OwnerId);

            //assert tasks have correct due date
            System.assertEquals(regReport2.Due_Date__c, report2sTask.ActivityDate);
            System.assertEquals(regReport3.Due_Date__c, report3sTask.ActivityDate);

        }
        
    }


    /**
    * If two reports with same name are inserted then task is created succesfully.
    * @story SQX-2368
    */
    static testmethod void givenTwoReportsWithSameName_TwoTasksAreCreatedAndLinked(){
        if(!runAllTests && !run_givenTwoReportsWithSameName_TwoTasksAreCreatedAndLinked){
            return;
        }
        
        //Arrange: Create an open complaint and add a regulatory report.
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runAs(standardUser){
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            SQX_Regulatory_Report__c regReport1 = complaint.addRegulatoryReport(null, SQX_Regulatory_Report.STATUS_PENDING, Date.today().addDays(10), standardUser2);

            //Act: Clone the regulatory report and create two regulatory reports and save it
            //     Note, the diff between previous test and this is that both report have same index fields which causes an issue for map.
            SQX_Regulatory_Report__c regReport2 = regReport1.clone(),
                                     regReport3 = regReport2.clone();


            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c> {regReport2, regReport3}, new List<SObjectField> {});


            //Assert: Ensure that tasks have been correctly created for both the report items
            Map<Id, SQX_Regulatory_Report__c> reports = new Map<Id, SQX_Regulatory_Report__c>([SELECT Id, Task_Id__c, Name, SQX_User__c
                                                                FROM SQX_Regulatory_Report__c
                                                                WHERE Id = : regReport2.Id OR Id = : regReport3.Id ]);

            Id  report2sTaskId = reports.get(regReport2.Id).Task_Id__c,
                report3sTaskId = reports.get(regReport3.Id).Task_Id__c;

            System.assertNotEquals(report2sTaskId, report3sTaskId, 'Expected the two task ids to be different for two tasks');

            Map<Id, Task> tasks = new Map<Id, Task>([SELECT Id, Subject, WhatId, OwnerId, ActivityDate FROM Task WHERE Id =: report2sTaskId OR Id = : report3sTaskId]);
            Task report2sTask = tasks.get(report2sTaskId);
            Task report3sTask = tasks.get(report3sTaskId);

            //assert tasks are correctly assigned
            System.assertEquals(regReport2.SQX_User__c, report2sTask.OwnerId); 
            System.assertEquals(regReport3.SQX_User__c, report3sTask.OwnerId);

            //assert tasks have correct due date
            System.assertEquals(regReport2.Due_Date__c, report2sTask.ActivityDate);
            System.assertEquals(regReport3.Due_Date__c, report3sTask.ActivityDate);

        }
        
    }
    
}
