/**
 * Unit tests for Supplier Escalation Step
 */
@isTest
public class SQX_Test_Supplier_Escalation_Step {
    @testSetup
    public static void commonSetup() {
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        
        System.runAs(adminUser) {
            // create auto number record for supplier escalation
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Escalation',
                Next_Number__c = 1,
                Number_Format__c = 'ESC-{1}',
                Numeric_Format__c = '0000'
            );
        }
        System.runAs(standardUser) {
            // Create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
        }
    }
    
    /**
     * Ensure following validation rules for Supplier Escalation Step are being validated : 
     * 1. 
     *  Given : Supplier Escalation and Step record
     *  When : Try to update step record with Issue_Date__c > Expiration_Date__c
     *  Then : Error is thrown
     * 2.
     *  Given : Supplier Escalation Record with step
     *  When : Try to update step record with Step__c value as zero
     *  Then : Error is thrown 'Step should be greater than zero'
     * 3.
     *  Given : Open Supplier Escalation Step record with corresponding Task
     *  When : Try to complete task without putting Result
     *  Then : Error is thrown
     * 4.
     *  Given : Completed Supplier Escalation Step record
     *  When : Try to update record
     *  Then : Error is thrown as locked record cannot be updted
     * 5.
     *  Given : Supplier Escalation Record with 2 steps with step 1 and 2, and step 1 is already complete
     *  When : New Supplier Escalation Step with Step__c value as 1 is tried to add to Escalation Record
     *  Then : Error is thrown
     * 6.
     *  Given : Open Escalation Step Record
     *  When : Try to make assignee as blank
     *  Then : Error is thrown
     *  
     *  When : try to add obsolete doc to Escalation Step
     *  Then : Error is thrown
     *  
     *  When : Try to add void Audit to Escalation Step
     *  Then : Error is thrown
     */
    public static testMethod void validateValidationRulesOfSupplierEscalationStep() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            // Arrange : Create obsolete doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_OBSOLETE;
            doc.save();

            // Arrange : Create Audit and void it
            SQX_Test_Audit audit = new SQX_Test_Audit().save();
            audit.audit.Status__c = SQX_Audit.STATUS_VOID;
            audit.save();
            
            // get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // create two pollicy task so that it will create 2 steps later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Perform Audit', standardUser, 2);
            
            // Arrange : create Supplier Escalation record
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            // Submit record
            escalation.submit();
            
            // Act : try to udpate with Issue_Date__c > Expiration_Date__c
            SQX_Supplier_Escalation_Step__c step = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id LIMIT 1];
            step.Expiration_Date__c = Date.today().addDays(-2);
            step.Issue_Date__c = Date.today().addDays(-1);

            // Assert : Ensure issue date should not be greater than expiration date
            String msg = 'Issue Date cannot be greater than Expiration Date.';
            String reasonOfNotSuccess = 'Issue date cannot be greater than Expiration date.';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // Act : try to put step value to 0
            step = new SQX_Supplier_Escalation_Step__c(Id = step.Id, Step__c = 0);

            // Assert : Ensure validation error is thrown as step should be greater than zero
            msg = 'Step should be greater than zero';
            reasonOfNotSuccess = 'Expected Step should be greater than zero';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // initiate record
            escalation.initiate();
            
            // get created task and try to complete without putting result it
            step = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN];
            Task tsk = [SELECT Id, Status  FROM Task WHERE WhatId =: escalation.supplierEscalation.Id AND Child_What_Id__c = :step.Id];
            tsk.Status = SQX_Task.STATUS_COMPLETED;
            tsk.Description = 'Completing task with result as Go';

            // Assert : Ensure Escalation Step is not complete without Result
            msg = 'Please set result in order to complete step';
            reasonOfNotSuccess = 'Expected task not to be completed with result blank';
            SQX_Test_Supplier_Escalation.performActionAndAssert(tsk, msg, reasonOfNotSuccess, null);

            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(tsk.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);


            // query the completed step
            step = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE Id = :step.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_COMPLETE];
            
            // Act : try to update the completed step (locked step)
            step.Step__c = 5;

            // Assert : Ensure locked record cannot be updated
            msg = 'Record status or Step status does not support the action performed.';
            reasonOfNotSuccess = 'Expected locked step not to be updated but is being updated';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // get the workflow status of the supplier escalation
            SQX_SUpplier_Escalation__c se = [SELECT Id, Workflow_Status__c, Current_Step__c FROM SQX_Supplier_Escalation__c WHERE Id = :escalation.supplierEscalation.Id];
            System.assert(se.Workflow_Status__c != SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED, 'Expected workflow status not to be complete');
            System.assert(se.Current_Step__c == 2, 'Expected step to be 2 but is' + se.Current_Step__c);


            // try to add step having step value 1 which is already completed
            step = new SQX_Supplier_Escalation_Step__c(Step__c = 1, SQX_User__c = standardUser.Id, SQX_Parent__c = se.Id);
            
            // Assert : Ensure new Escalation Step record with already completed step value cannot be added
            msg = 'Steps cannot be added for step levels that are completed. Add steps to active step level or for a non completed step level';
            reasonOfNotSuccess = 'Expected step should not be inserted having step lower than the current step of Escalation record';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // retrive the open escalation step 
            step = [SELECT Id,step__c,Status__c,Is_Parent_Locked__c,Is_Locked__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN];
            //complete and no go the sf task 
            Task sfTask = [SELECT Id, Status  FROM Task WHERE WhatId =: escalation.supplierEscalation.Id AND Child_What_Id__c = :step.Id];            

            SQX_BulkifiedBase.clearAllProcessedEntities();
            description = 'Completing task with result as NO Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_PERFORM_AUDIT);

            // Retrive the escaluation step which is complete and no  go 
            SQX_Supplier_Escalation_Step__c escalationPolicy = [SELECT Id FROM SQX_Supplier_Escalation_Step__c 
                                                                 WHERE SQX_Parent__c =:escalation.supplierEscalation.Id
                                                                 AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                                                 AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_GO];
            
            // query the NoGo record
            step = [SELECT Id,Step__c,Status__c,Result__c FROM SQX_Supplier_Escalation_Step__c WHERE Id = :step.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_COMPLETE and Result__c =:SQX_Steps_Trigger_Handler.RESULT_NO_GO ];
            String obRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, 'Task');
            //Act : add complete supplier escalation id while creating new escalation step 
            List<SQX_Supplier_Escalation_Step__c> escaluationPolicy=new SQX_Supplier_Escalation_Step__c[]{
                                                                    new SQX_Supplier_Escalation_Step__c (
                                                                                                            SQX_Parent__c=escalation.supplierEscalation.Id,
                                                                                                            Step__c=1,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            SQX_Parent_Step__c=escalationPolicy.id,
                                                                                                            RecordTypeId =obRecordTypeId
                                                                                                        ),
                                                                     new SQX_Supplier_Escalation_Step__c (
                                                                                                            SQX_Parent__c=escalation.supplierEscalation.Id,
                                                                                                            Step__c=2,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            SQX_Parent_Step__c=escalationPolicy.id,
                                                                                                            RecordTypeId =obRecordTypeId
                                                                                                        )};

            List<DataBase.SaveResult> redoResult = new SQX_DB().continueOnError().op_insert(escaluationPolicy, new List<Schema.SObjectField>{});
            //Assert: Verify the Validation Rule Error message
            System.assert(!redoResult[0].isSuccess(),msg);
           	System.assertEquals(msg,redoResult[0].getErrors()[0].getMessage(),'while redo escalation step which is complete step user should get error message');
            System.assertEquals(true, redoResult[1].isSuccess(),'Record save Successfully>>' + redoResult[1].getErrors());

            // Act : try to make assignee blank for open step
            step = [SELECT Id, SQX_User__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN LIMIT 1];
            step.SQX_User__c = null;

            // Assert : Ensure validation error is thrown as assignee cannot be null
            msg = 'Assignee cannot be blank.';
            reasonOfNotSuccess = 'Expected step should not be updated having assignee blank';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // Act : try to add obsolete doc to open step
            step.SQX_Controlled_Document__c = doc.doc.Id;
            step.SQX_User__c = standardUser.Id;

            // Assert : Ensure validation error is thrown
            msg = 'Only Pre-Release and Current Controlled Documents can be referred.';
            reasonOfNotSuccess = 'Expected step should not be updated having obsolete doc';            
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // Act : try to redo open step
            step.Redo_Policy__c = true;
            step.SQX_Controlled_Document__c = null;
            
            // Assert : Ensure validation error is thrown
            msg = 'Cannot redo step which has not been completed.';
            reasonOfNotSuccess = 'Expected step should not be redone';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);

            // Try to add void audit to step
            step.Redo_Policy__c = false;
            step.SQX_Audit_Number__c = audit.audit.Id;

            // Assert : Ensure validation error is thrown
            msg = 'Audit with void status cannot be used.';
            reasonOfNotSuccess = 'Expected void audit not to be added to Escalation Step';
            SQX_Test_Supplier_Escalation.performActionAndAssert(step, msg, reasonOfNotSuccess, null);
        }
    }
    
    /**
     * Given : On Hold Supplier Escalation Record
     * When : Redo escalation step whose task is completed with No Go result
     * Then : New open Supplier Escalation Step is created
     */
    public static testMethod void givenOnHoldSupplierEscalationRecord_WhenStepIsRedone_NewOpenEscalationStepRecordIsCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        
        System.runAs(standardUser) {
            
            // Arrange : get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // create policy task so that it will create step later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            
            // Arrange : create Supplier Escalation record
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            // submit
            escalation.submit();
            // initiate
            escalation.initiate();
            
            // Assert : Ensure task is created
            Task tsk = [SELECT Id, WhatId, Status FROM Task WHERE WhatId = :escalation.supplierEscalation.Id];
            System.assert(tsk.Id != null, 'Expected task to be created but is ' + tsk.Id);
            
            // complete task with result no go
            SQX_BulkifiedBase.clearAllProcessedEntities();
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(tsk.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            // Act : Redo step with standardUser
            SQX_Supplier_Escalation_Step__c step = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_COMPLETE];
            SQX_Supplier_Redo_Step.RedoStepResult redoStepResult = escalation.redoStep(step);
            
            // Assert : Ensure new open step is created
            System.assert(String.isBlank(redoStepResult.errorMessage), 'Expected no error message while doing redo by authorized user but is ' + redoStepResult.errorMessage);
            step = [SELECT Id, Archived__c, Applicable__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_OPEN];
            System.assert(step.Id != null, 'Expected new step to be created when performing redo');
        }
    }

    /**
     * Given : On Hold Supplier Escalation Record
     * When : Redo escalation step whose task is completed with No Go result by unauthorized user
     * Then : Redo should be failed.
     */
    public static testMethod void givenOnHoldSupplierEscalationRecord_WhenStepIsRedoneByUnauthorizedUser_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUser1 = allUsers.get('standardUser1');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        SQX_Test_Supplier_Escalation escalation;
        SQX_Supplier_Escalation_Step__c step;
        System.runAs(standardUser) {
            
            // Arrange : get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // create policy task so that it will create step later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            
            // Arrange : create Supplier Escalation record
            escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            // submit
            escalation.submit();
            // initiate
            escalation.initiate();
            
            // Assert : Ensure task is created
            Task tsk = [SELECT Id, WhatId, Status FROM Task WHERE WhatId = :escalation.supplierEscalation.Id];
            System.assert(tsk.Id != null, 'Expected task to be created but is ' + tsk.Id);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            // complete task with result no go
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(tsk.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);

        }

        System.runAs(standardUser1) {

            // Act : Try to Redo step with standardUser1 (this user does not have eidit access to step record)
            step = [SELECT Id FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id AND Status__c = :SQX_Supplier_Common_Values.STATUS_COMPLETE];
            SQX_Supplier_Redo_Step.RedoStepResult redoStepResult = escalation.redoStep(step);
            System.assert(String.isNotBlank(redoStepResult.errorMessage), 'Expected error message while doing redo by unauthorized user but is ' + redoStepResult.errorMessage);
        }
    }

        
    /**
     * Given: Supplier esculation  step
     *         a)Doc Request Step with Result Go and  expiration date empty
     *         b)step with need periodic update and notify before expiration is set  
     * When: a)user try to complete sf task  with out expiration date
     *        b)user try redo the supplier esculation step
     * Then: a)User get error message "CQ Document Expiration Date is Required"
     *       b) Redo record gets copy need periodic update and need before expiration
     * @story : SQX-6833
     */
    @isTest
    public static void givenEsculationStepWithDocRequestRecordType_WhenUserTryToCompleteSfTaskWithoutExpirationDate_ThenUserGetErrorMessage(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {

            // Arrange :Create a SE Record 
            SQX_Test_Supplier_Escalation escalation= new SQX_Test_Supplier_Escalation().save();

            
            String obRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, 'Task');
            //add supplier escalation steps 
            List<SQX_Supplier_Escalation_Step__c> escalationPolicies= new SQX_Supplier_Escalation_Step__c[]{
                                                                    new SQX_Supplier_Escalation_Step__c (   SQX_Parent__c=escalation.supplierEscalation.id,
                                                                                                            Step__c=1,
                                                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                                                            Due_Date__c=Date.today(),
                                                                                                            Allowed_Days__c=10,
                                                                                                            Needs_Periodic_Update__c=true,
                                                                                                            Notify_Before_Expiration__c=2,
                                                                                                            RecordTypeId = obRecordTypeId
                                                                                                )};

            //Try To insert escalation Step with needs periodic update and notify before expiration date
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(escalationPolicies, new List<Schema.SObjectField>{});
            System.assertEquals(true, result[0].isSuccess(),'Record save failed >>' + result[0].getErrors());

            // Submit the SE record
            escalation.setStage(SQX_Supplier_Common_Values.STAGE_TRIAGE);
            escalation.save();
            // SE is initiated
            escalation.initiate();
            //Arrange: retrive escalation step and sf task
            SQX_Supplier_Escalation_Step__c escalationPolicy=[select id ,Allowed_Days__c,Due_Date__c from SQX_Supplier_Escalation_Step__c where SQX_Parent__c=:escalation.supplierEscalation.id];

            escalationPolicy.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            escalationPolicy.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            escalationPolicy.Comment__c = 'Record is completed and result is nogo description.';

             //Try to complete sf task  with out adding expiration date 
            List<Database.SaveResult> results = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation_Step__c>{escalationPolicy}, new List<Schema.SObjectField>{});
            //Assert: verify validation error message 
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(results[0].getErrors(), SQX_Supplier_Common_Values.EXPIRATION_DATE_VALIDATION_ERROR_MSG),
                          'Task update should fail with error: ' + SQX_Supplier_Common_Values.EXPIRATION_DATE_VALIDATION_ERROR_MSG + '. Actual Errors: ' + results[0].getErrors());
            
            SQX_BulkifiedBase.clearAllProcessedEntities();

            escalationPolicy.Result__c = SQX_Steps_Trigger_Handler.RESULT_NO_GO;
            escalationPolicy.Expiration_Date__c=Date.today();
            //Should save the sf task 
            List<Database.SaveResult> successResults = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Escalation_Step__c>{escalationPolicy}, new List<Schema.SObjectField>{});
            
            //Assert: record save successfully
            System.assertEquals(true, successResults[0].isSuccess(),'Record save failed >>' + successResults[0].getErrors());

            
             //Act: Redo the record
            SQX_Supplier_Escalation_Step__c escalationStepRecord =  [SELECT Id,Needs_Periodic_Update__c,Notify_Before_Expiration__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: escalation.supplierEscalation.id
                                                            AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                                            AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
            SQX_Test_Supplier_Escalation os = new SQX_Test_Supplier_Escalation();
            os.redoStep(escalationStepRecord);
              
            SQX_BulkifiedBase.clearAllProcessedEntities();
            //retrive the redo record 
            SQX_Supplier_Escalation_Step__c redoRecord = [SELECT Id,Needs_Periodic_Update__c,Notify_Before_Expiration__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c =: escalation.supplierEscalation.id AND Status__c = :SQX_Steps_Trigger_Handler.STATUS_OPEN];
            //Assert: //Assert: Verify that Needs Periodic Update and Notify Before Expiration is transfer 
            System.assertEquals(escalationStepRecord.Needs_Periodic_Update__c,redoRecord.Needs_Periodic_Update__c,'Needs Periodic Update should be transfer to new onboardding');
            System.assertEquals(escalationStepRecord.Notify_Before_Expiration__c,redoRecord.Notify_Before_Expiration__c,'Notify Before Expiration should be transfer to new onboardding');
       }
    }
    /**
     * Given: Escalation Step record of type document request
     * When: Associated form to use document is revised and released
     * Then: Escalation Step should have reference to newly revised/released doc.
     * @story : SQX-7917
     */
    @isTest
    public static  void givenEscalationStep_WhenReferredDocIsRevisedAndReleased_FormToUseIsUpdatedInStep(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
	    //Arrange: Create required records
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
            doc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            doc1.save().synchronize();
            SQX_Controlled_Document__c initialDoc = [Select Id, Name, Document_Number__c from SQX_Controlled_Document__c where id = :doc1.doc.id];

            SQX_Test_Supplier_Escalation es = new SQX_Test_Supplier_Escalation().save();
            SQX_Supplier_Escalation_Step__c escalationStep =  new SQX_Supplier_Escalation_Step__c(SQX_Parent__c= es.supplierEscalation.id,
                                                                            Name='EscalationStep',
                                                                            Step__c=1,
                                                                            SQX_User__c=UserInfo.getUserId(),
                                                                            Due_Date__c=Date.today(),
                                                                            SQX_Controlled_Document__c=doc1.doc.id,
                                                                            RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.SupplierEscalationStep, 'Document_Request'),
                                                                            Document_Name__c = 'Sample Document Name'
                                                                            );
            insert escalationStep;
            
            // ACT : Control doc is revised and released
			SQX_Test_Controlled_Document  revisedDoc = doc1.revise().save();
            revisedDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Supplier_Escalation_Step__c updatedStep = [Select id, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, SQX_Controlled_Document__r.Revision__c from SQX_Supplier_Escalation_Step__c where id= :escalationStep.id];
	    //ASSERT : Make sure form to use field in deviation step is updated.
            System.assertEquals(true, (updatedStep.SQX_Controlled_Document__r.Name).containsIgnoreCase('REV-2'), 'Expected controlled doc referred in step record to be 2nd revision');
            System.assertEquals(escalationStep.SQX_Controlled_Document__c == doc1.doc.id, updatedStep.SQX_Controlled_Document__c != doc1.doc.id);
        }
    }
}