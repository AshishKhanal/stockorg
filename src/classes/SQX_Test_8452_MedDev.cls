/**
 * Test Class to ensure field validations works properly in MedDev records
 */
@isTest
public class SQX_Test_8452_MedDev {
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
     * GIVEN: MedDev record with various report types
     * WHEN: Save the record
     * THEN: Various field validation should work with valid/invalid data of different report type
     */
    public static testMethod void givenMedDevRecordWithVariousReportTypes_WhenSaveTheRecord_ThenVariousFieldValidationShouldWork(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            // Arrange: Create MedDev records with save results
            Map<SQX_MedDev__c, Boolean> medDevWithSaveResults = new Map<SQX_MedDev__c, Boolean>{
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Initial') => false,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Follow Up') => false,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Initial', 
                                    MfrInvestigation_MfrInitialCorrecAction__c = 'Initial corrective actions/preventive actions implemented by the manufacturer',
                                    MfrInvestigation_MfrPrelimAnalysis__c='Manufacturers preliminary analysis',
                                    Admin_Info_NCAName__c = 'Test NCA Name',
                                    Admin_Info_NCAAddress__c = 'Test Address',
                                    Admin_Info_ReportDate__c = Date.Today(),
                                    Admin_Info_MfrInternalNo__c = 'Num 12345',
                                    Admin_Info_Spht__c = 'Yes',
                                    Admin_Info_EventClassification__c = 'Death',
                                    Contact_Info__c = 'Manufacturer',
                                    Device_Info_DeviceClass__c = 'MDD Class III',
                                    Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                                    Admin_Info_MfrAwarenessDate__c = Date.Today(),
                                    MfrDetails_MfrName__c = 'Test Manufacturer Name',
                                    MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                                    MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                                    MfrDetails_MfrPostcode__c = 'BS 4519',
                                    MfrDetails_MfrCity__c = 'Test City',
                                    MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                                    MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                                    MfrDetails_MfrCountry__c = 'CA',
                                    ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                                    ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                                    ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                                    ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                                    ReporterDetails_ReporterOrgCity__c = 'Test City',
                                    ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                                    ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                                    ReporterDetails_ReporterOrgCountry__c = 'CA',
                                    Other_Reporting_Info_Distribution_All__c = 'All EEA, Candidate Countries and Switzerland'                                    
                                     ) => true,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Follow Up', 
                                    MfrInvestigation_MfrInitialCorrecAction__c = 'Initial corrective actions/preventive actions implemented by the manufacturer',
                                    MfrInvestigation_MfrPrelimAnalysis__c='Manufacturers preliminary analysis',
                                    Admin_Info_NCAReportNo__c = 'RN000011',
                                    Admin_Info_NCAName__c = 'Test NCA Name',
                                    Admin_Info_NCAAddress__c = 'Test Address',
                                    Admin_Info_ReportDate__c = Date.Today(),
                                    Admin_Info_MfrInternalNo__c = 'Num 12345',
                                    Admin_Info_Spht__c = 'Yes',
                                    Admin_Info_EventClassification__c = 'Death',
                                    Contact_Info__c = 'Manufacturer',
                                    Device_Info_DeviceClass__c = 'MDD Class III',
                                    Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                                    Admin_Info_MfrAwarenessDate__c = Date.Today(),
                                    MfrDetails_MfrName__c = 'Test Manufacturer Name',
                                    MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                                    MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                                    MfrDetails_MfrPostcode__c = 'BS 4519',
                                    MfrDetails_MfrCity__c = 'Test City',
                                    MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                                    MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                                    MfrDetails_MfrCountry__c = 'CA',
                                    ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                                    ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                                    ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                                    ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                                    ReporterDetails_ReporterOrgCity__c = 'Test City',
                                    ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                                    ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                                    ReporterDetails_ReporterOrgCountry__c = 'CA',
                                    Other_Reporting_Info_DistribEUCandidates__c = 'HR') => true,
                        
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Combined initial and final') => false,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Final') => false,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Combined initial and final', 
                                    MfrInvestigation_MfrDeviceAnalysis__c  = 'manufacturer’s device analysis results',
                                    MfrInvestigation_MfrsFinalComments__c = 'Final comments from the manufacturer',
                                    MfrInvestigation_CorrectiveAction__c = 'Remedial action/corrective action/preventive action / Field Safety Corrective Action',
                                    Admin_Info_NCAReportNo__c = 'RN000011',
                                    Admin_Info_NCAName__c = 'Test NCA Name',
                                    Admin_Info_NCAAddress__c = 'Test Address',
                                    Admin_Info_ReportDate__c = Date.Today(),
                                    Admin_Info_MfrInternalNo__c = 'Num 12345',
                                    Admin_Info_Spht__c = 'Yes',
                                    Admin_Info_EventClassification__c = 'Death',
                                    Contact_Info__c = 'Manufacturer',
                                    Device_Info_DeviceClass__c = 'MDD Class III',
                                    Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                                    Admin_Info_MfrAwarenessDate__c = Date.Today(),
                                    MfrDetails_MfrName__c = 'Test Manufacturer Name',
                                    MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                                    MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                                    MfrDetails_MfrPostcode__c = 'BS 4519',
                                    MfrDetails_MfrCity__c = 'Test City',
                                    MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                                    MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                                    MfrDetails_MfrCountry__c = 'CA',
                                    ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                                    ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                                    ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                                    ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                                    ReporterDetails_ReporterOrgCity__c = 'Test City',
                                    ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                                    ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                                    ReporterDetails_ReporterOrgCountry__c = 'CA',
                                    MfrInvestigation_MfrAwareSimEvents__c = 'Yes',
                                    Other_Reporting_Info_CountriesSimEvents__c = 'Denmark, England',
                                    Other_Reporting_Info_DistributionEEA__c = 'EE') => true,
                    new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                    Admin_Info_ReportType__c = 'Final', 
                                    MfrInvestigation_MfrDeviceAnalysis__c  = 'manufacturer’s device analysis results',
                                    MfrInvestigation_MfrsFinalComments__c = 'Final comments from the manufacturer',
                                    MfrInvestigation_CorrectiveAction__c = 'Remedial action/corrective action/preventive action / Field Safety Corrective Action',
                                    Admin_Info_NCAReportNo__c = 'RN000011',
                                    Admin_Info_NCAName__c = 'Test NCA Name',
                                    Admin_Info_NCAAddress__c = 'Test Address',
                                    Admin_Info_ReportDate__c = Date.Today(),
                                    Admin_Info_MfrInternalNo__c = 'Num 12345',
                                    Admin_Info_Spht__c = 'Yes',
                                    Admin_Info_EventClassification__c = 'Death',
                                    Contact_Info__c = 'Manufacturer',
                                    Device_Info_DeviceClass__c = 'MDD Class III',
                                    Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                                    Admin_Info_MfrAwarenessDate__c = Date.Today(),
                                    MfrDetails_MfrName__c = 'Test Manufacturer Name',
                                    MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                                    MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                                    MfrDetails_MfrPostcode__c = 'BS 4519',
                                    MfrDetails_MfrCity__c = 'Test City',
                                    MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                                    MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                                    MfrDetails_MfrCountry__c = 'CA',
                                    ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                                    ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                                    ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                                    ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                                    ReporterDetails_ReporterOrgCity__c = 'Test City',
                                    ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                                    ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                                    ReporterDetails_ReporterOrgCountry__c = 'CA',
                                    MfrInvestigation_MfrAwareSimEvents__c = 'Yes',
                                    Other_Reporting_Info_CountriesSimEvents__c = 'Denmark') => true
                        
            };
            
            // Act:
            // Assert: ensure that test results match with expected data when saving
            actAndAssertMedDevRecords(medDevWithSaveResults);
        }
    }
    
    /**
     * GIVEN: MedDev record with values selected in country fields (Section 11: within the EEA and Switzerland and Turkey and All EEA, candidate countries and Switzerland and Turkey)
     * WHEN: Attempt to Validate and Save the record
     * THEN: Field validation should prevent to save the record
     */
    public static testmethod void givenMedDevRecordWithValuesInCountryFields_WhenAttemptToSaveAndVlidateRecord_ThenFieldValidationShouldPreventToSaveTheRecord(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            
            // ARRANGE: Create MedDev record with values in the fields: within the EEA and Switzerland and Turkey and All EEA, candidate countries and Switzerland and Turkey
            SQX_MedDev__c medDevRecord = new SQX_MedDev__c(Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE,
                                                     Admin_Info_ReportType__c = 'Final', 
                                                     MfrInvestigation_MfrDeviceAnalysis__c  = 'manufacturer’s device analysis results',
                                                     MfrInvestigation_MfrsFinalComments__c = 'Final comments from the manufacturer',
                                                     MfrInvestigation_CorrectiveAction__c = 'Remedial action/corrective action/preventive action / Field Safety Corrective Action',
                                                     Admin_Info_NCAReportNo__c = 'RN000011',
                                                     Admin_Info_NCAName__c = 'Test NCA Name',
                                                     Admin_Info_NCAAddress__c = 'Test Address',
                                                     Admin_Info_ReportDate__c = Date.Today(),
                                                     Admin_Info_MfrInternalNo__c = 'Num 12345',
                                                     Admin_Info_Spht__c = 'Yes',
                                                     Admin_Info_EventClassification__c = 'Death',
                                                     Contact_Info__c = 'Manufacturer',
                                                     Device_Info_DeviceClass__c = 'MDD Class III',
                                                     Clinical_Event_Info_EventDescription__c = 'Incident description narrative',
                                                     Admin_Info_MfrAwarenessDate__c = Date.Today(),
                                                     MfrDetails_MfrName__c = 'Test Manufacturer Name',
                                                     MfrDetails_MfrContactName__c = 'Test Manufacturer Contact Name',
                                                     MfrDetails_MfrAddress__c = 'Test Manufacturer Address',
                                                     MfrDetails_MfrPostcode__c = 'BS 4519',
                                                     MfrDetails_MfrCity__c = 'Test City',
                                                     MfrDetails_MfrPhone__c = '+49 1234 56 78-0',
                                                     MfrDetails_MfrEmailAddress__c = 'testmfremail@test.com',
                                                     MfrDetails_MfrCountry__c = 'CA',
                                                     ReporterDetails_ReporterOrgName__c = 'Test Reporter Name',
                                                     ReporterDetails_ReporterContactPerson__c = 'Test Reporter Contact Name',
                                                     ReporterDetails_ReporterOrgAddress__c = 'Test Reporter Address',
                                                     ReporterDetails_ReporterOrgPostcode__c = 'AB 8912',
                                                     ReporterDetails_ReporterOrgCity__c = 'Test City',
                                                     ReporterDetails_ReporterOrgPhone__c = '+49 1234 56 78-0',
                                                     ReporterDetails_ReporterOrgEmail__c = 'testmfremail@test.com',
                                                     ReporterDetails_ReporterOrgCountry__c = 'CA',
                                                     MfrInvestigation_MfrAwareSimEvents__c = 'Yes',
                                                     Other_Reporting_Info_CountriesSimEvents__c = 'Denmark',
                                                     Other_Reporting_Info_DistributionEEA__c = 'EE',
                                                     Other_Reporting_Info_Distribution_All__c = 'All EEA, Candidate Countries and Switzerland');
            
            // ACT: Save the MedDev record
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_MedDev__c>{medDevRecord}, new List<Schema.SObjectField>{});
            
            // ASSERT: Ensure field validation prevent to save the MedDev record
            System.assertEquals(false, result[0].isSuccess(), 'MedDev record should not be saved.' + result[0].getErrors());
            String expErrMsg = 'Specific countries are selected deselect those countries to select All EEA candidate countries and Switzerland and Turkey.';
            System.assert(SQX_Utilities.checkErrorMessage(result[0].getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result[0].getErrors());
        }
    }
    
    /**
     * Helper method to save (act) and assert MedDev records
     */ 
    private static void actAndAssertMedDevRecords(Map<SQX_MedDev__c, Boolean>  data) {
        
        // ACT: Insert the records
        SQX_MedDev__c[] medDevs = new List<SQX_MedDev__c>(data.keySet());
        Database.SaveResult[] saveResults = new SQX_DB().continueOnError().op_insert(medDevs, new List<Schema.SObjectField>{});
        data = data.clone();
        
        // Assert: Ensure that result matches
        for(Integer i = 0; i < saveResults.size(); i++) {
            SQX_MedDev__c medDev = medDevs[i];
            Database.SaveResult result = saveResults[i];
            
            System.assertEquals(data.get(medDev), result.isSuccess(), 'Failing on ' + medDev +  'Actual Result: ' + result);
        }
    }
}