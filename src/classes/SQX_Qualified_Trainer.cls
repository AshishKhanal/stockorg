/**
* class to hold the methods for the actions performed on trigger for qualified trainer object
*/
public with sharing class SQX_Qualified_Trainer{
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newTrainers equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Qualified_Trainer__c> newTrainers, Map<Id,SQX_Qualified_Trainer__c> oldMap){
            
            super(newTrainers, oldMap);
        }

        /**
        * prevent manipulation of qualified trainer when its related controlled document is in approval
        */
        public Bulkified preventManipulationOfQualifiedTrainerOnApproval(){
            
            this.resetView();
            List<SQX_Qualified_Trainer__c> trainerList = (List<SQX_Qualified_Trainer__c>) this.view;

            if(trainerList.size() > 0){
                Set<Id> docIds = getIdsForField(trainerList, Schema.SQX_Qualified_Trainer__c.SQX_Controlled_Document__c);
                Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Approval_Status__c FROM SQX_Controlled_Document__c WHERE Id IN: docIds]);

                for(SQX_Qualified_Trainer__c trainer : trainerList){
                    if(docMap.get(trainer.SQX_Controlled_Document__c).Approval_Status__c == SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL ||
                        docMap.get(trainer.SQX_Controlled_Document__c).Approval_Status__c == SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL){
                        trainer.addError(Label.SQX_ERR_MSG_PREVENT_QUALIFIED_TRAINER_MANIPULATION);
                    }
                }
            }
            return this;
        }
    }
}