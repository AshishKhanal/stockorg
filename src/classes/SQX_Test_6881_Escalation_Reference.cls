/**
 * This test class ensures linking of Finding and Nonconformance
 * to a Supplier Escalation records are unique
 * @Author: Dil Bahadur Thapa
 * @Date: 10/05/2018
 */
@isTest
public class SQX_Test_6881_Escalation_Reference {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * GIVEN: Supplier Escalation with Finding linked
     * WHEN: Same Finding record is attempted to link
     * THEN: Finding record should not be linked
     * @Story: [SQX-6881]
     */
    
    public static testmethod void givenSupplierEscalationWithFindingLinked_WhenSameFindingIsAttemptedToLink_ThenFindingShouldNotBeLinked(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            
            // create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
            
            // Get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // Create Supplier Escalation
            SQX_Test_Supplier_Escalation supplierEscalation = new SQX_Test_Supplier_Escalation(accounts[0]);
            supplierEscalation.save();
            
            // Create Finding
            SQX_Test_Finding finding = new SQX_Test_Finding();
            finding.save();
                        
            // ARRANGE: Link the created Supplier Escalation and Finding
            SQX_Escalation_Reference__c escRef = new SQX_Escalation_Reference__c();
            escRef.SQX_Supplier_Escalation__c = supplierEscalation.supplierEscalation.Id;
            escRef.SQX_Related_Finding__c = finding.finding.Id;
            
            Database.SaveResult insertResult = Database.insert(escRef);
            
            // ACT: Link same Supplier Escalation and Finding
            SQX_Escalation_Reference__c escRef2 = new SQX_Escalation_Reference__c();
            escRef2.SQX_Supplier_Escalation__c = supplierEscalation.supplierEscalation.Id;
            escRef2.SQX_Related_Finding__c = finding.finding.Id;
            Database.SaveResult duplicateInsertResult = Database.insert(escRef2, false);
            
            // ASSERT: Ensure duplicate link is not created
            System.assert(!duplicateInsertResult.isSuccess(), 'Expected update action to fail when Supplier Escalation and Finding combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(duplicateInsertResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + duplicateInsertResult.getErrors());
                                   
        }
    }
    
    /**
     * GIVEN: Supplier Escalation with NC linked
     * WHEN: Same NC record is attempted to link
     * THEN: NC record should not be linked
     * @Story: [SQX-6881]
     */
    
    public static testmethod void givenSupplierEscalationWithNCLinked_WhenSameNCIsAttemptedToLink_ThenNCShouldNotBeLinked(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            
            // create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
            
            // Get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // Create Supplier Escalation
            SQX_Test_Supplier_Escalation supplierEscalation = new SQX_Test_Supplier_Escalation(accounts[0]);
            supplierEscalation.save();
                        
            // Create NC
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.save();
                        
            // ARRANGE: Link the created Supplier Escalation and Finding
            SQX_Escalation_Reference__c escRef = new SQX_Escalation_Reference__c();
            escRef.SQX_Supplier_Escalation__c = supplierEscalation.supplierEscalation.Id;
            escRef.SQX_Related_Nonconformance__c = nc.nc.Id;
            
            Database.SaveResult insertResult = Database.insert(escRef);
            
            // ACT: Link same Supplier Escalation and Finding
            SQX_Escalation_Reference__c escRef2 = new SQX_Escalation_Reference__c();
            escRef2.SQX_Supplier_Escalation__c = supplierEscalation.supplierEscalation.Id;
            escRef2.SQX_Related_Nonconformance__c = nc.nc.Id;
            Database.SaveResult duplicateInsertResult = Database.insert(escRef2, false);
            
            // ASSERT: Ensure duplicate link is not created
            System.assert(!duplicateInsertResult.isSuccess(), 'Expected update action to fail when Supplier Escalation and Finding combo already exist.');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(duplicateInsertResult.getErrors(), 'duplicate value found'), 'expected duplicate error msg not found, actual errs: ' + duplicateInsertResult.getErrors());
                                   
        }
    } 
    
    /**
     * GIVEN: Supplier Escalation
     * WHEN: Same Supplier Escalation is referred in Related Escalation
     * THEN: Escalation Reference should not be created
     * @Story: [SQX-6881]
     */
    
    public static testmethod void givenSupplierEscalation_WhenSameEscalationRecordIsReferredInRelatedEscalation_ThenEscalationReferenceShouldNotBeCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            
            // create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
            
            // Get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // ARRANGE: Create Supplier Escalation
            SQX_Test_Supplier_Escalation supplierEscalation = new SQX_Test_Supplier_Escalation(accounts[0]);
            supplierEscalation.save();
            
            SQX_Test_Supplier_Escalation supplierEscalation2 = new SQX_Test_Supplier_Escalation(accounts[1]);
            supplierEscalation2.save();                       
            
                        
            // ACT: Link the same Supplier Escalation in Related Escalation
            SQX_Escalation_Reference__c escRef = new SQX_Escalation_Reference__c();
            escRef.SQX_Supplier_Escalation__c = supplierEscalation.supplierEscalation.Id;
            escRef.SQX_Related_Escalation__c = supplierEscalation.supplierEscalation.Id;
            
            Database.SaveResult insertResult = Database.insert(escRef, false);      
                       
            // ASSERT: Ensure duplicate link is not created
            System.assert(!insertResult.isSuccess(), 'Expected escalation reference should not be created.');
            System.assertEquals('Supplier Escalation and Related Escalation cannot refer same record.', insertResult.getErrors()[0].getMessage());
                                   
        }
    }
}