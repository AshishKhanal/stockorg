/**
* This test ensures that the added functionality in the sobjectdataloader to return the tree map works
*/
@isTest
private class SQX_Test_SObjectDataLoader {

    static final String STANDARD_USER_1 = 'standardUser1',
                        ADMIN_USER_1 = 'adminUser1';

    /**
    * Setup users and data to use in the test.
    */
    @testSetup
    static void setupTestData(){
        UserRole role = SQX_Test_Account_Factory.createRole();
        
        User standardUser1 =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER_1);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER_1);

    }

    /**
    * ensures that the returned tree map doesn't contain objects that are not being followed.
    */  
    @isTest static void givenARecordId_OnlyRelatedDataAreDeserialized() {
        //Arrange: Create a user and create two accounts, one with three contacts and the other with one
        User user = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER_1);
        System.runas(user){
            Account account1 = SQX_Test_Account_Factory.createAccount(),
                    account2 = SQX_Test_Account_Factory.createAccount();

            Contact contact1 = SQX_Test_Account_Factory.createContact(account1),
                    contact2 = SQX_Test_Account_Factory.createContact(account1),
                    contact3 = SQX_Test_Account_Factory.createContact(account1),
                    contact4 = SQX_Test_Account_Factory.createContact(account2),
                    contact5 = SQX_Test_Account_Factory.createContact(account2);


            //Act: Attempt to serialize the data and get the account1
            Map<Id, SObjectTreeNode> tree = new  Map<Id, SObjectTreeNode>();
            SObjectDataLoader.serialize(new Set<Id> {account1.Id}, new SObjectDataLoader.SerializeConfig()
                                                                          .followChild(Contact.AccountId), tree);

            SObjectTreeNode node = tree.get(account1.Id);

            //Assert: assert that node exists. Returned node is the main node.
            System.assertNotEquals(null, node); //node is not null
            System.assertEquals(account1.Id, node.data.get('Id')); //the returned node is the same as one requested
            System.assertEquals(3, node.childObjects.get('Contacts').size()); //all the related contacts were fetched

            System.assertEquals(null, tree.get(account2.Id)); //assert that nothing was fetched for the account we didn't mention
        }
    }


    /**
    * ensures that the note of the followed record is being returned correctly
    */
    @isTest static void givenARecordHasNote_ItIsDeserialized(){
        //Arrange: Create a user and create an account, with two contacts. One of the contact has a note and so does the account
        User user = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER_1);
        
        System.runas(user){
            Account account1 = SQX_Test_Account_Factory.createAccount();

            Contact contact1 = SQX_Test_Account_Factory.createContact(account1),
                    contact2 = SQX_Test_Account_Factory.createContact(account1);

            Note note1 = new Note(Title = 'Hello', Body = 'World', ParentId = contact1.Id),
                 note2 = new Note(Title = 'Hello2', Body = 'World', ParentId = account1.Id);


            insert new List<Note> {note1, note2};

            //Act: Attempt to serialize the data and get the account1 and its contact along with the notes
            Map<Id, SObjectTreeNode> tree = new  Map<Id, SObjectTreeNode>();
            SObjectDataLoader.serialize(new Set<Id> {account1.Id}, new SObjectDataLoader.SerializeConfig()
                                                                          .followChild(Contact.AccountId)
                                                                          .followNoteOf(contact1.getSObjectType())
                                                                          , tree);

            SObjectTreeNode node = tree.get(account1.Id);

            //Assert: assert that node exists. Returned node is the main node. Ensure that the note of account isn't returned
            //because we aren't following its note. Also, ensure that the note of the contact is being returned.
            System.assertNotEquals(null, node); //node is not null
            System.assertEquals(account1.Id, node.data.get('Id')); //the returned node is the same as one requested
            System.assertEquals(null, node.childObjects.get('Notes')); //no notes of account was fetched


            System.assertEquals(1, tree.get(contact1.Id).childObjects.get('Notes').size()); //note of contact was fetched

        }
    }


    /**
    * ensures that related fields are added and returned correctly
    */
    @isTest static void givenAPartHasPartFamilyLookup_ItIsDeserialized(){
        //Arrange: Add a part and its relaled fied part family
        User user = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        
        System.runas(user){

            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random_Part_Family');
            insert partFamily;

            SQX_Part__c part = new SQX_Part__c (Name = 'Random_Part',
                                                Part_Number__c = 'PART-001',
                                                Part_Family__c = partFamily.Id,
                                                Part_Risk_Level__c = 4);

            insert part;
            //Act: Attempt to serailize the related fields of the object to the related field
            Map<Id, SObjectTreeNode> tree = new  Map<Id, SObjectTreeNode>();
            SObjectDataLoader.serialize(new Set<Id> {part.Id}, new SObjectDataLoader.SerializeConfig()
                                                                          .getFieldsFor(SQX_Part_Family__c.getSObjectType(),
                                                                           new List<SObjectField> {SQX_Part_Family__c.Name, SQX_Part_Family__c.OwnerId})
                                                                          , tree);

            SObjectTreeNode node = tree.get(part.Id);
            SQX_Part__c fetchedPart = (SQX_Part__c) node.data;

            //Assert: All the added fields are returned
            System.assertEquals(partFamily.Name, fetchedPart.Part_Family__r.Name);
            System.assertEquals(user.Id, fetchedPart.Part_Family__r.OwnerId);


            tree = new  Map<Id, SObjectTreeNode>();
            SObjectDataLoader.serialize(new Set<Id> {part.Id}, new SObjectDataLoader.SerializeConfig()
                                                                          , tree);

            node = tree.get(part.Id);
            fetchedPart = (SQX_Part__c) node.data;

            //Assert: Name field of the related list is returned even when related fields are not added
            System.assertEquals(partFamily.Name, fetchedPart.Part_Family__r.Name);
            Boolean exceptionThrown = false;
            try{
                System.debug(fetchedPart.Part_Family__r.OwnerId);
            }
            catch(SObjectException ex){
                exceptionThrown = true;
            }
            System.assertEquals(true, exceptionThrown, 'Expected an exception to be thrown because sobject didn\'t have the field fetched');


        }
    }


    /**
    * Given: There is a record with 200 or more child records
    * When: SObjectDataLoader is invoked
    * Then: Data is fetched properly and no exception is thrown
    * @story SQX-2386
    */
    static testmethod void given200PlusChildRecord_ItIsStillDeserialized(){
        User user = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        
        System.runas(user){
            //Arrange: Create a CAPA with 200+ Notes
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility(user).save();

            // Add 200 + Notes in capa
            List<Note> notes = new List<Note>();
            for(Integer i = 0; i < 500; i++){
                Note note = new Note(ParentId = capa.capa.Id, Body= 'Body of CAPA Note - 1' + i,
                                    Title = 'Note-'  + i);
                notes.add(note);
            }

            insert notes;

            // Act: Retrieve the CAPA and related Object using extension CAPA
            SQX_Extension_CAPA capaExt = new SQX_Extension_CAPA(new Apexpages.StandardController(capa.capa));
            Boolean hasError = false;
            Exception error;
            try{
                capaExt.getCompleteDataJSON();
            }
            catch(Exception ex){
                hasError = true;
                error = ex;
            }

            // Assert: Ensure that no error was thrown
            System.assert(!hasError, 'Expected no error to occur but found ' + error);
            
        } 
    }


    /**
    *   Given : a list of sobjectfields(various display types) and a config instance with translation enabled
    *   When : field list strings based on the two configs are retrieved
    *   Then : Expected field names have been concatenated accordingly
    */
    static testmethod void givenSObjectFieldsAndVariousConfigurations_WhenFieldListStringIsRetrieved_ThenFieldsAreAddedAccordingly(){


        // Arrange1 : a list of sobjectfields containing picklist types too and a config instance
        List<Schema.SObjectField> sObjectFields = new List<Schema.SObjectField>
        {
            Schema.SQX_Controlled_Document__c.Document_Category__c, // picklist
            Schema.SQX_Controlled_Document__c.Document_Status__c, // picklist
            Schema.SQX_Controlled_Document__c.Title__c,
            Schema.SQX_Controlled_Document__c.LastViewedDate,
            Schema.SQX_Controlled_Document__c.LastReferencedDate
        };

        SObjectDataLoader.SerializeConfig config = new SObjectDataLoader.SerializeConfig().translateLabels();

        // Act1 : Retrieve field list string based on the config and the list of sobjectfields
        String fieldList = SObjectDataLoader.getFieldList(sObjectFields, config);


        // Assert1 : Expected picklist fields to be wrapped with toLabel() and other fields as such
        
        // assertion for translation supported field list
        Set<String> fieldNames = getFieldListFromString(fieldList);
       
        System.assertEquals(sObjectFields.size() - 2, fieldNames.size());   // -2 because of the omitted fields

        System.assert(!fieldNames.contains('LastViewedDate') && !fieldNames.contains('LastReferencedDate'), 'Expected LastViewedDate and LastRefrencedDate fields to be omitted');

        System.assert(fieldNames.contains('toLabel(Document_Category__c)'));

        System.assert(fieldNames.contains('toLabel(Document_Status__c)'));

        System.assert(fieldNames.contains('Title__c'));


        // Arrange2: For the same list of sobjectfields, creating another config with translation disabled

        config = new SObjectDataLoader.SerializeConfig();

        // Act2: Retrieve field list string based on the config and the list of sobjectfields
        fieldList = SObjectDataLoader.getFieldList(sObjectFields, config);


        // Assert2: Expected picklist fields to be as such without toLabel() function
        fieldNames = getFieldListFromString(fieldList);

        System.assertEquals(sObjectFields.size() - 2, fieldNames.size());   // -2 because of the omitted fields

        System.assert(fieldNames.contains('Document_Category__c'));

        System.assert(fieldNames.contains('Document_Status__c'));

        System.assert(fieldNames.contains('Title__c'));


        // Arrange3: a list of sobjectfields with reference fields
        sObjectFields = new List<Schema.SObjectField>
        {
            Schema.SQX_Controlled_Document__c.SQX_Business_Unit__c,
            Schema.SQX_Controlled_Document__c.SQX_Service__c
        };

        // adding desired reference field
        config = config.getFieldsFor(SQX_Standard_Service__c.getSObjectType(), SQX_Standard_Service__c.Description__c);

        // Act3: Retrieve field list string based on the config and the list of sobjectfields
        fieldList = SObjectDataLoader.getFieldList(sObjectFields, config);

        // Assert3: Expected additional fields of the referenced object to be added to the field list
        fieldNames = getFieldListFromString(fieldList);

        System.assertEquals(sObjectFields.size() * 2, fieldNames.size());   // * 2 because of the added field for each field

        System.assert(fieldNames.contains('SQX_Business_Unit__c'));

        System.assert(fieldNames.contains('SQX_Business_Unit__r.Name'));

        System.assert(fieldNames.contains('SQX_Service__c'));

        System.assert(fieldNames.contains('SQX_Service__r.Description__c'));
    }

    /**
     * Given : Create Response Inclusion Approval record with following relationship order (4 level of child relationship)
     *         SQX_Audit__c
     *              |--> SQX_Audit_Response__c
     *                      |-->SQX_Finding_Response__c
     *                              |--> SQX_Response_Inclusion__c
     *                                          |--> SQX_Resp_Inclusion__c
     *
     * When : serialize(set<Id>,SObjectDataLoader.SerializeConfig,Map<Id, SObjectTreeNode>) method is called passing audit id
     * Then : Child record SQX_Resp_Inclusion__c should be queried and returned.
     *                                       
     */
    public static testmethod void ensureFourLevelOfChildRelationshipWorksFine(){
        User user = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        
        System.runas(user){
            SQX_DB sqxDb = new SQX_DB();
            
            // Arrange : Create SQX_Resp_Inclusion_Approval__c record
            SQX_Test_Audit audit = new SQX_Test_Audit(user).save();
            SQX_Test_Finding finding = audit.addAuditFinding().setRequired(false, false, false, false, false)
                                                             .save();

            SQX_Audit_Response__c auditResponse = new SQX_Audit_Response__c( 
                                                        SQX_Audit__c = audit.audit.Id,
                                                        Audit_Response_Summary__c = 'Audit Response for finding'
                                                    );
            
            sqxDb.op_insert(new List<SQX_Audit_Response__c> {auditResponse}, new List<Schema.SObjectField> {
                                                                      SQX_Audit_Response__c.SQX_Audit__c,
                                                                      SQX_Audit_Response__c.Audit_Response_Summary__c
                                                                    });

            SQX_Finding_Response__c findingResponse = new SQX_Finding_Response__c(
                                                            SQX_Finding__c = finding.finding.Id,
                                                            SQX_Audit_Response__c = auditResponse.Id,
                                                            Response_Summary__c = 'Response for audit'
                                                        );
            
            sqxDb.op_insert(new List<SQX_Finding_Response__c> {findingResponse}, new List<Schema.SObjectField> {
                                                                      SQX_Finding_Response__c.SQX_Finding__c,
                                                                      SQX_Finding_Response__c.SQX_Audit_Response__c,
                                                                      SQX_Finding_Response__c.Response_Summary__c
                                                                    });

            SQX_Containment__c containment = new SQX_Containment__c(
                                                    SQX_Finding__c = finding.finding.Id,
                                                    Containment_Summary__c = 'Containment for finding',
                                                    Completion_Date__c = Date.today()
                                                );
            
            sqxDb.op_insert(new List<SQX_Containment__c> {containment}, new List<Schema.SObjectField> {
                                                                      SQX_Containment__c.SQX_Finding__c,
                                                                      SQX_Containment__c.Containment_Summary__c,
                                                                      SQX_Containment__c.Completion_Date__c
                                                                    });

            SQX_Response_Inclusion__c responseInclusion = new SQX_Response_Inclusion__c(
                                                                Type__c = 'Containment',
                                                                SQX_Containment__c = containment.Id,
                                                                SQX_Response__c = findingResponse.Id
                                                            );
            
            sqxDb.op_insert(new List<SQX_Response_Inclusion__c> {responseInclusion}, new List<Schema.SObjectField> {
                                                                      SQX_Response_Inclusion__c.Type__c,
                                                                      SQX_Response_Inclusion__c.SQX_Containment__c,
                                                                      SQX_Response_Inclusion__c.SQX_Response__c
                                                                    });

            SQX_Resp_Inclusion_Approval__c responseInclusionApproval = new SQX_Resp_Inclusion_Approval__c(
                                                                            SQX_Response_Inclusion__c = responseInclusion.Id,
                                                                            SQX_Actual_Approver__c = user.Id
                                                                        );
            
            sqxDb.op_insert(new List<SQX_Resp_Inclusion_Approval__c> {responseInclusionApproval}, new List<Schema.SObjectField> {
                                                                      SQX_Resp_Inclusion_Approval__c.SQX_Response_Inclusion__c,
                                                                      SQX_Resp_Inclusion_Approval__c.SQX_Actual_Approver__c
                                                                    });

            //Act: Attempt to serailize the related fields of the object to the related field
            Map<Id, SObjectTreeNode> tree = new  Map<Id, SObjectTreeNode>();
            SObjectDataLoader.serialize(new Set<Id> {audit.audit.Id}, new SObjectDataLoader.SerializeConfig()
                                                                        .followChild(SQX_Audit_Response__c.SQX_Audit__c)
                                                                        .followChild(SQX_Finding_Response__c.SQX_Audit_Response__c)
                                                                        .followChild(SQX_Response_Inclusion__c.SQX_Response__c)
                                                                        .followChild(SQX_Resp_Inclusion_Approval__c.SQX_Response_Inclusion__c)
                                                                  , tree);


            // Assert : Ensure the SQX_Response_Inclusion_Approvals__r related list occurs and thus Id of response inclusion approval should be as above created record.
            System.assertEquals(responseInclusionApproval.Id, tree.get(audit.audit.Id).childObjects.get('compliancequest__SQX_Audit_Responses__r').get(0).childObjects.get('compliancequest__SQX_Finding_Responses__r').get(0).childObjects.get('compliancequest__SQX_Response_Inclusions__r').get(0).childObjects.get('compliancequest__SQX_Response_Inclusion_Approvals__r').get(0).data.get('Id'));
        }
    }

    /**
     * Method to return proper(normalized space and without ns prefix) field names
     * @param fieldListString - comma concatenated fields string
    */ 
    static Set<String> getFieldListFromString(String fieldListString){
        Set<String> fieldNames = new Set<String>();
        for(String fieldName : fieldListString.split(',')){
            fieldNames.add(fieldName.normalizeSpace().replaceAll(SQX.NSPrefix, ''));
        }
        return fieldNames;
    }

    // DESERIALIZATION TESTS (FROM https://github.com/afawcett/apex-sobjectdataloader/blob/master/apex-sobjectdataloader/src/classes/SObjectDataLoaderTest.cls)

    /*
    * Donot save an record subcomponent if error occurs
    */
     @isTest(seeAllData=False)
     private static void processUnorderedRecordsinJsonTest()
    {
        String testObjectJson = '{"RecordSetBundles":[{"Records":[{"attributes":{"type":"Contact","url":"/services/data/v29.0/sobjects/Contact/003b000000NStFRAA1"},'+
        '"IsEmailBounced":false,"CurrencyIsoCode":"USD","HasOptedOutOfFax":false,"LastModifiedDate":"2014-03-01T12:32:28.000+0000","HasOptedOutOfEmail":false,'+
        '"LastName":"MyContact","Name":"MyContact","DoNotCall":false,"AccountId":"001b000000P8LZgAAN","SystemModstamp":"2014-03-01T12:32:28.000+0000","CreatedDate":"2014-03-01T12:11:58.000+0000",'+
        '"IsDeleted":false,"Id":"003b000000NStFRAA1"}],"ObjectType":"Contact"},{"Records":[{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/001b000000P8LZgAAN"},'+
        '"Name":"ChildAccount","ParentId":"001b000000PBLqsAAH","CurrencyIsoCode":"USD","SystemModstamp":"2014-03-06T06:04:38.000+0000","CreatedDate":"2014-03-01T11:42:34.000+0000",'+
        '"LastModifiedDate":"2014-03-06T06:04:38.000+0000","IsDeleted":false,"Id":"001b000000P8LZgAAN"},{"attributes":{"type":"Account","url":"/services/data/v29.0/sobjects/Account/001b000000PBLqsAAH"},'+
        '"Name":"Parent Account","CurrencyIsoCode":"USD","SystemModstamp":"2014-03-06T06:04:15.000+0000","CreatedDate":"2014-03-06T06:04:15.000+0000","LastModifiedDate":"2014-03-06T06:04:15.000+0000",'+
        '"IsDeleted":false,"Id":"001b000000PBLqsAAH"}],"ObjectType":"Account"}]}';

        if(!UserInfo.IsMultiCurrencyOrganization())
        {
            testObjectJson = testObjectJson.remove('"CurrencyIsoCode":"USD",');
        }

         //Importing Records
        Set<Id> resultIds = SObjectDataLoader.deserialize(testObjectJson);
        List<Account> childAccountRecords = [select Id , ParentId from Account where Name = 'ChildAccount'];
        List<Account> parentAccountRecords = [select Id from Account where Name = 'Parent Account'];
        List<Contact> contactRecords = [select Id , AccountId from Contact where Name = 'MyContact'];

        system.assertEquals(childAccountRecords.size(), 1);
        system.assertEquals(parentAccountRecords.size(), 1);
        system.assertEquals(contactRecords.size(), 1);
        system.assertEquals(parentAccountRecords[0].Id, childAccountRecords[0].ParentId);
        system.assertEquals(childAccountRecords[0].Id,contactRecords[0].AccountId);
    }

    @isTest(seeAllData=False)
    private static void serializeIdWithDifferentSObjectTypes(){
        Account testAccount =  new Account(Name ='TestAccount');
        Opportunity testOpportunity = new Opportunity();
            testOpportunity.Name = 'TestOpportunity' ;
            testOpportunity.StageName = 'Open';
            testOpportunity.CloseDate = System.today();

        insert testAccount;
        insert testOpportunity;

        //Create Bundle with Account and Lead Objects as subcomponent
        Set<Id> objectIds = new Set<Id>();
        objectIds.add(testAccount.Id);
        objectIds.add(testOpportunity.Id);

        //Records are exported
        String serializedData = SObjectDataLoader.serialize(objectIds);

        delete testAccount;
        delete testOpportunity;

        Integer accountRecordsAfterDeletion = [Select count() from Account where Name = 'TestAccount'];
        system.assertEquals(accountRecordsAfterDeletion,0);
        Integer opportunityRecordsAfterDeletion = [Select count() from Opportunity where Name = 'TestOpportunity'];
        system.assertEquals(opportunityRecordsAfterDeletion,0);

        //Importing Records
        Set<Id> resultIds = SObjectDataLoader.deserialize(serializedData);
         Integer accountRecordsAfterImport = [Select count() from Account where Name = 'TestAccount'];
        system.assertEquals(accountRecordsAfterImport,1);
        Integer opportunityRecordsAfterImport = [Select count() from Opportunity where Name = 'TestOpportunity'];
        system.assertEquals(opportunityRecordsAfterImport,1);
    }

    /**
        --Without Blacklisting RecordTypeId, the autoconfig serialize/deserialize
            will try to insert a new RecordType object which throws:
            'System.TypeException: DML not allowed on RecordType'
        --Test uses dynamic binding to prevent compile-time errors in orgs without RecordTypes enabled
        --Currently, the test method only tests the logic if there are 2+ RecordTypes on the Account object
            otherwise, the if statement will silently ignore the rest of the testMethod.
    **/

    @isTest(seeAllData=False)
    private static void shouldNotTryToInsertRecordType(){
        List<RecordType> accountRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE sObjectType = 'Account' AND isActive = TRUE];
        //Only run this test if there are multiple active recordtypes on Account object
        if (accountRecordTypes.size() > 0){
            List<sObject> testAccounts = new List<Account>();
            for (RecordType aRT : accountRecordTypes){
                sObject testAccount = new Account(Name = 'Test' + aRT.DeveloperName);

                //dynamic binding will prevent any compile time errors if RecordTypeId field doesn't exist
                testAccount.put('RecordTypeId', aRT.Id);
                testAccounts.add(testAccount);
            }
            insert testAccounts;
            Set<Id> newAccountIds = new Set<Id>();
            for (sObject myAccount : testAccounts){
                newAccountIds.add(myAccount.Id);
            }
            String serializedData = SObjectDataLoader.serialize(newAccountIds);
            Set<Id> resultIds = SObjectDataLoader.deserialize(serializedData);

            //dynamic soql will prevent any compile time errors if RecordTypeId field doesn't exist
            String accountsQuery = 'SELECT Id, RecordTypeId FROM Account WHERE Id IN :resultIds';
            testAccounts = Database.query(accountsQuery);
            Set<Id> recordTypeIdsOfNewAccounts = new Set<Id>();

            for (sObject myAccount : testAccounts){
                recordTypeIdsOfNewAccounts.add((Id) myAccount.get('RecordTypeId'));
            }
            system.assertEquals(recordTypeIdsOfNewAccounts.size(), accountRecordTypes.size());
        }
    }

    @isTest(seeAllData=False)
    private static void deserializingObjectsWithSelfRefernces(){
        Account testParentAccount = new Account(Name = 'ParentAccount');
        insert testParentAccount;
        Account childAccount = new Account();
        childAccount.ParentId =testParentAccount.Id;
        childAccount.Name = 'ChildAccount';
        insert childAccount;
        Set<Id> childAccountIds = new Set<Id>();
        childAccountIds.add(childAccount.Id);
        String serializedData = SObjectDataLoader.serialize(childAccountIds);
        Integer recordsBeforeDeletion = [Select count() from Account];
        List<Account> recordsToDelete =  new List<Account>();
        recordsToDelete.add(testParentAccount);
        recordsToDelete.add(childAccount);
        delete recordsToDelete;
        Integer recordsAfterDeletion = [Select count() from Account];
        system.assertEquals(recordsBeforeDeletion,recordsAfterDeletion+2);
        Set<Id> resultIds = SObjectDataLoader.deserialize(serializedData);
        List<Account> recordsAfterDeserialization =[Select Id,Name,ParentId from Account];
        system.assertEquals(recordsBeforeDeletion,recordsAfterDeserialization.size());
        Id parentRecordId;
        for(Account acc : recordsAfterDeserialization){
            if('childAccount'.equals(acc.Name)){
                parentRecordId = acc.ParentId;
                break;
            }
        }
        for(Account acc : recordsAfterDeserialization){
            if(parentRecordId!=null && acc.id ==parentRecordId){
                system.assertEquals(acc.Name,'ParentAccount');
                break;
            }
        }
    }

    @isTest(seeAllData=False)
    private static void deserializingObjectsWithoutSelfRefernces(){
        List<Account> AccountList = new List<Account>();
        Account testAccount1 =  new Account(Name ='TestAccount1');
        Account testAccount2 =  new Account(Name ='TestAccount1');
        Account testAccount3 =  new Account(Name ='TestAccount1');

        AccountList.add(testAccount1);
        AccountList.add(testAccount2);
        AccountList.add(testAccount3);
        insert AccountList;

        //Create Bundle with Account Objects as subcomponent
        Set<Id> AccountIds = new Set<Id>();
        AccountIds.add(testAccount1.Id);
        AccountIds.add(testAccount2.Id);
        AccountIds.add(testAccount3.Id);
        //Records are exported
        String serializedData = SObjectDataLoader.serialize(AccountIds);

        Integer recordsBeforeDeletion = [Select count() from Account];
        List<Account> recordsToDelete =  new List<Account>();
        recordsToDelete.add(testAccount1);
        recordsToDelete.add(testAccount2);
        recordsToDelete.add(testAccount3);
        delete recordsToDelete;

        Integer recordsAfterDeletion = [Select count() from Account];
        system.assertEquals(recordsBeforeDeletion,recordsAfterDeletion+3);

        //Importing Records
        Set<Id> resultIds = SObjectDataLoader.deserialize(serializedData);
        List<Account> recordsAfterDeserialization =[select a.ParentId, a.Id from Account a where a.ParentId  =null];
        system.assertEquals(recordsBeforeDeletion,recordsAfterDeserialization.size());
    }

    /**
    *   Given : SObject having some fields with same api name
    *   When : Retrieve listed fields of that SObject
    *   Then : All fields should be retrieved
    */
    static testmethod void givenSObjectHavingSomeFieldsWithSameAPIName_WhenRetrieveListedFieldsOfThatSObject_ThenAllFieldsShouldBeRetrieved(){

        // Arrange : NC Objects having two fields: Closure Comment and Department with same API name 
        List<Schema.SObjectField> sObjectFields = new List<Schema.SObjectField>
        {
            Schema.SQX_Nonconformance__c.Closure_Comment__c,
            Schema.SQX_Nonconformance__c.Closure_Comment__c,
            Schema.SQX_Nonconformance__c.Department__c,
            Schema.SQX_Nonconformance__c.Department__c,
            Schema.SQX_Nonconformance__c.Description__c,
            Schema.SQX_Nonconformance__c.Priority__c
        };
            
        SObjectDataLoader.SerializeConfig config = new SObjectDataLoader.SerializeConfig();

        // Act : Retrieve fields of NC Object
        String fieldList = SObjectDataLoader.getFieldList(sObjectFields, config);
        
        String expectedFieldList = 'compliancequest__Closure_Comment__c,Closure_Comment__c,compliancequest__Department__c,Department__c,compliancequest__Description__c,compliancequest__Priority__c';

        // Assert : Ensure all listed fields of NC object is retrieved
        System.assertEquals(expectedFieldList, fieldList, 'Retrieved field list does not contains all the expected fields');

    }
}