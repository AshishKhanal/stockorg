/**
* The class is used as a paramter to various flows that will help in cloning records
*/
global class SQX_Create_Report_Configuration {
    
    /*
    * Holds record id of the regulatory report for which the report is being created.
    */
    @InvocableVariable(label='Report Id' description='Contains record id of the regulatory report for which the report is being created' required=true)
	global String reportId;
    
    /*
    * Holds id of the initial regulatory report for the final report being created.
    */
    @InvocableVariable(label='Initial Report Id' description='Contains id of the initial regulatory report for the final report being created')
	global String initialReportId;
}