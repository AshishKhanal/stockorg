/**
 * unit test for SQX_Picklist_Provider
 */
@IsTest
public class SQX_Test_Picklist_Provider {
    final static String PICKLIST_CATEGORY = 'category1';
    /**
     * WHEN: when getPicklistItems method is called
     * THEN: picklist values are retrieved
     */ 
    public  testmethod static void esurePickListAreProvided(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser){
            //Arrange: Picklist values are created without parent with Active true.
            SQX_Picklist_Value__c pv = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY, Active__c = true);
            insert pv;
            
            //Arrange: Picklist values are created without parent with Active false.
            SQX_Picklist_Value__c pv2 = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY, Active__c = false);
            insert pv2;
            
            //Act: Method is called with blank parent
            list<SQX_Picklist_Value__c> lists1 = SQX_Picklist_Provider.getPicklistItems(PICKLIST_CATEGORY, '');
            
            //Assert: two picklist values are provided.
            System.assertEquals(2, lists1.size(), 'Could not retrive pick list values');
            for(SQX_Picklist_Value__c p : lists1) {
                System.assertNotEquals(pv2.Id, p.Id, 'Inactive picklist values should not be retrived');
                System.assert(p.Id == null || p.Active__c == true, 'Picklist value is not active: ' + p);
            }
            
            //Act: Remote Method is called
            list<SQX_Picklist_Value__c> lists2 = SQX_Extension_UI.getPicklistValues(1, null, PICKLIST_CATEGORY);
            
            //Assert: One picklist values are provided.
            System.assertEquals(1, lists2.size(), 'Could not retrive pick list values');
            for(SQX_Picklist_Value__c p : lists2) {
                System.assertNotEquals(pv2.Id, p.Id, 'Inactive picklist values should not be retrived');
            }
            
            //Arrange: Picklist values are created with parent and Active false.
            SQX_Picklist_Value__c pv1 = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY, Parent__c = pv.Id, Active__c = false);
            insert pv1;
            
            //Arrange: Picklist values are created with parent and Active true.
            SQX_Picklist_Value__c pv3 = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY, Parent__c = pv.Id, Active__c = true);
            insert pv3;
            
            //Act: Method is called with parent
            list<SQX_Picklist_Value__c> lists3 = SQX_Picklist_Provider.getPicklistItems(PICKLIST_CATEGORY, pv.Id);
            
            //Assert: two picklist values are provided.
            System.assertEquals(2, lists3.size(), 'Could not retrive pick list values');
            for(SQX_Picklist_Value__c p : lists3) {
                System.assertNotEquals(pv1.Id, p.Id, 'Inactive picklist values should not be retrived');
                System.assert(p.Id == null || p.Active__c == true, 'Picklist value is not active: ' + p);
            }
            
            //Act: Remote Method is called
            list<SQX_Picklist_Value__c> lists4 = SQX_Extension_UI.getPicklistValues(1, null, PICKLIST_CATEGORY);
            
            //Assert: Two picklist values are provided.
            System.assertEquals(2, lists4.size(), 'Could not retrive pick list values');
            for(SQX_Picklist_Value__c p : lists4) {
                System.assertNotEquals(pv1.Id, p.Id, 'Inactive picklist values should not be retrived');
            }
        }
    }
    
    /**
     * GIVEN : Picklist value is added
     * WHEN: Inactive picklist value is referred in parent picklist value
     * THEN: Error is thrown
     */ 
    public  testmethod static void givenPicklistValue_WhenInactiveValueIsReferred_ErrorIsThrown(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser){
            //Arrange: Inactive Picklist value is created
            SQX_Picklist_Value__c pv = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY);
            insert pv;
            
            // ACT : Picklist value is added referring to inactive picklist value
            SQX_Picklist_Value__c pv1 = new SQX_Picklist_Value__c(Name='abc', Value__c='abc', Category__c = PICKLIST_CATEGORY, Parent__c = pv.Id);
            Database.SaveResult result = Database.insert( pv1, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Selected Picklist Value is not active.'));

            // ACT : Parent Picklist value is made active
            pv.Active__c = true;
            update pv;

            result = Database.insert( pv1, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is not successful');
        }
    }   
}