/**
* this class is responsible for enforcing CUD and FLS rule defined in SF profiles and permission sets
* @author Pradhanta Bhandari
* @date 2014/6/26
*/
public with sharing class SQX_DB{
    
    /**
    * Stores the list of objects in ordered format so that it can be used, while matching errors other save result
    * when more than one type is being updated/inserted at once
    */
    public List<SObject> lastDMLObjects {get; private set; }
    
    /** DB operation enumerations */
    public enum DBOperation { Create, Remove, Modify }

    private static Map<Schema.SObjectType, Map<String, Schema.SObjectField>> cachedResults = new Map<Schema.SObjectType, Map<String, Schema.SObjectField>>();

    /**
    * This function returns the list of fields in an SObject.
    * we are caching field maps because they are counted as field describes and we only have 100 field describe limits as of SF 2014
    */
    public static Map<String, Schema.SObjectField> getFieldMapOf(Schema.SObjectType objectType){
        Map<String, Schema.SObjectField> fieldMap = cachedResults.get(objectType);
        if(fieldMap == null){
            fieldMap = objectType.getDescribe().fields.getMap();
            cachedResults.put(objectType, fieldMap);
        }

        System.assert(fieldMap != null, 'Could not find field map for object of type ' + objectType); //by now we should have the fieldmap

        return fieldMap;
    }

    
    /** flag to turn on and off */
    private boolean disableSharingRules;

    /** fail on error, i.e. true means all objects must be saved or else error occurs */
    private boolean failOnError;
    
    /**
    * default constructor for the class, sets the default value to follow(honour) SF sharing rules.
    * additionally sets fail on error to true, suggesting that none of the objects can result in an error 
    */
    public SQX_DB(){
        disableSharingRules = false; //default is with sharing
        failOnError = true; //default is first failure aborts transaction
    }
    
    /**
    * sets the current context to without sharing i.e. disable salesforce sharing rule
    */
    public SQX_DB withoutSharing(){
        disableSharingRules = true;
        
        return this;
    }
    
    /**
    * sets the current context to with sharing i.e. enforces salesforce sharing rule
    */
    public SQX_DB withSharing(){
        disableSharingRules = false;
        
        return this;
    }

    /**
    * causes the database operation to fail if any of the object causes an error
    */
    public SQX_DB stopOnError(){
        this.failOnError = true;
        return this;
    }

    /**
    * permits failure of single object
    */
    public SQX_DB continueOnError(){
        this.failOnError = false;
        return this;
    }

    /**
    * This method inserts the given list of objects and returns the saveresult.
    */
    public List<Database.SaveResult> op_insert(Map<Schema.SObjectType, List<SObject>> objectsToInsert, Map<Schema.SObjectType, List<Schema.SObjectField>> fieldsToSet){
        List<Database.SaveResult> result = null;

        List<SObject> allObjectsToInsert = new List<SObject>(); //this variable groups all the objectsToInsert into one so that a single query can be fired

        //for each object type ensure that we have proper create rules
        for(Schema.SObjectType objectType: objectsToInsert.keySet()){
            Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();

            if(objectDescribe.isCreateable() == false){
                //throw an exception
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_OBJ_TYPE_IS_NOT_CREATABLE_BY_THE_CURRENT_USER.Replace('{objectType}', objectType.getDescribe().getName()));
            }

            //ensure correct for field level security
            ensureFieldsAreWriteable(fieldsToSet.get(objectType), false); 

            allObjectsToInsert.addAll(objectsToInsert.get(objectType));
        }

        lastDMLObjects = allObjectsToInsert;
        //actual insertion
        if(allObjectsToInsert.size() > 0){
            if(this.disableSharingRules){
                result = new PrivilegeEscalator().op_insert(allObjectsToInsert, this.failOnError);
            }
            else{
                result =  Database.Insert(allObjectsToInsert, this.failOnError);
            }
        }

        return result;
    }

    /**
    * This method updates the given list of objects and returns the update result.
    */
    public List<Database.SaveResult> op_update(Map<Schema.SObjectType, List<SObject>> objectsToUpdate, Map<Schema.SObjectType, List<Schema.SObjectField>> fieldsToSet){
        List<Database.SaveResult> result = null;

        List<SObject> allObjectsToUpdate = new List<SObject>(); //this variable groups all the objectsToInsert into one so that a single query can be fired

        //for each object type ensure that we have proper create rules
        for(Schema.SObjectType objectType: objectsToUpdate.keySet()){
            Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();

            if(objectDescribe.isUpdateable() == false){
                //throw an exception
                throw new SQX_ApplicationGenericException(objectType + ' is not updateable by current user');
            }

            //ensure correct for field level security
            ensureFieldsAreWriteable(fieldsToSet.get(objectType), true);

            allObjectsToUpdate.addAll(objectsToUpdate.get(objectType)); //add to the overall things to update
        }

        lastDMLObjects = allObjectsToUpdate;
        //actual insertion
        if(allObjectsToUpdate.size() > 0){
            if(this.disableSharingRules){
                result = new PrivilegeEscalator().op_update(allObjectsToUpdate, this.failOnError);
            }
            else{
                result =  Database.Update(allObjectsToUpdate, this.failOnError);
            }
        }

        return result;
    }

    /**
    * This method delete the given list of objects and returns the delete result.
    */
    public List<Database.DeleteResult> op_delete(Map<Schema.SObjectType, List<SObject>> objectsToDelete){
        List<Database.DeleteResult> result = null;

        List<SObject> allObjectsToDelete = new List<SObject>(); //this variable groups all the objectsToInsert into one so that a single query can be fired

        //for each object type ensure that we have proper create rules
        for(Schema.SObjectType objectType: objectsToDelete.keySet()){
            Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();

            if(objectDescribe.isDeletable() == false){
                //throw an exception
                throw new SQX_ApplicationGenericException(objectType + ' is not deletable by current user');
            }

            //no fls here because it does not mean anything ?

            allObjectsToDelete.addAll(objectsToDelete.get(objectType));
        }

        lastDMLObjects = allObjectsToDelete;
        //actual insertion
        if(allObjectsToDelete.size() > 0){
            if(this.disableSharingRules){
                result = new PrivilegeEscalator().op_delete(allObjectsToDelete, this.failOnError);
            }
            else{
                result =  Database.Delete(allObjectsToDelete, this.failOnError);
            }
        }

        return result;
    }

    /**
    * This method upserts the given list of objects and returns the upsert result. Upserter is not yet supported because SF upsert doesn't like SObject and wants specific object types
    */
    public List<Database.UpsertResult> op_upsert(Map<Schema.SObjectType, List<SObject>> objectsToUpsert, Map<Schema.SObjectType, List<Schema.SObjectField>> fieldsToSet){

        throw new SQX_ApplicationGenericException('Not Implemented Exception');
    }

    /**
    * this method ensures that the object manipulation operation is CUD (Create, Update, Delete ) compliant
    */
    public boolean isCUDCompliant(SObjectType objectType, DBOperation operation){
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();

        return ( (operation == DBOperation.Modify && objectDescribe.isUpdateable()) || (operation == DBOperation.Create  && objectDescribe.isCreateable()) 
                 || (operation == DBOperation.Remove && objectDescribe.isDeletable() ));
    }

    /**
    * this method ensures that the fields are updateable or createable
    * @param fields the list of fields that are being manipulated
    * @param isUpdate <code>true</code if can update is to be checked, else <code>false</code> to check for insert/create
    * @return <code>true</code> if the current user can manipulate the fields
    *         <code>false</code> if the current user can't manipulate the fields
    */
    public boolean isFLSCompliant(List<SObjectField> fields, DBOperation operation){
        return ensureFieldsAreWriteable(fields, operation == DBOperation.Modify);
    }


    /**
    * this method ensures that all the fields that are to be set(update/created) are accessible to the current user
    */
    private boolean ensureFieldsAreWriteable(List<Schema.SObjectField> fieldsToSet, boolean isUpdate){
        boolean isFLSSafe = true;
        System.assert(fieldsToSet != null, 'No field information was passed when performing an insert,update or upsert');

        //for each fields in the fieldset ensure they are updateable
        for(Schema.SObjectField field : fieldsToSet){
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            /*
                Truth table for update against result returned from creatable and updateable (? => don't care conditions)
                isUpdate    isCreateable    isUpdateable    Result
                T           ?               T               T
                T           ?               F               F
                F           T               ?               T
                F           F               ?               F

                Simplified expression  ( isUpdate && isUpdateable ) || ( !isUpdate && isCreateable )  -> Valid condition
                Invalid condition : NOT ( ( isUpdate && isUpdateable ) || ( !isUpdate && isCreateable )   )    , demorgans law NOT(A OR B ) => (NOT A ) AND (NOT B)
                                  : NOT( ( isUpdate && isUpdateable ) ) && NOT( ( !isUpdate && isCreateable ) )
            */
            if( (isUpdate && fieldDescribe.isUpdateable()) == false && (!isUpdate && fieldDescribe.isCreateable()) == false){
                isFLSSafe = false;
                //if it is not updateable throw an Exception
                throw new SQX_ApplicationGenericException(fieldDescribe.getName() + ' is not writeable');
            }
        }

        return isFLSSafe;
    }

    /* ************************************** SHORT HAND METHOD *********************************************** */

    /**
    * this is a shorthand to calling the main function, this way we avoid littering the rest of the code with parameter re formatting code.
    */
    public List<Database.SaveResult> op_update(List<SObject> objectsToUpdate, List<Schema.SObjectField> fieldsToSet){
        if(objectsToUpdate.size() == 0)
            return new List<Database.SaveResult>(); //return blank list when no objects are being sent

        Map<Schema.SObjectType, List<SObject>> mapOfObjectsToUpdate = new Map<Schema.SObjectType, List<SObject>>();
        Map<Schema.SObjectType, List<Schema.SObjectField>> mapOfFieldsToSet = new Map<Schema.SObjectType, List<Schema.SObjectField>>();
        Schema.SObjectType objectType = objectsToUpdate.get(0).getSObjectType(); //set the object type to sobject type of the first element

        //Consideration: Do we ensure that every object in the list of the same type ? For now lets save some clock cycles, by avoiding instance of calls

        //put the values
        mapOfObjectsToUpdate.put(objectType, objectsToUpdate);
        mapOfFieldsToSet.put(objectType, fieldsToSet);

        return op_update(mapOfObjectsToUpdate, mapOfFieldsToSet);

    }

    /**
    * shorthand for insertion
    */
    public List<Database.SaveResult> op_insert(List<SObject> objectsToInsert, List<Schema.SObjectField> fieldsToSet){
        if(objectsToInsert.size() == 0)
            return new List<Database.SaveResult>(); //return blank list when no objects are being sent

        Map<Schema.SObjectType, List<SObject>> mapOfObjectsToInsert = new Map<Schema.SObjectType, List<SObject>>();
        Map<Schema.SObjectType, List<Schema.SObjectField>> mapOfFieldsToSet = new Map<Schema.SObjectType, List<Schema.SObjectField>>();
        Schema.SObjectType objectType = objectsToInsert.get(0).getSObjectType(); //set the object type to sobject type of the first element

        //Consideration: Do we ensure that every object in the list of the same type ? For now lets save some clock cycles, by avoiding instance of calls

        //put the values
        mapOfObjectsToInsert.put(objectType, objectsToInsert);
        mapOfFieldsToSet.put(objectType, fieldsToSet);

        return op_insert(mapOfObjectsToInsert, mapOfFieldsToSet);

    }




    /**
    * shorthand for delete
    */
    public List<Database.DeleteResult> op_delete(List<SObject> objectsToDelete){
        if(objectsToDelete.size() == 0)
            return new List<Database.DeleteResult>(); //return blank list when no objects are being sent

        Map<Schema.SObjectType, List<SObject>> mapOfObjectsToDelete = new Map<Schema.SObjectType, List<SObject>>();
        Schema.SObjectType objectType = objectsToDelete.get(0).getSObjectType(); //set the object type to sobject type of the first element

        //Consideration: Do we ensure that every object in the list of the same type ? For now lets save some clock cycles, by avoiding instance of calls

        //put the values
        mapOfObjectsToDelete.put(objectType, objectsToDelete);

        return op_delete(mapOfObjectsToDelete);

    }
    
    /**
    * performs database operations in without sharing context. This class inherently doesn't do anything lookout for classes using .withoutSharing(). in all Apex classes
    * that is where the reason behind using without sharing is provided
    */
    private without sharing class PrivilegeEscalator{
        public List<Database.SaveResult> op_insert(List<SObject> objectsToInsert, boolean stopOnError){
            return Database.Insert(objectsToInsert, stopOnError);
        }
        
        public List<Database.SaveResult> op_update(List<SObject> objectsToUpdate, boolean stopOnError){
            return Database.Update(objectsToUpdate, stopOnError);
        }
        
        public List<Database.DeleteResult> op_delete(List<SObject> objectsToDelete, boolean stopOnError){
            return Database.Delete(objectsToDelete, stopOnError);
        }
        
        public List<Database.UpsertResult> op_upsert(List<SObject> objectsToUpsert, boolean stopOnError){
            return Database.Upsert(objectsToUpsert, stopOnError);
        }
        
    }
}