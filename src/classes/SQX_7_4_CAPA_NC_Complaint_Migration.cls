/**
* Batch processor to fix data of nc, capa and complaint
* Added in version  : 7.4.0
* Used for versions : prior to 7.2
*/
global with sharing class SQX_7_4_CAPA_NC_Complaint_Migration implements Database.Batchable<SObject> {

    public static final String MIGRATION_FAILED = '7.4 Migration Failed';
    public static final String MIGRATION_COMPLETED = '7.4 Migration Completed';
    
    List<String> objectList = new List<String> {'SQX_CAPA__c', 'SQX_Nonconformance__c', 'SQX_Complaint__c'};
    List<String> fieldNames = new List<String> {
                'Has_Response__c', 'Response_Required__c', 'Completion_Date_Response__c', 'Due_Date_Response__c', 'Completion_Date_Containment__c',
                'Completion_Date_Investigation__c', 'Containment_Required__c', 'Corrective_Action_Required__c', 'Description__c', 'Done_Responding__c',
                'Due_Date_Containment__c', 'Due_Date_Investigation__c', 'Due_Date__c', 'Has_Approved_Investigation__c', 'Has_Containment__c', 
                'Has_Corrective_Action__c', 'Has_Investigation__c', 'Has_Preventive_Action__c', 'Investigation_Approval__c', 'Investigation_Required__c',
                'Preventive_Action_Required__c', 'Probability__c', 'QMS_Reference_Number__c',
                'Recommended_Action__c', 'Risk_Justification__c', 'SQX_Investigation_Approver__c', 'SQX_Part__c', 'Severity__c'
                };
                    
    Map<SObjectType, String> sObjectTypeToRelationshipName = new Map<SObjectType, String> {
           Schema.SQX_Impacted_Product__c.SObjectType => 'SQX_Impacted_Products__r',
           Schema.SQX_Finding_Response__c.SObjectType => 'SQX_Responses__r',
           Schema.SQX_Containment__c.SObjectType => 'SQX_Containments__r',
           Schema.SQX_Investigation__c.SObjectType => 'SQX_Investigations__r',
           Schema.SQX_Impacted_Site__c.SObjectType => 'SQX_Impacted_Sites__r',
           Schema.SQX_Defect__c.SObjectType => 'SQX_Defects__r'    
           };
                                
    // default index of process object should be 0
    private Integer processObjectIndex = 0;
    
    private String obectType = '';
    
    private List<SQX_Finding_Response__c> findingResponsesToUpdate;
    private List<SQX_Investigation__c> investigationsToUpdate;
    private List<SQX_Containment__c> containmentsToUpdate;
    
    // this map will support for additional fields created in client org
    private Map<SObjectType, Map<String, String>> sObjectTypeToFieldsMap { get; set; }

    // error message which will give detail about where the error actually occurs
    private String errorMessage { get; set;}
    
    global SQX_7_4_CAPA_NC_Complaint_Migration(Integer objectIndex, Map<SObjectType, Map<String, String>> objectTypeToFieldsMap){
        if (objectIndex < objectList.size() && processObjectIndex >=0) {
            processObjectIndex = objectIndex;
            this.sObjectTypeToFieldsMap = objectTypeToFieldsMap;
        }else{

            // throw exception if index is invalid
            throw new SQX_ApplicationGenericException('Object index out of range. Please select 0 for CAPA, 1 for NC and 2 for Complaint');
        }
    }
    
    /**
    * Gets invoked when the batch job starts and selects all CAPA.NC and Complaint records
    * @param bc reference to the Database.BatchableContext object
    * @return Database.QueryLocator object that contains capa, nc and complaint passed to the job
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        Database.QueryLocator sobjectRecordList= Database.getQueryLocator('SELECT Id, SQX_Finding__c FROM '+ objectList.get(processObjectIndex) + ' WHERE Batch_Job_Status__c !=: MIGRATION_COMPLETED');
        return sobjectRecordList;
    }
    
    /** 
    * Execute the method to set new CAPA, NC ,Complaint fields to old finding fields
    * @param bc reference to the Database.BatchableContext object
    * @param scope list of sObjects containing capa, nc and complaint
    */
    global void execute(Database.BatchableContext bc, List<SObject> sobjectRecordList) {
        
        // Throw error if the batch size is more than 1
        System.assert(sobjectRecordList.size() == 1, 'This migration script is designed to process only 1 record at a time, please reduce the batch size');
        
        SQX_DB sqxDb = new SQX_DB();
        SavePoint sp = Database.setSavePoint();
        SObject mainRecord = sobjectRecordList[0];
        SObject mainRecordWithError; // used to update with migration failed fields if any error occurs due to migration
        List<SObjectField> sobjectFields; // will hold the sobjectfield (i.e. Batch_Job_Status__c and Batch_Job_Error__c) of respective main record being processed
        try {
            if (objectList.get(processObjectIndex) == 'SQX_CAPA__c') {
                
                // processing for capa record migration
                sobjectFields = new List<SObjectField> {SQX_CAPA__c.Batch_Job_Status__c, SQX_CAPA__c.Batch_Job_Error__c};
                mainRecordWithError = new SQX_CAPA__c(Id = mainRecord.Id);
                
                processCAPA((SQX_CAPA__c)mainRecord, sqxDb);

            } else if (objectList.get(processObjectIndex) == 'SQX_Nonconformance__c') {
                
                // processing for nc record migration
                sobjectFields = new List<SObjectField> {SQX_Nonconformance__c.Batch_Job_Status__c, SQX_Nonconformance__c.Batch_Job_Error__c};
                mainRecordWithError = new SQX_Nonconformance__c(Id = mainRecord.Id);
                processNC((SQX_Nonconformance__c)mainRecord, sqxDb);

            } else if (objectList.get(processObjectIndex) == 'SQX_Complaint__c') {
                
                // processing for complaint record migration
                sobjectFields = new List<SObjectField> {SQX_Complaint__c.Batch_Job_Status__c, SQX_Complaint__c.Batch_Job_Error__c};
                mainRecordWithError = new SQX_Complaint__c(Id = mainRecord.Id);
                processComplaint((SQX_Complaint__c)mainRecord, sqxDb);

            }
        } catch (Exception ex) {
            if (sp != null) {
                Database.rollBack(sp);
            }

            mainRecordWithError.put('Batch_Job_Status__c', MIGRATION_FAILED);
            mainRecordWithError.put('Batch_Job_Error__c', errorMessage + ' Exception: ' + ex.getMessage());
            sqxDb.op_update(new List<SObject> {mainRecordWithError}, sobjectFields);
        }
    }
    
    /**
    * This method update sets fields of defects
    */
    private void setDefects(List<SObject> defects, List<SObject> newDefectsToInsert, Id mainRecordId, String type, Map<Id, SObject> defectsToNewDefect) {
        for (SObject  defect : defects) {
            SObject newDefect;
            if(type == 'NC'){
                newDefect = new SQX_NC_Defect__c();
                newDefect.put('SQX_Nonconformance__c',mainRecordId);
                newDefect.put('SQX_Inspection_Detail__c',defect.get('SQX_Inspection_Detail__c'));
            }else if(type == 'CAPA'){
                newDefect = new SQX_CAPA_Defect__c();
                newDefect.put('SQX_CAPA__c',mainRecordId);
            }else if(type == 'COMPLAINT'){
                newDefect = new SQX_Complaint_Defect__c();
                newDefect.put('SQX_Complaint__c',mainRecordId);
            }
            newDefect.put('Actual__c',defect.get('Actual__c'));
            newDefect.put('Cost__c',defect.get('Cost__c'));
            newDefect.put('Defect_Category__c',defect.get('Defect_Category__c'));
            newDefect.put('Defect_Code__c',defect.get('Defect_Code__c'));
            newDefect.put('Defect_Description__c',defect.get('Defect_Description__c'));
            newDefect.put('Defective_Quantity__c',defect.get('Defective_Quantity__c'));
            newDefect.put('Lot_Ser_Number__c',defect.get('Lot_Ser_Number__c'));
            newDefect.put('Number_of_defects__c',defect.get('Number_of_defects__c'));
            newDefect.put('SQX_Defect_Code__c',defect.get('SQX_Defect_Code__c'));
            newDefect.put('SQX_Part__c',defect.get('SQX_Part__c'));
            newDefect.put('Severity__c',defect.get('Severity__c'));
            newDefect.put('Source__c',mainRecordId);
            newDefect.put('Specification__c',defect.get('Specification__c'));
            newDefect.put('Sublot_Number__c',defect.get('Sublot_Number__c'));
            
            // set additional fields too if any
            setAdditionalFields(defect, newDefect);
            
            newDefectsToInsert.add(newDefect);
            defectsToNewDefect.put(defect.Id, newDefect);
        }
    }
    
    /**
    * This method update containment
    */
    private void updateContainment(List<SQX_Containment__c> containments, String parentField, Id parentId, List<SQX_Containment__c> containmentsToUpdate) {
        for (SQX_Containment__c containment : containments) {
            containment.put(parentField, parentId);
        }
        containmentsToUpdate.addAll(containments);
    }
    
    /**
    * This method update investigation record
    */
    private void updateInvestigation(List<SQX_Investigation__c> investigations, String parentField, Id parentId, List<SQX_Investigation__c> investigationsToUpdate) {
        for (SQX_Investigation__c investigation : investigations) {
            investigation.put(parentField, parentId);
        }
        investigationsToUpdate.addAll(investigations);
    }
    
    /**
    * This method sets the common fields
    */
    private void setCommonFields(SObject mainRecord, SQX_Finding__c finding) {
        for (String fieldName : fieldNames) {
            mainRecord.put(fieldName, finding.get(fieldName));
        }
        
        // set additional fields too if any
        setAdditionalFields(finding, mainRecord);
    }
    
    /**
    * This method sets the additional fields that may be added in client org
    */
    private void setAdditionalFields(SObject oldRecord, SObject newRecord) {
        
        // adding additional fields
        if (sObjectTypeToFieldsMap != null) {
            Map<String, String> fieldsMaping = sObjectTypeToFieldsMap.get(oldRecord.Id.getSObjectType());            
            if (fieldsMaping != null) {
                for(String field : fieldsMaping.keySet()) {
                    newRecord.put(fieldsMaping.get(field), oldRecord.get(field));
                }
            }
        }
    }
    
    /**
    * This method used to process CAPA migration
    * @param 'objectList' list of CAPA records
    * @param 'SQX_DB' class reference
    */
    private void processCAPA(SQX_CAPA__c capa, SQX_DB sqxDb){
        
        Map<Id, SObject> defectsToNewDefect= new Map<Id, SObject>();
        SQX_Finding__c capaFinding = (SQX_Finding__c)getRecordsIncludingChildRecords(capa.SQX_Finding__c, sObjectTypeToRelationshipName);
        List<SQX_CAPA_Impacted_Site__c> capaImpactedSites = new List<SQX_CAPA_Impacted_Site__c>();
        List<SQX_CAPA_Defect__c> capaDefects = new List<SQX_CAPA_Defect__c>();
        
        findingResponsesToUpdate = new List<SQX_Finding_Response__c>();
        investigationsToUpdate = new List<SQX_Investigation__c>();
        containmentsToUpdate = new List<SQX_Containment__c>();
        obectType = SQX.getNSNameFor('SQX_CAPA__c');

        capa.Title__c = capaFinding.Title__c;
        capa.Create_New_Revision__c = false;
        capa.Closure_Comment__c = capaFinding.Closure_Comments__c;
        capa.SQX_Service__c = capaFinding.SQX_Service__c;
        capa.Operation__c = capaFinding.Operation__c;
        capa.Batch_Job_Status__c = MIGRATION_COMPLETED;
        capa.Batch_Job_Error__c = '';
        
        // set other fields, as nc and capa has many common fields so doing in common method
        setCommonFields(capa, capaFinding);
        
        // for each finding find SQX_Impacted_Sits__r and then for each SQX_Impacted_Site__c create SQX_CAPA_Impacted_Site__c
        List<SQX_Impacted_Site__c> impactedSites =  capaFinding.SQX_Impacted_Sites__r;
        for (SQX_Impacted_Site__c impactedSite : impactedSites) {
            SQX_CAPA_Impacted_Site__c capaImpactedSite = new SQX_CAPA_Impacted_Site__c(
                Business_Unit__c = impactedSite.Business_Unit__c,
                Department__c = impactedSite.Department__c,
                Division__c = impactedSite.Division__c,
                Operation__c = impactedSite.Operation__c,
                Production_Line__c = impactedSite.Production_Line__c,
                SQX_CAPA__c = capa.Id,
                Site__c = impactedSite.Site__c
            );
            
            // set additional impact site fields too if any
            setAdditionalFields(impactedSite, capaImpactedSite);
            
            capaImpactedSites.add(capaImpactedSite);
        }
        
        // for each finding find SQX_Defects__r and then for each SQX_Defect__c create SQX_CAPA_Defect__c
        setDefects(capaFinding.SQX_Defects__r, capaDefects, capa.Id, 'CAPA', defectsToNewDefect);
        
        // for each finding update SQX_Containment__c with SQX_CAPA__c
        updateContainment(capaFinding.SQX_Containments__r, obectType, capa.Id, containmentsToUpdate);
        
        // for each finding update SQX_Finding_Response__c
        updateResponse(capaFinding.SQX_Responses__r, obectType, capa.Id, findingResponsesToUpdate);
        
        // for each finding update SQX_Investigation__c
        updateInvestigation(capaFinding.SQX_Investigations__r, obectType, capa.Id, investigationsToUpdate);
             

        // insert capaImpactedSites
        errorMessage = 'Error occurred while processing capa impacted site records.';
        sqxDb.op_insert(capaImpactedSites, new List<Schema.SObjectField> {});
        
        // insert capaDefects
        errorMessage = 'Error occurred while processing capa defects records.';
        sqxDb.op_insert(capaDefects, new List<Schema.SObjectField> {}); 
        
        // insert attachment for capa defect
        errorMessage = 'Error occurred while processing attachment of capa defects records.';
        insertAttachments(defectsToNewDefect, sqxDb);
        
        // update response
        errorMessage = 'Error occurred while processing finding response records during capa processing.';
        sqxDb.op_update(findingResponsesToUpdate, new List<Schema.SObjectField> {});
        
        // update investigation too
        errorMessage = 'Error occurred while processing investigation records during capa processing.';
        sqxDb.op_update(investigationsToUpdate, new List<Schema.SObjectField> {});
        
        // update containments
        errorMessage = 'Error occurred while processing containments records during capa processing.';
        sqxDb.op_update(containmentsToUpdate, new List<Schema.SObjectField> {});
        
        // update capa
        errorMessage = 'Error occurred while processing capa records.';
        sqxDb.op_update(new List<SQX_CAPA__c> {capa}, new List<SObjectField>{});
    }
    
    /**
    * This method used to process NC migration
    * @param 'objectList' list of NC records
    * @param 'SQX_DB' class reference 
    */
    private void processNC(SQX_Nonconformance__c nc, SQX_DB sqxDb){
        Map<Id, SObject> defectsToNewDefect= new Map<Id, SObject>();
        SQX_Finding__c ncFinding = (SQX_Finding__c)getRecordsIncludingChildRecords(nc.SQX_Finding__c, sObjectTypeToRelationshipName);
        List<SQX_NC_Impacted_Product__c> ncImpactedProducts = new List<SQX_NC_Impacted_Product__c>();
        List<SQX_NC_Defect__c> ncDefects = new List<SQX_NC_Defect__c>();
        Map<Id, SQX_NC_Impacted_Product__c> impactedProductToNcImpactedProductMap = new Map<Id, SQX_NC_Impacted_Product__c>();
        
        findingResponsesToUpdate = new List<SQX_Finding_Response__c>();
        investigationsToUpdate = new List<SQX_Investigation__c>();
        containmentsToUpdate = new List<SQX_Containment__c>();
        obectType = SQX.getNSNameFor('SQX_Nonconformance__c');

        nc.NC_Title__c = ncFinding.Title__c;
        nc.Closure_Comment__c = ncFinding.Closure_Comments__c;
        nc.SQX_Service__c = ncFinding.SQX_Service__c;
        nc.Batch_Job_Status__c = MIGRATION_COMPLETED;
        nc.Batch_Job_Error__c = '';
        
        // set other fields, as nc and capa has many common fields so doing in common method
        setCommonFields(nc, ncFinding);
        
        // for each finding find SQX_Impacted_Products__r and then for each SQX_Impacted_Product__c create SQX_NC_Impacted_Product__c
        List<SQX_Impacted_Product__c> impactedProducts =  ncFinding.SQX_Impacted_Products__r;
        for (SQX_Impacted_Product__c impactedProduct : impactedProducts) {
            SQX_NC_Impacted_Product__c ncImpactedProduct = new SQX_NC_Impacted_Product__c(
                Lot_Number__c = impactedProduct.Lot_Number__c,
                Lot_Quantity__c = impactedProduct.Lot_Quantity__c,
                SQX_Nonconformance__c = nc.Id,
                SQX_Impacted_Part__c = impactedProduct.SQX_Impacted_Part__c,
                Sublot_Number__c = impactedProduct.Sublot_Number__c,
                Total_Quantity_Disposed__c = impactedProduct.Total_Quantity_Disposed__c,
                Uniqueness_Constraint__c = impactedProduct.Uniqueness_Constraint__c
            );
            
            // set additional for impact fields too if any
            setAdditionalFields(impactedProduct, ncImpactedProduct);
            
            impactedProductToNcImpactedProductMap.put(impactedProduct.Id, ncImpactedProduct); // ncImpactedProduct will posses its own id when it gets persisted
            ncImpactedProducts.add(ncImpactedProduct);
        }
        
        // for each finding find SQX_Defects__r and then for each SQX_Defect__c create SQX_NC_Defect__c
        setDefects(ncFinding.SQX_Defects__r, ncDefects, nc.Id, 'NC', defectsToNewDefect);
        
        // for each finding update SQX_Containment__c with SQX_NC
        updateContainment(ncFinding.SQX_Containments__r, obectType, nc.Id, containmentsToUpdate);
        
        // for each finding update SQX_Finding_Response__c
        updateResponse(ncFinding.SQX_Responses__r, obectType, nc.Id, findingResponsesToUpdate);
        
        // for each finding update SQX_Investigation__c
        updateInvestigation(ncFinding.SQX_Investigations__r, obectType, nc.Id, investigationsToUpdate);
            
        
        // Insert SQX_NC_Impacted_Product__c
        errorMessage = 'Error occurred while processing nc impacted product records.';
        sqxDb.op_insert(ncImpactedProducts, new List<Schema.SObjectField> {});

        // insert attachment for nc impacted product
        insertAttachments(impactedProductToNcImpactedProductMap, sqxDb);
        
        // Insert SQX_NC_Defect__c
        errorMessage = 'Error occurred while processing nc defects records.';
        sqxDb.op_insert(ncDefects, new List<Schema.SObjectField> {});
        
        // insert attachment of nc defect
        errorMessage = 'Error occurred while processing attachment of nc defect records.';
        insertAttachments(defectsToNewDefect, sqxDb);
        
        // update response(finding response)
        errorMessage = 'Error occurred while processing finding response records during nc processing.';
        sqxDb.op_update(findingResponsesToUpdate, new List<Schema.SObjectField> {});
        
        // update investigations
        errorMessage = 'Error occurred while processing investigation during nc processing.';
        sqxDb.op_update(investigationsToUpdate, new List<Schema.SObjectField> {});
        
        // update containments
        errorMessage = 'Error occurred while processing containment records during nc processing.';
        sqxDb.op_update(containmentsToUpdate, new List<Schema.SObjectField> {});
        
        // update disposition, to update disposition SQX_NC_Impacted_Product__c should be inserted already
        List<SQX_Disposition__c> dispositionsToUpdate = new List<SQX_Disposition__c>();
        List<SQX_Impacted_Product__c> impactedProductList = [SELECT Id, (SELECT Id, SQX_NC_Impacted_Product__c FROM SQX_Dispositions__r) FROM SQX_Impacted_Product__c WHERE Id IN : impactedProductToNcImpactedProductMap.keySet()];
        for (SQX_Impacted_Product__c impactedProduct : impactedProductList) {
            for (SQX_Disposition__c disposition : impactedProduct.SQX_Dispositions__r) {
                disposition.SQX_NC_Impacted_Product__c = impactedProductToNcImpactedProductMap.get(impactedProduct.Id).Id;
            }
            dispositionsToUpdate.addAll(impactedProduct.SQX_Dispositions__r);
        }
        
        // 
        errorMessage = 'Error occurred while processing disposition records during nc processing';
        sqxDb.op_update(dispositionsToUpdate, new List<Schema.SObjectField> {});
        
        // update nc
        errorMessage = 'Error occurred while processing nc records.';
        sqxDb.op_update(new List<SQX_Nonconformance__c> {nc}, new List<SObjectField>{});
    }
    
    /**
    * This method used to process Complaint migration
    * @param 'objectList' list of Complaint records
    * @param 'SQX_DB' class reference
    */
    private void processComplaint(SQX_Complaint__c complaint, SQX_DB sqxDb){
        Map<Id, SObject> defectsToNewDefect= new Map<Id, SObject>();
        SQX_Finding__c complaintFinding = (SQX_Finding__c)getRecordsIncludingChildRecords(complaint.SQX_Finding__c, sObjectTypeToRelationshipName);
        List<SQX_Complaint_Defect__c> complaintDefects = new List<SQX_Complaint_Defect__c>();
        
        investigationsToUpdate = new List<SQX_Investigation__c>();
        obectType = SQX.getNSNameFor('SQX_Complaint__c');
        
        complaint.Complaint_Title__c = complaintFinding.Title__c;
        complaint.Closure_Comment__c = complaintFinding.Closure_Comments__c;
        complaint.QMS_Reference_Number__c = complaintFinding.QMS_Reference_Number__c;
        complaint.Description__c = complaintFinding.Description__c;
        complaint.Batch_Job_Status__c = MIGRATION_COMPLETED;
        complaint.Batch_Job_Error__c = '';
        
        
        // for each finding find SQX_Defects__r and then for each SQX_Defect__c create SQX_Complaint_Defect__c
        setDefects(complaintFinding.SQX_Defects__r, complaintDefects, complaint.Id, 'COMPLAINT', defectsToNewDefect);
        
        // for each finding update SQX_Investigation__c
        updateInvestigation(complaintFinding.SQX_Investigations__r, obectType, complaint.Id, investigationsToUpdate);
        
        // Insert SQX_Complaint_Defect__c
        errorMessage = 'Error occurred while processing complaint defect records.';
        sqxDb.op_insert(complaintDefects, new List<Schema.SObjectField> {});
        
        // insert attachment for complaint defect
        errorMessage = 'Error occurred while processing attachment of complaint defect records.';
        insertAttachments(defectsToNewDefect, sqxDb);
        
        // update investigations
        errorMessage = 'Error occurred while processing investigation records during complaint processing.';
        sqxDb.op_update(investigationsToUpdate, new List<Schema.SObjectField> {});
        
        // update complaint
        errorMessage = 'Error occurred while processing complaint records.';
        sqxDb.op_update(new List<SQX_Complaint__c> {complaint}, new List<SObjectField>{});
    }
    
    /**
    * This method update response
    */
    private void updateResponse(List<SQX_Finding_Response__c> responses, String parentField, Id parentId, List<SQX_Finding_Response__c> findingResponsesToUpdate) {
        for (SQX_Finding_Response__c response : responses) {
            response.put(parentField, parentId);
        }
        findingResponsesToUpdate.addAll(responses);
        
    }
    
    /**
    * This method insert attachments from old object to new object (for e.g. defect to nc_defect, defect to capa defect, defect to complaint defect, impacted product to nc impacted product)
    */
    private void insertAttachments(Map<Id,Sobject> oldObjectIdToNewObjectMap, SQX_DB sqxDb){
        List<Attachment> attList=[SELECT Body, Description, ParentId, Name  FROM Attachment WHERE ParentID IN: oldObjectIdToNewObjectMap.keySet()];
        List<Attachment> aList=new  List<Attachment>();
        for(Attachment aObj : attList){
            Attachment obj = new Attachment(Id= null, Name =aObj.Name, Body = aObj.Body, Description = aObj.Description, ParentId = oldObjectIdToNewObjectMap.get(aObj.ParentId).Id);
            aList.add(obj);
        }
        sqxDb.op_insert(aList, new List<Schema.SObjectField> {}); 
    }
    
    /**
    * This method will return all fields along with child relationship dynamically of passed SObjectType
    */
    public SObject getRecordsIncludingChildRecords(Id recordId, Map<SObjectType, String> sObjectTypeToRelationshipName) {
        SObjectType sObjType = recordId.getSObjectType();
        String dynamicQuery = 'SELECT {!FieldList}, {!SubQuery}'
            + ' FROM ' + sObjType
            + ' WHERE Id = :recordId';
        
        String subQuery = '';
        for (SObjectType sType : sObjectTypeToRelationshipName.keySet()) {
            subQuery = subQuery == '' ? subQuery :  subQuery + ',';
            subQuery = subQuery + '(SELECT {!FieldList}'
                + ' FROM ' + sObjectTypeToRelationshipName.get(sType) + ')';
            
            String fieldList = SQX_DynamicQuery.getFieldList(sType.getDescribe().fields.getMap().values());
            subQuery = subQuery.replace('{!FieldList}', fieldList.trim());
        }
        dynamicQuery = dynamicQuery.replace('{!FieldList}', SQX_DynamicQuery.getFieldList(sObjType.getDescribe().fields.getMap().values()).trim());
        dynamicQuery = dynamicQuery.replace('{!SubQuery}', subQuery);
        
        System.debug('Querying for ' + dynamicQuery);
        return Database.query(dynamicQuery);
    }
    
    /** 
    * Used to execute post-processing operations and is called once after all batches are processed
    * @param bc reference to the Database.BatchableContext object
    */
    global void finish(Database.BatchableContext bc){}
}