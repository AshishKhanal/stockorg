/*
 * description: test for audit stage
 */
@IsTest
public class SQX_Test_3651_Audit_Stage{
    final static String RESPONSE_SUBMITTER = 'Std user 1',
                        ADMIN = 'Admin user 1';
    @testsetup
    static void setupData() {
        //Arrange: Create users
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN);
        User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, RESPONSE_SUBMITTER);
    }
    /**
    * helper funtion to setup Audit, Finding, Audit Response, Finding Response,Actions ,Investigations and Response Inclusions 
    * @param adminUser to create Test Audit and ResponseSubmitter for Auditee Contact 
    **/  
    public static List<Id> arrangeAudit(User adminUser, User responseSubmitter){
        /*
        * Arrange Audit as follows
        * Audit 1: Audit with
        *        'Open' Finding
        *        'Pending' Audit Response
        * Audit 2: Audit with
        *        'Complete' Finding
        *        'Pending' Audit Response
        *        'Open' Actions
        * Audit 3: Audit with
        *        'Complete' Finding
        *        'Pending' Audit Response
        *        'Open' Actions
        * Audit 4: Audit with
        *        'Open' Finding
        *        'Complete' Audit Response
        * Audit 5: Audit with
        *        'Complete' Finding
        *        'Complete' Audit Response
        *        'Complete' Actions
        * Audit 6: Audit with
        *        'Complete' Finding
        *        'Complete' Audit Response
        *         Actions with Null value as Status(Status is null when Implementation is Rejected)
        */

        SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
        audit1.audit.Title__c = 'Audit-1';
        audit1.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit1.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit1.setStage(SQX_Audit.STAGE_RESPOND);
        audit1.setPolicy(false, true, true);
        audit1.audit.Start_Date__c = Date.Today();
        audit1.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit2 = new SQX_Test_Audit(adminUser);
        audit2.audit.Title__c = 'Audit-2';
        audit2.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit2.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit2.setStage(SQX_Audit.STAGE_RESPOND);
        audit2.setPolicy(false, false, true);
        audit2.audit.End_Date__c = Date.Today().addDays(10);

        SQX_Test_Audit audit3 = new SQX_Test_Audit(adminUser);
        audit3.audit.Title__c = 'Audit-3';
        audit3.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit3.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit3.setStage(SQX_Audit.STAGE_RESPOND);
        audit3.setPolicy(false, false, true);
        audit3.audit.Start_Date__c = Date.Today();
        audit3.audit.End_Date__c = Date.Today().addDays(10);
        
        SQX_Test_Audit audit4 = new SQX_Test_Audit(adminUser);
        audit4.audit.Title__c = 'Audit-4';
        audit4.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit4.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit4.setStage(SQX_Audit.STAGE_RESPOND);
        audit4.setPolicy(false, true, true);
        audit4.audit.Start_Date__c = Date.Today();
        audit4.audit.End_Date__c = Date.Today().addDays(40);

        SQX_Test_Audit audit5 = new SQX_Test_Audit(adminUser);
        audit5.audit.Title__c = 'Audit-5';
        audit5.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit5.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit5.setStage(SQX_Audit.STAGE_RESPOND);
        audit5.setPolicy(false, true, true);
        audit5.audit.Start_Date__c = Date.Today();
        audit5.audit.End_Date__c = Date.Today().addDays(50);
        
		SQX_Test_Audit audit6 = new SQX_Test_Audit(adminUser);
        audit6.audit.Title__c = 'Audit-6';
        audit6.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
        audit6.setStatus(SQX_Audit.STATUS_COMPLETE);
        audit6.setStage(SQX_Audit.STAGE_RESPOND);
        audit6.setPolicy(false, true, true);
        audit6.audit.Start_Date__c = Date.Today();
        audit6.audit.End_Date__c = Date.Today().addDays(50);
        insert new List<SQX_Audit__c> {audit1.audit, audit2.audit, audit3.audit, audit4.audit, audit5.audit, audit6.audit };

        SQX_Test_Finding auditFinding1 =  audit1.addAuditFinding()
                                                .setRequired(true, true, true, false, false) 
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN);

        SQX_Test_Finding auditFinding2 =  audit2.addAuditFinding()
                                                .setRequired(false, false, false, false, false) 
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        SQX_Test_Finding auditFinding3 =  audit3.addAuditFinding()
                                                .setRequired(false, false, false, false, false) 
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        SQX_Test_Finding auditFinding4 =  audit4.addAuditFinding()
                                                .setRequired(true, true, true, false, false) 
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN);

        SQX_Test_Finding auditFinding5 =  audit5.addAuditFinding()
                                                .setRequired(true, true, true, false, false)
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);
        
       	SQX_Test_Finding auditFinding6 =  audit6.addAuditFinding()
                                                .setRequired(true, true, true, false, false)
                                                .setApprovals(true, false, false)
                                                .setStatus(SQX_Finding.STATUS_COMPLETE);

        insert new List<SQX_Finding__c> {auditFinding1.finding, auditFinding2.finding, auditFinding3.finding, auditFinding4.finding, auditFinding5.finding, auditFinding6.finding};

        SQX_Audit_Response__c auditResponse1 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit1.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 1',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_PENDING);

        SQX_Audit_Response__c auditResponse2 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit2.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 2',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        SQX_Audit_Response__c auditResponse3 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit3.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 3',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_PENDING);

        SQX_Audit_Response__c auditResponse4 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit4.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 4',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        SQX_Audit_Response__c auditResponse5 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit5.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 5',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);
        
        SQX_Audit_Response__c auditResponse6 = new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit6.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 6',
                                               Approval_Status__c = SQX_Audit_Response.APPROVAL_STATUS_NONE);

        insert new List<SQX_Audit_Response__c> { auditResponse1, auditResponse2, auditResponse3, auditResponse4, auditResponse5, auditResponse6 };

        SQX_Finding_Response__c findingResponse1 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding1.finding.Id,
                                               SQX_Audit_Response__c = auditResponse1.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse2 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding2.finding.Id,
                                               SQX_Audit_Response__c = auditResponse2.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse3 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding3.finding.Id,
                                               SQX_Audit_Response__c = auditResponse3.Id,
                                               Response_Summary__c = 'Response for audit',
                                               RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Response, SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITHOUT_FINDING));

        SQX_Finding_Response__c findingResponse4 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding4.finding.Id,
                                               SQX_Audit_Response__c = auditResponse4.Id,
                                               Response_Summary__c = 'Response for audit');

        SQX_Finding_Response__c findingResponse5 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding5.finding.Id,
                                               SQX_Audit_Response__c = auditResponse5.Id,
                                               Response_Summary__c = 'Response for audit');
        
        SQX_Finding_Response__c findingResponse6 = new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding6.finding.Id,
                                               SQX_Audit_Response__c = auditResponse6.Id,
                                               Response_Summary__c = 'Response for audit');

        insert new List<SQX_Finding_Response__c> { findingResponse1, findingResponse2, findingResponse3, findingResponse4, findingResponse5, findingResponse6 };

        SQX_Investigation__c investigation1 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding1');

        SQX_Investigation__c investigation4 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding4.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding4');

        SQX_Investigation__c investigation5 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding5.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding5');
        
        SQX_Investigation__c investigation6 = new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding6.finding.Id,
                                                Investigation_Summary__c = 'Investigation for finding6');

        insert new List<SQX_Investigation__c> { investigation1, investigation4, investigation5, investigation6 };

        SQX_Action__c action2 = new SQX_Action__c(SQX_Audit__c = audit2.audit.Id,
                                                    Plan_Number__c = 'PLAN-01',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(10),
                                                    Status__c = SQX_Implementation_Action.STATUS_OPEN);

        SQX_Action__c action3 = new SQX_Action__c(SQX_Audit__c = audit3.audit.Id,
                                                    Plan_Number__c = 'PLAN-03',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(10),
                                                    Status__c = SQX_Implementation_Action.STATUS_OPEN);

        SQX_Action__c action5 = new SQX_Action__c(SQX_Audit__c = audit5.audit.Id,
                                                    Plan_Number__c = 'PLAN-05',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(50),
                                                    Status__c = SQX_Implementation_Action.STATUS_COMPLETE);
        
        SQX_Action__c action6 = new SQX_Action__c(SQX_Audit__c = audit6.audit.Id,
                                                    Plan_Number__c = 'PLAN-06',
                                                    Plan_Type__c = 'Corrective',
                                                    Description__c = 'Corrective Action Plan Description',
                                                    Due_Date__c = Date.Today().addDays(50),
                                                    Status__c = '');

        insert new List<SQX_Action__c> { action2, action3, action5, action6};


        SQX_Response_Inclusion__c respInclusion1 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse1.Id,
                                                        SQX_Investigation__c = investigation1.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion3 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Action',
                                                        SQX_Response__c = findingResponse3.Id,
                                                        SQX_Action__c = action3.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion4 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse4.Id,
                                                        SQX_Investigation__c = investigation4.Id
                                                    );

        SQX_Response_Inclusion__c respInclusion5 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse5.Id,
                                                        SQX_Investigation__c = investigation5.Id
                                                    );
       
        SQX_Response_Inclusion__c respInclusion6 = new SQX_Response_Inclusion__c(
                                                        Type__c = 'Investigation',
                                                        SQX_Response__c = findingResponse6.Id,
                                                        SQX_Investigation__c = investigation6.Id
                                                    );

        insert new List<SQX_Response_Inclusion__c> { respInclusion1, respInclusion3, respInclusion4, respInclusion5,respInclusion6}; 
		
        SQX_Action__c implementationForAudit6 = new SQX_Action__c(
                                                                Plan_Number__c = 'PLN-000',
                                                                Plan_Type__c = 'Corrective',
                                                                SQX_Audit__c = audit6.audit.Id,
                                                                Description__c = 'Implementation with assignee',
                                                                Due_Date__c = Date.Today().addDays(10),
                                                                Status__c = null);

        insert implementationForAudit6;
        return new List<Id>{ audit1.audit.Id, audit2.audit.Id, audit3.audit.Id, audit4.audit.Id, audit5.audit.Id, audit6.audit.Id};
    }


   
    /*
    *   test for checking whether Audit Stage is set appropriately 
    */
    public static testmethod void givenAudit_thenAppropriateStageIsSetBasedOnAuditState(){

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User responseSubmitter = users.get(RESPONSE_SUBMITTER);
        User adminUser = users.get(ADMIN);

        System.runas(adminUser){
            /*
            * Arange : Add Audit, Finding, Audit Response, Finding Response,Actions ,Investigations and Response Inclusions 
            */
            List<Id> auditIdList = arrangeAudit(adminUser, responseSubmitter);

            // Act : Set Audit Stage 
            SQX_Stage_Setter.setRecordStage(auditIdList);
            
            final Map<String, String> auditExpectedStage = new Map<String, String> {
                'Audit-1' => SQX_Audit.STAGE_RESPONSE_APPROVAL,
                'Audit-2' => SQX_Audit.STAGE_IMPLEMENT,
                'Audit-3' => SQX_Audit.STAGE_IMPLEMENTATION_RESPONSE_APPROVAL,
                'Audit-4' => SQX_Audit.STAGE_RESPOND,
                'Audit-5' => SQX_Audit.STAGE_READY_FOR_CLOSURE,
                'Audit-6' => SQX_Audit.STAGE_IMPLEMENT
            };

            //Assert : Invocable method should set the Audit Stage appropriately 
            for ( SQX_Audit__c audit : [SELECT Id, Title__c, Stage__c FROM SQX_Audit__c WHERE Id IN: auditIdList ]) {
                System.assertEquals(auditExpectedStage.get(audit.Title__c), audit.Stage__c, 'Audit\'s stage is invalid');
            }
        }
        
    }
    
}