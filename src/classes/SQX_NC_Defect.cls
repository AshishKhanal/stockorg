/**
 * class for writing codes for the actions performed by the nc defect
 */
public with sharing class SQX_NC_Defect {

    /**
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newNCDefects equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_NC_Defect__c> newNCDefects, Map<Id,SQX_NC_Defect__c> oldMap){
            super(newNCDefects, oldMap);
        }
        
        /**
         * prevents defect defective quantity greater than impacted product lot quantity
         */
        public Bulkified preventDefectiveQtyGreaterThanLotQty(){
            Rule defectiveQtyChanged = new Rule();
            
            defectiveQtyChanged.addRule(Schema.SQX_NC_Defect__c.Defective_Quantity__c, RuleOperator.HasChanged);
            
            this.resetView()
                .applyFilter(defectiveQtyChanged, RuleCheckMethod.OnCreateAndEveryEdit);
            
            List<SQX_NC_Defect__c> defectList = (List<SQX_NC_Defect__c>) defectiveQtyChanged.evaluationResult;
            
            if(defectList.size() > 0){
                Set<Id> ncIds = new Set<Id>();
                Map<String, SObject> defectMap = new Map<String, SObject>();
                for(SQX_NC_Defect__c defect : defectList){
                    String ncPartLotCombination = '' + defect.SQX_Nonconformance__c + defect.SQX_Part__c + defect.Lot_Ser_Number__c;
                    ncIds.add(defect.SQX_Nonconformance__c);
                    defectMap.put(ncPartLotCombination, defect);
                    
                }
                validateImpactedPartAndDefectQuantity(ncIds, defectMap);
            }
            return this;
        }
    }

    /**
     * method to validate defect defective quantity and impacted product lot quantity
     * @param ncIds ids of NC whose defect defective quantity and impacted product lot quantity is to be validated
     * @param objectMap objects where validation is to be done and add error
     */
    public static void validateImpactedPartAndDefectQuantity(Set<Id> ncIds, Map<String, SObject> objectMap){
        
        AggregateResult[] defects = [SELECT SQX_Nonconformance__c NC, SQX_Part__c Part, Lot_Ser_Number__c Lot, SUM(Defective_Quantity__c) defectiveQuantity,
                                     GROUPING(SQX_Nonconformance__c) NCGroup, Grouping(SQX_Part__c) PartGroup, Grouping(Lot_Ser_Number__c) LotGroup
                                     FROM SQX_NC_Defect__c 
                                     WHERE SQX_Nonconformance__c IN :ncIds 
                                     GROUP BY  ROLLUP(SQX_Nonconformance__c, SQX_Part__c, Lot_Ser_Number__c)];
        
        List<SQX_NC_Impacted_Product__c> impactedProducts = new List<SQX_NC_Impacted_Product__c>();
        impactedProducts = [SELECT Id, SQX_Nonconformance__c, SQX_Impacted_Part__c, Lot_Number__c, Lot_Quantity__c FROM SQX_NC_Impacted_Product__c WHERE SQX_Nonconformance__c IN :ncIds];
        
        Map<String, Decimal> defectQtyMap = new Map<String, Decimal>();
        for(AggregateResult defect : defects){
            Boolean validDefect = defect.get('NCGroup') == 0 && defect.get('PartGroup') == 0 && defect.get('LotGroup') == 0;
            String ncPartLotCombination = '' + defect.get('NC') + defect.get('Part') + defect.get('Lot');
            if(validDefect){
                Decimal defectQty = (Decimal) defect.get('defectiveQuantity');
                if(defectQtyMap.containsKey(ncPartLotCombination)){
                    defectQty = defectQty + defectQtyMap.get(ncPartLotCombination);
                    defectQtyMap.put(ncPartLotCombination, defectQty);
                } else {
                    defectQtyMap.put(ncPartLotCombination, defectQty);
                }
            }
        }
        if(defectQtyMap.size() > 0){
            for(SQX_NC_Impacted_Product__c impactedProduct : impactedProducts){
                String ncPartLotCombinationforIP = '' + impactedProduct.SQX_Nonconformance__c + impactedProduct.SQX_Impacted_Part__c + impactedProduct.Lot_Number__c;
                if(defectQtyMap.containsKey(ncPartLotCombinationforIP) && defectQtyMap.get(ncPartLotCombinationforIP) > impactedProduct.Lot_Quantity__c){
                    objectMap.get(ncPartLotCombinationforIP).addError(Label.CQ_UI_Defective_Quantity_is_greater_than_Lot_Quantity);
                }
            }
        }
    }
}