/**
* Batch processor to set pending document training records of existing obsolete documents.
* Added in version  : 6.1.0
* Used for versions : 5.0.0 and later
*/
global with sharing class SQX_6_1_MigrateObsoleteDocumentTrainings implements Database.Batchable<SObject> {
    
    global SQX_6_1_MigrateObsoleteDocumentTrainings() { }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String docStatusObsolete = SQX_Controlled_Document.STATUS_OBSOLETE,
            trainingStatusPending = SQX_Personnel_Document_Training.STATUS_PENDING;
        
        // get pending trainings related to obsolete controlled documents
        String query = 'SELECT Id, Status__c FROM SQX_Personnel_Document_Training__c WHERE Status__c = :trainingStatusPending '
            + ' AND SQX_Controlled_Document__r.Document_Status__c = :docStatusObsolete';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> objList) {
        if (objList.size() > 0) {
            // set status to Obslete
            for (SQX_Personnel_Document_Training__c dt : (List<SQX_Personnel_Document_Training__c>)objList) {
                dt.Status__c = SQX_Personnel_Document_Training.STATUS_OBSOLETE;
            }
            
            /**
            * WITHOUT SHARING has been used 
            * ---------------------------------
            * Without sharing has been used because the owner running user might not have access to update trainings
            * This will cause and error.
            */
            new SQX_DB().withoutSharing().op_update(objList, new List<Schema.SObjectField>{SQX_Personnel_Document_Training__c.fields.Status__c});
        }
    }
    
    global void finish(Database.BatchableContext bc) { }
}