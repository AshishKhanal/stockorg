/*
* This method is used to get the implementation  plan document files that have controlled doc and new doc content
* 
*/
public with sharing class SQX_Implementation_Doc_Preview {
    
    /*
    * This method is used to get the implementation  plan document files
    * 
    */
    @AuraEnabled
    public Static List<sobject> getImplementationDocFile(String recordId){
        try{
            List<SQX_Implementation__C> implList=[select id,compliancequest__SQX_Change_Order__c,
                                                  compliancequest__SQX_Controlled_Document__r.Name,
                                                  compliancequest__SQX_Controlled_Document__r.compliancequest__Title__c,
                                                  compliancequest__SQX_Controlled_Document__r.compliancequest__Content_Reference__c,
                                                  compliancequest__SQX_Controlled_Document__r.compliancequest__Valid_Content_Reference__c,
                                                  compliancequest__SQX_Controlled_Document_New__r.Name,
                                                  compliancequest__SQX_Controlled_Document_New__r.compliancequest__Title__c,
                                                  compliancequest__SQX_Controlled_Document_New__r.compliancequest__Content_Reference__c,
                                                  compliancequest__SQX_Controlled_Document_New__r.compliancequest__Valid_Content_Reference__c,
                                                  compliancequest__Type__c,compliancequest__Title__c 
                                                  from compliancequest__SQX_Implementation__c 
                                                  where compliancequest__SQX_Change_Order__c=:recordId 
                                                  AND compliancequest__Type__c != 'Generic' AND (compliancequest__SQX_Controlled_Document__c!=null Or compliancequest__SQX_Controlled_Document_New__c !=null )];
            return implList.size()>0?implList:null;
        }catch (Exception ex){
            throw new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
        }
    }
}