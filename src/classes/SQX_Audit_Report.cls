/**
* SQX_Audit_Report objects class handles trigger
*/
public with sharing class SQX_Audit_Report{

    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_IN_APPROVAL = 'In Approval';
    public static final String STATUS_COMPLETE = 'Complete';
    public static final String STATUS_DRAFT = 'Draft';
    
    public static final String APPROVAL_STATUS_REJECTED = 'Rejected';
    public static final String APPROVAL_STATUS_APPROVED = 'Approved';
    public static final String APPROVAL_STATUS_NONE = '-';

    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(List<SQX_Audit_Report__c> newAuditReports, Map<Id, SQX_Audit_Report__c> oldAuditReports){
            super(newAuditReports, oldAuditReports);
        }


        /**
        * Change the status of Audit to "Complete", when Audit Report is Approved
        *
        * @author Pramesh
        * @date 7/6/2015
        * @story SQX-1291
        */
        public Bulkified updateAuditAfterAuditReportIsApproved(){
            final String ACTION_NAME = 'updateAuditAfterAuditReportIsApproved';

            //Get the newly complete audit reports
            Rule allAuditReportApproved = new Rule();
            allAuditReportApproved.addRule(Schema.SQX_Audit_Report__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);
            
            allAuditReportApproved.addRule(Schema.SQX_Audit_Report__c.Approval_Status__c, RuleOperator.NotEquals, APPROVAL_STATUS_REJECTED);

            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditReport, ACTION_NAME)
                .applyFilter(allAuditReportApproved, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(allAuditReportApproved.evaluationResult.size() > 0){

                this.addToProcessedRecordsFor(SQX.AuditReport, ACTION_NAME, allAuditReportApproved.evaluationResult);

                List<SQX_Audit_Report__c> auditReports = (List<SQX_Audit_Report__c>) allAuditReportApproved.evaluationResult;
            
                Set<Id> auditIds = getIdsForField(auditReports, Schema.SQX_Audit_Report__c.SQX_Audit__c);

                List<SQX_Audit__c> allAudits = [SELECT Id, 
                                                       Status__c
                                                FROM SQX_Audit__c 
                                                WHERE Id IN : auditIds FOR UPDATE];

                //update all audit status to complete
                for(SQX_Audit__c audit : allAudits){
                    audit.Status__c = SQX_Audit.STATUS_COMPLETE;
                    audit.Stage__c = SQX_Audit.STAGE_RESPOND;                
                }

                //WITHOUT SHARING used because auditee contact may be approving the report which inturn sets the 
                //Complete status of the Audit Report. Auditee Contact will not have R/W access on the Audit object, 
                //therefore we need to escalate the privilege.
                new SQX_DB().withoutSharing().op_update(allAudits, new List<Schema.SObjectField>{
                        Schema.SQX_Audit__c.Status__c,
                        Schema.SQX_Audit__c.Stage__c

                });
                
                //Audit stage is evaluated to support the cases :
                //i) If Finding is created with no policies checked then Finding is completed and Audit Response is not created. 
                //   Thus Process builder will not invoke the method SQX_Stage_Setter.setAuditStage()
                //ii)If Audit has no finding, the stage should be set to 'Ready For Closure'. 
                //   Invoking method SQX_Stage_Setter.setAuditStage() will evaluate Audit and set stage accordingly
                SQX_Stage_Setter.setAuditStage(new list<Id>(auditIds));
            }

            
            return this;    
        }

        /**
        * Change the status of Audit to "Finding Approval", when Audit Report is Sent For Approval
        *
        * @author Sudeep Maharjan
        * @date 10/10/2017
        * @story SQX-3999
        */
        public Bulkified updateAuditStageWhenAuditReportIsSentForApproval(){
            final String ACTION_NAME = 'updateAuditStageWhenAuditReportIsSentForApproval';

            //Get the In approval Audit Report
            Rule allAuditReportApproved = new Rule();
            allAuditReportApproved.addRule(Schema.SQX_Audit_Report__c.Status__c, RuleOperator.Equals, STATUS_IN_APPROVAL);

            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditReport, ACTION_NAME)
                .applyFilter(allAuditReportApproved, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(allAuditReportApproved.evaluationResult.size() > 0){

                this.addToProcessedRecordsFor(SQX.AuditReport, ACTION_NAME, allAuditReportApproved.evaluationResult);

                List<SQX_Audit__c> auditsToUpdate = new List<SQX_Audit__c>();
                for (SQX_Audit_Report__c rpt : (List<SQX_Audit_Report__c>)allAuditReportApproved.evaluationResult) {
                    // set Audit Stage to Finding Approval
                    auditsToUpdate.add(new SQX_Audit__c(
                        Id = rpt.SQX_Audit__c,
                        Stage__c = SQX_Audit.STAGE_FINDING_APPROVAL
                    ));
                }

                new SQX_DB().op_update(auditsToUpdate, new List<Schema.SObjectField>{
                        Schema.SQX_Audit__c.Stage__c

                });
            }

            return this;    
        }

        /**
        * Revert the Stage of Audit to "In Progress", when Audit Report is Rejected
        *
        * @author Sudeep Maharjan
        * @date 10/09/2017
        * @story SQX-3999
        */
        public Bulkified updateAuditwhenAuditReportIsRejected(){
            final String ACTION_NAME = 'updateAuditwhenAuditReportIsRejected';

            //Get the rejected audit reports
            Rule allAuditReportRejected = new Rule();
            allAuditReportRejected.addRule(Schema.SQX_Audit_Report__c.Status__c, RuleOperator.Equals, STATUS_COMPLETE);
            allAuditReportRejected.addRule(Schema.SQX_Audit_Report__c.Approval_Status__c, RuleOperator.Equals, APPROVAL_STATUS_REJECTED);

            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditReport, ACTION_NAME)
                .applyFilter(allAuditReportRejected, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(allAuditReportRejected.evaluationResult.size() > 0){

                this.addToProcessedRecordsFor(SQX.AuditReport, ACTION_NAME, allAuditReportRejected.evaluationResult);

                List<SQX_Audit__c> auditsToUpdate = new List<SQX_Audit__c>();
                for (SQX_Audit_Report__c rpt : (List<SQX_Audit_Report__c>)allAuditReportRejected.evaluationResult) {
                    // reset audit stage from Finding Approval to In Progress when recalled or rejected
                    auditsToUpdate.add(new SQX_Audit__c(
                        Id = rpt.SQX_Audit__c,
                        Stage__c = SQX_Audit.STAGE_IN_PROGRESS
                    ));
                }
                
                /*
                * Without sharing has been used to cover the following scenario:
                * ------------------------------------------ 
                * Scenario:
                *    As a Auditee Contact may be rejecting the report which inturn sets the Reject Status od Audit Rport. Auditee Contact will not have R/W access on Audit Report object.
                *
                */
                new SQX_DB().withoutSharing().op_update(auditsToUpdate, new List<Schema.SObjectField>{
                        Schema.SQX_Audit__c.Stage__c
                });
            }

            
            return this;    
        }
    }
}