/**
*/
@isTest
public class SQX_Test_1202_Review {

    /*
    Helper methods to fill in required field in controlled doc and get it ready for insertion
    */
    public static void setupControlledDoc(SQX_Controlled_Document__c doc){
        
        integer randomNumber = (Integer)( Math.random() * 1000000 );
        doc.Document_Number__c = 'Doc' + randomNumber;
        doc.Revision__c = 'REV-1';
        doc.Title__c = 'Document Title ' + randomNumber;
        doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
    }

    /**
    * adds a document review to a given document
    */
    private static void addDocumentReview(SQX_Controlled_Document__c doc, String reviewType, Date nextReviewDate){
        integer randomNumber = (Integer)( Math.random() * 1000000 );
        
        PageReference reviewPage = Page.SQX_Document_Review_Editor;
        reviewPage.getParameters().put('controlleddocid', doc.Id); //add the controlledocid as parameter coz this is how the ui will pass it

        Test.setCurrentPage(reviewPage);

        SQX_Document_Review__c docReview = new SQX_Document_Review__c(Controlled_Document__c = doc.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(docReview);
        SQX_Extension_Document_Review review = new SQX_Extension_Document_Review(controller);

        if(reviewType == 'Revision'){
            review.NewRevision = randomNumber + '';
        }

        docReview.Next_Review_Date__c = nextReviewDate;
        docReview.Performed_By__c = UserInfo.getUserId();
        docReview.Review_Decision__c = reviewType;

        review.SubmitReview();
    }

    /**
    * sets up controlled doc to be used
    */
    private static SQX_Controlled_Document__c setupCurrentControlledDoc(){
        SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(doc);
        SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

        extension.file = Blob.valueOf('Hello World');
        extension.fileName = 'Hello.txt';
        setupControlledDoc(doc);

        extension.create();

        doc.Next_Review_Date__c = Date.today().addDays(25);
        doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

        new SQX_DB().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
                Schema.SQX_Controlled_Document__c.Next_Review_Date__c,
                Schema.SQX_Controlled_Document__c.Document_Status__c
        });


        return doc;
    }

    static boolean  run_allTests = true,
                    run_givenNextReviewDate_TaskIsAddedForReview = false,
                    run_givenReviewIsReviseOrContinue_NextReviewDateIsCopied = false,
                    run_givenAReviewIsAddedByOther_ItFails = false,
                    run_givenControlledDoc_ConditionA_WhenReleased_NextReviewDate= false,
                    run_givenControlledDoc_ConditionB_WhenReleased_NextReviewDate= false,
                    run_givenControlledDoc_ConditionC_WhenReleased_NextReviewDate= false,
                    run_givenDocumentReview_DateInvalid_ShouldNotSave= false,
                    givenControlledDoc_WhenReviewDecisionIsContinueUse_ThenNextReviewDateIsRequired = false;

    /**
    * Ensures that
    * if next review date is given, task is added when review date appears close
    */
    public static testmethod void givenNextReviewDate_TaskIsAddedForReview(){

        if(!run_allTests && !run_givenNextReviewDate_TaskIsAddedForReview){
            return;
        }


        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             approver = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        SQX_Controlled_Document__c doc;

        //Arrange: Create a controlled doc, set its status to current
        System.runAs(standardUser){
            doc = setupCurrentControlledDoc();
        }

        System.runAs(approver){
            //Act: Set the Queue_Review_Task__c flag up, so that a task is created
            //This will be triggered by time based workflow using in SF. When the approver approves the doc.

            //note the usage of without sharing because an approver will have edit rights on the object.
            doc.Queue_Review_Task__c = true;
            new SQX_DB().withoutSharing().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
                    Schema.SQX_Controlled_Document__c.Next_Review_Date__c,
                    Schema.SQX_Controlled_Document__c.Document_Status__c
                });


        }


        System.runAs(standardUser){
            //Assert: assert that a task has been created for the standardUser (Owner of the doc)
            System.assertEquals(1, [SELECT Id FROM Task WHERE WhatId =: doc.Id AND OwnerId = : standardUser.Id].size());

            //Act: Adding a doc review resets the Queue_Review_Task flag
            addDocumentReview(doc, 'Continue Use', System.today());

            //Assert: assert that the queue_review_task flag is now cleared
            System.assertEquals(false, [SELECT Id, Queue_Review_Task__c FROM SQX_Controlled_Document__c
                         WHERE Id = : doc.Id].Queue_Review_Task__c);
        }

    }

    /**
    * Ensures that if the review is created with revise or continue as is
    * next review date is copied to the related controlled doc.
    */
    public static testmethod void givenReviewIsReviseOrContinue_NextReviewDateIsCopied(){
        if(!run_allTests && !run_givenReviewIsReviseOrContinue_NextReviewDateIsCopied){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        SQX_Controlled_Document__c doc;

        System.runAs(standardUser){

            //Arrange: create a controlled doc.
            doc = setupCurrentControlledDoc();

            //Act: add a review as continue use
            Date nextReviewDate = Date.today().addDays(40);
            addDocumentReview(doc, 'Continue Use', nextReviewDate);

            //Assert: Next review date is set properly
            doc = [SELECT Id, Next_Review_Date__c, Document_Status__c
                    FROM SQX_Controlled_Document__c
                    WHERE Id = : doc.Id];

            System.assertEquals(nextReviewDate, doc.Next_Review_Date__c);

            //Act: add a review as Revise
            nextReviewDate = Date.today().addDays(100);
            addDocumentReview(doc, 'Continue Use', nextReviewDate);

            //Assert: Next review date is set properly
            doc = [SELECT Id, Next_Review_Date__c, Document_Status__c
                    FROM SQX_Controlled_Document__c
                    WHERE Id = : doc.Id];

            System.assertEquals(nextReviewDate, doc.Next_Review_Date__c);
        }

    }

    /**
    * Ensures that a review can only be added by owner and others in the heirarchy.
    */
    public static testmethod void givenAReviewIsAddedByOther_ItFails(){
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER),
             anotherUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        SQX_Controlled_Document__c doc;

        System.runAs(standardUser){

            //Arrange: create a controlled doc.
            doc = setupCurrentControlledDoc();
        }

        System.runAs(anotherUser){
            //Act: try adding a review
            addDocumentReview(doc, 'Continue Use', System.today());
            

            //Assert: an error is added
            ApexPages.Message[] msgs =  ApexPages.getMessages();
            Boolean containsCorrectErrorMessage= false;
            String errorMessages='';
            for(ApexPages.Message msg : msgs){
                if(msg.getSummary().contains('Insufficient privilege on the parent Controlled Document')){
                    containsCorrectErrorMessage= true;
                    break;
                }
                errorMessages= errorMessages+ msg.getSummary() + '\r\n';
            }

            System.assertEquals(true, containsCorrectErrorMessage, errorMessages);
        }

    }

    /**
    * When next review date is not set and review interval is set to value >0 , next review date should be set today+ review interval
    * @author Sagar Shrestha
    * @date 2015/06/16
    * @story [SQX-1261]
    */ 
    public testMethod static void givenControlledDoc_ConditionA_WhenReleased_NextReviewDate(){
        if(!run_allTests && !run_givenControlledDoc_ConditionA_WhenReleased_NextReviewDate){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        System.runAs(standardUser){
            SQX_Test_Controlled_Document currentDoc= new SQX_Test_Controlled_Document();
            currentDoc.doc.Next_Review_Date__c= null;
            currentDoc.doc.Review_Interval__c  = 20;
            currentDoc.doc.Effective_Date__c = Date.Today().addDays(10);

            currentDoc.save();

            currentDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            System.assert([Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c == Date.today().addDays(20), 'Next review should be added 20 days after today'
                +[Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c);
        }


    }

    /**
    * When next review date is set and review interval is set to value >0 , next review date should be set to old review date
    * @author Sagar Shrestha
    * @date 2015/06/16
    * @story [SQX-1261]
    */ 
    public testMethod static void givenControlledDoc_ConditionB_WhenReleased_NextReviewDate(){
        if(!run_allTests && !run_givenControlledDoc_ConditionB_WhenReleased_NextReviewDate){
            return;
        }
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        System.runAs(standardUser){
            SQX_Test_Controlled_Document currentDoc= new SQX_Test_Controlled_Document();
            currentDoc.doc.Next_Review_Date__c= Date.Today();
            currentDoc.doc.Review_Interval__c  = 20;

            currentDoc.save();

            currentDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            System.assert([Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c == Date.today(),
             'Next review should be today but is '+[Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c);
        }


    }

    /**
    * When next review date is not set and review interval is set to value =0 , next review date should be set to old review date
    * @author Sagar Shrestha
    * @date 2015/06/16
    * @story [SQX-1261]
    */ 
    public testMethod static void givenControlledDoc_ConditionC_WhenReleased_NextReviewDate(){
        if(!run_allTests && !run_givenControlledDoc_ConditionC_WhenReleased_NextReviewDate){
            return;
        }
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        System.runAs(standardUser){

            SQX_Test_Controlled_Document currentDoc= new SQX_Test_Controlled_Document();
            currentDoc.doc.Next_Review_Date__c= null;
            currentDoc.doc.Review_Interval__c  = 0;

            currentDoc.save();

            currentDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            System.assert([Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c == null, 
                'Next review should null but is '+[Select Next_Review_Date__c FROM SQX_Controlled_Document__c WHERE Id= :currentDoc.doc.Id].Next_Review_Date__c);
        }

    }

    
    /**
    * When Performed date is set to future date, save should be unsuccessful.
    * When Next Review Date is set to Past Date, save should be unsuccessful.
    * @author Sagar Shrestha
    * @date 2015/06/16
    * @story [SQX-1274]
    */ 
    public testMethod static void givenDocumentReview_DateInvalid_ShouldNotSave(){
        if(!run_allTests && !run_givenDocumentReview_DateInvalid_ShouldNotSave){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        System.runAs(standardUser){
            SQX_Test_Controlled_Document currentDoc= new SQX_Test_Controlled_Document();
            currentDoc.save();
            currentDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Document_Review__c docReview = new SQX_Document_Review__c(Controlled_Document__c= currentDoc.doc.Id);

            docReview.Next_Review_Date__c = date.Today().addDays(1);//allow future date in Next Review Date
            docReview.Performed_By__c = UserInfo.getUserId();
            docReview.Review_Decision__c = 'Continue Use';
            
            docReview.Performed_Date__c= Date.Today().addDays(1);// Do not allow future date on Performed Date
            
            Database.SaveResult futurePerformedDateResult = Database.Insert(docReview,false);

            System.assert(futurePerformedDateResult.isSuccess() == false,
                     'Expected  save to be failed as future date is not allowed in Performed Date'+docReview);

            
            docReview.Performed_Date__c= Date.Today().addDays(-1);//allow past date in Performed Date
            docReview.Next_Review_Date__c = date.Today().addDays(-2);//do not Next Review Date less than Performed Date

            Database.SaveResult pastNextReviewDateResult = Database.Insert(docReview,false);

            System.assert(pastNextReviewDateResult.isSuccess() == false,
                     'Expected  save to be failed as past date is not allowed in Next Review Date'+docReview);

            docReview.Next_Review_Date__c = date.Today().addDays(1);//allow next review date after Performed date
            docReview.Performed_Date__c= Date.Today().addDays(-1);

            Database.SaveResult correctDatesResult = Database.Insert(docReview,false);

            System.assert(correctDatesResult.isSuccess() == true,
                     'Expected  save to be successful for correct datas'+docReview);
        }
    }

    /**
     * Given : Current Doc
     * When : Review with decision Continue Use is added with blank next review date
     * Then : Validation error will be thrown
     *
     * When : Calling setDefaultNextReviewDate method of extension
     * Then : Next review date will be calculated as performed date + review interval define in doc level
     */
    public testMethod static void givenControlledDoc_WhenReviewDecisionIsContinueUse_ThenNextReviewDateIsRequired(){
        if(!run_allTests && !givenControlledDoc_WhenReviewDecisionIsContinueUse_ThenNextReviewDateIsRequired){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        System.runAs(standardUser){

            // Arrange : Create current doc
            SQX_Test_Controlled_Document currentDoc = new SQX_Test_Controlled_Document();
            currentDoc.doc.Review_Interval__c = 365;
            currentDoc.save();
            currentDoc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            // Act : try to add review with continue use decision and next review date null
            SQX_Document_Review__c docReview = new SQX_Document_Review__c(Controlled_Document__c = currentDoc.doc.Id);
            docReview.Next_Review_Date__c = null;
            docReview.Performed_By__c = UserInfo.getUserId();
            docReview.Review_Decision__c = SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE;
            docReview.Performed_Date__c= Date.Today().addDays(-5);
            
            List<Database.SaveResult> saveResults = new SQX_DB().continueOnError().op_insert(new List<SQX_Document_Review__c> {docReview}, new List<Schema.SObjectField> {
                                                                                                SQX_Document_Review__c.Next_Review_Date__c,
                                                                                                SQX_Document_Review__c.Performed_By__c, 
                                                                                                SQX_Document_Review__c.Review_Decision__c, 
                                                                                                SQX_Document_Review__c.Performed_Date__c
                                                                                            });

            // Assert : Validation error should be thrown
            System.assert(saveResults[0].isSuccess() == false, 'Expected error but document review got saved. Error ' + saveResults[0].getErrors());
            String errorMsg = 'Next Review Date is required when review decision is Continue Use.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(saveResults[0].getErrors(), errorMsg), 'Expected validation error message to be matched with expected error. Expected: ' + errorMsg + ' in ' + saveResults[0].getErrors());

            // Instantiate extension of document review
            PageReference reviewPage = Page.SQX_Document_Review_Editor;
            reviewPage.getParameters().put('controlleddocid', currentDoc.doc.Id); //add the controlledocid as parameter coz this is how the ui will pass it

            Test.setCurrentPage(reviewPage);

            ApexPages.StandardController controller = new ApexPages.StandardController(docReview);
            SQX_Extension_Document_Review reviewExtension = new SQX_Extension_Document_Review(controller);

            // Act : Call setDefaultNextReviewDate() method of extension
            reviewExtension.setDefaultNextReviewDate();

            // submit the review
            reviewExtension.SubmitReview();

            // Assert : Review should be successfully submitted with calculated next review date.
            List<SQX_Document_Review__c> dReviews = [SELECT Id, 
                                                            Next_Review_Date__c, 
                                                            Performed_Date__c 
                                                     FROM SQX_Document_Review__c
                                                     WHERE Review_Decision__c = : SQX_Document_Review.REVIEW_DECISION_CONTINUE_USE
                                                        AND Performed_By__c = : UserInfo.getUserId()
                                                        AND Performed_Date__c = : Date.Today().addDays(-5)];

            System.assertEquals(1, dReviews.size(), 'Expected one document review to be added but got ' + dReviews.size());
            System.assertEquals(Date.Today().addDays(-5 + 365), dReviews[0].Next_Review_Date__c, 'Expected Next Review Date to be sum of performed date and review interval');
        }
    }
}