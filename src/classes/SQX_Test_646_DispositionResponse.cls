/**
* This test ensures that a disposition response can be added 
*/
@IsTest
public class SQX_Test_646_DispositionResponse {

    static boolean runAllTests = true,
                    run_givenDispositionResponseIsApproved_StatusIsUpdated = false,
                    run_givenAnNCMeetsAnOpenNCMeetsRequirement_ItCompletes = false,
                    run_givenAnNCMeetsDispositionRequirement_ItCompletes = false,
                    run_givenDispositionLotQty_CompletionIsReached = false,
                    run_givenDispositionIsRequired_ClosureCantBeDone = false;

    public static Id createNC(){
        SQX_Department__c department = new SQX_Department__c();
        department.Name = 'Random Department';

        insert department;

        SQX_NonConformance__c nonConformance = new SQX_NonConformance__c (
                Type__c = SQX_NC.TYPE_INTERNAL,
                Type_Of_Issue__c = SQX_NC.ISSUE_TYPE_PRODUCT,
                Disposition_Required__c = false,
                Occurrence_Date__c = Date.Today(),
                SQX_Department__c = department.Id,
                NC_Title__c = 'Random',
                Containment_Required__c = true,
                Investigation_Required__c = false,
                Preventive_Action_Required__c = false,
                Corrective_Action_Required__c = false,
                Due_Date_Containment__c = Date.Today().addDays(15),
                Response_Required__c = false,
                Severity__c = '2 - Low',
                Probability__c = '2 - Remote');
                                     
                                     
        Map<String, Object> submissionSet = new Map<String, Object>();
        List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
        
        Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance ));

        ncMap.put('Id', 'NC-1');
        changeSet.add(ncMap);

        submissionSet.put('changeSet', changeSet);

        String stringChangeSet= JSON.serialize(submissionSet);       
        Map<String, String> params = new Map<String, String>();
        params.put('nextAction', 'Save');
        Id result= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,params);
        
        System.assert(string.valueOf(result.getsObjectType()) == SQX.NonConformance, 
                'Expected to process changeset'+ changeSet);
                
                
        return result;

    }
    
    public static void submitNC(Id ncId){
        Map<String, String> params = new Map<String, String>();
        params.put('nextAction', 'submit');
        
        Id result= SQX_Extension_NC.processChangeSetWithAction(ncId, null, null, params);
        
        System.assertEquals(SQX_NC.STATUS_TRIAGE, [SELECT Status__c FROM SQX_Nonconformance__c WHERE Id = : ncId].Status__c);
        //NC must be in triage state
        
    }
    
    public static void openNC(Id ncId){
        Map<String, String> params = new Map<String, String>();
        params.put('nextAction', 'takeownershipandinitiate');
        
        Id result= SQX_Extension_NC.processChangeSetWithAction(ncId, null, null, params);
        
        System.assertEquals(SQX_NC.STATUS_OPEN, [SELECT Status__c FROM SQX_Nonconformance__c WHERE Id = : ncId].Status__c);
        //NC must be in open state
        
    }

    /**
    * Smoke Test A disposition can be provided for an NC and can be submitted. 
    */
    public static testmethod void givenDispositionResponseIsApproved_StatusIsUpdated(){

        if(!runAllTests && !run_givenDispositionResponseIsApproved_StatusIsUpdated){
            return;
        }

        User secondaryUser =  SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User primaryUser =  SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
    
        Id ncId;
        SQX_Part__c part;

        System.runAs(primaryUser){
            //setup
            ncId = createNC();
            submitNC(ncId);
            openNC(ncId);


            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
        

            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.SQX_Impacted_Part__c = part.Id;
            product.SQX_NonConformance__c = ncId;
            product.Lot_Number__c = 'asd';
            product.Lot_Quantity__c = 100;

            new SQX_DB().op_insert(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                    Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_NonConformance__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c
                });
        }

        //submit disposition as the secondary User
        System.runas(secondaryUser){
        
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(
                                                        [SELECT Id, Status__c
                                                         FROM SQX_NonConformance__c 
                                                         WHERE  Id = : ncId]);
             
            response.save()
                    .addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, part)
                    .submitForApproval();
             
        }
    
    }
    
    
    /**
    * When NC's requirements are met i.e. containment and investigation is required and they are provide the NC completes 
    */
    public testmethod static void givenAnNCMeetsAnOpenNCMeetsRequirement_ItCompletes(){

        if(!runAllTests && !run_givenAnNCMeetsAnOpenNCMeetsRequirement_ItCompletes)
            return;

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            nc = new SQX_Test_NC(true);
            nc.nc.Investigation_Approval__c = false;
            nc.nc.Corrective_Action_Required__c = false;
            nc.nc.Preventive_Action_Required__c = false;

            nc.setDispositionRules(false, null, false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            
            nc.synchronize();
            System.assertEquals( SQX_NC.STATUS_OPEN, nc.nc.Status__c);

            nc.nc.OwnerId = ncCoordinator.Id;

            nc.save();
        }

        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();

            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_CONTAINMENT);
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_INVESTIGATION);
            response.publishResponse();

            nc.synchronize();

            System.assertEquals( SQX_NC.STATUS_COMPLETE, nc.nc.Status__c);
        }


    }
    
    /**
    * When NC's requirements are met i.e. containment,disposition and investigation is required and they are provide the NC completes 
    */
    public testmethod static void givenAnNCMeetsDispositionRequirement_ItCompletes(){
        
        if(!runAllTests && !run_givenAnNCMeetsDispositionRequirement_ItCompletes)
                return;

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Part__c part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
        Id impactedProductId = null;
        SQX_Disposition__c disposition;

        System.runas(standardUser){
        //an nc with only disposition required
            nc = new SQX_Test_NC();
            nc.nc.Investigation_Approval__c = false;
            nc.nc.Investigation_Required__c = false;
            nc.nc.Containment_Required__c = false;
            nc.nc.Corrective_Action_Required__c = false;
            nc.nc.Preventive_Action_Required__c = false;

            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            

            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.SQX_Impacted_Part__c = part.Id;
            product.SQX_NonConformance__c = nc.nc.Id;
            product.Lot_Number__c = 'asd';
            product.Lot_Quantity__c = 100;

            new SQX_DB().op_insert(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                    Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_NonConformance__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c
                });

            nc.nc.OwnerId = ncCoordinator.Id; //ownership transferred because this is what happens using the queue

            nc.save();

            impactedProductId = product.Id;

        }

        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();
            
            disposition = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Open',
                                                         Lot_Number__c = 'Invalid-Lot-Number', 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 100);
            
            try {                                            
                new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                    SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                });

                System.assert(false, 'An exception should have been thrown when inserting the disposition since lot is invalid');
            }
            catch(DmlException ex){
                //SQX-783 invalid part-lot exception, an exception should be thrown that will be ignored
            }


            //SQX-783 lot-number val
            //change the lot-number back to valid one
            disposition.Lot_Number__c = 'asd';

            new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                    SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.New_Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c
                });

            System.assert(disposition.Id != null, 'Disposition should have been saved but wasnt');
            System.assert([SELECT Id, SQX_NC_Impacted_Product__c 
                FROM SQX_Disposition__c 
                WHERE Id = : disposition.Id].SQX_NC_Impacted_Product__c == impactedProductId
                , 'Impacted product has not been set');

            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition);
            response.publishResponse();

            nc.synchronize();
            

            System.assertEquals(SQX_NC.STATUS_OPEN, nc.nc.Status__c);
        }
        System.runas(adminUser){
            //set approve disposition set true
            nc.setDispositionRules(true, Date.today(), true, ncCoordinator)
              .save();
            nc.synchronize();

        }

        System.runAs(ncCoordinator){            
            Id dispositionID = disposition.Id;
            disposition = new SQX_Disposition__c(Id = dispositionId, New_Status__c = 'Open',
                                                         Comment__c = 'Done');
            new SQX_DB().op_update(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.New_Status__c
            });
            
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition);
            response.submitForApproval();

            nc.synchronize();
            System.assertEquals(SQX_NC.STATUS_OPEN, nc.nc.Status__c);

            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Approve Response
            response.performApproval(true);

            //Expected disposition approval status to be approved
            disposition = [Select Approval_Status__c, Status__c from SQX_Disposition__c where Id = :dispositionId];
            System.assertEquals(SQX_Disposition.APPROVAL_STATUS_APPROVED, disposition.Approval_Status__c, 'Expected disposition approval status to be approved' + disposition);
            

            disposition.New_Status__c = 'Complete';
            disposition.Completed_On__c = Date.today();
            disposition.Completed_By__c = 'Sagar';
            new SQX_DB().op_update(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.New_Status__c,
                SQX_Disposition__c.fields.Completed_On__c,
                SQX_Disposition__c.fields.Completed_By__c
            });

            response = new SQX_Test_Finding_Response(nc.nc);
            response.response.Requires_Additional_Response__c = true;

            response.save();
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition);
            response.publishResponse();

            nc.synchronize();

            //added check for additional response for verifying SQX-3717
            System.assertEquals(SQX_NC.STATUS_OPEN, nc.nc.Status__c, nc.nc);

            response = new SQX_Test_Finding_Response(nc.nc);
            response.response.Requires_Additional_Response__c = false;

            response.save();

            response.publishResponse();

            nc.synchronize();

            System.assertEquals(SQX_NC.STATUS_COMPLETE, nc.nc.Status__c, nc.nc);

            //Expected disposition approval status to be approved
            disposition = [Select Approval_Status__c from SQX_Disposition__c where Id = :dispositionId];
            System.assertEquals(SQX_Disposition.APPROVAL_STATUS_APPROVED, disposition.Approval_Status__c, 'Expected disposition approval status to be approved');
            
        }

    }

    /**
    * this test ensures that any NC requiring disposition is not completed until disposition is complete
    */
    public testmethod static void givenDispositionLotQty_CompletionIsReached(){
        if(!runAllTests && !run_givenDispositionLotQty_CompletionIsReached)
            return;

        //setup nc with disposition required and impacted products 
        //but no other requirements
        //open the nc
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Part__c part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
        final String lotNumber = 'LOT#543';
        final Integer lotQty = 100;

         System.runas(standardUser){
        //an nc with only disposition required
            nc = new SQX_Test_NC();
            nc.nc.Investigation_Approval__c = false;
            nc.nc.Investigation_Required__c = false;
            nc.nc.Containment_Required__c = false;
            nc.nc.Corrective_Action_Required__c = false;
            nc.nc.Preventive_Action_Required__c = false;

            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            //add an impacted part with lot#543 and qty 100
            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(SQX_NonConformance__c = nc.nc.Id,
                                                        SQX_Impacted_Part__c = part.Id,
                                                        Lot_Number__c = lotNumber,
                                                        Lot_Quantity__c = lotQty);

            insert impactedProduct;

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            
            
            //assert that the nc should not complete
            //because disposition needs to be done
            nc.synchronize();
            System.assertEquals(SQX_NC.STATUS_OPEN, nc.nc.Status__c);

            nc.nc.OwnerId = ncCoordinator.Id; //ownership transferred because this is what happens using the queue

            nc.save();
        }


        System.runAs(ncCoordinator){
            SQX_Test_Finding_Response response = new SQX_Test_Finding_Response(nc.nc);

            response.save();
            
            //add a partial disposition i.e. perform disposition of qty less then required
            //add a disposition of lot qty - 20.
            SQX_Disposition__c disposition = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Complete',
                                                         Completed_On__c = Date.today(),
                                                         Completed_By__c = 'PRB',
                                                         Lot_Number__c = lotNumber, 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = lotQty-20);
            


            new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.SQX_NonConformance__c,
                SQX_Disposition__c.fields.New_Status__c,
                SQX_Disposition__c.fields.Lot_Number__c,
                SQX_Disposition__c.fields.SQX_Part__c ,
                SQX_Disposition__c.fields.Disposition_Quantity__c
            });

            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, disposition);
            response.publishResponse();

            nc.synchronize();

            SQX_NonConformance__c fetchedNC = [SELECT Id, Status__c, Completion_Date_Disposition__c FROM SQX_Nonconformance__c WHERE Id = : nc.nc.Id];

            //SQX-783 added new assertion to ensure that completion date is not filled when NC is not complete
            System.assertEquals(null, fetchedNC.Completion_Date_Disposition__c,'Expected disposition completion date to be null, but found' + fetchedNC);
            
            
            //assert that nc should not complete
            System.assertEquals(SQX_NC.STATUS_OPEN, nc.nc.Status__c);
            
            //submit a disposition with remaining qty
            SQX_Disposition__c anotherDisposition = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         New_Status__c = 'Complete',
                                                         Completed_On__c = Date.today(),
                                                         Completed_By__c = 'PRB',
                                                         Lot_Number__c = lotNumber, 
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 20);

            new SQX_DB().op_insert(new List<SQX_Disposition__c>{anotherDisposition}, new List<Schema.SObjectField>{
                SQX_Disposition__c.fields.SQX_NonConformance__c,
                SQX_Disposition__c.fields.New_Status__c,
                SQX_Disposition__c.fields.Lot_Number__c,
                SQX_Disposition__c.fields.SQX_Part__c ,
                SQX_Disposition__c.fields.Disposition_Quantity__c
            });


            response = new SQX_Test_Finding_Response(nc.nc);
            response.save();
            response.addInclusion(SQX_Response_Inclusion.INC_TYPE_DISPOSITION, null, null, null, anotherDisposition);
            response.publishResponse();

            nc.synchronize();
            
            System.assertEquals(SQX_NC.STATUS_COMPLETE, nc.nc.Status__c, '' + nc);

            fetchedNC = [SELECT Id, Status__c, Completion_Date_Disposition__c FROM SQX_Nonconformance__c WHERE Id = : nc.nc.Id];

            //SQX-783 added new assertion to ensure that completion date is not filled when NC is complete
            System.assertEquals(Date.today(), fetchedNC.Completion_Date_Disposition__c, 'Expected disposition completion date to be set to today, but found' + fetchedNC);
        }

    }


        /**
    * this test ensures that any NC requiring disposition is not closed until disposition is complete
    */
    public testmethod static void givenDispositionIsRequired_ClosureCantBeDone(){

        if(!runAllTests && !run_givenDispositionIsRequired_ClosureCantBeDone)
            return;

        //setup nc with disposition required and impacted products 
        //but no other requirements
        //open the nc
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        SQX_Test_NC nc = null;
        User ncCoordinator = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Part__c part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
        final String lotNumber = 'LOT#543';
        final Integer lotQty = 100;

         System.runas(standardUser){
        //an nc with only disposition required
            nc = new SQX_Test_NC();

            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            //add an impacted part with lot#543 and qty 100
            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(SQX_NonConformance__c = nc.nc.Id,
                                                        SQX_Impacted_Part__c = part.Id,
                                                        Lot_Number__c = lotNumber,
                                                        Lot_Quantity__c = lotQty);

            insert impactedProduct;

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            

            //assert that the nc should not complete
            //because disposition needs to be done
            
            nc.nc.OwnerId = ncCoordinator.Id; //ownership transferred because this is what happens using the queue

            nc.save();
        }


        System.runAs(ncCoordinator){
           SQX_NonConformance__c ncs = new SQX_NonConformance__c(Id = nc.nc.Id, Status__c = SQX_NC.STATUS_CLOSED,
                                                                Resolution__c = SQX_Finding.RESOLUTION_EFFECTIVE);

           Database.SaveResult result = Database.Update(ncs, false);

           System.assertEquals(false, result.isSuccess());
           System.assert(result.getErrors().get(0).message.contains('NC requires disposition to be complete before closing'),
            'Unexpected error occurred: ' + result.getErrors());

            
        }

    }

}