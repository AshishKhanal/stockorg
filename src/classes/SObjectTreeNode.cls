/**
* This class represents the SObject relationship heirarchially.
* Example: If you wish to find all the contacts and notes of contact of an Account XYZ. The heirarchy would be
* 
* SObjectTreeNode:
* 			data : XYZ (Account)
*			chidren: ----
*						|
*						|---> Contacts (SObjectType)
*								|
*								----[
*										{
*											data: Contact 1 (Contact),
*											children : {
*												Note: [
*														data: Note 1
*													  ]
*											}
*										}
*									]
*/
global with sharing class SObjectTreeNode {


	/**
	* Represents the main SObject contained in the node.
	*/
	global SObject data {get; set;}

	/**
	* Represents the map of child objects that are related to the current SObject(node)
	*/
    global Map<String, List<SObjectTreeNode>> childObjects {get; set;}

    /**
    * The childObjectsKeys represents a string of all the keys present in the childObjects map
    * This is used to check if the key is present or not in a VF Page.
    */
    private String intChildObjectsKeys = null;
    global String childObjectsKeys {
        get {

        	//if the internal string hasn't been initialized then regenerate the keys
        	//WARNING: Caching could lead to a problem if the childObjectsKeys is called before fully setting
        	//			the childObjects map. However, the current usecase doesn't involve such cases. 
            if(intChildObjectsKeys == null){
                intChildObjectsKeys = '';
                for(String key : childObjects.keySet()){
                    intChildObjectsKeys += key + ';';
                }
            }

            return intChildObjectsKeys;

        }
    }

    /**
    * Initializes the SObject Tree during a constructor call
    */
    private void initSObjectTreeNode(){
		childObjects = new Map<String, List<SObjectTreeNode>>();
    }

    /**
    * This is a default constructor that lets us init the tree.
    */
    public SObjectTreeNode(){
        initSObjectTreeNode();
    }




    /**
    * This is a constructor that takes the sobject and sets it as the current nodes data
    * @param obj the value to be set as current nodes data
    */
    public SObjectTreeNode(SObject obj){
        initSObjectTreeNode();
        this.data = obj;
    }


    /**
    * Adds the list of children to the current node under the given relationshipName
    * @param children the children of the current SObject that are to be added to the hierarchy
    * @param relationshipName the relationship parameter that is to be used to group(Key) the children.
    */
    public SObjectTreeNode addChildren(SObject child, String relationshipName){
        SObjectTreeNode treeNode = null;

    	//if there are any new records that have been passed
        if(child != null){

        	//The list of SObjectTreeNode where the children are to be added.
            List<SObjectTreeNode> childObjectsToAdd = null;

            //if there is an existing list we use that list
            if(childObjects.containsKey(relationshipName)){
                childObjectsToAdd = childObjects.get(relationshipName);
            }
            else{

            	//if there isn't an existing list, we create a new list and put it in the map.
                childObjectsToAdd = new List<SObjectTreeNode>();
                childObjects.put(relationshipName, childObjectsToAdd);
            }

            
            treeNode = new SObjectTreeNode(child);
            childObjectsToAdd.add(treeNode);
            

        }

        return treeNode;

    }

}