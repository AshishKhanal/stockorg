/**
* @author - Piyush Subedi
* @description - This class generates a custom workflow json based on a template(generated from EasyPDF) and secondary format setting
* @date - 12/28/2016
*/
public with sharing class SQX_PDFWorkflowGenerator{

    public final static String  STATIC_RESOURCE_SECONDARY_FORMAT_SETTING_TEMPLATE = 'Secondary_Format_Setting_Workflow';

    public final static String  MOCK_PROFILE_PAGE_CONTENT = 'Profile Page',
                                MOCK_STAMP_PDF_CONTENT = 'Doc Number : {!Document_Number__c} Effective Date : {!Effective_Date__c}',
                                PRINT_SCRIPT_PRINT_TEXT_REPLACEMENT_TOKEN = '##PRINT_TEXT##',
                                PRINT_SCRIPT_DATE_FORMAT_REPLACEMENT_TOKEN = '##DATETIME_FORMAT##',
                                PRINT_SCRIPT_EXPIRATION_INTERVAL_REPLACEMENT_TOKEN = '##EXPIRATION_INTERVAL##';

    public final static String  WORKFLOW_JSON_KEY_TASKS = 'Tasks',
                                WORKFLOW_JSON_KEY_TYPE = 'Type',
                                WORKFLOW_JSON_KEY_ITEMS = 'Items',
                                WORKFLOW_JSON_KEY_SEARCHWORD = 'SearchWord',
                                WORKFLOW_JSON_KEY_REPLACEMENTTEXT = 'ReplacementText',
                                WORKFLOW_JSON_KEY_SCRIPT_CONTENT = 'ScriptContent',
                                WORKFLOW_JSON_KEY_PRINT_SCRIPT_POSITION = 'Position';

    public final static String  WORKFLOW_TASK_TYPE_CONVERT_TO_PDF = 'EPWF_PRINTER_CONVERT_TO_PDF',
                                WORKFLOW_TASK_TYPE_COMBINE_PDF = 'EPWF_PROCESSOR_COMBINE_FILES',
                                WORKFLOW_TASK_TYPE_ADD_WATERMARK = 'EPWF_PROCESSOR_ADD_WATERMARK',
                                WORKFLOW_TASK_TYPE_STAMP_PDF = 'EPWF_PROCESSOR_STAMP_PDF_ON_PDF',
                                WORKFLOW_TASK_TYPE_FIND_AND_REPLACE_WORDS = 'EPWF_PROCESSOR_FIND_AND_REPLACE_WORDS',
                                WORKFLOW_TASK_TYPE_SET_PRINT_SCRIPT = 'EPWF_PROCESSOR_SET_DOCUMENT_JAVASCRIPT_FOR_PRINT_ACTION';

    public final static String  SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_APPEND = 'Append',
                                SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_PREPEND = 'Prepend',
                                SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_EXCLUDE = 'Exclude';

    public final static String  WATERMARK_JSON_KEY_TEXT = 'Text',
                                WATERMARK_JSON_KEY_HPOSITION = 'HPosition',
                                WATERMARK_JSON_KEY_VPOSITION = 'VPosition',
                                WATERMARK_JSON_KEY_ZORDER = 'ZOrder',
                                WATERMARK_JSON_KEY_FONTSIZE = 'FontSize',
                                WATERMARK_JSON_KEY_COLOR = 'Color',
                                WATERMARK_JSON_KEY_COLOR_RED = 'Red',
                                WATERMARK_JSON_KEY_COLOR_GREEN = 'Green',
                                WATERMARK_JSON_KEY_COLOR_BLUE = 'Blue',
                                WATERMARK_JSON_KEY_ANGLE = 'Angle',
                                WATERMARK_JSON_KEY_OPACITY = 'Opacity';

    public final static String  WATERMARK_ZORDER_SEND_TO_BACK = 'Send to back',
                                WATERMARK_ZORDER_BRING_TO_FRONT = 'Bring to front';

    public final static String  PRINT_SCRIPT_COORDINATE_KEY_LEFT = 'Left',
                                PRINT_SCRIPT_COORDINATE_KEY_TOP = 'Top',
                                PRINT_SCRIPT_COORDINATE_KEY_RIGHT = 'Right',
                                PRINT_SCRIPT_COORDINATE_KEY_BOTTOM = 'Bottom';

    public final static Integer WATERMARK_TEXT_MAX_LENGTH = 64,
                                FIND_AND_REPLACE_ITEMS_LIMIT = 10,
                                DEFAULT_EXPIRATION_INTERVAL = 24;

    public final static String  UNRECOGNIZED_FORMAT_ERROR_MESSAGE_BODY_SECTION = 'Unrecognized format',
                                DATETIME_DEFAULT_FORMAT = 'dd-mmm-yyyy HH:MM:ss';

    // holds the controlled document record for which secondary content is to be generated
    private SQX_Controlled_Document__c doc;

    // holds a map of field of '{!<field>}' pattern and its corresponding value
    private Map<String, String> replacementMapper;

    // metadata type record for the current doc
    private Secondary_Format_Setting__mdt userSetting;

    // holds json content of the default workflow template
    private Blob workflowTemplate;

    // holds the text value for the watermark to be embedded in the secondary content
    private String watermarkText;

    // holds the date format to be used when merging date fields in the secondary content
    private String dateFormat;

    // holds the datetime format to be used when merging datetime fields in the secondary content
    private String dateTimeFormat;

    // holds a list of fields whose values are to be merged in the generated pdf
    private String[] mergeFields;

    // holds the text to be used as print script when using javascript embedding
    private String printText;

    // holds the datetime format compliant with JS to be used to format print stamp datetime fields
    private String printDateTimeFormat;

    // duration (in hrs) until which the document will be valid ( from print date )
    private Integer expirationInterval;

    // holds the coordinate space map for print script
    private Map<String, Decimal> printScriptPositionMap;


    // specifies how profile page is to be merged in the generated pdf (values: append, pre-pend, exclude)
    public String profilePageInclusionType;

    // holds the custom profile page rendered as pdf
    public Blob profilePageAsPDF;

    // holds the content of the stamping pdf
    public Blob stampPdf;

    // holds the print script content for embedding in the rendered pdf
    public Blob printScript;

    private String settingName;


    /**
    *   Constructor takes in rendition request for a document which will be used to generate custom workflow
    *   fetches the secondary format setting record
    *   fetches the default pdf workflow template
    */
    public SQX_PDFWorkflowGenerator(SQX_RenditionRequest request){

        doc = request.document;

        settingName = [SELECT Secondary_Format_Setting__c FROM SQX_Controlled_Document__c WHERE Id = :doc.Id].Secondary_Format_Setting__c;

        if( !String.isBlank(settingName) ){

            List<Secondary_Format_Setting__mdt> stgs = [ SELECT     Profile_Page_Inclusion_Type__c,
                                                                    Profile_Page__c,
                                                                    Page_Namespace__c,
                                                                    Profile_Page_Parameter__c,
                                                                    Watermark__c,
                                                                    Watermark_Angle__c,
                                                                    Watermark_Color__c,
                                                                    Watermark_Horizontal_Position__c,
                                                                    Watermark_Vertical_Position__c,
                                                                    Watermark_Text_Size__c,
                                                                    Watermark_Z_Order__c,
                                                                    Stamping_PDF__c,
                                                                    Date_Format__c,
                                                                    DateTime_Format__c,
                                                                    Merge_Fields_Set_1__c,
                                                                    Merge_Fields_Set_2__c,
                                                                    Enable_Stamping_When_Printed__c,
                                                                    Text_To_Stamp_When_Printed__c,
                                                                    Print_Stamp_DateTime_Format__c,
                                                                    Print_Stamp_Expiration__c,
                                                                    Print_Stamp_Coordinate__c
                                                                    FROM Secondary_Format_Setting__mdt
                                                                    WHERE DeveloperName = : settingName ];

            if( stgs.size() > 0){
                userSetting = stgs.get(0);
            }
        }

        // fetching standard workflow template from static resource
        workflowTemplate = getResourceContent(STATIC_RESOURCE_SECONDARY_FORMAT_SETTING_TEMPLATE);

    }

    public SQX_PDFWorkflowGenerator(){
        
    }

    /**
    * Apply new custom settings
    * @param 'task' original settings
    */
    private Map<String, Object> applyCustomSettings(Map<String, Object> task){
        List<StaticResource> resourceList = [SELECT Body FROM StaticResource WHERE Name = : settingName ];
        if(!resourceList.isEmpty()){
            Map<String, Object> newTemplate = (Map<String, Object>) JSON.deserializeUntyped(resourceList[0].Body.toString());
            mixJSON(task, newTemplate);
        }
        return task;
    }
    
    /**
    * Mix original and new custom setting JSON
    * @param 'originalJSON' original setting JSON
    * @param 'newJSON' new custom setting JSON
    */ 
    public void mixJSON(Map<String, Object> originalJSON, Map<String, Object> newJSON){
        for(String key : newJSON.keySet()){
            Object keyVal = (Object) newJSON.get(key);
            
            // We are not handle arrays
            if(!originalJSON.containsKey(key) || keyVal instanceof Boolean || keyVal instanceof Integer
               || keyVal instanceof String || keyVal instanceof Date || keyVal instanceof DateTime
               || keyVal instanceof Long || keyVal instanceof Decimal || keyVal instanceof Double
               || keyVal instanceof Time || keyVal instanceof Id){
                   originalJSON.put(key, keyVal);
               }else{
                   mixJSON((Map<String, Object>)originalJSON.get(key), (Map<String, Object>)newJSON.get(key)); 
               }
        }  
    }

    /**
    *   This method generates a workflow json based on the workflow template and current doc's secondary format setting
    */
    public String generate(){

        // scan user setting to incorporate those changes into the custom workflow
        parseUserSetting();

        // deserializing the template json
        Map<String, Object> template = (Map<String, Object>) JSON.deserializeUntyped(workflowTemplate.toString());

        // list of tasks in the workflow
        List<Object> tasks = (List<Object>) template.get(WORKFLOW_JSON_KEY_TASKS);

        // list of tasks to be included
        List<Object> customWorkflowTasks = new List<Object>();

        // constructing custom tasks
        for(Object task : tasks){

            // deserializing task object
            Map<String, Object> taskProperties = (Map<String, Object>) JSON.deserializeUntyped( JSON.serialize(task) );

            // identifying task type
            String taskType = (String) taskProperties.get(WORKFLOW_JSON_KEY_TYPE);

            if(taskType == WORKFLOW_TASK_TYPE_CONVERT_TO_PDF){

                customWorkflowTasks.add(applyCustomSettings((Map<String, Object>)task));

            }

            if( taskType == WORKFLOW_TASK_TYPE_COMBINE_PDF &&
                !String.isBlank(profilePageInclusionType) && profilePageInclusionType != SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_EXCLUDE ){

                customWorkflowTasks.add(task);

            }

            if( taskType == WORKFLOW_TASK_TYPE_ADD_WATERMARK &&
                !String.isBlank(watermarkText) ){

                customWorkflowTasks.add( getDynamicWatermarkWorkflow(taskProperties) );
            }

            if( taskType == WORKFLOW_TASK_TYPE_STAMP_PDF &&
                stampPdf != null ){

                customWorkflowTasks.add(task);
            }

            if( taskType == WORKFLOW_TASK_TYPE_FIND_AND_REPLACE_WORDS &&
                mergeFields != null &&
                mergeFields.size() > 0 ){

                List< Map<String, String> > items = getFindAndReplaceItems();

                if( items.size() > 0 ){

                    taskProperties.put(WORKFLOW_JSON_KEY_ITEMS, items);

                    customWorkflowTasks.add(taskProperties);
                }
            }

            if( taskType == WORKFLOW_TASK_TYPE_SET_PRINT_SCRIPT &&
                !String.isBlank(printText) ){

                String scriptContent = (String) taskProperties.get(WORKFLOW_JSON_KEY_SCRIPT_CONTENT);

                scriptContent = scriptContent.replace(PRINT_SCRIPT_PRINT_TEXT_REPLACEMENT_TOKEN, printText)
                                             .replace(PRINT_SCRIPT_DATE_FORMAT_REPLACEMENT_TOKEN, printDateTimeFormat)
                                             .replace(PRINT_SCRIPT_EXPIRATION_INTERVAL_REPLACEMENT_TOKEN, String.valueOf(expirationInterval) );

                taskProperties.put(WORKFLOW_JSON_KEY_SCRIPT_CONTENT, scriptContent);

                if( printScriptPositionMap != null ){
                    taskProperties.put(WORKFLOW_JSON_KEY_PRINT_SCRIPT_POSITION, printScriptPositionMap);
                }

                customWorkflowTasks.add(taskProperties);

            }

        }

        // holds the final customized workflow
        Map<String, List<Object>> customWorkflow = new Map<String,List<Object>> {
            WORKFLOW_JSON_KEY_TASKS => customWorkflowTasks
        };

        return JSON.serialize(customWorkflow);
    }

    /**
    *   This method is used to scan through the secondary format setting selected by the user
    *   and set associated properties for this class
    */
    private void parseUserSetting(){

        if( userSetting != null ){

            /* date format setting */
            dateFormat = userSetting.Date_Format__c;

            /* datetime format setting */
            dateTimeFormat = userSetting.DateTime_Format__c;

            /* profile page merge setting  */
            profilePageInclusionType = userSetting.Profile_Page_Inclusion_Type__c;

            // check if user wants to combine pdf
            if(userSetting.Profile_Page_Inclusion_Type__c != SECONDARY_FORMAT_SETTING_PROFILE_PAGE_INCLUSION_TYPE_EXCLUDE &&
                !String.isBlank(userSetting.Profile_Page__c) ) {

                // getting the profile page content as pdf which will be merged with the new secondary content
                PageReference profilePage = getCustomPageReference(userSetting.Profile_Page__c, userSetting.Page_Namespace__c, userSetting.Profile_Page_Parameter__c);

                profilePageAsPDF = Test.isRunningTest() ? Blob.valueOf(MOCK_PROFILE_PAGE_CONTENT) : profilePage.getContentAsPdf();

            }

            /* add watermark setting */
            watermarkText = replaceFieldsWithValues( userSetting.Watermark__c );

            /* stamp pdf setting */
            String stampingResource = userSetting.Stamping_PDF__c;

            if( !String.isBlank(stampingResource) ){

                // fetching stamping pdf from static resource

                stampPdf = Test.isRunningTest() ? Blob.valueOf(MOCK_STAMP_PDF_CONTENT) : getResourceContent(stampingResource);
            }

            /** extracting merge fields */
            mergeFields = new List<String>();

            if( !String.isBlank(userSetting.Merge_Fields_Set_1__c) ){
                mergeFields.addAll( userSetting.Merge_Fields_Set_1__c.split('[,]') );   // splitting by ',' or newline
            }

            if( !String.isBlank(userSetting.Merge_Fields_Set_2__c) ){
                mergeFields.addAll( userSetting.Merge_Fields_Set_2__c.split('[,]') );
            }

            /** add print script setting */
            if(userSetting.Enable_Stamping_When_Printed__c){

                printText = userSetting.Text_To_Stamp_When_Printed__c;

                if(!String.isBlank(printText)){

                    if( !String.isBlank(userSetting.Print_Stamp_Coordinate__c) ){

                        String[] cSpace = userSetting.Print_Stamp_Coordinate__c.split(',');

                        if( cSpace.size() > 3){

                            printScriptPositionMap = new Map<String, Decimal>{

                                PRINT_SCRIPT_COORDINATE_KEY_LEFT => Decimal.valueOf(cSpace.get(0).trim()),
                                PRINT_SCRIPT_COORDINATE_KEY_TOP => Decimal.valueOf(cSpace.get(1).trim()),
                                PRINT_SCRIPT_COORDINATE_KEY_RIGHT => Decimal.valueOf(cSpace.get(2).trim()),
                                PRINT_SCRIPT_COORDINATE_KEY_BOTTOM => Decimal.valueOf(cSpace.get(3).trim())
                            };
                        }
                    }

                    printDateTimeFormat = String.isBlank(userSetting.Print_Stamp_DateTime_Format__c) ? DATETIME_DEFAULT_FORMAT : userSetting.Print_Stamp_DateTime_Format__c;
                    expirationInterval = userSetting.Print_Stamp_Expiration__c == null ? DEFAULT_EXPIRATION_INTERVAL : Integer.valueOf(userSetting.Print_Stamp_Expiration__c);
                }
            }

        }
    }

    /**
    * return the page reference for a custom page with id set with the given id
    * @param pageName - name of the page whose pagereference is to be fetched
    * @param namespace - namespace prefix of the page
    * @param recordId - id parameter value for the page
    * @param parameters - additional parameters to be supplied to the page
    * @return pagereference to the custom page with current doc's id
    */
    private PageReference getCustomPageReference(String pageName, String namespace, String parameters){

        List<ApexPage> pgs;
        if( String.isBlank(namespace) ){
            // assign default namespace
            namespace = 'c';
            pgs = [SELECT Id FROM ApexPage WHERE Name = :pageName];
        }else{
            pgs = [SELECT Id FROM ApexPage WHERE Name = :pageName AND NamespacePrefix = :namespace];
        }

        if( pgs.size() == 0 ){

            throw new SQX_ApplicationGenericException( Label.SQX_ERR_MSG_PAGE_NOT_FOUND.replaceAll('\\{PAGE\\}',pageName) );

        }

        String host = '';

        if( ApexPages.currentPage() != null ){
            host = 'https://' + ApexPages.currentPage().getHeaders().get('Host');
            // replacing the host instance's namespace with the namespace provided
            // hack for 404 Dispatcher issue
            host = host.replace(SQX.NSPrefix.substringBefore('__'), namespace);
        }else{
            pageName = namespace + '__' + pageName;
        }

        /*
            Using full url for page reference rather than partial url as session id was getting changed for partial url
            which was causing an error, sort of a HACK
        */
        String refUrl = host + '/apex/' + pageName;

        if(!String.isBlank(parameters)){
            refUrl += '?' + parameters;
        }

        PageReference customPage = new PageReference(refUrl);
        customPage.getParameters().put('id', doc.Id);

        return customPage;
    }

    /**
    *   This method returns the content of the static resource of the given name
    *   @param resourceName - name of the static resource
    *   @return body of the resource
    */
    private Blob getResourceContent(String resourceName){

        StaticResource resource;
        try{

            // surrounding by try/catch such that meaningful error message can be returned in case such resource doesn't exist

            resource = [ SELECT Body FROM StaticResource WHERE Name = : resourceName ];

        }catch(System.QueryException e){
            throw new SQX_ApplicationGenericException( Label.SQX_ERR_MSG_STATIC_RESOURCE_NOT_FOUND.replaceAll('\\{RESOURCE\\}', resourceName));
        }

        return resource.Body;
    }

    /**
    *   This method returns a list of items (search word and replacement text) to be included in the workflow
    *   @return list of objects that represent search word and replacement texts ( size <= 10,  REASON : Easypdf only supports 10 of such items)
    */
    private List< Map<String, String> > getFindAndReplaceItems(){

        constructReplacementMapper();

        List< Map<String, String> > items = new List< Map<String, String> >();

        for( String mergeField : mergeFields ){

            String searchWord = mergeField.trim()
                                            .toLowerCase();

            if( replacementMapper.containsKey( searchWord ) ){

                String replacementText = replacementMapper.get( searchWord );

                items.add( new Map<String, String> {

                        WORKFLOW_JSON_KEY_SEARCHWORD => '{!' + searchWord + '}',
                        WORKFLOW_JSON_KEY_REPLACEMENTTEXT => replacementText
                    }
                );
            }

            // EasyPDF doesn't support more than 10 items for find and replace
            if( items.size() == FIND_AND_REPLACE_ITEMS_LIMIT ){
                break;
            }
        }

        return items;
    }

    /**
    *   This method sets configurable properties of watermark based on custom metadata
    *   @param taskProperties - holds the task for adding watermark to the custom workflow
    *   @return configured watermark workflow object
    */
    private Map<String, Object> getDynamicWatermarkWorkflow( Map<String, Object> taskProperties ){

        final String    hPositionPrefix = 'PRC_WMARK_HPOS_',
                        vPositionPrefix = 'PRC_WMARK_VPOS_',
                        zOrderPrefix = 'PRC_WMARK_ZORDER_',
                        zOrder_Top = 'TOP',
                        zOrder_Bottom = 'BOTTOM';

        /* add watermark text */
        taskProperties.put( WATERMARK_JSON_KEY_TEXT, watermarkText);

        /* add color settings, if any */
        if( !String.isBlank(userSetting.Watermark_Color__c) ){

            String[] rgba = userSetting.Watermark_Color__c.split(',');
            Map<String, Integer> colorMap = new Map<String, Integer> {
                WATERMARK_JSON_KEY_COLOR_RED => Math.mod( Integer.valueOf( rgba.get(0) ) , 256 ) ,
                WATERMARK_JSON_KEY_COLOR_GREEN => Math.mod( Integer.valueOf( rgba.get(1) ) , 256 ),
                WATERMARK_JSON_KEY_COLOR_BLUE => Math.mod( Integer.valueOf( rgba.get(2) ) , 256 )
            };

            Integer opacity = Integer.valueOf( Double.valueOf(rgba.get(3)) * 100 ); // converting normalized opacity to percentage

            taskProperties.put( WATERMARK_JSON_KEY_COLOR, colorMap );
            taskProperties.put( WATERMARK_JSON_KEY_OPACITY, opacity );
        }

        /* add custom text size, if any */
        if( userSetting.Watermark_Text_Size__c != null ){

            taskProperties.put( WATERMARK_JSON_KEY_FONTSIZE, Integer.valueOf( userSetting.Watermark_Text_Size__c) );
        }

        /* set watermark position, if any */
        if( !String.isBlank(userSetting.Watermark_Horizontal_Position__c) ){

            String hPosition = hPositionPrefix + userSetting.Watermark_Horizontal_Position__c.toUpperCase();    // converting to workflow grammar
            taskProperties.put( WATERMARK_JSON_KEY_HPOSITION, hPosition );
        }
        if( !String.isBlank(userSetting.Watermark_Vertical_Position__c) ){

            String vPosition = vPositionPrefix + userSetting.Watermark_Vertical_Position__c.toUpperCase();  // converting to workflow grammar
            taskProperties.put( WATERMARK_JSON_KEY_VPOSITION, vPosition );
        }

        /* set watermark orientation/angle, if any  */
        if( userSetting.Watermark_Angle__c != null ){

            taskProperties.put( WATERMARK_JSON_KEY_ANGLE, userSetting.Watermark_Angle__c );
        }

        /* set watermark's z-order, if any */
        if( !String.isBlank(userSetting.Watermark_Z_Order__c) ){

            String zOrderSuffix = ( userSetting.Watermark_Z_Order__c == WATERMARK_ZORDER_SEND_TO_BACK ) ? zOrder_Bottom : zOrder_Top;
            String zOrder = zOrderPrefix + zOrderSuffix;   // converting to workflow grammar
            taskProperties.put( WATERMARK_JSON_KEY_ZORDER, zOrder );
        }

        return taskProperties;

    }

    /**
    *   This method takes in a string and replaces all the references of field(api) names with their respective values
    *   @param text - string to be modified
    *   @return string with field values replaced from the argument string
    */
    private String replaceFieldsWithValues(String text){

        if( !String.isBlank(text) ){

            // set replacement mapper
            constructReplacementMapper();

            for( String searchWord : replacementMapper.keySet() ){

                // defining the search pattern
                String pattern = '(?i)\\{!<field>\\}';

                String target = pattern.replaceAll('<field>', searchWord);
                String replacement = replacementMapper.get(searchWord).left(WATERMARK_TEXT_MAX_LENGTH);

                text = text.replaceAll( target, replacement );

            }
        }

        return text;
    }

    /*
    *   This method sets a map of field name ( in VF access pattern) and corresponding value of the field for the given ctrl doc record
    *   Ex:
    *       {!Name} => DOC1
    *       {!Status__c} => Current
    */
    private void constructReplacementMapper(){

        if( replacementMapper == null){

            replacementMapper = new Map<String, String>();

            // fetching complete record ( including relationship field values )
            SObject record = getCompleteRecord();

            // getting all the fields from controlled document object
            Map<String, Schema.SObjectField> fieldMap = record.getSObjectType().getDescribe().fields.getMap();

            /*
            *   Fields that are ignored for replacement :
            *       -> Long text area fields
            *       -> In Accessible fields
            *       -> Lookup(reference) fields whose parent object doesn't have 'Name' field ( e.g. Case)
            */

            for(String field : fieldMap.keySet()){

                Schema.SObjectField sField = fieldMap.get(field);    // field map key set contain field names in lower case

                Schema.DescribeFieldResult sFieldDescribe = sField.getDescribe();

                Schema.DisplayType type = sFieldDescribe.getType();

                if( type == Schema.DisplayType.TEXTAREA ||
                    !sField.getDescribe().isAccessible() ){

                    continue;
                }

                // trimming namespace from field name before replacing the field name
                String target = field.replace(SQX.NSPrefix, '');

                String replacement = '';

                /* formatting the field value based on its type */

                try{

                    /*
                        If field is of date or date time type, the value is formatted to the format specified in setting
                        If no format has been specified, user's locale context format is used
                    */
                    if( type == Schema.DisplayType.DATE ||
                        type == Schema.DisplayType.DATETIME ){

                        replacement = getFormattedDate( record.get(sField) , type == Schema.DisplayType.DATE );

                    }else if( type == Schema.DisplayType.REFERENCE ){

                        /*
                            If field is of referenced type, the name of the referenced record is fetched for replacement
                            if the referenced object doesn't have 'Name' field, leave the field name as is
                        */
                        replacement = String.valueOf( record.get(sField) );

                        if( !String.isBlank(replacement) ){

                            // get the referenced object
                            SObject refObj = record.getSObject(sField);

                            // check if 'Name' field exists in the referenced type
                            if( refObj.getSObjectType().getDescribe().fields.getMap().containsKey('Name') ){

                                replacement = String.valueOf( refObj.get('Name') );

                            }
                        }

                    }else{

                        replacement = String.valueOf( record.get(sField) );
                    }


                }catch(System.SObjectException ex){
                    // if field hasn't been fetched in the sobject
                    // consume the error and move on
                }

                replacement = replacement == null ? '' : replacement;

                replacementMapper.put( target, replacement);
            }
        }
    }

    /**
    *   This method returns date or datetime string formatted to the user specified format or to user's locale format if no format is specified
    *   @param dtObj - could be date or datetime value
    *   @param isDate - boolean value that specifies whether the given value if of date type or not
    *   @return formatted date or datetime string
    */
    private String getFormattedDate( Object dtObj, Boolean isDate){

        String fmDt = '';

        try{

            if( dtObj != null ){

                if( isDate ){

                    Date dt = (Date) dtObj;

                    if( !String.isBlank(dateFormat) ){


                        /*
                            Converting to datetime field , formatting it to the specified format
                            since date field doesn't support custom format
                        */

                        Datetime dtTm = Datetime.newInstance( dt, Time.newInstance(0,0,0,0) );

                        fmDt = dtTm.format( dateFormat );

                    }else{

                        fmDt = dt.format(); // formatting to user's locale context
                    }

                }else{

                    DateTime dtTm = (DateTime) dtObj;

                    if( !String.isBlank(dateTimeFormat) ){

                        fmDt = dtTm.format( dateTimeFormat );

                    }else{

                        fmDt = dtTm.format();   // formatting to user's locale context
                    }

                }

            }

        }catch(System.StringException ex){

            /* This type of exception is thrown when format() function isn't able to recognize the specified format */

            // throw an elaborate message
            throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_UNRECOGNIZED_DATE_FORMAT.replace('{FORMAT}', ex.getMessage().split(':').get(1) ));

        }

        return fmDt;
    }

    /**
    *   This method returns the complete record data for the given controlled document record
    */
    private SObject getCompleteRecord(){

        Map<Id, SObjectTreeNode> docRecordTree = new  Map<Id, SObjectTreeNode>();

        SObjectDataLoader.serialize( new Set<Id>{ doc.Id },
                                        new SObjectDataLoader.SerializeConfig().translateLabels(),
                                        docRecordTree );
        return docRecordTree.get(doc.Id).data;
    }

}
