/**
 * Test class for General Report records
 */ 
@isTest
public class SQX_Test_8096_General_Report {
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
     * Given: A locked General Report record
     * When: User tires to update the locked record
     * Then: Error Is Thrown
    */
    static testmethod void givenGeneralReportRecord_WhenUserTriesToEditLockedGeneralReportRecord_ThenErrorIsThrown() {
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            Database.SaveResult saveResult;

            // ARRANGE: Create and lock General Report record
            
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            complaint.initiate();
            
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c,SQX_Regulatory_Report__c.SQX_Complaint__c});
            
            SQX_General_Report__c generalReportRecord = new SQX_General_Report__c();
            generalReportRecord.SQX_Regulatory_Report__c = regulatoryReport.Id;
            new SQX_DB().op_insert(new List<SQX_General_Report__c>{generalReportRecord}, new List<SObjectField>{SQX_General_Report__c.SQX_Regulatory_Report__c});
            
            SQX_Submission_History__c submissionHistory = new SQX_Submission_History__c();
            submissionHistory.Submitted_By__c = 'Test797123 User50767';
            submissionHistory.Status__c = SQX_Submission_History.STATUS_COMPLETE;
            submissionHistory.SQX_Complaint__c = complaint.complaint.Id;
            submissionHistory.SQX_General_Report__c = generalReportRecord.Id;
            submissionHistory.SQX_Regulatory_Report__c = regulatoryReport.Id;
            new SQX_DB().op_insert(new List<SQX_Submission_History__c>{submissionHistory}, 
                                   new List<SObjectField>{SQX_Submission_History__c.Submitted_By__c, SQX_Submission_History__c.SQX_Complaint__c, SQX_Submission_History__c.SQX_General_Report__c, SQX_Submission_History__c.SQX_Regulatory_Report__c});
            
            regulatoryReport.Status__c = 'Complete';
            regulatoryReport.SQX_Submission_History__c = submissionHistory.Id;
            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c> { regulatoryReport }, new List<SObjectField> { SQX_Regulatory_Report__c.Status__c });
            
            // ACT : Editing the record after locking
            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_General_Report__c> { generalReportRecord }, new List<SObjectField> ())[0];
            
            // ASSERT: Error is thrown
            System.assertEquals(false, saveResult.isSuccess(), 'Expected update to fail');
            System.assert(SQX_Utilities.checkErrorMessage(saveResult.getErrors(), 'Record locked. Cannot perform the desired action.'), 'Expected error message not found. Instead found ' + saveResult.getErrors());

        }
    }

    /**
     * GIVEN: General Report record linked with Regulatory Report having Reg Body TGA (Australia) 
     * WHEN: createSubmissionRecord() method is invoked for the General Report record
     * THEN: A Submission History record should be created for that General Report record
     */
    static testMethod void givenGeneralReportRecord_WhenMethodToCreateManualSubmissionReportIsInvoked_ThenSubmissionHistoryRecordShouldBeCreated(){
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            
            // ARRANGE: Create General Report record with required related record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            complaint.complaint.Country_of_Origin__c = 'AU';
            complaint.save();            
            complaint.initiate();
            
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'AU-90 Day Reportable Test76812';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA;
            
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c, SQX_Regulatory_Report__c.Name, SQX_Regulatory_Report__c.Reg_Body__c, SQX_Regulatory_Report__c.SQX_Complaint__c});            
            
            SQX_General_Report__c generalReport = new SQX_General_Report__c();
            generalReport.SQX_Regulatory_Report__c = regulatoryReport.Id;
            new SQX_DB().op_insert(new List<SQX_General_Report__c>{generalReport}, new List<SObjectField>{SQX_General_Report__c.SQX_Regulatory_Report__c});
            
            // ARRANGE: Create contentdocument for medwatch
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='General-Report-Attachment';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with General Report');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // ARRANGE: link it with general report
            ContentDocumentLink lnk = new ContentDocumentLink (LinkedEntityId =generalReport.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED);
            
            insert lnk;
            
            // ACT: Invoke createSubmissionRecord() method for that General Report record
            SQX_Submission_History.createSubmissionRecord(generalReport.Id);
            
            
            List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id, SQX_General_Report__c, Status__c FROM SQX_Submission_History__c WHERE SQX_General_Report__c =:generalReport.Id ];
            
            // Assert: Get content Document Link
            List<ContentDocumentLink> links = [SELECT Id,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =:subHistoryRecordList[0].Id];
            
            // Assert: The links should not be null
            System.assertNotEquals(NULL, links);
            
            // ASSERT: Ensure Submission History record is created for that General Report record
            System.assertEquals(true, subHistoryRecordList.size()>0, 'Submission record for Manual Submission type reports was not created.');
            
            System.assertEquals(SQX_Submission_History.STATUS_COMPLETE, subHistoryRecordList.get(0).Status__c);
            System.assertEquals(generalReport.Id, subHistoryRecordList.get(0).SQX_General_Report__c);
        }
    }

    /**
     * GIVEN: A Regulatory Report record linked with Complaint
     * WHEN: General Report record is created for the Regulatory Report
     * THEN: Record Values from Complaint to General Report record should be mapped
     */
    static testMethod void  givenRegulatoryReportLinkedWithComplaint_WhenGeneralReportIsCreatedForTheRegulatoryReport_ThenRecordValuesFromComplaintToGeneralReportShouldBeMapped(){
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            // ARRANGE: Create Regulatory Report record for Complaint

            Account account = SQX_Test_Account_Factory.createAccount();
            Contact contact = SQX_Test_Account_Factory.createContact(account);
            
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            complaint.complaint.Country_of_Origin__c = 'AU';            
            complaint.complaint.SQX_External_Contact__c = contact.Id;
            complaint.save();            
            complaint.initiate();
            
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'AU-90 Day Reportable Test76812';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA;
            
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c, SQX_Regulatory_Report__c.Name, SQX_Regulatory_Report__c.Reg_Body__c, SQX_Regulatory_Report__c.SQX_Complaint__c});
            
            // ACT: Create General Report record for the Regulatory Report
            SQX_General_Report__c generalReport = new SQX_General_Report__c();
            generalReport.SQX_Regulatory_Report__c = regulatoryReport.Id;
            new SQX_DB().op_insert(new List<SQX_General_Report__c>{generalReport}, new List<SObjectField>{SQX_General_Report__c.SQX_Regulatory_Report__c});
            
            SQX_General_Report__c generalReportRecord = [SELECT Id, Date_of_Incident__c, Incident_Detail__c, Reported_Date__c, SQX_Primary_Part__c, Reporter_Name__c
                                                         FROM SQX_General_Report__c WHERE Id=:generalReport.Id];
            
            // ASSERT: Ensure record values from Complaint is mapped to General Report record
            System.assertEquals( complaint.complaint.Occurrence_Date__c, generalReportRecord.Date_of_Incident__c, 'Occurrence Date was not mapped with Date of Incident.');
            System.assertEquals(complaint.complaint.Description_As_Reported__c, generalReportRecord.Incident_Detail__c, 'Description As Reported was not mapped with Incident Detail.');
            System.assertEquals(complaint.complaint.Reported_Date__c, generalReportRecord.Reported_Date__c, 'Report Date was not mapped with Reported Date.' );
            System.assertEquals(complaint.complaint.SQX_Part__c, generalReportRecord.SQX_Primary_Part__c, 'Part was not mapped with Primary Part.' );
            System.assertEquals(contact.LastName, generalReportRecord.Reporter_Name__c, 'Contact was not Mapped to Reporter Name');
                      
        }
    }
    
    /**
     * GIVEN: General Report record linked with Followup Regulatory Report having Reg Body TGA (Australia) 
     * WHEN: createSubmissionRecord() method is invoked for the General Report record
     * THEN: A Submission History record should be created for that General Report record and isFollowup should be set
     * to true and report number should be populated with the followup number
     */
    static testMethod void givenGeneralFollowupRegulatoryReport_WhenMethodToCreateManualSubmissionReportIsInvoked_ThenIsFollowupAndReportNumberShouldBeSetInTheSubmissionRecord(){
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            
             // ARRANGE:reate Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            complaint.complaint.Country_of_Origin__c = 'AU';
            complaint.save();            
            complaint.initiate();
            
            // ARRANGE: Create submissionRecord for parent regulatory report.
            SQX_Submission_History__c submissionRecord = new SQX_Submission_History__c(
                Status__c = 'Complete',
                SQX_Complaint__c = complaint.complaint.Id,
                Submitted_By__c= UserInfo.getName(),
                SQX_Submitted_By__c = UserInfo.getUserId(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA,
                Submitted_Date__c=System.today(),
                Name = 'AU-90 Day Reportable Test76812',
                Due_Date__c = Date.today() + 2
            );
            
            insert submissionRecord;
            
            //ARRANGE: Create parent regulatory report
            SQX_Regulatory_Report__c regulatoryReportParent = new SQX_Regulatory_Report__c();
            regulatoryReportParent.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReportParent.Due_Date__c =  Date.today() + 2;
            regulatoryReportParent.Status__c = 'Complete';
            regulatoryReportParent.compliancequest__SQX_Submission_History__c=submissionRecord.Id;
            insert regulatoryReportParent;
            
            //ARRANGE: Create followup regulatory report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'AU-90 Day Reportable Test76812';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA;
            regulatoryReport.SQX_Follow_Up_Of__c = regulatoryReportParent.Id;
            regulatoryReport.compliancequest__Follow_Up_Number_Internal__c =3;
            
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c, SQX_Regulatory_Report__c.Name, SQX_Regulatory_Report__c.Reg_Body__c, SQX_Regulatory_Report__c.SQX_Complaint__c});            
            
            SQX_General_Report__c generalReport = new SQX_General_Report__c();
            generalReport.SQX_Regulatory_Report__c = regulatoryReport.Id;
            new SQX_DB().op_insert(new List<SQX_General_Report__c>{generalReport}, new List<SObjectField>{SQX_General_Report__c.SQX_Regulatory_Report__c});
           
            // ACT: Invoke createSubmissionRecord() method for that General Report record
            SQX_Submission_History.createSubmissionRecord(generalReport.Id);
            
            // ASSERT: Query submissionHistory
            List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id, SQX_General_Report__c, Status__c,Report_Number__c,
                                                                    Is_Follow_Up__c FROM SQX_Submission_History__c WHERE SQX_General_Report__c =:generalReport.Id ];
            // ASSERT: The report number and is followup should be set.
            System.assertEquals('003', subHistoryRecordList[0].Report_Number__c);
            System.assertEquals(true, subHistoryRecordList[0].Is_Follow_Up__c);
        }
    }

    /**
     * GIVEN: General record.
     * WHEN:  When user other than Report creator adds attachments.
     * THEN: A Submission should not fail.
     */
     static testMethod void givenGeneralRecord_WhenUserOtherThanReportCreatorAddsAttachments_ThenSubmissionShouldNotFail(){
        
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER);
         
        SQX_General_Report__c generalReport = new SQX_General_Report__c();
        SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
         
        SQX_Test_Complaint complaint; 
         
        System.runAs(adminUser) {
            
            // ARRANGE:Create Complaint and initiate
            complaint= new SQX_Test_Complaint(adminUser);
            
            complaint.save();
            
            complaint.addPolicy(true,Date.today(),standardUser.Id);
            complaint.initiate();
            
        }
        
        system.runAs(standardUser){
            
            // ARRANGE: Create regulatory Report.
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            
			insert regReport;
           
           
            generalReport.SQX_Regulatory_Report__c=regReport.Id;
            
            // ARRANGE: Create general report and link with regulatory report.
    		insert generalReport;
            
            regReport.Report_Id__c = generalReport.Id;
            update regReport;
        }
        
        
        system.runas(adminUser){
            
            // ARRANGE: Create file content that goes in attachment section for general report.
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='General-File';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with General');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // ARRANGE: link it with general report
            ContentDocumentLink link = new ContentDocumentLink (LinkedEntityId =generalReport.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            
            insert link;
        }
         
        
        ContentVersion cv;
        System.runAs(standardUser) {
            
            // ACT: Invoke createSubmissionRecord() method for that General Report record
            SQX_Submission_History.createSubmissionRecord(generalReport.Id);

             cv = new ContentVersion(
                 PathOnClient = 'Test.txt',
                 VersionData = Blob.valueOf('VersionData')
             );
             insert cv;
             
             Id documentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
             
             SQX_Submission_History.setRecordTypeForRegulatoryReport(documentId);
        }
          
         // ASSERT: Query submissionHistory
        List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id FROM SQX_Submission_History__c];
        
        Id expectedRTId = SQX_ContentVersion.getRegulatoryContentRecordType();

         cv = [SELECT RecordTypeId FROM ContentVersion WHERE Id =: cv.Id];
         System.assertEquals(expectedRTId, cv.RecordTypeId, 'Unexpected record type of reg. report');

        // Assert: Get content Document Link
        List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistoryRecordList[0].Id];
        
        // Assert: There should be 1 ContentDocumentLink.
        System.assertEquals(1, links.size());
        
    }
}