@IsTest
public class SQX_Test_Extension_Supplier{

    /**
    * This test ensures that the new controller allows creation of account, contact, services, parts in on go
    */
    public static testmethod void givenAccountAndRelatedDataItCanBeCreated(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,'Standard User Profile', role);
        
        System.runas(newUser){
        
            Account currentAccount = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(currentAccount);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);

            
            ApexPages.StandardController sc = new ApexPages.standardController(currentAccount);
            
            SQX_Extension_Supplier supplierController = new SQX_Extension_Supplier(sc);
            
            
            System.assert(supplierController.getAllContacts().size() > 0, 
                'Expected a new contact to be created but found none');
                   
            Contact contact = (Contact)supplierController.getAllContacts().get(0).baseObject;
            contact.FirstName = 'Ramesh';
            contact.LastName = 'Chowdary';
            contact.Email = 'rams.chowdary@ambarkaar.com';
            
            supplierController.saveAllChanges();
                 
            
            supplierController.removeTemporaryService();
            supplierController.removeTemporaryPart();
            
            
            supplierController.saveAllChanges();
            
            
            System.assert([SELECT ID FROM Contact WHERE Email='rams.chowdary@ambarkaar.com' 
            AND AccountID=:supplierController.getCurrentAccount().ID].size() > 0,'Expected a contact to be created for the account but found none');
        }
        

        
    }
    
    
    /**
    * This test ensures that given an error occurs in the creation of an account 
    * correction of the error can persist the account.
    */
    public static testmethod void givenAccountCreationFailsItCanBeSaved(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,'Standard User Profile', role);
        
        System.runas(newUser){
            Account currentAccount = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(currentAccount);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);


            ApexPages.StandardController sc = new ApexPages.standardController(currentAccount);
            
            SQX_Extension_Supplier supplierController = new SQX_Extension_Supplier(sc);
            
            
            System.assert(supplierController.getAllContacts().size() > 0, 
                'Expected a new contact to be created but found none');
                   
            Contact contact = (Contact)supplierController.getAllContacts().get(0).baseObject;
            contact.FirstName = 'Ramesh';
            contact.LastName = 'Chowdary';
            contact.Email = 'rams.chowdary@ambarkaar.com';
            
            supplierController.getAllSupplierParts();
            supplierController.addNewSupplierPart();
           // supplierController.getAllSupplierServices();
            
           // supplierController.removeTemporaryService();
            
            SQX_Part_Family__c   newPF1;
            newPF1 = new SQX_Part_Family__c();
            newPF1.Name = 'dummyFamily';
            insert newPF1;
                    
            //add a part 
            SQX_Part__c myPart1 = new SQX_Part__c();
            mypart1.Name = 'newPart1';
            mypart1.Part_Number__c = 'prt111' + Math.Random();
            mypart1.Part_Risk_Level__c = 3;
            mypart1.Active__c = false;
            mypart1.Part_Family__c = newPF1.id;
            Insert myPart1;   
            
            
            List<SQX_ObjectWrapper> supplierParts = supplierController.getAllSupplierParts();
            supplierParts.get(0).baseObject.put('Account__c', currentAccount.ID);
            supplierParts.get(0).baseObject.put('Part__c', myPart1.ID);
            supplierParts.get(0).baseObject.put('Active__c', true); //this should ensure that it should fail since part is inactive
            supplierParts.get(0).baseObject.put('Approved__c', false); 
            
            supplierController.saveAllChanges();
            
            List<SQX_Supplier_Part__c> checkSupplierPart = [SELECT ID FROM SQX_Supplier_Part__c WHERE Account__c = : currentAccount.Id];
            
            System.assert(checkSupplierPart.size() == 0, 'Expected no supplier part to be created');
            
            //correct error
            supplierParts = supplierController.getAllSupplierParts();
            supplierParts.get(0).baseObject.put('Active__c', false);
            supplierParts.get(0).baseObject.put('Approved__c', false);
            
            supplierController.saveAllChanges();
            
            checkSupplierPart = [SELECT ID FROM SQX_Supplier_Part__c WHERE Account__c = : currentAccount.Id];
            
            System.assert(checkSupplierPart.size() > 0, 'Expected supplier part to be created. Error: ' 
            + ApexPages.getMessages() + supplierController + myPart1);    
        }
    }
    
    
    /**
    * This test ensures that the new controller allows creation of a new account, contact, services, parts in on go
    */
    public static testmethod void givenANewAccountAndRelatedDataItCanBeCreated(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,'Standard User Profile', role);
        
        System.runas(newUser){
        
            Test.setCurrentPage(Page.SQX_Supplier);
            Account accountToInsert = SQX_Test_Account_Factory.createAccount();
            accountToInsert.Id = null; //this is a new record forget the one that was inserted
            ApexPages.StandardController sc = new ApexPages.standardController(accountToInsert);
            
            SQX_Extension_Supplier supplierController = new SQX_Extension_Supplier(sc);
            
            
            System.assert(supplierController.getAllContacts().size() > 0, 
                'Expected a new contact to be created but found none');
                   
            Contact contact = (Contact)supplierController.getAllContacts().get(0).baseObject;
            contact.FirstName = 'Ramesh';
            contact.LastName = 'Chowdary';
            contact.Email = 'rams.chowdary@ambarkaar.com';

            
            supplierController.removeTemporaryService();
            supplierController.removeTemporaryPart();
            
            supplierController.saveAllChanges();
        }
             

        
        //System.assert([SELECT ID FROM Contact WHERE Email='rams.chowdary@ambarkaar.com' 
        //AND AccountID=:supplierController.getCurrentAccount().ID].size() > 0,'Expected a contact to be created for the account but found none');
        

        
    }
    
}