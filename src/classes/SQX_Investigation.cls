/**
* This class is a wrapper for the various function for investigations
* @author Pradhanta Bhandari
* @date 2014/2/7
*/
public with sharing class SQX_Investigation{
    /*** Status pick list field listing for use in code ***/
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_APPROVED_DRAFT = 'Approved Draft';
    public static final String STATUS_IN_APPROVAL = 'In Approval';
    public static final String STATUS_PUBLISHED = 'Completed',
                               STATUS_VOID = 'Void';
    
    public static final String TYPE_OF_INVESTIGATION_RETAINED_SAMPLE = 'Retained Sample',
                                TYPE_OF_INVESTIGATION_RETURNED_SAMPLE = ' Returned Sample',
                                TYPE_OF_INVESTIGATION_PRODUCT_TRENT = 'Product Trend',
                                TYPE_OF_INVESTIGATION_LOT_TRAINED = 'Lot Trend';
    
    public static final String APPROVAL_STATUS_NONE = '-';
    public static final String APPROVAL_STATUS_PENDING = 'Pending';
    public static final String APPROVAL_STATUS_APPROVED = 'Approved';
    public static final String APPROVAL_STATUS_REJECTED = 'Rejected';
    
    public static final String RECORD_TYPE_COMPLAINT_INVESTIGATION = 'Complaint_Investigation',
                                RECORD_TYPE_INVESTIGATION = 'Investigation';
    
    private SQX_Investigation__c investigation = null;
    
    public SQX_Investigation(SQX_Investigation__c investigation){
        this.investigation = investigation;
    }
    
    
    
    /**
    * clones root causes, action plans, investigation tools
    * returns the cloned object
    */
    public SQX_Investigation__c cloneInvestigationAndRelatedList(){
        /*get the original investigation then clone it*/

        SQX_Investigation__c clonedInvestigation =
             (SQX_Investigation__c)cloneAllFieldsOfAnObject(Schema.SObjectType.SQX_Investigation__c, investigation.Id,
              SQX_Investigation__c.SQX_Original_Investigation__c.getDescribe() , null, investigation.Id, 
              true, new Set<SObjectField>(new SObjectField[] { SQX_Investigation__c.Status__c, SQX_Investigation__c.Approval_Status__c } ))[0];
        
        //clone action plan
        cloneAllFieldsOfAnObject(Schema.SObjectType.SQX_Action_Plan__c, null, SQX_Action_Plan__c.SQX_Investigation__c.getDescribe(),
            investigation.Id, clonedInvestigation.Id, true, 
            new Set<SObjectField>(new SObjectField[] {SQX_Action_Plan__c.Approved__c} ));

        //clone root causes
        cloneAllFieldsOfAnObject(Schema.SObjectType.SQX_Root_Cause__c, null, SQX_Root_Cause__c.SQX_Investigation__c.getDescribe(),
         investigation.Id, clonedInvestigation.Id, true, null);


        //clone investigation tools
        cloneAllFieldsOfAnObject(Schema.SObjectType.SQX_Investigation_Tool__c, null, SQX_Investigation_Tool__c.SQX_Investigation__c.getDescribe(),
         investigation.Id, clonedInvestigation.Id, true, null);

        return clonedInvestigation;
    }


    /**
    * this method clones all the custom field of an object or a child of the object(parent) 
    * @author Pradhanta Bhandari
    * @date 2014/2/20
    * @param objectDescription the object type which is to be cloned
    * @param objectId the Id of the object to clone if single object is to be cloned
    * @param parentRelationshipField the field which relates the child objects to parent object, if child objects are to be cloned
    * @param parentId if child objects are to be cloned, pass the parent objects Id
    * @param newParentId the id of new parent
    * @param cloneNameField <code>true</code>if name field has to be cloned. the only standard field that is cloned
    * @param ignoreFields the fields to be ignored
    * @return returns the list of object that have been inserted
    */
    private static SObject[] cloneAllFieldsOfAnObject(Schema.DescribeSObjectResult objectDescription, Id objectId, 
        Schema.DescribeFieldResult parentRelationshipField, Id parentId, Id newParentId, boolean cloneNameField,
         Set<Schema.SObjectField> ignoreFields){

        List<SObject> objectsToBeInserted = new List<SObject>();
        List<SObjectField> fieldsToSet = new List<SObjectField>();


        Map<String, Schema.SObjectField> fieldMap = objectDescription.fields.getMap();

        String dynamicSOQL = 'SELECT ';

        if(cloneNameField)
            dynamicSOQL += ' NAME,';

        
        for(Schema.SObjectField field : fieldMap.values()){
            Schema.DescribeFieldResult fieldDetail = field.getDescribe();

            if(fieldDetail.isCustom()){
                if(ignoreFields != null && ignoreFields.contains(field))
                    continue;

                if(fieldDetail.isCreateable())
                    fieldsToSet.add(field);
                
                dynamicSOQL += fieldDetail.getName() + ',';

            }
        }


        dynamicSOQL = dynamicSOQL.substring(0, dynamicSOQL.length() - 1); //remove trailing comma

        dynamicSOQL += ' FROM ' + objectDescription.getName();

        if(objectId != null){
            dynamicSOQL += ' WHERE Id = \'' + objectId + '\' ORDER BY Name';
        }
        else{
            dynamicSOQL += ' WHERE ' + parentRelationshipField.getName() + ' = \'' + parentId + '\'  ORDER BY Name';
        }


        for(SObject objectToClone : Database.query(dynamicSOQL)){
            SObject clonedObject = objectToClone.clone();

            if(newParentId != null)
                clonedObject.put(parentRelationshipField.getName(), newParentId);

            objectsToBeInserted.add(clonedObject);
        }

        if(objectsToBeInserted.size() > 0){
            new SQX_DB().op_insert(objectsToBeInserted, fieldsToSet);
        }

        return objectsToBeInserted;
    }

    /**
     * calculate due date of action plan based upon the due date or days required of the action plan
     * @param actionPlan action plan from which action due date is to be calculated
     * @return calculated due date
     */
    public static Date calculateActionDueDate (SQX_Action_Plan__c actionPlan){
        Date calculatedDate;

        if(actionPlan.Due_Date__c != null){
            calculatedDate = actionPlan.Due_Date__c;
        } else {
            if(actionPlan.Days_Required__c != null){
                calculatedDate = Date.Today().addDays(Integer.valueOf(actionPlan.Days_Required__c));
            }
        }

        return calculatedDate;
    }


    /**
    * This class contains all the static methods that are designed to be executed as a task in trigger.
    * @author Pradhanta Bhandari
    * @date 2014/5/5
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(List<SQX_Investigation__c> newInvestigations, Map<Id, SQX_Investigation__c> oldInvestigations){
            super(newInvestigations, oldInvestigations);
        }

        /**
        * This trigger handler approves action plan and creates required actions.
        * @author Pradhanta Bhandari
        * @date 2014/5/5
        * @story SQX-12
        */
        public Bulkified onPublishingApproveActionPlanAndCreateActions(){

            //get all published approved responses in this transaction
            Rule publishedAndNotRejectedRule = new Rule();
            publishedAndNotRejectedRule.addRule(Schema.SQX_Investigation__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Investigation.STATUS_PUBLISHED);
            publishedAndNotRejectedRule.addRule(Schema.SQX_Investigation__c.Approval_Status__c, RuleOperator.NotEquals,
                 (Object) SQX_Investigation.APPROVAL_STATUS_REJECTED);
            
            this.applyFilter(publishedAndNotRejectedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            if(this.view.size() > 0){
                List<Id> allPublishedApprovedInvestigation = new List<Id>(new Map<Id,sObject>(this.view).keySet());
                //get the findings
                List<SQX_Action_Plan__c> actionPlansToUpdate = new List<SQX_Action_Plan__c>();
                List<SQX_Action__c> actionsToCreate = new List<SQX_Action__c>();
                
                for(SQX_Investigation__c investigation : 
                        [SELECT Id, OwnerId, SQX_Finding__c, SQX_Finding__r.Name, SQX_Finding__r.Is_Primary_Finding__c, SQX_Finding__r.SQX_Capa__c,SQX_Finding__r.SQX_Audit__c, SQX_Capa__c,
                            (SELECT Approved__c,Completed__c,Completion_Date__c, SQX_User__c,
                                Description__c,Due_Date__c,Id,Plan_Number__c,Plan_Type__c, Days_Required__c
                                FROM SQX_Action_Plans__r)
                         FROM SQX_Investigation__c
                         WHERE Id IN : allPublishedApprovedInvestigation ]){
                         
                         //always approve all action plans
                         for(SQX_Action_Plan__c actionPlan : investigation.SQX_Action_Plans__r){
                             actionPlan.Approved__c = true;
                             actionPlansToUpdate.add(actionPlan);
                             
                             //spawn actions in capa if necessary or 
                             //if capa is defined. For NC a CAPA is not required.
                             //SQX-715 created for the bug.
                             if(investigation.SQX_Capa__c != null || 
                                    investigation.SQX_Finding__r.SQX_Audit__c != null){
                                
                                SQX_Action__c action = new SQX_Action__c();
                                if(investigation.SQX_Capa__c != null){                                   
                                    action.SQX_Capa__c = investigation.SQX_Capa__c;
                                }
                                else if(investigation.SQX_Finding__r.SQX_Audit__c != null){                      
                                    action.SQX_Audit__c = investigation.SQX_Finding__r.SQX_Audit__c;
                                }

                                action.SQX_Action_Plan__c = actionPlan.ID;
                                action.Due_Date__c = calculateActionDueDate(actionPlan);
                                action.Description__c = actionPlan.Description__c;
                                action.Plan_Number__c = actionPlan.Plan_Number__c;
                                action.Plan_Type__c = actionPlan.Plan_Type__c;
                                action.Is_Approved__c = true;
                                action.Completion_Date__c = actionPlan.Completion_Date__c;
                                action.Status__c = actionPlan.Completed__c ? SQX_Implementation_Action.STATUS_COMPLETE : SQX_Implementation_Action.STATUS_OPEN;
                                action.OwnerId = investigation.OwnerId; 
                                action.SQX_User__c = actionPlan.SQX_User__c; //added user/assignee to action plan and action. On approval the field must be copied.

                                actionsToCreate.add(action);
                             }
                         }
                 }
                 
                 if(actionPlansToUpdate.size() > 0){
                    // response approver may only have read permission on the action plan objects
                    new SQX_DB().withoutSharing().op_update(actionPlansToUpdate, new List<SObjectField>{ SQX_Action_Plan__c.fields.Approved__c });
                     //update actionPlansToUpdate;
                 }

                 if(actionsToCreate.size() > 0){
                    new SQX_DB().op_insert(actionsToCreate, new List<SObjectField>{ 
                        SQX_Action__c.fields.SQX_Capa__c,
                        SQX_Action__c.fields.SQX_Audit__c,
                        SQX_Action__c.fields.SQX_Action_Plan__c,
                        SQX_Action__c.fields.Due_Date__c,
                        SQX_Action__c.fields.Description__c,
                        SQX_Action__c.fields.Plan_Number__c,
                        SQX_Action__c.fields.Plan_Type__c,
                        SQX_Action__c.fields.Is_Approved__c,
                        SQX_Action__c.fields.Completion_Date__c,
                        SQX_Action__c.fields.Status__c,
                        SQX_Action__c.fields.OwnerId,
                        SQX_Action__c.fields.SQX_User__c
                        });
                }
            }

            return this;
        }
        
        /**
        * This method defaults the investigat ion's account and primary contact from finding
        * account and investigator contact is copied to allow high volume sharing if and when used.
        * @author Pradhanta Bhandari
        */
        public Bulkified onCreationDefaultAccountAndContact(){
            Set<Id> allFindingIds = new Set<Id>();

            // Get information of parents account

            for(SQX_Investigation__c investigation : (List<SQX_Investigation__c>)Trigger.New){
                investigation.SQX_Account__c = investigation.Parent_Account_Id__c;
                investigation.SQX_Primary_Contact__c = investigation.Parent_Contact_Id__c;
            } 
            
            return this;
        }

        /**
        * This method updates the completed date on investigation of complaint.
        * @author Anish Shrestha
        * @date 2015-12-24
        * @story SQX-1758
        */
        public Bulkified updateInvestigationOnComplaint(){

            final String ACTION_NAME = 'updateInvestigationOnComplaint';

            for(SQX_Investigation__c investigation: (List<SQX_Investigation__c>)this.view) {
                if(investigation.SQX_Complaint__c != null && investigation.Status__c == STATUS_PUBLISHED){
                    investigation.Completed_On__c = Date.Today();
                }
            }

            return this;
        }


        /**
        * Method to complete complaint task when investigation is complete
        */
        public Bulkified completeComplaintTask(){
            final String ACTION_NAME = 'completeComplaintTask';
            //get all completed Investigation in this transaction
            Rule completedInvestigationRule = new Rule();
            completedInvestigationRule.addRule(Schema.SQX_Investigation__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Investigation.STATUS_PUBLISHED);

            this.resetView()
                .removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)//Remove all the previously processed records
                .applyFilter(completedInvestigationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Investigation__c> investigations = (List<SQX_Investigation__c>) completedInvestigationRule.evaluationResult;

            if(investigations.size() > 0){
                this.addToProcessedRecordsFor(SQX.Investigation, ACTION_NAME, investigations);

                Set<Id> complaintIds  = this.getIdsForField(investigations,Schema.SQX_Investigation__c.SQX_Complaint__c); 
                Set<Id> cqTaskIds  = this.getIdsForField(investigations,Schema.SQX_Investigation__c.SQX_Task__c); 

                //complete related complaint tasks
                SQX_Complaint_Task.completeComplaintTasks(complaintIds, cqTaskIds);
                
            }

            return this;
        }
        
        /**
         * This method is to prevent deletion of record if parent is locked
         * @author Sanjay Maharjan
         * @date 2019-01-14
         * @story [SQX-7470]
         */
        public Bulkified preventDeletionOfLockedRecords() {
            
            this.resetView();
            
            Map<Id, SQX_Investigation__c> allInvestigations = new Map<Id, SQX_Investigation__c>((List<SQX_Investigation__c>)this.view);
            
            for(SQX_Investigation__c investigation : [SELECT Id FROM SQX_Investigation__c WHERE Id IN : allInvestigations.keySet() AND SQX_Complaint__r.Is_Locked__c =: true]) {
                    allInvestigations.get(investigation.id).addError(Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked);
            }
            
            return this;
            
        }
        
        /**
         * This method updates Complaint Conclusion Code with Primary Investigation's Defect Code if investigation is closed
         * Deletes the Linked Investigation belonging to investigation if investigation is void
         */
        public Bulkified removeAllLinkedInvestigationOnVoid() {
            final String ACTION_NAME = 'removeAllLinkedInvestigationOnVoid';
            Rule investigationRule = new Rule();
            investigationRule.addRule(Schema.SQX_Investigation__c.Status__c, RuleOperator.Equals, STATUS_VOID);
            this.resetView().removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)
                .applyFilter(investigationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(investigationRule.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.Investigation, ACTION_NAME, investigationRule.evaluationResult);
                
                List<SQX_Linked_Investigation__c> linkedInvestigations = [SELECT Id
                                                                            FROM SQX_Linked_Investigation__c
                                                                            WHERE SQX_Investigation__c IN :investigationRule.evaluationResult];
                
                if (linkedInvestigations.size() > 0) {
                    // delete linked investigation of voided investigation
                    new SQX_DB().withoutSharing().op_delete(linkedInvestigations);
                }
            }
            return this;
        }
        
        /**
         * This method will sync Conclusion Code of Complaint with Primary Diagnostic of closed investigation
         * It actually notifies to Linked Investigation that I am being voided, LI will take care of rest.
         */
        public Bulkified syncComplaintConclusionCodeWithPrimaryInvestigationOnClose() {
            final String ACTION_NAME = 'syncComplaintConclusionCodeWithPrimaryInvestigationOnClose';
            Rule investigationRule = new Rule();
            investigationRule.addRule(Schema.SQX_Investigation__c.Status__c, RuleOperator.Equals, STATUS_PUBLISHED);
            this.resetView().removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)
                .applyFilter(investigationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if(investigationRule.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.Investigation, ACTION_NAME, investigationRule.evaluationResult);
                SQX_Linked_Investigation.notifyAboutPrimaryInvestigationCompletion(getIdsForField(investigationRule.evaluationResult, Schema.SQX_Investigation__c.Id));
            }
            return this;
        }
        
        /**
         * before creating investigation, put some of the important fields (Lot Number, Defect Code and Part) from Soruce (Complaint or Associated Item) to Investigaion
         */
        public Bulkified onCreationSetPartDefectAndLotFromSoruce() {
            final String ACTION_NAME = 'updatePartDefectAndLotFromSoruce';
            Rule investigationRule = new Rule();
            investigationRule.addRule(Schema.SQX_Investigation__c.SQX_Complaint__c, RuleOperator.NotEquals, null);
            investigationRule.addRule(Schema.SQX_Investigation__c.SQX_Associated_Item__c, RuleOperator.NotEquals, null);
            investigationRule.operator = RuleCombinationOperator.OpOr;
            this.resetView().removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)
                .applyFilter(investigationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (investigationRule.evaluationResult.size() > 0) {
                Set<Id> complaintIds = getIdsForField(investigationRule.evaluationResult, Schema.SQX_Investigation__c.SQX_Complaint__c);
                Set<Id> associatedItemIds = getIdsForField(investigationRule.evaluationResult, Schema.SQX_Investigation__c.SQX_Associated_Item__c);
                Map<Id,SQX_Complaint__c> complaintsMap = new Map<Id, SQX_Complaint__c>([SELECT Id,SQX_Part__c,SQX_Part_Family__c,Lot_Number__c,SQX_Complaint_Code__c FROM SQX_Complaint__c WHERE Id IN :complaintIds]);
                Map<Id,SQX_Complaint_Associated_Item__c> itemsMap = new Map<Id, SQX_Complaint_Associated_Item__c>([SELECT Id,SQX_Part__c,SQX_Part_Family__c,Lot_Number__c,SQX_Complaint__r.SQX_Complaint_Code__c FROM SQX_Complaint_Associated_Item__c WHERE Id IN :associatedItemIds]);

                for (SQX_Investigation__c investigation : (List<SQX_Investigation__c>)investigationRule.evaluationResult) {
                    // if Investigation has Associated item then always use it else go for its Complaint
                    // since Associated Item does not have defect, so use defect from Complaint of Associated Item in case Associated Item is present in Investigation
                    if (investigation.SQX_Associated_Item__c != null) {
                        investigation.SQX_Defect_Code__c = itemsMap.get(investigation.SQX_Associated_Item__c).SQX_Complaint__r.SQX_Complaint_Code__c;
                        investigation.SQX_Part__c = itemsMap.get(investigation.SQX_Associated_Item__c).SQX_Part__c;
                        investigation.SQX_Part_Family__c = itemsMap.get(investigation.SQX_Associated_Item__c).SQX_Part_Family__c;
                        investigation.Lot_Number__c = itemsMap.get(investigation.SQX_Associated_Item__c).Lot_Number__c;
                    } else {
                        investigation.SQX_Defect_Code__c = complaintsMap.get(investigation.SQX_Complaint__c).SQX_Complaint_Code__c;
                        investigation.SQX_Part__c = complaintsMap.get(investigation.SQX_Complaint__c).SQX_Part__c;
                        investigation.SQX_Part_Family__c = complaintsMap.get(investigation.SQX_Complaint__c).SQX_Part_Family__c;
                        investigation.Lot_Number__c = complaintsMap.get(investigation.SQX_Complaint__c).Lot_Number__c;
                    }  
                }         
            }
            
            return this;
        }
        
        /**
         * Creates Linked Investigation after Investigation is inserted.
         */
        public Bulkified createLinkedInvestigationAfterInvestigationIsCreated() {
            final String ACTION_NAME = 'updatePartDefectAndLotFromSoruce';
            Rule createLinkedInvestigationRule = new Rule();
            createLinkedInvestigationRule.addRule(Schema.SQX_Investigation__c.SQX_Complaint__c, RuleOperator.NotEquals, null);
            createLinkedInvestigationRule.addRule(Schema.SQX_Investigation__c.SQX_Associated_Item__c, RuleOperator.NotEquals, null);
            createLinkedInvestigationRule.operator = RuleCombinationOperator.OpOr;
            this.resetView().removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)
                .applyFilter(createLinkedInvestigationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (createLinkedInvestigationRule.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.Investigation, ACTION_NAME, createLinkedInvestigationRule.evaluationResult);
                List<SQX_Linked_Investigation__c> linkedInvestigationsToInsert = new List<SQX_Linked_Investigation__c>();
                
                for (SQX_Investigation__c inv : [SELECT Id, RecordTypeId, SQX_Complaint__c, SQX_Associated_Item__c FROM SQX_Investigation__c WHERE Id IN :createLinkedInvestigationRule.evaluationResult]) {
                    if (inv.RecordTypeId != null) {
                        linkedInvestigationsToInsert.add( new SQX_Linked_Investigation__c(
                                                    SQX_Complaint__c = inv.SQX_Complaint__c, 
                                                    SQX_Investigation__c = inv.Id, 
                                                    SQX_Associated_Item__c = inv.SQX_Associated_Item__c)
                                                );
                    }
                }
                
                /*
                 * Insert Linked investigation
                 */
                new SQX_DB().op_insert(linkedInvestigationsToInsert, new List<Schema.SObjectField> {
                                                                                            Schema.SQX_Linked_Investigation__c.SQX_Complaint__c,
                                                                                            Schema.SQX_Linked_Investigation__c.SQX_Investigation__c,
                                                                                            Schema.SQX_Linked_Investigation__c.SQX_Associated_Item__c
                                                                                        });
            }
            
            return this;
        }

        /**
	 * Before update, if the investigation is primary investigation of some complaint then prevent update of scope match fields i.e. part family, defect code and part.
 	 */
        public Bulkified preventUpdateOnPrimaryInvestigaton() {
            final String ACTION_NAME = 'preventUpdateOnPrimaryInvestigaton';
            Rule updateRule = new Rule();
            updateRule.addRule(Schema.SQX_Investigation__c.SQX_Part_Family__c, RuleOperator.HasChanged);
            updateRule.addRule(Schema.SQX_Investigation__c.SQX_Defect_Code__c, RuleOperator.HasChanged);
            updateRule.addRule(Schema.SQX_Investigation__c.SQX_Part__c, RuleOperator.HasChanged);
            updateRule.operator = RuleCombinationOperator.OpOr;
            
            this.resetView().removeProcessedRecordsFor(SQX.Investigation, ACTION_NAME)
                .applyFilter(updateRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            if (updateRule.evaluationResult.size() > 0) {
                Map<Id, SQX_Investigation__c> investigatinMap = new Map<Id, SQX_Investigation__c>((List<SQX_Investigation__c>)updateRule.evaluationResult);
                List<SQX_Complaint__c> complaintsWithPrimaryInvestigation = [SELECT Id,Name,SQX_Primary_Investigation__c FROM SQX_Complaint__c WHERE SQX_Primary_Investigation__c IN :investigatinMap.keySet()];

                Set<Id> primaryInvestigationIds = this.getIdsForField(complaintsWithPrimaryInvestigation,Schema.SQX_Complaint__c.SQX_Primary_Investigation__c); 
                if (complaintsWithPrimaryInvestigation.size() > 0) {
                    for (SQX_Complaint__c complaint : complaintsWithPrimaryInvestigation) {
                        SQX_Investigation__c investigation = investigatinMap.get(complaint.SQX_Primary_Investigation__c);
                        investigatinMap.get(complaint.SQX_Primary_Investigation__c).addError(Label.SQX_ERR_MSG_Prevent_Update_Of_Primary_Investigation.replace('{investigationName}', investigation.Name).replace('{complaintName}', complaint.Name));
                    }
                }
            }
            return this;
        }
    }
}