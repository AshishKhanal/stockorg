/*
 * description: test for capa, nc, audit recall
 */
@IsTest
public class SQX_Test_1439_NC_Reopen{

    private static Boolean runAllTests = true,
                            run_ncIsClosed_ClosedNCCanBeReopened = false;

    /**
    *   Setup: create NC, save 
    *   Action: close nc, recall nc and again close nc; 
    *   Expected: nc should be reopened
    *             nc should be able to add Notes, Attachments, modify Risk, recommended actions. 
    *
    * @author Anish Shrestha
    * @date 2015/09/21
    * 
    */
    public static testmethod void ncIsClosed_ClosedNCCanBeReopened(){

        if(!runAllTests && ! run_ncIsClosed_ClosedNCCanBeReopened)
            return;

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        SQX_Test_NC nc = null;

        System.runas(standardUser){
            nc = new SQX_Test_NC(new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                            .setRequired(false,false,false,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(false, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save());

            nc.setDispositionRules(false, null, false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();
            
            nc.finding.synchronize();

            nc.nc.OwnerId = standardUser.Id;

            nc.save();

            SQX_Test_ChangeSet changeSet1 = new SQX_Test_ChangeSet();

            SQX_Nonconformance__c closedNC = [SELECT Id, Closure_Comment__c, Risk_Justification__c FROM SQX_Nonconformance__c WHERE Id =: nc.nc.Id];
            closedNC.Closure_Comment__c = 'CLOSURE_COMMENTS';

            changeSet1.addChanged(closedNC)
                .addOriginalValue(SQX_Nonconformance__c.Closure_Comment__c, null);

            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'close');
            paramsClose.put('recordID', nc.nc.Id);
            paramsClose.put('comment', 'Mock comment');

            Id result = SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, changeSet1.toJSON(), new SObject[]{}, paramsClose);

            closedNC = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Nonconformance__c WHERE Id =: nc.nc.Id];
            System.assertEquals(SQX_NC.STATUS_CLOSED, closedNC.Status__c, 'Expected the NC to be closed but found: ' + closedNC.Status__c);

            /*
            * Act : Reopen the nc 
            */
            Map<String, String> paramsReopen = new Map<String, String>();
            paramsReopen.put('nextAction', 'reopen');
            paramsReopen.put('recordID', nc.nc.Id);
            paramsReopen.put('comment', 'Mock comment');

            result= SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, '{"changeSet": []}', new SObject[]{}, paramsReopen);

            closedNC = [SELECT Id, Risk_Justification__c, Status__c FROM SQX_Nonconformance__c WHERE Id =: nc.nc.Id];
            System.assertEquals(SQX_NC.STATUS_COMPLETE, closedNC.Status__c, 'Expected the NC to be closed but found: ' + closedNC.Status__c);

            closedNC.Risk_Justification__c = 'RISK_JUSTIFICATION';
            SQX_Test_ChangeSet changeSet3 = new SQX_Test_ChangeSet();

            changeSet3.addChanged(closedNC)
                .addOriginalValue(SQX_Nonconformance__c.Risk_Justification__c, null)
                .addOriginalValue(SQX_Nonconformance__c.Status__c, closedNC.Status__c);

            Map<String, String> saveParams = new Map<String, String>();
            saveParams.put('nextAction', 'save');
            saveParams.put('comment', 'Mock comment');

            result = SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, changeSet3.toJSON(), new SObject[]{}, saveParams);
            closedNC = [SELECT Risk_Justification__c FROM SQX_Nonconformance__c WHERE Id =: closedNC.Id];

            System.assertEquals('RISK_JUSTIFICATION', closedNC.Risk_Justification__c, 'Expected the risk justification to be changed but found: ' + closedNC.Risk_Justification__c);
        
            /*
            * Act : close the nc 
            */
            SQX_Test_ChangeSet changeSet2 = new SQX_Test_ChangeSet();

            closedNC = [SELECT Id, Closure_Comment__c, Risk_Justification__c FROM SQX_Nonconformance__c WHERE Id =: nc.nc.Id];
            closedNC.Closure_Comment__c = 'CLOSURE_COMMENTS_REOPENED_CLOSED';

            changeSet2.addChanged(closedNC)
                .addOriginalValue(SQX_Nonconformance__c.Closure_Comment__c, 'CLOSURE_COMMENTS')
                .addOriginalValue(SQX_Nonconformance__c.Risk_Justification__c, closedNC.Risk_Justification__c);

            paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'close');
            paramsClose.put('recordID', nc.nc.Id);
            paramsClose.put('comment', 'Mock comment Reopen Close');

            result = SQX_Extension_NC.processChangeSetWithAction(nc.nc.Id, changeSet2.toJSON(), new SObject[]{}, paramsClose);

            closedNC = [SELECT Id, Status__c FROM SQX_Nonconformance__c WHERE Id =: nc.nc.Id];
            System.assertEquals(SQX_NC.STATUS_CLOSED, closedNC.Status__c, 'Expected the NC to be closed but found: ' + closedNC.Status__c);

        }
    }
}