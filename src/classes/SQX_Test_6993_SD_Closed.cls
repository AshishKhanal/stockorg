/**
* unit test for when sd record closed then supplier document request information transfer.
*/
@isTest
public class SQX_Test_6993_SD_Closed {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { adminUser});
        System.runAs(adminUser){
            SQX_Part__c part = SQX_Test_Part.insertPart(null, adminUser, true, '');
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation();
            sd.sd.SQX_Part__c = part.Id;
            sd.sd.Scope__c ='Test Scope';
            sd.save();
            SQX_Supplier_Deviation__c sdObject=[SELECT Status__c, Record_Stage__c FROM SQX_Supplier_Deviation__c LIMIT 1];
            
            //Create workflow policy(deviation process)
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Deviation_Process__c dp = new SQX_Deviation_Process__c();
            dp.SQX_Parent__c =sd.sd.Id;
            dp.Name = 'TestName1';
            dp.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            dp.Step__c =1;
            dp.Due_Date__c = Date.today();
            dp.SQX_User__c = adminUser.Id;
            dp.Document_Name__c='Sample Document Name';
            dp.Issue_Date__c = Date.today();
            dp.Needs_Periodic_Update__c=true;
            dp.Notify_Before_Expiration__c=10;
            dp.Expiration_Date__c = Date.today()+1;
            dp.SQX_Controlled_Document__c = cDoc1.doc.Id;
            dp.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
            new SQX_DB().op_insert(new List<SQX_Deviation_Process__c> { dp },  new List<Schema.SObjectField>{});
            sd.submit();
            sd.initiate();
        }
    }
    
    /**
    * GIVEN : given sd record
    * WHEN : when closed with result approved
    * THEN : transfer information
    */  
    public static testMethod void givenSD_WhenClosedWithResultApproved_ThenTransferInformation(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: set close SD record with result approved
            SQX_Supplier_Deviation__c sd = [SELECT Id, Status__c, Record_Stage__c, Activity_Code__c FROM SQX_Supplier_Deviation__c LIMIT 1];
            
            sd.Status__c =SQX_Supplier_Deviation.STATUS_CLOSED;
            sd.Record_Stage__c = SQX_Supplier_Deviation.STAGE_CLOSED;
            sd.Activity_Code__c = 'close';
            sd.Result__c = SQX_Supplier_Deviation.SD_RESULT_APPROVED;
            
            //Act: close the SD record
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__c>{ sd }, new List<SObjectField>{});
            
            //Assert: Ensured that supplier document request was created
            SQX_Deviation_Process__c dp = [SELECT Issue_Date__c, Expiration_Date__c,Document_Name__c,Notify_Before_Expiration__c,Needs_Periodic_Update__c,SQX_Parent__r.Part_Name__c, SQX_Parent__r.SQX_Part__c, SQX_Parent__r.Scope__c FROM SQX_Deviation_Process__c 
                                           WHERE RecordTypeId=:SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST)];
            List<SQX_Certification__c> sdr = [SELECT Issue_Date__c, Expire_Date__c,Document_Name__c,Notify_Before_Expiration__c,Needs_Periodic_Update__c,Part_Name__c,SQX_Part__c,Description__c FROM SQX_Certification__c LIMIT 1];
            
            System.assertEquals(1, sdr.size());
            System.assertEquals(dp.Issue_Date__c, sdr[0].Issue_Date__c);
            System.assertEquals(dp.Expiration_Date__c, sdr[0].Expire_Date__c);
            System.assertEquals(dp.Document_Name__c, sdr[0].Document_Name__c);
            System.assertEquals(dp.Notify_Before_Expiration__c, sdr[0].Notify_Before_Expiration__c);
            System.assertEquals(dp.Needs_Periodic_Update__c, sdr[0].Needs_Periodic_Update__c);
            System.assertEquals(dp.SQX_Parent__r.Part_Name__c, sdr[0].Part_Name__c);
            System.assertEquals(dp.SQX_Parent__r.SQX_Part__c, sdr[0].SQX_Part__c);
            System.assertEquals(dp.SQX_Parent__r.Scope__c, sdr[0].Description__c);
        }
    }
}