/**
 * This class exposes the SObject tree that CQ uses for its UI to the org where it is deployed as managed package. By using this class
 * client org can configure vf pages such as audit draft report
 */ 
global with sharing class SQX_Extension_SObjectReport {
    
    /**
     * The main record that is related to the controller object
     */
    public SObject mainRecord;
    
    /**
     * Default constructor for the extension that can be used by the VF page.
     */
    global SQX_Extension_SObjectReport(ApexPages.StandardController controller){
        try {
            controller.addFields(new String[]{SQX.getNSNameFor('Status__c'), 'Name'});
        } catch (Exception e) {
            //ignore add fields error
        }
        mainRecord = controller.getRecord();
    }
    
    /**
     * Loads the main tree node by selecting the correct extension that fetches the data using sobject data loader
     */
    private void getCompleteDataJSON(){
        SQX_Extension_UI extUI = null;
        extUI = SQX_Extension_UI.getExtension(mainRecord,false);
        this.intMainTreeNode = extUI.mainRecordNode;
    }

	    
    private SObjectTreeNode intMainTreeNode = null;
    
    /**
     * The root node of the object, which provides access to the whole object hierarchy.
     */ 
    global SObjectTreeNode mainRecordNode {
        get {
            if(intMainTreeNode == null){
                getCompleteDataJSON();
            }

            return intMainTreeNode;
        }
    }

}
