/**
* This class contains the trigger handler for the Personnel Job Function
*/
public with sharing class SQX_Personnel_Job_Function {
    public static final String  TRAINING_STATUS_CURRENT = 'Current',
                                TRAINING_STATUS_PENDING = 'Pending',
                                TRAINING_STATUS_OVERDUE = 'Overdue',
                                TRAINING_STATUS_NA = 'NA';
    
    /**
    * returns Personnel Job Function list of a Personnel
    */
    public static List<SQX_Personnel_Job_Function__c> getPersonnelJobFunctionList(Id personnelId, boolean showNewOrActiveOnly) {
        if (showNewOrActiveOnly) {
            return [SELECT ID,Name,SQX_Job_Function__c,Activation_Date__c,Active__c,Deactivation_Date__c, Training_Status__c, SQX_Job_Function__r.Name
                    FROM SQX_Personnel_Job_Function__c
                    WHERE SQX_Personnel__c = :personnelId AND Deactivation_Date__c = null
                    ORDER BY Personnel_Name__c];
        } else {
            return [SELECT ID,Name,SQX_Job_Function__c,Activation_Date__c,Active__c,Deactivation_Date__c, Training_Status__c, SQX_Job_Function__r.Name
                    FROM SQX_Personnel_Job_Function__c
                    WHERE SQX_Personnel__c = :personnelId
                    ORDER BY Personnel_Name__c, Activation_Date__c DESC];
        }
    }

    /**
     * This method sets activation dates for given actived pjf
     * @param pjf record in which field value is to be set
     */
    public static void onPJFActivation(SQX_Personnel_Job_Function__c pjf) {
        pjf.Activation_Date__c = System.today();
    }

    /**
     * This method sets deactivation date for given deactivated pjf
     * @param pjf record in which field value is to be set
     */
    public static void onPJFDeactivation(SQX_Personnel_Job_Function__c pjf) {
        pjf.Deactivation_Date__c = System.today();
    }

    /**
    * Personnel Job Function Trigger Handler
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified() {
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newPJFs equivalent to trigger.new in salesforce
        * @param oldMap equivalent to trigger.oldMap in salesforce
        */
        public Bulkified(List<SQX_Personnel_Job_Function__c> newPJFs, Map<Id, SQX_Personnel_Job_Function__c> oldMap) {
            super(newPJFs, oldMap);
        }
        
        /**
        * This method sets uniqueness constrainst value in field Unique_New_Or_Active_Constraint__c
        * using Job Function, Personnel and Deactivation Date.
        */
        public Bulkified setUniquenessConstraintValue() {
            this.resetView();
            
            if (this.view.size() > 0) {
                // set Unique_New_Or_Active_Constraint__c for each record
                for (SQX_Personnel_Job_Function__c pjf : (List<SQX_Personnel_Job_Function__c>)this.view) {
                    // set value for all existing or new not-deactivated record
                    if (pjf.Id != null || pjf.Deactivation_Date__c == null) {
                        String uniquenessConstraintValue = '' + pjf.SQX_Personnel__c + pjf.SQX_Job_Function__c;
                        if (pjf.Name != null && pjf.Deactivation_Date__c != null) {
                            uniquenessConstraintValue += pjf.Name;
                        }
                        pjf.Unique_New_Or_Active_Constraint__c = uniquenessConstraintValue;
                    }
                }
            }

            return this;
        }
        
        /**
        * This method prevents deletion on once activated User Job Function.
        */
        public Bulkified preventDeletionOnOnceActivatedRecord() {
            this.resetView();
            
            for (SQX_Personnel_Job_Function__c pjf : (List<SQX_Personnel_Job_Function__c>)this.view) {
                if (pjf.Activation_Date__c != null) {
                    pjf.addError(Label.SQX_ERR_MSG_CANNOT_DELETE_ONCE_ACTIVATED_PERSONNEL_JOB_FUNCTION);
                }
            }

            return this;
        }
        
        /**
        * This method generates/re-evaluates personnel document trainings when a user job function is activated/deactivated
        */
        private Bulkified createOrUpdatePersonnelDocumentTrainingWhenActivatedOrDeactivated(String actionName, Boolean activeValue) {
            //Remove all the previously processed records
            this.resetView().removeProcessedRecordsFor(SQX.PersonnelJobFunction, actionName);

            if (this.view.size() > 0) {
                // find all activated personnel job functions
                Rule filter = new Rule();
                filter.addRule(Schema.SQX_Personnel_Job_Function__c.Active__c, RuleOperator.Equals, (Object)activeValue);

                this.applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

                if (filter.evaluationResult.size() > 0){
                    this.addToProcessedRecordsFor(SQX.PersonnelJobFunction, actionName, filter.evaluationResult);
                    
                    if (activeValue) {
                        SQX_Personnel_Document_Job_Function.processActivatedPersonnelJobFunctions(filter.evaluationResult);
                    }
                    else {
                        SQX_Personnel_Document_Job_Function.processDeactivatedPersonnelJobFunctions(filter.evaluationResult);
                    }
                }
            }

            return this;
        }
        
        /**
        * This method generates personnel document trainings when a user job function is activated
        */
        public Bulkified createOrUpdatePersonnelDocumentTrainingWhenActivated() {
            final String ACTION_NAME = 'createOrUpdatePersonnelDocumentTrainingWhenActivated';
            
            return createOrUpdatePersonnelDocumentTrainingWhenActivatedOrDeactivated(ACTION_NAME, true);
        }
        
        /**
        * This method re-evaluates personnel document trainings when a user job function is deactivated
        */
        public Bulkified updatePersonnelDocumentTrainingWhenDeactivated() {
            final String ACTION_NAME = 'updatePersonnelDocumentTrainingWhenDeactivated';
            
            return createOrUpdatePersonnelDocumentTrainingWhenActivatedOrDeactivated(ACTION_NAME, false);
        }

        /**
         * This method updates the activation and deactivation date of pjf
         */
        public Bulkified updateActivationAndDeactivationDate() {
            this.resetView();
            
            for (SQX_Personnel_Job_Function__c pjf : (List<SQX_Personnel_Job_Function__c>)this.view) {
                SQX_Personnel_Job_Function__c pjfOld = (SQX_Personnel_Job_Function__c)this.oldValues.get(pjf.Id);
                
                if (pjf.Active__c == true && (pjfOld == null || pjf.Active__c != pjfOld.Active__c)) {
                    
                    // set dates when new active PJF is created or existing PJF is activated
                    SQX_Personnel_Job_Function.onPJFActivation(pjf);

                } else if (pjf.Active__c == false && pjfOld != null && pjf.Active__c != pjfOld.Active__c) {
                    
                    // set deactivation dates when active PJF is deactivated
                    SQX_Personnel_Job_Function.onPJFDeactivation(pjf);
                }
            }
            return this;
        }
    }
}