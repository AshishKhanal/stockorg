/**
* unit test for extend the supplier deviation record. Do not extend status draft and already extended supplier deviation.
*/
@isTest
public class SQX_Test_5763_Extend_SD_Record {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { standardUser, adminUser});
        System.runAs(adminUser){
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation().save();
        }
    }
    
    /**
    * GIVEN : given supplier deviation status draft record 
    * WHEN : extend the record
    * THEN : throw validation error message
    */  
    public static testMethod void giveDraftSupplierDeviation_WhenRecordExtend_ThenThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrage: Retrive supplier deviation record
            SQX_Supplier_Deviation__c sd1 = [SELECT Id, Name, SQX_Account__c, SQX_External_Contact__c, SQX_Supplier_Deviation__c FROM SQX_Supplier_Deviation__c];
            sd1.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            sd1.Record_Stage__c = SQX_Supplier_Deviation.STAGE_TRIAGE;
            sd1.Activity_Code__c = 'submit';
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__c> { sd1 }, new List<Schema.SObjectField>{});
            
            //Act: Extend supplier deviation
            sd1.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            sd1.Record_Stage__c = SQX_Supplier_Deviation.STAGE_DRAFT;
            sd1.Activity_Code__c = 'extend';
            List<Database.SaveResult> result =new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_Deviation__c>{ sd1 }, new List<SObjectField>{});
            
            //Assert: Ensured that validation error throw
            System.assert(result[0].isSuccess() == false, 'Extend only status is open or completed.');
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG, result[0].getErrors().get(0).getMessage());
        }  
    }
    
    /**
    * GIVEN : given supplier deviation status record 
    * WHEN : extend is done
    * THEN : Create clone of old supplier deviation record
    */  
    public static testMethod void givenCompletedSupplierDeviation_WhenRecordExtend_ThenCreateNewSDRecord(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrage: Retrive supplier deviation record
            SQX_Supplier_Deviation__c sd1 = [SELECT Id, Name, compliancequest__Effective_Date__c, compliancequest__Expiration_Date__c, compliancequest__scope__c, SQX_Supplier_Deviation__c FROM SQX_Supplier_Deviation__c];
            sd1.compliancequest__Effective_Date__c = Date.today();
            sd1.compliancequest__Expiration_Date__c = Date.today().addMonths(3);
            sd1.compliancequest__scope__c = 'ScopeOfRecord';
            update sd1;
            
            //Act: Extend record
            SQX_Clone_Configuration config = new SQX_Clone_Configuration();
            config.ids = new List<Id>{sd1.id};
            config.jsonDefaults = '{"compliancequest__Effective_Date__c": "2020-01-01","compliancequest__Workflow_Status__c": "Draft","compliancequest__Status__c": "Draft","compliancequest__Record_Stage__c": "Draft","compliancequest__Current_Step__c": 0,"compliancequest__Activity_Comment__c": "","compliancequest__Expiration_Date__c": "2021-01-01","compliancequest__SQX_Supplier_Deviation__c": "'+sd1.id+'"}';
            List<Id> extendedRecordIds = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{config});
            
            //Assert:Make sure json date is present in cloned record 
            SQX_Supplier_Deviation__c extendedRecord = [SELECT Id, Name, compliancequest__scope__c, compliancequest__Effective_Date__c, compliancequest__Expiration_Date__c, SQX_Supplier_Deviation__c, compliancequest__Activity_Comment__c FROM SQX_Supplier_Deviation__c where Id IN :extendedRecordIds limit 1];
            System.assertEquals(sd1.compliancequest__scope__c, extendedRecord.compliancequest__scope__c);
            System.assertEquals(Date.valueof('2021-01-01'), extendedRecord.compliancequest__Expiration_Date__c);
            System.assertEquals(Date.valueof('2020-01-01'), extendedRecord.compliancequest__Effective_Date__c);
            System.assertEquals(sd1.id, extendedRecord.compliancequest__SQX_Supplier_Deviation__c);
            System.assertEquals(null, extendedRecord.compliancequest__Activity_Comment__c);
        }  
    }
    
    /**
    * GIVEN : Clone configuration without id of the record that needs to be cloned
    * WHEN : extend is done
    * THEN : error message, 'At least one id is required to proceed further' is thrown.
    */
    public static testMethod void GivenCloneConfigurationWithoutParentId_WhenRecordTriedToBeCloned_ThenThrowErrorMsg(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Setup clone configuration
            SQX_Clone_Configuration config = new SQX_Clone_Configuration();
            config.ids = new List<Id>();
            config.jsonDefaults = '{"compliancequest__Effective_Date__c": "2020-01-01","compliancequest__Workflow_Status__c": "Draft","compliancequest__Status__c": "Draft","compliancequest__Record_Stage__c": "Draft","compliancequest__Current_Step__c": 0,"compliancequest__Activity_Comment__c": "","compliancequest__Expiration_Date__c": "2021-01-01","compliancequest__SQX_Supplier_Deviation__c": 121"}';
            try {
                //Act: Try cloning records without parent record id 
                List<Id> extendedRecordIds = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{config});
                System.assert(false, 'Receiving this error means, cloneRecord passed without parent Id');
            }catch(SQX_ApplicationGenericException e) {
                //Assert: Ensure that an exception occurs
                System.assertEquals('At least one id is required to proceed further.', e.getMessage(), 'Expected error msg, since no id was passed to clone_configuration');
            }
        }  
    }
    
    /**
    * GIVEN : Multiple clone configurations
    * WHEN : Above configuration used for cloning
    * THEN : error message, 'Multiple configurations aren\'t supported.' is thrown.
    */
    public static testMethod void GivenMultipleConfigurations_WhenConfigUsedForCloning_ThenThrowErrorMsg(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            Id recordId = [Select id from SQX_Supplier_Deviation__c limit 1].Id;
            SQX_Clone_Configuration config1 = new SQX_Clone_Configuration();
            SQX_Clone_Configuration config2 = new SQX_Clone_Configuration();
            config1.ids = new List<Id>{recordId};
            config1.jsonDefaults = '{"compliancequest__Effective_Date__c": "2020-01-01","compliancequest__Workflow_Status__c": "Draft","compliancequest__Status__c": "Draft","compliancequest__Record_Stage__c": "Draft","compliancequest__Current_Step__c": 0,"compliancequest__Activity_Comment__c": "","compliancequest__Expiration_Date__c": "2021-01-01","compliancequest__SQX_Supplier_Deviation__c": "'+recordId+'"}';
            config2.ids = new List<Id>{recordId};
            config2.jsonDefaults = '{"compliancequest__Effective_Date__c": "2020-01-01","compliancequest__Status__c": "Draft","compliancequest__Record_Stage__c": "Draft","compliancequest__Current_Step__c": 0,"compliancequest__Activity_Comment__c": "","compliancequest__Expiration_Date__c": "2021-01-01","compliancequest__SQX_Supplier_Deviation__c": "'+recordId+'"}';
            //Arrange: Setup clone configuration
            List<SQX_Clone_Configuration> cloneConfigs = new List<SQX_Clone_Configuration>();
            cloneConfigs.add(config1);
            cloneConfigs.add(config2);
            try {
                //Act: Try cloning records without parent record id 
                List<Id> extendedRecordIds = SQX_Clone_Records.cloneRecord(cloneConfigs);
                System.assert(false, 'Receiving this error means, multiple configs were thrown');
            }catch(SQX_ApplicationGenericException e) {
                //Assert: Ensure that an exception occurs
                System.assertEquals(2, cloneConfigs.size(), 'Expected size of cloneConfigs to be 2');
                System.assertEquals('Multiple configurations aren\'t supported.', e.getMessage(), 'Multiple clone config is not supported');
            }
        }  
    }
    
    /**
    * GIVEN : Clone configuration with relationalField
    * WHEN : Above configuration used for cloning
    * THEN : relationalField must have parentId
    */
    public static testMethod void GivenConfigWithRelationalField_WhenConfigUsedForCloning_ThenRelationalFieldShouldHaveParentId(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrage: Retrive supplier deviation record
            SQX_Supplier_Deviation__c sd1 = [SELECT Id FROM SQX_Supplier_Deviation__c limit 1];
            
            //Act: Configuration have a relationalField and record is cloned
            SQX_Clone_Configuration config = new SQX_Clone_Configuration();
            config.ids = new List<Id>{sd1.id};
            config.relationalField = 'compliancequest__Activity_Comment__c';
            config.jsonDefaults = '{"compliancequest__Effective_Date__c": "2020-01-01","compliancequest__Workflow_Status__c": "Draft","compliancequest__Status__c": "Draft","compliancequest__Record_Stage__c": "Draft","compliancequest__Current_Step__c": 0,"compliancequest__Expiration_Date__c": "2021-01-01","compliancequest__SQX_Supplier_Deviation__c": "'+sd1.id+'"}';
            config.fieldSetName = SQX_Clone_Records.DEFAULT_FIELD_EXCLUSION_FIELDSET;
            List<Id> extendedRecordIds = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{config});
            
            //Assert:Make sure field mentioned in relationalField have parentId
            SQX_Supplier_Deviation__c extendedRecord = [SELECT Id, compliancequest__Activity_Comment__c FROM SQX_Supplier_Deviation__c where Id IN :extendedRecordIds limit 1];
            System.assertEquals(sd1.id, extendedRecord.compliancequest__Activity_Comment__c,'Expected parent id to be present in activity comment field');
        }  
    }
    
    /**
     * GIVEN: A set of fields
     * WHEN: Above set of fields is passed to SQX_DynamicQuery.prepareAllFieldsQuery as ignoreFields
     * THEN: Ignorefields should be negated and should not present in the query string
     */
    public static testMethod void negateFieldSetFieldsInQuery(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Set up records
            SQX_Supplier_Deviation__c sd1 = [SELECT Id FROM SQX_Supplier_Deviation__c limit 1];
            DescribeSObjectResult objDescribe = (sd1.Id).getSobjectType().getDescribe();
            Integer pageSize = 1;
            Integer offset = 0;
            Set<SObjectField> fieldSetFields = SQX_Utilities.getFieldsFromFieldSet(objDescribe, SQX_Clone_Records.DEFAULT_FIELD_EXCLUSION_FIELDSET);
            
            //Act: Clone Records
            String query = SQX_DynamicQuery.prepareAllFieldsQuery(offset,
                                                                  pageSize,
                                                                  objDescribe,
                                                                  fieldSetFields,
                                                                  null,
                                                                  new List<SObjectField>(),
                                                                  false);
            
            SObject clonedRecord = new SQX_Clone_Records().cloneRecords(Database.query(query), new SQX_Clone_Configuration())[0];
            System.assertEquals(true, clonedRecord != null);
            String s = JSON.serialize(clonedRecord);
            Map<String,Object> obj = (Map<String,Object>) JSON.deserializeUntyped(s);
            Set<String> fieldsInFieldSet = new Set<String>();
            
            //Assert: Ensure that cloned record does not have field present in clone exclusion fieldset
            for(SObjectField sobfield: fieldSetFields) {
                String fieldName = sobfield + '';
                System.assertEquals(null, obj.get(fieldName));
                fieldsInFieldSet.add(fieldName);
            }
            
            //Assert: Ensure that cloned record have a field 'compliancequest__Record_Stage__c'
            System.assertEquals(true, obj.keySet().size() > 0);
            if(!fieldsInFieldSet.contains('compliancequest__Record_Stage__c')) {
                System.assertEquals(true, obj.get('compliancequest__Record_Stage__c') != null);
            }
            
        }
    }
}