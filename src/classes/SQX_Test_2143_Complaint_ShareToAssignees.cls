/**
* test for edit access on open complaint (status as open or closure rejected) and its related finding, investigation and PHR records for task assignees
* test for edit access on open complaint and its related finding, investigation and PHR records for task assignees when task assignee is added/changed/deleted/undeleted
*/
@isTest
public class SQX_Test_2143_Complaint_ShareToAssignees {

    // static Boolean runAllTests = true,
    //                run_givenChangeOwnerAndModifyPoliciesOnOpenComplaint = false,
    //                run_givenComplaintTask_AssigneeCanbeChanged = false;
    
    // @testSetup
    // public static void commonSetup() {
    //     // add required users
    //     UserRole role = SQX_Test_Account_Factory.createRole();
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'admin1');
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user1');
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user2');
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user3');
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user4');
    //     SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'user5');
    // }
    
    // /**
    // * checks sharing rules with edit access on complaint and its related finding, investigation and PHR records for expected and not expected users
    // */
    // static void checkSharedUsersOnComplaintRecords(Id complaintId, Set<Id> expectedSharedToUserIds, Set<Id> expectedNotSharedToUserIds) {
    //     Set<Id> actualSharedUserIds;
    //     if (expectedNotSharedToUserIds == null)
    //         expectedNotSharedToUserIds = new Set<Id>();
        
    //     // get main complaint record with sharing info
    //     SQX_Complaint__c complaintWithShares = [SELECT Id, Name, SQX_Finding__c, OwnerId,
    //                                                    (SELECT Id, ParentId, UserOrGroupId FROM Shares WHERE AccessLevel = 'edit')
    //                                             FROM SQX_Complaint__c
    //                                             WHERE Id = :complaintId];
                                                
    //     actualSharedUserIds = new Set<Id> { complaintWithShares.OwnerId }; // owner always has full access
    //     for (SQX_Complaint__Share shareRec : complaintWithShares.Shares) {
    //         actualSharedUserIds.add(shareRec.UserOrGroupId);
    //     }
        
    //     // verify main complaint sharing
    //     for (Id userId : expectedSharedToUserIds) {
    //         System.assertEquals(true, actualSharedUserIds.contains(userId), 'Expected sharing rules to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
    //     for (Id userId : expectedNotSharedToUserIds) {
    //         System.assertEquals(false, actualSharedUserIds.contains(userId), 'Expected sharing rules not to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
        
    //     // get PHR record with sharing info
    //     SQX_Product_History_Review__c complaint1PHR = [SELECT Id, Name, OwnerId, (SELECT Id, ParentId, UserOrGroupId FROM Shares WHERE AccessLevel = 'edit')
    //                                                    FROM SQX_Product_History_Review__c
    //                                                    WHERE SQX_Complaint__c = :complaintWithShares.Id];
                                            
    //     actualSharedUserIds = new Set<Id> { complaint1PHR.OwnerId }; // owner always has full access
    //     for (SQX_Product_History_Review__Share shareRec : complaint1PHR.Shares) {
    //         actualSharedUserIds.add(shareRec.UserOrGroupId);
    //     }
        
    //     // verify PHR sharing
    //     for (Id userId : expectedSharedToUserIds) {
    //         System.assertEquals(true, actualSharedUserIds.contains(userId), 'Expected sharing rules to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
    //     for (Id userId : expectedNotSharedToUserIds) {
    //         System.assertEquals(false, actualSharedUserIds.contains(userId), 'Expected sharing rules not to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
        
    //     // get PHR record with sharing info
    //     SQX_Investigation__c complaint1Inv = [SELECT Id, Name, OwnerId, (SELECT Id, ParentId, UserOrGroupId FROM Shares WHERE AccessLevel = 'edit')
    //                                           FROM SQX_Investigation__c
    //                                           WHERE SQX_Complaint__c = :complaintWithShares.Id];
                                            
    //     actualSharedUserIds = new Set<Id> { complaint1Inv.OwnerId }; // owner always has full access
    //     for (SQX_Investigation__Share shareRec : complaint1Inv.Shares) {
    //         actualSharedUserIds.add(shareRec.UserOrGroupId);
    //     }
        
    //     // verify investigation sharing
    //     for (Id userId : expectedSharedToUserIds) {
    //         System.assertEquals(true, actualSharedUserIds.contains(userId), 'Expected sharing rules to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
    //     for (Id userId : expectedNotSharedToUserIds) {
    //         System.assertEquals(false, actualSharedUserIds.contains(userId), 'Expected sharing rules not to exists for user ' + userId + ' on main complaint record ' + complaintId);
    //     }
    // }
    
    // public static testmethod void givenChangeOwnerAndModifyPoliciesOnOpenComplaint() {
    //     if(!runAllTests && !run_givenChangeOwnerAndModifyPoliciesOnOpenComplaint){
    //         return;
    //     }
    //     // get users
    //     Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
    //     User admin1 = userMap.get('admin1'),
    //         user1 = userMap.get('user1'),
    //         user2 = userMap.get('user2'),
    //         user3 = userMap.get('user3'),
    //         user4 = userMap.get('user4'),
    //         user5 = userMap.get('user5');
        
    //     SQX_Test_Complaint complaint1;
    //     Set<Id> currentAssignees, oldAssignees;
        
    //     System.runas(user1) {
    //         // add required complaint
    //         complaint1 = new SQX_Test_Complaint(admin1);
    //         complaint1.save();
            
    //         // add complaint policies for user3 and user4
    //         SQX_Complaint_Task__c policy1 = new SQX_Complaint_Task__c(
    //             Applicable__c = true,
    //             SQX_User__c = user3.Id,
    //             SQX_Complaint__c = complaint1.complaint.Id,
    //             Due_Date__c = System.today() + 30,
    //             Name = 'Policy1'
    //         );
    //         SQX_Test_ChangeSet changes = new SQX_Test_ChangeSet();
    //         changes.addChanged('Policy1', policy1);
            
    //         // ACT: initiate complaint with new policies
    //         Map<String, String> initiateActionParam = new Map<String, String>{ 'nextAction' => 'initiate' };
    //         Id result = SQX_Extension_Complaint.processChangeSetWithAction(complaint1.complaint.Id, changes.toJSON(), null, initiateActionParam, null);
            
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user3.Id };
    //         oldAssignees = null;
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
            
            
    //         //ACT: change owner
    //         complaint1.complaint.OwnerId = user2.Id;
    //         update complaint1.complaint;
            
    //         // check sharing rules for task assignees on main complaint record
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
    //     }
        
    //     Test.startTest();
        
    //     System.runas(user2) {
    //         // set complaint status to closure rejected
    //         complaint1.setStage(SQX_Complaint.STAGE_CLOSURE_REJECTED).save();
            
    //         // get policy
    //         SQX_Complaint_Task__c policy1 = [SELECT Id, SQX_Complaint__c, SQX_User__c FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint1.complaint.Id AND SQX_User__c = :user3.Id];
            
    //         // ACT: add new policy
    //         SQX_Complaint_Task__c policy2 = complaint1.addPolicy(true, System.today() + 30, user4.Id);
            
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user3.Id, user4.Id };
    //         oldAssignees = null;
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
            
            
    //         // ACT: delete assignee
    //         delete policy1;
            
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user4.Id };
    //         oldAssignees = new Set<Id>{ user3.Id };
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
            
            
    //         // ACT: change assignee
    //         policy2.SQX_User__c = user5.Id;
    //         update policy2;
            
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user5.Id };
    //         oldAssignees = new Set<Id>{ user4.Id };
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
            
    //         // ACT: undelete assignee
    //         undelete policy1;
            
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user3.Id, user5.Id };
    //         oldAssignees = null;
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees);
    //     }
        
    //     Test.stopTest();
    // }

    // /**
    // * @Story : SQX-2351
    // * Given  : Complaint is created by User 1, creates a complaint task with user 2 as assignee and initiates it
    // * When : User 2 assigns complaint task assigned to him to user 3
    // * Then : Complaint is saved without any errors and task is shared to user 3
    // */
    // public static testmethod void givenComplaintTask_AssigneeCanbeChanged() {
    //     if(!runAllTests && !run_givenComplaintTask_AssigneeCanbeChanged){
    //         return;
    //     }
    //     // get users
    //     Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
    //     User admin1 = userMap.get('admin1'),
    //         user1 = userMap.get('user1'),
    //         user2 = userMap.get('user2'),
    //         user3 = userMap.get('user3');

    //     SQX_Test_Complaint complaint1;
    //     SQX_Complaint_Task__c policy1;
    //     Set<Id> currentAssignees, oldAssignees;
        
    //     System.runas(user1) {
    //         //Arrange : add required complaint
    //         complaint1 = new SQX_Test_Complaint(admin1);
    //         complaint1.save();
            
    //         // add complaint policies for user3 and user4
    //         policy1 = new SQX_Complaint_Task__c(
    //             Applicable__c = true,
    //             SQX_User__c = user2.Id,
    //             SQX_Complaint__c = complaint1.complaint.Id,
    //             Due_Date__c = System.today() + 30,
    //             Name = 'Policy1'
    //         );
    //         SQX_Test_ChangeSet changes = new SQX_Test_ChangeSet();
    //         changes.addChanged('Policy1', policy1);
            
    //         //initiate complaint with new policies
    //         Map<String, String> initiateActionParam = new Map<String, String>{ 'nextAction' => 'initiate' };
    //         SQX_Extension_Complaint.processChangeSetWithAction(complaint1.complaint.Id, changes.toJSON(), null, initiateActionParam, null);
            
    //     }
        
    //     Test.startTest();
        
    //     System.runas(user2) {

    //         //ACT : Change assignee of the policy
    //         List<SQX_Complaint_Task__c> policies = [Select Id, SQX_User__c from SQX_Complaint_Task__c where SQX_Complaint__c = :complaint1.complaint.Id];
    //         policies[0].SQX_User__c = user3.Id;
    //         Database.SaveResult assigneeChanged = Database.update(policies[0], false);

    //         //Assert: Save is successful
    //         System.assertEquals(true, assigneeChanged.isSuccess(), 'Expected assignee change to be successful' + SQX_Utilities.getFormattedErrorMessage(assigneeChanged.getErrors())); 

    //         //Assert : Shared with user 3 and share with user 2 is removed
    //         // check sharing rules for task assignees on main complaint record
    //         currentAssignees = new Set<Id>{ user3.Id };
    //         oldAssignees = new Set<Id>{ user2.Id };
    //         checkSharedUsersOnComplaintRecords(complaint1.complaint.Id, currentAssignees, oldAssignees) ;         
    //     }
        
    //     Test.stopTest();
    // }
}