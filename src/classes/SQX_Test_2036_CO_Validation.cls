@isTest
public class SQX_Test_2036_CO_Validation{

    static boolean runAllTests = true,
                run_givenChangeOrder_WhenUserAddImplThanEnsureRecordAccess_ThenIfUserDidnotGetRecordAccessGiveErrorMessage=false,
                run_givenACO_WhenCOIsClosed_NoChangesCanBeMade = false;

    /**
     * Action: Create a CO and close the doc and trying changing the record
     * Expected: Must not be saved and throw an error
     * @author: Anish Shrestha
     * @date: 2016-03-02
     * @story: [SQX-2036]
     */
    public testmethod static void givenACO_WhenCOIsClosed_NoChangesCanBeMade(){

        if(!runAllTests && !run_givenACO_WhenCOIsClosed_NoChangesCanBeMade){
            return;
        }
        //Arrange: Create a change order
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User taskAssignee = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser){

            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            SQX_Test_Change_Order changeOrderVoid = new SQX_Test_Change_Order();
            changeOrderVoid.save();

            //Act: open change order 
            changeOrder.changeOrder.Status__C=SQX_Change_Order.STATUS_OPEN;
            changeOrder.save();
            
            // Approve the change and complete all the actions
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            changeOrder.save();
            
            changeOrder.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED;
            changeOrder.setStatus(SQX_Change_Order.STATUS_COMPLETE);
            changeOrder.changeOrder.Record_Stage__c=SQX_Change_Order.STAGE_COMPLETE;
            changeOrder.save();

            //Act:Release the change order  and effective date to today so that change order get closed 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_RELEASE;
            changeOrder.changeOrder.Effective_Date__c=Date.today();
            changeOrder.save();
            
            // Act : Void the change order and change the record
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrderVoid.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrderVoid.changeOrder.Resolution_Code__c='Duplicate';
            changeOrderVoid.save();
            SQX_Change_Order__c closedCO = new SQX_Change_Order__c();
            closedCO = [SELECT Status__c,Record_Stage__c	,Title__c FROM SQX_Change_Order__c WHERE Id =: changeOrder.changeOrder.Id];

            System.assertEquals(SQX_Change_Order.STATUS_CLOSED, closedCO.Status__c, 'Expected the status to be closed but found: ' + closedCO.Status__c);
            System.assertEquals(SQX_Change_Order.STATUS_CLOSED, closedCO.Record_Stage__c	, 'Expected the status to be closed but found: ' + closedCO.Record_Stage__c	);

            //Temporary disable unit test
            /* Act :try to  Void the change order that is already closed 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrder.changeOrder.Resolution_Code__c='Duplicate';
            //Act: user try to void the record which record is already closed 
            List<Database.SaveResult> closedRecord = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{changeOrder.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            //Assert: record should give error : 'You do not have enough permission to void the record. Please ask your administrator to grant you CQ Change Order Supervisor permission.
            String errorMsg = 'You do not have enough permission to void the record. Please ask your administrator to grant you CQ Change Order Supervisor permission.';
            System.assertEquals(errorMsg,closedRecord[0].getErrors()[0].getMessage());

            // Add change order  supervisor custom permission to std user
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, SQX_Change_Order.CUSTOM_PERMISSION_CHANGEORDER_SUPERVISOR);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrder.changeOrder.Resolution_Code__c='Duplicate';
            //Act: user try to void the record which record is already closed 
            List<Database.SaveResult> closedRecordResult = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{changeOrder.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            //Assert: record should give error : Unable to perform the desired action on the record. Please ensure that the record\'s status/stage permits the action.
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,closedRecordResult[0].getErrors()[0].getMessage());
            
            SQX_Change_Order__c voidCO = new SQX_Change_Order__c();
            voidCO = [SELECT Status__c,Record_Stage__c,Title__c FROM SQX_Change_Order__c WHERE Id =: changeOrderVoid.changeOrder.Id];
            System.assertEquals(SQX_Change_Order.STATUS_VOID, voidCO.Status__c, 'Expected the status to be void but found: ' + voidCO.Status__c);
            System.assertEquals(null, voidCO.Record_Stage__c, 'Expected the status to be void but found: ' + voidCO.Record_Stage__c	);
            */
            try{
                closedCO.Title__c = 'Changed Title';
                update closedCO;

               // voidCO.Title__c = 'Changed Title void';
                //update voidCO;
                System.assert(false, 'Expected the save not to be sucessful but was saved');
            }
            catch(Exception e){
                // Assert: Change order save should not be successful
                Boolean expectedExceptionThrown =  e.getMessage().contains('Change Order is locked, no changes can be made to it.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 

        }
    }

    /**
    * Given: change order when its in queue 
    * When : User add the plan 
    * Then : if user have record acces its save successfully 
    *        if user didn't have record access than got error message :
    *        "Insufficient privilege to create record of type Change order"
    */
    public testmethod static void givenChangeOrder_WhenUserAddImplThanEnsureRecordAccess_ThenIfUserDidnotGetRecordAccessGiveErrorMessage(){
         if(!runAllTests && ! run_givenChangeOrder_WhenUserAddImplThanEnsureRecordAccess_ThenIfUserDidnotGetRecordAccessGiveErrorMessage){
            return;
        }
        //Arrange: Create a change order
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        //Add user to queue
        SQX_Test_Change_Order.addUserToQueue(new List<User> { adminUser});
        System.runAs(adminUser){
            //Arrange :create change order and add implementation
            SQX_Test_Change_Order changeOrder =  new SQX_Test_Change_Order().save();
            // Act: Add a implementation plan  change with assignee admin
            List<SQX_Implementation__c> planImpl=  new SQX_Implementation__c[]{
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 1',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'description',
                                                            RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, SQX_Implementation.RECORD_TYPE_PLAN),
                                                            SQX_Change_Order__c = changeOrder.Id)
                                    };

            //Act :Try To insert plan implementation
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(planImpl, new List<Schema.SObjectField>{});
            //Assert:save successfull implementation
            System.assert(result[0].isSuccess(),'Expected the criteria to be inserted');            
        }
        //Std user
        System.runAs(standardUser){
            //Arrange :retrive  change order 
            SQX_Change_Order__c changeOrder1=[select id,Status__c,Record_Stage__c from SQX_Change_Order__c];
            //Add imp which have no record access
            List<SQX_Implementation__c> planImpl=  new SQX_Implementation__c[]{
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 2',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'description',
                                                            RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, SQX_Implementation.RECORD_TYPE_PLAN),
                                                            SQX_Change_Order__c = changeOrder1.Id)
                                };

            //Act :Try To insert plan implementation  by std user
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(planImpl, new List<Schema.SObjectField>{});
            //Assert:should get error message :"Insufficient privilege to create record of type Change order"
            System.assertEquals('Insufficient privilege to create record of type Change Order',result[0].getErrors().get(0).getMessage());   
        }
    }
}