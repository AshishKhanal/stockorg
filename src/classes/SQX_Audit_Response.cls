/**
* Everything related to audit response, and its trigger. Its statuses approval statuses 
*/
public with sharing class SQX_Audit_Response{
    /** Status values for the audit response*/
    public static final String  STATUS_DRAFT = 'Draft',
                                STATUS_IN_APPROVAL = 'In Approval',
                                STATUS_COMPLETED = 'Completed',
                                STATUS_VOID = 'Void';
    

    /** Approval status values for the audit response*/
    public static final String  APPROVAL_STATUS_NONE = '-',
                                APPROVAL_STATUS_PENDING = 'Pending',
                                APPROVAL_STATUS_APPROVED = 'Approved',
                                APPROVAL_STATUS_REJECTED = 'Rejected',
                                APPROVAL_STATUS_RECALLED = 'Recalled',
                                APPROVAL_STATUS_PARTIAL_APPROVAL = 'Partially Approved';
    
    /**
    * The Audit response SObject that is being wrapped by the Object
    */
    public SQX_Audit_Response__c response { get; set; }

    /**
    * Create a wrapper audit response object with SObject audit response
    * @param response the audit response object that is to be enclosed
    */
    public SQX_Audit_Response(SQX_Audit_Response__c response){
        this.response = response;
    }

    /**
    * Publishes the base response, throws an error if already published
    * @throws SQX_ApplicationGenericException if already published
    */
    public void publish(){
        if(this.response != null){
            SQX_Audit_Response__c response = [SELECT ID, Status__c FROM SQX_Audit_Response__c WHERE Id = : this.response.Id FOR UPDATE];
            if(response.Status__c != STATUS_DRAFT){
                throw new SQX_ApplicationGenericException(Label.SQX_ERR_MSG_CANT_REPUBLISH);
            }
            else{
                this.response.Status__c = STATUS_COMPLETED;

                new SQX_DB().op_update(new List<SObject>{this.response}, new List<Schema.SObjectField>{ SQX_Audit_Response__c.fields.Status__c});

            }
        }

    }


    /**
    * Submits the base response (one that was passed through constructor) for approval.
    * is a short hand method
    */
    public void submitForApproval(){
        if(this.response != null){
            SQX_Approval_Util.submitForApproval(new sObject[] { this.response });
        }
    }

    /**
    * This method checks whether or not the given audit response needs to go through approval process
    * Note: This method queries for responses and its components, for those related to audit response
    * @return returns <code>true</code> if  approval is necessary else returns <code>false</code>
    */
    public boolean requiresApproval() {
        boolean needsApproval = false;
        List<SQX_Finding_Response__c> responses = 
                        [SELECT Id, Action_Post_Approval_Required__c,Action_Pre_approval_Required__c, SQX_Finding__c,
                            Requires_Investigation_Approval__c,
                                (SELECT Id, SQX_Investigation__r.Id, SQX_Disposition__r.Id, SQX_Action__r.Id, SQX_Action__r.Record_Action__c, SQX_Action__r.Is_Approved__c, SQX_Finding__r.Id, SQX_Finding__r.Stage__c
                                 FROM SQX_Response_Inclusions__r)
                        FROM SQX_Finding_Response__c
                        WHERE SQX_Audit_Response__c = :response.Id];

        SQX_Audit__c audit = [Select Requires_Investigation_Approval__c, Action_Pre_approval_Required__c, Action_Post_Approval_Required__c FROM SQX_Audit__c where Id= : response.SQX_Audit__c];

        for (SQX_Finding_Response__c response : responses) {
            needsApproval = new SQX_Finding_Response(response).requiresApproval(audit.Requires_Investigation_Approval__c,
                                                                             audit.Action_Pre_approval_Required__c,
                                                                             audit.Action_Post_Approval_Required__c);
            if (needsApproval) {
                //optimization if we need approval we don't need to process further
                break;
            }
        }

        return needsApproval;
    }

    /**
    * This class contains all the static methods that are designed to be executed as a task in trigger.
    * @author Sagar Shrestha
    * @date 2015/07/10
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(List<SQX_Audit_Response__c> newResponses, Map<Id, SQX_Audit_Response__c> oldResponses){
            super(newResponses, oldResponses);
        }

        /**
        * This trigger handler prevents any response that is published from deletion and is invoked before deletion
        * @author Sagar Shrestha
        * @date 2015/7/10
        */
        public Bulkified preventPublishedResponsesFromDeletion(){
            this.resetView();

            for(SQX_Audit_Response__c response: (List<SQX_Audit_Response__c>)this.view){
                if(response.Status__c == SQX_Audit_Response.STATUS_COMPLETED ||
                        response.Status__c == SQX_Audit_Response.STATUS_IN_APPROVAL){

                    //modified static text to use custom label
                    response.addError(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE);                
                }
            }

            return this;
        }

        /**
        * this method sets the published date when a response gets published
        * @author Sagar Shrestha
        * @date 2014/2/13
        */
        public Bulkified onPublishedAddPublishedDate(){
            final String ACTION_NAME = 'onPublishedAddPublishedDate';
            Rule publishedRecently = new Rule();
            publishedRecently.addRule(Schema.SQX_Audit_Response__c.Status__c, RuleOperator.Equals, (Object)SQX_Audit_Response.STATUS_COMPLETED);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME)
                .applyFilter(publishedRecently, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            for(SQX_Audit_Response__c response: (List<SQX_Audit_Response__c>)this.view){
                
                //add the records in the list of processed records, to prevent duplicate evaluation in this same transaction
                this.addToProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME, this.view );
                response.Published_Date__c = DateTime.Now();
            }
            
            return this;
        }

        /**
        * this method sets the submitter's name and submission date to the response record
        * @author Sagar Shrestha
        * @date 2014/7/10        
        */
        public Bulkified onSubmissionAddSubmittedInformation(){
            final String ACTION_NAME = 'onSubmissionAddESignature';
            //get all published only responses
            Rule publishedWithNoApproval = new Rule();
            publishedWithNoApproval.addRule(Schema.SQX_Audit_Response__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Audit_Response.STATUS_COMPLETED);
            publishedWithNoApproval.addRule(Schema.SQX_Audit_Response__c.Approval_Status__c, RuleOperator.Equals,
                 (Object) SQX_Audit_Response.APPROVAL_STATUS_NONE);

            //get all in approval responses
            Rule inApprovalRule = new Rule();
            inApprovalRule.addRule(Schema.SQX_Audit_Response__c.Status__c, RuleOperator.Equals,
                 (Object) SQX_Audit_Response.STATUS_IN_APPROVAL);
            inApprovalRule.addRule(Schema.SQX_Audit_Response__c.Approval_Status__c, RuleOperator.Equals,
                 (Object) SQX_Audit_Response.APPROVAL_STATUS_PENDING);

            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME)
                .groupByRules(new Rule[]{publishedWithNoApproval, inApprovalRule}, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Audit_Response__c> responses = new List<SQX_Audit_Response__c>();
            responses.addAll((List<SQX_Audit_Response__c>)publishedWithNoApproval.evaluationResult);
            responses.addAll((List<SQX_Audit_Response__c>)inApprovalRule.evaluationResult);
            //add the records in the list of processed records, to prevent duplicate evaluation in this same transaction
            this.addToProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME, responses );
            
            User currentUser = SQX_Utilities.getCurrentUser();
            for(SQX_Audit_Response__c response: responses){
                response.Submitted_On__c = DateTime.now();
                response.Submitted_By__c = currentUser.Name;
            }

            return this;
        }

        /**
        * this syncs status and approval status of related finding response with audit responses
        * @author Sagar Shrestha
        * @date 2014/7/10        
        */
        public Bulkified onPublishingPercolateToRelated(){
            final String ACTION_NAME = 'onPublishingPercolateToRelated';

            System.debug('Entering response percolation');

            //get all responses whose status are changed in this transaction
            Rule statusOrApprovalStatusChanged= new Rule();
            statusOrApprovalStatusChanged.addRule(Schema.SQX_Audit_Response__c.Status__c, RuleOperator.HasChanged);
            statusOrApprovalStatusChanged.addRule(Schema.SQX_Audit_Response__c.Approval_Status__c, RuleOperator.HasChanged);
            statusOrApprovalStatusChanged.operator= RuleCombinationOperator.OpOr;

            this.resetView()
                .removeProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME)
                .applyFilter(statusOrApprovalStatusChanged , RuleCheckMethod.OnCreateAndEveryEdit);

                if(statusOrApprovalStatusChanged.evaluationResult.size()>0){
                    List<SQX_Audit_Response__c> filteredAuditResponse = statusOrApprovalStatusChanged.evaluationResult; 
                    this.addToProcessedRecordsFor(SQX.AuditResponse, ACTION_NAME, filteredAuditResponse );

                    //get all related finding responses
                    List<SQX_Finding_Response__c> findingResponses= [SELECT Id, SQX_Audit_Response__c, Status__c , Approval_Status__c
                                                                                FROM SQX_Finding_Response__c WHERE SQX_Audit_Response__c in :filteredAuditResponse ];

                    Map<Id, SQX_Audit_Response__c> auditResponsesMap   = new Map<Id,SQX_Audit_Response__c>(filteredAuditResponse);                                                        
                                                                                
                    List<SQX_Finding_Response__c> findingResponseToUpdate = new List<SQX_Finding_Response__c>();

                    //sync status and approval status of finding response with audit response
                    if( findingResponses.size() > 0){
                        for(SQX_Finding_Response__c findingResponse : findingResponses){
                            SQX_Audit_Response__c auditResponse = auditResponsesMap.get(findingResponse.SQX_Audit_Response__c);
                            
                            findingResponse.Status__c= auditResponse.Status__c;
                            findingResponse.Approval_Status__c= auditResponse.Approval_Status__c;
                            findingResponseToUpdate.add(findingResponse);
                            
                        }
                    }
                    if(findingResponseToUpdate.size() > 0){
                            new SQX_DB().op_update(new List<SQX_Finding_Response__c>(findingResponseToUpdate), new List<Schema.SObjectField>{
                                SQX_Finding_Response__c.fields.Status__c, 
                                SQX_Finding_Response__c.Approval_Status__c
                            });
                        }
            }    
            
            return this;
        }
        
        /**
        * This method assigns default approver to audit response
        **/
        public Bulkified assignApproverToAuditResponse(){
            System.debug('Assigning default approver');
            this.resetView();
            List<SQX_Audit_Response__c> allAuditResponse= (List<SQX_Audit_Response__c>)this.view;
            Set<Id> allAuditIds = getIdsForField(allAuditResponse, Schema.SQX_Audit_Response__c.SQX_Audit__c);
            Map<Id, SQX_Audit__c> auditMap= new Map<Id,SQX_Audit__c>([SELECT OwnerId  from SQX_Audit__c WHERE Id in :allAuditIds]);
            for(SQX_Audit_Response__c auditResponse : allAuditResponse){
                SQX_Audit__c auditForCurrentAuditResponse= auditMap.get(auditResponse.SQX_Audit__c);
                auditResponse.Response_Approver__c= auditForCurrentAuditResponse.OwnerId;
            }

            return this;

        }


    }

}