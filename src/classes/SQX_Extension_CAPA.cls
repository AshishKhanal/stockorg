/**
* @author AR
* @description this class is the extension controller for CAPA Form with CAPA as top record
*/
public with sharing virtual class SQX_Extension_CAPA extends SQX_Extension_UI {

    /*
    * Add default policies
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
        defaultEsigPolicies.put('close', this.POLICY_COMMENT_REQUIRED);
        defaultEsigPolicies.put('void', this.POLICY_COMMENT_REQUIRED);
        return defaultEsigPolicies;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/3
    * @description This is the main entry for the constructor, it copies the sent controller to mainController for further reference
    */
    public SQX_Extension_CAPA (ApexPages.StandardController controller) {
        super(controller, new List<String> {'Status__c'});

        SQX_CAPA__c capa = (SQX_CAPA__c)controller.getRecord();


        //we don't need CAPA esig validation if CAPA is new or is in draft state.
        Boolean isRecInDraftOrNew =!(capa.Id != null && capa.Status__c != SQX_CAPA.STATUS_DRAFT);
        addStaticPolicy('save', getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.CAPA));

        this.purposeOfSigMap.putAll(new Map<String, String>{
                'save' => Label.SQX_PS_CAPA_Saving_the_record,
                'close' => Label.SQX_PS_CAPA_Closing_the_record,
                'initiate' => Label.SQX_PS_CAPA_Initiating_the_record,
                'approve' => Label.SQX_PS_CAPA_Approving_Response,
                'reject' => Label.SQX_PS_CAPA_Rejecting_Response,
                'void' => Label.SQX_PS_CAPA_Voiding_the_record,
                'submitresponse' => Label.SQX_PS_CAPA_Submitting_Response,
                'recall' => Label.SQX_PS_CAPA_Recalling_Response
            });
        this.addActionSaveAfterDraft();
        fetchRecordTypesOf.add(SQX.Finding);
    }

    /**
    * redirect if supplier to supplier page
    */
    public PageReference redirectIfSupplier(){
        if(SQX_Utilities.getCurrentUsersRole() == SQX_Utilities.SQX_UserRole.RoleSupplier){
            SQX_CAPA__c capa = (SQX_CAPA__c) getMainRecord();
            PageReference redirectTo = Page.SQX_CAPA_Response;
            
            boolean hasInitialTab = false;
            for(String paramName : ApexPages.currentPage().getParameters().keySet()){
                redirectTo.getParameters().put(paramName, ApexPages.currentPage().getParameters().get(paramName));
                hasInitialTab = hasInitialTab || 'initialtab'.equalsIgnoreCase(paramName);
                
            }
            redirectTo.getParameters().put('id', capa.Id);
            if(!hasInitialTab){
                redirectTo.getParameters().put('initialtab', 'responseHistoryTab'); //sets the default tab to response history for supplier
            }
            
            
            return redirectTo;
        }
        
        return null;
    }

    /**
    * Defines the list of entities whose data are to be exported along with the main record type
    */
    public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){

        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

        return new SObjectDataLoader.SerializeConfig()
                                    .followChild(SQX_Action__c.SQX_CAPA__c)
                                    .followChild(SQX_Impacted_Document__c.SQX_CAPA__c)
                                    .followChild(SQX_Effectiveness_Review__c.SQX_CAPA__c)
                                    .followChild(SQX_Investigation__c.SQX_CAPA__c)
                                    .followChild(SQX_Finding_Response__c.SQX_CAPA__c)
                                    .followChild(SQX_Containment__c.SQX_CAPA__c)
                                    .followChild(SQX_Response_Inclusion__c.SQX_Response__c) 
                                    .follow(SQX_Response_Inclusion__c.SQX_Investigation__c)                                   
                                    .followChild(SQX_Root_Cause__c.SQX_Investigation__c)
                                    .followChild(SQX_Root_Cause__c.SQX_Root_Cause_Code__c)
                                    .followChild(SQX_Investigation_Tool__c.SQX_Investigation__c)
                                    .followChild(SQX_Action_Plan__c.SQX_Investigation__c)
                                    .followChild(SQX_CAPA_Defect__c.SQX_CAPA__c)
                                    .followChild(SQX_CAPA_Impacted_Site__c.SQX_CAPA__c)
                                    .followChild(SQX_Resp_Inclusion_Approval__c.SQX_Response_Inclusion__c)
                                    .followChild(SQX_CAPA_Record_Activity__c.SQX_CAPA__c)
                                    .followChild(SQX_Finding_CAPA__c.SQX_CAPA__c)
                                    
                                    .getFieldsFor(SQX_Part__c.getSObjectType(), new List<SObjectField> {SQX_Part__c.Part_Number_and_Name__c, SQX_Part__c.Name, SQX_Part__c.Part_Number__c})

                                    /*attachments supported for*/
                                    .followAttachmentOf(globalDescribe.get(SQX.Containment))
                                    .followAttachmentOf(globalDescribe.get(SQX.Investigation))
                                    .followAttachmentOf(globalDescribe.get(SQX.Capa))
                                    .followAttachmentOf(globalDescribe.get(SQX.Evidence))
                                    .followAttachmentOf(globalDescribe.get(SQX.Defect))
                                    .followAttachmentOf(globalDescribe.get(SQX.Action))
                                    .followAttachmentOf(globalDescribe.get(SQX.EffectivenessReview ))
                                    .followAttachmentOf(globalDescribe.get(SQX.ImpactedDocument ))
                                    .followAttachmentOf(globalDescribe.get(SQX.CAPADefect))
                                    
                                    .followApprovalOf(globalDescribe.get(SQX.Response))

                                    .followNoteOf(globalDescribe.get(SQX.Capa))
                                    .followNoteOf(globalDescribe.get(SQX.Evidence))
                                    .followNoteOf(globalDescribe.get(SQX.Defect));
    }


    /**
    * Returns the upsert configuration that must be used by process change set.
    */
    public static SQX_Upserter.SQX_Upserter_Config getUpserterConfig(){
        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config();
        config.omit(SQX.Finding, SQX_Finding__c.Status__c)
                .omit(SQX.Capa, SQX_CAPA__c.Status__c)
                .omit(SQX.Finding, SQX_Finding__c.SQX_CAPA__c)
                .omit(SQX.Finding, SQX_Finding__c.Risk_Level__c)
                .omit(SQX.Finding, SQX_Finding__c.Resolution__c)
                .omit(SQX.Finding, SQX_Finding__c.Has_Response__c)
                .omit(SQX.Finding, SQX_Finding__c.Has_Containment__c)
                .omit(SQX.Finding, SQX_Finding__c.Has_Investigation__c)
                .omit(SQX.Finding, SQX_Finding__c.Has_Corrective_Action__c)
                .omit(SQX.Finding, SQX_Finding__c.Has_Preventive_Action__c)
                .omit(SQX.Finding, SQX_Finding__c.Completion_Date_Response__c)
                .omit(SQX.Finding, SQX_Finding__c.Completion_Date_Containment__c)
                .omit(SQX.Finding, SQX_Finding__c.Completion_Date_Investigation__c)
                .omit(SQX.Finding, SQX_Finding__c.Risk_Level__c)
                .omit(SQX.Defect, SQX_Defect__c.Part_Name__c);
          
        return config;
    }

    /**
    * @author Sagar Shrestha
    * @date 2014/11/18
    * @description override processChangeSetWithAction without electronicSignatureData, used in test class
    */
    public static String processChangeSetWithAction(String capaID, String changeset, sObject[] objectsToDelete, Map<String, String> params){
         return processChangeSetWithAction(capaID, changeset, objectsToDelete, params, null);
    }

    /**
    * @author AR
    * @date 2014/4/23
    * @description Saves a CAPA and executes next action
    */
    @RemoteAction
    public static String processChangeSetWithAction(String capaID, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData)
    {

        Boolean isNewRecord = !SQX_Upserter.isValidId(capaID);
        SQX_CAPA__c  capa;
        SQX_Extension_CAPA capaExtension;

        if(isNewRecord){
            capa = new SQX_CAPA__c();
        }
        else{
            capa = [SELECT Id, Status__c FROM SQX_CAPA__c WHERE Id = : capaID];
        }

        capaExtension = new SQX_Extension_CAPA(new ApexPages.StandardController(capa));

        Id savedCapaId = capaExtension.executeTransaction(capaID, changeset, objectsToDelete, params, esigData, getUpserterConfig(), null);
        
        SQX_CAPA__c revisedCAPA =  [SELECT Id, SQX_Revised_CAPA__c FROM SQX_CAPA__c WHERE Id =: savedCapaId];
        
        if(revisedCAPA.SQX_Revised_CAPA__c != null){
            return revisedCAPA.SQX_Revised_CAPA__c;
        } else {
            return savedCapaId;
        }
   
    }

    /**
    * Used to perform the next action based on the parameters supplied after saving record(s).
    * @param params the map containing key and value of various parameters that guide the next action
    *               example: recordId => contains the response which must be approved.
    */
    public override void processNextAction(Map<String, String> params){
        SQX_CAPA__c currentCapa = (SQX_CAPA__c) mainRecord;

        //////perform next action

        if ('initiate'.equalsIgnoreCase(nextAction)){
            SQX_CAPA__c capa = new SQX_CAPA__c();
            capa.Id = currentCapa.Id;
            capa.Status__c = SQX_CAPA.STATUS_OPEN;
            new SQX_DB().op_update(new List<SObject>{capa}, new List<SObjectField>{SQX_Capa__c.fields.Status__c});

        }
        else if('approve'.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 
            
            approveRecord(true, responseID, (String)params.get('remark'), originalApproverId);
        }
        else if('reject'.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 

            approveRecord(false, responseID, (String) params.get('remark'), originalApproverId);
        }
        else if('void'.equalsIgnoreCase(nextAction)|| 'close'.equalsIgnoreCase(nextAction)){
            Id voidCapaID = currentCapa.Id;
            SQX_Capa__c capa = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Capa__c WHERE Id = : voidCapaID]; //just ensuring record access is valid
            
            if(capa.Status__c != SQX_Finding.STATUS_CLOSED){
                capa.Status__c = SQX_Finding.STATUS_CLOSED;
                if ('void'.equalsIgnoreCase(nextAction))capa.Resolution__c = SQX_Finding.RESOLUTION_VOID;
                
                new SQX_DB().op_update(new List<SObject>{capa}, new List<SObjectField>{SQX_Capa__c.fields.Status__c, SQX_Capa__c.fields.Resolution__c});
            }
            else{
                throw new SQX_ApplicationGenericException('CAPA is alread closed');
            }
        }
        else if('recall'.equalsIgnoreCase(nextAction)){
            Id responseId = Id.valueOf(params.get('recordID'));
            SQX_Finding_Response__c response = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Finding_Response__c WHERE Id = : responseID]; //just ensuring record access is valid
            Id originalApproverID = params.get('originalApproverId') == null ? UserInfo.getUserID() : params.get('originalApproverId'); 

            recallRecord(true, responseID, (String) params.get('remark'), originalApproverId);

        }

    }

}