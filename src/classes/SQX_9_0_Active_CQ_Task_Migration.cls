/**
* Batch processor to migrate all old CQ Task record as active
* Added in version  : 9.0
* Used for versions : less than 9.0
*/
global with sharing class SQX_9_0_Active_CQ_Task_Migration implements Database.Batchable<SObject> {
    
    global SQX_9_0_Active_CQ_Task_Migration(){
        
    }
    
    /**
    * Gets invoked when the batch job starts and selects all inactive CQ Tasks
    * @param bc reference to the Database.BatchableContext object
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        Database.QueryLocator sobjecRecordList= Database.getQueryLocator('SELECT Id, Active__c FROM SQX_Task__c WHERE Active__c = false');
        return sobjecRecordList;
    }
    
    /** 
    * Execute the method to set inactive CQ Task to active
    * @param bc reference to the Database.BatchableContext object
    * @param lstsObject list of sObjects containing CQ tasks
    */ 
    global void execute(Database.BatchableContext bc, List<SObject> lstsObject) {
        if (lstsObject.size() > 0) {
            List<SQX_Task__c> uptTasks = new List<SQX_Task__c>();
            for(SQX_Task__c task : (List<SQX_Task__c>)lstsObject) {
                SQX_Task__c taskRecord = new SQX_Task__c();
                taskRecord.Id = task.Id;
                taskRecord.Active__c = true;
                uptTasks.add(taskRecord);
            }
            Database.update(uptTasks, false);
        }
    }
    global void finish(Database.BatchableContext bc){}
}