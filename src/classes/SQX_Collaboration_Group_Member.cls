/**
* @author Piyush Subedi
* @date 2016/11/28
*/
public with sharing class SQX_Collaboration_Group_Member{  

    public enum Action {
        Add,
        Remove
    }  

    /**
    *   Trigger handler
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newReq equivalent to trigger.new in salesforce
        * @param oldMap equivalent to trigger.oldMap in salesforce
        */
        public Bulkified(List<CollaborationGroupMember> newReq, Map<Id, CollaborationGroupMember> oldMap) {
            super(newReq, oldMap);            
        }

        /**
        *   Inserts feed item to group regarding addition of group member 
        */
        public Bulkified insertMemberAddedFeed(){  
            return insertFeed(Action.Add);
        }

        /**
        *   Inserts feed item to group regarding removal of group member    
        */
        public Bulkified insertMemberRemovedFeed(){ 
            return insertFeed(Action.Remove);
        }

        /**
        *   Returns a map of all the groups which are part of a controlled document record and their corresponding list of users
        */
        private Map<Id, List<User>> getValidGroupUsersMap(){
            Map<Id, List<User>> grpUsrMap = new Map<Id, List<User>>();
            List<CollaborationGroupMember> allMembers = (List<CollaborationGroupMember>) this.view;
            Map<Id, List<Id>> grpMemberMap = new Map<Id, List<Id>>();
            Map<Id, User> allUsers = new Map<Id, User>();

            for(CollaborationGroupMember member: allMembers){
                List<Id> memberIDs = grpMemberMap.get(member.CollaborationGroupId);
                if (memberIDs == null) {
                    memberIDs = new List<Id>();
                }
                memberIDs.add(member.memberId);
                grpMemberMap.put(member.CollaborationGroupId, memberIDs);
                allUsers.put(member.memberId, null);
            }

            allUsers = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN :allUsers.keySet()]);

            for (SQX_Controlled_Document__c doc : [ SELECT Collaboration_Group_Id__c FROM SQX_Controlled_Document__c
                                                    WHERE Collaboration_Group_Id__c IN :grpMemberMap.keySet() ]) {
                List<User> users = new List<User>();
                for (Id memId : grpMemberMap.get(doc.Collaboration_Group_Id__c)) {
                    users.add(allUsers.get(memId));
                }
                grpUsrMap.put(doc.Collaboration_Group_Id__c, users);
            }

            return grpUsrMap;
        }

        /**
        * Inserts feed item to group regarding addition or removal of group member   
        */
        private Bulkified insertFeed(Action act){
            this.resetView();

            if (this.view.size() > 0) {
                List<FeedItem> posts = new List<FeedItem>();    

                Map<Id, List<User>> grpUsrMap = getValidGroupUsersMap();

                for(Id grpId : grpUsrMap.keySet()){
                    for(User usr : grpUsrMap.get(grpId)){
                        FeedItem post = new FeedItem();
                        // creating feed
                        if(act == Action.Add){                       
                            post.Title = Label.CQ_UI_New_Member_Added_In_Group_Title;
                            post.Body = Label.CQ_UI_New_Member_Added_In_Group.replace('{NAME}', usr.Name);
                        }else if(act == Action.Remove){
                            post.Title = Label.CQ_UI_Member_Removed_From_Group_Title;
                            post.Body = Label.CQ_UI_Member_Removed_From_Group.replace('{NAME}', usr.Name);
                        }   
                        post.ParentId = grpId;
                        posts.add(post);
                    }
                }

                if(posts.size() > 0){
                    new SQX_DB().op_insert(posts, new List<sObjectField> {} );
                }
            }

            return this;
        }

    }
}