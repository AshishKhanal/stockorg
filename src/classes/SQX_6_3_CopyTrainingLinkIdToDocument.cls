/**
* Batch processor to set training link id in all controlled documents in pre-release state prior to 6.3.0
* Added in version  : 6.3.0
* Used for versions : less than 6.3
*/
global with sharing class SQX_6_3_CopyTrainingLinkIdToDocument implements Database.Batchable<SObject>{

    global SQX_6_3_CopyTrainingLinkIdToDocument(){}

    /**
    * Selects all documents that are in pre-release state.
    */
    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator([SELECT ID, Document_Status__c, Valid_Content_Reference__c
                    FROM SQX_Controlled_Document__c
                    WHERE Document_Status__c = :SQX_Controlled_Document.STATUS_PRE_RELEASE AND TrainingLinkId__c = null]);
    }

    /**
    * Executes to set related training link on pre-released controlled document
    */
    global void execute(Database.BatchableContext bc, List<SObject> oDocs){
        Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>((List<SQX_Controlled_Document__c>)oDocs);
        Set<Id> contentRefIds = new Set<Id>();

        for(SQX_Controlled_Document__c doc : docs.values()){
            contentRefIds.add(doc.Valid_Content_Reference__c);
        }

        List<SQX_Controlled_Document__c> docsToUpdate = new List<SQX_Controlled_Document__c>();

        // Identify the content document link for documents to update
        // Note: this can be safely assumed for all prior version because without our code adding the link manually
        // pre-release would have failed.
        for(ContentDocumentLink link : [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink
                                        WHERE LinkedEntityId IN : docs.keySet() AND ContentDocumentId IN : contentRefIds]){
            SQX_Controlled_Document__c doc = docs.get(link.LinkedEntityId);
            if(doc.Valid_Content_Reference__c == link.ContentDocumentId){
                doc.TrainingLinkId__c = link.Id;
                docsToUpdate.add(doc);
            }
        }

        /*  WARNING !!! TRIGGER DISABLED
            CQ's trigger for controlled document will be bypassed when performing this action. This will remove any side-effects that
            can occur because of updating the document.
        */
        SQX_Controlled_Document.disableTrigger = true;
        
        /**
        * WITHOUT SHARING has been used 
        * ---------------------------------
        * Without sharing has been used because the running user might not have access to set training link id
        * This will cause and error.
        */
        new SQX_DB().withoutSharing().op_update(docsToUpdate, new List<SObjectField> {
            SQX_Controlled_Document__c.TrainingLinkId__c
        });

        SQX_Controlled_Document.disableTrigger = false;

    }

    /**
    */
    global void finish(Database.BatchableContext bc){

    }

}