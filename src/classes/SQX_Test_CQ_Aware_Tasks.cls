/**
 * test class to ensure that all CQ Aware tasks are working correctly functionality works correctly
 * @author: Sagar Shrestha
 * @date: 2016-02-09
 * @story: [SQX-1947]
 */
@isTest
public class SQX_Test_CQ_Aware_Tasks{
    static boolean runAllTests = true,
                                    run_givenCreateDocImp_DocumentinPreReleased_ImpComplete= false,
                                    run_givenReviseDocImp_DocumentinPreReleased_ImpComplete= false,
                                    run_givenObseleteDocImp_DocumentinPreExpire_ImpComplete= false,
                                    run_givenImplActionOfGenericType_WhenUSerTryToCompleteTask_ThenUserRedirectToReleatedPage=false,
                                    run_givenImplementationWithOpenTask_whenUserClickOtherActionLink_ThenIfTypeIsDifferentThanGivenErrorMessage=false,
                                    run_givenObseleteDocImp_DocumentinObsolete_ImpComplete= false;

    public static final String CONTROLLED_DOC_PLAN_IMPL_VALIDATION_ERROR_MESSAGE='Controlled Document is required for Revise Document and Obsolete Document type of change.';
    /**
     *  Secnario
         *  Given: Create Document Implementation is created
         *  Action:  Referred Controlled Document is in Pre-release state
         *  Expected: Create Document Implementation is completed
    **/
    public testmethod static void givenCreateDocImp_DocumentinPreReleased_ImpComplete(){
            if(!runAllTests && !run_givenCreateDocImp_DocumentinPreReleased_ImpComplete){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
                //Arrange : Add a controlled doc, add implementation which directs to create a controlled doc
                //Add a controlled doc
                SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();

                SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();

                doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;

                doc1.save();

                //add Action 
                SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Create a New Document Please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

                ccAction.SQX_Controlled_Document_New__c = doc1.doc.Id;
                ccAction.Type__c=SQX_Implementation.AWARE_DOCUMENT_NEW;
                insert ccAction;
                //Retrive the custom meta data  of type action
                CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
                if(actions!=null){
                    for(CQ_Action__mdt cm:actions){
                        //Arrange:Go to page that complete task of action type and its component as vf page
                        pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                        pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                        pager.getParameters().put(SQX_ChangeOrder_Complete_Task.PARAM_IMPL_URL_TYPE,SQX_Implementation.AWARE_DOCUMENT_NEW);
                        Test.setCurrentPage(pager);
                        //Act:Complete Task and page redirect to its depends type 
                        SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                        pageReference newDocCompleteTask=comp.validateImplementationTask();
                        String accessUrl=page.SQX_Controlled_Document_Editor.getUrl()+'?implementation='+ccAction.Id+'&retURL='+ccAction.SQX_Change_order__c;
                        //Assert:verify that page reference when user try to complete task that redirect to its pages depends upon its action type 
                        System.assertNotEquals(null, newDocCompleteTask);
                        System.assertEquals(accessUrl, newDocCompleteTask.getURL(), 'get new document page that redirect to create new doc');
                    }
                }
                //Act : Set Status of controlled Doc to Pre-Release
                doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

                ccAction = [Select Id,  Status__c from SQX_Implementation__c where Id = :ccAction.Id];

                //Assert : Verify that releated implementation is complete
                System.AssertEquals(SQX_Implementation.STATUS_COMPLETE, ccAction.Status__c);

                

        }
    }

    /*
    * Given :Implementation with open status
    * When  :User click in other action link dont same as implementation type
    * Then  :give error message "Selected action cannot be performed based on the Implementation Plan Type."
    */
    @isTest
    public static void givenImplementationWithOpenTask_whenUserClickOtherActionLink_ThenIfTypeIsDifferentThanGivenErrorMessage(){
        if(!runAllTests && !run_givenImplementationWithOpenTask_whenUserClickOtherActionLink_ThenIfTypeIsDifferentThanGivenErrorMessage){
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(standardUser){
            //Arrange : Add a controlled doc, add implementation which directs to create a controlled doc
            //Add a controlled doc
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;
            doc1.save();

            //add Action 
            SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Create a New Document Please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

            ccAction.SQX_Controlled_Document_New__c = doc1.doc.Id;
            ccAction.Type__c=SQX_Implementation.AWARE_DOCUMENT_NEW;
            insert ccAction;
            //Retrive the custom meta data  of type action
            CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
            if(actions!=null){
                for(CQ_Action__mdt cm:actions){
                    //Arrange:Go to page that complete task of action type and its component as vf page
                    pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                    pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                    pager.getParameters().put(SQX_ChangeOrder_Complete_Task.PARAM_IMPL_URL_TYPE,SQX_Implementation.AWARE_DOCUMENT_OBSOLETE);
                    Test.setCurrentPage(pager);
                    //Act:Complete Task and page redirect to its depends type 
                    SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                    pageReference newDocCompleteTask=comp.validateImplementationTask();
                    List<Apexpages.Message> pageMessages = ApexPages.getMessages();
                    System.assertEquals(1, pageMessages.size());
                    for(ApexPages.Message msg :  ApexPages.getMessages()) {
                        System.assertEquals('Selected action cannot be performed based on the Implementation Plan Type.', msg.getSummary());
                    }
                }
            }
        }
    }
    /**
     *  Secnario
         *  Given: Revise Document Implementation is created
         *  Action:  Referred Controlled Document is in Pre-release state
         *  Expected: Revise Document Implementation is completed
    **/
    public testmethod static void givenReviseDocImp_DocumentinPreReleased_ImpComplete(){
            if(!runAllTests && !run_givenReviseDocImp_DocumentinPreReleased_ImpComplete){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
                //Arrange : Add a controlled doc, add implementation which directs to revise a controlled doc
                //Add a controlled doc
                SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();

                SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();

                doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;
                // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
                doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
                doc1.save();

                //add Action 
                SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Revise this document please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

                ccAction.SQX_Controlled_Document__c = doc1.doc.Id;
                SQX_Test_Controlled_Document revisedDoc =  doc1.revise();
                ccAction.SQX_Controlled_Document_New__c = revisedDoc.doc.Id;
                ccAction.Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;

                insert ccAction;

                //Retrive the custom meta data  of type action
                CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
                if(actions!=null){
                    for(CQ_Action__mdt cm:actions){
                        //Arrange:Go to page that complete task of action type and its component as vf page
                        pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                        pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                        pager.getParameters().put(SQX_ChangeOrder_Complete_Task.PARAM_IMPL_URL_TYPE,SQX_Implementation.AWARE_DOCUMENT_REVISE);
                        Test.setCurrentPage(pager);
                        //Act:Complete Task and page redirect to its depends type 
                        SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                        pageReference reviseCompleteTask=comp.validateImplementationTask();
                        String accessUrl=page.SQX_Revise_Controlled_Doc.getUrl()+'?id='+doc1.doc.id+'&implementation='+ccAction.Id+'&retURL='+ccAction.SQX_Change_order__c;
                        //Assert:verify that page reference when user try to complete task that redirect to its pages depends upon its action type 
                        System.assertNotEquals(null, reviseCompleteTask);
                        System.assertEquals(accessUrl, reviseCompleteTask.getURL(), 'get revise page reference that redirect to revise change order action impl ');
                    }
                }
                //Act : Revise Document
                revisedDoc.doc.Effective_Date__c = Date.Today();
                revisedDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

                ccAction = [Select Id, Status__c from SQX_Implementation__c where Id = :ccAction.Id];              

                //Assert : Verify that releated implementation is complete
                System.AssertEquals(SQX_Implementation.STATUS_COMPLETE, ccAction.Status__c);

                

        }
    }

    /**
     *  Secnario
         *  Given: Obsolete Document Implementation is created
         *  Action:  Referred Controlled Document is in Pre-expire state
         *  Expected: Obsolete Document Implementation is completed
    **/
    public testmethod static void givenObseleteDocImp_DocumentinPreExpire_ImpComplete(){
            if(!runAllTests && !run_givenObseleteDocImp_DocumentinPreExpire_ImpComplete){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
                //Arrange : Add a controlled doc, add implementation which directs to revise a controlled doc
                //Add a controlled doc
                SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();

                SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();

                doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;

                doc1.doc.Expiration_Date__c = Date.Today().addDays(1);

                doc1.save();

                //add Action 
                SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Obsolete this document please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

                ccAction.SQX_Controlled_Document__c = doc1.doc.Id;
                ccAction.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;

                insert ccAction;

                //Act : Obsolete Document
                doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_EXPIRE).save();

                ccAction = [Select Id,  Status__c from SQX_Implementation__c where Id = :ccAction.Id];

                SQX_Controlled_Document__c doc = [Select Document_Status__c from SQX_Controlled_Document__c where Id = :doc1.doc.Id]  ;

                System.AssertEquals(SQX_Controlled_Document.STATUS_PRE_EXPIRE, doc.Document_Status__c);             

                //Assert : Verify that related implementation is complete
                System.AssertEquals(SQX_Implementation.STATUS_COMPLETE, ccAction.Status__c);

                

        }
    }

    /**
     *  Secnario
         *  Given: Obsolete Document Implementation is created
         *  Action:  Referred Controlled Document is in Obsolete state
         *  Expected: Obsolete Document Implementation is completed
    **/
    public testmethod static void givenObseleteDocImp_DocumentinObsolete_ImpComplete(){
            if(!runAllTests && !run_givenObseleteDocImp_DocumentinObsolete_ImpComplete){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
                //Arrange : Add a controlled doc, add implementation which directs to revise a controlled doc
                //Add a controlled doc
                SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();

                SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();

                doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;

                doc1.doc.Expiration_Date__c = Date.Today();

                doc1.save();

                //add Action 
                SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Obsolete this document please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

                ccAction.SQX_Controlled_Document__c = doc1.doc.Id;
                ccAction.Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;

                insert ccAction;
                //Retrive the custom meta data  of type action
                CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
                if(actions!=null){
                    for(CQ_Action__mdt cm:actions){
                        //Arrange:Go to page that complete task of action type and its component as vf page
                        pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                        pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                        pager.getParameters().put(SQX_ChangeOrder_Complete_Task.PARAM_IMPL_URL_TYPE,SQX_Implementation.AWARE_DOCUMENT_OBSOLETE);
                        Test.setCurrentPage(pager);
                        //Act:Complete Task and page redirect to its depends type 
                        SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                        pageReference obsoluteCompleteTask=comp.validateImplementationTask();
                        String accessUrl=page.SQX_Document_Review_Editor.getUrl()+'?controlleddocid='+doc1.doc.id+'&implementation='+ccAction.Id+'&retURL='+ccAction.SQX_Change_order__c;
                        //Assert:verify that page reference when user try to complete task that redirect to its pages depends upon its action type 
                        System.assertNotEquals(null, obsoluteCompleteTask);
                        System.assertEquals(accessUrl, obsoluteCompleteTask.getURL(), 'get obsolute doc page reference that redirect to obsolute doc ');
                    }
                }

                //Act : Obsolete Document
                doc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();

                ccAction = [Select Id,  Status__c from SQX_Implementation__c where Id = :ccAction.Id];

                SQX_Controlled_Document__c doc = [Select Document_Status__c from SQX_Controlled_Document__c where Id = :doc1.doc.Id]  ;

                System.AssertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, doc.Document_Status__c);           

                //Assert : Verify that related implementation is complete
                System.AssertEquals(SQX_Implementation.STATUS_COMPLETE, ccAction.Status__c);             
                for(CQ_Action__mdt cm:actions){
                    //Arrange:Go to page that complete task of action type and its component as vf page
                    pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                    pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                    Test.setCurrentPage(pager);
                    //Act:Complete Task and page redirect to its depends type 
                    SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                    pageReference obsoluteCompleteTask=comp.validateImplementationTask();
                    List<Apexpages.Message> pageMessages = ApexPages.getMessages();
                    System.assertEquals(1, pageMessages.size());
                    for(ApexPages.Message msg :  ApexPages.getMessages()) {
                        System.assertEquals('Complete task can only be done when action status is in open', msg.getSummary());
                    }
                }
        }
    }
    
    
    /**
    * Given : Action implememtation of generic task
    * When: user try to complete implementation task 
    * Then: User gets redirect to its pages depends upon action types 
    */
    public testmethod static void givenImplActionOfGenericType_WhenUSerTryToCompleteTask_ThenUserRedirectToReleatedPage(){
        if(!runAllTests && !run_givenImplActionOfGenericType_WhenUSerTryToCompleteTask_ThenUserRedirectToReleatedPage){
           return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
            //Arrange : Add a controlled doc, add implementation which directs to revise a controlled doc
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            //add Action 
            SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Obsolete this document please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate
            ccAction.Type__c = 'Generic';
            insert ccAction;
            //Retrive the custom meta data  of type action
            CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
            if(actions!=null){
                for(CQ_Action__mdt cm:actions){
                    pageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                    pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                    Test.setCurrentPage(pager);
                    //Act:Complete Task and page redirect to its depends type 
                    SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                    pageReference genericCompleteTask=comp.validateImplementationTask();
                    String accessUrl=page.SQX_Change_Imp.getUrl()+'?id='+ccAction.Id+'&retURL='+ccAction.SQX_Change_order__c;
                    //Assert:verify that page reference when user try to complete task that redirect to its pages depends upon its action type 
                    System.assertNotEquals(null, genericCompleteTask);
                    System.assertEquals(accessUrl, genericCompleteTask.getURL(), 'redirect to edit page of  impl action type of generic');
                }
            }
        }
    }

    /**
    * Given :Implememtation plan of different type 
    *         i)Added doc  in new doc type
    *        ii)without doc in new doc type
    *       iii)Added doc  in generic type
    *        iv)without doc in generic type
    *         v)Added doc in obsolute doc type
    *        vi)without doc in obsolute doc type
    * When: user try to add the plan in change order
    * Then: User gets validation error message When user try to add     gives validation error message "
    */
    @isTest 
    public static void givenChangeOrderPlan_WhenUserAddControlDoc_ThenEnsureGiveErrorMessage(){


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(standardUser){
            //Arrange : create change order 
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            //Add a controlled doc
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            doc1.save();
            
            //add plan of different type
            Id recordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, SQX_Implementation.RECORD_TYPE_PLAN);
            List<SQX_Implementation__c> ccActionList=   new SQX_Implementation__c[]{
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 1',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'doc is not added in new doc type',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                            SQX_Controlled_Document__c=doc1.doc.Id,
                                                            Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW
                                                        ),
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 2',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'doc is added in new doc type',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                           Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW
                                                        ),
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 3',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'doc is added in generic type ',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                            SQX_Controlled_Document__c=doc1.doc.Id,
                                                           Type__c ='Generic'
                                                        ),
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 4',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'doc is added in generic type ',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                           Type__c ='Generic'
                                                        ),
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 5',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                            Description__c = 'obsolute doc controll doc is added',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                            SQX_Controlled_Document__c=doc1.doc.Id,
                                                            Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE
                                                        ),
                                                        new SQX_Implementation__c(
                                                            Title__c = 'Implementation - 6',
                                                            SQX_User__c = UserInfo.getUserId(),
                                                           Description__c = ' obsolute doc controll doc is not added ',
                                                            Due_Date__c = Date.Today().addDays(1),
                                                            RecordTypeId = recordTypeId,
                                                            SQX_Change_Order__c = changeOrder.Id,
                                                            Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE
                                                        )
            };
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(ccActionList,new List<SObjectField> {});
            // Assert: Ensure that validation error message when user add controll doc in different type
            System.assertEquals(CONTROLLED_DOC_PLAN_IMPL_VALIDATION_ERROR_MESSAGE, result[0].getErrors().get(0).getMessage());
            System.assertEquals(true, result[1].isSuccess(),'Expected the new doc type implementation to be saved but failed with ' + result[3]);
            System.assertEquals(CONTROLLED_DOC_PLAN_IMPL_VALIDATION_ERROR_MESSAGE, result[2].getErrors().get(0).getMessage());
            System.assertEquals(true, result[3].isSuccess(),'Expected the generic type implementation to be saved but failed with ' + result[3]);
            System.assertEquals(true, result[4].isSuccess(),'Expected the obsolute type implementation to be saved but failed with ' + result[4]);
            System.assertEquals(CONTROLLED_DOC_PLAN_IMPL_VALIDATION_ERROR_MESSAGE, result[5].getErrors().get(0).getMessage());
        }
    }

    /**
    * Given: given change order voided record
    * When: Try to complete change order voided record related tasks
    * Then: Thrown error message
    * @Story: [SQX-4064]
    */ 
    @isTest 
    public static void givenChangeOrderVoid_WhenUserTryToCompleteTheTask_ThenEnsureGiveErrorMessage(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runAs(standardUser){
            //Arrange : create void change order 
            SQX_Test_Change_Order changeOrder = new SQX_Test_Change_Order().save();
            //add Action 
            SQX_Implementation__c ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Click to Create Controlled Document', Date.Today().addDays(1));
            insert ccAction;
            changeOrder.changeOrder.Resolution_Code__c = 'Invalid';
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrder.save();

            //Retrive the custom meta data  of type action
            CQ_Action__mdt[] actions=SQX_Steps_Controller.getActionForCompleteTask(ccAction.Id.getSObjectType().getDescribe().getName());
            if(actions!=null){
                for(CQ_Action__mdt cm:actions){
                    //Arrange:Go to page that complete task of action type and its component as vf page
                    PageReference pager =new PageReference(SQX_Utilities.getPageName('', cm.Component_Name__c));
                    pager.getParameters().put(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId, ccAction.Id);
                    Test.setCurrentPage(pager);
                    //Act:Complete Task the change order voided task 
                    SQX_ChangeOrder_Complete_Task comp=new SQX_ChangeOrder_Complete_Task(new ApexPages.StandardController(new SQX_Implementation__c()));
                    //Assert: Ensured that change order voided record related task should not be completed.
                    PageReference reviseCompleteTask=comp.validateImplementationTask();
                    System.assertEquals(1, ApexPages.getMessages().size()); 
                    System.assertEquals(Label.SQX_ERR_CANNOT_COMPLETE_VOIDED_CHANGE_ORDER_TASK, ApexPages.getMessages().get(0).getDetail());
                }
            }
        }
    }
}