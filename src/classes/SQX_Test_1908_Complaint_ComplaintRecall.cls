/**
 * test class to ensure that the recall action on complaint reset complaint status to old status
 * @author: sagar Shrestha 
 * @date: 2016-01-14
 * @story: [SQX-1908] 
 */
@isTest
public class SQX_Test_1908_Complaint_ComplaintRecall{   

    static boolean runAllTests = true, 
                    run_givenComplaint_SentForClosureReview_Recalled= false;
    /**
     * Scenario 1
     * Action: Complaint is send for closure review 
     * Expected: old status is set in field old status
     * 
     * Scenario 2
     * Action : Complaint is recalled
     * Expected : Complaint is reverted back to old status
     *
     * @date: 2015-12-22
     * @author: Anish Shrestha 
     */
    public testmethod static void givenComplaint_SentForClosureReview_Recalled(){

        if(!runAllTests && !run_givenComplaint_SentForClosureReview_Recalled){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals( 1,[SELECT Id, Status__c FROM SQX_Complaint__c].size(), 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assertEquals(SQX_Complaint.STATUS_DRAFT, complaintMain.Status__c , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 3: Close the complaint
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});


            complaintMain = [SELECT Status__c, Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //save old status for further assertion
            String oldStatus = complaintMain.Status__c;
            SQX_Test_ChangeSet changeset = new SQX_Test_ChangeSet();
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            changeSet.addChanged(complaintMain)
                     .addOriginalValue(SQX_Complaint__c.Status__c, oldStatus)
                     .addOriginalValue(SQX_Complaint__c.Require_Closure_Review__c, false)
                     .addOriginalValue(SQX_Complaint__c.OwnerId, standardUser.Id)
                     .addOriginalValue(SQX_Complaint__c.Closure_Review_By__c, null);

            //Act1: Send complaint for closure review         
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, changeSet.toJSON(), null, paramsClose, null);
            complaintMain = [SELECT Record_Stage__c, Status__c, Old_Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert1 : Expected old status to be set in field 'old status'
            System.assertEquals(SQX_Complaint.STAGE_CLOSURE_REVIEW, complaintMain.Record_Stage__c, 'Expected complaint to be in review'); 
            System.assertEquals(oldStatus, complaintMain.Old_Status__c, 'Expected old status to be set in old status field on closure review');

            //Act 2: Recall complaint, which is equivalent to setting is recalled flag to true
            complaintMain = [SELECT Is_Recalled__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Is_Recalled__c = true;
            SQX_Test_ChangeSet recalledChangeset = new SQX_Test_ChangeSet();
            recalledChangeset.addChanged(complaintMain)
                     .addOriginalValue(SQX_Complaint__c.Is_Recalled__c, false);

            Map<String, String> paramsSave = new Map<String, String>();
            paramsSave.put('nextAction', 'Save');
            paramsSave.put('comment', 'Saving');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, recalledChangeset.toJSON(), null, paramsSave, null);

            complaintMain = [SELECT Status__c, Is_Recalled__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert 2: Expected status to be reset to old value and is recalled flag to be reset
            System.assertEquals(oldStatus,complaintMain.Status__c, 'Expected status to be reset to old status');
            System.assertEquals(false, complaintMain.Is_Recalled__c, 'Expected is recalled to be reset back to false');

            
            
        }
    }
}