/**
 * test class for record activity component controller used in the standard vf pages
 */
@isTest
public class SQX_Test_Controller_Sign_Off {
    
    public static Boolean runAllTests = true,
                            run_givenAUser_WhenEsigIsTurnedOff_PasswordIsNotRequiredButCommentIsRequired = false,
                            run_givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToTrue_PasswordAndCommentAreRequired = false,
                            run_givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToFalse_PasswordAndCommentAreNotRequired = false;

    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
     * GIVEN: Custom settings with esig turned off
     * WHEN: Controller is called through extension
     * THEN: Password is not required but comment is required
     * @date: 2016-02-10
     */
    public static testMethod void givenAUser_WhenEsigIsTurnedOff_PasswordIsNotRequiredButCommentIsRequired(){
        if (!runAllTests && !run_givenAUser_WhenEsigIsTurnedOff_PasswordIsNotRequiredButCommentIsRequired) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange : Create custom settings with esig turned off
            SQX_CQ_Electronic_Signature__c esSettings = new SQX_CQ_Electronic_Signature__c(
                                                                Consumer_Key__c = 'RANDOM_CONSUMER_KEY',
                                                                Consumer_Secret__c = 'RANDOM_CONSUMER_SECRET',
                                                                Enabled__c = false,
                                                                Ask_UserName__c = false);
            new SQX_DB().op_insert(new List<SQX_CQ_Electronic_Signature__c>{esSettings}, 
                                    new List<Schema.SObjectField>{});

            SQX_Test_Inspection inspection = new SQX_Test_Inspection().save();

            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_InitiatingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c 
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];

            // Act : Controller is called through extension
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection signOffController = new SQX_Extension_Inspection(controller);

            System.assertEquals(inspection.inspection.Id, signOffController.recordId);
            System.assertEquals(SQX_Inspection.PurposeOfSignature_InitiatingInspection, signOffController.actionName);
            String userDetails = signOffController.getUserDetails();
            signOffController.getSelf();
            System.assert(userDetails.contains([SELECT Name FROM User WHERE Id =: adminUser.Id].Name), 'Name should match');

            // Assert: Password is not required but comment is required
            System.assert(signOffController.getRequireComment(), 'Comment must be required');
            System.assert(!signOffController.getPasswordRequired(), 'Password must not be required');

        }

    }
    
    /**
     * GIVEN: Custom settings with esig turned on and policy defined for initiating inspection with both required and comment flag set to true
     * WHEN: Controller is called through extension
     * THEN: Password and comment are required
     * @date: 2016-02-10
     */
    public static testMethod void givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToTrue_PasswordAndCommentAreRequired(){
        if (!runAllTests && !run_givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToTrue_PasswordAndCommentAreRequired) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange : Create custom settings with esig turned off
            SQX_CQ_Electronic_Signature__c esSettings = new SQX_CQ_Electronic_Signature__c(
                                                                Consumer_Key__c = 'RANDOM_CONSUMER_KEY',
                                                                Consumer_Secret__c = 'RANDOM_CONSUMER_SECRET',
                                                                Enabled__c = true,
                                                                Ask_UserName__c = false);
            new SQX_DB().op_insert(new List<SQX_CQ_Electronic_Signature__c>{esSettings}, new List<Schema.SObjectField>{});

            SQX_CQ_Esig_Policies__c esPolicy = new SQX_CQ_Esig_Policies__c(
                                                                Name = 'RANDOM_ESIG_POLICY_NAME',
                                                                Object_Type__c = SQX.Inspection,
                                                                Action__c = SQX_Inspection.PurposeOfSignature_InitiatingInspection,
                                                                Required__c = true,
                                                                Comment__c = true);
            new SQX_DB().op_insert(new List<SQX_CQ_Esig_Policies__c>{esPolicy}, new List<Schema.SObjectField>{});

            SQX_Test_Inspection inspection = new SQX_Test_Inspection().save();

            // Act : Controller is called through extension
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_InitiatingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c 
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection signOffController = new SQX_Extension_Inspection(controller);

            // Assert : Password and comment are required
            System.assert(signOffController.getRequireComment(), 'Comment must be required');
            System.assert(signOffController.getPasswordRequired(), 'Password must be required');
        }

    }
    
    /**
     * GIVEN: Custom settings with esig turned on and policy defined for initiating inspection with both required and comment flag set to false
     * WHEN: Controller is called through extension
     * THEN: Password and comment are not required
     * @date: 2016-02-10
     */
    public static testMethod void givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToFalse_PasswordAndCommentAreNotRequired(){
        if (!runAllTests && !run_givenAUser_WhenEsigIsTurnedOnAndEsigPolicyIsAddedWithValueSetToFalse_PasswordAndCommentAreNotRequired) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange : Create custom settings with esig turned off
            SQX_CQ_Electronic_Signature__c esSettings = new SQX_CQ_Electronic_Signature__c(
                                                                Consumer_Key__c = 'RANDOM_CONSUMER_KEY',
                                                                Consumer_Secret__c = 'RANDOM_CONSUMER_SECRET',
                                                                Enabled__c = true,
                                                                Ask_UserName__c = false);
            new SQX_DB().op_insert(new List<SQX_CQ_Electronic_Signature__c>{esSettings}, new List<Schema.SObjectField>{});

            SQX_CQ_Esig_Policies__c esPolicy = new SQX_CQ_Esig_Policies__c(
                                                                Name = 'RANDOM_ESIG_POLICY_NAME',
                                                                Object_Type__c = SQX.Inspection,
                                                                Action__c = SQX_Inspection.PurposeOfSignature_InitiatingInspection,
                                                                Required__c = false,
                                                                Comment__c = false);
            new SQX_DB().op_insert(new List<SQX_CQ_Esig_Policies__c>{esPolicy}, new List<Schema.SObjectField>{});

            SQX_Test_Inspection inspection = new SQX_Test_Inspection().save();

            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_InitiatingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c 
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];

            // Act : Controller is called through extension
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection signOffController = new SQX_Extension_Inspection(controller);

            // Assert : Password and comment are not required
            System.assert(!signOffController.getRequireComment(), 'Comment must not be required');
            System.assert(!signOffController.getPasswordRequired(), 'Password must not be required');
        }

    }
}