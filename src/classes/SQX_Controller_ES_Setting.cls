/**
* @author Sagar Shrestha
* @description this class is extension for Electronic Setting Page
*/
public with sharing class SQX_Controller_ES_Setting {


    private SQX_CQ_Electronic_Signature__c mainRecord = null;
    
    public static final String CODE_OF_SAVE_ESIG_SETTING= 'saveesigsetting';
    
    public static Map<String, String> purposeOfSigMap = new Map<String, String>{
        CODE_OF_SAVE_ESIG_SETTING => Label.SQX_PS_Change_E_Sig_Setting
    };

    public Boolean ES_Enabled { get { return mainRecord.Enabled__c; } }
    public Boolean ES_Ask_Username { get { return (mainRecord.Enabled__c && mainRecord.Ask_Username__c); } }
    public String SigningOffUsername { get; set; }
    public String SigningOffPassword { get; set; }
    public String SigningOffComment { get; set; }

    public Map<String, List<SQX_Esig_Policy_Provider>> providers;

    // ApexPages.StandardSetController must be instantiated   
    // for standard list controllers       
    public ApexPages.StandardSetController esigController {
        get {
            if(esigController == null) {
                esigController = new ApexPages.StandardSetController(Database.getQueryLocator(
                       [SELECT Activity__c, Comment__c, Modified_By__c, Purpose_Of_Signature__c, CreatedDate FROM SQX_Electronic_Signature_Record_Activity__c Order BY CreatedDate DESC]));
            }
            return esigController;
        }
        set;
    }

    // Initialize esigController and return a list of records
    public List<SQX_Electronic_Signature_Record_Activity__c> getESRecordActivities() {
         esigController.setpagesize(5);
         return (List<SQX_Electronic_Signature_Record_Activity__c>) esigController.getRecords();
    }

    /*
    * get current user details for esig record activity
    */
    public String getUserDetails() {
        return SQX_Extension_UI.getUserDetailsForRecordActivity();
    }

    /**
    * Add Label of object name so that it shows correctly in the UI.
    * TODO: remove static names from the list and build it dynamically based on extensions in constructor
    */
    private final Map<String, String> objectNameToLabel = new Map<String, String>{
        SQX.Audit => 'Audit',
        SQX.AuditProgram => 'Audit Program',
        SQX.CAPA => 'CAPA',
        SQX.Finding => 'Finding',
        SQX.Nonconformance => 'Nonconformance',
        SQX.Complaint => 'Complaint',
        SQX.ChangeOrder => 'Change Order',
        SQX.TrainingSession => 'Training Session',
        SQX.ControlledDoc => 'Controlled Document',
        SQX.PersonnelAssessment => 'Personnel Assessment',
        SQX.EquipmentEventSchedule => 'Equipment Event Schedule',
        SQX.Calibration => 'Calibration',
        SQX.Equipment => 'Equipment',
        SQX.Inspection => 'Inspection',
        SQX.PersonnelDocumentTraining => 'Personnel Document Training',
        SQX.PersonnelAssessment => 'Personnel Assessment'
    };

    public Map<String, String> getObjectNameToLabel() {
        return objectNameToLabel;
    }

    public Map<String, String> actionNameToLabel { get; set; }

    /**
     * The object map containing all the objects that must be passed to the extension.
     */
    public Map<SObjectType, SObject> objectMap {get; private set;}

    /**
     * Constructor to pass an object map which can be used to inject existing record into the constructors
     * of extension classes. This allows us to get accurate policies based on the state of the record
     */
    public SQX_Controller_ES_Setting(Map<SObjectType, SObject> objectMap) {

        this.objectMap = objectMap;

        // get electronic signature settings
        mainRecord = SQX_CQ_Electronic_Signature__c.getInstance();
        if (ES_Ask_Username == false) {
            // set username of the current user when not asked
            SigningOffUsername = UserInfo.getUserName();
        }

        this.esigObjects  = new List<SelectOption>();
        for(String key : objectNameToLabel.keySet()){
            this.esigObjects.add(new SelectOption(key, objectNameToLabel.get(key)));
        }

        onLoadMethod();

        Map<String, Boolean> customizations = new Map<String, Boolean>();
        for(SQX_CQ_Esig_Policies__c policy : allEsigPolicyList){
            customizations.put(policy.Object_Type__c + policy.Action__c, policy.Required__c);
        }

        /*
        * Esig configuration Map.
        */
        Map<String, List<SQX_Esig_Policy_Provider>> extensions = getProviders();

        providers = extensions;

        this.esigActions = new Map<String, List<SelectOption>>();
        this.actionNameToLabel = new Map<String, String>();

        for(String objectName : extensions.keySet()){

            List<SelectOption> selectOptions = new List<SelectOption>();
            Map<String, String> purposeOfSigs = new Map<String, String>();
            Map<String, SQX_CQ_Esig_Policies__c> overridenPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
            Set<String> staticPolicies = new Set<String>();

            for ( SQX_Esig_Policy_Provider ext : extensions.get(objectName) ){

                purposeOfSigs = ext.getPurposeOfSig();
                overridenPolicies = ext.getEsigPolicy();
                staticPolicies = ext.getStaticPolicies();

                for(String act : purposeOfSigs.keySet()){
                    Boolean addOption = false;
                    //if it is added in static policies, then add Option would be false, else true
                    addOption = !staticPolicies.contains(act);
                    if(addOption) {
                        SelectOption option = new SelectOption(act, purposeOfSigs.get(act));
                        selectOptions.add(option);
                        this.actionNameToLabel.put(objectName + act, purposeOfSigs.get(act));
                    }
                }
            }

            this.esigActions.put(objectName, selectOptions);
        }
    }

    private Map<String, List<SQX_Esig_Policy_Provider>> getProviders() {
        return new Map<String, List<SQX_Esig_Policy_Provider>>{
            SQX.CAPA => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_CAPA(new ApexPages.StandardController(getSObjectOfType(SQX_CAPA__c.SObjectType)))
            },
            SQX.Finding => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Finding(new ApexPages.StandardController(getSObjectOfType(SQX_Finding__c.SObjectType)))
            },
            SQX.Nonconformance => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_NC(new ApexPages.StandardController(getSObjectOfType(SQX_Nonconformance__c.SObjectType)))
            },
            SQX.Audit => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Audit(new ApexPages.StandardController(getSObjectOfType(SQX_Audit__c.SObjectType)))
            },
            SQX.Complaint => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Complaint(new ApexPages.StandardController(getSObjectOfType(SQX_Complaint__c.SObjectType)))
            },
            SQX.ChangeOrder => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Change_Order(new ApexPages.StandardController(getSObjectOfType(SQX_Change_Order__c.SObjectType)))
            },
            SQX.TrainingSession => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Training_Session(new ApexPages.StandardController(getSObjectOfType(SQX_Training_Session__c.SObjectType)))
            },
            SQX.AuditProgram => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Audit_Program(new ApexPages.StandardController(getSObjectOfType(SQX_Audit_Program__c.SObjectType)))
            },
            SQX.ControlledDoc => new List<SQX_Esig_Policy_Provider>{
                new SQX_Extension_Controlled_Doc_Approval(),
                new SQX_Extension_Controlled_Doc_Checkout(new ApexPages.StandardController(new SQX_Controlled_Document__c()))                    
            },
            SQX.EquipmentEventSchedule => new List<SQX_Esig_Policy_Provider> {
                new SQX_Extension_Equipment_Event_Schedule( new ApexPages.StandardController(getSObjectOfType( SQX_Equipment_Event_Schedule__c.SObjectType)))
            },
            SQX.Inspection  => new List<SQX_Esig_Policy_Provider> {
                new SQX_Extension_Inspection( new ApexPages.StandardController(new SQX_Inspection__c()))
            },
            SQX.Calibration => new List<SQX_Esig_Policy_Provider> {
                new SQX_Extension_Calibration()
            },
            SQX.Equipment => new List<SQX_Esig_Policy_Provider> {
                new SQX_Extension_Equipment(new ApexPages.StandardController(getSObjectOfType( SQX_Equipment__c.SObjectType)))
            },
            SQX.PersonnelDocumentTraining => new List<SQX_Esig_Policy_Provider> {
                new SQX_Controller_eContent(getSObjectOfType(SQX_Personnel_Document_Training__c.SObjectType).Id)
                        .setPurposeOfSig(false)
            },
            SQX.PersonnelAssessment => new List<SQX_Esig_Policy_Provider> {
                new SQX_Controller_eContent(getSObjectOfType(SQX_Personnel_Document_Training__c.SObjectType).Id)
                        .setPurposeOfSig(true),
                new SQX_Controller_Take_Assessment()
            }
        };
    }

    public SQX_Controller_ES_Setting (){
        this(new Map<SObjectType, SObject>());
    }

    /**
     * Internal method that returns any record present in object map or new sobject if missing
     */
    private SObject getSObjectOfType(SObjectType sobjType) {
        if(objectMap.containsKey(sobjType)) {
            return objectMap.get(sobjType);
        } else {
            return sobjType.newSObject();
        }
    }
    
    public List<SelectOption> esigObjects {get; set;}
    public Map<String, List<SelectOption>> esigActions {get; set;}

    private void initElectronicSignatureSetting() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance(UserInfo.getOrganizationId());
        esSettings = instance;
        
        esSettings.Consumer_Key__c = !String.isBlank(esSettings.Consumer_Key__c)? 'Old Key Key Key Key' : '';//made long to give feel that consumer key is the longer
        esSettings.Consumer_Secret__c = !String.isBlank(esSettings.Consumer_Secret__c) ? 'Old Secret' : '';
        
    }

    public SQX_CQ_Electronic_Signature__c esSettings {
        get {
            if (esSettings == null) {
                initElectronicSignatureSetting();
            }
            return esSettings;
        }
        set;
    }

    public List<SQX_CQ_Esig_Policies__c> allEsigPolicyList = new List<SQX_CQ_Esig_Policies__c>();
    public List<SQX_CQ_Esig_Policies__c> removeEsigPolicyList = new List<SQX_CQ_Esig_Policies__c>();

    public void onLoadMethod(){
        allEsigPolicyList = [SELECT Name, Object_Type__c, Action__c, Required__c, Comment__c FROM SQX_CQ_Esig_Policies__c ORDER BY Object_Type__c ASC];
    }

    public List<SQX_CQ_Esig_Policies__c> getEsigPolicy(){
        return allEsigPolicyList;
    }
    //Add a temporary policy to the table. Not saved to the database
    public void addPolicy(){
        SQX_CQ_Esig_Policies__c policy = new SQX_CQ_Esig_Policies__c();
        Blob aes = Crypto.generateAesKey(128);
        String hex = EncodingUtil.convertToHex(aes);
        policy.Name = hex;
        policy.Object_Type__c = SQX.Audit;
        policy.Required__c = true;
        policy.Action__c = '';
        policy.Comment__c = true;
        allEsigPolicyList.add(policy);
    }

    //Remove a policy from the table.
    public void removePolicy(){
        Integer indexVal = Integer.valueof(Apexpages.currentPage().getparameters().get('index'));
        //If the policy is an existing policy then add it to the list to delete from the databse
        if(allEsigPolicyList[indexVal - 1].Id != null)
            removeEsigPolicyList.add(allEsigPolicyList[indexVal - 1]);
        //Remove the policy from the table    
        allEsigPolicyList.remove(indexVal - 1);            
    } 

    public PageReference saveElectronicSignatureSetting() {

        Boolean hasError = false;        
        PageReference returnUrl = null;
        SavePoint sp = null; 
        Database.SaveResult saveResultESigSetting; 
        Database.SaveResult saveResultEsigPolicy;
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance(UserInfo.getOrganizationId());
        //if consumer key is not changed then, take the old consumer key
        if(esSettings.Consumer_Key__c == 'Old Key Key Key Key')
            esSettings.Consumer_Key__c = instance.Consumer_Key__c;
        //if consumer secret is not changed then, take the old consumer secret    
        if(esSettings.Consumer_Secret__c == 'Old Secret')
            esSettings.Consumer_Secret__c = instance.Consumer_Secret__c;
        
        if (ES_Enabled == false || new SQX_OauthEsignatureValidation().validateCredentials(SigningOffUsername, SigningOffPassword, esSettings.Consumer_Key__c, esSettings.Consumer_Secret__c)) {
            try {
                //set savepoint
                sp = Database.setSavePoint();

                for(SQX_CQ_Esig_Policies__c policyToCheck : allEsigPolicyList){
                    String existingPolicy = String.valueOf(policyToCheck.Object_Type__c) + String.valueOf(policyToCheck.Action__c);
                    
                    for(SQX_CQ_Esig_Policies__c newEsigPolicy : allEsigPolicyList){
                        String newPolicy = String.valueOf(newEsigPolicy.Object_Type__c) + String.valueOf(newEsigPolicy.Action__c);
                        
                        if(existingPolicy == newPolicy && policyToCheck != newEsigPolicy){
                            hasError = true;
                            policyToCheck.addError('The object ' + objectNameToLabel.get(policyToCheck.Object_Type__c) +' with same action already exist.');
                        }
                    }
                }
                if(!hasError){

                    if(esSettings.Id != null){
                        saveResultESigSetting = new SQX_DB().op_update(new List<SQX_CQ_Electronic_Signature__c>{esSettings}, new List<Schema.SObjectField>{
                                Schema.SQX_CQ_Electronic_Signature__c.Consumer_Secret__c
                            }).get(0);
                        }
                    else{
                        saveResultESigSetting = new SQX_DB().op_insert(new List<SQX_CQ_Electronic_Signature__c>{esSettings}, new List<Schema.SObjectField>{
                                Schema.SQX_CQ_Electronic_Signature__c.Consumer_Secret__c
                            }).get(0);
                    }


                    List<SQX_CQ_Esig_Policies__c>   policiesToUpdate = new List<SQX_CQ_Esig_Policies__c>(),
                                                    policiesToInsert = new List<SQX_CQ_Esig_Policies__c>();

                    for(SQX_CQ_Esig_Policies__c policy : allEsigPolicyList){
                        if(policy.Id != null)
                            policiesToInsert.add(policy);
                        else
                            policiesToUpdate.add(policy);
                    }
                    
                    allEsigPolicyList.clear();
                    if(policiesToInsert.size() > 0){
                        saveResultEsigPolicy = new SQX_DB().op_update(policiesToInsert, new List<Schema.SObjectField>{
                            Schema.SQX_CQ_Esig_Policies__c.Action__c,
                            Schema.SQX_CQ_Esig_Policies__c.Object_Type__c,
                            Schema.SQX_CQ_Esig_Policies__c.Comment__c,
                            Schema.SQX_CQ_Esig_Policies__c.Required__c
                        }).get(0);
                    }
                    
                    if(policiesToUpdate.size() > 0){
                        saveResultEsigPolicy = new SQX_DB().op_insert(policiesToUpdate, new List<Schema.SObjectField>{
                            Schema.SQX_CQ_Esig_Policies__c.Action__c,
                            Schema.SQX_CQ_Esig_Policies__c.Object_Type__c,
                            Schema.SQX_CQ_Esig_Policies__c.Comment__c,
                            Schema.SQX_CQ_Esig_Policies__c.Required__c
                        }).get(0);
                    }
                    
                    //delete policies that were removed
                    if(removeEsigPolicyList.size() > 0){
                        new SQX_DB().op_delete(removeEsigPolicyList); 
                        removeEsigPolicyList.clear();      
                    }
                    
                    // reload settings and polies from db after saving
                    esSettings = null;
                    onLoadMethod();
                }
                
                // if updating esig record is sucess
                if (saveResultESigSetting.isSuccess() && (saveResultEsigPolicy==null || saveResultEsigPolicy.isSuccess())) {
                    // insert record activity
                    Id esigId = saveResultESigSetting.getId();
                    SQX_Record_History.insertRecordHistory(string.valueOf(SQX_CQ_Electronic_Signature__c.SObjectType), esigId, (String) esigId, SigningOffComment, purposeOfSigMap.get(CODE_OF_SAVE_ESIG_SETTING), false, CODE_OF_SAVE_ESIG_SETTING);
                    returnUrl = new PageReference('/apex/SQX_ES_Settings');
                    returnUrl.setRedirect(true);   
                }
                else{
                    for(Database.Error error : saveResultESigSetting.getErrors()){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                    }
                    if (saveResultEsigPolicy != null) {
                        for(Database.Error error : saveResultEsigPolicy.getErrors()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                        }
                    }
                    
                    Database.rollback(sp);
                }
            } catch(Exception ex) {
                // ignore error cause these will have been added by the dml exceptions
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                
                // rollback savepoint
                if (sp != null) {
                    Database.rollback(sp);
                }
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
        }
        return returnUrl;
    }

    public PageReference cancelElectronicSignatureSetting(){   
       PageReference pageRef= Page.SQX_ES_Settings;
       pageRef.setRedirect(true);        
       return pageRef;           
    }
    
}