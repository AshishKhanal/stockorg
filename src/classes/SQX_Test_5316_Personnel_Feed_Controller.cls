/**
 * This test class verifies that the controller SQX_Consolidated_Home_Page_Component returns valid items for personnel.
 * @author : Piyush Subedi
 * @story  : [SQX-5316],[SQX-7241]
 */
@isTest
public class SQX_Test_5316_Personnel_Feed_Controller {
    @testSetup
    static void commonSetup() {
        // add required users
        User stdUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'StdUser1');
    }
    
    /**
     * Given : Pending Trainings for given personnel
     * When : Controller method 'getPendingItems()' is called
     * Then : Correct number of pending items should be returned
     */
    public testmethod static void ensurePersonnelFeedViewControllerReturnsCorrectFeedItems() {
        User StdUser1 = SQX_Test_Account_Factory.getUsers().get('StdUser1');
        System.runAs(StdUser1) {
            
            // Arrange : Create document, personnel record and personnel document training
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            SQX_Test_Personnel psn = new SQX_Test_Personnel();
            psn.mainRecord.SQX_User__c = StdUser1.Id;
            psn.save();
            
            SQX_Personnel_Document_Training__c pdt = new SQX_Personnel_Document_Training__c(
                                                         SQX_Controlled_Document__c = doc.doc.id,
                                                         Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                                                         Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                                                         SQX_Personnel__c = psn.mainRecord.id,
                                                         Due_Date__c = Date.today(),
                                                         Completion_Date__c = Date.today()
            );
            //insert
            new SQX_DB().op_insert(new List<SQX_Personnel_Document_Training__c> {pdt}, new List<Schema.SObjectField> {});
            
            // Act : call controller method 'getPendingItems()' to get the pending feed items
            // de-serialize the string HomepageItems
            SQX_Consolidated_Home_Page_Component.HomepageItems feedItems = (SQX_Consolidated_Home_Page_Component.HomepageItems)JSON.deserialize(SQX_Consolidated_Home_Page_Component.getPendingItems(), SQX_Consolidated_Home_Page_Component.HomepageItems.class);
            
            // Assert : There should be one pending feed item
            System.assert(feedItems.items.size() == 1, 'Expected one pending feed items but got ' + feedItems.items.size());
            // Assert : 2 actions should be associated with item i.e. view and user sign off
            System.assert(feedItems.items[0].actions.size() == 2, 'Expected 2 action associated with the feed item but got ' + feedItems.items[0].actions.size());
            // Assert : relatedRecord should be document in this case
            System.assert(feedItems.items[0].relatedRecord.Id == doc.doc.Id, 'Expected feed id to be ' + doc.doc.Id + 'but got ' + feedItems.items[0].relatedRecord.Id);
            
           
            // Assert : There should be 2 actions with name 'View' and 'User Sign Of'
            Map<String, SQX_Homepage_Item_Action> actionMap = new Map<String, SQX_Homepage_Item_Action>();
            for (SQX_Homepage_Item_Action hia : feedItems.items[0].actions) {
                actionMap.put(hia.name, hia);
            }

            System.assert(actionMap.get(SQX_Homepage_Constants.ACTION_USER_SIGNOFF) != null, 'Expected action name to be ' + SQX_Homepage_Constants.ACTION_USER_SIGNOFF +' but is ' + actionMap.get(SQX_Homepage_Constants.ACTION_USER_SIGNOFF));
            System.assert(actionMap.get(SQX_Homepage_Constants.ACTION_VIEW_CONTENT) != null, 'Expected action name to be ' + SQX_Homepage_Constants.ACTION_VIEW_CONTENT +' but is ' + actionMap.get(SQX_Homepage_Constants.ACTION_VIEW_CONTENT));
        }
    }
}