/**
 * test class to ensure all the methods of the complaint extension are working as per their functionality
 * @author: Anish Shrestha
 * @date: 2015-12-04
 * @story: [SQX-938]
 */
@isTest
public class SQX_Test_Complaint_Extension{

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                         Active__c = true,
                                                         Defect_Category__C = 'Test_Category',
                                                         Description__c = 'Test Description',
                                                         Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }        
    }

    static boolean runAllTests = true,
                    run_givenAUser_ComplaintCanBeCreated = false,
                    run_givenAUser_ComplaintCanBeSubmitted = false,
                    run_givenAUser_ComplaintCanBeSubmittedThenTakeOwnershipAndInitate = false,
                    run_givenAUser_ComplaintCanBeInitated = false,
                    run_givenAUser_ComplaintCanBeClosedWithoutClosureReview = false,
                    run_givenAUser_ComplaintCannotBeSendForClosureReviewWithCurrentOwnerAsApprover= false,
                    run_givenAUser_ComplaintCanBeVoid = false,
                    run_givenAUser_ComplaintCanBeClosedWithClosureReviewAndApproved = false,
                    run_givenAUser_ComplaintCanBeClosedWithClosureReviewAndRejected = false;

    /**
     *   Action: Create a complaint
     *   Expected: The complaint is saved after entering all required information.
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeCreated(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeCreated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);
        }
    }

    /**
     *   Action: Saved complaint is submitted
     *   Expected: Complaint is submitted and its status is changed to triage
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeSubmitted(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeSubmitted){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Submit the complaint
            Map<String, String> paramsSubmit = new Map<String, String>();
            paramsSubmit.put('nextAction', 'Submit');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsSubmit, null);
            complaintMain = [SELECT Id, Record_Stage__c, Status__c, OwnerId, SQX_Department__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in triage
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_TRIAGE , 'Expected the stage to be in Triage and found: ' + complaintMain.Record_Stage__c);
        }
    }

    /**
     *   Action: Submitted complaint is taken ownership and initiated
     *   Expected: Comlaint in the queue can be taken ownership and intiated and its status is changed to open.
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeSubmittedThenTakeOwnershipAndInitate(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeSubmittedThenTakeOwnershipAndInitate){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Submit the complaint
            Map<String, String> paramsSubmit = new Map<String, String>();
            paramsSubmit.put('nextAction', 'Submit');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsSubmit, null);
            complaintMain = [SELECT Id, Record_Stage__c, Status__c, OwnerId, SQX_Department__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in triage
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_TRIAGE , 'Expected the stage to be in Triage and found: ' + complaintMain.Record_Stage__c);
        
            //Act 3: Take ownership and initiate the complaint
            Map<String, String> paramsTakeOwnershipAndInitiate = new Map<String, String>();
            paramsTakeOwnershipAndInitiate.put('nextAction', 'takeownershipandinitiate');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsTakeOwnershipAndInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 4: Expected the status of the complaint to be in triage
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 5: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

        }
    }

    /**
     *   Action: Saved complaint is initiated diretly.
     *   Expected: Complaint can be initated, its status becomes open, open date is set to today and its ownership is the user who initiated the complaint.
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeInitated(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeInitated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId, Open_Date__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');
            // Assert 5: Expected the status of the complaint to be in STATUS_OPEN
            System.assertEquals(Date.Today(), complaintMain.Open_Date__c , 'Expected Open Date to be set to Today but found: ' + complaintMain.Open_Date__c);
            
        }
    }

    /**
     *   Action: Complaint is closed.
     *   Expected: Saved complaint can be closed at any state if the closure review is not required.
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeClosedWithoutClosureReview(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeClosedWithoutClosureReview){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, SQX_Finding__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Close the complaint
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;

            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.SQX_Conclusion_Code__c, 
                                                                                            SQX_Complaint__c.fields.Resolution__c});
                                                                                            
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be in Closed and found: ' + complaintMain.Status__c);

        }
    }

    /**
     *   Action: Closure Reviewer is assigned as complaint owner.
     *   Expected: Closure reviewer cannot be the complaint owner and throws error.
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCannotBeSendForClosureReviewWithCurrentOwnerAsApprover(){

        if(!runAllTests && !run_givenAUser_ComplaintCannotBeSendForClosureReviewWithCurrentOwnerAsApprover){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be current user');

            //Act 3: Set the current user as the closure review approver

            complaintMain = [SELECT Id, Status__c, Require_Closure_Review__c, Closure_Review_By__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            complaintMain.Require_Closure_Review__c = true;
            complaintMain.Closure_Review_By__c = standardUser.Id;

            try{
                new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                                SQX_Complaint__c.fields.Require_Closure_Review__c});
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Complaint Owner Cannot Be Closure Reviewer for the same complaint.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 

        }
    }

    /**
     *   Action: Saved complaint is voided.
     *   Expected: Compaint can be voided by providing appropriate comments
     * @author: Anish Shrestha
     * @date: 2015-12-04
     * @story: [SQX-938]
     */
    public testmethod static void givenAUser_ComplaintCanBeVoid(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeVoid){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Act 1: Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Complaint__c complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Void the complaint
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            Map<String, String> paramsVoid = new Map<String, String>();
            paramsVoid.put('nextAction', 'Void');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsVoid, null);
            complaintMain = [SELECT Id, Resolution__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_VOID , 'Expected the status to be in Void and found: ' + complaintMain.Status__c);
            // Assert 4 : Expected the resolution of the complain to be voided
            System.assertEquals(complaintMain.Resolution__c, 'Void' , 'Expected the resolution to be in Void and found: ' + complaintMain.Resolution__c);

        }
    }

    /**
     *   Action: Complaint is closed with closure review required and approved
     *   Expected: Complaint is sent for closure review 
     *              and when approved complaint is closed 
     *              and the user who approved the record is the closure reviewer.
     * @author: Anish Shrestha
     * @date: 2015-12-16
     * @story: [SQX-1756]
     */
    public testmethod static void givenAUser_ComplaintCanBeClosedWithClosureReviewAndApproved(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeClosedWithClosureReviewAndApproved){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, SQX_Finding__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Close the complaint
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                            SQX_Complaint__c.fields.OwnerId});
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Record_Stage__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STAGE_CLOSURE_REVIEW
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REVIEW , 'Expected the stage to be in in closure review but found: ' + complaintMain.Record_Stage__c);
           


        }
        System.runas(standardUserApprover){
            //Act 4: Approve the closure review
            Map<String, String> paramsApproveApproval = new Map<String, String>();
            paramsApproveApproval.put('nextAction', 'approveapproval');
            paramsApproveApproval.put('workItemId', complaintMain.Id); // This ID is not needed for test, so sent a dummy Id

            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsApproveApproval, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be closed but found: ' + complaintMain.Status__c);

        }
    }

    /**
     *   Action: Complaint is closed with closure review required and reject
     *   Expected: Complaint is sent for closure review 
     *              and when rejected complaint status becomes closure rejected
     *              and the close by and close date are reset.
     * @author: Anish Shrestha
     * @date: 2015-12-17
     * @story: [SQX-1756]
     */
    public testmethod static void givenAUser_ComplaintCanBeClosedWithClosureReviewAndRejected(){

        if(!runAllTests && !run_givenAUser_ComplaintCanBeClosedWithClosureReviewAndRejected){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, SQX_Finding__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Close the complaint
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                            SQX_Complaint__c.fields.OwnerId});
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Record_Stage__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STAGE_CLOSURE_REVIEW
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REVIEW , 'Expected the stage to be in in closure review but found: ' + complaintMain.Record_Stage__c);


        }
        System.runas(standardUserApprover){
            //Act 4: Approve the closure review
            Map<String, String> paramsRejectApproval = new Map<String, String>();
            paramsRejectApproval.put('nextAction', 'rejectapproval');
            paramsRejectApproval.put('workItemId', complaintMain.Id);// This ID is not needed for test, so sent a dummy Id

             Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsRejectApproval, null);
            complaintMain = [SELECT Id, Record_Stage__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REJECTED , 'Expected the stage to be closure rejected but found: ' + complaintMain.Record_Stage__c);

        }
    }
}