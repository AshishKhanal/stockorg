/**
* This class stores the common operations to store business rules in Audit Program
* 
*/
public with sharing class SQX_Audit_Program {

    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_ACTIVE = 'Active';
    public static final String STATUS_CLOSED = 'Closed';
    public static final String STATUS_VOID = 'Void';
    public static final String APPROVAL_STATUS_PLAN_APPROVAL = 'Plan Approval';
    public static final String APPROVAL_STATUS_REJECTED = 'Rejected';
    public static final String APPROVAL_STATUS_APPROVED = 'Approved';
    
    /**
    * audit program closed statuses
    */
    public static final List<String> CLOSED_STATUSES = new List<String>{
        STATUS_CLOSED,
        STATUS_VOID
    };


    /**
    * This is the collection of methods that perform bulkified operation on Audit Program object
    * These methods are called from the audit trigger based on the type of trigger.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified(){
        }


        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newPrograms equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Audit_Program__c> newPrograms, Map<Id,SQX_Audit_Program__c> oldMap){
        
            super(newPrograms, oldMap);
        }
        
        /**
        * sets Unique Name field with audit program Name
        */
        public Bulkified setUniqueName() {
            this.resetView();
            
            for (SQX_Audit_Program__c ap : (List<SQX_Audit_Program__c>)this.newValues) {
                ap.Unique_Name__c = ap.Name;
            }
            
            return this;
        }


        /**
        * Related to : SQX-1136
        * This method ensures that the calendar id entered is valid Public Calendar/Resource Id and nothing else
        * @author Pradhanta Bhandari
        */
        public Bulkified ensureCalendarIdIsValid(){
            
            Rule hasCalendarIdChanged = new Rule();
            hasCalendarIdChanged.addRule(Schema.SQX_Audit_Program__c.Calendar__c, RuleOperator.HasChanged);
            hasCalendarIdChanged.addRule(Schema.SQX_Audit_Program__c.Calendar__c, RuleOperator.NotEquals, null);
            
            this.resetView().applyFilter(hasCalendarIdChanged, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Audit_Program__c> validAudits = (List<SQX_Audit_Program__c>) hasCalendarIdChanged.evaluationResult;
            for(SQX_Audit_Program__c program : validAudits){

                Boolean isIdValid = false;
                
                //check if id is valid
                try{
                    /*
                    * to check if the id is valid we use the following test
                    * i. the entered string value is a valid id.
                    * ii. the sobjecttype of the Id is 'Calendar'
                    * Note: the sobjecttype is matched using string because SF doesn't expose Calendar object.
                    */
                    Id calendarId = Id.valueOf(program.Calendar__c);
                    String objectType = calendarId.getSObjectType() + ''; //converting the sObjectType to string to change it to the Calendar type
                    isIdValid = objectType.equals('Calendar');
                }
                catch(Exception ex){
                    //if the id is not valid and an exception is thrown just ignore it.
                }

                //if the id is present and not valid add the exception message.
                if(!isIdValid)
                    program.Calendar__c.addError(Label.SQX_ERR_MSG_INVALID_CALENDAR_ID);
            }

            return this;
        }


        /*
        * From SQX-1136:
        * "When a program is Activated, all of its related audits will be considered 'Scheduled' " 
        *
        * This method ensures that when an audit program is activated/approved all related audits are set to scheduled
        * @author Pradhanta Bhandari
        */
        public Bulkified scheduleAllPlannedAuditsWhenApproved(){

            Rule allApprovedPrograms = new Rule();
            allApprovedPrograms.addTransitionRule(Schema.SQX_Audit_Program__c.Approval_Status__c, RuleOperator.Equals, APPROVAL_STATUS_APPROVED, APPROVAL_STATUS_PLAN_APPROVAL);
            
            this.resetView().applyFilter(allApprovedPrograms, RuleCheckMethod.OnCreateAndEveryEdit);

            if(allApprovedPrograms.evaluationResult.size() > 0){

                //select all audit and then schedule it.
                Map<Id, SObject> allAuditPrograms = new Map<Id, SObject>(allApprovedPrograms.evaluationResult);
                List<SQX_Audit__c> allAudits = [SELECT Id, Stage__c FROM SQX_Audit__c 
                            WHERE Stage__c = : SQX_Audit.STAGE_PROGRAM_APPROVAL AND SQX_Audit_Program__c IN : allAuditPrograms.keySet() ];

                for(SQX_Audit__c audit : allAudits){
                    audit.Stage__c = SQX_Audit.STAGE_SCHEDULED;
                    audit.Status__c = SQX_Audit.STATUS_OPEN;
                }

                /*
                * WITHOUT Sharing has been used because audit(s) included in audit program may be created by 
                * different users and the sharing setting has been configured to make Audit 'Public Read Only'.
                * This implies that the approver will not be able to approve all the audits i.e. change their statuses.
                */
                new SQX_DB().withoutSharing().op_update(allAudits,  new List<Schema.SObjectField>{
                    SQX_Audit__c.fields.Status__c,
                    SQX_Audit__c.fields.Stage__c
                });
            }

            return this;
        }
        
        /**
        * Set Audit's Stage to Program Approval when Program is submitted for approval and back to Plan when Rejected or Recalled
        */
        public Bulkified setRelatedAuditsToPlanAndProgramApproval(){
            
            Rule allPlanApprovalApprovedRecalledOrRejectedPrograms = new Rule();
            allPlanApprovalApprovedRecalledOrRejectedPrograms.addTransitionRule(Schema.SQX_Audit_Program__c.Approval_Status__c, RuleOperator.Equals, APPROVAL_STATUS_REJECTED, APPROVAL_STATUS_PLAN_APPROVAL);
            allPlanApprovalApprovedRecalledOrRejectedPrograms.addTransitionRule(Schema.SQX_Audit_Program__c.Approval_Status__c, RuleOperator.Equals, null, APPROVAL_STATUS_PLAN_APPROVAL);
            allPlanApprovalApprovedRecalledOrRejectedPrograms.addRule(Schema.SQX_Audit_Program__c.Approval_Status__c, RuleOperator.Equals, APPROVAL_STATUS_PLAN_APPROVAL);
            allPlanApprovalApprovedRecalledOrRejectedPrograms.operator = RuleCombinationOperator.OpOr;
            
            this.resetView().applyFilter(allPlanApprovalApprovedRecalledOrRejectedPrograms, RuleCheckMethod.OnCreateAndEveryEdit);
            
            if(allPlanApprovalApprovedRecalledOrRejectedPrograms.evaluationResult.size() > 0){
                
                Map<Id, SObject> allFilteredAuditPrograms = new Map<Id, SObject>(allPlanApprovalApprovedRecalledOrRejectedPrograms.evaluationResult);
                List<SQX_Audit__c> childAudits = [SELECT Id, Stage__c, Name, SQX_Audit_Program__r.Approval_Status__c, SQX_Audit_Program__c FROM SQX_Audit__c WHERE 
                                                  (Stage__c = : SQX_Audit.STAGE_PROGRAM_APPROVAL OR Stage__c = : SQX_Audit.STAGE_PLAN) AND 
                                                  Status__c = : SQX_Audit.STATUS_DRAFT AND 
                                                  SQX_Audit_Program__c IN : allFilteredAuditPrograms.keySet() ];
                
                for(SQX_Audit__c audit : childAudits){
                    if(audit.SQX_Audit_Program__r.Approval_Status__c == APPROVAL_STATUS_PLAN_APPROVAL){
                        audit.Stage__c = SQX_Audit.STAGE_PROGRAM_APPROVAL;
                    }else{
                        audit.Stage__c = SQX_Audit.STAGE_PLAN;
                    }
                }
                
                /*
                * WITHOUT Sharing has been used because audit(s) included in audit program may be created by 
                * different users and the sharing setting has been configured to make Audit 'Public Read Only'.
                * This implies that user won't be able to change audit's status.
                */
                
                List<Database.SaveResult> results = new SQX_DB().continueOnError().withoutSharing().op_update(childAudits,  new List<Schema.SObjectField>{
                    SQX_Audit__c.fields.Stage__c
                });
                
                Map<Id, String> auditProgramErrorMap = new Map<Id, String>();
                
                // loop through database save result and map audit program and it errors.
                for(Integer i = 0; i < results.size(); i++){
                    Database.SaveResult sr = results[i];
                    if(sr.isSuccess()){
                        continue;
                    }                    
                    SQX_Audit__c erroneousAudit= childAudits[i];
                    Id parentProgramId = erroneousAudit.SQX_Audit_Program__c;
                    String errMsg= erroneousAudit.Name+': '+sr.getErrors()[0].getMessage()+' \n';
                    
                    if(auditProgramErrorMap.containsKey(parentProgramId)){
                        auditProgramErrorMap.put(parentProgramId, auditProgramErrorMap.get(parentProgramId)+errMsg);
                    }else{
                        auditProgramErrorMap.put(parentProgramId, errMsg);
                    }
                }
                
                // loop through auditProgramErrorMap and add error
                for(Id programId: auditProgramErrorMap.keySet()){
                    String errMsg=Label.SQX_ERR_Submitting_Audit_Program_For_Approval+' \n'+auditProgramErrorMap.get(programId);
                    allFilteredAuditPrograms.get(programId).addError(errMsg);
                }
            }
            
            return this;
        }


        /*
        * From SQX-1136:
        * "A program may be Voided by owner after it is activated as long as no Audits are in process."
        *
        * This method ensures that when an audit program is voided audits are not already started.
        * @author Pradhanta Bhandari
        */
        public Bulkified ensureOnlyAuditsWithoutInProcessCanBeVoided(){

            Rule allVoidedAuditPrograms = new Rule();
            allVoidedAuditPrograms.addRule(Schema.SQX_Audit_Program__c.Status__c, RuleOperator.Equals, STATUS_VOID);
            
            this.resetView().applyFilter(allVoidedAuditPrograms, RuleCheckMethod.OnCreateAndEveryEdit);
                
            if(allVoidedAuditPrograms.evaluationResult.size() > 0){
                Map<Id, SObject> allAuditPrograms = new Map<Id, SObject>(allVoidedAuditPrograms.evaluationResult);
            

                //select all audits that are in progress state for the program, presence of in progress audits
                //means that the audit program can't be voided.
                List<SQX_Audit__c> allAudits = [SELECT Id, Stage__c, SQX_Audit_Program__c FROM SQX_Audit__c 
                            WHERE Stage__c = :SQX_Audit.STAGE_IN_PROGRESS AND SQX_Audit_Program__c IN : allAuditPrograms.keySet() ];

                for(SQX_Audit__c audit : allAudits){
                    SQX_Audit_Program__c programWithInProcessAudit = (SQX_Audit_Program__c)allAuditPrograms.get(audit.SQX_Audit_Program__c);
                    if(programWithInProcessAudit != null){
                        programWithInProcessAudit.addError(Label.SQX_ERR_MSG_AUDIT_PRGM_CANT_BE_VOIDED);
                        allAuditPrograms.remove(audit.SQX_Audit_Program__c); //we don't want to add the same error multiple times if more than one inprocess entity is present
                    }
                }
            }

            return this;
        }


        /*
        * From SQX-1136:
        * "If a program is Voided, then all planned audits are voided "
        *
        * This method ensures that when an audit program is voided all related audits are voided.
        * @author Pradhanta Bhandari
        */
        public Bulkified voidAllPlannedAuditsForTheAuditProgram(){

            Rule allVoidedAuditPrograms = new Rule();
            allVoidedAuditPrograms.addRule(Schema.SQX_Audit_Program__c.Status__c, RuleOperator.Equals, STATUS_VOID);
            
            this.resetView().applyFilter(allVoidedAuditPrograms, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_Audit__c> auditsToRecall = new List<SQX_Audit__c>();

            if(allVoidedAuditPrograms.evaluationResult.size() > 0){

                //select all audits for the voided programs that are in state before in process and
                //anything that has in process audit will be prevented from reaching this state. 
                //change their status to void.
                Map<Id, SObject> allAuditPrograms = new Map<Id, SObject>(allVoidedAuditPrograms.evaluationResult);
                List<SQX_Audit__c> allAudits = [SELECT Id, Status__c,Stage__c, SQX_Audit_Program__c FROM SQX_Audit__c 
                            WHERE ( Stage__c = :SQX_Audit.STAGE_PLAN
                                OR Stage__c = :SQX_Audit.STAGE_SCHEDULED
                                OR Stage__c = :SQX_Audit.STAGE_CONFIRMED
                                OR Stage__c = :SQX_Audit.STAGE_PLAN_APPROVAL ) AND SQX_Audit_Program__c IN : allAuditPrograms.keySet() ];

                for(SQX_Audit__c audit : allAudits){
                    if(audit.Stage__c == SQX_Audit.STAGE_PLAN_APPROVAL){
                        auditsToRecall.add(audit);
                    }
                    audit.Status__c = SQX_Audit.STATUS_VOID;
                    audit.Stage__c = SQX_Audit.STAGE_CLOSED;
                }

                //recall all the audits that are in approval if any are in approval process.
                if(auditsToRecall.size() > 0){
                    SQX_Approval_Util.approveRejectRecallRecords(null, null, auditsToRecall);
                }

                /*
                * Without sharing has been used because Audit Program and Audit could be created by different users,
                * because of which the user attempting to void the audit program may not have write access on the audit.
                * Hence, giving an insufficient permission error.
                */
                new SQX_DB().withoutSharing().op_update(allAudits,  new List<Schema.SObjectField>{SQX_Audit__c.fields.Status__c,
                                                                                            SQX_Audit__c.fields.Stage__c});
            }

            return this;
        }
    


        /**
        * From SQX-1136
        * "If there are no planned audits remaining to be performed, an Audit Program may be 'Closed'"
        *
        * @author Pradhanta Bhandari
        */
        public Bulkified ensureOnlyAuditProgramsWithCompleteAuditCanBeClosed(){
            Rule allClosedAuditPrograms = new Rule();
            allClosedAuditPrograms.addRule(Schema.SQX_Audit_Program__c.Status__c, RuleOperator.Equals, STATUS_CLOSED);

            this.resetView().applyFilter(allClosedAuditPrograms, RuleCheckMethod.OnCreateAndEveryEdit);

            if(allClosedAuditPrograms.evaluationResult.size() > 0){
                Map<Id, SObject> allAuditPrograms = (new Map<Id, SObject>(allClosedAuditPrograms.evaluationResult));
            

                //select all audits that are in progress state for the program, presence of in progress audits
                //means that the audit program can't be voided.
                List<SQX_Audit__c> allAudits = [SELECT Id, Status__c, SQX_Audit_Program__c FROM SQX_Audit__c 
                            WHERE (Status__c != :SQX_Audit.STATUS_CLOSE AND Status__c != :SQX_Audit.STATUS_VOID) AND SQX_Audit_Program__c IN : allAuditPrograms.keySet() ];


                for(SQX_Audit__c audit : allAudits){
                    SQX_Audit_Program__c programWithInProcessAudit = (SQX_Audit_Program__c)allAuditPrograms.get(audit.SQX_Audit_Program__c);
                    if(programWithInProcessAudit != null){
                        programWithInProcessAudit.addError(Label.SQX_ERR_MSG_AUDIT_PRGM_CANT_BE_CLOSED);
                        allAuditPrograms.remove(audit.SQX_Audit_Program__c); //we don't want to add the same error multiple times if more than one inprocess entity is present
                    }
                }
            }


            return this;    
        }

    }
}