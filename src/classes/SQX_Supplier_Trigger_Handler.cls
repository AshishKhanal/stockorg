/**
*   Wrapper class that extends the bulkified base for Supplier Management module
*   Class provides functionalities specific for Supplier Management module in addition
*   to the functionalities provided by SQX_BulkifiedBase class
*
*   BEFORE TRIGGER :
*       UPDATE :
*           a. [setStatusAndStageBasedOnActivityCode] - sets the new status and stage of the record based on the activity code
*           b. [evaluateSaveActivity] - sets the activity code to be 'Save' if the record is not in draft and the activity code is blank
*           c. {assignOwnershipToCurrentUser] - Sets record owner to current user when record is initiated
*           d. [evaluateWorkflowStatus] - Sets Record's status/stage based on workflow status
*           e. [evaluateRecordInitiationOrRestart] - Sets workflow status/ status/ stage of the record on record initiation or restart
*           f. [setRecordSubmittedFields] - sets the submitted date and submitted by when the record is submitted
*           g. [setRecordInitiationDate] - sets the initiation date when the record is initiated
*           h. [setActivityComment] - sets value of closure comment to activity comment field when record is closed
*           i. [lockRecords] - lock record when status is void or closed
*   AFTER TRIGGER :
*       UPDATE :
*           a. [openChildSteps] - open steps on initiation or restart
*           b. [recordActivity] - inserts record activities based on the type of activity(activity code) being performed on the records
*           c. [autoCloseOnRecordOpen] - auto-complete if workflow status is complete on initiation or restart
*           d. [clearActivityCode] - clear out activity code of the record
*           e. [rewindOpenStepsOnClosure] - move Open Steps to Draft on closure
*
*/
public with sharing virtual class SQX_Supplier_Trigger_Handler extends SQX_Trigger_Handler {


    /* object's activity code field */
    private SObjectField activityCodeField;


    // Unique name to be recorded for Save action 
    final String RECORD_SAVE_ACTIVITY = 'recordActivity_Save';


    public SQX_Supplier_Trigger_Handler() {}


    /**
    * Default constructor for this class, that accepts old and new map from the trigger
    * @param newValues equivalent to trigger.New in salesforce
    * @param oldMap equivalent to trigger.OldMap in salesforce
    */
    public SQX_Supplier_Trigger_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
        super(newValues, oldMap);

        activityCodeField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_CODE);
    }


    /**
     * Method to execute all actions before any trigger actions of inheriting classes are run
     */
    protected override void commonActionsBefore() {

        if(Trigger.isDelete) {
            preventRecordDeletion();
        }

        if(Trigger.isUpdate) {
            preventInitiationWithInvalidSteps();
            setRecordInitiationDate();
            setStatusAndStageBasedOnActivityCode();
            evaluateSaveActivity();
            assignOwnershipToCurrentUser();
            evaluateWorkflow();
            evaluateWorkflowStatus();
            evaluateRecordInitiationOrRestart();
            setRecordSubmittedFields();
            setActivityComment();
            lockRecords();
        }

    }

    /**
     * Method to execute all actions after all the before trigger actions of inheriting extending classes are run
     */
    protected override void commonActionsAfter() {

        if(Trigger.isUpdate) {
            openChildSteps();
            recordActivity();
            autoCloseOnRecordOpen();
            clearActivityCode();
            rewindOpenStepsOnClosure();
        }
    }



    /**
    * Method to prevent deletion of Supplier Mgmt records
    */
    public void preventRecordDeletion() {
        this.resetView();

        for(SObject record: (List<SObject>) this.view) {
            record.addError(Label.SQX_ERR_MSG_CANNOT_DELETE_RECORD);
        }
    }


    /**
    * Method to prevent initiating record when any of steps is invalid
    */
    private void preventInitiationWithInvalidSteps(){

        Rule openRecordsRule = new Rule();
        openRecordsRule.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE);
        openRecordsRule.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE);
        openRecordsRule.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART);
        openRecordsRule.operator = RuleCombinationOperator.OpOr;

        this.resetView()
            .applyFilter(openRecordsRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        
        if(this.view.size() > 0){
            Map<Id, SObject> orgRecordMap = new Map<Id, SObject>(this.view);

            SObjectType childObject = SQX_Utilities.getChildObjectFromRelationshipName(this.objType, SQX_Steps_Trigger_Handler.childRelationshipApiName);

            String query = 'SELECT ' +
                            '(' + SQX_Steps_Trigger_Handler.getInvalidStepsQuery(childObject) + ')' +
                            ' FROM ' + this.getObjName() +
                            ' WHERE Id IN: view';

            for(SObject record : Database.query(query)) {
                String allErrors = '';
                for(SObject step : record.getSObjects(SQX_Steps_Trigger_Handler.childRelationshipApiName)) {
                    allErrors += SQX_Steps_Trigger_Handler.getInvalidStepErrorMsgFor(step);
                }
                if(String.isNotEmpty(allErrors)) {
                    orgRecordMap.get(record.Id).addError(allErrors);
                }
            }
        }
    }

    /**
     *  Method sets the new status and stage of the record based on the activity code
     */
    private void setStatusAndStageBasedOnActivityCode() {

        Rule recordsWithActivityCodeRule = new Rule();
        recordsWithActivityCodeRule.addRule(activityCodeField, RuleOperator.NotEquals, null);

        this.resetView()
            .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        Map<String, String> statusMap = SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STATUS_MAP;
        Map<String, String> stageMap = SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STAGE_MAP;

        for(SObject record : this.view) {
            String activityCode = (String) record.get(activityCodeField);
            if(statusMap.containsKey(activityCode)) {
                record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS, statusMap.get(activityCode));
            }
            if(stageMap.containsKey(activityCode)) {
                record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STAGE, stageMap.get(activityCode));
            }
        }

    }

    /**
     *  Method evaluates the record and if the record is not in draft and the activity code is blank,
     *  it sets the activity code to be 'Save'
     */
    private void evaluateSaveActivity() {

        SObjectField statusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS);

        Rule recordsBeingSaved = new Rule();
        recordsBeingSaved.addRule(statusField, RuleOperator.NotEquals, SQX_Supplier_Common_Values.STATUS_DRAFT);
        recordsBeingSaved.addRule(activityCodeField, RuleOperator.Equals, null);

        this.resetView()
            .removeProcessedRecordsFor(getObjName(), RECORD_SAVE_ACTIVITY)
            .applyFilter(recordsBeingSaved, RuleCheckMethod.OnCreateAndEveryEdit);

        for(SObject record : this.view) {
            this.addToProcessedRecordsFor(getObjName(), RECORD_SAVE_ACTIVITY, this.view);
            record.put(activityCodeField, SQX_Supplier_Common_Values.ACTIVITY_CODE_SAVE);
        }

    }


    /**
    *   Method to assign ownership to user on record initiation
    */
    public void assignOwnershipToCurrentUser(){

        Rule openRecords = new Rule();
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE);
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE);
        openRecords.operator = RuleCombinationOperator.OpOr;

        this.resetView()
            .applyFilter(openRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        for(SObject record : this.view) {
            record.put('OwnerId', UserInfo.getUserId());
        }
    }

    /**
    *   Method to evaluate the current workflow status of the record
    *   Based on its value, the method makes the necessary updates to the record's state(Status/Stage)
    */
    private void evaluateWorkflowStatus() {

        final String ACTION_NAME = 'evaluateWorkflowStatus';

        SObjectField workflowStatusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS);

        Rule workflowStatusChanged = new Rule();
        workflowStatusChanged.addRule(workflowStatusField, RuleOperator.HasChanged);
        workflowStatusChanged.addRule(workflowStatusField, RuleOperator.NotEquals, NULL);

        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(workflowStatusChanged, RuleCheckMethod.OnCreateAndEveryEdit);

        if(this.view.size() > 0) {
            this.addToProcessedRecordsFor(getObjName(), ACTION_NAME, this.view);

            for(SObject record : this.view) {

                String wfStatus = (String) record.get(workflowStatusField);

                if(wfStatus.equals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED)) {
                    completeRecord(record);
                } else if(wfStatus.equals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_IN_PROGRESS)) {
                    record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STAGE, SQX_Supplier_Common_Values.STAGE_IN_PROGRESS);
                } else if(wfStatus.equals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_ON_HOLD)) {
                    record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STAGE, SQX_Supplier_Common_Values.STAGE_ON_HOLD);
                }
            }
        }

    }

    /**
    * Method to evaluate when steps evaluate workflow is true
    */
    private void evaluateWorkflow() {
        final String ACTION_NAME = 'evaluateWorkflow';
        SObjectField evaluateWorkflowField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_EVALUATE_WORKFLOW);
        Boolean trueVal = true;
        Rule openRecords = new Rule();
        openRecords.addRule(evaluateWorkflowField, RuleOperator.Equals, (Object) trueVal);
        
        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(openRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        
        if(this.view.size() > 0){
            evalSteps(this.view);
        }
    }
    
    /**
    * Method to evaluate workflow status and step number
    */
    private void evalSteps(List<SObject> recordList){

        Map<Id, SObject> records = new Map<Id, SObject>(recordList);

        SQX_Step_Evaluation_Engine evaluationEngine = new SQX_Step_Evaluation_Engine();

        Map<Id, SObject> evaluatedRecords = evaluationEngine.evaluateSteps(new List<Id>(records.keySet()), false);  //only evaluate, do not update

        SObject evaluatedRecord;
        String newWorkflowStatus;

        List<SObject> intermediateRecords = new List<SObject>();

        for(SObject record : records.values()) {

            evaluatedRecord = evaluatedRecords.get(record.Id);

            newWorkflowStatus = (String) evaluatedRecord.get(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS);

            // update workflow status and step number
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS, newWorkflowStatus);
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP, evaluatedRecord.get(SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP));

            // update result field
            // if the parent record is being 'RESTARTED' (i.e Status = Open), the result field has to be cleared out [SQX-6361]
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_RESULT, null);
        }
    }

    /**
     *  Method to handle record initiation/restart
     *
     *  Updates the workflow status and the step number
     *  In addition, if the workflow is discovered to be complete which is the case when
     *  No workflow step exists when initiating OR
     *  Workflow steps are all complete when restarting
     *  , the method completes the record
     */
    private void evaluateRecordInitiationOrRestart() {

        final String ACTION_NAME = 'evaluateRecordInitiationOrRestart';

        Rule openRecords = new Rule();
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE);
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE);
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_RESTART);
        openRecords.operator = RuleCombinationOperator.OpOr;

        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(openRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        if(this.view.size() > 0){

            this.addToProcessedRecordsFor(getObjName(), ACTION_NAME, this.view);
            List<SObject> recordList = new List<SObject>();
            SObjectField evaluateWorkflowField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_EVALUATE_WORKFLOW);
            for(SObject record : this.view){
                Boolean evaluateStep = (Boolean) record.get(evaluateWorkflowField);
                if(evaluateStep){
                    recordList.add(record);
                }
            }

            if(recordList.size() > 0){
                evalSteps(recordList);
            }
        }
    }

    /**
     * method to set the submitted date and submitted by when the record is submitted
     */
    public void setRecordSubmittedFields(){
        final String ACTION_NAME = 'setRecordSubmittedFields';
        Rule openRecords = new Rule();
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_SUBMIT);
        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(openRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        this.addToProcessedRecordsFor(getObjName(), ACTION_NAME, this.view);
        
        for(SObject record : this.view){
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_SUBMITTED_BY, UserInfo.getUserId());
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_SUBMITTED_DATE, System.Datetime.now());
        }
    }

    /**
     *  Method to lock record when status is void or closed
     */
    private void lockRecords() {

        final String ACTION_NAME = 'lockRecords';
        SObjectField statusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS);

        Rule closedOrVoidRecords = new Rule();
        closedOrVoidRecords.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_CLOSED);
        closedOrVoidRecords.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_VOID);
        closedOrVoidRecords.operator = RuleCombinationOperator.OpOr;


        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(closedOrVoidRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        for(SObject record : this.view) {
            this.addToProcessedRecordsFor(getObjName(), RECORD_SAVE_ACTIVITY, this.view);
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_IS_LOCKED, true);
        }

    }

    /**
     * method to transfer value of closure_comment to activity_comment
     */
    public void setActivityComment(){
        final String ACTION_NAME = 'setActivityComment';
        Rule closedRecord = new Rule();
        closedRecord.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_CLOSE);
        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(closedRecord, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        this.addToProcessedRecordsFor(getObjName(), ACTION_NAME, this.view);

        for(SObject record : this.view){
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_COMMENT, (String)record.get(SQX_Supplier_Common_Values.PARENT_FIELD_CLOSURE_COMMENT));

        }
    }

    /**
     * Method to set the initiaion date when the record is initiated
     */
    private void setRecordInitiationDate(){
        Rule openRecords = new Rule();
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_INITIATE);
        openRecords.addRule(activityCodeField, RuleOperator.Equals, SQX_Supplier_Common_Values.ACTIVITY_CODE_TAKE_OWNERSHIP_AND_INITIATE);
        openRecords.operator = RuleCombinationOperator.OpOr;

        this.resetView().applyFilter(openRecords, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        for(SObject record : this.view) {
            record.put(SQX_Supplier_Common_Values.PARENT_FIELD_INITIATION_DATE, System.Datetime.now());
        }
    }

    /////////////////// AFTER TRIGGER ACTIONS BEGIN /////////////////////////////////


    /**
    *   Method to open steps on initiation or restart
    *   Only those steps are opened that match the parent's current step
    */
    private void openChildSteps(){

        final String ACTION_NAME = 'openChildSteps';

        SObjectField statusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS);
        SObjectField evaluateWorkflowField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_EVALUATE_WORKFLOW);
        Boolean trueVal = true;

        Rule openRecords = new Rule();
        openRecords.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_OPEN);
        openRecords.addRule(evaluateWorkflowField, RuleOperator.Equals, (Object) trueVal);

        this.resetView()
            .removeProcessedRecordsFor(getObjName(), ACTION_NAME)
            .applyFilter(openRecords, RuleCheckMethod.OnCreateAndEveryEdit);

        if(this.view.size() > 0){

            this.addToProcessedRecordsFor(getObjName(), ACTION_NAME, this.view);

            String stepsQuery = SQX_Steps_Trigger_Handler.getApplicableDraftStepsQuery();

            String query = 'SELECT  ' + SQX_Supplier_Common_Values.PARENT_FIELD_STATUS + ',' + SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP + ', ' +
                            '(' + stepsQuery + ')' +
                            ' FROM ' + getObjName() +
                            ' WHERE Id IN: view';

            try {
                SQX_Steps_Trigger_Handler.onParentOpened(Database.query(query));
            } catch(SQX_ApplicationGenericException ex) {
                for(SObject obj : this.view) {
                    obj.addError(ex.getMessage());
                }
            }

            List<SObject> stepsToUpdate = new List<SObject>();

            for(SObject record : Database.query(query)) {
                Integer stepToOpen = Integer.valueOf(record.get(SQX_Supplier_Common_Values.PARENT_FIELD_CURRENT_STEP));

                for(SObject step : record.getSObjects(SQX_Steps_Trigger_Handler.childRelationshipApiName)) {
                    Integer stepNum = Integer.valueOf(step.get(SQX_Steps_Trigger_Handler.FIELD_STEP));

                    // step number of Step to be opened can be lesser than the parent's current step in case of a Restart
                    // but it can at no point be greater than the parent's current step

                    if(stepNum > stepToOpen ) {
                        break;
                    }

                    if(stepNum == stepToOpen) {
                        SQX_Steps_Trigger_Handler.openStep(step);
                        stepsToUpdate.add(step);
                    }

                }
            }


            /*
            * WITHOUT SHARING used
            * --------------------
            * when one of the assignee complete the task of the current step, 
            * it tries to update the step of the nsi but the user has no edit access to record
            */
            new SQX_DB().withoutSharing().op_update(stepsToUpdate, new List<Schema.SObjectField> {});

        }
    }


    /**
     * Method to get a list of records that falls under a particular activityname param.
     */
    private Map<String, List<SObject>> getRecordForSameActivityMap(List<SObject> records, String activityName, String actionName) {
        Map<String, List<SObject>> activityRecordMap = new Map<String, List<SObject>>();
        for(SObject obj : records) {
                String fieldValue = (String) obj.get(activityName);
                String keyToUse = actionName + '_' + fieldValue;
                if(!activityRecordMap.containsKey(keyToUse)) {
                    activityRecordMap.put(fieldValue, new List<SObject>());
                }
                activityRecordMap.get(fieldValue).add(obj);
            }
        return activityRecordMap;
    }
    
    /**
     * Method to insert record activities based on the type of activity(activity code) being performed on the records
     */
    
    private void recordActivity() {
        final String ACTION_NAME_PREFIX = 'recordActivity';

        Rule recordsWithActivityCodeRule = new Rule();
        recordsWithActivityCodeRule.addRule(activityCodeField, RuleOperator.NotEquals, null);
		
        this.resetView()
            .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
        
        if(this.view.size() > 0) {
            Map<String, List<SObject>> activityRecordMap = getRecordForSameActivityMap(this.view, SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_CODE,ACTION_NAME_PREFIX);
            List<SObject> recordsToInsertActivityFor = new List<SObject>();

            for(String activityCode : activityRecordMap.keySet()) {
                this.removeProcessedRecordsFor(this.getObjName(), activityCode);
                
                List<SObject> firstTimeEntries = new List<SObject>();

                Map<Id, SObject> filteredView = new Map<Id, SObject>(this.view);
                for(SObject record : activityRecordMap.get(activityCode)) {
                    firstTimeEntries = new List<SObject>();
                    if(filteredView.containsKey(record.Id)) {
                        firstTimeEntries.add(record);
                    }
                    this.addToProcessedRecordsFor(this.getObjName(), activityCode, firstTimeEntries);
                    recordsToInsertActivityFor.addAll(firstTimeEntries);
                }
            }

            SQX_Supplier_Record_Activity_Helper helper = new SQX_Supplier_Record_Activity_Helper(this.objType);
            helper.insertRecordActivity(recordsToInsertActivityFor);
        }
    }

    /**
     * Method to auto-complete Open record if the Workflow status is Complete
     */
    private void autoCloseOnRecordOpen() {

        final String ACTION_NAME = 'autoCloseOnRecordOpen';
        Boolean trueVal = true;

        SObjectField statusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS),
                     stageField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STAGE),
                     resultField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_RESULT),
                     workflowStatusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_WORKFLOW_STATUS),
                     evaluateWorkflowField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_EVALUATE_WORKFLOW);

        Rule openRecordsToAutoComplete = new Rule();
        openRecordsToAutoComplete.addRule(workflowStatusField, RuleOperator.Equals, SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED);
        openRecordsToAutoComplete.addRule(statusField, RuleOperator.HasChanged);
        openRecordsToAutoComplete.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_OPEN);
        openRecordsToAutoComplete.addRule(evaluateWorkflowField, RuleOperator.Equals, (Object) trueVal);

        this.resetView()
            .removeProcessedRecordsFor(this.getObjName(), ACTION_NAME)
            .applyFilter(openRecordsToAutoComplete, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        List<SObject> recordsToComplete = new List<SObject>();

        for(SObject record : this.view) {
            SObject recordToComplete = this.objType.newSObject();
            recordToComplete.put('Id', record.Id);
            completeRecord(recordToComplete);
            recordsToComplete.add(recordToComplete);
        }

        if(recordsToComplete.size() > 0) {
            /**
            * WITHOUT SHARING USED:
            * ---------------------
            * Closure Reviewer may not have enough permission to update stage field as stage needs to be updated from
            * from Closure Reviewer to verification, if complaint is rejected by reviewer
            */
            new SQX_DB().withoutSharing().op_update(recordsToComplete, new List<SObjectField> { statusField, stageField, resultField });

            // now insert record activity
            SQX_Supplier_Record_Activity_Helper helper = new SQX_Supplier_Record_Activity_Helper(this.objType);
            helper.insertRecordActivity(recordsToComplete);
        }

    }

    /**
     * Method to clear out activity code on every updates to the record
     */
    private void clearActivityCode() {

        Rule recordsWithActivityCodeRule = new Rule();
        recordsWithActivityCodeRule.addRule(activityCodeField, RuleOperator.NotEquals, null);

        this.resetView()
            .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEveryEdit);

        List<SObject> recordsToBeUpdated = new List<SObject>();

        // adding to processed record
        // such that when the before trigger executes after this action,
        // this action isn't treated as a SAVE activity
        if(this.view.size() > 0){
            this.addToProcessedRecordsFor(getObjName(), RECORD_SAVE_ACTIVITY, this.view);
        }

        for(SObject record : this.view) {
            SObject recordToBeUpdated = this.objType.newSObject();
            recordToBeUpdated.put('Id', record.Id);
            recordToBeUpdated.put(activityCodeField, null);
            recordToBeUpdated.put(SQX_Supplier_Common_Values.PARENT_FIELD_ACTIVITY_COMMENT, null);

            recordsToBeUpdated.add(recordToBeUpdated);
        }

        /*
         * WITHOUT SHARING USED
         * =====================
         * Privilege escalated as a user completing a task which in turn completes the parent record may not have access on the parent record
         */
        new SQX_DB().withoutSharing().op_update(recordsToBeUpdated, new List<SObjectField> { activityCodeField });
    }

    /**
     *  Method moves Open steps to Draft on record closure (by void or close action)
     */
    private void rewindOpenStepsOnClosure() {

        SObjectField statusField = this.getSObjectField(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS);

        Rule closedRecordRule = new Rule();
        closedRecordRule.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_CLOSED);
        closedRecordRule.addRule(statusField, RuleOperator.Equals, SQX_Supplier_Common_Values.STATUS_VOID);
        closedRecordRule.operator = RuleCombinationOperator.OpOr;

        this.resetView()
            .applyFilter(closedRecordRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        if(this.view.size() > 0) {

            List<SObject> stepsToUpdate = new List<SObject>();

            String query = 'SELECT ' +
                            '(' + SQX_Steps_Trigger_Handler.getOpenStepsQuery() + ')' +
                            ' FROM ' + this.getObjName() + 
                            ' WHERE Id IN: view';

            for(SObject record : Database.query(query)) {
                for(SObject step : record.getSObjects(SQX_Steps_Trigger_Handler.childRelationshipApiName)) {
                    step.put(SQX_Steps_Trigger_Handler.FIELD_STATUS, SQX_Steps_Trigger_Handler.STATUS_DRAFT);
                    step.put(SQX_Steps_Trigger_Handler.FIELD_AUDIT_NUMBER, null);
                    stepsToUpdate.add(step);
                }
            }

            if(stepsToUpdate.size() > 0) {
                statusField = stepsToUpdate.get(0).getSObjectType().getDescribe().fields.getMap().get(SQX_Steps_Trigger_Handler.FIELD_STATUS);
                new SQX_DB().op_update(stepsToUpdate, new List<SObjectField> { statusField });
            }
        }
    }

    ////////////////////// TRIGGER METHODS END  /////////////////////////////////


    ////////////////////// HELPER METHODS START  /////////////////////////////////


    /**
    *   Method to complete the given record
    *   Complete implies setting record status to Complete, stage to Verification
    *   result to Approved and Activity Code to complete
    */
    private void completeRecord(SObject record) {

        Map<String, String> statusMap = SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STATUS_MAP;
        Map<String, String> stageMap = SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STAGE_MAP;

        record.put(activityCodeField, SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE);
        record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STATUS, SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STATUS_MAP.get(SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE));
        record.put(SQX_Supplier_Common_Values.PARENT_FIELD_STAGE, SQX_Supplier_Common_Values.ACTIVITY_CODE_AND_STAGE_MAP.get(SQX_Supplier_Common_Values.ACTIVITY_CODE_COMPLETE));
        record.put(SQX_Supplier_Common_Values.PARENT_FIELD_RESULT, SQX_Supplier_Common_Values.RESULT_APPROVED);

    }

}