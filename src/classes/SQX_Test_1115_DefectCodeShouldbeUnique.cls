/**
* Unit tests for SQX_DefectCodeShouldbeUnique
*
* @author Shailesh Maharjan
* @date 2015/06/03
* 
*/
@IsTest
public class SQX_Test_1115_DefectCodeShouldbeUnique{

    static boolean runAllTests = true,
                    run_defectCodeAdded_DuplicateNotAllowed = true;

    /**
    *   Setup: Create A Defect Code
    *   Action: To check the DefectCodes Name is Unique
    *   Expected: Second insert should be failed with its error msg
    *
    * @author Shailesh Maharjan
    * @date 2015/06/03
    * 
    */

     public static testmethod void defectCodeAdded_DuplicateNotAllowed(){

        if(!runAllTests && !run_defectCodeAdded_DuplicateNotAllowed){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
     
        System.runas(adminUser){
             SQX_Defect_Code__c defectCode1 = new SQX_Defect_Code__c(Name='Defecttest',Defect_Category__c='DefectCat');
             Database.SaveResult result1 = Database.Insert(defectCode1 ,false);

            System.assert(result1.isSuccess() == true,
                 'Expected Defect code save to be success ');
                 
                 SQX_Defect_Code__c defectCode2 = new SQX_Defect_Code__c(Name='Defecttest',Defect_Category__c='DefectCat');
             Database.SaveResult result2 = Database.Insert(defectCode2 ,false);

            System.assert(result2.isSuccess() == false,
                 'Expected Defect code save to be Unsuccess ');
        }
         
     }

}