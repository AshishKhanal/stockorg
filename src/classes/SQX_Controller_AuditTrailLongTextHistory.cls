/**
* controller class to get long text history from audit trail record
*/
public with sharing class SQX_Controller_AuditTrailLongTextHistory {
    
    /**
    * constants
    */
    public static final String  URL_PARAM_HISTORY_ID = 'historyid', // url param for audit trail id
                                URL_PARAM_OBJECT_TYPE = 'objtype', // url param for audit trail sobject type api name
                                CUSTOM_PERMISSION_VIEW_AUDIT_TRAIL_SECURITY = 'SQXViewAuditTrailSecurity';
    
    /**
    * holds api name of history object type related to the parent record
    */
    @TestVisible
    Boolean userHasViewAuditTrailPermission = false;
    
    /**
    * holds api name of history object type related to the parent record
    */
    @TestVisible
    String historyObjectTypeName;
    
    /**
    * holds api name of the parent record object type
    */
    String parentObjectTypeName;
    
    /**
    * holds label of the parent record object type
    */
    public String parentObjectTypeLabel { get; set; }
    
    /**
    * holds audit trail history record
    */
    public SObject historyObj { get; set; }
    
    /**
    * holds related long text history record of the provided audit trail record
    */
    public SQX_Long_Text_History__c longTextHistoryObj { get; set; }
    
    /**
    * default constructor
    */
    public SQX_Controller_AuditTrailLongTextHistory() {
        userHasViewAuditTrailPermission = SQX_Utilities.checkIfUserHasPermission(CUSTOM_PERMISSION_VIEW_AUDIT_TRAIL_SECURITY);
        initHistoryObjectFromUrlParams();
        initLongTextHistoryRecords();
    }
    
    /**
    * method to read provided audit trail record id and parent record id from url and query audit trail record
    */
    public void initHistoryObjectFromUrlParams() {
        historyObj = null;
        historyObjectTypeName = '';
        
        if (userHasViewAuditTrailPermission) {
            Id histId;
            SObjectType histObjType;
            
            // read search params from url
            String urlPrmHistObjTypeName = ApexPages.currentPage().getParameters().get(URL_PARAM_OBJECT_TYPE);
            if (String.isNotBlank(urlPrmHistObjTypeName)) {
                // get valid history object type
                histObjType = Schema.getGlobalDescribe().get(urlPrmHistObjTypeName);
            }
            String urlPrmHistId = ApexPages.currentPage().getParameters().get(URL_PARAM_HISTORY_ID);
            if (String.isNotBlank(urlPrmHistId)) {
                try {
                    histId = (Id)urlPrmHistId;
                } catch (Exception ex) {
                    // ignore invalid values
                }
            }
            
            if (histId != null && histObjType != null) {
                Schema.DescribeSObjectResult objDesc = histObjType.getDescribe();
                historyObjectTypeName = objDesc.getName();
                
                List<SObject> histObjs = Database.query('SELECT ParentId, Field, CreatedById, CreatedDate FROM ' + historyObjectTypeName + ' WHERE Id = :histId');
                
                if (histObjs.size() == 1) {
                    // set audit trail record
                    historyObj = histObjs[0];
                }
            }
            
            if (historyObj == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_AUDIT_TRAIL_RECORD_NOT_AVAILABLE));
            }
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_USER_DOES_NOT_HAVE_VIEW_AUDIT_TRAIL_PERMISSION));
        }
    }
    
    /**
    * method to read long text history record for the provided audit trail record
    */
    public void initLongTextHistoryRecords() {
        parentObjectTypeName = '';
        parentObjectTypeLabel = '';
        longTextHistoryObj = null;
        
        if (historyObj != null) {
            // set parent record object info
            Id parentId = (Id)historyObj.get('ParentId');
            Schema.DescribeSObjectResult parentObjTypeDesc = parentId.getSObjectType().getDescribe();
            parentObjectTypeName = parentObjTypeDesc.getName();
            parentObjectTypeLabel = parentObjTypeDesc.getLabel();
            
            Id histId = historyObj.Id;
            String parentRecordId = (String)historyObj.get('ParentId');
            String fieldName = (String)historyObj.get('Field');
            Id changedById = (Id)historyObj.get('CreatedById');
            DateTime changedOnTo = (DateTime)historyObj.get('CreatedDate');
            DateTime changedOnFrom = null;
            
            // get past audit trail history object on the same field to specify data time range to get long text history
            String pastHistQuery = 'SELECT CreatedDate FROM ' + historyObjectTypeName
                                    + ' WHERE ParentId = :parentRecordId AND Id < :histId '
                                    + ' AND CreatedById = :changedById AND CreatedDate < :changedOnTo AND Field = :fieldName'
                                    + ' ORDER BY Id DESC LIMIT 1';
            List<SObject> pastHistObjs = Database.query(pastHistQuery);
            if (pastHistObjs.size() > 0) {
                changedOnFrom = (DateTime)pastHistObjs[0].get('CreatedDate');
            }
            
            String searchRecordIndex = SQX_Long_Text_History.generateRecordIndex(parentRecordId, fieldName, changedById);
            
            // get long text history
            String soqlQuery = 'SELECT Name, Field_Name__c, SQX_Changed_By__c, Changed_On__c, Old_Value__c'
                                + ' FROM SQX_Long_Text_History__c'
                                + ' WHERE Record_Index__c = :searchRecordIndex'
                                + ' AND Changed_On__c <= :changedOnTo';
            if (changedOnFrom != null) {
                soqlQuery += ' AND Changed_On__c > :changedOnFrom';
            }
            soqlQuery += ' ORDER BY Changed_On__c DESC LIMIT 1';
            system.debug('soqlQuery= ' + soqlQuery);
            
            List<SObject> historyRows = Database.query(soqlQuery);
            
            if (historyRows.size() > 0) {
                longTextHistoryObj = (SQX_Long_Text_History__c)historyRows[0];
            }
            else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_AUDIT_TRAIL_LONG_TEXT_HISTORY_NOT_AVAILABLE));
            }
        }
    }
}