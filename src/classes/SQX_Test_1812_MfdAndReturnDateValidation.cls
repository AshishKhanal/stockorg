/**
 * test class to ensure that the date fields in Manufacturing Date and Future Date in Complaint cannot be future date
 * @author: Sagar Shrestha
 * @date: 2015-12-31
 * @story: [SQX-1812] 
 */
@isTest
public class SQX_Test_1812_MfdAndReturnDateValidation{ 
    static boolean runAllTests = true,
                   givenComplaint_ManufacturingDateIsValidated = false,
                   givenComplaint_ReturnReceivedDateIsValidated= false;

    /**
     * Scenario 1
     * Action: Manufacturing Date is Set to future date
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Manufacturing Date is Set to past date
     * Expected: Complaint Save should be successful
     *
     * @author: Sagar Shrestha
     * @date: 2015-12-31
     */
    public testmethod static void givenComplaint_ManufacturingDateIsValidated(){
        if(!runAllTests && !givenComplaint_ManufacturingDateIsValidated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
            //Arrange : Save a complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

            //Act1 : Set Manufacturing Date as future date
            complaint.complaint.Manufacturing_Date__c = Date.Today().addDays(1);
            Database.SaveResult futureManufacturingDateUpdate = Database.update(complaint.complaint, false);

            //Assert 1 : Expected save to be unsuccessful for future Manufacturing Date
          System.assertEquals(false, futureManufacturingDateUpdate.isSuccess(), 'Expected save to be unsuccessful for future Manufacturing Date');

          //Act2 : Set Manufacturing Date as past date
          complaint.complaint.Manufacturing_Date__c = Date.Today().addDays(-1);
          Database.SaveResult pastManufacturingDateUpdate = Database.update(complaint.complaint, false);

          //Assert 2 : Expected save to be successful for past Manufacturing Date
          System.assertEquals(true, pastManufacturingDateUpdate.isSuccess(), 'Expected save to be successful for past Manufacturing Date');

        }
    }

    /**
     * Scenario 1
     * Action: Return Received Date is Set to future date
     * Expected: Complaint Save should not be successful
     *
     * Scenario 2
     * Action: Return Received Date is Set to past date
     * Expected: Complaint Save should be successful
     *
     * @author: Sagar Shrestha
     * @date: 2015-12-31
     */
    public testmethod static void givenComplaint_ReturnReceivedDateIsValidated(){
      if(!runAllTests && !givenComplaint_ReturnReceivedDateIsValidated){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
               
        System.runas(standardUser){
          //Arrange : Save a complaint
          SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();

          //Act1 : Set Return Received Date as future date
          complaint.complaint.Return_Received_Date__c = Date.Today().addDays(1);
          Database.SaveResult futureReturnReceivedDateUpdate = Database.update(complaint.complaint, false);

          //Assert 1 : Expected save to be unsuccessful for future Return Received Date
          System.assertEquals(false, futureReturnReceivedDateUpdate.isSuccess(), 'Expected save to be unsuccessful for future Return Received Date');

          //Act2 : Set Return Received Date as past date
          complaint.complaint.Return_Received_Date__c = Date.Today().addDays(-1);
          Database.SaveResult pastReturnReceivedDateUpdate = Database.update(complaint.complaint, false);

          //Assert 2 : Expected save to be successful for past Return Received Date
          System.assertEquals(true, pastReturnReceivedDateUpdate.isSuccess(), 'Expected save to be successful for past Return Received Date');

        }
    }

}