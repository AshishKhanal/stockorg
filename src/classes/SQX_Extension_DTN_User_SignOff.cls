/*
* author: Shailesh Maharjan
* date : 2015/07/20
* description:  Used for User SignOff
*/
public with sharing class SQX_Extension_DTN_User_SignOff extends SQX_Extension_DTN_SignOff_Base {
    
    public final static String URL_KEY_FOR_ID = 'dtnId';
    
    public SQX_Extension_DTN_User_SignOff(ApexPages.StandardSetController controller) {
        super(controller, URL_KEY_FOR_ID, false);
    }
    
}