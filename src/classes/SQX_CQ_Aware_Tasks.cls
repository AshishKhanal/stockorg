/**
* This class will contains all CQ Aware tasks
*/
public with sharing class SQX_CQ_Aware_Tasks extends SQX_Trigger_Handler {
    /**
    * This function routes list of sobject to all its respective CQ Aware tasks
    **/
    public static void performCQAwareTasks(List<SObject> objects, Map<Id, SObject> oldValues){
        if(objects.size()>0){
            Schema.SObjectType  listObjectType= objects.getSObjectType();
            //route to appropriate functions according to objecttype
            //Similarly other sobject can be supported by adding else if statements
            if (listObjectType == SQX_Controlled_Document__c.SObjectType){
                Map<Id, SQX_Controlled_Document__c> oldDocs = new Map<Id, SQX_Controlled_Document__c>();
                if (oldValues != null && oldValues.size() > 0) {
                    oldDocs = (Map<Id, SQX_Controlled_Document__c>)oldValues;
                }
                
                processDocRelatedActions(objects, oldDocs);
            }
            else if(listObjectType == SQX_Change_Order__c.SObjectType){
                copyEffectiveDatesToRelatedDocs((List<SQX_Change_Order__c>) objects, (Map<Id, SQX_Change_Order__c>) oldValues);
            }
        }        
    }
    
    /**
    * updates or completes all related implementations
    **/
    public static void processDocRelatedActions(List<SQX_Controlled_Document__c> docs, Map<Id, SQX_Controlled_Document__c> oldValues) {
        if (docs.size() > 0) {
            final String PROCESS_RELEASED_DOCS = 'cqaware-processReleasedDocs',
                         PROCESS_EXPIRED_DOCS = 'cqaware-processExpiredDocs',
                         PROCESS_IN_CHANGE_APPROVAL_DOCS = 'cqaware-processInChangeApprovalDocs',
                         PROCESS_RECALLED_FROM_CHANGE_APPROVAL_DOCS = 'cqaware-processRecalledFromChangeApprovalDocs';
            
            Map<Id, SQX_Controlled_Document__c> releasedDocs = new Map<Id, SQX_Controlled_Document__c>(),
                                                expiredDocs = new Map<Id, SQX_Controlled_Document__c>(),
                                                inChangeApprovalDocs = new Map<Id, SQX_Controlled_Document__c>(),
                                                recalledFromChangeApprovalDocs = new Map<Id, SQX_Controlled_Document__c>();
            
            SQX_BulkifiedBase base = new SQX_BulkifiedBase(docs, oldValues);
            
            for (SQX_Controlled_Document__c doc : (List<SQX_Controlled_Document__c>)base.view) {
                if (doc.Document_Status__c == SQX_Controlled_Document.STATUS_DRAFT){
                    SQX_Controlled_Document__c oldDoc = oldValues.get(doc.Id);
                    
                    if (doc.Approval_Status__c == SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL && !base.getProcessedEntitiesFor(SQX.ControlledDoc, PROCESS_IN_CHANGE_APPROVAL_DOCS).contains(doc.Id)) {
                        inChangeApprovalDocs.put(doc.Id, doc);
                    }
                    else if (String.isBlank(doc.Approval_Status__c) && oldDoc != null && oldDoc.Approval_Status__c == SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL && !base.getProcessedEntitiesFor(SQX.ControlledDoc, PROCESS_RECALLED_FROM_CHANGE_APPROVAL_DOCS).contains(doc.Id)) {
                        recalledFromChangeApprovalDocs.put(doc.Id, doc);
                    }
                }
                else if((doc.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_RELEASE || doc.Document_Status__c == SQX_Controlled_Document.STATUS_CURRENT) && !base.getProcessedEntitiesFor(SQX.ControlledDoc, PROCESS_RELEASED_DOCS).contains(doc.Id)){
                    releasedDocs.put(doc.Id, doc);
                }
                else if((doc.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_EXPIRE ||  doc.Document_Status__c == SQX_Controlled_Document.STATUS_OBSOLETE)&& !base.getProcessedEntitiesFor(SQX.ControlledDoc, PROCESS_EXPIRED_DOCS).contains(doc.Id)){
                    expiredDocs.put(doc.Id, doc);
                }
            }
            
            if (releasedDocs.size() > 0 || expiredDocs.size() > 0 || inChangeApprovalDocs.size() > 0 || recalledFromChangeApprovalDocs.size() > 0) {
                base.addToProcessedRecordsFor(SQX.ControlledDoc, PROCESS_RELEASED_DOCS, releasedDocs.values());
                base.addToProcessedRecordsFor(SQX.ControlledDoc, PROCESS_EXPIRED_DOCS, expiredDocs.values());
                base.addToProcessedRecordsFor(SQX.ControlledDoc, PROCESS_IN_CHANGE_APPROVAL_DOCS, inChangeApprovalDocs.values());
                base.addToProcessedRecordsFor(SQX.ControlledDoc, PROCESS_RECALLED_FROM_CHANGE_APPROVAL_DOCS, recalledFromChangeApprovalDocs.values());
                
                Map<Id, SQX_Controlled_Document__c> docOwnerMap = new Map<Id, SQX_Controlled_Document__c>([ SELECT Id, Owner.Name
                                                                                                            FROM SQX_Controlled_Document__c
                                                                                                            WHERE Id IN :releasedDocs.keySet()
                                                                                                                OR Id IN :expiredDocs.keySet() ]);
                
                // method is used to update the impacted document status to complete when the document status is complete
                updateImpactedDocumentStatus(releasedDocs.keySet());
                
                Id actionRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, SQX_Implementation.RECORD_TYPE_ACTION);
                List<String>    validCQAwareTypes = new List<String> {  SQX_Implementation.AWARE_DOCUMENT_NEW,
                                                                        SQX_Implementation.AWARE_DOCUMENT_REVISE,
                                                                        SQX_Implementation.AWARE_DOCUMENT_OBSOLETE },
                                impStatuses = new List<String>{ SQX_Implementation.STATUS_OPEN,
                                                                SQX_Implementation.STATUS_READY };
                
                List<SQX_Implementation__c> actionsToUpdate = [ SELECT Id, Status__c, SQX_Controlled_Document__c, SQX_Controlled_Document_New__c, CQ_Aware_Type__c,Type__c
                                                                FROM SQX_Implementation__c
                                                                WHERE RecordTypeId = :actionRecordTypeId
                                                                    AND Type__c IN :validCQAwareTypes
                                                                    AND Status__c IN :impStatuses
                                                                    AND ( SQX_Controlled_Document_New__c IN :releasedDocs.keySet()
                                                                        OR SQX_Controlled_Document_New__c IN :inChangeApprovalDocs.keySet()
                                                                        OR SQX_Controlled_Document_New__c IN :recalledFromChangeApprovalDocs.keySet()
                                                                        OR SQX_Controlled_Document__c IN :expiredDocs.keySet() )
                                                                FOR UPDATE];
                
                if (actionsToUpdate.size() > 0) {
                    for (SQX_Implementation__c implementation : actionsToUpdate) {
                        // update implementation fields as per controlled document status
                        if (implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_NEW 
                            || implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_REVISE) {
                            
                            if (releasedDocs.containsKey(implementation.SQX_Controlled_Document_New__c)) {
                                implementation.Completed_By__c = docOwnerMap.get(implementation.SQX_Controlled_Document_New__c).Owner.Name;
                                implementation.Status__c = SQX_Implementation.STATUS_COMPLETE;
                                implementation.Completion_Date__c = Date.Today();
                            }
                            else if (inChangeApprovalDocs.containsKey(implementation.SQX_Controlled_Document_New__c)) {
                                implementation.Status__c = SQX_Implementation.STATUS_READY;
                            }
                            else if (recalledFromChangeApprovalDocs.containsKey(implementation.SQX_Controlled_Document_New__c)) {
                                implementation.Status__c = SQX_Implementation.STATUS_OPEN;
                            }
                        }
                        else if (implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_OBSOLETE) {
                            implementation.Status__c = SQX_Implementation.STATUS_COMPLETE;
                            implementation.Completion_Date__c = Date.Today();
                            implementation.Completed_By__c = docOwnerMap.get(implementation.SQX_Controlled_Document__c).Owner.Name;
                        }
                    }
                    
                    /*
                    * WITHOUT Sharing USED:
                    * ---------
                    * Documents can be created, approved and released by someother user where as change actions could be owned by somebody else.
                    */
                    new SQX_DB().withoutSharing().op_update(actionsToUpdate, new List<Schema.SObjectField>{
                            Schema.SQX_Implementation__c.Status__c,
                            Schema.SQX_Implementation__c.Completed_By__c,
                            Schema.SQX_Implementation__c.Completion_Date__c
                    });
                }
            }
        }
    }

    /**
    * If controlled doc is in pre-release and effective date changes change the effective date of all related controlled docs.
    */
    public static void copyEffectiveDatesToRelatedDocs(List<SQX_Change_Order__c> objects, Map<Id, SQX_Change_Order__c> oldValues){
        if(objects.size() > 0){
            final String ACTION_NAME = 'cqaware-effectiveDateCopy';
            Map<Id, SQX_Change_Order__c> validChangeOrders = new Map<Id, SQX_Change_Order__c>();
            
            SQX_BulkifiedBase base = new SQX_BulkifiedBase(objects, oldValues);
            base.removeProcessedRecordsFor(SQX.ChangeOrder, ACTION_NAME); //prevent reentrant issue
            
            if(base.view.size() > 0){
                
                // identify change order that has been recently released or has effective date change when in pre-release
                for(SQX_Change_Order__c changeOrder : (List<SQX_Change_Order__c>)base.view){
                    SQX_Change_Order__c oldChangeOrder = oldValues.get(changeOrder.Id);
                    
                    if(oldChangeOrder != null){
                        // if the change order has been recently release or 
                        // if the effective date of change order has been changed after release i.e. in pre-release or closed state
                        
                        if((changeOrder.Status__c == SQX_Change_Order.STATUS_COMPLETE && changeOrder.Record_Stage__c == SQX_Change_Order.STAGE_COMPLETE )|| changeOrder.Status__c == SQX_Change_Order.STATUS_CLOSED){
                            if( oldChangeOrder.Effective_Date__c != changeOrder.Effective_Date__c){
                                validChangeOrders.put(changeOrder.Id, changeOrder);
                            }
                        }
                    }
                }
                
                base.addToProcessedRecordsFor(SQX.ChangeOrder, ACTION_NAME, validChangeOrders.values());
                
                if(validChangeOrders.size() > 0){

                    // get all implementations for the change order along with their controlled documents.
                    Map<Id, SQX_Controlled_Document__c> controlledDocsToUpdate = new Map<Id, SQX_Controlled_Document__c>();
                    for(SQX_Implementation__c implementation : [SELECT Id, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Document_Status__c , CQ_Aware_Type__c,Type__c,
                                                               SQX_Controlled_Document_New__c, SQX_Controlled_Document_New__r.Document_Status__c,
                                                               SQX_Change_Order__c
                                                               FROM SQX_Implementation__c
                                                                WHERE SQX_Change_Order__c = : validChangeOrders.keySet()
                                                                AND Status__c = : SQX_Implementation_Action.STATUS_COMPLETE]){
                        
                        SQX_Change_Order__c change = validChangeOrders.get(implementation.SQX_Change_Order__c);
                        
                        
                        if(implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_NEW || 
                            implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_REVISE){
                            
                            // update the controlled document's effectived date if it is new or revise and is in pre-release
                            
                            if(implementation.SQX_Controlled_Document_New__r != null 
                                && implementation.SQX_Controlled_Document_New__r.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_RELEASE){
                                    controlledDocsToUpdate.put(implementation.SQX_Controlled_Document_New__c, 
                                        new SQX_Controlled_Document__c(
                                            Id = implementation.SQX_Controlled_Document_New__c,
                                            Effective_Date__c = change.Effective_Date__c
                                    ));
                            }
                        }
                        else if (implementation.Type__c == SQX_Implementation.AWARE_DOCUMENT_OBSOLETE){
                            
                            // update the controlled document's expiration date if it is in pre-expire
                            if(implementation.SQX_Controlled_Document__r != null 
                                    && implementation.SQX_Controlled_Document__r.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_EXPIRE){
                                    controlledDocsToUpdate.put(implementation.SQX_Controlled_Document_New__c, 
                                        new SQX_Controlled_Document__c(
                                            Id = implementation.SQX_Controlled_Document__c,
                                            Expiration_Date__c = change.Effective_Date__c
                                    ));     
                            }
                        }
                                                
                    }
                    
                    if(controlledDocsToUpdate.values().size()  > 0){
                        try{
                            // WITHOUT SHARING:
                            // using without sharing because a controlled document could be owned by someone else than Change coordinatr 
                            new SQX_DB().withoutSharing().op_update(controlledDocsToUpdate.values(), new List<SObjectField> {
                                SQX_Controlled_Document__c.Effective_Date__c
                            });
                        }
                        catch(DMLException ex){
                            for(SQX_Change_Order__c changeOrder : validChangeOrders.values()){
                                String errorDocs = ''; 
                                for(integer i  = 0; i < ex.getNumDml(); i++){
                                    errorDocs += ex.getDmlId(i) + ', ';
                                }
                                
                                String message = Label.SQX_ERR_MSG_CHANGE_ORDER_RELEASE_BLOCKED_BY_DOC;
                                message = message.replace('{ERRORDOCIDS}', errorDocs);
                                changeOrder.addError(message);
                            }
                        }
                    }                
                    
                }
            }
            
        }
    }

    /**
     * method to complete impacted document in CAPA when document is current
     */
    public static void updateImpactedDocumentStatus(Set<Id> currentDocIds){
        List<SQX_Impacted_Document__c> impactedDocumentList = new List<SQX_Impacted_Document__c>();
        impactedDocumentList = [SELECT Id, Status__c, SQX_CAPA__c,Completion_Date__c 
                                FROM SQX_Impacted_Document__c 
                                WHERE SQX_Controlled_Document__c =: currentDocIds AND Status__c =: SQX_Impacted_Document.STATUS_OPEN AND SQX_Controlled_Document__r.Document_Status__c =: SQX_Controlled_Document.STATUS_CURRENT];
        Set<Id> capaIds = new Set<Id>();
        if(impactedDocumentList.size() > 0){
            for(SQX_Impacted_Document__c impactedDocument : impactedDocumentList){
                impactedDocument.Status__c = SQX_Impacted_Document.STATUS_COMPLETE;
                if(impactedDocument.Completion_Date__c == null){
                    impactedDocument.Completion_Date__c = Date.today();
                }
                capaIds.add(impactedDocument.SQX_CAPA__c);
            }
            List<SQX_CAPA__c> capaList = [SELECT Id FROM SQX_CAPA__c WHERE Id =: capaIds];

            /*
            * WITHOUT SHARING used
            * --------------------
            * When controlled doc is in moved to current status, current user may not have edit access to the impacted document
            * so without sharing is used to give user access to update impacted document
            */
            new SQX_DB().withoutSharing().op_update(impactedDocumentList, new List<Schema.SObjectField>{SQX_Impacted_Document__c.Status__c,
                                                                                                        SQX_Impacted_Document__c.Completion_Date__c});

            /*
            * WITHOUT SHARING used
            * --------------------
            * When controlled doc is in moved to current status, current user may not have edit access to the CAPA
            * so without sharing is used to give user access to update CAPA so that CAPA completion logic can be evaluated
            */
            new SQX_DB().withoutSharing().op_update(capaList, new List<Schema.SObjectField>{});
        }
    }

    /**
     * This method completes related sf tasks of audit
     * @param auditList List of audits whose sf task to be completed
     */
    public static void completeSFTask(List<SQX_Audit__c> auditList) {
        Set<Id> childWhatIds = new Set<Id>();
        Set<Id> whatIds = new Set<Id>();
        Map<String, SQX_Audit__c> stepsToAuditStatus = new Map<String, SQX_Audit__c>();
        Map<Id, SQX_Audit__c> auditMap = new Map<Id, SQX_Audit__c>(auditList);
        
        List<Sobject> steps = new List<Sobject>();
        for(SQX_Audit__c audit: new PrivilegeEscalator().getAuditsSteps(auditList)) {
            steps.addAll(audit.SQX_Onboarding_Steps__r);
            steps.addAll(audit.SQX_Deviation_Process_Steps__r);
            steps.addAll(audit.SQX_Supplier_Interaction_Steps__r);
            steps.addAll(audit.SQX_Supplier_Escalation_Steps__r);
        }

        Schema.DescribeFieldResult fieldResult = SQX_Onboarding_Step__c.Result__c.getDescribe();
        Set<String> stepResults = new Set<String>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            stepResults.add(f.getValue());
        }
        
        /**
         * WITHOUT SHARING Used
         * -----------------------
         * In case audit has private sharing setting we will not be able to access the audit related tasks in
         * incorrect behaviour of the system. Therefore, without sharing has to be used.
         */
        Action updateSupplierStep = new Action(true);

        for(Sobject step : steps){
            
            SQX_Audit__c audit = auditMap.get((String)step.get('SQX_Audit_Number__c'));
            String auditResult = audit.Audit_Result__c;
            String auditStatus = audit.Status__c;
            String result = '';
            if(stepResults.contains(auditResult)){
                result = auditResult;
                if(auditStatus == SQX_Audit.STATUS_COMPLETE){

                    step.put(SQX_Steps_Trigger_Handler.FIELD_STATUS, SQX_Steps_Trigger_Handler.STATUS_COMPLETE);
                    step.put(SQX_Steps_Trigger_Handler.FIELD_RESULT,result);
                    step.put(SQX_Steps_Trigger_Handler.FIELD_COMMENT, Label.CQ_UI_Audit_Task_Completion_Comment);
                }
                // if the audit is void then update result to NO GO and also put separate description
                if (auditStatus == SQX_Audit.STATUS_VOID) {
                    step.put(SQX_Steps_Trigger_Handler.FIELD_RESULT,result);
                    step.put(SQX_Steps_Trigger_Handler.FIELD_COMMENT, Label.CQ_UI_Audit_Task_Completion_Comment_On_Audit_Void);
                }
                updateSupplierStep.op_update(audit, step);
            } else{
                audit.addError(Label.CQ_UI_Audit_Cannot_Be_Closed_Due_To_A_Pending_Issue);
            }
        }

        updateSupplierStep.op_commit();
    }

    /**
     * WITHOUT SHARING Used
     * -----------------------
     * In case audit has private sharing setting we will not be able to access the audit related tasks in
     * incorrect behaviour of the system. Therefore, without sharing has to be used.
     */
    public without sharing class PrivilegeEscalator {

        /**
        * this method is used to get audits related steps
        * @param 'auditList' list of audits
        */
        public List<SQX_Audit__c> getAuditsSteps(List<SQX_Audit__c> auditList) {
            return [SELECT Id, Status__c,
                    (SELECT Id, SQX_Audit_Number__c FROM SQX_Onboarding_Steps__r WHERE Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN),
                    (SELECT Id, SQX_Audit_Number__c FROM SQX_Deviation_Process_Steps__r WHERE Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN),
                    (SELECT Id, SQX_Audit_Number__c FROM SQX_Supplier_Interaction_Steps__r WHERE Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN),
                    (SELECT Id, SQX_Audit_Number__c FROM SQX_Supplier_Escalation_Steps__r WHERE Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN)
                    FROM SQX_Audit__c WHERE Id = : auditList];
        }
    }
}