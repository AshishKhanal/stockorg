/**
* the trigger handler for supplier part
*/
public with sharing class SQX_Supplier_Part{
    

    public with sharing class Bulkified extends SQX_BulkifiedBase{


        public Bulkified(List<SQX_Supplier_Part__c> newSupplierParts, Map<Id, SQX_Supplier_Part__c> oldSupplierParts){
            super(newSupplierParts, oldSupplierParts);
        }



        /**
        * this trigger ensures that reason for change reflects the change in roll up summary
        * @author Prashan Vaidya
        * @date 2014/2/5
        */
        public Bulkified AfterUpdateSupplierPartActiveFieldUpdateReasonForChangeOnAccount(){
        
            Rule haveBeenActivated = new Rule();
            haveBeenActivated.addRule(Schema.SQX_Supplier_Part__c.Active__c, RuleOperator.Equals, true);
            this.resetView().applyFilter(haveBeenActivated, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            Set<Id> activePartsAccounts = this.getIdsForField(Schema.SQX_Supplier_Part__c.Account__c);
            Map<Id, SQX_Supplier_Part__c> activeParts = new Map<Id, SQX_Supplier_Part__c>((List<SQX_Supplier_Part__c>) this.view);

            Rule haveBeenDeactivated = new Rule();
            haveBeenDeactivated.addRule(Schema.SQX_Supplier_Part__c.Active__c, RuleOperator.Equals, false);
            this.resetView().applyFilter(haveBeenDeactivated, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            Set<Id> inactivePartsAccounts = this.getIdsForField(Schema.SQX_Supplier_Part__c.Account__c);
            Map<Id, SQX_Supplier_Part__c> inactiveParts = new Map<Id, SQX_Supplier_Part__c>((List<SQX_Supplier_Part__c>) this.view);
            
            Map<Id, Account> accountsToUpdate = new Map<Id, Account>();
            
            if(activeParts.values().size() > 0){

                

                for(Account account : [SELECT Id,Supplier_Risk_Level__c FROM Account WHERE Id IN : activePartsAccounts]){
                    
                    for(SQX_Supplier_Part__c supplierPart : activeParts.values()){
                        if(supplierPart.Account__c == account.Id &&
                        (account.Supplier_Risk_Level__c == null || account.Supplier_Risk_Level__c > supplierPart.Supplier_Part_Risk_Level__c) ){
                            
                            account.Reason_For_Change__c = 'Account Risk level updated from ' + account.Supplier_Risk_Level__c 
                                                    + ' to ' + supplierPart.Supplier_Part_Risk_Level__c;

                            accountsToUpdate.put(account.Id, account);
                        }
                    }
                    
                    
                }
            }

            if(inactiveParts.values().size() > 0 ){

                Set<Id> ignoreinactiveParts = new Set<Id>();
                
                //now for inactive parts
                for(AggregateResult aggregate: [SELECT Min(Supplier_Part_Risk_Level__c) minRisk, Account__c
                                          FROM  SQX_Supplier_Part__c
                                          WHERE  Active__c = TRUE AND Account__c IN : inactivePartsAccounts AND Id NOT IN : inactiveParts.values()
                                          GROUP BY Account__c]){

                    
                    integer minimumRisk = Integer.valueOf(aggregate.get('minRisk'));
                    Id accountId = ID.valueOf((String)aggregate.get(SQX.getNSNameFor('Account__c')));

                    Account account = accountsToUpdate.get(accountId);

                    for(SQX_Supplier_Part__c supplierPart : inactiveParts.values()){

                        if(supplierPart.Account__c == accountId){
                        
                            if(minimumRisk > supplierPart.Supplier_Part_Risk_Level__c){

                                if(account == null){
                                    account = new Account();
                                    account.Id = accountId;
    
                                    accountsToUpdate.put(accountId, account);
                                }
    
                                account.Reason_For_Change__c = 'Account Risk level updated from'+ supplierPart.Supplier_Part_Risk_Level__c 
                                                        + ' to ' + minimumRisk;
                            }
                            else{
                                ignoreinactiveParts.add(supplierPart.Id);
                            }
                        }
                    }
                }


                //if any account is not in update list, then the supplier part was the last one
                for(SQX_Supplier_Part__c supplierPart: inactiveParts.values()){
                    Account account = accountsToUpdate.get(supplierPart.Account__c);

                    if(account == null && ignoreinactiveParts.contains(supplierPart.Id) == false){
                    
                        account = new Account();
                        account.Id = supplierPart.Account__c;
                        account.Reason_For_Change__c = 'Account risk level updated from ' + supplierPart.Supplier_Part_Risk_Level__c;
                        accountsToUpdate.put(account.Id, account);


                    }
                }
                
            }


            if(accountsToUpdate.values().size() > 0){
                new SQX_DB().op_update(accountsToUpdate.values(), new List<SObjectField>{
                    Account.fields.Reason_For_Change__c

                });
            }

                
            return this;
        }
    }
}