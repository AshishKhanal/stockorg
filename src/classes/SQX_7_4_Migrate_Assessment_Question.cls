/**
* Batch processor to copy old question and answer options to new question long and answer options long
* Added in version  : 7.4.0
* Used for versions : all earlier versions
*/
global with sharing class SQX_7_4_Migrate_Assessment_Question implements Database.Batchable<sObject> {

    public static final String MIGRATION_FAILED = '7.4 Migration Failed';
    public static final String MIGRATION_COMPLETED = '7.4 Migration Completed';
    
    /**
    * Gets invoked when the batch job starts and selects all assessment questions
    * @param bc reference to the Database.BatchableContext object
    * @return Database.QueryLocator object that contains asessment questions passed to the job
    */
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Question, Question Long, Answer Options and Answer Options Long whose Migration Status is not Completed
		return Database.getQueryLocator([SELECT Id, Question__c, Question_Long__c, Answer_Options__c, Answer_Options_Long__c from SQX_Assessment_Question__c WHERE Batch_Job_Status__c !=: MIGRATION_COMPLETED]);
    }
	
	/** 
    * Execute the method to set answer options long and question long from old answer options and question
    * @param bc reference to the Database.BatchableContext object
    * @param scope list of sObjects containing assessment question
    */    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        
        //List of Assessment Questions
        List<SQX_Assessment_Question__c> assessmentQuestions = (List<SQX_Assessment_Question__c>)scope;
        
        //Loop through each Assessment Questions and copy Answer Options to Answer Options Long and Question to Question Long
        for(SQX_Assessment_Question__c assessmentQuestion : assessmentQuestions){
            assessmentQuestion.Answer_Options_Long__c = assessmentQuestion.Answer_Options__c;
            assessmentQuestion.Question_Long__c = assessmentQuestion.Question__c;
        }
		
        //List for Assessment Questions which failed in migration
		List<SQX_Assessment_Question__c> failedAssessmentQuestions = new List<SQX_Assessment_Question__c>();
        
        /**
        * Without Sharing is used because migration should affect all data
        * Update continues even when error occurs during batch update
        */
        Database.SaveResult[] saveResults = new SQX_DB().withoutSharing().continueOnError().op_update(addMigrationStatus(assessmentQuestions), new List<SObjectField>{SQX_Assessment_Question__c.Answer_Options_Long__c, SQX_Assessment_Question__c.Question_Long__c});
        for(Integer i = 0; i < saveResults.size(); i++){
            Database.SaveResult saveResult = saveResults[i];
            SQX_Assessment_Question__c assessmentQuestion = assessmentQuestions[i];
            
            //If any Assessment Question is failed to migrate then add status of Migration Failed
            if(!saveResult.isSuccess()){
                SQX_Assessment_Question__c failedAssessmentQuestion = new SQX_Assessment_Question__c();
                failedAssessmentQuestion.Id = failedAssessmentQuestion.Id;
                failedAssessmentQuestion.Batch_Job_Status__c = MIGRATION_FAILED;
                failedAssessmentQuestion.Batch_Job_Error__c = SQX_Utilities.getFormattedErrorMessage(saveResult.getErrors());
                failedAssessmentQuestions.add(failedAssessmentQuestion);
            }
        }
        
        //If any Assessment Question failed to migrate exist update Assessment Question
        if(!failedAssessmentQuestions.isEmpty()){
            
            /**
            * Without Sharing is used because migration should affect all data
            * Update is halted and reverted if error occurs during batch update
            */
            new SQX_DB().withoutSharing().op_update(failedAssessmentQuestions, new List<SObjectField>{SQX_Assessment_Question__c.Batch_Job_Status__c, SQX_Assessment_Question__c.Batch_Job_Error__c});
        }
    }
    
    /**
    * Used to add Migration Status in all assessment questions
    * @param {audits} is the list of audit which contains all audits in which migration status is to be added
    * @return {auditWithMigrationStatus} is the list of audit with migration status set
    */
    public List<SQX_Assessment_Question__c> addMigrationStatus(List<SQX_Assessment_Question__c> assessmentQuestions){
        List<SQX_Assessment_Question__c> assessmentQuestionWithMigrationStatus = new List<SQX_Assessment_Question__c>();
        for(SQX_Assessment_Question__c assessmentQuestion : assessmentQuestions){
            assessmentQuestion.Batch_Job_Status__c = MIGRATION_COMPLETED;
            assessmentQuestion.Batch_Job_Error__c = null;
            assessmentQuestionWithMigrationStatus.add(assessmentQuestion);
        }
        return assessmentQuestionWithMigrationStatus;
    }
    
    /** 
    * Used to execute post-processing operations and is called once after all batches are processed
    * @param bc reference to the Database.BatchableContext object
    */
    global void finish(Database.BatchableContext bc) {}
    
}