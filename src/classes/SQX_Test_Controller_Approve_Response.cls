/**
* Unit tests for SQX_Controller_Approve_Response
*
* @author Sagar Shrestha
* @date 2014/5/08
* 
*/
@isTest
public class SQX_Test_Controller_Approve_Response{

    /**
    *   Setup: Create a SCAR for the account X
    *   Action: Submit a response for approval
    *   Expected: Should return a List of responses to be approved by the user. This list should contain the submitted response.
    *            Also returns a list of responses which are not Closed/Void
    *
    * @author Sagar Shrestha
    * @date 2014/5/08
    *
    */
    public static testmethod void CreateCAPA_SubmitResponseForApproval_ShouldReturnSubmittedResponse(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(adminUser){
            //setup
            SQX_Test_Finding finding1 = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            
            //create CAPA1
            SQX_CAPA__c CAPA1= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =finding1.supplier.Id,
                SQX_Finding__c=finding1.finding.Id
                );
    
            //insert CAPA1;
            Database.SaveResult result1 = Database.insert(CAPA1,false);
    
            //insert and publish a response 
            SQX_Finding_Response__c SCARresponse1 = new SQX_Finding_Response__c(SQX_Finding__c = finding1.finding.Id, Response_Summary__c = 'I am a response');
            insert SCARresponse1;
    
            SQX_Approval_Util.submitForApproval(new sObject[]{SCARresponse1});
    
            SQX_Test_Finding finding2 = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                .setRequired(true,true,true,true,true) //response required, containment reqd, investigation required, ca required, pa required
                                .setApprovals(true, true, true)   //investigation approval, changes in ap approval, effectiveness review
                                .setStatus(SQX_Finding.STATUS_OPEN)
                                .save();
            
            //select a profile
    
            Account newAccount= SQX_Test_Account_Factory.createAccount();
            Contact newContact= SQX_Test_Account_Factory.createContact(newAccount);
    
            User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
    
            //create CAPA2 with another user as approver
            SQX_CAPA__c CAPA2= new SQX_CAPA__c(Issued_Date__c=Date.today(),
                Target_Due_Date__c=Date.today(),
                SQX_Account__c =finding1.supplier.Id,
                SQX_Finding__c=finding1.finding.Id,
                SQX_Action_Approver__c=newUser.Id
                );
    
            //insert CAPA2;
            Database.SaveResult result2 = Database.insert(CAPA2,false);
    
            //insert and publish a response 
            SQX_Finding_Response__c SCARresponse2 = new SQX_Finding_Response__c(SQX_Finding__c = finding2.finding.Id, Response_Summary__c = 'I am a response');
            insert SCARresponse2;
            
    
            //Submit for Approval
            
            SQX_Approval_Util.submitForApproval(new sObject[]{SCARresponse2});
    
            SQX_Controller_Approve_Response scar= new SQX_Controller_Approve_Response();
            List<SQX_Controller_Approve_Response.ResponseToApprove> responses= scar.getResponseForApproval();
    
            boolean containsSubmittedResponseforDefaultUser=false;
    
            //check if controller returns response for default user
            for(SQX_Controller_Approve_Response.ResponseToApprove response :responses)
            {
                if(response.relatedItem.Id==SCARresponse1.Id)
                {
                    containsSubmittedResponseforDefaultUser=true;
                    break;
                }
            }
    
            boolean containsSubmittedResponseforAnotherUser=false;
    
            //check if controller returns response for another user
            for(SQX_Controller_Approve_Response.ResponseToApprove response :responses)
            {
                if(response.relatedItem.Id==SCARresponse2.Id)
                {
                    containsSubmittedResponseforAnotherUser=true;
                    break;
                }
            }
            
            boolean inApproval = SQX_Approval_Util.checkIfObjectIsInApproval(SCARresponse1);
            containsSubmittedResponseforDefaultUser = containsSubmittedResponseforDefaultUser  || SQX_Test_Mock_Approval_Util.objectsInApproval.contains(SCARResponse1.Id);
            
    
            System.assert(containsSubmittedResponseforDefaultUser == true,
                 'Expected SQX_Controller_Approve_Response to return submitted response for default user. Submitted response is '
                 +SCARresponse1+' Returned list of responses '+responses);
    
            System.assert(containsSubmittedResponseforAnotherUser == false,
                 'Expected SQX_Controller_Approve_Response not to return submitted response for another user. Submitted response is '
                 +SCARresponse2+' Returned list of responses '+responses);
           
            //Act : To check the responses displays even after the NC/CAPA is closed or void
            //Void the CAPA
            CAPA1.Status__c= SQX_CAPA.STATUS_CLOSED;
            CAPA1.Resolution__c = SQX_CAPA.RESOLUTION_VOID;
            CAPA1.Closure_Comment__c = 'CAPA Voided';
            
            result1 = Database.Update(CAPA1,false); 

            SQX_Controller_Approve_Response scar1= new SQX_Controller_Approve_Response();
            List<SQX_Controller_Approve_Response.ResponseToApprove> responses1= scar1.getResponseForApproval();

            boolean containsVoidResponse=false;
    
            //check if controller returns response for void response
            for(SQX_Controller_Approve_Response.ResponseToApprove response :responses1)
            {
                if(response.relatedItem.Id==SCARresponse1.Id)
                {
                    containsVoidResponse=true;
                    break;
                }
            }

            //Assert: Response doesnot displays while the status is Void/Close
            System.assert(containsVoidResponse == false,
                 'Expected SQX_Controller_Approve_Response to doesnot return submitted response of Void Response. Submitted response is '
                 +SCARresponse1+' Returned list of responses '+responses1);
         }


    }

    
}