/**
* @author Piyush Subedi
* @date 2016/12/1
*/
public with sharing class SQX_Collaboration_Group{    

    /**
    *   Trigger handler
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{        

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newReq equivalent to trigger.new in salesforce
        * @param oldMap equivalent to trigger.oldMap in salesforce
        */
        public Bulkified(List<CollaborationGroup> newReq, Map<Id, CollaborationGroup> oldMap) {
            super(newReq, oldMap);            
        }
        
        /**
        *   Method prevents user to delete group if the group has been initiated from a controlled doc record and is still active
        */
        public Bulkified preventDeletion(){
            this.resetView();

            Map<Id, CollaborationGroup> allGroups = new Map<Id, CollaborationGroup>((List<CollaborationGroup>) this.view);
            Set<Id> allIds = allGroups.keySet();
            for (SQX_Controlled_Document__c doc : [ SELECT Collaboration_Group_Id__c FROM SQX_Controlled_Document__c 
                                                    WHERE Collaboration_Group_Id__c IN :allIds]){
                CollaborationGroup grp = allGroups.get(doc.Collaboration_Group_Id__c);
                if(grp != null){
                    grp.addError(Label.CQ_UI_Cannot_Delete_Controlled_Doc_Collab_Group);
                }
            }
            return this;
        }
    }
}