/**
 * test class to test inbound material transaction processing
 */
@isTest
public class SQX_Test_3103_Inbound_MT_Processing {

    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }

    /**
     * method to create part family
     */
    public static Id createPartFamily() {
        Schema.SObjectField f = SQX_Part_Family__c.Fields.name;
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'testPartFamily');

        Database.upsert(partFamily,f);
        
        return partFamily.Id;
    }

    /**
     * method to create account
     */
    public static Account createAccount(){
        account acct =  new Account(Name='testAccount');
        database.insert(acct);
        return acct;
    }


    
    /**
     * method to create process
     */
    public static SQX_Standard_Service__c createStandardService(){
        Schema.SObjectField f = SQX_Standard_Service__c.Fields.name;
        SQX_Standard_Service__c service = new SQX_Standard_Service__c(name='testService');
        database.upsert(service,f);  
        return service;       
    }

    /**
     * method to create part
     */
    public static SQX_Part__c createPart() {
        Schema.SObjectField f = SQX_Part__c.Fields.Part_Number__c;
        SQX_Part__c part = new SQX_Part__c(
                                            Part_Family__c = createPartFamily(),
                                            Part_Number__c = 'testPartNumber',
                                            Part_Risk_Level__c = 1,
                                            Name = 'testPartName'
                                            );

        //insert new Part record
        Database.upsert(part,f);

        return part;
    }

    /**
     * method to create material transaction with required fields to run batch job
     */
    public static SQX_Material_Transaction__c createMT (){
        SQX_Material_Transaction__c mt = new SQX_Material_Transaction__c(
                                                Transaction_Code__c = SQX_Material_Transaction.CODE_TRANSACTION_INCOMING_RECEIPT,
                                                Status__c = SQX_Material_Transaction.STATUS_PENDING);

        return mt;
    }
    
    /*
     * Given: An material transaction is saved with invalid part number
     * When: Batch job for material transaction is run
     * Then: Error is thrown
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_InvalidPartNumberIsAdded_ReceiptWithPartIsNotCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            createPart();
        }
        System.runas(standardUser){
            // Arrange :  An material transaction is saved with invalid part number
            SQX_Material_Transaction__c mt = createMT();
            mt.Part_Number__c = 'testPartNumber1';
            insert mt;
            
            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Error is thrown
            System.assertEquals(SQX_Material_Transaction.STATUS_FAILED, mt.Status__c);

            System.assert(mt.Error_Description__c.contains(Label.SQX_ERR_MSG_Part_Number_does_not_exist));
        }
    }  
    
    /*
     * Given: An material transaction is saved with valid part number
     * When: Batch job for material transaction is run
     * Then: Receipt is created
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_ValidPartNumberIsAdded_ReceiptWithPartIsCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part__c part;
        
        System.runAs(adminUser){
            part = createPart();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Part_Number__c = 'testPartNumber';
            mt.Part_Rev__c = '1';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c, SQX_Part_Number__c, SQX_Receipt_Number__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Receipt is created
            System.assertNotEquals(null, mt.SQX_Receipt_Number__c);
            
            System.assertEquals(SQX_Material_Transaction.STATUS_SUCCESS, mt.Status__c);

            System.assertEquals( part.Id, mt.SQX_Part_Number__c);
        }
    }    
    
    /*
     * Given: An material transaction is saved with invalid Process
     * When: Batch job for material transaction is run
     * Then: Error is thrown
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_InvalidProcessIsAdded_ReceiptWithProcessIsNotCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            createStandardService();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Process__c = 'testService1';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Error is thrown
            System.assertEquals(SQX_Material_Transaction.STATUS_FAILED, mt.Status__c);

            System.assert(mt.Error_Description__c.contains(Label.SQX_ERR_MSG_Process_does_not_exist));
        }
    }  
    
    /*
     * Given: An material transaction is saved with valid process
     * When: Batch job for material transaction is run
     * Then: Receipt is created
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_ValidProcessIsAdded_ReceiptWithProcessIsCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Standard_Service__c process;
        
        System.runAs(adminUser){
            process = createStandardService();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Process__c = 'testService';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c, SQX_Process__c, SQX_Receipt_Number__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Receipt is created
            System.assertNotEquals(null, mt.SQX_Receipt_Number__c);
            System.assertEquals(SQX_Material_Transaction.STATUS_SUCCESS, mt.Status__c);

            System.assertEquals( process.Id, mt.SQX_Process__c);
        }
    }  
    
    /*
     * Given: An material transaction is saved with invalid account
     * When: Batch job for material transaction is run
     * Then: Error is thrown
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_InvalidAccountIsAdded_ReceiptWithAccountIsNotCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            createPart();
            createAccount();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Part_Number__c = 'testPartNumber';
            mt.Part_Rev__c = '1';
            mt.Supplier_Name__c = 'testAccount1';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Error is thrown
            System.assertEquals(SQX_Material_Transaction.STATUS_FAILED, mt.Status__c);

            System.assert(mt.Error_Description__c.contains(Label.SQX_ERR_MSG_Supplier_does_not_exist));
        }
    } 
    
    /*
     * Given: An material transaction is saved with valid Account
     * When: Batch job for material transaction is run
     * Then: Receipt is created
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_ValidAccountIsAdded_ReceiptWithAccountIsCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        Account acc;
        
        System.runAs(adminUser){
            createPart();
            acc = createAccount();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Part_Number__c = 'testPartNumber';
            mt.Part_Rev__c = '1';
            mt.Supplier_Name__c = 'testAccount';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c, Supplier__c, SQX_Receipt_Number__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Receipt is created
            System.assertNotEquals(null, mt.SQX_Receipt_Number__c);

            System.assertEquals(SQX_Material_Transaction.STATUS_SUCCESS, mt.Status__c);

            System.assertEquals( acc.Id, mt.Supplier__c);
        }
    } 
    
    /*
     * Given: An material transaction is saved with invalid division, business unit and site
     * When: Batch job for material transaction is run
     * Then: Error is thrown
     * @date: 2017-04-06
     * @story: [SQX-3103]
     */
    public static testMethod void givenAMaterialTransaction_InvalidDivBUSiteIsAdded_ReceiptWithDivBUSiteIsNotCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            createPart();
        }
        System.runas(standardUser){
            SQX_Material_Transaction__c mt = createMT();
            mt.Part_Number__c = 'testPartNumber';
            mt.Part_Rev__c = '1';
            mt.Division__c = 'Sample Division';
            mt.Business_Unit__c = 'Invalid Sample Business Unit';
            mt.Region__c = 'Invalid Sample Region';
            mt.Site__c = 'Invalid Sample Site';
            insert mt;

            // Act : Batch job for material transaction is run
            Test.startTest();
            SQX_Material_Transaction_Job_Processor mtProcessor = new SQX_Material_Transaction_Job_Processor();
            Database.executeBatch(mtProcessor);
            Test.stopTest();

            mt = [SELECT Id, Status__c, Error_Description__c FROM SQX_Material_Transaction__c WHERE Id =: mt.Id];

            // Assert : Error is thrown
            System.assertEquals(SQX_Material_Transaction.STATUS_FAILED, mt.Status__c);
            System.assert(mt.Error_Description__c.contains('Org. Business Unit: bad value'), 'Expected error for invalid values');
        }
    } 

}