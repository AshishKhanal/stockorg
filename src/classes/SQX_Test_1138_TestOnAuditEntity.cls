/**
* This test ensures 
* a) when an audit is created, the owner is set as a member of audit team
* b) the owner can't be removed from the audit
* c) when audit team member is added or removed, sharing rules are updated accordingly
*/
@IsTest
public class SQX_Test_1138_TestOnAuditEntity {

  public static boolean runAllTests = true,
              run_givenAuditIsScheduled_OwnerIsAddedAsTeamMember = false,
              run_givenAuditOwnerIsRemovedFromAudit_ExceptionIsThrown = false;
             // run_givenAuditTeamMemberIsAddedRemoved_SharingRulesAreSynced = false;


    /**
    * Ensures that once audit is schedules, owner is added as a team member by default.
    * SQX-1138
    * "Audit team at the minimum should have a Lead Auditor who is same as the Record Owner"
    */
    public testmethod static void run_givenAuditIsScheduled_OwnerIsAddedAsTeamMember(){

      if(!runAllTests && !run_givenAuditIsScheduled_OwnerIsAddedAsTeamMember){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        

        System.runAs(standardUser){
          //Arrange: Create a test audit and schedule it
          SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
          audit.setStage(SQX_Audit.STAGE_SCHEDULED);
          audit.setStatus(SQX_Audit.STATUS_OPEN);

          //Act: Save the audit
          audit.Save();

          //Assert: Ensure that owner of the audit is added to audit team.
          System.assertEquals(1, [SELECT Id FROM SQX_Audit_Team__c 
                    WHERE SQX_User__c =: standardUser.Id AND SQX_Audit__c = : audit.audit.Id]
                    .size());

        }
    }

    /**
    * Ensures that an audit owner can't be removed from audit team
    */
    public testmethod static void givenAuditOwnerIsRemovedFromAudit_ExceptionIsThrown(){
      if(!runAllTests && !run_givenAuditOwnerIsRemovedFromAudit_ExceptionIsThrown){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);
        User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        

        System.runAs(standardUser){
          //Arrange: Create a test audit and schedule it
          SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
          audit.setStage(SQX_Audit.STAGE_SCHEDULED);
          audit.setStatus(SQX_Audit.STATUS_OPEN);

          //Act: Save the audit, this should add an entry in audit team
          audit.Save();
          SQX_Audit_Team__c ownerMember = [SELECT Id FROM SQX_Audit_Team__c 
                    WHERE SQX_User__c =: standardUser.Id AND SQX_Audit__c = : audit.audit.Id];

        try{
          new SQX_DB().op_delete(new List<SQX_Audit_Team__c>{  ownerMember});

          System.assert(false, 'Expected an exception to be thrown i.e. delete to fail');
        }
        catch(DMLException ex){

          //Assert: assert that an exception was thrown with the actual message.
          System.assert(ex.getMessage().contains(Label.SQX_ERR_MSG_AUDIT_TEAM_CANT_BE_DELETED)
            , 'Expected error didn\'t occur, something else went wrong' + ex);
        }

        }
    }

    /**
    * Adding team member shares the record with the team member as read/write,
    * Removing team member removes the read/write access from the record.
    * Transferring the ownership adds the adds the new owner as member
    */

    /*
    public testmethod static void givenAuditTeamMemberIsAddedRemoved_SharingRulesAreSynced(){

      if(!runAllTests && !run_givenAuditTeamMemberIsAddedRemoved_SharingRulesAreSynced){
            return;
      }

        //Arrange: Create required users and test audit
      User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
           secondUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
           thirdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
           fourthUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null);

      SQX_Test_Audit audit = null;

      System.runAs(standardUser){
        audit = new SQX_Test_Audit();
        audit.Save();

        //initialAccessLevel is required because in assertion 2, we need to assert that the original access level
        //was restored.
        Boolean initialAccessLevel = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : secondUser.Id].HasEditAccess;

        //Act 1: Add second user as a team member in the audit
        SQX_Audit_Team__c newMember = new SQX_Audit_Team__c();
        newMember.SQX_Audit__c = audit.audit.Id;
        newMember.SQX_User__c = secondUser.Id;


        new SQX_DB().op_insert(new List<SQX_Audit_Team__c> { newMember }, new List<Schema.SObjectField>{
            Schema.SQX_Audit_Team__c.SQX_Audit__c,
            Schema.SQX_Audit_Team__c.SQX_User__c
          });

        //Assert 1: assert that the new member has read/write access on the audit.
        //we don't really care if sharing rule was added or sharing setting is set to Public R/W
        //Means don't matter, end is what matters. ;-)

      UserRecordAccess access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : secondUser.Id];

      System.assertEquals(true, access.HasEditAccess);


      //Act 2: Remove the second user from the audit
      new SQX_DB().op_delete(new List<SQX_Audit_Team__c>{newMember} );

      //Assert 2: assert that new member's R/W access has been removed.
      //In this case we need to care if
      access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : secondUser.Id];

      System.assertEquals(initialAccessLevel, access.HasEditAccess);


      //Arrange 3: Insert a audit team member
      initialAccessLevel = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : fourthUser.Id].HasEditAccess;

      newMember = new SQX_Audit_Team__c();
        newMember.SQX_Audit__c = audit.audit.Id;
        newMember.SQX_User__c = fourthUser.Id;

        new SQX_DB().op_insert(new List<SQX_Audit_Team__c> { newMember }, new List<Schema.SObjectField>{
            Schema.SQX_Audit_Team__c.SQX_Audit__c,
            Schema.SQX_Audit_Team__c.SQX_User__c
          });


        //Act 3: Change the audit team member (update the record)
        newMember.SQX_User__c = secondUser.Id;
        new SQX_DB().op_update(new List<SQX_Audit_Team__c> { newMember }, new List<Schema.SObjectField>{
            Schema.SQX_Audit_Team__c.SQX_User__c
          });


      //Assert 3: Ensure that the second user has R/W access and fourth has now initial access.
      access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : fourthUser.Id];
      System.assertEquals(initialAccessLevel, access.HasEditAccess );

      access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : secondUser.Id];
      System.assertEquals(true, access.HasEditAccess );



      //Act 4: Change the owner of the audit and save it.
      audit.audit.OwnerId = thirdUser.Id;
      audit.save();
    }
    //change of ownership will require reinitializing else we have an issue
    System.runas(thirdUser){
      Test.startTest();

      SQX_Audit_Team.reinitSharingRulesForAudit(audit.audit.Id);
    
      //Assert 4.1: Ensure new audit owner has been added to the team
      System.assertEquals(1, [SELECT Id FROM SQX_Audit_Team__c 
                    WHERE SQX_User__c =: thirdUser.Id AND SQX_Audit__c = : audit.audit.Id]
                    .size());


      //Assert 4.2: Ensure sharing rules have been initialized properly
      UserRecordAccess access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : secondUser.Id];
      System.assertEquals(true, access.HasEditAccess );

      access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : thirdUser.Id];
      System.assertEquals(true, access.HasEditAccess );

      access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess
                   WHERE RecordId =: audit.audit.Id AND UserId = : standardUser.Id];
      System.assertEquals(true, access.HasEditAccess );

      Test.stopTest();
        

      }
    }

    */


}