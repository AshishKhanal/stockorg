/*
* author: Anish Shrestha
* date : 2014/11/20
* description:  Use to redirect to related CAPA or NC when the finding record is clicked
                Used for standard approval process in salesforce1 mobile
*/

public with sharing class SQX_Extension_Finding_Redirect  {

    private SQX_Finding__c finding = new SQX_Finding__c();

    public SQX_Extension_Finding_Redirect (ApexPages.StandardController controller) {
        this.finding = (SQX_Finding__c) controller.getRecord();
    }

    public PageReference redirectToParent(){
        
        if(finding.Id != null){

            finding = [Select Id, SQX_CAPA__c, SQX_Nonconformance__c, RecordType.Name From SQX_Finding__c Where Id = : finding.Id];

            System.debug(finding.RecordType.Name);
           
            if(finding.RecordType.Name == 'CAPA Finding'){
                PageReference redirect = new PageReference('/apex/SQX_CAPA?id='+finding.SQX_CAPA__c+'&initialTab=responseHistoryTab' );
                redirect.getParameters().put('id',finding.SQX_CAPA__c);
                //redirect('&initialTab=responseHistoryTab');
                redirect.setRedirect(true); 
                return redirect;
            }
            else if(finding.RecordType.Name == 'NC Finding'){
                PageReference redirect= new PageReference('/apex/SQX_NC?id='+ finding.SQX_Nonconformance__c +'&initialTab=responseHistoryTab');
                redirect.getParameters().put('id',finding.SQX_Nonconformance__c);
                redirect.setRedirect(true); 
                return redirect;
            }
           
        }
     return null;       
    }    
}