@IsTest
public class SQX_Test_PersonnelDocTraining_SignOff {
    
    static Boolean runAllTests = true,
        run_givenUserSignOff_WhenTrainerApprovalNotNeeded = false,
        run_givenUserAndTrainerSignOff_WhenTrainerApprovalNeeded = false,
        run_givenTrainerSignOff_RecordRequiringUserSignOff = false,
        run_givenSignOff_CompletionDateSetBeforeSignOff_DateNotChanged = false,
        run_givenValidateUserSigningOffAsTraineeAndTrainer_ErrorShown = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, 'adminUser');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user2');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user3');
    }
    
    /**
    * Electronic Signature Validation Mock Class
    */
    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; // Esignature is not validating password when consumer key is wrong.
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            if (Invalid_Request) {
                response.setStatusCode(400);
                response.setBody('Error: 400');
            } else {
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();
                if (url.contains('rightPassword')) {
                    response.setBody('{"id":"allcorrect"}');
                } else {
                    response.setBody('{"error":"incorrect password"}');
                }
            }
            
            return response;
        }
    }
    
    /**
    * test for user sign-off using SQX_Extension_PDT_User_SignOff
    */
    public testmethod static void givenUserSignOff_WhenTrainerApprovalNotNeeded() {
        if (!runAllTests && !run_givenUserSignOff_WhenTrainerApprovalNotNeeded) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('user1');
        User testUser1 = users.get('user2');
        
        SQX_Personnel_Document_Training__c DocT1;
    
        System.runas(standardUser) {
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(testUser1);
            personnel.save();
            
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required personnel document training
            DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.SaveResult result1 = Database.insert(DocT1, false);
            System.assert(result1.isSuccess(),
                'Expected personnel document training to be saved.\n' + result1.getErrors());
        }
        
        // User signoff
        System.runas(testUser1) {
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>();
            dtList.add(DocT1);
            
            // user sign-off page SQX_PersonnelDocTraining_User_SignOff using SQX_Extension_PDT_User_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_User_SignOff);
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_User_SignOff ext = new SQX_Extension_PDT_User_SignOff(controller);
            
            Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
            
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'ConsumerKeyValue';
            cqES.Consumer_Secret__c = 'ConsumerSecretValue';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            // ACT: sign-off with user credentials and comment
            ext.signingOffComment = 'test UserSign off';
            ext.SigningOffUsername = testUser1.username;
            ext.SigningOffPassword = 'rightPassword';
            ext.signOff();
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, User_Signoff_Comment__c, SQX_User_Signed_Off_By__c, User_Signature__c, User_SignOff_Date__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE,
                'Expected personnel document training to be complete.');
            
            System.assert(DocT1.Completion_Date__c != null,
                'Expected completion date of personnel document training to be set when signed off by user and trainer approval not needed.');
            
            System.assert(DocT1.SQX_User_Signed_Off_By__c == testUser1.Id,
                'Expected user signed off by value of personnel document training to be current when signed off by user.');
            
            System.assert(DocT1.User_Signature__c != null,
                'Expected user signature data of personnel document training to be set when signed off by user.');
            
            System.assert(DocT1.User_SignOff_Date__c != null,
                'Expected user signed off date of personnel document training to be set when signed off by user.');
            
            System.assert(DocT1.User_Signoff_Comment__c == 'test UserSign off',
                'Expected personnel document training User Signoff Comment to be set.');
        }
    }
    
    /**
    * test for trainer sign-off using SQX_Extension_PDT_Trainer_SignOff
    */
    public testmethod static void givenUserAndTrainerSignOff_WhenTrainerApprovalNeeded() {
        if (!runAllTests && !run_givenUserAndTrainerSignOff_WhenTrainerApprovalNeeded) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('user1');
        User testUser1 = users.get('user2');
        
        SQX_Personnel_Document_Training__c DocT1;
        
        System.runas(standardUser) {
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(testUser1);
            personnel.save();
            
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add personnel document training for trainer approval needed
            DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.SaveResult result1 = Database.insert(DocT1, false);
            System.assert(result1.isSuccess(),
                'Expected personnel document training to be saved.\n' + result1.getErrors());
        }
        
        // User signoff
        System.runas(testUser1) {
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>();
            dtList.add(DocT1);
            
            // user sign-off page SQX_PersonnelDocTraining_User_SignOff using SQX_Extension_PDT_User_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_User_SignOff);
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_User_SignOff ext = new SQX_Extension_PDT_User_SignOff(controller);
            ext.CQ_Electronic_Signature_Enabled = false;
            
            // ACT: sign-off with user credentials and comment
            ext.signingOffComment = 'test UserSign off';
            ext.signOff();
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, User_Signoff_Comment__c, SQX_User_Signed_Off_By__c, User_Signature__c, User_SignOff_Date__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                'Expected personnel document training to be in approval when trainer required and signed off by user.');
            
            System.assert(DocT1.Completion_Date__c == null,
                'Expected completion date of personnel document training not to be set when date is not entered, signed off by user and trainer approval needed.');
            
            System.assert(DocT1.SQX_User_Signed_Off_By__c == testUser1.Id,
                'Expected user signed off by value of personnel document training to be current user when signed off by user.');
            
            System.assert(DocT1.User_Signature__c != null,
                'Expected user signature data of personnel document training to be set when signed off by user.');
            
            System.assert(DocT1.User_SignOff_Date__c != null,
                'Expected user signed off date of personnel document training to be set when signed off by user.');
            
            System.assert(DocT1.User_Signoff_Comment__c == 'test UserSign off',
                'Expected personnel document training User Signoff Comment to be set.');
        }
        
        // Trainer signoff
        System.runas(standardUser) {
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>();
            dtList.add(DocT1);
            
            // trainer sign-off page SQX_PersonnelDocTraining_Trainer_SignOff using SQX_Extension_PDT_Trainer_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_Trainer_SignOff);
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_Trainer_SignOff ext = new SQX_Extension_PDT_Trainer_SignOff(controller);
            
            Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
            
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'ConsumerKeyValue';
            cqES.Consumer_Secret__c = 'ConsumerSecretValue';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            // ACT: sign-off with trainer credentials and comment
            ext.signingOffComment = 'test TrainerSign off';
            ext.SigningOffUsername = standardUser.username;
            ext.SigningOffPassword = 'rightPassword';
            ext.signOff();
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, Trainer_SignOff_Comment__c, SQX_Training_Approved_By__c, Trainer_Signature__c, Trainer_SignOff_Date__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE,
                'Expected personnel document training to be complete.');
            
            System.assert(DocT1.Completion_Date__c != null,
                'Expected completion date of personnel document training to be set when signed off by trainer and trainer approval needed.');
            
            System.assert(DocT1.SQX_Training_Approved_By__c == standardUser.Id,
                'Expected trainer approved by value of personnel document training to be current user when signed off by trainer.');
            
            System.assert(DocT1.Trainer_Signature__c != null,
                'Expected trainer signature data of personnel document training to be set when signed off by trainer.');
            
            System.assert(DocT1.Trainer_SignOff_Date__c != null,
                'Expected trainer signed off date of personnel document training to be set when signed off by trainer.');
            
            System.assert(DocT1.Trainer_SignOff_Comment__c == 'test TrainerSign off',
                'Expected personnel document training Trainer signoff comment to be set.');
        }
    }
    
    /**
    * test for trainer sign-off using SQX_Extension_PDT_Trainer_SignOff for records requiring user sign-off
    * @story 1671
    */
    public testmethod static void givenTrainerSignOff_RecordRequiringUserSignOff() {
        if (!runAllTests && !run_givenTrainerSignOff_RecordRequiringUserSignOff) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('user1');
        User testUser1 = users.get('user2');
        
        SQX_Personnel_Document_Training__c DocT1;
        
        System.runas(standardUser) {
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(standardUser);
            personnel.save();
            
            // provide read access to testUser1 to perform trainer sign-off
            insert new SQX_Personnel__Share(
                ParentId = personnel.mainRecord.Id,
                UserOrGroupId = testUser1.Id,
                AccessLevel = 'read'
            );
            
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add personnel document training
            DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );
            Database.SaveResult result1 = Database.insert(DocT1, false);
            System.assert(result1.isSuccess(),
                'Expected personnel document training to be saved.\n' + result1.getErrors());
        }
        
        // Trainer signoff
        System.runas(testUser1) {
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>();
            dtList.add(DocT1);
            
            // trainer sign-off page SQX_PersonnelDocTraining_Trainer_SignOff using SQX_Extension_PDT_Trainer_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_Trainer_SignOff);
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_Trainer_SignOff ext = new SQX_Extension_PDT_Trainer_SignOff(controller);
            ext.CQ_Electronic_Signature_Enabled = false;
            
            // ACT: trainer sign-off with comment
            ext.signingOffComment = 'test TrainerSign off';
            ext.signOff();
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, User_Signoff_Comment__c, SQX_User_Signed_Off_By__c, User_Signature__c, User_SignOff_Date__c,
                            Trainer_SignOff_Comment__c, SQX_Training_Approved_By__c, Trainer_Signature__c, Trainer_SignOff_Date__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE,
                'Expected personnel document training to be complete.');
            
            System.assert(DocT1.Completion_Date__c != null,
                'Expected completion date of personnel document training to be set when signed off by trainer.');
            
            System.assert(DocT1.SQX_User_Signed_Off_By__c == null,
                'Expected user signed off by value of personnel document training not to be set when signed off by trainer.');
            
            System.assert(DocT1.User_Signature__c == null,
                'Expected user signature data of personnel document training not to be set when signed off by trainer.');
            
            System.assert(DocT1.User_SignOff_Date__c == null,
                'Expected user signed off date of personnel document training not to be set when signed off by trainer.');
            
            System.assert(string.isBlank(DocT1.User_Signoff_Comment__c),
                'Expected User Signoff Comment of personnel document training not to be set when signed off by trainer.');
            
            System.assert(DocT1.SQX_Training_Approved_By__c == testUser1.Id,
                'Expected trainer approved by value of personnel document training to be current user when signed off by trainer.');
            
            System.assert(DocT1.Trainer_Signature__c != null,
                'Expected trainer signature data of personnel document training to be set when signed off by trainer.');
            
            System.assert(DocT1.Trainer_SignOff_Date__c != null,
                'Expected trainer signed off date of personnel document training to be set when signed off by trainer.');
            
            System.assert(DocT1.Trainer_SignOff_Comment__c == 'test TrainerSign off',
                'Expected personnel document training Trainer signoff comment to be set.');
        }
    }
    
    /**
    * test to ensure completion date not changed by sign-off process when set before sign-off
    * @story 1671
    */
    public testmethod static void givenSignOff_CompletionDateSetBeforeSignOff_DateNotChanged() {
        if (!runAllTests && !run_givenSignOff_CompletionDateSetBeforeSignOff_DateNotChanged) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('user1');
        User user2 = users.get('user2');
        
        SQX_Personnel_Document_Training__c DocT1;
        
        System.runas(standardUser) {
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(user2);
            personnel.save();
            
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add personnel document training
            DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Completion_Date__c = System.today() - 5
            );
            Date completionDate = DocT1.Completion_Date__c;
            Database.SaveResult result1 = Database.insert(DocT1, false);
            System.assert(result1.isSuccess(),
                'Expected personnel document training to be saved.\n' + result1.getErrors());
            
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>();
            dtList.add(DocT1);
            
            ApexPages.StandardSetController controller;
            
            System.runas(user2) {
                // user sign-off page SQX_PersonnelDocTraining_User_SignOff using SQX_Extension_PDT_User_SignOff
                Test.setCurrentPage(Page.SQX_PersonnelDocTraining_User_SignOff);
                controller = new ApexPages.StandardSetController(dtList);
                controller.setSelected(dtList);
                SQX_Extension_PDT_User_SignOff ext = new SQX_Extension_PDT_User_SignOff(controller);
                ext.CQ_Electronic_Signature_Enabled = false;
                
                // ACT: user sign-off with comment
                ext.signingOffComment = 'user';
                ext.signOff();
            }
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, User_Signoff_Comment__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                'Expected personnel document training to be in approval when trainer required and signed off by user.');
            
            System.assertEquals(completionDate, DocT1.Completion_Date__c,
                'Expected completion date not to be changed when user sign-off.');
            
            System.assert(DocT1.User_Signoff_Comment__c == 'user',
                'Expected user sign-off comment of personnel document training to be set when signed off by user.');
            
            
            // trainer sign-off page SQX_PersonnelDocTraining_Trainer_SignOff using SQX_Extension_PDT_Trainer_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_Trainer_SignOff);
            controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_Trainer_SignOff ext2 = new SQX_Extension_PDT_Trainer_SignOff(controller);
            ext2.CQ_Electronic_Signature_Enabled = false;
            
            // ACT: trainer sign-off with comment
            ext2.signingOffComment = 'trainer';
            ext2.signOff();
            
            DocT1 = [SELECT Id, Completion_Date__c, Status__c, Trainer_SignOff_Comment__c
                     FROM SQX_Personnel_Document_Training__c WHERE Id = :DocT1.Id];
            
            System.assert(DocT1.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE,
                'Expected personnel document training to be complete.');
            
            System.assertEquals(completionDate, DocT1.Completion_Date__c,
                'Expected completion date not to be changed when user sign-off.');
            
            System.assert(DocT1.Trainer_SignOff_Comment__c == 'trainer',
                'Expected personnel document training Trainer signoff comment to be set.');
        }
    }
    
    /**
    * validation rule Validate_User_Signing_Off_As_Trainee:
    *   ensure user other than the personnel user cannot perform User Sign-Off
    * validation rule Prevent_Trainer_SignOff_By_PersonnelUser:
    *   ensure personnel user cannot perform Trainer Sign-Off
    * ensures migrated data works properly
    * @story 2355, 2419
    */
    public testmethod static void givenValidateUserSigningOffAsTraineeAndTrainer_ErrorShown() {
        if (!runAllTests && !run_givenValidateUserSigningOffAsTraineeAndTrainer_ErrorShown) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        System.runas(user1) {
            // add required personnels
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user1);
            p1.save();
            SQX_Test_Personnel p2 = new SQX_Test_Personnel(user2);
            p2.save();
            SQX_Test_Personnel p3 = new SQX_Test_Personnel();
            p3.save();
            
            // add required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // add required personnel document training
            SQX_Personnel_Document_Training__c p1dt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            SQX_Personnel_Document_Training__c p2dt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p2.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            SQX_Personnel_Document_Training__c p3dt1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p3.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            // add migrated data
            SQX_Personnel_Document_Training__c p1dt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Completion_Date__c = Date.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                SQX_User_Signed_Off_By__c = user2.Id, // in old version, user sign-off can be done by anyone
                SQX_Training_Approved_By__c = user1.Id, // in old version, trainer sign-off can be done by anyone
                Uniqueness_Constraint__c = 'p5dt',
                IsMigrated__c = true
            );
            SQX_Personnel_Document_Training__c p2dt2 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = p2.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                SQX_Trainer__c = user1.Id,
                Due_Date__c = Date.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                SQX_User_Signed_Off_By__c = user1.Id, // in old version, user sign-off can be done by anyone
                Uniqueness_Constraint__c = 'p4dt',
                IsMigrated__c = true
            );
            List<SQX_Personnel_Document_Training__c> pdts = new List<SQX_Personnel_Document_Training__c>{ p1dt1, p2dt1, p3dt1, p1dt2, p2dt2 };
            insert pdts; // Note: p1dt2 and p2dt2 are migrated data and they are expected to be saved without error.
            
            // ACT: sign off as trainee by user1 i.e. user1 other than personnel user on training record of p2
            p2dt1.SQX_User_Signed_Off_By__c = user1.Id;
            Database.SaveResult res1 = Database.update(p2dt1, false);
            
            String expectedUserSignOffErrMsg = 'User Sign-Off as trainee can only be done by the personnel user.'; // error message of validation rule Validate_User_Signing_Off_As_Trainee
            System.assertEquals(false, res1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(res1.getErrors(), expectedUserSignOffErrMsg), 'Expected error message "' + expectedUserSignOffErrMsg + '"');
            
            
            // user sign-off page SQX_PersonnelDocTraining_User_SignOff using SQX_Extension_PDT_User_SignOff
            Test.setCurrentPage(Page.SQX_PersonnelDocTraining_User_SignOff);
            List<SQX_Personnel_Document_Training__c> dtList = new List<SQX_Personnel_Document_Training__c>{ p3dt1 };
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(dtList);
            controller.setSelected(dtList);
            SQX_Extension_PDT_User_SignOff ext = new SQX_Extension_PDT_User_SignOff(controller);
            ext.CQ_Electronic_Signature_Enabled = false;
            
            // ACT: user sign-off by user1 on training record of p3
            ext.signingOffComment = 'user sign off';
            ext.signOff();
            
            System.assertEquals(true, SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), expectedUserSignOffErrMsg), 'Expected error message "' + expectedUserSignOffErrMsg + '"');
            
            // ensure internally changed fields values are not shown to users when error occurs
            System.assertEquals(null, p3dt1.SQX_User_Signed_Off_By__c, 'Expected value not to be set when error occurred.');
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p3dt1.Status__c, 'Expected value not to be set when error occurred.');
            
            // verify database data are not modified
            p3dt1 = [SELECT Id, Status__c, SQX_User_Signed_Off_By__c FROM SQX_Personnel_Document_Training__c WHERE Id = :p3dt1.Id];
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p3dt1.Status__c);
            System.assertEquals(null, p3dt1.SQX_User_Signed_Off_By__c);
            
            
            // ACT: sign off as trainer by user1 i.e. personnel user on own training record
            p1dt1.SQX_Training_Approved_By__c = user1.Id;
            res1 = Database.update(p1dt1, false);
            
            String expectedTrainerSignOffErrMsg = 'Trainer Sign-Off cannot be done by the personnel user.'; // error message of validation rule Prevent_Trainer_SignOff_By_PersonnelUser
            System.assertEquals(false, res1.isSuccess());
            System.assertEquals(true, SQX_Utilities.checkErrorMessage(res1.getErrors(), expectedTrainerSignOffErrMsg), 'Expected error message "' + expectedTrainerSignOffErrMsg + '"');
            
            
            // ACT: sign off as trainer to complete migrated record
            p2dt2.SQX_Training_Approved_By__c = user1.Id;
            res1 = Database.update(p2dt2, false);
            
            System.assertEquals(true, res1.isSuccess(), 'Migrated training approval pending training is expected to be completed upon proper trainer sign-off.');
        }
    }
    
}