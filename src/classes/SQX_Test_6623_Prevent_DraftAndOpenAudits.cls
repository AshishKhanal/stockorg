/**
 * test cases for to prevent draft and open audit when sf task completing
 */ 
@isTest
public class SQX_Test_6623_Prevent_DraftAndOpenAudits {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
    * GIVEN: NSI record with perform audit
    * WHEN: SF Task completing with draft audit
    * THEN: throws error message
    */ 
    public testmethod static void givenNSIWithPerformAudit_WhenCompleteSFTaskWithDraftAudit_ThenThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_NSI.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.save();
            // Arrange : NSI is created
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            
            SQX_Onboarding_Step__c wp1 = new SQX_Onboarding_Step__c();
            wp1.SQX_Parent__c =nsi.nsi.Id;
            wp1.Name = 'TestName1';
            wp1.Status__c = SQX_NSI.STATUS_DRAFT;
            wp1.Step__c =1;
            wp1.Due_Date__c = Date.today();
            wp1.SQX_User__c = adminUser.Id;
            wp1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, 'Perform_Audit');
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{});
            
            // submit nsi
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();
            
            // initiate nsi
            nsi.setStage(SQX_NSI.STAGE_IN_PROGRESS);
            nsi.setStatus(SQX_NSI.STATUS_OPEN);
            nsi.nsi.Current_Step__c = 1;
            nsi.save();
            
            //Act: Complete SF task with draft audit
            Task task = [SELECT Id, Status, Description  FROM Task WHERE Child_What_Id__c =: wp1.Id];

            wp1.SQX_Audit_Number__c = audit.audit.Id;
            wp1.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            wp1.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            wp1.Comment__c = 'Test_Comment';
            List<Database.SaveResult> taskResult =  new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c> {wp1}, new List<Schema.SObjectField>{});
            
            //Assert: Ensured that validation error message should be throws
            System.assertEquals(false, taskResult[0].isSuccess());
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(taskResult[0].getErrors(),'Unable to complete the Task as the Audit associated is not completed yet.'));
        }
    }
    
    /**
    * GIVEN: SD record with perform audit
    * WHEN: SF Task completing with draft audit
    * THEN: throws error message
    */ 
    public testmethod static void givenSDWithPerformAudit_WhenCompleteSFTaskWithDraftAudit_ThenThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.save();
            // Arrange : SD is created
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();
            
            SQX_Deviation_Process__c dp = new SQX_Deviation_Process__c();
            dp.SQX_Parent__c =sd.sd.Id;
            dp.Name = 'TestName1';
            dp.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            dp.Step__c =1;
            dp.Due_Date__c = Date.today();
            dp.SQX_User__c = adminUser.Id;
            dp.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, 'Perform_Audit');
            new SQX_DB().op_insert(new List<SQX_Deviation_Process__c> { dp },  new List<Schema.SObjectField>{});
            
            // submit sd
            sd.setStage(SQX_Supplier_Deviation.STAGE_TRIAGE);
            sd.save();
            
            // initiate sd
            sd.setStage(SQX_Supplier_Deviation.STAGE_IN_PROGRESS);
            sd.setStatus(SQX_Supplier_Deviation.STATUS_OPEN);
            sd.sd.Current_Step__c = 1;
            sd.save();
            
            //Act: Complete SF task with draft audit
            dp.SQX_Audit_Number__c = audit.audit.Id;
            dp.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
            dp.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            dp.Comment__c = 'Test_Comment';
            List<Database.SaveResult> taskResult =  new SQX_DB().continueOnError().op_update(new List<SQX_Deviation_Process__c> {dp}, new List<Schema.SObjectField>{});
            
            //Assert: Ensured that validation error message should be throws
            System.assertEquals(false, taskResult[0].isSuccess());
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(taskResult[0].getErrors(),'Unable to complete the Task as the Audit associated is not completed yet.'));
        }
    }
    
    /**
    * GIVEN: NSI record with perform audit
    * WHEN: add draft audit from steps
    * THEN: Should not throws error message
    */ 
    public testmethod static void givenNSIWithPerformAudit_WhenAddDraftAudit_ThenShouldNotThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.save();
            // Arrange : NSI is created
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            
            //Act: Add draft audit to policy
            SQX_Onboarding_Step__c wp1 = new SQX_Onboarding_Step__c();
            wp1.SQX_Parent__c =nsi.nsi.Id;
            wp1.SQX_Audit_Number__c = audit.audit.Id;
            wp1.Name = 'TestName1';
            wp1.Status__c = SQX_NSI.STATUS_DRAFT;
            wp1.Step__c =1;
            wp1.Due_Date__c = Date.today();
            wp1.SQX_User__c = adminUser.Id;
            wp1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, 'Perform_Audit');
            List<Database.SaveResult> policyResult =  new SQX_DB().continueOnError().op_insert(new List<SQX_Onboarding_Step__c> { wp1 },  new List<Schema.SObjectField>{});
            
            //Assert: Ensured that save successfully without error
            System.assertEquals(true, policyResult[0].isSuccess());
        }
    }
    
    /**
    * GIVEN: SD record with perform audit
    * WHEN: add draft audit from steps
    * THEN: Should not throws error message
    */ 
    public testmethod static void givenSDWithPerformAudit_WhenAddDraftAudit_ThenShouldNotThrowErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.save();
            // Arrange : SD is created
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();
            
            //Act: Add draft audit to policy
            SQX_Deviation_Process__c dp = new SQX_Deviation_Process__c();
            dp.SQX_Parent__c =sd.sd.Id;
            dp.SQX_Audit_Number__c = audit.audit.Id;
            dp.Name = 'TestName1';
            dp.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            dp.Step__c =1;
            dp.Due_Date__c = Date.today();
            dp.SQX_User__c = adminUser.Id;
            dp.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, 'Perform_Audit');
            List<Database.SaveResult> policyResult =  new SQX_DB().continueOnError().op_insert(new List<SQX_Deviation_Process__c> { dp },  new List<Schema.SObjectField>{});
            
            //Assert: Ensured that save successfully without error
            System.assertEquals(true, policyResult[0].isSuccess());
        }
    }
}