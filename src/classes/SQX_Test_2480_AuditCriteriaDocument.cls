/**
* This test ensures that audit criteria document can be created and revised
*/
@IsTest
public class SQX_Test_2480_AuditCriteriaDocument {


    static final String USER_STD_1 = '2480-StdUser-1',
                        USER_ADMIN_1 = '2480-Admin-1',
                        TEMPLATE_DOC_NUMBER = 'Test-2480-Template-Doc';


    @testSetup
    static void setupTest(){
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User stdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, USER_STD_1);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, USER_ADMIN_1);

        // Create template document to be used
        System.runas(stdUser){    
            SQX_Test_Controlled_Document templateDoc = new SQX_Test_Controlled_Document();
            templateDoc.doc.Document_Number__c = TEMPLATE_DOC_NUMBER;
            templateDoc.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            templateDoc.save(true);       
        }
    }

    /**
    * This test checks if controlled document is created, primary content check in method is set correctly
    * 
    * Following combinations will be tested
    * a. Controlled Document with file content uploaded (< 6.1.0) has Manual
    * b. Doc with has large file checked (< 6.1.0) has Default
    * c. Doc with primary content reference set (>= 6.1.0) has Manual
    * d. Doc with template content reference set has Template
    * e. Doc with no content for types supporting default content doc has Default
    * f. Doc with no content for types not supporting default content gives error
    */
    static testmethod void givenDocumentIsCreated_PrimaryContentCheckInSetCorrectly(){

        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create template document to use as parent and template doc
            SQX_Controlled_Document__c templateDoc = [SELECT Id, Name, Primary_Content_Check_In_Method__c FROM SQX_Controlled_Document__c WHERE Document_Number__c = : TEMPLATE_DOC_NUMBER];

            // Act: Create controlled document with content uploaded using file uploader (MANUAL)
            SQX_Controlled_Document_Builder builder = new SQX_Controlled_Document_Builder(new SQX_Test_Controlled_Document().doc);
            builder.primaryContent = new ContentVersion();
            builder.prepareContent();

            // Assert: Ensure that Manual was set for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, builder.doc.Primary_Content_Check_In_Method__c);    
        
        
            // Act: Create Controlled Document with content has large file (DEFAULT)
            builder = new SQX_Controlled_Document_Builder(new SQX_Test_Controlled_Document().doc);
            builder.hasLargeFile = true;
            builder.prepareContent();

            // Assert: Ensure that Default was set for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_DEFAULT, builder.doc.Primary_Content_Check_In_Method__c);    



            // Arrange: Create controlled document with primary content
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.updateContent(true, 'Primary.txt');
            
            // Act: Create controlled document with previously uploaded content (MANUAL)
            builder = new SQX_Controlled_Document_Builder(doc.doc);
            builder.prepareContent();
        
            // Assert: Ensure that Manual was set for content check in  mehtod
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, builder.doc.Primary_Content_Check_In_Method__c);    
        
            // Act: Create controlled document with revised doc
            SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document().save();
            builder = new SQX_Controlled_Document_Builder(doc1.doc).revise('Moc Revison','Moc Changes','Moc Scope',doc1.doc.Id,doc1.doc.SQX_Change_Order__c, null);
            builder.parentDoc = templateDoc;
            builder.prepareContent();

            // Assert: Ensure that Previous Revision was set for for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_PREV_REV, builder.doc.Primary_Content_Check_In_Method__c);


            // Act: Create controlled document with revised doc with content provided
            builder = new SQX_Controlled_Document_Builder(new SQX_Test_Controlled_Document().doc);
            builder.parentDoc = templateDoc;
            builder.primaryContent = new ContentVersion();
            builder.prepareContent();

            // Assert: Ensure that Previous Revision was set for for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, builder.doc.Primary_Content_Check_In_Method__c);



            // Act: Create controlled document with template doc
            builder = new SQX_Controlled_Document_Builder(new SQX_Test_Controlled_Document().doc);
            builder.doc.Document_Type__c = templateDoc.Id;
            builder.prepareContent();

            // Assert: Ensure that Template was set for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_TEMPLATE, builder.doc.Primary_Content_Check_In_Method__c);



            // Act: Create controlled document with template doc but content provided
            builder = new SQX_Controlled_Document_Builder(new SQX_Test_Controlled_Document().doc);
            builder.doc.Document_Type__c = templateDoc.Id;
            builder.primaryContent = new ContentVersion();
            builder.prepareContent();

            // Assert: Ensure that Template was set for content check in method
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, builder.doc.Primary_Content_Check_In_Method__c);


        
            // Act: Create controlled document with template [IT]
            SQX_Test_Controlled_Document newDoc = new SQX_Test_Controlled_Document();
            newDoc.doc.Document_Type__c = templateDoc.Id;
            newDoc.save(false);

            // Assert: Ensure that content type is set to Template
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_TEMPLATE, [SELECT Primary_Content_Check_In_Method__c 
                                FROM SQX_Controlled_Document__c WHERE Id = : newDoc.doc.Id].Primary_Content_Check_In_Method__c);

        }
    }


    /**
    * This test ensures that if default/template content is used, upon upload content check in method is set properly
    */
    static testmethod void givenDocWithDefaultContent_WhenNewContentIsCheckedIn_CheckInChangesToManual(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document that uses template document 
            SQX_Controlled_Document__c templateDoc = [SELECT Id, Name FROM SQX_Controlled_Document__c WHERE Document_Number__c = : TEMPLATE_DOC_NUMBER];
            SQX_Test_Controlled_Document newDoc = new SQX_Test_Controlled_Document();
            newDoc.doc.Document_Type__c = templateDoc.Id;
            newDoc.save();

            SQX_Controlled_Document__c doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : newDoc.doc.Id ];


            // Act: Upload new version of the content for the document
            ContentVersion newVersion = new ContentVersion(VersionData = Blob.valueOf('Hello World'), PathOnClient = 'New.txt');
            newVersion.ContentDocumentId = doc.Content_Reference__c;
            insert newVersion;

            // Assert: Ensure that template document's check in status is manual
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, [SELECT Primary_Content_Check_In_Method__c 
                                FROM SQX_Controlled_Document__c WHERE Id = : newDoc.doc.Id].Primary_Content_Check_In_Method__c);


            // Arrange: Create document that supports default document (Audit Criteria)
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document();
            auditCriteria.doc.RecordTypeId =  new SQX_Extension_Controlled_Document(new ApexPages.StandardController(auditCriteria.doc)).getRecordTypes().get(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save(true);

            doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : auditCriteria.doc.Id ];


            // Act: Upload new version of the content for the document
            newVersion = new ContentVersion(VersionData = Blob.valueOf('Hello World'), PathOnClient = 'New.txt');
            newVersion.ContentDocumentId = doc.Content_Reference__c;
            insert newVersion;

            // Assert: Ensure that template document's check in status is manual
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL, [SELECT Primary_Content_Check_In_Method__c 
                                FROM SQX_Controlled_Document__c WHERE Id = : auditCriteria.doc.Id].Primary_Content_Check_In_Method__c);


        }
    }


    /**
    * This test checks that if audit criteria document is revised with default content, default content is changed but
    * else manual content is used.
    */
    static testmethod void givenDocWithDefaultContent_WhenRevised_NewDefaultContentIsGenerated(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {
            // Arrange: Create document with Default content uses template document 
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document();
            auditCriteria.doc.RecordTypeId =  new SQX_Extension_Controlled_Document(new ApexPages.StandardController(auditCriteria.doc)).getRecordTypes().get(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            ApexPages.StandardController controller = new ApexPages.StandardController(auditCriteria.doc);
            SQX_Extension_Controlled_Document docExt = new SQX_Extension_Controlled_Document(controller);
            docExt.Create();

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            auditCriteria.save();

            auditCriteria.synchronize();
            
            // Act: Revise the document
            SQX_Test_Controlled_Document rev1 = auditCriteria.revise('New Scope', false, false);

            // Assert: Ensure that revised document has default content and uses new content
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_DEFAULT, [SELECT Primary_Content_Check_In_Method__c 
                                FROM SQX_Controlled_Document__c WHERE Id = : rev1.doc.Id].Primary_Content_Check_In_Method__c);


            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            rev1.doc.Primary_Content_Check_In_Method__c = SQX_Controlled_Document.CONTENT_CHECKIN_MANUAL;
            rev1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            rev1.save();

            Test.startTest();
            
            // Act: Revise the document
            SQX_Test_Controlled_Document rev2 = rev1.revise('New Scope', false, false);

            // Assert: Ensure that revised document has previous revision
            System.assertEquals(SQX_Controlled_Document.CONTENT_CHECKIN_PREV_REV, [SELECT Primary_Content_Check_In_Method__c 
                                FROM SQX_Controlled_Document__c WHERE Id = : rev2.doc.Id].Primary_Content_Check_In_Method__c);
            
            Test.stopTest();
        }
    }

    /**
    * Given: Creat an audit criteria document with document criteria reqirements
    * When:document is revised 
    * Then: The revised document has all the parent's document criteria requirements
    *
    * @date:2016/9/8
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2461]
    */
    public static testmethod void givenDocumentWithCriteriaRequirements_WhenDocumentIsRevised_AllCriteriaRequirementsAreCoppied(){
         System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document with criteria requirements 
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save(true);

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            auditCriteria.save();
            
            List<SQX_Result_Type__c> result_Types = SQX_Test_Utilities.setupResultTypes(1, 0);
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirements = auditCriteria.addCriterionRequirements(2, result_Types[0].Id);
            
            // Act: Revise the document
            SQX_Test_Controlled_Document rev1 = auditCriteria.revise('New Scope', false, false);

            // Assert: Ensure that revised document has all the criteria requirements
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirement = [SELECT Id  FROM SQX_Doc_Criterion_Requirement__c WHERE SQX_Controlled_Document__c = : rev1.doc.Id];
            System.assertEquals(2,docCriteriaRequirement.size());
         }
    }

    /**
    * Given: Creat an audit criteria document with document criteria reqirements
    * When:document is cloned 
    * Then: The cloned document has all the datas of parent document along with criteria requirements except mentioned in blacklist
    *
    * @date:2016/9/8
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2461]
    */
    public static testmethod void givenDocumentWithCriteriaRequirements_WhenDocumentIsCloned_AllCriteriaRequirementsAreCoppiedWithOtherData(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document with criteria requirements 
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save(true);
            List<SQX_Result_Type__c> result_Types = SQX_Test_Utilities.setupResultTypes(1, 0);
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirements = auditCriteria.addCriterionRequirements(2, result_Types[0].Id);
            
            // Act: Revise the document
            SQX_Test_Controlled_Document rev1 = auditCriteria.cloneDocument('Moc Document Number','Moc Revison','Moc Title','New Scope', false);

            //Assert : Ensure that the Document number , revison and title are saved
            System.assertEquals('Moc Document Number', rev1.doc.Document_Number__c);
            System.assertEquals('Moc Revison', rev1.doc.Revision__c);
            System.assertEquals('Moc Title', rev1.doc.Title__c);

            // Assert: Ensure that revised document has all the criteria requirements
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirement = [SELECT Id  FROM SQX_Doc_Criterion_Requirement__c WHERE SQX_Controlled_Document__c = : rev1.doc.Id];
            System.assertEquals(2,docCriteriaRequirement.size());
        }
    }


    /**
    * This test ensures that a training record can be added to criteria document. (Regression Test)
    * @story: SQX-2574
    */
    static testmethod void givenCriteriaDocument_WhenTrainingIsAdded_NoErrorIsThrown(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create Pre-released Audit Criteria document with criteria requirements and Create a Personnel
            SQX_Test_Controlled_Document auditCriteria = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteria.save(false);
            List<SQX_Result_Type__c> result_Types = SQX_Test_Utilities.setupResultTypes(1, 0);
            List<SQX_Doc_Criterion_Requirement__c> docCriteriaRequirements = auditCriteria.addCriterionRequirements(2, result_Types[0].Id);
            

            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            auditCriteria.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            // Act: Add a document training for the personnel and criteria document
            SQX_Personnel_Document_Training__c dtn1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = auditCriteria.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today(),
                Optional__c = false
            );

            Database.SaveResult sr = Database.insert(dtn1, false);

            //Assert : Ensure that the training can be saved for criteria doc
            System.assert(sr.isSuccess(), 'Expected training to be added but failed with ' + sr);
        }
    }

    /**
    * Given: Creat an audit criteria document without any libraries.
    * When: document is saved 
    * Then: ContentDocumentLink of primary content is added to the document
    *
    * @date:2016/9/21
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2369]
    */
    public static testmethod void givenDocumentWithoutLibraries_WhenDocumentIsSaved_ContentDocumentLinkOfPrimaryContentIsAddedToDocument(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document with primary content
            SQX_Test_Controlled_Document auditCriteriaDocument = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
            auditCriteriaDocument.updateContent(true, 'Primary.txt');
            auditCriteriaDocument.save();
            SQX_Controlled_Document__c doc = [SELECT Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: auditCriteriaDocument.doc.Id];
            
            // Assert : ContentDocumentLink of primary content is added for this doc
            List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: auditCriteriaDocument.doc.Id];
            System.assertEquals(doc.Content_Reference__c,contentDocumentLinks[0].ContentDocumentId,'Expected the contentDocumentLinks to be added to doc');
        }
    }

    /**
    * Given: Creat an audit criteria document without any libraries.
    * When: document is saved 
    * Then: ContentDocumentLink of secondary content is added to the document
    *
    * @date:2016/9/21
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2369]
    */
    public static testmethod void givenDocumentWithoutLibraries_WhenDocumentIsSaved_ContentDocumentLinkOfSecondaryContentIsAddedToDocument(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document with default content
            SQX_Test_Controlled_Document auditCriteriaDocument = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);

            // add the primary and secondary content
            auditCriteriaDocument.updateContent(true, 'Primary.txt');
            auditCriteriaDocument.updateContent(false,'Secondary.text');
            auditCriteriaDocument.save();

            SQX_Controlled_Document__c doc = [SELECT Content_Reference__c, Secondary_Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: auditCriteriaDocument.doc.Id];
            
            // Assert : ContentDocumentLink of primary as well as secondary is added for this doc
            List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: auditCriteriaDocument.doc.Id];
            
            // Assert : contentDocumentLinks list size is 2
            System.assertEquals(2,contentDocumentLinks.size(),'Expected the contentDocumentLinks list size to be 2');
            
            // Assert : ContentDocumentLink of primary content is added for this doc
            System.assertEquals(doc.Content_Reference__c,contentDocumentLinks[0].ContentDocumentId,'Expected the primary contentDocumentLinks to be added to doc');
            
            // Assert : ContentDocumentLink of secondary content is added for this doc
            System.assertEquals(doc.Secondary_Content_Reference__c,contentDocumentLinks[1].ContentDocumentId,'Expected the secondary contentDocumentLinks to be added to doc');
        }
    }

    /**
    * Given: Creat an audit criteria document without any libraries.
    * When: Document is gone to current
    * Then: ContentDocumentLink of secondary content is added to the document
    *
    * @date:2016/12/23
    * @author:Paras Kumar Bishwakarma
    * @story:[SQX-2903]
    */
    public static testmethod void givenDocumentWithoutLibraries_WhenDocumentIsCurrent_ContentDocumentLinkOfSecondaryContentIsAddedToDocument(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(USER_STD_1)) {

            // Arrange: Create document with default content
            SQX_Test_Controlled_Document auditCriteriaDocument = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);

            //Act : add the primary and secondary content
            auditCriteriaDocument.updateContent(true, 'Primary.txt');
            auditCriteriaDocument.updateContent(false,'Secondary.text');
            auditCriteriaDocument.save();

            SQX_Controlled_Document__c doc = [SELECT Content_Reference__c, Secondary_Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: auditCriteriaDocument.doc.Id];
            
            // Assert : ContentDocumentLink of primary as well as secondary is added for this doc
            List<ContentDocumentLink> contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: auditCriteriaDocument.doc.Id];
            
            // Assert : contentDocumentLinks list size is 2
            System.assertEquals(2,contentDocumentLinks.size(),'Expected the contentDocumentLinks list size to be 2');
            
            // Assert : ContentDocumentLink of primary content is added for this doc
            System.assertEquals(doc.Content_Reference__c,contentDocumentLinks[0].ContentDocumentId,'Expected the primary contentDocumentLinks to be added to doc');
            
            // Assert : ContentDocumentLink of secondary content is added for this doc
            System.assertEquals(doc.Secondary_Content_Reference__c,contentDocumentLinks[1].ContentDocumentId,'Expected the secondary contentDocumentLinks to be added to doc');

            // Act : Make the status of doc to current
            auditCriteriaDocument.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            auditCriteriaDocument.save();

            //Assert : Docuement status should be current
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, auditCriteriaDocument.doc.Document_Status__c, 'Expected doc to be current but is'+auditCriteriaDocument.doc.Document_Status__c);

            // Assert : ContentDocumentLink of secondary is added for this current doc
            doc = [SELECT Secondary_Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id =: auditCriteriaDocument.doc.Id];
            contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: auditCriteriaDocument.doc.Id AND ContentDocumentId =: doc.Secondary_Content_Reference__c ];
            System.assertEquals(1, contentDocumentLinks.size());
        }
    }
}