/**
 * This class ensures that temp store attachments are persisted properly and very large attachments can be persisted as well
 * @issue SQX-600
 * @date 2014/8/26
 */
@isTest
private class SQX_Test_630_AttachmentAndStoreCheck {


    private static boolean  runAllTests = true,
                            run_givenAnAttachment_ItIsReparented = false,
                            run_givenAnAttachmentOfNonPersisted_ItIsReparented = false,
                            run_givenTempStoreHasManyAttachment_OnlyValidOnesAreReparented = false;

    /**
    * this test checks that a temporary attachment for a persisted record(Running Account in this case)
    * is saved properly
    */
    static testmethod void givenAnAttachment_ItIsReparented(){
        
        if(!runAllTests && ! run_givenAnAttachment_ItIsReparented){
            return;
        }
        
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
        	Account supplier = SQX_Test_Account_Factory.createAccount();
        	
            SQX_TempStore__c store = SQX_TempStore.storeObject('User', supplier.Id, 'Test object', '', '');
            Attachment att = new Attachment();
            att.Body = Blob.valueOf('This is an test attachment');
            att.Name = 'Test.txt';
            
            SQX_TempStore.storeTemporaryAttachment(supplier.Id, supplier.Id, att);
            
            Id attachmentId = att.Id;
            att.Id = null;
            att.ParentId = supplier.Id;
            att.Id = attachmentId;
            
            String changeSet = '{"changeSet": [' + JSON.serialize(att) + ']}';
            
            SQX_Upserter.UpsertRecords(changeSet);
            
            List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : supplier.Id];
            
            System.assert(attachments.size() > 0 && attachments.get(0).Name == att.Name, 'Expected attachment to be reparented but found ' + attachments );
            
            // ACT (SQX-8211): update SQX_Temp_Store_Attachment__c
            update [SELECT Id FROM SQX_Temp_Store_Attachment__c LIMIT 1];
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Temp_Store_Attachment__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Temp_Store_Attachment__c.SObjectType);
        }
    }
    
    
    /**
    * this test checks that a temporary attachment for a record that has yet to persisted in the same transaction
    * it is saved properly
    */
    static testmethod void givenAnAttachmentOfNonPersisted_ItIsReparented(){
        
        if(!runAllTests && ! run_givenAnAttachmentOfNonPersisted_ItIsReparented){
            return;
        }
        
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
        	Contact contact = new Contact();
    		contact.LastName = 'con' + Math.random();
        	
            SQX_TempStore__c store = SQX_TempStore.storeObject('User', newUser.Id, 'Test object', '', '');
            Attachment att = new Attachment();
            att.Body = Blob.valueOf('This is an test attachment');
            att.Name = 'Test.txt';
            
            SQX_TempStore.storeTemporaryAttachment(newUser.Id, 'SUPPLIER-1', att);
            
            Map<String, Object> contactJSON = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(contact));
            contactJSON.put('Id', 'CONTACT-1');
            
            Map<String, Object> attJSON = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(att));
            attJSON.put('ParentId', 'CONTACT-1'); //JSON has the parent id is set to appropriate parent, even though actual parent Id is temp store attachment
            
            List<Object> objects = new List<Object>();
            objects.add(attJSON);
            objects.add(contactJSON);
            
            Map<String,Object> changeSetJSON = new Map<String, Object>();
            changeSetJSON.put('changeSet', objects);
            
            String changeSet =  JSON.serialize(changeSetJSON);
            
            SQX_Upserter.UpsertRecords(changeSet);
            
            //get the account object
            Contact contactS = [SELECT Id FROM Contact WHERE LastName = : contact.LastName];
            List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : contactS.Id];
            
            System.assert(attachments.size() > 0 && attachments.get(0).Name == att.Name,  'Expected attachment to be reparented but found ' + attachments );
        }
    }
    
    /**
    * this test ensures that when multiple attachments are present in temp store, which is the case when new CAPA is being created
    * only the attachments that are sent along with change set are persisted.
    */
    static testmethod void givenTempStoreHasManyAttachment_OnlyValidOnesAreReparented(){
        
        if(!runAllTests && ! run_givenTempStoreHasManyAttachment_OnlyValidOnesAreReparented){
            return;
        }
        
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
        	Account supplier = SQX_Test_Account_Factory.createAccount();
        	
            SQX_TempStore__c store = SQX_TempStore.storeObject('User', supplier.Id, 'Test object', '', '');
            Attachment att = new Attachment();
            att.Body = Blob.valueOf('This is an test attachment');
            att.Name = 'Test.txt';
            
            SQX_TempStore.storeTemporaryAttachment(supplier.Id, supplier.Id, att);
            
            Attachment att2 = new Attachment();
            att2.Body = Blob.valueOf('This attachment must not be persisted');
            att2.Name = 'Invalid.txt';
            
            SQX_TempStore.storeTemporaryAttachment(supplier.Id, supplier.Id, att2);
                        
            Id attachmentId = att.Id;
            att.Id = null;
            att.ParentId = supplier.Id;
            att.Id = attachmentId;
            
            String changeSet = '{"changeSet": [' + JSON.serialize(att) + ']}';
            
            SQX_Upserter.UpsertRecords(changeSet);
            
            List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : supplier.Id AND Name = : att2.Name];
            
            System.assert(attachments.size() == 0 , 'Expected invalid attachment to not be reparented');
        }
    }
}