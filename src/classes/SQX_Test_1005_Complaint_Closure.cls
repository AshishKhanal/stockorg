/**
 * test class to ensure that the complaint closure is working as per the requirement in the given story
 * @author: Anish Shrestha 
 * @date: 2015-12-11
 * @story: [SQX-1005] 
 */
@isTest
public class SQX_Test_1005_Complaint_Closure{

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Defect_Code__c complaintConclusion = null;
        System.runAs(adminUser){
            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                         Active__c = true,
                                                         Defect_Category__C = 'Test_Category',
                                                         Description__c = 'Test Description',
                                                         Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }        
    }

    static boolean runAllTests = true,
                    run_givenAUser_ComplaintReviewerIsApproverOfComplaint = false,
                    run_givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewRequired = false,
                    run_givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewNotRequired = false,
                    run_givenAClosedComplaint_UserCanReopenAndCloseTheComplaint = false,
                    run_givenAClosedComplaint_UserCanReopenAndVoidTheComplaint = false,
                    run_givenAComplaint_When_PolicyDefined_ComplaintApprovalFollowsEsigPolicy = false,
                    run_givenAComplaint_When_PolicyNotDefined_ComplaintApprovalFollowsDefaultEsigPolicy = false,
                    run_givenAComplaint_WhenClosureReviewIsNotRequired_ClosureReviewerShouldNotBeEntered = false,
                    run_givenAComplaintWithDecisionTree_WhenComplaintIsClosedWithPendingRegulatoryReport_ErrorMessageIsThrown = false;

    /**
     * Action: Complaint closure is approved by user 
     * Expected: The "Closure reviewer" field is set to the person performing the approval.
     * @date: 2015-12-11
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAUser_ComplaintReviewerIsApproverOfComplaint(){

        if(!runAllTests && !run_givenAUser_ComplaintReviewerIsApproverOfComplaint){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

            SQX_Test_Complaint complaint;
            SQX_Complaint__c complaintMain;

        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in current user');

            //Act 3: close the complaint providing closure comment and making complaint closure required
            complaintMain = [SELECT Status__c, SQX_Finding__c, Closure_Review_By__c, Require_Closure_Review__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;

            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Closure_Review_By__c, SQX_Complaint__c.fields.Require_Closure_Review__c});

            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';

            update new List<SObject>{finding};
            
            // Approve the complaint
            complaintMain = [SELECT Status__c, Closure_Review_By__c, Is_Approved__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Status__c = SQX_Complaint.STATUS_CLOSED;
            complaintMain.Record_Stage__c = SQX_Complaint.STAGE_CLOSED;
            complaintMain.Is_Approved__c = true;
            complaintMain.OwnerId = standardUserApprover.Id;
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;

            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Status__c, 
                                                                                            SQX_Complaint__c.fields.OwnerId, 
                                                                                            SQX_Complaint__c.fields.Is_Approved__c});

            complaintMain = [SELECT Status__c, Closure_Review_By__c, Require_Closure_Review__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 4: Expected the user who approves the complaint closure is the closure reviewer.
            System.assertEquals(complaintMain.Closure_Review_By__c, standardUser.Id, 'Expected the closure reviewer to be user who approved the record with user id:' + standardUser.Id + ' but found: ' + complaintMain.Closure_Review_By__c);
        }
    }
    /**
     * Action: Complaint is send for closure review and approved by the reviewer
     * Expected: The close date is set to the date when the complaint is closed and sent for closure review 
     *           and close by is set to user who has closed the complaint
     * @date: 2015-12-22
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewRequired(){

        if(!runAllTests && !run_givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewRequired){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, SQX_Finding__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Close the complaint
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';

            update new List<SObject>{finding};

            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                            SQX_Complaint__c.fields.OwnerId});
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Record_Stage__c, Status__c, Closed_By__c, Close_Date__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the stage of the complaint to be in STAGE_CLOSURE_REVIEW
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REVIEW , 'Expected the stage to be in in closure review but found: ' + complaintMain.Record_Stage__c);
            System.assertEquals(complaintMain.Closed_By__c, standardUser.Id, 'Expected the user who send for closure review to be set');
            System.assertEquals(complaintMain.Close_Date__c,  Date.Today(), 'Expected the date to be set to the date send for closure review');

        }
        System.runas(standardUserApprover){
            //Act 4: Approve the closure review
            Map<String, String> paramsApproveApproval = new Map<String, String>();
            paramsApproveApproval.put('nextAction', 'approveapproval');
            paramsApproveApproval.put('workItemId', complaintMain.Id); // This ID is not needed for test, so sent a dummy Id

             Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsApproveApproval, null);
            complaintMain = [SELECT Id, Status__c, Closed_By__c, Close_Date__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be closed but found: ' + complaintMain.Status__c);
            System.assertEquals(complaintMain.Closed_By__c, standardUser.Id, 'Expected the user who send for closure review to be set');
            System.assertEquals(complaintMain.Close_Date__c,  Date.Today(), 'Expected the date to be set to the date send for closure review');
        }
    }
    /**
     * Action: Complaint is closed without sending for closure review
     * Expected: The close date is set to the date when the complaint is closed
     *           and close by is set to user who has closed the complaint
     * @date: 2015-12-22
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewNotRequired(){

        if(!runAllTests && !run_givenAUser_CloseByAndCloseDateIsSetToCurrentUserWhenComplaintClosedWithClosureReviewNotRequired){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, SQX_Finding__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in queue');

            //Act 3: Close the complaint
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.SQX_Conclusion_Code__c,
                                                                                            SQX_Complaint__c.fields.Resolution__c});

            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';

            update new List<SObject>{finding };

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c, Closed_By__c, Close_Date__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be in in closed but found: ' + complaintMain.Status__c);
            System.assertEquals(complaintMain.Closed_By__c, standardUser.Id, 'Expected the user who send for closure review to be set');
            System.assertEquals(complaintMain.Close_Date__c,  Date.Today(), 'Expected the date to be set to the date send for closure review');

        }
    }
    /**
     * Action: Closed Complaint is reopened and closed again.
     * Expected: Closed complaint can be reopened and closed again.
     * @date: 2016-01-12
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAClosedComplaint_UserCanReopenAndCloseTheComplaint(){

        if(!runAllTests && !run_givenAClosedComplaint_UserCanReopenAndCloseTheComplaint){
            return;
        }
        //Arrange: Create a complaint and close the complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){

            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaint.complaint.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaintMain.Activity_Code__c = 'close';
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                            SQX_Complaint__c.fields.OwnerId});
            
            complaintMain = [SELECT Id, Record_Stage__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REVIEW , 'Expected the stage to be in in closure review but found: ' + complaintMain.Record_Stage__c);
        }
        System.runas(standardUserApprover){
            Map<String, String> paramsApproveApproval = new Map<String, String>();
            paramsApproveApproval.put('nextAction', 'approveapproval');
            paramsApproveApproval.put('workItemId', complaintMain.Id); // This ID is not needed for test, so sent a dummy Id
            
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsApproveApproval, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be closed but found: ' + complaintMain.Status__c);
        }

        System.runas(standardUser){
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act 1: Reopen complaint
            Map<String, String> paramsReopen = new Map<String, String>();
            paramsReopen.put('nextAction', 'reopen');
            paramsReopen.put('comment', 'Reopening complaint');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsReopen, null);

            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 1: Expected the complaint to be open
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_DRAFT , 'Expected the status to be draft but found: ' + complaintMain.Status__c);

            complaintMain.Require_Closure_Review__c = false;
            complaintMain.Closure_Review_By__c = null;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c, SQX_Complaint__c.fields.Closure_Review_By__c});

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act 2: Close the complaint
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 2: Expected the status of the complaint to be in STATUS_CLOSED
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_CLOSED , 'Expected the status to be in in closed but found: ' + complaintMain.Status__c);

        }
    }
    
    /**
     * Scenario 1:
     * Action: Closure reviewer is selected when closure review is not required
     * Expected: Should throw an error that closure reviewer cannot be entered when closure review is not required
     *
     * Scenario 2:
     * Action: Closure reviewer is selected when closure review is required
     * Expected: Should be able to close complaint
     * @date: 2016-01-14
     * @author: Anish Shrestha 
     */
    public testmethod static void givenAComplaint_WhenClosureReviewIsNotRequired_ClosureReviewerShouldNotBeEntered(){

        if(!runAllTests && !run_givenAComplaint_WhenClosureReviewIsNotRequired_ClosureReviewerShouldNotBeEntered){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

            SQX_Test_Complaint complaint;
            SQX_Complaint__c complaintMain;

        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, SQX_Finding__c, Closure_Review_By__c, Require_Closure_Review__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaintMain.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});

            //Act 1: Close the complaint by making closure review nt required and entering closure reviewer
            complaintMain.Require_Closure_Review__c = false;
            complaintMain.Status__c = SQX_Complaint.STATUS_CLOSED;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;
            complaintMain.SQX_Conclusion_Code__c = [SELECT Id FROM SQX_Defect_Code__c LIMIT 1].Id;
            complaintMain.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;

            // Assert 1: Should not be able to close complaint and must throw error
            try{
                new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c, 
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c, 
                                                                                            SQX_Complaint__c.fields.Status__c});
                System.assert(false, 'Closure successful which should not be.');
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Closure Reviewer cannot be selected if closure review is not required.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 

            //Act 2: Close the complaint by making closure review  required and entering closure reviewer
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.Status__c = SQX_Complaint.STATUS_CLOSED;
            complaintMain.Closure_Review_By__c = standardUserApprover.Id;

            //Assert 2: Should be able to close complaint
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c, 
                                                                                        SQX_Complaint__c.fields.Closure_Review_By__c, 
                                                                                        SQX_Complaint__c.fields.Status__c});

            
        }
    }

    /**
     * Given: A complaint with decision tree, Decision Tree is ran and a pending task is created for Regulatory report
     * When: Complaint is sent for closure review without completing all the pending tasks
     * Then: Process is stopped with the error message 'Please complete all the pending tasks before closing Complaint.'
     * @date: 2017-07-07
     * @author: Sudeep Maharjan
     */
    public testmethod static void givenAComplaintWithDecisionTree_WhenComplaintIsClosedWithPendingRegulatoryReport_ErrorMessageIsThrown(){

        if(!runAllTests && !run_givenAComplaintWithDecisionTree_WhenComplaintIsClosedWithPendingRegulatoryReport_ErrorMessageIsThrown){
            return;
        }
    
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        SQX_Test_Task task1;

        System.runAs(adminUser){
            task1 = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE);
            task1.task.SQX_User__c = standardUser.Id;
            task1.save();
        }

        System.runAs(standardUser){

            //Arrange: Create a Complaint and set status as Open
            complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();

            //Act: Run Decision Tree
            List<SQX_Complaint_Task__c> complaintTasks = [Select Id, SQX_Complaint__c from SQX_Complaint_Task__c where SQX_Complaint__c = : complaint.complaint.Id and SQX_task__c =:task1.task.Id];
            Set<Id> childWhatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, Schema.SQX_Complaint_Task__c.Id);
            Set<Id> whatIds = new SQX_BulkifiedBase().getIdsForField(complaintTasks, Schema.SQX_Complaint_Task__c.SQX_Complaint__c);
            List<Task> sfDecisionTreeTasks = [Select Id, Status from Task where Child_What_Id__c IN: childWhatIds AND WhatId IN: whatIds AND Status != : SQX_Task.getClosedStatuses()];
            SQX_Decision_Tree__c task1Run1 = complaint.runDecisionTreeTask(task1.task);

            Task decisionTreeTask = [Select Status from Task where Id = :sfDecisionTreeTasks[0].Id];

            //Assert : related investigation SF task is completed
            System.AssertEquals(SQX_Task.STATUS_COMPLETED, decisionTreeTask.Status, 'Expected phr SF task to be completed on phr completion');

            //Act: Add a Regulatory Report
            SQX_Regulatory_Report__c regReport = complaint.addRegulatoryReport(task1Run1, SQX_Regulatory_Report.STATUS_PENDING);
            SQX_Regulatory_Report__c report = [SELECT Id, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : regReport.Id];

            //Assert: Single task is created for the report
            System.assertNotEquals(null, report.Task_Id__c, 'Expected task to be added');
            System.assertEquals(1, [SELECT Id FROM Task WHERE Id = : report.Task_Id__c].size(), 'Expected a single task to be created for the report');
            
            Test.startTest();

            //Act: Add Closure Comment                                            
            SQX_Finding__c finding = [SELECT Closure_Comments__c FROM SQX_Finding__c WHERE Id =: complaint.complaint.SQX_Finding__c];
            finding.Closure_Comments__c = 'Closing finding';
            new SQX_DB().op_update(new List<SObject>{finding}, new List<SObjectField>{SQX_Finding__c.fields.Closure_Comments__c});
            
            //Act : Close the complaint (set required closure review false)
            SQX_Complaint__c complaintMain1 = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain1.Require_Closure_Review__c = false;
            complaintMain1.OwnerId = standardUser.Id;
            complaintMain1.Closure_Review_By__c = null;
            new SQX_DB().op_update(new List<SObject>{complaintMain1}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                SQX_Complaint__c.fields.Closure_Review_By__c,
                SQX_Complaint__c.fields.OwnerId});
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Assert: Try closing Complaint.
            //        Error Message 'Please complete all the pending tasks before closing Complaint.' is expected
            try{
                Map<String, String> paramsClose1 = new Map<String, String>();
                paramsClose1.put('nextAction', 'Close');
                
                Id result1= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose1, null);
            }
            catch(Exception ex){
                System.assertEquals(ex.getMessage(), Label.SQX_ERR_MSG_WHEN_CLOSING_COMPLAINT_WITH_PENDING_TASKS, 'Error message is different');
            }

            //Act : Close the complaint by entering closure reviewer
            complaintMain = [SELECT Require_Closure_Review__c, Closure_Review_By__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            complaintMain.Require_Closure_Review__c = true;
            complaintMain.OwnerId = standardUser.Id;
            complaintMain.Closure_Review_By__c = adminUser.Id;
            new SQX_DB().op_update(new List<SObject>{complaintMain}, new List<SObjectField>{SQX_Complaint__c.fields.Require_Closure_Review__c,
                                                                                            SQX_Complaint__c.fields.Closure_Review_By__c,
                                                                                            SQX_Complaint__c.fields.OwnerId});
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Assert: Try closing Complaint.
            //        Error Message 'Please complete all the pending tasks before closing Complaint.' is expected
            try{
                Map<String, String> paramsClose = new Map<String, String>();
                paramsClose.put('nextAction', 'Close');

                Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);
            }
            catch(Exception ex){
                System.assertEquals(ex.getMessage(), Label.SQX_ERR_MSG_WHEN_CLOSING_COMPLAINT_WITH_PENDING_TASKS, 'Error message is different');
            }

            //Act: Complete the pending regulatory report task assigned to standard user 2 [e]
            SQX_Submission_History__c submissionHistory = complaint.addSubmissionHistory(null);
            regReport.Status__c = SQX_Regulatory_Report.STATUS_COMPLETE;
            regReport.SQX_Submission_History__c = submissionHistory.Id;

            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c> {regReport}, new List<SObjectField>{});

            //Assert: Ensure that the task associated with the report was closed
            regReport = [SELECT Id, Status__c, Task_Id__c FROM SQX_Regulatory_Report__c WHERE Id = : regReport.Id];
            Task regReportTask = [SELECT Id, Status FROM Task WHERE Id = : regReport.Task_Id__c];

            System.assertEquals(SQX_Regulatory_Report.STATUS_COMPLETE, regReport.Status__c);
            System.assertEquals('Completed', regReportTask.Status, 'Expected the task to be completed');

            //Act: Send Complaint for closure review
            Map<String, String> paramsClose = new Map<String, String>();
            paramsClose.put('nextAction', 'Close');
            
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsClose, null);

            complaintMain = [SELECT Id, Record_Stage__c, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            //Assert : Closure Review is sent for review
            System.assertEquals(complaintMain.Record_Stage__c, SQX_Complaint.STAGE_CLOSURE_REVIEW , 'Expected the stage to be in in closure review but found: ' + complaintMain.Record_Stage__c);
           
        }
    }
    
    /**
     * GIVEN : An complaint
     * WHEN : Complaint is void using activity code
     * THEN : Complaint Status is set as void and stage as blank
     */
    public testmethod static void givenComplaint_WhenVoid_StatusAndStageIsSet(){

        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        SQX_Test_Complaint complaint;
        SQX_Complaint__c complaintMain;
        System.runas(standardUser){

            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // ACT : Void complaint
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_VOID;
            complaint.complaint.Activity_Code__c = SQX_Complaint.ACTIVITY_CODE_VOID;
            complaint.save();

            SQX_Complaint__c voidComplaint = [SELECT Id, Status__c, Record_Stage__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id];

            // ASSERT : Complaint Status is set as void and stage as blank
            System.assertEquals(SQX_Complaint.RESOLUTION_VOID, voidComplaint.Status__c);
            System.assertEquals(null, voidComplaint.Record_Stage__c);
        }
    }
}