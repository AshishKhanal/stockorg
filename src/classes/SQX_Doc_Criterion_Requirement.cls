/*
 * class for handling triggers in Document Criterion Requirement
 * author: Sudeep Maharjan
 * date: 2016/09/12
 * story: SQX-2566
 **/
public class SQX_Doc_Criterion_Requirement {
    public with sharing class Bulkified extends SQX_BulkifiedBase{
    
        public Bulkified(){
        }
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newAuditChecklists equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Doc_Criterion_Requirement__c> newAuditChecklists, Map<Id,SQX_Doc_Criterion_Requirement__c> oldMap){
        
            super(newAuditChecklists, oldMap);
        }
        
        /**
        * When the audit criteria document is in current state, audit criteria requirement cannot be deleted
        * @author Sudeep Maharjan
        * @date 2016/08/10
        * @story [SQX-2566]
        */
        public Bulkified preventDeletionOfCriteriaRequirementDocumentWithCurrentStatus(){
            List<SQX_Doc_Criterion_Requirement__c> deletedDocCriteriaRequirements = (List<SQX_Doc_Criterion_Requirement__c>) this.resetView().view;
             for(SQX_Doc_Criterion_Requirement__c deletedDocCriteriaRequirement : deletedDocCriteriaRequirements){
                if(deletedDocCriteriaRequirement.Is_Parent_Locked__c){
                     deletedDocCriteriaRequirement.addError(Label.SQX_ERR_DELETING_DOC_CRITERIA_REQUIREMENT_WHEN_PARENT_IS_LOCKED);
                }
            }
            return this;
        }
    }
}