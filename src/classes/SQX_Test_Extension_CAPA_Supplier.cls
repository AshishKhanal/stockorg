/**
 * Test Class for extension capa supplier
 */
@IsTest
public class SQX_Test_Extension_CAPA_Supplier{

    final static String ADMIN_USER_1 = 'ADMIN_USER_1';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER_1);
    }

    // Mock parameter for response submission
    static final Map<String, String> params = new Map<String, String> {
                'comment' =>'Mock comment', 
                'purposeOfSignature' =>'Mock purposeOfSignature',  
                'nextAction' => 'submitresponse' };

    /**
    * ensures that information stored in change set/temp store can be retrieved
    */
    public testmethod static void givenACapaID_StoredChangesAreRetrieved(){

        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility().save();
            String deltaValue = 'I am delta';
            
            Database.SaveResult result1 = Database.insert(capa.CAPA,false);
            SQX_TempStore__c store = SQX_TempStore.storeObject('Capa',capa.Capa.Id, '', 'Hello World', deltaValue);

            String result = SQX_Extension_UI.getStoredChangeSetData(capa.capa.Id);

            System.assertEquals(result, deltaValue);

            System.assertNotEquals(deltaValue, SQX_Extension_UI.getStoredChangeSetData(UserInfo.getUserId())); //ensure different value is returned

            SQX_Extension_UI.discardChangeSet(capa.capa.Id);

            Test.setCurrentPage(Page.SQX_CAPA_Response);
            ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
            SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);

            result = controller.getChangeSetJSON();

            System.assertEquals('[]', result); //delta has been cleared
        }
        
        
    }
    
    /**
    * ensures that temp store object for capa can be shared
    */
    public testmethod static void givenACapaID_StoredChangesCanBeShared(){

        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility().save();
            String deltaValue = 'I am delta';

            Database.SaveResult result1 = Database.insert(capa.CAPA,false);
            SQX_TempStore__c store = SQX_TempStore.storeObject(SQX_Extension_UI.TEMPSTORE_TYPE_NAME ,capa.Capa.Id, '', 'Hello World', deltaValue);

            String accessKey = SQX_Extension_UI.shareChanges(capa.capa.Id , 'Changed Value');

            System.assertNotEquals('-NAK', accessKey); //access key is retrieved

            Test.setCurrentPage(Page.SQX_CAPA_Response);
            ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
            SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);

            ApexPages.currentPage().getParameters().put(SQX_Extension_UI.PARAM_ACCESS_KEY,  accessKey);

            String result = controller.getChangeSetJSON();

            System.assertEquals('Changed Value', result); //delta has been cleared
        }
        
        
    }
    
    
    /**
    * ensures that a complex response can be submitted and approved
    */
    public testmethod static void givenACapaIsPublished_ResponseCanBeAdded(){
        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        System.runas(adminUser){
            SQX_Test_CAPA_Utility capa=  new SQX_Test_CAPA_Utility(true, adminUser, adminUser);
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa.save();

            Test.setCurrentPage(Page.SQX_CAPA_Response);
            ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
            SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);

            controller.initializeTemporaryStorage();

            /* construct a response */
            SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.capa.Id,
                                                                          Response_Summary__c = 'Response');

            SQX_Containment__c containment = new SQX_Containment__c(SQX_CAPA__c = capa.capa.Id,
                                                                                     Containment_Summary__c = 'Containment',
                                                                                     Completion_Date__c = Date.today());

            SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_CAPA__c = capa.capa.Id,
                                                                         Investigation_Summary__c = 'Is');

            SQX_Response_Inclusion__c inclusion1 = new SQX_Response_Inclusion__c(Type__c = 'Containment');
            SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');

            SQX_Action_Plan__c actionPlan1 = new SQX_Action_Plan__c(Description__c = 'asd', Plan_Type__c = 'Preventive'
                                                , Due_Date__c = Date.Today().addDays(10)); 

            SQX_Action__c action1 = new SQX_Action__c(SQX_Capa__c = capa.Capa.Id, Description__c = 'asd', Plan_Type__c = 'Preventive',
                                                    Record_Action__c = 'New', Due_Date__c = Date.Today().addDays(10));

            Map<String,Object>  changeSet = new Map<String, Object>(),
                                responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                containmentUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(containment)),
                                investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                actionPlan1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(actionPlan1)),
                                action1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(action1)),
                                inclusion1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion1)),
                                inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped

            //set virtual IDSs
            responseUT.put('Id', 'Response-1');
            containmentUT.put('Id', 'Containment-1');
            investigationUT.put('Id', 'Investigation-1');
            inclusion1UT.put('Id', 'Inclusion-1');
            inclusion2UT.put('Id', 'Inclusion-2');
            actionPlan1UT.put('Id', 'ActionPlan-1');
            action1UT.put('Id', 'Action-1');

            inclusion1UT.put(SQX.getNSNameFor('SQX_Containment__c'), 'Containment-1');
            inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
            inclusion1UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
            inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
            actionPlan1UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');

            List<Object> changes = new List<Object>();

            changes.add(responseUT);
            changes.add(containmentUT);
            changes.add(investigationUT);
            changes.add(inclusion2UT);
            changes.add(inclusion1UT);
            changes.add(actionPlan1UT);
            changes.add(action1UT);

            changeSet.put('changeSet', changes);

            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));

            System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].size());
            
            Map<String, String> paramsApprove = new Map<String, String>();
            paramsApprove.put('nextAction', 'approve');
            paramsApprove.put('recordID', [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].Id);

            Id result= SQX_Extension_CAPA.processChangeSetWithAction(capa.capa.Id, '{"changeSet": []}', new SObject[]{}, paramsApprove);

            SQX_CAPA__c thiscapa = [SELECT Id, SQX_Finding__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id];
            SQX_Finding_Response__c findingResponse = new SQX_Finding_Response__c();
            findingResponse = [SELECT Id, Approval_Status__c FROM SQX_Finding_Response__c WHERE SQX_CAPA__c =: thiscapa.Id];

            System.assert(findingResponse.Approval_Status__c == 'Approved', 'Expected the reponse to be approved but found approval status' + findingResponse.Approval_Status__c);

        }

    }

    /**
    * ensures that a complex response can be submitted and rejected
    */
    public testmethod static void givenResponsePublised_ResponseCanBeRejected(){

        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        System.runas(adminUser){
            SQX_Test_CAPA_Utility capa=  new SQX_Test_CAPA_Utility(true, adminUser, adminUser);
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa.save();

            Test.setCurrentPage(Page.SQX_Capa_Supplier);
            ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
            SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);

            controller.initializeTemporaryStorage();

            /* construct a response */
            SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.capa.Id,
                                                                          Response_Summary__c = 'Response');

            SQX_Containment__c containment = new SQX_Containment__c(SQX_CAPA__c = capa.capa.Id,
                                                                                     Containment_Summary__c = 'Containment',
                                                                                     Completion_Date__c = Date.today());

            SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_CAPA__c = capa.capa.Id,
                                                                         Investigation_Summary__c = 'Is');

            SQX_Response_Inclusion__c inclusion1 = new SQX_Response_Inclusion__c(Type__c = 'Containment');
            SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');

            SQX_Action_Plan__c actionPlan1 = new SQX_Action_Plan__c(Description__c = 'asd', Plan_Type__c = 'Preventive'
                                                , Due_Date__c = Date.Today().addDays(10)); 

            SQX_Action__c action1 = new SQX_Action__c(SQX_Capa__c = capa.Capa.Id, Description__c = 'asd', Plan_Type__c = 'Preventive',
                                                    Record_Action__c = 'New', Due_Date__c = Date.Today().addDays(10));

            Map<String,Object>  changeSet = new Map<String, Object>(),
                                responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                containmentUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(containment)),
                                investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                actionPlan1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(actionPlan1)),
                                action1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(action1)),
                                inclusion1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion1)),
                                inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped

            //set virtual IDSs
            responseUT.put('Id', 'Response-1');
            containmentUT.put('Id', 'Containment-1');
            investigationUT.put('Id', 'Investigation-1');
            inclusion1UT.put('Id', 'Inclusion-1');
            inclusion2UT.put('Id', 'Inclusion-2');
            actionPlan1UT.put('Id', 'ActionPlan-1');
            action1UT.put('Id', 'Action-1');

            inclusion1UT.put(SQX.getNSNameFor('SQX_Containment__c'), 'Containment-1');
            inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
            inclusion1UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
            inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
            actionPlan1UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');

            List<Object> changes = new List<Object>();

            changes.add(responseUT);
            changes.add(containmentUT);
            changes.add(investigationUT);
            changes.add(inclusion2UT);
            changes.add(inclusion1UT);
            changes.add(actionPlan1UT);
            changes.add(action1UT);
            
            changeSet.put('changeSet', changes);

            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
            
            System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].size());


            Map<String, String> paramsApprove = new Map<String, String>();
            paramsApprove.put('nextAction', 'reject');
            paramsApprove.put('recordID', [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].Id);

            Id result= SQX_Extension_CAPA.processChangeSetWithAction(capa.capa.Id, '{"changeSet": []}', new SObject[]{}, paramsApprove);

            SQX_CAPA__c thiscapa = [SELECT Id, SQX_Finding__c FROM SQX_CAPA__c WHERE Id =: capa.capa.Id];
            SQX_Finding_Response__c findingResponse = new SQX_Finding_Response__c();
            findingResponse = [SELECT Id, Approval_Status__c FROM SQX_Finding_Response__c WHERE SQX_CAPA__c =: thiscapa.Id];

            System.assert(findingResponse.Approval_Status__c == 'Rejected', 'Expected the reponse to be approved but found approval status' + findingResponse.Approval_Status__c);

        }
        
    }


    /**
    * Scenario: Ensures that response expectation of same object (i.e. parent) is met
    * Given two CAPA(s)
    * When a response is submitted for the different CAPA
    * Then save fails
    */
    static testmethod void givenACAPA_WhenResponseIsSubmittedForDifferentCAPA_SaveFails(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            // Arrange: Create two CAPAs
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa.save();

            SQX_Test_CAPA_Utility capa2= new  SQX_Test_CAPA_Utility();
            capa2.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa2.save();

            // Act: Submit a response for CAPA2 feigning as CAPA 1
            boolean exceptionThrown = false;
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Test', SQX_Finding__c = capa2.finding.finding.Id));

            try {
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false, null, params);
            }
            catch (SQX_ApplicationGenericException ex) {
                exceptionThrown = true;
            }

            // Assert: Ensure that save failed
            System.assertEquals(true, exceptionThrown, 'Expected and exception to be thrown but found none');
        }
    }

    /**
    * Scenario: Ensures that response expectation of count limit is met
    * Given A CAPA
    * When a response is submitted for the different CAPA
    * Then save fails
    */
    static testmethod void givenACAPA_WhenTwoResponsesAreSubmittedForACAPA_SaveFails(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            // Arrange: Create a CAPA
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa.save();

            // Act: Submit a response for a different CAPA Finding
            boolean exceptionThrown = false;
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Test', SQX_Finding__c = capa.finding.finding.Id));
            changeSet.addChanged('Response-2', new SQX_Finding_Response__c( Response_Summary__c = 'Test', SQX_Finding__c = capa.finding.finding.Id));

            try {
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false, null, params);
            }
            catch (SQX_ApplicationGenericException ex) {
                exceptionThrown = true;
            }

            // Assert: Ensure that save failed
            System.assertEquals(true, exceptionThrown, 'Expected and exception to be thrown but found none');
        }
    }

    /**
    * Scenario: Ensures that response expectation of at least one response is met
    * Given A CAPA
    * When a response is without any response is submitted
    * Then save fails
    */
    static testmethod void givenACAPA_WhenResponseWithoutResponseIsSubmitted_SaveFails(){
        System.runas(SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1)) {
            // Arrange: Create a CAPA
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility();
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;

            capa.save();

            // Act: Submit a response for a different CAPA Finding
            boolean exceptionThrown = false;
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            changeSet.addChanged('Containment-1',
                    new SQX_Containment__c( Containment_Summary__c = 'Hello World',
                                            SQX_Finding__c = capa.finding.finding.Id,
                                            Completion_Date__c = Date.today() ));

            try {
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false, null, params);
            }
            catch (SQX_ApplicationGenericException ex) {
                exceptionThrown = true;
            }

            // Assert: Ensure that save failed
            System.assertEquals(true, exceptionThrown, 'Expected and exception to be thrown but found none');
        }
    }

    /**
    * Scenario: Ensures that response expectation is met when an implementation is completed and submitted for CAPA
    * Given A CAPA with implementation
    * When a completion response is submitted
    * Then save goes through
    */
    static testmethod void givenACAPA_WhenAResponseWithCompletedImplementationISSubmitted_SaveGoesThrough(){
        User admin = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER_1);
        System.runas(admin) {
            // Arrange: Create a CAPA
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility(true, admin, admin);
            capa.Capa.Status__c = SQX_Capa.STATUS_OPEN;
            capa.CAPA.Pre_Approval_Required__c = false;
            capa.CAPA.Post_Approval_Required__c = false;

            capa.save();

            boolean exceptionThrown = false;
            SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Test', SQX_CAPA__c = capa.capa.Id));
            changeSet.addChanged('Response-Inc-1', new SQX_Response_Inclusion__c( Type__c = SQX_Response_Inclusion.INC_TYPE_ACTION))
                     .addRelation(SQX_Response_Inclusion__c.SQX_Action__c, 'Action-1')
                     .addRelation(SQX_Response_Inclusion__c.SQX_Response__c, 'Response-1');

            changeSet.addChanged('Action-1',
                    new SQX_Action__c( SQX_CAPA__c = capa.capa.ID,
                                       Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_NEW,
                                       Plan_Type__c = SQX_Action_Plan.PLAN_TYPE_CORRECTIVE,
                                       Due_Date__c = Date.today() ));

            SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false, null, params);

            // Act: Submit a response for completing the action
            SQX_Action__c act = [SELECT Id FROM SQX_Action__c WHERE SQX_CAPA__c = : capa.capa.Id];
            changeSet = new SQX_Test_ChangeSet();
            changeSet.addChanged('Response-1', new SQX_Finding_Response__c( Response_Summary__c = 'Test', SQX_CAPA__c = capa.capa.Id));
            changeSet.addChanged('Response-Inc-1', new SQX_Response_Inclusion__c( Type__c = SQX_Response_Inclusion.INC_TYPE_ACTION, SQX_Action__c = act.Id))
                     .addRelation(SQX_Response_Inclusion__c.SQX_Response__c, 'Response-1');
            changeSet.addChanged(new SQX_Action__c(Id = act.Id,
                                 Record_Action__c = SQX_Implementation_Action.TARGET_ACTION_COMPLETE,
                                 Completion_Date__c = Date.today()))
                     .addOriginalValue(SQX_Action__c.Record_Action__c, null)
                     .addOriginalValue(SQX_Action__c.Completion_Date__c, null);

            try {
                SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', changeSet.toJSON(), false, null, params);
            }
            catch (SQX_ApplicationGenericException ex) {
                exceptionThrown = true;
            }

            // Assert: Ensure that save failed
            System.assertEquals(false, exceptionThrown, 'Expected and exception to be thrown but found none');
        }
    }
}