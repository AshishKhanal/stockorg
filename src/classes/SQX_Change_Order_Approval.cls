/**
* Extension class for Change Order Approval object
*/
public with sharing class SQX_Change_Order_Approval {
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        /**
        * default constructor for Bulkified
        */
        public Bulkified(List<SQX_Change_Order_Approval__c> newList, Map<Id, SQX_Change_Order_Approval__c> oldMap){
            super(newList, oldMap);
        }
        
        /**
        * copies step number as text
        */
        public Bulkified setStepAsTextValue() {
            this.resetView();
            
            if (this.view.size() > 0) {
                for (SQX_Change_Order_Approval__c coa : (List<SQX_Change_Order_Approval__c>)this.view) {
                    coa.Step_As_Text__c = String.valueOf(coa.Step__c);
                }
            }
            
            return this;
        }
    }
}