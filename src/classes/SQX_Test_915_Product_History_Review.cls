/**
 * test for product history review
 */
@isTest
public class SQX_Test_915_Product_History_Review{

    /**
     * GIVEN : PHR is saved
     * WHEN : PHR is saved without summary with relevant observation yes
     * THEN : Error is thrown
     */
    @IsTest
    static void givenPHR_WhenRelevantObservationIsYes_SummaryIsRequired(){
      
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        system.RunAs(standardUser){
            // ARRANGE : PHR is saved
            SQX_Product_History_Review__c phr = new SQX_Product_History_Review__c();
            Database.SaveResult result = Database.insert(phr, false);

            // ACT : PHR is saved without summary
            phr.Relevant_Observation__c = SQX_Product_History_Review.RELEVANT_OBSERVATION_NO;
            result = Database.update(phr, false);

            // ASSERT : Error is thrown
            System.assert(result.isSuccess(), 'Save is not successful');

            // ACT : PHR is saved without summary
            phr.Relevant_Observation__c = SQX_Product_History_Review.RELEVANT_OBSERVATION_YES;
            result = Database.update(phr, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Summary is required.'), result.getErrors());

            // ACT : PHR is saved with summary
            phr.Product_History_Review_Summary__c = 'PHR Summary';
            result = Database.update(phr, false);

            // ASSERT : PHR is saved
            System.assert(result.isSuccess(), 'Save is not successful');
            
            
            // ARRANGE (SQX-8211): add SQX_Product_History_Answer__c, SQX_Related_Product__c
            List<SObject> objsToUpdate = new List<SObject>();
            objsToUpdate.add(new SQX_Product_History_Answer__c( SQX_Product_History_Review__c = phr.Id ));
            objsToUpdate.add(new SQX_Related_Product__c( SQX_Product_History_Review__c = phr.Id ));
            insert objsToUpdate;
            
            // ACT (SQX-8211): update SQX_Product_History_Answer__c, SQX_Related_Product__c
            update objsToUpdate;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Product_History_Answer__c, PHR, SQX_Related_Product__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Product_History_Answer__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Product_History_Review__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Related_Product__c.SObjectType);

        }      
    }

    /**
     * GIVEN : PHR is saved
     * WHEN : PHR is completed without summary and completed by
     * THEN : Error is thrown
     */
    @IsTest
    static void givenPHR_WhenCompletedWithBlankSummaryAndCompletedBy_ErrorIsThrown(){
      
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        system.RunAs(standardUser){
            // ARRANGE : PHR is saved
            SQX_Product_History_Review__c phr = new SQX_Product_History_Review__c();
            Database.SaveResult result = Database.insert(phr, false);

            // ACT : PHR is completed without summary
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Completed_By__c = 'Test User';
            result = Database.update(phr, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Summary must be provided to complete the Product History Review.'), 'Expected : Summary must be provided to complete the Product History Review. Actual: ' + result.getErrors());
            
            // ACT : PHR is completed without completed by
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Product_History_Review_Summary__c = 'Test Summary';
            phr.Completed_By__c = '';
            result = Database.update(phr, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Completed By must be provided to complete the Product History Review.'), 'Expected : Completed By must be provided to complete the Product History Review. Actual: ' + result.getErrors());

            // ACT : PHR is completed with summary and completed by
            phr.Status__c = SQX_Product_History_Review.STATUS_COMPLETE;
            phr.Product_History_Review_Summary__c = 'Test Summary';
            phr.Completed_By__c = 'Test User';

            result = Database.update(phr, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());

        }      
    }
}