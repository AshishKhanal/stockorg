@IsTest
public class SQX_Test_Extension_Supplier_Admin{

    /**
    * This test ensures that the new controller allows creation of account, contact, services, parts in on go
    */
    public static testmethod void givenAccountAndRelatedDataItCanBeCreated(){

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
        
            Account currentAccount = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(currentAccount);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);

            
            System.runas(primaryContactUser){
                ApexPages.StandardController sc = new ApexPages.standardController(currentAccount);
                
                SQX_Extension_Supplier_Admin supplierController = new SQX_Extension_Supplier_Admin(sc);
                
                
                System.assert(supplierController.getAllContacts().size() > 0, 
                    'Expected a new contact to be created but found none');
                       
                Contact contact = (Contact)supplierController.getAllContacts().get(0).baseObject;
                contact.FirstName = 'Ramesh';
                contact.LastName = 'Chowdary';
                contact.Email = 'rams.chowdary@ambarkaar.com';
                
                supplierController.saveAllChanges();
                     
                
                
                System.assert([SELECT ID FROM Contact WHERE Email='rams.chowdary@ambarkaar.com' 
                AND AccountID=:currentAccount.ID].size() > 0,'Expected a contact to be created for the account but found none');
            }
        }

        
    }
    
    
   
    
}