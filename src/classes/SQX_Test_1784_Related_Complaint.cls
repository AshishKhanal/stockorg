/**
 * test class for the Related Complaint
 * @author: Anish Shrestha 
 * @date: 2015-12-18
 * @story: [SQX-1784] 
 */
@isTest
public class SQX_Test_1784_Related_Complaint{

    static boolean runAllTests = false,
                    run_givenAUserAndComplaint_ReferredAndReferrerComplaintCannotBeSame = true;


/**
 * Action: Complaint is referred
 * Expected: Complaint referred is not the current complaint
 * @date: 2015-12-18
 * @author: Anish Shrestha 
 */
    public testmethod static void givenAUserAndComplaint_ReferredAndReferrerComplaintCannotBeSame(){

        if(!runAllTests && !run_givenAUserAndComplaint_ReferredAndReferrerComplaintCannotBeSame){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User standardUserApprover = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

            SQX_Test_Complaint complaint;
            SQX_Complaint__c complaintMain;

        System.runas(standardUser){
            //Act 1: Save a complaint
            complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            complaintMain = [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            //Assert 1: Expected the complaint to be saved and one complaint is created
            System.assertEquals([SELECT Id, Status__c FROM SQX_Complaint__c].size(), 1, 'Expected the complaint to be created');
            //Assert 2: Expected the saved complaint to be in draft statis
            System.assert(complaintMain.Status__c == SQX_Complaint.STATUS_DRAFT , 'Expected the status to be in Draft and found: ' + complaintMain.Status__c);

            //Act 2: Initiate the complaint
            Map<String, String> paramsInitiate = new Map<String, String>();
            paramsInitiate.put('nextAction', 'initiate');
            Id result= SQX_Extension_Complaint.processChangeSetWithAction(complaint.complaint.Id, null, null, paramsInitiate, null);
            complaintMain = [SELECT Id, Status__c, OwnerId FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];

            // Assert 3: Expected the status of the complaint to be in STATUS_COMPLETE
            System.assertEquals(complaintMain.Status__c, SQX_Complaint.STATUS_COMPLETE , 'Expected the status to be in Complete and found: ' + complaintMain.Status__c);
            // Assert 4: Expected the owner of the complaint to be current user
            System.assertEquals(complaintMain.OwnerId, standardUser.Id, 'Expected the owner of the complaint to be in current user');

            //Arrange 2:Add the cross reference complaint to the current complaint
            //Act 2: Refer the current complaint
            SQX_Cross_Reference_Complaint__c referComplaint = new SQX_Cross_Reference_Complaint__c(
                                                                    SQX_Complaint__c = complaint.complaint.Id,
                                                                    SQX_Related_Complaint__c = complaint.complaint.Id,
                                                                    Reference_Type__c = 'Child');

            //Assert 5: Throws error when refer to the same complaint
            try{
                new SQX_DB().op_insert(new List<SObject>{referComplaint}, new List<SObjectField>{SQX_Cross_Reference_Complaint__c.fields.SQX_Complaint__c,
                                                                                                    SQX_Cross_Reference_Complaint__c.fields.SQX_Related_Complaint__c,
                                                                                                    SQX_Cross_Reference_Complaint__c.fields.Reference_Type__c});
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Can\'t self reference complaint') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 

        }
    }
}