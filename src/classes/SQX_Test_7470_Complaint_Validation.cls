/**
 * Test class to ensure closed or voided complaint cannot be edited or it's child objects can be inserted or edited
 * @author: Sanjay Maharjan
 * @date: 2019-01-10
 * @story: [SQX-7470]
 */
@isTest
public class SQX_Test_7470_Complaint_Validation {
    
    static Database.SaveResult result;
    
    static final String ERROR_MESSAGE = 'Record Status does not support the Action Performed.';
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(adminUser) {
            SQX_Defect_Code__c defectCode = new SQX_Defect_Code__c(Name = 'Test Defect Code', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            new SQX_DB().op_insert(new List<SQX_Defect_Code__c>{ defectCode }, new List<SObjectField>());
        }

	}
    
    /**
     * GIVEN : A complaint is created and voided or closed
     * WHEN  : Tried to edit
     * THEN  : Error is thrown
     */
    public testMethod static void givenComplaintVoidedOrClosed_WhenTriedToEdit_ThenErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser) {
            
            // ARRANGE : Create complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Void complaint
            complaint.void();
            
            SQX_Complaint__c com = [SELECT Id FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ACT : Edit complaint
            com.Description__c = 'New Description';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{com}, new List<SObjectField>{})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + ' But was : ' + result.getErrors()[0]);
            
            SQX_Defect_Code__c complaint_defect = [SELECT Id FROM SQX_Defect_Code__c].get(0);

            // ARRANGE : Create complaint
            complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Close complaint
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.complaint.SQX_Conclusion_Code__c = complaint_defect.Id;
            complaint.close();
            
            com = [SELECT Id FROM SQX_Complaint__c WHERE Id =: complaint.complaint.Id];
            
            // ACT : Edit complaint
            com.Description__c = 'New Description';
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Complaint__c>{com}, new List<SObjectField>{})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + ' But was : ' + result.getErrors()[0]);
            
        }
    }
    
    /**
     * GIVEN : A locked complaint
     * WHEN  : Related objects of complaint are created
     * THEN  : Error is thrown
     */
    public testMethod static void givenLockedComplaint_WhenRelatedObjectsAreCreated_ThenErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create complaint 
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ARRANGE : Lock complaint by voiding it
            complaint.void();
            
            // ARRANGE : Set Decision Tree
            SQX_Decision_Tree__c decision_tree = new SQX_Decision_Tree__c();
            decision_tree.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Create Decision Tree related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Decision_Tree__c>{decision_tree}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
            
            // ARRANGE : Set Regulatory Report
            SQX_Regulatory_Report__c regulatory_report = new SQX_Regulatory_Report__c();
            regulatory_report.SQX_Complaint__c = complaint.complaint.Id;
            regulatory_report.Due_Date__c = Date.today();
            
            // ACT : Create Regulatory Report related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Regulatory_Report__c>{regulatory_report}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
            
            // ARRANGE : Set Submission History
            SQX_Submission_History__c submission_history = new SQX_Submission_History__c();
            submission_history.SQX_Complaint__c = complaint.complaint.Id;
            submission_history.Submitted_By__c = standardUser.Id;
            
            // ACT : Create Submission History related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Submission_History__c>{submission_history}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);

            // ARRANCE : Set Sample Tracking 
            SQX_Sample_Tracking__c sample_tracking = new SQX_Sample_Tracking__c();
            sample_tracking.SQX_Complaint__c = complaint.complaint.Id;
            sample_tracking.From_Location__c = 'Sample Location';
            sample_tracking.To_Location__c = 'Sample Location';

            // ACT : Create Sample Tracking related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Sample_Tracking__c>{sample_tracking}, new List<Schema.SObjectField>())[0];

            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
			
            // ARRANGE : Set Product History Review
            SQX_Product_History_Review__c product_history_review = new SQX_Product_History_Review__c();
            product_history_review.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Create Product History Review related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Product_History_Review__c>{product_history_review}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
            
            // ARRANGE : Set Complaint Defect
            SQX_Part__c part = SQX_Test_Part.insertPart(null, standardUser, true, '');
            
            SQX_Complaint_Defect__c complaint_defect = new SQX_Complaint_Defect__c();
            complaint_defect.SQX_Complaint__c = complaint.complaint.Id;
            complaint_defect.Defect_Code__c = 'New Defect Code';
            complaint_defect.Defect_Category__c = 'New Defect Category';
            complaint_defect.Number_of_defects__c = 1;
            complaint_defect.SQX_Part__c = part.Id;
            
            // ACT : Create Complaint Defect related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Defect__c>{complaint_defect}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
            
            // ARRANGE : Set Associated Item
            SQX_Complaint_Associated_Item__c complaint_associated_item = new SQX_Complaint_Associated_Item__c();
            complaint_associated_item.SQX_Complaint__c = complaint.complaint.Id;
            complaint_associated_item.SQX_Part_Family__c = part.Part_Family__c;
            
            // ACT : Create Associated Item related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Associated_Item__c>{complaint_associated_item}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
            
            // ARRANGE : Set Complaint Contact
            SQX_Complaint_Contact__c complaint_contact = new SQX_Complaint_Contact__c();
            complaint_contact.SQX_Complaint__c = complaint.complaint.Id;
            
            // ACT : Create Complaint Contact related to complaint
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Contact__c>{complaint_contact}, new List<Schema.SObjectField>())[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), ERROR_MESSAGE), 'Error message should be "' + ERROR_MESSAGE + '" but was : ' + result.getErrors()[0]);
           
        }
    }
    
    /**
     * GIVEN : Complaint with Related Objects
     * WHEN  : Complaint is locked and Related Objects are deleted
     * THEN  : Error is thrown
     */
    public testMethod static void givenComplaintWithRelatedObjects_WhenComplaintIsLockedAndChildObjectsAreDeleted_ThenErrorIsThrown() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser) {
            
            // ARRANGE : Complaint with related objects
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            SQX_Decision_Tree__c decision_tree = new SQX_Decision_Tree__c();
            decision_tree.SQX_Complaint__c = complaint.complaint.Id;
            
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Decision_Tree__c>{decision_tree}, new List<Schema.SObjectField>())[0];
            
            System.assertEquals(true, result.isSuccess(), 'Decision Tree should be successfully inserted.' + result.getErrors());
            
            SQX_Submission_History__c submission_history = new SQX_Submission_History__c();
            submission_history.SQX_Complaint__c = complaint.complaint.Id;
            submission_history.Status__c = SQX_Submission_History.STATUS_COMPLETE;
            submission_history.Submitted_By__c = adminUser.Id;
            
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Submission_History__c>{submission_history}, new List<Schema.SObjectField>())[0];
            
            System.assertEquals(true, result.isSuccess(), 'Submission History should be successfully inserted.');
            
            SQX_Regulatory_Report__c regulatory_report = new SQX_Regulatory_Report__c();
            regulatory_report.SQX_Complaint__c = complaint.complaint.Id;
            regulatory_report.SQX_Submission_History__c = submission_history.Id;
            regulatory_report.Status__c = SQX_Regulatory_Report.STATUS_COMPLETE;
            regulatory_report.Due_Date__c = Date.today();
            
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Regulatory_Report__c>{regulatory_report}, new List<Schema.SObjectField>())[0];
            
            System.assertEquals(true, result.isSuccess(), 'Regulatory Report should be successfully inserted.' + result.getErrors());

            SQX_Test_Complaint newComplaint = new SQX_Test_Complaint(adminUser).save();
            SQX_Cross_Reference_Complaint__c related_complaints = new SQX_Cross_Reference_Complaint__c();
            related_complaints.SQX_Complaint__c = complaint.complaint.Id;
            related_complaints.SQX_Related_Complaint__c = newComplaint.complaint.Id;
            
            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Cross_Reference_Complaint__c>{related_complaints}, new List<Schema.SObjectField>())[0];
            
            System.assertEquals(true, result.isSuccess(), 'Related Complaint should be successfully inserted.');

            SQX_Sample_Tracking__c sample_tracking = new SQX_Sample_Tracking__c();
            sample_tracking.SQX_Complaint__c = complaint.complaint.Id;
            sample_tracking.From_Location__c = 'Sample Location';
            sample_tracking.To_Location__c = 'Sample Location';
            sample_tracking.Performed_By__c = 'Test User';
            sample_tracking.Performed_Date__c = DateTime.now();
            

            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Sample_Tracking__c>{sample_tracking}, new List<Schema.SObjectField>())[0];

            System.assertEquals(true, result.isSuccess(), 'Sample Tracking should be successfully inserted.' + result.getErrors());

            SQX_Product_History_Review__c product_history_review = new SQX_Product_History_Review__c();
            product_history_review.SQX_Complaint__c = complaint.complaint.Id;

            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Product_History_Review__c>{product_history_review}, new List<Schema.SObjectField>())[0];

            System.assertEquals(true, result.isSuccess(), 'Product History Review should be successfully inserted.');
            
            SQX_Part__c part = SQX_Test_Part.insertPart(null, adminUser, true, '');
            
            SQX_Complaint_Defect__c complaint_defect = new SQX_Complaint_Defect__c();
            complaint_defect.SQX_Complaint__c = complaint.complaint.Id;
            complaint_defect.Defect_Code__c = 'New Defect Code';
            complaint_defect.Defect_Category__c = 'New Defect Category';
            complaint_defect.Number_of_defects__c = 1;
            complaint_defect.SQX_Part__c = part.Id;
            

            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Defect__c>{complaint_defect}, new List<Schema.SObjectField>())[0];

            System.assertEquals(true, result.isSuccess(), 'Complaint Defect should be successfully inserted.');

            SQX_Complaint_Associated_Item__c complaint_associated_item = new SQX_Complaint_Associated_Item__c();
            complaint_associated_item.SQX_Complaint__c = complaint.complaint.Id;
            complaint_associated_item.SQX_Part_Family__c = part.Part_Family__c;

            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Associated_Item__c>{complaint_associated_item}, new List<Schema.SObjectField>())[0];
			
            System.assertEquals(true, result.isSuccess(), 'Complaint Associated Item should be successfully inserted.');

            SQX_Complaint_Contact__c complaint_contact = new SQX_Complaint_Contact__c();
            complaint_contact.SQX_Complaint__c = complaint.complaint.Id;

            result = new SQX_DB().continueOnError().op_insert(new List<SQX_Complaint_Contact__c>{complaint_contact}, new List<Schema.SObjectField>())[0];

            System.assertEquals(true, result.isSuccess(), 'Complaint Contact should be successfully inserted.');
            
            // ACT : Complaint is locked by voiding
            complaint.void();

            // ACT : Delete related regulatory report
            regulatory_report = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Regulatory_Report__c WHERE Id =: regulatory_report.Id];
            Database.DeleteResult deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Regulatory_Report__c>{regulatory_report})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assertEquals(false, deleteResult.isSuccess(), 'Regulatory Reports cannot be deleted.');
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT), 'Error message should be "' + Label.SQX_ERR_MSG_CANNOT_DELETE_REGULATORY_REPORT + '" but was : ' + deleteResult.getErrors()[0]);
            
            // ACT : Delete related related complaints
            related_complaints = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Cross_Reference_Complaint__c WHERE Id =: related_complaints.Id];
            deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Cross_Reference_Complaint__c>{related_complaints})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), 'Error message should be "' + Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked + '" but was : ' + deleteResult.getErrors()[0]);

            // ACT : Delete related complaint defect
            complaint_defect = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Complaint_Defect__c WHERE Id =: complaint_defect.Id];
            deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Complaint_Defect__c>{complaint_defect})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), 'Error message should be "' + Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked + '" but was : ' + deleteResult.getErrors()[0]);

            // ACT : Delete related complaint associated item
            complaint_associated_item = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Complaint_Associated_Item__c WHERE Id =: complaint_associated_item.Id];
            deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Complaint_Associated_Item__c>{complaint_associated_item})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), 'Error message should be "' + Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked + '" but was : ' + deleteResult.getErrors()[0]);

            // ACT : Delete related complaint contact
            complaint_contact = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Complaint_Contact__c WHERE Id =: complaint_contact.Id];
            deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Complaint_Contact__c>{complaint_contact})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), 'Error message should be "' + Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked + '" but was : ' + deleteResult.getErrors()[0]);
            
            // ACT : Delete related sample tracking
            sample_tracking = [SELECT Id, SQX_Complaint__r.Is_Locked__c FROM SQX_Sample_Tracking__c WHERE Id =: sample_tracking.Id];
            deleteResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Sample_Tracking__c>{sample_tracking})[0];
            
            // ASSERT : Appropriate error is thrown
            System.assert(SQX_Utilities.checkErrorMessage(deleteResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), 'Error message should be "' + Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked + '" but was : ' + deleteResult.getErrors()[0]);
            
        }
        
    }
    
    /**
     * GIVEN : Complaint not in draft status and activity code null
     * WHEN  : Complaint is updated
     * THEN  : Complaint record activity is created with appropriate activity
     */ 
    public testMethod static void givenComplaintNotInDraftAndActivityCodeNull_WhenComplaintIsUpdated_ThenComplaintRecordActivityWithAppropriateActivityIsCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Complaint in open status
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            complaint.complaint.Status__c = SQX_Complaint.STATUS_OPEN;
            result = new SQX_DB().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{})[0];
            
            // ACT : Complaint is updated
            complaint.complaint.Description__c = 'New Description Test';
            result = new SQX_DB().op_update(new List<SQX_Complaint__c>{complaint.complaint}, new List<SObjectField>{})[0];
            
            SQX_Complaint_Record_Activity__c complaintRecordActivity = [SELECT Id, Activity__c FROM SQX_Complaint_Record_Activity__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            
            // ASSERT : Complaint Record Activity is created and 'Saving Complaint' activity is set
            System.assertEquals(Label.SQX_PS_Complaint_Saving_the_record, complaintRecordActivity.Activity__c);
            
        }
    }

}