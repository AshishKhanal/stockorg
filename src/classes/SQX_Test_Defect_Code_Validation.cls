/**
 * Unit tests for Unique Defect Code and also against the flow that sets 
 * Part Name name and Part Number when Part is created.
 * Flow Name: CQ_Part_Copy_Product_Name_and_Code_to_Part_Name_and_Number
 * @author Kishor Bikram Oli
 * @date 2018/12/03
 * 
 */

@IsTest
public class SQX_Test_Defect_Code_Validation{
    /**
     * Setup: Create two defect code
     * Action: To check the DefectCodes name and type is unique
     * Expected: Second insert should be failed with its error msg
     *
     */
    public static testmethod void defectCodeAdded_DuplicateNotAllowed(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser){
            //arrange: Create two defect codes with same name and type.
            SQX_Defect_Code__c defectCode1 = new SQX_Defect_Code__c(Name = 'Defecttest', Type__c = 'DefectType1', Defect_Category__c = 'abc');

            //act: Save two created records
            Database.SaveResult result1 = Database.Insert(defectCode1 ,false);
            System.assert(result1.isSuccess() == true, 'Expected Defect code save to be success ');
            SQX_Defect_Code__c defectCode2 = new SQX_Defect_Code__c(Name = 'Defecttest', Type__c = 'DefectType1', Defect_Category__c = 'abc');
            
            //assert: Second record cannot be saved.
            Database.SaveResult result2 = Database.Insert(defectCode2 ,false);
            System.assert(result2.isSuccess() == false, 'Expected Defect code save to be Unsuccess');
        }
    }

    /**
     * GIVEN : given Defect Code
     * WHEN : when save the record with negative level
     * THEN : throw validation error message
     */
    public static testMethod void givenDefectCode_WhenSaveRecordWithLevelNegative_ThenThrowValidationErrorMessage(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(adminUser){
            //arrange: Create a defect code.
            SQX_Defect_Code__c defectCode = new SQX_Defect_Code__c(Name = 'Defecttest',
                                                                    Type__c ='DefectType1',
                                                                    Defect_Category__c ='abc',
                                                                       Level__c = -1);
            //act: insert record with level less than 0.
            Database.SaveResult result = Database.Insert(defectCode, false);

            //assert: Record cannot be saved.
            System.assertEquals(result.isSuccess(), false, 'Level should be greater or equals to zero.');
            System.assertEquals(result.getErrors().get(0).getMessage(), 'Level should be greater or equals to zero.', 'Level cannot be negative.');
        }
    }

    /**
     * GIVEN : given two parent child Defect Codes
     * WHEN : when child record is updated with level smaller than the parent's level
     * THEN : throw validation error message
     */
     public static testMethod void givenTwoDefectCode_WhenChildRecordIsUpdatedWithLevelSmallerThanParentLevel_ThenThrowValidationErrorMessage(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runAs(adminUser){
            //arrange: Create and insert two parent child defect codes
            SQX_Defect_Code__c defectCode1 = new SQX_Defect_Code__c(Name = 'Defecttest1',
                                                                    Type__c ='Type1',
                                                                    Defect_Category__c ='Cat1',
                                                                    Active__c = true,
                                                                    Level__c = 0);
            Database.SaveResult result1 = Database.Insert(defectCode1, false);

            SQX_Defect_Code__c defectCode2 = new SQX_Defect_Code__c(Name = 'Defecttest2',
                                                                    Type__c ='Type1',
                                                                    Defect_Category__c ='Cat1',
                                                                    SQX_Parent_Code__c = defectCode1.Id,
                                                                    Active__c = true,
                                                                    Level__c = 1);
            Database.SaveResult result2 = Database.Insert(defectCode2, false);

            //act: Update child record with level less than its parent's level
            defectCode2.Level__c = 0;
            Database.SaveResult result3 = Database.Update(defectCode2, false);

            //assert: Record cannot be updated.
            System.assertEquals(result3.isSuccess(), false, 'Level has to be 1 more than parent');
            System.assertEquals(result3.getErrors().get(0).getMessage(), 'Level has to be 1 more than parent', 'Level has to be 1 more than parent');
        }
    }

    /**
     * Ensures that Part Name name and Part Number are inserted when Part is created.
     */
    @isTest static void givenAProductAndPartHasLookupProductAndPartFamily_WhenPartIsSaved_ThenPartNamePartNumberShouldBeInserted(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            //Arrange: Product and Part Family are created
            Product2 product = new Product2(
                Name = 'Product',
                ProductCode = 'Product-001'
            );

            insert product;
            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random_Part_Family');
            insert partFamily;

            //Act: SQX_Part__c is inserted and retrieved again
            SQX_Part__c part = new SQX_Part__c (Part_Number__c = 'PART-001',
                                                Part_Family__c = partFamily.Id,
                                                Part_Risk_Level__c = 4);
            insert part;

            SQX_Part__c part1 = [SELECT Name, Part_Number__c FROM SQX_Part__c WHERE Id =: part.Id];

            //Assert: Check if Part Name and Part Number is null or not. Null means our flow is not working.
            System.assertNotEquals(part1.Name, null, 'Part name is empty');
            System.assertNotEquals(part1.Part_Number__c, null, 'Part number is empty');
        }
    }
    
    /**
     * GIVEN : Defect Code is created
     * WHEN : Catogory is blank for complaint and non complaint related defect codes
     * THEN : Error is thrown
     */
    @IsTest
    static void givenDefectCode_WhenDefectCodeIsSaved_CategoryIsRequiredAsPerRecordType(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        Map<String, Map<String, Id>> sobjectRecordTypeMap = SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new List<String> {SQX_Defect_Code__c.SObjectType.getDescribe().getName()});

        system.RunAs(standardUser){
            
            Map<String, Id> defectRecordTypeMap = sobjectRecordTypeMap.get(SQX_Defect_Code__c.SObjectType.getDescribe().getName());
            Id complaintRelatedRecordTypeId = defectRecordTypeMap.get('Complaint_Related');
            Id nonComplaintRelatedRecordTypeId = defectRecordTypeMap.get('Non_Complaint_Related');
            Id externalReportingRecordTypeId = defectRecordTypeMap.get('External_Reporting');

            // ARRANGE : Defect code is created
            SQX_Defect_Code__c defectCode = new SQX_Defect_Code__c(Name = 'Defecttest1',
                                                                    Type__c ='Type1',
                                                                    Active__c = true,
                                                                    Level__c = 0);

            // ACT : Complaint related defect code is inserted without category
            defectCode.RecordTypeId = complaintRelatedRecordTypeId;
            Database.SaveResult result = Database.Insert(defectCode, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Category is required.'), result.getErrors());

            // ACT : Non Complaint related defect code is inserted without category
            defectCode.RecordTypeId = nonComplaintRelatedRecordTypeId;
            result = Database.Insert(defectCode, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Category is required.'), result.getErrors());

            // ACT : External Reporting defect code is inserted without category
            defectCode.RecordTypeId = externalReportingRecordTypeId;
            result = Database.Insert(defectCode, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());

            // ACT : External Reporting defect code is inserted with category
            defectCode.Defect_Category__c = 'DEFECT_CATEGORY';
            result = Database.update(defectCode, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());

            // ACT : Complaint related defect code is inserted without category
            defectCode.RecordTypeId = complaintRelatedRecordTypeId;
            defectCode.Defect_Category__c = 'DEFECT_CATEGORY';
            result = Database.update(defectCode, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());

            // ACT : Non Complaint related defect code is inserted with category
            defectCode.RecordTypeId = nonComplaintRelatedRecordTypeId;
            defectCode.Defect_Category__c = 'DEFECT_CATEGORY';
            result = Database.update(defectCode, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
        }
    }
    
    /**
     * ARRANGE : Defect Code is created
     * WHEN : Reporting Term of incorrect type is inserted
     * THEN : Error is thrown
     */
    @IsTest
    static void givenDefectCode_WhenInvalidReportingTermIsSaved_ErrorIsThrown(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        Map<String, Map<String, Id>> sobjectRecordTypeMap = SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new List<String> {SQX_Defect_Code__c.SObjectType.getDescribe().getName()});
        system.RunAs(standardUser){
            
            Map<String, Id> defectRecordTypeMap = sobjectRecordTypeMap.get(SQX_Defect_Code__c.SObjectType.getDescribe().getName());
            Id complaintRelatedRecordTypeId = defectRecordTypeMap.get('Complaint_Related');
            Id externalReportingRecordTypeId = defectRecordTypeMap.get('External_Reporting');

            // ARRANGE : Defect Codes are created
            SQX_Defect_Code__c defectCode1 = new SQX_Defect_Code__c(Name = 'Defecttest1',
                                                                    Type__c ='Type1',
                                                                    Defect_Category__c ='Cat1',
                                                                    RecordTypeId = complaintRelatedRecordTypeId,
                                                                    Active__c = true,
                                                                    Level__c = 0);

            SQX_Defect_Code__c defectCode2 = new SQX_Defect_Code__c(Name = 'Defecttest2',
                                                                    Type__c ='Type2',
                                                                    Defect_Category__c ='Cat2',
                                                                    RecordTypeId = externalReportingRecordTypeId,
                                                                    Active__c = true,
                                                                    Level__c = 0);

            List<Database.SaveResult> results = Database.Insert(new List<SQX_Defect_Code__c>{defectCode1, defectCode2}, false);

            SQX_Defect_Code__c defectCode = new SQX_Defect_Code__c(Name = 'Defecttest',
                                                                    Type__c ='Type',
                                                                    Defect_Category__c ='Cat',
                                                                    RecordTypeId = externalReportingRecordTypeId,
                                                                    Active__c = true,
                                                                    SQX_Reporting_Term__c = defectCode1.Id,
                                                                    Level__c = 0);

            // ACT : Reporting Term with invalid defect code is added
            Database.SaveResult result = Database.Insert(defectCode, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Hierarchical Code of type External Reporting is required.'), result.getErrors());

            // ACT : Reporting Term with valid defect code is added
            defectCode.SQX_Reporting_Term__c = defectCode2.Id;
            result = Database.Insert(defectCode, false);
            
            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is successful' + result.getErrors());
        }
    }
}