/**
* class used for passing variable to update audit in supplier steps
*/ 
global class SQX_Supplier_Steps {
    @InvocableVariable
    global Id StepId;
    
    @InvocableVariable
    global Id AuditId;
    
}