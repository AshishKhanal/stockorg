/*
 * class to copy the specification details of the active inspection criteria to the inspection matching the association
 */
global with sharing class SQX_Specification_Migration {

    /*
     * method to copy the specification details of the active inspection criteria to the inspection matching the association
     * @param inspections list of inspection of which inspection detail is to be created
     */
    @InvocableMethod(Label='Copy Specification To Inspection Detail of the Inspection')
    global static void copySpecificationToInspectionDetail(List<SQX_Inspection__c> inspections){
        List<SQX_Inspection_Detail__c> inspectionDetailToBeAdded = new List<SQX_Inspection_Detail__c>();
        Map<Id, SQX_Inspection__c> inspectionParamMap = new Map<Id, SQX_Inspection__c>(inspections);

        // 1. Identify all the associations that need to be fetched for the given inspections
        //    Each inspection can have association as follows:
        //    Product:  (Part + Rev + Process + Part Family)
        //    Process:  (Process + Part + Rev + Part Family)
        Map<Id, SQX_Inspection__c> inspectionsMap = new Map<Id, SQX_Inspection__c>([
            SELECT Id, SQX_Part__c, Rev__c, Inspection_Type__c, SQX_Process__c, SQX_Part__r.Part_Family__c, SQX_Inspection_Criteria__c
            FROM SQX_Inspection__c
            WHERE Id IN : inspectionParamMap.keySet()
            FOR UPDATE
        ]);

        List<SpecAssociation> associationLevels = new List<SpecAssociation>();
        Set<String> allAssociations = new Set<String>();
        for(SQX_Inspection__c ins : inspectionsMap.values()) {
            SpecAssociation spec = new SpecAssociation(ins, inspectionParamMap.get(ins.Id).SQX_Inspection_Criteria__c);
            associationLevels.add(spec);
            spec.addAllTo(allAssociations);
        }

        // 2. Get all valid associations present in the system and associate it inspection with criteria
        if(allAssociations.size() > 0) {
            Id inspectionCriteriaId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ControlledDoc, SQX_Controlled_Document.RT_INSPECTION_CRITERIA_API_NAME);
            for(SQX_Association__c association : [SELECT Id, SQX_Inspection_Criteria__c, Uniqueness_Constraint__c
                                                FROM SQX_Association__c
                                                WHERE SQX_Inspection_Criteria__r.RecordTypeId = :inspectionCriteriaId
                                                    AND SQX_Inspection_Criteria__r.Document_Status__c IN: SQX_Controlled_Document.DOCUMENT_STATUSES_ACTIVE
                                                    AND Uniqueness_Constraint__c IN : allAssociations
                                                ORDER BY SQX_Part__c, Rev__c, SQX_Process__c, SQX_Part_Family__c NULLS LAST]){

                for(SpecAssociation spec : associationLevels) {
                    if(spec.isApplicable(association.Uniqueness_Constraint__c)) {
                        spec.apply(association.Uniqueness_Constraint__c, association.SQX_Inspection_Criteria__c);
                    }
                }
            }
        }

        Map<Id, Set<SpecAssociation>> specsByCriteria = new Map<Id, Set<SpecAssociation>>();
        for(SpecAssociation spec : associationLevels) {
            spec.mapCriteriaTo(specsByCriteria);
        }

        // 3. Fetch all document along with criteria
        List<SQX_Inspection_Detail__c> detailsToAdd = new List<SQX_Inspection_Detail__c>();
        for(SQX_Controlled_Document__c doc : [SELECT Id,
                                                    (SELECT Id,
                                                        SQX_Inspection_Criteria__c,
                                                        Characteristics__c,
                                                        Specification__c,
                                                        Measurement_Standard__c,
                                                        Impact__c,
                                                        Lower_Spec_Limit__c,
                                                        Upper_Spec_Limit__c,
                                                        Inspection_Method__c,
                                                        SQX_Equipment__c
                                                    FROM SQX_Specifications__r
                                                    ORDER BY Specification_Number__c ASC)
                                                FROM SQX_Controlled_Document__c
                                                WHERE Id IN : specsByCriteria.keySet()]) {
            for(SpecAssociation spec : specsByCriteria.get(doc.Id)) {
                spec.addSpec(doc);
            }
        }

        for(SpecAssociation spec : associationLevels) {
            detailsToAdd.addAll(spec.SpecCriteria);
        }

        if(detailsToAdd.size() > 0){
            new SQX_DB().op_insert(detailsToAdd, new List<SObjectField>{SQX_Inspection_Detail__c.SQX_Inspection__c,
                                                                                        SQX_Inspection_Detail__c.SQX_Specification__c,
                                                                                         SQX_Inspection_Detail__c.Characteristics__c,
                                                                                         SQX_Inspection_Detail__c.Measurement_Standard__c,
                                                                                         SQX_Inspection_Detail__c.Impact__c,
                                                                                         SQX_Inspection_Detail__c.Specification__c,
                                                                                         SQX_Inspection_Detail__c.Lower_Spec_Limit__c,
                                                                                         SQX_Inspection_Detail__c.Upper_Spec_Limit__c,
                                                                                         SQX_Inspection_Detail__c.Inspection_Method__c,
                                                                                         SQX_Inspection_Detail__c.SQX_Equipment__c});
        }

        new SQX_DB().op_update(inspectionsMap.values(), new List<SObjectField>{SQX_Inspection__c.SQX_Inspection_Criteria__c});
    }



    /**
     * This class contains the logic for managing possible associations for an inspection
     * and applying the applicable criteria on the inspection
     */
    public class SpecAssociation {
        // spec applicable because of part or process
        public List<String> MainSpecs {get; private set;}

        // spec applicable because of part family
        public List<String> PartFamilySpecs {get; private set;}

        // the inspection related to the spec
        public SQX_Inspection__c Inspection {get; private set;}

        // currently applicable spec in part family and main level
        private Id MainSpecId = null, PartFamilySpecId = null;

        // caches the index of applicable main spec and part family spec
        private Integer MainSpecIndex, PartFamilySpecIndex;

        public List<SQX_Inspection_Detail__c> SpecCriteria { get; set;}

        /**
         * Default constructor for handling inspection criteria.
         * @param ins the inspection for which specification is to be determined
         * @param mainSpecId the id of currently selected specification.
         */
        public SpecAssociation(SQX_Inspection__c ins, Id mainSpecId) {
            Inspection = ins;
            MainSpecs = new List<String>();
            PartFamilySpecs = new List<String>();
            // associate inspection criteria if it is already provided
            MainSpecId = mainSpecId;
            if(MainSpecId == null){
                // only load specifications matches if we haven't already identified the criteria to use
                initializeSpecifications();
            }
            SpecCriteria = new List<SQX_Inspection_Detail__c>();
        }

        /**
         * Initializes the specifications based on the inspection 
         */
        private void initializeSpecifications(){
            SQX_Inspection__c ins = Inspection;
            // add specifications from most specific to least
            MainSpecs.Add(SQX_Association.getActiveAssociationKey(ins.SQX_Part__c, ins.Rev__c, ins.SQX_Process__c, null));
            MainSpecs.Add(SQX_Association.getActiveAssociationKey(ins.SQX_Part__c, null, ins.SQX_Process__c, null));

            if(ins.Inspection_Type__c == SQX_Inspection.INSPECTION_TYPE_PRODUCT) {
                MainSpecs.Add(SQX_Association.getActiveAssociationKey(ins.SQX_Part__c, ins.Rev__c, null, null));
                MainSpecs.Add(SQX_Association.getActiveAssociationKey(ins.SQX_Part__c, null, null, null));

                PartFamilySpecs.Add(SQX_Association.getActiveAssociationKey(null, null, ins.SQX_Process__c, ins.SQX_Part__r.Part_Family__c));
                PartFamilySpecs.Add(SQX_Association.getActiveAssociationKey(null, null, null, ins.SQX_Part__r.Part_Family__c));
            }
            else {
                //process
                MainSpecs.Add(SQX_Association.getActiveAssociationKey(null, null, ins.SQX_Process__c, ins.SQX_Part__r.Part_Family__c));
                MainSpecs.Add(SQX_Association.getActiveAssociationKey(null, null, ins.SQX_Process__c, null));
            }
        }

        /**
         * Adds all applicable associations in the specification to the set.
         * @param allData the set where all associations are to be added
         */
        public void addAllTo(Set<String> allData) {
            allData.addAll(MainSpecs);
            allData.addAll(PartFamilySpecs);
        }

        /**
         * Adds the inspection to the criteria map to properly download the specifications
         * @param criteriaToIns the map where inspection is to be stored
         */
        public void mapCriteriaTo(Map<Id, Set<SpecAssociation>> criteriaToIns) {
            for(Id specId : new Id [] {MainSpecId, PartFamilySpecId}) {
                if(specId != null){
                    if(!criteriaToIns.containsKey(specId)) {
                        criteriaToIns.put(specId, new Set<SpecAssociation>());
                    }
                    criteriaToIns.get(specId).add(this);
                }
            }
        }

        /**
         * Checks whether or not the specification is valid for the association
         * @param specId the key that identifies the association.
         * @return returns <code>true</code> if the association is applicable to the current inspection, else <code>false</code>
         */
        public Boolean isApplicable(String specId) {
            MainSpecIndex = indexOf(specId, MainSpecs);
            PartFamilySpecIndex = indexOf(specId, PartFamilySpecs);

            return MainSpecIndex != -1 || PartFamilySpecIndex != -1;
        }

        /**
         * Internal method to search the key in the spec lists and return the index
         * @param specId the string to be searched
         * @param searchList the list where the string is to be found
         * @return returns the index of the specId in the list if found else returns -1
         */
        private Integer indexOf(String specId, List<String> searchList) {
            Integer index = -1;
            for(Integer i = 0; i < searchList.size(); i++) {
                if(searchList[i] == specId) {
                    index = i;
                    break;
                }
            }
            return index;
        }

        /**
         * Remove everything in the list after a given index. It is useful for removing all lower
         * priority associations from the specification list
         * @param index the index from which the other items are to be removed
         * @param listToAlter the list from which items are to be removed.
         */
        public void removeAfter(Integer index, List<String> listToAlter) {
            for(Integer i = listToAlter.size() -1; i > index; i--) {
                listToAlter.remove(i);
            }
        }

        /**
         * Applies a specification to the inspection and associates it with the criteria
         * It should be noted that isApplicable is called before apply.
         * @param specId the specification that is to be applied
         * @param newSpecId the Id of the specification criteria that is to be applied
         */
        public void apply(String specId, Id newSpecId) {
            System.assert(MainSpecIndex != -1 || PartFamilySpecIndex != -1, 'Please ensure isApplicable is called before apply');

            if(MainSpecIndex != -1) {
                removeAfter(MainSpecIndex, MainSpecs);
                MainSpecId = newSpecId;
            }
            else {
                removeAfter(PartFamilySpecIndex, PartFamilySpecs);
                PartFamilySpecId = newSpecId;
            }

            Inspection.SQX_Inspection_Criteria__c = MainSpecId != null ? MainSpecId : PartFamilySpecId;
        }

        public void addSpec(SQX_Controlled_Document__c doc) {
            for(SQX_Specification__c [] specifications : doc.SQX_Specifications__r){
                List<SQX_Inspection_Detail__c> detail = getInspectionDetails(doc.SQX_Specifications__r, Inspection);
                if(doc.Id == MainSpecId) {
                    detail.addAll(SpecCriteria);
                    SpecCriteria = detail;
                }
                else {
                    SpecCriteria.addAll(detail);
                }
            }
        }

        /*
        * this method copies the details from specification to inspection detail
        * @param {specificationList} list of specification whose inspection detail is to be created
        * @param {inspection} inspection in which inspection detail to be added
        * @returns list of inspection detail which is to be added
        */
        private List<SQX_Inspection_Detail__c> getInspectionDetails(List<SQX_Specification__c> specificationList, SQX_Inspection__c inspection){
            List<SQX_Inspection_Detail__c> inspectionDetailToBeAdded = new List<SQX_Inspection_Detail__c>();
            for(SQX_Specification__c specification : specificationList){
                    SQX_Inspection_Detail__c newInspectionDetail = new SQX_Inspection_Detail__c(
                        SQX_Inspection__c = inspection.Id,
                        SQX_Specification__c = specification.Id,
                        Characteristics__c = specification.Characteristics__c,
                        Measurement_Standard__c = specification.Measurement_Standard__c,
                        Impact__c = specification.Impact__c,
                        Specification__c = specification.Specification__c,
                        Lower_Spec_Limit__c = specification.Lower_Spec_Limit__c,
                        Upper_Spec_Limit__c = specification.Upper_Spec_Limit__c,
                        Inspection_Method__c = specification.Inspection_Method__c,
                        SQX_Equipment__c = specification.SQX_Equipment__c);
                    inspectionDetailToBeAdded.add(newInspectionDetail);
                }
            return inspectionDetailToBeAdded;
        }
    }
}