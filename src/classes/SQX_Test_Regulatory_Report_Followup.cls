@IsTest
public class SQX_Test_Regulatory_Report_Followup {
	
    
    /**
    * Given: A regulatory Report in Pending status.
    * When : Follow up report is created.
    * Then : Custom Validation Exception is thrown.
    */
    @IsTest
    public static void givenPendingReport_WhenNewFollowupIsCreated_ExceptionIsThrown(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
         	   
             // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Status__c = 'Pending'
            );
            
            insert regReport;
            
            // Arrange: Create followup
            SQX_Regulatory_Report__c regReportFollowup = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                SQX_Follow_Up_Of__c = regReport.Id
            );
            
            try{
            	insert regReportFollowup;
            }catch(Exception ex){
				  // Assert: Follow up save should not be successful because the report is in pending status.
                Boolean expectedExceptionThrown =  ex.getMessage().contains(' A pending report cannot be followed up') ? true : false;
                System.assertEquals(expectedExceptionThrown,true);  
            }
        }
    }
    
    /**
    * Given: A regulatory Report with a pending followup.
    * When : When followup record for that report is created.
    * Then : Custom Validation exception is thrown.
    */
    @IsTest
    public static void givenPendingFollowup_WhenNewFollowupIsCreated_ExceptionIsThrown(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
         	
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Status__c = 'Aborted'
            );
            
            insert regReport;
            
            // Arrange: Create followup in pending status
            SQX_Regulatory_Report__c regReportFollowup = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                SQX_Follow_Up_Of__c = regReport.Id,
                Status__c = 'Pending'
            );
            
            insert regReportFollowup;
            
            regReport.SQX_Last_Followup__c =regReportFollowup.Id;
            update regReport;
            
             // Arrange: Create Second followup
            SQX_Regulatory_Report__c secondFollowup = new SQX_Regulatory_Report__c( 
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                SQX_Follow_Up_Of__c = regReport.Id
            );
            
            try{
                // Act: Insert secondFollowup
            	insert secondFollowup;
            }catch(Exception ex){
				  // Assert: Second followup save should not be successful because the first follow up is still in pending status.
                Boolean expectedExceptionThrown =  ex.getMessage().contains('Cannot follow up if last followup is pending') ? true : false;
                System.assertEquals(expectedExceptionThrown,true);  
            }
        }
    }
    
    /**
    * Given: A followup regulatory report.
    * When : When it is further followedup.
    * Then : Custom validation exception is thrown.
    */
    @IsTest
    public static void givenFollowupReport_WhenTriedToFollowup_ExceptionIsThrown(){
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
         	
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Status__c = 'Aborted'
            );
            
            insert regReport;
            
            // Arrange: Create first followup
            SQX_Regulatory_Report__c firstFollowup = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                SQX_Follow_Up_Of__c = regReport.Id,
                Status__c = 'Aborted'
            );
            
            insert firstFollowup;
            
            
            // Arrange: Followup further
            SQX_Regulatory_Report__c secondFollowup = new SQX_Regulatory_Report__c( 
            	Name=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                SQX_Follow_Up_Of__c = firstFollowup.Id
            );
            
            
            try{
                // Act: insert the second followup
            	insert secondFollowup;
            }catch(Exception ex){
				  // Assert: Second followup save should not be successful
                Boolean expectedExceptionThrown =  ex.getMessage().contains('Follow up report cannot be further followed up') ? true : false;
                System.assertEquals(expectedExceptionThrown,true);  
            }

        }
    }
    
    /**
    * Given: A  regulatory report.
    * When : When it is followedup.
    * Then : Followup record should be created and set as last followup.
    */
    @IsTest
    public static void givenRegulatoryReport_WhenFollowupIsCreated_LastFollowupShouldBeSet(){
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
         	
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Report Submission
            SQX_Submission_History__c regSubmission = new SQX_Submission_History__c();
            regSubmission.Status__c='Complete';
            regSubmission.SQX_Complaint__c=complaint.complaint.Id;
            regSubmission.Submitted_By__c='Submitter';
            insert regSubmission;
            
            // Arrange: Create Regulatory Report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c(
                SQX_User__c = standardUser.Id,
                SQX_Complaint__c = complaint.complaint.Id,
                Due_Date__c = Date.today(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                Status__c=SQX_Regulatory_Report.STATUS_PENDING
            );
            
            
             
            insert regulatoryReport;
            
            // Arrange: Create Medwatch.
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c(
             	SQX_Regulatory_Report__c = regulatoryReport.Id,
                Header_Mfr_Report_Number__c='3008382007-2018-70012',
                G9_MFR_Report_Number__c='3008382007-2018-70012',
                F2_UF_Importer_Report_Number__c='0002939301-2018-0700',
                Header_UF_Importer_Report_Number__c='0002939301-2018-0700'
            );
             
            insert medwatch;
            
            // Update: Update the regulatory report
            regulatoryReport.SQX_Submission_History__c=regSubmission.Id;
            regulatoryReport.Status__c='Complete';
            regulatoryReport.Report_Id__c=medwatch.id;
            update regulatoryReport;
            
            // Act: Followup the regulatory report
            List<Id> ids = SQX_Create_Followup.createFollowup(new List<Id>{regulatoryReport.Id});
            
            // Assert: Query the clone medwatch
            SQX_Medwatch__c cloneMedwatch = [SELECT Id,SQX_Regulatory_Report__c, Header_Mfr_Report_Number__c,G9_MFR_Report_Number__c,
            								F2_UF_Importer_Report_Number__c,Header_UF_Importer_Report_Number__c FROM SQX_Medwatch__c WHERE Id=: ids[0]];
            
            // Assert:Find reg report assciated with the medwatch
            SQX_Regulatory_Report__c clonedRegReport = [SELECT Id FROM SQX_Regulatory_Report__c WHERE Id=: cloneMedwatch.SQX_Regulatory_Report__c];
            
            // Assert: Get the original regulatory report
            SQX_Regulatory_Report__c regReportParent = [SELECT Id,SQX_Last_Followup__c FROM SQX_Regulatory_Report__c WHERE Id=: regulatoryReport.Id];
			
            // Assert: Clone report  should be set as last followup of the parent.
            System.assertEquals(clonedRegReport.Id, regReportParent.SQX_Last_Followup__c);
            
            // Assert: Assert the cloned fields of medwatch
            System.assertEquals(medwatch.Header_Mfr_Report_Number__c, cloneMedwatch.Header_Mfr_Report_Number__c);
            System.assertEquals(medwatch.G9_MFR_Report_Number__c, cloneMedwatch.G9_MFR_Report_Number__c);
            System.assertEquals(medwatch.F2_UF_Importer_Report_Number__c, cloneMedwatch.F2_UF_Importer_Report_Number__c); 
            System.assertEquals(medwatch.Header_UF_Importer_Report_Number__c, cloneMedwatch.Header_UF_Importer_Report_Number__c);

        }
    }
}