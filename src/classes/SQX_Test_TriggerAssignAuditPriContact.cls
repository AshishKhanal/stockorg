@istest
public class SQX_Test_TriggerAssignAuditPriContact{
    static testmethod void testtrigger2(){
    
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){

            Account account = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(account);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
            User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            
            integer randomNumber = (integer)(Math.random() * 1000000);
            SQX_Department__c department = new SQX_Department__c();
            SQX_Division__c division = new SQX_Division__c();

            System.runas(adminUser){

            department.Name = 'Department' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                    Schema.SQX_Department__c.Name
                } ) ;

            division.Name = 'Division' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Division__c> {division}, new List<Schema.SObjectField>{
                    Schema.SQX_Division__c.Name
                } ) ;
            }
            SQX_Audit__c newAudit = new SQX_Audit__c();
            newAudit.Audit_Type__c = 'On-Site';
            newAudit.Audit_Result__c = 'Pass';
            newAudit.Audited_On__c = date.parse('12/27/2009');
            newAudit.Lead_Auditor__c = 'Sanjish Singh';
            newAudit.Account__c = account.ID;
            newAudit.Title__c = 'Audit Title';
            newAudit.Audit_Type__c = 'Internal';
            newAudit.Audit_Category__c = 'ISO';
            newAudit.Start_Date__c = Date.Today();
            newAudit.SQX_Auditee_Contact__c = adminUser.Id;
            newAudit.SQX_Department__c = department.Id;
            newAudit.SQX_Division__c = division.Id;
            newAudit.Org_Division__c = 'Sample Division';
            
            insert newAudit;
               
            SQX_Finding__c finding = null;              
            finding = new SQX_Finding__c();
            finding.SQX_Audit__c = newAudit.id;        
            finding.RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Finding,'Audit Finding');
            finding.Due_Date_Response__c = Date.today().addDays(30);
            finding.Due_Date_Containment__c = finding.Due_Date_Investigation__c = finding.Due_Date__c = 
                finding.Due_Date_Response__c;
            
            insert finding;
            
            
            finding = [SELECT ID,SQX_Supplier_Account__c,SQX_Investigator_Contact__c
                       FROM SQX_Finding__c
                       WHERE ID =: finding.ID ];
            system.assert(newAudit.Account__c == finding.SQX_Supplier_Account__c, 'Account Name Mismatch');
            system.assert(newAudit.Primary_Contact_Name__c == finding.SQX_Investigator_Contact__c, 
                          'Primary Contact Name Mismatch');
                          
              }
            
        }
 

}