/**
* unit test for tasks delete when NSI record is void and open deviation process changes to draft
*/
@isTest
public class SQX_Test_5757_Delete_Tasks {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
    }
    
    /**
    * GIVEN : given supplier deviation record with deviation process and SF Tasks
    * WHEN : void the record
    * THEN : delete SF tasks and open deviation process change to draft
    */
    public static testMethod void GivenSupplierDeviation_WhenVoidTheRecord_ThenDeleteTasksAndDeviationProcessOpenChangeToDraft(){ 
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            // ARRANGE: Create Supplier Deviation and policy Tasks 
            SQX_Test_Supplier_Deviation.createPolicyTasks(2, 'Task', adminUser, 1);
            SQX_Test_Supplier_Deviation.createPolicyTasks(2, 'Task', adminUser, 2);
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation().save();
            sd.submit();
            
            System.assertEquals(4, [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sd.sd.Id].size());
            
            // ACT: Supplier Deviation is initiated
            sd.initiate();
            
            // ASSERT: SF Task with lowest step are open
            System.assertEquals(2, [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sd.sd.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
            
            // ASSERT: Supplier Deviation Status is in progress
            System.assertEquals(SQX_Supplier_Deviation.WORKFLOW_STATUS_IN_PROGRESS, [SELECT Id, Workflow_Status__c FROM SQX_Supplier_Deviation__c WHERE Id =: sd.sd.Id].Workflow_Status__c);
            
            //Assert: Tasks created
            System.assertEquals(2, [SELECT Id FROM Task WHERE WhatId =: sd.sd.Id].size());
            
            //Act: Supplier Deviation is void
            sd.void();
            
            //Assert: Ensured that tasks deleted and open deviation process changed to draft
            System.assertEquals(0, [SELECT Id FROM Task WHERE WhatId =: sd.sd.Id].size());
            System.assertEquals(0, [SELECT Id FROM SQX_Deviation_Process__c WHERE SQX_Parent__c =: sd.sd.Id  AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
        } 
    }
    
    /**
    * GIVEN : given Supplier Deviation record with Deviation proess step and SF Task
    * WHEN : void the record
    * THEN : clear audit number in deviation process step
    */
    public static testMethod void GivenSupplierDeviation_WhenVoidTheRecord_ThenClearAuditNumberInOnboardingStep(){ 
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> { adminUser });
        System.runAs(adminUser){
            // ARRANGE: Create Supplier Deviation and policy Tasks 
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Task.TASK_TYPE_PERFORM_AUDIT, adminUser, 1);
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation().save();
            sd.submit();
            sd.initiate();
            
            // ACT: Add audit number in deviation process step
            SQX_Test_Audit auditRecord = new SQX_Test_Audit().save();
            SQX_Deviation_Process__c devProcRecord = [SELECT Id,SQX_Audit_Number__c FROM SQX_Deviation_Process__c];
            devProcRecord.SQX_Audit_Number__c = auditRecord.audit.id;
            update devProcRecord;
            System.assertEquals(auditRecord.audit.id, [Select sqx_audit_number__c from SQX_Deviation_Process__c].sqx_audit_number__c);
            
            //Act: Supplier Deviation is voided
            sd.void();
            
            //Assert: Ensure that perform audit deviation process step have null value in audit_number
            System.assertEquals(null, [Select sqx_audit_number__c from SQX_Deviation_Process__c].sqx_audit_number__c,'Expected, audit number to be blank if NSI is voided');

        } 
    }
}