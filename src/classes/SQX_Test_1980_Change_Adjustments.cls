@isTest
public class SQX_Test_1980_Change_Adjustments{
    
    static Boolean runAllTests = true,
                   run_giveADocWhenPendingTraining_PendingDocumnetIsAdded = false,
                   run_giveADocWhenPendingTrainingIsCompleted_PendingDocumnetIsRemoved = false,
                   run_giveADocWhenPendingTrainingIsDeleted_PendingDocumnetIsRemoved = false,
                   run_giveADocWhenPendingTrainingIsUnDeleted_PendingDocumnetIsAdded = false,
                   run_giveADocWhenTaskIsCompleted_CompletionDateAndCompletedByAreGenerated = false,
                   run_givenADocWithPendingTraining_WhenTrainingIsObsoleted_PendingTraingCountIsUpdated = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        System.runas(adminUser) {
            // add required job function
            insert new SQX_Job_Function__c( Name = 'JF1' );
        }
    }
    
    /**
     * ACTION: Create a controlled doc with status set as current and add a pending personnel document training
     * EXPECTED: Pending document training to be set as 1
     *           Ensures new requirement of the controlled document is not activated
     * @date: 2016-04-19
     * @story: [SQX-1980]
    */
    public static testmethod void run_giveADocWhenPendingTraining_PendingDocumnetIsAdded() {
        if (!runAllTests && !run_giveADocWhenPendingTraining_PendingDocumnetIsAdded) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        
        System.runas(standardUser) {
            Database.SaveResult result1 = null;
            
            // Arrange : add required personnel record
            //              add required current controlled doc
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = cDoc1.doc.Effective_Date__c = System.today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            // get required job function
            SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = 'JF1'];
            
            // add new inactive requirement
            SQX_Requirement__c req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            insert req1;

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: add personnel document training with pending status
            SQX_Personnel_Document_Training__c DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            result1 = Database.insert(DocT1, false);

            SQX_Controlled_Document__c savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            // Assert : Expected 1 pending document training to be found
            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');
            
            req1 = [SELECT Id, Name, Active__c FROM SQX_Requirement__c WHERE Id = :req1.Id];
            System.assertEquals(false, req1.Active__c, 'Requirements of controlled document are expected not to be activated.');
            
        }
    }
    /**
     * ACTION: Create a controlled doc with status set as current and add a complete the pending personnel document training
     * EXPECTED: Pending document training to be set as 0
     *           Ensures new requirement of the controlled document is not activated
     * @date: 2016-04-19
     * @story: [SQX-1980]
    */
    public static testmethod void giveADocWhenPendingTrainingIsCompleted_PendingDocumnetIsRemoved() {
        if (!runAllTests && !run_giveADocWhenPendingTrainingIsCompleted_PendingDocumnetIsRemoved) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            Database.SaveResult result1 = null;
            
            // Arrange : add required personnel record
            //              add required current controlled doc
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = cDoc1.doc.Effective_Date__c = System.today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Personnel_Document_Training__c DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            result1 = Database.insert(DocT1, false);

            SQX_Controlled_Document__c savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');
            
            // get required job function
            SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = 'JF1'];
            
            // add new inactive requirement
            SQX_Requirement__c req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            insert req1;

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // ACT: add personnel document training with complete status
            DocT1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            DocT1.Completion_Date__c = System.today() - 1;

            result1 =  Database.update(DocT1, false);

            System.assertEquals(true, result1.isSuccess(), 'error:' + result1.getErrors());

            savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Assert : Expected 0 pending document training to be found
            System.assertEquals(0, savedDoc.Pending_Document_Training__c, 'Expected zero pending doc to be found');
            
            req1 = [SELECT Id, Name, Active__c FROM SQX_Requirement__c WHERE Id = :req1.Id];
            System.assertEquals(false, req1.Active__c, 'Requirements of controlled document are expected not to be activated.');
        }
    }

    /**
     * ACTION: Create a controlled doc with status set as current and delete the pending personnel document training
     * EXPECTED: Pending document training to be set as 0
     *           Ensures new requirement of the controlled document is not activated
     * @date: 2016-04-19
     * @story: [SQX-1980]
    */
    public static testmethod void giveADocWhenPendingTrainingIsDeleted_PendingDocumnetIsRemoved() {
        if (!runAllTests && !run_giveADocWhenPendingTrainingIsDeleted_PendingDocumnetIsRemoved) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            Database.SaveResult result1 = null;
            
            // Arrange : add required personnel record
            //              add required current controlled doc
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = cDoc1.doc.Effective_Date__c = System.today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Personnel_Document_Training__c DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            result1 = Database.insert(DocT1, false);

            SQX_Controlled_Document__c savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');
            
            // get required job function
            SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = 'JF1'];
            
            // add new inactive requirement
            SQX_Requirement__c req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            insert req1;

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act : Delete the pending doucment training
            Database.DeleteResult result2 =  Database.delete(DocT1, false);

            System.assertEquals(true, result2.isSuccess(), 'error:' + result2.getErrors());

            savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Assert: Expected 0 pending document training to be found
            System.assertEquals(0, savedDoc.Pending_Document_Training__c, 'Expected zero pending doc to be found');
            
            req1 = [SELECT Id, Name, Active__c FROM SQX_Requirement__c WHERE Id = :req1.Id];
            System.assertEquals(false, req1.Active__c, 'Requirements of controlled document are expected not to be activated.');
        }
    }

    /**
     * ACTION: Create a controlled doc with status set as current and delete the pending personnel document training and undelete it
     * EXPECTED: Pending document training to be set as 1
     *           Ensures new requirement of the controlled document is not activated
     * @date: 2016-04-19
     * @story: [SQX-1980]
    */
    public static testmethod void giveADocWhenPendingTrainingIsUnDeleted_PendingDocumnetIsAdded() {
        if (!runAllTests && !run_giveADocWhenPendingTrainingIsUnDeleted_PendingDocumnetIsAdded) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            Database.SaveResult result1 = null;
            
            // Arrange : add required personnel record
            //              add required current controlled doc
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.doc.Date_Issued__c = cDoc1.doc.Effective_Date__c = System.today();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            
            SQX_Personnel_Document_Training__c DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            result1 = Database.insert(DocT1, false);

            SQX_Controlled_Document__c savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            Database.DeleteResult result2 =  Database.delete(DocT1, false);

            System.assertEquals(true, result2.isSuccess(), 'error:' + result2.getErrors());

            savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            System.assertEquals(0, savedDoc.Pending_Document_Training__c, 'Expected zero pending doc to be found');
            
            // get required job function
            SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c WHERE Name = 'JF1'];
            
            // add new inactive requirement
            SQX_Requirement__c req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Active__c = false
            );
            insert req1;

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // Act: Undelete the pending docuement training
            Database.UndeleteResult  result3 =  Database.undelete(DocT1, false);

            System.assertEquals(true, result3.isSuccess(), 'error:' + result3.getErrors());

            savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Assert : Expected one pending doucment training to be found
            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');
            
            req1 = [SELECT Id, Name, Active__c FROM SQX_Requirement__c WHERE Id = :req1.Id];
            System.assertEquals(false, req1.Active__c, 'Requirements of controlled document are expected not to be activated.');
        }
    }

    /**
     * ACTION:  Create Document Implementation is created and Referred Controlled Document is in Pre-release state
     * EXPECTED: Create Document Implementation is completed and completed by and completion date is generated.
     * @date: 2016-05-18
     * @story: [SQX-1980]
    */
    public testmethod static void giveADocWhenTaskIsCompleted_CompletionDateAndCompletedByAreGenerated(){
        if(!runAllTests && !run_giveADocWhenTaskIsCompleted_CompletionDateAndCompletedByAreGenerated){
        return;
        }   
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        User adminUser = userMap.get('adminUser');
            
        SQX_Test_Controlled_Document doc1 = new SQX_Test_Controlled_Document();
        SQX_Test_Change_Order changeOrder;
        SQX_Implementation__c ccAction = new SQX_Implementation__c();

        System.runas(adminUser){
            //Add a controlled doc
            doc1.save();
        }
        System.runAs(standardUser){
            //Arrange : Add a controlled doc, add implementation which directs to create a controlled doc
            changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();

            //add Action 
            ccAction = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Create a New Document Please', Date.Today().addDays(1));//recordTypeId, assignee, description, dueDate

            ccAction.SQX_Controlled_Document_New__c = doc1.doc.Id;
            ccAction.Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;

            insert ccAction;
        }
        System.runAs(adminUser){

            doc1.doc.SQX_Change_Order__c = changeOrder.changeOrder.Id;

            //Act : Set Status of comtrolled Doc to Pre-Release
            doc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            ccAction = [Select Id,  Status__c, Completion_Date__c, Completed_by__c from SQX_Implementation__c where Id = :ccAction.Id];

            User userInfo = [SELECT Name FROM User WHERE Id =: adminUser.Id];

            //Assert : Verify that releated implementation is complete and completion date and completed by are generated
            System.AssertEquals(SQX_Implementation.STATUS_COMPLETE, ccAction.Status__c);
            System.AssertEquals(Date.Today(), ccAction.Completion_Date__c);
            System.AssertEquals(userInfo.Name, ccAction.Completed_by__c);

        }
    }


    /**
     * ARRANGE: Create a controlled doc with status set as current and add the pending personnel document training
     * ACT: Obsolete the pending document training
     * EXPECTED: Pending document training to be set as 0
     * @story: [SQX-2378]
    */
    public static testmethod void givenADocWithPendingTraining_WhenTrainingIsObsoleted_PendingTraingCountIsUpdated() {
        if (!runAllTests && !run_givenADocWithPendingTraining_WhenTrainingIsObsoleted_PendingTraingCountIsUpdated) {
            return;
        }
        
        // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser = userMap.get('standardUser');
        
        System.runas(standardUser) {
            Database.SaveResult result1 = null;
            
            // Arrange : Create a controlled doc and pending personnel document training
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            
            SQX_Personnel_Document_Training__c DocT1 = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            result1 = Database.insert(DocT1, false);

            SQX_Controlled_Document__c savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            System.assertEquals(1, savedDoc.Pending_Document_Training__c, 'Expected one pending doc to be found');

            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            // ACT: Obsolete the pending document training
            update new SQX_Personnel_Document_Training__c(Id = DocT1.Id, Status__c = SQX_Personnel_Document_Training.STATUS_OBSOLETE);


            // Assert: Ensure that the document has zero pending training
            savedDoc = [SELECT Id, Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
           System.assertEquals(0, savedDoc.Pending_Document_Training__c, 'Expected zero pending doc to be found');
        }
    }

}