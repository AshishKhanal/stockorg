/**
 * Ensures that trigger inconsistency due to cached (static) variable in base doesn't occur.
 * Following scenario elaborates one such scenario where partial save causes the issue.
 *
 * Let us assume that DOC A[1] which is current
 * Let us assume that we have two records DOC A[2] and DOC B[1] former is release queued
 * and latter is pre-release queued.
 *
 * However, DOC B[1] has effective date set to past date (Causes error). Responsible for
 * partial rollback in trigger.
 *
 * When batch picks the documents DOC A[2] and DOC B[1], DOC B[1] causes rollback and DOC A[2]
 * gets processed. However, since the trigger framework internally marked A[2] as processed prior
 * to rollback. It doesn't perform all actions such as Obsoleting DOC A[1], hence resulting in
 * two current documents
 */
@IsTest
public class SQX_Test_5804_TriggerInconsistency {

    /**
     * Given Two Documents in Batch Queued
     *          DOC A[2] - Release Queued
     *          DOC B[1] - Pre-release Queued, Effective Date: Today() - 2
     *          DOC A[1] - Current
     * When Batch executes with one document failing DOC B[1] because issue date will be greater than effective
     * Then DOC A[2] is released and DOC A[1] is obsoleted and consistency is seen
     */
    static testmethod void givenTwoDocsInBatchQueued_WhenOneDocFails_ThenOtherDocIsConsistent() {
        // Arrange: Create A[1] in current, A[2] in release queued and B[1] in pre-release queued with effective date today() - 2
        //          Batch is enabled too

        SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
            Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
        });

        SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document();
        a1.doc.Document_Number__c = 'A';
        a1.doc.Revision__c = '1';
        a1.doc.Date_Issued__c = Date.today().addDays(-10);
        a1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
        a1.save();

        SQX_Test_Controlled_Document b1 = new SQX_Test_Controlled_Document();
        b1.doc.Document_Number__c = 'B';
        b1.doc.Effective_Date__c = Date.today().addDays(-2);
        b1.save();

        b1.setStatus(SQX_Controlled_Document.STATUS_APPROVED);
        b1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
        b1.save();

        SQX_Test_Controlled_Document a2 = a1.revise();
        a2.doc.Effective_Date__c = Date.today();
        a2.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
        a2.save();
        
        System.debug('A[1]: ' + a1.doc.Id + ' A[2]: ' + a2.doc.Id + ' B[1]: ' + b1.doc.Id);
        
        SQX_BulkifiedBase.clearAllProcessedEntities();

        // Act: Execute batch job
        Test.startTest();
        Database.executeBatch(new SQX_Document_Batch_Job_Processor(), 2);
        Test.stopTest();

        // Assert: Ensure A[1] is obsoleted, A[2] is released and B[2] has error
        Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Document_Status__c, Batch_Job_Status__c, Batch_Job_Error__c
                                                    FROM SQX_Controlled_Document__c
                                                    WHERE Id IN : new Id[] {a1.doc.Id, a2.doc.Id, b1.doc.Id }]);
        
        System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, docs.get(a2.doc.Id).Document_Status__c, docs.get(a2.doc.Id));
        System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, docs.get(a1.doc.Id).Document_Status__c, docs.get(a1.doc.Id));
        System.assertEquals(SQX_Controlled_Document.BATCH_PRE_RELEASE_ERROR, docs.get(b1.doc.Id).Batch_Job_Status__c, docs.get(b1.doc.Id));
    }
}