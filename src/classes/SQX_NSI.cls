/**
 * class to perform actions for NSI
 */
public with sharing class SQX_NSI {

    //Static text of proposed status
    public static final String PROPOSED_STATUS_APPROVED = 'Approved',
                                PROPOSED_STATUS_CONDITIONALLY_APPROVED = 'Conditionally Approved',
                                PROPOSED_STATUS_REJECTED = 'Rejected';
    
    //Static text of NSI status
    public static final String STATUS_DRAFT = 'Draft',
                                STATUS_OPEN = 'Open',
                                STATUS_COMPLETE = 'Complete',
                                STATUS_CLOSED = 'Closed',
                                STATUS_VOID ='Void';
    
    //Static text of NSI stage
    public static final String STAGE_DRAFT = 'Draft',
                                STAGE_TRIAGE = 'Triage',
                                STAGE_ONHOLD = 'On Hold',
                                STAGE_IN_PROGRESS = 'In Progress',
                                STAGE_APPROVAL = 'Approval',
                                STAGE_VERIFICATION = 'Verification',
                                STAGE_CLOSED = 'Closed';
    
    //Static text of workflow status
    public static final String WORKFLOW_STATUS_NOT_STARTED = 'Not Started',
                                WORKFLOW_STATUS_ON_HOLD = 'On Hold',
                                WORKFLOW_STATUS_IN_PROGRESS = 'In Progress',
                                WORKFLOW_STATUS_COMPLETED = 'Complete';
     
    //Static text of NSI result                           
    public static final String NSI_RESULT_REJECTED = 'Rejected',
                                NSI_RESULT_CONDITIONALLY_APPROVED ='Conditionally Approved',
                                NSI_RESULT_APPROVED = 'Approved';     

    //Statis text of task type
    public static final String TASK_TYPE_TASK = 'Task',
                                TASK_TYPE_DOC_REQUEST = 'Document Request',
                                TASK_TYPE_PERFORM_AUDIT = 'Perform Audit',
                                TASK_TYPE_APPROVAL = 'Approval';
    
     /** 
     * NSI Trigger Handler
     * Bulkified class to handle the actions performed by the trigger by bulkifying data
     */
    public with sharing class Bulkified extends SQX_Supplier_Trigger_Handler{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newNSIs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_New_Supplier_Introduction__c> newNSIs, Map<Id,SQX_New_Supplier_Introduction__c> oldMap){
            
            super(newNSIs, oldMap);
        }

        /**
        *  Method executes all actions that are run in Before Trigger context
        */
        protected override void onBefore() {

        }

        /**
        *  Method executes all actions that are run in After Trigger context
        */
        protected override void onAfter() {
            if(Trigger.isUpdate) {
                transferTheInformationWhenNSIClosed();
                createSupplierPartAndServices();
            }
        }


        /**
        * method to tranfer the informatin(account, contact and task ) when NSI is closed
        */
        public Bulkified transferTheInformationWhenNSIClosed(){
            final String ACTION_NAME = 'transferTheInformationWhenNSIClosed';
            Rule closeNSIRule = new Rule();
            closeNSIRule.addRule(Schema.SQX_New_Supplier_Introduction__c.Status__c, RuleOperator.Equals, STATUS_CLOSED);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.NSI, ACTION_NAME)
                .applyFilter(closeNSIRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            List<SQX_New_Supplier_Introduction__c> nsis = (List<SQX_New_Supplier_Introduction__c>) closeNSIRule.evaluationResult;
            
            if(nsis.size() > 0){
                SQX_NSI.informationTransfer(nsis);
            }
            return this;
        }
        
        /**
         * @story: [SQX-6150]
         * @desc: When Account is closed with approved or conditionally approved result, new Supplier part and services
         *        is created each referring nsi part and nsi service respectively.
         */ 
        public Bulkified createSupplierPartAndServices(){
            final String ACTION_NAME = 'createSupplierPartAndServices';
            this.resetView();
            
            Rule closedAndApprovedNSIRule = new Rule();
            closedAndApprovedNSIRule.addRule(Schema.SQX_New_Supplier_Introduction__c.Status__c, RuleOperator.Equals, SQX_NSI.STATUS_CLOSED);
            closedAndApprovedNSIRule.addRule(Schema.SQX_New_Supplier_Introduction__c.Result__c, RuleOperator.NotEquals, SQX_NSI.NSI_RESULT_REJECTED);
            closedAndApprovedNSIRule.addRule(Schema.SQX_New_Supplier_Introduction__c.SQX_Account__c, RuleOperator.NotEquals, NULL);
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.NSI, ACTION_NAME)
                .applyFilter(closedAndApprovedNSIRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            
            List<SQX_New_Supplier_Introduction__c> nsiResult = (List<SQX_New_Supplier_Introduction__c>) closedAndApprovedNSIRule.evaluationResult;
            List<SQX_Supplier_Service__c> supplierServiceList = new List<SQX_Supplier_Service__c>();
            List<SQX_Supplier_Part__c> supplierPartList = new List<SQX_Supplier_Part__c>();
            Map<Id, SQX_Supplier_Service__c> nsiAndSuppServiceMap = new Map<Id, SQX_Supplier_Service__c>();
            Map<Id, SQX_Supplier_Part__c> nsiAndSuppPartMap = new Map<Id, SQX_Supplier_Part__c>();
            //List of nsi records that are closed, approved/conditionally approved and account is not null
            if(nsiResult.size() > 0){
                this.addToProcessedRecordsFor(SQX.NSI, ACTION_NAME, nsiResult);
                
                Action act = getAction();
                Set<Id> accountIds = new Set<Id>();
                Set<Id> partIds = new Set<Id>();
                Set<Id> serviceIds = new Set<Id>();
                
                for(SQX_New_Supplier_Introduction__c nsi : nsiResult) {
                    accountIds.add(nsi.SQX_Account__c);
                    partIds.add(nsi.SQX_Part__c);
                    serviceIds.add(nsi.SQX_Standard_Service__c);
                }
                Map<Id, Account> accounts = new Map<Id, Account>([SELECT Id, (SELECT Id, Part__c, Account__c, Active__c FROM Supplier_Parts__r WHERE Part__c IN: partIds), 
                                          (SELECT Id, Standard_Service__c, Account__c, Active__c FROM Supplier_Services__r WHERE Standard_Service__c IN: serviceIds) 
                                           FROM Account WHERE Id IN: accountIds]);
                List<SQX_Supplier_Service__c> uptServices = new List<SQX_Supplier_Service__c>();
                List<SQX_Supplier_Part__c> uptParts = new List<SQX_Supplier_Part__c>();
                for(SQX_New_Supplier_Introduction__c nsi : nsiResult) {
                    Boolean serviceExists =false;
                    Boolean partExists =false;
                    for(SQX_Supplier_Service__c ss : accounts.get(nsi.SQX_Account__c).Supplier_Services__r){
                        if(ss.Standard_Service__c == nsi.SQX_Standard_Service__c){
                            serviceExists = true;
                            if(ss.Active__c == false){
                                SQX_Supplier_Service__c obj = new SQX_Supplier_Service__c();
                                obj.Id = ss.Id;
                                obj.Standard_Service__c = ss.Standard_Service__c;
                                obj.Active__c = true;
                                uptServices.add(obj); 
                            }
                        }
                    }
                    for(SQX_Supplier_Part__c sp : accounts.get(nsi.SQX_Account__c).Supplier_Parts__r){
                            if(sp.Part__c == nsi.SQX_Part__c){
                                partExists = true;
                                if(sp.Active__c == false){
                                    SQX_Supplier_Part__c obj = new SQX_Supplier_Part__c();
                                    obj.Id = sp.Id;
                                    obj.Part__c = sp.Part__c;
                                    obj.Active__c = true;
                                    uptParts.add(obj);
                                }
                            }
                        }
                  
                    if(nsi.SQX_Standard_Service__c != null && serviceExists == false) {

                        SQX_Supplier_Service__c supplierService = new SQX_Supplier_Service__c(account__c = nsi.SQX_Account__c, 
                                                    Standard_Service__c=nsi.SQX_Standard_Service__c, approved__c = true, active__c = true);
                        nsiAndSuppServiceMap.put(nsi.id, supplierService);
                        supplierServiceList.add(supplierService);
                        
                        act.op_insert(nsi, supplierService);
                    }
                    
                    if(nsi.SQX_Part__c != null && partExists == false) {
                        SQX_Supplier_Part__c supplierPart = new SQX_Supplier_Part__c(account__c = nsi.SQX_Account__c, Part__c = nsi.SQX_Part__c,
                                                 approved__c = true, active__c = true);
                        nsiAndSuppPartMap.put(nsi.id, supplierPart);
                        supplierPartList.add(supplierPart);

                        act.op_insert(nsi, supplierPart);
                    }
                }
                act.op_commit();
                
                if(uptParts.size() > 0){
                    new SQX_DB().op_update(uptParts, new List<SobjectField>{});
                }
                if(uptServices.size() > 0){
                    new SQX_DB().op_update(uptServices, new List<SobjectField>{});
                }
                
                //Update Supplier Service and Supplier Part references of NSI
                List<SQX_New_Supplier_Introduction__c> nsiToUpdate = new List<SQX_New_Supplier_Introduction__c>();
                Boolean hasUpdate = false;
                for(SQX_New_Supplier_Introduction__c nsi : nsiResult){
                    SQX_New_Supplier_Introduction__c nsiUpdate = new SQX_New_Supplier_Introduction__c(Id = nsi.Id);
                    hasUpdate = false;
                    if(nsiAndSuppServiceMap.get(nsi.Id) != null && nsiAndSuppServiceMap.get(nsi.Id).id != null) {
                        hasUpdate = true;
                        nsiUpdate.Id = nsi.Id;
                        nsiUpdate.Supplier_Service__c = nsiAndSuppServiceMap.get(nsi.Id).id;
                    }
                        
                    if(nsiAndSuppPartMap.get(nsi.Id) != null && nsiAndSuppPartMap.get(nsi.Id).id != null) {
                        hasUpdate = true;
                        nsiUpdate.Id = nsi.Id;
                        nsiUpdate.Supplier_Part__c = nsiAndSuppPartMap.get(nsi.Id).id;
                    }  
                    if(hasUpdate){
                        nsiToUpdate.add(nsiUpdate);
                    }
                }
                if(nsiToUpdate.size() > 0){
                    new SQX_DB().op_update(nsiToUpdate, new List<SobjectField>{});
                }
            }
            return this;
        }

    }

    /**
    * method to create account, contact and sf task when nsi closed conditionally
    * @param Ids nsi recordId
    */
    private static void informationTransfer(List<SQX_New_Supplier_Introduction__c> nsis){
        
        
        SQX_Trigger_Handler triggerHandler = new SQX_Trigger_Handler();
        SQX_Trigger_Handler.Action act = triggerHandler.getAction();
        Map<Id, SQX_New_Supplier_Introduction__c> nsiTriggerMap = new Map<Id, SQX_New_Supplier_Introduction__c>(nsis);
        // retrieve nsi information
        Map<Id,SQX_New_Supplier_Introduction__c> mapNSI = new Map<Id,SQX_New_Supplier_Introduction__c>([SELECT Id, Result__c, Supplier_Number__c, Location__c,
                                                                                                        Company_Name__c, First_Name__c, Last_Name__c, Job_Title__c, Email__c, Phone__c, SQX_Account__c                                      
                                                                                                        ,Task_Name__c, SQX_Task_Assignee__c, Due_Date__c, Activity_Comment__c,
                                                                                                        (
                                                                                                            SELECT Id, Name,Document_Name__c, Issue_Date__c,Needs_Periodic_Update__c,Notify_Before_Expiration__c,Expiration_Date__c, Due_Date__c, Comment__c,
                                                                                                            SQX_User__c, SQX_Parent__c, SQX_Parent__r.SQX_Part__c, SQX_Parent__r.Part_Name__c, SQX_Parent__r.SQX_Standard_Service__c, SQX_Parent__r.Description__c, SQX_Controlled_Document__c FROM SQX_Steps__r 
                                                                                                            WHERE RecordTypeId =: SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST) and Archived__c=false
                                                                                                        ) 
                                                                                                        FROM SQX_New_Supplier_Introduction__c WHERE Id IN: nsis ]
                                                                                                      );
        if(mapNSI.size()>0){
            // declare variables
            List<Account> accountToInsert = new List<Account>();   
            List<Account> updateList = new List<Account>();
            List<Contact> contactList = new List<Contact>(); 
            List<SQX_Certification__c> sdrList = new List<SQX_Certification__c>();
            Map<Id, Account> mapNSIAccount = new Map<Id, Account>();
            Map<Id, Id> mapNSIPolicy = new Map<Id, Id>();

            Id managedSupplierId= SQX_Utilities.getSupplierRecordType();

            for(Id nsiId : mapNSI.keySet())  {
                SQX_New_Supplier_Introduction__c nsi = mapNSI.get(nsiId);
                Account  account = new Account();
                // check linked account is empty then create account and contact otherwise update account approve status as NSI_Result
                account.Approval_Status__c = nsi.Result__c;
                account.AccountNumber = nsi.Supplier_Number__c;

                if(!String.isBlank(managedSupplierId)){
                    account.put('RecordTypeId', managedSupplierId);
                }

                if(String.isBlank(nsi.SQX_Account__c) && nsi.Result__c != SQX_NSI.NSI_RESULT_REJECTED){
                    account.Name = nsi.Company_Name__c;
                    account.site = nsi.location__c;
                    account.Approval_Status__c = nsi.Result__c;
                    account.Status__c = 'Active';
                    accountToInsert.add(account);
                }else if(!String.isBlank(nsi.SQX_Account__c)){ 
                    account.Id = nsi.SQX_Account__c;
                    account.Approval_Status__c = nsi.Result__c;
                    account.Status__c = 'Active';
                    updateList.add(account);
                }
                mapNSIAccount.put(nsi.Id, account);
                if(nsi.Result__c != SQX_NSI.NSI_RESULT_REJECTED){
                    for(SQX_Onboarding_Step__c wp : nsi.SQX_Steps__r){
                        mapNSIPolicy.put(wp.Id, nsi.Id);
                    }
                }
            }
            
            //Create account
            if(accountToInsert.size()>0){
                new SQX_DB().op_insert(accountToInsert, new List<SobjectField>{ Account.AccountNumber, Account.Name, Account.Site});
            }
            
            //Update account
            if(updateList.size() > 0){
                new SQX_DB().op_update(updateList, new List<SobjectField>{ Account.AccountNumber, Account.Name, Account.Approval_Status__c });
            }
            
            List<SQX_New_Supplier_Introduction__c> nsiList = new List<SQX_New_Supplier_Introduction__c>();
            List<Task> taskList = new List<Task>();
            for(Id nsiId : mapNSI.keySet())  {
                SQX_New_Supplier_Introduction__c nsi = mapNSI.get(nsiId);

                //create contact and supplier document
                if(nsi.Result__c != SQX_NSI.NSI_RESULT_REJECTED){
                    Contact contact = new Contact();
                    contact.AccountId = mapNSIAccount.get(nsiId).Id;
                    contact.FirstName = nsi.First_Name__c;
                    contact.LastName = nsi.Last_Name__c;
                    contact.Title = nsi.Job_Title__c;
                    contact.Phone = nsi.Phone__c;
                    contact.Email = nsi.Email__c;
                    contactList.add(contact);
 
                    for(SQX_Onboarding_Step__c wp : nsi.SQX_Steps__r){
                        SQX_Certification__c sdr = new SQX_Certification__c();
                        sdr.Part_Name__c = wp.SQX_Parent__r.Part_Name__c;
                        sdr.SQX_Part__c = wp.SQX_Parent__r.SQX_Part__c;
                        sdr.SQX_Standard_Service__c = wp.SQX_Parent__r.SQX_Standard_Service__c;
                        sdr.Document_Name__c = wp.Document_Name__c;
                        sdr.Description__c = wp.SQX_Parent__r.Description__c;
                        sdr.Issue_Date__c = wp.Issue_Date__c;
                        sdr.Expire_Date__c = wp.Expiration_Date__c;
                        sdr.Notify_Before_Expiration__c = wp.Notify_Before_Expiration__c;
                        sdr.Needs_Periodic_Update__c = wp.Needs_Periodic_Update__c;
                        sdr.Account__c = mapNSIAccount.get(nsiId).Id;
                        sdr.SQX_Controlled_Document__c = wp.SQX_Controlled_Document__c;
                        sdrList.add(sdr);
                    }
                }
                // update account in NSI
                nsi.SQX_Account__c = mapNSIAccount.get(nsi.Id).Id;
                
                act.op_update(nsiTriggerMap.get(nsi.Id), nsi);
                nsiList.add(nsi);
                
                // create task when nsi result is conditionally approved
                if(nsi.Result__c == SQX_NSI.NSI_RESULT_CONDITIONALLY_APPROVED){
                    Task task = new Task();
                    task.Subject = nsi.Task_Name__c;
                    task.OwnerId = nsi.SQX_Task_Assignee__c;
                    task.ActivityDate = nsi.Due_Date__c;
                    task.Description = nsi.Activity_Comment__c;
                    task.WhatId = mapNSIAccount.get(nsi.Id).Id;
                    taskList.add(task);
                }
            }
            
            // update account id in NSI
            if(accountToInsert.size() > 0){
                act.op_commit();
            }
            
            //Create account contact
            if(contactList.size()>0){
                new SQX_DB().op_insert(contactList, new List<SobjectField>{
                            Contact.AccountId, Contact.FirstName, Contact.LastName, Contact.Title, Contact.Phone, Contact.Email
                        });
            }
            
            // Create supplier document        
            if(sdrList.size() > 0){
                new SQX_DB().op_insert(sdrList, new List<SobjectField>{});
            }
            
            //Create task
            if(taskList.size() > 0){
                new SQX_DB().op_insert(taskList, new List<SobjectField>{ 
                            Task.Subject, Task.ActivityDate, Task.Description, Task.WhatId, Task.OwnerId 
                        });
            }
            
            if(!mapNSIPolicy.isEmpty()){
                Set<Id> onboardingIds = mapNSIPolicy.keySet();
                List<ContentDocumentLink> links = [SELECT ContentDocumentId, ShareType, LinkedEntityId FROM ContentDocumentLink
                                                   WHERE LinkedEntityId IN : onboardingIds];
                
                List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
                for(ContentDocumentLink cdl : links){
                    ContentDocumentLink cdlObj =new ContentDocumentLink();
                    cdlObj.ContentDocumentId = cdl.ContentDocumentId;
                    cdlObj.ShareType =  cdl.ShareType;
                    cdlObj.LinkedEntityId = mapNSIAccount.get(mapNSIPolicy.get(cdl.LinkedEntityId)).Id;
                    cdlList.add(cdlObj);
                }
                // Add attachments
                new SQX_DB().op_insert(cdlList, new List<SObjectField> {
                            ContentDocumentLink.ContentDocumentId,
                            ContentDocumentLink.LinkedEntityId,
                            ContentDocumentLink.ShareType
                        });
             }
        }
        
    }
}