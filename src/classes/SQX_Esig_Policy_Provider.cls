/**
*   @author : Piyush Subedi
*   @description: Class that defines policy provider to be extended by classes requiring policy based esig
*/
public with sharing abstract class SQX_Esig_Policy_Provider {

    final String APPROVE_REJECT = 'approvereject';

    /*
        Property holds an esig policy for the case when Comment is not a required entity for sign off
    */
    protected SQX_CQ_Esig_Policies__c POLICY_NO_COMMENT_REQUIRED {
        get {
            SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
            return new SQX_CQ_Esig_Policies__c(Required__c  = instance.Enabled__c, Comment__c = false);
        }
    }

    /*
		Property holds an esig policy for the case when Comment is required entity for sign off
    */
    protected SQX_CQ_Esig_Policies__c POLICY_COMMENT_REQUIRED {
        get {
            SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
            return new SQX_CQ_Esig_Policies__c(Required__c  = instance.Enabled__c, Comment__c = true);
        }
    }


    /**
        default policy which comment is false when record is in draft
    */
    public static final SQX_CQ_Esig_Policies__c DEFAULT_POLICY_SAVE_NEW_OR_DRAFT_RECORD = new SQX_CQ_Esig_Policies__c(Required__c = false, Comment__c = false);

    /**
        default policy which comment is true when user have to save after draft
    */
    public static final SQX_CQ_Esig_Policies__c DEFAULT_POLICY_SAVE_RECORD_AFTER_DRAFT = new SQX_CQ_Esig_Policies__c(Required__c = true, Comment__c = true);
    /**
        Action name save after draft
    */
    public static final String ACTION_NAME_SAVE_AFTER_DRAFT='saveafterdraft';
    /**
        label of action name
    */
    public static final String ACTION_LABEL_SAVE_RECORD_AFTER_DRAFT_STATUS='Save Record After Draft state';

    /*
     *  The purpose of this method is to add the save after draft record action in esig setting
    */
    public void addActionSaveAfterDraft(){
        this.purposeOfSigMap.put(ACTION_NAME_SAVE_AFTER_DRAFT, ACTION_LABEL_SAVE_RECORD_AFTER_DRAFT_STATUS);
    }

    /**
    * This map will store esig-policies for actions. Default is require esig if enabled, so if esig is not required for a
    * particular action, please add it to the map.
    */
    transient private Map<String, SQX_CQ_Esig_Policies__c> esigPolicies;

    /**
    * The purpose of signature map contains the purpose of signature for the particular action in the UI.
    */
    protected Map<String, String> purposeOfSigMap = new Map<String, String>();

    /**
    * This set with hold all actions for which static policies are added.
    * @see #addStaticPolicy
    */
    private final Set<String> staticPolicies = new Set<String>();

    /**
    * Method to be overridden by extension classes to add default policies
    */
    protected virtual Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        return new Map<String, SQX_CQ_Esig_Policies__c>();
    }

    /**
    * Returns the map containing the map of purpose of signature with their labels
    */
    public Map<String, String> getPurposeOfSig() {
        return this.purposeOfSigMap;
    }

    /**
    * Returns the map of esignature policies based on the action(next-action) that is being performed.
    */
    public Map<String, SQX_CQ_Esig_Policies__c> getEsigPolicy() {
        if(this.esigPolicies == null){
            this.esigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
        }

        return this.esigPolicies;
    }

    public SQX_CQ_Esig_Policies__c getApplicablePolicies(String action) {
        SQX_CQ_Esig_Policies__c policy = null;
        if(action.startsWith(APPROVE_REJECT)) {
            policy = getApprovalPolicy();
        } else {
            policy = getEsigPolicy().get(action);
        }

        return policy;
    }

    protected virtual SQX_CQ_Esig_Policies__c getApprovalPolicy() {
        return getEsigPolicy().get(APPROVE_REJECT);
    }

    public String getApplicablePurposeOfSig(String action) {
        String purpose = null;
        if(action == APPROVE_REJECT) {
            purpose = getApplicableApproveRejectPurpose();
        } else {
            purpose = getPurposeOfSig().get(action);
        }

        return purpose;
    }

    protected virtual String getApplicableApproveRejectPurpose() {
        return getPurposeOfSig().get(APPROVE_REJECT);
    }

    /**
    * method to return serialized pupose of signature map
    */
    public String getPurposeOfSigJSON(){
        return JSON.serialize(this.purposeOfSigMap);
    }


    /*
    * returns the JSON string of all esig policies.
    */
    public String getEsigPoliciesJSON(){
        return JSON.serialize( getEsigPolicy());
    }

    /**
    * Returns the list of policies that are applicable for the current extension
    */
    public Set<String> getStaticPolicies(){
        return this.staticPolicies;
    }

    /*
    * Method to add static policies, such policies will not be listed in the esig policies setup.
    * @param action the action that is used for the esig policy
    * @param policy the policy specifiying whether comment or password or both are required
    */
    protected void addStaticPolicy(String action, SQX_CQ_Esig_Policies__c policy){
        getEsigPolicy().put(action, policy);
        this.staticPolicies.add(action);
    }

    /**
    * Method to initialize all the esig policies in the esig policies map
    */
    protected void addEsigPolicies(String mainRecordType){


        //add default policies
        getEsigPolicy().putall(getDefaultEsigPolicies());

        for(SQX_CQ_Esig_Policies__c esigPolicy : [SELECT Comment__c, Required__c, Action__c FROM SQX_CQ_Esig_Policies__c WHERE Object_Type__c =: mainRecordType]){
            this.esigPolicies.put(esigPolicy.Action__c, esigPolicy);
        }
    }

    /**
     * return ploicy when user define the audit in esign with saveafterdraft
     * @param isRecInDraftOrNew validate record is in draft or isnew or not
     * @param objecType is object name in esig setting
    */
    public SQX_CQ_Esig_Policies__c getPolicySaveRecordAfterDraft(Boolean isRecInDraftOrNew,String objectType){

        SQX_CQ_Esig_Policies__c retPolicy;
        if (isRecInDraftOrNew) {

            retPolicy = DEFAULT_POLICY_SAVE_NEW_OR_DRAFT_RECORD;

        } else {

            List <SQX_CQ_Esig_Policies__c> saveAfterDraftPolicy = [SELECT Id, Action__c, Object_Type__c, Required__c, Comment__c
                                                                   FROM SQX_CQ_Esig_Policies__c
                                                                   WHERE Object_Type__c = :objectType AND Action__c =:ACTION_NAME_SAVE_AFTER_DRAFT];

            retPolicy=(saveAfterDraftPolicy.size()==1)? saveAfterDraftPolicy[0] : DEFAULT_POLICY_SAVE_RECORD_AFTER_DRAFT;
        }
        return retPolicy;
    }

    public SQX_Esig_Policy_Provider(){

    }

    public class SQX_EsigAction {
        public SObject sobj {get; private set;}
        public String objType {get; private set;}
        public SObjectType sObjType {get; private set;}
        public String action {get; private set;}


        public SQX_EsigAction(Id objId, String action) {
            sObjType = objId.getSObjectType();
            objType = sobjType.getDescribe().getName();
            sobj = SQX_DynamicQuery.getSObjectWithId(objId);
            this.action = action;
        }

        public SQX_Esig_Signature_Setting getApplicableSetting() {
            SQX_Esig_Signature_setting esig=new SQX_Esig_Signature_setting();

            //some policy check the status of object therefore we are passing an instance of the object
            SQX_Controller_ES_Setting settings = new SQX_Controller_ES_Setting(new Map<SObjectType, SObject> {
                sobjType => sobj
            });
            if(settings.providers.containsKey(objType)) {
                SQX_Esig_Policy_Provider provider = settings.providers.get(objType).get(0); //HACK: for now

                SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getInstance();
                esig.username=(cqES.compliancequest__Ask_UserName__c && cqES.compliancequest__Enabled__c);
                esig.enabled=cqES.compliancequest__Enabled__c;
                esig.required=esig.enabled;
                esig.comment = true;
                
                SQX_CQ_Esig_Policies__c policy = provider.getApplicablePolicies(action);
                if(policy != null) {
                    esig.comment = policy.Comment__c;
                    esig.required= cqES.compliancequest__Enabled__c && policy.Required__c;
                }

                if(cqES.compliancequest__Ask_UserName__c != true || cqES.compliancequest__Enabled__c != true){
                    esig.loggedInAs = SQX_Utilities.getUserDetailsForRecordActivity();
                }

                esig.purposeOfSignature = provider.getApplicablePurposeOfSig(action);
                if(esig.purposeOfSignature == null) {
                    if(objType.equalsIgnoreCase(SQX.PersonnelDocumentTraining)) {
                        // HACK: Since esig setting doesn't have access to training signoff we will be using
                        //       it statically here, once it is added to esig page, the first condition will be met and
                        //       this will be redundant.
                        esig.purposeOfSignature = 'userSignOff'.equalsIgnoreCase(action) ? Label.CQ_UI_Doc_Training_User_Sign_Off
                                    : Label.CQ_UI_PDT_Training_Document_Signoff;
                    } else {
                        esig.purposeOfSignature = 'No purpose found for ' + action + '-' + objType;
                    }
                }
					
                return esig;
            } else {
                throw new SQX_ApplicationGenericException('No matching action found for object ' + objType + ' for action ' + action);
            }
        }
    }

    @AuraEnabled
    public static String validateEsig(Id objId, String action, String username, String password, String comments) {
        String response = 'INVALIDTOKEN';
        try {
            SQX_EsigAction actionable = new SQX_EsigAction(objId, action);
            SQX_Esig_Signature_Setting setting = actionable.getApplicableSetting();

            if(setting.enabled == true && setting.required == true) {
                // validate esig if invalid raise an exception
                if((setting.comment == true && String.isEmpty(comments)) || !new SQX_OauthEsignatureValidation((username==null)?UserInfo.getUserName():username, password).validateCredentials()) {
                    throw new SQX_ApplicationGenericException(Label.SQX_INVALID_USERNAME_PASSWORD);
                }
            }
           
           
            response = 'VALIDATEDTOKEN';
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }

        return response;
    }

    @AuraEnabled
    public static void performActionOnObject(Id objId, String action, String username, String password, String comments, String token) {
        SavePoint sp = null;
        try{
           
            if(token != 'VALIDATEDTOKEN') {
                throw new SQX_ApplicationGenericException(Label.SQX_INVALID_USERNAME_PASSWORD);
            }

            SQX_EsigAction actionable = new SQX_EsigAction(objId, action);

            Map<String, SObjectField> fields = actionable.sObjType.getDescribe().fields.getMap();
            List<SObjectField> fieldsUpdated = new List<SObjectField>();
            if(fields.containsKey('compliancequest__Activity_Comment__c')) {
                actionable.sobj.put('compliancequest__Activity_Comment__c', comments);
                fieldsUpdated.add(fields.get('compliancequest__Activity_Comment__c'));
            }

            if(fields.containsKey('compliancequest__Activity_Code__c')) {
                actionable.sobj.put('compliancequest__Activity_Code__c', action);
                fieldsUpdated.add(fields.get('compliancequest__Activity_Code__c'));
            }
            sp = Database.setSavepoint();

            Database.SaveResult [] saveResults = new SQX_DB().continueOnError().op_update(new List<SObject> { actionable.sobj }, fieldsUpdated);
            if(!saveResults[0].isSuccess()) {
                String errors = '';
                for(Database.Error error: saveResults[0].errors) {
                    errors += error.getMessage();
                }
                throw new SQX_ApplicationGenericException(errors);
            }
        }catch(Exception ex){
            if(sp != null ){
                Database.rollback(sp);
            }
              throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
    * retriveEsigSetting Method is to  retrive the esig policy and its esig setting
    */
    @AuraEnabled(cacheable=true)
    public static SQX_Esig_Signature_Setting retriveEsigSetting(Id objId, String action){

        try{
            SQX_EsigAction actionable = new SQX_EsigAction(objId, action);
            return actionable.getApplicableSetting();
        }catch(Exception ex){
            AuraHandledException e = new AuraHandledException(ex.getMessage() + ex.getStackTraceString());
            e.setMessage(ex.getMessage() + ex.getStackTraceString());
            system.debug(e);
            throw e;
        }
    }

    /**
    * This class represent the esignature setting and its esig provider
    */
    public class SQX_Esig_Signature_Setting{
        @AuraEnabled
        public Boolean username ;
        @AuraEnabled
        public String purposeOfSignature ;
        @AuraEnabled
        public Boolean comment ;
        @AuraEnabled
        public Boolean enabled ;
        @AuraEnabled
        public Boolean required ;
        @AuraEnabled
        public String loggedInAs ;
        @AuraEnabled
        public Boolean showComponent{
              get { return comment == true || required == true;}
        }

    }

}