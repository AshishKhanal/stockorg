/**
* contains common methods, statuses and types for action plans 
* @author Pradhanta Bhandari
* @date 2014/2/18
*/
public with sharing class SQX_Action_Plan {
	/** corrective action plan string type */
    public static final String PLAN_TYPE_CORRECTIVE = 'Corrective';

    /** preventive action plan string type */
    public static final String PLAN_TYPE_PREVENTIVE = 'Preventive';
}