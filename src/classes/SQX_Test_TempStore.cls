/**
* @author Pradhanta Bhandari
* @date 2014/1/28
* @issueNumber
*/
@isTest
public class SQX_Test_TempStore{

    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @acceptanceCriteria
    * @objective a given base and delta object can be stored in temporary store for a user object
    * @expected string is stored and can be fetched correctly
    */
    public static testmethod void givenARecordExistsInTempStore_CorrectValueIsReturned(){
        String valueToStore = 'Hello my name is a test object';
        String deltaToStore = 'asdasd';
        SQX_TempStore.storeObject('User', UserInfo.getUserID(), valueToStore, deltaToStore);
        
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserID());
        
        System.assert(valueToStore == retrievedValue, 
            'Expected stored and retrieved value to match, but found it different. Orignal: ' + valueToStore 
            + ' Fetched Value: ' + retrievedValue); 
            
        retrievedValue = SQX_TempStore.getDeltaObject(UserInfo.getUserID());
        
        System.assert(deltaToStore == retrievedValue, 
            'Expected stored and retrieved value to match, but found it different. Orignal: ' + deltaToStore 
            + ' Fetched Value: ' + retrievedValue); 
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @acceptanceCriteria
    * @objective a blank value should be returned when no temp object has been stored
    * @expected blank value is retrieved
    */
    public static testmethod void givenNothingHasBeendSaved_BlankValueIsReturned(){
        
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserID());
        System.assert(retrievedValue == '', 
            'Expected blank value to be returned but found ' + retrievedValue); 
            
        retrievedValue = SQX_TempStore.getDeltaObject(UserInfo.getUserID());
        System.assert(retrievedValue == '', 
            'Expected blank value to be returned but found ' + retrievedValue); 
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @acceptanceCriteria
    * @objective a stored temp record can be deleted
    * @expected the record is removed from data store
    */
    public static testmethod void givenASavedRecordIsDeleted_BlankValueIsReturned(){
        
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();
        
        //execution of this means a record is created agains the userid ensure
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        System.assert(retrievedValue != '' ,'Expected non null value but found null');
        
        //remove the record
        SQX_TempStore.removeObject(UserInfo.getUserId());
        
        //ensure we get a blank response
        retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        System.assert(retrievedValue == '' ,'Expected null value but found ' + retrievedValue);
        
        
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @acceptanceCriteria
    * @objective a given string object stored is modified 
    * @expected modified string is returned
    */
    public static testmethod void givenARecordExistsInTempStoreIsModified_ChangedValueIsReturned(){
        String newValueToStore = 'this is a new value';
        
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();
        
        //execution of this means a record is created agains the userid ensure
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        System.assert(retrievedValue != '' ,'Expected non null value but found null');
        System.assert(retrievedValue != newValueToStore, 'Test Error: Expected old and new value to be different but found same');
        
        SQX_TempStore.storeObject('User', UserInfo.getUserID(), newValueToStore, null);
        
        retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserID());
        
        System.assert(newValueToStore == retrievedValue, 
            'Expected stored and retrieved value to match, but found it different. Orignal: ' + newValueToStore 
            + ' Fetched Value: ' + retrievedValue); 
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/28
    * @acceptanceCriteria
    * @objective given a record in store is frozen and updates are made
    * @expected frozen record is not modified
    */
    public static testmethod void givenARecordIsFrozen_ChangesArentAppliedToIt(){

        String newValueToStore = 'this is a new value';
        
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();
        
        //execution of this means a record is created agains the userid ensure
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        String baseValueUnchanged = retrievedValue;
        System.assert(retrievedValue != '' ,'Expected non null value but found null');
        
        String key = SQX_TempStore.freezeTempObject(UserInfo.getUserId());
        
        SQX_TempStore.storeObject('User', UserInfo.getUserID(), newValueToStore, null);
        
        retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserID());
        
        System.assert(newValueToStore == retrievedValue, 
            'Expected stored and retrieved value to match, but found it different. Orignal: ' + newValueToStore 
            + ' Fetched Value: ' + retrievedValue); 
            
        SQX_TempStore st = SQX_TempStore.getTempObjectWithAccessKey(key);
        System.assert(st != null, 'Expected a frozen object to exist but found none');
        System.assert(st.baseObject == baseValueUnchanged , 
            'Expected the same value as when frozen but found different [Retrieved Value: ' +
            st.baseObject + '] [Original Value: ' + baseValueUnchanged + ']' );
            
            
        //get store of given type
        //just for coverage :-)
        System.assert(SQX_TempStore.getStoresOfType('User').size() > 0, 'Expected one or more temp store but found none');
    }
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/1/29
    * @acceptanceCriteria
    * @objective given a delta exists for a record in store and is deleted
    * @expected the delta attachment gets deleted while the record still exists
    */
    public static testmethod void givenDeltaIsRemoved_RecordStillExistsAndDeltaIsRemoved(){
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();
        
        //execution of this means a record is created agains the userid ensure
        String retrievedValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        String retrievedDelta = SQX_TempStore.getDeltaObject(UserInfo.getUserId());
        
        System.assert(retrievedDelta != '', 'Expected delta to be non null but found it null');
        
        SQX_TempStore.removeDelta(UserInfo.getUserId());
        
        String afterUpdateValue = SQX_TempStore.getBaseObject(UserInfo.getUserId());
        String afterUpdateDelta = SQX_TempStore.getDeltaObject(UserInfo.getUserId());
        
        System.assert(afterUpdateValue == retrievedValue, 'Expected the original value to remain unchanged but found it changed');
        
        System.assert(afterUpdateDelta == '', 'Expected delta to be cleared but found it not cleared');
        
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/13
    * @acceptanceCriteria
    * @objective ensures that an attachment can be added to temp store object
    * @expected attachment is added
    */
    public static testmethod void givenAttachmentIsAdded_ItIsAdded(){
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();

        //now add an attachment
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('I am testing :-)');
        att.Name = 'Test';
        SQX_TempStore.storeTemporaryAttachment(UserInfo.getUserId(), 'Ramdin', att);

        List<Attachment> atts = SQX_TempStore.getAttachments(UserInfo.getUserId(), 'Ramdin');
        System.assert(atts.size() == 1 && atts.get(0).Name == 'Test', 'Expected an attachment to be stored but found none');

    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/3/13
    * @acceptanceCriteria
    * @objective ensures that attachments in old record are moved to new record when saved
    * @expected attachment is moved to new parent
    * @modifiedby Pradhanta [2014/4/2] - modified to use new reparentAttachmentAndUpdateDelta, previously was passing garbage, but after attachment is reparent it will be removed from the list
    */
    public static testmethod void givenTempStoreObjectsArePersisted_AttachmentsAreMoved(){
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();

        //now add an attachment
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('I am testing :-)');
        att.Name = 'Test324';
        SQX_TempStore.storeTemporaryAttachment(UserInfo.getUserId(), 'Ramdin', att);

        List<Attachment> atts = SQX_TempStore.getAttachments(UserInfo.getUserId(), 'Ramdin');
        System.assert(atts.size() == 1 && atts.get(0).Name == 'Test324', 'Expected an attachment to be stored but found none');

        //now move the attachments
        Account account = SQX_Test_Account_Factory.createAccount();
        //Map<String,sObject> objectsPersisted = new Map<String, sObject>();
        //objectsPersisted.put('Ramdin', account);

        Id attachmentId = att.Id;
        att.Id = null;
        att.ParentId = account.Id;
        att.Id = attachmentId;
        String changeSet = '{"changeSet": [' + JSON.serialize(att) + ']}';

        Test.startTest();

        SQX_Upserter.UpsertRecords(changeSet);
        //
        //SQX_TempStore.reparentAttachmentAndUpdateDelta(UserInfo.getUserId(), objectsPersisted, '{ "changeSet": [{"Id" : "RandomObject"},{"Id" : "' + atts.get(0).Id + '"}]}');

        atts = [SELECT Id, Name FROM Attachment WHERE ParentID = : account.Id AND Name = 'Test324'];
        System.assert(atts.size() == 1, 'Expected the attachment to be moved to the account, but could not find it');

        //String newDelta = SQX_TempStore.getDeltaObject(UserInfo.getUserId()) ;

        //System.assert(newDelta == '{"changeSet":[{"Id":"RandomObject"}]}',
        //    'Expected new delta to be stored but could not find it ' + newDelta);
        Test.stopTest();

    }
        
    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/3
    * @acceptanceCriteria
    * @objective ensures that an added attachment can be deleted from the record
    * @expected attachment is deleted
    */
    public static testmethod void givenAnTemporaryAttachmentExists_ItCanBeRemoved(){
        //execute base test to create a record
        givenARecordExistsInTempStore_CorrectValueIsReturned();

        //now add an attachment
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('I am testing :-)');
        att.Name = 'Test324';
        SQX_TempStore.storeTemporaryAttachment(UserInfo.getUserId(), 'Ramdin', att);

        List<Attachment> atts = SQX_TempStore.getAttachments(UserInfo.getUserId(), 'Ramdin');
        System.assert(atts.size() == 1 && atts.get(0).Name == 'Test324', 'Expected an attachment to be stored but found none');

        Test.startTest();

        //now delete an attachment

        boolean deletionResult = SQX_TempStore.removeTemporaryAttachment(UserInfo.getUserId(), 'Ramdin', (String) atts.get(0).Id);

        System.assert(deletionResult, 'Expected the temporary attachment to be deleted but failed in doing so');

        List<Attachment> attachments = [SELECT Id FROM Attachment WHERE Id = : atts.get(0).Id];

        System.assert(attachments.size() == 0, 'Expected the temporary attachment to be actually deleted, but found ' + attachments);

        Test.stopTest();
    }
    
    /**
    * This test ensures that large attachments are also reparented properly
    */
    public testmethod static void givenLargeAttachmentsItIsReparented(){
        integer NUMBER_OF_ATTACHMENTS = 30;
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random');
        insert partFamily;
        
        SQX_TempStore.storeObject('PartFamily', partFamily.Id, '', '');
        
        String superLargeString = '';
        StaticResource sr = [select id,body, bodylength from StaticResource Where Name = 'CAPASchema'];
        String contents = sr.body.toString();
        integer size = 0;

        while(size < 2*1024*1024){ //2M
            superLargeString += contents;
            size += sr.bodyLength;
        }
        
        contents = null;
        sr.body = null;
        sr = null;
        
        
        //add 10 large attachments
        Attachment att = new Attachment();
        att.Name = 'asdd.txt';
        att.Body = Blob.valueOf(superLargeString);
        
        superLargeString = null;
        
        Map<String, Object> changeSetJSON = new Map<String, Object>();
        List<Object> allChanges = new List<Object>();
        
        for(integer count = 0; count < NUMBER_OF_ATTACHMENTS ; count++){
            SQX_TempStore.storeTemporaryAttachment(partFamily.Id, partFamily.Id, att);
            Attachment clonedAtt = att.clone(true); //clone the attachment because you don't want to serialize the body
            clonedAtt.Body = null;
            
            Map<String, Object> attJSON = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(clonedAtt));
            attJSON.put('ParentId', partFamily.Id);
            allChanges.add(attJSON);
            
            att.Id = null;
        }
        att = null;
        
        changeSetJSON.put('changeSet', allChanges);
        
        
        //Map<String, sObject> persistedMap = new Map<String, sObject>();
        //persistedMap.put(partFamily.Id, partFamily);
        //SQX_TempStore.reparentAttachmentAndUpdateDelta(partFamily.Id, persistedMap, '');
        
        SQX_Upserter.upsertRecords(JSON.serialize(changeSetJSON));
        
        System.debug([SELECT Name FROM Attachment WHERE ParentID = : partFamily.Id]);
        
        System.assertEquals( NUMBER_OF_ATTACHMENTS, [SELECT Id FROM Attachment WHERE ParentID = : partFamily.Id].size());
        
    }
}