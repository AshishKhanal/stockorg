/**
* this is a common base class for all the sObject classes to implement an action when triggered.
*               All the actions are provided in this class have follow bulkified pattern.
* @author Pradhanta Bhandari
* @date 2014/5/3
*/
public with sharing virtual class SQX_BulkifiedBase {

    /************** Properties *******************/
    public Map<Id, sObject> oldValues { get; set; } //Equivalent to what triggers stores in Trigger.oldMap
    public List<sObject> newValues { get; set; } //record of all act

     //view provides the list of the objects that have been recently filtered
    public List<sObject> view { get { return this.intView; } }
    private List<sObject> intView;

    /**
     * Used by unit test to not to process some logic esp. when creating data for migration tests
     */
    private static Boolean value_BypassForUnitTests = false;
    public static Boolean bypassForUnitTests {
        get { return Test.isRunningTest() && value_BypassForUnitTests; }
        set { value_BypassForUnitTests = value; }
    }


    //TODO:
    //1. create a filtering logic
    //    a. created
    //    b. created and everytime it is edited
    //    c. created and subsequently meets the entry criteria
    //2. Create bucketing logic (Check for multiple rules at once and bucket them), useful for avoiding multiple loops
    //3. percolation value -> set some values to related object
    //4. percolation action -> perform some action such as approval etc.
    //5. set fields lookup usually not possible to set by workflow
    //6. set error on field useful for preventing deletion

    /********* CONSTRUCTORS *************/
    public SQX_BulkifiedBase(List<sObject> newValues, Map<Id, sObject> oldValues){
        this.oldValues = oldValues == null ? new Map<Id, sObject>() : oldValues;

        this.newValues = newValues;

        System.assert(this.newValues != null,
            'Expected new value to be always passed but found none. If you are sending it via delete trigger, consider reversing the new and old params order.');
        //the assumption here is that in delete trigger we only have old values but no new values
        //however just reversing the parameter's order will make this class work
        //if any issues are found doing the same, this base class will have to be fixed.

        //without any filter everything is a view, resetting to no applied filter does the job
        this.resetView();
    }

    public SQX_BulkifiedBase(){
        this.oldValues = new Map<Id, sObject>();
        this.newValues = new List<sObject>();

        this.resetView();
    }

    /********* FILTER LOGIC ************/

    /**
    * applies a given filter to the current list and updates the view
    * @author Pradhanta Bhandari
    * @date 2014/5/4
    * @param criteria the rule which is to be applied to filter the current objects
    * @param applyOnNewValue selects whether the check is to be done on new value list or old one
    *        <code>true</code> selects the new list
    *        <code>false</code> selects the old list
    * @return returns self/this object
    */
    public SQX_BulkifiedBase applyFilter(Rule criterion, RuleCheckMethod method){

        this.groupByRules(new Rule[]{criterion}, method);

        //update the view;
        this.intview = criterion.evaluationResult;

        return this;
    }

    /**
    * applies a given filter to the current list and updates the view
    * @author Pradhanta Bhandari
    * @date 2014/5/4
    * @param criteria the rule which is to be applied to filter the current objects
    * @param applyOnNewValue selects whether the check is to be done on new value list or old one
    *        <code>true</code> selects the new list
    *        <code>false</code> selects the old list
    * @return returns self/this object
    */
    public SQX_BulkifiedBase groupByRules(Rule [] criteria, RuleCheckMethod method){

        //reset all previous evaluation
        for(integer index = 0; index < criteria.size(); index++){
            criteria[index].evaluationResult = new List<SObject>();
        }

        for(Integer index = 0, length = this.intview.size(); index < length; index++ ){
            //for each sObject in view we will be matching rule criteria
            SObject currentObject = this.intview.get(index);

            for(Integer criterionIndex = 0; criterionIndex < criteria.size(); criterionIndex++){
                List<SObject> currentView = criteria[criterionIndex].evaluationResult;
                if(currentView == null){
                    criteria[criterionIndex].evaluationResult = currentView;
                }

                boolean meetsCriteria = this.checkIfCriterionIsMet(currentObject, criteria[criterionIndex], false);

                if(meetsCriteria){
                    //then add to new view if further conditions are met

                    sObject oldValue = this.oldValues.get(currentObject.Id);

                    if(method != RuleCheckMethod.OnCreateAndEditThatMakesItMeet ||  oldValue == null ){
                        //if it is on create and every edit, then we don't need to evaluate for old values
                        //additionally no old value means this is an insert
                        currentView.add(currentObject);
                    }
                    else{

                        boolean previouslyMetCriteria = this.checkIfCriterionIsMet(oldValue, criteria[criterionIndex], true);

                        if(!previouslyMetCriteria){
                            //this means this edit does make it meet the criterion, when it didn't previously
                            currentView.add(currentObject);
                        }
                    }
                }

            }

        }


        //update the view;
        this.intview = view;

        return this;
    }

    /**
    * checks whether a particular sObject meets the given rule or not
    * @author Pradhanta Bhandari
    * @date 2014/5/4
    * @return <code>true</code> if criterion is met
    *         <code>false</code> if criterion is not met
    */
    private boolean checkIfCriterionIsMet(SObject objectToEvaluate, Rule criterion, boolean ignorePriorValueChecks){
        boolean meetsCriterion = false;

        System.debug('Evaluating for : ' + objectToEvaluate);

        for(Integer ruleIndex = 0, ruleCount = criterion.elements.size(); ruleIndex < ruleCount; ruleIndex++ ){
            //TODO: currently supports only single level of rule, make it support multi combination

            RuleElement currentRuleElement = criterion.elements.get(ruleIndex);

            boolean evaluation = false;

            if(currentRuleElement.operator == RuleOperator.HasChanged ||
                currentRuleElement.operator == RuleOperator.Transitioned){

                if(ignorePriorValueChecks){
                    evaluation = false;
                }
                else{
                    //get existing element
                    SObject oldObject = this.oldValues.get(objectToEvaluate.Id);

                    if(currentRuleElement.operator == RuleOperator.HasChanged){
                        evaluation = oldObject == null ||
                                    ( oldObject.get(currentRuleElement.field) != objectToEvaluate.get(currentRuleElement.field) );
                    }
                    else{
                        evaluation = objectToEvaluate.get(currentRuleElement.field) == currentRuleElement.value &&
                                    oldObject != null &&
                                    ( oldObject.get(currentRuleElement.field) == currentRuleElement.fromValue );
                    }
                }
            }
            else if(currentRuleElement.operator == RuleOperator.Equals){
                evaluation = objectToEvaluate.get(currentRuleElement.field) == currentRuleElement.value;
            }
            else{
                evaluation = objectToEvaluate.get(currentRuleElement.field) != currentRuleElement.value;
            }



            System.debug('Evaluation result : ' + evaluation);

            //if it is OR operator a single positive is enough
            if(criterion.operator == RuleCombinationOperator.OpOr && evaluation){
                meetsCriterion = true;
                break;
            }

            //if it is AND operator a single negative is enough to not meet the criterion
            if(criterion.operator == RuleCombinationOperator.OpAnd && !evaluation){
                meetsCriterion = false;
                break;
            }

            meetsCriterion = evaluation; //copy the last evaluation for AND operators, because if it is OR evaluation it will never reach here if positive
                                        //if there are any negatives then previous check eliminates the need to reach here
                                        //so for AND the last positive evaluation really means complete positive evaluation
        }

        return meetsCriterion;
    }


    /**
    * clears the result of application of all filters or in short sets the view back to default
    * @author Pradhanta Bhandari
    * @date 2014/5/4
    */
    public SQX_BulkifiedBase resetView(){
        this.intView = this.newValues;

        return this;
    }

    /**
    * returns the list of ids in a particular field
    */
    public Set<Id> getIdsForField(Schema.SObjectField field){

        return getIdsForField(this.view, field);

    }

    /**
    * gets the list of Ids set in a specific field for all the objects passed
    * @param objects the list of objects from which to extract the field
    * @param field the field whose Id is to be collected
    */
    public Set<Id> getIdsForField(List<SObject> objects, Schema.SObjectField field){
        Set<Id> ids = new Set<Id>();

        for(sObject currentObject: objects){
            Id idValue = (Id)currentObject.get(field);
            if(idValue != null)
                ids.add(idValue);
        }


        return ids;

    }

    /********* RULE CRITERIA ***********/

    /**
    * represents the individual rule element for the filter to be applied
    */
    public with sharing class Rule{
        /*the list of rule element to be evaluated */
        public List<RuleElement> elements {get; set;}

        /* the operator which is to be used to combine the multiple rule elements, it is either AND or OR*/
        public  RuleCombinationOperator operator {get; set;}

        /* the result of the evaluation */
        public List<SObject> evaluationResult {get; set;}
        //TODO: add a complex rule allowing combinations

        public Rule(){
            this.elements = new List<RuleElement>();
            this.operator = RuleCombinationOperator.OpAnd;
            this.evaluationResult = null;
        }

        /**
        * adds a rule of that checks for transition from particular value to another
        * @param field the field to compare
        * @param operator the operator to use in this case should be transitioned, accepts any operator even though the name says addTransitionRule
        *                 just pass value or from value null as required.
        * @param value the final value
        * @param fromValue the from value of the object
        */
        public void addTransitionRule(Schema.SObjectField field, RuleOperator operator, Object value, Object fromValue){
            RuleElement ruleElem = new RuleElement();
            ruleElem.field = field;
            ruleElem.operator = operator;
            ruleElem.value = value;
            ruleElem.fromValue = fromValue;

            this.elements.add(ruleElem);
        }

        /**
        * adds a rule for given operator
        */
        public void addRule(Schema.SObjectField field, RuleOperator operator, Object value){

            System.assert(operator != RuleOperator.HasChanged && operator != RuleOperator.Transitioned,
                'Operator is not of binary arity');

            this.addTransitionRule(field, operator, value , BLANKFIELD);

        }

        public void addRule(Schema.SObjectField field, RuleOperator operator){

            System.assert(operator == RuleOperator.HasChanged, 'Operator is a binary operator please provide a value');

            this.addTransitionRule(field, operator, BLANKFIELD , BLANKFIELD);

        }

    }

    /**
    * represents the basic element of a rule criteria
    * @author Pradhanta Bhandari
    * @date 2014/5/4
    */
    public with sharing class RuleElement{
        public Schema.SObjectField field {get; set;} //the field to evaluate
        public RuleOperator operator {get; set; } //operator like equals, not equals, has changed, or transitioned
        public Object value; //value with which to compare the field
        public Object fromValue; //value used by transitioned operator to check if there was a transition from X -> Y i.e. fromValue -> value
    }

    public static Object BLANKFIELD = null;
    public enum RuleOperator {Equals, NotEquals, HasChanged,  Transitioned} //TODO: add other operators such as startsWith refer to SF entry rule criteria
    public enum RuleCombinationOperator { OpAnd, OpOr }

    public enum RuleCheckMethod { OnCreate, OnCreateAndEveryEdit, OnCreateAndEditThatMakesItMeet }


    /********************** UNIQUENESS SUPPORT ***********************************************/

    /**
    * copies the values from the fields list into a uniquefield to detect combination is unique, Error is not immediate but occurs when the object is persisted.
    * @param fields the Id fields whose combination is to be copied to uniquefield
    * @param uniqueField the field that has uniqueness constraint enabled, this field actually enforces uniqueness of data
    */
    public SQX_BulkifiedBase ensureCombinationIsUnique(Schema.SObjectField [] fields, Schema.SObjectField uniqueField){

        if (bypassForUnitTests) {
            return this;
        }

        for(SObject objectToCheck : this.resetView().view){
            String uniqueMap = '';
            //for each field that is to be unique add the combination to get a string which must be unique
            for(Schema.SObjectField field : fields){
                uniqueMap = uniqueMap + objectToCheck.get(field);
            }

            System.debug('New Record: ' + uniqueMap);

            objectToCheck.put(uniqueField, uniqueMap); //updates the value in the unique field
        }

        return this;

    }


    /**************************** LOCKING MECHANISM ******************************************/

    /**
    * ensures that once a record is locked (write or delete), can not be modified or deleted.
    * Please note that the locks are considered valid only for next transaction.
    * i.e. if the lock field was modified in this update they are ignored.
    * @param lockField the field that has the lock toggle
    * @param parentLockField the formula field that checks whether a parent is locked or not
    * @param errorMessage the message to display in case of an error
    * @param ignoreOldValue <code>true</code> should be set to true only for delete, because setting this true for insert/update causes immediate failure
    *                       <code>false</code> set for insert or update
    */
    public SQX_BulkifiedBase preventChangesIfLocked(Schema.SObjectField lockField, Schema.SObjectField parentLockField, String errorMessage, boolean ignoreOldValue){
        for(SObject objectToCheck : this.view){

            boolean isLocked = (boolean)objectToCheck.get(lockField);
            boolean wasLocked = ignoreOldValue || (oldValues != null && (boolean)oldValues.get(objectToCheck.Id).get(lockField));
            if(parentLockField != null){
                isLocked = isLocked || (boolean)objectToCheck.get(parentLockField);
            }

            //throw error only if it was already locked
            //that is not locked in this transaction.
            if(isLocked &&  wasLocked  ){
                objectToCheck.addError(errorMessage);
            }
        }
        return this;
    }

    /**
    * This function redirects all object to CQ Aware Tasks
    **/
    public SQX_BulkifiedBase completeRelatedCQAwareTasks(){
        this.resetView();
        SQX_CQ_Aware_Tasks.performCQAwareTasks(this.view, this.oldValues);

        return this;

    }


    /**************************** RE-ENTRANT PREVENTION MECHANISM ******************************************/

    static Map<String, ProcessedRecords[]> processedEntities = new Map<string, ProcessedRecords[]>(); //record entities that are processed for an action

    /**
     * Internal method to get the key value for organizing the processed records
     * @return   returns an integer representing the key to use while storing processed records
     */
    private static Integer getCurrentKey() {
        return (Limits.getQueries() + Limits.getDmlStatements());
    }

    /**
     * Internal class to store the list of Ids that have been processed along with an
     * Integer key, which can be used to validate whether or not the current list of records is
     * valid.
     */
    public with sharing class ProcessedRecords {
        public Integer Key;
        public Set<Id> Ids;

        /**
         * Constructor to create a Processed Record with a given key
         * @param  key the key value to set for a given item
         */
        public ProcessedRecords(Integer key) {
            this.Key = key;
            Ids = new Set<Id>();
        }

        /**
         * If the current key is greater than one stored in processed record, the
         * processed record is valid. However, if the Key is less than current key,
         * we need to understand tha SF has caused a rollback. Therfore, the processed records
         * is no longer valid 
         * @return   <code>true</code> if the record is valid else returns <code>false</code>
         */
        public Boolean isValid() {
            return getCurrentKey() > Key;
        }
    }

    /**
    * Returns the Processed Records for the given action name or initializes
    * one and returns it
    * @param actionName the key field for the set whose processed records is to be returned
    */
    private static ProcessedRecords[] processedRecordsFor(String actionName){
        ProcessedRecords[] processedRecords = processedEntities.get(actionName);
        if(processedRecords == null) {
            // Initialize processed records if none exists for the action
            processedRecords = new ProcessedRecords[] {};
            processedEntities.put(actionName, processedRecords);
        } else {
            // Return a list of previously processed records which are valid
            for(Integer i = processedRecords.size() - 1; i >= 0; i--) {
                if(!processedRecords[i].isValid()) {
                    processedRecords.remove(i);
                }
            }
        }

        processedRecords.add(new ProcessedRecords(getCurrentKey()));
        
        return processedRecords;
    }

    /**
    * returns a set of IDs given a actionName of an object, returns empty set
    */
    public Set<Id> getProcessedRecordIdsFor(String objectName, String actionName){
        actionName = objectName + '.' + actionName;
        
        Set<Id> ids = new Set<Id>();
        if (processedEntities.containsKey(actionName)) {
            for(ProcessedRecords records : processedEntities.get(actionName)) {
                if (records.isValid()) {
                    ids.addAll(records.Ids);
                }
            }
        }
        
        return ids;
    }

    /**
    * returns a set of IDs given a actionName, returns empty set
    * @param actionName the key field for the set which is to be returned
    * Note: This returns all processed records ids including rolled backed records. Use getProcessedRecordIdsFor() to get only valid processed record ids.
    */
    public Set<Id> getProcessedEntitiesFor(String objectName, String actionName){
        actionName = objectName + '.' + actionName;
        Set<Id> ids = new Set<Id>();
        if(processedEntities.containsKey(actionName)) {
            for(ProcessedRecords records : processedEntities.get(actionName)) {
                ids.addAll(records.Ids);
            }
        }

        return ids;
    }

    /**
    * builds a set of for gicen sobjects and a action name, in a map
    * @param actionName the key field for the set
    * @param processedEntitie list of sObject whose IDs are to be put in the set
    */
    public static Set<Id> setProcessedEntitiesFor(String actionName, List<SObject> processedEntityList){
        Set<Id> allIds = new Map<Id, SObject>(processedEntityList).keySet();

        ProcessedRecords[] existingList = processedRecordsFor(actionName);

        existingList[existingList.size() - 1].Ids.addAll(allIds);

        return allIds;
    }

    /**
    * This method removes all the records that have been previously processed for the action.
    * It will also set the view property of the bulkified base to new processed entity.
    *
    * @param objectName the name of the object for which the transaction is to be checked
    * @param actionName the name of the action which is to be checked
    */
    public SQX_BulkifiedBase removeProcessedRecordsFor(String objectName, String actionName){
        String keyName = objectName + '.' + actionName;

        List<SObject> remainingObjects = new List<SObject>();
        Set<Id> ids = getProcessedRecordIdsFor(objectName, actionName);

        for(SObject entity : this.view){
            Id entityId = (Id)entity.get('Id');
            if(!ids.contains(entityId)) {
                System.debug(keyName + ' ' + entityId);
                remainingObjects.add(entity);
            }
        }
        
        this.intView = remainingObjects;

        return this;

    }

    /**
    * Adds all the item in view as processed for an action.
    * @param objectName the name of the object for which the transaction is to be checked
    * @param actionName the name of the action which is to be checked
    * @param entities the list of entities that have been processed
    */
    public SQX_BulkifiedBase addToProcessedRecordsFor(String objectName, String actionName, List<SObject> entities){
        setProcessedEntitiesFor(objectName + '.' + actionName, entities);

        return this;
    }

    /**
    * Clears all set in map processedEntities
    */
    public static void clearAllProcessedEntities(){
        processedEntities.clear();
    }
    
    /**
    * cache of tracking enabled long text fields of already identified object
    */
    @TestVisible
    static Map<String, List<String>> trackedLongTextFieldsCache = new Map<String, List<String>>();
    
    /**
    * Used by unit tests to skip long text field history tracking
    * Notes:
    *   - added to avoid "Internal Salesforce Error" occurring while deploying cq package to ci org running all tests
    *   - field tracking cannot be fully tested through unit tests
    *   - manually executing tests does not throw above exception
    */
    static Boolean skipFieldTrackingQueryForUnitTestsValue = true;
    public static Boolean skipFieldTrackingQueryForUnitTests {
        get { return Test.isRunningTest() && skipFieldTrackingQueryForUnitTestsValue; }
        set { skipFieldTrackingQueryForUnitTestsValue = value; }
    }
    
    /**
    * returns list of tracking enabled long text fields of an object type
    * @param sObjType object type for which tracking enabled long text fields are identified
    * @returns list of tracking enabled long text fields of the given object type
    */
    public static List<String> getTrackedLongTextFields(String sObjTypeName) {
        // use lowercase object name
        sObjTypeName = sObjTypeName.toLowerCase();
        // get already identified tracked long text fields
        List<String> trackedFldNames = trackedLongTextFieldsCache.get(sObjTypeName);
        
        // identify long text fields if already not done
        if (trackedFldNames == null) {
            // instantiate empty list obj for long text fields
            trackedFldNames = new List<String>();
            
            // added skip logic here since we can still verify trigger process for long text history tracking
            if (!skipFieldTrackingQueryForUnitTests) {
                String historyObjTypeName = sObjTypeName.replace('__c', '__history');
                
                // check object history tracking is enabled or not
                if (Type.forName(historyObjTypeName) != null) {
                    final String LONG_TEXT_FIELD_VALUE_TYPE_ID = 'string';
                    final Integer LONG_TEXT_FIELD_MIN_LENGTH = 256; // Long text field type (long text area and rich text area) cannot be created lower than 256
                    
                    // get field tracking enabled long text fields
                    for (FieldDefinition mdt : [SELECT QualifiedApiName
                                                FROM FieldDefinition
                                                WHERE EntityDefinition.QualifiedApiName = :sObjTypeName
                                                    AND ValueTypeId = :LONG_TEXT_FIELD_VALUE_TYPE_ID
                                                    AND IsFieldHistoryTracked = true
                                                    AND IsCalculated = false
                                                    AND Length >= :LONG_TEXT_FIELD_MIN_LENGTH]) {
                        trackedFldNames.add(mdt.QualifiedApiName);
                    }
                }
            }
            
            // add list of identified long text fields to cache obj
            trackedLongTextFieldsCache.put(sObjTypeName, trackedFldNames);
        }
        
        return trackedFldNames;
    }
    
    /**
    * trigger method to track long text fields changes
    */
    public SQX_BulkifiedBase addModifiedLongTextFieldHistory() {
        if (Trigger.new != null && !Trigger.new.isEmpty()) {
            final string ACTION_NAME_FORMAT = 'addModifiedLongTextFieldHistory:{FieldName}', ACTION_NAME_FIELD_NAME_KEY = '{FieldName}';
            
            SObjectType sobjType = Trigger.new[0].Id.getSobjectType();
            String sObjTypeName = sobjType.getDescribe().getName();
            List<String> trackedFldNames = getTrackedLongTextFields(sObjTypeName);
            
            if (!trackedFldNames.isEmpty()) {
                List<SQX_Long_Text_History__c> newHistoryItems = new List<SQX_Long_Text_History__c>();
                
                Id currentUserId = UserInfo.getUserId();
                DateTime currentDT = System.now();
                
                // process identified long text fields
                for (String fldName : trackedFldNames) {
                    // action name for each field to track
                    String actionNameForField = ACTION_NAME_FORMAT.replace(ACTION_NAME_FIELD_NAME_KEY, fldName);
                    
                    System.debug('Processing long text field tracking for object ' + sObjTypeName + ' and field ' + fldName);
                    
                    // get remaining records
                    Map<Id, SObject> remainingObjs = Trigger.newMap.clone();
                    for (Id objId : getProcessedRecordIdsFor(sObjTypeName, actionNameForField)) {
                        remainingObjs.remove(objId);
                    }
                    
                    List<SObject> processedObjs = new List<SObject>();
                    
                    // process remaining records
                    for (SObject rec : remainingObjs.values()) {
                        SObject recOld = Trigger.oldMap.get(rec.Id);
                        String oldLongTextVal = String.isBlank((String)recOld.get(fldName)) ? '' : (String)recOld.get(fldName);
                        String newLongTextVal = String.isBlank((String)rec.get(fldName)) ? '' : (String)rec.get(fldName);
                        
                        // perform case sensitive check
                        if (!oldLongTextVal.equals(newLongTextVal)) {
                            // list processed record
                            processedObjs.add(rec);
                            
                            // create new history record
                            newHistoryItems.add(new SQX_Long_Text_History__c(
                                Parent_Record_Id__c = rec.Id,
                                Field_Name__c = fldName,
                                SQX_Changed_By__c = currentUserId,
                                Changed_On__c = currentDT,
                                Old_Value__c = oldLongTextVal
                            ));
                        }
                    }
                    
                    if (!processedObjs.isEmpty()) {
                        // add processed records
                        addToProcessedRecordsFor(sObjTypeName, actionNameForField, processedObjs);
                    }
                }
                
                if (!newHistoryItems.isEmpty()) {
                    // insert new history records
                    // FLS not checked for fields of history tracking object
                    new SQX_DB().op_insert(newHistoryItems, new List<SObjectField>());
                }
            }
        }
        
        return this;
    }

}