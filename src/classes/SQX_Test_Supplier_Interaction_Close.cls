/**
 * Test class for validating supplier interaction closure 
 */ 
@isTest
public class SQX_Test_Supplier_Interaction_Close {
    
    // Map of supplier interaction step fields and values to set and compare with new supplier document
    private static Map<String, Object> fieldValuesToSet = new Map<String, Object>{
        SQX.NSPrefix+'Document_Name__c' => 'Sample Document Name',
            SQX.NSPrefix+'Issue_Date__c' => Date.today().addDays(-5),
            SQX.NSPrefix+'Needs_Periodic_Update__c' => true,
            SQX.NSPrefix+'Notify_Before_Expiration__c' => 10
            };
    static SQX_DB db = new SQX_DB();
    
    /**
     * Common method for creating account, contact, service and supplier interaction
     **/
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(standardUser) {

            // create account record
            Account account = new Account(
                Name = SQX_Test_Supplier_Interaction.ACCOUNT_NAME 
            );
            insert account;
            
            //Create contact for Account
            insert new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = account.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            
            SQX_Standard_Service__c service = new SQX_Standard_Service__c(name='testService');
            insert service;
            
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(SQX_Test_Supplier_Interaction.getAccountAndContactRecords()[0]);
            interaction.si.SQX_Supplier_Liaison__c = standardUser.Id;
            interaction.si.SQX_Standard_Service__c = service.Id;
            interaction.si.Status__c = SQX_Supplier_Common_Values.STATUS_COMPLETE;
            interaction.si.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_VERIFICATION;
            interaction.si.Scope__c = 'Test Scope';
            interaction.save();
        }
    }
    
    /**
     * Method for getting all field values for a given sobject type
     **/ 
    public static List<SObject> getRecords(SObjectType type, String fieldName, Object value){
        
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.field = fieldName;
        filter.operator = 'eq';
        filter.value = value;

        return SQX_DynamicQuery.getAllFields(type.getDescribe(), new Set<SObjectField>(), filter, new List<SObjectField>(), false);
    }
    
    /**
     * Method for creating document request type step
     **/ 
    private static ContentDocumentLink createInteractionSteps(SQX_Supplier_Interaction__c interaction, User assignee, Id supplierDocumentId, Boolean applicable){
        List<SQX_Supplier_Interaction_Step__c> stepList = new List<SQX_Supplier_Interaction_Step__c>();
        SQX_Supplier_Interaction_Step__c step = new SQX_Supplier_Interaction_Step__c(
            Name = SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST,
            Applicable__c = applicable,
            SQX_Supplier_Document__c = supplierDocumentId,
            SQX_User__c = assignee.Id,
            SQX_Parent__c = interaction.Id,
            Expiration_Date__c = Date.today().addDays(30),
            Comment__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE,
            Due_Date__c = Date.today().addDays(30),
            Step__c = 1,
            RecordTypeId = Schema.SObjectType.SQX_Supplier_Interaction_Step__c.getRecordTypeInfosByDeveloperName().get(SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST).getRecordTypeId()
        );
        
        for(String fieldName: fieldValuesToSet.keySet()){
            step.put(fieldName, fieldValuesToSet.get(fieldName));
        }
        
        stepList.add(step);
        
        step = new SQX_Supplier_Interaction_Step__c(
            Name = SQX_Steps_Trigger_Handler.RT_TASK,
            Applicable__c = applicable,
            SQX_User__c = assignee.Id,
            SQX_Parent__c = interaction.Id,
            Comment__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE,
            Due_Date__c = Date.today().addDays(30),
            Step__c = 1,
            RecordTypeId = Schema.SObjectType.SQX_Supplier_Interaction_Step__c.getRecordTypeInfosByDeveloperName().get(SQX_Steps_Trigger_Handler.RT_TASK).getRecordTypeId()
        );
        
        stepList.add(step);
        
        db.op_insert(stepList, new List<SObjectField>{});
        
        ContentVersion cv = new ContentVersion(VersionData = Blob.valueOf('New Content Document'), 
                                               PathOnClient = 'LargeFile.txt');
        
        db.op_insert(new List<ContentVersion>{cv}, new List<SObjectField>{});
        
        cv = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id];
        
        ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = cv.ContentDocumentId, 
                                                                   LinkedEntityId = stepList[0].Id,
                                                                   ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
        db.op_insert(new List<ContentDocumentLink>{documentLink}, new List<SObjectField>{});
        
        for(SQX_Supplier_Interaction_Step__c currentStep: stepList){
            currentStep.Result__c = SQX_Steps_Trigger_Handler.RESULT_GO;
            currentStep.Status__c = SQX_Steps_Trigger_Handler.STATUS_COMPLETE;
        }
        db.op_update(stepList, new List<SObjectField>{});
                
        return documentLink;
    }
    
    /**
     * Method for validating supplier interaction closure
     **/ 
    private static void validateSupplierInteraction(SQX_Test_Supplier_Interaction interaction, String result){
        interaction.si.Result__c = result;
        interaction.close();
        
        SQX_Supplier_Interaction_Step__c step = (SQX_Supplier_Interaction_Step__c)getRecords(SQX_Supplier_Interaction_Step__c.SObjectType, SQX_Steps_Trigger_Handler.FIELD_NAME, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST)[0];
        
        if(interaction.si.Result__c != SQX_Supplier_Common_Values.RESULT_APPROVED){
            System.assert(step.SQX_New_Or_Updated_Supplier_Document__c == null, 'No supplier document is created for rejected result');
        }else if(step.Applicable__c == false){
            System.assert(step.SQX_New_Or_Updated_Supplier_Document__c == null, 'No supplier document is created for not applicable step');
        }
        else{
            SQX_Certification__c supplierDocument = (SQX_Certification__c)getRecords(SQX_Certification__c.SObjectType, 'Id', 
                                                                                     step.SQX_Supplier_Document__c != null ? 
                                                                                     step.SQX_Supplier_Document__c : 
                                                                                     step.SQX_New_Or_Updated_Supplier_Document__c)[0];
            
            for(String fieldName: fieldValuesToSet.keySet()){
                System.assertEquals(step.get(fieldName), supplierDocument.get(fieldName));
            }
            
            System.assertEquals(step.Expiration_Date__c, supplierDocument.Expire_Date__c);
            System.assertEquals(interaction.si.SQX_Account__c, supplierDocument.Account__c);
            System.assertEquals(interaction.si.SQX_Standard_Service__c, supplierDocument.SQX_Standard_Service__c);
            System.assertEquals(interaction.si.Part_Name__c, supplierDocument.Part_Name__c);
            System.assertEquals(interaction.si.Scope__c, supplierDocument.Description__c);
            
            List<ContentDocumentLink> docLink = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: supplierDocument.Id];
            System.assert(docLink.size() == 1, 'One content should be shared with Supplier Document but actual size is '+docLink.size());
            
            docLink = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: interaction.si.SQX_Account__c];
            System.assert(docLink.size() == 1, 'One content should be shared with Account');
        }
    }
    
    /*
     * GIVEN: Supplier Interaction record in complete status
     * WHEN: Record is closed
     * THEN: New Supplier Document is created
     */
    static testmethod void whenSupplierInteractionIsClosed_ThenNewSupplierDocumentIsCreatedForDocumentStep(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            // Arrange: create new interaction and a document request step
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction();
            interaction.si = (SQX_Supplier_Interaction__c)getRecords(interaction.si.getSObjectType(), SQX.NSPrefix+'SQX_Supplier_Liaison__c', standardUser.Id)[0];
            ContentDocumentLink link = createInteractionSteps(interaction.si, standardUser, null, true);
            
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = link.ContentDocumentId, 
                                                                       LinkedEntityId = interaction.si.SQX_Account__c,
                                                                       ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            db.op_insert(new List<ContentDocumentLink>{documentLink}, new List<SObjectField>{});
            
            validateSupplierInteraction(interaction, SQX_Supplier_Common_Values.RESULT_APPROVED);
        }
    }
    
    /*
     * GIVEN: Supplier Interaction record in complete status
     * WHEN: Record is closed with reject result
     * THEN: No New Supplier Document is created
     */
    static testmethod void whenSupplierInteractionIsClosedWIthRejectResult_ThenNoNewSupplierDocumentIsCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction();
            interaction.si = (SQX_Supplier_Interaction__c)getRecords(interaction.si.getSObjectType(), SQX.NSPrefix+'SQX_Supplier_Liaison__c', standardUser.Id)[0];
            ContentDocumentLink link = createInteractionSteps(interaction.si, standardUser, null, true);
            
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = link.ContentDocumentId, 
                                                                       LinkedEntityId = interaction.si.SQX_Account__c,
                                                                       ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            db.op_insert(new List<ContentDocumentLink>{documentLink}, new List<SObjectField>{});
            
            validateSupplierInteraction(interaction, SQX_Supplier_Common_Values.RESULT_REJECTED);
        }
    }
    
    /*
     * GIVEN: Supplier Interaction record in complete status
     * WHEN: Record is closed with not applicable step
     * THEN: No New Supplier Document is created
     */
    static testmethod void whenSupplierInteractionIsClosedWithNotApplicableStep_ThenNoNewSupplierDocumentIsCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction();
            interaction.si = (SQX_Supplier_Interaction__c)getRecords(interaction.si.getSObjectType(), SQX.NSPrefix+'SQX_Supplier_Liaison__c', standardUser.Id)[0];
            ContentDocumentLink link = createInteractionSteps(interaction.si, standardUser, null, false);
            
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = link.ContentDocumentId, 
                                                                       LinkedEntityId = interaction.si.SQX_Account__c,
                                                                       ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            db.op_insert(new List<ContentDocumentLink>{documentLink}, new List<SObjectField>{});
            
            validateSupplierInteraction(interaction, SQX_Supplier_Common_Values.RESULT_APPROVED);
        }
    }
    
    /*
     * GIVEN: Supplier Interaction record in complete status with Linked Supplier Document Step
     * WHEN: Record is closed with not applicable step
     * THEN: No New Supplier Document is created
     */
    static testmethod void whenSupplierInteractionIsClosedWithSupplierDocumentLink_ThenSupplierDocumentIsUpdated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction();
            interaction.si = (SQX_Supplier_Interaction__c)getRecords(interaction.si.getSObjectType(), SQX.NSPrefix+'SQX_Supplier_Liaison__c', standardUser.Id)[0];
            
            SQX_Certification__c existingSupplierDocument = new SQX_Certification__c(Document_Name__c = 'Sample Document Name', Account__c = interaction.si.SQX_Account__c);
            db.op_insert(new List<SQX_Certification__c>{existingSupplierDocument}, new List<SObjectField>{});
            
            ContentDocumentLink link = createInteractionSteps(interaction.si, standardUser, existingSupplierDocument.Id, true);
            
            ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = link.ContentDocumentId, 
                                                                       LinkedEntityId = existingSupplierDocument.Id,
                                                                       ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            db.op_insert(new List<ContentDocumentLink>{documentLink}, new List<SObjectField>{});
            
            validateSupplierInteraction(interaction, SQX_Supplier_Common_Values.RESULT_APPROVED);
        }
    }
}