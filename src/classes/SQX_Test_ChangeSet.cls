@isTest
public class SQX_Test_ChangeSet{

    List<SQX_Test_JSONObject> changedObjects;

    public SQX_Test_ChangeSet(){
        this.changedObjects = new List<SQX_Test_JSONObject>();
    }

    /**
    * used to add objects to the list
    */
    public SQX_Test_ChangeSet addToChangeSet(SQX_Test_JSONObject jsonObject){
        changedObjects.add(jsonObject);

        return this;
    }

    /**
    * convert the list of records to JSON*/
    public String toJSON(){
        Map<String, Object> changesetJSON = new Map<String, Object>();
        List<Map<String, Object>> changes = new List<Map<String, Object>>();

        for(SQX_Test_JSONObject changedObject : changedObjects){
            changes.add(changedObject.toJSONObject());
        }

        changesetJSON.put('changeSet', changes);

        return JSON.serialize(changesetJSON);
    }

    /**
    * used to add created object record in the changeset
    */
    public SQX_Test_JSONObject addChanged(String friendlyId, SObject sObj){
        SQX_Test_JSONObject newChange = new SQX_Test_JSONObject(friendlyId, sObj);

        this.changedObjects.add(newChange);

        return newChange;

    }

    public SQX_Test_JSONObject addChanged(SObject sObj){
        SQX_Test_JSONObject newChange = new SQX_Test_JSONObject(sObj);

        this.changedObjects.add(newChange);
        
        return newChange;

    }
}