/**
 *   This class contains test cases for ensuring that Document Management source is providing homepage items with desired values
 *   @author - Anuj Bhandari
 *   @date - 17-04-2017
 */

@isTest
public class SQX_Test_Homepage_Source_DocumentMgmt {



    /*
     * Creates the Controlled Document items that are supposed to show up in hompepage component
     */
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser) {

            // Arrange2: Create 3 Controlled document with Draft, Approved and Rejected Draft statuses

            // Controlled document in draft status
            SQX_Test_Controlled_Document cd1 = new SQX_Test_Controlled_Document().save();

            //Approved controlled document with auto-release off
            SQX_Test_Controlled_Document cd2 = new SQX_Test_Controlled_Document().save();
            cd2.doc.Auto_release__c = false;
            cd2.setStatus(SQX_Controlled_Document.STATUS_APPROVED);
            cd2.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            cd2.save();

            //Rejected controlled document
            SQX_Test_Controlled_Document cd3 = new SQX_Test_Controlled_Document();
            cd3.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_REJECTED;
            cd3.save();

        }
    }


    /**
     *   Given : A set of controlled documents (Draft, Rejeceted and Approved) and a homepage component expecting records from Document Management Source
     *   When :  Document Records are fetched and processed
     *   Then :  All document types are added to the homepage with desired values
     *   @story -3165
     */
    testmethod static void givenControlledDocumentItemsOfDifferentTypes_WhenRecordsAreFetchedAndProcessed_ThenCorrepondingHomepageItemsShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange1: Fetch Items
            List < SQX_Controlled_Document__c > itms = [SELECT  Id,
                                                                Name,
                                                                Title__c,
                                                                Document_Status__c,
                                                                Approval_Status__c,
                                                                CreatedDate,
                                                                OwnerId
                                                                FROM SQX_Controlled_Document__c
                                                            ];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List < SQX_Homepage_ItemSource > {
                new SQX_Homepage_Source_DocumentMgmt_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            System.assertEquals( 3, hpItems.size());

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertNotEquals(null, hpItem.itemAge);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_DOCUMENT_MANAGEMENT, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                SQX_Homepage_Item_Action act = hpitem.actions[0];

                System.assertEquals(false, act.supportsBulk);

                System.assertEquals(false, hpItem.supportsBulk);


                for (SQX_Controlled_Document__c itm: itms) {

                    if (itm.id == hpItem.itemId) {

                        if (itm.Document_Status__c == SQX_Controlled_Document.STATUS_DRAFT) {

                            PageReference pr = new PageReference('/' + itm.Id);
                            pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
                            System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

                            if (itm.Approval_Status__c == SQX_Controlled_Document.APPROVAL_REJECTED) {

                                System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

                                validateRejectedDraftControlledDocumentFeed(itm, hpItem);

                                break;

                            } else {

                                System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);

                                validateDraftControlledDocumentFeed(itm, hpItem);

                                break;
                            }

                        } else if (itm.Document_Status__c == SQX_Controlled_Document.STATUS_APPROVED) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

                            validateApprovedControlledDocumentFeed(itm, hpItem);

                            PageReference pr = new PageReference('/' + itm.Id);
                            pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
                            System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

                            break;

                        } else {

                            System.assert(false, 'Unknown record : ' + hpItem);

                        }
                    }
                }
            }
        }
    }


    /**
     *   Method validates the homepage item for draft controlled document
     */
    private static void validateDraftControlledDocumentFeed(SQX_Controlled_Document__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_DRAFT, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }


    /**
     *   Method validates the homepage item for approved controlled document
     */
    private static void validateApprovedControlledDocumentFeed(SQX_Controlled_Document__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_APPROVED, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }

    /**
     *   Method validates the homepage item for rejected document
     */
    private static void validateRejectedDraftControlledDocumentFeed(SQX_Controlled_Document__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DOCUMENT_MANAGEMENT_REJECTED, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }
}