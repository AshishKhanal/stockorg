/**
 * Test class for SQX_Test_Clone_Records
 */
@IsTest
public class SQX_Test_Clone_Records {
    
    /**
    * Given: A regulatory report
    * When :  The report is cloned
    * Then : Appropriate fieldset should be used based on variable:useInclusionFieldset of 
    * SQX_Clone_Configuration 
    */
    public static testMethod void givenRegulatoryReport_whenCloned_thenAppropriateFieldSetShouldBeUsed(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Follow_Up_Number_Internal__c=6
            );
            insert regReport;
            
            // Arrange: Set up clone configurations.
            SQX_Clone_Configuration config = new SQX_Clone_Configuration();
            config.ids = new List<String>{regReport.Id};
            config.jsonDefaults='{}';
            
            // Act: Clone with useInclusionFieldset set to null. This is equivalent to useInclusionFieldset set to false.
            // So, exclusion fieldset should be used.
            config.useInclusionFieldset=true;
            
            List<Id> ids = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{config});
            
            // Assert: Query cloned regulatory report
            SQX_Regulatory_Report__c regReport1 = [SELECT Id, Name, compliancequest__SQX_Complaint__c, compliancequest__Comment__c, compliancequest__Decision_Tree_Task__c, compliancequest__Due_Date__c, compliancequest__Is_Locked__c, compliancequest__Reg_Body__c, compliancequest__SQX_Decision_Tree_Void__c, compliancequest__SQX_Decision_Tree__c, compliancequest__SQX_Submission_History__c, compliancequest__SQX_User__c, compliancequest__Status__c, compliancequest__Submitted_Date__c, compliancequest__Task_Id__c, compliancequest__Report_Name__c, compliancequest__Follow_Up_Number_Internal__c, compliancequest__Follow_Up_Number__c, compliancequest__SQX_Follow_Up_Of__c, compliancequest__SQX_Last_Followup__c, compliancequest__SQX_Reporting_Default__c, compliancequest__Report_Id__c, compliancequest__Report_Type__c, compliancequest__SQX_Initial_Regulatory_Report__c FROM compliancequest__SQX_Regulatory_Report__c
                                                   where Id IN: ids][0];
            // Assert: Do assertion
            doAssert(regReport1,regReport);
        }
    }
    
     /**
        Method that asserts the fields of the cloned record with the parent record. 
      * @param: regReport1: The cloned regulatory report.
      * @param: regReport: The parent regulatory report that was cloned.
      */ 
     public static void doAssert(SQX_Regulatory_Report__c regReport1,SQX_Regulatory_Report__c regReport){
        
        // Assert: Value should be copied for these fields
        system.assertEquals(regReport.compliancequest__Reg_Body__c, regReport1.compliancequest__Reg_Body__c);
        system.assertEquals(regReport.compliancequest__SQX_Complaint__c,regReport1.compliancequest__SQX_Complaint__c);
        
        // Assert: Values should not be copied for these fields
        system.assertEquals(null, regReport1.compliancequest__SQX_Submission_History__c);
        system.assertEquals(0, regReport1.compliancequest__Follow_Up_Number_Internal__c); 
        system.assertEquals(null, regReport1.compliancequest__Follow_Up_Number__c);
        system.assertEquals(null, regReport1.compliancequest__SQX_Follow_Up_Of__c);
        system.assertEquals(null, regReport1.compliancequest__SQX_Last_Followup__c);
    }
}