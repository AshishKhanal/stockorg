/**
* This test cases for whether record is locked or not and should not edit PHR record when complaint record is closure review 
*/
@isTest
public class SQX_Test_7795_Locked_Complaint_Record {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runas(adminUser){
            
            // ARRANGE : Create CQ task
            SQX_Task__c task1 = new SQX_Task__c(Allowed_Days__c = 30, 
                                                SQX_User__c = standardUser.Id, 
                                                Description__c = 'Test_DESP', 
                                                Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                                Step__c = 1,
                                                Name = 'task1',
                                                Task_Type__c = SQX_Task.TYPE_SAMPLE_REQUEST);
            
            insert task1;

        }
    }
    
    /**
     * GIVEN : Complaint is created and send for closure review
     * WHEN : Complaint stage is closure review
     * THEN : Error is thrown
     */
    public static testMethod void givenClosureReviewComplaintRecord_WhenEditPHR_ThenThrowErrorMessage(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Complaint complaint1 = null;
        System.runas(standardUser){ 

            // ARRANGE : Complaint is created and send for closure review
            complaint1 = new SQX_Test_Complaint( adminUser);
            complaint1.complaint.Require_Closure_Review__c = true;
            complaint1.complaint.Closure_Review_By__c = adminUser.Id;
            complaint1.save();
            
            SQX_Product_History_Review__c product_history_review = new SQX_Product_History_Review__c();
            product_history_review.SQX_Complaint__c = complaint1.complaint.Id;
            insert product_history_review;
            
            complaint1.initiate();

            List<Task> sfTaskList = [SELECT Id FROM Task WHERE Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assertEquals(1, sfTaskList.size());

            Task sfTask = sfTaskList.get(0);
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;         
			//Act: When complaint record is closure review
            SQX_Complaint__c complaint = new SQX_Complaint__c (Id = complaint1.complaint.Id);
            complaint.Record_Stage__c = SQX_Complaint.STAGE_CLOSURE_REVIEW;
            update complaint;
            //Assert: Ensured that record is locked and throw error message when edit PHR.
            System.assertEquals(true, [SELECT Is_Locked__c FROM SQX_Complaint__c WHERE Id=:complaint1.complaint.Id].Is_Locked__c);
            SQX_Product_History_Review__c phrRecord = [SELECT Id, Product_History_Review_Summary__c FROM SQX_Product_History_Review__c WHERE Id=:product_history_review.Id];
            phrRecord.Product_History_Review_Summary__c = 'summary description.';
            
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Product_History_Review__c> { phrRecord }, new List<Schema.SObjectField>{ })[0];
            System.assertEquals('Record Status does not support the Action Performed.', result.getErrors().get(0).getMessage());
        }
    }
}