/**
*/
public with sharing class SQX_ContentDocument {

    /**
    * ShareType and Visibility field values of ContentDocumentLink object
    */
    public static final String  CONTENTDOCUMENTLINK_SHARETYPE_INFERRED = 'I',
                                CONTENTDOCUMENTLINK_SHARETYPE_VIEWER = 'V',
                                CONTENTDOCUMENTLINK_VISIBILITY_ALLUSERS = 'AllUsers';

    static Id cqDocumentRecordTypeId, scormTypeContentId, collaborationContentRecordTypeId;

    private static void loadRecordTypeIds() {
        if(cqDocumentRecordTypeId == null) {
            cqDocumentRecordTypeId = SQX_ContentVersion.getCQContentRecordTypeId();
            scormTypeContentId = SQX_ContentVersion.getScormContentRecordTypeId();
            collaborationContentRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CQCollaboration);
        }
    }

    /**
     * Prevents deletion of content document if it has to be protected.
     * @return returns <code>true</code> if content document can't be deleted
     */
    private static boolean preventDeletion(ContentVersion cv, Map<Id, ContentDocument> cDocs) {
        String errorMessage = null;
        Id contentDocId = cv.ContentDocumentId;
        loadRecordTypeIds();

        /*
        * prevents deletion of document referred by onboarding step task
        */
        if(cv.RecordTypeId == cqDocumentRecordTypeId){
            errorMessage  = Label.SQX_ERR_MSG_CQ_DOCUMENT_CANNOT_BE_DELETED;
        } else if(cv.RecordTypeId == scormTypeContentId &&
            cv.Assessment__c != null) {
            /*
            * Prevents content referenced by scorm type assessment from being deleted directly. To delete a content document the related
            * assessment must be first deleted
            */
            errorMessage = Label.SQX_ERR_MSG_ASSESSMENT_CONTENT_CANNOT_BE_DELETED;

        } else if(cv.RecordTypeId == collaborationContentRecordTypeId){
            /*
            *   Prevents content associated with a Controlled Doc collaboration group to be deleted
            */
            if(cDocs.containsKey(cv.Controlled_Document__r.Collaboration_Group_Content_Id__c)){
                errorMessage = Label.CQ_UI_Cannot_Delete_Collaboration_Content;
                contentDocId = cv.Controlled_Document__r.Collaboration_Group_Content_Id__c;
            }

        } else if(cv.RecordTypeId == SQX_ContentVersion.getRegulatoryContentRecordType()){
                errorMessage = Label.CQ_ERR_MSG_REGULATORY_CONTENT_CANNOT_BE_UPDATED_OR_DELETED;
        } else {
            /*
            * Prevents content document from being deleted directly. To delete a content document the related
            * controlled document must be first deleted
            */
            if(!SQX_Controlled_Document.allowDeletionOf(new ContentDocument(Id = cv.ContentDocumentId)) && cv.Controlled_Document__c != null){
                errorMessage = Label.SQX_ERR_MSG_CONTROLLED_DOC_CONTENT_CANNOT_BE_DELETED;
            }
        }

        if(!String.isEmpty(errorMessage)) {
            cDocs.get(contentDocId).addError(errorMessage);
        }

        return !String.isEmpty(errorMessage);
    }

    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newDocs equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<ContentDocument> newDocs, Map<Id,ContentDocument> oldMap){

            super(newDocs, oldMap);
        }

        /**
        * prevents content document from being archived directly. To archive a content document the related
        * controlled document must be first obsoleted.
        */
        public Bulkified preventControlledDocFromGettingArchivedDirectly(){

            Rule isBeingArchived = new Rule();
            isBeingArchived.addRule(Schema.ContentDocument.IsArchived, RuleOperator.Equals, true);

            this.resetView()
                .applyFilter(isBeingArchived, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            Map<Id, ContentDocument> docs = new Map<Id, ContentDocument>((List<ContentDocument>) isBeingArchived.evaluationResult);

            if(docs.keySet().size() > 0) {

                for(ContentVersion cv : [SELECT ContentDocumentId FROM ContentVersion WHERE
                                        ContentDocumentId IN: docs.keySet() AND IsLatest = true
                                        AND Controlled_Document__c != null
                                        AND Controlled_Document__r.Document_Status__c !=: SQX_Controlled_Document.STATUS_OBSOLETE])
                {
                    docs.get(cv.ContentDocumentId).addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_CONTENT_CANNOT_BE_ARCHIVED);
                }
            }

            return this;
        }


        /**
        * Prevents content referenced by scorm type assessment which is not obsolete from being archived directly. To archive a content document the related
        * assessment must be first obsoleted.
        */
        public Bulkified preventAssessmentFromGettingArchived(){

            Rule isArchived = new Rule();
            isArchived.addRule(ContentDocument.IsArchived, RuleOperator.Equals, true);

            this.resetView()
                .applyFilter(isArchived, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<ContentDocument> archivedDocs = (List<ContentDocument>) isArchived.evaluationResult;

            if(archivedDocs.size() > 0){
                Map<Id, ContentDocument> cDocs = new Map<Id, ContentDocument> (archivedDocs);

                Set<Id> contentVersionIds = getIdsForField(Schema.ContentDocument.LatestPublishedVersionId);

                Id scormTypeContentId = SQX_ContentVersion.getScormContentRecordTypeId();
                for(ContentVersion cv : [SELECT ContentDocumentId, RecordTypeId, Assessment__c, Assessment__r.Status__c  FROM ContentVersion WHERE Id IN: contentVersionIds]){

                    if(cv.RecordTypeId == scormTypeContentId &&
                       cv.Assessment__c != null &&
                       cv.Assessment__r.Status__c != SQX_Assessment.STATUS_OBSOLETE)
                    {
                        cDocs.get(cv.ContentDocumentId).addError(Label.SQX_ERR_MSG_ASSESSMENT_CONTENT_CANNOT_BE_ARCHIVED);
                    }

                }
            }
            return this;
        }


        /**
        * method to prevent deletion of content document
        */
        public Bulkified preventCQDocumentGettingDeleted(){

            Map<Id, ContentDocument> cDocs = new Map<Id, ContentDocument> ( (List<ContentDocument>)this.resetView().view );

            Set<Id> contentVersionIds = getIdsForField(Schema.ContentDocument.LatestPublishedVersionId);

            List<ContentVersion> cvs = [SELECT ContentDocumentId,
                                        RecordTypeId,
                                        Assessment__c,
                                        Assessment__r.Status__c,
                                        Controlled_Document__r.Collaboration_Group_Content_Id__c
                                        FROM ContentVersion WHERE Id IN: contentVersionIds];


            Set<Id> docs = new Set<Id>();

            Set<Id> contentsToCheckFurther = new Set<Id>();

            for(ContentVersion cv : cvs){
                if(!preventDeletion(cv, cDocs)) {
                    contentsToCheckFurther.add(cv.ContentDocumentId);
                }
            }

            enforceComplexDeletionRules(new List<Id>(contentsToCheckFurther), cDocs);

            return this;
        }

        private void enforceComplexDeletionRules(Id [] documentIds, Map<Id, ContentDocument> documents) {
            if(!documentIds.isEmpty()) {
                Map<Id, Set<Id>> contentsByAudit = new Map<Id, Set<Id>>();
                Set<Id> allAudits = new Set<Id>();
                for(ContentDocumentLink docLink : [SELECT Id, LinkedEntityId, LinkedEntity.Type, ContentDocumentId
                     FROM ContentDocumentLink
                     WHERE ContentDocumentID = : documentIds]) {
                    if(docLink.LinkedEntity.Type == SQX.Audit) {
                        if(!contentsByAudit.containsKey(docLink.LinkedEntityId)) {
                            contentsByAudit.put(docLink.LinkedEntityId, new Set<Id>());
                        }
                        contentsByAudit.get(docLink.LinkedEntityId).add(docLink.ContentDocumentId);
                        allAudits.add(docLink.LinkedEntityId);
                    }
                }

                if(!allAudits.isEmpty()) {
                    for(SQX_Audit__c audit: [SELECT Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id IN : allAudits]){
                        for(Id contentDocumentId : contentsByAudit.get(audit.Id)) {
                            // TODO: Change label in next release
                            documents.get(contentDocumentId).addError(Label.SQX_ERR_MSG_CQ_DOCUMENT_CANNOT_BE_DELETED);
                        }
                    }
                }
            }

        }
    }
}