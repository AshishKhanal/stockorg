/**
 * Test class to ensure that when regulatory report is created a reporting default is assigned to it
 * @author: Sanjay Maharjan
 * @date: 2019 - 01 - 25
 * @story: [SQX-7561]
 */
@isTest
public class SQX_Test_7561_Set_Reporting_Default {
    
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
	}
    
    /**
     * GIVEN : A reporting default
     * WHEN  : A regulatory report is created
     * THEN  : Reporting default is set to newly created regulatory report
     */
    public testMethod static void givenReportingDefault_WhenRegulatoryReportIsCreated_ThenReportingDefaultIsSet() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Reporting_Default__c reportingDefault;
        
        System.runAs(adminUser) {
            // ARRANGE : Create Reporting Default
            reportingDefault = new SQX_Reporting_Default__c();
            reportingDefault.Name = 'New Reporting Default';

            new SQX_DB().op_insert(new List<SQX_Reporting_Default__c>{reportingDefault}, new List<Schema.SObjectField>());
        }
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ACT : Create regulatory report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Due_Date__c = Date.today();
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<Schema.SObjectField>());
            
            regulatoryReport = [SELECT Id, SQX_Reporting_Default__c FROM SQX_Regulatory_Report__c WHERE Id =: regulatoryReport.Id];
            reportingDefault = [SELECT Id FROM SQX_Reporting_Default__c WHERE Name =: 'New Reporting Default'];
            
            // ASSERT : Reporting default is set to newly created regulatory report
            System.assertNotEquals(null, regulatoryReport.SQX_Reporting_Default__c, 'Reporting default should be set to newly created regulatory report');
            System.assertEquals(reportingDefault.Id, regulatoryReport.SQX_Reporting_Default__c, 'Expected reporting default id to be : ' + reportingDefault.Id + ' but was : ' + regulatoryReport.SQX_Reporting_Default__c);
            
        }
        
    }

    /**
     * GIVEN : A reporting default record is not present
     * WHEN  : A regulatory report is created
     * THEN  : Regulatory report should have null reporting default
     */
    public testMethod static void givenReportingDefaultNotPresent_WhenRegulatoryReportIsCreated_ThenReportingDefaultIsSetNull() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            // ACT : Create regulatory report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Due_Date__c = Date.today();
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<Schema.SObjectField>());
            
            regulatoryReport = [SELECT Id, SQX_Reporting_Default__c FROM SQX_Regulatory_Report__c WHERE Id =: regulatoryReport.Id];
            
            // ASSERT : Reporting default is set to null in newly created regulatory report
            System.assertEquals(null, regulatoryReport.SQX_Reporting_Default__c, 'Reporting default should be set to newly created regulatory report');
            
        }
        
    }
    
    /**
     * @author : Sanjay Maharjan
     * @date : 2019 - 04 - 05
     * @story : [SQX-8455]
     * GIVEN : A reporting default set to Regulatory Report
     * WHEN  : A regulatory report is created
     * THEN  : Then same Reporting Default is set on Regulatory Report
     */
    public testMethod static void givenReportingDefaultSetToRegulatoryReport_WhenRegulatoryReportIsCreated_ThenSameReportingDefaultIsSet() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Reporting_Default__c reportingDefault1, reportingDefault2;
        
        System.runAs(adminUser) {
            // ARRANGE : Create Reporting Default
            reportingDefault1 = new SQX_Reporting_Default__c();
            reportingDefault1.Name = 'New Reporting Default 1';
            
            reportingDefault2 = new SQX_Reporting_Default__c();
            reportingDefault2.Name = 'New Reporting Default 2';

            new SQX_DB().op_insert(new List<SQX_Reporting_Default__c>{reportingDefault1, reportingDefault2}, new List<Schema.SObjectField>());
        }
        
        System.runAs(standardUser) {
            
            // ARRANGE : Create complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser).save();
            
            reportingDefault1 = [SELECT Id FROM SQX_Reporting_Default__c WHERE Name =: 'New Reporting Default 1'];
            reportingDefault2 = [SELECT Id FROM SQX_Reporting_Default__c WHERE Name =: 'New Reporting Default 2'];

            // ACT : Create regulatory report
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Due_Date__c = Date.today();
            regulatoryReport.SQX_Reporting_Default__c = reportingDefault2.Id;
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<Schema.SObjectField>());
            
            regulatoryReport = [SELECT Id, SQX_Reporting_Default__c FROM SQX_Regulatory_Report__c WHERE Id =: regulatoryReport.Id];
            
            // ASSERT : Reporting default is set as required
            System.assertNotEquals(reportingDefault1.Id, regulatoryReport.SQX_Reporting_Default__c, 'Expected reporting default id not to be : ' + reportingDefault1.Id + ' but was : ' + regulatoryReport.SQX_Reporting_Default__c);
            System.assertEquals(reportingDefault2.Id, regulatoryReport.SQX_Reporting_Default__c, 'Expected reporting default id to be : ' + reportingDefault2.Id + ' but was : ' + regulatoryReport.SQX_Reporting_Default__c);
        }
        
    }

}