/**
 *  Controller for the component that handles Regulatory Report submission
*/

public class SQX_Regulatory_Report_Submit_Controller {

    /** All object types that support Regulatory Submissions */
    static final Map<SObjectType, SQX_Regulatory_Report_eSubmitter> eSubmitters = new Map<SObjectType, SQX_Regulatory_Report_eSubmitter> {
        SQX_Medwatch__c.SObjectType => new SQX_Medwatch_eSubmitter(),
        SQX_MedDev__c.SObjectType => new SQX_MedDev_eSubmitter(),
        SQX_Canada_Report__c.SObjectType => new SQX_Canada_Report_eSubmitter(),
        SQX_General_Report__c.SObjectType => new SQX_General_Report_eSubmitter()
    };

    /**
    *   Send a submission request for the given Regulatory record
    */
    @AuraEnabled
    public static String submit(String recordId) {

        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.submit(recordId);

        } catch (Exception ex) {
             throw new AuraHandledException(ex.getMessage());
        }

    }

    /**
     * Method to invoke to perform SF Validations (i.e invoke validation rules)
     * Ideally, to be called before sending request to external service to validate
     * @return validation result info (list of errors along with information on whether or not the reg.record supports XML validation)
    */
    @AuraEnabled
    public static Map<String, Object> validateInSF(String recordId) {

        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            Map<String, Object> validationResult = new Map<String, Object>();

            validationResult.put('errors', submitter.validateInSF(recordId));
            validationResult.put('supportsXMLValidation', submitter.supportsXMLValidation());

            return validationResult;

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }

    }

    /**
    *   Send a (XML)validation request for the given Regulatory record
    */
    @AuraEnabled
    public static String validate(String recordId) {

        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.validate(recordId);

        } catch (Exception ex) {
             throw new AuraHandledException(ex.getMessage());
        }

    }


    /**
     * Method to poll the status of the given job from server
    */
    @AuraEnabled
    public static String poll(String jobId, String recordId) {
        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.poll(jobId);

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }


    /**
     * Method to poll the status of the given job from server
     * It differs from the 'poll' method in that this method doesn't just poll the status from the external server
     * After polling the status of the job, this checks if the submission record has been created or not
    */
    @AuraEnabled
    public static String pollSubmission(String jobId, String recordId) {
        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.pollSubmission(jobId, recordId);

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }


    /**
     * Method to send PDF export request for the given regulatory record
    */
    @AuraEnabled
    public static String submitPDFGenerationRequest(String recordId) {
        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.exportToPDF(recordId);

        } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     *  Method is used to preset the regulatory record before submitting
     *  One such presetting is to set the auto generated Submission # in case of Medwatch and MedDev
     */
    @AuraEnabled
    public static List<String> prepareRecordForSubmission(Id recordId) {

        try {

            SQX_Regulatory_Report_eSubmitter submitter = getSubmitterFor(recordId);

            return submitter.prepareRecordForSubmission(recordId);

        } catch (Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }



    /**
     * Returns the submitter based on the type of Regulatory record
    */
    private static SQX_Regulatory_Report_eSubmitter getSubmitterFor(String recordId) {

        SQX_Regulatory_Report_eSubmitter submitter = eSubmitters.get(Id.valueOf(recordId).getSObjectType());

        System.assert(submitter != null, 'No submitter found for the given record type');

        return submitter;
    }

}