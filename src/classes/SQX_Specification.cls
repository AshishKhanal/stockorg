/*
 * class to store the methods to perform the actions specified in trigger
 */
public with sharing class SQX_Specification {

    public static final String IMPACT_LOW = 'Low',
                                IMPACT_MEDIUM = 'Medium',
                                IMPACT_HIGH = 'High';
                                
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(){
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newSpecifications equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Specification__c> newSpecifications, Map<Id,SQX_Specification__c> oldMap){
            
            super(newSpecifications, oldMap);
        }
        
        /**
        * prevents modification of specification of the active inspection criteria documents
        * document in release approval can be modified by user with supervisor permission
        */
        public Bulkified preventModificationOfSpecification(){
            
            this.resetView();

            if(this.view.size() > 0){
                // get Specification ids
                Set<Id> ids = this.getIdsForField(SQX_Specification__c.SQX_Inspection_Criteria__c);
                
                Map<Id, SQX_Controlled_Document__c> docMap = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Document_Status__c, Approval_Status__c FROM SQX_Controlled_Document__c WHERE Id IN: ids]);
                for(SQX_Specification__c specification : (List<SQX_Specification__c>) this.view){
                    SQX_Controlled_Document__c inspectionCriteria = docMap.get(specification.SQX_Inspection_Criteria__c);
                    if(inspectionCriteria.Document_Status__c != SQX_Controlled_Document.STATUS_DRAFT || 
                        ((inspectionCriteria.Approval_Status__c == SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL || 
                         inspectionCriteria.Approval_Status__c == SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL) && 
                        !SQX_Utilities.checkIfUserHasPermission(SQX_Controlled_Document.CUSTOM_PERMISSION_DOCUMENT_SUPERVISOR)) ){

                        specification.addError(Label.SQX_ERR_MSG_SPECIFICATION_CANNOT_BE_CHANGED_FOR_ACTIVE_DOCUMENTS);
                    }
                }
            }
            return this;
        }
    }

}