/**
* This controller is used to fetch schema of any sObject(s) of a provided Salesforce Object(s).
* @author Pradhanta Bhandari
* @date 2014/3/26
*/
public with sharing class SQX_Controller_Schema_Generator {
     static final String OBJECT_FIELD_SEPARATOR = '.';
     static Map<String, Set<String>> fetchedPickListValues;

    /**
    * This is the list of fields that will be ignored when dumping the schema
    * because they are protected fields and no manipulation is allowed
    * Moreover, they interfere with the UI framework
    */
     private final Set<String> fieldIgnoreList= 
      new Set<String>
        { 
            'SystemModstamp',
            'LastReferencedDate',
            'LastModifiedById',
            'LastViewedDate',
            'CreatedById',
            'CreatedDate',
            'IsDeleted'
        };
        
    /**
    * This is the list of relationships that will be ignored when constructing the schema
    */
    private final Set<String> relationshipIgnoreList = 
        new Set<String>
        {
            'Tags',
            'AttachedContentDocuments',
            'null',
            'CombinedAttachments',
            'Shares', 
            'ProcessInstances', 
            'ProcessSteps', 
            'Tasks', 
            'ActivityHistories', 
            'FeedSubscriptionsForEntity', 
            'Events', 
            'NotesAndAttachments', 
            'OpenActivities', 
            'Histories', 
            'Feeds'
        };
    
    public static final String PARAM_SCHEMA_OF = 'schemaof'; /** the GET Param used for sending the type of object whose schema is to be retrieved */
    public static final String PARAM_ASSIGN_TO = 'assignto'; /** the GET Param for sending the JS variable name that the schema will be assigned to in the namespace */
    public static final String PARAM_PRETTIFY = 'prettify'; /** the GET Param if present will prettify the returned Javascript */
    public static final String PARAM_NAMESPACE = 'ns'; /** the GET Param to configure the namespace within which the assignTo is set. default being SQXSchema*/
    public static final String DEFAULT_NAMESPACE = 'SQXSchema'; /** Default namespace (container object) used by the returned object */
    

    /**
    *   returns the JSON Schema of an object, the object whose schema is required must be
    *              sent as a get parameter 'schemaof'
    *
    *              HTTP Get request Parameters:
    *              schemaof : the name of the object whose schema is to be fetched
    *              assignto : the name of the variable to assign the result to
    *              prettify : <code>true</code> if the returned JSON is to be pretty printed
    *                         <code>false</code> or nothing if it is to be compacted.
    *
    * @author Pradhanta Bhandari
    * @date 2014/3/26
    * @return returns the schema of the object if valid, else return a zero length string.
    */
    public String getSchemaJSON(){     
        String schemaOf = ApexPages.currentPage().getParameters().get(PARAM_SCHEMA_OF);
        String assignTo = ApexPages.currentPage().getParameters().get(PARAM_ASSIGN_TO);
        String prettify = ApexPages.currentPage().getParameters().get(PARAM_PRETTIFY);
        String serializedJSON = '{}';

        if(schemaOf != null){
            Schema.SObjectType objectType = Schema.getGlobalDescribe().get(schemaOf);


            if(objectType != null){
                Schema.DescribeSObjectResult objectDescription = objectType.getDescribe();
                assignto = assignto == null ? objectDescription.getName() : assignto;
                assignto = getNameSpace() + '.' + assignto;

                if(prettify == null){
                    serializedJSON = JSON.serialize(getSchemaFor(objectDescription));
                } else {
                    serializedJSON = JSON.serializePretty(getSchemaFor(objectDescription));
                }
            }
        }
        
        return assignTo + ' = ' + serializedJSON; //empty by default
    }

    /**
    * Returns the list of languages that are available for use in the Salesforce org.
    */
    public List<SelectOption> getLanguages(){
        List<SelectOption> options = new List<SelectOption>();

        for(Schema.PicklistEntry entry : User.LanguageLocaleKey.getDescribe().getPicklistValues()){
            SelectOption option = new SelectOption(entry.value, entry.label);
            options.add(option);
        }

        return options;
    }

    /**
    * getter for the namespace to group the schema
    * @author Pradhanta Bhandari
    * @date 2014/4/9
    */
    private string getNameSpace(){
        String nameSpace = ApexPages.currentPage().getParameters().get(PARAM_NAMESPACE);
        nameSpace = nameSpace == null || nameSpace.trim().length() == 0 ? DEFAULT_NAMESPACE : nameSpace;

        return nameSpace;
    }

    /**
    * returns the namespace initializer if provided else defaults to SQX
    * @author Pradhanta Bhandari
    * @date 2014/4/9
    */
    public String getNameSpaceInitializer(){
        String nameSpace = getNameSpace();

        return '"use strict"; var ' + nameSpace + ' = ' + nameSpace + ' || {} ;';
    }

    /**
    * This method creates the ObjectSchema of the object
    * @author Pradhanta Bhandari
    * @date 2014/3/26
    * @param objectType the object for which the schema is to be generated
    * @return returns a valid object schema
    */
    private ObjectSchema getSchemaFor(Schema.DescribeSObjectResult objectType){
        
        ObjectSchema schema = new ObjectSchema();
        Map<String, Schema.SObjectField> fields = objectType.fields.getMap();
        
        //1. Id is always the primary key, and sObjectName the name of the object
        schema.id = 'Id';
        schema.sObjectName = objectType.getName();
        schema.sObjectLabel = objectType.getLabel();
        
                    
        //2. Add the child relationships of the object by iterating through the relationships
        schema.childRelationships = new Set<ObjectRelationship>();
        for(Schema.ChildRelationship relationship : objectType.getChildRelationships()){
            String name = relationship.getRelationshipName();

            //ignore if the relationship's name is null (don't know why SF returns this ;-) )
            //          or is contained in the ignore list
            if(name == null || relationshipIgnoreList.contains(name)) {
                continue;
            }
                

            //add the relationship to the child relationship
            ObjectRelationship relation = new ObjectRelationship();
            relation.relationshipName = name;
            relation.childSObject = '' + relationship.getChildSObject();
            relation.field = '' + relationship.getField(); //connector field
            relation.setNullOnDelete = !relationship.isRestrictedDelete(); //child should be deleted if child cannot exist without parent
            relation.isMasterDetail = relationship.getField().getDescribe().getRelationshipOrder() != null;
            
            schema.childRelationships.add(relation);
        }
        
        
        //3. Get the fields and add them to the schema one by one
        for(Schema.SObjectField field : fields.values()){
            Schema.DescribeFieldResult fieldResult = field.getDescribe();
            
            //ignore any field in ignore list
            if(fieldIgnoreList.contains(fieldResult.getName())){
                continue;
            }
                
            ObjectField sField = new ObjectField();
            
            //sField.nullable = fieldResult.isNillable();
            sField.editable = fieldResult.isUpdateable(); 
            sField.createable = fieldResult.isCreateable();
            sField.additionalInfo.put('isNameField', fieldResult.isNameField());
            
            DisplayType fieldResultType = fieldResult.getType(); 
            
            //map sf type to javascript framework type
            /*
              DisplayType.Boolean  => boolean
              
              DisplayType.DateTime => date //we are ignoring date time type
              DisplayType.Date => date

              DisplayType.Currency , Double, Integer, Percent => number  //however scale, maxlength etc are added

              DisplayType.String, TextArea, Email, URL, Phone => string

              //TODO: add validation rules /type based on kendo

              DisplayType.Picklist, MultiPickList => values //adds the valid list of values

              DisplayType.Reference => reference

              DisplayType.Id => string

              All others given an assertion error.

            */
            if(fieldResultType  == DisplayType.Boolean){
                sField.type = 'boolean';
            }
            else if(fieldResultType  == DisplayType.DateTime){
                sField.type = 'datetime';
                sField.nullable = true;
            }
            else if(fieldResultType == DisplayType.Date){
                sField.type = 'date';
                sField.nullable = true;
            }
            else if(fieldResultType == DisplayType.Currency
                    || fieldResultType == DisplayType.Double
                    || fieldResultType == DisplayType.Integer
                    || fieldResultType == DisplayType.Percent){
                sField.type = 'number';
                sField.additionalInfo.put('scale', fieldResult.getScale());
                integer maxLength = fieldResult.getPrecision();
                if(fieldResult.getScale() > 0) {
                    maxLength++; //add one extra character to accomodate decimal point
                }
                sField.validation.put('maxlength', maxlength);
                
            }
            else if(fieldResultType == DisplayType.String
                    || fieldResultType == DisplayType.TextArea
                    || fieldResultType == DisplayType.Email
                    || fieldResultType == DisplayType.URL
                    || fieldResultType == DisplayType.Phone
                    || fieldResultType == DisplayType.COMBOBOX){
                sField.type = 'string';
                sField.validation.put('maxlength', fieldResult.getLength());
                sField.additionalInfo.put('richtext', fieldResult.isHtmlFormatted());
                sField.additionalInfo.put('multiline', fieldResultType == DisplayType.TextArea);
                
                if(fieldResultType == DisplayType.Email){
                  sField.additionalInfo.put('inputType', 'email');
                }

            }
            
            else if(fieldResultType == DisplayType.PickList
                    || fieldResultType == DisplayType.MultiPicklist){
                    sField.type = 'values';
                    SObjectField controllingField = fieldResult.getController();

                    sField.additionalInfo.put('allowMultiSelect', fieldResultType == DisplayType.MultiPicklist);

                    //for picklists/multi-picklist also supply the list of possible values as additional info
                    List<Map<String,Object>> validValues = new List<Map<String,Object>>();
                    Map<String, List<String>> validFor = new Map<String, List<String>>();

                    if(controllingField != null){
                        sField.additionalInfo.put('controllingField', '' + controllingField);
                    }

                    for(Schema.PicklistEntry pickListItem : fieldResult.getPicklistValues()){
                        Map<String, Object> valueOpt = new Map<String,Object>();
                        valueOpt.put('value', pickListItem.getValue());
                        valueOpt.put('text', pickListItem.getLabel());

                        if(controllingField != null && validFor.containsKey(pickListItem.getValue())){
                            valueOpt.put('validFor', validFor.get(pickListItem.getValue()));
                        }

                        validValues.add(valueOpt);
                    }

                    sField.additionalInfo.put('validValues', validValues);
            }
            else if(fieldResultType == DisplayType.Reference){
                sField.type = 'reference';
                //note: we are getting only the first reference because none of the custom fields created will return multiple values
                sField.additionalInfo.put('referenceTo', fieldResult.getReferenceTo().get(0).getDescribe().getName());

                if(fieldResult.FilteredLookupInfo != null) {
                    sField.additionalInfo.put('filteredInfo', fieldResult.FilteredLookupInfo);
                }
            }
            else if(fieldResultType == DisplayType.Id){
                sField.type = 'string';
            }
            else if(fieldResultType == DisplayType.Base64){
                sField.type = 'blob';
            }
            else{
                //better break than give erroneous result
                //we are ignoring some field types
                System.assert(false, 'Unsupported data type found ' + fieldResultType + ' in field ' + fieldResult.getName() );
            }
            
            boolean isRequired = !fieldResult.isAutoNumber() && !fieldResult.isCalculated() && !fieldResult.isNillable();
            if(isRequired && fieldResultType != DisplayType.Boolean){
                //since kendo doesn't support required boolean

                sField.validation.put('required', true);
            }
            
            //name of the field
            sField.name = fieldResult.getLabel();
            sField.additionalInfo.put('apiname', fieldResult.getName());

            //helptext
            sField.name=fieldResult.getLabel();

            String inlineHelpText = fieldResult.inlineHelpText;
            if(inlineHelpText != null) {
                sField.additionalInfo.put('helptext', fieldResult.inlineHelpText);
            }

            Object defaultvalue = fieldResult.DefaultValueFormula;
            defaultvalue = defaultvalue == null  ? fieldResult.DefaultValueFormula : defaultvalue;
            if(defaultvalue != null) {
                sfield.additionalInfo.put('defaultvalue', defaultvalue);
            }

            schema.fields.put(fieldResult.getName(), sField);
        }

        return schema;
    }

    /**
    * represents a single JSON field object of the schema i.e. used to represent individual field
    * @author Pradhanta Bhandari
    * @date 2014/3/26
    */
    public class ObjectField{
        public String type {get; set; } //type of field based on JS framework
        public String name {get; set; } //SF name of this field
        public boolean editable { get; set; } //true if editable
        public boolean createable { get; set; } //true if creatable
        public boolean nullable { get; set; } //true if nullable
        public Map<String, Object> validation { get; set; } //Map to add validation rule as required
        public Map<String, Object> additionalInfo { get; set; } //Anything else can be passed using additional info

        
        /*
        * default constructor that intializes the maps to non-null object.
        */
        public ObjectField(){
            validation = new Map<String, Object>();
            additionalInfo = new Map<String, Object>();
            
        }
    }
    
    /**
    * represents the child relationship of an object
    * @author Pradhanta Bhandari
    * @date 2014/3/26
    */
    public class ObjectRelationship{
        public String relationshipName { get; set;}
        public String childSObject { get; set;}
        public String field { get; set; }
        public Boolean setNullOnDelete {get; set;}
        public Boolean isMasterDetail {get; set;}
    }
    
    
    /**
    * represents the schema of the object.
    * @author Pradhanta Bhandari
    * @date 2014/3/26
    */
    public class ObjectSchema{
        public String sObjectName { get; set; }
        public String sObjectLabel { get; set; }
        public String id {get; set; }
        
        public Map<String, ObjectField> fields { get ; set;}
        
        public Set<ObjectRelationship> childRelationships {get; set;}

        
        public ObjectSchema(){
            fields = new Map<String, ObjectField>();

        }
    }

      public String val {get;set;}
     
      public List<SelectOption> getName()
      {
            List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
            List<SelectOption> options = new List<SelectOption>();
            
            for(Schema.SObjectType f : gd)
            {
               options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getLabel()));
            }
            return options;
       }

        /**
         * get added objects from custom metadata
         */
        public Set<String> getAddedCustomObjects(){
            List<CQ_Related_Object_Addition_Support__mdt> relatedObjs = new List<CQ_Related_Object_Addition_Support__mdt>();
            relatedObjs = [SELECT Id, Related_Object__c FROM CQ_Related_Object_Addition_Support__mdt];
            Set<String> addedObjs = new Set<String>();

            for(CQ_Related_Object_Addition_Support__mdt relatedObj : relatedObjs){
                addedObjs.add(relatedObj.Related_Object__c);
            }
            return addedObjs;
        }

}