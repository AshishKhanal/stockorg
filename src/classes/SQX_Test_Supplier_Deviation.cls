/**
 * factory class to create required records for supplier deviation
 */
@isTest
public class SQX_Test_Supplier_Deviation {
    public SQX_Supplier_Deviation__c sd;


    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser'); 
    }
    
    /**
     * constructor method to create supplier deviation with required fields
     */
    public SQX_Test_Supplier_Deviation(){
        Account account = SQX_Test_Account_Factory.createAccount();
        Contact contact = SQX_Test_Account_Factory.createContact(account);
        SQX_Part__c testPart = SQX_Test_Part.insertPart(null,new User(Id = UserInfo.getUserId()), true, '');
        sd = new SQX_Supplier_Deviation__c(Name = 'TestName', 
                                           Phone__c = '1234567890',
                                           SQX_Account__c = account.Id, 
                                           SQX_External_Contact__c = contact.Id,
                                           SQX_Part__c = testPart.Id,
                                           Result__c = 'Approved');
    }
    
    /**
     * method to set status of supplier deviation
     */
    public SQX_Test_Supplier_Deviation setStatus(String status){
        sd.Status__c = status;
        return this;
    }
    
    
    /**
     * method to set stage of supplier deviation
     */
    public SQX_Test_Supplier_Deviation setStage(String stage){
        sd.Record_Stage__c = stage;
        return this;
    }
    
    /**
     * method to save/update supplier deviation
     */
    public SQX_Test_Supplier_Deviation save(){
        if(sd.Id == null){
            new SQX_DB().op_insert(new List<SQX_Supplier_Deviation__c>{sd}, 
                                   new List<SObjectField> {SQX_Supplier_Deviation__c.Name, 
                                                            SQX_Supplier_Deviation__c.Phone__c,
                                                            SQX_Supplier_Deviation__c.SQX_Account__c, 
                                                            SQX_Supplier_Deviation__c.SQX_External_Contact__c,
                                                            SQX_Supplier_Deviation__c.SQX_Part__c,
                                                            SQX_Supplier_Deviation__c.Result__c});
        } else {
            new SQX_DB().op_update(new List<SQX_Supplier_Deviation__c>{sd}, 
                                   new List<SObjectField> {SQX_Supplier_Deviation__c.Name, 
                                                            SQX_Supplier_Deviation__c.Phone__c,
                                                            SQX_Supplier_Deviation__c.SQX_Account__c, 
                                                            SQX_Supplier_Deviation__c.SQX_External_Contact__c,
                                                            SQX_Supplier_Deviation__c.SQX_Part__c,
                                                            SQX_Supplier_Deviation__c.Result__c});
        }
        
        return this;
    }


    /**
     * Method to sumit SD
     */
    public void submit() {
        this.sd.Activity_Code__c = 'submit';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     * Method to extend SD
     */
    public void extend() {
        this.sd.Activity_Code__c = 'extend';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
     * method to initiate supplier deviation
     */
    public void initiate() {
        this.sd.Activity_Code__c = 'initiate';
        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }
    
    /**
     *  method to close NSI
     */
    public void close() {
        this.sd.Activity_Code__c = 'close';
        this.sd.Is_Locked__c = true;

        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to close NSI
     */
    public void void() {
        this.sd.Activity_Code__c = 'void';
        this.sd.Is_Locked__c = true;

        this.save();

        SQX_BulkifiedBase.clearAllProcessedEntities();
    }

    /**
     *  method to clear existing field values
     */
    public void clear() {
        this.sd = [SELECT Id, Activity_Code__c FROM SQX_Supplier_Deviation__c WHERE Id=: this.sd.Id];
    }

    /**
     * method to restart SD
     */
    public void restart() {
        this.sd.Activity_Code__c = 'restart';
        this.sd.Current_Step__c = 1;
        this.save();
    }
    
    /**
     * method to redo deviation process
     */
    public void redoDeviationProcess(SQX_Deviation_Process__c deviationProcess){
        deviationProcess = [SELECT Id, 
                                 Step__c, 
                                 SQX_Parent__c, 
                                 Name, 
                                 SQX_User__c,
                                 Description__c, 
                                 Due_Date__c,
                                 Allowed_Days__c,
                                 RecordTypeId,  
                                 SQX_Controlled_Document__c 
                            FROM SQX_Deviation_Process__c 
                            WHERE Id =: deviationProcess.Id];

        SQX_Supplier_Redo_Step.RedoStepRequest req = new SQX_Supplier_Redo_Step.RedoStepRequest();
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_PRE;
        req.recordId = deviationProcess.Id;
        req.step = Integer.valueOf(deviationProcess.Step__c);
        req.assignee = deviationProcess.SQX_User__c;
        req.controlledDocument = deviationProcess.SQX_Controlled_Document__c;
        req.description = deviationProcess.Name;
        req.dueDate=deviationProcess.Due_Date__c;
        req.allowedDays=(Integer)deviationProcess.Allowed_Days__c;
        SQX_Supplier_Redo_Step.redoStep(req);
        
        req.actionType = SQX_Supplier_Redo_Step.ACTION_TYPE_POST;
        SQX_Supplier_Redo_Step.redoStep(req);
    }

    /**
    * method to add user to queue
    */
    public static void addUserToQueue(List<User> usersToAssign){
        SQX_Test_Utilities.addUserToQueue(usersToAssign, 'CQ_Supplier_Deviation_Queue', SQX.SupplierDeviation);
    }





    /*
        This number holds the number of SDs created which shall make the policy task name unique
    */
    static Integer sdNumber = 0;
    
    /**
     * method to create policy task of different task types
     */
    public static List<SQX_Task__c> createPolicyTasks(Integer numberOfTasks, String taskType, User assignee, Decimal step){
        List<SQX_Task__c> taskList = new List<SQX_Task__c>();
        for(Integer i = 0; i < numberOfTasks; i++){
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = assignee.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = 'Supplier Deviation', 
                                               Step__c = step,
                                               Active__c = true,
                                               Name = 'SD_' + sdNumber++ + '_Test_Policy_Task_' + i + 'Step ' + step,
                                               Task_Type__c = taskType);
                                               
            if(taskType == SQX_Task.TASK_TYPE_DOC_REQUEST){
                task.Document_Name__c = 'Sample Document Name';
            }
            taskList.add(task);
        }
        
        insert taskList;
        
        return taskList;
    }

    /*
    * GIVEN: New Supplier deviation
    * WHEN: Contact is left blank
    * THEN: Error is thrown
    * Story: SQX-7527
    */
    public static testmethod void whenContactIsLeftBlank_ThenErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            // Arrange: New Supplier deviation
            SQX_Test_Supplier_Deviation sd = new SQX_Test_Supplier_Deviation().save();
            
            // Act: Set the contact to null
            sd.sd.SQX_External_Contact__c = null;
            Database.SaveResult result = new SQX_DB().continueOnError().op_update(new List<SQX_Supplier_DEviation__c>{sd.sd}, new List<SObjectField>{})[0];
            
            // Assert: Error is thrown
            String expErrMsg = 'Supplier Contact is required.';
            System.assertEquals(false, result.isSuccess(), 'Record should not be saved while removing Supplier Contact');
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), expErrMsg), 'Expected error: ' + expErrMsg + ' Actual Errors: ' + result.getErrors());
        }
    }   
}