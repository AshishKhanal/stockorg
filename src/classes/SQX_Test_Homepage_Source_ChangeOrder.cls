/**
 * This class tests the source for Change Order homepage items
 * @author  Anuj Bhandari
 * @date    19-04-2017
 */

@isTest
public class SQX_Test_Homepage_Source_ChangeOrder {
    // static map to be used by the test CO items for their titles
    Static Map < string, string > coItems = new Map < string, string > {
        'draft' => 'test_coDraft',
        'open' => 'test_coOpen',
        'triage' => 'test_coTriage',
        'completed' => 'test_coCompletedWithEffDate',
        'incomplete' => 'test_coCompletedWithoutEffDate',
        'planRejected' => 'test_coPlanRejected',
        'planRejectedVoid' => 'test_coPlanRejectedVoid'
    };

    @testSetup
    /*
     * Creates the Change order items that are supposed to show up in hompepage component
     */
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');

        Group queue = new Group();
        queue.Name = 'TestGroup';
        queue.DeveloperName = 'Test_Queue';
        queue.Type = 'Queue';

        System.runAs(adminUser) {

            insert queue;

            QueueSObject sobj = new QueueSObject(SobjectType = SQX.ChangeOrder,
                                                 QueueId = queue.Id);
            insert sobj;

            GroupMember queueMembers = new GroupMember(GroupId = queue.Id, UserOrGroupId = standardUser.Id);
            insert queueMembers;
        }

        System.runAs(standardUser) {

            /* Arrange: Create 7 Change order items with
              1) Draft,
              2) Open,
              3) Triage,
              4) Plan Rejected with approval status rejected and status other than VOID (Eligible homepage item)
              5) Plan Rejected with approval status rejected and status of either VOID, COMPLETE and CLOSE (ineligible homepage item)
              6) Completed item with effective date (ineligible)
              7) Completed item without effective date (eligible)
            */

            
            // draft status item - Must appear in homepage
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order();
            co1.changeOrder.Title__c = coItems.get('draft');
            co1.save();

            // Open status item - Must appear in homepage
            SQX_Test_Change_Order co2 = new SQX_Test_Change_Order();
            co2.setStatus(SQX_Change_Order.STATUS_OPEN);
            co2.changeOrder.Title__c = coItems.get('open');
            co2.save();

            // Triage Item - Must appear in homepage
            SQX_Test_Change_Order co3 = new SQX_Test_Change_Order();
            co3.setStatus(SQX_Change_Order.STATUS_DRAFT);
            co3.changeOrder.Record_Stage__c =SQX_Change_Order.STAGE_TRIAGE;
            co3.changeOrder.Title__c = coItems.get('triage');
            co3.changeOrder.Queue_Name__c = queue.Name;
            co3.save();

            // Plan Rejected Item - Must appear in homepage
            SQX_Test_Change_Order co4 = new SQX_Test_Change_Order();
            co4.changeOrder.Approval_status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_REJECTED;
            co4.changeOrder.Title__c = coItems.get('planRejected');
            co4.save();

            // Completed Item without effective Date - Must appear in homepage
            SQX_Test_Change_Order co5 = new SQX_Test_Change_Order();
            co5.setStatus(SQX_Change_Order.STATUS_COMPLETE);
            co5.changeOrder.Record_Stage__c =SQX_Change_Order.STAGE_COMPLETE;
            co5.changeOrder.Title__c = coItems.get('incomplete');
            co5.save();

            // Completed Item with effective Date - Must NOT appear in homepage
            SQX_Test_Change_Order co6 = new SQX_Test_Change_Order();
            co6.setStatus(SQX_Change_Order.STATUS_COMPLETE);
            co5.changeOrder.Record_Stage__c =SQX_Change_Order.STAGE_COMPLETE;
            co6.changeOrder.Effective_Date__c = System.Date.today();
            co6.changeOrder.Title__c = coItems.get('completed');
            co6.save();

            // Plan Rejected Item with VOID status - Must NOT appear in homepage
            SQX_Test_Change_Order co7 = new SQX_Test_Change_Order();
            co7.changeOrder.Approval_status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_REJECTED;
            co7.setStatus(SQX_Change_Order.STATUS_VOID);
            co7.changeOrder.Title__c = coItems.get('planRejectedVoid');
            co7.changeOrder.Resolution_Code__c = 'voiding';
            co7.save();
        }
    }


    /**
     *   Given : A set of change order items (Open, Draft, Triage, Completed without effective date, Plan Rejected) and a homepage component expecting records from Change Order Source
     *   When :  CO Records are fetched and processed
     *   Then :  All CO items are added to the homepage with desired values
     *   @story -3162
     */
    testmethod static void givenChangeOrderItemsOfDifferentTypes_WhenRecordsAreFetchedAndProcessed_ThenCorrepondingHomepageItemsShouldBeAddedToHomepageComponent() {

        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // Arrange1: Fetch Items
            List < SQX_Change_Order__c > itms = [SELECT Id,
                                                        Name,
                                                        Title__c,
                                                        Status__c,Record_Stage__c,
                                                        Approval_Status__c,
                                                        Effective_Date__c,
                                                        CreatedDate,
                                                        OwnerId
                                                        FROM SQX_Change_Order__c
                                                    ];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List < SQX_Homepage_ItemSource > {
                new SQX_Homepage_Source_ChangeOrder_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert:Document items have been fetched with desired values

            System.assertNotEquals(hpItems, null);

            System.assertEquals(5, hpItems.size());

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertNotEquals(null, hpItem.itemAge);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_CHANGE_ORDER, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                SQX_Homepage_Item_Action act = hpitem.actions[0];

                System.assertEquals(false, act.supportsBulk);

                System.assertEquals(false, hpItem.supportsBulk);

                for (SQX_Change_Order__c itm: itms) {

                    if (itm.id == hpItem.itemId) {

                        system.assertNotEquals(itm.Title__c, coItems.get('planRejectedVoid'));

                        system.assertNotEquals(itm.Title__c, coItems.get('completed'));
                        
                        PageReference pr = new PageReference('/' + itm.Id);
                        pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
                        System.assertEquals(SQX_Homepage_Constants.SF_BASE_URL + pr.getUrl(), act.actionUrl);

                        if (itm.Title__c == coItems.get('draft')) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);

                            validateDraftFeed(itm, hpItem);

                            break;

                        } else if (itm.Title__c == coItems.get('triage')) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

                            validateTriageFeed(itm, hpItem);

                            break;

                        } else if (itm.Title__c == coItems.get('open')) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS, hpItem.actionType);

                            validateOpenFeed(itm, hpItem);

                            break;

                        } else if (itm.Title__c == coItems.get('incomplete')) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

                            validateCompletedFeed(itm, hpItem);

                            break;

                        } else if (itm.Title__c == coItems.get('planRejected')) {

                            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

                            validatePlanRejectedFeed(itm, hpItem);

                            break;

                        } else {

                            System.assert(false, 'Unknown record : ' + hpItem);

                        }
                    }
                }
            }
        }
    }


    /**
     *   Method validates the homepage item for draft item
     */
    private static void validateDraftFeed(SQX_Change_Order__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item for Triage item
     */
    private static void validateTriageFeed(SQX_Change_Order__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item for Completed item - without effective date
     */
    private static void validateCompletedFeed(SQX_Change_Order__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_CHANGE_ORDER_COMPLETED, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item for Plan Rejected item
     */
    private static void validatePlanRejectedFeed(SQX_Change_Order__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_REJECTED_ITEMS, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }

    /**
     *   Method validates the homepage item for Open records
     */
    private static void validateOpenFeed(SQX_Change_Order__c itm, SQX_Homepage_Item hpItem) {

        String expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, new String[] {
            itm.Name, itm.Title__c
        });

        System.assertEquals(expectedFeedText, hpItem.feedText);

    }
}