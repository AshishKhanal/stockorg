/**
* This test class is like a smoke test class because history tracking can't be unit tested based on
* a) Experimentation
* b) http://salesforce.stackexchange.com/questions/4007/is-it-possible-to-test-apex-that-relies-on-field-history-tracking
*/
@IsTest
public class SQX_Test_75_AuditTrailReport {


    /**
    * This test ensures that audit trail report for an object (Audit) is generated correctly.
    */
    public testmethod static void given_ARecordIsCreated_AuditTrailIsGeneratedCorrectly(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser){

            //Arrange: Create an audit with two findings  and change the status of the audit
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.save();

            audit.addAuditFinding().setRequired(false, false, false, false, false).save();
            audit.addAuditFinding().setRequired(false, false, false, false, false).save();

            DateTime beforeStatusChange = DateTime.Now();
            Integer sec = beforeStatusChange.second();

            while(sec == DateTime.Now().second()); //wait for a second or more

            audit.audit.Audit_Result__c = 'Pass';
            audit.save();


            //Act: Get the changes from the controller
            PageReference ref = Page.SQX_Audit_Trail_Report;
            ref.getParameters().put(SQX_Controller_Audit_Trail_Report.QUERY_PARAM_OBJID, audit.audit.Id);
            Test.setCurrentPage(ref);
            SQX_Controller_Audit_Trail_Report report = new SQX_Controller_Audit_Trail_Report();


            report.generateReport();

            //Assert: that history was returned correctly for the object

            //System.assertNotEquals(0, report.histories.size(), 'Report log: ' + report.debugLog);
            
            //Since we can't assert on results lets assert on the debug log
            System.assert(report.debugLog.contains('SQX_Audit__history'), 'Expected audit history to be queried. ' + report.debugLog);


            //Act: Set search child records to true
            report.includeChildren = true;
            report.debugLog = '';
            report.generateReport();

            //Assert: Ensure that finding was queried too.
            System.assert(report.debugLog.contains('SQX_Finding__history'), 'Expected finding history to be queried. ' + report.debugLog);

            //Act: Delete the audit and check for deleted audit
            delete audit.audit;

            report.includeDeleted = true;

            report.debugLog = '';
            report.generateReport();

            //Assert: Ensure that all rows was set so that the deleted record was queried
            System.assert(report.debugLog.contains('ALL ROWS'), 'Expected all rows to be added show that even deleted records are queried. ' + report.debugLog);

            //Ensure that created date is added in the filter
            report.changedFrom = DateTime.now();

            report.changedUntil = DateTime.now().addDays(1);

            report.debugLog = '';
            report.generateReport();

            //Assert: Ensure that filters are properly set
            System.assert(report.debugLog.contains('CreatedDate >= : changedFrom'), report.debugLog);
            System.assert(report.debugLog.contains('CreatedDate < : changedUntil'), report.debugLog);

            //Act: get changes made by a particular user
            report.Note.OwnerId = adminUser.Id;
            report.generateReport();

            //Assert: Ensure that filter for user is enforced.
            System.assert(report.debugLog.contains('CreatedById = : performedById'), report.debugLog);
        }
        

    }



    /**
    * This test ensures that audit trail report for an object (CAPA) is generated correctly.
    */
    public testmethod static void given_ACAPARecordIsSelected_ItsHistoryIsQueried(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser){

            //Arrange: Create an audit with two findings  and change the status of the audit
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility(adminUser);
            capa.save();


            //Act: Get the changes from the controller
            SQX_Controller_Audit_Trail_Report report = new SQX_Controller_Audit_Trail_Report();
            report.objectName = [SELECT Name FROM SQX_CAPA__c WHERE Id = : capa.capa.Id].Name;
            report.objectType = SQX.CAPA.toLowerCase();


            report.generateReport();

            //Assert: that history was returned correctly for the object
            System.assert(report.debugLog.contains('SQX_CAPA__history'), 'Expected capa history to be queried. ' + report.debugLog);


        }
        

    }


    /**
    * This test ensures that audit trail report for an object (Non conformance) is generated correctly.
    */
    public testmethod static void given_ANCRecordIsAuditSelected_ItsChildrenAreIncluded(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(adminUser){

            //Arrange: Create an audit with two findings  and change the status of the audit
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.save();
            nc.addImpactedPart(adminUser); //add an impacted product to the nc


            // Get the changes from the controller
            PageReference ref = Page.SQX_Audit_Trail_Report;
            ref.getParameters().put(SQX_Controller_Audit_Trail_Report.QUERY_PARAM_OBJID, nc.nc.Id);
            ref.getParameters().put(SQX_Controller_Audit_Trail_Report.QUERY_PARAM_DEL, SQX_Controller_Audit_Trail_Report.QUERY_PARAM_DEL_TRUE_VALUE);
            Test.setCurrentPage(ref);
            
            SQX_Controller_Audit_Trail_Report report = new SQX_Controller_Audit_Trail_Report();
            report.generateReport();


            //Act: Set search child records to true
            report.debugLog = '';
            report.includeChildren = true;
            report.generateReport();

            //Assert: Ensure that finding was queried too.
            System.assert(report.debugLog.contains('SQX_NC_Impacted_Product__history'), 'Expected finding history to be queried. ' + report.debugLog);

        }
        

    }

}