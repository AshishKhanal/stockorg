/**
 * This is controller for document folder management UI and provides basic functionality for the same.
 */
public with sharing class SQX_Document_Folder_Management {

    // the default size of page for dynamic query, assumption being that all records within the range will be fetched. TODO: need to support paging
    final static Integer PAGE_SIZE = 2000,
                         DEFAULT_PAGE = 1,
                         MAX_RELATED_SIZE = 200; //TODO: Remove maximum limitation on document folder related list and improve UI [SQX-3751]

    final static String WHERE_KEYWORD = 'WHERE';
    final static String CV_DOC_RELATION = 'compliancequest__Controlled_Document__r';

   /**
    * This method returns the detail record of the binder whose Id has been passed.
    * @param binderId The id of the binder whose detail is to be returned
    */
    @AuraEnabled
    public static SQX_Binder__c getBinder(String binderId){

        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
        filter.field = 'Id';
        filter.value = binderId;

        List<SObject> binders = SQX_DynamicQuery.getallFields(SQX_Binder__c.SObjectType.getDescribe(), new Set<Schema.SObjectField> {}, filter, new List<Schema.SObjectField> {}, true);

        if(binders.size() != 1){
            throw new AuraHandledException(Label.CQ_UI_Error_Occured_While_Getting_Binder);
        }
        
        return (SQX_Binder__c)binders.get(0);
    }
    
   /**
    * This method return the list of binders that are available. This method supports paging parameters
    * However, it should be noted that duplicate entries can be returned by this method if items in 
    * database gets shuffled.
    * @param pageNumber the page that is to be returned
    * @param pageSize the page size that is to be used
    */
    @AuraEnabled
    public static List<SObject> getBinderList(Integer pageNumber, Integer pageSize){
        final  List<Schema.SObjectField> fields = new List<Schema.SObjectField> { SQX_Binder__c.Name };

        return SQX_DynamicQuery.evaluate(1, 200, SQX_Binder__c.SObjectType.getDescribe(),fields, null, fields, true);
    }

    /**
     * Overload for use in tests and other references
     */
    public static Result getMatchingDocs(String globalSearchText,String searchText,String folderFilterString, String topicFilterString,String documentFilterString, String pageSize, String pageNumber, List<String> statuses) {
        return getMatchingDocs(globalSearchText, searchText, folderFilterString, topicFilterString, documentFilterString, pageSize, pageNumber, statuses, null, null);
    }

    /**
     * This method get document details
     * @param globalTextToMatch is a global search text
     * @param searchText is a search text on selected nodeitem doc details level
     * @param folderFilterString is a selected treeview nodeitem json object
     * @param topicFilterString is a selected treeview nodeitem json object
     * @param documentFilterString is a selected treeview nodeitem json 
     * @param pageSize number of records per page for the current binder [ Note : could have made it an Integer type parameter but this has an issue 
     *          refer to : https://developer.salesforce.com/forums/?id=906F00000005FxqIAE ]
     * @param pageNumber specifies the current page number based on which offset will be determined 
     * @param statuses is the list of document statuses that are to be returned
     */ 
    @AuraEnabled
    public static Result getMatchingDocs(String globalSearchText,String searchText,String folderFilterString, String topicFilterString,String documentFilterString, String pageSize, String pageNumber, List<String> statuses, List<String> sortBy, String sortOrder){

        Result res;

        String whereClause = '';
        String relationalWhereClause = '';
        Set<Id> folderDocumentIds = new Set<Id>(), topicDocumentIds = new Set<Id>();
        
        Boolean wasFolderFiltered = getDocumentsInFolder(folderFilterString, folderDocumentIds);
        Boolean wasTopicFiltered = getDocumentsWithTopic(topicFilterString, topicDocumentIds);

        Set<Id> limitDocumentsToId = null;

        if((wasTopicFiltered && !wasFolderFiltered) || (!wasTopicFiltered && wasFolderFiltered)){
            // if either of the folder or topic filter was applied

            limitDocumentsToId = new Set<Id>();

            limitDocumentsToId.addAll(folderDocumentIds);
            limitDocumentsToId.addAll(topicDocumentIds);
        }
        else if(wasTopicFiltered && wasFolderFiltered){
            
            limitDocumentsToId = new Set<Id>();

            // get the list of documents present in both folder and topic documents
            for(Id folderDocId : folderDocumentIds){
                if(topicDocumentIds.contains(folderDocId)){
                    limitDocumentsToId.add(folderDocId);
                }
            }
        }


        whereClause = getConditionStringForFilter(documentFilterString);

        Boolean emptySearch = String.isBlank(whereClause) && limitDocumentstoId == null;
        
        Integer pSize = pageSize == null ? null : Integer.valueOf(pageSize);
        Integer pNumber = pageNumber == null ? null : Integer.valueOf(pageNumber);
        if(String.isBlank(globalSearchText) && String.isBlank(searchText)){

            // return documents only if there is at least one of folder filter, document filter or topic filter
            if(!emptySearch){
                res = getMatchingDocuments(whereClause, limitDocumentstoId, pSize, pNumber, sortBy, sortOrder);
            }
        }
        else{
            if(!String.isBlank(globalSearchText)){
                res = findMatchingDocuments(globalSearchText, null, null, null, pSize, pNumber, sortBy, sortOrder); //ignore where clause and limiting docs based on filter if global search is performed.
            }
            else if(!emptySearch){
                relationalWhereClause = getConditionStringForFilter(documentFilterString, CV_DOC_RELATION);
                res = findMatchingDocuments(searchText, whereClause, relationalWhereClause, limitDocumentstoId, pSize, pNumber, sortBy, sortOrder);
            }
        }

        return res;
        
    }
    /**
     * get related documents for particular controlled document
     * @param documentId the id of the document whose related records are to be fetched
     * @param sortBy the fields to use to sort the result
     * @param sortOrder ascending/descending order
     */
    @AuraEnabled
    public static List<Document> getRelatedDocuments(String documentId, List<String> sortBy, String sortOrder){
        
        
        if(!SQX_Related_Document__c.SObjectType.getDescribe().isAccessible() || 
            !SQX_Related_Document__c.Referenced_Document__c.getDescribe().isAccessible() ||
            !SQX_Related_Document__c.Controlled_Document__c.getDescribe().isAccessible() ||
            !SQX_Related_Document__c.Comment__c.getDescribe().isAccessible() ||
            !SQX_Related_Document__c.Active__c.getDescribe().isAccessible()){
            throw new AuraHandledException(Label.CQ_UI_Error_Insufficient_Access);
        }



        List<Document> documents = new List<Document>();

        List<SQX_Related_Document__c> relatedDocs = [SELECT Name, Referenced_Document__c,
                         Active__c, Comment__c, Controlled_Document__c
                         FROM SQX_Related_Document__c WHERE Controlled_Document__c =: documentId ];

        if(relatedDocs.size() > 0){
            Map<Id, SQX_Related_Document__c> referencedByRelated = new Map<Id, SQX_Related_Document__c>();
            for(SQX_Related_Document__c rel : relatedDocs){
                referencedByRelated.put(rel.Referenced_Document__c, rel);
            }

            documents = getMatchingDocuments('', referencedByRelated.keySet(), MAX_RELATED_SIZE, 1, sortBy, sortOrder).documents;
            for(Document doc : documents){
                doc.related = referencedByRelated.get(doc.doc.Id);
            }
        }
        
        return documents;
    }
    
    /**
    * Returns the list of document matching where clause and belonging to the list of document ids
    * @param whereClause the condition that must be matched by the documents
    * @param docIds the list of document Ids which the search should be limited to. Pass null to keep the search unrestricted
    * @return returns list of controlled document that match the criteria
    */
    private static Result getMatchingDocuments(String whereClause, Set<Id> docIds){
        return getMatchingDocuments(whereClause, docIds, null, null, null, null);
    }

    /**
    * Returns the list of document matching where clause and belonging to the list of document ids
    * @param whereClause the condition that must be matched by the documents
    * @param docIds the list of document Ids which the search should be limited to. Pass null to keep the search unrestricted
    * @param pageSize number of records per page for the current binder
    * @param pageNumber pageNumber value after which to fetch the records
    * @return returns list of controlled document that match the criteria
    */
    private static Result getMatchingDocuments(String whereClause, Set<Id> docIds, Integer pageSize, Integer pageNumber, List<String> sortBy, String sortOrder){
        if(!SQX_Controlled_Document__c.SObjectType.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.Name.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.Title__c.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.Document_Status__c.getDescribe().isAccessible() || 
            !SQX_Controlled_Document__c.Expiration_Date__c.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.CreatedDate.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.Valid_Content_Reference__c.getDescribe().isAccessible() ||
            !SQX_Controlled_Document__c.OwnerId.getDescribe().isAccessible()){
            throw new AuraHandledException(Label.CQ_UI_Error_Insufficient_Access);
        }

        Result res = new Result();

        if(docIds != null){
            String docLimit = ' ID IN : docIds ';
            whereClause = String.isBlank(whereClause) ? docLimit : ( whereClause + ' AND ' + docLimit);
        }

        whereClause = String.isBlank(whereClause) ? '' : WHERE_KEYWORD + whereClause;

        // CRUD check
        String topicRelation = '';
        if(Schema.getGlobalDescribe().containsKey('TopicAssignment')){
            topicRelation = ', (SELECT EntityId,Topic.Name FROM TopicAssignments ORDER BY Topic.Name ASC) ';
        }

        Map<String, SObjectField> docFields = SQX_Controlled_Document__c.SObjectType.getDescribe().fields.getMap();
        String query = 'SELECT ' + SQX_DynamicQuery.getFieldList(docFields.values()) + topicRelation + ' FROM SQX_Controlled_Document__c ' + whereClause + ' ORDER BY ';

        String orderby = 'Title__c';
        if(sortBy != null && sortBy.size() > 0) {
            Integer index = 0;
            orderby = '';
            for(String sortField : sortBy) {
                if(docFields.containsKey(sortField)) {
                    orderby = docFields.get(sortField) + (index == 0 ? '' : ', ');
                    index++;
                }
            }
        }

        orderby = orderby + ('desc'.equalsIgnoreCase(sortOrder) ? ' DESC' : ' ASC');
        query = query + orderby;
        
        ApexPages.StandardSetController ct = new ApexPages.StandardSetController(
            Database.getQueryLocator(query)
        );

        pageNumber = pageNumber == null ? 1 : pageNumber;

        // If pagesize has not been defined, default page size of 20( STANDARD VALUE FOR StandardSetController ) is set
        if(pageSize != null){
            ct.setPageSize(pageSize);
        }

        ct.setPageNumber(pageNumber);

        res.hasMoreDocuments = ct.getHasNext();
        res.remainingDocuments = ct.getResultSize() - ct.getPageNumber() * ct.getPageSize();

        Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>((List<SQX_Controlled_Document__c>) ct.getRecords());

        List<Document> documents = new List<Document>();

        if(docs.size() > 0){
            // get thumbnails
            Set<Id> contentRefs = new Set<Id>();
            for(SQX_Controlled_Document__c doc : docs.values()){
                contentRefs.add(doc.Valid_Content_Reference__c);
            }

            Map<Id, ContentDocument> contents = new Map<Id, ContentDocument>(
                    [SELECT Id, FileExtension FROM ContentDocument WHERE Id IN : contentRefs]
                );

            // get related document count
            AggregateResult [] results = [SELECT Controlled_Document__c Document, Count(Id) RelatedCount
                                         FROM SQX_Related_Document__c
                                         WHERE Controlled_Document__c IN : docs.keySet()
                                         GROUP BY Controlled_Document__c];


            Map<Id, AggregateResult> resultMap = new Map<Id, AggregateResult>();
            for(AggregateResult result : results){

                resultMap.put((Id) result.get('Document'), result);
            }

            for(SQX_Controlled_Document__c doc : docs.values()){

                ContentDocument content = contents.get(doc.Valid_Content_Reference__c);
                String ext = content != null ? content.FileExtension : null;

                Document document = new Document(doc, ext);

                if(resultMap.containsKey(doc.Id)){
                    // document has related document
                    document.relatedCount = (Integer) resultMap.get(doc.Id).get('RelatedCount');
                }

                documents.add(document);
            }
        }

        res.documents = documents;
        return res;

    }

    /**
    * Finds a match for a given document using the text to match limited by the where clause and in document ids
    * @param textToMatch the text to search in controlled document
    * @param whereClause the condition to match for controlled documents
    * @param relationalWhereClause the condition to match for controlled documents with prefix
    * @param docIds the list of document Ids which should limit the search
    * @param pageSize the page size to return the results
    * @param pageNumber the page number for which to fetch the result
    * @param sortBy the fields to use to sort
    * @param sortOrder the sort order either ascending or descending
    * @return returns the list of matching documents
    */
    private static Result findMatchingDocuments(String textToMatch, String whereClause, String relationalWhereClause, Set<Id> docIds, Integer pageSize, Integer pageNumber, List<String> sortBy, String sortOrder){
        if(!SQX_Controlled_Document__c.SObjectType.getDescribe().isAccessible()){
            throw new AuraHandledException(Label.CQ_UI_Error_Insufficient_Access);
        }

        String query='';
        

        relationalWhereClause = String.isBlank(relationalWhereClause) ? '' : ', ContentVersion'
                + '(compliancequest__Controlled_Document__c '+ relationalWhereClause + ' AND compliancequest__Controlled_Document__c != null {DOCIDLIMIT})';

        String relationalLimit = '';
        if(docIds != null){
            String docLimit = ' ID IN : docIds ';
            whereClause = String.isBlank(whereClause) ? docLimit : ( whereClause + ' AND ' + docLimit);
            relationalLimit = ' AND compliancequest__Controlled_Document__c IN : docIds ';
        }

        relationalWhereClause = relationalWhereClause.replace('{DOCIDLIMIT}', relationalLimit);
        
        whereClause = String.isBlank(whereClause) ? '' : WHERE_KEYWORD + whereClause;
        
        query = 'FIND \'' + String.escapeSingleQuotes(textToMatch) + '\' IN ALL FIELDS'
                + ' RETURNING ' +  SQX.ControlledDoc + '(Id '+ whereClause +') '+relationalWhereClause;
        
        Search.SearchResults searchResults = Search.find(query);
        Set<Id> matchingIds = new Set<Id>();
        for(Search.SearchResult sr  : searchResults.get(SQX.ControlledDoc)){
            matchingIds.add(sr.getSObject().Id);
        }
        if(String.isNotBlank(relationalWhereClause)){
            for(Search.SearchResult sr  : searchResults.get('ContentVersion')){
                ContentVersion cv = (ContentVersion)sr.getSObject();
                matchingIds.add(cv.Controlled_Document__c);
            }
        }
        return matchingIds.size() > 0 ? getMatchingDocuments(null, matchingIds, pageSize, pageNumber, sortBy, sortOrder) : new Result();
    }

    /**
    * Returns a filter from a filter string if valid one is found. If filter string isn't valid
    * an excpetion is thrown
    * @param filterString the string to convert to filter
    */
    private static SQX_DynamicQuery.Filter getFilterFromString(String filterString){
        SQX_DynamicQuery.Filter filter = null;

        if(String.isNotBlank(filterString) && filterString.length()  > 2){
            
            try{
                filter = (SQX_DynamicQuery.Filter)JSON.deserialize(filterString, SQX_DynamicQuery.Filter.class);
            }
            catch(JSONException ex){
                String errorMessage = Label.CQ_UI_Error_Filter_Invalid.replace('{message}', ex.getMessage());
                throw new AuraHandledException(errorMessage);
            }
        
        }

        return filter;
    }


    /**
    * returns the Set of documents that are present in the folders matching the folder string
    * @param folderFilterString JSON string representing the filter for folder elements 
    * @param documentIds the set where all the documents matching the folder filter will be added
    */
    private static Boolean getDocumentsInFolder(String folderFilterString, Set<Id> documentIds){

        SQX_DynamicQuery.Filter folderFilter = getFilterFromString(folderFilterString);
        
        Boolean wasFiltered = folderFilter != null;

        if(wasFiltered){
                    
            List<SObject> folderIds = SQX_DynamicQuery.evaluate(DEFAULT_PAGE, PAGE_SIZE, 
                                                                ContentFolder.SObjectType.getDescribe(),
                                                                new List<SObjectField> {},
                                                                folderFilter,
                                                                new List<SObjectField> {},
                                                                false);
            
            if(folderIds.size() > 0){
                Map<Id, ContentFolderItem> folderItems = new Map<Id, ContentFolderItem>([SELECT Id FROM ContentFolderItem WHERE ParentContentFolderId IN : folderIds ]);
                if(folderItems.size() > 0){

                    for(ContentDocument document : [SELECT LatestPublishedVersion.Controlled_Document__c
                                                    FROM ContentDocument
                                                    WHERE Id IN : folderItems.keySet() 
                                                    AND LatestPublishedVersion.Controlled_Document__c != NULL]){
                                                        
                                                        documentIds.add(document.LatestPublishedVersion.Controlled_Document__c);
                                                    }
                }
            }
        }

        return wasFiltered;
    }

    /**
    * This method returns a list of controlled document that match the filter sent in topic filter string
    * @param topicFilterString JSON string representing the filter for added topics in Controlled Documents
    * @param documentIds the set where all the documents matching the topic filter will be added
    */    
    private static Boolean getDocumentsWithTopic(String topicFilterString, Set<Id> documentIds){
        SQX_DynamicQuery.Filter topicFilter = getFilterFromString(topicFilterString);
        Boolean wasFiltered = topicFilter != null;
        if(wasFiltered){
            SObjectType topicType = Schema.getGlobalDescribe().get('Topic');
            SObjectType topicAssignmentType=Schema.getGlobalDescribe().get('TopicAssignment');
            if(topicType != null && topicAssignmentType!=null){
                DescribeSObjectResult topicResult = topicType.getDescribe();
                DescribeSObjectResult topicAssignmentResult = topicAssignmentType.getDescribe();
                if(topicResult.isAccessible() && topicAssignmentResult.IsAccessible()){
                    Map<String, Schema.SObjectField> allFields=topicResult.fields.getMap();
                    try{
                        if(!allFields.containsKey(topicFilter.field)){
                            String errorMessage = Label.CQ_UI_Error_Field_Not_Present.replace('{field}', topicFilter.field);
                            throw new AuraHandledException(errorMessage);
                        }
                    }catch(Exception e){
                    }

                    Set<String> topicNames = new Set<String>();
                    getAllTopics(topicNames, topicFilter);
                    List<String> allTopics = new List<String>(topicNames);

                    if (allTopics.size() > 0) {

                        documentIds.addAll(getAllDocsWithTopics(allTopics));
                    }
                }
            }
        }
        return wasFiltered;
    }

    /**
     * Returns all objects of controlled document type with given topics
     * @param topics the list of topics that need to match in a controlled document
     * @return returns the list of all documents with a given topic
     */
    public static Set<Id> getAllDocsWithTopics(List<String> topics) {
        Set<Id> documentIds = new Set<Id>();
        String docPrefix = SQX_Controlled_Document__c.SObjectType.getDescribe().getKeyPrefix();
        for(AggregateResult res : [SELECT EntityId
                                            FROM TopicAssignment
                                            WHERE Topic.Name IN : topics AND EntityKeyPrefix = :docPrefix
                                            GROUP BY EntityId
                                            HAVING Count(Topic.Name) = :topics.size() ]){
            Id entityId = (Id) res.get('EntityId');
            documentIds.add(entityId);
        }

        return documentIds;
    }

    /**
    * Converts all topic filters into a list of topic name
    * @param topics the list of topics that have been identified recursively
    * @param filter the filter that is to be checked for topic name
    */
    private static void getAllTopics(Set<String> topics, SQX_DynamicQuery.Filter filter) {
        if(filter.filters == null) {
            System.assert(filter.operator == SQX_DynamicQuery.FILTER_OPERATOR_EQUALS , 'Only equals supported in topic');
            topics.add((String)filter.value);
        }
        else {
            for(SQX_DynamicQuery.Filter f : filter.filters){
                getAllTopics(topics, f);
            }
        }
    }


    /**
    * This method returns a list of controlled document that match the filter sent in tag filter string
    * @param tagFilterString JSON string representing the filter for tagged Controlled Documents
    * @param documentIds the set where all the documents matching the tag filter will be added
    */
    private static Boolean getDocumentsWithTag(String tagFilterString, Set<Id> documentIds){
        SQX_DynamicQuery.Filter tagFilter = getFilterFromString(tagFilterString);
        Boolean wasFiltered = tagFilter != null;

        if(wasFiltered){
            SObjectType tagType = Schema.getGlobalDescribe().get(SQX.NSPrefix + 'SQX_Controlled_Document__tag');
            if(tagType != null){
                DescribeSObjectResult tagResult = tagType.getDescribe();
                SObjectField field = tagResult.fields.getMap().get('ItemId');
                for(SObject item : SQX_DynamicQuery.evaluate(DEFAULT_PAGE, PAGE_SIZE, 
                                             tagResult,
                                             new List<SObjectField> { field },
                                             tagFilter,
                                             new List<SObjectField> {},
                                             false)){
                    documentIds.add((Id)item.get('ItemId'));
                }
            }
            

        }

        return wasFiltered;
    }

    /**
    * Returns the condition that can be used where clause in a SOQL statement
    * @param filterString the JSON filterString that needs to be converted into SOQL compliant condition string
    */
    private static String getConditionStringForFilter(String filterString){
        
        String whereClause = '';
        
        SQX_DynamicQuery.Filter filter = getFilterFromString(filterString);
        if(filter != null){
            whereClause = filter.getWhereClause(SQX_Controlled_Document__c.SObjectType.getDescribe().fields.getMap());
            whereClause = whereClause.subStringAfter(WHERE_KEYWORD); // remove where keyword returned by filter to reduce complexity of query composition later on
        }

        return whereClause;
    }

    /**
    * Returns the condition that can be used where clause in a SOQL statement
    * @param filterString the JSON filterString that needs to be converted into SOQL compliant condition string
    * @param fieldNamePrefix prefix to be appended before field name
    */
    private static String getConditionStringForFilter(String filterString, String fieldNamePrefix){
        
        String whereClause = '';
        
        SQX_DynamicQuery.Filter filter = getFilterFromString(filterString);
        if(filter != null){
            whereClause = filter.getWhereClause(SQX_Controlled_Document__c.SObjectType.getDescribe().fields.getMap(), fieldNamePrefix);
        }

        return whereClause;
    }

    /**
    * This method is used to set default folder 
    * @param binderId is a default selected binder id
    * @param defaultFolder is a default selected path
    */
    @AuraEnabled
    public static void setDefaultFolder(String binderId,String defaultFolder){
        String currentUserId = UserInfo.getUserId();
        List<SQX_User_Default_Option__c> options = [SELECT BinderId__c, DefaultFolder__c FROM SQX_User_Default_Option__c WHERE SetupOwnerId =: currentUserId];
        SQX_User_Default_Option__c userOption = new SQX_User_Default_Option__c();
        userOption.BinderId__c = binderId;
        userOption.DefaultFolder__c = defaultFolder;
        userOption.SetupOwnerId = currentUserId;
        
        if(options.size() == 1) {
            userOption.Id = options.get(0).Id;
        }
        
        upsert userOption;
    }

    /**
    * This method get default folder information
    */
    @AuraEnabled
    public static SQX_User_Default_Option__c getDefaultFolderInfo(){
        String currentUserId=UserInfo.getUserId();
        List<SQX_User_Default_Option__c> preferences = [SELECT BinderId__c, DefaultFolder__c FROM SQX_User_Default_Option__c WHERE SetupOwnerId =: currentUserId];
        return preferences.size() > 0 ? preferences.get(0) : null;
    }

    /**
     * This method returns the visualforce url from the custom settings.
     */
    @AuraEnabled
    public static String getCQVFUrl() {
        SQX_Custom_Settings_Public__c setting = SQX_Custom_Settings_Public__c.getInstance();

        if(setting == null || String.isBlank(setting.CQ_VisualForce_Base_URL__c)) {
            throw new AuraHandledException(Label.CQ_UI_Error_Base_Url_Not_Configured);
        }
        return setting.CQ_VisualForce_Base_URL__c;
    }
    
    /**
    * This method is used to get field label 
    */
    @AuraEnabled
    public static Map<String, String> getFieldLabels(){
        Map<String, SObjectField> fieldMap = SQX_Controlled_Document__c.sObjectType.getDescribe().fields.getMap();
        Map<String, String>allLabels = new Map<String, String>();
        for (String fieldName: fieldMap.keySet()) {
            if((fieldMap.get(fieldName).getDescribe().getRelationshipName()!= null) &&(!(fieldMap.get(fieldName).getDescribe().getRelationshipName()).endsWith('__r'))){
                allLabels.put(fieldName,(fieldMap.get(fieldName).getDescribe().getRelationshipName()));//Provides the relationship name of lookup fields not ending in __r
            }
            else{
                allLabels.put(fieldName,(fieldMap.get(fieldName).getDescribe().getLabel()));//Provides field label of all the fields.
            }
        }
        return allLabels;
    } 
    
    /**
    * This class is used to deserialize object out to lightning UI.
    */
    public class Document {

        /**
        * Controlled Document object related to the doc
        */
        @AuraEnabled
        public SQX_Controlled_Document__c doc {get; set;}

        /**
        * The related document record that for the document
        */
        @AuraEnabled
        public SQX_Related_Document__c related {get; set;}
        
        /**
        * Count of related documents for the controlled doc
        */
        @AuraEnabled
        public Integer relatedCount {get; set;}

        /**
        * Extension of the related content 
        */
        @AuraEnabled
        public String extension {get; set;}

        /**
        * This is icon field for the document. This is used by the ui and present here
        * for serialization purpose only
        */
        @AuraEnabled
        public String icon {get; set;}

        /**
        * Initialize the document
        * @param doc related controlled doc
        * @param extension file extension of the document
        */
        public Document(SQX_Controlled_Document__c doc, String extension){
            this.doc = doc;
            this.relatedCount = 0;
            this.icon = '';
            this.extension = extension;
        }
    }

    /*
        Wrapper class that holds the result that is sent to the folder each time it requests for records
    */
    public class Result{

        @AuraEnabled
        public List<Document> documents;

        @AuraEnabled
        public Boolean hasMoreDocuments;

        @AuraEnabled
        public Integer remainingDocuments;
    }
}