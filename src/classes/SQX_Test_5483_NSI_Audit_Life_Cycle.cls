/**
 * This Test class ensures that the audit life cycle functionality through nsi works as expected.
 */
@isTest
public class SQX_Test_5483_NSI_Audit_Life_Cycle {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        
    }
    
    /**
     * Given : An initiated NSI with one on-boarding step with Perform Audit as record type. 
     * When : Audit with void status is linked
     * Then : Error message, 'Audit with void status cannot be used.' is thrown.
     */
    @isTest
    public static void givenNSI_WhenVoidAuditIsLinked_ErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User assigneeUser = allUsers.get('assigneeUser');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        SQX_Test_NSI nsi;
        SQX_Onboarding_Step__c policy;
        SQX_New_Supplier_Introduction__c currentNSI;
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Perform Audit', assigneeUser, 1);
        }
        
        System.runAs(standardUser){
            
            // Arrange : NSI is created
            nsi = new SQX_Test_NSI().save();
            
            // submit nsi
            nsi.submit();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // initiate nsi
            nsi.initiate();
            
            policy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            
            // Assert : ensure sf task is created
            Task sfTask = [SELECT Id, Status FROM Task WHERE Child_What_Id__c = : policy.Id];
            System.assert(sfTask.Id != null, 'Expected sf task should be created on initiating nsi');
            System.assert(sfTask.Status == SQX_Task.STATUS_NOT_STARTED, 'Expected Status of sf task to be ' + SQX_Task.STATUS_NOT_STARTED + ' but got ' + sfTask.Status);
            
            // Create voided audit
            SQX_Test_Audit audit = new SQX_Test_Audit();
            audit.audit.Audit_Result__c = 'No Go';
            audit.save();
            audit.setStatus(SQX_Audit.STATUS_VOID);
            audit.save();
            
            // Act : Link void audit to onboarding step
            policy.SQX_Audit_Number__c = audit.audit.Id;
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_update(new List<SQX_Onboarding_Step__c>{policy},  new List<Schema.SObjectField>{
                    SQX_Onboarding_Step__c.fields.SQX_Audit_Number__c   
                });
            
            //Assert: Make sure void audit cannot be used in perform audit task.
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), 'Audit with void status cannot be used.'),result[0].getErrors());
             
            // Assert : sf task should be not be complete when void audit is used
            sfTask = [SELECT Id, Status, Description FROM Task WHERE Child_What_Id__c = : policy.Id];
            System.assert(sfTask.Status != SQX_Task.STATUS_COMPLETED, 'Expected task not to be completed');
        }
    }

    
    /*
     * Given:Perform audit from onboarding step sf task  and save the record
     * when : user edit the audit record and change the reference nsi than 
     * Then: User should get error message 
     */
    @isTest
    public static void givenOnboardingPerformAndSaveAudit_WhenUserEditAuditRecordAndChangeRefNsiAndTryToUpdate_ThenGiveErrorMessage() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Perform Audit', adminUser, 1);
            // Arrange : NSI is created
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            SQX_Test_NSI nsi1 = new SQX_Test_NSI().save();
            // submit nsi
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();

            // initiate nsi
            nsi.setStage(SQX_NSI.STAGE_IN_PROGRESS);
            nsi.setStatus(SQX_NSI.STATUS_OPEN);
            nsi.nsi.Current_Step__c = 1;
            nsi.save();

            SQX_Onboarding_Step__c policy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            // Create audit
            SQX_Test_Audit audit = new SQX_Test_Audit();
            audit.audit.SQX_Supplier_Introduction__c=nsi.nsi.id;
            audit.save();
            //Act :change reference nsi record 
            audit.audit.SQX_Supplier_Introduction__c=nsi1.nsi.id;
            
            List<Database.SaveResult> changeNsiResult = new SQX_DB().continueOnError().op_update(new List<SQX_Audit__c> {audit.audit}, 
                                                                                                 new List<Schema.SObjectField>{
                                                                                                    SQX_Audit__c.SQX_Supplier_Introduction__c}
                                                                                                );
            //Assert:validation error message is displayed when reference nsi is changed 
            System.assertEquals('Reference NSI cannot be changed once it is linked with audit.',changeNsiResult[0].getErrors().get(0).getMessage()); 
            //Arrange: update desc and set nsi record
            audit.audit.Scope_Description__c='nsi perform audit record';
            audit.audit.SQX_Supplier_Introduction__c=nsi.nsi.id;
            //Act: update desc and set reference nsi record
            List<Database.SaveResult> changeDescResult = new SQX_DB().continueOnError().op_update(new List<SQX_Audit__c> {audit.audit}, 
                                                                                                  new List<Schema.SObjectField>{
                                                                                                      SQX_Audit__c.Scope_Description__c
                                                                                                    });
            //Assert:update desc and record should be saved 
            System.assertEquals(true, changeDescResult[0].isSuccess(),'Record update failed >>' + changeDescResult[0].getErrors());
        }
    }

    /*
     * Given: create an audit with nsi and without nsi
     * when: user edit the audit record and change the audit type and didn't select account 
     * Then: User should get an error message
     *       a)when the audit is related to nsi than only in customer audit type should give an error message when the account is not selected
     *       b)Audit without nsi with audit type customer and supplier that didn't select account than error message should be displayed
     */
    @isTest
    public static void givenCreateAuditWithOrWithOutNsi_UserEditAuditAndChangeTheAuditTypeAndNotSelectAccount_ThenShouldgetErrorMessageInDifferentScenario(){
        /**
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');

        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(1, 'Perform Audit', adminUser, 1);
            // Arrange : NSI is created
            SQX_Test_NSI nsi = new SQX_Test_NSI().save();
            SQX_Test_NSI nsi1 = new SQX_Test_NSI().save();
            // submit nsi
            nsi.setStage(SQX_NSI.STAGE_TRIAGE);
            nsi.save();

            // initiate nsi
            nsi.setStage(SQX_NSI.STAGE_IN_PROGRESS);
            nsi.setStatus(SQX_NSI.STATUS_OPEN);
            nsi.nsi.Current_Step__c = 1;
            nsi.save();

            SQX_Onboarding_Step__c policy = [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsi.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN];
            // Create audit
            SQX_Test_Audit audit = new SQX_Test_Audit();
            audit.audit.SQX_Onboarding_Step__c = policy.Id;
            audit.audit.SQX_Supplier_Introduction__c=nsi.nsi.id;
            audit.save();
            //Act :Edit audit change audit type and didn't select audit
            audit.audit.Audit_Type__c='Customer';
            audit.audit.Account__c=null;
            
            List<Database.SaveResult> customerResult =  new SQX_DB().continueOnError().op_update(
                                                        new List<SQX_Audit__c> {audit.audit}, 
                                                        new List<Schema.SObjectField>{
                                                                SQX_Audit__c.account__c,
                                                                SQX_Audit__c.Audit_Type__c
                                                        });
            //Assert:Get error message when audit type is customer and with nsi didn't select account
            System.assertEquals('For Internal Audit Type, Division is required and for other Audit Types Account is required.',customerResult[0].getErrors().get(0).getMessage());
            //Edit audit type to supplier 
            audit.audit.Audit_Type__c='Supplier';
            audit.audit.Account__c=null;

            List<Database.SaveResult> supplierResult =  new SQX_DB().continueOnError().op_update(
                                                        new List<SQX_Audit__c> {audit.audit}, 
                                                        new List<Schema.SObjectField>{
                                                                SQX_Audit__c.account__c,
                                                                SQX_Audit__c.Audit_Type__c
                                                        });
            //verify  audit with supplier audit type and nsi record 
            System.assertEquals(true,supplierResult[0].isSuccess(),'supplier account with nsi  record is saved ' );
            //change audit type to internal 
            audit.audit.Audit_Type__c='Internal';
            audit.audit.Account__c=null;

            List<Database.SaveResult> internalResult =  new SQX_DB().continueOnError().op_update(
                                                        new List<SQX_Audit__c> {audit.audit}, 
                                                        new List<Schema.SObjectField>{
                                                                SQX_Audit__c.account__c,
                                                                SQX_Audit__c.Audit_Type__c
                                                        });
             System.assertEquals(true,internalResult[0].isSuccess(),'Internal  account with nsi  record is saved ' );

            //create a audit without nsi 
            SQX_Test_Audit auditWithoutAccount = new SQX_Test_Audit().save();
            //change audit type and didn't select account
            auditWithoutAccount.audit.Audit_Type__c='Customer';
            auditWithoutAccount.audit.Account__c=null;

            List<Database.SaveResult> withoutnsicustomerResult = new SQX_DB().continueOnError().op_update(
                                                                 new List<SQX_Audit__c> {auditWithoutAccount.audit}, 
                                                                 new List<Schema.SObjectField>{
                                                                        SQX_Audit__c.account__c,
                                                                        SQX_Audit__c.Audit_Type__c
                                                                });
            //Assert:verify account is selected or not in particular  audit type
            System.assertEquals('For Internal Audit Type, Division is required and for other Audit Types Account is required.',withoutnsicustomerResult[0].getErrors().get(0).getMessage());

            auditWithoutAccount.audit.Audit_Type__c='Supplier';
            auditWithoutAccount.audit.Account__c=null;

            List<Database.SaveResult> nsiWithoutSupplierResult = new SQX_DB().continueOnError().op_update(
                                                                 new List<SQX_Audit__c> {auditWithoutAccount.audit}, 
                                                                 new List<Schema.SObjectField>{
                                                                        SQX_Audit__c.account__c,
                                                                        SQX_Audit__c.Audit_Type__c
                                                                });
            System.assertEquals('For Internal Audit Type, Division is required and for other Audit Types Account is required.',nsiWithoutSupplierResult[0].getErrors().get(0).getMessage());
            //edit audit type 
            auditWithoutAccount.audit.Audit_Type__c='Internal';
            auditWithoutAccount.audit.Account__c=null;
            //Act :try to save the record
            List<Database.SaveResult> nsiWithOutInternalResult = new SQX_DB().continueOnError().op_update(
                                                                 new List<SQX_Audit__c> {auditWithoutAccount.audit}, 
                                                                 new List<Schema.SObjectField>{
                                                                        SQX_Audit__c.account__c,
                                                                        SQX_Audit__c.Audit_Type__c
                                                                });
           //Assert: verify the  audit type and with out selection of account
            System.assertEquals(true,nsiWithOutInternalResult[0].isSuccess(),'Internal  account with out  nsi  record is saved ' );
        }
         */
    }
}