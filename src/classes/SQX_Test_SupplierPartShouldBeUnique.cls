/*
 * unit test for supplier part uniqueness constraint 
 * 
*/
@istest
public class SQX_Test_SupplierPartShouldBeUnique{
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
    }
    static testmethod void testtrigger3()
    {
        Account Acc = SQX_Test_Account_Factory.createAccount();
        
        SQX_Part_Family__c newPF1 = new SQX_Part_Family__c();
        newPF1.Name = 'dummyFamily';
        insert newPF1;
        
        SQX_Part__c myPart1 = new SQX_Part__c();
        mypart1.Name = 'newPart1';
        mypart1.Part_Number__c = 'prt111';
        mypart1.Part_Risk_Level__c = 3;
        mypart1.Active__c = TRUE;
        mypart1.Part_Family__c = newPF1.id;
        Insert myPart1;     
        
        SQX_Supplier_Part__c SP1 = new SQX_Supplier_Part__c();
        SP1.Account__c = Acc.id;
        SP1.Part__c = mypart1.id;
        SP1.Active__c = true;
        SP1.Approved__c = true;
        Insert SP1;
        
        /*
        try{
            SP1.Id = null;
            Insert SP1;
            System.assert(false, 'Expected the insertion to fail, but succeeded');
        }
        catch(Exception ex){
            //ignored because it should fail
        }
        */
        
        
        SP1.Id = null;
        Database.SaveResult result = Database.insert(sp1,false);
        System.assert(result.isSuccess() == false, 'Expected the insertion to fail, but succeeded');

    }

    /**
     * Given: Account record with part family and part 
     * When: User try to add them 
     *                 1)supplier part with part name and part empty
     *                 2)Supplier part with the part and part name
     * Then: User gets an error message  'Either Part or Part Name should be added.'
     * */
    @isTest
    public static void givenPartNameorParts_WhenEitherBothareEmptyOrBothAreEnter_ThenDisplayErrorMessage(){
       
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
       
        System.runAs(adminUser) {
        //Arrange : Add account    
        Account Acc = SQX_Test_Account_Factory.createAccount();
        // Add part family 
        SQX_Part_Family__c newPF1 = new SQX_Part_Family__c();
        newPF1.Name = 'dummyFamily';
        new SQX_DB().op_insert(new List<SQX_Part_Family__c>{ newPF1 }, new List<SObjectField>{
                                                                                        SQX_Part_Family__c.Name
                                                                                            });
        //Add  parts
        SQX_Part__c myPart1 = new SQX_Part__c();
        mypart1.Name = 'newPart1';
        mypart1.Part_Number__c = 'prt111';
        mypart1.Part_Risk_Level__c = 3;
        mypart1.Active__c = TRUE;
        mypart1.Part_Family__c = newPF1.id;
        new SQX_DB().op_insert(new List<SQX_Part__c>{ myPart1 }, 
                                                    new List<SObjectField>{
                                                                        SQX_Part__c.Name,
                                                                        SQX_Part__c.Part_Number__c,
                                                                        SQX_Part__c.Part_Risk_Level__c,
                                                                        SQX_Part__c.Active__c,
                                                                        SQX_Part__c.Part_Family__c
                                                                    });
        
        List<SQX_Supplier_Part__c> suppliers=new SQX_Supplier_Part__c[]{
                                             new SQX_Supplier_Part__c(
                                                                        Account__c=Acc.id,
                                                                        Active__c=true,
                                                                        Approved__c=true
                                                                    ),
                                             new SQX_Supplier_Part__c(
                                                                        Account__c= Acc.id,
                                                                        Part__c = mypart1.id,
                                                                        Part_Text__c='test part name', 
                                                                        Active__c = true,
                                                                        Approved__c = true
                                                                    ),
                                            new SQX_Supplier_Part__c(
                                                                        Account__c= Acc.id,
                                                                        Part__c = mypart1.id,
                                                                        Active__c = true,
                                                                        Approved__c = true
                                                                    ),
                                            new SQX_Supplier_Part__c(
                                                                        Account__c= Acc.id,
                                                                        Part__c = mypart1.id,
                                                                        Active__c = true,
                                                                        Approved__c = true
                                                                    )};

            //Act :Try To insert the supplier part with part and part name
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(suppliers, new List<Schema.SObjectField>{});
            //Assert: Verify the part name and part  Validation Rule Error message in a different scenario
            System.assertEquals('Either Part or Part Name should be added.',result[0].getErrors()[0].getMessage(), 'Either Part or Part Name should be added.');
            System.assertEquals('Either Part or Part Name should be added.',result[1].getErrors()[0].getMessage(), 'Either Part or Part Name should be added.');
            System.assertEquals(true, result[2].isSuccess(),'Record save Successfully');
            System.assert(result[3].getErrors()[0].getMessage().contains('duplicate value found'), result[3].getErrors()[0].getMessage());
        }
    }

     /**
     * Given: supplier part record with Active and Approved 
     * When: User try to set 
     *                 1) approved is set false
                       2) approved set to false and not added part or part Name 
     *                 3)Both active and approved is set true and added part name 
     * Then: User gets an error message  'Both Active and Approved should be checked to Activated Supplier Part.'
     * */
    @isTest
    public static void givenSupplierPartActiveAndApproved_WhenUserSaveRecordWithoutApproved_ThenDisplayErrorMessage(){
       
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
       
        System.runAs(adminUser) {
            //Arrange : Add account    
            Account Acc = SQX_Test_Account_Factory.createAccount();
            // Add part family 
            SQX_Part_Family__c newPF1 = new SQX_Part_Family__c();
            newPF1.Name = 'dummyFamily';
            new SQX_DB().op_insert( new List<SQX_Part_Family__c>{ newPF1 }, 
                                    new List<SObjectField>{
                                        SQX_Part_Family__c.Name
                                    });
            //Add  parts
            SQX_Part__c myPart1 = new SQX_Part__c();
            mypart1.Name = 'newPart1';
            mypart1.Part_Number__c = 'prt111';
            mypart1.Part_Risk_Level__c = 3;
            mypart1.Active__c = TRUE;
            mypart1.Part_Family__c = newPF1.id;
            new SQX_DB().op_insert(new List<SQX_Part__c>{ myPart1 }, 
                                new List<SObjectField>{
                                    SQX_Part__c.Name,
                                    SQX_Part__c.Part_Number__c,
                                    SQX_Part__c.Part_Risk_Level__c,
                                    SQX_Part__c.Active__c,
                                    SQX_Part__c.Part_Family__c
                                    });
            List<SQX_Supplier_Part__c> suppliers=new SQX_Supplier_Part__c[]{
                                                new SQX_Supplier_Part__c(
                                                                            Account__c=Acc.id,
                                                                            Part_Text__c='test part name', 
                                                                            Active__c=true,
                                                                            Approved__c=false
                                                                        ),
                                                new SQX_Supplier_Part__c(
                                                                            Account__c= Acc.id,
                                                                            Active__c = true,
                                                                            Approved__c = false
                                                                        ),
                                                new SQX_Supplier_Part__c(
                                                                            Account__c= Acc.id,
                                                                            Active__c = true,
                                                                            Approved__c = true,
                                                                            Part_Text__c='test part name'
                                                                        )};
                                                                        
            //Act :Try To inser supplier part with active and approved checked
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(suppliers, new List<Schema.SObjectField>{});          
            //Assert: Verify the active and approved for Validation Rule Error message in a different scenario
            System.assertEquals('A supplier part can not be activated without being approved.',result[0].getErrors()[0].getMessage(), 'Both Active and Approved should be checked to Activated Supplier Part.');
            System.assertEquals('Either Part or Part Name should be added.',result[1].getErrors()[0].getMessage(), 'Either Part or Part Name should be added.');
            System.assertEquals('A supplier part can not be activated without being approved.',result[1].getErrors()[1].getMessage(), 'Both Active and Approved should be checked to Activated Supplier Part.');
            System.assertEquals(true, result[2].isSuccess(),'Record save Successfully after  both  active and approved checked ');
        }
    }
}