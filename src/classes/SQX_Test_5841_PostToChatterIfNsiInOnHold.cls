/*
 * Unit test to check if FeedItem is posted on chatter or not.
 */
@isTest
public class SQX_Test_5841_PostToChatterIfNsiInOnHold {
   final static String ONBOARDING_STEP_ON_HOLD = 'On Hold';

   final static String chatterMsg = 'Supplier Introduction {0} is OnHold due to the following comment and needs your attention.';

    @testSetup
     public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
         
        SQX_Test_NSI.addUserToQueue(new List<User> { standardUser, assigneeUser, adminUser});
        
        SQX_Test_NSI nsiRecord;
        SQX_Part__c part;
        SQX_Standard_Service__c stdServiceName;
        SQX_Custom_Settings_Public__c customSetting;
         
        System.runAs(adminUser) {
            //Arrange: Create tasks, part and standard service
            SQX_Test_NSI.createPolicyTasks(2, 'Task', assigneeUser, 1);
            SQX_Test_NSI.createPolicyTasks(1, 'Task', assigneeUser, 2); 
            part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');
            stdServiceName = new SQX_Standard_Service__c(name = 'StandardService');
            insert stdServiceName;
            
            customSetting = new SQX_Custom_Settings_Public__c( Community_URL__c = 'https://login.salesforce.com',
                                                                Org_Base_URL__c = SQX_Utilities.getBaseUrlForUser()+'/',
                                                                NC_Analysis_Report__c = 'abc123def');
            insert customSetting;
        }
        
        System.runAs(standardUser) {
            //Act: Nsi Record initiated and ownership taken
            nsiRecord = new SQX_Test_NSI().save();
            nsiRecord.setStage(SQX_NSI.STAGE_TRIAGE);
            nsiRecord.nsi.SQX_Part__c = part.id;
            nsiRecord.nsi.SQX_Standard_Service__c = stdServiceName.id;

            nsiRecord.save();
            nsiRecord.initiate();
            
            //Assert: Make sure that NSI record is in 'Inprogress' Stage
            System.assertEquals(SQX_NSI.STAGE_IN_PROGRESS, [Select Record_Stage__c from sqx_new_supplier_introduction__c where id = :nsiRecord.nsi.id].Record_Stage__c);
            System.assertEquals(true, part.id != null, 'Expected, part to be inserted');
            System.assertEquals(true, stdServiceName.id != null, 'Expected, standard service to be inserted');
            System.assertEquals(2, [SELECT Id, Step__c FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsiRecord.nsi.Id AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_OPEN].size());
        }
                  
        System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord.nsi.Id];
            Task sfTask = sfTasks.get(0);
            
            //Act: any one of the task is updated where status is 'completed' and result is 'No Go' and notification is sent to chatter
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
        }
        
     }
    
    /*
     * Given: A NSI Record
     * When: Any NSI is in onHold by setting onboarding step to complete-nogo
     * Then: A chatter notification is added in chatter feeds
     */ 
    @isTest
    public static void  givenNSIRecord_WhenNSIIsinOnHold_ThenPostMsgOnChatter(){
        //Arrange: Get nsiRecord, under which chatter msg has been posted
        SQX_New_Supplier_Introduction__c nsiRecord = [Select id, name from SQX_New_Supplier_Introduction__c limit 1];
        
        //Act: Get the feed item 
        FeedItem feed = [SELECT parentid, body FROM FeedItem WHERE parentId = :nsiRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST limit 1];
            
        //Assert: Make sure nsi record status is in onhold and msg is posted on chatter
        System.assertEquals(ONBOARDING_STEP_ON_HOLD, [SELECT Record_Stage__c FROM sqx_new_supplier_introduction__c WHERE id = :nsiRecord.id].Record_Stage__c);
        System.assertEquals(true, feed != null, 'Expected, a feedMessage to be present in chatter');
        System.assertEquals(true, feed.body.contains(String.format(chatterMsg, new String[]{nsiRecord.name})));            
    }
    
    /*
     * Given: A NSI Record already in onHold
     * When:  Any onboarding step  in completed with result nogo
     * Then:  Chatter notification is added in chatter feeds
     */ 
    @isTest
    public static void  givenNSIRecord_WhenNSIAlreadyInOnHoldAndOnboardingStepIsCompleteNoGo_ThenMsgIsPostedOnChatter(){
        //Get users
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        SQX_New_Supplier_Introduction__c nsiRecord = [Select id, name from SQX_New_Supplier_Introduction__c limit 1];

        System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord.Id];
            
            //Act: Task2 of step1 status is set to complete and result to no-go
            Task sfTask2 = sfTasks.get(1);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask2.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            //Assert: Make sure that 2 onboarding policy are  completed and in no-go
            System.assertEquals(2, [Select id from SQX_Onboarding_Step__c where status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE and result__c = :SQX_Steps_Trigger_Handler.RESULT_NO_GO].size());			
            System.assertEquals(ONBOARDING_STEP_ON_HOLD, [SELECT Record_Stage__c FROM sqx_new_supplier_introduction__c WHERE id = :nsiRecord.id].Record_Stage__c);
        }
        //Assert: Make sure only 2 message are posted on chatter.
          System.assertEquals(2, [SELECT parentid, body FROM FeedItem WHERE parentId = :nsiRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());

    }
    /*
     * Given: A NSI Record
     * When:  NSI is in onHold multiple times for different onboarding step
     * Then:  chatter notification is sent for each different step.
     */
    @isTest
    public static void  givenNSIRecord_WhenNSIInOnHoldAndOnboardingStepIsCompleteNoGoForDifferentStep_ThenMultipleMsgIsPostedOnChatter(){
        //Get users
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        SQX_New_Supplier_Introduction__c nsiRecord = [Select id, name, Record_Stage__c from SQX_New_Supplier_Introduction__c limit 1];
        
        System.runAs(standardUser){
            SQX_Onboarding_Step__c onBoardingStepRecord =  [SELECT Id FROM SQX_Onboarding_Step__c WHERE SQX_Parent__c =: nsiRecord.Id 
                                               			AND Status__c =: SQX_Steps_Trigger_Handler.STATUS_COMPLETE 
                                               			AND Result__c =: SQX_Steps_Trigger_Handler.RESULT_NO_GO];
            //Act: Redo the task
            SQX_Test_NSI os = new SQX_Test_NSI();
            os.redoOnboardingStep(onBoardingStepRecord);

            SQX_BulkifiedBase.clearAllProcessedEntities();
        }

        System.runAs(assigneeUser) {
            //Act: Complete-Go remaining SF tasks
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord.Id  AND Status != :SQX_Task.STATUS_COMPLETED];
            Task sfTask1 = sfTasks.get(0);
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask1.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();

            Task sfTask2 = sfTasks.get(1);
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask2.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
        }
        //Assert: Make sure than only one chatter feed has been posted till now.
        System.assertEquals(1, [SELECT parentid, body FROM FeedItem WHERE parentId = :nsiRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());
        
        Test.startTest();
        System.runAs(assigneeUser) {
            //Arrange: Get step2 tasks
            List<Task> step2Tasks = [SELECT Id FROM Task WHERE WhatId =: nsiRecord.Id and Status != :SQX_Task.STATUS_COMPLETED];
            System.assertEquals(1, step2Tasks.size(), 'Expected, only one remaining task');
            
            //Act: Update the nsi stage to inprogress
            Task step2Task = step2Tasks.get(0);
            String description = 'Completing task with result as No Go';
            SQX_Test_Utilities.completeSupplierStep(step2Task.Id, SQX_Steps_Trigger_Handler.RESULT_NO_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            //Assert: NSI stage should now be in onHold.
            System.assertEquals(ONBOARDING_STEP_ON_HOLD, [Select Record_Stage__c from sqx_new_supplier_introduction__c where id = :nsiRecord.id].Record_Stage__c);

        }
         //Assert: Make sure 2 chatter feeds have been posted in FeedItem.
          System.assertEquals(2, [SELECT parentid, body FROM FeedItem WHERE parentId = :nsiRecord.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST].size());
    }
        
}