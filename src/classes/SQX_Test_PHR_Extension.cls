/**
 * Test class for PHR Extension class
*/

@isTest
public class SQX_Test_PHR_Extension {

    @testsetup
    static void initSetup() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();

        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'standardUser');
    }


    /**
     * GIVEN : PHR Extension class
     * WHEN : Create method is Invoked
     * THEN : New PHR is created
    */
    static testmethod void givenPHRExtension_WhenCreateMethodIsInvoked_ThenANewPHRIsCreated() {
        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {

            // ARRANGE : Create Complaint record and pass the complaint id to new PHR extension
            Id newComplaintId = new SQX_Test_Complaint(stdUser).save().complaint.Id;

            SQX_Product_History_Review__c phr = new SQX_Product_History_Review__c();
            phr.SQX_Complaint__c = newComplaintId;


            // ACT : Invoke create method
            Test.startTest();
            PageReference pageRef = Page.SQX_PHR_Create;
            test.setCurrentPageReference(pageRef);
            pageRef.getParameters().put('complaintId',newComplaintId);
            
            SQX_Extension_PHR ext = new SQX_Extension_PHR(new ApexPages.StandardController(phr));
            PageReference pr = ext.create();

            // ASSERT : PHR is created and associated with complaint
            phr = [SELECT Id FROM SQX_Product_History_Review__c WHERE SQX_Complaint__c =: newComplaintId];
            System.assert(pr.getUrl().contains(phr.Id), 'Expected return url to contain the newly created phr id but got ' + pr.getUrl());

            Test.stopTest();
        }

    }

}