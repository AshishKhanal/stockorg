/*
 * description: test for audit response
 */
@IsTest
public class SQX_Test_1353_AttachMultipleFinding{

    private static Boolean runAllTests = true,
                            run_auditResponseSubmitted_ResponseProcessed=false,
                            run_auditResponseSubmitted_SentForApproval=false,
                            run_auditResponse_ApprovalRequired_Approved=false,
                            run_auditResponse_ApprovalRequired_Rejected=false;                          
    
    /**
    * helper funtion to setup Audit, finding and finding response
    * @param investigationApproval set whether investigation approval is required or not
    **/                      
    public static SQX_Test_Audit arrangeForAuditResponse(User adminUser, boolean investigationApproval, user responseSubmitter){
            /*
            * Arrange : Create Audit with investigation apprval required
            *           Add a finding with investigation and containment required
            *           Add another finding with containment required
            *           Create audit response with investigation and containment for first finding and containment for second finding
            */
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c= responseSubmitter.Id;
            audit.setStatus(SQX_Audit.STATUS_COMPLETE);
            audit.setPolicy(investigationApproval, false, false);//investigation aproval required, action post approval not required, action pre approval not required
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            SQX_Test_Finding auditFinding1 =  audit.addAuditFinding()
                                                .setRequired(true, true, true, false, false) //response, containment, and investigation required
                                                .setApprovals(investigationApproval, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();
            SQX_Test_Finding auditFinding2 =  audit.addAuditFinding()
                                                .setRequired(true, true, true, false, false)//response, containment, and investigation required
                                                .setApprovals(false, false, false)
                                                .setStatus(SQX_Finding.STATUS_OPEN)
                                                .save();

            System.runas(responseSubmitter) {

                SQX_Test_ChangeSet changeSet = new SQX_Test_ChangeSet();

                // added audit response
                changeSet.addChanged('AuditResponse-1', 
                                         new SQX_Audit_Response__c(
                                               SQX_Audit__c= audit.audit.Id,
                                               Audit_Response_Summary__c ='Audit Response for finding 1'));

                // added finding response 1
                changeSet.addChanged('responseForFinding1-1', 
                                        new SQX_Finding_Response__c(
                                               SQX_Finding__c = auditFinding1.finding.Id,
                                               //RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Response , SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITH_FINDING),
                                               Response_Summary__c = 'Response for audit')
                                    )
                            .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');

                // added finding response 2
                changeSet.addChanged('responseForFinding2-1',
                                        new SQX_Finding_Response__c( 
                                                SQX_Finding__c = auditFinding2.finding.Id,
                                                //RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Response , SQX_Finding_Response.RECORD_TYPE_RESPONSE_WITH_FINDING),
                                                Response_Summary__c = 'Response for audit'))
                            .addRelation(SQX_Finding_Response__c.SQX_Audit_Response__c, 'AuditResponse-1');

                // added containment in finding 1
                changeSet.addChanged('ContainmentForBoth-1',
                                        new SQX_Containment__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                 Containment_Summary__c = 'Containment for both finding',
                                                 Completion_Date__c = Date.today()) );

                //added  investigation for finding 1
                changeSet.addChanged('InvestigationForBoth-1',
                                        new SQX_Investigation__c(
                                                SQX_Finding__c = auditFinding1.finding.Id,
                                                Investigation_Summary__c = 'Investigation for both finding'));

                 //added response inlusion of type containment
                changeSet.addChanged('Inclusion-1',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'ContainmentForBoth-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

                 changeSet.addChanged('Inclusion-2',
                                        new SQX_Response_Inclusion__c(Type__c = 'Containment'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Containment__c, 'ContainmentForBoth-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding2-1');

                //added response inlusion of type investigation
                changeSet.addChanged('Inclusion-3',
                                        new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'InvestigationForBoth-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding1-1');

                changeSet.addChanged('Inclusion-4',
                                        new SQX_Response_Inclusion__c(Type__c = 'Investigation'))
                            .addRelation(SQX_Response_Inclusion__c.SQX_Investigation__c, 'InvestigationForBoth-1')
                            .addRelation(SQX_Response_Inclusion__c.SQX_Response__c,'responseForFinding2-1');

                
                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'submitresponse');
                params.put('comment', 'Mock comment');
                params.put('purposeOfSignature', 'Mock purposeOfSignature');

                /*
                * Act : Submit audit response for audit
                */
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(audit.audit.Id, '{"changeSet": []}', changeSet.toJSON(), false , null, params ), 'Expected to successfully submit reponse');
            }                                   
            

            return audit;
    }
    
    /*
    *   test for actions to be performed on audit response submission when aproval is not required
    */
    public static testmethod void auditResponseSubmitted_ResponseProcessed(){
            if(!runAllTests && !run_auditResponseSubmitted_ResponseProcessed){
                return;
            }
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange and act 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, false, responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id, Submitted_On__c, Submitted_By__c, Response_Approver__c, Approval_Status__c, Status__c from SQX_Audit_Response__c 
                                                        where SQX_Audit__c= :audit.audit.Id];

                /**
                * Test for response once published should not be allowed to be deleted
                **/
                Database.DeleteResult deleteAuditResponseResult = Database.Delete(auditResponse,false);
                System.assertEquals(false, deleteAuditResponseResult.isSuccess(), 'Expected audit cannot be deleted once published');

                //Assert the Submitted on and submitted by are filled 
                System.assertEquals([Select Name from User where Id= :responseSubmitter.Id ].Name, auditResponse.Submitted_By__c,  'Expected current UserName to be set in submitted by');
                System.assertEquals(Date.Today(), ((DateTime)auditResponse.Submitted_On__c).date(),  'Expected today date to be set in submitted on');

                //Assert that audit response approver is set to audit owner
                System.assertEquals([Select OwnerId from SQX_Audit__c where Id= :audit.audit.Id].OwnerId, auditResponse.Response_Approver__c,  'Expected current UserName to be set in submitted by');
                
                /*
                * Approval is not required, so the audit reponse should be approved
                */
                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_NONE, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_NONE+ ' but found ' +auditResponse.Approval_Status__c);

                List<SQX_Finding_Response__c> findingResponses = [SELECT Approval_Status__c, Status__c FROM SQX_Finding_Response__c where 
                                                                                SQX_Audit_Response__c =:auditResponse.Id ];

                System.assertEquals(2, findingResponses.size(),  
                    'Expected 2 finding responses to be associated with Audit Response but found '+ findingResponses.size());

                /*
                * Test that finding response status and approval status should be synced with audit response 
                * when changes in these field are detected in audit response
                */
                System.assertEquals(findingResponses[0].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[0].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                System.assertEquals(findingResponses[1].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[1].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                
            }
    }

    /*
    *   test for actions to be performed on audit response submission when iinvestigation aproval is required
    */
    public static testmethod void auditResponseSubmitted_SentForApproval(){
            if(!runAllTests && !run_auditResponseSubmitted_SentForApproval){
                return;
            }           
            /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange and act 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id, Submitted_On__c, Submitted_By__c, Response_Approver__c, Approval_Status__c, Status__c from SQX_Audit_Response__c 
                                                        where SQX_Audit__c= :audit.audit.Id];
                
                //Assert that audit response approver is set to audit owner
                System.assertEquals([Select OwnerId from SQX_Audit__c where Id= :audit.audit.Id].OwnerId, auditResponse.Response_Approver__c,  'Expected current UserName to be set in submitted by');
                
                /*
                * Approval is required in fining 1 for investigation, so the audit reponse should go for approval
                */
                System.assertEquals(SQX_Audit_Response.STATUS_IN_APPROVAL, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_IN_APPROVAL+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_PENDING, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_PENDING+ ' but found ' +auditResponse.Approval_Status__c);

                List<SQX_Finding_Response__c> findingResponses = [SELECT Approval_Status__c, Status__c FROM SQX_Finding_Response__c where 
                                                                                SQX_Audit_Response__c =:auditResponse.Id ];

                System.assertEquals(2, findingResponses.size(),  
                    'Expected 2 finding responses to be associated with Audit Response but found '+ findingResponses.size());

                /*
                * Test that finding response status and approval status should be synced with audit response 
                * when changes in these field are detected in audit response
                */
                System.assertEquals(findingResponses[0].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[0].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                System.assertEquals(findingResponses[1].Status__c, auditResponse.Status__c, 
                    'Expected finding response status to be synced with audit response status but finding response status is '+ 
                    findingResponses[0].Status__c + ' and audit response status is '+ auditResponse.Status__c);
                System.assertEquals(findingResponses[1].Approval_Status__c, auditResponse.Approval_Status__c, 
                    'Expected finding response approval status to be synced with audit response status but finding response approval status is '+ 
                    findingResponses[0].Approval_Status__c + ' and audit response status is '+ auditResponse.Approval_Status__c);

                
            }
    }

    /*
    *   test for status check when audit response is approved
    */
    public static testmethod void auditResponse_ApprovalRequired_Approved(){
            if(!runAllTests && !run_auditResponse_ApprovalRequired_Approved){
                return;
               }  
        /* construct a response */
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User stdUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);


            
            System.runas(stdUser){
                /*
                * Arrange 
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true,responseSubmitter);//investigation approval not required

                /*
                * Assert
                */
                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                System.runas(adminUser){
                    SQX_Audit_Response__c auditResponse = [Select Id from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id]; 

                    /*
                    * Act : approve the audit response 
                    */
                    Map<String, String> params = new Map<String, String>();
                    params.put('nextAction', 'Approve');
                    params.put('recordID', auditResponse.Id);
                    Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                    System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                    'Expected to successfully Approve '+ auditId);

                    auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                    /*
                    * Assert : Response should be approved 
                    */
                    System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
                    System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_APPROVED, auditResponse.Approval_Status__c,  
                        'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_APPROVED+ ' but found ' +auditResponse.Approval_Status__c);

                }

                
            }
    }

    /*
    *   test for status check when audit response is rejected
    */
    public static testmethod void auditResponse_ApprovalRequired_Rejected(){
            if(!runAllTests && !run_auditResponse_ApprovalRequired_Rejected){
                return;
            }  
            UserRole role = SQX_Test_Account_Factory.createRole(); 
            User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
            User responseSubmitter =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

            
            System.runas(adminUser){
                /*
                * Arrange
                */
                SQX_Test_Audit audit = arrangeForAuditResponse(adminUser, true, responseSubmitter);//investigation approval not required

                System.assertEquals(1, [Select count() from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id], 
                    'Expected to have only one audit response' );

                SQX_Audit_Response__c auditResponse = [Select Id from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id]; 

                 /*
                * Act : Reject the response
                */
                Map<String, String> params = new Map<String, String>();
                params.put('nextAction', 'Reject');
                params.put('recordID', auditResponse.Id);
                Id auditId = SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, '{"changeSet": []}', null, params, null);

                System.assertEquals(SQX.Audit, string.valueOf(auditId.getsObjectType()),  
                'Expected to successfully Reject '+ auditId);

                auditResponse = [Select Status__c, Approval_Status__c from SQX_Audit_Response__c where SQX_Audit__c= :audit.audit.Id];

                /*
                * Assert: Response should be approved 
                */
                System.assertEquals(SQX_Audit_Response.STATUS_COMPLETED, auditResponse.Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.STATUS_COMPLETED+ ' but found ' +auditResponse.Status__c);
                System.assertEquals(SQX_Audit_Response.APPROVAL_STATUS_REJECTED, auditResponse.Approval_Status__c,  
                    'Expected Status to be '+ SQX_Audit_Response.APPROVAL_STATUS_REJECTED+ ' but found ' +auditResponse.Approval_Status__c);

            }
    }

}