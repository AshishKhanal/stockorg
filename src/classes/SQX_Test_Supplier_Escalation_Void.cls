/**
 * Unit tests for Supplier Escalation void
 */
@isTest
public class SQX_Test_Supplier_Escalation_Void {

    @testSetup
    public static void commonSetup() {
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(adminUser) {
            // create auto number record for supplier escalation
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Escalation',
                Next_Number__c = 1,
                Number_Format__c = 'ESC-{1}',
                Numeric_Format__c = '0000'
            );
        }
        System.runAs(standardUser) {
            // Create accounts
            SQX_Test_Supplier_Escalation.createAccounts();
        }
    }

    /**
     * Given : Draft Supplier Escalation Record with draft steps
     * When : Voiding record
     * Then : Status is void and Stage is null and Escalation Record is locked and cannot be edited and step record remains unaffected.
     */
    public static testMethod void givenDrftSupplierEscalationRecord_WhenRecordIsVoided_StatusIsVoidStageIsNull() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            //Arrange: get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // create policy task so that it will create step later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            // submit
            escalation.submit();
            
            // Assert : Ensure Supplier Escalation Step record is created
            SQX_Supplier_Escalation_Step__c step = [SELECT Id, Status__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id];
            System.assert(step.Id != null, 'Expected Supplier Escalation Step record to be created but is not being created.');
            System.assert(step.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT, 'Expected Supplier Escalation Step record to be in draft status but is ' + step.Status__c);
            
            // Act : void the record
            escalation.voidEscalation();
            
            // Assert : Ensure escalation is void and there is no any impact on step's status
            SQX_Supplier_Escalation__c se = [SELECT Id, Status__c, Record_Stage__c, Is_Locked__c, Activity_Code__c FROM SQX_Supplier_Escalation__c WHERE Id = :escalation.supplierEscalation.Id];
            System.assert(se.Status__c == SQX_Supplier_Common_Values.STATUS_VOID, 'Expected supplier record status to be voided but is ' + se.Status__c);
            System.assert(se.Record_Stage__c == null, 'Expected supplier record stage to be null but is ' + se.Record_Stage__c);
            
            // Assert : Ensure step is unaffected
            step = [SELECT Id, Status__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id];
            System.assert(step.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT, 'Expected supplier record status to be' + SQX_Supplier_Common_Values.STATUS_DRAFT + ' but is ' + step.Status__c);
            
            // Act : try to edit void record
            se.Part_Name__c = 'Sample part name';
            
            //System.assert(false, se.Is_Locked__c);
            final String msg = 'Record status or Step status does not support the Action Performed.';
            SQX_Test_Supplier_Escalation.performActionAndAssert(se, msg, 'Expected void record not be updated but is being updated.', null);
        }
    }

    /**
     * Given : Open Supplier Escalation Record with Open step
     * When : Voiding record
     * Then : Status is void and Stage is null and open step becomes draft and task of step is deleted
     */
    public static testMethod void givenOpenSupplierRecord_WhenRecordIsVoided_StatusIsVoidStageIsNullAndOpenStepBecomesDraftAndTaskIsDeleted() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            //Arrange: get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            
            // create policy task so that it will create step later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            
            // submit
            escalation.submit();
            // initiate
            escalation.initiate();
            
            // Assert : Ensure Supplier Escalation Step record is created and is open
            SQX_Supplier_Escalation_Step__c step = [SELECT Id, Status__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id];
            System.assert(step.Id != null, 'Expected Supplier Escalation Step record to be created but is not being created.');
            System.assert(step.Status__c == SQX_Supplier_Common_Values.STATUS_OPEN, 'Expected Supplier Escalation Step record to be in' + SQX_Supplier_Common_Values.STATUS_OPEN + 'Open status but is ' + step.Status__c);
            
            // Assert : Ensure task is created
            Task t = [SELECT Id FROM Task WHERE Child_What_Id__c = :step.Id];
            System.assert(t.Id != null, 'Expected task to be created but is ' + t.Id);
            
            // Act : void the record
            escalation.voidEscalation();
            
            // Assert : Ensure escalation is void
            SQX_Supplier_Escalation__c se = [SELECT Id, Status__c, Record_Stage__c FROM SQX_Supplier_Escalation__c WHERE Id = :escalation.supplierEscalation.Id];
            System.assert(se.Status__c == SQX_Supplier_Common_Values.STATUS_VOID, 'Expected supplier record status to be voided but is ' + se.Status__c);
            System.assert(se.Record_Stage__c == null, 'Expected supplier record stage to be null but is ' + se.Record_Stage__c);
            
            // Assert : Ensure Open Step becomes Draft and the corresponding task is deleted.
            step = [SELECT Id, Status__c FROM SQX_Supplier_Escalation_Step__c WHERE SQX_Parent__c = :escalation.supplierEscalation.Id];
            System.assert(step.Status__c == SQX_Supplier_Common_Values.STATUS_DRAFT, 'Expected supplier record status to be' + SQX_Supplier_Common_Values.STATUS_DRAFT + ' but is ' + step.Status__c);
            List<Task> tasks = [SELECT Id FROM Task WHERE Child_What_Id__c = :step.Id];
            System.assert(tasks.size() == 0, 'Expected task to be deleted but still ' + tasks.size() + ' task exists.');
        }
    }

    /**
     * Given : Open Supplier Escalation Record say se1 and is escalated which results new Escalation Record say se2
     * When : Voiding new Escalation record
     * Then : Account of supplier is linked with Source Escalation of newly created Escalation Record (se2) i.e. se1 is linked to Account.
     */
    public static testMethod void givenOpenSupplierRecordWithSourceEscalation_WhenRecordIsVoided_ThenSourceEscalationIsLinkedToAccount() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            //Arrange: get account record
            List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
            // create policy task so that it will create step later on submitting escalation record
            SQX_Test_Supplier_Escalation.createPolicyTasks(1, 'Task', standardUser, 1);
            
            //Arrange: create Supplier Escalation record
            SQX_Test_Supplier_Escalation escalation = new SQX_Test_Supplier_Escalation(accounts[0]).save();
            escalation.save();
            
            // submit and initiate
            escalation.submit();
            escalation.initiate();
            escalation.escalate('2');
            
            // get new supplier escalation record which is created by escalating
            SQX_Supplier_Escalation__c escalatedRecord = [SELECT Id, SQX_Source_Escalation__c, Status__c FROM SQX_Supplier_Escalation__c WHERE SQX_Source_Escalation__c = :escalation.supplierEscalation.Id];
            System.assert(escalatedRecord.Id != null, 'On doing escalation new Supplier Escalation record to be created but is ' + escalatedRecord.Id);
            System.assert(escalatedRecord.SQX_Source_Escalation__c != null && escalatedRecord.SQX_Source_Escalation__c == escalation.supplierEscalation.Id, 'Expected SQX_Source_Escalation__c to be in esclated record but is ' + escalatedRecord.SQX_Source_Escalation__c);
                                    
            // Act : void the record
            escalatedRecord.compliancequest__Activity_Code__c = SQX_Supplier_Common_Values.ACTIVITY_CODE_VOID;
            update escalatedRecord;
            
            // Assert : Ensure escalation is void
            escalatedRecord = [SELECT Id, SQX_Source_Escalation__c, Status__c, Record_Stage__c FROM SQX_Supplier_Escalation__c WHERE Id = :escalatedRecord.Id];
            System.assert(escalatedRecord.Status__c == SQX_Supplier_Common_Values.STATUS_VOID, 'Expected supplier record status to be voided but is ' + escalatedRecord.Status__c);
            System.assert(escalatedRecord.Record_Stage__c == null, 'Expected supplier record stage to be null but is ' + escalatedRecord.Record_Stage__c);
            
            // Assert : ensure SQX_Supplier_Escalation__c is linked to account
            Account acc = [SELECT Id, SQX_Supplier_Escalation__c FROM Account WHERE Id =:accounts[0].Id];
            System.assert(acc.SQX_Supplier_Escalation__c != null && acc.SQX_Supplier_Escalation__c == escalatedRecord.SQX_Source_Escalation__c, 'Expected Supplier Escalation is linked with account but is ' + acc.SQX_Supplier_Escalation__c);
        }
    }
    
    /**
     * Given : a escalated record with step in draft status
     * When : further escalation record is voided
     * Then : source escalation is reverted back to open status
     */
    public static testMethod void whenFurtherActionIsVoid_ThenSourceEscalationIsRevertedToOpen() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            // Escalation step in draft status
            SQX_Supplier_Escalation__c escalation = validateVoidEscalation(standardUser, SQX_Steps_Trigger_Handler.STATUS_DRAFT, null);
            
            // Assert: Source escalation is reopened
            System.assertEquals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_IN_PROGRESS, escalation.Workflow_Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_OPEN, escalation.Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STAGE_IN_PROGRESS, escalation.Record_Stage__c);
            System.assert(escalation.SQX_Further_Action__c == null, 'Futher action of source escalation is cleared with voided');
        }
    }
    
    /**
     * Given : a escalated record with complete step in NoGo result
     * When : further escalation record is voided
     * Then : source escalation is reverted back to On Hold
     */
    public static testMethod void whenFurtherActionIsVoid_ThenSourceEscalationIsRevertedToOnHold() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            // Escalation step in draft status
            SQX_Supplier_Escalation__c escalation = validateVoidEscalation(standardUser, SQX_Steps_Trigger_Handler.STATUS_COMPLETE, SQX_Steps_Trigger_Handler.RESULT_NO_GO);
            
            // Assert: Source escalation is reopened
            System.assertEquals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_ON_HOLD, escalation.Workflow_Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_OPEN, escalation.Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STAGE_ON_HOLD, escalation.Record_Stage__c);
            System.assert(escalation.SQX_Further_Action__c == null, 'Futher action of source escalation is cleared with voided');
        }
    }
    
    /**
     * Given : a escalated record with complete step in GO result
     * When : further escalation record is voided
     * Then : source escalation is reverted back to On Hold
     */
    public static testMethod void whenFurtherActionIsVoid_ThenSourceEscalationIsRevertedToComplete() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Escalation.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            // Escalation step in draft status
            SQX_Supplier_Escalation__c escalation = validateVoidEscalation(standardUser, SQX_Steps_Trigger_Handler.STATUS_COMPLETE, SQX_Steps_Trigger_Handler.RESULT_GO);
            
            // Assert: Source escalation is reopened
            System.assertEquals(SQX_Supplier_Common_Values.WORKFLOW_STATUS_COMPLETED, escalation.Workflow_Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STATUS_COMPLETE, escalation.Status__c);
            System.assertEquals(SQX_Supplier_Common_Values.STAGE_VERIFICATION, escalation.Record_Stage__c);
            System.assert(escalation.SQX_Further_Action__c == null, 'Futher action of source escalation is cleared with voided');
        }
    }
    
    private static SQX_Supplier_Escalation__c validateVoidEscalation(User user, String stepStatus, String result){
        //Get account record
        List<Account> accounts = SQX_Test_Supplier_Escalation.getAccountAndContactRecords();
        
        //Given: Create Further Supplier Escalation record in triage state
        SQX_Test_Supplier_Escalation furtherAction = new SQX_Test_Supplier_Escalation(accounts[0]);
        furtherAction.supplierEscalation.Status__c = SQX_Supplier_Common_Values.STATUS_DRAFT;
        furtherAction.supplierEscalation.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_TRIAGE;
        furtherAction.save();
        
        // Create a escalated source escalation with source escalation linked
        SQX_Test_Supplier_Escalation sourceEscalation = new SQX_Test_Supplier_Escalation(accounts[0]);
        sourceEscalation.supplierEscalation.Status__c = SQX_Supplier_Common_Values.STATUS_CLOSED;
        sourceEscalation.supplierEscalation.Record_Stage__c = SQX_Supplier_Common_Values.STAGE_ESCALATED;
        sourceEscalation.supplierEscalation.Target_Escalation_Level__c = '2';
        sourceEscalation.supplierEscalation.SQX_Further_Action__c = furtherAction.supplierEscalation.Id;
        sourceEscalation.save();
        
        furtherAction.supplierEscalation.SQX_Source_Escalation__c = sourceEscalation.supplierEscalation.Id;
        furtherAction.save();
        
        // Create a step with given criteria
        SQX_Supplier_Escalation_Step__c step  = new SQX_Supplier_Escalation_Step__c(RecordTypeId = Schema.SObjectType.SQX_Supplier_Escalation_Step__c.getRecordTypeInfosByDeveloperName().get('Task').getRecordTypeId(), 
                                                                                    SQX_Parent__c = sourceEscalation.supplierEscalation.Id, 
                                                                                    Applicable__c = true, 
                                                                                    SQX_User__c = user.Id, 
                                                                                    Status__c = stepStatus, 
                                                                                    Step__c = 1,
                                                                                    Result__c = result
                                                                                    );
        insert step;
        
        // ACT: Void the further escalation record
        furtherAction.voidEscalation();
        sourceEscalation.synchronize();
        
        return sourceEscalation.supplierEscalation;
    }
}