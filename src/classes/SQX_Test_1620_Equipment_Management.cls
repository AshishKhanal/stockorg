/**
* Unit tests ensure that Next Calibration Date cannot be less than Last Calibration Date
* for Equipment and Calibration sObject
*/
@isTest
public class SQX_Test_1620_Equipment_Management{

    static Boolean runAllTests = true,
                   run_givenLongerLastCalibrationDate_ValidationErrorThrown = false,
                   run_givenLongerCalibrationDate_ValidationErrorThrown = false,
                   run_givenLongerNextCalibrationDate_ValidationErrorNotThrown = false;

    /**
    * This test ensures that Validation error message is thrown
    * if a Last calibration date is longer than next calibration date
    * in Equipment object
    */
    public testmethod static void givenLongerLastCalibrationDate_ValidationErrorThrown(){

        if (!runAllTests && !run_givenLongerLastCalibrationDate_ValidationErrorThrown){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){ 

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c equipment = new SQX_Equipment__c();
            equipment.Name = '123test';
            equipment.Equipment_Description__c = 'This is test equipment';
            equipment.Last_Calibration_Date__c = Date.today().addDays(10); // last calibration date is added 10 days more
            equipment.Next_Calibration_Date__c = Date.today();
            equipment.Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE ;
            equipment.Equipment_Status__c = SQX_Equipment.STATUS_NEW;

            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert( equipment, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(false, sr.isSuccess(), 'Expecting equipment reocord should not be inserted');

            //Act: get messages from exception
            Boolean actualErrorMessage, expectedExceptionThrown;

            Database.Error[] e = sr.getErrors();
                    Integer count = 1, length = e.size();
                    for (Integer i = 0; i < length; i++){
                        actualErrorMessage = e[i].getMessage().contains('Next calibration date cannot be less than last calibration date') ? true : false;
                        
                        if(actualErrorMessage) {
                            expectedExceptionThrown =  true;
                            break;
                        }
                    }           

            

            //Assert: ensure Validation is thrown or not
            System.assertEquals(expectedExceptionThrown, true, 'Expected Error message not thrown ');
        }
    }


    /**
    * This test ensures that Validation error message is thrown
    * if a Calibration date is longer than next calibration date
    * in Calibration object
    */
    public testmethod static void givenLongerCalibrationDate_ValidationErrorThrown(){

        if (!runAllTests && !run_givenLongerCalibrationDate_ValidationErrorThrown){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){ 

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-123',
                Equipment_Description__c = 'EQP 123 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );

            Database.insert(eqp1, false);
            
            //Arrange: Create Calibration for above Equipment with all required field
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today().addDays(20), // new calibration date is 20 days more
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN,
                Next_Calibration_Date__c = Date.today()
            );

            //Act: inserting new calibration record
            Database.SaveResult sr = Database.insert( cal1, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(false, sr.isSuccess(), 'Expecting calibration reocord should not be inserted');

            //Act: get messages from exception
            Boolean actualErrorMessage, expectedExceptionThrown;

            Database.Error[] e = sr.getErrors();
                    Integer count = 1, length = e.size();
                    for (Integer i = 0; i < length; i++){
                        actualErrorMessage = e[i].getMessage().contains('Next Calibration Date cannot be less than Calibration Date') ? true : false;
                        
                        if(actualErrorMessage) {
                            expectedExceptionThrown =  true;
                            break;
                        }       

                    }

            //Assert: ensure Validation is thrown or not
            System.assertEquals(expectedExceptionThrown, true, 'Expected Error message not thrown ');

        }
    }


    /**
    * This test ensures that Validation error message is not thrown
    * if a Next Calibration date is longer than Calibration date in Calibration object and
    * if a Next Calibration date is longer than Last Calibration date in Equipment object
    */
    public testmethod static void givenLongerNextCalibrationDate_ValidationErrorNotThrown(){

        if (!runAllTests && !run_givenLongerNextCalibrationDate_ValidationErrorNotThrown){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){ 

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-123',
                Equipment_Description__c = 'EQP 123 desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Calibration_Interval__c = 20,
                Next_Calibration_Date__c = Date.today().addDays(20)
            );

            //Act: inserting new equipment record
            Database.SaveResult sr1 = Database.insert( eqp1, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr1.isSuccess(), 'Expecting calibration reocord should be inserted');
            
            //Arrange: Create Calibration for above Equipment with all required field
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN,
                Calibration_Date__c = Date.today(),
                Next_Calibration_Date__c = Date.today().addDays(20)
            );
            
            //Act: inserting new equipment record
            Database.SaveResult sr2 = Database.insert( cal1, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr2.isSuccess(), 'Expecting calibration reocord should be inserted');

        }
    }        


}