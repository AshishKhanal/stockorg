/**
* Test to ensure Skip Revision Training functionality of Document Management
*/
@isTest
public class SQX_Test_2165_SkipRevisionTraining {
    @testSetup
    public static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User user2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user2');
        
        SQX_Job_Function__c jf1, jf2, jf3, jf4;
        SQX_Personnel__c p1, p2;
        
        System.runAs(admin1) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = SQX_Test_Job_Function.createJFs(4, true);
            insert jfs;

            jf1 = jfs[0];
            jf2 = jfs[1];
            jf3 = jfs[2];
            jf4 = jfs[3];
        }
    
        System.runAs(user1) {
            // add required personnels
            p1 = new SQX_Test_Personnel().mainRecord;
            p2 = new SQX_Test_Personnel().mainRecord;
            p1.Full_Name__c = 'P1';
            p2.Full_Name__c = 'P2';
            p1.Active__c = true;
            p2.Active__c = true;
            insert new List<SQX_Personnel__c> { p1, p2 };
            
            // add required active personnel job functions
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf2.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf3.Id, Active__c = true ));
            insert pjfs;
        }
    }
    
    static Map<String, SQX_Job_Function__c> getJobFunctions() {
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        
        Map<String, SQX_Job_Function__c> jfMap = new Map<String, SQX_Job_Function__c>();
        for (SQX_Job_Function__c jf : jfs) {
            jfMap.put(jf.Name, jf);
        }
        
        return jfMap;
    }
    
    static Map<String, SQX_Personnel__c> getPersonnels() {
        List<SQX_Personnel__c> psns = [SELECT Id, Name, Full_Name__c, Identification_Number__c, SQX_User__c, Active__c FROM SQX_Personnel__c];
        
        Map<String, SQX_Personnel__c> psnMap = new Map<String, SQX_Personnel__c>();
        for (SQX_Personnel__c psn : psns) {
            psnMap.put(psn.Full_Name__c, psn);
        }
        
        return psnMap;
    }
    
    /**
    * ensures default value for Skip Revision Training field of copied requirements are set properly as per revised document change scope
    * i.e. when change scope is Minor Change, then default skip value is true else false
    */
    public static testmethod void givenReviseDocumentWithMinorAndMajorChangeScope_SkipRevisionTrainingSetProperly() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        System.assert(user1 != null);
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1'),
                            jf2 = jfs.get('JF2');
        System.assert(jf1 != null);
        System.assert(jf2 != null);
        
        System.runAs(user1) {
            // add required controlled document
            SQX_Test_Controlled_Document d1 = new SQX_Test_Controlled_Document().save();

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            d1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            d1.save();

            // add requirements
            List<SQX_Requirement__c> reqs = new List<SQX_Requirement__c>();
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = d1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Skip_Revision_Training__c = false
            ));
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = d1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Skip_Revision_Training__c = false
            ));
            insert reqs;
            
            // ACT: revise document with change scope as minor change
            SQX_Test_Controlled_Document r1 = d1.revise(SQX_Controlled_Document.CHANGE_SCOPE_MINOR_CHANGE);
            
            List<SQX_Requirement__c> r1reqs = [SELECT Id FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c = :r1.doc.Id AND Skip_Revision_Training__c = true];
            System.assertEquals(2, r1reqs.size(), 'All copied requirements of revised document are expected to have Skip Revision Training as true.');
            
            Test.startTest();
            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            r1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            r1.save();
            
            // ACT: revise document with change scope as major change
            SQX_Test_Controlled_Document r2 = r1.revise(SQX_Controlled_Document.CHANGE_SCOPE_MAJOR_CHANGE);
            
            List<SQX_Requirement__c> r2reqs = [SELECT Id FROM SQX_Requirement__c WHERE SQX_Controlled_Document__c = :r2.doc.Id AND Skip_Revision_Training__c = false];
            System.assertEquals(2, r2reqs.size(), 'All copied requirements of revised document are expected to have Skip Revision Training as false.');
            Test.stopTest();
        }
    }
    
    /**
    * ensures PDJF of next revision to skip revision training gets completed and pending training gets cancelled when previous revision training relate to the job funciton is completed
    */
    public testmethod static void givenCompletePreviousRevisionTraining_NextRevisionPDJFIsCompletedAndPendingTrainingIsCancelled() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf2 = jfs.get('JF2');
        SQX_Job_Function__c jf3 = jfs.get('JF3');
        
        SQX_Personnel__c p2 = getPersonnels().get('P2');
        
        SQX_DB db = new SQX_DB();
        List<SQX_Controlled_Document__c> docRevs = new List<SQX_Controlled_Document__c>();
        
        System.runAs(user1) {
            // read access for user2
            List<SQX_Personnel__Share> shares = new List<SQX_Personnel__Share>();
            shares.add(new SQX_Personnel__Share(
                ParentId = p2.Id,
                UserOrGroupId = user2.Id,
                AccessLevel = 'read'
            ));
            db.op_insert(shares, new List<SObjectField> {});
            
            // add required controlled documents
            docRevs.add(new SQX_Test_Controlled_Document().doc);
            docRevs[0].Date_Issued__c = System.today() - 40;
            docRevs[0].Effective_Date__c = System.today();
            docRevs[0].Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            docRevs.add(docRevs[0].clone()); // add revision
            docRevs[1].Revision__c = SQX_Utilities.getNextRev(docRevs[0].Revision__c);
            docRevs[1].Date_Issued__c = System.today() - 20;
            docRevs[1].Effective_Date__c = System.today() + 20;
            docRevs[1].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            docRevs.add(docRevs[1].clone()); // add revision
            docRevs[2].Revision__c = SQX_Utilities.getNextRev(docRevs[1].Revision__c);
            docRevs[2].Date_Issued__c = System.today();
            docRevs[2].Effective_Date__c = System.today() + 40;
            docRevs[2].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            db.op_insert(docRevs, new List<SObjectField> {});
            
            List<SQX_Requirement__c> reqs = new List<SQX_Requirement__c>();
            
            // add active requirement for rev 1 to create initial training
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = docRevs[0].Id,
                SQX_Job_Function__c = jf2.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Skip_Revision_Training__c = false,
                Active__c = true
            ));
            db.op_insert(reqs, new List<SObjectField> {});
            
            // get initial PDJF with pending training
            List<SQX_Personnel_Document_Job_Function__c> pdjfs = [  SELECT Id,
                                                                        SQX_Personnel_Document_Training__c
                                                                    FROM SQX_Personnel_Document_Job_Function__c
                                                                    WHERE Personnel_Id__c = :p2.Id
                                                                        AND Controlled_Document_Id__c = :docRevs[0].Id
                                                                        AND Job_Function_Id__c = :jf2.Id
                                                                        AND SQX_Personnel_Document_Training__c != null
                                                                        AND Training_Type__c = :SQX_Personnel_Document_Job_Function.TRAINING_TYPE_INITIAL
                                                                        AND Training_Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            System.assertEquals(1, pdjfs.size(), 'Initial training type PDJF and related document training is expected to be created');
            
            // complete initial training
            // note: initial training must be completed to create revision trainings
            SQX_Personnel_Document_Training__c initialPdt = new SQX_Personnel_Document_Training__c(
                Id = pdjfs[0].SQX_Personnel_Document_Training__c,
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                Completion_Date__c = System.today() - 20,
                Trainer_SignOff_Date__c = System.today() - 20
            );
            db.op_update(new List<SQX_Personnel_Document_Training__c>{ initialPdt }, new List<SObjectField> {});
            
            Test.startTest();
            
            // add active requirement for rev 2 and JF3 to create revision training with lower competency
            reqs.clear();
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = docRevs[1].Id,
                SQX_Job_Function__c = jf3.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Skip_Revision_Training__c = false,
                Active__c = true
            ));
            db.op_insert(reqs, new List<SObjectField> {});
            
            // get revision PDJF with pending training
            pdjfs =[SELECT Id,
                        SQX_Personnel_Document_Training__c
                    FROM SQX_Personnel_Document_Job_Function__c
                    WHERE Personnel_Id__c = :p2.Id
                        AND Controlled_Document_Id__c = :docRevs[1].Id
                        AND Job_Function_Id__c = :jf3.Id
                        AND SQX_Personnel_Document_Training__c != null
                        AND Training_Type__c = :SQX_Personnel_Document_Job_Function.TRAINING_TYPE_REVISION
                        AND Training_Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            System.assertEquals(1, pdjfs.size(), 'Revision training type PDJF and related document training is expected to be created');
            
            // do user sign off on revision training
            SQX_Personnel_Document_Training__c pdtJf3Rev2 = new SQX_Personnel_Document_Training__c(
                Id = pdjfs[0].SQX_Personnel_Document_Training__c,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                User_SignOff_Date__c = System.today() - 20
            );
            db.op_update(new List<SQX_Personnel_Document_Training__c>{ pdtJf3Rev2 }, new List<SObjectField> {});
            
            // add required requirements
            reqs.clear();
            
            // add active requirement for rev 2 and JF2 to create revision training with higher competency to create pending training
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = docRevs[1].Id,
                SQX_Job_Function__c = jf2.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Skip_Revision_Training__c = false,
                Active__c = true
            ));
            // add active requirements for rev 3 and JF2 to create revision training with skip revision training
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = docRevs[2].Id,
                SQX_Job_Function__c = jf2.Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Skip_Revision_Training__c = true,
                Active__c = true
            ));
            db.op_insert(reqs, new List<SObjectField> {});
            
            // get revision PDJF with pending training
            pdjfs =[SELECT Id,
                        Personnel_Id__c,
                        Controlled_Document_Id__c,
                        Job_Function_Id__c,
                        SQX_Personnel_Document_Training__c
                    FROM SQX_Personnel_Document_Job_Function__c
                    WHERE Personnel_Id__c = :p2.Id
                        AND Job_Function_Id__c = :jf2.Id
                        AND SQX_Personnel_Document_Training__c != null
                        AND Training_Type__c = :SQX_Personnel_Document_Job_Function.TRAINING_TYPE_REVISION
                        AND Training_Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            System.assertEquals(2, pdjfs.size(), '2 revision training type PDJFs and related document trainings are expected to be created');
            
            // map previous revision training and next revision training for JF2
            SQX_Personnel_Document_Job_Function__c pdjfJf2Rev2, pdjfJf2Rev3;
            for (SQX_Personnel_Document_Job_Function__c pdjf : pdjfs) {
                if (pdjf.Controlled_Document_Id__c == docRevs[1].Id) {
                    pdjfJf2Rev2 = pdjf;
                }
                else if (pdjf.Controlled_Document_Id__c == docRevs[2].Id) {
                    pdjfJf2Rev3 = pdjf;
                }
            }
            
            System.assertNotEquals(null, pdjfJf2Rev2, 'Revision training type PDJF and related document training is expected to be created');
            System.assertNotEquals(null, pdjfJf2Rev3, 'Revision training type PDJF and related document training is expected to be created');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: complete previous revision training for JF3 i.e. rev 2
            pdtJf3Rev2.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            pdtJf3Rev2.Completion_Date__c = System.today();
            pdtJf3Rev2.Trainer_SignOff_Date__c = System.today();
            db.op_update(new List<SQX_Personnel_Document_Training__c>{ pdtJf3Rev2 }, new List<SObjectField> {});
            
            // ASSERT: next revision training for JF2 is still pending 
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING,
                [ SELECT Id, Training_Status__c FROM SQX_Personnel_Document_Job_Function__c WHERE Id = :pdjfJf2Rev3.Id ].Training_Status__c,
                'Next revision PDJF is expected not to be changed and be pending when previous training related to other job functions is completed.');
            
            // ACT: complete previous revision training for JF2 i.e. rev 2
            SQX_Personnel_Document_Training__c pdtJf2Rev2 = new SQX_Personnel_Document_Training__c(
                Id = pdjfJf2Rev2.SQX_Personnel_Document_Training__c,
                Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE,
                Completion_Date__c = System.today(),
                Trainer_SignOff_Date__c = System.today()
            );
            db.op_update(new List<SQX_Personnel_Document_Training__c>{ pdtJf2Rev2 }, new List<SObjectField> {});
            
            // get pending training created for rev 3 and JF2
            SQX_Personnel_Document_Training__c pdtJf2Rev3 = [   SELECT Id,
                                                                    Status__c,
                                                                    Trainer_SignOff_Date__c,
                                                                    Trainer_SignOff_Comment__c
                                                                FROM SQX_Personnel_Document_Training__c
                                                                WHERE Id = :pdjfJf2Rev3.SQX_Personnel_Document_Training__c ];
            
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_OBSOLETE, pdtJf2Rev3.Status__c,
                'Pending training on next revision not having active job functions is expected to be cancelled.');
            System.assertNotEquals(null, pdtJf2Rev3.Trainer_SignOff_Date__c, 'Cancelled pending training due to skip revision training is expected to have cancellation date.');
            System.assertEquals(Label.SQX_MSG_PDT_SYS_CANCEL_INCOMPLETE_TRAINING_COMMENT_FOR_SKIPPED_REVISION, pdtJf2Rev3.Trainer_SignOff_Comment__c,
                'Cancelled pending training due to skip revision training is expected to have cancellation comment: ' + Label.SQX_MSG_PDT_SYS_CANCEL_INCOMPLETE_TRAINING_COMMENT_FOR_SKIPPED_REVISION);
            
            // get PDJF and related training for JF2 of next revision i.e. rev 3 needing skip revision training
            pdjfJf2Rev3 = [ SELECT Id,
                                Training_Status__c,
                                SQX_Personnel_Document_Training__c
                            FROM SQX_Personnel_Document_Job_Function__c
                            WHERE Id = :pdjfJf2Rev3.Id ];
            
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjfJf2Rev3.Training_Status__c,
                'Next revision training and its PDJF is expected to be complete.');
            System.assertEquals(pdjfJf2Rev2.SQX_Personnel_Document_Training__c, pdjfJf2Rev3.SQX_Personnel_Document_Training__c,
                'Completed document training of previous revision is expected to be linked to next revision training type PDJF needing skip revision training.');
            
            Test.stopTest();
        }
    }
    
}