/**
* @author Pramesh Bhattarai
* @date 2015/09/23
* This class will handles trigger for SQX_Calibration object
*/
public with sharing class SQX_Calibration {

    // Calibration status
    public static final String  STATUS_OPEN = 'Open',
                                STATUS_CLOSE = 'Close',
                                STATUS_VOID = 'Void',

    // New equipment status
                                NEW_EQUIPMENT_STATUS_ACTIVE = SQX_EQUIPMENT.STATUS_ACTIVE,
                                NEW_EQUIPMENT_STATUS_SUSPENDED = SQX_EQUIPMENT.STATUS_SUSPENDED,
                                NEW_EQUIPMENT_STATUS_RETIRED = SQX_EQUIPMENT.STATUS_RETIRED,

    // Calibration results
                                CALIBRATION_RESULT_WITHIN_TOLERANCE = 'Within Tolerance',
                                CALIBRATION_RESULT_OUT_OF_TOLERANCE = 'Out of Tolerance',
                                CALIBRATION_RESULT_NA = 'NA';
    public static final String PurposeOfSignature_CloseCalibration = 'equipmentcalibrationclosed';
    public static final String PurposeOfSignature_VoidCalibration = 'equipmentcalibrationvoid';

    /**
     * map of activity code to purpose of signature label
     */
    public static final Map<String, String> purposeOfSigMap = new Map<String, String>{
        PurposeOfSignature_CloseCalibration => Label.SQX_PS_Calibration_Close_Calibration,
        PurposeOfSignature_VoidCalibration => Label.SQX_PS_Calibration_Void_Calibration
    };

    /**
    * returns default value for new equipment status as per the final calibration result
    * if result = 'Within Tolerance', then returns 'Active'
    * if result = 'Out of Tolerance', then returns 'Suspended'
    * otherwise returns null
    */
    public static String getDetaultNewEquipmentStatus(SQX_Calibration__c cal) {
        if (cal != null) {
            if (cal.Final_Calibration_Result__c == SQX_Calibration.CALIBRATION_RESULT_WITHIN_TOLERANCE) {
                return SQX_Calibration.NEW_EQUIPMENT_STATUS_ACTIVE;
            } if (cal.Final_Calibration_Result__c == SQX_Calibration.CALIBRATION_RESULT_OUT_OF_TOLERANCE) {
                return SQX_Calibration.NEW_EQUIPMENT_STATUS_SUSPENDED;
            }
        }
        return null;
    }

    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        public Bulkified(List<SQX_Calibration__c> newCalibrations, Map<Id, SQX_Calibration__c> oldCalibrations){
            super(newCalibrations, oldCalibrations);
        }

        /**
        * @description This method prevent Calibration to be deleted when Status is Close or Void,
        *               We cannot delete Voided and Closed Calibration
        */
        public Bulkified preventDeletionOnClouserAndVoid(){

            final String ACTION_NAME = 'preventDeletionOnClouserAndVoid';

            Rule allClosedAndVoidedCalibration = new Rule();
            allClosedAndVoidedCalibration.addRule(Schema.SQX_Calibration__c.Calibration_Status__c, RuleOperator.Equals, STATUS_CLOSE);
            allClosedAndVoidedCalibration.addRule(Schema.SQX_Calibration__c.Calibration_Status__c, RuleOperator.Equals, STATUS_VOID);
            allClosedAndVoidedCalibration.operator = RuleCombinationOperator.OpOr;

            
            this.resetView().applyFilter(allClosedAndVoidedCalibration, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            List<SQX_Calibration__c> calibrations = (List<SQX_Calibration__c>) allClosedAndVoidedCalibration.evaluationResult;

            for(SQX_Calibration__c calibration: calibrations){
                calibration.addError(Label.SQX_ERR_MSG_CALIBRATION_IN_CLOSE_OR_VOID_STATUS_CANNOT_BE_DELETED);
            }
            return this;            
        }
     }                                
}