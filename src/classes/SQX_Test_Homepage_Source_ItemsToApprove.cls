/**
*   This class contains test cases for ensuring that responses to be approved source is providing desired values
*   @author - Anuj Bhandari
*   @date - 06-04-2017
*/
@isTest
public class SQX_Test_Homepage_Source_ItemsToApprove{


    /*
       Items to be approved source currently returns items of type :
        1. Audits Responses
        2. Capa Responses
        3. NC Responses
        4. Document Approval
        5. Audit Approval
        6. Change Order Approval
        7. Complaint Approval
        8. Audit Program Approval
        9. Audit Report Approval
    */

    static Set<SObjectType> targetedModules = new Set<SObjectType>
    {
        SQX_Audit__c.SObjectType,
        SQX_Audit_Program__c.SObjectType,
        SQX_Audit_Report__c.SObjectType,
        SQX_Change_Order__c.SObjectType,
        SQX_Complaint__c.SObjectType,
        SQX_Controlled_Document__c.SObjectType,
        SQX_Finding_Response__c.SObjectType,
        SQX_Audit_Response__c.SObjectType
    };

    static List<SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended> getMockApprovalItems()
    {

        List<SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended> items = new List<SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended>();

        List<SObject> relatedRecords = new List<SObject>
        {
                new SQX_SObject_Generator(new SQX_Audit__c(Title__c = 'Mock Title')).getSObject(),
                new SQX_SObject_Generator(new SQX_Audit_Program__c(Title__c = 'Mock Title')).getSObject(),
                new SQX_SObject_Generator(new SQX_Change_Order__c(Title__c = 'Mock Title')).getSObject(),
                new SQX_SObject_Generator(new SQX_Complaint__c(Complaint_Title__c = 'Mock Title')).getSObject(),
                new SQX_SObject_Generator(new SQX_Controlled_Document__c(Title__c = 'Mock Title')).getSObject()
        };

        SQX_SObject_Generator gen;

        // adding response approval records

        // audit response
        SQX_Audit__c mockAudit = (SQX_Audit__c) new SQX_SObject_Generator(new SQX_Audit__c(
                                                            Title__c = 'Mock Title'
                                                        ))
                                                        .setValueForField('Name', 'Mock Audit')
                                                        .getSObject();

        gen = new SQX_SObject_Generator(new SQX_Audit_Response__c(
                                                SQX_Audit__c = mockAudit.Id,
                                                SQX_Audit__r = mockAudit
                                            ));
        relatedRecords.add(gen.getSObject());

        gen = new SQX_SObject_Generator(new SQX_Audit_Report__c(
                                                SQX_Audit__c = mockAudit.Id,
                                                SQX_Audit__r = mockAudit
                                            ));
        relatedRecords.add(gen.getSObject());


        // finding response

        // for capa
        SQX_CAPA__c mockCAPA = (SQX_CAPA__c) new SQX_SObject_Generator(new SQX_CAPA__c(Title__c = 'Mock Resp Title'))
                                                    .setValueForField('Name', 'Mock CAPA')
                                                    .getSObject();

        gen = new SQX_SObject_Generator(new SQX_Finding_Response__c(
                                                SQX_CAPA__c = mockCAPA.Id,
                                                SQX_CAPA__r = mockCAPA
                                            ));

        relatedRecords.add(gen.getSObject());

        // for NC
        SQX_NonConformance__c mockNC = (SQX_NonConformance__c) new SQX_SObject_Generator(new SQX_NonConformance__c(NC_Title__c = 'Mock Resp Title'))
                                                                        .setValueForField('Name', 'Mock NC')
                                                                        .getSObject();

        gen = new SQX_SObject_Generator(new SQX_Finding_Response__c(
                                                SQX_NonConformance__c = mockNC.Id,
                                                SQX_NonConformance__r = mockNC
                                            ));

        relatedRecords.add(gen.getSObject());

        // for finding
        SQX_Finding__c mockFinding = (SQX_Finding__c) new SQX_SObject_Generator(new SQX_Finding__c(Title__c = 'Mock Resp Title'))
                                                                .setValueForField('Name', 'Mock Finding')
                                                                .getSObject();

        gen = new SQX_SObject_Generator(new SQX_Finding_Response__c(
                                                SQX_Finding__c = mockFinding.Id,
                                                SQX_Finding__r = mockFinding
                                            ));

        relatedRecords.add(gen.getSObject());


        for(SObject relatedRecord : relatedRecords)
        {
            SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended extd 
                            = new SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended();

            ProcessInstance pi = (ProcessInstance) new SQX_SObject_Generator(new ProcessInstance())
                                                        .setValueForField('TargetObjectId', relatedRecord.Id)
                                                        .setValueForField('TargetObject.Name', 'Apprvl Item')
                                                        .getSObject();

            extd.workItem = (ProcessInstanceWorkItem) new SQX_SObject_Generator(new ProcessInstanceWorkItem())
                                                                .setValueForField('ProcessInstance', pi)
                                                                .setValueForField('CreatedDate', Date.today())
                                                                .getSObject();

            extd.relatedRecord = relatedRecord;

            items.add(extd);
        }

        return items;
    }



    testmethod static void givenApprovalItemsFromVariousModles_WhenHomePageItemsAreRequested_ThenCorrespondingItemsToApproveItemsAreReturned()
    {
        SQX_Homepage_Source_ItemsToApprove itemToApproveSrc = new SQX_Homepage_Source_ItemsToApprove();
        for(SQX_Homepage_Source_ItemsToApprove.ProcessInstanceWorkItemExtended approvalItem : getMockApprovalItems())
        {
            SQX_Homepage_Item itemToApprove = itemToApproveSrc.getHomePageItemFromObject(approvalItem);

            System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE, itemToApprove.actionType);

            System.assertEquals(Date.today(), Date.valueOf(approvalItem.workItem.CreatedDate));

            if(approvalItem.relatedRecord.getSObjectType() == SQX_Finding_Response__c.SObjectType || approvalItem.relatedRecord.getSObjectType() == SQX_Audit_Response__c.SObjectType) {
                // expecting exactly one items -  'Approve/Reject' action
                System.assertEquals(1, itemToApprove.actions.size());
            } else {
                // expecting exactly two items -  'Approve/Reject' action and 'Reassign' action
                System.assertEquals(2, itemToApprove.actions.size());
            }

            for(SQX_Homepage_Item_Action act : itemToApprove.actions)
            {
                System.assert(act.Name == SQX_Homepage_Constants.ACTION_REASSIGN || act.Name == SQX_Homepage_Constants.ACTION_APPROVE_REJECT, 'Unexpected action item - ' + act);
            }

            System.assertNotEquals(null, itemToApprove.actions.get(0).actionUrl);

            Id targetObjectId = Id.valueOf(itemToApprove.itemId);

            SObjectType targetType = targetObjectId.getSObjectType();

            System.assert(targetedModules.contains(targetType), 'Unexpected target module ' + targetType);

            // expected title in feed text
            System.assert(itemToApprove.feedText.contains(':'), 'Expected title section in feed text but got ' + itemToApprove.feedText + ' for module ' + targetType );

            if(targetType == SQX_Finding_Response__c.SObjectType)
            {
                Set<String> validModules = new Set<String> { SQX_Homepage_Constants.MODULE_TYPE_CAPA,
                                                            SQX_Homepage_Constants.MODULE_TYPE_NC,
                                                            SQX_Homepage_Constants.MODULE_TYPE_FINDING };
                System.assert(validModules.contains(itemToApprove.moduleType), 'Invalid module for response type : ' + itemToApprove.moduleType);

                // expected record title in feed text
                System.assertEquals('Mock Resp Title', itemToApprove.feedText.split(':')[1].trim());
            }
        }

        // simply for coverage
        SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { itemToApproveSrc };

        Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));
        List<Object> errors = (List<Object>) homepageItems.get('errors');

        // Assert : Expecting no errors
        System.assertEquals(0, errors.size());
    }
}