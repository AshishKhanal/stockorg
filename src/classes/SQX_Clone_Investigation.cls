/**
 * invocable class to clone investigation
 */
global with sharing class SQX_Clone_Investigation {
    
    /**
     * method to clone investigation
     * @param investigations list of investigations to be cloned 
     */
    @InvocableMethod(Label='Clone Investigation')
    global static void cloneInvestigation(List<SQX_Investigation__c> investigations){
        if(investigations.size() > 0){
            SQX_Investigation__c investigation = investigations.get(0);
            SQX_Investigation inv = new SQX_Investigation(investigation);
            inv.cloneInvestigationAndRelatedList();
        }
    }
}