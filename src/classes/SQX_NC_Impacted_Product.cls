/**
* Impacted product update class performs the following
* a) update disposition if impacted product has changed [SQX-783] [Migrated from SQX_Impacted_Product to SQX_NC_Impacted_Product]
* @author Pradhanta Bhandari
*/
public with sharing class SQX_NC_Impacted_Product {

    /**
    * Bulkified method called from trigger to perform various operation.
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newImpactedProducts equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_NC_Impacted_Product__c> newImpactedProducts, Map<Id,SQX_NC_Impacted_Product__c> oldMap){
            super(newImpactedProducts, oldMap);
        }


        /**
        * This method updates the disposition with new impacted part and lot number if impacted product is updated,
        * for all dispositions that have already been saved into SF system.
        *
        */
        public Bulkified updateDispositionsIfImpactedProductIsUpdated(){
            Rule partOrLotHasChangedRule = new Rule();
            partOrLotHasChangedRule.addRule(Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c, RuleOperator.HasChanged);
            partOrLotHasChangedRule.addRule(Schema.SQX_NC_Impacted_Product__c.Lot_Number__c, RuleOperator.HasChanged);

            partOrLotHasChangedRule.operator = RuleCombinationOperator.OpOr;

            this.resetView().applyFilter(partOrLotHasChangedRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
            if(partOrLotHasChangedRule.evaluationResult.size() > 0){
                Map<Id, SQX_NC_Impacted_Product__c> impactedProducts = new Map<Id, SQX_NC_Impacted_Product__c>((List<SQX_NC_Impacted_Product__c>) partOrLotHasChangedRule.evaluationResult);


                List<SQX_Disposition__c> dispositionsToUpdate = [SELECT Id, SQX_NC_Impacted_Product__c FROM SQX_Disposition__c
                                                        WHERE SQX_NC_Impacted_Product__c IN : impactedProducts.keySet()
                                                        FOR UPDATE];

                for(SQX_Disposition__c disposition : dispositionsToUpdate){

                    //for each disposition find get the updated impacted product and update the part and lot.
                    SQX_NC_Impacted_Product__c product = impactedProducts.get(disposition.SQX_NC_Impacted_Product__c);
                    disposition.SQX_Part__c = product.SQX_Impacted_Part__c;
                    disposition.Lot_Number__c = product.Lot_Number__c;

                }

                /**
                * WITHOUT Sharing has been used:
                * the person updating the impacted product will not necessarily have sharing rights on all disposition
                * this is because disposition is created by an individual different from the impacted product manager.
                */
                new SQX_DB().withoutSharing().op_update(dispositionsToUpdate, new List<Schema.SObjectField> {
                        Schema.SQX_Disposition__c.SQX_Part__c,
                        Schema.SQX_Disposition__c.Lot_Number__c
                    });
            }

            return this;
        }
        
        /**
         * prevents defect defective quantity greater than impacted product lot quantity
         */
        public Bulkified preventDefectiveQtyGreaterThanLotQty(){
            Rule lotQtyChanged = new Rule();
            
            lotQtyChanged.addRule(Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c, RuleOperator.HasChanged);
            
            this.resetView()
                .applyFilter(lotQtyChanged, RuleCheckMethod.OnCreateAndEveryEdit);
            
            List<SQX_NC_Impacted_Product__c> impactedProductList = (List<SQX_NC_Impacted_Product__c>) lotQtyChanged.evaluationResult;
            
            if(impactedProductList.size() > 0){
                Set<Id> ncIds = new Set<Id>();
                Map<String, SObject> impactedProductMap = new Map<String, SObject>();
                for(SQX_NC_Impacted_Product__c impactedProduct : impactedProductList){
                    String ncPartLotCombinationforIP = '' + impactedProduct.SQX_Nonconformance__c + impactedProduct.SQX_Impacted_Part__c + impactedProduct.Lot_Number__c;
                    ncIds.add(impactedProduct.SQX_Nonconformance__c);
                    impactedProductMap.put(ncPartLotCombinationforIP, impactedProduct);
                    
                }
                SQX_NC_Defect.validateImpactedPartAndDefectQuantity(ncIds, impactedProductMap);
            }
            return this;
        }
    }
}