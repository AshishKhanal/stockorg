@IsTest
public class SQX_Test_Impacted_Customer{
    public testmethod static void givenImpactedCustomerExists_ItCantBeDuplicated(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            //setup
            SQX_Test_Finding findingContainer = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
            .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
            .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
            .setStatus(SQX_Finding.STATUS_DRAFT)
            .save();
            
            Account customerAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_CUSTOMER);
            
            SQX_Impacted_Customer__c impactedCustomer = new SQX_Impacted_Customer__c(SQX_Impacted_Account__c = customerAccount.Id,
                                                                            SQX_Finding__c = findingContainer.finding.Id);
            
            insert impactedCustomer;
            
            //lets try inserting an impacted customer again
            impactedCustomer.Id = null;
            
            Database.SaveResult result = Database.insert(impactedCustomer, false);
            System.assert(result.isSuccess() == false, 'Expected duplicate to be prevented');
        }
    }
}