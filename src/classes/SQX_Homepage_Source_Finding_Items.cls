/**
* This class acts as an item source for Finding related items
* @author  Sanjay Maharjan
* @date    16-04-2018
*/
public with sharing class SQX_Homepage_Source_Finding_Items extends SQX_Homepage_ItemSource {

    private final String module = SQX_Homepage_Constants.MODULE_TYPE_FINDING;

    private final User loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();

    String findingPage;    // getPageName of finding main page
    /**
    *   Constructor method
    */
    public SQX_Homepage_Source_Finding_Items(){
        super();
        findingPage=SQX_Utilities.getPageName('', 'SQX_Finding');
    }

    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Finding__c.SObjectType => new List<SObjectField> 
            {
                SQX_Finding__c.Name,
                SQX_Finding__c.Title__c,
                SQX_Finding__c.Status__c,
                SQX_Finding__c.CreatedDate,
                SQX_Finding__c.OwnerId
            },
            SQX_Investigation__c.SObjectType => new List<SObjectField>
            {
                SQX_Investigation__c.Name,
                SQX_Investigation__c.CreatedDate,
                SQX_Investigation__c.SQX_Finding__c ,
                SQX_Investigation__c.Status__c
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }


    /**
    *   returns a list of homepage items for the current user related to Finding module
    */
    protected override List<SObject> getUserRecords(){

        List<SObject> allItems = new List<SObject>();
        List <String> findingStatuses = new list <String>
        {
            SQX_Finding.STATUS_DRAFT,
            SQX_Finding.STATUS_OPEN,
            SQX_Finding.STATUS_COMPLETE
        };

        List <SQX_Finding__c> findingItems = [SELECT  Id,
                                                        Name,
                                                        Title__c,
                                                        Status__c,
                                                        CreatedDate
                                                        FROM SQX_Finding__c
                                                        WHERE
                                                        Status__c IN: findingStatuses
                                                        AND Stage__c !=: SQX_Finding.STAGE_WAITING_FOR_CAPA
                                                        AND OwnerId IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                                    ];

        List<SQX_Investigation__c> investigationAction=[SELECT Id,Name,
                                                            CreatedDate , 
                                                            SQX_Finding__c ,
                                                            SQX_Finding__r.Name,
                                                            SQX_Finding__r.Title__c,
                                                            Approval_Status__c ,
                                                            Status__c 
                                                            FROM SQX_Investigation__c
                                                            WHERE Approval_Status__c=:SQX_Investigation.APPROVAL_STATUS_PENDING
                                                            AND Id NOT IN(SELECT SQX_Investigation__c 
                                                                            FROM SQX_Response_Inclusion__c 
                                                                            WHERE SQX_Investigation__c !=null
                                                                          )
                                                            AND SQX_Finding__r.SQX_Investigation_Approver__c=: loggedInUser.Id
                                                                   
                                                        ];
        allItems.addAll((List<SObject>) findingItems);
        allItems.addAll((List<SObject>) investigationAction);
        return allItems;
    }




    /**
    *   Method returns a SQX_Homepage_Item type item from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item){

        SQX_Homepage_Item findingItem;
        if(item instanceOf SQX_Finding__c){
            findingItem =  getFindingItem((SQX_Finding__c) item);
        }else if(item instanceOf SQX_Investigation__c ){
            findingItem =  getInvestigationItem((SQX_Investigation__c) item);
        }
        return findingItem;
    }

    /**
    *   Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
    *   @param item - Record to be converted to home page item
    */
    private SQX_Homepage_Item getFindingItem(SQX_Finding__c item) {

        SQX_Homepage_Item findingItem = new SQX_Homepage_Item();
        
        findingItem.itemId = item.Id;

        findingItem.createdDate = Date.valueOf(item.CreatedDate);

        findingItem.moduleType = this.module;

        findingItem.creator = this.loggedInUser;

        if (item.Status__c == SQX_Finding.STATUS_DRAFT) {

            findingItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;
            findingItem.actions = getActionsForFinding(item.Id, SQX_Homepage_Constants.ACTION_VIEW, SQX_Homepage_Constants.ICON_UTILITY_VIEW);
            findingItem.feedText = getFindingFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS, item);

        } else if (item.Status__c == SQX_Finding.STATUS_OPEN) {

            findingItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS;
            findingItem.actions = getActionsForFinding(item.Id, SQX_Homepage_Constants.ACTION_VIEW, SQX_Homepage_Constants.ICON_UTILITY_VIEW);
            findingItem.feedText = getFindingFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS, item);

        } else if (item.Status__c == SQX_Finding.STATUS_COMPLETE) {

            findingItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_ACTION_TO_CLOSE;
            findingItem.actions = getActionsForFinding(item.Id, SQX_Homepage_Constants.ACTION_CLOSE, SQX_Homepage_Constants.ICON_UTILITY_CLOSE);
            findingItem.feedText = getFindingFeedText(SQX_Homepage_Constants.FEED_TEMPLATE_COMPLETED_ITEMS, item);

        }

        return findingItem;

    }

    /**
    *   Returns the view action type for Finding item
    *   @param itemId - Id of the record for which action is to be set
    *   @param actionName - Name of the action to be set
    *   @param actionIcon - Icon of the action to be set
    */
    private List <SQX_Homepage_Item_Action> getActionsForFinding(String itemId, String actionName, String actionIcon) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = actionName;

        action.actionIcon = actionIcon;

        PageReference pr = new PageReference('/' + itemId);
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.supportsBulk = false;

        return new List <SQX_Homepage_Item_Action> {
            action
        };
    }

    /**
    *   Method replaces the given Finding feed template with Finding's info and returns the string
    */
    private String getFindingFeedText(String template, SQX_Finding__c item)
    {
        return String.format(template, new String[] {
            item.Name,
            String.isBlank(item.Title__c) ? '' : item.Title__c
        });
    }
    
    /**
    *   Method returns a corresponding homepage item for the given finding's record item for Investigation
    *   @param item - SQX_Investigation__c record
    */
    private  SQX_Homepage_Item getInvestigationItem(SQX_Investigation__c item){

        // flag to check if item is draft of finding investigation
        Boolean isApprovalInvestigation = item.Status__c == SQX_Investigation.STATUS_IN_APPROVAL;
        SQX_Homepage_Item InvestigationItem = new SQX_Homepage_Item();
        InvestigationItem.itemId = item.Id;
        // set action type
        InvestigationItem.actionType =SQX_Homepage_Constants.ACTION_TYPE_ITEMS_TO_APPROVE;

        InvestigationItem.createdDate = Date.valueOf(item.CreatedDate);
        // set module type
        InvestigationItem.moduleType =this.module;

        // set actions for the current item
        String recordId =item.SQX_Finding__c;
        InvestigationItem.actions = getInvestigationItemActions(recordId,item);
        // set feed text for the current item
        InvestigationItem.feedText = getInvestigationFeedText(item);
        // set user details of the user related to this item
        InvestigationItem.creator = loggedInUser;
        return InvestigationItem;
    }

    /**
    *   Returns a list of actions for in Approval of finding investigation
    *   @param id - id of the record for which action is to be set
    *   @param item - SQX_Investigation__c record
    */
    private List<SQX_Homepage_Item_Action> getInvestigationItemActions(String id,SQX_Investigation__c item){
        SQX_Homepage_Item_Action investigationAction = new SQX_Homepage_Item_Action();
        investigationAction.name = SQX_Homepage_Constants.ACTION_APPROVE_REJECT;
        PageReference pr = new PageReference(findingPage);
        pr.getParameters().put('Id',id);
        pr.getParameters().put('initialTab','responseHistoryTab');
        investigationAction.actionUrl = getHomePageItemActionUrl(pr);
        investigationAction.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_APPROVE_REJECT;
        investigationAction.supportsBulk = false;
        return new List<SQX_Homepage_Item_Action> { investigationAction };
     }

    /**
    *   Method returns the feed text for  Investigation items
    *   @param item -  Investigation record
    */
    private String getInvestigationFeedText(SQX_Investigation__c item){
        String template =SQX_Homepage_Constants.FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE;
        return String.format(template, new String[] { 
                            item.Name,(String.isBlank(item.SQX_Finding__r.Title__c) ? '' : item.SQX_Finding__r.Title__c)
                        });
    }
}