/**
* This test case for to update additional info alert date and due date when user checked additional info.
*/
@isTest
public class SQX_Test_8278_Reporting_Alert_Date {
    @testSetup
    public static void commonSetup() {
        // Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    /**
     * Given : complaint
     * When : when additional info is checked
     * Then : update additional alert date
     */
    static testmethod void givenComplaintWithRegulatoryReport_WhenUpdateAdditinalInfoAsTrue_ThenUpdateAdditionalAlertDateAndDueDate() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser) {
            //Act: Insert complaint with additional info checked
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.complaint.Aware_Date__c = Date.today();
            complaint.complaint.Additional_Info__c = true;
            complaint.save();

            // Assert: Ensure that the Additional Info Alert Date is set to today
            System.assertEquals(Date.today(), [SELECT Additional_Info_Alert_Date__c FROM SQX_Complaint__c WHERE Id = : complaint.complaint.Id].Additional_Info_Alert_Date__c);

        }
    }

    /**
     * Given a complaint with regulatory report owner specified and additional info aware date specified
     * When regulatory report is created
     * Then regulatory report assignee is regulatory report owner and due date is based on additional info
     */
    static testmethod void givenComplaintWithRegReport_WhenRegReportIsCreated_ThenAssignedAndDateIsSetCorrectly() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(adminUser) {
            // ARRANGE : Create a CQ Task
            SQX_Test_Task task = new SQX_Test_Task(SQX_Task.RTYPE_COMPLAINT, SQX_Task.TYPE_DECISION_TREE).save();
            task.task.SQX_User__c = standardUser.Id;
            task.save();

            // ARRANGE : Create Task Question
            SQX_Task_Question__c question = task.addQuestion();

            // ARRANGE : Create Answer Option
            SQX_Answer_Option__c answerOption = task.addAnswer(question, null, false);

            // ARRANGE : Create Answer Option Attributes with reportable flag checked and all required fields.
            SQX_Answer_Option_Attribute__c answerOptionAttribute = new SQX_Answer_Option_Attribute__c(SQX_Answer_Option__c = answerOption.Id,
                                                                                                      Attribute_Type__c = 'Sample Attribute Type',
                                                                                                      Value__c = 'Test Value',
                                                                                                      Due_In_Days__c = 10,
                                                                                                      Reg_Body__c = SQX_REgulatory_Report.REGULATORY_BODY_FDA,
                                                                                                      Report_Name__c = SQX_REgulatory_Report.REPORT_NAME_30_DAY_MDR,
                                                                                                      Reportable__c = true);
            Database.insert(answerOptionAttribute, false);

            //Arrange: Create complaint and decision tree for regulatory report
            //         Create second complaint owned by another user
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.complaint.Aware_Date__c = Date.today();
            complaint.complaint.SQX_Regulatory_Owner__c = standardUser.Id;
            complaint.complaint.Additional_Info_Alert_Date__c = Date.today().addDays(-2);
            complaint.save();

            SQX_Test_Complaint complaint2 = new SQX_Test_Complaint(standardUser).save();
            complaint2.complaint.OwnerId = standardUser.Id;
            complaint2.complaint.Aware_Date__c = Date.today();
            complaint2.save();

            SQX_Decision_Tree__c decisionTree = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint.complaint.Id,
                                                                         SQX_Task__c = task.task.Id);

            SQX_Decision_Tree__c decisionTree2 = new SQX_Decision_Tree__c(SQX_Complaint__c = complaint2.complaint.Id,
                                                                         SQX_Task__c = task.task.Id);
            insert new SQX_Decision_Tree__c[] {decisionTree, decisionTree2};

            SQX_DT_Answer__c decisionTreeAnswer = new SQX_DT_Answer__c(SQX_Decision_Tree__c = decisionTree.Id,
                                                                       SQX_Answer_Option__c = answerOption.Id);

            SQX_DT_Answer__c decisionTreeAnswer2 = new SQX_DT_Answer__c(SQX_Decision_Tree__c = decisionTree2.Id,
                                                                       SQX_Answer_Option__c = answerOption.Id);
            insert new SQX_DT_Answer__c [] { decisionTreeAnswer, decisionTreeAnswer2} ;

            // Act: Execute decision tree tasks
            SQX_Create_Regulatory_Report.createRegulatoryReportBasedOnAnswerOptionAttribute(new List<SQX_Decision_Tree__c>{decisionTree, decisionTree2});

            // Assert: Ensure that the due date is set from additional info alert date for first and assignee is regulatory owner
            SQX_Regulatory_Report__c reportWithDT = [SELECT Id, Due_Date__c, SQX_User__c FROM SQX_Regulatory_Report__c WHERE SQX_Decision_Tree__c =: decisionTree.Id];

            System.assertEquals(complaint.complaint.Additional_Info_Alert_Date__c.addDays((Integer)answerOptionAttribute.Due_In_Days__c), reportWithDT.Due_Date__c);
            System.assertEquals(standardUser.Id, reportWithDT.SQX_User__c);

            // Assert: Ensure that the user is set to decision tree executor for the second dt task.
            reportWithDT = [SELECT Id, Due_Date__c, SQX_User__c FROM SQX_Regulatory_Report__c WHERE SQX_Decision_Tree__c =: decisionTree2.Id];
            System.assertEquals(adminUser.Id, reportWithDT.SQX_User__c);

        }
    }
}