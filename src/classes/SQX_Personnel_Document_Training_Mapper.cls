/**
* SQX_Personnel_Document_Training__c object mapper class that generates / maps personnel document training list to insert and/or update
* by matching job functions of active personnel job functions and requirements.
* Note: This class does not saves the personnel document training list into database.
* 
* WITHOUT SHARING used
* --------------------
* user releasing a controlled document may not have enough permissions on all personnel records and its child records
*/
public without sharing class SQX_Personnel_Document_Training_Mapper {
    
    /**
    * Mapper modes for traning generation
    */
    public enum MapperModes {
        CreateRefresher,
        JobFunctionActivated,
        JobFunctionDeactivated
    }
    
    /**
    * Mapper mode for current context
    */
    MapperModes mapperMode;
    
    /**
    * flag that is set when document trainings to insert and update have been generated from mapped job functions
    */
    Boolean isDocumentTrainingsMapGenerated;
    
    /**
    * returns <code>true</code> when current mode is Create Refresher
    */
    Boolean isCreateRefresherMode {
        get { return mapperMode == MapperModes.CreateRefresher; }
    }
    
    /**
    * returns <code>true</code> when current mode is Job Function Activated
    */
    Boolean isJobFunctionActivatedMode {
        get { return mapperMode == MapperModes.JobFunctionActivated; }
    }
    
    /**
    * returns <code>true</code> when current mode is Job Function Deactivated
    */
    Boolean isJobFunctionDeactivatedMode {
        get { return mapperMode == MapperModes.JobFunctionDeactivated; }
    }
    
    /**
    * overall or stricter training requirement evaluated for a personnel and a document from mapped job functions
    * format: Map<{SQX_Personnel__c.Id + SQX_Controlled_Document__c.Id + SQX_Assessment__c.Id}, SQX_OverallTrainingRequirement>
    */
    Map<String, SQX_OverallTrainingRequirement> overallRequirementMap;
    
    /**
    * document trainings generated from mapped job functions with proper field values, which needs to be inserted or updated
    * format: Map<{SQX_Personnel__c.Id + SQX_Controlled_Document__c.Id + SQX_Assessment__c.Id}, SQX_Personnel_Document_Training__c>
    */
    Map<String, SQX_Personnel_Document_Training__c> documentTrainingsToInsertMap,
                                                    documentTrainingsToUpdateMap;
    
    /**
    * processed pdjfs that uses existing document training due to credit module
    */
    Set<SQX_Personnel_Document_Job_Function__c> processedPdjfsToUpdate;
    
    /**
    * Level of competency mapper to compare competencies
    */
    SQX_Requirement.SQX_LevelOfCompetencyMapper LevelOfCompetencyMapper = new SQX_Requirement.SQX_LevelOfCompetencyMapper();
    
    /**
    * initializes variables
    */
    private void initialize() {
        isDocumentTrainingsMapGenerated = false;
        
        documentTrainingsToInsertMap = new Map<String, SQX_Personnel_Document_Training__c>();
        documentTrainingsToUpdateMap = new Map<String, SQX_Personnel_Document_Training__c>();
        overallRequirementMap = new Map<String, SQX_OverallTrainingRequirement>();
        processedPdjfsToUpdate = new Set<SQX_Personnel_Document_Job_Function__c>();
    }
    
    /**
    * generates document trainings for active PDJFs
    * @param pdjfs  not archived PDJFs to generate/reevaluate document trainings
    */
    void generatePersonnelDocumentTraining(List<SQX_Personnel_Document_Job_Function__c> pdjfs) {
        if (pdjfs.size() > 0) {
            List<SQX_Personnel_Document_Job_Function__c> validPdjfs = new List<SQX_Personnel_Document_Job_Function__c>();
            Set<Id> allPsnIds = new Set<Id>();
            Set<Id> allAssessmentIds = new Set<Id>();
            // pending training ids of deactivated pdjfs, which needs to reevaluate
            Set<Id> allPendingPdtIds = new Set<Id>();
            Set<String> allDocNumbers = new Set<String>();
            Map<Id, SQX_Controlled_Document__c> allDocMap = new Map<Id, SQX_Controlled_Document__c>();
            // Map<{PsnId + DocId + AstId}, Map<{training status}, training list>>
            Map<String, Map<String, List<SQX_Personnel_Document_Training__c>>> existingPdtMap = new Map<String, Map<String, List<SQX_Personnel_Document_Training__c>>>();
            // Map<{PsnId + AstId}, personnel assessment id>
            Map<String, Id> allCompletedPersonnelAssessmentMap = new Map<String, Id>();
            // Map<{PsnId + AstId}, personnel assessment id>
            Map<String, Id> docCompletedPersonnelAssessmentMap = new Map<String, Id>();
            // Map<{PsnId + DocId}, personnel assessment id>
            Set<String> docsWithTrainerSignOff = new Set<String>();
            
            for (SQX_Personnel_Document_Job_Function__c pdjf : pdjfs) {
                // only process PDJFs for valid controlled document statuses
                if (SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING.contains(pdjf.Controlled_Document_Status__c)) {
                    validPdjfs.add(pdjf);
                    
                    String psnDocAstKey = '' + pdjf.Personnel_Id__c + pdjf.Controlled_Document_Id__c + pdjf.SQX_Assessment__c;
                    String psnAstKey = '' + pdjf.Personnel_Id__c + pdjf.SQX_Assessment__c;
                    String docNumberInUpperCase = pdjf.Controlled_Document_Number__c.toUpperCase();
                    
                    allPsnIds.add(pdjf.Personnel_Id__c);
                    allDocNumbers.add(docNumberInUpperCase);
                    allDocMap.put(pdjf.Controlled_Document_Id__c, null);
                    allAssessmentIds.add(pdjf.SQX_Assessment__c);
                    
                    if (isJobFunctionDeactivatedMode) {
                        // list pending trainings to reevaluate
                        allPendingPdtIds.add(pdjf.SQX_Personnel_Document_Training__c);
                    }
                }
            }
            
            if (validPdjfs.size() > 0) {
                // read controlled documents
                allDocMap = new Map<Id, SQX_Controlled_Document__c>([
                    SELECT Id,
                        Name,
                        Document_Number__c,
                        Document_Status__c,
                        Date_Issued__c,
                        Effective_Date__c,
                        Duration__c,
                        RecordTypeId
                    FROM SQX_Controlled_Document__c 
                    WHERE Id IN :allDocMap.keySet()
                ]);
                
                if (isJobFunctionDeactivatedMode || isCreateRefresherMode) {
                    List<SQX_Personnel_Document_Training__c> allExistingTrainings = new List<SQX_Personnel_Document_Training__c>();
                    
                    if (isJobFunctionDeactivatedMode) {
                        // read existing pending trainings of deactivated job functions to reevaluate
                        allExistingTrainings = [SELECT Id,
                                                    SQX_Personnel__c,
                                                    SQX_Controlled_Document__c,
                                                    SQX_Assessment__c,
                                                    SQX_Personnel_Assessment__c,
                                                    SQX_Controlled_Document__r.Document_Number__c,
                                                    Level_Of_Competency__c,
                                                    Optional__c,
                                                    Status__c,
                                                    Completion_Date__c,
                                                    Is_Retrain__c,
                                                    Is_Refresher__c,
                                                    Is_Completed_By_System__c,
                                                    Overall_Competency__c,
                                                    Overall_Optional__c
                                                FROM SQX_Personnel_Document_Training__c
                                                WHERE Id IN :allPendingPdtIds];
                    }
                    else if (isCreateRefresherMode) {
                        // read pending trainings
                        allExistingTrainings = [SELECT Id,
                                                    SQX_Personnel__c,
                                                    SQX_Controlled_Document__c,
                                                    SQX_Assessment__c,
                                                    SQX_Personnel_Assessment__c,
                                                    SQX_Controlled_Document__r.Document_Number__c,
                                                    Level_Of_Competency__c,
                                                    Optional__c,
                                                    Status__c,
                                                    Completion_Date__c,
                                                    Is_Retrain__c,
                                                    Is_Refresher__c,
                                                    Is_Completed_By_System__c,
                                                    Overall_Competency__c,
                                                    Overall_Optional__c
                                                FROM SQX_Personnel_Document_Training__c
                                                WHERE SQX_Personnel__c IN :allPsnIds
                                                    AND SQX_Controlled_Document__c IN :allDocMap.keySet()
                                                    AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
                    }
                    
                    // map existing trainings
                    for (SQX_Personnel_Document_Training__c pdt : allExistingTrainings) {
                        String psnDocAstKey = '' + pdt.SQX_Personnel__c + pdt.SQX_Controlled_Document__c + pdt.SQX_Assessment__c;
                        
                        if (!existingPdtMap.containsKey(psnDocAstKey)) {
                            existingPdtMap.put(psnDocAstKey, new Map<String, List<SQX_Personnel_Document_Training__c>>());
                        }
                        if (!existingPdtMap.get(psnDocAstKey).containsKey(pdt.Status__c)) {
                            existingPdtMap.get(psnDocAstKey).put(pdt.Status__c, new List<SQX_Personnel_Document_Training__c>());
                        }
                        existingPdtMap.get(psnDocAstKey).get(pdt.Status__c).add(pdt);
                    }
                }
                else {
                    // read existing trainings and map existing trainings
                    for (SQX_Personnel_Document_Training__c pdt : [ SELECT Id,
                                                                        SQX_Personnel__c,
                                                                        SQX_Controlled_Document__c,
                                                                        SQX_Assessment__c,
                                                                        SQX_Personnel_Assessment__c,
                                                                        SQX_Controlled_Document__r.Document_Number__c,
                                                                        Level_Of_Competency__c,
                                                                        Optional__c,
                                                                        Status__c,
                                                                        Completion_Date__c,
                                                                        Is_Retrain__c,
                                                                        Is_Refresher__c,
                                                                        Is_Completed_By_System__c,
                                                                        Overall_Competency__c,
                                                                        Overall_Optional__c,
                                                                        Trainer_Approval_Needed__c,
                                                                        Trainer_SignOff_Date__c
                                                                    FROM SQX_Personnel_Document_Training__c
                                                                    WHERE SQX_Personnel__c IN :allPsnIds
                                                                        AND SQX_Controlled_Document__c IN :allDocMap.keySet()
                                                                    ORDER BY SQX_Personnel__c,
                                                                        SQX_Controlled_Document__c,
                                                                        SQX_Assessment__c,
                                                                        Status__c,
                                                                        Level_Of_Competency__c DESC,
                                                                        Completion_Date__c DESC,
                                                                        Trainer_SignOff_Date__c DESC ]) {
                        String psnDocKey = '' + pdt.SQX_Personnel__c + pdt.SQX_Controlled_Document__c;
                        String psnDocAstKey = psnDocKey + pdt.SQX_Assessment__c;
                        
                        // we do not use obsolete training
                        if (pdt.Status__c != SQX_Personnel_Document_Training.STATUS_OBSOLETE) {
                            if (!existingPdtMap.containsKey(psnDocAstKey)) {
                                existingPdtMap.put(psnDocAstKey, new Map<String, List<SQX_Personnel_Document_Training__c>>());
                            }
                            if (!existingPdtMap.get(psnDocAstKey).containsKey(pdt.Status__c)) {
                                existingPdtMap.get(psnDocAstKey).put(pdt.Status__c, new List<SQX_Personnel_Document_Training__c>());
                            }
                            existingPdtMap.get(psnDocAstKey).get(pdt.Status__c).add(pdt);
                            
                            if (pdt.Trainer_Approval_Needed__c == true && pdt.Status__c == SQX_Personnel_Document_Training.STATUS_COMPLETE) {
                                docsWithTrainerSignOff.add(psnDocKey);
                            }
                        }
                    }
                    
                    // training statuses use for credit module for activated job functions
                    List<String> trainingStatusesToCredit = new List<String>{
                        SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                        SQX_Personnel_Document_Training.STATUS_COMPLETE
                    };
                    
                    // link activated pdjfs to existing signed off trainings
                    for (SQX_Personnel_Document_Job_Function__c pdjf : validPdjfs) {
                        String psnDocAstKey = '' + pdjf.Personnel_Id__c + pdjf.Controlled_Document_Id__c + pdjf.SQX_Assessment__c;
                        
                        // personnels having pending training for the assessment should not be credited and should linked to pending training
                        if (pdjf.Training_Type__c != SQX_Personnel_Document_Job_Function.TRAINING_TYPE_RETRAIN
                            && existingPdtMap.containsKey(psnDocAstKey)
                            && !existingPdtMap.get(psnDocAstKey).containsKey(SQX_Personnel_Document_Training.STATUS_PENDING)) {
                            SQX_Personnel_Document_Training__c pdtToUse = null;
                            
                            for (String trainingStatus : trainingStatusesToCredit) {
                                if (pdtToUse == null && existingPdtMap.get(psnDocAstKey).containsKey(trainingStatus)) {
                                    for (SQX_Personnel_Document_Training__c pdt : existingPdtMap.get(psnDocAstKey).get(trainingStatus)) {
                                        if (LevelOfCompetencyMapper.compare(pdt.Level_Of_Competency__c, pdjf.Level_Of_Competency__c) >= 0) {
                                            pdtToUse = pdt;
                                            break;
                                        }
                                    }
                                }
                            }
                            
                            if (pdtToUse != null) {
                                // found existing training that can be used for crediting new job function
                                pdjf.SQX_Personnel_Document_Training__c = pdtToUse.Id;
                                pdjf.Training_Status__c = pdtToUse.Status__c;
                                
                                processedPdjfsToUpdate.add(pdjf);
                            }
                        }
                    }
                    
                    if (processedPdjfsToUpdate.size() > 0) {
                        if (processedPdjfsToUpdate.size() == validPdjfs.size()) {
                            // all pdjfs have been processed so no need to create document trainings
                            validPdjfs.clear();
                        }
                        else {
                            List<SQX_Personnel_Document_Job_Function__c> allPdjfs = validPdjfs;
                            validPdjfs = new List<SQX_Personnel_Document_Job_Function__c>();
                            
                            for (SQX_Personnel_Document_Job_Function__c pdjf : allPdjfs) {
                                if (!processedPdjfsToUpdate.contains(pdjf)) {
                                    validPdjfs.add(pdjf);
                                }
                            }
                        }
                    }
                    
                    if (validPdjfs.size() > 0) {
                        // read completed assessments in order to credit initial/revision pdjfs
                        for (SQX_Personnel_Assessment__c pa : [ SELECT Id,
                                                                    SQX_Personnel__c,
                                                                    SQX_Assessment__c,
                                                                    (
                                                                        SELECT Id,
                                                                            SQX_Controlled_Document__c
                                                                        FROM SQX_Personnel_Document_Trainings__r
                                                                        WHERE SQX_Controlled_Document__c IN :allDocMap.keySet()
                                                                    )
                                                                FROM SQX_Personnel_Assessment__c
                                                                WHERE SQX_Personnel__c IN :allPsnIds
                                                                    AND SQX_Assessment__c IN :allAssessmentIds
                                                                    AND Is_Successfully_Completed__c = true
                                                                ORDER BY SQX_Personnel__c,
                                                                    SQX_Assessment__c,
                                                                    SQX_Last_Assessment_Attempt__r.Submitted_Date__c DESC ]) {
                            String psnAstKey = '' + pa.SQX_Personnel__c + pa.SQX_Assessment__c;
                            
                            // list all completed assessment by a personnel to credit assessment to take
                            if (!allCompletedPersonnelAssessmentMap.containsKey(psnAstKey)) {
                                allCompletedPersonnelAssessmentMap.put(psnAstKey, pa.Id);
                            }
                            
                            // list completed assessment by a personnel per document to credit user sign off
                            if (pa.SQX_Personnel_Document_Trainings__r.size() > 0) {
                                for (SQX_Personnel_Document_Training__c pdt : pa.SQX_Personnel_Document_Trainings__r) {
                                    String psnDocAstKey = '' + pa.SQX_Personnel__c + pdt.SQX_Controlled_Document__c + pa.SQX_Assessment__c;
                                    
                                    if (!docCompletedPersonnelAssessmentMap.containsKey(psnDocAstKey)) {
                                        docCompletedPersonnelAssessmentMap.put(psnDocAstKey, pa.Id);
                                    }
                                }
                            }
                        }
                    }
                }
                
                // build overall requirements
                for (SQX_Personnel_Document_Job_Function__c pdjf : validPdjfs) {
                    String psnDocAstKey = '' + pdjf.Personnel_Id__c + pdjf.Controlled_Document_Id__c + pdjf.SQX_Assessment__c;
                    SQX_OverallTrainingRequirement overallReq = overallRequirementMap.get(psnDocAstKey);
                    
                    if (overallReq == null) {
                        // instantiate using values
                        overallReq = new SQX_OverallTrainingRequirement(pdjf.Personnel_Id__c, pdjf.Controlled_Document_Id__c, pdjf.SQX_Assessment__c, LevelOfCompetencyMapper);
                        
                        if (overallReq.PersonnelDocumentTraining == null) {
                            SQX_Personnel_Document_Training__c pendingDT = null;
                            
                            if (existingPdtMap.containsKey(psnDocAstKey) && existingPdtMap.get(psnDocAstKey).containsKey(SQX_Personnel_Document_Training.STATUS_PENDING)) {
                                pendingDT = existingPdtMap.get(psnDocAstKey)
                                                          .get(SQX_Personnel_Document_Training.STATUS_PENDING)
                                                          .get(0); // only one pending training exists per assessment
                            }
                            
                            if (pendingDT != null) {
                                if (isJobFunctionDeactivatedMode) {
                                    // list pending training to reevaluate
                                    overallReq.PersonnelDocumentTraining = pendingDT;
                                }
                                else {
                                    // link and evaluate policies using pending training
                                    overallReq.initPendingPersonnelDocumentTraining(pendingDT);
                                }
                            }
                        }
                        
                        overallRequirementMap.put(psnDocAstKey, overallReq);
                    }
                    
                    overallReq.evalPersonnelDocumentJobFunction(pdjf);
                }
                
                // process training generation
                for (String psnDocAstKey : overallRequirementMap.keySet()) {
                    SQX_OverallTrainingRequirement overallReq = overallRequirementMap.get(psnDocAstKey);
                    SQX_Controlled_Document__c doc = allDocMap.get(overallReq.ControlledDocumentId);
                    SQX_Personnel_Document_Training__c documentTraining = null;
                    Integer defaultTrainingDuration = 0;
                    Boolean addPDT = false,
                            updatePDT = false;
                    
                    // use training duration of controlled document as default
                    if (doc.Duration__c != null) {
                        defaultTrainingDuration = doc.Duration__c.intValue();
                    }
                    
                    // use existing pending training if available
                    documentTraining = overallReq.PersonnelDocumentTraining;
                    
                    // note: pending training always exists in deactivation mode
                    if (documentTraining == null && !isJobFunctionDeactivatedMode) {
                        // create new document training if no pending training exists
                        documentTraining = new SQX_Personnel_Document_Training__c(
                            SQX_Personnel__c = overallReq.PersonnelId,
                            SQX_Controlled_Document__c = overallReq.ControlledDocumentId,
                            SQX_Assessment__c = overallReq.AssessmentId,
                            Level_Of_Competency__c = overallReq.LevelOfCompetency,
                            Due_Date__c = Date.today().addDays(defaultTrainingDuration),
                            Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                            Is_Refresher__c = overallReq.IsRefresher,
                            Is_Retrain__c = overallReq.IsRetrain
                        );
                        
                        // link new training object to overall requirement
                        overallReq.PersonnelDocumentTraining = documentTraining;
                        
                        if (isCreateRefresherMode) {
                            // set due date as per next refresh date for refresher
                            documentTraining.Due_Date__c = overallReq.NextRefreshDate.addDays(overallReq.DaysToCompleteRefresher.intValue());
                        }
                        else if (isJobFunctionActivatedMode) {
                            // credit completed assessments and completed trainings when the required assessment for activated job functions
                            if (overallReq.AssessmentId != null && overallReq.IsRefresher == false && overallReq.IsRetrain == false) {
                                String psnAstKey = '' + overallReq.PersonnelId + overallReq.AssessmentId;
                                
                                if (docCompletedPersonnelAssessmentMap.containsKey(psnDocAstKey)) {
                                    // link completed psersonnel assessment
                                    documentTraining.SQX_Personnel_Assessment__c = docCompletedPersonnelAssessmentMap.get(psnDocAstKey);
                                    
                                    // auto signoff for already completed trainings of this document with same assessment
                                    documentTraining.User_Signoff_Comment__c = Label.SQX_MSG_PDT_SYS_SIGN_OFF_COMMENT_WHEN_ALREADY_COMPLETED_ASSESSMENT_EXISTS;
                                    documentTraining.User_SignOff_Date__c = System.now();
                                    documentTraining.Is_System_User_Sign_Off__c = true;
                                    
                                    SQX_Personnel_Document_Training.setNextStatusForUserSignedOffTraining(documentTraining);
                                    
                                    if (documentTraining.Status__c == SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING) {
                                        String psnDocKey = '' + overallReq.PersonnelId + overallReq.ControlledDocumentId;
                                        
                                        if (docsWithTrainerSignOff.contains(psnDocKey)) {
                                            // auto sign off and complete when any training needing trainer sign off is completed for this controlled document
                                            documentTraining.Trainer_SignOff_Date__c = System.now();
                                            documentTraining.Completion_Date__c = System.today();
                                            documentTraining.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
                                            documentTraining.Is_Completed_By_System__c = true;
                                        }
                                    }
                                }
                                else if (allCompletedPersonnelAssessmentMap.containsKey(psnAstKey)) {
                                    // only link completed personnel assessment when no trainings have been completed with same assessment for this controlled document
                                    documentTraining.SQX_Personnel_Assessment__c = allCompletedPersonnelAssessmentMap.get(psnAstKey);
                                }
                             }
                        }
                    }
                    
                    // set new policies
                    documentTraining.Level_Of_Competency__c = overallReq.LevelOfCompetency;
                    documentTraining.Optional__c = overallReq.Optional;
                    documentTraining.Overall_Competency__c = overallReq.LevelOfCompetency;
                    documentTraining.Overall_Optional__c = overallReq.Optional;
                    
                    // add to insert/update list
                    if (documentTraining.Id == null) {
                        documentTrainingsToInsertMap.put(psnDocAstKey, documentTraining);
                    }
                    else {
                        documentTrainingsToUpdateMap.put(psnDocAstKey, documentTraining);
                    }
                }
            }
        }
        
        isDocumentTrainingsMapGenerated = true;
    }
    
    /**
    * saves generated document trainings and processed PDJFs
    */
    public void save() {
        System.assertEquals(true, isDocumentTrainingsMapGenerated, 'Document training records need to generated by training mapper before saving any changes.');
        
        if (documentTrainingsToInsertMap.size() > 0) {
            /*
            * WITHOUT SHARING used
            * --------------------
            * user releasing a controlled document may not have permission on personnels
            */
            new SQX_DB().withoutSharing().op_insert(documentTrainingsToInsertMap.values(), new List<Schema.SObjectField>{ });
            
            for (String psnDocAstKey : documentTrainingsToInsertMap.keySet()) {
                SQX_Personnel_Document_Training__c pdt = documentTrainingsToInsertMap.get(psnDocAstKey);
                
                for (SQX_Personnel_Document_Job_Function__c pdjf : overallRequirementMap.get(psnDocAstKey).JobFunctionRequirementMap.values()) {
                    pdjf.SQX_Personnel_Document_Training__c = pdt.Id;
                    pdjf.Training_Status__c = pdt.Status__c;
                }
            }
        }
        if (documentTrainingsToUpdateMap.size() > 0) {
            /*
            * WITHOUT SHARING used
            * --------------------
            * user releasing a controlled document may not have permission on personnels
            */
            new SQX_DB().withoutSharing().op_update(
                                            documentTrainingsToUpdateMap.values(),
                                            new List<Schema.SObjectField>{
                                                SQX_Personnel_Document_Training__c.Level_Of_Competency__c,
                                                SQX_Personnel_Document_Training__c.Optional__c,
                                                SQX_Personnel_Document_Training__c.Overall_Competency__c,
                                                SQX_Personnel_Document_Training__c.Overall_Optional__c,
                                                SQX_Personnel_Document_Training__c.Status__c
            });
            
            if (!isJobFunctionDeactivatedMode) {
                // for job deactivated mode, only pending trainings need to be updated
                for (String psnDocAstKey : documentTrainingsToUpdateMap.keySet()) {
                    SQX_Personnel_Document_Training__c pdt = documentTrainingsToUpdateMap.get(psnDocAstKey);
                    
                    for (SQX_Personnel_Document_Job_Function__c pdjf : overallRequirementMap.get(psnDocAstKey).JobFunctionRequirementMap.values()) {
                        pdjf.SQX_Personnel_Document_Training__c = pdt.Id;
                        pdjf.Training_Status__c = pdt.Status__c;
                    }
                }
            }
        }
    }
    
    /**
    * SQX_Personnel_Document_Training_Mapper constructor
    * @param pdjfs  active PDJFs to generate/reevaluate document trainings
    * @param mode   mapper mode to generate document trainings
    */
    public SQX_Personnel_Document_Training_Mapper(List<SQX_Personnel_Document_Job_Function__c> pdjfs, MapperModes mode) {
        initialize();
        
        mapperMode = mode;
        
        generatePersonnelDocumentTraining(pdjfs);
    }
    
    /**
    * class to hold overall training requirement that is generated from mapped job functions between a personnel and a document
    */
    public class SQX_OverallTrainingRequirement {
        /**
        * internally used variables
        */
        private Boolean isPolicyInitialized;
        private SQX_Requirement.SQX_LevelOfCompetencyMapper CompetencyMapper;
        
        public Id AssessmentId { get; set; }
        public Id PersonnelId { get; set; }
        public Id ControlledDocumentId { get; set; }
        public String LevelOfCompetency { get; set; }
        public Boolean Optional { get; set; }
        public Boolean IsRetrain { get; set; }
        public Boolean IsRefresher { get; set; }
        public Decimal DaysToCompleteRefresher { get; set; }
        public Date NextRefreshDate { get; set; }
        
        public Map<Id, SQX_Personnel_Document_Job_Function__c> JobFunctionRequirementMap { get; set; }
        public SQX_Personnel_Document_Training__c PersonnelDocumentTraining { get; set; }
        
        /**
        * SQX_OverallTrainingRequirement constructor
        */
        public SQX_OverallTrainingRequirement(Id personnelId, Id documentId, Id assessmentId, SQX_Requirement.SQX_LevelOfCompetencyMapper competencyMapper) {
            this.JobFunctionRequirementMap = new Map<Id, SQX_Personnel_Document_Job_Function__c>();
            this.PersonnelId = personnelId;
            this.ControlledDocumentId = documentId;
            this.AssessmentId = assessmentId;
            if (competencyMapper == null) {
                this.CompetencyMapper = new SQX_Requirement.SQX_LevelOfCompetencyMapper();
            }
            else {
                this.CompetencyMapper = competencyMapper;
            }
            this.IsRefresher = false;
            this.IsRetrain = false;
            this.isPolicyInitialized = false;
        }
        
        /**
        * evaluate training policies using PDJF
        */
        public void evalPersonnelDocumentJobFunction(SQX_Personnel_Document_Job_Function__c pdjf) {
            if (isPolicyInitialized == false) {
                LevelOfCompetency = pdjf.Level_Of_Competency__c;
                Optional = pdjf.Optional__c == true;
                
                isPolicyInitialized = true;
            }
            else {
                if (String.isBlank(LevelOfCompetency) || CompetencyMapper.compare(pdjf.Level_Of_Competency__c, LevelOfCompetency) > 0) {
                    LevelOfCompetency = pdjf.Level_Of_Competency__c;
                }
                if (pdjf.Optional__c != true) {
                    Optional = false;
                }
            }
            
            if (pdjf.Training_Type__c == SQX_Personnel_Document_Job_Function.TRAINING_TYPE_REFRESHER) {
                IsRefresher = true;
                
                if (NextRefreshDate == null || NextRefreshDate > pdjf.Next_Refresh_Date__c) {
                    // uses earliest next refresh date
                    NextRefreshDate = pdjf.Next_Refresh_Date__c;
                }
                if (DaysToCompleteRefresher == null || DaysToCompleteRefresher < pdjf.Days_to_Complete_Refresher__c) {
                    // uses largest duration for completing refresher training
                    DaysToCompleteRefresher = pdjf.Days_to_Complete_Refresher__c;
                }
            }
            else if (pdjf.Training_Type__c == SQX_Personnel_Document_Job_Function.TRAINING_TYPE_RETRAIN) {
                IsRetrain = true;
            }
            
            // map evaluated PDJF
            JobFunctionRequirementMap.put(pdjf.Job_Function_Id__c, pdjf);
        }
        
        /**
        * initialize training policies using pending training
        */
        public void initPendingPersonnelDocumentTraining(SQX_Personnel_Document_Training__c pendingDT) {
            LevelOfCompetency = pendingDT.Overall_Competency__c;
            Optional = pendingDT.Overall_Optional__c == true;
            IsRetrain = pendingDT.Is_Retrain__c == true;
            IsRefresher = pendingDT.Is_Refresher__c == true;
            
            // link evaluated pending training
            this.PersonnelDocumentTraining = pendingDT;
            
            isPolicyInitialized = true;
        }
    }
    
}