/**
* This test class to Initiate collaboration
*/
@isTest
public class SQX_Test_2646_Ctrl_Doc_Collaboration {
    /**
    * 'SeeAllDate=true' this is required when we call the ConnectApi, in this method it calls connectApi.
    * This method access all data, when we use 'SeeAllDate=true'.
    * When:controlled document inititate collaboration
    * Then: create a collaboration group
    */
    @isTest(SeeAllData=true)
    static void WhenInitiateCollaboration_ThenCreateCollaborationGroup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(adminUser){
            // Arrange: Create controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.save();
            
            // Act: submit to create collaboration group and members
            List<String> userIDs= new List<String> { standardUser.Id };
            SQX_Extension_Controlled_Doc_Collaborate obj=new SQX_Extension_Controlled_Doc_Collaborate(new ApexPages.StandardController(cDoc1.doc));
            obj.CollaborationTitle='testGroup';
            obj.UserIDs =userIDs;
            obj.saveAndInitiateCollaboration();

            CollaborationGroupMember objCollMem=new CollaborationGroupMember();
            objCollMem.CollaborationRole=[SELECT CollaborationRole FROM CollaborationGroupMember WHERE MemberId =: standardUser.Id].CollaborationRole;
            cDoc1.synchronize();

            //Assert: Ensure that CQ Collaboration Record Type assigned
            Id RecordTypeId=[SELECT RecordTypeId FROM ContentVersion WHERE ContentDocumentId=:cDoc1.doc.Collaboration_Group_Content_Id__c].RecordTypeId;
            System.assertEquals(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CQCollaboration),RecordTypeId);
            
            // Assert: Ensure that collaboration group created 
            System.assertNotEquals(null,cDoc1.doc.Collaboration_Group_Id__c,'Expected Not Null');

            //Assert : Ensure that collaboration group member role added
            System.assertEquals(SQX_SF_Collaboration_Provider.COLLABORATION_MEMBER_ROLE_ADMIN,objCollMem.CollaborationRole,'Expected Admin Role');
            
            // Assert: Ensure that checkout returned true and checkoutBy,checkedOutOn should not returned null
            System.assertEquals(true, cDoc1.doc.Checked_Out__c);
            System.assertNotEquals(null, cDoc1.doc.SQX_Checked_Out_By__c);
            System.assertNotEquals(null, cDoc1.doc.Checked_Out_On__c);

            //**** When members upload new version of main file, members should notified ****//
            
            // Arrange: Created new content to upload new version in collaboration group
            ContentVersion newCv = new ContentVersion();
            newCv.VersionData = Blob.valueOf('Uploaded New Version');
            newCv.PathOnClient = 'New Doc.txt';
            newCV.ReasonForChange='what changes';
            newCv.ContentDocumentId = cDoc1.doc.Collaboration_Group_Content_Id__c; 
            insert newCv;
            
            //Assert: Ensure that feed to be posted
            CollaborationGroupFeed feed = [SELECT Body FROM CollaborationGroupFeed WHERE ParentId = :cDoc1.doc.Collaboration_Group_Id__c ORDER BY CreatedDate DESC LIMIT 1];
            System.assertEquals('@'+adminUser.Name+' '+Label.CQ_UI_Document_Review_uploaded+' '+newCv.ReasonForChange,feed.Body);
            
            //Assert: Ensure that new uploaded version record typeid equals to CQ Collaboration recordTypeId
            Id RecordType=[SELECT RecordTypeId FROM ContentVersion WHERE ContentDocumentId=:newCv.ContentDocumentId ORDER BY CreatedDate DESC LIMIT 1].RecordTypeId;
            System.assertEquals(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CQCollaboration), RecordType);
        }
    }
}