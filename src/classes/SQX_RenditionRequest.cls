/**
* Represents the request for generating/submitting/fetching a rendition 
*/
global class SQX_RenditionRequest {
    /**
    * The content that is related to the request for which the rendition is to be generated
    */
    global ContentVersion content { get; set; }

    /**
    * The controlled document that is related to the rendition request
    */
    global SQX_Controlled_Document__c document { get; set;}

    /**
    * The job id that is related to the request, it is null for new request and non-null for continuation request
    */
    global String jobId {get; set;}
    
    /**
    * The content version Id which was used to initiate the request.
    */
    public Id primaryContentVersion {get; set;}

    /**
    * The previous provider for the request if any
    */
    public String previousProviderName {get; set;}
    

    /**
    * Internal property used to track the name of the rendition provider
    */
    public String providerName {get; set;}

    /**
    * Resets the current request to look like a new request.
    */
    public void resetRequest(){
        jobId = null;
        primaryContentVersion = content.Id;
        previousProviderName = null;
    }

    /**
    * checks whether or not the provider has changed after initiation of the request
    * @return returns true if provider has changed else returns false
    */
    public Boolean hasProviderChanged(SQX_RenditionProvider provider){
        return this.JobId != null && this.ProviderName != provider.getProviderName();
    }

    /**
    * checks whether or not the content has changed after initiation of the request
    * @return returns true if content has changed else returs false
    */
    public Boolean hasContentChanged(){

        return this.primaryContentVersion != null && this.content != null && this.content.Id != this.primaryContentVersion;
    }

    /**
    *   checks whether the document's secondary content has gone out of sync
    *
    *   Note : there are some fields ( e.g. Secondary Format Setting ) which when changed, make the secondary content out of sync with the current doc content
    *   To incorporate those changes, this flag has been introduced
    *   @return <code>true</code> if doc's secondary content is currently out of sync , else <code>false</code>
    */
    public Boolean hasDocGoneOutOfSync(){

        return this.document != null && this.document.Synchronization_Status__c == SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC;
    }
}