/**
* Unit tests for private sharing model of personnel object and its child objects for document training module
*/
@IsTest
public class SQX_Test_2355_Personnel_PrivateSharing {
    
    static Boolean runAllTests = true,
                   run_givenImportUserAsPersonnel_RelatedUserHasReadAccess = false,
                   run_givenModifyPersonnelManager_RelatedUserHasReadAccess = false,
                   run_givenModifyPersonnelUser_RelatedUserHasReadAccess = false,
                   run_givenModifyPersonnelRecord_ManuallyAddedSharingToPersonnelUserNotChanged = false,
                   run_givenActivateUserRelatedToPersonnel_RelatedUserHasReadAccess = false,
                   run_givenReleaseDocument_AllDocumentTrainingsAdded = false,
                   run_givenActivateRequirement_AllDocumentTrainingsUpdated = false,
                   run_givenDeactivateRequirement_AllDocumentTrainingsUpdated = false,
                   run_givenRetireDocument_AllPendingDocumentTrainingsUpdatedToObsolete = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, mockRole, 'sysAdminUser');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user1');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user2');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, 'user3');
        
        System.runas(adminUser) {
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(2, true);
            insert jfs;
        }
    }
    
    /**
    * ensures imported user as personnel with another user as manager has read access
    */
    public testmethod static void givenImportUserAsPersonnel_RelatedUserHasReadAccess() {
        if (!runAllTests && !run_givenImportUserAsPersonnel_RelatedUserHasReadAccess) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        System.runas(user1) {
            // ACT: import user as personnel
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            Database.SaveResult res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
            
            List<SQX_Personnel__Share> user2share= [SELECT ParentId, UserOrGroupId, AccessLevel
                                                    FROM SQX_Personnel__Share
                                                    WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user2.Id AND AccessLevel = 'read'];
            
            // ensure related personnel user has read access
            System.assertEquals(1, user2share.size());
        }
    }
    
    /**
    * ensures manager / owner change action for a personnel adds read access to the related personnel user
    */
    public testmethod static void givenModifyPersonnelManager_RelatedUserHasReadAccess() {
        if (!runAllTests && !run_givenModifyPersonnelManager_RelatedUserHasReadAccess) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        User user3 = users.get('user3');
        
        SQX_Test_Personnel p1;
        Database.SaveResult res1;
        
        System.runas(user1) {
            // add required personnel
            p1 = new SQX_Test_Personnel(user2);
            res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: change owner
            p1.mainRecord.OwnerId = user3.Id;
            res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
        }
        
        System.runas(user3) {
            List<SQX_Personnel__Share> user2share= [SELECT ParentId, UserOrGroupId, AccessLevel
                                                    FROM SQX_Personnel__Share
                                                    WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user2.Id AND AccessLevel = 'read'];
            
            // ensure related personnel user has read access
            System.assertEquals(1, user2share.size());
        }
    }
    
    /**
    * ensures changing related personnel user adds read access to the new related personnel user and removes access for old related user
    */
    public testmethod static void givenModifyPersonnelUser_RelatedUserHasReadAccess() {
        if (!runAllTests && !run_givenModifyPersonnelUser_RelatedUserHasReadAccess) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        User user3 = users.get('user3');
        
        System.runas(user1) {
            Database.SaveResult res1;
            
            // add required personnel
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: change related user
            p1.mainRecord.SQX_User__c = user3.Id;
            res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
            
            List<SQX_Personnel__Share> user2share= [SELECT ParentId, UserOrGroupId, AccessLevel
                                                    FROM SQX_Personnel__Share
                                                    WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user2.Id];
            
            // ensure old related personnel user has no access
            System.assertEquals(0, user2share.size());
            
            List<SQX_Personnel__Share> user3share= [SELECT ParentId, UserOrGroupId
                                                    FROM SQX_Personnel__Share
                                                    WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user3.Id AND AccessLevel = 'read'];
            
            // ensure new related personnel user has read access
            System.assertEquals(1, user3share.size());
        }
    }
    
    /**
    * ensures modifying personnel record with a related user not to change manually added sharing rule for the personnel user
    */
    public testmethod static void givenModifyPersonnelRecord_ManuallyAddedSharingToPersonnelUserNotChanged() {
        if (!runAllTests && !run_givenModifyPersonnelRecord_ManuallyAddedSharingToPersonnelUserNotChanged) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        User user3 = users.get('user3');
        
        System.runas(user1) {
            Database.SaveResult res1;
            
            // add required personnel
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user2);
            res1 = p1.save();
            
            // get added sharing rule for user3
            insert new SQX_Personnel__Share(
                ParentId = p1.mainRecord.Id,
                UserOrGroupId = user3.Id,
                AccessLevel = 'edit'
            );
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: change personnel user to user3
            p1.mainRecord.SQX_User__c = user3.Id;
            p1.save();
            
            // ensure sharing rule has not been changed
            List<SQX_Personnel__Share> user3share= [SELECT ParentId, UserOrGroupId
                                                    FROM SQX_Personnel__Share
                                                    WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user3.Id AND AccessLevel = 'edit'];
            
            System.assertEquals(1, user3share.size());
            
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: modify personnel record
            p1.mainRecord.Full_Name__c += '123';
            p1.save();
            
            // ensure sharing rule has not been changed
            user3share=[SELECT ParentId, UserOrGroupId
                        FROM SQX_Personnel__Share
                        WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user3.Id AND AccessLevel = 'edit'];
            
            System.assertEquals(1, user3share.size());
        }
    }
    
    /**
    * ensures activating user adds read access on the related personnel record
    */
    public testmethod static void givenActivateUserRelatedToPersonnel_RelatedUserHasReadAccess() {
        if (!runAllTests && !run_givenActivateUserRelatedToPersonnel_RelatedUserHasReadAccess) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get('sysAdminUser');
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        System.runas(adminUser) {
            Database.SaveResult res1;
            List<SQX_Personnel__Share> user1share, user2share;
            
            // make users inactive
            user1.isActive = false;
            user2.isActive = false;
            update new List<User>{ user1, user2 };
            
            // add required personnels for inactive users
            SQX_Test_Personnel p1 = new SQX_Test_Personnel(user1);
            res1 = p1.save();
            System.assertEquals(true, res1.isSuccess());
            
            SQX_Test_Personnel p2 = new SQX_Test_Personnel(user2);
            p2.mainRecord.Active__c = true; // set personnel record active for inactive user
            res1 = p2.save();
            System.assertEquals(true, res1.isSuccess());
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            Test.startTest();
            
            // ACT: activate users
            user1.isActive = true;
            user2.isActive = true;
            Database.SaveResult[] res2 = Database.update(new List<User>{ user1, user2 }, false);
            
            String errMsgLicenseLimitExceeded = 'license limit exceeded',
                suggestedSolution = ' Deactivate users to free 4 salesforce user licenses in the org to run this test, and activate them later.';
            System.assertEquals(true, res2[0].isSuccess(),
                'Error occurred while activating user for this test.'
                + (SQX_Utilities.checkErrorMessage(res2[0].getErrors(), errMsgLicenseLimitExceeded) ? suggestedSolution : '')
                + '\r\nActual Errors: '
                + res2[0].getErrors());
            System.assertEquals(true, res2[1].isSuccess(),
                'Error occurred while activating user for this test.'
                + (SQX_Utilities.checkErrorMessage(res2[1].getErrors(), errMsgLicenseLimitExceeded) ? suggestedSolution : '')
                + '\r\nActual Errors: '
                + res2[1].getErrors());
            
            Test.stopTest();
            
            // ensure user1 has read access on p1
            user1share=[SELECT ParentId, UserOrGroupId, AccessLevel
                        FROM SQX_Personnel__Share
                        WHERE ParentId = :p1.mainRecord.Id AND UserOrGroupId = :user1.Id AND AccessLevel = 'read'];
            System.assertEquals(1, user1share.size());
            
            // ensure user2 has read access on p2
            user2share=[SELECT ParentId, UserOrGroupId, AccessLevel
                        FROM SQX_Personnel__Share
                        WHERE ParentId = :p2.mainRecord.Id AND UserOrGroupId = :user2.Id AND AccessLevel = 'read'];
            System.assertEquals(1, user2share.size());
        }
    }
    
    /**
    * ensures all document trainings related to a controlled document are created disregarding the owner of any personnel records
    *   when the document is released by document owner
    */
    public testmethod static void givenReleaseDocument_AllDocumentTrainingsAdded() {
        if (!runAllTests && !run_givenReleaseDocument_AllDocumentTrainingsAdded) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get('sysAdminUser');
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        // get required job function
        SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c LIMIT 1];
        
        SQX_Test_Controlled_Document testDoc1;
        
        System.runas(user1) {
            // add required personnels
            SQX_Test_Personnel p1 = new SQX_Test_Personnel();
            p1.save();
            SQX_Test_Personnel p2 = new SQX_Test_Personnel();
            p2.save();
            
            // add required personnel job functions
            List<SQX_Personnel_Job_Function__c> pfjs = new List<SQX_Personnel_Job_Function__c>();
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            ));
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p2.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            ));
            insert pfjs;
        }
        
        System.runas(user2) {
            // add required personnel
            SQX_Test_Personnel p3 = new SQX_Test_Personnel();
            p3.save();
            
            // add required personnel job function
            insert new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p3.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            
            // add required draft controlled document
            testDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required document requirement
            insert new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: release controlled document
            testDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            System.assertEquals(3, [SELECT Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id = :testDoc1.doc.Id].Pending_Document_Training__c);
        }
        
        System.runas(adminUser) {
            // get list of personnel document training created while adding active requirement
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id];
            
            System.assertEquals(3, dtList.size(), 'Expected all personnel document trainings are created when controlled document is released.');
        }
    }
    
    /**
    * ensures all document trainings related to a controlled document are created/updated disregarding the owner of any personnel records
    *   when a requirement of the document is activated by document owner
    */
    public testmethod static void givenActivateRequirement_AllDocumentTrainingsUpdated() {
        if (!runAllTests && !run_givenActivateRequirement_AllDocumentTrainingsUpdated) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get('sysAdminUser');
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        // get required job function
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        System.assert(jfs.size() >= 2);
        SQX_Job_Function__c jf1 = jfs[0];
        SQX_Job_Function__c jf2 = jfs[1];
        
        SQX_Test_Controlled_Document testDoc1;
        SQX_Test_Personnel p1, p2;
        SQX_Personnel_Document_Training__c p1dt, p2dt;
        
        System.runas(user1) {
            // add required personnels
            p1 = new SQX_Test_Personnel();
            p2 = new SQX_Test_Personnel();
            p1.save();
            p2.save();
            
            // add required personnel job functions
            List<SQX_Personnel_Job_Function__c> pfjs = new List<SQX_Personnel_Job_Function__c>();
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            ));
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = true
            ));
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p2.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = true
            ));
            insert pfjs;
        }
        
        System.runas(user2) {
            // add required draft controlled document
            testDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required document requirement
            insert new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            
            // release controlled document
            testDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            System.assertEquals(1, [SELECT Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id = :testDoc1.doc.Id].Pending_Document_Training__c);
        }
        
        System.runas(adminUser) {
            // get all document trainings
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id];
            
            System.assertEquals(1, dtList.size());
            
            p1dt = dtList[0];
            System.assertEquals(p1.mainRecord.Id, p1dt.SQX_Personnel__c);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p1dt.Status__c);
            System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, p1dt.Level_of_Competency__c);
            System.assertEquals(true, p1dt.Optional__c);
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: activate requirement with higher competency
            insert new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Optional__c = false,
                Active__c = true
            );
            
            System.assertEquals(2, [SELECT Pending_Document_Training__c FROM SQX_Controlled_Document__c WHERE Id = :testDoc1.doc.Id].Pending_Document_Training__c);
        }
        
        System.runas(adminUser) {
            // get all document trainings to verify
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id];
            
            System.assertEquals(2, dtList.size());
            
            if (dtList[0].Id == p1dt.Id) {
                p1dt = dtList[0];
                p2dt = dtList[1];
            }
            else if (dtList[1].Id == p1dt.Id) {
                p1dt = dtList[1];
                p2dt = dtList[0];
            }
            
            // verify updated document training with higher competency for p1
            System.assertEquals(p1.mainRecord.Id, p1dt.SQX_Personnel__c);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p1dt.Status__c);
            System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, p1dt.Level_of_Competency__c);
            System.assertEquals(false, p1dt.Optional__c);
            
            // verify new document training with higher competency for p2
            System.assertEquals(p2.mainRecord.Id, p2dt.SQX_Personnel__c);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p2dt.Status__c);
            System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, p2dt.Level_of_Competency__c);
            System.assertEquals(false, p2dt.Optional__c);
        }
    }
    
    /**
    * ensures all document trainings related to a controlled document are created/updated disregarding the owner of any personnel records
    *   when a requirement of the document is deactivated by document owner
    */
    public testmethod static void givenDeactivateRequirement_AllDocumentTrainingsUpdated() {
        if (!runAllTests && !run_givenDeactivateRequirement_AllDocumentTrainingsUpdated) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get('sysAdminUser');
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        // get required job function
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        System.assert(jfs.size() >= 2);
        SQX_Job_Function__c jf1 = jfs[0];
        SQX_Job_Function__c jf2 = jfs[1];
        
        SQX_Test_Controlled_Document testDoc1;
        Id reqIdToDeactivate;
        SQX_Test_Personnel p1;
        SQX_Personnel_Document_Training__c p1dt;
        
        System.runas(user1) {
            // add required personnels
            p1 = new SQX_Test_Personnel();
            p1.save();
            
            // add required personnel job functions
            List<SQX_Personnel_Job_Function__c> pfjs = new List<SQX_Personnel_Job_Function__c>();
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            ));
            pfjs.add(new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf2.Id,
                Active__c = true
            ));
            insert pfjs;
        }
        
        System.runas(user2) {
            // add required draft controlled document
            testDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required document requirements
            List<SQX_Requirement__c> reqs = new List<SQX_Requirement__c>();
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            ));
            reqs.add(new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf2.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                Optional__c = false,
                Active__c = false
            ));
            insert reqs;
            
            reqIdToDeactivate = reqs[1].Id; // set requirement with higher competency as reqToDeactivate
            
            // release controlled document
            testDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
        }
        
        System.runas(adminUser) {
            // get all document trainings
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id];
            
            System.assertEquals(1, dtList.size());
            
            p1dt = dtList[0];
            System.assertEquals(p1.mainRecord.Id, p1dt.SQX_Personnel__c);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, p1dt.Status__c);
            System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, p1dt.Level_of_Competency__c);
            System.assertEquals(false, p1dt.Optional__c);
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: deactivate requirement with higher competency
            update new SQX_Requirement__c(
                Id = reqIdToDeactivate,
                Active__c = false
            );
        }
        
        System.runas(adminUser) {
            // get all document trainings to verify
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id];
            
            System.assertEquals(1, dtList.size());
            System.assertEquals(p1dt.Id, dtList[0].Id);
            System.assertEquals(p1.mainRecord.Id, dtList[0].SQX_Personnel__c);
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_PENDING, dtList[0].Status__c);
            System.assertEquals(SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, dtList[0].Level_of_Competency__c);
            System.assertEquals(true, dtList[0].Optional__c);
        }
    }
    
    /**
    * ensures all pending document trainings related to a controlled document are updated t obsolete disregarding the owner of any personnel records
    *   when a document is retired
    */
    public testmethod static void givenRetireDocument_AllPendingDocumentTrainingsUpdatedToObsolete() {
        if (!runAllTests && !run_givenRetireDocument_AllPendingDocumentTrainingsUpdatedToObsolete) {
            return;
        }
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User adminUser = users.get('sysAdminUser');
        User user1 = users.get('user1');
        User user2 = users.get('user2');
        
        // get required job function
        SQX_Job_Function__c jf1 = [SELECT Id, Name FROM SQX_Job_Function__c LIMIT 1];
        
        SQX_Test_Controlled_Document testDoc1;
        SQX_Test_Personnel p1, p2;
        
        System.runas(user1) {
            // add required personnel
            p1 = new SQX_Test_Personnel();
            p1.save();
            
            // add required personnel job function
            insert new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p1.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
        }
        
        System.runas(user2) {
            // add required personnel
            p2 = new SQX_Test_Personnel();
            p2.save();
            
            // add required personnel job function
            insert new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = p2.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );
            
            // add required draft controlled document
            testDoc1 = new SQX_Test_Controlled_Document().save();
            
            // add required document requirement
            insert new SQX_Requirement__c(
                SQX_Controlled_Document__c = testDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Optional__c = true,
                Active__c = false
            );
            
            // release controlled document
            testDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
        }
        
        System.runas(adminUser) {
            // get all pending document trainings
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            System.assertEquals(2, dtList.size());
        }
        
        System.runas(user2) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: retire controlled document
            testDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
        }
        
        System.runas(adminUser) {
            // get all pending document trainings
            List<SQX_Personnel_Document_Training__c> dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                                                               FROM SQX_Personnel_Document_Training__c
                                                               WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            System.assertEquals(0, dtList.size());
            
            // get all obsolete document trainings
            dtList = [SELECT Id, SQX_Personnel__c, SQX_Controlled_Document__c, Status__c, Level_of_Competency__c, Optional__c
                       FROM SQX_Personnel_Document_Training__c
                       WHERE SQX_Controlled_Document__c = :testDoc1.doc.Id AND Status__c = :SQX_Personnel_Document_Training.STATUS_OBSOLETE];
            
            System.assertEquals(2, dtList.size());
        }
    }
    
}