@IsTest
public class SQX_Test_Refresher_Document_Training {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'standardUser');
        
        SQX_Job_Function__c jf1, jf2, jf3, jf4;
        SQX_Personnel__c p1, p2;
        
        System.runAs(admin1) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(4, true);
            insert jfs;

            jf1 = jfs[0];
            jf2 = jfs[1];
            jf3 = jfs[2];
            jf4 = jfs[3];
        }
    
        System.runAs(standardUser) {
            // add required personnels
            p1 = new SQX_Test_Personnel().mainRecord;
            p2 = new SQX_Test_Personnel().mainRecord;
            p1.Full_Name__c = 'P1';
            p2.Full_Name__c = 'P2';
            p1.Active__c = true;
            p2.Active__c = true;
            insert new List<SQX_Personnel__c> { p1, p2 };
            
            // add required active personnel job functions
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p1.Id, SQX_Job_Function__c = jf1.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf2.Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = p2.Id, SQX_Job_Function__c = jf3.Id, Active__c = true ));
            insert pjfs;


            SQX_Test_Controlled_Document document1 = new SQX_Test_Controlled_Document();
            document1.doc.Title__c = 'document1';
            document1.save();

            SQX_Test_Controlled_Document document2 = new SQX_Test_Controlled_Document();
            document2.doc.Title__c = 'document2';
            document2.save();

            //Add assessment1   
            SQX_Test_Assessment assessment1 = new SQX_Test_Assessment();
            assessment1.assessment.Name = 'assessment1';
            assessment1.save(); 
            SQX_Test_Assessment_Question.createAssessmentQuestions(assessment1.assessment,false);

            //update the assessment with total question to ask number and status
            assessment1.assessment.Total_Questions_To_Ask__c = 4;
            assessment1.assessment.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;
            assessment1.save();
            
            //Add assessment2   
            SQX_Test_Assessment assessment2 = new SQX_Test_Assessment();
            assessment2.assessment.Name = 'assessment2';
            assessment2.save(); 
            SQX_Test_Assessment_Question.createAssessmentQuestions(assessment2.assessment,false);

            //update the assessment with total question to ask number and status
            assessment2.assessment.Total_Questions_To_Ask__c = 4;
            assessment2.assessment.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;
            assessment2.save();

            //Arrange: Requirement is created  
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf1.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 20, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY, 
                                                             Days_to_Complete_Refresher__c = 10,
                                                             Days_in_Advance_To_Start_Refresher__c = 5);

            SQX_Requirement__c req2 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf2.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 30, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 2,
                                                             Days_in_Advance_To_Start_Refresher__c = 10,
                                                             SQX_Initial_Assessment__c = assessment1.assessment.Id,
                                                             SQX_Refresher_Assessment__c = assessment2.assessment.Id,
                                                             SQX_Revision_Assessment__c = assessment1.assessment.Id);
            
            SQX_Requirement__c req3 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                             SQX_Job_Function__c = jf3.Id, 
                                                             Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Require_Refresher__c = true, 
                                                             Refresher_Interval__c = 60, 
                                                             Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                             Days_to_Complete_Refresher__c = 10,
                                                             Days_in_Advance_To_Start_Refresher__c = 40,
                                                             SQX_Refresher_Assessment__c = assessment2.assessment.Id
                                                             );

            new SQX_DB().op_insert(new List<SObject>{req1, req2, req3}, new List<SObjectField>{});

            document1.doc.Effective_Date__c = Date.Today();
            document1.doc.Date_Issued__c = Date.Today();
            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();
        }

    }

    /*
    * This returns Map of Job Function 
    */
    static Map<String, SQX_Job_Function__c> getJobFunctions() {
        List<SQX_Job_Function__c> jfs = [SELECT Id, Name FROM SQX_Job_Function__c];
        
        Map<String, SQX_Job_Function__c> jfMap = new Map<String, SQX_Job_Function__c>();
        for (SQX_Job_Function__c jf : jfs) {
            jfMap.put(jf.Name, jf);
        }
        
        return jfMap;
    }
    
    /*
    * This returns Map of Personnels
    */
    static Map<String, SQX_Personnel__c> getPersonnels() {
        List<SQX_Personnel__c> psns = [SELECT Id, Name, Full_Name__c, Identification_Number__c, SQX_User__c, Active__c FROM SQX_Personnel__c];
        
        Map<String, SQX_Personnel__c> psnMap = new Map<String, SQX_Personnel__c>();
        for (SQX_Personnel__c psn : psns) {
            psnMap.put(psn.Full_Name__c, psn);
        }
        
        return psnMap;
    }

    /*
    * This returns Map of Document 
    */
    static Map<String, SQX_Test_Controlled_Document> getControlledDocuments() {
        Map<String, SQX_Test_Controlled_Document> cdocMap = new Map<String, SQX_Test_Controlled_Document>();
        for (SQX_Controlled_Document__c cdoc : [SELECT Id, Name, Title__c, Document_Status__c, Effective_Date__c, Date_Issued__c, Expiration_Date__c FROM SQX_Controlled_Document__c]) {
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.doc = cdoc;
            cdocMap.put(cdoc.Title__c, testDoc);
        }
        
        return cdocMap;
    }

    /*
    * This returns Map of Requirement 
    */
    static Map<String, SQX_Requirement__c> getRequirements() {
        List<SQX_Requirement__c> reqs = [SELECT Id, 
                                                Refresher_Interval__c, 
                                                Days_in_Advance_to_Start_Refresher__c, 
                                                Days_to_Complete_Refresher__c, 
                                                Require_Refresher__c,
                                                SQX_Controlled_Document__r.Id, 
                                                SQX_Job_Function__r.Id,
                                                Refresher_Competency__c 
                                        FROM SQX_Requirement__c];
        
        String cdocJFKey;
        Map<String, SQX_Requirement__c> reqMap = new Map<String, SQX_Requirement__c>();
        for (SQX_Requirement__c req : reqs) {
            cdocJFKey = '' + req.SQX_Controlled_Document__r.Id + req.SQX_Job_Function__r.Id;
            reqMap.put(cdocJFKey, req);
        }
        
        return reqMap;
    }

    /*
    * This returns Map of Personnel Document Job Function 
    */
    static Map<String, SQX_Personnel_Document_Job_Function__c> getPdjf() {

        List<SQX_Personnel_Document_Job_Function__c>  pdjfs = [SELECT Id,
                                                                       SQX_Personnel_Document_Training__c,
                                                                       Next_Refresh_Date__c, 
                                                                       Refresher_Queue_Date__c, 
                                                                       Create_Refresher__c,
                                                                       Training_Type__c,
                                                                       Training_Status__c,
                                                                       Personnel_Id__c,
                                                                       Job_Function_Id__c,
                                                                       SQX_Requirement__r.Id
                                                               FROM SQX_Personnel_Document_Job_Function__c
                                                              ];
        String psnReqKey;
        Map<String, SQX_Personnel_Document_Job_Function__c> pdjfMap = new Map<String, SQX_Personnel_Document_Job_Function__c>();
        for (SQX_Personnel_Document_Job_Function__c pdjf : pdjfs) {
            psnReqKey = '' + pdjf.Personnel_Id__c + pdjf.SQX_Requirement__r.Id;
            pdjfMap.put(psnReqKey, pdjf);
        }
        return pdjfMap;
    }

    /*
    * This returns Map of Assessment
    */
    static Map<String, SQX_Test_Assessment> getAssessments() {
        Map<String, SQX_Test_Assessment> assessmentMap = new Map<String, SQX_Test_Assessment>();

        for (SQX_Assessment__c assessments : [SELECT Id, Name, Status__c FROM SQX_Assessment__c]) {
            SQX_Test_Assessment testAssessment = new SQX_Test_Assessment();
            testAssessment.assessment = assessments;
            assessmentMap.put(assessments.Name, testAssessment);
        }

        return assessmentMap;
    }

    /**  
    * Given: New Document in Pre-Release state and Active Requirement  .
    * When : When DT is completed
    * Then : Calculate Next Refresher Date
    */
    public static testmethod void givenActiveRequirement_WhenDocumentTrainingisCompleted_NextRefreshDateIsCalculated() {

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');

        Map<String, SQX_Test_Controlled_Document> cdocs = getControlledDocuments();
        SQX_Test_Controlled_Document    document1 = cdocs.get('document1');

        Map<String, SQX_Requirement__c> reqs = getRequirements();
        String cdocJFKey1 = '' + document1.doc.Id + jf1.Id;
        
        SQX_Requirement__c    req1 = reqs.get(cdocJFKey1);  

        Map<String, SQX_Personnel_Document_Job_Function__c> pdjfs = getPdjf();
        String psnReqKey1 = '' + p1.Id + req1.Id;
        
        SQX_Personnel_Document_Job_Function__c    pdjf1 = pdjfs.get(psnReqKey1); 

        System.runas(standardUser) {

            document1.doc.Effective_Date__c = Date.Today();
            document1.doc.Date_Issued__c = Date.Today();
            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();

            Test.startTest();
            //Act2: Document Training Status are updated to complete and Completion date is set.
            SQX_Personnel_Document_Training__c docTraining1 = [SELECT Id, Completion_Date__c, Status__c
                                                              FROM SQX_Personnel_Document_Training__c 
                                                              WHERE SQX_Controlled_Document__c = :document1.doc.Id AND SQX_Personnel__c = :p1.Id];

            //Act: Document Trainings are Completed
            docTraining1.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            docTraining1.Completion_Date__c = Date.Today();

            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            new SQX_DB().op_update(new List<SObject>{docTraining1}, new List<SObjectField>{Schema.SQX_Personnel_Document_Training__c.fields.Status__c, 
                                                                                            Schema.SQX_Personnel_Document_Training__c.fields.Completion_Date__c});

            //Get Refresher PDJF
            SQX_Personnel_Document_Job_Function__c pdjf1R = [SELECT Id,
                                                                   SQX_Personnel_Document_Training__c,
                                                                   Next_Refresh_Date__c, 
                                                                   Refresher_Queue_Date__c, 
                                                                   Create_Refresher__c,
                                                                   Training_Type__c,
                                                                   Training_Status__c
                                                           FROM SQX_Personnel_Document_Job_Function__c
                                                           WHERE Personnel_Id__c =:p1.Id
                                                           AND SQX_Requirement__c =:req1.Id 
                                                           AND Training_Status__c =''].get(0);
            
            //Assert1: The Next Refresher Date should be equal to Completed Date plus the Refresher Interval for docTraining1              
            System.assertEquals(docTraining1.Completion_Date__c.addDays((req1.Refresher_Interval__c).intValue()), pdjf1R.Next_Refresh_Date__c );
            
            //Assert2: The Refresher Queue Date should be equal to Next Refresher Date minus Days in Advance to start Refresher for docTraining1               
            System.assertEquals(pdjf1R.Next_Refresh_Date__c.addDays(-(req1.Days_in_Advance_To_Start_Refresher__c).intValue()), pdjf1R.Refresher_Queue_Date__c);
                
            Test.stopTest();
        }             
    }

    /**  
    * Given: New Document in Current state and Active Requirement.
    * When : When DT is completed  and Refresher Queue Date is Met 
    * Then : New Refresher DT is created
    */

    public static testmethod void givenCurrentDocument_WhenDocumentTrainingIsCompletedAndRefresherQueueDateIsMet_RefresherDocumentTrainingIsCreated() {
       
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1'),
                            jf2 = jfs.get('JF2'),
                            jf3 = jfs.get('JF3');
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1'),
                            p2 = psns.get('P2');

        Map<String, SQX_Test_Controlled_Document> cdocs = getControlledDocuments();
        SQX_Test_Controlled_Document    document1 = cdocs.get('document1');

        Map<String, SQX_Requirement__c> reqs = getRequirements();
        String cdocJFKey1 = '' + document1.doc.Id + jf1.Id;
        String cdocJFKey2 = '' + document1.doc.Id + jf2.Id;
        String cdocJFKey3 = '' + document1.doc.Id + jf3.Id;
        SQX_Requirement__c    req1 = reqs.get(cdocJFKey1),
                              req2 = reqs.get(cdocJFKey2),
                              req3 = reqs.get(cdocJFKey3);  

        Map<String, SQX_Test_Assessment> assessments = getAssessments();
        SQX_Test_Assessment    assessment1 = assessments.get('assessment1'),
                               assessment2 = assessments.get('assessment2');


        Map<String, SQX_Personnel_Document_Job_Function__c> pdjfs = getPdjf();
        String psnReqKey1 = '' + p1.Id + req1.Id;
        String psnReqKey2 = '' + p2.Id + req2.Id;
        String psnReqKey3 = '' + p2.Id + req3.Id;
        SQX_Personnel_Document_Job_Function__c    pdjf1 = pdjfs.get(psnReqKey1),
                                                  pdjf2 = pdjfs.get(psnReqKey2),
                                                  pdjf3 = pdjfs.get(psnReqKey3);  

        System.runas(standardUser) {
            
            //Act: Set Document Status to Current 
            document1.doc.Effective_Date__c = Date.Today();
            document1.doc.Date_Issued__c = Date.Today();
            document1.setStatus(SQX_Controlled_Document.STATUS_CURRENT);
            document1.save();

            SQX_Personnel_Document_Training__c docTraining2 = [SELECT Id, Completion_Date__c, Status__c
                                                              FROM SQX_Personnel_Document_Training__c 
                                                              WHERE Id =:pdjf2.SQX_Personnel_Document_Training__c]; 

            SQX_Personnel_Document_Training__c docTraining3 = [SELECT Id, Completion_Date__c, Status__c
                                                              FROM SQX_Personnel_Document_Training__c 
                                                              WHERE Id =:pdjf3.SQX_Personnel_Document_Training__c];
            Test.startTest();                                                 
            // Complete Assessement for Document Training 2
            

            Id paId1 = SQX_Test_Document_Training_Generation.completeAssessment(p2.Id, assessment1.assessment.Id);
            docTraining2.SQX_Personnel_Assessment__c = paId1;
            docTraining2.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            docTraining2.Completion_Date__c = Date.Today();
            new SQX_DB().op_update( new List<SQX_Personnel_Document_Training__c>{docTraining2}, new List<Schema.SObjectField>{});

            //Act: Get Completed Document Training 
            docTraining2 = [SELECT Id,  Completion_Date__c, Status__c
                                  FROM SQX_Personnel_Document_Training__c 
                                  WHERE Id =:docTraining2.Id];

            //Assert: Completion Date is set
            System.assertNotEquals(null, docTraining2.Completion_Date__c); 
            //Assert: Document Training is completed                
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, docTraining2.Status__c);                     

            SQX_Personnel_Document_Job_Function__c pdjf2R = [SELECT Id,
                                                                   SQX_Personnel_Document_Training__c, 
                                                                   Next_Refresh_Date__c, 
                                                                   Refresher_Queue_Date__c, 
                                                                   Create_Refresher__c, 
                                                                   Training_Type__c,
                                                                   Training_Status__c
                                                           FROM SQX_Personnel_Document_Job_Function__c
                                                           WHERE Personnel_Id__c =:p2.Id
                                                           AND SQX_Requirement__c =:req2.Id 
                                                           AND Training_Status__c =''].get(0);

            // Complete Document Training 3                                               
            docTraining3.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            docTraining3.Completion_Date__c = Date.Today().addDays(-30);
            Date nextRefresherDate3 = docTraining3.Completion_Date__c.addDays(60);

            new SQX_DB().op_update(new List<SObject>{docTraining3}, new List<SObjectField>{Schema.SQX_Personnel_Document_Training__c.fields.Status__c, 
                                                                                            Schema.SQX_Personnel_Document_Training__c.fields.Completion_Date__c});

            SQX_Personnel_Document_Job_Function__c pdjf3R = [SELECT Id,
                                                                   SQX_Personnel_Document_Training__c, 
                                                                   Next_Refresh_Date__c, 
                                                                   Refresher_Queue_Date__c, 
                                                                   Create_Refresher__c, 
                                                                   Training_Type__c,
                                                                   Training_Status__c
                                                           FROM SQX_Personnel_Document_Job_Function__c
                                                           WHERE Personnel_Id__c =:p2.Id
                                                           AND SQX_Requirement__c =:req3.Id 
                                                           AND Training_Status__c =''].get(0);

            //Act: Set Create Refresher Flag as true
            //     [NOTE: This flag is set through Time-Based Workflow]
            pdjf2R.Create_Refresher__c = true;
            new SQX_DB().op_update(new List<SObject>{pdjf2R}, new List<SObjectField>{});

            SQX_Personnel_Document_Training__c refresherdocTraining2 = [SELECT Id, Completion_Date__c, Status__c
                                                                        FROM SQX_Personnel_Document_Training__c 
                                                                        WHERE SQX_Controlled_Document__c = :document1.doc.Id 
                                                                        AND SQX_Personnel__c = :p2.Id
                                                                        AND Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING];
            
            // Complete Assessment for Refrsher Document Training 
            SQX_BulkifiedBase.clearAllProcessedEntities();
           
            paId1 = SQX_Test_Document_Training_Generation.completeAssessment(p2.Id, assessment2.assessment.Id);
            refresherdocTraining2.SQX_Personnel_Assessment__c = paId1;
            refresherdocTraining2.Status__c = SQX_Personnel_Document_Training.STATUS_COMPLETE;
            refresherdocTraining2.Completion_Date__c = Date.Today();
            new SQX_DB().op_update( new List<SQX_Personnel_Document_Training__c>{refresherdocTraining2}, new List<Schema.SObjectField>{});

            //Act: Get Completed Document Training 
            refresherdocTraining2 = [SELECT Id,  Completion_Date__c, Status__c
                                  FROM SQX_Personnel_Document_Training__c 
                                  WHERE Id =:refresherdocTraining2.Id];

            //Assert: RefresherDocumentTraining2 is completed
                              
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, refresherdocTraining2.Status__c);

            //Assert: New PDJF is Created with Next Refresher and Queue Refresher Date Evaluated For New PDJFs
            SQX_Personnel_Document_Job_Function__c pdjf2RR = [SELECT Id,
                                                                   SQX_Personnel_Document_Training__c, 
                                                                   Next_Refresh_Date__c, 
                                                                   Refresher_Queue_Date__c, 
                                                                   Create_Refresher__c, 
                                                                   Training_Type__c,
                                                                   Training_Status__c,
                                                                   Is_Archived__c
                                                           FROM SQX_Personnel_Document_Job_Function__c
                                                           WHERE Personnel_Id__c =:p2.Id
                                                           AND SQX_Requirement__c =:req2.Id 
                                                           AND Training_Status__c = '' ].get(0);                                            
            
            System.assertEquals(false, pdjf2RR.Create_Refresher__c);
            System.assertEquals(false, pdjf2RR.Is_Archived__c);
             
            pdjf2R = [SELECT Id,
                           SQX_Personnel_Document_Training__c, 
                           Next_Refresh_Date__c, 
                           Refresher_Queue_Date__c, 
                           Create_Refresher__c, 
                           Training_Type__c,
                           Training_Status__c,
                           Is_Archived__c
                   FROM SQX_Personnel_Document_Job_Function__c
                   WHERE Id =:pdjf2R.Id];
            //Assert: Previous PDJF is not archived unless Next Refresher Document Training is created
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_COMPLETE, pdjf2R.Training_Status__c);
            System.assertEquals(false, pdjf2R.Is_Archived__c);

            //Act: Create Refresher Document Training 
            pdjf2RR.Create_Refresher__c = true;
            new SQX_DB().op_update(new List<SObject>{pdjf2RR}, new List<SObjectField>{});

            pdjf2R = [SELECT Id,
                           Is_Archived__c
                   FROM SQX_Personnel_Document_Job_Function__c
                   WHERE Id =:pdjf2R.Id];

            //Assert: Previous PDJF is Archived
            System.assertEquals(true, pdjf2R.Is_Archived__c);

            pdjf3R = [SELECT Id,
                           SQX_Personnel_Document_Training__c, 
                           Next_Refresh_Date__c, 
                           Refresher_Queue_Date__c, 
                           Create_Refresher__c, 
                           Training_Type__c,
                           Training_Status__c,
                           Is_Archived__c
                   FROM SQX_Personnel_Document_Job_Function__c
                   WHERE Id =:pdjf3R.Id];

            //Assert: PDJF's having same Refresher Assessment as pdjf2R is updated and New Refresher Date is calculated 
            System.assertNotEquals(nextRefresherDate3, pdjf3R.Next_Refresh_Date__c);
            System.assertEquals(Date.Today().addDays(60), pdjf3R.Next_Refresh_Date__c);
            Test.stopTest();
        }             
    }

    /**  
    * Given: New Document with Active Requirement  .
    * When : When Refresher Required is Checked and Refresher Values are not set
    * Then : Validation Rule is Triggered
    */
    public static testmethod void givenActiveRequirement_WhenRefresherRequiredIsCheckedAndAllRefresherRelatedValuesAreNotSet_ValidationIsTriggered() {
        
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        Map<String, SQX_Job_Function__c> jfs = getJobFunctions();
        SQX_Job_Function__c jf1 = jfs.get('JF1');
                            
        
        Map<String, SQX_Personnel__c> psns = getPersonnels();
        SQX_Personnel__c    p1 = psns.get('P1');
                       
        Map<String, SQX_Test_Controlled_Document> cdocs = getControlledDocuments();
        SQX_Test_Controlled_Document    document1 = cdocs.get('document1');

        System.runas(standardUser) {


            //Assert 1: Save Requirement with Require Refresher True and other Refresher related fields empty
            //         It should throw an error with the message 'Refresher Interval, Refresher Competency, Days to Complete Refresher and Days in advance to Start Refresher are required if refresher is needed.' 
            
            SQX_Requirement__c req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                         SQX_Job_Function__c = jf1.Id, 
                                                         Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                         Require_Refresher__c = true
                                                         );
            Database.SaveResult result1 = new SQX_DB().continueOnError().op_insert(new List<SQX_Requirement__c> {req1 }, new List<Schema.SObjectField>{}).get(0);

            System.assert(result1.isSuccess() == false,
                'Expected Requirement not to be saved if all the Refresher Values are not provided');
            String expectedErrMsg = 'Refresher Interval, Refresher Competency, Days to Complete Refresher and Days in advance to Start Refresher are required if refresher is needed.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result1.getErrors(), expectedErrMsg),
                'Expected error message "' + expectedErrMsg + '"');


            //Assert 2: Save Requirement with Days in Advance To Start Refresher  greater than Refresher Interval.
            //         It should throw an error with the message 'Days in Advance to Start Refresher should always be less than Refresher Interval.'
            req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                         SQX_Job_Function__c = jf1.Id, 
                                                         Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                         Require_Refresher__c = true, 
                                                         Refresher_Interval__c = 1, 
                                                         Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                         Days_to_Complete_Refresher__c = 1,
                                                         Days_in_Advance_To_Start_Refresher__c = 10
                                                         );

            result1 = new SQX_DB().continueOnError().op_insert(new List<SQX_Requirement__c> {req1 }, new List<Schema.SObjectField>{}).get(0);
            System.assert(result1.isSuccess() == false,
                'Expected Requirement not to be saved');
            expectedErrMsg = 'Days in Advance to Start Refresher should always be less than Refresher Interval.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result1.getErrors(), expectedErrMsg),
                'Expected error message "' + expectedErrMsg + '"');

            //Assert 3: Save Requirement with Refresher Interval, Days to Complete Refresher and Days in Advance to Start Refresher less than zero (negative value).
            //          It should throw an error with the message 'Refresher Interval, Days to Complete Refresher and Days in Advance to Start Refresher should not be less than zero.'
            req1 = new SQX_Requirement__c(SQX_Controlled_Document__c = document1.doc.Id, 
                                                         SQX_Job_Function__c = jf1.Id, 
                                                         Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                         Require_Refresher__c = true, 
                                                         Refresher_Interval__c = -20, 
                                                         Refresher_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND, 
                                                         Days_to_Complete_Refresher__c = -30,
                                                         Days_in_Advance_To_Start_Refresher__c = -40
                                                         );

            result1 = new SQX_DB().continueOnError().op_insert(new List<SQX_Requirement__c> {req1 }, new List<Schema.SObjectField>{}).get(0);
            System.assert(result1.isSuccess() == false,
                'Expected Requirement not to be saved');
            expectedErrMsg = 'Refresher Interval, Days to Complete Refresher and Days in Advance to Start Refresher should not be less than zero.';
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result1.getErrors(), expectedErrMsg),
                'Expected error message "' + expectedErrMsg + '"');
 
        }             
    }     
}