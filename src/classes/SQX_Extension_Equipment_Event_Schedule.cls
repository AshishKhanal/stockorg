/**
* Extension class used for signing off / completing equipment event schedule.
*/
global with sharing class SQX_Extension_Equipment_Event_Schedule extends SQX_Controller_Sign_Off{
    
    private ApexPages.StandardController controller;
    private SQX_Equipment_Event_Schedule__c mainRecord;
    private transient Attachment eventHistoryAttachment;
    
    global SQX_Equipment_Event_History__c eventHistory { get; set; }
    global String isFileUploaded { get; set; }
        
    /**
    * returns eventHistoryAttachment object
    */
    global Attachment getEventHistoryAttachment() {
        if (eventHistoryAttachment == null) {
            eventHistoryAttachment = new Attachment();
        }
        
        return eventHistoryAttachment;
    }
    
    /**
     * Add default policies
     */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            SQX_Personnel_Assessment.PurposeOfSignature_SubmitingAssessmentResult => this.POLICY_NO_COMMENT_REQUIRED
            
        };
        return defaultEsigPolicies;
    }
    
    /**
    * SQX_Extension_Equipment_Event_Schedule constructor
    */
    global SQX_Extension_Equipment_Event_Schedule(ApexPages.StandardController controller){
        super(controller);
        mainRecord = (SQX_Equipment_Event_Schedule__c)controller.getRecord();
        this.controller = controller;
        
        // initialize new equipment event history
        eventHistory = new SQX_Equipment_Event_History__c();
        eventHistory.SQX_Equipment_Event_Schedule__c = mainRecord.Id;
        eventHistory.SQX_Equipment__c = mainRecord.SQX_Equipment__c;
        eventHistory.Performed_By__c = UserInfo.getName();
        
        this.purposeOfSigMap.putAll( SQX_Equipment_Event_Schedule.purposeOfSigMap );
        recordId = mainRecord.Id;
        actionName = SQX_Equipment_Event_Schedule.PurposeOfSignature_Event_Sign_Off;
        purposeOfSignature = SQX_Equipment_Event_Schedule.purposeOfSigMap.containsKey(actionName) ? SQX_Equipment_Event_Schedule.purposeOfSigMap.get(actionName) : '';
    }
    
    /**
    * sign off equipment event schedule to record maintenance history
    */
    global PageReference signOff() {
        PageReference returnUrl = null;
        SavePoint sp = null;
        boolean isValid = true;
        
        if (isFileUploaded == 'true') {
            // check for zero file size
            if (eventHistoryAttachment != null && eventHistoryAttachment.Body != null && eventHistoryAttachment.Body.size() == 0) {
                isValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_FILE_DOES_NOT_HAVE_CONTENT));
            }
            
            // check for max file size limit
            // when uploaded file is over max file size limit, body is null and page already has error.
            if (eventHistoryAttachment == null || eventHistoryAttachment.Body == null) {
                isValid = false;
            }
        }
        
        if (isValid) {
            if(hasValidCredentials()) {
                try {
                    // set equipment event history values
                    eventHistory.Id = null; // making sure Id is not passed for new record
                    eventHistory.Sign_Off_By__c = getUserDetails();
                    eventHistory.Sign_Off_Date__c = System.Now();
                    eventHistory.Scheduled_Due_Date__c = mainRecord.Next_Due_Date__c;
                    eventHistory.Comments__c = RecordActivityComment;
                    
                    // set savepoint
                    sp = Database.setSavePoint();
                    
                    // save equipment event history i.e. completed event schedule
                    Database.SaveResult saveResult = new SQX_DB().continueOnError().op_insert(new List<SQX_Equipment_Event_History__c>{ eventHistory },
                        new List<Schema.SObjectField>{ }).get(0);
                    
                    if (saveResult.isSuccess()) {
                        if (isFileUploaded == 'true') {
                            eventHistoryAttachment.Id = null; // making sure Id is not passed for new record
                            eventHistoryAttachment.ParentId = eventHistory.Id;
                            
                            // save uploaded attachment for event history
                            saveResult = new SQX_DB().continueOnError().op_insert(new List<Attachment>{ eventHistoryAttachment }, new List<Schema.SObjectField>{ }).get(0);
                        }
                        
                        if (saveResult.isSuccess() && !String.isBlank(mainRecord.Event_Task_Id__c)) {
                            // get associated task created by system
                            Id taskId = (Id)mainRecord.Event_Task_Id__c;
                            List<Task> eventTask = [SELECT Id, Status, IsArchived, IsClosed, IsDeleted FROM Task WHERE Id = :taskId ALL ROWS];
                            
                            if (eventTask.size() == 1) {
                                Task tsk = eventTask[0];
                                
                                if (tsk.IsArchived == false && tsk.IsClosed == false && tsk.IsDeleted == false && tsk.Status != 'Completed') {
                                    // close associated task if not closed or deleted
                                    tsk.Status = 'Completed';
                                    
                                    // update task
                                    /**
                                    * WITHOUT SHARING has been used 
                                    * ---------------------------------
                                    * Without sharing has been used because the user(Queue Member) who has permission to sign off Equipment Event Schedule
                                    * may not have permission to complete the related tasks 
                                    */
                                    saveResult = new SQX_DB().withoutSharing().continueOnError().op_update(new List<Task>{ tsk }, new List<Schema.SObjectField>{ }).get(0);
                                }
                            }
                        }
                        
                        if (saveResult.isSuccess()) {
                            // use a clone object of mainRecord to update in order to avoid data value issues on page when update fails
                            SQX_Equipment_Event_Schedule__c mainRecordToUpdate = mainRecord.clone(true, false, false, false);
                            // clear closed task id
                            mainRecordToUpdate.Event_Task_Id__c = '';
                            // set to queue event task to false
                            mainRecordToUpdate.Queue_Event_Task__c = false;
                            // last performed date of equipment event schedule
                            mainRecordToUpdate.Last_Performed_Date__c = eventHistory.Performed_Date__c;
                            
                            if (mainRecordToUpdate.Recurring_Event__c == true) {
                                // set calculate next due date for recurring event
                                // get interval value
                                Integer interval = mainRecordToUpdate.Recurring_Interval__c == null ? 0 : (Integer)mainRecordToUpdate.Recurring_Interval__c;
                                // calculate next due date
                                Date nextDueDate = SQX_Equipment_Event_Schedule.calculateNextDueDateByScheduleBasis(mainRecordToUpdate.Schedule_Basis__c,
                                    mainRecordToUpdate.Recurring_Unit__c, mainRecordToUpdate.Last_Performed_Date__c, mainRecordToUpdate.Next_Due_Date__c, interval);
                                // set calculated next due date
                                mainRecordToUpdate.Next_Due_Date__c = nextDueDate;
                            } else {
                                // clear next due date if  for non-recurring event to prevent task creation
                                mainRecordToUpdate.Next_Due_Date__c = null;
                            }
                            
                            // update event schedule record when event history is saved
                            saveResult = new SQX_DB().continueOnError().op_update(new List<SQX_Equipment_Event_Schedule__c>{ mainRecordToUpdate }, new List<Schema.SObjectField>{
                                SQX_Equipment_Event_Schedule__c.fields.Last_Performed_Date__c,
                                SQX_Equipment_Event_Schedule__c.fields.Next_Due_Date__c,
                                SQX_Equipment_Event_Schedule__c.fields.Event_Task_Id__c,
                                SQX_Equipment_Event_Schedule__c.fields.Queue_Event_Task__c
                            }).get(0);
                        }
                    }
                    
                    if (saveResult.isSuccess()) {
                        // return to equipment record if all records are saved
                        returnURL = new ApexPages.StandardController(new SQX_Equipment__c( Id = mainRecord.SQX_Equipment__c )).view();
                    } else {
                        // if any save record fails
                        for(Database.Error error : saveResult.getErrors()){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error.getMessage()));
                        }
                        
                        Database.rollback(sp);
                    }
                } catch (Exception ex) {
                    // ignore error cause these will have been added by the dml exceptions
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                    
                    // rollback savepoint
                    if (sp != null) {
                        Database.rollback(sp);
                    }
                }
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_INVALID_USERNAME_PASSWORD));
            }
        }
        
        return returnURL;
    }
}