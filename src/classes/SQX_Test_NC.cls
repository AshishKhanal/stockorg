/**
* this is a factory class that assists in creation of NC
* Test to ensure migration script SQX_6_2_MigrateRelatedCapasOfNC (using SQX_PostInstall_Processor)
* deletes those SQX_NC_CAPA record that contains duplicate Capa for same NC
*/
@isTest
public class SQX_Test_NC{

    // Mock parameter for response submission
    static final Map<String, String> params = new Map<String, String> {
                'comment' =>'Mock comment', 
                'purposeOfSignature' =>'Mock purposeOfSignature',  
                'nextAction' => 'submitresponse'};

    @testSetup
    public static void commonSetup() {

        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }

    public SQX_NonConformance__c nc {get; set; }
    public SQX_Test_Finding finding { get; set; }

    User adminUser = SQX_Test_Account_Factory.getUser(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
    
    public SQX_Test_NC(){
        SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                            .setRequired(false,true,true,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(false, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save();
                            
        initializeNC(finding);
    }

    public SQX_Test_NC(Boolean withoutFinding) {
        System.assert(withoutFinding, 'Please use other override for creating nc with finding. Parameter kept only to provide override until migration is complete');
        initializeNC(null);
    }
    
    public SQX_Test_NC(SQX_Test_Finding finding){
        initializeNC(finding);
    }
    
    private void initializeNC(SQX_Test_Finding finding){
        this.finding = finding;
        nc = new SQX_NonConformance__c();
        nc.Type__c = SQX_NC.TYPE_INTERNAL;
        nc.Type_Of_Issue__c =  SQX_NC.ISSUE_TYPE_PRODUCT;
        nc.SQX_Finding__c = finding != null ? finding.finding.Id : null;
        nc.SQX_Department__c = randomDepartment().Id;
        nc.Occurrence_Date__c = Date.Today();
    }

    public SQX_Department__c randomDepartment(){
        SQX_Department__c randomDepartment= new SQX_Department__c();
        System.runas(adminUser){
            randomDepartment = new SQX_Department__c(
                                                 Name='Random Department'
                                               );
            insert randomDepartment;
            
        }
        return randomDepartment;       
    }
    
    public SQX_Test_NC setStatus(String newStatus){
        nc.Status__c = newStatus;
        return this;
    }
    
    public SQX_Test_NC setDispositionRules(boolean requireDisposition, Date dispositionDueDate, boolean approveDisposition,
         User dispositionApprover){
         
         nc.SQX_Disposition_Approver__c = dispositionApprover == null ? null : dispositionApprover.Id;
         nc.Disposition_Required__c = requireDisposition;
         nc.Due_Date_Disposition__c = dispositionDueDate;
         nc.Disposition_Approval__c = approveDisposition;
         
         return this;
    }
    
    public SQX_Test_NC setTypeOfIssue(String typeOfIssue, Id productId, Id processId){
        
        nc.Type_Of_Issue__c = typeOfIssue;
        finding.finding.SQX_Part__c = productId;
        finding.finding.SQX_Service__c = processId; 
        finding.save();
        
        return this;
    }
    
    
    public SQX_Test_NC save(){
        upsert nc;
        
        return this;
    }
    
    public void synchronize(){
        
        this.NC = [SELECT Id, 
                          Type_Of_Issue__c, 
                          Type__c, 
                          Title__c, 
                          Status__c, 
                          SQX_Finding__c, 
                          Department__c,
                          SQX_Department__c, 
                          Occurrence_Date__c, 
                          OwnerId, 
                          SQX_Disposition_Approver__c  FROM SQX_NonConformance__c WHERE ID = : nc.Id];
    }


    /**
    * Allows the user to create and add an impacted part/product in the nc at one go.
    */
    public SQX_NC_Impacted_Product__c addImpactedPart(User adminUser){
      SQX_Part__c part = null;

      integer randomNumber = (Integer)(Math.random() * 1000000);

      System.runas(adminUser){
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random');
        insert partFamily;
        
        part = new SQX_Part__c(Part_Family__c = partFamily.Id,
                                          Name = '123',
                                           Part_Number__c = 'Random' + randomNumber,
                                           Part_Risk_Level__c = 3,
                                           Active__c = true);
        
        insert part;
      }

      return addImpactedPart(part, 'LOT-' + randomNumber, 100);//default to hundred items 
    }

    /**
    * adds an impacted product to the nc and returns the part/product that was used to add the impacted product.
    */
    public SQX_NC_Impacted_Product__c addImpactedPart(SQX_Part__c part, String lotNumber, Integer lotQty){

      integer randomNumber = (Integer)(Math.random() * 1000000);
      SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(SQX_Impacted_Part__c = part.Id,
                                                                            SQX_Nonconformance__c = nc.Id,
                                                                            Lot_Number__c = lotNumber,
                                                                            Lot_Quantity__c = lotQty);
            
            insert impactedProduct;

      return impactedProduct;
    }
    
    
    /******Basic test methods******/

    /**
     * Given: An NC is created and saved
     * When: Duplicate Related Capa is selected
     * Then: It shoud throw duplicate capa error.
     *
     * @date:2016/7/4
     * @author:Paras Kumar Bishwakarma
     * @story :[SQX-2114]
     */
    public static testmethod void gevenNC_WhenNCIsClosedWithDuplicateCapa_RelatedDuplicateCapaError(){
        //get the map of all created users
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        //get user from the map 
        User stdUser = allUsers.get('standardUser');
        System.runas(stdUser){
            //Arrange :  Create an NC
            SQX_Test_NC nc = new SQX_Test_NC(new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.NCType)
                            .setRequired(false,false,true,false,false) //response required, containment reqd, investigation required, ca required, pa required
                            .setApprovals(false, false, false)   //investigation approval, changes in ap approval, effectiveness review
                            .setStatus(SQX_Finding.STATUS_DRAFT)
                            .setRequiredDueDate(null, Date.Today().addDays(10), Date.Today().addDays(20)) //respond null, containment in 10 days and investigation in 20 days
                            .save());
            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();

            //Create CAPA
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility().save();

            //Create NC_CAPA
            SQX_NC_CAPA__c ncCapa = new SQX_NC_CAPA__c(SQX_CAPA__c = capa.capa.Id,
                                                        SQX_Nonconformance__c = nc.nc.Id);
            //Act : insert the ncCapa
            Database.saveResult result=Database.insert(ncCapa);
            System.assert(result.isSuccess(),'Expected the ncCapa to be saved but got error'+result.getErrors());
            
            //Create NC_CAPA again with duplicated capa
            SQX_NC_CAPA__c ncCapa1 = new SQX_NC_CAPA__c(SQX_CAPA__c = capa.capa.Id,
                                                         SQX_Nonconformance__c = nc.nc.Id);
            
            //save the same NC_CAPA in this case the capa is duplicate                                          
            Database.saveResult result1=Database.insert(ncCapa1,false);
            //Assert: Expected error (from custom label) is to be thrown
            System.assert(result1.getErrors()[0].getStatusCode()==StatusCode.DUPLICATE_VALUE,'Expected error status code to be '+StatusCode.DUPLICATE_VALUE+' but found'+result1.getErrors()[0].getStatusCode());
        }
    }

    /**
    * Given: SQX_NC_CAPA with duplicate related Capa
    * When: Upgrade
    * Then: Migration Script Runs which deletes duplicate related capa associated with same NC.
    * 
    * @author: Paras Kumar Bishwakarma
    * @date: 2016/7/26
    * @story: [SQX-2114]
    */
    public static testmethod void givenNcCapaWithDuplicateRelatedCapa_AfterUpgrade_MigrationScriptRuns() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        System.assert(standardUser != null);
        System.runas(standardUser){

            //bypassForUnitTests is set true as we need duplicate related capa test so uniqueness constraint is dissable.
            SQX_BulkifiedBase.bypassForUnitTests = true;

            //Arrange :  Create an NC
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();

            //Create CAPA
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility().save();

            //Create NC_CAPA
            SQX_NC_CAPA__c ncCapa = new SQX_NC_CAPA__c(SQX_CAPA__c = capa.capa.Id,
                                                        SQX_Nonconformance__c = nc.nc.Id);
            //Act : insert the ncCapa
            Database.saveResult result=Database.insert(ncCapa);
            System.assert(result.isSuccess(),'Expected the ncCapa to be saved but got error'+result.getErrors());
            
            //Create NC_CAPA again with duplicated capa
            SQX_NC_CAPA__c ncCapa1 = new SQX_NC_CAPA__c(SQX_CAPA__c = capa.capa.Id,
                                                         SQX_Nonconformance__c = nc.nc.Id);
            
            //save the same NC_CAPA in this case the capa is duplicate                                          
            Database.saveResult result1=Database.insert(ncCapa1,false);

            //Assert: Expected no error as we have set bypassForUnitTests to true
            System.assert(result1.isSuccess(),'Expected the ncCapa to be saved but got error'+result.getErrors());

            //Assert: Expected the SQX_NC_CAPA's size to be 2
            List<SQX_NC_CAPA__c> duplicateNcCapaList = [SELECT Id, SQX_Nonconformance__c, SQX_CAPA__c from SQX_NC_CAPA__c WHERE SQX_Nonconformance__c = :nc.nc.Id AND SQX_CAPA__c = :capa.capa.Id];
            System.assertEquals(2,duplicateNcCapaList.size(),'Expected the of uniqeNcCapaList to be 2 but found'+duplicateNcCapaList.size());

            //bypassForUnitTests is set false so uniqueness constraint is enabled.
            SQX_BulkifiedBase.bypassForUnitTests = false;

            Test.startTest();

            // ACT: start post install migration
            SQX_PostInstall_Processor processor = new SQX_PostInstall_Processor();
            Test.testInstall(processor, new Version(6,1));

            Test.stopTest();

            //get the ncCapa with unique nc+capa
            List<SQX_NC_CAPA__c> uniqeNcCapaList = [SELECT Id, SQX_Nonconformance__c, SQX_CAPA__c from SQX_NC_CAPA__c WHERE SQX_Nonconformance__c =:nc.nc.Id AND SQX_CAPA__c =:capa.capa.Id];

            //Assert expected the size of uniqeNcCapaList to be one
            System.assertEquals(1,uniqeNcCapaList.size(),'Expected the of uniqeNcCapaList to be 1 but found'+uniqeNcCapaList.size());
        }
    }
    
    /**
     * GIVEN : A NC is created and impacted product is added
     * WHEN : defect is added or updated with defective quantity greater than lot quantity
     * THEN : Error is thrown
     * 
     * WHEN : Defect is added or updated with defective quantity less than or equal to lot quantity
     * THEN : Save is successful
     * @date : 2017/12/19
     * @story : SQX-3202
     */
    public static testmethod void givenNC_WhenDefectIsGreaterThanImpactedProduct_ErrorIsThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        User adminUser = users.get('adminUser');
            
        SQX_Part__c part = null;
        integer randomNumber = (Integer)(Math.random() * 1000000);
        
        System.runAs(adminUser){
            
            System.runas(adminUser){
                SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random');
                insert partFamily;
                
                part = new SQX_Part__c(Part_Family__c = partFamily.Id,
                                       Name = '123',
                                       Part_Number__c = 'Random' + randomNumber,
                                       Part_Risk_Level__c = 3,
                                       Active__c = true);
                
                insert part;
            }
        }
        System.runas(standardUser){

            //Arrange :  A NC is created and impacted product and defect is added
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            
            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(SQX_Impacted_Part__c = part.Id, 
                                                                                        Lot_Number__c = '1', 
                                                                                        Lot_Quantity__c = 10, 
                                                                                        SQX_Nonconformance__c = nc.nc.Id);
            insert impactedProduct;
            
            SQX_NC_Defect__c ncDefect = new SQX_NC_Defect__c(SQX_Part__c = part.Id, 
                                                             Lot_Ser_Number__c = '1', 
                                                             Defect_Code__c = '1', 
                                                             Defect_Category__c = '1',
                                                             Defective_Quantity__c = 20,
                                                             Number_of_defects__c = 10, 
                                                             SQX_Nonconformance__c = nc.nc.Id);
            
            // ACT : Defect is inserted with quantity greater than impacted product
            Database.SaveResult result = Database.insert(ncDefect, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Insert fails');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.CQ_UI_Defective_Quantity_is_greater_than_Lot_Quantity));
            
            ncDefect = new SQX_NC_Defect__c(SQX_Part__c = part.Id, 
                                                             Lot_Ser_Number__c = '1', 
                                                             Defect_Code__c = '1', 
                                                             Defect_Category__c = '1',
                                                             Defective_Quantity__c = 5,
                                                             Number_of_defects__c = 10, 
                                                             SQX_Nonconformance__c = nc.nc.Id);
            
            // ACT : Defect is inserted with quantity less than impacted product
            result = Database.insert(ncDefect, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Insert passes');
            
            // ACT :Another Defect is inserted with quantity greater than impacted product
            SQX_NC_Defect__c ncDefect1 = new SQX_NC_Defect__c(SQX_Part__c = part.Id, 
                                                             Lot_Ser_Number__c = '1', 
                                                             Defect_Code__c = '1', 
                                                             Defect_Category__c = '1',
                                                             Defective_Quantity__c = 10,
                                                             Number_of_defects__c = 10, 
                                                             SQX_Nonconformance__c = nc.nc.Id);
            
            result = Database.insert(ncDefect1, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Insert fails');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.CQ_UI_Defective_Quantity_is_greater_than_Lot_Quantity), result.getErrors());
            
            // ACT : Defect is inserted with quantity less than impacted product
            ncDefect1 = new SQX_NC_Defect__c(SQX_Part__c = part.Id, 
                                                             Lot_Ser_Number__c = '1', 
                                                             Defect_Code__c = '1', 
                                                             Defect_Category__c = '1',
                                                             Defective_Quantity__c = 5,
                                                             Number_of_defects__c = 10, 
                                                             SQX_Nonconformance__c = nc.nc.Id);
            
            result = Database.insert(ncDefect1, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Insert passes');
            
            // ACT :Another Defect is updated with quantity greater than impacted product
            ncDefect1.Defective_Quantity__c = 6;
            result = Database.update(ncDefect1, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Update fails');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.CQ_UI_Defective_Quantity_is_greater_than_Lot_Quantity));
            
            // ACT :Another Defect is updated with quantity less than or equal than impacted product
            ncDefect1.Defective_Quantity__c = 5;
            result = Database.update(ncDefect1, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Update passes');
            
        }
    }
    
    /**
     * GIVEN : A NC is created and impacted product and defect is added
     * WHEN : impacted product is updated with quantity less than defect
     * THEN : Error is thrown
     *
     * WHEN : impacted product is updated with quantity greated than defect
     * THEN : Save is successful
     * @date : 2017/12/19
     * @story : SQX-3202
     */
    public static testmethod void givenNC_WhenImpactedProductIsLessThanDefect_ErrorIsThrown() {
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        User adminUser = users.get('adminUser');
            
        SQX_Part__c part = null;
        integer randomNumber = (Integer)(Math.random() * 1000000);
        
        System.runAs(adminUser){
            SQX_Part_Family__c partFamily = new SQX_Part_Family__c(Name = 'Random');
            insert partFamily;
            
            part = new SQX_Part__c(Part_Family__c = partFamily.Id,
                                   Name = '123',
                                   Part_Number__c = 'Random' + randomNumber,
                                   Part_Risk_Level__c = 3,
                                   Active__c = true);
            
            insert part;
        }
        System.runas(standardUser){

            //Arrange :  A NC is created and impacted product and defect is added
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            
            SQX_NC_Impacted_Product__c impactedProduct = new SQX_NC_Impacted_Product__c(SQX_Impacted_Part__c = part.Id, 
                                                                                        Lot_Number__c = '1', 
                                                                                        Lot_Quantity__c = 10, 
                                                                                        SQX_Nonconformance__c = nc.nc.Id);
            insert impactedProduct;
            
            SQX_NC_Defect__c ncDefect = new SQX_NC_Defect__c(SQX_Part__c = part.Id, 
                                                             Lot_Ser_Number__c = '1', 
                                                             Defect_Code__c = '1', 
                                                             Defect_Category__c = '1',
                                                             Defective_Quantity__c = 10,
                                                             Number_of_defects__c = 10, 
                                                             SQX_Nonconformance__c = nc.nc.Id);
            
            Database.SaveResult result = Database.insert(ncDefect, false);
            System.assert(result.isSuccess(), 'Insert passes');
            
            // ACT : impacted product is updated with quantity less than defect
            impactedProduct.Lot_Quantity__c = 5;
            result = Database.update(impactedProduct, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Update fails');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), Label.CQ_UI_Defective_Quantity_is_greater_than_Lot_Quantity), result.getErrors());
            
            // ACT : impacted product is updated with quantity more than defect
            impactedProduct.Lot_Quantity__c = 20;
            result = Database.update(impactedProduct, false);
            
            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Update passes');
            
        }
    }

    /**
     * Given : NC with response for investigation is in approval
     * When : NC is voided
     * Then : Response is recalled
     */
    public testmethod static void givenNCResponse_WhenNCIsVoided_ResponseIsRecalled(){
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get('standardUser');
        
        System.runas(standardUser){
            SQX_Test_NC nc = new SQX_Test_NC();
            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.finding.setRequired(true,true,true,false,false);

            // Arrange : create nc, construct a response and submit for approval
            ApexPages.StandardController sc = new ApexPages.StandardController(nc.nc);
            SQX_Extension_NC controller = new SQX_Extension_NC(sc);
        
            controller.initializeTemporaryStorage();
            
            SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_Nonconformance__c = nc.nc.Id,
                                                                          Response_Summary__c = 'Response');
            
            SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_Nonconformance__c = nc.nc.Id,
                                                                         Investigation_Summary__c = 'Is');
            
            SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');
            
            Map<String,Object>  changeSet = new Map<String, Object>(),
                                responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped
            
            // set virtual IDSs
            responseUT.put('Id', 'Response-1');
            investigationUT.put('Id', 'Investigation-1');
            inclusion2UT.put('Id', 'Inclusion-2');
            
            inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
            inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
            
            List<Object> changes = new List<Object>();
            
            changes.add(responseUT);
            changes.add(investigationUT);
            changes.add(inclusion2UT);
            
            changeSet.put('changeSet', changes);
        
            System.assertEquals(SQX_Extension_UI.OK_STATUS,
                SQX_Extension_UI.submitResponse(nc.nc.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
        
            System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_Nonconformance__c = : nc.nc.Id].size());

            /* ensure we are not very close to the limit */
            System.assert(((double)Limits.getDMLStatements() / Limits.getLimitDMLStatements()) < 0.5,
             'Limit too close ' + Limits.getDMLStatements() + ' out of ' + Limits.getLimitDMLStatements() );

            response = [SELECT Id, Status__c FROM SQX_Finding_Response__c WHERE SQX_Nonconformance__c =: nc.nc.Id ];

            Test.startTest();
            SQX_BulkifiedBase.clearAllProcessedEntities();
                
            // Act : void nc
            nc.nc.Resolution__c = SQX_Finding.RESOLUTION_VOID;
            nc.nc.Status__c = SQX_CAPA.STATUS_CLOSED;
            nc.nc.Closure_Comment__c = 'NC voided';
            new SQX_DB().op_update(new List<SQX_Nonconformance__c> {nc.nc}, new LiST<Schema.SObjectField> {});

            // Assert : response should be recalled and status to be void
            response = [SELECT Id, Status__c, Approval_Status__c FROM SQX_Finding_Response__c WHERE Id = : response.Id];
            System.assertEquals(SQX_Finding_Response.APPROVAL_STATUS_RECALLED, response.Approval_Status__c, 'Expected approval status of response to be recalled whenever nc is voided.');
            System.assertEquals(SQX_Finding_Response.STATUS_VOID, response.Status__c, 'Expected status of response to be void whenever nc is voided.');
        }
    }

    /**
     * Given NC without completion date
     * When tasks are completed i.e. has response, investigation and containment etc
     * Then completion date is set correctly
     */
    static testmethod void givenNCWithoutCompletionDate_WhenTaskssAreCompleted_ThenCompletionDateIsSet() {
        SQX_NC.Bulkified ncBulk = new SQX_NC.Bulkified();
        SQX_Nonconformance__c nc = new SQX_Nonconformance__c(Has_Response__c = true);

        //Act: Invoke the method to set completion dates
        ncBulk.setCompletionDates(nc);

        //Assert: Ensure that completion date of response is set
        System.assertEquals(Date.Today(), nc.Completion_Date_Response__c);
        System.assertEquals(null, nc.Completion_Date_Investigation__c);

        //Arrange: Set has investigation completion and has containment
        nc.Has_Investigation__c = true;
        nc.Has_Containment__c = true;

        // Act: Invoke method to set completion dates
        ncBulk.setCompletionDates(nc);

        // Assert: ensure that completion date of containment and investigation is set
        System.assertEquals(Date.Today(), nc.Completion_Date_Containment__c);
        System.assertEquals(Date.Today(), nc.Completion_Date_Investigation__c);


    }

    /**
     * Given NC with completion date
     * When tasks are completed i.e. has response, investigation and containment etc
     * Then completion date is not updated
     */
    static testmethod void givenNCWithCompletionDate_WhenTaskssAreCompleted_ThenCompletionDateIsntSet() {
        SQX_NC.Bulkified ncBulk = new SQX_NC.Bulkified();
        SQX_Nonconformance__c nc = new SQX_Nonconformance__c(Has_Response__c = true, Completion_Date_Response__c = Date.today().addDays(-2),
            Has_Investigation__c = true, Completion_Date_Investigation__c = Date.today().addDays(-3),
            Has_Containment__c = true, Completion_Date_Containment__c = Date.today().addDays(-4));

        //Act: Invoke the method to set completion dates
        ncBulk.setCompletionDates(nc);

        //Assert: Ensure that completion date of response is set
        System.assertEquals(Date.Today().addDays(-2), nc.Completion_Date_Response__c);
        System.assertEquals(Date.Today().addDays(-3), nc.Completion_Date_Investigation__c);
        System.assertEquals(Date.Today().addDays(-4), nc.Completion_Date_Containment__c);
    }
}