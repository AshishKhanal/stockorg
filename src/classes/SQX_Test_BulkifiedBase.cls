/**
* This class tests for the functionality provided by SQX_BulkifiedBase, these include functionality provided by rule class
* and processed entities.
* @author Pradhanta Bhandari
* @date 2014/5/4
*/
@IsTest
public class SQX_Test_BulkifiedBase {

    static final Contact CONTACT_1 = new Contact(LastName =  'Ramesh', Email = 'ramesh@gmail.com', Fax = '123123123'),
                  CONTACT_2 = new Contact(LastName = 'Dinesh', Email = 'dinesh@gmail.com', Fax = '123123124'),
                  CONTACT_3 = new Contact(LastName = 'Binay', Email = 'binay@gmail.com', Fax = '123123123'),
                  OLD_CONTACT_1 = new Contact(LastName =  'Ramesh', Email = 'ramesh@gmail.com', Fax = '123123123'),
                  OLD_CONTACT_2 = new Contact(LastName =  'Dinesh', Email = 'dinesh@gmail.com', Fax = '123123124'),
                  OLD_CONTACT_3 = new Contact(LastName =  'Binay', Email = 'binay@gmail.com', Fax = '123123123');

    /**
    * asserts long text field tracking is processed for an object type
    */
    public static void assertLongTextFieldTrackingForUpdatedObjectType(SObjectType objType) {
        String sObjTypeName = objType.getDescribe().getName();
        
        // ASSERT: check updated object type is processed for long text history tracking
        System.assert(SQX_BulkifiedBase.trackedLongTextFieldsCache.containsKey(sObjTypeName.toLowerCase()),
            'Expected ' + sObjTypeName + ' object type is processed for long text history tracking when related object updated successfully.');
    }
    
    /**
     * Helper method to create a bulikified base object with given parameter
     * @return returns the bulkified base with given data for test
     */
    static SQX_BulkifiedBase createBaseWithNewOnly() {

        Contact[] contacts = new Contact[] { CONTACT_1, CONTACT_2, CONTACT_3 };

        return new SQX_BulkifiedBase(contacts, null);
    }

    /**
     * Helper method to create a bulkified base object for test. It will contain both new and old values
     * @return returns the bulkified base with both new and old values
     */
    static SQX_BulkifiedBase createBaseWithBothNewAndOldValues() {
        Contact[] newContacts = new Contact[] { CONTACT_1, CONTACT_2, CONTACT_3 };
        Contact[] oldContacts = new Contact[] { OLD_CONTACT_1, OLD_CONTACT_2, OLD_CONTACT_3 };

        for(Integer i = 0; i < newContacts.size(); i++) {
            Id contactId = SQX_SObject_Generator.getFakeId(Contact.SObjectType);
            newContacts[i].Id = contactId;
            oldContacts[i].Id = contactId;
        }

        return new SQX_BulkifiedBase(newContacts, new Map<Id, Contact>(oldContacts));
    }

    /**
    * Given: We have contacts
    * When: we invoke a filter with equality match on email
    * Then: only items with matching email are returned
    */
    static testmethod void givenRecords_WhenEqualityFilterIsInvoked_ThenOutputHasEqualItemsOnly(){

        // Arrange: Create rule with email equality test
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, CONTACT_1.Email);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithNewOnly();

        // Act: Apply the equality filter for contact email
        bulkifiedBase.applyFilter(rule1, SQX_BulkifiedBase.RuleCheckMethod.OnCreate);

        // Assert: Esnure that only single matching item is returned
        System.assertEquals(1, bulkifiedBase.view.size(), 'Expected 1 object to match the email rule but found ' + bulkifiedBase.view);
    }


    /**
    * Given: We have contacts
    * When: we invoke a filter with inequality match on fax
    * Then: only items with not matching fax are returned
    */
    static testmethod void givenRecords_WhenInEqualityFilterIsInvoked_ThenOutputHasEqualItemsOnly(){

        // Arrange: Create rule with fax inequality test
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.NotEquals, CONTACT_3.Fax);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithNewOnly();

        // Act: Apply the equality filter for contact email
        bulkifiedBase.applyFilter(rule1, SQX_BulkifiedBase.RuleCheckMethod.OnCreate);

        // Assert: Ensure that 1 contact is returned because contact 2 has a different fax number
        System.assertEquals(1, bulkifiedBase.view.size(), 'Expected 1 object to not match the fax rule but found ' + BulkifiedBase.view);
    }

    /**
     * Given: We have contacts
     * When: we invoke a filter with equality match on fax and inequality on email
     * Then: only items with no items are returned because no record match the criteria
     */
    static testmethod void givenRecords_WhenCompoundFilterWithBothEqualityAndInEqualityIsInvoked_ThenOutputHasMatchingItems() {
        // Arrange: Create a rule1  Email != Contact1.Email AND Fax == Contact1.Fax (Contact 3)
        //                   rule2  Email == Contact1.Email OR Fax != Contact1.Fax (Contact 1, Contact 2)
        //                   rule3  Email != Contact1.Email OR Fax == Contact1.Fax (Contact 1, Contact 2, Contact 3)
        //                   rule4  Email != Contact1.Email AND Fax != Contact1.Fax (Contact 2)
        //                   rule5  Email != Contact1.Email AND Fax != Contact3.Fax AND Email == Contact2.Email (Contact 2)
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Email);
        rule1.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Fax);

        SQX_BulkifiedBase.Rule rule2 = new SQX_BulkifiedBase.Rule();
        rule2.operator = SQX_BulkifiedBase.RuleCombinationOperator.OpOr;
        rule2.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Email);
        rule2.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Fax);

        SQX_BulkifiedBase.Rule rule3 = new SQX_BulkifiedBase.Rule();
        rule3.operator = SQX_BulkifiedBase.RuleCombinationOperator.OpOr;
        rule3.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Email);
        rule3.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Fax);

        SQX_BulkifiedBase.Rule rule4 = new SQX_BulkifiedBase.Rule();
        rule4.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Email);
        rule4.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Fax);

        SQX_BulkifiedBase.Rule rule5 = new SQX_BulkifiedBase.Rule();
        rule5.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_1.Email);
        rule5.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.NotEquals, (Object) CONTACT_3.Fax);
        rule5.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_2.Email);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithNewOnly();

        // Act: Apply the equality filter for contact email
        bulkifiedBase.groupByRules(new SQX_BulkifiedBase.Rule[] { rule1, rule2, rule3, rule4, rule5 }, SQX_BulkifiedBase.RuleCheckMethod.OnCreate);

        // Assert:  Ensure rule1 has 1 records i.e. Contact 1 and Contact 3
        //          Ensure rule2 has 2 records i.e. Contact 1 and Contact 2
        //          Ensure rule3 has 1 records i.e. Contact 1, Contact 2 and Contact 3
        //          Ensure rule4 has 1 records i.e. Contact 2
        //          Ensure rule5 has 1 records i.e. Contact 2
        System.assertEquals(1, rule1.evaluationResult.size(), 'Found' + rule1.evaluationResult);
        System.assertEquals(2, rule2.evaluationResult.size(), 'Found' + rule2.evaluationResult);
        System.assertEquals(3, rule3.evaluationResult.size(), 'Found' + rule3.evaluationResult);
        System.assertEquals(1, rule4.evaluationResult.size(), 'Found' + rule4.evaluationResult);
        System.assertEquals(1, rule5.evaluationResult.size(), 'Found' + rule5.evaluationResult);
    }


    /**
    * Given: Contact Records that have their email edited
    * When: Filter is applied to match the contact1.email; contact1.email & contact3.fax and contact1.email & contact2.fax
    * Then: 1 record is found for first filter, 1 for second and 0 for last
    */
    static testmethod void givenContactRecordsWhichHaveBeenEditedToMeet_WhenFilterIsApplied_ThenRecordsAreSelectedCorrectly(){
        // Arrange: Create rules
        //          1. simple rule email equals 'ramesh@gmail.com' after edit i.e. oldContact had a different email => Just 1 output
        //          2. email equals 'ramesh@gmail.com' and fax equals '123123123' => 1 should meet the criteria
        //          3. email equals 'ramesh@gmail.com' and fax equals '123123124' => 0 should match
        //          4. email equals 'ramesh@gmail.com' after edit => 0 because we are reverting
        OLD_CONTACT_1.Email = 'rajesh@gmail.com';
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Email);

        SQX_BulkifiedBase.Rule rule2 = new SQX_BulkifiedBase.Rule();
        rule2.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Email);
        rule2.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_3.Fax);

        SQX_BulkifiedBase.Rule rule3 = new SQX_BulkifiedBase.Rule();
        rule3.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Email);
        rule3.addRule(Schema.Contact.Fax, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_2.Fax);

        // Act: Apply the filter with on create and edit
        bulkifiedBase.groupByRules(new SQX_BulkifiedBase.Rule[] { rule1, rule2, rule3 }, SQX_BulkifiedBase.RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        // Assert: ensure rule 1, rule 2 and rule 3 have 1, 1 and 0 records respectively.
        System.assertEquals(1, rule1.evaluationResult.size(), 'Expected 1 object to match the email rule but found ' + rule1.evaluationResult);
        System.assertEquals(1, rule2.evaluationResult.size(), 'Expected 1 object to match the email rule but found ' + rule2.evaluationResult);
        System.assertEquals(0, rule3.evaluationResult.size(), 'Expected 0 object to match the email rule but found ' + rule3.evaluationResult);

    }

    /**
    * Given: Contact Records are edited with no change to field of interest (previously met)
    * When: Filter is applied to match the contact1.email
    * Then: 0 record is selected (since it previously met the criteria)
    */
    static testmethod void givenContactWhichPreviouslyMet_WhenMeetSubsequentFilterIsApplied_ThenRecordsAreSelectedCorrectly(){
        // Arrange: Create rules
        //          1. email equals 'ramesh@gmail.com' after edit => 0 because there is no change between old and new contact
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Equals, (Object) CONTACT_1.Email);

        // Act: Apply the filter with on create and edit
        bulkifiedBase.groupByRules(new SQX_BulkifiedBase.Rule[] { rule1 }, SQX_BulkifiedBase.RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        // Assert: ensure rule 1 selects 0 records.
        System.assertEquals(0, rule1.evaluationResult.size(), 'Expected 0 object to match the email rule but found ' + rule1.evaluationResult);

    }

    /**
    * Given: We have contact record with email XYZ which is edited to PQR
    * When: Transition filter is applied to for identifying items that transitioned from XYZ -> PQR
    * Then: The contact record is filtered
    */
    public static testmethod void givenItTransitionedFromXToY_FilteredOutputIsCorrect(){

        // Arrange: simple rule email transitioned from abc@gmail.com -> 'ramesh@gmail.com' => Just 1 output
        OLD_CONTACT_1.Email = 'abc@gmail.com';
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addTransitionRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.Transitioned, (Object) CONTACT_1.Email, (Object)OLD_CONTACT_1.Email);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        // Act: apply the rule
        bulkifiedBase.applyFilter(rule1, SQX_BulkifiedBase.RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        System.assertEquals(1, bulkifiedBase.view.size(), 'Expected 1 object to match the email rule but found ' + bulkifiedBase.view);

    }

    /**
    * Given: A records email has been modified
    * When: a filter to check for change is applied
    * Then: the record is selected
    */
    public static testmethod void givenItHasChanged_FilteredOutputIsCorrect(){
        // Arrange: simple rule email has changed
        OLD_CONTACT_1.Email = 'abc@gmail.com';
        SQX_BulkifiedBase.Rule rule1 = new SQX_BulkifiedBase.Rule();
        rule1.addRule(Schema.Contact.Email, SQX_BulkifiedBase.RuleOperator.HasChanged);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        // Act: apply the filter
        bulkifiedBase.applyFilter(rule1, SQX_BulkifiedBase.RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

        System.assertEquals(1, bulkifiedBase.view.size(), 'Expected 1 object to match the email rule but found ' + bulkifiedBase.view);

        //default constructor
        SQX_BulkifiedBase base2 = new SQX_BulkifiedBase(); //coverage only
    }


    /**
     * Given: Contacts have all the same owner
     * When: Ids for owner field are fetched
     * Then: only distinct values are returned
     */
    static testmethod void givenAllContactsHaveSameOwner_WhenIdsForFieldAreRequested_ThenDistinctValuesAreRequired(){
        //Arrange: create contacts with user id
        CONTACT_1.OwnerId = UserInfo.getUserId();
        CONTACT_2.OwnerId = UserInfo.getUserId();
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        // Assert: ensure that when only one id is returned since it is the only distinct value
        System.assertEquals(1, bulkifiedBase.getIdsForField(Schema.Contact.OwnerId).size(), 'Expected 1 owner id to be got but found some other value');
    }

    /**
     * Given: Contacts have all the different owner
     * When: Ids for owner field are fetched
     * Then: all owners are returned
     */
    static testmethod void givenAllContactsHaveDistinctOwner_WhenIdsForFieldAreRequested_ThenAllValuesAreRequired(){
        //Arrange: create contacts with user id
        CONTACT_1.OwnerId = UserInfo.getUserId();
        CONTACT_2.OwnerId = SQX_SObject_Generator.getFakeId(User.SObjectType);
        CONTACT_3.OwnerId = SQX_SObject_Generator.getFakeId(User.SObjectType);
        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        // Assert: ensure that when only one id is returned since it is the only distinct value
        System.assertEquals(3, bulkifiedBase.getIdsForField(Schema.Contact.OwnerId).size(), 'Expected 3 owner id to be got but found some other value');
    }

    /**
    * When: Records are sent to be marked as processed (addToProcessedRecordsFor) an action
    * Then: Records are marked as processed for the action
    */
    static testmethod void givenRecords_WhenAddedToProcessedRecordsForAnAction_ThenAllRecordsHaveBeenMarkedAsProcessedForTheAction(){
        String ACTION_NAME = 'PROCESS_CONTACTS',
               OBJECT_NAME = 'Contact';

        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        // Act: Add the records to processed list
        bulkifiedBase.addToProcessedRecordsFor(OBJECT_NAME, ACTION_NAME, new Contact[] { CONTACT_1, CONTACT_2, CONTACT_3});

        //Assert: Expected method to return List with 3 Ids
        System.assertEquals(3, bulkifiedBase.getProcessedEntitiesFor(OBJECT_NAME, ACTION_NAME).size(), 'Expected to return list of Ids with 3 elements');
    }

    /**
    * When: Records are sent to be marked as processed (addToProcessedRecordsFor) an action
    * Then: Records aren't marked as processed for a different action
    */
    static testmethod void givenRecords_WhenAddedToProcessedRecordsForAnAction_ThenAllRecordsHaventBeenMarkedAsProcessedForAnotherAction(){
        String ACTION_NAME = 'PROCESS_CONTACTS',
               OBJECT_NAME = 'Contact',
               ANOTHER_ACTION_NAME = 'ANOTHER_ACTION';

        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();

        bulkifiedBase.addToProcessedRecordsFor(OBJECT_NAME, ACTION_NAME, new Contact[] { CONTACT_1, CONTACT_2, CONTACT_3});

        //Assert: Expected 0 records since all were marked as processed
        System.assertEquals(0, bulkifiedBase.getProcessedEntitiesFor(OBJECT_NAME, ANOTHER_ACTION_NAME).size(), 'Expected no elements' + bulkifiedBase.getProcessedEntitiesFor(OBJECT_NAME, ANOTHER_ACTION_NAME));
    }

    /**
     * Given: Contact1 and Contact 2 have been processed for Action 1
     * When: Remove processed records for Action 1 is called
     * Then: View has Contact 3 only
     */
    static testmethod void givenRecordsAreProcessed_WhenRemovingProcessedRecords_ThenOnlyPreviouslyMarkedRecordsAreRemoved(){
        // Arrange: Contact1 and 2 are marked as processed.
        final String ACTION_NAME = 'PROCESS_CONTACTS',
               OBJECT_NAME = 'Contact';

        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();
        bulkifiedBase.addToProcessedRecordsFor(OBJECT_NAME, ACTION_NAME, new Contact[] { CONTACT_1, CONTACT_2});
        //Simulate SOQL query to change key
        Contact[] ct = [SELECT Id FROM Contact];

        // Act: remove all processed records
        bulkifiedBase.removeProcessedRecordsFor(OBJECT_NAME, ACTION_NAME);

        //Assert: Ensure only contact 3 is left
        System.assertEquals(1, bulkifiedBase.view.size());
        System.assertEquals(CONTACT_3, bulkifiedBase.view[0], 'Contact 3 should be left as unprocessed');
    }

    /**
     * Given: Contact 1, Contact 2 and Contact 3 have been processed for Action 1
     * When: rollback occurs prior to removal of processed records for Action 1 is called
     * Then: View has Contact 1, 2 and 3 only
     */
    static testmethod void givenRecordsAreMarkedasProcessed_WhenRemovingProcessedRecordsAfterRollBack_ThenAllRecordsAreRemoved(){
        // Arrange: Contact1 and 2 are marked as processed.
        final String ACTION_NAME = 'PROCESS_CONTACTS',
               OBJECT_NAME = 'Contact';

        SQX_BulkifiedBase bulkifiedBase = createBaseWithBothNewAndOldValues();
        //Simulate SOQL query to change key
        Contact[] ct = [SELECT Id FROM Contact];
        bulkifiedBase.addToProcessedRecordsFor(OBJECT_NAME, ACTION_NAME, new Contact[] { CONTACT_1, CONTACT_2, CONTACT_3});

        // Act: reset limit and remove processed records
        Test.startTest();
        bulkifiedBase.removeProcessedRecordsFor(OBJECT_NAME, ACTION_NAME);
        Test.stopTest();

        //Assert: Ensure only contact 3 is left
        System.assertEquals(3, bulkifiedBase.view.size());
    }


}