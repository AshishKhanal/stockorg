/**
 * test class for regulatory information
 */
@isTest
public class SQX_Test_7253_Regulatory_Information {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        System.runAs(adminUser){
            SQX_Test_Part.insertPart(new SQX_Part__c(), adminUser, true, '');
        }
    }
    
    /**
     * GIVEN : Regulatory Information Record
     * WHEN : Duplicate RI is created
     * THEN: Error is thrown
     */
    public static testMethod void givenRegulatoryInformation_WhenDuplicateRecordIsCreated_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            // ARRANGE : Regulatory Record is created
            SQX_Part__c part = [SELECT Id FROM SQX_Part__c].get(0);
            SQX_Regulatory_Information__c ri = new SQX_Regulatory_Information__c(SQX_Part__c = part.Id, Is_Active_PRI__c = true, Market_Country__c = 'US');
            Database.SaveResult result = Database.insert(ri, false);
            
            System.assert(result.isSuccess(), 'Save is successful');
            
            // ACT : Inactive duplicate record is added
            ri = new SQX_Regulatory_Information__c(SQX_Part__c = part.Id, Is_Active_PRI__c = false, Market_Country__c = 'US');
            result = Database.insert(ri, false);
            
            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is successful');
            
            // ACT : Active duplicate record is added
            ri = new SQX_Regulatory_Information__c(SQX_Part__c = part.Id, Is_Active_PRI__c = true, Market_Country__c = 'US');
            result = Database.insert(ri, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Duplicate Record Already Exist'), result.getErrors());
        }
    }

    /**
     * GIVEN : Complaint is created
     * WHEN : Regulatory Information is saved
     * THEN : Either part or part family is required
     */
    @IsTest
    static void givenComplaint_WhenRegulatoryInformationIsSaved_EitherPartOrPartFamilyIsRequired(){
      
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            // ARRANGE : Regulatory Record is created
            SQX_Part__c part = [SELECT Id, Part_Family__c FROM SQX_Part__c].get(0);

            // ACT : Regulatory information is added without part and part family
            SQX_Regulatory_Information__c ri = new SQX_Regulatory_Information__c(Is_Active_PRI__c = true, Market_Country__c = 'US');
            Database.SaveResult result = Database.insert(ri, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Either Part or Part Family is required.'), result.getErrors());

            // ACT : Regulatory information is added with part
            ri.SQX_Part__c = part.Id;
            result = Database.insert(ri, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is not successful ' + result.getErrors());

            // ACT : Regulatory information is added with part family
            ri.SQX_Part__c = null;
            ri.SQX_Part_Family__c = part.Part_Family__c;
            result = Database.update(ri, false);

            // ASSERT : Record is saved
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());
            
            // ACT : Regulatory information is added with part and part family
            ri.SQX_Part__c = part.Id;
            ri.SQX_Part_Family__c = part.Part_Family__c;
            result = Database.update(ri, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Either Part or Part Family is required.'), result.getErrors());
        }
    }
}