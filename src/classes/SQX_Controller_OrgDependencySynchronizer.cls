/**
 * This controller is used to fetch all objects that have org field dependencies defined.
 */
public class SQX_Controller_OrgDependencySynchronizer {
    public static final String TYPE_PICKLIST = 'PICKLIST',
                        CUSTOM_OBJECT_ENDSWITH = '__c',
                        KEY_NAME = 'name',
                        KEY_LABEL = 'label',
                        FIELD_API_NAMES = 'fieldApiNames',
                        CHAR_DOT = '.',
                        FIELD_API_NAME = 'fieldApiName',
                        FIELD_LABEL = 'fieldLabel',
                        FIELD_API_NAME_TO_LABEL = 'fieldApiNameToLabel';

    // holds list of select option for api name and its label of objects.
    public List<SelectOption> objectNameList { get; set; }

    // holds all available object that may be used in synchronization process.
    private List<Map<String,String>> allAvailableObjectsToProcess { get; set; }

    /**
     * constructor for page
     */
    public SQX_Controller_OrgDependencySynchronizer() {
        objectNameList = new List<SelectOption>();

        Map<String, Schema.SObjectType> allObjectMap = Schema.getGlobalDescribe();
        allAvailableObjectsToProcess = new List<Map<String,String>>();

        for (Schema.SObjectType objectType : allObjectMap.values()) {
            
            Schema.DescribeSObjectResult objectDescription = objectType.getDescribe();
            String objectName = objectDescription.getName();
            String objectLabel = objectDescription.getLabel();

            if (objectName.contains(SQX.NSPrefix) && objectName.endsWith(CUSTOM_OBJECT_ENDSWITH)) {
                
                List<String> fieldApiNames = new List<String>();
                Map<String, Schema.SObjectField> fieldMap = objectDescription.fields.getMap();
                Map<String, String> objMap = new Map<String, String>();
                List<Map<String, String>> fieldApiNameToLabelList = new List<Map<String, String>>(); // holds map of field api name to label for all objects

                for (Schema.SObjectField sObjField : fieldMap.values()) {
                    if (''+sObjField.getDescribe().getType() == TYPE_PICKLIST) {

                        // constructing field api name with object api name eg. compliancequest__SQX_Audit__c.compliancequest__SQX_Org_Division__c
                        // this helps to read custom field with jsforce
                        fieldApiNames.add( objectName + CHAR_DOT + sObjField );
                        objMap.put( KEY_NAME, objectName );
                        objMap.put( KEY_LABEL, objectLabel );

                        // this map will hold field api name with its corresponding label. It will help to get the label of field from api name in js part
                        Map<String, String> fieldNameToLable = new Map<String, String>();
                        fieldNameToLable.put(FIELD_API_NAME, ''+sObjField);
                        fieldNameToLable.put(FIELD_LABEL, sObjField.getDescribe().getLabel());
                        fieldApiNameToLabelList.add(fieldNameToLable);
                    }
                }

                if (fieldApiNames.size() > 0 ) {
                    boolean isAlreadyAdded = false;
                    for (Integer i = 0; i < objectNameList.size(); i++) {
                        if (objMap.get(KEY_LABEL) < objectNameList[i].getLabel()){
                            isAlreadyAdded = true;
                            objectNameList.add(i, new SelectOption(objMap.get(KEY_NAME), objMap.get(KEY_LABEL)));
                            break;
                        }
                    }
                    if(!isAlreadyAdded)
                        objectNameList.add(new SelectOption(objMap.get(KEY_NAME), objMap.get(KEY_LABEL)));
                    objMap.put( FIELD_API_NAMES, JSON.serialize( fieldApiNames ) );
                    objMap.put(FIELD_API_NAME_TO_LABEL, JSON.serialize(fieldApiNameToLabelList));
                    allAvailableObjectsToProcess.add( objMap );
                }
            }
        }
        
        // Add None as first element in the drop down list
        objectNameList.add(0, new SelectOption('', Label.CQ_UI_Dropdown_None));
    }

    /**
     * This method returns serialized map of cq objects that have org field dependencies defined.
     */
    public String getObjectToProcess(){

        return allAvailableObjectsToProcess.size() > 0 ? JSON.serialize( allAvailableObjectsToProcess ) : '{}';
    }
}