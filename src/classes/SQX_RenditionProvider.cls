/**
* Interface that represents the rendition provider 
*/
global interface SQX_RenditionProvider {

    /**
    * Checks whether rendition is supported by provider or not 
    * @param requests the list of request whose verification is to be done
    */
    Map<SQX_RenditionRequest, Boolean> supportsRendition(SQX_RenditionRequest[] requests); 

	/**
	* Submits a request to create a rendition
	* @param request contains the rendition request for which the rendition is to be created
	*/
	SQX_RenditionResponse submitRequest(SQX_RenditionRequest request);
    
    /**
    * Polls whether or not the rendition is ready
    * @param requests the rendition requests whose status is to be polled
    */
    Map<SQX_RenditionRequest, SQX_RenditionResponse> poll(SQX_RenditionRequest [] requests);
    
    /**
    * returns the rendition request whose rendition is to be retrieved
    */
    SQX_RenditionResponse retrieve(SQX_RenditionRequest request);
    
    /**
    * aborts a given rendition request
    */
    void abort(SQX_RenditionRequest[] requests);
    
    /**
    * returns the name of the provider that is used in identifying the provider in the log
    */
    String getProviderName();
}