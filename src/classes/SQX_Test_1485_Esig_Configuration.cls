/*
 * test for esig configuration
 * author: Anish Shrestha
 * date: 2015/08/27
 * story: SQX-1485
 **/
@isTest
public class SQX_Test_1485_Esig_Configuration{

    static boolean runAllTests = true,
                    run_givenUser_EsigConfigurationCanBeAdded = false,
                    run_givenUser_EsigConfigurationCanBeRemoved = false,
                    run_givenUser_EsigConfigurationCannotBeDuplicated = false,
                    run_givenUser_EsigConfigurationRecordActivityIsSaved = false,
                    run_givenUser_EsigConfigCommentRequired_CommentMustBeProvided = false,
                    run_givenUser_StaticPoliciesAdded_NotAddedInOption = false;
                    
    public class MockAuthClass implements HttpCalloutMock {
        public Boolean Invalid_Request = false; // Esignature is not validating password when consumer key is wrong.
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            /**
            * description: when the invalid request is send by sending invalid consumer key or secret.
            */
            if(Invalid_Request){
                response.setStatusCode(400);
                response.setBody('Error: 400');
            }
            else{
                response.setHeader('Content-Type', 'application/json');
                response.setStatusCode(200);
                
                String url = req.getEndpoint();
                if(url.contains('rightPassword')){
                    response.setBody('{"id":"allcorrect"}');
                }
                else{
                    response.setBody('{"error":"incorrect password"}');
                }
            }
            
            return response;
        }
    }
    

    /**
    * When the user adds esig configuration, esig configuration record must be saved
    * @author Anish Shrestha
    * @date 2015/08/27
    * @story [SQX-1485]
    */ 
    public testMethod static void givenUser_EsigConfigurationCanBeAdded(){

        if(!runAllTests && !run_givenUser_EsigConfigurationCanBeAdded){
            return;
        }

        //Arrange: Add a esig configuration in esignature setting
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        System.runas(adminUser){

            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = false;
            cqES.Ask_Username__c = false;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration';
            esigConfiguration.Object_Type__C = SQX.Capa;
            esigConfiguration.Action__c = 'initiate';
            esigConfiguration.Required__c = false;
            //Act: Save an esig config
            estest.allEsigPolicyList.add(esigConfiguration);
            SQX_CQ_Esig_Policies__c esigConfiguration1 = new SQX_CQ_Esig_Policies__c();
            esigConfiguration1.Name ='personnel doc training Esig Config';
            esigConfiguration1.Object_Type__C = SQX.PersonnelDocumentTraining;
            esigConfiguration1.Action__c = 'userSignOff';
            esigConfiguration1.Required__c = false;
            estest.allEsigPolicyList.add(esigConfiguration1);

            PageReference r = estest.saveElectronicSignatureSetting();

            List<ApexPages.Message> msgs = ApexPages.getMessages();

            System.assertEquals(0, msgs.size(), 'Expected no errors to occur while saving ES setting');

            List<SQX_CQ_Esig_Policies__c> esigConfigurationList = new List<SQX_CQ_Esig_Policies__c>();
            esigConfigurationList = [SELECT Id FROM SQX_CQ_Esig_Policies__c];

            //Assert: Esig Configuration must be saved
            System.assertEquals( 2, esigConfigurationList.size(), 'Must have one esig configuration saved');

            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility().save();
            //Act:Retrive the Esig setting and its policy depend upon sobject type and its purpose of action
            SQX_Esig_Policy_Provider.SQX_Esig_Signature_setting esigPolicy =SQX_Esig_Policy_Provider.retriveEsigSetting(capa.capa.Id,'initiate');
            //Assert:verify the esig setting and its related policy
            System.assertEquals(cqES.Ask_Username__c, esigPolicy.username);
            System.assertEquals(esigConfiguration.Comment__c, esigPolicy.comment);
            System.assertEquals(esigConfiguration.Required__c, esigPolicy.required);
            if(esigPolicy.purposeOfSignature==null){
                 System.assertEquals(null, esigPolicy.purposeOfSignature);
            }
            if(!cqES.Ask_Username__c){
                System.assertEquals(SQX_Utilities.getUserDetailsForRecordActivity(), esigPolicy.loggedInAs);
            }
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();
            try{
                //Act:Retrive the Esig setting and its policy depend upon sobject type and its purpose action
                SQX_Esig_Policy_Provider.SQX_Esig_Signature_setting esigPolicyProvider =SQX_Esig_Policy_Provider.retriveEsigSetting(personnel.mainRecord.Id,'userSignOff');
            }catch(AuraHandledException ex){
                System.assert(ex.getMessage().contains('No matching action found for object '+SQX.NsPrefix+'SQX_Personnel__c for action userSignOff'), 'message=' + ex.getMessage());
            }
        }
    }

    /**
    * When the user removed esig configuration, esig configuration record must be removed
    * @author Anish Shrestha
    * @date 2015/08/27
    * @story [SQX-1485]
    */ 
    public testMethod static void givenUser_EsigConfigurationCanBeRemoved(){

        if(!runAllTests && !run_givenUser_EsigConfigurationCanBeRemoved){
            return;
        }

        //Arrange: Add a esig configuration in esignature setting
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        System.runas(adminUser){

            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = false;
            cqES.Ask_Username__c = false;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration';
            esigConfiguration.Object_Type__C = SQX.CAPA;
            esigConfiguration.Action__c = 'initiate';
            esigConfiguration.Required__c = false;
            //Act: Save an esig config
            estest.allEsigPolicyList.add(esigConfiguration);

            PageReference r = estest.saveElectronicSignatureSetting();

            List<ApexPages.Message> msgs = ApexPages.getMessages();

            System.assertEquals(0, msgs.size(), 'Expected no errors to occur while saving ES setting');

            List<SQX_CQ_Esig_Policies__c> esigConfigurationList = new List<SQX_CQ_Esig_Policies__c>();
            esigConfigurationList = [SELECT Id FROM SQX_CQ_Esig_Policies__c];

            //Assert: Esig Configuration must be saved
            System.assertEquals( 1, esigConfigurationList.size(), 'Must have one esig configuration saved');

            estest.removeEsigPolicyList.add(esigConfiguration);
            PageReference saveResult1 = estest.saveElectronicSignatureSetting();
            
            //Assert: Esig config must be romoved
            esigConfigurationList = [SELECT Id FROM SQX_CQ_Esig_Policies__c];
            System.assertEquals( 0, esigConfigurationList.size(), 'Must have no esig configuration saved');
        }
    }

    /**
    * When the user adds two same esig configuration, esig configuration record must not be saved and should throw an error
    * @author Anish Shrestha
    * @date 2015/08/27
    * @story [SQX-1485]
    */ 
    public testMethod static void givenUser_EsigConfigurationCannotBeDuplicated(){

        if(!runAllTests && !run_givenUser_EsigConfigurationCannotBeDuplicated){
            return;
        }

        //Arrange: Add a esig configuration in esignature setting
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        System.runas(adminUser){

            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = false;
            cqES.Ask_Username__c = false;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration';
            esigConfiguration.Object_Type__C = SQX.CAPA;
            esigConfiguration.Action__c = 'initiate';
            esigConfiguration.Required__c = false;

            //Act: save an esig config and save the clone of the esig config
            estest.allEsigPolicyList.add(esigConfiguration);

            PageReference r = estest.saveElectronicSignatureSetting();

            List<ApexPages.Message> pmsgs = ApexPages.getMessages();

            System.assertEquals(0, pmsgs.size(), 'Expected no errors to occur while saving ES setting');

            List<SQX_CQ_Esig_Policies__c> esigConfigurationList = new List<SQX_CQ_Esig_Policies__c>();
            esigConfigurationList = [SELECT Id FROM SQX_CQ_Esig_Policies__c];

            System.assertEquals( 1, esigConfigurationList.size(), 'Must have one esig configuration saved');

            // Assert: Must throw an error that same esig config cannot be saved

            SQX_CQ_Esig_Policies__c esigConfigurationClone = new SQX_CQ_Esig_Policies__c();
            esigConfigurationClone.Name = 'CAPA Esig Configuration Clone';
            esigConfigurationClone.Object_Type__C = SQX.CAPA;
            esigConfigurationClone.Action__c = 'initiate';
            esigConfigurationClone.Required__c = false;
            estest.allEsigPolicyList.add(esigConfigurationClone);

            System.PageReference saveResult1 = estest.saveElectronicSignatureSetting();
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            Boolean expectedExceptionThrown, 
                    containsErrorMessage = false;
            for(ApexPages.Message msg : msgs){
                    //System.assert(false, ''+ msg);
                expectedExceptionThrown = msg.getDetail().contains('The object CAPA with same action already exist.') ? true : false;
                if(expectedExceptionThrown){
                    containsErrorMessage = true;
                }
            } 
            //System.assert(false, ''+ msg.getDetail());
            System.assertEquals(containsErrorMessage, true, 'Must return true as esig configuration cannot be duplicated');
            
            System.assertEquals(2, estest.allEsigPolicyList.size(), 'Expected unsaved changes to exist.');
        }    
    }

    /**
    * This test ensures User SignOff with E-signature to work as expected.
    * @author Anish Shrestha
    * @date 2015/009/30
    * @story [SQX-1486]
    */ 
    public testmethod static void givenUser_EsigConfigurationRecordActivityIsSaved() {
        if (!runAllTests && !run_givenUser_EsigConfigurationRecordActivityIsSaved) {
            return;
        }    
        //Arrange: Add a esig configuration in esignature setting
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        System.runas(adminUser){
            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = true;
            cqES.Ask_Username__c = true;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new MockAuthClass());//instruct the Apex runtime to send this fake response
            
            // Act: Save a record with esig enabled
            PageReference r = estest.saveElectronicSignatureSetting();

            List<ApexPages.Message> msgs = ApexPages.getMessages();

            System.assertEquals(0, msgs.size(), 'Expected no errors to occur while saving ES setting');
            
            SQX_Electronic_Signature_Record_Activity__c esigRecordActivity = new SQX_Electronic_Signature_Record_Activity__c();
            esigRecordActivity = [SELECT Id, Comment__c, Modified_By__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_Electronic_Signature_Record_Activity__c];
            
            //Assert: Ensuring user signed off by value
            System.assertEquals(esigRecordActivity.Modified_By__c, estest.getUserDetails(),
                'Expected user had modified the record.');

            //Assert :ensuring User Signing off comment set
            System.assertEquals(esigRecordActivity.Comment__c, 'Changing E-signature',
                'Expected comment had been added to record history.');

            Test.stopTest();
        }
    }
    

    /**
    * When the user adds esig configuration with comment required, comment must be provided
    * @author Anish Shrestha
    * @date 2015/10/29
    * @story [SQX-1046]
    */ 
    public testMethod static void givenUser_EsigConfigCommentRequired_CommentMustBeProvided(){

        if(!runAllTests && !run_givenUser_EsigConfigCommentRequired_CommentMustBeProvided){
            return;
        }

        //Arrange: Add a esig configuration in esignature setting
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(adminUser){

            // enable electronic signature with username
            SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
            cqES.Consumer_Key__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Consumer_Secret__c = 'SQX_Test_1485_Esig_Configuration';
            cqES.Enabled__c = false;
            cqES.Ask_Username__c = false;
            Database.insert(cqES, true);
            
            SQX_Controller_ES_Setting estest= new SQX_Controller_ES_Setting();

            Test.setCurrentPage(Page.SQX_ES_Settings_Edit); //setting SQX_ES_Settings_Edit for Test

            estest.SigningOffComment = 'Changing E-signature';
            estest.SigningOffUsername = adminUser.username;
            estest.SigningOffPassword = 'rightPassword';

            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration';
            esigConfiguration.Object_Type__C = SQX.CAPA;
            esigConfiguration.Action__c = 'initiate';
            esigConfiguration.Required__c = false;
            esigConfiguration.Comment__c = true;
            //Act: Save an esig config
            estest.allEsigPolicyList.add(esigConfiguration);

            PageReference r = estest.saveElectronicSignatureSetting();

            List<ApexPages.Message> msgs = ApexPages.getMessages();

            System.assertEquals(0, msgs.size(), 'Expected no errors to occur while saving ES setting: ' + msgs);

            List<SQX_CQ_Esig_Policies__c> esigConfigurationList = new List<SQX_CQ_Esig_Policies__c>();
            esigConfigurationList = [SELECT Id FROM SQX_CQ_Esig_Policies__c];

            //Assert: Esig Configuration must be saved
            System.assertEquals( 1, esigConfigurationList.size(), 'Must have one esig configuration saved');

        }
        System.runas(newUser){

            Account supplierAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_SUPPLIER);
            Contact supplierContact = SQX_Test_Account_Factory.createContact(supplierAccount);
            User supplierUser = SQX_Test_Account_Factory.createUser(supplierContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER  );
            // Arrange: Create capa and initiate it
            SQX_CAPA__c capa = new SQX_CAPA__c(
                                    Needs_Effectiveness_Review__c = false,
                                    Effectiveness_Monitoring_Days__c = 90,
                                    Issued_Date__c = Date.Today(),
                                    Target_Due_Date__c = Date.Today().addDays(15),
                                    SQX_Account__c = supplierAccount.Id,
                                    SQX_External_Contact__c = supplierContact.Id);

            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));

            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(capa));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            capaMap.put('Id', 'CAPA-1');
            capaMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
            
            changeSet.add(capaMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            //Act: initiate it without comment
            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> paramsWithNoComment = new Map<String, String>();
            paramsWithNoComment.put('nextAction', 'initiate');
            paramsWithNoComment.put('comment', '');

            //Assert: should throw an error
            try{
                Id result= SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null,paramsWithNoComment);
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Comment hasn\'t been provided for the action') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);       
            } 
      
            //Act: initiate capa with comment
            Map<String, String> paramsWithComment = new Map<String, String>();
            paramsWithComment.put('nextAction', 'initiate');
            paramsWithComment.put('comment', 'Inititing CAPA');
            Id result= SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null,paramsWithComment);

            //Assert: should sucessfully save the result
            System.assert(string.valueOf(result.getsObjectType()) == SQX.Capa, 
                'Expected to process changeset'+ changeSet);

        }
    }

    /**
    * When the user adds static esig policies, such policies should not list in the essetting option for that object
    * @author Sagar Shrestha
    * @date 2015/10/29
    * @story [SQX-1046]
    */ 
    public testMethod static void givenUser_StaticPoliciesAdded_NotAddedInOption(){
        if(!runAllTests && !run_givenUser_StaticPoliciesAdded_NotAddedInOption){
            return;
        }
        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(standardUser){
            //Arrange: Save a complaint, initialize complaint extension and es setting controller
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();  

            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name, Status__c, SQX_Finding__c FROM SQX_Complaint__c WHERE Id=:complaint.complaint.Id]);

            SQX_Extension_Complaint compExtension =  new SQX_Extension_Complaint(controller);
            SQX_Controller_ES_Setting esSetting = new SQX_Controller_ES_Setting();

            //Act : Get esigAction list, complaintPurposeOfSigs and complaintStaticPolicies
            Map<String, List<SelectOption>> esigactions = essetting.esigActions;
            
            Map<String, String> complaintPurposeOfSigs = compExtension.getPurposeOfSig();
            Set<String> complaintStaticPolicies = compExtension.getStaticPolicies();

            List<SelectOption> complaintOptions = esigactions.get(SQX.Complaint);

            //Assert1 : complaint Options size should be equal to (complaintPurposeOfSigs  - complaintStaticPolicies )
            System.assertEquals(complaintPurposeOfSigs.size() - complaintStaticPolicies.size(),complaintOptions.size(), 'Expected static policies to be removed from Esig Actions');

            //Assert 2 : static policies should not be in the options
            for(SelectOption complaintOption : complaintOptions){
                System.assertEquals(false, complaintStaticPolicies.contains(complaintOption.getLabel()));
            }

        }

    }

    
    /**
    * Given : SaveAfterDraft action label is aded in extension to bypass the comment
    * When : User try to add the action name of saveAfterDraft 
    * Then :Action name called saveAfterDraft should available
    * @story [SQX-5938]
    */ 
    public testMethod static void givenObjectGetsEsigActionLabel_WhenUserDropDownActionPicklistInEsig_ActionLabelSaveAfterDraftShouldVisible(){

        //Arrange: Create a complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(standardUser){
            SQX_Controller_ES_Setting esSetting = new SQX_Controller_ES_Setting();
            
            //Arrange :get all esig action depends upon object
            Map<String, List<SelectOption>> esigactions = esSetting.esigActions;
            //Act: seperate the object name and its selection option
            Map<String, Map<String, String>> esigactionMap = new Map<String, Map<String, String>>();
            
            for (String esigobject : esigactions.keySet()) {
                //map with object related with select option
                esigactionMap.put(esigobject, new Map<String, String>());
                
                for (SelectOption actionOption : esigactions.get(esigobject)) {
                    
                    esigactionMap.get(esigobject).put(actionOption.getValue(), actionOption.getLabel());
                }
            }
            //Assert : verify the object with its action name  and lable of it
            System.assertEquals(true, esigactionMap.get(SQX.Complaint).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'complaint contain saveafter draft');
            System.assertEquals(SQX_Esig_Policy_Provider.ACTION_LABEL_SAVE_RECORD_AFTER_DRAFT_STATUS, esigactionMap.get(SQX.Complaint).get(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'complaint contains saveafterDraft label ');
            
            System.assertEquals(true, esigactionMap.get(SQX.CAPA).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Capa contain saveafter draft');
            System.assertEquals(SQX_Esig_Policy_Provider.ACTION_LABEL_SAVE_RECORD_AFTER_DRAFT_STATUS, esigactionMap.get(SQX.CAPA).get(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Capa contains saveafterDraft label ');

            System.assertEquals(true, esigactionMap.get(SQX.Nonconformance).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Nonconformance contain saveafter draft');
            System.assertEquals(SQX_Esig_Policy_Provider.ACTION_LABEL_SAVE_RECORD_AFTER_DRAFT_STATUS, esigactionMap.get(SQX.Nonconformance).get(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Nonconformance contains saveafterDraft label ');

            System.assertEquals(false, esigactionMap.get(SQX.Calibration).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Calibration does not contain saveafter draft');
            System.assertEquals(false, esigactionMap.get(SQX.Equipment).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'Equipment does not contain saveafter draft');
            System.assertEquals(false, esigactionMap.get(SQX.PersonnelAssessment).containsKey(SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT), 'PersonnelAssessment doesnot contain saveafter draft');
                  
        }
    }

    /**
    * Given :Capa record 
    *           i)Draft Staus
    *           ii)Open status 
    *           iii)Added saveAfterDraft esig policy in esig setting 
    * When : User try to update the record and save the record 
    * Then : User should get
    *           i) record is in draft  than comment set to be false 
    *           ii) record is in open status comment set to be  true 
    *           iii)After adding policy depend upon esig policy comment is set true or false
    * @story [SQX-5938]
    */
    @isTest 
    public static void  givenCapaRecordAndSaveBeforeDraftAndAfterDraft_WhenUserTryToSaveRecord_ThenUserCanSaveAccordingToPolicy(){

        //Arrange: Create a capa
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        final Boolean NEW_OR_DRAFT_RECORD = true,
                      AFTER_DRAFT_RECORD = false;
        System.runas(standardUser){

            //Arrange: Save a capa and es setting controller
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility().Save();

            SQX_CAPA__c capaRecord = [SELECT ID, Name, Status__c FROM SQX_CAPA__c WHERE Id=:capa.capa.Id];

            System.assertEquals(SQX_CAPA.STATUS_DRAFT, capaRecord.Status__c, 'Draft CAPA is expected.');
            
            ApexPages.StandardController controller = new ApexPages.StandardController(capaRecord);

            SQX_Extension_CAPA capaExtension =  new SQX_Extension_CAPA(controller);

            SQX_CQ_Esig_Policies__c esigpolicyIsNewOrDraft = capaExtension.getpolicySaveRecordAfterDraft(NEW_OR_DRAFT_RECORD, SQX.CAPA);

            //Assert:comment is false when capa is in draft
            System.assertEquals(SQX_Esig_Policy_Provider.DEFAULT_POLICY_SAVE_NEW_OR_DRAFT_RECORD, esigpolicyIsNewOrDraft,'Comment is false when capa record is in draft  ');
            
            //update capa record to open
            capaRecord.Status__c=SQX_CAPA.STATUS_OPEN;
            new SQX_DB().op_update(new List<SQX_CAPA__c>{capaRecord}, new List<SObjectField>{});
            
            //Act :get policy when record  is in  open status
            SQX_CQ_Esig_Policies__c esigPolicySaveRecordAfterDraft = capaExtension.getpolicySaveRecordAfterDraft(AFTER_DRAFT_RECORD,SQX.CAPA);
            //Assert:comment is true when capa is in open status
            System.assertEquals(SQX_Esig_Policy_Provider.DEFAULT_POLICY_SAVE_RECORD_AFTER_DRAFT, esigPolicySaveRecordAfterDraft,'Comment is true when capa record is in open state ');

            //Arrange :After adding esig config for save after draft in esigSetting
            SQX_CQ_Esig_Policies__c esigConfiguration = new SQX_CQ_Esig_Policies__c();
            esigConfiguration.Name = 'CAPA Esig Configuration Clone';
            esigConfiguration.Object_Type__C = SQX.CAPA;
            esigConfiguration.Action__c =SQX_Esig_Policy_Provider.ACTION_NAME_SAVE_AFTER_DRAFT ;
            esigConfiguration.Required__c = false;
            esigConfiguration.Comment__c=false;
            insert esigConfiguration;

            //Act:get policy depends upon setup in esig setting
            SQX_CQ_Esig_Policies__c esigAddedpolicy = capaExtension.getpolicySaveRecordAfterDraft(AFTER_DRAFT_RECORD,SQX.CAPA);
            //Assert:Verify the added policy comment
            System.assertEquals(esigConfiguration.Comment__c,esigAddedpolicy.Comment__c);

            //Arrange:created a new capa 
            capaExtension = new SQX_Extension_CAPA(new ApexPages.StandardController(new SQX_CAPA__c( Status__c = SQX_CAPA.STATUS_DRAFT )));

            SQX_CQ_Esig_Policies__c esigpolicyInDraft = capaExtension.getpolicySaveRecordAfterDraft(NEW_OR_DRAFT_RECORD,SQX.CAPA);
            //Assert:comment is false when capa is in draft
            System.assertEquals(SQX_Esig_Policy_Provider.DEFAULT_POLICY_SAVE_NEW_OR_DRAFT_RECORD, esigpolicyInDraft,'Comment is false when capa record is in draft after adding esig policy too ');
        }
    }

}