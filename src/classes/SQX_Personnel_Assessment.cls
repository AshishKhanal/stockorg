/**
 * This class contains static strings required for the personnel assessment
 */
public with sharing class SQX_Personnel_Assessment {
	
    /**
     * static string declaration
     */
    public static final String PurposeOfSignature_SubmitingAssessmentResult  = 'submit';
    
    /**
     * map of activity code to purpose of signature label
     */
    public static final Map<String, String> purposeOfSigMap = new Map<String, String>{
        PurposeOfSignature_SubmitingAssessmentResult => Label.SQX_PS_Personnel_Assessment_Submitting_Assessment
    };

    /**
     * returns personnel assessment
     * @param psnAstId  personnel assessment id
     * @returns returns personnel assessment
     */
    public static SQX_Personnel_Assessment__c getPersonnelAssessment(Id psnAstId) {
        List<SQX_Personnel_Assessment__c> paList = [SELECT Id,
                                                        SQX_Personnel__c,
                                                        SQX_Assessment__c,
                                                        Allow_Retake__c,
                                                        SQX_Last_Assessment_Attempt__c,
                                                        Is_Archived__c,
                                                        Can_Retake__c,
                                                        Is_Successfully_Completed__c,
                                                        Total_Personnel_Assessment_Attempt__c,
                                                        Personnel_User_Id__c
                                                    FROM SQX_Personnel_Assessment__c
                                                    WHERE Id =: psnAstId];
        
        return paList.isEmpty() ? null : paList[0];
    }
    
    /**
     * returns active personnel assessment
     * @param assessmentId assessment which is being used for the personnel assessment
     * @param personnelId  personnel for which personnel assessment is made
     * @returns returns personnel assessment
     */
    public static SQX_Personnel_Assessment__c getPersonnelAssessment(Id assessmentId, Id personnelId) {
        List<SQX_Personnel_Assessment__c> paList = [SELECT Id,
                                                        SQX_Personnel__c,
                                                        SQX_Assessment__c,
                                                        Allow_Retake__c,
                                                        SQX_Last_Assessment_Attempt__c,
                                                        Is_Archived__c,
                                                        Can_Retake__c,
                                                        Is_Successfully_Completed__c,
                                                        Total_Personnel_Assessment_Attempt__c,
                                                        Personnel_User_Id__c
                                                    FROM SQX_Personnel_Assessment__c
                                                    WHERE SQX_Personnel__c =: personnelId
                                                        AND SQX_Assessment__c =: assessmentId
                                                        AND Is_Archived__c = false
                                                        ORDER BY CreatedDate DESC LIMIT 1];
        
        return paList.isEmpty() ? null : paList[0];
    }

}