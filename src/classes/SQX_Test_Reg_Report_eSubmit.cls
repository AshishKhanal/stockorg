/**
 * Test class for Medwatch Export controller functions
*/

@isTest
public class SQX_Test_Reg_Report_eSubmit {

    static final String STANDARD_USER = 'standardUser',
                        ADMIN_USER = 'adminUser';

    static final String JOB_ID = 'AB12';

    static final String ERROR_MESSAGE = 'Something bad happened',
                        SUCCESS_MESSAGE = 'Awesome';

    /**
     * Setup a medwatch record
    */
    @testSetup
    static void setup() {

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole, STANDARD_USER);

        SQX_Reporting_Default__c def = new SQX_Reporting_Default__c();
        System.runAs(adminUser) {

            List<SQX_Auto_Number__c> autoNums = new List<SQX_Auto_Number__c>();
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Medwatch',
                Next_Number__c = 0,
                Number_Format__c = 'MD-{0}-{1}',
                Numeric_Format__c = '000000'
            ));
            insert autoNums;

            def.Reporting_Site__c = 'Manufacturing Site';
            def.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            def.SQX_Man_Num_Scheme__c = autoNums.get(0).Id;

            insert def;

        }

        System.runAs(standardUser) {

            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();

            SQX_Regulatory_Report__c regulatoryReport_Medwatch = new SQX_Regulatory_Report__c();
            regulatoryReport_Medwatch.Due_Date__c =  Date.today() + 21;
            regulatoryReport_Medwatch.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport_Medwatch.SQX_Reporting_Default__c = def.Id;
            regulatoryReport_Medwatch.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regulatoryReport_Medwatch.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;

            SQX_Regulatory_Report__c regulatoryReport_MedDev = new SQX_Regulatory_Report__c();
            regulatoryReport_MedDev.Due_Date__c =  Date.today() + 21;
            regulatoryReport_MedDev.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport_MedDev.SQX_Reporting_Default__c = def.Id;
            regulatoryReport_MedDev.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_EMA;
            regulatoryReport_MedDev.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA;

            SQX_Regulatory_Report__c regulatoryReport_Canada = new SQX_Regulatory_Report__c();
            regulatoryReport_Canada.Due_Date__c =  Date.today() + 21;
            regulatoryReport_Canada.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport_Canada.SQX_Reporting_Default__c = def.Id;
            regulatoryReport_Canada.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_HEALTH_CANADA;
            regulatoryReport_Canada.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_10_DAY_MDPR;

            SQX_Regulatory_Report__c regulatoryReport_General = new SQX_Regulatory_Report__c();
            regulatoryReport_General.Due_Date__c =  Date.today() + 21;
            regulatoryReport_General.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport_General.SQX_Reporting_Default__c = def.Id;
            regulatoryReport_General.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA;
            regulatoryReport_General.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_2_DAY_MDIR;


            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport_Medwatch, regulatoryReport_MedDev, regulatoryReport_Canada, regulatoryReport_General}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c,SQX_Regulatory_Report__c.SQX_Complaint__c});

            SQX_General_Report__c general = new SQX_General_Report__c(SQX_Regulatory_Report__c = regulatoryReport_General.Id);
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c(SQX_Regulatory_Report__c = regulatoryReport_Medwatch.Id,B5_Describe_event_or_problem__c = 'Prob', D2_a_Common_Device_Name__c = 'Cod', D2_b_Procode__c = 'Pro',H1_Type_of_Reportable_Event__c = 'C53569');
            SQX_MedDev__c medDev = new SQX_MedDev__c(SQX_Regulatory_Report__c = regulatoryReport_MedDev.Id);
            SQX_Canada_Report__c canada = new SQX_Canada_Report__c(SQX_Regulatory_Report__c = regulatoryReport_Canada.Id);

            new SQX_DB().op_insert(new List<SObject> { general, medwatch, medDev, canada }, new List<SObjectField> {});

            regulatoryReport_General.Report_Id__c = general.Id;
            regulatoryReport_Medwatch.Report_Id__c = medwatch.Id;
            regulatoryReport_MedDev.Report_Id__c = medDev.Id;
            regulatoryReport_Canada.Report_Id__c = canada.Id;

            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c>{regulatoryReport_Medwatch, regulatoryReport_MedDev, regulatoryReport_Canada, regulatoryReport_General}, new List<SObjectField>{ SQX_Regulatory_Report__c.Report_Id__c });

        }
    }

    /**
     * Returns a Medwatch record
    */
    static SQX_Medwatch__c getMedwatchRecord() {
        return [SELECT Id, Name, SQX_Regulatory_Report__c FROM SQX_Medwatch__c LIMIT 1];
    }


    /**
     * Returns a MedDev record
    */
    static SQX_MedDev__c getMedDevRecord() {
        return [SELECT Id, Name, SQX_Regulatory_Report__c FROM SQX_MedDev__c LIMIT 1];
    }


    /**
     * Returns a General Report record
    */
    static SQX_General_Report__c getGeneralReportRecord() {
        return [SELECT Id, Name, SQX_Regulatory_Report__c FROM SQX_General_Report__c LIMIT 1];
    }


    /**
     * Returns a Canada report record
    */
    static SQX_Canada_Report__c getCanadaReportRecord() {
        return [SElECT Id, Name, SQX_Regulatory_Report__c FROM SQX_Canada_Report__c LIMIT 1];
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Export to PDF service is invoked
     * THEN: JobId is returned
    */
    static testmethod void givenMedwatchRecord_WhenPDFExportServiceIsCalled_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.PDF_EXPORT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submitPDFGenerationRequest(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
            
            
            // ACT (SQX-8211): update SQX_Medwatch__c, SQX_Reporting_Default__c
            List<SObject> objsToUpdate = new List<SObject>();
            objsToUpdate.add(new SQX_Medwatch__c( Id = md.Id ));
            objsToUpdate.add([SELECT Id FROM SQX_Reporting_Default__c LIMIT 1]);
            update objsToUpdate;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_Medwatch__c, SQX_Reporting_Default__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Medwatch__c.SObjectType);
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_Reporting_Default__c.SObjectType);
        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Record is submitted
     * THEN: JobId is returned
    */
    static testmethod void givenMedwatchRecord_WhenRecordIsSubmitted_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submit(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }

    /**
     * GIVEN: Medwatch record
     * WHEN: Record is submitted for XML validation
     * THEN: JobId is returned
    */
    static testmethod void givenMedwatchRecord_WhenRecordIsSubmittedForXMLValidation_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke validation service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.VALIDATE));

            String jobId = SQX_Regulatory_Report_Submit_Controller.validate(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted for validation and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }

    /**
     * GIVEN: Meddev record
     * WHEN: Export to PDF service is invoked
     * THEN: JobId is returned
    */
    static testmethod void givenMedDevRecord_WhenPDFExportServiceIsCalled_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : MedDev record
            SQX_MedDev__c md = getMedDevRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.PDF_EXPORT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submitPDFGenerationRequest(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
            
            
            // ACT (SQX-8211): update SQX_MedDev__c
            update md;
            
            // ASSERT (SQX-8211): ensure long text field tracking for SQX_MedDev__c
            SQX_Test_BulkifiedBase.assertLongTextFieldTrackingForUpdatedObjectType(SQX_MedDev__c.SObjectType);
        }
    }


    /**
     * GIVEN: MedDev record
     * WHEN: Record is submitted
     * THEN: JobId is returned
    */
    static testmethod void givenMedDevRecord_WhenRecordIsSubmitted_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : MedDev record
            SQX_MedDev__c md = getMedDevRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submit(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }


    /**
     * GIVEN: MedDev record
     * WHEN: Record is submitted for XML validation
     * THEN: JobId is returned
    */
    static testmethod void givenMedDevRecord_WhenRecordIsSubmittedForXMLValidation_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : MedDev record
            SQX_MedDev__c md = getMedDevRecord();

            // ACT : Invoke validation service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.VALIDATE));

            String jobId = SQX_Regulatory_Report_Submit_Controller.validate(md.Id);

            Test.stopTest();

            // ASSERT : Request is submitted for validation and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }


    /**
     * GIVEN: Canada record
     * WHEN: Export to PDF service is invoked
     * THEN: JobId is returned
    */
    static testmethod void givenCanadaReportRecord_WhenPDFExportServiceIsCalled_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Canada Report record
            SQX_Canada_Report__c cr = getCanadaReportRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.PDF_EXPORT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submitPDFGenerationRequest(cr.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }


    /**
     * GIVEN: Canada record
     * WHEN: Record is submitted
     * THEN: JobId is returned
    */
    static testmethod void givenCanadaRecord_WhenRecordIsSubmitted_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Canada Report record
            SQX_Canada_Report__c cr = getCanadaReportRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submit(cr.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }

    /**
     * GIVEN: Canada record
     * WHEN: Record is submitted for XML validation
     * THEN: Error is thrown as Canada doesn't support XML validation
    */
    static testmethod void givenCanadaRecord_WhenRecordIsSubmittedForXMLValidation_ThenErrorIsThrown() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Canada Report record
            SQX_Canada_Report__c cr = getCanadaReportRecord();

            // ACT : Invoke validation service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT));

            String exceptionMsg;
            try {
                SQX_Regulatory_Report_Submit_Controller.validate(cr.Id);
            }catch(AuraHandledException ex) {
                exceptionMsg = ex.getMessage();
            }

            Test.stopTest();

            // ASSERT : Error is thrown
            System.assertNotEquals(null, exceptionMsg, 'Expected error to be thrown when Canada Report is submitted for validation');
        }
    }

    /**
     * GIVEN: General Report record
     * WHEN: Export to PDF service is invoked
     * THEN: JobId is returned
    */
    static testmethod void givenGeneralReportRecord_WhenPDFExportServiceIsCalled_ThenJobIdIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_General_Report__c gr = getGeneralReportRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.PDF_EXPORT));

            String jobId = SQX_Regulatory_Report_Submit_Controller.submitPDFGenerationRequest(gr.Id);

            Test.stopTest();

            // ASSERT : Request is submitted and job id is returned
            System.assertEquals(JOB_ID, jobId, 'Unexpected result when exporting pdf');
        }
    }



    /**
     * GIVEN: General Report record
     * WHEN: Record is submitted
     * THEN: JobId is returned
    */
    static testmethod void givenGeneralReportRecord_WhenRecordIsSubmitted_ThenErrorIsThrown() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_General_Report__c gr = getGeneralReportRecord();

            // ACT : Invoke export service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT));

            AuraHandledException expectedExpection;
            try{
                SQX_Regulatory_Report_Submit_Controller.submit(gr.Id);
            } catch(AuraHandledException ex) {
                expectedExpection = ex;
            }

            Test.stopTest();

            // ASSERT : Error is thrown as eSubmission is not supported for general report
            System.assertNotEquals(null, expectedExpection, 'Expected eSubmit request to fail for general reports');
        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Export to PDF service is invoked to fail
     * THEN: Error is thrown
    */
    static testmethod void givenMedwatchRecord_WhenPDFExportServiceIsCalledToFail_ThenErrorIsThrown() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke export service to fail
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.SUBMIT_WITH_ERROR));

            String errorMsg;
            try {
                SQX_Regulatory_Report_Submit_Controller.submitPDFGenerationRequest(md.Id);
            } catch(AuraHandledException e) {
                errorMsg = e.getMessage();
            }

            Test.stopTest();

            // ASSERT : Error is thrown and captured
            System.assertNotEquals(null, errorMsg, 'Expected error message to be thrown when submission fails');
        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Poll service is called after the job has been pushed
     * THEN: Job Status is returned
    */
    static testmethod void givenMedwatchRecord_WhenPollServiceIsCalledAfterSubmission_ThenJobStatusIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // mock submission completion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c = String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { md.SQX_Regulatory_Report__c });
            cv.Title = 'Test-Title';
            cv.PathOnClient = md.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;

            SQX_Submission_History__c subHistory = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            // ACT : Invoke poll service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.POLL_SUBMISSION));

            String responseJSON = SQX_Regulatory_Report_Submit_Controller.pollSubmission(JOB_ID, md.Id);

            Test.stopTest();

            // ASSERT : Job status response is returned
            System.assertNotEquals(null, responseJSON, 'Expected job status to be returned');

            JobStatus response = (JobStatus) JSON.deserializeStrict(responseJSON, JobStatus.class);
            System.assertEquals(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_COMPLETE, response.status, 'Expected response status to be complete');
            System.assertEquals(SUCCESS_MESSAGE, response.message, 'Unexpected success message');
            System.assertEquals(subHistory.Id, response.submissionRecordId, 'Expected submission record to be associated with the response');
        }
    }



    /**
     * GIVEN: MedDev record
     * WHEN: Poll service is called after the job has been pushed
     * THEN: Job Status is returned
    */
    static testmethod void givenMedDevRecord_WhenPollServiceIsCalledAfterSubmission_ThenJobStatusIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : MedDev record
            SQX_MedDev__c mdv = getMedDevRecord();

            // mock submission completion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_INITIAL, new String[] { mdv.SQX_Regulatory_Report__c });
            cv.Title ='Test-Title';
            cv.PathOnClient = mdv.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');

            insert cv;

            SQX_Submission_History__c subHistory = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            // ACT : Invoke poll service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.POLL_WITH_ERROR));    // poll with error should still return submission id as xml generation has passed

            String responseJSON = SQX_Regulatory_Report_Submit_Controller.pollSubmission(JOB_ID, mdv.Id);

            Test.stopTest();

            // ASSERT : Job status response is returned
            System.assertNotEquals(null, responseJSON, 'Expected job status to be returned');

            JobStatus response = (JobStatus) JSON.deserializeStrict(responseJSON, JobStatus.class);
            System.assertEquals(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_COMPLETE, response.status, 'Expected response status to be error');
            System.assertEquals(ERROR_MESSAGE, response.message, 'Unexpected error message');
            System.assertEquals(subHistory.Id, response.submissionRecordId, 'Expected submission record to be associated with the response');
        }
    }


    /**
     * GIVEN: Canada Report record
     * WHEN: Poll service is called after the job has been pushed
     * THEN: Job Status is returned
    */
    static testmethod void givenCanadaReportRecord_WhenPollServiceIsCalledAfterSubmission_ThenJobStatusIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Canada report record
            SQX_Canada_Report__c report = getCanadaReportRecord();

            // mock submission completion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { report.SQX_Regulatory_Report__c });
            cv.Title = 'Test-Title';
            cv.PathOnClient = report.Name + '.pdf';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;

            SQX_Submission_History__c subHistory = [SELECT Id FROM SQX_Submission_History__c LIMIT 1];

            // ACT : Invoke poll service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.POLL_SUBMISSION));

            String responseJSON = SQX_Regulatory_Report_Submit_Controller.pollSubmission(JOB_ID, report.Id);

            Test.stopTest();

            // ASSERT : Job status response is returned
            System.assertNotEquals(null, responseJSON, 'Expected job status to be returned');

            JobStatus response = (JobStatus) JSON.deserializeStrict(responseJSON, JobStatus.class);
            System.assertEquals(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_COMPLETE, response.status, 'Expected response status to be complete');
            System.assertEquals(SUCCESS_MESSAGE, response.message, 'Unexpected success message');
            System.assertEquals(subHistory.Id, response.submissionRecordId, 'Expected submission record to be associated with the response');
        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Poll service is called after the job has been pushed
     * THEN: Job Status is returned
    */
    static testmethod void givenMedwatchRecord_WhenPollServiceIsCalled_ThenJobStatusIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke poll service
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.POLL));

            String responseJSON = SQX_Regulatory_Report_Submit_Controller.poll(JOB_ID, md.Id);

            Test.stopTest();

            // ASSERT : Job status response is returned
            System.assertNotEquals(null, responseJSON, 'Expected job status to be returned');

            JobStatus response = (JobStatus) JSON.deserializeStrict(responseJSON, JobStatus.class);
            System.assertEquals(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_COMPLETE, response.status, 'Expected response status to be complete');
            System.assertEquals(SUCCESS_MESSAGE, response.message, 'Unexpected success message');
        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Record is preset before submission
     * THEN: Record is populated with the desired submission number
    */
    static testmethod void givenMedwatchRecord_WhenRecordIsPreparedBeforeSubmission_ThenSubmissionNumberIsSetAccordingly() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            Test.startTest();

            // ACT: Invoke presetting method
            List<String> errors = SQX_Regulatory_Report_Submit_Controller.prepareRecordForSubmission(md.Id);

            Test.stopTest();

            // ASSERT : Submission number has been set
            System.assertEquals(0, errors.size(), 'Did not expect errors when preparing medwatch record for submission');
            md = [SELECT Header_Mfr_Report_Number__c, G9_MFR_Report_Number__c FROM SQX_Medwatch__c WHERE Id =: md.Id];

            String expectedSubNumber = String.format('MD-{0}-{1}', new String[] {  String.valueOf(Date.today().year()), '000000'  });

            System.assertEquals(md.Header_Mfr_Report_Number__c, md.G9_MFR_Report_Number__c);
            System.assertEquals(expectedSubNumber, md.Header_Mfr_Report_Number__c);

        }
    }


    /**
     * GIVEN: Medwatch record
     * WHEN: Poll service is called after the job has been pushed to fail
     * THEN: Job Status is returned
    */
    static testmethod void givenMedwatchRecord_WhenPollServiceIsCalledToFail_ThenFailedJobStatusIsReturned() {

        System.runAs(SQX_Test_Account_Factory.getUsers().get(STANDARD_USER)) {

            // ARRANGE : Medwatch record
            SQX_Medwatch__c md = getMedwatchRecord();

            // ACT : Invoke poll service to fail
            Test.startTest();

            Test.setMock(HttpCalloutMock.class, new SQX_Rest_Handler(Mode.POLL_WITH_ERROR));

            String responseJSON = SQX_Regulatory_Report_Submit_Controller.poll(JOB_ID, md.Id);

            Test.stopTest();

            // ASSERT : Response is returned but with failed status
            System.assertNotEquals(null, responseJSON, 'Expected job status to be returned');

            JobStatus response = (JobStatus) JSON.deserializeStrict(responseJSON, JobStatus.class);
            System.assertEquals(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_ERROR, response.status, 'Expected failed job status');
            System.assertEquals(ERROR_MESSAGE, response.message, 'Unexpected failure message');
        }
    }

    /**
     * Handles Medwatch REST Service request
    */
    private class SQX_Rest_Handler implements HttpCalloutMock {

        Mode requestMode;

        public SQX_Rest_Handler(Mode requestMode) {
            this.requestMode = requestMode;
        }

        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();

            if (requestMode == Mode.SUBMIT || requestMode == Mode.VALIDATE) {
                this.submit(res);
            } else if(requestMode == Mode.PDF_EXPORT) {
                this.submit(res);
            }
            else if(requestMode == Mode.SUBMIT_WITH_ERROR) {
                this.sendError(res);
            }
            else if(requestMode == Mode.POLL) {
                this.sendCompletedJobStatus(res);
            }
            else if(requestMode == Mode.POLL_SUBMISSION) {
                this.sendCompletedJobStatus(res);
            }
            else if(requestMode == Mode.POLL_WITH_ERROR) {
                this.sendFailedJobStatus(res);
            }

            return res;
        }

        private void exportPDF(HttpResponse res) {
            res.setStatusCode(200);
            res.setBody(JOB_ID);
        }

        private void submit(HttpResponse res) {
            res.setStatusCode(200);
            res.setBody(JOB_ID);
        }

        private void sendError(HttpResponse res) {
            res.setStatusCode(400);
            res.setBody(ERROR_MESSAGE);
        }

        private void sendCompletedJobStatus(HttpResponse res) {
            res.setStatusCode(200);
            res.setBody(JSON.serialize(new JobStatus(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_COMPLETE, SUCCESS_MESSAGE)));
        }

        private void sendFailedJobStatus(HttpResponse res) {
            res.setStatusCode(200);
            res.setBody(JSON.serialize(new JobStatus(SQX_Regulatory_Report_eSubmitter.JOB_STATUS_ERROR, ERROR_MESSAGE)));
        }

    }

    private class JobStatus {
        public String status;
        public String message;
        public String submissionRecordId;

        public JobStatus(String status, String message) {
            this.status = status;
            this.message = message;
        }
    }

    enum Mode { PDF_EXPORT, SUBMIT, VALIDATE, SUBMIT_WITH_ERROR, POLL, POLL_WITH_ERROR, POLL_SUBMISSION }
}