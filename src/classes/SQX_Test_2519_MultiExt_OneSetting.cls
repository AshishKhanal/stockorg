/**
* This test class checks the given file extension
*/
@isTest
public class SQX_Test_2519_MultiExt_OneSetting {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
     /*
     * Given: given the multiple file extensions with comma separated
     * Then: split and check the extensions
     */ 
    testmethod static void GivenMultiExtensions_ThenSplitAndCheckExtension(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        String fileExtension='';
        System.runAs(adminUser){
            // Arrange: Create a new rendition provider
            SQX_Rendition_Providers__c obj=new SQX_Rendition_Providers__c();
            obj.compliancequest__Extension__c='pdf,xls,xlsx,txt,html';
            obj.compliancequest__Provider_Class__c='SQX_PDFRenditionProvider1';
            obj.Name='easyPDF Provider';
            insert obj;
            // Act: submit to split and check the extension
            Map<String, SQX_RenditionProvider> providers =SQX_Controlled_Document.getRenditionProviders();
            // Assert 1: Ensure that return true
            fileExtension='txt';
            system.assertEquals(true, providers.containsKey(fileExtension));
            // Assert 2: Ensure that return false
            fileExtension='doc';
            system.assertEquals(false, providers.containsKey(fileExtension));
        }
    }
}