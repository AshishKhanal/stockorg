/*
 * @author: Anish Shrestha
 * @date: 2014/12/09
 * @description: test for the class for creating record activity for CAPA and NC
 */

@isTest
public class SQX_Test_SQX_Record_History{

    /*
     * creates NC and check if SQX_Record_Activity__c record is created or not.
     * edit NC and again check if SQX_Record_Activity__c record is created or not.
     */
    public static testMethod void givenNC_Created_ActivityInserted(){

        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){

            departmentResearch = new SQX_Department__c(
                                                     Name='Research1'
                                                    );
            insert departmentResearch;
        }

        //SQX_Test_NC nc = null;

        System.runas(standardUser){
            SQX_NonConformance__c nonConformance = new SQX_NonConformance__c (
                                        Type__c = SQX_NC.TYPE_INTERNAL,
                                        Type_Of_Issue__c = SQX_NC.ISSUE_TYPE_PRODUCT,
                                        Disposition_Required__c = false,
                                        Occurrence_Date__c = Date.Today(),
                                        SQX_Department__c = departmentResearch.Id);
                                         
                                         
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
            
            Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance));
    
            ncMap.put('Id', 'NC-1');
            
                
            changeSet.add(ncMap);
    
            submissionSet.put('changeSet', changeSet);
    
            String stringChangeSet= JSON.serialize(submissionSet); 
              
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            Id savedNCId= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,params,null);

            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name, Status__c, SQX_Finding__c FROM SQX_NonConformance__c WHERE Id = :savedNCId]);

            SQX_Extension_NC ncExtension = new SQX_Extension_NC(controller);

            Map<String, String> purposeOfSignaturesNC = ( Map<String, String>) JSON.deserialize(ncExtension.getPurposeOfSigJSON(), Map<String, String>.class);

            List<SQX_NC_Record_Activity__c> ncRecordActivity = [SELECT Comment__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_NC_Record_Activity__c WHERE SQX_Nonconformance__c =: savedNCId];

            System.assert(ncRecordActivity.size()==1, 'Record History should saved');

            Map<String, String> params1 = new Map<String, String>();
            params1.put('nextAction', 'Save');
            params1.put('comment', 'Mock comment edit');   
            Id savedNCId1 = SQX_Extension_NC.processChangeSetWithAction(savedNCId, null, null,params1,null);

            List<SQX_NC_Record_Activity__c> ncRecordActivityList = [SELECT Name, Comment__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_NC_Record_Activity__c WHERE SQX_Nonconformance__c =: savedNCId1 ORDER BY Name DESC];
            

            System.assert(ncRecordActivityList[0].Type_Of_Activity__c == 'Edit', 'Expected to get Create and found: '+ ncRecordActivityList[0].Type_of_Activity__c);
            System.assert(ncRecordActivityList[0].Comment__c == 'Mock comment edit', 'Expected to get (Mock comment edit) as comment and found: ' + ncRecordActivityList[0].Comment__c);
            System.assert(ncRecordActivityList[0].Purpose_Of_Signature__c == purposeOfSignaturesNC.get('save'), 'Expected to get '+purposeOfSignaturesNC.get('save')+' as a purposeOfSignature and found: '+ ncRecordActivityList[0].Purpose_Of_Signature__c);        
        }
    }

    /*
     * creates CAPA and check if SQX_Record_Activity__c record is created or not.
     * edit CAPA and again check if SQX_Record_Activity__c record is created or not.
     */
    public static testMethod void givenCAPA_Created_ActivityInserted(){
        
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        
        System.runas(newUser){

            Account supplierAccount = SQX_Test_Account_Factory.createAccount(SQX_Test_Account_Factory.ACCOUNT_TYPE_SUPPLIER);
            Contact supplierContact = SQX_Test_Account_Factory.createContact(supplierAccount);
            User supplierUser = SQX_Test_Account_Factory.createUser(supplierContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER  );

            SQX_CAPA__c capa = new SQX_CAPA__c(
                                    Needs_Effectiveness_Review__c = false,
                                    Effectiveness_Monitoring_Days__c = 90,
                                    Issued_Date__c = Date.Today(),
                                    Target_Due_Date__c = Date.Today().addDays(15),
                                    SQX_Account__c = supplierAccount.Id,
                                    SQX_External_Contact__c = supplierContact.Id);

            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();

            Map<String, Object> capaMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(capa));

            capaMap.put('Id', 'CAPA-1');
            
            changeSet.add(capaMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'Save');
            params.put('comment', 'Mock comment');
            params.put('purposeOfSignature', 'Mock purposeOfSignature');

            Id savedCAPAId= SQX_Extension_CAPA.processChangeSetWithAction('', stringChangeSet, null,params,null);

            ApexPages.StandardController controller = new ApexPages.StandardController([SELECT ID, Name, Status__c, SQX_Finding__c FROM SQX_Capa__c WHERE Id = :savedCAPAId]);

            SQX_Extension_CAPA capaExtension = new SQX_Extension_CAPA(controller);

            Map<String, String> purposeOfSignatures = ( Map<String, String>) JSON.deserialize(capaExtension.getPurposeOfSigJSON(), Map<String, String>.class);

            List<SQX_CAPA_Record_Activity__c> capaRecordActivity = [SELECT Comment__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_CAPA_Record_Activity__c WHERE SQX_CAPA__c =: savedCAPAId];

            System.assert(capaRecordActivity.size()==1, 'Record History should saved');

            Map<String, String> params1 = new Map<String, String>();
            params1.put('nextAction', 'Save');
            params1.put('comment', 'Mock comment edit');
            params1.put('purposeOfSignature', 'Mock purposeOfSignature edit');   
            Id savedCAPAId1 = SQX_Extension_CAPA.processChangeSetWithAction(savedCAPAId, null, null,params1,null);

            List<SQX_CAPA_Record_Activity__c> capaRecordActivityList = [SELECT Name, Comment__c, Purpose_Of_Signature__c, Type_of_Activity__c FROM SQX_CAPA_Record_Activity__c WHERE SQX_CAPA__c =: savedCAPAId1 ORDER BY Name DESC];
            
            System.assert(capaRecordActivityList[0].Type_Of_Activity__c == 'Edit', 'Expected to get Create and found: '+ capaRecordActivityList[0].Type_of_Activity__c);
            System.assert(capaRecordActivityList[0].Comment__c == 'Mock comment edit', 'Expected to get (Mock comment edit) as comment and found: ' + capaRecordActivityList[0].Comment__c);
            System.assert(capaRecordActivityList[0].Purpose_Of_Signature__c == purposeOfSignatures.get('save'), 'Expected to get '+ purposeOfSignatures.get('save') +' as a purposeOfSignature and found: '+ capaRecordActivityList[0].Purpose_Of_Signature__c);       
        }
    }

    /**
    * Given: Comment is provided with length more than maximum length of Record Activity Comment
    * When: Record Activity is inserted with Comment
    * Then: Comment is truncated to the maximum length of Record Activity Comment
    * @story SQX-3974
    */
    public static testMethod void givenCAPA_WhenRecordActivityWithCommentLongerThenMaxLengthIsAdded_ThenItSaves(){

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);

        System.runas(newUser){
            // Arrange: Create a CAPA with Comment of length Maximum Comment Length + 1
            SQX_Test_CAPA_Utility capa = new SQX_Test_CAPA_Utility();
            final Integer MAX_COMMENT_LENGTH = SQX_CAPA_Record_Activity__c.Comment__c.getDescribe().getLength();
            capa.CAPA.Closure_Comment__c = generateRandomString(MAX_COMMENT_LENGTH + 1);

            // Act: Inserted and Retrieve Capa and Capa Record Activity
            Database.SaveResult result = Database.insert(capa.CAPA,false);
            SQX_Capa__c resultCapa = [SELECT Closure_Comment__c FROM SQX_Capa__c WHERE Id = :result.getId()];
            SQX_Record_History.insertRecordHistory(String.valueOf(SQX_Capa__c.SObjectType), result.getId(), (String) result.getId(), resultCapa.Closure_Comment__c, null, null);
            SQX_Capa_Record_Activity__c capaRecordActivity = [SELECT Comment__c FROM SQX_Capa_Record_Activity__c WHERE SQX_CAPA__c = : result.getId()];

            // Assert: Ensure that Comment of Capa is length of Maximum Comment Length + 1 and Comment of Record Activity is length of Maximum Comment Length
            System.assertEquals(MAX_COMMENT_LENGTH + 1, resultCapa.Closure_Comment__c.length(), 'Expected to get ' + MAX_COMMENT_LENGTH + 1 + 'and Found ' + resultCapa.Closure_Comment__c.length());
            System.assertEquals(MAX_COMMENT_LENGTH, capaRecordActivity.Comment__c.length(), 'Expected to get ' + MAX_COMMENT_LENGTH + 'and Found' + capaRecordActivity.Comment__c.length());
        }

    }
    
    /**
    * Method to generate random string of given length.
    * @param len is the length of random string that is to be generated
    * @return randStr is the random string generated by this method
    */
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
}