/**'
 * This invocable class will check for scope match between AI and Investigation or Complaint and Investigation
 */
public with sharing class SQX_Complaint_Investigation_ScopeMatcher {
    
    /**
     * Check scope match between AI and Investigation or Complaint and Investigation and update Complaint or Associated Item with Primary Investigation accordingly.
     */
    @InvocableMethod(label='CQ Check Investigation Scope Match')
    public static void checkInvestigationScopeMatch(List<Id> linkedInvestigationIds) {
        List<SQX_Linked_Investigation__c> linkedInvestigations = [SELECT Id, SQX_Complaint__c, SQX_Investigation__c, SQX_Associated_Item__c FROM SQX_Linked_Investigation__c WHERE Id IN :linkedInvestigationIds];
        
        // it holds id of complaints for those LI which do not have AI
        Set<Id> complaintIds = new Set<Id>();
        // it holds ids of investigation for all LI
        Set<Id> investigationIds =  new Set<Id>();
        // it holds ids of AI for LI
        Set<Id> associatedItemIds = new Set<Id>();
        // it holds ids of complaints which have AI
        Set<Id> complaintIdsWithAI = new Set<Id>();
        
        for (SQX_Linked_Investigation__c li : linkedInvestigations) {
            investigationIds.add(li.SQX_Investigation__c);
            if (li.SQX_Associated_Item__c != null) {
                associatedItemIds.add(li.SQX_Associated_Item__c);
                complaintIdsWithAI.add(li.SQX_Complaint__c);
            } else {
                complaintIds.add(li.SQX_Complaint__c);
            }
        }
        
        List<SQX_Investigation__c> investigations = [SELECT Id, SQX_Part__c, SQX_Part_Family__c, SQX_Defect_Code__c 
                                                            FROM SQX_Investigation__c 
                                                            WHERE Id IN :investigationIds 
                                                            AND SQX_Part_Family__c != NULL 
                                                            AND SQX_Defect_Code__c != NULL];
        
        // case : without AI 
        if (complaintIds.size() > 0) {
            List<SQX_Complaint__c> complaints = [SELECT Id, Status__c, SQX_Part__c, SQX_Part_Family__c, SQX_Complaint_Code__c, SQX_Primary_Investigation__c 
                                                                FROM SQX_Complaint__c 
                                                                WHERE Id IN : complaintIds 
                                                                AND Status__c = :SQX_Complaint.STATUS_OPEN 
                                                                AND SQX_Primary_Investigation__c = NULL];
            
            // time to check scope match between investigations and complaints
            SQX_Linked_Investigation.checkScopeMatchBetweenComplaintAndInvestigationAndUpdateComplaint(investigations, complaints);
        }
        
        // case : with AI
        if (associatedItemIds.size() > 0) {
            List<SQX_Complaint_Associated_Item__c> associatedItems = [SELECT Id, SQX_Primary_Investigation__c, SQX_Complaint__c, Is_Primary__c, SQX_Part_Family__c, SQX_Part__c, SQX_Complaint__r.SQX_Complaint_Code__c
                                                                              FROM SQX_Complaint_Associated_Item__c 
                                                                              WHERE Id IN :associatedItemIds 
                                                                              AND SQX_Primary_Investigation__c = null
                                                                              AND SQX_Complaint__c IN :complaintIdsWithAI
                                                                              AND SQX_Complaint__r.Status__c = :SQX_Complaint.STATUS_OPEN];
            
            // time to check scope match between investigations and associated items
            SQX_Linked_Investigation.checkScopeMatchBetweenAssociatedImtemAndInvestigationAndUpdateAI(investigations, associatedItems);
        }
    }
}