/***
* test for audit checkin and checkout
*/
@isTest
public class SQX_Test_2346_Audit_Checkin_Checkout {
    public class MockHttpClass implements HttpCalloutMock{
        public HttpResponse respond(HttpRequest request){
            
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"spreadsheetId": "Random_Spreadsheet_Id_For_Test",'+
                             '"id": "Random_Spreadsheet_Id_For_Test",'+
                             '"namedRanges": [{ "namedRangeId": "hp34xui50kbn","name": "Status__c","range": {"startRowIndex": 0,"endRowIndex": 1, "startColumnIndex": 9,"endColumnIndex": 10}}]}');
            response.setStatusCode(200);
            return response;
        }
    }    
    static Boolean runAllTests = true,
                    run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOutAndCheckedIn = false,
                    run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut = false,
                    run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn = false,
                    run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeUndoCheckedout = false,
                    run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCannotBeEdited = false,
                    run_givenAnActiveAuditProgramAndAudit_WhenAuditIsDirectlyInitiated_AllAuditScopesAreAddedInAudit = false;
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'testUser1');

        System.runas(adminUser){
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME).save(true);
        }
    }
    
    /**
    * Given: An audit whose status is in progress
    * Action: Remote actions are called to perform callouts
    * Expected: Callout returns correct result
    * @date: 2016-09-09
    * @story: SQX-2346
    */
    public testMethod static void givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOutAndCheckedIn(){
        
        if(!runAllTests && !run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOutAndCheckedIn){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 

            // Arrange : Create a audit and move its status to in progress
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());
            
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.Spreadsheet_Id__c = '1234567890';
            audit.save();

            // Act 1: Try getting named ranges without defining template
            try{
                String getNamedRangeResponse1 = SQX_Extension_Audit.getNamedRanges();
            }catch(Exception ex){

                // Assert 1: Error is expected
                String errorMessage = ex.getMessage();
                System.assert(errorMessage.contains('Template Id hasn\'t been set'));
            }

            // custom setting to define template id
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890'
            });

            // Act 2: Call remote function to perform callouts
            Test.startTest();
            String getNamedRangeResponse = SQX_Extension_Audit.getNamedRanges();
            String getSheetNamedRangeResponse = SQX_Extension_Audit.getSheetNamedRanges(audit.audit.Id, null);
            String getSheetValuesResponse = SQX_Extension_Audit.getSheetValues(audit.audit.Id, new String[] {'Audit_Checklist!A1:B1'});
            Test.stopTest();

            // Assert 2: Callout should return correct results
            System.assert(getNamedRangeResponse.contains('spreadsheetId'));
            System.assert(getSheetNamedRangeResponse.contains('spreadsheetId'));
            System.assert(getSheetValuesResponse.contains('spreadsheetId'));
                
            
        }       
    }

    /**
    * Given: An audit whose status is in progress
    * Action: Audit is checked out
    * Expected: Audit should be checked out setting spreadsheet id and checked out by user
    * @date: 2016-09-09
    * @story: SQX-2346
    */
    public testMethod static void givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut(){
        
        if(!runAllTests && !run_givenAnAudit_WhenAuditIsInProgress_AuditCanBeCheckedOut){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            // Arrange: Create an audit and move its status to in progress
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();


            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();
            
            // custom setting to define template
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890'
            });

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkoutaudit');
            params.put('comment', 'Mock comment');
            
            List<Map<String, String>> requestList = new List<Map<String, String>>();
            Map<String, String> request = new Map<String, String>();
            request.put('VALUES_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            request.put('BATCH_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            requestList.add(request);

            // Act : Checkout audit
            Test.startTest();
            String checkOutResponse = SQX_Extension_Audit.checkOut((String) audit.audit.Id,'{"changeSet":[]}',null,params, null,requestList);
            Test.stopTest();

            SQX_Audit__c currentAudit = [SELECT Id, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert: Checked out user should be set to current user
            System.assertEquals(standardUser.Id, currentAudit.SQX_Checked_Out_By__c, 'Expected user field to be returned');

            // Assert: Spreadsheet Id should be set when checked out
            System.assertEquals('Random_Spreadsheet_Id_For_Test', currentAudit.Spreadsheet_Id__c, 'Expected spreadsheetId to be returned');

            
        }       
    }

    /**
    * Given: An audit whose status is in progress and checked out
    * Action: Checkin the audit
    * Expected: Audit should be checked in resetting spreadsheet id and checked out user
    * @date: 2016-09-09
    * @story: SQX-2346
    */
    public testMethod static void givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn(){
        
        if(!runAllTests && !run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeCheckedIn){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            // Arrange : Create a audit, move its status to in progress and check out audit
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

                
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();

            audit.audit.Spreadsheet_Id__c = '1234567890';
            audit.audit.SQX_Checked_Out_By__c = standardUser.Id;
            audit.save(); 

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkinaudit');
            params.put('recordID', audit.audit.Id);

            // Act : Check in audit
            Test.startTest();
            String result= SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id,'{"changeSet":[]}', null, params, null);
            Test.stopTest();

            SQX_Audit__c currentAudit = [SELECT Id, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert : Checked out user should be reset when checked in
            System.assertEquals(null, currentAudit.SQX_Checked_Out_By__c, 'Expected user field to be cleared');

            // Assert : Spreadsheet id should be reset when checked in
            System.assertEquals('1234567890', currentAudit.Spreadsheet_Id__c, 'Expected spreadsheetId to be cleared');

            
        }       
    }

    /**
    * Given: An audit whose status is in progress and checked out
    * Action: Undo checkout the audit
    * Expected: Checked of the audit should be to previous state resetting spreadsheet id and checked out user
    * @date: 2016-09-09
    * @story: SQX-2346
    */
    public testMethod static void givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeUndoCheckedout(){
        
        if(!runAllTests && !run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCanBeUndoCheckedout){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            // Arrange : Create a audit, move its status to in progress and check out audit
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();

            audit.audit.Spreadsheet_Id__c = '1234567890';
            audit.audit.SQX_Checked_Out_By__c = standardUser.Id;
            audit.save(); 

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'undocheckoutaudit');
            params.put('recordID', audit.audit.Id);

            // Act : Undo check out audit
            Test.startTest();
            String result= SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id,'{"changeSet":[]}', null, params, null);
            Test.stopTest();

            SQX_Audit__c currentAudit = [SELECT Id, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert : Checked out user should be reset when undoing check out
            System.assertEquals(null, currentAudit.SQX_Checked_Out_By__c, 'Expected user field to be cleared');

            // Assert : Spreadsheet id should be reset when undoing check out
            System.assertEquals('1234567890', currentAudit.Spreadsheet_Id__c, 'Expected spreadsheetId to be cleared');

            
        }       
    }

    /**
    * Given: An audit whose status is in progress and set its spreadsheet id
    * Action: Try changing audit fields
    * Expected: Error is thrown when audit is changed
    * @date: 2016-09-09
    * @story: SQX-2346
    */
    public testMethod static void givenAnAudit_WhenAuditIs_CheckedOut_AuditCannotBeEdited(){
        
        if(!runAllTests && !run_givenAnAudit_WhenAuditIs_CheckedOut_AuditCannotBeEdited){
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){ 
                
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();

            audit.audit.Spreadsheet_Id__c = '1234567890';
            audit.audit.SQX_Checked_Out_By__c = standardUser.Id;
            audit.save(); 

            try{
                audit.audit.Title__c = 'Changed Title';
                audit.save();
                System.assert(false, 'Audit cannot be changed when the audit is checked out');
            }catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Audit is Locked.') ? true : false;
                system.assertEquals(expectedExceptionThrown,true);    
            }

            
        }       
    }

    /**
    * Given : An active audit program with two criterion requirement
    * Action : Create an audit and directly initiate the audit
    * Expected : Equal number of criterion requirement is created as audit checklist in the audit
    * @date: 2016-09-12
    * @story: SQX-2345
    */
    public static testmethod void givenAnActiveAuditProgramAndAudit_WhenAuditIsDirectlyInitiated_AllAuditScopesAreAddedInAudit(){

        if(!runAllTests && !run_givenAnActiveAuditProgramAndAudit_WhenAuditIsDirectlyInitiated_AllAuditScopesAreAddedInAudit){
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runas(standardUser){
            /**
             * Arrange:
             * Create  audit program with one program scope that refers to a criteria document.
             * and change the status of both audit program to active approved
             * and add audit with its status set to plan and save the audit
             */

            SQX_Test_Audit_Program program = new SQX_Test_Audit_Program().save();

            SQX_Audit_Program_Scope__c scope2 = program.createScopeWithNewCriteriaDocument(2, adminUser); //refers to a criteria with single requirement


            program.program.Status__c = SQX_Audit_Program.STATUS_ACTIVE;
            program.program.Approval_Status__c = SQX_Audit_Program.APPROVAL_STATUS_APPROVED;
            program.save();

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
          
            // Act 1: Add the audit program in audit and progarm is added to audit check if the audit checklist is added in audit
            audit.includeInProgram(false, program.program).save();

            List<SQX_Audit_Checklist__c> auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
         
            // Assert 1: Audit checklist should not be added to audit until it is in planned state
            System.assert(auditChecklistList.size() == 0, 'Expected the number of checklist to be equal to 0 when the audit is in planned state');
            
            SQX_Audit__c auditData = [SELECT Id, SQX_Audit_Program__c, SQX_Audit_Criteria_Document__c  FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert 2: Audit program can be changed until audit is in planned state
            auditData = [SELECT Id, SQX_Audit_Program__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            System.assert(auditData.SQX_Audit_Program__c == program.program.Id,'Expected to have program in the audit');
            audit.save();

            //Act 3: Change the status of audit to scheduled
            audit.audit.End_Date__c = Date.today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            auditChecklistList = [SELECT Id FROM SQX_Audit_Checklist__c WHERE SQX_Audit__c =: audit.audit.Id];
          
            // Assert 3: Audit checklist should be added to audit and the number of audit checklist should be equal to the criterion document requirement in audit criteria dcument
            System.assert(auditChecklistList.size() == 2, 'Expected the number of checklist to be equal to the number of criterion requirements in audit critera once the audit is scheduled');


        }
    }

    /**
    * Given : A checked out audit (using Excel provider)
    * Action : Check in audit
    * Expected : Audit to be checked in and checked in file to be attached to audit
    * @story: SQX-5025
    */
    public static testmethod void givenACheckedOutAudit_WhenAuditIsCheckedIn_ThenAuditIsCheckedInAndFilesIsSaved(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runas(standardUser){
            /**
             * Arrange:
             * Create  a checked out audit
             */

            // make provider excel
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => SQX_Extension_Audit.PROVIDER_EXCEL
            });

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME).save(true);

            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                 .save();

            Test.startTest();
            checkOutAudit(audit.audit.Id);

            // validating that the record has been checked out
            SQX_Audit__c updatedAudit = [SELECT Id, Audit_Checked_Out__c, Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            System.assertEquals(true, updatedAudit.Audit_Checked_Out__c);
            
            // checking in audit
            checkInAudit(standardUser.Id, audit.audit.Id);
            
            // checking in audit again
            checkInAudit(standardUser.Id, audit.audit.Id);
            Test.stopTest();

            updatedAudit = [SELECT Id, Audit_Checked_Out__c, Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert : Audit has been checked in
            System.assertEquals(false, updatedAudit.Audit_Checked_Out__c);  

            // Assert : Checked in file to be attached with audit
            List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId =: updatedAudit.Id
                                                                AND ContentDocumentId =: updatedAudit.Spreadsheet_Id__c];
            System.assertEquals(1, links.size());

        }
    }


    /**
    * Given : A checked out audit (using Excel provider)
    * Action : Undo checkout audit, checkout and then Check in audit
    * Expected : Working copy field as well as the file have to be cleared upon undocheckout as well as upon checkin
    * @story: SQX-5514
    */
    static testmethod void givenACheckedOutAudit_WhenAuditIsCheckedInOrCheckoutIsUndone_ThenWorkingCopyIsClearedOut() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runas(standardUser){
            
            // make provider excel
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => SQX_Extension_Audit.PROVIDER_EXCEL
            });

            // Arrange: Create a checked out audit

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME).save(true);

            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                 .save();


            Test.startTest();
            checkOutAudit(audit.audit.Id);

            SQX_Audit__c updatedAudit = [SELECT Temp_Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            
            // get working copy
            ContentDocument workingCopy = [SELECT Id FROM ContentDocument WHERE Id =: updatedAudit.Temp_Spreadsheet_Id__c];

            // ACT : Undo Checkout
            SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id,'{"changeSet":[]}', null, new Map<String, String> { 'nextAction' => 'undocheckoutaudit' }, null);

            updatedAudit = [SELECT Temp_Spreadsheet_Id__c FROM SQX_Audit__c  WHERE Id =: audit.audit.Id];

            // Assert : Working copy has been cleared out
            System.assertEquals(null, updatedAudit.Temp_Spreadsheet_Id__c);

            // asserting that the working copy file has been cleared out as well
            System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Id =: workingCopy.Id].size());

            // checkout again
            checkOutAudit(audit.audit.Id);

            // Check-In audit
            checkInAudit(standardUser.Id, audit.audit.Id);

            Test.stopTest();

            updatedAudit = [SELECT Temp_Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert : Working copy has been cleared out
            System.assertEquals(null, updatedAudit.Temp_Spreadsheet_Id__c);

            // asserting that the working copy file has been cleared out as well
            System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Id =: workingCopy.Id].size());
        }

    }

    /**
    * Given : An audit in draft status and in progress stage (using Excel provider)
    * Action : Checkout audit, checkin and then again checkout
    * Expected : Working copy has to be cleared and created and master copy has to be updated
    * @story: SQX-5514
    */
    static testmethod void givenAnAudit_WhenAuditIsCheckedOutMoreThanOnce_ThenMasterCopyIsUpdatedButWorkingCopyIsErasedAndCreated() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runas(standardUser){
            
            // make provider excel
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => SQX_Extension_Audit.PROVIDER_EXCEL
            });

            // Arrange: Create an audit

            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                                        .setStatus(SQX_Audit.STATUS_DRAFT)
                                        .setStage(SQX_Audit.STAGE_PLAN)
                                        .save();
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME).save(true);

            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                 .save();


            Test.startTest();

            // Act: Checkout Audit
            checkOutAudit(audit.audit.Id);

            // Assert : Working Copy and Master Copy are created
            SQX_Audit__c updatedAudit = [SELECT Spreadsheet_Id__c, Temp_Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            System.assertEquals(2, [SELECT Id FROM ContentDocument WHERE Id =: updatedAudit.Spreadsheet_Id__c OR Id =: updatedAudit.Temp_Spreadsheet_Id__c].size());

            // Act : CheckIn Audit
            checkInAudit(standardUser.Id, audit.audit.Id);

            // Assert : Master copy is updated and no working copy exists
            System.assertEquals(2, [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: updatedAudit.Spreadsheet_Id__c].size());
            System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Id =: updatedAudit.Temp_Spreadsheet_Id__c].size());

            // Act : CheckOut Audit again
            checkOutAudit(audit.audit.Id);

            // Assert : Working copy has been created again and Master copy remains the same
            String masterCopyId = updatedAudit.Spreadsheet_Id__c;
            updatedAudit = [SELECT Spreadsheet_Id__c, Temp_Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            System.assertEquals(masterCopyId, updatedAudit.Spreadsheet_Id__c);
            System.assertEquals(1, [SELECT Id FROM ContentDocument WHERE Id =: updatedAudit.Temp_Spreadsheet_Id__c].size());

            Test.stopTest();
        }
    }

    
    /**
    * Given : An audit which has already been checked out and checked in by user A (using Excel provider)
    * Action : Checkout audit and checkin as User B
    * Expected : Audit has to be succesfully checked in
    * @story: SQX-5514
    */
    static testmethod void givenAnAuditCheckedInByUserA_WhenAuditIsCheckedOutAndCheckedInByUserB_ThenAuditIsCheckedIn() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User standardUserB = allUsers.get('testUser1');
        User adminUser = allUsers.get('adminUser');
        
        SQX_Test_Audit audit;

        System.runas(standardUser){
            
            // make provider excel
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Offline_Provider__c => SQX_Extension_Audit.PROVIDER_EXCEL
            });

            // Arrange: Create an audit

            audit = new SQX_Test_Audit(adminUser)
                        .setStatus(SQX_Audit.STATUS_DRAFT)
                        .setStage(SQX_Audit.STAGE_PLAN)
                        .save();
            SQX_Test_Controlled_Document tcd = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME).save(true);

            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.audit.SQX_Audit_Criteria_Document__c = tcd.doc.Id;
            
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                 .save();

            // Share Audit with standardUserB to checkout-checkin
            SQX_Audit__Share audShare = new SQX_Audit__Share();
            audShare.ParentId = audit.audit.Id;
            audShare.UserOrGroupId = standardUserB.Id;
            audShare.AccessLevel = 'Edit';

            insert audShare;
        }

        System.runas(standardUserB) {
            
            Test.startTest();
            
            // Act : Checkout and CheckIn Audit 
            checkOutAudit(audit.audit.Id);

            checkInAudit(standardUserB.Id, audit.audit.Id);

            SQX_Audit__c updatedAudit = [SELECT Audit_Checked_Out__c, Spreadsheet_Id__c, Temp_Spreadsheet_Id__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];

            // Assert : Audit is checked in and Master copy is updated and no working copy exists
            System.assertEquals(false, updatedAudit.Audit_Checked_Out__c);
            System.assertEquals(2, [SELECT Id FROM ContentVersion WHERE ContentDocumentId =: updatedAudit.Spreadsheet_Id__c].size());
            System.assertEquals(0, [SELECT Id FROM ContentDocument WHERE Id =: updatedAudit.Temp_Spreadsheet_Id__c].size());

            Test.stopTest();

        }

    }

    /**
     * Helper method for Audit Check Out
     * Action : Check Out Audit
     * Expected : Audit to be Checked Out
     */
    private static void checkOutAudit(Id auditId) {

        Map<String, String> params = new Map<String, String>();
        params.put('nextAction', 'checkoutaudit');
        params.put('recordID', auditId);

        SQX_Extension_Audit.checkOut(auditId, '{"changeSet":[]}', null, params, null, null);
    }

    /**
     * Helper method for Audit Check In
     * Action : Check In Audit
     * Expected : Audit to be Checked In
     */    
    private static void checkInAudit(Id userId, Id auditId){        
        
            // ACT : Check-In audit 
            SQX_TempStore__c store = new SQX_TempStore__c();
            store.Related_Object_Id__c = userId;
            store.Type__c = 'Audit Checkin Temp Store';
            insert store;

            Attachment at = new Attachment();
            at.Name = 'MockTest.txt';
            at.ParentId = store.Id;
            at.Body = Blob.valueOf('Mock Data');
            insert at;

            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkinaudit');
            params.put('recordID', auditId);
            params.put(SQX_Extension_Audit.JSON_ATTACHMENT_ID, at.Id);

            // Act : Check Out audit and then Check in audit
            String result = SQX_Extension_Audit.processChangeSetWithAction(auditId,'{"changeSet":[]}', null, params, null);        
    }
    
    /**
    * Given :An audit whose status is in progress
    * When : checkout and checkin audit using google sheets after that checkout microsoft excel
    * Then : set spreadsheet id
    * @story: SQX-5098
    */
    public static testMethod void givenAnAudit_WhenCheckoutAndCheckinUsingGoogleSheetAfterCheckoutUsingMSExcel_ThenSetSpreadSheetId(){
                
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(adminUser){ 
            Test.setMock(HttpCalloutMock.class, new MockHttpClass());
            // Arrange: Create an audit and move its status to in progress
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser)
                .setStatus(SQX_Audit.STATUS_DRAFT)
                .setStage(SQX_Audit.STAGE_PLAN)
                .save();
            
            audit.audit.SQX_Audit_Criteria_Document__c = [select id from sqx_controlled_document__c limit 1].id;
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS)
                .save();
            
            // custom setting to define template
            SQX_Custom_Settings_Public__c customSetting = SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890'
            });
            
            Map<String, String> params = new Map<String, String>();
            params.put('nextAction', 'checkoutaudit');
            params.put('comment', 'Mock comment');
            
            List<Map<String, String>> requestList = new List<Map<String, String>>();
            Map<String, String> request = new Map<String, String>();
            request.put('VALUES_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            request.put('BATCH_UPDATE', '"majorDimension" : "ROWS", "range" : "Audit_Checklist!I3:I3","values" : "test" ');
            requestList.add(request);
            
            // Act : Checkout audit (using google sheets)
            Test.startTest();
            String checkOutResponse = SQX_Extension_Audit.checkOut((String) audit.audit.Id,'{"changeSet":[]}', null, params, null,requestList);

            
            SQX_Audit__c currentAudit = [SELECT Id, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            
            // Assert: Spreadsheet Id should be set when checked out
            System.assertEquals('Random_Spreadsheet_Id_For_Test', currentAudit.Spreadsheet_Id__c, 'Expected spreadsheetId to be returned');
            
            
            Map<String, String> params1 = new Map<String, String>();
            params1.put('nextAction', 'checkinaudit');
            params1.put('recordID', audit.audit.Id);
            
            // Act : Check in audit (using google sheets)
            String result= SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id,'{"changeSet":[]}', null, params1, null);
            SQX_Audit__c currentAudit1 = [SELECT Id, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            
            // Assert : Checked out user should be reset when checked in
            System.assertEquals(null, currentAudit1.SQX_Checked_Out_By__c, 'Expected user field to be cleared');
            
            // Assert : Spreadsheet id should be reset when checked in
            System.assertEquals('Random_Spreadsheet_Id_For_Test', currentAudit1.Spreadsheet_Id__c, 'Expected spreadsheetId to be cleared');

            //update custon settings
            customSetting = new SQX_Custom_Settings_Public__c(
                Id = customSetting.Id,
                Audit_Offline_Provider__c = 'Microsoft Excel'
            );
            update customSetting;

            // Assumed audit is check out and spreadsheet id has been set to audit
            String checkOutResponse1 = SQX_Extension_Audit.checkOut((String) audit.audit.Id,'{"changeSet":[]}',null,params, null,requestList);
            Test.stopTest();

            SQX_Audit__c currentAudit2 = [SELECT Id, Name, Spreadsheet_Id__c, SQX_Checked_Out_By__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id];
            
            ContentVersion ctd = [SELECT ContentDocumentId, Title, FileExtension FROM ContentVersion WHERE ContentDocumentId =: currentAudit2.Spreadsheet_Id__c];
            // Assert: Spreadsheet Id should be set when checked out
            System.assertEquals(ctd.ContentDocumentId, currentAudit2.Spreadsheet_Id__c, 'Expected file to be created');
        }       
    }

}