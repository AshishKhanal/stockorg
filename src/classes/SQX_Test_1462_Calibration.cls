/**
* Unit tests for creation or modification of calibration records.
*
* @author Dibya Shrestha
* @date 2015/09/15
* 
*/
@IsTest
public class SQX_Test_1462_Calibration {
    
    static Boolean runAllTests = true,
                   run_givenAddCalibration_Saved = false,
                   run_givenCalibrationDoneBySupplier_NoCertificateNumber_NotSaved = false,
                   run_givenNewCalibrationInterval_negative_value_check = false,
                   run_givenVoid_Close_State_Cannot_Delete = false;

     private static Boolean checkErrorMessage(Database.SaveResult sres, String errmsg){
        return checkErrorMessage(sres.getErrors(), errmsg);
     }

     private static Boolean checkErrorMessage(Database.DeleteResult dres, String errmsg){
        return checkErrorMessage(dres.getErrors(), errmsg);
     }

     private static Boolean checkErrorMessage(Database.Error [] errors, String errmsg) {
        for (Database.Error err : errors) {
            if (err.getMessage().equals(errmsg)) {
                return true;
            }
        }
        return false;
    }
    
    /**
    * test to ensure calibration certificate number is set when calibration is performed by supplier
    */
    public static testmethod void givenCalibrationDoneBySupplier_NoCertificateNumber_NotSaved() {
        if (!runAllTests && !run_givenCalibrationDoneBySupplier_NoCertificateNumber_NotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // add required account for supplier
            Account supplier = SQX_Test_Account_Factory.createAccount();
            
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-1',
                Equipment_Description__c = 'Equipment desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Owner_Type__c = SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, true);
            
            // ACT: add calibration record with supplier without certificate number
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                SQX_Calibration_Supplier__c = supplier.Id
            );
            Database.SaveResult sr = Database.insert(cal1, false);
            
            System.assert(sr.isSuccess() == false,
                'Expected calibration performed by external supplier without certificate not to be saved.');

            // check proper error message for empty certificate error
            System.assert(checkErrorMessage(sr, 'Calibration Certificate Number is required when calibration is performed by a supplier.'),
                'Expected error message to be "Calibration Certificate Number is required when calibration is performed by a supplier."');
        }
    }
    
    /**
    * test to ensure New Calibration Interval should not be negative.
    */
    public static testmethod void givenNewCalibrationInterval_negative_value_check() {
        if (!runAllTests && !run_givenNewCalibrationInterval_negative_value_check) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-1',
                Equipment_Description__c = 'Equipment desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Owner_Type__c = SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, true);
            
            // ACT: add calibration record with supplier without certificate number
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                New_Calibration_Interval__c =-1
            );
            Database.SaveResult sr = Database.insert(cal1, false);
            
             System.assert(sr.isSuccess() == false,
                'Expected calibration not to be saved when New Calibration Interval is negative.');
            
            // check proper error message for empty certificate error
            System.assert(checkErrorMessage(sr, 'New Calibration Interval should not be negative.'),
                'Expected error message to be "New Calibration Interval should not be negative."');
        }
    }

    /**
    * To Ensure Calibration in Close or Void State cannot be deleted.
    */
    public static testmethod void givenVoid_Close_State_Cannot_Delete() {
        if (!runAllTests && !run_givenVoid_Close_State_Cannot_Delete) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(standardUser) {
            // To ensire Calibration not delete when its status is Close
            // add required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'EQP-1',
                Equipment_Description__c = 'Equipment desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Owner_Type__c = SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Requires_Periodic_Calibration__c = false
            );
            Database.insert(eqp1, true);
            
            // add closed calibration record
            SQX_Calibration__c cal1 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                New_Calibration_Interval__c = 1,
                Calibration_Status__c = SQX_Calibration.STATUS_CLOSE
            );
            Database.SaveResult sr = Database.insert(cal1, false);
            
            // ACT: delete calibration record
            Database.DeleteResult delResult = Database.delete(cal1, false);
            
            //Assert: ensuring calibration not to be deleted
            System.assert(delResult.isSuccess() == false,
                'Expected calibration Cannot be deleted.');

             // check proper error message for empty certificate error
            System.assert(checkErrorMessage(delResult, Label.SQX_ERR_MSG_CALIBRATION_IN_CLOSE_OR_VOID_STATUS_CANNOT_BE_DELETED),
                'Expected error message to be "' + Label.SQX_ERR_MSG_CALIBRATION_IN_CLOSE_OR_VOID_STATUS_CANNOT_BE_DELETED  + '"' + delResult);

            
            // To ensure Calibration not delete when its status is Void
            // add void calibration record
            SQX_Calibration__c cal2 = new SQX_Calibration__c(
                SQX_Equipment__c = eqp1.Id,
                Performed_By__c = standardUser.UserName,
                Calibration_Date__c = Date.today(),
                New_Calibration_Interval__c = 1,
                Calibration_Status__c = SQX_Calibration.STATUS_OPEN
            );
            sr = Database.insert(cal2, false);
            cal2.Calibration_Status__c  = SQX_Calibration.STATUS_VOID;
            sr = Database.update(cal2, false);
            
            // ACT: delete calibration record
            delResult = Database.delete(cal2, false);
            
            //Assert: ensuring calibration not to be deleted
            System.assert(delResult.isSuccess() == false,
                'Expected calibration Cannot be deleted.');

             // check proper error message for empty certificate error
            System.assert(checkErrorMessage(delResult, Label.SQX_ERR_MSG_CALIBRATION_IN_CLOSE_OR_VOID_STATUS_CANNOT_BE_DELETED),
                'Expected error message to be "' + Label.SQX_ERR_MSG_CALIBRATION_IN_CLOSE_OR_VOID_STATUS_CANNOT_BE_DELETED  + '"' + delResult);
        }
    }
}