/**
 * Test Class for Medwatch Extension
 */
@isTest
public class SQX_Test_Extension_Medwatch {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    
     @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
       
    /**
     * GIVEN: Medwatch and Null value record
     * WHEN: A2. Age is set and Null Value for D1. Brand Name is set
     * THEN: Medwatch record with A2. Age and null value record D1. Brand Name should be created
     */
    public static testmethod void givenMedwatchAndNullValueRecord_WhenA2AgeIsSetAndNullValueForD1BrandNameIsSet_ThenMedwatchRecordWithA2AgeAndNullRecordD1BrandNameShouldBeCreated(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            Test.setCurrentPage(Page.SQX_Medwatch_Editor);
            
            // Arrange: Create a Medwatch record
            SQX_MedWatch__c medwatch = new SQX_MedWatch__c();
            SQX_Extension_Medwatch medwatchExt = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatch));
            
            // Act: Set Age as 10 and Brand Name as NA
			Test.startTest();
            medwatchExt.Medwatch.A2_Age__c  = 10;
            medwatchExt.Medwatch.A2_Age_Unit__c = 'YR';
            medwatchExt.NullValues.D1_Brand_Name__c = 'NA';
            medwatchExt.save();
            SQX_MedWatch__c mdw = [SELECT Id, A2_Age__c, SQX_Medwatch_Null_Value__r.D1_Brand_Name__c FROM SQX_MedWatch__c  WHERE Id = : medwatchExt.Medwatch.Id];            
            Test.stopTest();
            
            // Assert: Ensure No Errors occurred while creating Medwatch record
            System.assertEquals(0, ApexPages.getMessages().size(), 'Errors occurred when creating medwatch record record. ' + ApexPages.getMessages());
            
            // Assert: Ensure Medwatch record is created
            System.assertNotEquals(null, medwatchExt.Medwatch.Id, 'Medwatch record is not created.');            

            // Assert: Ensure age was set to 10 and brand name was NA                        
            System.assertEquals(10, mdw.A2_Age__c, 'Age should be 10');
            System.assertEquals('NA', mdw.SQX_Medwatch_Null_Value__r.D1_Brand_Name__c, 'Null value wasnt set correctly');          

        }
    }
    
    /**
     * GIVEN: A Medwatch Record
     * WHEN: Related Record is updated
     * THEN: Corresponding related records should be updated 
     */ 
    public static testmethod void givenMedwatchRecord_WhenRelatedRecordIsUpdated_CorrespondingRelatedRecordsShouldBeUpdated(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            
            // Arrange: Create a Medwatch record
            SQX_MedWatch__c medwatch = new SQX_MedWatch__c();                        
            
            SQX_Extension_Medwatch medwatchExt = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatch));
            medwatchExt.Medwatch.A2_Age__c  = 10;
            medwatchExt.Medwatch.A2_Age_Unit__c = 'YR';
            medwatchExt.NullValues.D1_Brand_Name__c = 'NA';
            medwatchExt.save();
            
            SQX_Extension_Medwatch medwatchExt1 = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatchExt.Medwatch));
            
            // Update Medwatch record along with its null value record
            medwatchExt1.Medwatch.A2_Age__c = 67;
            medwatchExt.Medwatch.A2_Age_Unit__c = 'YR';
            medwatchExt1.NullValues.D1_Brand_Name__c = 'ASKU';
            
            // Update F10 Device Code and F10 Patient Code
            medwatchExt1.F10DeviceCodes.get(0).F10_Device_Code__c = '4567';
            
            medwatchExt1.save();
            
            SQX_Medwatch__c medwatchRecord = [SELECT Id, A2_Age__c, SQX_Medwatch_Null_Value__r.D1_Brand_Name__c FROM SQX_MedWatch__c  WHERE Id = :medwatchExt1.Medwatch.Id ];
            SQX_Medwatch_F10_Device_Code__c f10DevCode = [SELECT Id, F10_Device_Code__c FROM SQX_Medwatch_F10_Device_Code__c WHERE Id =:medwatchExt1.F10DeviceCodes.get(0).Id];
            
            // ASSERT: Ensure no errors occurred while updating records
            System.assertEquals(0, ApexPages.getMessages().size(), 'Errors occurred when updating record. ' + ApexPages.getMessages());
               
            // ASSERT: Weight should be 67
            System.assertEquals(67, medwatchRecord.A2_Age__c, 'Expected 67 actual: '+medwatchRecord.A2_Age__c);
                        
            // ASSERT: Null Value for Brand Name should be ASKU
            System.assertEquals('ASKU', medwatchRecord.SQX_Medwatch_Null_Value__r.D1_Brand_Name__c, 'Expected ASKU actual: '+medwatchRecord.SQX_Medwatch_Null_Value__r.D1_Brand_Name__c);
            
            // ASSERT: Ensure F10 Device Code is updated
            System.assertEquals('4567', f10DevCode.F10_Device_Code__c, 'Expected F10 Device Code: 4567, Actual: '+f10DevCode.F10_Device_Code__c);
            
        }
    }
    
    /**
     * GIVEN: Medwatch Record
     * WHEN: Add and Remove Child record
     * THEN: Corresponding Child Record Should be Removed
     */
    public static testmethod void givenMedwatchRecord_WhenAddAndRemoveChildRecord_ThenCorrespondingChildRecordShouldBeRemoved(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);
        
        System.runAs(standardUser){
            Test.setCurrentPage(Page.SQX_Medwatch_Editor);
            
            
            // Arrange: Create a Medwatch record
            SQX_MedWatch__c medwatch = new SQX_MedWatch__c();
            SQX_Extension_Medwatch medwatchExt = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatch));
            
            // Act: Add and Remove Child record
			Test.startTest();
            medwatchExt.AddSuspect();
            medwatchExt.AddD11MedicalProducts();
            medwatchExt.AddF10DeviceCodes();
            medwatchExt.AddF10PatientCodes();
            medwatchExt.AddH6DeviceCodes();
            medwatchExt.AddH6PatientCodes();
            medwatchExt.AddH6MethodCodes();
            medwatchExt.AddH6ResultCodes();
            medwatchExt.AddH6ConclusionCodes();
            medwatchExt.save();
            
            SQX_Extension_Medwatch medwatchExt1 = new SQX_Extension_Medwatch(new ApexPages.StandardController([
                SELECT Id, SQX_Medwatch_Null_Value__c, F11_Date_Report_Sent_To_FDA__c, F13_Date_Sent_to_Manufacturer__c
                FROM SQX_MedWatch__c
                WHERE Id =: medwatchExt.Medwatch.Id
            ]));
            medwatchExt1.RemoveSuspect();
            medwatchExt1.RemoveD11MedicalProducts();
            medwatchExt1.RemoveF10DeviceCodes();
            medwatchExt1.RemoveF10PatientCodes();
            medwatchExt1.RemoveH6DeviceCodes();
            medwatchExt1.RemoveH6PatientCodes();
            medwatchExt1.RemoveH6MethodCodes();
            medwatchExt1.RemoveH6ResultCodes();
            medwatchExt1.RemoveH6ConclusionCodes();    
            medwatchExt1.save();
            
            Test.stopTest();
            
            // Assert: Ensure Suspect product was removed
            System.assertEquals(1, medwatchExt1.Suspects.size(), 'Expected 0 Suspect Product but was :' +medwatchExt1.Suspects.size());
            // Assert: Ensure D11 Medical Product was removed
            System.assertEquals(1, medwatchExt1.D11MedicalProducts.size(), 'Expected 0 D11 Medical Product but was :' +medwatchExt1.D11MedicalProducts.size());
            // Assert: Ensure F10 Device Code was removed
            System.assertEquals(1, medwatchExt1.F10DeviceCodes.size(), 'Expected 0 F10 Device Code but was :' +medwatchExt1.F10DeviceCodes.size());
            // Assert: Ensure F10 Patient Code was removed
            System.assertEquals(1, medwatchExt1.F10PatientCodes.size(), 'Expected 0 F10 Patient Code but was :' +medwatchExt1.F10PatientCodes.size());
            // Assert: Ensure H6 Device Code was removed
            System.assertEquals(1, medwatchExt1.H6DeviceCodes.size(), 'Expected 0 H6 Device Code but was :' +medwatchExt1.H6DeviceCodes.size());
             // Assert: Ensure H6 Patient Code was removed
            System.assertEquals(1, medwatchExt1.H6PatientCodes.size(), 'Expected 0 H6 Patient Code but was :' +medwatchExt1.H6PatientCodes.size());
            // Assert: Ensure H6 Method Code was removed
            System.assertEquals(1, medwatchExt1.H6MethodCodes.size(), 'Expected 0 H6 Method Code but was :' +medwatchExt1.H6MethodCodes.size());
            // Assert: Ensure H6 Result Code was removed
            System.assertEquals(1, medwatchExt1.H6ResultCodes.size(), 'Expected 0 H6 Result Code but was :' +medwatchExt1.H6ResultCodes.size());
            // Assert: Ensure H6 Conclusion Code was removed
            System.assertEquals(1, medwatchExt1.H6ConclusionCodes.size(), 'Expected 0 H6 Conclusion Code but was :' +medwatchExt1.H6ConclusionCodes.size());           

        }

    }

    /**
     * GIVEN : Extension class for Medwatch
     * WHEN : New medwatch is created using the Extension
     * THEN : The Null Values Records that are created have a link back to their child record
    */
    static testmethod void givenMedwatchExtension_WhenNewMedwatchIsCreated_ThenTheNullValueRecordsShouldHaveLinkToThierChildRecords() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get(STANDARD_USER);

        System.runAs(standardUser){

            Test.setCurrentPage(Page.SQX_Medwatch_Editor);

            // Arrange: Setup extension
            SQX_MedWatch__c medwatch = new SQX_MedWatch__c();
            SQX_Extension_Medwatch medwatchExt = new SQX_Extension_Medwatch(new ApexPages.StandardController(medwatch));

            // Act: Create Medwatch record
            Test.startTest();
            medwatchExt.save();

            // Assert : Null Values Records have link to their child records
            System.assertEquals(medwatch.Id, [SELECT SQX_Medwatch__c FROM SQX_Medwatch_Null_Value__c WHERE Id =: medwatchExt.NullValues.Id].SQX_Medwatch__c, 'Expected Medwatch link in Null Values Record');
            System.assertEquals(medwatchExt.Suspects[0].Id, [SELECT SQX_Medwatch_Suspect_Product__c FROM SQX_Medwatch_Suspect_Product_Null_Value__c WHERE Id =: medwatchExt.Suspects[0].SQX_Suspect_Product_Null_Value__c].SQX_Medwatch_Suspect_Product__c, 'Expected Suspect Product link in Null Values Record');
            System.assertEquals(medwatchExt.D11MedicalProducts[0].Id, [SELECT SQX_Medwatch_D11_Medical_Product__c FROM SQX_Medwatch_D11_Medical_Prod_Null_Value__c WHERE Id =: medwatchExt.D11MedicalProducts[0].SQX_D11_Medical_Product_Null_Value__c].SQX_Medwatch_D11_Medical_Product__c, 'Expected D11 Medical Product link in Null Values Record');

            Test.stopTest();
        }
    }
}