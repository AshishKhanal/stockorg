/**
 * test class for closing inspection
 */
@isTest
public with sharing class SQX_Test_2812_Close_Inspection {
    
    /**
     * TEST SCENARIOS:
     * 1. When the inspection is closed with result pass, material transaction record is created with transaction code 'INSPECTION_PASS' [givenAnPassedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode]
     * 2. When the inspection is closed with result failed, material transaction record is created with transaction code 'INSPECTION_FAIL' [givenAnFailedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode]
     * 3. When the passed inspection is closed, its corresponding NC is not created [givenAnPassedInspection_WhenInspectionIsClosed_NCIsNotCreated]
     * 4. When the failed inspection is closed, its corresponding NC is created [givenAnFailedInspection_WhenInspectionIsClosed_NCIsCreated]
     * 5. When the failed part inspection is closed with failed inspection details, defects are created for only those inspection details that have failed [givenAnFailedInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreCreated]
     * 6. When the closed inspection is reopened, error is thrown [givenAClosedInspection_WhenReopened_ErrorIsThrown]
     * 7. When the failed process inspection is closed with failed inspection details, NC is created but defects are not created [givenAnFailedProcessInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreNotCreated]
     */
    
    public static Boolean runAllTests = true,
                            run_givenAnPassedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode = false,
                            run_givenAnFailedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode = false,
                            run_givenAnPassedInspection_WhenInspectionIsClosed_NCIsNotCreated = false,
                            run_givenAnFailedInspection_WhenInspectionIsClosed_NCIsCreated = false,
                            run_givenAnFailedInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreCreated = false,
                            run_givenAClosedInspection_WhenReopened_ErrorIsThrown = false,
                            run_givenAnFailedProcessInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreNotCreated = false;
    
    public static String PART_FAMILY_1 = 'PART_FAMILY_1',
                            PART_1 = 'PART_1',
                            PART_2 = 'PART_2',
                            PROCESS_1 = 'PROCESS_1';
    
    // setup data for user
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * Given : An inspection in created
    * When : Inspection in closed
    * Then : Corresponding material transaction record is created and its corresponding details are copied
    */
    public testmethod static void givenAnPassedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode(){
        if (!runAllTests && !run_givenAnPassedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        SQX_Custom_Settings_Public__c customSetting;
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
            
            // custom setting to define template id
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Audit_Template_Spreadsheet_Id__c => 'AMBARKAAR_AUDIT_1234567890',
                Schema.SQX_Custom_Settings_Public__c.Community_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.Org_Base_URL__c => 'https://login.salesforce.com',
                Schema.SQX_Custom_Settings_Public__c.NC_Analysis_Report__c => '1234567890',
                Schema.SQX_Custom_Settings_Public__c.Inspection_Outbound_Transactions__c => true
            });
        }
        System.runas(standardUser){
            // Arrange : An inspection is created
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();

            inspection.inspection.Lot_Quantity__c = 10;
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act : Inspection is completed and closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Previous_Status__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : Corresponding material transaction record is created and its corresponding details are copied
            List<SQX_Material_Transaction__c> mt = [SELECT Id, Transaction_Code__c, Transaction_Date__c, Status__c, Quantity__c FROM SQX_Material_Transaction__c WHERE SQX_Inspection__c =: inspection.inspection.Id ORDER By CreatedDate ASC];
            System.assertEquals(2, mt.size());
            System.assertEquals(SQX_Material_Transaction.CODE_TRANSACTION_INSPECTION_PASS, mt[1].Transaction_Code__c);
            System.assertEquals(SQX_Material_Transaction.STATUS_PENDING, mt[1].Status__c);
            System.assertEquals(Date.Today(), mt[1].Transaction_Date__c);
            System.assertEquals(10, mt[1].Quantity__c);

        }
    }
    
    /**
    * Given : An inspection in created
    * When : Inspection in closed
    * Then : Corresponding material transaction record is created
    */
    public testmethod static void givenAnFailedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode(){
        if (!runAllTests && !run_givenAnFailedInspection_WhenInspectionIsClosed_MaterialTransactionRecordIsCreatedWithProperCode) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        SQX_Custom_Settings_Public__c customSetting;
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
            
            // custom setting to define template id
            customSetting = new SQX_Custom_Settings_Public__c(
                Audit_Template_Spreadsheet_Id__c = 'AMBARKAAR_AUDIT_1234567890',
                Community_URL__c = 'https://login.salesforce.com',
                Org_Base_URL__c = 'https://login.salesforce.com',
                NC_Analysis_Report__c = '1234567890',
                Inspection_Outbound_Transactions__c = true
            );
            
            insert customSetting;
        }
        System.runas(standardUser){
            // Arrange : An inspection is created
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[0].Number_of_Defects__c   = 10;
            inspectionDetails[0].Defective_Quantity__c  = 10;
            inspectionDetails[1].Result__c              = SQX_Inspection.INSPECTION_RESULT_FAIL;
            inspectionDetails[1].Number_of_Defects__c   = 20;
            inspectionDetails[1].Defective_Quantity__c  = 20;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Act : Inspection is completed and closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Previous_Status__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : Corresponding material transaction record is created
            List<SQX_Material_Transaction__c> mt = [SELECT Id, Transaction_Code__c FROM SQX_Material_Transaction__c WHERE SQX_Inspection__c =: inspection.inspection.Id ORDER By CreatedDate ASC];
            System.assertEquals(2, mt.size());
            System.assertEquals(SQX_Material_Transaction.CODE_TRANSACTION_INSPECTION_FAIL, mt[1].Transaction_Code__c);

        }
    }
    
    /**
    * Given : An inspection is created and completed with inspection resulted to pass
    * When : Inspection in closed
    * Then : NC is not created
    */
    public testmethod static void givenAnPassedInspection_WhenInspectionIsClosed_NCIsNotCreated(){
        if (!runAllTests && !run_givenAnPassedInspection_WhenInspectionIsClosed_NCIsNotCreated) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed with inspection resulted to pass
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[0].Number_of_Defects__c   = 10;
            inspectionDetails[0].Defective_Quantity__c  = 10;
            inspectionDetails[1].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Number_of_Defects__c   = 20;
            inspectionDetails[1].Defective_Quantity__c  = 20;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            // Act : Inspection in closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c,
                                                Previous_Status__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : NC is not created
            System.assertEquals(0, [SELECT Id FROM SQX_Nonconformance__c WHERE SQX_Inspection__c =: inspection.inspection.Id].size());
            
            
        }
    }
    
    /**
    * Given : An inspection is created and completed with inspection resulted to fail
    * When : Inspection in closed
    * Then : NC is created
    */
    public testmethod static void givenAnFailedInspection_WhenInspectionIsClosed_NCIsCreated(){
        if (!runAllTests && !run_givenAnFailedInspection_WhenInspectionIsClosed_NCIsCreated) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed with inspection resulted to fail
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[0].Number_of_Defects__c   = 10;
            inspectionDetails[0].Defective_Quantity__c  = 10;
            inspectionDetails[1].Result__c              = SQX_Inspection.INSPECTION_RESULT_FAIL;
            inspectionDetails[1].Number_of_Defects__c   = 20;
            inspectionDetails[1].Defective_Quantity__c  = 20;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            // Act : Inspection in closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c,
                                                Previous_Status__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : NC is created
            System.assertEquals(1, [SELECT Id FROM SQX_Nonconformance__c WHERE SQX_Inspection__c =: inspection.inspection.Id].size());
            
            
        }
    }
    
    /**
    * Given : An inspection is created and completed with inspection resulted to fail
    * When : Inspection in closed
    * Then : NC with defect is created
    */
    public testmethod static void givenAnFailedInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreCreated(){
        if (!runAllTests && !run_givenAnFailedInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreCreated) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed with inspection resulted to fail
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[0].Number_of_Defects__c   = 10;
            inspectionDetails[0].Defective_Quantity__c  = 10;
            inspectionDetails[1].Result__c              = SQX_Inspection.INSPECTION_RESULT_FAIL;
            inspectionDetails[1].Number_of_Defects__c   = 20;
            inspectionDetails[1].Defective_Quantity__c  = 20;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            // Act : Inspection in closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c,
                                                Previous_Status__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : NC with defect is created
            List<SQX_Nonconformance__c> nc = [SELECT Id, SQX_Finding__c FROM SQX_Nonconformance__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(1, nc.size());
            
            System.assertEquals(1, [SELECT Id FROM SQX_NC_Defect__c WHERE SQX_Nonconformance__c =: nc[0].Id].size());
            
        }
    }
    
    /**
    * Given : An inspection in closed
    * When : Inspection in reopened
    * Then : Error is thrown
    */
    public testmethod static void givenAClosedInspection_WhenReopened_ErrorIsThrown(){
        if (!runAllTests && !run_givenAClosedInspection_WhenReopened_ErrorIsThrown) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Part_Family__c partFamily = new SQX_Part_Family__c();
        SQX_Part__c part = new SQX_Part__c();
        
        System.runAs(adminUser){
            partFamily = SQX_Test_Inspection.newPartFamily(PART_FAMILY_1);
            part = SQX_Test_Inspection.newPart(partFamily, PART_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and closed
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(part.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PART);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, part.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[1].Result__c = SQX_Inspection.INSPECTION_RESULT_PASS;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Previous_Status__c,
                                                Review_Required__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Act : Inspection is reopened
            inspect.Status__c = SQX_Inspection.INSPECTION_STATUS_OPEN;
            Database.SaveResult updateResult = Database.update(inspect, false);

            // Assert: Save is unsuccessful
            System.assert(!updateResult.isSuccess(), 'Expected save to be unsuccessful');

        }
    }
    
    /**
    * Given : An process inspection is created and completed with inspection resulted to fail
    * When : Inspection in closed
    * Then : NC is created but defect is not created
    */
    public testmethod static void givenAnFailedProcessInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreNotCreated(){
        if (!runAllTests && !run_givenAnFailedProcessInspectionWithFailedInspectionDetails_WhenInspectionIsClosed_DefectsAreNotCreated) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Standard_Service__c process = new SQX_Standard_Service__c();
        
        System.runAs(adminUser){
            process = SQX_Test_Inspection.newProcess(PROCESS_1);
        }
        System.runas(standardUser){
            // Arrange : An inspection is created and completed with inspection resulted to fail
            Map<String, String> associationMap = new Map<String, String>();
            associationMap.put(process.Id, SQX_Test_Inspection.ASSOCIATION_TYPE_PROCESS);
            SQX_Test_Inspection.createActiveInspectionCriteria(associationMap, 2);
            
            SQX_Test_Inspection inspection = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PROCESS, process.Id);
            inspection.save();
            
            //method invoked by flow in managed package
            //SQX_Specification_Migration.copySpecificationToInspectionDetail(new List<SQX_Inspection__c>{inspection.inspection});            
            
            List<SQX_Inspection_Detail__c> inspectionDetails = [SELECT ID, Result__c FROM SQX_Inspection_Detail__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(2, inspectionDetails.size());  
            
            inspectionDetails[0].Result__c              = SQX_Inspection.INSPECTION_RESULT_PASS;
            inspectionDetails[0].Number_of_Defects__c   = 10;
            inspectionDetails[0].Defective_Quantity__c  = 10;
            inspectionDetails[1].Result__c              = SQX_Inspection.INSPECTION_RESULT_FAIL;
            inspectionDetails[1].Number_of_Defects__c   = 20;
            inspectionDetails[1].Defective_Quantity__c  = 20;
            Database.SaveResult [] results = Database.update(inspectionDetails, false);
                        
            // Act : Inspection in closed
            Test.setCurrentPageReference(Page.SQX_Inspection_Esig); 
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_ID, inspection.inspection.Id);
            System.currentPageReference().getParameters().put(SQX_Extension_Inspection.PARAM_MODE, SQX_Inspection.PurposeOfSignature_CompletingInspection);
            SQX_Inspection__c inspect = [SELECT Id, 
                                                Name,
                                                OwnerId,
                                                Status__c,
                                                Approval_Status__c,
                                                Passed_Characteristics__c, 
                                                Skipped_Characteristics__c,
                                                Failed_Characteristics__c,
                                                Total_Characteristics__c,
                                                Review_Required__c,
                                                Previous_Status__c
                                         FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id];
            ApexPages.StandardController controller = new ApexPages.StandardController(inspect);
            SQX_Extension_Inspection extension = new SQX_Extension_Inspection(controller);
            extension.recordActivityComment = 'Complete Inspection.';
            extension.saveRecord();
            
            System.assertEquals(SQX_Inspection.INSPECTION_STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Inspection__c WHERE Id =: inspection.inspection.Id].Status__c);
            
            // Assert : NC is created but defect is not created
            List<SQX_Nonconformance__c> nc = [SELECT Id, SQX_Finding__c FROM SQX_Nonconformance__c WHERE SQX_Inspection__c =: inspection.inspection.Id];
            System.assertEquals(1, nc.size());
            
            System.assertEquals(0, [SELECT Id FROM SQX_NC_Defect__c WHERE SQX_Nonconformance__c =: nc[0].Id].size());
            
        }
    }

}