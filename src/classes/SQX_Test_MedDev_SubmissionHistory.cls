/**
* Test for submission record creation of MedDev Record.
*/
@IsTest
public class SQX_Test_MedDev_SubmissionHistory {
    
     static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        }

    /**
     * GIVEN : A MedDev Record
     * WHEN :  When the content version for the record is created.
     * THEN :  Submission history for the MedDev record is created.
     */
    public testMethod static void givenMedDev_WhenContentVersionIsInserted_SubmissionHistoryIsCreated(){

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        System.runas(standardUser){

            //ARRANGE: Create a complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            //ARRANGE: Create regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_EMA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            insert regReport;

            // ARRANGE : MedDev is created
            SQX_MedDev__c medDev = new SQX_MedDev__c();
            medDev.SQX_Regulatory_Report__c=regReport.Id;

            insert medDev;

            regReport.Report_Id__c = medDev.Id;
            update regReport;
            
            // Create file content that goes in attachment section for meddev.
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='Meddev-File';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with Meddev');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // link it with meddev
            ContentDocumentLink lnk = new ContentDocumentLink (LinkedEntityId =medDev.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_INFERRED);
            
            insert lnk;

            Test.startTest();

            // Act: Create initial contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_INITIAL, new String[] { medDev.SQX_Regulatory_Report__c });
            cv.Title ='Test-Title';
            cv.PathOnClient = medDev.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');

            insert cv;

            // Assert: Submission history for MedDev needs to be created.
            SQX_Submission_History__c subHistory = [SELECT Id, SQX_MedDev__c, Status__c FROM SQX_Submission_History__c LIMIT 1];
            System.assertEquals(SQX_Submission_History.STATUS_GENERATING_ADDITIONAL_FILES, subHistory.Status__c);
            System.assertEquals(medDev.Id, subHistory.SQX_MedDev__c);

            // Assert: Get content Document Link
            List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistory.Id];

            // Assert: 2 ContentDocumentLinks should be linked with submission record.
            System.assertEquals(2, links.size());

            // Assert: Reg Report should have the new submission history linked
            System.assertEquals(subHistory.Id, [SELECT SQX_Submission_History__c FROM SQX_Regulatory_Report__c WHERE Id =: regReport.Id].SQX_Submission_History__c);

            // Act: Mocking submission by inserting content version again despite the pending submission
            cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_INITIAL, new String[] { medDev.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=medDev.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            Database.SaveResult result = Database.insert(cv, false);

            // Assert: Error is thrown as pending submission already exists
            System.assertEquals(false, result.isSuccess());
            System.assert(SQX_Utilities.checkErrorMessage(result.getErrors(), 'A submission record already exists for the Regulatory Report.'), 'Unexpected error received ' + result.getErrors());

            Test.stopTest();

            // Act: Insert new final content
            cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_FINAL, new String[] { medDev.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient = medDev.Name + '.pdf';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;

            // Assert: Submission history status needs to be updated
            subHistory = [SELECT Id, Status__c FROM SQX_Submission_History__c WHERE Id =: subHistory.Id];
            System.assertEquals(SQX_Submission_History.STATUS_PENDING, subHistory.Status__c);

            // Three files need to be linked with the submissio record
            System.assertEquals(3, [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistory.Id].size());

        }
    }
    
    
    /**
     * GIVEN: Meddev record linked with Followup Regulatory Report.
     * WHEN: createSubmissionRecord() method is invoked for the General Report record
     * THEN: A Submission History record should be created for that General Report record and isFollowup should be set
     * to true and report number should be populated with the followup number
     */
    static testMethod void givenMedwatchFollowupRegulatoryReport_WhenContentVersionOfTheMedwatchIsCreated_ThenIsFollowupAndReportNumberShouldBeSetInTheSubmissionRecord(){
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            
            // ARRANGE:reate Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            
            // ARRANGE: Create submissionRecord for parent regulatory report.
            SQX_Submission_History__c submissionRecord = new SQX_Submission_History__c(
                Status__c = 'Complete',
                SQX_Complaint__c = complaint.complaint.Id,
                Submitted_By__c= UserInfo.getName(),
                SQX_Submitted_By__c = UserInfo.getUserId(),
                Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_TGA,
                Submitted_Date__c=System.today(),
                Due_Date__c = Date.today() + 2
            );
            
            insert submissionRecord;
            
            //ARRANGE: Create parent regulatory report
            SQX_Regulatory_Report__c regulatoryReportParent = new SQX_Regulatory_Report__c();
            regulatoryReportParent.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regulatoryReportParent.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regulatoryReportParent.SQX_Complaint__c=complaint.complaint.Id;
            regulatoryReportParent.Due_Date__c= Date.today().addDays(3);
            regulatoryReportParent.SQX_Submission_History__c = submissionRecord.Id;
            regulatoryReportParent.Status__c = 'Complete';
            
            insert regulatoryReportParent;
			
            //ARRANGE: Create followup regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_FDA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            regReport.SQX_Follow_Up_Of__c = regulatoryReportParent.Id;
            regReport.compliancequest__Follow_Up_Number_Internal__c =3;
            
			insert regReport;
            
             // ARRANGE : Medwatch is created
            SQX_Meddev__c meddev = new SQX_Meddev__c();
            meddev.SQX_Regulatory_Report__c=regReport.Id;
            
    		insert meddev;
            
            regReport.Report_Id__c = meddev.Id;
            update regReport;
            
            // Act: Create contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { meddev.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=meddev.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;
            
            // ASSERT: Query submissionHistory
            List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id, SQX_General_Report__c, Status__c,Report_Number__c,
                                                                    Is_Follow_Up__c FROM SQX_Submission_History__c WHERE SQX_Meddev__c =:meddev.Id ];
            // ASSERT: The report number and is followup should be set.
            System.assertEquals('003', subHistoryRecordList[0].Report_Number__c);
            System.assertEquals(true, subHistoryRecordList[0].Is_Follow_Up__c);
        }
    }

    /**
     * GIVEN: Meddev record.
     * WHEN:  When user other than Meddev creator adds attachments.
     * THEN: A Submission should not fail.
     */
    static testMethod void givenMeddevRecord_WhenUserOtherThanMeddevCreatorAddsAttachments_ThenSubmissionShouldNotFail(){
        
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        User adminUser = SQX_Test_Account_Factory.getUsers().get(ADMIN_USER);
        
        SQX_Meddev__c meddev = new SQX_Meddev__c();
        SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
        SQX_Test_Complaint complaint; 
        
        System.runAs(adminUser) {
            
            // ARRANGE:Create Complaint and initiate
            complaint= new SQX_Test_Complaint(adminUser);
            
            complaint.save();
            
            complaint.addPolicy(true,Date.today(),standardUser.Id);
            complaint.initiate();
            
        }
        
        system.runAs(standardUser){
            
            //ARRANGE: Create regulatory report
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_EMA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            
			insert regReport;
           
           
            meddev.SQX_Regulatory_Report__c=regReport.Id;
           
            // ARRANGE : Meddev is created
    		insert meddev;
            
            regReport.Report_Id__c = meddev.Id;
            update regReport;
        }
        
        
        system.runas(adminUser){
            
            // ARRANGE: Create file content that goes in attachment section for meddev.
            ContentVersion cVersion = new ContentVersion();
            cVersion.Title='Meddev-File';
            cVersion.PathOnClient='test.txt';
            cVersion.VersionData = Blob.valueOf('This is the content for file associated with Meddev');
            insert cVersion;
            
            cVersion = [SELECT Id, ContentDocumentId from ContentVersion where Id=: cVersion.Id];
                        
            // ARRANGE: link it with meddev
            ContentDocumentLink link = new ContentDocumentLink (LinkedEntityId =meddev.Id, ContentDocumentId = cVersion.ContentDocumentId, ShareType = SQX_ContentDocument.CONTENTDOCUMENTLINK_SHARETYPE_VIEWER);
            
            insert link;
        }
         
        
        
        System.runAs(standardUser) {
            
            // Act: Create contentVersion
            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { meddev.SQX_Regulatory_Report__c });
            cv.Title='Test-Title';
            cv.PathOnClient=meddev.Name + '.xml';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;
        }
        
         // ASSERT: Query submissionHistory
        List<SQX_Submission_History__c> subHistoryRecordList = [SELECT Id FROM SQX_Submission_History__c];
        
        // ASSERT: Get content Document Link
        List<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =:subHistoryRecordList[0].Id];
        
        // ASSERT: There should be 2 ContentDocumentLinks.
        System.assertEquals(2, links.size());
            
        
    }


}