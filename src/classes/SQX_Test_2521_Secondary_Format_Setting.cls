/**
*   @description : Class for testing various scenarios using controlled document's secondary format setting
*   @author : Piyush Subedi
*   @date : 2016/12/27
*   @story : 2521 [epic]
*/
@isTest
public class SQX_Test_2521_Secondary_Format_Setting{

    static Blob result = Blob.valueOf(''); // stores the entire content of the secondary content generated after rendition

    public static String workflowJson; // stores the dynamic workflow generated from secondary format setting

    static Boolean runAllTests = true,
                    run_givenAControlledDocumentOfTypeTemplate_WhenControlledDocumentReferencingThatTemplateDocIsCreated_ThenSecondaryFormatSettingIsTransferredFromTemplate = false,
                    run_givenSetOfRecordOfSecondaryFormatSetting_WhenControlledDocumentNotReferencingAnyTemplateDocIsCreated_ThenDefaultSecondaryFormatSettingShouldBeAppliedToDoc= false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToExcludeProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenOnlyPrimaryContentShouldBeRenderedIntoSecondaryContent = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToAppendProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBeAppendedWithProfilePageIntoSecondaryContent = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToPrependProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBePrependedWithProfilePageIntoSecondaryContent = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToIncludeWatermark_WhenUserTriesToGenerateSecondaryContent_ThenSecondaryContentWithWatermarkShouldBeGenerated = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToIncludeStampPDF_WhenUserGeneratesSecondaryContent_ThenSecondaryContentShouldBeGeneratedStampedWithGivenPDF = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToEmbedPrintScript_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSet = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithOtherSettingsSetToDefault_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithDefaultValues = false,
                    run_givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithValuesSpecifiedInSetting_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithSuppliedValues = false;

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');

        System.runAs(standardUser){

            // custom setting to define CQ's rendition provider
            SQX_Rendition_Providers__c providerSetting = new SQX_Rendition_Providers__c(
                Name = 'easyPDFProvider',
                Extension__c = '*',
                Provider_Class__c = 'SQX_PDFRenditionProvider1',
                Provider_Namespace__c = SQX.NSPrefix.substringBefore('__')
            );
            insert providerSetting;

            // creating a controlled document record of 'Template Document' type
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document(SQX_Controlled_Document.RT_TEMPLATE_API_NAME);
            tstObj.doc.Document_Number__c = 'TDOC_1';
            tstObj.save(true);

            // creating controlled documents with the various secondary format settings

            SQX_Test_Controlled_Document tstObj1 = new SQX_Test_Controlled_Document();
            tstObj1.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO;
            tstObj1.doc.Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC;
            tstObj1.doc.Document_Number__c = 'CDOC_1';
            tstObj1.save(true);

            /********************************************************************************
            * NOTE :
            *   Custom Metadata Type records cannot be created or edited using Apex code
            *   Hence, in order to test various configurations of Secondary Format Setting, several test records are created beforehand
            *    However, since these records will be eliminated when deploying to packaging org, the test should be able to handle the case when test records are missing
            *
            *********************************************************************************/

            // 'TestCase1' includes configuration to append profile page
            List<Secondary_Format_Setting__mdt> set1 = [SELECT DeveloperName FROM Secondary_Format_Setting__mdt WHERE MasterLabel = 'TestCase1'];

            if( set1.size() > 0 ){

                // non-production org, so test records are present

                // 'TestCase2' includes configuration to pre-pend profile page
                Secondary_Format_Setting__mdt set2 = [SELECT DeveloperName FROM Secondary_Format_Setting__mdt WHERE MasterLabel = 'TestCase2'];

                SQX_Test_Controlled_Document tstObj2 = new SQX_Test_Controlled_Document();
                tstObj2.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO;
                tstObj2.doc.Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC;
                tstObj2.doc.Secondary_Format_Setting__c = set1.get(0).DeveloperName;
                tstObj2.doc.Document_Number__c = 'CDOC_2';
                tstObj2.save(true);

                SQX_Test_Controlled_Document tstObj3 = new SQX_Test_Controlled_Document();
                tstObj3.doc.Secondary_Content__c = SQX_Controlled_Document.SEC_CONTENT_SYNC_AUTO;
                tstObj3.doc.Synchronization_Status__c = SQX_Controlled_Document.SYNC_STATUS_OUT_OF_SYNC;
                tstObj3.doc.Secondary_Format_Setting__c = set2.DeveloperName;
                tstObj3.doc.Document_Number__c = 'CDOC_3';
                tstObj3.save(true);

            }
        }
    }


    /**
    * returns controlled document with the given document number
    * @param documentNumber is controlled document number
    */
    static SQX_Controlled_Document__c getDoc(String documentNumber){

        List<SQX_Controlled_Document__c> docs = [SELECT Id, Content_Reference__c, Description__c, Title__c,
                Synchronization_Status__c, Effective_Date__c, Approval_Status__c, Document_Status__c, Secondary_Content_Reference__c, OwnerId,
                Secondary_Format_Setting__c, Document_Number__c
                FROM SQX_Controlled_Document__c
                WHERE Document_Number__c = : documentNumber];

        if( docs.size() > 0)
            return docs.get(0);

        return null;
    }

    final static String PARSEDWORKFLOW_KEY_WATERMARK = 'WATERMARK',
                        PARSEDWORKFLOW_KEY_FIND_AND_REPLACE = 'FIND_AND_REPLACE',
                        PARSEDWORKFLOW_KEY_PRINT_SCRIPT = 'PRINT_SCRIPT';

    /**
    *   This method parses the custom workflow generated when trying to generate secondary content and returns the parsed workflow
    *   @return Returns the parsed workflow for assertion purposes
    */
    static Map<String, String> parseWorkflow(){

        Map<String, String> parsedWorkflow = new Map<String, String>();

        if( !String.isBlank( workflowJson) ){

            // de-serializing the custom workflow json
            Map<String, Object> customFlow = (Map<String, Object>) JSON.deserializeUntyped( workflowJson );

            // list of tasks in the workflow
            List<Object> tasks = (List<Object>) customFlow.get(SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_TASKS);

            for ( Object task : tasks ){

                // de-serializing task object
                Map<String, Object> taskProperties = (Map<String, Object>) JSON.deserializeUntyped( JSON.serialize(task) );

                String taskType = (String) taskProperties.get(SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_TYPE);

                if ( taskType == SQX_PDFWorkflowGenerator.WORKFLOW_TASK_TYPE_ADD_WATERMARK ){

                    String wtmrkText = (String) taskProperties.get(SQX_PDFWorkflowGenerator.WATERMARK_JSON_KEY_TEXT);

                    parsedWorkflow.put(PARSEDWORKFLOW_KEY_WATERMARK , wtmrkText);

                }

                if ( taskType == SQX_PDFWorkflowGenerator.WORKFLOW_TASK_TYPE_FIND_AND_REPLACE_WORDS ){

                    List<Object> items = ( List <Object> ) taskProperties.get(SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_ITEMS);

                    String content = SQX_PDFWorkflowGenerator.MOCK_STAMP_PDF_CONTENT;

                    for ( Object item : items ){

                        Map<String,Object> dsItem = ( Map<String, Object> ) item;

                        String searchWord = (String) dsItem.get( SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_SEARCHWORD );
                        String replacementText = (String) dsItem.get( SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_REPLACEMENTTEXT );

                        System.assert( searchWord.startsWith('{!') && searchWord.endsWith('}'), 'Expected search word pattern to be : {!<searchWord>} but found ' + searchWord);

                        String target = searchWord.replace('{', '(?i)\\{')
                                                    .replace('}', '\\}');

                        content = content.replaceAll( target, replacementText );
                    }

                    parsedWorkflow.put( PARSEDWORKFLOW_KEY_FIND_AND_REPLACE, content);
                }

                if ( taskType == SQX_PDFWorkflowGenerator.WORKFLOW_TASK_TYPE_SET_PRINT_SCRIPT ){

                    String script = (String) taskProperties.get(SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_SCRIPT_CONTENT);

                    String dtFormat = script.subStringBetween('dtFormat = \"','\",');
                    String printText = script.subStringBetween('printText = \"', '\",');
                    String expirationInterval = script.subStringBetween('expirationInterval = ',';');

                    Map<String,Object> positionMap = (Map<String, Object>) taskProperties.get( SQX_PDFWorkflowGenerator.WORKFLOW_JSON_KEY_PRINT_SCRIPT_POSITION );

                    String position =   ''  + positionMap.get(SQX_PDFWorkflowGenerator.PRINT_SCRIPT_COORDINATE_KEY_LEFT) +
                                        ',' + positionMap.get(SQX_PDFWorkflowGenerator.PRINT_SCRIPT_COORDINATE_KEY_TOP) +
                                        ',' + positionMap.get(SQX_PDFWorkflowGenerator.PRINT_SCRIPT_COORDINATE_KEY_RIGHT) +
                                        ',' + positionMap.get(SQX_PDFWorkflowGenerator.PRINT_SCRIPT_COORDINATE_KEY_BOTTOM);

                    parsedWorkflow.put( PARSEDWORKFLOW_KEY_PRINT_SCRIPT, dtFormat + '::' + printText + '::' + expirationInterval + '::' + position);
                }
            }
        }
        return parsedWorkflow;
    }

    /**
    *   Given a controlled document of type 'Template Document' with secondary format setting set
    *   When user creates a new controlled document of type 'Controlled Document' referencing that template doc as parent doc
    *   Then secondary format setting field value of template doc should be transferred to the new controlled doc
    *   @story - SQX-2841
    */
    static testmethod void givenAControlledDocumentOfTypeTemplate_WhenControlledDocumentReferencingThatTemplateDocIsCreated_ThenSecondaryFormatSettingIsTransferredFromTemplate(){

        if(!runAllTests && !run_givenAControlledDocumentOfTypeTemplate_WhenControlledDocumentReferencingThatTemplateDocIsCreated_ThenSecondaryFormatSettingIsTransferredFromTemplate){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching the template doc

            SQX_Controlled_Document__c template = getDoc('TDOC_1');

            // Act : creating new controlled document with the above template document as parent type

            Test.startTest();

            // creating a new doc referencing the template doc
            SQX_Test_Controlled_Document tstObj = new SQX_Test_Controlled_Document();
            tstObj.doc.Document_Type__c = template.Id;

            tstObj.save().synchronize();
            
            Test.stopTest();

            // Assert : secondary format setting field is copied to the new controlled document from the template
            System.assert(tstObj.doc.Secondary_Format_Setting__c == template.Secondary_Format_Setting__c, 'Expected secondary format setting to be pulled from template into the new controlled document');
        }
    }

    /**
    *   Given a set of records of custom metadata type Secondary Format Setting 
    *   When user creates a new controlled document of type 'Controlled Document' not referencing any template doc
    *   Then default secondary format setting should be set to the new controlled document
    *   @story - SQX-2841
    */
    static testmethod void givenSetOfRecordOfSecondaryFormatSetting_WhenControlledDocumentNotReferencingAnyTemplateDocIsCreated_ThenDefaultSecondaryFormatSettingShouldBeAppliedToDoc(){

        if(!runAllTests && !run_givenSetOfRecordOfSecondaryFormatSetting_WhenControlledDocumentNotReferencingAnyTemplateDocIsCreated_ThenDefaultSecondaryFormatSettingShouldBeAppliedToDoc){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching default setting

            String defaultRecord = SQX_Controlled_Document.getDefaultSecondaryFormatSetting();

            // Act 

            /* Nothing to act, doc has already been created */

            // Assert : default secondary format setting has been set to the new controlled documents

            SQX_Controlled_Document__c doc = getDoc('CDOC_1');

            System.assert(doc.Secondary_Format_Setting__c == defaultRecord, 'Expected default secondary format setting to be applied to the new controlled doc');
        }
    }

    /**
    *   Given : controlled document with secondary format setting which has profile page inclusion excluded
    *   When : user tries to generate secondary content
    *   Then : primary content should be rendered into secondary content
    *   @story - SQX-2842
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToExcludeProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenOnlyPrimaryContentShouldBeRenderedIntoSecondaryContent(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToExcludeProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenOnlyPrimaryContentShouldBeRenderedIntoSecondaryContent){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching various controlled documents with secondary format settings to exclude profile page

            SQX_Controlled_Document__c doc = getDoc('CDOC_1');

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : Synchronizing secondary content for all the above docs

            Test.startTest();

            // generating secondary content for all the docs
            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : asserting if only primary content has been rendered into secondary content

            // synchronizing doc
            doc = getDoc('CDOC_1');

            // fetching secondary content data
            Blob doc_vd = [ SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :doc.Secondary_Content_Reference__c ].VersionData;

            // asserting if doc only has primary content in the secondary content
            System.assert( doc_vd.toString().startsWith('New Test Document'), 'Expected primary content to be rendered' );

            //asserting that the secondary content doesn't have profile page content
            System.assert( !doc_vd.toString().contains(SQX_PDFWorkflowGenerator.MOCK_PROFILE_PAGE_CONTENT) , 'Expected profile page content not to be added in the secondary content');
        }

    }

    /**
    *   Given : controlled document with secondary format setting which has profile page inclusion type set to 'Append'
    *   When : user tries to generate secondary content
    *   Then : primary content along with profile page should be rendered into secondary content
    *   @story - SQX-2842
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToAppendProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBeAppendedWithProfilePageIntoSecondaryContent(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToAppendProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBeAppendedWithProfilePageIntoSecondaryContent){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching various controlled documents with secondary format settings to exclude profile page

            SQX_Controlled_Document__c doc = getDoc('CDOC_2');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : Synchronizing secondary content for all the above docs

            Test.startTest();

            // generating secondary content for all the docs
            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : asserting if only primary content has been rendered into secondary content

            // synchronizing doc
            doc = getDoc('CDOC_2');

            // fetching secondary content data
            Blob doc_vd = [ SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :doc.Secondary_Content_Reference__c ].VersionData;

            // asserting if doc has primary content followed by profile page content rendered into the secondary content
            System.assertEquals( 'New Test Document' + SQX_PDFWorkflowGenerator.MOCK_PROFILE_PAGE_CONTENT, doc_vd.toString(), 'Expected profile page content to be appended with primary content in the rendered secondary content');
        }

    }

    /**
    *   Given : controlled document with secondary format setting which has profile page inclusion type set to 'Prepend'
    *   When : user tries to generate secondary content
    *   Then : primary content along with profile page prepended should be rendered into secondary content
    *   @story - SQX-2842
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToPrependProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBePrependedWithProfilePageIntoSecondaryContent(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToPrependProfilePage_WhenUserTriesToGenerateSecondaryContent_ThenPrimaryContentShouldBePrependedWithProfilePageIntoSecondaryContent){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching various controlled documents with secondary format settings to exclude profile page

            SQX_Controlled_Document__c doc = getDoc('CDOC_3');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : Synchronizing secondary content for all the above docs

            Test.startTest();

            // generating secondary content for all the docs
            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : asserting if only primary content has been rendered into secondary content

            // synchronizing doc
            doc = getDoc('CDOC_3');

            // fetching secondary content data
            Blob doc_vd = [ SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :doc.Secondary_Content_Reference__c ].VersionData;

            // asserting if doc has profile page content followed by primary content rendered in the secondary content
            System.assert( doc_vd.toString().startsWith(SQX_PDFWorkflowGenerator.MOCK_PROFILE_PAGE_CONTENT + 'New Test Document'), 'Expected profile page content to be pre-pended with primary content in the rendered secondary content');
        }

    }

    /**
    *   Given : controlled document with secondary format setting field set to a metadata record that has watermark field set
    *   When : user generates secondary content
    *   Then : secondary content should have the watermark embedded
    *   @story - SQX-2843
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToIncludeWatermark_WhenUserTriesToGenerateSecondaryContent_ThenSecondaryContentWithWatermarkShouldBeGenerated(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToIncludeWatermark_WhenUserTriesToGenerateSecondaryContent_ThenSecondaryContentWithWatermarkShouldBeGenerated){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : fetching controlled document that has secondary format setting record with watermark included

            SQX_Controlled_Document__c doc = getDoc('CDOC_2');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : synchronizing secondary content

            Test.startTest();

            // generating secondary content for all the docs
            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : asserting that watermark has been correctly included in the custom workflow

            // parsing the custom workflow and fetching the result
            String watermarkText = parseWorkflow().get(PARSEDWORKFLOW_KEY_WATERMARK);

            // asserting that watermark has been added to the workflow
            System.assert( watermarkText != null , 'Expected watermark to be added to the workflow');

            String[] txts = watermarkText.split('::');

            // asserting that first field should be document number
            System.assertEquals( doc.Document_Number__c, txts.get(0) );

            // asserting that second field should be date of origin of the doc
            System.assertEquals( doc.Effective_Date__c.format(), txts.get(1) );

            // asserting that long text field 'Description' is left as is
            System.assertEquals( '{!Description__c}', txts.get(2) );

            // asserting that names that don't match the pattern '{!<field>}' are left as is
            System.assertEquals( 'MOCK', txts.get(3) );

        }
    }

    /**
    *   Given : Controlled document with secondary format setting field set to a metadata record that has stamp pdf value set
    *   When : User tries to generate secondary content
    *   Then : Secondary content should be generated stamped with the given pdf
    *   @story : SQX-2844, SQX-2845
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToIncludeStampPDF_WhenUserGeneratesSecondaryContent_ThenSecondaryContentShouldBeGeneratedStampedWithGivenPDF(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToIncludeStampPDF_WhenUserGeneratesSecondaryContent_ThenSecondaryContentShouldBeGeneratedStampedWithGivenPDF){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : get controlled document that has stamp pdf field set

            SQX_Controlled_Document__c doc = getDoc('CDOC_3');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : generate secondary content

            Test.startTest();

            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : assert if stamp pdf content has been added to the newly generated secondary content and field replacement has been set correctly

            // synchronizing doc
            doc = getDoc('CDOC_3');

            String content = parseWorkflow().get(PARSEDWORKFLOW_KEY_FIND_AND_REPLACE);

            String expectedContent = SQX_PDFWorkflowGenerator.MOCK_STAMP_PDF_CONTENT.replace( '{!Document_Number__c}', doc.Document_Number__c )
                                                                                    .replace( '{!Effective_Date__c}', DateTime.newInstance(doc.Effective_Date__c, Time.newInstance(0,0,0,0)).format('dd/MM/yyyy') );   // TestCase2 setting has date format set as 'dd/MM/yyyy'

            // asserting that field replacement has been set
            System.assertEquals(expectedContent, content);

            // getting newly generated secondary content
            Blob doc_vd = [ SELECT VersionData FROM ContentVersion WHERE ContentDocumentId = :doc.Secondary_Content_Reference__c ].VersionData;

            System.assert( doc_vd.toString().contains(SQX_PDFWorkflowGenerator.MOCK_STAMP_PDF_CONTENT), 'Expected stamping pdf content to be added to the secondary content but received : ' + doc_vd.toString() );
        }

    }

    /**
    *   Given : Controlled document that uses secondary format setting to embed print script with other settings such as Date format, Expiration interval set to Default
    *   When : User tries to generate secondary content
    *   Then : Secondary content should be generated with print script embedded with default values set
    *   @story : SQX-2846
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithOtherSettingsSetToDefault_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithDefaultValues(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithOtherSettingsSetToDefault_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithDefaultValues){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : get controlled document that uses setting to embed print script

            SQX_Controlled_Document__c doc = getDoc('CDOC_1');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : generate secondary content

            Test.startTest();

            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : assert if print script has been embedded in the secondary content with default values

            String[] printScriptInfo = parseWorkflow().get(PARSEDWORKFLOW_KEY_PRINT_SCRIPT).split('::');

            String dtFormat = printScriptInfo.get(0);
            String printText = printScriptInfo.get(1);
            String expirationInterval = printScriptInfo.get(2);

            String expectedPrintText = 'Printed on {0}, Expires on {1}';

            /// asserting that default datetime format has been set
            System.assertEquals( SQX_PDFWorkflowGenerator.DATETIME_DEFAULT_FORMAT, dtFormat);
            // asserting that print text has been correctly added
            System.assertEquals( expectedPrintText, printText );
            // asserting that default expiration interval has been set
            System.assertEquals( String.valueOf(SQX_PDFWorkflowGenerator.DEFAULT_EXPIRATION_INTERVAL), expirationInterval);
        }
    }

    /**
    *   Given : Controlled document that uses secondary format setting to embed print script with Date format, Expiration interval set to user defined values
    *   When : User tries to generate secondary content
    *   Then : Secondary content should be generated with print script embedded based on the user defined values
    *   @story : SQX-2846
    */
    static testmethod void givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithValuesSpecifiedInSetting_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithSuppliedValues(){

        if(!runAllTests && !run_givenControlledDocumentWithSecondaryFormatSettingToStampPrintScriptWithValuesSpecifiedInSetting_WhenUserGeneratesSecondaryContent_ThenPrintScriptShouldBeSetWithSuppliedValues){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // Arrange : get controlled document that uses setting to embed print script

            SQX_Controlled_Document__c doc = getDoc('CDOC_2');

            if(doc == null){
                // production org, so skip this test
                return;
            }

            // getting extension object for the given controlled doc
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(doc));

            // setting mock http class
            Test.setMock(HttpCalloutMock.class, new SQX_Test_2677_Controlled_Document.MockHttpClass());

            // Act : generate secondary content

            Test.startTest();

            PageReference pr = ext.syncContent();

            Test.stopTest();

            // Assert : assert if print script has been embedded in the secondary content with expiration interval

            String[] printScriptInfo = parseWorkflow().get(PARSEDWORKFLOW_KEY_PRINT_SCRIPT).split('::');

            String dtFormat = printScriptInfo.get(0);
            String printText = printScriptInfo.get(1);
            String expirationInterval = printScriptInfo.get(2);
            String position = printScriptInfo.get(3);

            /// asserting that provided datetime format has been set
            System.assertEquals( 'dd-MM hh:mm', dtFormat);
            // asserting that provided expiration interval has been set
            System.assertEquals( '5', expirationInterval);
            // asserting that print text has been correctly added
            System.assertEquals( 'ABCDEF', printText);
            // asserting that coordinate have been properly set for print script
            System.assertEquals( '1,2,3,4', position);
        }
    }
}