/**
* This class ensures that 
*/
@IsTest
public class SQX_Test_1291_AuditReport{

    final static String STD_USER = 'Std user 1',
                        AUDITEE_CONTACT = 'Std user 2',
                        ADMIN_USER = 'Admin user 1';

    static Boolean  runAllTests = true,
                    run_givenAuditReportIsInApproval_AuditAndFindingIsLocked = false,
                    run_givenAuditReportIsApproved_AuditIsCompletedAndFindingInitiated = false,
                    run_givenAuditReportIsInApproval_NoNewReportCanBeAdded = false,
                    run_givenAudit_ReportCanBeAddedOnlyIfInProgress = false,
                    run_givenAuditMemberWhoDoesntOwnFinding_CanPublishReport = false,
                    run_givenAuditReportIsSubmittedFromUI_ItIsPublished = false,
                    run_givenAuditReportIsSubmittedFromUIForApproval_ItIsSentForApproval = false,
                    run_givenAuditReportIsSubmittedByNonMember_ItFails = false,
                    run_givenAuditReportIsRejected_AuditDoesntComplete = false,
                    run_givenAuditsWithAndWithoutFinding_whenAuditReportIsCompleted_thenAppropriateStageIsSet = false;

    @testsetup
    static void setupData() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        //Arrange: Create users
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STD_USER),
             auditeeContact = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, AUDITEE_CONTACT),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
    }

    /**
    * This test ensures that when an Audit Report is sent for approval. The audit that it is related to 
    * is locked and so are all the findings that it belongs too.
    */
    public static testmethod void givenAuditReportIsInApproval_AuditAndFindingIsLocked(){

        if(!runAllTests && !run_givenAuditReportIsInApproval_AuditAndFindingIsLocked){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){

            //Arrange:  1. Create an audit Audit1 with two findings Ffnding1 and Finding2.
            //          2. Generate a audit report and submit for approval

            //1.
            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
            
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);

            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            SQX_Test_Finding finding1 = audit1.addAuditFinding()
                                              .setRequired(true, false, false, false, false)
                                              .save();

            SQX_Test_Finding finding2 = audit1.addAuditFinding()
                                              .setRequired(true, false, false, false, false)
                                              .save();

            //2. Generate audit report and send for approval
            SQX_Audit_Report__c auditReport1 = audit1.generateReport(true);


            //Act: Try editing
            //      1. Audit
            //      2. Findings F1, F2
            //      3. New finding f3 can't be inserted
            SQX_Audit__c auditToUpdate = new SQX_Audit__c(Id = audit1.audit.Id, Lead_Auditor__c = 'PRB');
            SQX_Finding__c findingToUpdate = new SQX_Finding__c(Id = finding1.finding.Id, Closure_Comments__c = 'This is the issue');
            SQX_Finding__c finding3 = audit1.addAuditFinding()
                                            .setRequired(true, false, false, false, false)
                                            .finding;

            Database.SaveResult auditToUpdateResult = new SQX_DB().continueOnError()
                                                                    .op_update(new List<SQX_Audit__c> { auditToUpdate }
                                                                             , new List<Schema.SObjectField> { Schema.SQX_Audit__c.Lead_Auditor__c }
                                                                    )
                                                                    .get(0);

            Database.SaveResult findingToUpdateResult = new SQX_DB().continueOnError()
                                                                    .op_update(new List<SQX_Finding__c> { findingToUpdate }
                                                                             , new List<Schema.SObjectField> { Schema.SQX_Finding__c.Closure_Comments__c }
                                                                    )
                                                                    .get(0);

            
            Database.SaveResult findingInsertedResult = new SQX_DB().continueOnError()
                                                                    .op_insert(new List<SQX_Finding__c> { finding3 }
                                                                             , new List<Schema.SObjectField> {  }
                                                                    )
                                                                    .get(0);


            //Assert: All edits failed
            //1. Audit is not modifiable when audit report is in approval
            System.assertEquals(false, auditToUpdateResult.isSuccess(), 'Audit should not be modifiable when audit report is in approval');

            //2. Finding related to the audit isn't modifiable when audit report is in approval
            //System.assertEquals(false, findingToUpdateResult.isSuccess(), 'Finding should not be modifiable when audit report is in approval');

            
            //3. Ensure that no new finding can be inserted
            System.assertEquals(false, findingInsertedResult.isSuccess(), 'No new finding should be added to the audit.');


            //TODO: ensure the audit is indeed lock and so are related entities
            //add a method in SQX_Test_Audit to check for lock

        }

    }


    /**
    * This test ensures that when an Audit Report is approved then audit is completed and all findings are initiated.
    *
    * "Once draft report is approved, all findings are initiated and audit may be completed by giving a result along with option to schedule a followup audit. "
    */
    public static testmethod void givenAuditReportIsApproved_AuditIsCompletedAndFindingInitiated(){

        if(!runAllTests && !run_givenAuditReportIsApproved_AuditIsCompletedAndFindingInitiated){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             auditeeContact = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        SQX_Test_Audit audit1;
        SQX_Test_Finding finding1, finding2;
        SQX_Audit_Report__c auditReport1;

        System.runAs(standardUser){
            //Arrange:  1. Create an audit Audit1 with two findings Ffnding1 and Finding2.

            //1.
            audit1 = new SQX_Test_Audit(adminUser);
            
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);
            audit1.audit.SQX_Auditee_Contact__c = auditeeContact.Id;

            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            finding1 = audit1.addAuditFinding()
                              .setRequired(true, false, false, false, false)
                              .save();

            finding2 = audit1.addAuditFinding()
                              .setRequired(true, false, false, false, false)
                              .save();

            auditReport1 = audit1.generateReport(true);
        }


        System.runAs(auditeeContact){
            //Act: Approve the Audit report and publish it
            auditReport1.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            auditReport1.Approval_Status__c = 'Approved';

            new SQX_DB().withoutSharing().op_update(new List<SQX_Audit_Report__c> { auditReport1 }, new List<Schema.SObjectField> { });


            //Assert: Finding 1 and Finding 2 are open and Audit1 is completed
            //1. Finding1 and Finding 2 are Open
            finding1.synchronize();
            finding2.synchronize();

            System.assertEquals(SQX_Finding.STATUS_OPEN, finding1.finding.Status__c);
            System.assertEquals(SQX_Finding.STATUS_OPEN, finding2.finding.Status__c);

            //2. Audit 1 is completed
            System.assertEquals(SQX_Audit.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit1.audit.Id ].Status__c );
        }
    }


    /**
    * This test ensures that when an Audit Report is approved then audit is completed and appropriate Stage is set based on associated finding
    */
    public static testmethod void givenAuditsWithAndWithoutFinding_whenAuditReportIsCompleted_thenAppropriateStageIsSet(){

        if(!runAllTests && !run_givenAuditsWithAndWithoutFinding_whenAuditReportIsCompleted_thenAppropriateStageIsSet){
            return; //if both run switches are off don't execute the test.
        }

        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User standardUser = users.get(STD_USER);
        User auditeeContact = users.get(AUDITEE_CONTACT);
        User adminUser = users.get(ADMIN_USER);

        SQX_Test_Audit audit1, audit2, audit3;
        SQX_Test_Finding finding1, finding2;
        SQX_Audit_Report__c auditReport1, auditReport2, auditReport3;

        System.runAs(standardUser){
            //Arrange:  1. Create an audit Audit1 with fnding
            //          2. Create an audit Audit2 without fnding
            //          3. Create an audit Audit3 with fnding but no policy checked

            //1.
            audit1 = new SQX_Test_Audit(adminUser);
            
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);
            audit1.audit.SQX_Auditee_Contact__c = auditeeContact.Id;

            //2.
            audit2 = new SQX_Test_Audit(adminUser);

            audit2.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit2.audit.End_Date__c = Date.Today().addDays(30);
            audit2.audit.SQX_Auditee_Contact__c = auditeeContact.Id;
            
            //3.
            audit3 = new SQX_Test_Audit(adminUser);
            
            audit3.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit3.audit.End_Date__c = Date.Today().addDays(30);
            audit3.audit.SQX_Auditee_Contact__c = auditeeContact.Id;
            
            //Act: Set Stage
            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();
            audit2.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();
            audit3.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();
            
            finding1 = audit1.addAuditFinding()
                              .setRequired(true, false, false, false, false)
                              .save();
            finding2 = audit3.addAuditFinding()
                              .setRequired(false, false, false, false, false)
                              .save();

            //Act: Generate Report
            auditReport1 = audit1.generateReport(true);
            auditReport2 = audit2.generateReport(true);
            auditReport3 = audit3.generateReport(true);
        }


        System.runAs(auditeeContact){
            //Act: Approve the Audit report and publish it
            auditReport1.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            auditReport1.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_APPROVED;

            auditReport2.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            auditReport2.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_APPROVED;
            
            auditReport3.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            auditReport3.Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_APPROVED;

            new SQX_DB().withoutSharing().op_update(new List<SQX_Audit_Report__c> { auditReport1, auditReport2, auditReport3 }, new List<Schema.SObjectField> { });

            //Assert: Audit 1 is completed and Stage is Set to Respond Since finding is still open
            System.assertEquals(SQX_Audit.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit1.audit.Id ].Status__c, 'Expected Audit Status to be complete once Audit Report is Approved' );
            System.assertEquals(SQX_Audit.STAGE_RESPOND, [SELECT Stage__c FROM SQX_Audit__c WHERE Id = : audit1.audit.Id ].Stage__c, 'Expected Audit Stage to be in Respond if Findings exists for the audit');

            //Assert: Audit 2 is completed and Stage is Set to Ready For Closure Since no finding is present
            System.assertEquals(SQX_Audit.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit2.audit.Id ].Status__c, 'Expected Audit Status to be complete once Audit Report is Approved' );
            System.assertEquals(SQX_Audit.STAGE_READY_FOR_CLOSURE, [SELECT Stage__c FROM SQX_Audit__c WHERE Id = : audit2.audit.Id ].Stage__c, 'Expected Audit Stage to be in Ready For Closure if no Finding exists for Audit' );
            
            //Assert: Audit 3 is completed and Stage is Set to Ready For Closure Since finding has no policy and is complete
            System.assertEquals(SQX_Audit.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit3.audit.Id ].Status__c, 'Expected Audit Status to be complete once Audit Report is Approved' );
            System.assertEquals(SQX_Audit.STAGE_READY_FOR_CLOSURE, [SELECT Stage__c FROM SQX_Audit__c WHERE Id = : audit3.audit.Id ].Stage__c, 'Expected Audit Stage to be in Ready For Closure if no Finding exists for Audit' );
        }
    }

    /**
    * This test ensures that when an Audit Report is in approval, no new report can be added to the same audit. This helps us
    * prevent multiple audit report from being sent to approval at once.
    */
    public static testmethod void givenAuditReportIsInApproval_NoNewReportCanBeAdded(){

        if(!runAllTests && !run_givenAuditReportIsInApproval_NoNewReportCanBeAdded){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){

            //Arrange:  1. Create an audit Audit1 with a draft report in approval

            //1.
            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser);
            
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);

            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            //Generate audit report and send for approval
            SQX_Audit_Report__c auditReport1 = audit1.generateReport(true);

            //Act: Try creating another report
            Boolean reportWasntCreated = false;
            try{
                audit1.generateReport(true); //create another report and send for approval
            }
            catch(DMLException ex){
                //an exception signifies that the report wasn't created. Note we are not ensuring a validation
                //exception of a particular type occurred, which might be a better way to do this 
                reportWasntCreated = true;
            }

            //Assert: No new report was added
            System.assertEquals(true, reportWasntCreated, 'Expected the new report creation to fail. However report was created');

        }

    }


    /**
    * This test ensures that when an Audit Report can only be added when an audit is 'In Progress' state an never other wise.
    */
    public static testmethod void givenAudit_ReportCanBeAddedOnlyIfInProgress(){

        if(!runAllTests && !run_givenAudit_ReportCanBeAddedOnlyIfInProgress){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){

            //Arrange:  1. Create an audit Audit1 in draft/plan state [Audit report should not be added in draft state]
            SQX_Test_Audit audit1 = new SQX_Test_Audit(adminUser).save();

            //Act: Try creating an audit report when audit is in draft
            Boolean reportWasntCreated = false;
            try{
                audit1.generateReport(false); //attemp to create a report
            }
            catch(DMLException ex){
                //an exception signifies that the report wasn't created. Note we are not ensuring a validation
                //exception of a particular type occurred, which might be a better way to do this 
                reportWasntCreated = true;
            }

            //Assert: No audit report should be added when audit is in draft.
            System.assertEquals(true, reportWasntCreated, 'Audit Report should not be added when audit is in Draft state');


            /////////////// next test case [Audit report should not be added in scheduled state]
            //Arrange: 2. Change the status of audit to Schedule state
            audit1.setStage(SQX_Audit.STAGE_SCHEDULED).save();


            //Act: 2. Try creating an audit report for an audit in schedule state.
            reportWasntCreated = false;
            try{
                audit1.generateReport(false); //attemp to create a report
            }
            catch(DMLException ex){
                //an exception signifies that the report wasn't created. Note we are not ensuring a validation
                //exception of a particular type occurred, which might be a better way to do this 
                reportWasntCreated = true;
            }

            //Assert: No audit report should be added when audit is in draft.
            System.assertEquals(true, reportWasntCreated, 'Audit Report should not be added when audit is in Schedule state');


            /////////////// next test case [Audit report should not be added in confirmed state]
            //Arrange: 3. Change the status of audit to Confirmed state
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);

            audit1.setStage(SQX_Audit.STAGE_CONFIRMED).save();

            //Act: 3. Try creating an audit report for an audit in confirmed state.
            reportWasntCreated = false;
            try{
                audit1.generateReport(false); //attemp to create a report
            }
            catch(DMLException ex){
                //an exception signifies that the report wasn't created. Note we are not ensuring a validation
                //exception of a particular type occurred, which might be a better way to do this 
                reportWasntCreated = true;
            }

            //Assert: No audit report should be added when audit is in draft.
            System.assertEquals(true, reportWasntCreated, 'Audit Report should not be added when audit is in Confirmed state');


            /////////////// next test case [Audit report should be added in progress state]
            //Arrange: 4. Change the status of audit to In Progress state
            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            //Act: 4. Try creating an audit report for an audit in In Progress state.
            reportWasntCreated = false;
            try{
                audit1.generateReport(false); //attemp to create a report
            }
            catch(DMLException ex){
                reportWasntCreated = true;
            }

            //Assert: No audit report should be added when audit is in draft.
            System.assertEquals(false, reportWasntCreated, 'Audit Report should be added when audit is in In Progress state');

        }

    }


    /**
    * This test ensures that any audit team member can generate and add a audit report
    */
    public static testmethod void givenAuditMemberWhoDoesntOwnFinding_CanPublishReport(){
        if(!runAllTests && !run_givenAuditMemberWhoDoesntOwnFinding_CanPublishReport){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             teamMember = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        SQX_Test_Audit audit1;
        SQX_Test_Finding finding1, finding2;
        SQX_Audit_Report__c auditReport1;

        System.runAs(standardUser){
            //Arrange:  1. Create an audit Audit1 with two findings Ffnding1 and Finding2. and the team member to the audit

            //1.
            audit1 = new SQX_Test_Audit(adminUser);
            
            audit1.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit1.audit.End_Date__c = Date.Today().addDays(30);

            audit1.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

            finding1 = audit1.addAuditFinding()
                              .setRequired(true, false, false, false, false)
                              .save();

            finding2 = audit1.addAuditFinding()
                              .setRequired(true, false, false, false, false)
                              .save();

            SQX_Audit_Team__c newMember = new SQX_Audit_Team__c(SQX_User__c = teamMember.Id, SQX_Audit__c = audit1.audit.Id);

            new SQX_DB().op_insert(new List<SQX_Audit_Team__c> { newMember }, new List<Schema.SObjectField> { });

        }


        System.runAs(teamMember){

            //Act: create an  the Audit report and publish it
            try{
                auditReport1 = audit1.generateReport(false);
                auditReport1.Status__c = SQX_Audit_Report.STATUS_COMPLETE;
                auditReport1.Approval_Status__c = 'Approved';

                new SQX_DB().op_update(new List<SQX_Audit_Report__c> { auditReport1 }, new List<Schema.SObjectField> { });
            }
            catch(Exception ex){

            }
            
            //Assert: audit report was created 
            System.assertNotEquals(null, auditReport1);

            //Assert: Finding 1 and Finding 2 are open and Audit1 is completed [Extra assertions]
            //1. Finding1 and Finding 2 are Open
            finding1.synchronize();
            finding2.synchronize();

            System.assertEquals(SQX_Finding.STATUS_OPEN, finding1.finding.Status__c);
            System.assertEquals(SQX_Finding.STATUS_OPEN, finding2.finding.Status__c);

            //2. Audit 1 is completed
            System.assertEquals(SQX_Audit.STATUS_COMPLETE, [SELECT Status__c FROM SQX_Audit__c WHERE Id = : audit1.audit.Id ].Status__c );
        }
    }



    /**
    * This test ensures that if any audit report is submitted from the UI it is published if approval is not required
    */
    public static testmethod void givenAuditReportIsSubmittedFromUI_ItIsPublished(){
        if(!runAllTests && !run_givenAuditReportIsSubmittedFromUI_ItIsPublished){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){
            //Arrange: Create audit for the audit finding 
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit.audit.End_Date__c = Date.Today().addDays(30);

            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();


            //Act: submit a changeset with the required draft report and next param

            SQX_Audit_Report__c report = new SQX_Audit_Report__c(SQX_Audit__c = audit.audit.Id);
            Map<String,Object>  changeSet = new Map<String, Object>(),
                                reportJSONObject = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(report));

            reportJSONObject.put('Id', 'Draft-Report-1');

            List<Object> changes = new List<Object>();
            changes.add(reportJSONObject);

            changeSet.put('changeSet', changes);

            Map<String, String> paramMap = new Map<String, String>();
            paramMap.put('nextAction', 'SubmitAuditReport');
            paramMap.put('UseGeneratedContent', 'true');




            SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, JSON.serialize(changeSet), null, paramMap, null);

            //Assert: Draft Report was created and published
            List<SQX_Audit_Report__c> reports = [SELECT Id, Status__c, SQX_Audit__c, (SELECT Id FROM Attachments) FROM SQX_Audit_Report__c WHERE SQX_Audit__c =: audit.audit.Id];

            //ensure that report was generated
            System.assertEquals(1, reports.size());

            //ensure that report is now completed
            System.assertEquals(SQX_Audit_Report.STATUS_COMPLETE, reports.get(0).Status__c);

            //ensure that content was attached
            System.assertEquals(1, reports.get(0).Attachments.size(), 'Expected one attachment to be added to the report');

        }
    }


    /**
    * This test ensures that if any audit report is submitted and sent for approval it is sent accordingly
    */
    public static testmethod void givenAuditReportIsSubmittedFromUIForApproval_ItIsSentForApproval(){
        if(!runAllTests && !run_givenAuditReportIsSubmittedFromUIForApproval_ItIsSentForApproval){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        System.runAs(standardUser){
            //Arrange: Create audit for the audit finding 
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit.audit.End_Date__c = Date.Today().addDays(30);
            audit.audit.SQX_Auditee_Contact__c = standardUser.Id;

            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();


            //Act: submit a changeset with the required draft report and next param with send for approval = true

            SQX_Audit_Report__c report = new SQX_Audit_Report__c(SQX_Audit__c = audit.audit.Id);
            Map<String,Object>  changeSet = new Map<String, Object>(),
                                reportJSONObject = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(report));

            reportJSONObject.put('Id', 'Draft-Report-1');

            List<Object> changes = new List<Object>();
            changes.add(reportJSONObject);

            changeSet.put('changeSet', changes);

            Map<String, String> paramMap = new Map<String, String>();
            paramMap.put('nextAction', 'SubmitAuditReport');
            paramMap.put('UseGeneratedContent', 'true');
            paramMap.put('SendForApproval', 'true');


            SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, JSON.serialize(changeSet), null, paramMap, null);

            //Assert: Audit Report was created and sent for approval
            List<SQX_Audit_Report__c> reports = [SELECT Id, Status__c, SQX_Audit__c, (SELECT Id FROM Attachments) FROM SQX_Audit_Report__c WHERE SQX_Audit__c =: audit.audit.Id];

            //ensure that report was generated
            System.assertEquals(1, reports.size());

            //ensure that report is now completed
            System.assertEquals(SQX_Audit_Report.STATUS_IN_APPROVAL, reports.get(0).Status__c);

            //ensure that content was attached
            System.assertEquals(1, reports.get(0).Attachments.size(), 'Expected one attachment to be added to the report');

        }
    }


    /**
    * This test ensure that non-team member can't generate audit report
    */
    public static testmethod void givenAuditReportIsSubmittedByNonMember_ItFails(){
        if(!runAllTests && !run_givenAuditReportIsSubmittedByNonMember_ItFails){
            return; //if both run switches are off don't execute the test.
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             anotherStdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        SQX_Test_Audit audit;

        System.runAs(standardUser){
            //Arrange: Create audit for the audit finding 
            audit = new SQX_Test_Audit(adminUser);
            audit.audit.Start_Date__c = Date.Today().addDays(7); //set the start and end date before setting the status to in progress
            audit.audit.End_Date__c = Date.Today().addDays(30);

            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();

        }

        System.runAs(anotherStdUser){
            //Act: submit a changeset with the required draft report

            SQX_Audit_Report__c report = new SQX_Audit_Report__c(SQX_Audit__c = audit.audit.Id);
            Map<String,Object>  changeSet = new Map<String, Object>(),
                                reportJSONObject = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(report));

            reportJSONObject.put('Id', 'Draft-Report-1');

            List<Object> changes = new List<Object>();
            changes.add(reportJSONObject);

            changeSet.put('changeSet', changes);

            Map<String, String> paramMap = new Map<String, String>();
            paramMap.put('nextAction', 'SubmitAuditReport');

            boolean errorOccurredWhileSubmittingResponse = false;
            try{
                SQX_Extension_Audit.processChangeSetWithAction(audit.audit.Id, JSON.serialize(changeSet), null, paramMap, null);
            }
            catch(Exception ex){
                errorOccurredWhileSubmittingResponse = true;
            }

            //Assert: No report was created
            List<SQX_Audit_Report__c> reports = [SELECT Id, Status__c, SQX_Audit__c, (SELECT Id FROM Attachments) FROM SQX_Audit_Report__c WHERE SQX_Audit__c =: audit.audit.Id];

            //ensure that no report was generated
            System.assertEquals(0, reports.size());

            //ensure that error was thrown by the transaction
            System.assertEquals(true, errorOccurredWhileSubmittingResponse);    
        }
    }

    /**
    * This test ensures that if an audit report is rejected the corresponding audit isn't completed and Stage is reverted
    */
    public testmethod static void givenAuditReportIsRejected_AuditDoesntComplete(){
        if(!runAllTests && !run_givenAuditReportIsRejected_AuditDoesntComplete){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             approver = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null),
             adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null);

        SQX_Test_Audit audit;

        System.runAs(standardUser){
            //Arrange: Create audit and generate an audit report and send it for approval to auditee contact
            audit = new SQX_Test_Audit(adminUser);
            
            audit.audit.SQX_Auditee_Contact__c = approver.Id;
            audit.audit.Start_Date__c = Date.Today().addDays(7); 
            audit.audit.End_Date__c = Date.Today().addDays(30);

            audit.setStage(SQX_Audit.STAGE_IN_PROGRESS).save();
            
            SQX_Audit_Report__c report = audit.generateReport(true);

            //Assert: Audit Stage is in Finding Approval
            System.assertEquals(SQX_Audit.STAGE_FINDING_APPROVAL, [SELECT Id, Stage__c FROM SQX_Audit__c WHERE Id =: audit.audit.Id].Stage__c, 'Expected Audit to be in Finding Approval once Audit Report is sent for approval');

        }

        System.runAs(approver){
    
            List<SQX_Audit_Report__c> reports = [SELECT Id, Status__c, SQX_Audit__c, (SELECT Id FROM Attachments) FROM SQX_Audit_Report__c WHERE SQX_Audit__c =: audit.audit.Id];

            //Act: Reject the report
            reports[0].Status__c = SQX_Audit_Report.STATUS_COMPLETE;
            reports[0].Approval_Status__c = SQX_Audit_Report.APPROVAL_STATUS_REJECTED;

            new SQX_DB().withoutSharing().op_update( reports , new List<Schema.SObjectField> {
                });

            //Assert: Ensure that the audit is Reverted back to In Progress stage
            System.assertEquals(SQX_Audit.STAGE_IN_PROGRESS, [SELECT Stage__c FROM SQX_Audit__c WHERE Id = :audit.audit.Id].Stage__c);

        }
    } 

}

/*


Lock all finding
Users should be able to generate draft report and attach it to audit record 
Draft report may be shared with the Auditee and optionally approved by Auditee representative 
Once draft report is approved, all findings are initiated and audit may be completed by giving a result along with option to schedule a followup audit. 

Audit Report can be customized by the user. (The report component can be modified by the customer to meet their requirements.) 

*/