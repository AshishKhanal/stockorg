/**
 * This class contains the logic for distributing controlled document using Controlled Document Release object
 */
public with sharing class SQX_Controlled_Document_Release {

    /**
    * updates the controlled documentfield  when the previous document is obsoleted by the new rev.
    */
    public static void updateOnDocObsolete(List<SQX_Controlled_Document__c> docs){
        List<SQX_Controlled_Document_Release__c> releasesToUpdate = new List<SQX_Controlled_Document_Release__c>();

        Map<String, SQX_Controlled_Document_Release__c> existingReleases = getExistingReleaseObjectFor(docs);

        for(SQX_Controlled_Document__c doc : docs){
            SQX_Controlled_Document_Release__c release = new SQX_Controlled_Document_Release__c();
            release.SQX_Controlled_Document__c = null;

            if(existingReleases.containsKey(doc.Document_Number__c.toUpperCase())){
                release.Id = existingReleases.get(doc.Document_Number__c.toUpperCase()).Id;
                releasesToUpdate.add(release);
            }
        }

        /*
        * WITHOUT SHARING used:
        * ---------------------
        * is necessary because the controlled document release created in prior revision might have been created by someone else than the 
        * person revising the doc
        */
        new SQX_DB().withoutSharing().op_update(releasesToUpdate, new List<SObjectField> { SQX_Controlled_Document_Release__c.SQX_Controlled_Document__c });
    }

    /**
    * updates the controlled document field when the related doc is released
    */
    public static void updateOnDocRelease(List<SQX_Controlled_Document__c> docs){
        List<SQX_Controlled_Document_Release__c> releasesToUpdate = new List<SQX_Controlled_Document_Release__c>();
        List<SQX_Controlled_Document_Release__c> releasesToInsert = new List<SQX_Controlled_Document_Release__c>();

        Map<String, SQX_Controlled_Document_Release__c> existingReleases = getExistingReleaseObjectFor(docs);

        for(SQX_Controlled_Document__c doc : docs){
            SQX_Controlled_Document_Release__c release = new SQX_Controlled_Document_Release__c();
            release.SQX_Controlled_Document__c = doc.Id;

            if(existingReleases.containsKey(doc.Document_Number__c.toUpperCase())){
                release.Id = existingReleases.get(doc.Document_Number__c.toUpperCase()).Id;
                releasesToUpdate.add(release);
            }
            else{
                release.Name = doc.Document_Number__c;
                releasesToInsert.add(release);
            }
        }

        new SQX_DB().op_insert(releasesToInsert, new List<SObjectField> { 
            SQX_Controlled_Document_Release__c.SQX_Controlled_Document__c,
            SQX_Controlled_Document_Release__c.Name
        });
                
        /*
        * WITHOUT SHARING used:
        * ---------------------
        * is necessary because the controlled document release created in prior revision might have been created by someone else than the 
        * person revising the doc
        */
        new SQX_DB().withoutSharing().op_update(releasesToUpdate, new List<SObjectField> { SQX_Controlled_Document_Release__c.SQX_Controlled_Document__c });
    }


    /**
    * returns the map of the controlled document release of the related controlled doc
    */
    private static Map<String, SQX_Controlled_Document_Release__c> getExistingReleaseObjectFor(List<SQX_Controlled_Document__c> docs){

        Map<String, SQX_Controlled_Document_Release__c> releaseByDocNumber = new Map<String, SQX_Controlled_Document_Release__c>();

        Map<String, SQX_Controlled_Document__c> docNumber2Doc = new Map<String, SQX_Controlled_Document__c>();
        for(SQX_Controlled_Document__c doc : docs){
            System.assert(!docNumber2Doc.containsKey(doc.Document_Number__c.toUpperCase()), 'Two document with same number are being activated');
            docNumber2Doc.put(doc.Document_Number__c.toUpperCase(), doc);
        }
        
        for(SQX_Controlled_Document_Release__c release : [SELECT Id, SQX_Controlled_Document__c , Name, Distribution_Content__c, Distribution_Vault__c,
                                                          Content_Document_Id__c, Last_Published_VersionId__c
                                                          FROM SQX_Controlled_Document_Release__c
                                                          WHERE Name IN : docNumber2Doc.keySet()]){

            releaseByDocNumber.put(release.Name.toUpperCase(), release);
        }

        return releaseByDocNumber;
                
    }


    /**
    * updates the distribution library of the controlled document release when the distribution library is changed in the controlled doc
    */
    public static void updateOnDistributionChanged(List<SQX_Controlled_Document__c> docs){
        List<SQX_Controlled_Document_Release__c> releasesToUpdate = new List<SQX_Controlled_Document_Release__c>(getExistingReleaseObjectFor(docs).values());

        manageContent(releasesToUpdate);

        /**
        * WITHOUT Sharing used
        * ---------------------
        * Distribution release object might be owned by previous document owner who may not be
        * the same as user releasing new version.
        *
        * TODO: Change the ownership of the object to the new document owner in every release.
        */
        new SQX_DB().withoutSharing().op_update(releasesToUpdate, new List<SObjectField> {
            SQX_Controlled_Document_Release__c.Content_Document_Id__c
        });
    }

    /**
    * manages the content of the controlled doc when the doc library is updated or doc status is changed
    */
    private static void manageContent(List<SQX_Controlled_Document_Release__c> releases){

        Map<Id, SQX_Controlled_Document_Release__c> docsToDistribute = new Map<Id, SQX_Controlled_Document_Release__c>();
        Map<Id, SQX_Controlled_Document_Release__c> contentToRelease = new Map<Id, SQX_Controlled_Document_Release__c>();
        Set<Id> contentToArchive = new Set<Id>();

        for(SQX_Controlled_Document_Release__c rel : releases){

            if(rel.SQX_Controlled_Document__c == null){
                
                if(!String.IsBlank(rel.Content_Document_Id__c)){
                    contentToArchive.add(rel.Content_Document_Id__c);
                }

            }
            else{
                if(!String.isBlank(rel.Distribution_Vault__c)){
                    docsToDistribute.put(rel.Distribution_Content__c, rel);
                    if(rel.Content_Document_Id__c != null){
                        contentToRelease.put(rel.Content_Document_Id__c, rel);
                    }
                }
                else{
                    if(!String.IsBlank(rel.Content_Document_Id__c)){
                        contentToArchive.add(rel.Content_Document_Id__c);
                    }
                }
            }

        }

        Map<Id, ContentDocument> docs = new Map<Id, ContentDocument>();
        if(contentToArchive.size() > 0 || contentToRelease.keySet().size() > 0){
            
            List<ContentDocument> docsToArchive = new List<ContentDocument>();

            docs = new Map<Id, ContentDocument>([SELECT Id, ParentId
                    FROM ContentDocument 
                    WHERE Id IN : contentToRelease.keySet() OR Id IN : contentToArchive]);

            for(Id docId : contentToArchive){
                if(docs.containsKey(docId)){
                    ContentDocument doc = docs.get(docId);
                    doc.IsArchived = true;
                    docsToArchive.add(doc);
                }
            }

            for(SQX_Controlled_Document_Release__c rel : contentToRelease.values()){
                if(!docs.containsKey(rel.Content_Document_Id__c)){
                    rel.Content_Document_Id__c = null;
                }
            }

            if(docsToArchive.size() > 0){
                new SQX_DB().op_update(docsToArchive, new List<SObjectField> { ContentDocument.IsArchived });
            }
        }

        if(docsToDistribute.keySet().size() > 0){
                        
            Map<SQX_Controlled_Document_Release__c, ContentVersion> releaseToVersion = new Map<SQX_Controlled_Document_Release__c, ContentVersion>();
            List<ContentVersion> noPriorContents = new List<ContentVersion>();
            List<ContentVersion> versions = [SELECT Id,
                                                VersionData,
                                                ContentDocumentId,
                                                Title,
                                                PathOnClient
                                             FROM ContentVersion
                                             WHERE ContentDocumentId IN :docsToDistribute.keySet()
                                                AND ContentSize <= :SQX_Controlled_Document.LIMIT_QUERY_CONTENT_VERSION_DATA_SIZE
                                                AND IsLatest = true ];
            
            // list contents with large content data
            Set<Id> contentDocIdsWithLargeData = new Set<Id>();
            if (versions.size() != docsToDistribute.size()) {
                for (ContentVersion cv : [  SELECT Id,
                                                VersionData,
                                                ContentDocumentId,
                                                Title
                                            FROM ContentVersion
                                            WHERE ContentDocumentId IN :docsToDistribute.keySet()
                                                AND ContentSize > :SQX_Controlled_Document.LIMIT_QUERY_CONTENT_VERSION_DATA_SIZE
                                                AND IsLatest = true ]) {
                    contentDocIdsWithLargeData.add(cv.ContentDocumentId);
                    versions.add(cv);
                }
            }

            List<ContentDocument> documentsToUpdate = new List<ContentDocument>();
            List<ContentVersion> versionsToInsert = new List<ContentVersion>();


            for(ContentVersion version : versions){
                     
                SQX_Controlled_Document_Release__c rel = docsToDistribute.get(version.ContentDocumentId);

                
                if(rel.Content_Document_Id__c != null){
                    ContentDocument doc = new ContentDocument(Id = rel.Content_Document_Id__c, ParentId = rel.Distribution_Vault__c);
                    documentsToUpdate.add(doc);
                }
                
                if(rel.Last_Published_VersionId__c != version.Id){

                    ContentVersion cvTemp = version;
                    rel.Last_Published_VersionId__c = version.Id;   
                    if (contentDocIdsWithLargeData.contains(version.ContentDocumentId)) {
                        // create substitute content when existing content to clone is large
                        version = SQX_Controlled_Document.createSubstituteContentForLargeFile();
                        version.Title = cvTemp.Title;
                    } else {
                        version.Id = null;
                        // From api version 40.0, ContentBodyId is being queried by default when querying VersionData from ContentVersion and sf throws exception on inserting
                        // ContentVersion for api version higher than 39.0 with both fields VersionData and ContentBodyId. Thus making ContentBodyId null if it is not null.
                        version.ContentBodyId = null;
                    }
                    version.ContentDocumentId = rel.Content_Document_Id__c;
                    version.Controlled_Document__c = rel.SQX_Controlled_Document__c;

                    releaseToVersion.put(rel, version); 
                    if(rel.Content_Document_Id__c == null){
                        version.FirstPublishLocationId = rel.Distribution_Vault__c;
                        noPriorContents.add(version);
                    }

                    versionsToInsert.add(version);                  
                }     
            }
            
            if(versionsToInsert.size() > 0){
                new SQX_DB().op_insert(versionsToInsert, new List<SObjectField> {
                    ContentVersion.fields.FirstPublishLocationId,
                    ContentVersion.fields.Controlled_Document__c,
                    ContentVersion.fields.ContentDocumentId
                });
            }

            if(documentsToUpdate.size() > 0)
                new SQX_DB().op_update(documentsToUpdate, new List<SObjectField> { });
            if(noPriorContents.size()  > 0){
                // if new content was created update the link in the content document id of release object.
                Map<Id, SQX_Controlled_Document_Release__c> versionIdToRelease = new Map<Id, SQX_Controlled_Document_Release__c>();
                for(SQX_Controlled_Document_Release__c rel : docsToDistribute.values()){
                    versionIdToRelease.put(releaseToVersion.get(rel).Id, rel);
                }
                for(ContentVersion version : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE IsLatest = true AND Id IN : noPriorContents]){
                    versionIdToRelease.get(version.Id).Content_Document_Id__c = version.ContentDocumentId;
                }
            }
            
        }
    }

    public with sharing class Bulkified extends SQX_BulkifiedBase{
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newAuditCriterias equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Controlled_Document_Release__c> newReleases, Map<Id,SQX_Controlled_Document_Release__c> oldMap){
        
            super(newReleases, oldMap);
        }
        
        
        /**
        * this method checks whether or not content has to be updated and updates it as required.
        * This method processes all the records and filters based on the content version id that was previously
        * published
        */
        public Bulkified distributeContentOnRelease(){

            manageContent(this.resetView().view);

            return this;
        }
    }
}