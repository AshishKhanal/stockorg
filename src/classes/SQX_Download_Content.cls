/**
    Controller class for SQX_Download_Content page to download file from Chatter
*/
public with sharing class SQX_Download_Content{
    public String id = null;    
        
    public SQX_Download_Content(){
        id = ApexPages.currentPage().getParameters().get('id');        
    }   
    
    /**
     * Returns the download url of the file specified by the id already set
    */
    public String getDownloadURL(){
        ContentDocument ctd = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE ID =:id];
        String url = SQX_Utilities.getContentServerNativeLinkFor(ctd); 
        return url;
    }
}