/**
* manages the sobject SQX_Part related stuffs
* @author Pradhanta Bhandari
*/
public with sharing class SQX_Part{

    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(List<SQX_Part__c> newParts, Map<Id, SQX_Part__c> oldParts){
            super(newParts, oldParts);
        }
    
        /**
        * This method changes the supplier parts deactivates a supplier part if part is deactivated  or
        * risk level has changed
        * @author Pradhanta
        */
        public Bulkified percolateRiskAndActiveToSupplierPart(){
            //1. find all parts that have risk level changed
            Rule riskLevelChanged = new Rule();
            riskLevelChanged.addRule(Schema.SQX_Part__c.Part_Risk_Level__c, RuleOperator.HasChanged);

            //or those that have been deactivated
            Rule partIsInactive = new Rule();
            partIsInactive.addRule(Schema.SQX_Part__c.Active__c, RuleOperator.Equals, false);

            this.groupByRules(new Rule[]{riskLevelChanged, partIsInactive}, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            Set<SQX_Part__c> allChangedParts = new Set<SQX_Part__c>();
            allChangedParts.addAll((List<SQX_Part__c>)riskLevelChanged.evaluationResult);
            allChangedParts.addAll((List<SQX_Part__c>)partIsInactive.evaluationResult);


            if(allChangedParts.size() > 0){
                Map<Id,SQX_Part__c> partIsInactiveMap = new Map<Id,SQX_Part__c>((List<SQX_Part__c>) partIsInactive.evaluationResult);
                Map<Id,SQX_Part__c> riskLevelChangedMap = new Map<Id,SQX_Part__c>((List<SQX_Part__c>) riskLevelChanged.evaluationResult);

                Map<Id,SQX_Supplier_Part__c> supplierPartsToUpdate = new Map<Id,SQX_Supplier_Part__c>();
                Map<Id,Account> accountsToUpdate = new Map<Id,Account>();

                //for each supplier part
                for(SQX_Supplier_Part__c supplierPart: [SELECT Id, Active__c, Supplier_Part_Risk_Level__c, Account__c , Part__c, Account__r.Supplier_Risk_Level__c
                                                        FROM SQX_Supplier_Part__c
                                                        WHERE Part__c IN: allChangedParts]){

                    //deactivate a part if it has been deactivated
                    if(partIsInactiveMap.containsKey(supplierPart.Part__c)){
                        supplierPart.Active__c = false;
                    }

                    //update the risk level for all supplier part
                    if(riskLevelChangedMap.containsKey(supplierPart.Part__c)){
                        SQX_Part__c updatedPart = riskLevelChangedMap.get(supplierPart.Part__c);
                        SQX_Part__c oldPart = (SQX_Part__c) this.oldValues.get(supplierPart.Part__c);
                        
                        supplierPart.Supplier_Part_Risk_Level__c = updatedPart.Part_Risk_Level__c;                
                        supplierPart.Reason_For_Change__c = 'Part risk level updated on ' + DateTime.now();
               
                        //update the supplier's record with the correct reason for change.
                        //since the supplier part risk level is rolled up from supplier part in Account any changes happen instantenously without any history tracking
                        //this allows addition of a message to the respective account
                        if(supplierPart.Active__c && supplierPart.Account__r.Supplier_Risk_Level__c > supplierPart.Supplier_Part_Risk_Level__c){
                            Account selectedAccount = accountsToUpdate.get(supplierPart.Account__c);
                            

                            if(selectedAccount == null || selectedAccount.Supplier_Risk_Level__c > supplierPart.Supplier_Part_Risk_Level__c ){
                                //this second part ensures that if risk level is updated by some record in same transaction updates, it is honored
                                selectedAccount = selectedAccount != null ? selectedAccount : new Account(Id = supplierPart.Account__c);

                                selectedAccount.Reason_For_Change__c = updatedPart.Name + ' Part Risk Level Updated from ' 
                                                                       + oldPart.Part_Risk_Level__c + ' To ' 
                                                                       + updatedPart.Part_Risk_Level__c;

                               accountsToUpdate.put(selectedAccount.Id, selectedAccount);
                            }
                            
                        }
                        
                    }
                    
                    supplierPartsToUpdate.put(supplierPart.Id, supplierPart);
                }

                //update all records as necessary
                Map<SObjectType, List<SObject>> objectsToUpdate = new Map<SObjectType, List<SObject>>();
                Map<SObjectType, List<SObjectField>> fieldsToSet = new Map<SObjectType, List<SObjectField>>();

                if(supplierPartsToUpdate.values().size() > 0){
                    SObjectType objectType = supplierPartsToUpdate.values().get(0).getSObjectType();
                    objectsToUpdate.put(objectType, supplierPartsToUpdate.values());
                    fieldsToSet.put(objectType, new List<SObjectField>{
                        SQX_Supplier_Part__c.fields.Supplier_Part_Risk_Level__c,
                        SQX_Supplier_Part__c.fields.Reason_For_Change__c,
                        SQX_Supplier_Part__c.fields.Active__c
                    });
                }

                if(accountsToUpdate.values().size() > 0){
                    SObjectType objectType = accountsToUpdate.values().get(0).getSObjectType();
                    objectsToUpdate.put(objectType, accountsToUpdate.values());
                    fieldsToSet.put(objectType, new List<SObjectField>{
                        Account.fields.Reason_For_Change__c
                    });
                }

                if(objectsToUpdate.keySet().size() > 0){
                    new SQX_DB().op_update(objectsToUpdate, fieldsToSet);
                }
            }
            
            return this;
        }
    }     

}