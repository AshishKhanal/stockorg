/**
 * This class contains factory methods for constructing change order.
 */
@isTest
public class SQX_Test_Change_Order {

    public SQX_Change_Order__c changeOrder;
    public Database.SaveResult lastSaveResult;
    
    public Id Id {
    	get {
    		return this.changeOrder.Id;
    	}
    }
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
    * This is a constructor for the change order class
    * @param changeOrderToClone the change order that is to be cloned and wrapped by this test change order
    */
    public SQX_Test_Change_Order(SQX_Change_Order__c changeOrderToClone){
    	changeOrder = changeOrderToClone.clone();
    }
    
    private static Integer changeOrderCount = 1;
    
    /**
    * Create a new change order with the minimal data required to create and 
    */
    public SQX_Test_Change_Order(){
    	changeOrder = new SQX_Change_Order__c(
    		Title__c = 'Change Order ' + changeOrderCount,
    		Description__c = 'Description of change order',
    		Justification__c  = 'Rationale for the change order',
    		Change_Category__c = 'Administrative',
    		Submitted_Date__c = Date.today(),
    		Target_Completion_Date__c = Date.today().addDays(20),
    		Status__c = SQX_Change_Order.STATUS_DRAFT
    	);
    }
    
    /**
    * Shorthand for setting the status of change order in a fluent way :)
    */
    public SQX_Test_Change_Order setStatus(String status){
    	changeOrder.Status__c = status;
    	
    	return this;
    }
    
    /**
    * Saves/Updates the change order and returns the save result
    */
    public SQX_Test_Change_Order save(){
    	SQX_DB db = new SQX_DB().continueOnError();
    		
    	if(this.changeOrder.Id == null){
    		this.lastSaveResult = db.op_insert(new List<SObject> { this.changeOrder }, new List<SObjectField> {
															    				SQX_Change_Order__c.Title__c,
															    				SQX_Change_Order__c.Status__c,
															    				SQX_Change_Order__c.Description__c,
															    				SQX_Change_Order__c.Justification__c,
															    				SQX_Change_Order__c.Change_Category__c,
															    				SQX_Change_Order__c.Target_Completion_Date__c
															    			 }).get(0);
    	}
    	else {
    		this.lastSaveResult = db.op_update(new List<SObject> { this.changeOrder }, new List<SObjectField> {
															    				SQX_Change_Order__c.Title__c,
															    				SQX_Change_Order__c.Status__c,
															    				SQX_Change_Order__c.Description__c,
															    				SQX_Change_Order__c.Justification__c,
															    				SQX_Change_Order__c.Change_Category__c,
															    				SQX_Change_Order__c.Target_Completion_Date__c
															    			 }).get(0);
    	}
    	
    	return this;
    }
    
    
    private static Integer actionNumber = 1;
    /**
    * Returns an action object that is linked with the wrapped changed order
    * the action must be inserted before any operation is to be done.
    */
    public SQX_Implementation__c addAction(String recordType, User assignee, String description, Date dueDate){
        Id recordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Implementation, recordType);
        Id assigneeId = null;
        
        dueDate = dueDate == null ? Date.today().addDays(15) : dueDate;
        description = description == null ? 'This is an action' : description;
        
        if(assignee != null)
        	assigneeId = assignee.Id;
        
        SQX_Implementation__c action = new SQX_Implementation__c(
        								Title__c = 'Implementation - ' + actionNumber,
                                        SQX_User__c = assigneeId,
                                        Description__c = description,
                                        Status__c = SQX_Implementation_Action.STATUS_OPEN,
                                        Due_Date__c = dueDate,
                                        RecordTypeId = recordTypeId,
                                        SQX_Change_Order__c = changeOrder.Id);
                 
		actionNumber++;                   
        return action;
    }
    
    
    ///////// Basic test cases
    
    /**
    * @Story SQX-1932
    * This is a basic test case which is used to ensure that a user can add action to change order as mentioned in SQX-1932
    * Internally, it is a simple test of permissions
    */
    static testmethod void givenChangeOrder_ActionCanBeAddedUsers(){
    	UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User stdUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole),
        	 adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
       	User [] users = new List<User> { stdUser, adminUser };
            
        // Arrange: Create a change order on which we will add actions
        SQX_Test_Change_Order changeOrder;
        System.runAs(stdUser){
            // Arrange: Create a change order on which we will add actions
            changeOrder = new SQX_Test_Change_Order();
            changeOrder.save();
        }
        
        for(User usr : users) {
            System.runAs(usr) {
                // Act: Create and save an action as standard user
                SQX_Implementation__c act = changeOrder.addAction(SQX_Implementation.RECORD_TYPE_PLAN, adminUser, null, null);
                Database.SaveResult result = new SQX_DB().continueOnError()
                    .op_insert(new List<SObject> { act },
                            new List<SObjectField> { SQX_Implementation__c.SQX_User__c,
                                SQX_Implementation__c.Description__c,
                                SQX_Implementation__c.Status__c,
                                SQX_Implementation__c.Due_Date__c,
                                SQX_Implementation__c.RecordTypeId,
                                SQX_Implementation__c.SQX_Change_Order__c 
                                }
                            )
                    .get(0);
                
                // Assert: Ensure that all the action where saved properly
                System.assertEquals(true, result.isSuccess(), 'Expected the implementation to be saved but failed with ' + result);
                 //ensure is lock false
                System.assertEquals(false,[select id,Is_Locked__c from SQX_Implementation__c where SQX_Change_Order__c=:changeOrder.changeOrder.id].Is_Locked__c,'implementation is not locked');
                //Assert:deletion can be done
                List<Database.DeleteResult> deleteimplResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Implementation__c> { act });
                System.assertEquals(true, deleteimplResult[0].isSuccess(), 'impl can be deleted ');
            }
        }
    }
    
    /**
    * Given: change order with future submitted date,
    * When:  try to save the record,
    * Then:  error message is thrown and save is unsucessful
    * 
    * @author: Sajal Joshi
    * @date: 23/11/2016
    * @story: [SQX-2786]
    */    
    public static testmethod void whenFutureSubmittedDateIsUsed_ErrorIsThrown(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runas(standardUser) {
            //Act: Create Change Order (change) with future submitted date and save it
            SQX_Test_Change_Order change = new SQX_Test_Change_Order();
            change.changeOrder.Submitted_Date__c = System.today()+2;
            List<Database.SaveResult> result = new SQX_DB().continueOnError().op_insert(new List<SQX_Change_Order__c>{change.changeOrder}, new List<Schema.SObjectField>{});
            
            //Assert: Save is UnSuccessful
            System.assertEquals(false, result[0].isSuccess(), 'Expected Insert failure due to validation rule');
            
            //Act: create another change order (change1) and update it with future submitted date
            SQX_Test_Change_Order change1 = new SQX_Test_Change_Order().save();
            change1.changeOrder.Submitted_Date__c = System.today() + 5;
             
            //Assert: Update is Unsuccessful
            result = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{change1.changeOrder}, new List<Schema.SObjectField>{});
            System.assertEquals(false, result[0].isSuccess(), 'Expected Update failure due to validation rule');
        }
    }

    /**
    * Given: Change Order with Actions and tasks in Ready state,
    * When:  When Change is Completed,
    * Then:  All Related tasks are Completed
    * 
    * @author: Sudeep Maharjan
    * @date: 03/16/2017
    * @story: [SQX-3123]
    */
    public static testmethod void givenChangeOrderWithTaskInReadyState_whenChangeIsCompleted_thenAllRelatedTaskIsCompleted() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');

        System.runAs(standardUser) {

            SQX_Test_Change_Order co = new SQX_Test_Change_Order().save();
            
            // add required controlled documents
            List<SQX_Controlled_Document__c> docs = new List<SQX_Controlled_Document__c>();
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs.add(docs[0].clone()); // cloned to revise
            docs[0].SQX_Change_Order__c = co.changeOrder.Id;
            docs[1].Auto_Release__c = false;
            docs[1].Revision__c = SQX_Utilities.getNextRev(docs[0].Revision__c);
            docs[1].SQX_Change_Order__c = co.changeOrder.Id;
            new SQX_DB().op_insert(docs, new List<Schema.SObjectField>{});

            // add required new and revise document CO implementations
            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(co.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'New Doc', Date.Today().addDays(1)));
            actions.add(co.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Revise Doc', Date.Today().addDays(1)));
            actions[0].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[0].SQX_Controlled_Document_New__c = docs[0].Id;
            actions[1].Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            actions[1].SQX_Controlled_Document__c = docs[0].Id;
            actions[1].SQX_Controlled_Document_New__c = docs[1].Id;
            new SQX_DB().op_insert(actions, new List<Schema.SObjectField>{});

            // submit controlled documents for change approval
            docs[0].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            docs[1].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            new SQX_DB().op_update(docs, new List<Schema.SObjectField>{});
            
            List<SQX_Implementation__c> coActions = [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id IN :actions];
            
            System.assertEquals(SQX_Implementation.STATUS_READY, coActions[0].Status__c,
                '"Open" Implementation status is expected to be "Ready" when document is submitted for change approval.');
            System.assertEquals(SQX_Implementation.STATUS_READY, coActions[1].Status__c,
                '"Open" Implementation status is expected to be "Ready" when document is submitted for change approval.');

            //Assert : Task is NOT STARTED when Implementation is in NOT_STARTED status 
            List<Task> task = [SELECT Id, Status FROM Task WHERE WhatId =: co.changeOrder.Id];
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, task[0].Status);     
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, task[1].Status);   
             
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: approve CO implementation approval
            co.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IMPLEMENTATION_APPROVED;
            co.save();

            Map<Id, SQX_Controlled_Document__c> coDocs = new Map<Id, SQX_Controlled_Document__c>(
                [SELECT Id, Document_Status__c, Approval_Status__c FROM SQX_Controlled_Document__c WHERE Id IN :docs]);
            
            System.assertEquals(SQX_Controlled_Document.APPROVAL_APPROVED, coDocs.get(docs[0].Id).Approval_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.APPROVAL_APPROVED, coDocs.get(docs[1].Id).Approval_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, coDocs.get(docs[0].Id).Document_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.STATUS_APPROVED, coDocs.get(docs[1].Id).Document_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            
            // ACT: release controlled document approved for change approval
            update new SQX_Controlled_Document__c(
                Id = docs[1].Id,
                Auto_Release__c = true,
                Effective_Date__c = System.today() + 20
            );

            coActions = [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id IN :actions];
            
            System.assertEquals(SQX_Implementation.STATUS_COMPLETE, coActions[0].Status__c,
                '"Ready" Implementation status is expected to be "Complete" when document is released.');
            System.assertEquals(SQX_Implementation.STATUS_COMPLETE, coActions[1].Status__c,
                '"Ready" Implementation status is expected to be "Complete" when document is released.');

            //Assert : Task is completed once Implementation is in Ready Status
            task = [SELECT Id, Status FROM Task WHERE WhatId =: co.changeOrder.Id];
            System.assertEquals(SQX_Task.STATUS_COMPLETED, task[0].Status);     
            System.assertEquals(SQX_Task.STATUS_COMPLETED, task[1].Status);   
        }
    }
    
    /**
    * method to add user to queue
    */
    public static void addUserToQueue(List<User> usersToAssign){
        SQX_Test_Utilities.addUserToQueue(usersToAssign, 'SQX_Change_Queue', SQX.ChangeOrder);
    }

    /**
    * Given: Change Order with Actions and impact ,
    * When:  When Change is void or closed,
    * Then:  delete the change order ,impact and impl than give error message.
    */
    @isTest
    public static void givenChangeOrderOrImplOrImpact_WhenUserDeleteVoidCORecord_ThengiveErrorMessage(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        SQX_Test_Change_Order.addUserToQueue(new List<User> {standardUser, adminUser});
        List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
        System.runAs(standardUser) {
            //Arrange:create change order 
            SQX_Test_Change_Order co =new SQX_Test_Change_Order().save();
            //impact
            SQX_Impact__c impact = new SQX_Impact__c(
                SQX_Change_Order__c = co.changeOrder.Id,
                RecordTypeId =  SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Impact, SQX_Impact.RECORD_TYPE_EXTERNAL),
                Description_of_Impact__c = 'Impact External Test',
                External_Identifier__c = 'External Idenfier Test'
            );
            List<SQX_Impact__c> impactList = new List<SQX_Impact__c>{impact};
            List<DataBase.SaveResult> result = new SQX_DB().continueOnError().op_insert(impactList, new List<Schema.SObjectField>{});
            System.assertEquals(true, result[0].isSuccess(),'Record save Successfully>>' + result[0].getErrors());
            //Arrange : add plan 
            actions.add(co.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser, 'Action Description',  Date.Today().addDays(15)));
            new SQX_DB().op_insert(actions, new List<Schema.SObjectField> { });
            actions = [SELECT Id, Status__c, TaskID__c FROM SQX_Implementation__c];
            //Act:Void record 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            co.changeOrder.Resolution_Code__c = 'Invalid';
            co.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            List<Database.SaveResult> voidResults = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{co.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            
            //Assert:void record successfully
            System.assertEquals(true, voidResults[0].isSuccess(),'Record void Successfully>>' + voidResults[0].getErrors());
            System.assertEquals(SQX_Change_Order.STATUS_VOID,[select id,Status__c,Record_Stage__c from SQX_Change_Order__c where id=:co.changeOrder.id].Status__c);
            System.assertEquals(true,[select id,Is_Locked__c from SQX_Change_Order__c where id=:co.changeOrder.id].Is_Locked__c);
            
            List<Database.DeleteResult> deleteCOResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Change_Order__c>{co.changeOrder});

            // Assert: Deletion error is thrown and change order is not deleted
            System.assertEquals(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE, deleteCOResult[0].getErrors()[0].getMessage()); 
            
            List<Database.DeleteResult> deleteImplResult = new SQX_DB().continueOnError().op_delete(actions);
            
            // Assert: Deletion error is thrown and implementation is not deleted
            System.assertEquals(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE, deleteImplResult[0].getErrors()[0].getMessage()); 
            
            SQX_Impact__c imp=[select id,Description_of_Impact__c from SQX_Impact__c where SQX_Change_Order__c=:co.changeOrder.id];
          
            List<Database.DeleteResult> deleteImpactResult = new SQX_DB().continueOnError().op_delete(new List<SQX_Impact__c>{imp});
            
            // Assert: Deletion error is thrown and impact is not deleted
            System.assertEquals(Label.SQX_ERR_MSG_ONCE_LOCKED_NO_DELETE, deleteImpactResult[0].getErrors()[0].getMessage()); 

        }
    }

    /**
    * Given: create a Change order record with different status
    *       i)draft record 
    *       ii)open record 
    * When:  When Change record  is void
    *       i)Without Supervisor Permission
    *       ii)With  Supervisor Permission
    * Then: user try to Void the record 
    *       i)Draft status record can void successfully
    *       ii)Open status record without Supervisor Permission get error message "You do not have enough permission to void the record. Please ask your administrator to grant you CQ Change Order Supervisor permission."
    *       iii)After adding Supervisor Permission user can void the record successfully
    */
    @isTest
    public static void givenChangeOrWithOrWithoutSupervisorPermission_WhenUserVoidCORecord_ThenGiveValidationErrorMessage(){
        
        // Arrange: Create a change order in draft state as standard user which will be used to test the next actions
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
        SQX_Test_Change_Order.addUserToQueue(new List<User> { standardUser1,adminUser });
        System.runAs(standardUser1){
            //Arrange: change order record
            SQX_Test_Change_Order changeOrder =  new SQX_Test_Change_Order();
            changeOrder.changeOrder.Queue_Name__c = 'SQX_Change_Queue';
            changeOrder.save();

            //Submit change order record 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_SUBMIT;
            changeOrder.save();

            // Assert: Ensure that the change order is now in triage
            System.assertEquals(SQX_Change_Order.STATUS_DRAFT, [SELECT Status__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Status__c);
            System.assertEquals(SQX_Change_Order.STAGE_TRIAGE, [SELECT Record_Stage__c FROM SQX_Change_Order__c WHERE Id = : changeOrder.Id].Record_Stage__c	);

            //Void the record in status draft
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrder.changeOrder.Resolution_Code__c = 'Invalid';

            //Act: user try to void the record 
            List<Database.SaveResult> voidResults = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{changeOrder.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            
            //Assert: Void can be done successfully
            System.assertEquals(true, voidResults[0].isSuccess(), 'Co record can be void successfully when record status is in draft');

            //Arrange:create new change order record 
            SQX_Test_Change_Order changeOrder1 =  new SQX_Test_Change_Order().save();
            
            //initiate change order record 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder1.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_INITIATE;
            changeOrder1.save();

            // Assert: Ensure that the change order is now in open
            System.assertEquals(SQX_Change_Order.STATUS_OPEN, [SELECT Status__c FROM SQX_Change_Order__c WHERE Id = : changeOrder1.Id].Status__c);
            System.assertEquals(SQX_Change_Order.STAGE_PLAN, [SELECT Record_Stage__c FROM SQX_Change_Order__c WHERE Id = : changeOrder1.Id].Record_Stage__c	);

            //Void the record when status open
            SQX_BulkifiedBase.clearAllProcessedEntities();
            changeOrder1.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            changeOrder1.changeOrder.Resolution_Code__c = 'Invalid';
            //Act: user try to void the record 
            List<Database.SaveResult> voidResults1 = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{changeOrder1.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c});
            //Assert: Void can be done successfully
            System.assertEquals(true, voidResults1[0].isSuccess(), 'Co record can be void successfully when record status is in open');

            // Add change order  supervisor custom permission to std user
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, SQX_Change_Order.CUSTOM_PERMISSION_CHANGEORDER_SUPERVISOR);

            //Arrange : create a change order 
            SQX_Test_Change_Order voidCoWithPerme =  new SQX_Test_Change_Order().setStatus(SQX_Change_Order.STATUS_OPEN).save();
            //add implementation
            SQX_Implementation__c [] implementations = new SQX_Implementation__c[]{
                    voidCoWithPerme.addAction(SQX_Implementation.RECORD_TYPE_PLAN, standardUser1, 'We have got to do something', null) 
            };
            insert implementations;
            //sumit for approval change order
            voidCoWithPerme.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            voidCoWithPerme.save();
            System.assertEquals('Plan Approval',[SELECT Id, Status__c,Record_Stage__c,Approval_Status__c  FROM SQX_Change_Order__c
                                                    WHERE Id = : voidCoWithPerme.Id].Approval_Status__c );
            
            //Act:void the record which is in plan for approval
            SQX_BulkifiedBase.clearAllProcessedEntities();
            voidCoWithPerme.changeOrder.Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_VOID;
            voidCoWithPerme.changeOrder.Resolution_Code__c = 'Invalid';
            //Act: void the recor
            List<Database.SaveResult> voidRecord = new SQX_DB().continueOnError().op_update(new List<SQX_Change_Order__c>{voidCoWithPerme.changeOrder}, new List<Schema.SObjectField>{SQX_Change_Order__c.Activity_Code__c,SQX_Change_Order__c.Resolution_Code__c});
            //Assert:Give validation error message when user with supervisor permission try to void the record plan approval or implementation approval 
            System.assertEquals(SQX_Supplier_Common_Values.RECORD_TRANSITION_VALIDATION_ERROR_MSG,voidRecord[0].getErrors()[0].getMessage());
        }
    }
}