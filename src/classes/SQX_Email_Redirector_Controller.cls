/**
 * class to redirect page from email to lightning app if user is in lightning mode else use url given in email
 */
public with sharing class SQX_Email_Redirector_Controller {
    /**
     * redirect page from email to lightning app if user is in lightning mode else use url given in email
     */
    public String getRedirectToRecord(){
        String redirectTo = ApexPages.currentPage().getParameters().get('redirectTo');
        Boolean isLightningComponent = ApexPages.currentPage().getParameters().get('isLightningComponent')== 'true' ? true : false;
        
        String redirectUrl = SQX_Utilities.getBaseUrlForUser(isLightningComponent);

        redirectUrl = redirectUrl + SQX_Utilities.redirectUrlProtection(redirectTo);
        return redirectUrl;
    }
}