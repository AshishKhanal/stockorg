/**
*   This class contains test cases for ensuring that Inspection source is providing homepage items with desired values
*/

@isTest
public class SQX_Test_Homepage_Source_Inspection {

    /*
        Inspection source currently returns items of type :
        1. Open/Draft Inspection Items

    */

    @testSetup
    public static void commonSetup() {

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(adminUser) {

            // inserting test inspection records

            SQX_Test_Inspection draftIns = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PRODUCT, null)
                                                            .setStatus(SQX_Inspection.INSPECTION_STATUS_DRAFT)
                                                            .save();   // ins type : product

            SQX_Standard_Service__c process = new SQX_Standard_Service__c();
            process.Name = 'PRC-1';
            process.Description__c = 'Test Process';
            insert process;
            
            SQX_Test_Inspection openIns = new SQX_Test_Inspection(adminUser, SQX_Inspection.INSPECTION_TYPE_PROCESS, process.Id)
                                                .save(); // ins type : process
        }
    }


    /**
     *   Given : A set of Inspection items (open, draft) and a homepage component expecting records from Inspection Source
     *   When :  Homepage component is requested
     *   Then :  All inspection record types are added to the homepage with desired values
     */
    testmethod static void givenInspectionItemsWithDifferentStatus_WhenHomepageComponentIsRequested_ThenCorrepondingHomepageItemsShouldBeAddedToHomepageComponent() {

        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {

            // Arrange: Fetch Inspection Items
            Map<Id, SQX_Inspection__c> itms = new Map<Id, SQX_Inspection__c> (
                                                [SELECT Id, Name, CreatedDate, Status__c, Rev__c, Inspection_Type__c,
                                                    SQX_Part__r.Name, SQX_Part__r.Part_Number__c,
                                                    SQX_Process__r.Name
                                                    FROM SQX_Inspection__c
                                                ]
                                              );

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Inspection_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert: Inspection Items have been fetched with desired values
            System.assertNotEquals(hpItems, null);

            System.assertEquals(itms.size(), hpItems.size());

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(adminUser.Id, hpItem.creator.Id);

                System.assertEquals(null, hpItem.dueDate);

                System.assertEquals(0, hpItem.overdueDays);

                System.assertEquals(0, hpItem.itemAge);

                System.assertEquals(null, hpItem.urgency);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_INSPECTION, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                System.assertEquals(1, hpItem.actions.size());

                System.assertEquals(false, hpItem.supportsBulk);

                SQX_Homepage_Item_Action act = hpItem.actions.get(0);

                System.assertEquals(SQX_Homepage_Constants.ACTION_VIEW, act.name);

                System.assertEquals(SQX_Homepage_Constants.ICON_UTILITY_VIEW, act.actionIcon);

                System.assertEquals(false, act.supportsBulk);

                System.assert(itms.containsKey(hpItem.itemId), 'Expecting given item id to be that of the inspection record');

                SQX_Inspection__c ins = itms.get(hpItem.itemId);

                if(ins.Status__c == SQX_Inspection.INSPECTION_STATUS_DRAFT)
                {
                    System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS, hpItem.actionType);

                    String expectedFeedText = String.format
                                                        (SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS,
                                                            new String[] {
                                                                ins.Name,
                                                                ins.SQX_Part__r.Part_Number__c + ' '
                                                                + '[' + ins.Rev__c + '] - '
                                                                + ins.SQX_Part__r.Name
                                                            }
                                                        );
                    System.assertEquals(expectedFeedText, hpItem.feedText);
                }
                else if(ins.Status__c == SQX_Inspection.INSPECTION_STATUS_OPEN)
                {
                    System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS, hpItem.actionType);

                    String expectedFeedText = String.format
                                                        (SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS,
                                                            new String[] {
                                                                ins.Name,
                                                                ins.SQX_Process__r.Name
                                                            }
                                                        );
                    System.assertEquals(expectedFeedText, hpItem.feedText);
                }
            }

        }

    }
}
