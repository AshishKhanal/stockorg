/**
 *   This class contains test cases for ensuring that Training source is providing homepage items with desired values
 *   @author - Anuj Bhandari
 *   @date - 17-04-2017
 */

@isTest
public class SQX_Test_Homepage_Source_Training {

    /*
        Training source currently returns items of type :
        1. User Signoff/training signoff (along with refresher) type of pending training items for the current user

    */


    /*
     * Creates the Training items of personnel signoff, personnel refresher, Trainer signoff and trainer signoff of refresher
     */
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser) {
            // Arrange1: create required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel(standardUser);
            personnel.save();


            // Arrange2: Create controlled documents for training
            SQX_Test_Controlled_Document cd1 = new SQX_Test_Controlled_Document()
                                                    .setStatus(SQX_Controlled_Document.STATUS_CURRENT),
                                         cd2 = new SQX_Test_Controlled_Document()
                                                    .setStatus(SQX_Controlled_Document.STATUS_CURRENT),
                                         cd3 = new SQX_Test_Controlled_Document()
                                                    .setStatus(SQX_Controlled_Document.STATUS_CURRENT),
                                         cd4 = new SQX_Test_Controlled_Document()
                                                    .setStatus(SQX_Controlled_Document.STATUS_CURRENT);

            // making some of the contents as SCORM contents
            cd2.doc.Is_Scorm_Content__c = true;
            cd4.doc.Is_Scorm_Content__c = true;

            new SQX_DB().op_insert(new List<SQX_Controlled_Document__c>{ cd1.doc, cd2.doc, cd3.doc, cd4.doc },
                                    new List<Schema.SObjectField> {});

            /* Creating a Personnel Training */
            SQX_Personnel_Document_Training__c pt = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cd1.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today().addDays(-5),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                Is_Refresher__c = False,
                SQX_Trainer__c = StandardUser.id
            );

            // creating assessment to associate with training
            SQX_Test_Assessment tAsmt = new SQX_Test_Assessment()
                                            .setStatus(SQX_Assessment.STATUS_CURRENT)
                                            .save();

            /* Creating a Personnel Refresher Training */
            SQX_Personnel_Document_Training__c ptRef = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cd2.doc.Id,
                SQX_Assessment__c = tAsmt.assessment.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today().addDays(-5),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                Is_Refresher__c = True,
                SQX_Trainer__c = StandardUser.id
            );

            /* Creating a Trainer Signoff */
            SQX_Personnel_Document_Training__c tt = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cd3.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = System.today().addDays(-5),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                Is_Refresher__c = False,
                SQX_Trainer__c = adminUser.id,
                User_SignOff_Date__c = Date.today()
            );

            /* Creating a Trainer Signoff of Refresher Training */
            SQX_Personnel_Document_Training__c ttRef = new SQX_Personnel_Document_Training__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Controlled_Document__c = cd4.doc.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT,
                Due_Date__c = System.today().addDays(-5),
                Optional__c = false,
                Status__c = SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING,
                Is_Refresher__c = True,
                SQX_Trainer__c = StandardUser.id,
                User_SignOff_Date__c = Date.today()
            );

            new SQX_DB().op_insert(new List <SQX_Personnel_Document_Training__c> { pt, ptRef, tt, ttRef },
                                    new List <SObjectField> {});

        }
    }


    /**
     *   Given : A set of Document Training items (Personnel, Trainer, Personnel Refresher and Trainer Refresher) and a homepage component expecting records from Training Source
     *   When :  Training Records are fetched and processed
     *   Then :  All Training record types are added to the homepage with desired values
     *   @story -3165
     */
    testmethod static void givenPendingDocumentTrainingItemsOfDifferentTypes_WhenRecordsAreFetchedAndProcessed_ThenCorrepondingHomepageItemsShouldBeAddedToHomepageComponent() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        User stdUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        System.runAs(stdUser) {
            Test.startTest();
            // Arrange1: Fetch Pending Training Items
            List <SQX_Personnel_Document_Training__c> itms = [SELECT  Id,
                                                                        Name,
                                                                        CreatedDate,
                                                                        Title__c,
                                                                        Personnel_Name__c,
                                                                        Due_Date__c,
                                                                        Status__c,
                                                                        Is_Refresher__c,
                                                                        SQX_Controlled_Document__r.Is_Scorm_Content__c,
                                                              			Can_Take_Assessment__c,
                                                                        Document_Number_Rev__c
                                                                        FROM SQX_Personnel_Document_Training__c
                                                                    ];

            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Training_Items()
            };

            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

            List<Object> hpItems = (List<Object>) homepageItems.get('items');

            List<Object> errors = (List<Object>) homepageItems.get('errors');

            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());

            // Assert2:Document Training items have been fetched with desired values

            System.assertNotEquals(hpItems, null);

            System.assert(hpItems.size() == 3, 'Expected three items but got ' + hpItems.size());

            for (Object hpObj: hpItems) {

                SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpObj), SQX_Homepage_Item.class);

                System.assertEquals(stdUser.Id, hpItem.creator.Id);

                System.assertNotEquals(null, hpItem.dueDate);

                System.assertEquals(5, hpItem.overdueDays);

                System.assertEquals(SQX_Homepage_Constants.MODULE_TYPE_DOCUMENT_TRAINING, hpItem.moduleType);

                System.assertNotEquals(hpItem.actions, null);

                for (SQX_Personnel_Document_Training__c itm: itms) {
                    if (itm.id == hpItem.itemId) {
                        if (itm.Status__c == SQX_Personnel_Document_Training.STATUS_PENDING) {

                            validatePersonnelSignoffTrainigItem(itm, hpItem);
                            break;

                        } else if (itm.Status__c == SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING) {

                            validateTrainerSignoffTrainigItem(itm, hpItem);
                            break;

                        } else {

                            System.assert(false, 'Unknown record : ' + hpItem);

                        }
                    }
                }
            }
            Test.stopTest();
        }

    }

    /**
    * Ensured that trainer has their own item to approve
    **/
    private testMethod static void ensuredThatTrainerHasTheirOwnItemToApprove(){
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        System.runAs(adminUser) {
            Test.startTest();
            // add mock source to list of sources
            SQX_Consolidated_Home_Page_Component.allItemSources = new List <SQX_Homepage_ItemSource> {
                new SQX_Homepage_Source_Training_Items()
                    };
                        
            // Act1: Fetch Homepage items
            Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));
            
            List<Object> hpItems = (List<Object>) homepageItems.get('items');
            
            List<Object> errors = (List<Object>) homepageItems.get('errors');
            
            // Assert : Expecting no errors
            System.assertEquals(0, errors.size());
            
            // Assert2:Document Training items have been fetched with desired values
            
            System.assertNotEquals(hpItems, null);
            
            System.assert(hpItems.size() == 1, 'Expected one items but got ' + hpItems.size());
            Test.stopTest();
        }
    }


    /**
     *   Method validates the homepage item of type Personnel Signoff
     */
    private static void validatePersonnelSignoffTrainigItem(SQX_Personnel_Document_Training__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

        System.assertNotEquals(hpItem.actions, null);

        Set<String> expectedActions = new Set<String>();

        Boolean supportsBulk = false;
        if(itm.SQX_Controlled_Document__r.Is_Scorm_Content__c)
        {
            expectedActions.add(SQX_Homepage_Constants.ACTION_LAUNCH_CONTENT);
        }
        else
        {
            expectedActions.add(SQX_Homepage_Constants.ACTION_VIEW_CONTENT);
            supportsBulk = true;
        }

        if(itm.Can_Take_Assessment__c)
        {
            expectedActions.add(SQX_Homepage_Constants.ACTION_TAKE_ASSESSMENT);
        }
        else if(!itm.SQX_Controlled_Document__r.Is_Scorm_Content__c)
        {
            expectedActions.add(SQX_Homepage_Constants.ACTION_USER_SIGNOFF);
            supportsBulk = true;
        }

        System.assertEquals(supportsBulk, hpItem.supportsBulk);

        System.assertEquals(expectedActions.size(), hpItem.actions.size());

        for (SQX_Homepage_Item_Action act: hpitem.actions) {
            System.assert(expectedActions.contains(act.Name), 'Unexpected action : ' + act.Name);
        }

        String expectedFeedText = null;
        if (itm.Is_Refresher__c) {
            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_USER_REFRESHER, new String[] {
                itm.Name, itm.Title__c, itm.Document_Number_Rev__c
            });
        } else {
            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_USER, new String[] {
                itm.Name, itm.Title__c, itm.Document_Number_Rev__c
            });
        }

        System.assertEquals(expectedFeedText, hpItem.feedText);
    }


    /**
     *   Method validates the homepage item of type Personnel Signoff
     */
    private static void validateTrainerSignoffTrainigItem(SQX_Personnel_Document_Training__c itm, SQX_Homepage_Item hpItem) {

        System.assertEquals(SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION, hpItem.actionType);

        System.assertNotEquals(hpItem.actions, null);

        Set<String> expectedActions = new Set<String>();

        System.assertEquals(true, hpItem.supportsBulk);
        
        expectedActions.add(SQX_Homepage_Constants.ACTION_VIEW_CONTENT);

        expectedActions.add(SQX_Homepage_Constants.ACTION_TRAINER_SIGNOFF);

        System.assertEquals(expectedActions.size(), hpItem.actions.size());

        for (SQX_Homepage_Item_Action act: hpitem.actions) {
            System.assert(expectedActions.contains(act.Name), 'Unexpected action : ' + act.Name);
        }

        String expectedFeedText = null;
        if (itm.Is_Refresher__C) {
            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER_REFRESHER, new String[] {
                itm.Name, itm.Title__c, itm.Document_Number_Rev__c
            });
        } else {
            expectedFeedText = String.format(SQX_Homepage_Constants.FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER, new String[] {
                itm.Name, itm.Title__c, itm.Document_Number_Rev__c
            });
        }
        System.assertEquals(expectedFeedText, hpItem.feedText);
    }
}