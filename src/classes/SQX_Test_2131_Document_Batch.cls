@isTest
public class SQX_Test_2131_Document_Batch {

    static final String DRAFT_LIB_TYPE = 'Draft',
                        RELEASE_LIB_TYPE = 'Release',
                        DISTRIBUTION_LIB_TYPE = 'Distribution';

    static Boolean runAllTests = true,
                   run_givenUser_WhenControlledDocIsReleasedOrObsoleted_RelatedContentDocsAreReleasedorObsoleted = false,
                   run_givenUser_WhenControlledDocIsReleasedorObsoleted_RelatedContentDocsError= false,
                   run_givenUser_WhenControlledDocIsRelease_BothQueueProcessed= false,
                   run_ensureDatesPopulatedOnRelease= false,
                   run_givenControlledDocIsReleased_PreviousRevisionsAreObsoletedImmediately = false;

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');

        List<Id> userIdList = new List<Id>();
         userIdList.add(adminUser.Id);
         userIdList.add(standardUser1.Id);
         userIdList.add(standardUser2.Id);
         
        SQX_Test_Utilities.createLibraries(userIdList);
    }


    /**
    * Scenario: Batch jobs can handle release and also obsolescence of documents
    * Given Create two controlled documents a1 and b1
    *         a1, b1 are released, with b1 having expiration date set to today
    * When batch job is run twice
    * Then a1 release job is completed in first run and b1 is scheduled for obsolescence
    *      b1 is obsoleted in the second run.
    **/
    public testmethod static void givenUser_WhenControlledDocIsReleasedOrObsoleted_RelatedContentDocsAreReleasedorObsoleted(){
        if (!runAllTests && !run_givenUser_WhenControlledDocIsReleasedOrObsoleted_RelatedContentDocsAreReleasedorObsoleted) {
            return;
        }

         // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser1 = userMap.get('standardUser1');
        User adminUser = userMap.get('adminUser');
        System.runas(adminUser){
            // make Use_Batch_For_Document_Status_Change__c true
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
            });
        
            //Arrange: create document a1, b1
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save(), //DOC 1
                                        b1 = new SQX_Test_Controlled_Document().save(); //DOC 2

            a1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            a1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);
                                        
            b1.doc.Expiration_Date__c = Date.Today();                            
            
            //approve a1, b1
            a1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            a1.doc.First_Approver__c = adminUser.Id;
            a1.save();
            b1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            b1.doc.First_Approver__c = adminUser.Id;
            b1.save();

            a1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            b1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;

            a1.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();
            
            b1.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();

            Set<Id> allControlledDocIds = new Set<Id>();
            allControlledDocIds.add(a1.doc.Id);
            allControlledDocIds.add(b1.doc.Id);


            Map<Id, SQX_Controlled_Document__c> allControlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Batch_Job_Status__c from SQX_Controlled_Document__c where Id in :allControlledDocIds]);

            //get updated status
            SQX_Controlled_Document__c doc1 = allControlledDocs.get(a1.doc.Id);
            SQX_Controlled_Document__c doc2 = allControlledDocs.get(b1.doc.Id);

            //Assert: Documents are queued for pre-release
            //Assert: a1 is pre-release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED, doc1.Batch_Job_Status__c);

            //Assert: b1 is pre-release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED, doc2.Batch_Job_Status__c);

            Test.startTest();
            //Act: Start batch process
            SQX_Document_Batch_Job_Processor docBatch = new SQX_Document_Batch_Job_Processor();
            Database.executeBatch(docBatch);
            Test.stopTest();
            
            allControlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Document_Status__c, Batch_Job_Status__c, Batch_Job_Error__c 
            														from SQX_Controlled_Document__c where Id in :allControlledDocIds]);

            //get updated status
            doc1 = allControlledDocs.get(a1.doc.Id);
            doc2 = allControlledDocs.get(b1.doc.Id);
            
            //Assert: batch jobs are completed
            //Assert : a1 is pre-released and batch release status is release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_RELEASE_QUEUED, doc1.Batch_Job_Status__c);
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_RELEASE, doc1.Document_Status__c);

            //Assert : b1 is pre-released and batch release status is release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_RELEASE_QUEUED, doc2.Batch_Job_Status__c);
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_RELEASE, doc2.Document_Status__c);

            //call the function which processes the batch job, as test.starttest cannot be used twice
            new SQX_Document_Batch_Job_Processor().processBatchJob([SELECT Id, Batch_Job_Status__c, Document_Status__c, Document_Number__c 
            FROM SQX_Controlled_Document__c
            WHERE Batch_Job_Status__c = :SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED 
            Order By LastModifiedDate ASC]);

            allControlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Document_Status__c, Batch_Job_Status__c, Batch_Job_Error__c 
                                                                    from SQX_Controlled_Document__c where Id in :allControlledDocIds]);

            //get updated status
            doc1 = allControlledDocs.get(a1.doc.Id);
            doc2 = allControlledDocs.get(b1.doc.Id);

            //Assert: a1 is released and batch release status is blanked out
            System.assertEquals(null, doc1.Batch_Job_Status__c);
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, doc1.Document_Status__c);

            //Assert: b1 is obsoleted, and batch release status is blanked out
            System.assertEquals(null, doc2.Batch_Job_Status__c);
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, doc2.Document_Status__c);

            //Act: Pre-expire b1 so that it is queued for obsolescence
            b1.setStatus(SQX_Controlled_Document.STATUS_PRE_EXPIRE).save();
            allControlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Document_Status__c, Batch_Job_Status__c, Batch_Job_Error__c 
                                                                    from SQX_Controlled_Document__c where Id in :allControlledDocIds]);

            //get updated status
            doc2 = allControlledDocs.get(b1.doc.Id);
            System.assertEquals(SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED, doc2.Batch_Job_Status__c);

            //call the function which processes the batch job, as test.starttest cannot be used twice
            new SQX_Document_Batch_Job_Processor().processBatchJob([SELECT Id, Batch_Job_Status__c, Document_Status__c, Document_Number__c 
            FROM SQX_Controlled_Document__c
            WHERE Batch_Job_Status__c = :SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_RELEASE_QUEUED 
                OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED 
            Order By LastModifiedDate ASC]);

            allControlledDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Document_Status__c, Batch_Job_Status__c, Batch_Job_Error__c 
                                                                    from SQX_Controlled_Document__c where Id in :allControlledDocIds]);

            //get updated status
            doc2 = allControlledDocs.get(b1.doc.Id);
            System.assertEquals(null, doc2.Batch_Job_Status__c);
            System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, doc2.Document_Status__c);

        }
    }

    /**
    * Given : Create three controlled documents a1, b1 and c1
    *           a1 is released
    *           b1 is released and obsoleted 
    *           c1 is pre-released
    * When : batch jobs are run
    * Then : Error is generated as different standard user wont have access to update controlled doc  
    **/
    public testmethod static void givenUser_WhenControlledDocIsReleasedorObsoleted_RelatedContentDocsError(){

        if (!runAllTests && !run_givenUser_WhenControlledDocIsReleasedorObsoleted_RelatedContentDocsError) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();

        System.runas(allUsers.get('adminUser')){
            // make Use_Batch_For_Document_Status_Change__c true
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
            });
        }

        SQX_Test_Controlled_Document a1, b1, c1;
        System.runas(allUsers.get('standardUser1')){ 
            //Arrange create document a1, b1, c1 and d1
            a1 = new SQX_Test_Controlled_Document().save(); //DOC 1
            b1 = new SQX_Test_Controlled_Document().save(); //DOC 2
            c1 = new SQX_Test_Controlled_Document().save(); //DOC 3

            c1.doc.Effective_Date__c = Date.Today().addDays(4);
            b1.doc.Expiration_Date__c = Date.Today(); 
            
            //Act: Release a1, b1 and c1
            a1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            b1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();
            //obsolete c1
            b1.setStatus(SQX_Controlled_Document.STATUS_PRE_EXPIRE).save();

            //pre-release d1
            c1.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();

            SQX_Controlled_Document__c doc1 = [SELECT Batch_Job_Status__c from SQX_Controlled_Document__c where Id = :a1.doc.Id];
            SQX_Controlled_Document__c doc2 = [SELECT Batch_Job_Status__c from SQX_Controlled_Document__c where Id = :b1.doc.Id];
            SQX_Controlled_Document__c doc3 = [SELECT Batch_Job_Status__c from SQX_Controlled_Document__c where Id = :c1.doc.Id];

            //Assert: batch jobs are queued

            //Assert: a1 is release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_RELEASE_QUEUED, doc1.Batch_Job_Status__c);

            //Assert: b1 is release, distribution and obsolete queued
            System.assertEquals(SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED, doc2.Batch_Job_Status__c);

            //Assert: c1 is pre-release queued
            System.assertEquals(SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED, doc3.Batch_Job_Status__c);

        }

        System.runas(allUsers.get('standardUser2')){
            try{
                //Act: Start batch release
                Test.startTest();
                    SQX_Document_Batch_Job_Processor docBatch = new SQX_Document_Batch_Job_Processor();
                    Database.executeBatch(docBatch);              
                Test.stopTest();
                System.assert(false, 'should not come here, batch should throw exception and should go to catch block before execution this statement');
            }
            catch(Exception e){
                //Assert: Error is generated as standard user 2 wont have access to update controlled doc 
                String expectedExceptionThrown =  e.getMessage();
                System.assert(expectedExceptionThrown!=null, 'Expected to throw error'+expectedExceptionThrown);       
            }
        } 

    }

    /**
    * Given : Controlled Document is saved
    * When : Controlled Document is Pre-released and then released immendiately
    * Then : 1st batch job is run: pre-release is processed. 2nd batch job is run, release is processed
    */
    public testmethod static void givenUser_WhenControlledDocIsRelease_BothQueueProcessed(){

        if (!runAllTests && !run_givenUser_WhenControlledDocIsRelease_BothQueueProcessed) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();

        System.runas(allUsers.get('adminUser')){
            // make Use_Batch_For_Document_Status_Change__c true
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
            });
        }


        System.runas(allUsers.get('standardUser1')){

            //Arrange : Save a controlled document, approve it and release it
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save();
            a1.doc.Draft_Vault__c = SQX_Test_Utilities.getLibraryId(DRAFT_LIB_TYPE);
            a1.doc.Release_Vault__c = SQX_Test_Utilities.getLibraryId(RELEASE_LIB_TYPE);

            //approve a1
            a1.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            a1.doc.First_Approver__c = UserInfo.getUserId();
            a1.save();
            a1.setStatus(SQX_Controlled_Document.STATUS_APPROVED).save();

            update new SQX_Controlled_Document__c(Id = a1.doc.Id, Batch_Job_Status__c = SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED);

            System.runas(allUsers.get('adminUser')){
                Test.startTest();
                    //Act: Start batch process
                    SQX_Document_Batch_Job_Processor docBatch = new SQX_Document_Batch_Job_Processor();
                    Database.executeBatch(docBatch);
                Test.stopTest();
            }

            //get updated status
            SQX_Controlled_Document__c doc1 = [SELECT Batch_Job_Status__c, Document_Status__c, Batch_Job_Error__c from SQX_Controlled_Document__c where Id = :a1.doc.Id];


            System.assertEquals(null, doc1.Batch_Job_Error__c, 'Expected to have no error');
            //Assert : Ensure that batch job successfully processes the pre-release
            System.assertEquals(SQX_Controlled_Document.BATCH_RELEASE_QUEUED, doc1.Batch_Job_Status__c, 'Expected to successfully process the batch for pre-release');
            System.assertEquals(SQX_Controlled_Document.STATUS_PRE_RELEASE, doc1.Document_Status__c, 'Expected document to be pre-released');

            System.runas(allUsers.get('adminUser')){
                //call the function which process the batch job as the test.starttest cannot be used twice
                new SQX_Document_Batch_Job_Processor().processBatchJob([SELECT Id, Batch_Job_Status__c, Document_Status__c, Document_Number__c
                FROM SQX_Controlled_Document__c
                WHERE Batch_Job_Status__c = :SQX_Controlled_Document.BATCH_PRE_RELEASE_QUEUED 
                    OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_RELEASE_QUEUED 
                    OR Batch_Job_Status__c= :SQX_Controlled_Document.BATCH_OBSOLESCENCE_QUEUED 
                Order By LastModifiedDate ASC]);
            }

            //get updated status
            doc1 = [SELECT Batch_Job_Status__c, Document_Status__c from SQX_Controlled_Document__c where Id = :a1.doc.Id];


            //Assert : Ensure that batch job successfully processes the release
            System.assertEquals(null, doc1.Batch_Job_Status__c, 'Expected to successfully process the batch for pre-release');
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, doc1.Document_Status__c, 'Expected document to be released');

        }


    }

    /**
    * @Story : SQX-2106
    * Scenario 1
    * Given : Controlled Document is saved without Issued Date, Effective Date, Next Review Date and with Duration = 2, and review interval =3
    * When : Controlled Doc is Released
    * Then : Issued Date, Effective Date is set to  Today +2 and Next Review Date is set to Today +3
    *
    * Scenario 2
    * Given : Controlled Document is saved with Issued Date = Today, Effective Date= Today and with Duration = 2, and review interval =3
    * When : Controlled Doc is Released
    * Then : Issued Date, Effective Date is set to  Today +2 and Next Review Date is set to Today +3
    */

    public testmethod static void ensureDatesPopulatedOnRelease(){
        if (!runAllTests && !run_ensureDatesPopulatedOnRelease){
            return;
        }

        //Arrange1 : create new document with effective date, issued date and next review date as null, 
        //duration as 2 and review interval as 3
        SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document(),
                                    b1 = new SQX_Test_Controlled_Document();
        

        a1.doc.Date_Issued__c = null;
        a1.doc.Effective_Date__c = null;
        a1.doc.Next_Review_Date__c = null;
        a1.doc.Duration__c = 2;
        a1.doc.Review_Interval__c = 3;

        a1.save();

        //Act: release document
        a1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

        SQX_Controlled_Document__c doc = [Select Date_Issued__c, Effective_Date__c, Next_Review_Date__c 
                                            FROM SQX_Controlled_Document__c WHERE Id =:a1.doc.Id];

        //Assert1 : 
        //Expected  Issued Date to be Today
        //Expected Effecive Date to today + duration
        //Expected Next Review Date to be today + review Interval                                  
        System.assertEquals(Date.Today(), doc.Date_Issued__c, 'Expected Issued date to be today');
        System.assertEquals(Date.Today()+Integer.valueof(a1.doc.Duration__c), doc.Effective_Date__c, 'Expected effective date to be today plus duration');
        System.assertEquals(Date.Today()+Integer.valueof(a1.doc.Review_Interval__c), doc.Next_Review_Date__c, 'Expected next review date to be today plus Review Interval');

        

        //Arrange2 : create new document with effective date, issued date and next review date as Today+1, 
        //duration as 2 and review interval as 3
        b1.doc.Date_Issued__c = Date.Today()+1;
        b1.doc.Effective_Date__c = Date.Today()+1;
        b1.doc.Next_Review_Date__c = Date.Today()+1;
        b1.doc.Duration__c = 2;
        b1.doc.Review_Interval__c = 3;

        b1.save();

        //Act: release document
        b1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

        doc = [Select Date_Issued__c, Effective_Date__c, Next_Review_Date__c
                                            FROM SQX_Controlled_Document__c WHERE Id =:b1.doc.Id];

        //Assert : ensure that Issued Date, Effective Date and Next Review Date to be unchanged                                   
        System.assertEquals(Date.Today()+1, doc.Date_Issued__c, 'Expected Issued date to be unchanged');
        System.assertEquals(Date.Today()+1, doc.Effective_Date__c, 'Expected effective date to be unchanged');
        System.assertEquals(Date.Today()+1, doc.Next_Review_Date__c, 'Expected next review date to be unchanged');


    }


    /**
    * This test ensurest that if a controlled document is released, previous current revisions are obsoleted immediately.
    *
    * @story SQX-2375
    */
    testmethod static void givenControlledDocIsReleased_PreviousRevisionsAreObsoletedImmediately(){
        if (!runAllTests && !run_givenControlledDocIsReleased_PreviousRevisionsAreObsoletedImmediately) {
            return;
        }

         // get users
        Map<String, User> userMap = SQX_Test_Account_Factory.getUsers();
        User standardUser1 = userMap.get('standardUser1');
        User adminUser = userMap.get('adminUser');

        System.runas(adminUser){
            // make Use_Batch_For_Document_Status_Change__c true
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
            });
            
            //Arrange: create a document that is current and revise it
            SQX_Test_Controlled_Document currentDoc = new SQX_Test_Controlled_Document().save(); //DOC 1

            currentDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            currentDoc.doc.First_Approver__c = adminUser.Id;
            currentDoc.save();

            currentDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            currentDoc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

            currentDoc.save();

            SQX_Test_Controlled_Document revisedDoc = currentDoc.revise();

            Test.startTest();

            // Act: Make the revised doc current
            revisedDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_RELEASE_APPROVAL;
            revisedDoc.doc.First_Approver__c = adminUser.Id;
            revisedDoc.save();

            revisedDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_APPROVED;
            revisedDoc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

            revisedDoc.save();

            Test.stopTest();



            // Assert: Ensure that revised document is current and old document is obsolete
            Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c
             WHERE Id = : currentDoc.doc.Id OR Id = : revisedDoc.doc.Id ]);

            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, docs.get(revisedDoc.doc.Id).Document_Status__c, 'Expected revised doc to be current');
            System.assertEquals(SQX_Controlled_Document.STATUS_OBSOLETE, docs.get(currentDoc.doc.Id).Document_Status__c, 'Expected old doc to be obsoleted');


        }
       
    }
}