/**
* This class will contain the common methods and static strings required for the Material Transaction object 
* @story   SQX-2810 Inspection: Process Receipt
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-01-10
*/
public with sharing class SQX_Material_Transaction {
    public static String CODE_TRANSACTION_INCOMING_RECEIPT = 'INCOMING_RECEIPT',
                         CODE_TRANSACTION_INSPECTION_CREATED = 'INSPECTION_CREATED',
                         CODE_TRANSACTION_INSPECTION_PASS = 'INSPECTION_PASS',
                         CODE_TRANSACTION_INSPECTION_FAIL = 'INSPECTION_FAIL';
        
    public static String STATUS_SUCCESS = 'Completed',
                         STATUS_PENDING = 'Pending',
                         STATUS_FAILED  = 'Failed';

    public List<SObject> scope;

    Map<String, SQX_Material_Transaction__c> mtMap = new Map<String, SQX_Material_Transaction__c>();
    Map<String, SQX_Part__c> partMap = new Map<String, SQX_Part__c>();
    Map<String, SQX_Standard_Service__c> processMap = new Map<String, SQX_Standard_Service__c>();
    Map<String, Account> supplierMap = new Map<String, Account>();

    //Material Transaction to Receipt Map
    public static Map<Schema.SObjectField, Schema.SObjectField>  MATERIAL_TRANSACTION_TO_RECEIPT_MAP=
        new Map<Schema.SObjectField, Schema.SObjectField>{
            SQX_Material_Transaction__c.fields.Business_Unit__c => SQX_Receipt__c.fields.Org_Business_Unit__c,
            SQX_Material_Transaction__c.fields.Division__c => SQX_Receipt__c.fields.Org_Division__c,
            SQX_Material_Transaction__c.fields.Region__c => SQX_Receipt__c.fields.Org_Region__c,
            SQX_Material_Transaction__c.fields.Site__c => SQX_Receipt__c.fields.Org_Site__c,
            SQX_Material_Transaction__c.fields.External_Reference__c => SQX_Receipt__c.fields.External_Reference__c,
            SQX_Material_Transaction__c.fields.Line_Number__c => SQX_Receipt__c.fields.Line_Number__c,
            SQX_Material_Transaction__c.fields.Lot_Number__c => SQX_Receipt__c.fields.Lot_Number__c,
            SQX_Material_Transaction__c.fields.Inspection_Required__c => SQX_Receipt__c.fields.Needs_Inspection__c,
            SQX_Material_Transaction__c.fields.SQX_Part_Number__c => SQX_Receipt__c.fields.SQX_Part_Number__c,
            SQX_Material_Transaction__c.fields.Part_Rev__c => SQX_Receipt__c.fields.Part_Revision__c,
            SQX_Material_Transaction__c.fields.PO_WO_Number__c => SQX_Receipt__c.fields.PO_Number__c,
            SQX_Material_Transaction__c.fields.SQX_Process__c => SQX_Receipt__c.fields.SQX_Process__c,
            SQX_Material_Transaction__c.fields.Quantity__c => SQX_Receipt__c.fields.Received_Quantity__c,
            SQX_Material_Transaction__c.fields.Supplier__c => SQX_Receipt__c.fields.SQX_Supplier_Number__c,
            SQX_Material_Transaction__c.fields.Id => SQX_Receipt__c.fields.SQX_Transaction_Number__c,
            SQX_Material_Transaction__c.fields.Unit_of_Measure__c => SQX_Receipt__c.fields.Unit_of_Measure__c
        };

    /**
     * constructor method with sobject as parameter
     * @param scopeToUse list of sobject
     */
    public SQX_Material_Transaction(List<SObject> scopeToUse){
        scope = scopeToUse;
    }

    /**
     * method to process material transaction to create receipt
     */
    public void processMaterialTransaction(){
        preProcess();
        processMaterialTransactionBatchJob();
    }

    /**
     * method to process the  material transaction to find the id of the record from the string given for part, process, division, business unit, site, etc
     */
    private void preProcess(){
        List<String> partList = new List<String>();
        List<String> processList = new List<String>();
        List<String> supplierList = new List<String>();

        for(Integer i = 0; i < scope.size(); i++){
            SQX_Material_Transaction__c mt = (SQX_Material_Transaction__c) scope.get(i);

            mtMap.put(mt.Id, mt);

            if(!String.isBlank(mt.Part_Number__c)){
                partList.add(mt.Part_Number__c);
            }

            if(!String.isBlank(mt.Process__c)){
                processList.add(mt.Process__c);
            }

            if(!String.isBlank(mt.Supplier_Name__c)){
                supplierList.add(mt.Supplier_Name__c);
            }
        }

        for(SQX_Part__c part : [SELECT Id, Name, Part_Number__c FROM SQX_Part__c WHERE Part_Number__c =: partList]){
            partMap.put(part.Part_Number__c, part);
        }

        for(SQX_Standard_Service__c process : [SELECT Id, Name FROM SQX_Standard_Service__c WHERE Name =: processList]){
            processMap.put(process.Name, process);
        }

        for(Account acc : [SELECT Id, Name FROM Account WHERE Name =: supplierList]){
            supplierMap.put(acc.Name, acc);
        }

        SQX_Part__c part;
        SQX_Standard_Service__c process;
        Account supplier;
        List<String> errors = new List<String>();

        for(Integer i = 0; i < scope.size(); i++){
            SQX_Material_Transaction__c mt = (SQX_Material_Transaction__c) scope.get(i);

            if(mt.Part_Number__c != null){
                part = partMap.get(mt.Part_Number__c);
                if(part == null){
                    errors.add(Label.SQX_ERR_MSG_Part_Number_does_not_exist);
                } else {
                    mt.SQX_Part_Number__c = part.Id;
                }
            }

            if(mt.Process__c != null){
                process = processMap.get(mt.Process__c);
                if(process == null){
                    errors.add(Label.SQX_ERR_MSG_Process_does_not_exist);
                } else {
                    mt.SQX_Process__c = process.Id;
                }
            }

            if(mt.Supplier_Name__c != null){
                supplier = supplierMap.get(mt.Supplier_Name__c);
                if(supplier == null){
                    errors.add(Label.SQX_ERR_MSG_Supplier_does_not_exist);
                } else {
                    mt.Supplier__c = supplier.Id;
                }
            }


           if (errors.size() > 0){
               mt.Status__c = SQX_Material_Transaction.STATUS_FAILED;
               mt.Error_Description__c = JSON.serialize(errors);
           }
           errors.clear();
        }
    }
    
    /**
    * creates the receipt entry for the material transaction records that need processing. In case of update error, updates error field with error message
    */
    public void processMaterialTransactionBatchJob(){
        List<SQX_Material_Transaction__c> allPendingIncomingReceiptsInMaterialTransaction =  (List<SQX_Material_Transaction__c>) scope;
        List<SQX_Receipt__c> receiptItemsToAdd = new List<SQX_Receipt__c>();
        List<SQX_Material_Transaction__c> materialTransactionItemsToUpdateProcessingStatus = new List<SQX_Material_Transaction__c>();
        List<SQX_Material_Transaction__c> failedMaterialTransactionItemsToUpdateProcessingStatus = new List<SQX_Material_Transaction__c>();
        Map<Schema.SObjectField, Schema.SObjectField> materialTransactionToRecieptMap = SQX_Material_Transaction.MATERIAL_TRANSACTION_TO_RECEIPT_MAP;
       
        //get all receipt item objects to add to the database
        for (SQX_Material_Transaction__c transactionItem: allPendingIncomingReceiptsInMaterialTransaction) {
            if(transactionItem.Status__c != SQX_Material_Transaction.STATUS_FAILED){
               receiptItemsToAdd.add(generateReceiptObjectToAdd(materialTransactionToRecieptMap, transactionItem));
            } else {
                failedMaterialTransactionItemsToUpdateProcessingStatus.add(transactionItem);
            }
        }
        //add receipt objects to database
        Database.SaveResult [] insertResults = addReceiptRecordsForBatchedMaterialTransactions(receiptItemsToAdd);
       
        //Update the material transaction object with the result- The outcome of the insert operation
        //if successful, update the material transaction table with the receipt number and status as Completed
        //else update the error fields and status as Failed for corresponding material transaction record.
        for(Integer i = 0; i < insertResults.size(); i++){
            Database.SaveResult result = insertResults[i];
            if(!result.isSuccess()){
                //Update tasks for successful items
                SQX_Receipt__c errReceipt = receiptItemsToAdd.get(i);
                Database.Error error = result.getErrors().get(0);
                SQX_Material_Transaction__c failedTransactionRecord = updateFailureStatus(errReceipt, error);                                                                     
                materialTransactionItemsToUpdateProcessingStatus.add(failedTransactionRecord);
            }
            else{ 
                //update tasks if the items do not get inserted to Receipts object
                SQX_Receipt__c successReceipt = receiptItemsToAdd.get(i);
                SQX_Material_Transaction__c successfulTransactionRecord = updateSuccessStatus(successReceipt);
                materialTransactionItemsToUpdateProcessingStatus.add(successfulTransactionRecord);                   
            }
        }

        materialTransactionItemsToUpdateProcessingStatus.addAll(failedMaterialTransactionItemsToUpdateProcessingStatus);
        updateMaterialTransactions(materialTransactionItemsToUpdateProcessingStatus);
    }
    
    /**
    * Updates and returns the successful material transaction records after processing.
    * @params successfulReceipt - Receipt item that was added successfully.
    */
   SQX_Material_Transaction__c updateSuccessStatus(SQX_Receipt__c successfulReceipt){
        //SQX_Material_Transaction__c successfulTran = new SQX_Material_Transaction__c(Id = successfulReceipt.SQX_Transaction_Number__c);
        SQX_Material_Transaction__c successfulTran = mtMap.get(successfulReceipt.SQX_Transaction_Number__c);               
        successfulTran.Status__c= SQX_Material_Transaction.STATUS_SUCCESS;
        successfulTran.SQX_Receipt_Number__c= successfulReceipt.id;
        //wipe it off if the record failed in last processing and had the error field values.
        successfulTran.Error_Description__c=''; 
        successfulTran.Error_Code__c = '';
        return successfulTran;
    }
    
    /**
    * Updates and returns material transaction record for unsuccessful processing - i.e. creation of its receipt record.
    * @param errReceipt Receipt item that was not inserted successfully.
    * @param error Database error captured during the insertion.
    * @return failed material transaction item with updated status and populated error fields
    */
    SQX_Material_Transaction__c updateFailureStatus(SQX_Receipt__c errReceipt, Database.Error error){                    
        //SQX_Material_Transaction__c errTran = new SQX_Material_Transaction__c(Id = errReceipt.SQX_Transaction_Number__c);
        SQX_Material_Transaction__c errTran = mtMap.get(errReceipt.SQX_Transaction_Number__c);               
        errTran.Error_Code__c= String.valueOf(error.getStatusCode());
        errTran.Error_Description__c = error.getMessage();
        errTran.Status__c = SQX_Material_Transaction.STATUS_FAILED; 
        return errTran;
    }

    /**
    * Creates and Returns a receipt object for the Material transactions in the batch.
    * @param materialTransactionToRecieptMap Map of Material Transaction and Receipt Objects
    * @param transactionItem Transaction Record that is currently being processed
    * @return Receipt object to be added for the processed material transaction
    */
    @testVisible 
    SQX_Receipt__c generateReceiptObjectToAdd(Map<Schema.SObjectField, Schema.SObjectField>materialTransactionToRecieptMap, SQX_Material_Transaction__c transactionItem){
        SQX_Receipt__c rec = new SQX_Receipt__c();
        for (Schema.SObjectField transacField: materialTransactionToRecieptMap.keySet())
        {
            Schema.SObjectField rField=materialTransactionToRecieptMap.get(transacField);
            rec.put(rField, transactionItem.get(transacField));
        }
        return rec;
    }
    
    /**
    * Adds new Receipt records for the material transactions that were batched for processing.
    * Returns the SaveResult which can be used to update the status and error fields of the corresponding material transaction records.
    * @param receiptItemsToAdd- List of new receipt items that need to be added
    */
    @testVisible
    Database.SaveResult [] addReceiptRecordsForBatchedMaterialTransactions(List<SQX_Receipt__c> receiptItemsToAdd){
        return new SQX_DB().continueOnError().op_insert(receiptItemsToAdd, 
                                                        new List<Schema.SObjectField>{
                                                            Schema.SQX_Receipt__c.Org_Division__c,
                                                            Schema.SQX_Receipt__c.Org_Business_Unit__c,
                                                            Schema.SQX_Receipt__c.Org_Region__c,
                                                            Schema.SQX_Receipt__c.Org_Site__c,
                                                            Schema.SQX_Receipt__c.External_Reference__c,
                                                            Schema.SQX_Receipt__c.Line_Number__c,
                                                            Schema.SQX_Receipt__c.fields.Lot_Number__c,
                                                            Schema.SQX_Receipt__c.Needs_Inspection__c,
                                                            Schema.SQX_Receipt__c.SQX_Part_Number__c,
                                                            Schema.SQX_Receipt__c.Part_Revision__c,
                                                            Schema.SQX_Receipt__c.fields.PO_Number__c,
                                                            Schema.SQX_Receipt__c.fields.SQX_Process__c,                
                                                            Schema.SQX_Receipt__c.Received_Quantity__c,
                                                            Schema.SQX_Receipt__c.SQX_Supplier_Number__c,
                                                            Schema.SQX_Receipt__c.SQX_Transaction_Number__c
                                                            }
                                                       );
    }
    
    /**
    * Updates the Material Transaction records with their corresponding Receipt record values 
    * If successful, SQX_Receipt_Number and Status are updated
    * If Failed, Status, Error Code and Error Description fields are updated
    * @param materialTransactions - List of material transaction records that were batched for processing.
    */
    void updateMaterialTransactions(List<SQX_Material_Transaction__c> materialTransactions){
        Database.SaveResult [] updateResults = new SQX_DB().op_update(materialTransactions,
                                                                        new List<Schema.SObjectField>{Schema.SQX_Material_Transaction__c.Status__c,
                                                                            Schema.SQX_Material_Transaction__c.SQX_Receipt_Number__c,                    
                                                                            Schema.SQX_Material_Transaction__c.Error_Code__c,
                                                                            Schema.SQX_Material_Transaction__c.Error_Description__c,
                                                                            Schema.SQX_Material_Transaction__c.Supplier__c,
                                                                            Schema.SQX_Material_Transaction__c.SQX_Process__c,
                                                                            Schema.SQX_Material_Transaction__c.SQX_Part_Number__c
                                                                            }
                                                                       );
    }

}