// test class to ensure that controlled document release is working as per requirement
@isTest
public class SQX_Test_1965_Controlled_Doc_Release{

    static Boolean runAllTests = true,
                   run_givenUser_WhenControlledSaved_DateValidationEnsuredProperly = false,
                   run_givenADoc_RequirementAreActive_WhenDocIsPreReleased = false,
                   run_givenADoc_RequirementCanBeAdded_WhenDocIsInPreRelease = false,
                   run_givenADoc_DocumentTrainingIsAdded_WhenDocIsPreReleased = false,
                   run_givenADoc_DocumentTrainingCanBeAdded_WhenDocIsPreReleased = false,
                   run_givenADoc_DocIsApprovedAndCurrentAndEffectiveDateIsToday_DocIsCurrent = false,
                   run_givenADoc_WhenUserWithNoAccessRelasesDoc_ItGivesError = false,
                   run_givenADoc_WhenEffectiveAndIssuedDatesAreChanged_EnsureProperValidaionIsDone = false,
                   run_givenADoc_WhenDocIsReleasedUsingReleaseButton_BatchJobStatusIsCleared = false;

    public static final String  ENSURE_EXPIRATION_MORE_THAN_EFFECTIVE_ERR_MSG = 'Expiration date should always be greater than or equal to effective date';

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'testUser1');
    }
    
    /**
    * Given : Controlled doc is saved
    * When  : Expiration date is  set greater than effecive date
    * Then : Controlled doc is saved without any error
    *
    * Given : Controlled doc is saved
    * When  : Expiration date is set equal than effecive date
    * Then : Controlled doc is saved without any error
    *
    * Given : Controlled doc is saved
    * When  : Expiration date is set less than effecive date
    * Then : Controlled doc is not saved due to validation rule 'Ensure_expiration_more_than_effective'
    */
    public testmethod static void givenUser_WhenControlledSaved_DateValidationEnsuredProperly(){

        if(!runAllTests && !run_givenUser_WhenControlledSaved_DateValidationEnsuredProperly){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){ 
            //Arrange : save a controlled document
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            //Act1 : Set Expiration date greater than effecive date
            a1.doc.Expiration_Date__c = Date.Today()+3;
            a1.doc.Effective_Date__c = Date.Today()+1;

            Database.SaveResult expDateMoreThanEffdate = Database.Update(a1.doc, false);

            //Assert1 : Expected controlled document to be saved
            System.assertEquals(true, expDateMoreThanEffdate.isSuccess(), 'Expected save to be successful');


            //Act1 : Set Expiration date equals than effecive date
            a1.doc.Expiration_Date__c = Date.Today()+3;
            a1.doc.Effective_Date__c = Date.Today()+3;

            Database.SaveResult expDatEqualsThanEffdate = Database.Update(a1.doc, false);

            //Assert1 : Expected controlled document to be saved
            System.assertEquals(true, expDatEqualsThanEffdate.isSuccess(), 'Expected save to be successful');

            //Act1 : Set Expiration date less than effecive date
            a1.doc.Expiration_Date__c = Date.Today()+3;
            a1.doc.Effective_Date__c = Date.Today()+5;

            Database.SaveResult expDatelessThanEffdate = Database.Update(a1.doc, false);

            //Assert1 : Expected controlled document to be saved
            System.assertEquals(false, expDatelessThanEffdate.isSuccess(), SQX_Utilities.getFormattedErrorMessage(expDatelessThanEffdate.getErrors()));
            System.assert(SQX_Utilities.getFormattedErrorMessage(expDatelessThanEffdate.getErrors()).contains(ENSURE_EXPIRATION_MORE_THAN_EFFECTIVE_ERR_MSG), 
                'Expected error message to include '+ ENSURE_EXPIRATION_MORE_THAN_EFFECTIVE_ERR_MSG +' but got following messages :' + SQX_Utilities.getFormattedErrorMessage(expDatelessThanEffdate.getErrors()));


        }
    }
    
    /**
    * Given : Controlled doc is saved
    * When  : Effective date is  set greater than issued date
    * Then : Controlled doc is saved without any error
    *    
    * Given : Controlled doc is saved
    * When  : Effective date is equal to issued date
    * Then  : Controlled doc is saved without any error
    *
    * Given : Controlled doc is saved
    * When  : Effective date is less than issued date
    * Then  : Controlled doc is not saved
    */
    public testmethod static void givenADoc_WhenEffectiveAndIssuedDatesAreChanged_EnsureProperValidaionIsDone(){

        if(!runAllTests && !run_givenADoc_WhenEffectiveAndIssuedDatesAreChanged_EnsureProperValidaionIsDone){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){ 
            //Arrange : save a controlled document
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save();

            //Act 1 : Set effective date greater than issued date
            a1.doc.Date_Issued__c = Date.Today();
            a1.doc.Effective_Date__c = Date.Today()+1;

            Database.SaveResult result = Database.Update(a1.doc, false);

            //Assert 1 : Expected controlled document to be saved
            System.assertEquals(true, result.isSuccess(), 'Expected save to be successful');

            // Act 2 : Set effective date equal to issued date
            a1.doc.Date_Issued__c = Date.Today();
            a1.doc.Effective_Date__c = Date.Today();

            result = Database.Update(a1.doc, false);

            //Assert 2 : Expected controlled document to be saved
            System.assertEquals(true, result.isSuccess(), 'Expected save to be successful');

            // Act 3 : Set effective date lesser than  issued date
            a1.doc.Date_Issued__c = Date.Today()+1;
            a1.doc.Effective_Date__c = Date.Today();

            result = Database.Update(a1.doc, false);

            //Assert 3 : Expected controlled document to be saved
            System.assertEquals(false, result.isSuccess(), 'Record shouldnot be saved');
        }

    }


    /**
     * ACTION: Add a requirement in doc and release the document
     * EXPECTED: Requirement must be added and must be active when the doc is pre-released.
     * @date: 2016-02-03
     * @story: [SQX-1965]
    */
    public testMethod static void givenADoc_RequirementAreActive_WhenDocIsPreReleased(){
        if (!runAllTests && !run_givenADoc_RequirementAreActive_WhenDocIsPreReleased) {
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User testUser1 = allUsers.get('testUser1');

        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.doc.Effective_Date__c =  null;
            cDoc1.save();
            
            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND

            );
            Database.SaveResult result1 = Database.insert(Req1, false);
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for draft document.');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act: Release test controlled doc.
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            cDoc1.doc.Effective_Date__c =  Date.Today().addDays(10);
            cDoc1.save();

            Req1 = [SELECT Active__c FROM SQX_Requirement__c WHERE Id =: Req1.Id];

            // Assert: Expected the requirement to be active
            System.assertEquals(true, Req1.Active__c, 'Expected the requirement to be active but found: ' + Req1.Active__c);
            
        }
    }

    /**
     * ACTION: Add a requirement as active in doc when the doc status is pre-release
     * EXPECTED: Requirement must be saved as active.
     * @date: 2016-02-03
     * @story: [SQX-1965]
    */
    public testMethod static void givenADoc_RequirementCanBeAdded_WhenDocIsInPreRelease(){
        if (!runAllTests && !run_givenADoc_RequirementCanBeAdded_WhenDocIsInPreRelease) {
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND

            );
            //Act : Insert the requirement
            Database.SaveResult result1 = Database.insert(Req1, false);

            //Assert: Expected the requirement to be inserted
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for pre-release document.');
            
        }
    }

    /**
     * ACTION: Add a requirement in doc and release the document
     * EXPECTED: Document Training must be added.
     * @date: 2016-02-03
     * @story: [SQX-1965]
    */
    public testMethod static void givenADoc_DocumentTrainingIsAdded_WhenDocIsPreReleased(){
        if (!runAllTests && !run_givenADoc_DocumentTrainingIsAdded_WhenDocIsPreReleased) {
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.doc.Effective_Date__c =  null;
            cDoc1.save();
            
             // creating required job function
            SQX_Job_Function__c jf1 = new SQX_Job_Function__c( Name = 'job function for test' );
            SQX_Test_Job_Function jf = new SQX_Test_Job_Function(jf1);
            Database.SaveResult jf1Result = jf.save();

            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();
            personnel.save();

            // creating required Personnel Job Function
            SQX_Personnel_Job_Function__c pjf1 = new SQX_Personnel_Job_Function__c(
                SQX_Personnel__c = personnel.mainRecord.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = true
            );

            Database.SaveResult saveResult1 = Database.insert(pjf1, false);

            SQX_Requirement__c Req1 = new SQX_Requirement__c(
                SQX_Controlled_Document__c = cDoc1.doc.Id,
                SQX_Job_Function__c = jf1.Id,
                Active__c = false,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND

            );
            Database.SaveResult result1 = Database.insert(Req1, false);
            System.assert(result1.isSuccess() == true,
                'Expected Requirement to be saved for draft document.');
            
            //clear for trigger handler
            SQX_BulkifiedBase.clearAllProcessedEntities();

            //Act: released test controlled doc.
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE);
            cDoc1.doc.Effective_Date__c =  Date.Today().addDays(10);
            cDoc1.save();

            List<SQX_Personnel_Document_Training__c> docTraining = [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE SQX_Controlled_Document__c =:  cDoc1.doc.Id];

            // Assert: Expected one document training to be inserted
            System.assertEquals(1, docTraining.size() , 'Expected one doc training to be found but found: ' +  docTraining.size());
            
        }
    }
    /**
     * ACTION: Add the requirement in the document, and pre-release the document
     * EXPECTED: Document Training must be added when the document is pre-released.
     * @date: 2016-02-03
     * @story: [SQX-1965]
    */
    public testMethod static void givenADoc_DocumentTrainingCanBeAdded_WhenDocIsPreReleased(){
        if (!runAllTests && !run_givenADoc_DocumentTrainingCanBeAdded_WhenDocIsPreReleased) {
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User testUser1 = allUsers.get('testUser1');

        System.runas(testUser1) {

            // add required controlled document
            SQX_Test_Controlled_Document testDoc = new SQX_Test_Controlled_Document();
            testDoc.doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
            testDoc.save();
            testDoc.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            
            // add required personnel record
            SQX_Test_Personnel personnel = new SQX_Test_Personnel();

            personnel.save();

            SQX_Personnel_Document_Training__c dt = new SQX_Personnel_Document_Training__c(
                SQX_Trainer__c = standardUser.Id,
                SQX_Controlled_Document__c = testDoc.doc.Id,
                SQX_Personnel__c = personnel.mainRecord.Id,
                Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = Date.today().addDays(15)
            );
            
            // ACT: add document training for Controlled document
            Database.SaveResult saveResult = Database.insert(dt, false);

            // Assert: Expected the document training to be inserted
            System.assertEquals(true, saveResult.isSuccess(), 'Expected the document training to be inserted but found errors: ' + saveResult.getErrors());
        }
    }

    /**
     * Scenario 1:
     * ACTION: Controlled Doc is approved and pre-release and effective date is today
     * EXPECTED: Doc Status to be current
     *
     * @date: 2016-02-05
     * @story: [SQX-1965]
    */
    public testMethod static void givenADoc_DocIsApprovedAndCurrentAndEffectiveDateIsToday_DocIsCurrent(){
        if (!runAllTests && !run_givenADoc_DocIsApprovedAndCurrentAndEffectiveDateIsToday_DocIsCurrent) {
            return;
        }
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');

        System.runas(standardUser) {
            // Arrange: Create a controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();

            SQX_Controlled_Document__c controlledDoc = [SELECT Id, 
                                                               Document_Status__c,
                                                               Approval_Status__c, 
                                                               Effective_Date__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];

            // Act 2: Set approval status to approved and document status to current
            controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            controlledDoc.Approval_Status__c = SQX_Controlled_Document.STATUS_APPROVED;


            Database.SaveResult docStatus =  Database.update(controlledDoc, false);

            controlledDoc = [SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            // Assert 1: Expected doc status to be current
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, controlledDoc.Document_Status__c, 'Expected the doucment status to be current but found: ' + controlledDoc.Document_Status__c);

        }
    }

    /**
     * SETUP: Create a controlled doc
     * ACTION: Release the doc with user who have no access to the record
     * EXPECTED: Error is given and save is not successful
     * @date: 2016-05-26
     * @story: [SQX-2161]
    */
    public testMethod static void givenADoc_WhenUserWithNoAccessRelasesDoc_ItGivesError(){
        if (!runAllTests && !run_givenADoc_WhenUserWithNoAccessRelasesDoc_ItGivesError) {
            return;
        }
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User testUser1 = allUsers.get('testUser1');

        SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
        System.runas(standardUser) {
            // Arrange: creating required draft controlled doc
            cDoc1.save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
        }

        System.runas(testUser1) {
            
            SQX_Controlled_Document__c doc = [SELECT Id, Document_Status__c FROM SQX_Controlled_Document__c WHERE Id =: cDoc1.doc.Id];
            doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;

            //SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);
            extension.Release();

            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            //System.assert(!result.isSuccess(), 'Expected the save not to be successful');
            Boolean correctErrMsg = false;
            for(Apexpages.Message msg : pageMessages){
                if(msg.getDetail() == Label.SQX_ERR_MSG_INSUFFICIENT_ACCESS_ON_DOC){
                    correctErrMsg =  true;
                    break;
                }
            }

            System.assert(correctErrMsg, 'Expected to get correct error msg');
        }
    }
    
    /**
    * Given : Controlled doc is saved
    * When  : Doc is released using release button
    * Then : Batch job status is cleared
    */
    public testmethod static void givenADoc_WhenDocIsReleasedUsingReleaseButton_BatchJobStatusIsCleared(){

        if(!runAllTests && !run_givenADoc_WhenDocIsReleasedUsingReleaseButton_BatchJobStatusIsCleared){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){ 

            // custom setting to define template id
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<Schema.SObjectField, Object> {
                Schema.SQX_Custom_Settings_Public__c.Use_Batch_For_Document_Status_Change__c => true
            });

            //Arrange : save a controlled document
            SQX_Test_Controlled_Document a1 = new SQX_Test_Controlled_Document().save();
            a1.setStatus(SQX_Controlled_Document.STATUS_PRE_RELEASE).save();
            SQX_Controlled_Document__c cDoc = [SELECT Batch_Job_Status__c FROM SQX_Controlled_Document__c WHERE Id =: a1.doc.Id];
            System.assertEquals(SQX_Controlled_Document.BATCH_RELEASE_QUEUED, cDoc.Batch_Job_Status__c);

            a1.doc.Effective_Date__c = Date.Today().addDays(1);
            Database.SaveResult result = Database.Update(a1.doc, false);

            // Act 1 : Release the controlled doc using release button
            ApexPages.StandardController controller = new ApexPages.StandardController(a1.doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);
            extension.Release();

            cDoc = [SELECT Batch_Job_Status__c FROM SQX_Controlled_Document__c WHERE Id =: a1.doc.Id];

            //Assert 1 : Expected controlled document to be saved
            System.assertEquals(null, cDoc.Batch_Job_Status__c, 'Expected the batch job status to be blank');

        }

    }
    
}