/**
*   @description : Helper class for generating SObject instance primarily to bypass constraints when creating mock SObjects
*   @author : Piyush Subedi
*   @date : 03-05-2017
*/
@isTest
public class SQX_SObject_Generator{


    private static final String JSON_KEY_TOTALSIZE = 'totalSize',
                                JSON_KEY_DONE = 'done',
                                JSON_KEY_RECORDS = 'records';


    /* Holds the sobject type of the record associated with the instance of this class */ 
    private SObjectType sType;

    /* Map equivalent of the record (introduced for ease of insertion/removal/update of fields and values) */
    private Map<String, Object> recordInMap;


    /*
    *   @param record - SObject instance
    */
    public SQX_SObject_Generator(SObject record){

        this(record, true);
    }


    /*
    *   @param record - SObject instance
    *   @param setId - flag to check if (fake) Id is to be set to the record
    */
    public SQX_SObject_Generator(SObject record, Boolean setId){

        this.recordInMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(record));

        this.sType = record.getSObjectType();

        if(setId){
            this.setRecordId();
        }
    }



    /**
    *   Returns an SObject instance from the record map
    */
    public SObject getSObject(){

        /* Determine class type from SObjectType */
        Type cType = Type.forName(this.sType.getDescribe().getName());

        return (SObject) JSON.deserialize(JSON.serialize(this.recordInMap), cType);
    }


    /**
    *   Method to set fake id to the associated sObject instance
    */
    public void setRecordId(){
        this.recordInMap.put('Id', getFakeId(this.sType));
    }


    /**
    *   Method adds child record to the parent record
    *   @param relationshipName - api name of the relationship
    *   @param cRecords - list of child record
    */
    public SQX_SObject_Generator addChildRecord(SObjectField relationshipName, SObject cRecord){
        return this.addChildRecords(relationshipName, new List<SObject> { cRecord });
    }


    /**
    *   Method adds child records to the parent record
    *   @param relationshipField - SObjectfield type of relationship field
    *   @param cRecords - list of child records
    */
    public SQX_SObject_Generator addChildRecords(SObjectField relationshipField, List<SObject> cRecords){

        String relationshipName = getRelationshipNameFromField(relationshipField);

        /* Form child record structure */
        Map<String, Object> records = new Map<String, Object>{
            JSON_KEY_TOTALSIZE => cRecords.size(),
            JSON_KEY_DONE => true,
            JSON_KEY_RECORDS => cRecords
        };

        this.recordInMap.put(relationshipName, records);

        return this;
    }


    /**
    *   Method returns the relationship name(api name) of the given field
    *   Returns null if the field is not a valid relationship field
    *   @param relationshipField - sobjectfield type of relationship field
    */
    private String getRelationshipNameFromField(SObjectField relationshipField){

        String relationshipName = null;

        List<Schema.ChildRelationship> childRelationships = this.sType.getDescribe().getChildRelationships();
        for(Schema.ChildRelationship childRelationship : childRelationships) {
            if(childRelationship.getField() == relationshipField) {
                relationshipName = childRelationship.getRelationshipName();
                break;
            }
        }


        if(String.isBlank(relationshipName)){
            System.assert(false, 'Unknown relationship field ' + relationshipField );
        }


        return relationshipName;
    }


    /**
    *   Method sets the value for the given field
    *   Useful in writing values to otherwise non-writable fields
    *   @param fieldName - name of the field onto which the value is to be inserted
    *   @param value - value to be inserted into the given field
    */
    public SQX_SObject_Generator setValueForField(String fieldName, Object value){

        this.recordInMap.put(fieldName, value);

        return this;
    }


    ////////////////////// HELPER METHODS START ///////////////////

    /**
    * Count for the object type. Note: That a single counter is applied to all object types
    */
    static Integer count = 1;

    /**
     * Useful for generating fake ID for objects without actually saving it
     * Refer to : http://salesforce.stackexchange.com/a/21297
     *            https://foobarforce.com/2013/11/25/df13return/
     */
    public static String getFakeId(SObjectType objType) {
        String result = String.valueOf(count++);

        return objType.getDescribe().getKeyPrefix() +
            '0'.repeat(12-result.length()) + result;
    }

    ////////////////////// HELPER METHODS END ///////////////////

}