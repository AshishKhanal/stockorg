/**
* This class contains the trigger handler for Requirement
*/
public with sharing class SQX_Requirement {
    
    public static String LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND = 'Read and Understand',
                         LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY = 'Read and Exhibit Competency',
                         LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT = 'Instructor Led with Assessment';
                         
    public static Set<String> COMPETENCY_NEEDING_TRAINER_APPROVAL = new Set<String>{
        LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
        LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT
    };
    
    public static String TRAINING_JOB_STATUS_ACTIVATION_PENDING     = 'Activation Pending',
                         TRAINING_JOB_STATUS_ACTIVATION_ERROR       = 'Activation Error',
                         TRAINING_JOB_STATUS_DEACTIVATION_PENDING   = 'Deactivation Pending',
                         TRAINING_JOB_STATUS_DEACTIVATION_ERROR     = 'Deactivation Error';
    
    public static SQX_Custom_Settings_Public__c cqSetting = null;
    
    /**
    * returns use batch job custom setting to process training when requirement is activated or deactivated
    */
    public static Boolean useBatchJobToProcessTraining() {
        if (cqSetting == null) {
            cqSetting = SQX_Custom_Settings_Public__c.getInstance();
        }
        
        return cqSetting.Use_Batch_Job_To_Process_REQ_Training__c == true;
    }
    
    /**
    * This class is used for ranking or mapping level of competency to an integer value.
    * It is used for comparing stricter proficiency between different levels of competency.
    */
    public with sharing class SQX_LevelOfCompetencyMapper
    {
        /**
        * Ranked different levels of competency as following
        * - Read and Understand            => 1 (lowest poficiency)
        * - Read and Exhibit Competency    => 2
        * - Instructor Led with Assessment => 3 (highest poficiency)
        */
        private Map<String, Integer> levelMap = new Map<String, Integer> {
            SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND => 1,
            SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY => 2,
            SQX_Requirement.LEVEL_OF_COMPETENCY_INSTRUCTOR_LED_WITH_ASSESSMENT => 3
        };
        
        /**
        * It returns the integer value or rank assigned to a level of competency.
        */
        public Integer getValue(String levelOfCompetency) {
            return levelMap.get(levelOfCompetency);
        }
        
        /**
        * returns the integer value when compared by rank between two level of competencies
        * @return -1 if rank of levelOfCompetency1 < rank of levelOfCompetency2,
        *          0 if rank of levelOfCompetency1 = rank of levelOfCompetency2,
        *          1 if rank of levelOfCompetency1 > rank of levelOfCompetency2
        */
        public Integer compare(String levelOfCompetency1, String levelOfCompetency2) {
            Integer value1 = getValue(levelOfCompetency1),
                value2 = getValue(levelOfCompetency2);
            
            if (value1 == value2)
                return 0;
            else if (value1 > value2)
                return 1;
            else
                return -1;
        }
    }
    
    /**
    * Requirement Trigger Handler
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase {

        public Bulkified() {
        }
        
        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newReq equivalent to trigger.new in salesforce
        * @param oldMap equivalent to trigger.oldMap in salesforce
        */
        public Bulkified(List<SQX_Requirement__c> newReq, Map<Id, SQX_Requirement__c> oldMap) {
            super(newReq, oldMap);
        }
        
        /**
        * This method prevents requirement creation on document with no edit permission.
        */
        public Bulkified preventCreationOnDocumentWithoutEditPermission() {
            this.resetView();
            
            List<Id> docIds = new List<Id>();
            for (SQX_Requirement__c req: (List<SQX_Requirement__c>)this.view) {
                docIds.add(req.SQX_Controlled_Document__c);
            }
            
            Map<Id, UserRecordAccess> access = new Map<Id, UserRecordAccess>([SELECT RecordId FROM UserRecordAccess
                                                                              WHERE UserId = :UserInfo.getUserId()
                                                                              AND RecordId IN :docIds
                                                                              AND HasEditAccess = true]);
            
            for (SQX_Requirement__c req: (List<SQX_Requirement__c>)this.view) {
                if (access.get(req.SQX_Controlled_Document__c) == null) {
                    req.addError(Label.SQX_ERR_MSG_CANNOT_ADD_REQUIREMENT_FOR_DOCUMENT_WITH_INSUFFICIENT_PRIVILEGES);
                }
            }

            return this;
        }
        
        /**
        * This method prevents deletion on once activated Requirement.
        */
        public Bulkified preventDeletionOnOnceActivatedRecord() {
            this.resetView();
            
            for (SQX_Requirement__c req: (List<SQX_Requirement__c>)this.view) {
                if (req.Activation_Date__c != null) {
                    req.addError(Label.SQX_ERR_MSG_CANNOT_DELETE_ONCE_ACTIVATED_REQUIREMENT);
                }
            }

            return this;
        }
        
        /**
        * This method generates/re-evaluates pending personnel document trainings when a requirement is activated/deactivated
        */
        private Bulkified createUpdatePersonnelDocumentTrainingWhenRequirementIsActivatedOrDeactivated(String actionName, Boolean activeValue) {
            //Remove all the previously processed records
            this.resetView().removeProcessedRecordsFor(SQX.Requirement, actionName);

            // find all activated/deactivated requirements
            List<SQX_Requirement__c> reqsToProcess = new List<SQX_Requirement__c>();
            
            for (SQX_Requirement__c req : (List<SQX_Requirement__c>)this.view) {
                // process requirements that are not processed using batch job
                if (req.Training_Job_Status__c == null && req.Active__c == activeValue) {
                    SQX_Requirement__c reqOld = (SQX_Requirement__c)oldValues.get(req.Id);
                    
                    if (reqOld == null) {
                        if (activeValue == true) {
                            // list newly activated requirements
                            reqsToProcess.add(req);
                        }
                    }
                    else if (req.Active__c != reqOld.Active__c) {
                        // list requirements with changed active value
                        reqsToProcess.add(req);
                    }
                }
            }

            if (!reqsToProcess.isEmpty()) {
                this.addToProcessedRecordsFor(SQX.Requirement, actionName, reqsToProcess);
                
                if (activeValue) {
                    SQX_Personnel_Document_Job_Function.processActivatedRequirements(reqsToProcess);
                }
                else {
                    SQX_Personnel_Document_Job_Function.processDeactivatedRequirements(reqsToProcess);
                }
            }

            return this;
        }
        
        /**
        * This method generates/re-evaluates personnel document trainings when a requirement is activated
        */
        public Bulkified createOrUpdatePersonnelDocumentTrainingWhenActivated() {
            final String ACTION_NAME = 'createOrUpdatePersonnelDocumentTrainingWhenActivated';
            
            return createUpdatePersonnelDocumentTrainingWhenRequirementIsActivatedOrDeactivated(ACTION_NAME, true);
        }
        
        /**
        * This method re-evaluates personnel document trainings when a requirement is deactivated
        */
        public Bulkified updatePersonnelDocumentTrainingWhenDeactivated() {
            final String ACTION_NAME = 'updatePersonnelDocumentTrainingWhenDeactivated';
            
            return createUpdatePersonnelDocumentTrainingWhenRequirementIsActivatedOrDeactivated(ACTION_NAME, false);
        }
        
        /**
        * sets pending training batch job status when requirement is activated or deactivated and batch job is enabled
        */
        public Bulkified queueTrainingBatchJob() {
            if (useBatchJobToProcessTraining() == false)
                return this;
            
            this.resetView();
            
            for (SQX_Requirement__c req : (List<SQX_Requirement__c>)this.view) {
                Boolean queueTrainingJob = false;SQX_Requirement__c oldReq;
                
                if (req.Id == null) {
                    if (req.Active__c == true) {
                        // queue new active requirements
                        queueTrainingJob = true;
                    }
                }
                else {
                     oldReq = (SQX_Requirement__c)oldValues.get(req.Id);
                    if (req.Active__c != oldReq.Active__c) {
                        // queue when active value changes
                        queueTrainingJob = true;
                    }
                }
                
                if (queueTrainingJob) {
                    req.Training_Job_Status__c = req.Active__c == true ? TRAINING_JOB_STATUS_ACTIVATION_PENDING : TRAINING_JOB_STATUS_DEACTIVATION_PENDING;
                    req.Training_Job_Last_Processed_Record__c = null;
                    req.Training_Job_Error__c = null;
                }
            }
            
            return this;
        }
    }
}