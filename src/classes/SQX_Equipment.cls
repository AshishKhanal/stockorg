/**
* This class will contain the common methods and static strings required for the Equipment object 
*/
public with sharing class SQX_Equipment {

    public static final String  STATUS_NEW = 'New',
                                STATUS_ACTIVE = 'Active',
                                STATUS_SUSPENDED = 'Suspended',
                                STATUS_RETIRED = 'Retired',
                                STATUS_LOST = 'Lost',
                                
                                // Equipment types
                                TYPE_ANALOG_GAUGE = 'Analog Gauge',
                                TYPE_DIGITAL_GAUGE = 'Digital Gauge',
                                TYPE_MACHINE = 'Machine',
                                TYPE_MEASUREMENT_INSTRUMENT = 'Measurement Instrument',
                                TYPE_STANDARD = 'Standard',
                                
                                //Owner Type
                                OWNER_TYPE_COMPANY_OWNED = 'Company Owned',
                                OWNER_TYPE_CUSTOMER_OWNED = 'Customer Owned',
                                OWNER_TYPE_SUPPLIER_CONSIGNED = 'Supplier Consigned',
                                
                                //Calibration Source
                                SOURCE_INTERNAL = 'Internal',
                                SOURCE_EXTERNAL = 'External';

    public static final String PurposeOfSignature_Status_Change  = 'statuschange',
                               PurposeOfSignature_Date_Change = 'datechange';
    
    /**
     * map of activity code to purpose of signature label
     */
    public static final Map<String, String> purposeOfSigMap = new Map<String, String>{
        PurposeOfSignature_Status_Change => Label.SQX_PS_Equipment_Changing_Equipment_Status,
        PurposeOfSignature_Date_Change => Label.SQX_PS_Equipment_Changing_Calibration_Date
    };
    
    /**
    * Returns change list information of a modified equipment record for record activity
    */
    public static List<String> getChangeList(SQX_Equipment__c original, SQX_Equipment__c modified, List<Schema.SObjectField> fields) {
        List<String> changelist = new List<String>();
        
        if (original == null || modified == null) {
            // throw if any original and modified objects are null
            throw new SQX_ApplicationGenericException('Original and/or modified equipment value not found to generate change list.');
        }
        
        if (fields == null) {
            // null value is passed in fields then compare all fields of the Equipment object
            fields = new List<Schema.SObjectField>();
            Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.SQX_Equipment__c.fields.getMap();
            for (String f : fieldsMap.keySet()) {
                fields.add(fieldsMap.get(f));
            }
        }
        
        if (fields.size() == 0) {
            // throw if no fields are found to compare 
            throw new SQX_ApplicationGenericException('No equipment fields found to generate change list.');
        }
        
        for (Schema.SObjectField f : fields) {
            if (modified.get(f) != original.get(f)) {
                DescribeFieldResult dfr = f.getDescribe();
                String originalValue = '';
                String modifiedValue = '';
                
                if (dfr.getType() == Schema.DisplayType.Date) {
                    // format original and modified date values of same field
                    Date d = null;
                    DateTime dt = null;
                    
                    // format original value
                    originalValue = 'null'; // set null as default
                    if (original.get(f) != null) {
                        // format original value when found
                        d = (Date)original.get(f);
                        dt = DateTime.newInstance(d.year(), d.month(),d.day());
                        originalValue = dt.format('d-MMM-yyyy');
                    }
                    
                    // format modified value
                    modifiedValue = 'null'; // set null as default
                    if (modified.get(f) != null) {
                        // format modified value when found
                        d = (Date)modified.get(f);
                        dt = DateTime.newInstance(d.year(), d.month(),d.day());
                        modifiedValue = dt.format('d-MMM-yyyy');
                    }
                } else {
                    // convert original and modified values of same field to String
                    originalValue = '' + original.get(f);
                    modifiedValue = '' + modified.get(f);
                }
                
                // add changes with formatted text
                changelist.add(String.format('{0} changed from {1} to {2}.', new List<String>{ dfr.getLabel(), originalValue, modifiedValue }));
            }
        }
        
        return changelist;
    }
}