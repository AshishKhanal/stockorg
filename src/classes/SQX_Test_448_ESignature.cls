@IsTest
public class SQX_Test_448_ESignature{

    /* test run support */
    static boolean  runAllTests = true,
                    givenACapaResponse_CorrectPassword_PublishesResponse = false;

    
                   

    /**
    * ensures that a given a CAPA response is sent to multiple approval, when both approve the inclusion is approved
    */
    public testmethod static void checkPasswordVerification(){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SQX_Test_OauthEsignatureValidation.MockAuthClass());
            
            //send right password for current user, but wrong username, should return false
            String esignatureDataRightPasswordWrongUsername= '{"enabled":true,"username":"anyusername","password":"rightPassword"}';
            boolean verified= SQX_Extension_UI.checkForElectronicSignature(esignatureDataRightPasswordWrongUsername);
            System.assertEquals(false, verified);

            //send right password for current user, and current username, should return true
            String esignatureDataRightPasswordRightUsername= '{"enabled":true,"username":"'+UserInfo.getUserName()+'","password":"rightPassword"}';
            verified= SQX_Extension_UI.checkForElectronicSignature(esignatureDataRightPasswordRightUsername);
            System.assertEquals(true, verified);

            //send wrong password, should return false
            String esignatureDataWrongPassword= '{"enabled":true,"username":"anyusername","password":"anyPassword"}';
            verified= SQX_Extension_UI.checkForElectronicSignature(esignatureDataWrongPassword);
            System.assertEquals(false, verified);
            Test.stopTest();
            //send esignature disabled, with wrong password, should return true
            //String esignatureDataNotEnabled= '{"enabled":false,"username":"anyusername","password":"anyPassword"}';
            //verified= SQX_Extension_UI.checkForElectronicSignature(esignatureDataNotEnabled);
            //System.assertEquals(true, verified);

    }   
    
}
