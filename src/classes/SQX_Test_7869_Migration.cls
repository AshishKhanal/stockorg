/**
 * This migration test case for to update suplier management steps old records(update newly created contentId in steps contentId field).
 */
@isTest
public class SQX_Test_7869_Migration {

    /**
     * Create nsi and supplier deviation with various combinations as required for the migration. Please note that content document links
     * are added manually because the system is already at 8.2.0 level and won't create such data
     */
    @testSetup
    public static void commonSetup() {

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
                    SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> {adminUser});
        System.runAs(adminUser){

            // Create two controlled document that is to be referred by all the steps
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document()
                                                        .updateContent(true, 'Doc-1-Primary.txt');
            cDoc1.save();
            cDoc1.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document()
                                                        .updateContent(true, 'Doc-2-Primary.txt');
            cDoc2.save();
            cDoc2.setStatus(SQX_Controlled_Document.STATUS_CURRENT).save();

            //Create NSI Record
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            SQX_Onboarding_Step__c obStep1 = new SQX_Onboarding_Step__c();
            obStep1.SQX_Parent__c =nsi.nsi.Id;
            obStep1.Name = 'NSITask';
            obStep1.Status__c = SQX_NSI.STATUS_DRAFT;
            obStep1.Step__c = 1;
            obStep1.SQX_User__c = adminUser.Id;
            obStep1.Document_Name__c='Sample Document Name';
            obStep1.SQX_Controlled_Document__c = cDoc1.doc.Id;
            obStep1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.OnboardingStep, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);

            SQX_Onboarding_Step__c obStep3 = obStep1.clone();

            SQX_Onboarding_Step__c obStep2 = obStep1.clone();
            obStep2.Step__c = 2;
            new SQX_DB().op_insert(new List<SQX_Onboarding_Step__c> { obStep1, obStep2, obStep3 },  new List<Schema.SObjectField>{});
            nsi.submit();
            nsi.initiate();

            update new SQX_Onboarding_Step__c[] {
                new SQX_Onboarding_Step__c(Id = obStep2.Id, ContentDocId__c = null)
            };

            insert new ContentDocumentLink[] {
                new ContentDocumentLink(LinkedEntityId = obStep2.Id, ContentDocumentId = cDoc1.doc.Content_Reference__c, ShareType = 'V'),
                new ContentDocumentLink(LinkedEntityId = obStep1.Id, ContentDocumentId = cDoc1.doc.Content_Reference__c, ShareType = 'V'),
                new ContentDocumentLink(LinkedEntityId = obStep3.Id, ContentDocumentId = cDoc1.doc.Content_Reference__c, ShareType = 'V')
            };

            update new SQX_Onboarding_Step__c(Id = obStep3.Id, Result__c = SQX_Steps_Trigger_Handler.RESULT_GO, Status__c = SQX_NSI.STATUS_COMPLETE);
            Test.startTest();
            //Create SD Record
            SQX_Test_Supplier_Deviation sd= new SQX_Test_Supplier_Deviation().save();
            SQX_Deviation_Process__c dpStep1 = new SQX_Deviation_Process__c();
            dpStep1.SQX_Parent__c =sd.sd.Id;
            dpStep1.Name = 'SDTask';
            dpStep1.Status__c = SQX_Supplier_Deviation.STATUS_DRAFT;
            dpStep1.Step__c = 1;
            dpStep1.SQX_User__c = adminUser.Id;
            dpStep1.Document_Name__c='Sample Document Name';
            dpStep1.SQX_Controlled_Document__c = cDoc2.doc.Id;
            dpStep1.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.DeviationProcess, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);

            SQX_Deviation_Process__c dpStep2 = dpStep1.clone();
            dpStep2.Step__c = 2;
            new SQX_DB().op_insert(new List<SQX_Deviation_Process__c> { dpStep1, dpStep2 },  new List<Schema.SObjectField>{});
            sd.submit();
            sd.initiate();
            Test.stopTest();
        }
    }

    /**
     * GIVEN: given old supplier management steps records
     * WHEN: migration run
     * THEN: update new content id in steps
     */
    static testmethod void givenOldStepsOfSupplierManagementNSI_WhenMigrate_ThenUpdateContentDocId(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Act: Migration run
            Test.startTest();
            SQX_8_2_Update_ContentDocId_Migration job = new SQX_8_2_Update_ContentDocId_Migration();
            Id batchProcessId = Database.executeBatch(job);
            Test.stopTest();

            //Assert: Ensured that newly created content id updated in steps
            SQX_Onboarding_Step__c nsiStep = [SELECT Id, ContentDocumentId__c, ContentDocId__c FROM SQX_Onboarding_Step__c WHERE Step__c = 2];
            system.assertNotEquals(null, nsiStep.ContentDocId__c);
            System.assertEquals(0, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = : nsiStep.Id AND ContentDocumentId = : nsiStep.ContentDocumentId__c].size());
        }
    }

    /**
     * GIVEN: given old supplier management steps records
     * WHEN: migration run
     * THEN: update new content id in steps
     */
    static testmethod void givenOldStepsOfSupplierManagementSD_WhenMigrate_ThenUpdateContentDocId(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Act: Migration run
            Test.startTest();
            SQX_8_2_Update_ContentDocId_Migration job = new SQX_8_2_Update_ContentDocId_Migration(1);
            Id batchProcessId = Database.executeBatch(job);
            Test.stopTest();

            //Assert: Ensured that newly created content id updated in steps
            SQX_Deviation_Process__c dpStep = [SELECT Id, ContentDocumentId__c, ContentDocId__c FROM SQX_Deviation_Process__c WHERE Step__c = 2];
            system.assertNotEquals(null, dpStep.ContentDocId__c);
            System.assertEquals(0, [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = : dpStep.Id AND ContentDocumentId = : dpStep.ContentDocumentId__c].size());
        }
    }
}