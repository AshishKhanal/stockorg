/**
* This class is used to return all the open Actions for which the user is NC CoordinatorActions which are open for the currently logged in user's account .
*/
public with sharing class SQX_Controller_OpenActions{
    
    /**
    * returns the list of Actions which are open for the currently logged in user's account 
    */
    public List<SQX_Action__c> getActionsForCurrentUser() {
            
            return [

            SELECT Id, Name, Plan_Number__c, SQX_CAPA__c,SQX_CAPA__r.Name, SQX_Audit__c, 
                    SQX_Audit__r.Name, Plan_Type__c, Description__c , Due_Date__c, Overdue_By__c
                    FROM SQX_Action__c
                    WHERE SQX_User__c=:UserInfo.getUserId() AND 
                    Status__c = :SQX_Implementation_Action.STATUS_OPEN
                    ORDER BY Name DESC];
        
    }

}