/**
* This test ensures that various functionalities developed for Assessment are done properly
*
*/
@IsTest
public class SQX_Test_Assessment {
    public SQX_Assessment__c assessment;//assessment that is created
    static final String ERR_MSG_QUESTION_CANNOT_BE_ALTERED = 'Assessment Questions cannot be added or modified because referred assessment is locked.',
                        ERR_MSG_TOTAL_QUESTION_SHOULD_BE_LESS = 'Total Questions To Ask should be less or equal to Total Questions.';
    static final String ACCESSLEVEL_READ = 'Read';

    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
    }

    public SQX_Test_Assessment(String recordType){
        this();

        assessment.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.Assessment, recordType);
    }

    public SQX_Test_Assessment(SQX_Assessment__c assessment){
        this.assessment = assessment;
    }
    
    public SQX_Test_Assessment(){
        Integer randomNumber = (Integer)( Math.random() * 1000000 );
        this.assessment = new SQX_Assessment__c();
        assessment.Name = 'assessment'+randomNumber;
        assessment.Total_Questions_To_Ask__c = 0;
        assessment.Passing_Percentage__c = 0.00;
        assessment.Status__c = SQX_Assessment.STATUS_DRAFT;
    }

    /**
     *	Method sets random content to assessment
    */
    public SQX_Test_Assessment setContent(ContentVersion cv){
        assessment.SCORM_Content_Reference__c = cv.ContentDocumentId;
        return this;
    }

    /**
     *	Method to set the status of the assessment
    */
    public SQX_Test_Assessment setStatus(String status){
        assessment.Status__c = status;
        return this;
    }

    /**
     *	Method to set the name of the assessment
    */
    public SQX_Test_Assessment setName(String name){
        assessment.Name = name;
        return this;
    }
    
    public SQX_Test_Assessment save(){

        // create new assessment using extension
        ApexPages.StandardController controller = new ApexPages.StandardController(this.assessment);
        SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
        PageReference pr = assmentExt.save();
        return this;
    }

    /**
    *	synchronize with the data store to update the memory object
    */
    public SQX_Test_Assessment synchronize(){
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.field = 'Id';
        filter.operator = 'eq';
        filter.value = this.assessment.Id;

        this.assessment = (SQX_Assessment__c)SQX_DynamicQuery.getAllFields(SQX_Assessment__c.SObjectType.getDescribe(), new Set<SObjectField>(), filter, new List<SObjectField>(), false).get(0);

        return this;
    }
    
    /**
    * Given : Create Assessment with required fields
    * When : Assessment is inserted
    * Then : Insertion is successfull 
    */
    public testmethod static void givenRequiredFieldsOfAssessment_WhenAssessmentIsInserted_InsertionIsSuccessfull(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            
            //Arrange : Create assessment
            SQX_Test_Assessment assessment = new SQX_Test_Assessment();
            
            //Act : save assessment
            assessment.save();
            
            //Assert : Assessment is saved successfully
            System.assert(assessment.assessment.Id != null,'Excpected assessment to be inserted');
        }
    }
    
    /**
    * Given : Create Assessment.
    * When : Assessment status is changed from draft to other status
    * Then : Status of Assessment can't be set back to draft
    */
    public testmethod static void givenAssessment_WhenStatusIsChangedFromDraftToOther_ItCanNotSetBackTDraft(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            
            //Arrange : Create and Save Assessment
            SQX_Test_Assessment assessment = new SQX_Test_Assessment();
            assessment.save();
            
            //Assert : Ensure that the status is draft
            System.assertEquals(SQX_Assessment.STATUS_DRAFT, assessment.assessment.Status__c, 'Excpected assessment status to be'+SQX_Assessment.STATUS_DRAFT+'but found'+assessment.assessment.Status__c);
           
            //Act : Change the assessment status to Pre-Release
            assessment.assessment.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;
            assessment.save();

            //Assert : Ensure the status is Pre-Release
            System.assertEquals(SQX_Assessment.STATUS_PRE_RELEASE, assessment.assessment.Status__c, 'Excpected assessment status to be'+SQX_Assessment.STATUS_PRE_RELEASE+'but found'+assessment.assessment.Status__c);
            
            //Act : Change the status back to draft
            assessment.assessment.Status__c = SQX_Assessment.STATUS_DRAFT;
            Database.SaveResult result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Assessment status cannot set back to draft from other statuses');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Assessment Status cannot set back to draft from other statuses.', result.getErrors().get(0).getMessage());
        }
    }
    
    /**
    * Given : Create Assessment
    * When : passing percentage,total questio to ask, days between retakes, number of retakes fields have negative number or passing percentage has value greater than 100
    * Then : Exception should occur
    */
    
    public testmethod static void givenAssessment_WhenNumberFieldsHaveNegativeNumber_ExcetptionShouldOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            
            //Arrange : Create Assessment
            SQX_Test_Assessment assessment = new SQX_Test_Assessment();
            assessment.save();
            
            //Act : set the value of passing percentage,total question to ask,Days between Retakes and Number of Retakes to negative number
            assessment.assessment.Passing_Percentage__c = -80;
            Database.SaveResult result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected Pasing percentage can not be less than zero');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Passing Percentage cannot have value less than zero and greater than 100.', result.getErrors().get(0).getMessage());
            
            //Act : Set value of passing percentage greater than 100
            assessment.assessment.Passing_Percentage__c = 101;
            result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected Pasing percentage can not be greater than 100');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Passing Percentage cannot have value less than zero and greater than 100.', result.getErrors().get(0).getMessage());
            
            //Act : Set value of Number of Retakes Allowed to negative number ie less than zero
            assessment = new SQX_Test_Assessment().save();
            assessment.assessment.Number_Of_Retakes_Allowed__c = -2;
            result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected Number of retakes allowed value to be non negative number');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Total Question To Ask, Days between Retakes and Number of Retakes Allowed cannot have value less than zero.', result.getErrors().get(0).getMessage());
            
            //Act : Set value of total question to ask to negative number ie less than zero
            assessment = new SQX_Test_Assessment().save();
            assessment.assessment.Total_Questions_To_Ask__c = -2;
            result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected total question to ask to be non negative number');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Total Question To Ask, Days between Retakes and Number of Retakes Allowed cannot have value less than zero.', result.getErrors().get(0).getMessage());
            
            //Act : Set value of Days between Retakes to less than zero
            assessment = new SQX_Test_Assessment().save();
            assessment.assessment.Days_between_Retake__c = -2;
            result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected total question to ask to be non negative number');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals('Total Question To Ask, Days between Retakes and Number of Retakes Allowed cannot have value less than zero.', result.getErrors().get(0).getMessage());
        }
    }
    
    /**
    * Given : Create Assessment
    * When : total questio to ask is greater than total question
    * Then : Exception should occur
    */
    
    public testmethod static void givenAssessment_WhenTotalQuestionToAskIsGreaterThanTotalQuestions_ExcetptionShouldOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            
            //Arrange : Create Assessment
            SQX_Test_Assessment assessment = new SQX_Test_Assessment();
            assessment.save();
            
            //Act : Set value of total question to ask field more than zero initially
            assessment.assessment.Total_Questions_To_Ask__c = 2;
            Database.SaveResult result = Database.update(assessment.assessment,false);

            //Assert : result is not success
            System.assert(!result.isSuccess(), 'Expected the total question to ask should be less or equal to total questions');

            //Assert : Ensure the error message is from validation rule
            System.assertEquals(ERR_MSG_TOTAL_QUESTION_SHOULD_BE_LESS, result.getErrors().get(0).getMessage());
        }
    }
    
    /**
    * Given : Create Assessment and add some questions
    * When : total questio to ask is greater than total question
    * Then : Exception should occur
    */
    
    public testmethod static void givenAssessmentWithQuestions_WhenTotalQuestionToAskIsGreaterThanTotalQuestions_ExcetptionShouldOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            Integer randomNumber = (Integer)( Math.random() * 1000000 );
            SQX_Assessment__c assessment1 = new SQX_Assessment__c();
            assessment1.Name = 'assessment'+randomNumber;
            assessment1.Total_Questions_To_Ask__c = 2;
            assessment1.Passing_Percentage__c = 0.00;
            assessment1.Status__c = SQX_Assessment.STATUS_DRAFT;

            //Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            //Add two questions
            assmentExt.addQuestion();
            assmentExt.addQuestion();
            
            //Create questions
            for(SQX_Assessment_Question__c q : assmentExt.questions){
                q.Question_Long__c = 'Sample Question';
                q.Answer_Options_Long__c = 'A)Yes\r\nB)No';
                q.Correct_Answer__c = 'A';
                q.Navigation__c = 'A=Next\r\nB=End';
            }
            
            //set the current page
            PageReference pageRef = Page.SQX_Assessment_Editor_Detail_Layout;
            Test.setCurrentPage(pageRef);
            
            //Act : Save the assessment and questions together using extension
            assmentExt.save();
            
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            //Assert : No page message should occur
            System.assert(pageMessages.size() == 0, 'Expected no page message error but is'+pageMessages);
            
            //Assert : Total_Questions_To_Ask__c should be 2
            System.assertEquals(2,assessment1.Total_Questions_To_Ask__c);
            
            //Remove one question
            Apexpages.currentPage().getparameters().put('index','1');
            assmentExt.removeQuestion();
            
            //Act : Save the assessment and questions together using extension
            assmentExt.save();
            
            pageMessages = ApexPages.getMessages();
            boolean result = SQX_Utilities.checkPageMessageContainingTexts(pageMessages,ERR_MSG_TOTAL_QUESTION_SHOULD_BE_LESS);
            System.assert(result,'Expected the Error message should match the page message error'+pageMessages);

            //Assert : the id of the assessment is not null
            System.assert(assessment1.Id != null, 'Expected the id of the assessment should not be null');
        }
    }

    /**
    * Given : Create Assessment and add some questions
    * When : Assessment is cloned
    * Then : Assessment and its all questions are copied to new cloned assessment
    * @Story : SQX-2992
    * @Author : Paras Kumar Bishwakarma
    */
    public testmethod static void givenAssessmentWithQuestions_WhenAssessmentIsCloned_AllQuestionsAreCopiedToNewCloneAssessment(){
        User standardUser1 = SQX_Test_Account_Factory.getUsers().get('standardUser1');
        Id assessmentIdBeforClone;
        SQX_Assessment__c assessment1;
        List<SQX_Assessment_Question__c> questionBeforeClone;

        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            assessment1 = new SQX_Assessment__c();
            assessment1.Name = 'test assessment name';
            assessment1.Total_Questions_To_Ask__c = 2;
            assessment1.Passing_Percentage__c = 0.00;
            assessment1.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;

            // Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            // Add two questions
            assmentExt.addQuestion();
            assmentExt.addQuestion();

            // add required fields of questions
            assmentExt.questions[0].Question_Long__c = 'Sample Question';
            assmentExt.questions[0].Answer_Options_Long__c = 'A)Yes\r\nB)No';
            assmentExt.questions[0].Correct_Answer__c = 'A';
            assmentExt.questions[0].Navigation__c = 'A=Next\r\nB=End';

            assmentExt.questions[1].Question_Long__c = 'Sample Question1';
            assmentExt.questions[1].Answer_Options_Long__c = 'A)No\r\nB)Yes';
            assmentExt.questions[1].Correct_Answer__c = 'B';
            assmentExt.questions[1].Navigation__c = 'A=End\r\nB=Next';
            
            // Act : Save the assessment and questions together using extension
            // set the current page
            PageReference pageRef = Page.SQX_Assessment_Editor_Detail_Layout;
            Test.setCurrentPage(pageRef);
            assmentExt.save();

            // share assessment with standardUser1 with read access
            SQX_Assessment__Share assessmentShare = new SQX_Assessment__Share();
            assessmentShare.ParentId = assessment1.Id;
            assessmentShare.UserOrGroupId = standardUser1.Id;
            assessmentShare.AccessLevel = ACCESSLEVEL_READ;
            Database.SaveResult sr = Database.insert(assessmentShare,false);
            System.assert(sr.isSuccess(),'assessment sharing is failed');

            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            // Assert : No page message should occur
            System.assert(pageMessages.size() == 0, 'Expected no page message error but is'+pageMessages);
            system.assert(assessment1.Id != null,'assessment not inserted');
            assessmentIdBeforClone = assessment1.Id;
            questionBeforeClone = assmentExt.questions;
        }

        // being the standardUser1 clone the assessment
        System.runAs(standardUser1) {
            // querying all fields to ensure that the values are properly cloned
            assessment1 = new SQX_Test_Assessment(assessment1).synchronize().assessment;
            assessment1.Name = 'test assessment name cloned';
            // Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);

            // Act : Clone assessment
            assmentExt.cloneAssessment();

            SQX_Assessment__c assessmentAfterClone = [SELECT Id, Name, Total_Questions_To_Ask__c, Passing_Percentage__c, Status__c, OwnerId FROM SQX_Assessment__c WHERE Name =: assessment1.Name];
            
            // Assert : new cloned assessment's status should be draft, owner should be changed and should be standardUser1
            System.assertEquals(SQX_Assessment.STATUS_DRAFT, assessmentAfterClone.Status__c, 'Expected the status of new cloned assessment to be draft but is :'+assessmentAfterClone.Status__c);

            System.assertEquals(standardUser1.Id, assessmentAfterClone.OwnerId);

            List<SQX_Assessment_Question__c> questionAfterClone = [SELECT Id, Question_Long__c, Question_Number__c, Answer_Options_Long__c, Navigation__c FROM SQX_Assessment_Question__c WHERE SQX_Assessment__c =: assessmentAfterClone.Id ORDER BY Question_Number__c ASC];
            
            // Assert : all two questions should be cloned
            System.assert(questionAfterClone.size() == 2, '2 Questions Should be cloned but is'+questionAfterClone.size());

            // Assert : new assessment should be created
            System.assertNotEquals(assessmentIdBeforClone, assessmentAfterClone.Id);

            // Assert : Question should also be copied
            System.assertNotEquals(questionBeforeClone.get(0).Id, questionAfterClone.get(0).Id);
            System.assertEquals(questionBeforeClone.get(0).Question_Long__c, questionAfterClone.get(0).Question_Long__c);
            System.assertEquals(questionBeforeClone.get(0).Question_Number__c, questionAfterClone.get(0).Question_Number__c);
            System.assertEquals(questionBeforeClone.get(0).Answer_Options_Long__c, questionAfterClone.get(0).Answer_Options_Long__c);
            System.assertEquals(questionBeforeClone.get(0).Navigation__c, questionAfterClone.get(0).Navigation__c);
            
            System.assertNotEquals(questionBeforeClone.get(1).Id, questionAfterClone.get(1).Id);
            System.assertEquals(questionBeforeClone.get(1).Question_Long__c, questionAfterClone.get(1).Question_Long__c);
            System.assertEquals(questionBeforeClone.get(1).Question_Number__c, questionAfterClone.get(1).Question_Number__c);
            System.assertEquals(questionBeforeClone.get(1).Answer_Options_Long__c, questionAfterClone.get(1).Answer_Options_Long__c);
            System.assertEquals(questionBeforeClone.get(1).Navigation__c, questionAfterClone.get(1).Navigation__c);
        }
    }

    /**
    * Given : Create Assessment with status pre-release and add some questions
    * When : Assessment is locked and is tried to modify.
    * Then : Validation error should occur since the assessment is locked
    * @Story : SQX-2992
    * @Author : Paras Kumar Bishwakarma
    */
    public testmethod static void givenAssessmentWithQuestions_WhenLockedAssessmentIsModified_ErrorOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            SQX_Assessment__c assessment1 = new SQX_Assessment__c();
            assessment1.Name = 'test assessment name';
            assessment1.Total_Questions_To_Ask__c = 2;
            assessment1.Passing_Percentage__c = 0.00;
            assessment1.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;

            // Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            // Add two questions
            assmentExt.addQuestion();
            assmentExt.addQuestion();
            
            // Insert required fields of question
            for(SQX_Assessment_Question__c q : assmentExt.questions) {
                q.Question_Long__c = 'Sample Question';
                q.Answer_Options_Long__c = 'A)Yes\r\nB)No';
                q.Correct_Answer__c = 'A';
                q.Navigation__c = 'A=Next\r\nB=End';
            }
            
            // Act : Save the assessment and questions together using extension
            // set the current page
            PageReference pageRef = Page.SQX_Assessment_Editor_Detail_Layout;
            Test.setCurrentPage(pageRef);
            assmentExt.save();
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            // Assert : No page message should occur
            System.assert(pageMessages.size() == 0, 'Expected no page message error but is'+pageMessages);
            system.assert(assessment1.Id != null,'assessment not inserted');
            
            // Act : Try to modify the locked assessment
            assessment1.Name = 'test assessment name cloned';
            assmentExt.save();
            
            // Assert : Check that the error message  is as expected
            Boolean result = SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), 'Assessment is locked. Only status can be updated.');
            system.assert(result,'Expected page message error is not found:Assessment is locked. Only status can be updated.');
            
            // Act : Try to modify the question of locked assessment
            assessment1.Name = 'test assessment name';// undo the name of assessment
            assmentExt.questions[0].Correct_Answer__c = 'B';
            assmentExt.save();
            result = SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), ERR_MSG_QUESTION_CANNOT_BE_ALTERED);
            system.assert(result,'Expected page message error is not found : '+ERR_MSG_QUESTION_CANNOT_BE_ALTERED);
        }
    }

    /**
    * Given : Create Assessment with status pre-release
    * When : Assessment is locked and try to add new Question
    * Then : Validation error should occur since the assessment is locked
    *
    * When : Assessment is obsoleted
    * Then : Assessment is locked
    * @Story : SQX-2992
    * @Author : Paras Kumar Bishwakarma
    */
    public testmethod static void givenAssessment_WhenNewQuestionIsAddedToLockedAssessment_ErrorOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            SQX_Assessment__c assessment1 = new SQX_Assessment__c();
            assessment1.Name = 'test assessment name';
            assessment1.Total_Questions_To_Ask__c = 0;
            assessment1.Passing_Percentage__c = 0.00;
            assessment1.Status__c = SQX_Assessment.STATUS_CURRENT;

            // Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            // Act : Save the assessment and questions together using extension
            // set the current page
            PageReference pageRef = Page.SQX_Assessment_Editor_Detail_Layout;
            Test.setCurrentPage(pageRef);
            assmentExt.save();
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            // Assert : No page message should occur
            System.assert(pageMessages.size() == 0, 'Expected no page message error but is'+pageMessages);
            system.assert(assessment1.Id != null,'assessment not inserted');

            // Add questions
            assmentExt.addQuestion();
            
            // add requirement field of question
            for(SQX_Assessment_Question__c q : assmentExt.questions) {
                q.Question_Long__c = 'Sample Question';
                q.Answer_Options_Long__c = 'A)Yes\r\nB)No';
                q.Correct_Answer__c = 'A';
                q.Navigation__c = 'A=Next\r\nB=End';
            }

            // try to save the questions
            assmentExt.save();

            Boolean result = SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), ERR_MSG_QUESTION_CANNOT_BE_ALTERED);
            system.assert(result,'Expected page message error is not found : '+ERR_MSG_QUESTION_CANNOT_BE_ALTERED);

            // Act : Create obsolete assessment
            SQX_Assessment__c assessment2 = new SQX_Assessment__c();
            assessment2.Name = 'test obsolete assessment';
            assessment2.Total_Questions_To_Ask__c = 0;
            assessment2.Passing_Percentage__c = 0.00;
            assessment2.Status__c = SQX_Assessment.STATUS_OBSOLETE;
            insert assessment2;

            // Assert: Assessment is locked
            Boolean isLocked = [SELECT Id, Is_Locked__c FROM SQX_Assessment__c WHERE Id =: assessment2.Id][0].Is_Locked__c;
            System.assert(isLocked,'Expected the assessment to be locked');

        }
    }

    /**
    * Given : Create Assessment with status pre-release and add some questions
    * When : Assessment is locked and is tried to delete it.
    * Then : Validation error should occur since the assessment is locked
    * @Story : SQX-2992
    * @Author : Paras Kumar Bishwakarma
    */
    public testmethod static void givenAssessmentWithQuestions_WhenLockedAssessmentQuestionIsTriedToDelete_ErrorOccur(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            SQX_Assessment__c assessment1 = new SQX_Assessment__c();
            assessment1.Name = 'test assessment name';
            assessment1.Total_Questions_To_Ask__c = 1;
            assessment1.Passing_Percentage__c = 0.00;
            assessment1.Status__c = SQX_Assessment.STATUS_PRE_EXPIRE;

            // Create the extension object
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment1);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            // Add two questions
            assmentExt.addQuestion();
            
            // Create questions
            for(SQX_Assessment_Question__c q : assmentExt.questions) {
                q.Question_Long__c = 'Sample Question';
                q.Answer_Options_Long__c = 'A)Yes\r\nB)No';
                q.Correct_Answer__c = 'A';
                q.Navigation__c = 'A=Next\r\nB=End';
            }
            
            // Act : Save the assessment and questions together using extension
            // set the current page
            PageReference pageRef = Page.SQX_Assessment_Editor_Detail_Layout;
            Test.setCurrentPage(pageRef);
            assmentExt.save();
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            
            // Assert : No page message should occur
            System.assert(pageMessages.size() == 0, 'Expected no page message error but is'+pageMessages);
            system.assert(assessment1.Id != null,'assessment not inserted');
            
            // Remove one question
            Apexpages.currentPage().getparameters().put('index','0');
            assmentExt.removeQuestion();
            
            // Act : Save the assessment and questions together using extension
            assmentExt.save();
            Boolean result = SQX_Utilities.checkPageMessageContainingTexts(ApexPages.getMessages(), 'Assessment is locked. Only status can be updated.');
            system.assert(result,'Expected page message error is not found : Assessment is locked. Only status can be updated.');
        }
    }

    /**
     * Given: Create SCORM Assessment with Content Reference that is zip file.
     * When: Assessment is treid to save
     * Then: It is saved
     * @Story: SQX-6230
     * @Author: Kishor Bikram Oli
     */
    public testmethod static void givenScromAssessmentWithContentReference_WhenScromAssessmentSaved_ThenRecordIsSaved(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('standardUser')){
            // creating random file
            ContentVersion cv = new ContentVersion();
            cv.VersionData = Blob.valueOf('Random Data');
            cv.PathOnClient = 'RandomFile.zip';
            cv.Title = 'FILE';
            
            //Arrange : Create SCORM assessment and set its status to null.
            SQX_Test_Assessment assessment = new SQX_Test_Assessment(SQX_Assessment.RT_SCORM_ASSESMENT_API_NAME);
            new SQX_DB().op_insert(new List<ContentVersion> { cv }, new List<Schema.SObjectField> { ContentVersion.VersionData, ContentVersion.PathOnClient });
            
            cv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id];
            assessment.setContent(cv);
            ApexPages.StandardController controller = new ApexPages.StandardController(assessment.assessment);
            SQX_Extension_Assessment assmentExt = new SQX_Extension_Assessment(controller);
            
            //Act : save assessment
            PageReference page = assmentExt.save();
            //Assert : Check if Assessment saved or not.
            System.assertNotEquals(null, page, 'Scorm Assessment is not saved.');
        }
    }
}