@isTest
public class SQX_Test_3591_Disposition_Approval
{
    /**
    * Map for given finding response and expected approval required for disposition
    */
    private static Map<SQX_Finding_Response__c, Boolean> dispositionApprovalMap = new Map<SQX_Finding_Response__c, Boolean>
    {
        setDispisitionStatusAsDraft() => true,
        setDispisitionStatusAsComplete() => true,
        setDispisitionStatusAsCompleteAndAppStatusAsApproved() => false,
        setDispisitionStatusAsOpenAndAppStatusAsApproved() => false,
        setDispisitionStatusAsDraftAndAppStatusAsPending() => true
    };
    
    /**
    * Map for given finding response and expected approval required for pre action
    */
    private static Map<SQX_Finding_Response__c, Boolean> actionPreApprovalMap = new Map<SQX_Finding_Response__c, Boolean>
    {
        setActionReasonAsNew() => true,
        setActionReasonAsSkip() => true,
        setActionReasonAsComplete() => false,
        setActionReasonAsPublish() => false
    };
    
    /**
    * Map for given finding response and expected approval required for post action
    */
    private static Map<SQX_Finding_Response__c, Boolean> actionPostApprovalMap = new Map<SQX_Finding_Response__c, Boolean>
    {
        setActionReasonAsNew() => false,
        setActionReasonAsSkip() => false,
        setActionReasonAsComplete() => true,
        setActionReasonAsPublish() => false
    };
    
    /**
    *   Given : Finding Response containing investigaton
    *   Given : Blank Finding Response
    *   When  : When requiresApproval method is checked setting investigation approval required
    *   Then  : requiresApproval method returns true
    *   Then  : requiresApproval method returns false
    *   @author - Sajal Joshi
    *   @date - 26/06/2017
    *   @story - SQX-3591
    */
    static testMethod void whenInvestigationApprovalIsRequired_ThenResponseIsSubmittedForApproval()
    {
        SQX_Finding_Response__c response = setInvestigation();
        SQX_Finding_Response resp = new SQX_Finding_Response(response);
        System.assertEquals(true, resp.requiresApproval(true, false, false), 'Approval should be required when investigation is not null '+ response.SQX_Response_Inclusions__r[0].SQX_Investigation__c);
        
        resp = new SQX_Finding_Response(new SQX_Finding_Response__c());
        System.assertEquals(false, resp.requiresApproval(true, false, false),'Approval should be required when investigation is not null '+ response.SQX_Response_Inclusions__r[0].SQX_Investigation__c );
    }
    
    /**
    *   Given : Finding Response containing disposition with different status
    *   When  : When requiresApproval method is checked setting disposition approval required
    *   Then  : approval is required according to status
    *           [ Status: Draft, Approval Status: null ]        -> true,
    *           [ Status: Complete, Approval Status: null ]     -> true,
    *           [ Status: Complete, Approval Status: Approved ] -> false,
    *           [ Status: Open, Approval Status: Approved ]     -> false,
    *           [ Status: Draft, Approval Status: Pending ]     -> true
    *   @author - Sajal Joshi
    *   @date - 26/06/2017
    *   @story - SQX-3591
    */
    static testMethod void whenDispositionApprovalIsRequired_ThenDispostionIsSubmittedForApproval()
    {
        for(SQX_Finding_Response__c response: dispositionApprovalMap.keySet()){
            SQX_Finding_Response resp = new SQX_Finding_Response(response);
            resp.requiresDispositionApproval = true;
            System.assertEquals(dispositionApprovalMap.get(response), resp.requiresApproval(false, false, false),
                                +'When Status is '+ response.SQX_Response_Inclusions__r[0].SQX_Disposition__r.Status__c 
                                +' and Approval Status is '+ response.SQX_Response_Inclusions__r[0].SQX_Disposition__r.Approval_Status__c
                                +' disposition approval required should be '+ dispositionApprovalMap.get(response));
        }
    }
    
    /**
    *   Given : Finding Response containing action with different record action
    *   When  : When requiresApproval method is checked setting preaction approval required
    *   Then  : approval is required according to record action
    *           [ Record Action: 'New' ]        -> true,
    *           [ Record Action: 'Skip' ]       -> true,
    *           [ Record Action: 'Complete' ]   -> false,
    *           [ Record Action: 'Publish' ]    -> false,
    *   @author - Sajal Joshi
    *   @date - 26/06/2017
    *   @story - SQX-3591
    */
    static testMethod void whenActionPreApprovalIsRequired_ThenActionIsSubmittedForApproval()
    {
        for(SQX_Finding_Response__c response: actionPreApprovalMap.keySet()){
            SQX_Finding_Response resp = new SQX_Finding_Response(response);
            System.assertEquals(actionPreApprovalMap.get(response), resp.requiresApproval(false, true, false), 
                                'When Inclusion Reason is '+ response.SQX_Response_Inclusions__r[0].SQX_Action__r.Record_Action__c
                                +' pre action approval required should be '+ actionPreApprovalMap.get(response));
        }
    }
    
    /**
    *   Given : Finding Response containing action with different record action
    *   When  : When requiresApproval method is checked setting postaction approval required
    *   Then  : post approval is required according to record action
    *           [ Record Action: 'New' ]        -> false,
    *           [ Record Action: 'Skip' ]       -> false,
    *           [ Record Action: 'Complete' ]   -> true,
    *           [ Record Action: 'Publish' ]    -> false,
    *   @author - Sajal Joshi
    *   @date - 26/06/2017
    *   @story - SQX-3591
    */
    static testMethod void whenActionPostApprovalIsRequiredAndReasonIsComplete_ThenActionIsSubmittedForApproval()
    {
        for(SQX_Finding_Response__c response: actionPostApprovalMap.keySet()){
            SQX_Finding_Response resp = new SQX_Finding_Response(response);
            System.assertEquals(actionPostApprovalMap.get(response), resp.requiresApproval(false, false, true), 
                                'When Inclusion Reason is '+ response.SQX_Response_Inclusions__r[0].SQX_Action__r.Record_Action__c
                                +' post action approval required should be '+ actionPostApprovalMap.get(response));
        }
    }
    
    /**
    * method to get data for investigation approval required condition
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setInvestigation(){
        // Create investigation
        SQX_Investigation__c investigation = new SQX_Investigation__c(
            Id = SQX_Test_Object_Factory.generateFakeId(SQX_Investigation__c.SObjectType)
            );
        // Create response inclusion
        List<SQX_Response_Inclusion__c> IncList = new List<SQX_Response_Inclusion__c>{
            new SQX_Response_Inclusion__c(
                Id = SQX_Test_Object_Factory.generateFakeId(SQX_Response_Inclusion__c.SObjectType) ,
                    SQX_Investigation__r = investigation
                )
        };
        
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for disposition as :
    * Status : Draft
    * Approval Status : none
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setDispisitionStatusAsDraft(){
        
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithDisposition(SQX_Disposition.STATUS_DRAFT, '');
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for disposition as :
    * Status : Complete
    * Approval Status : none
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setDispisitionStatusAsComplete(){
        
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithDisposition(SQX_Disposition.STATUS_COMPLETE, '');
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for disposition as :
    * Status : Complete
    * Approval Status : Approved
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setDispisitionStatusAsCompleteAndAppStatusAsApproved(){
        
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithDisposition(SQX_Disposition.STATUS_COMPLETE, SQX_Disposition.APPROVAL_STATUS_APPROVED); 
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for disposition as :
    * Status : Open
    * Approval Status : Approved
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setDispisitionStatusAsOpenAndAppStatusAsApproved(){
        
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithDisposition(SQX_Disposition.STATUS_OPEN, SQX_Disposition.APPROVAL_STATUS_APPROVED); 
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for disposition as :
    * Status : Draft
    * Approval Status : Pending
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setDispisitionStatusAsDraftAndAppStatusAsPending(){
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithDisposition(SQX_Disposition.STATUS_DRAFT, SQX_Disposition.APPROVAL_STATUS_PENDING); 
        return setupDataResponseData(IncList);
    }
    
    /**
     * method to create response inclusion with disposition
     * @param status status of disposition
     * @param statusApproval approval status of disposition
     * @return List<SQX_Response_Inclusion__c> list of inclusion with disposition
     */
    private static List<SQX_Response_Inclusion__c> createInclusionWithDisposition(String status, String approvalStatus){
        // Create disposition
        SQX_Disposition__c disposition = new SQX_Disposition__c(
                Id = SQX_Test_Object_Factory.generateFakeId(SQX_Disposition__c.SObjectType),
                Status__c = status,
            Approval_Status__c = approvalStatus);
        
        // Create response inclusion
        List<SQX_Response_Inclusion__c> IncList = new List<SQX_Response_Inclusion__c>{
            new SQX_Response_Inclusion__c(
                Id = SQX_Test_Object_Factory.generateFakeId(SQX_Response_Inclusion__c.SObjectType),
                SQX_Disposition__r = disposition
            )
        }; 
            
        // Relate disposition and response inclusion
        disposition = ((List<SQX_Disposition__c>)
            SQX_Test_Object_Factory.makeRelationship(
            List<SQX_Disposition__c>.class,
            new List<SQX_Disposition__c>{disposition},
            SQX_Response_Inclusion__c.SQX_Disposition__c,
            new List<List<SQX_Response_Inclusion__c>> { IncList }))[0];
        
    return IncList;
    }
    
    /**
    * method to setup data for action as :
    * action: New
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setActionReasonAsNew(){
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithAction(SQX_Response_Inclusion.INC_REASON_NEW); 
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for action as :
    * action: Skip
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setActionReasonAsSkip(){
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithAction(SQX_Response_Inclusion.INC_REASON_SKIP); 
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for action as :
    * action: Complete
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setActionReasonAsComplete(){
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithAction(SQX_Response_Inclusion.INC_REASON_COMPLETE); 
        return setupDataResponseData(IncList);
    }
    
    /**
    * method to setup data for action as :
    * action: Publish
    * @return SQX_Finding_Response__c object
    */
    private static SQX_Finding_Response__c setActionReasonAsPublish(){
        // Get response inclusion list
        List<SQX_Response_Inclusion__c> IncList = createInclusionWithAction(SQX_Response_Inclusion.INC_REASON_PUBLISH); 
        return setupDataResponseData(IncList);
    }
    
    /**
     * method to create response inclusion with action
     * @param recordAction action type of the record
     * @return List<SQX_Response_Inclusion__c> list of inclusion with action
     */
    private static List<SQX_Response_Inclusion__c> createInclusionWithAction(String recordAction){
    // create action
        SQX_Action__c action = new SQX_Action__c(
            Id = SQX_Test_Object_Factory.generateFakeId(SQX_Action__c.SObjectType),
                Record_Action__c = recordAction,
                Is_Approved__c = true
        );
        
        // Create response inclusion
        List<SQX_Response_Inclusion__c> IncList = new List<SQX_Response_Inclusion__c>{
            new SQX_Response_Inclusion__c(
                Id = SQX_Test_Object_Factory.generateFakeId(SQX_Response_Inclusion__c.SObjectType) ,
                    SQX_Action__r = action
                )
        };
        
        // relate action and response inclusion
        action = ((List<SQX_Action__c>)
        SQX_Test_Object_Factory.makeRelationship(
            List<SQX_Action__c>.class,
            new List<SQX_Action__c> { action },
            SQX_Response_Inclusion__c.SQX_Action__c,
            new List<List<SQX_Response_Inclusion__c>> { IncList }))[0];  
        
        return IncList;
    }
    
    /**
     * method to setup data for finding respone
     * @param inclusionList list to inclusion to relate with response
     * @return SQX_Finding_Response__c related finding response
     */ 
    private static SQX_Finding_Response__c setupDataResponseData(List<SQX_Response_Inclusion__c> inclusionList){
        SQX_Finding_Response__c response = new SQX_Finding_Response__c(
                id = SQX_Test_Object_Factory.generateFakeId(SQX_Finding_Response__c.SObjectType)
            );
        
        response = ((List<SQX_Finding_Response__c>)
        SQX_Test_Object_Factory.makeRelationship(
            List<SQX_Finding_Response__c>.class,
            new List<SQX_Finding_Response__c> { response },
            SQX_Response_Inclusion__c.SQX_Response__c,
            new List<List<SQX_Response_Inclusion__c>> {inclusionList}))[0];  
        
        return response;
    }
}