/**
* this test case for to allow edit change owner when training session is locked
*/
@isTest
public class SQX_Test_6803_Allow_Edit_Owner {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');     
    }
     /**
     * GIVEN : a training session in complete
     * WHEN : change the owner
     * THEN : allow to changed the owner without validation error
     */
    public static testMethod void givenCompleteTraningSession_WhenChangeTheOwner_ThenAllowToEditWithoutValidationError(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(adminUser){
            //Arrange: create a training session
            SQX_Test_Training_Session session = new SQX_Test_Training_Session();
            session.setStatus(SQX_Training_Session.STATUS_DRAFT);
            session.save();
            // Set SQX_Training_Session_E_Sig page with session id and mode as complete
            PageReference ref = Page.SQX_Training_Session_E_Sig;
            ref.getParameters().put(SQX_Extension_Training_Session.PARAM_ID, session.trainingSession.id);
            ref.getParameters().put(SQX_Extension_Training_Session.PARAM_MODE, SQX_Training_Session.PurposeOfSignature_CompletingTrainingSession);
            Test.setCurrentPage(ref);

            //Act: Save the SQX_Training_Session_E_Sig page
            SQX_Extension_Training_Session extension = new SQX_Extension_Training_Session(new ApexPages.StandardController(session.trainingSession));
            extension.recordActivityComment = 'Completing Training Session';
            System.assertEquals(true, extension.getIsActionValid());
            extension.saveRecord();
            
            //Assert: Training Session status is complete
            session.trainingSession = [SELECT id, Status__c, Is_Locked__c FROM SQX_Training_Session__c WHERE id =: session.trainingSession.id];
            System.assertEquals(SQX_Training_Session.STATUS_COMPLETE, session.trainingSession.Status__c, 'Training Session is Completed');
            system.assertEquals(true, session.trainingSession.Is_Locked__c);
            
            //Act: update the owner
            SQX_Training_Session__c ts = [SELECT Id, OwnerId FROM SQX_Training_Session__c WHERE Id =: session.trainingSession.Id];
            ts.OwnerId = standardUser.Id;
            List<Database.SaveResult> sessionResult =  new SQX_DB().continueOnError().op_update(new List<SQX_Training_Session__c> {ts}, new List<Schema.SObjectField>{});
            
            //Assert: Ensured that validation error message should not be throws(allowed to edit the owner)
            System.assertEquals(true, sessionResult[0].isSuccess());
        }
    }
}