/**
* Unit tests for creating new New Department 
* check Queue Id is valid or not for Created/updated Department 
*
* @author Shailesh Maharjan
* @date 2016/1/29
* 
*/
@IsTest
public class SQX_Test_1929_Department {
    static boolean run_AllTests = true,
            run_givenCreateDepartmentwithValidQueueId = false,
            run_givenUpdateDepartmentwithValidQueueId = false;

    /**
    * Scenario 1
    *  Given: New Group is Created
    *  Action: Department is Created with invalid QueueId
    *  Expected: No Department saved with invalid QueueId
    *
    * Scenario 2
    *  Action: Department is Created with valid QueueId from Created Group
    *  Expected: Department is successful saved.
    * Given New Department can only be created when QueueId is Valid
    * [SQX-1929]
    */         
    public testmethod static void givenCreateDepartmentwithValidQueueId(){
        if(!run_AllTests && !run_givenCreateDepartmentwithValidQueueId){
            return;
        }

        //Inserting new Queue
        Group queue = new Group();
        queue.Name = 'NewTestGroup';
        queue.DeveloperName = 'New_Test_Queue';
        queue.Type = 'Queue';

        insert queue;

        QueueSObject sobj = new QueueSObject();
        sObj.SobjectType = SQX.NonConformance;
        sObj.QueueId = queue.Id;

        insert sObj;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        System.runas(adminUser){
            // Arrange: creating New Department with Invalid QueueId
            SQX_Department__c department = new SQX_Department__c(
                Name='Depaetmenttest',
                Queue_Id__c='123456A'
            );
            //Act
            Database.SaveResult resultWithInvalidQueueId = Database.Insert(department ,false);
            //Assert
            System.assertEquals(1, resultWithInvalidQueueId.getErrors().size(), '1 error expected');
            System.assertEquals(true, resultWithInvalidQueueId.getErrors()[0].getMessage().equals(Label.SQX_ERR_MSG_INVALID_QUEUE_ID), 'Invalid queue error message expected');

            // Arrange: creating New Department with generated valid QueueId
            SQX_Department__c department1 = new SQX_Department__c(
                Name='Depaetmenttest',
                Queue_Id__c= queue.id
            );
            //Act
            Database.SaveResult resultWithValidQueueId = Database.Insert(department1 ,false);
            //Assert
            System.assertEquals(true, resultWithValidQueueId.isSuccess(), 'No error expected'+resultWithValidQueueId.getErrors());         
        }
    }


    /**
    * Scenario 1
    *  Given: New Group is Created
    *  Action: Department is Created without QueueId
    *  Expected: Department is successful saved without QueueId
    *
    * Scenario 2
    *  Action: Saved created QueueId before deleting Group.
    *          Delete Created Group
    *          Update Department with saved QueueId
    *  Expected: Department is not Updated.
    * Given QueueId can only be updated in Department when QueueId is Valid
    * [SQX-1929]
    */         
    public testmethod static void givenUpdateDepartmentwithValidQueueId(){
        if(!run_AllTests && !run_givenUpdateDepartmentwithValidQueueId){
            return;
        }

        //Adding new Queue
        Group queue = new Group();
        queue.Name = 'TestGroup';
        queue.DeveloperName = 'Test_Queue';
        queue.Type = 'Queue';

        insert queue;
        

        QueueSObject sobj = new QueueSObject();
        sObj.SobjectType = SQX.NonConformance;
        sObj.QueueId = queue.Id;

        insert sObj;
        
        

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        SQX_Department__c department;
        System.runas(adminUser){
            // Arrange: creating New Department without QueueId
            department = new SQX_Department__c(Name='Depaetmenttest');
            Database.SaveResult resultWithoutQueueId = Database.Insert(department ,false);

            System.assertEquals(true, resultWithoutQueueId.isSuccess(),
                'Expected Department save to be success');
         }
         
         Id queueID = queue.Id;
         delete queue;

         System.runas(adminUser){
            //Act: Updating Department with invalid Queue Id
            department.Queue_Id__c='123123';
            Database.SaveResult resultWithInvalidUpdatedQueueId = Database.update(department, false);

            //Assert
            System.assertEquals(1, resultWithInvalidUpdatedQueueId.getErrors().size(), '1 error expected');
            System.assertEquals(true, resultWithInvalidUpdatedQueueId.getErrors()[0].getMessage().equals(Label.SQX_ERR_MSG_INVALID_QUEUE_ID), 'Invalid queue error message expected');

            //Act: Updating Department with deleted Queue Id
            department.Queue_Id__c= queueID;
            Database.SaveResult resultWithDeletedQueueId = Database.update(department, false);

            System.assertEquals(true, resultWithDeletedQueueId.getErrors()[0].getMessage().equals(Label.SQX_ERR_MSG_INVALID_QUEUE_ID), 'Invalid queue error message expected');

        }
    }
}