/**
 * Class provides specific details required for submission of a MedDev record
*/

public with sharing class SQX_MedDev_eSubmitter extends SQX_Regulatory_Report_eSubmitter {

	public static final String RECORD_TYPE_MEDDEV = 'MEDDEV';

    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * the desired actions(XML generation and Insertion) for the given MedDev Record
    */
    protected override String constructPayloadForSubmission(Id medDevId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act, successAct1, successAct1_1, successAct1_1_1;
        SQX_External_Service_Payload.Job job1, job2, job3, job4;
        SQX_External_Service_Payload.HTTP_Request_Detail httpRequestDetail;
        SQX_External_Service_Payload.JWTPrincipal principal;
        Map<String,String> labelByApiName;
        String reportType;

        Submission_Job_Detail submissionJobDetail;
        Export_To_PDF_Job_Detail pdfJobDetail;
        SObjectDataLoader.SerializeConfig serializationConfig;
        ContentVersion cv;

        String baseUrl, versionDataInsertionPath;
        String recordJSON, recordName;

        recordName = getRecordName(medDevId);

        payload = new SQX_External_Service_Payload();

        serializationConfig = getSerializationConfig();

        recordJSON = this.getRecordJSONFor(medDevId, serializationConfig).replaceAll(SQX.NSPrefix, '');	

        baseUrl = Url.getSalesforceBaseUrl().toExternalForm();

        principal = payload.getDefaultPrincipalForCurrentUser();

        versionDataInsertionPath = baseUrl + '/services/data/v44.0/sobjects/ContentVersion/';

        // first action : XML generation
        act = payload.createAction();
        
         // List of sobject types for meddev and all its child objects.
        List<Schema.SObjectType> sObjectTypeList = new List<Schema.SObjectType>{SQX_Meddev__c.SObjectType};
      
        
        // get the map linking api name of the fields with the label for Medwatch
        labelByApiName = this.getFieldApiNameLabelMap(sObjectTypeList);

        // get the report type of the Meddev report
        reportType = getReportType(medDevId);
        
        // now adding additional data required for XML generation which is the record json
        submissionJobDetail = new Submission_Job_Detail(recordJSON,labelByApiName,reportType);

        job1 = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_XML, null, submissionJobDetail);

        // second action : On Success of XML generation, insert the content to SF
        successAct1 = act.createSuccessAction();

        cv = new ContentVersion(
            PathOnClient = recordName + EXTENSION_XML,
            CQ_Actions__c = String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_INITIAL, new String[] { getRegReportIdFor(medDevId) }),
            RecordTypeId = SQX_ContentVersion.getRegulatoryContentRecordType()
        );

        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(versionDataInsertionPath, SQX_External_Service_Payload.HttpMethod.POST, JSON.serialize(cv), principal);

        job2 = successAct1.addJob(SQX_External_Service_Payload.JobType.INSERT_CONTENT_TO_SF, new Integer[] { job1.getIdentifier() }, httpRequestDetail);

        // third action : On success of XML insertion, generate PDF
        successAct1_1 = successAct1.createSuccessAction();

        serializationConfig.translateLabels();

        recordJSON = this.getRecordJSONFor(medDevId, serializationConfig);

        pdfJobDetail = new Export_To_PDF_Job_Detail(recordJSON, getPDFTemplateName(medDevId), recordName);

        job3 = successAct1_1.addJob(SQX_External_Service_Payload.JobType.GENERATE_PDF, null, pdfJobDetail);

        // final action : insert PDF to SF
        successAct1_1_1 = successAct1_1.createSuccessAction();

        cv = new ContentVersion(
            PathOnClient = recordName + EXTENSION_PDF,
            CQ_Actions__c = String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION_FINAL, new String[] { getRegReportIdFor(medDevId) }),
            RecordTypeId = SQX_ContentVersion.getRegulatoryContentRecordType()
        );

        httpRequestDetail = new SQX_External_Service_Payload.HTTP_Request_Detail(versionDataInsertionPath, SQX_External_Service_Payload.HttpMethod.POST, JSON.serialize(cv), principal);

        job4 = successAct1_1_1.addJob(SQX_External_Service_Payload.JobType.INSERT_CONTENT_TO_SF, new Integer[] { job3.getIdentifier() }, httpRequestDetail);
        return payload.convertToJSONString();
    }

    /**
     * Method to generate the payload(request body) that instructs the external service to perform
     * validation of the given MedDev Record
    */
    protected override String constructPayloadForValidation(Id medDevId) {

        SQX_External_Service_Payload payload = new SQX_External_Service_Payload();
        SQX_External_Service_Payload.Action act;
        SQX_External_Service_Payload.Job job1;
        SQX_External_Service_Payload.HTTP_Request_Detail httpRequestDetail;
        SQX_External_Service_Payload.JWTPrincipal principal;

        Submission_Job_Detail submissionJobDetail;
        SObjectDataLoader.SerializeConfig serializationConfig;
        Map<String,String> labelByApiName;
        String reportType;

        String baseUrl;
        String recordJSON;

        payload = new SQX_External_Service_Payload();

        serializationConfig = getSerializationConfig();

        recordJSON = this.getRecordJSONFor(medDevId, serializationConfig).replaceAll(SQX.NSPrefix, '');

        baseUrl = Url.getSalesforceBaseUrl().toExternalForm();

        principal = payload.getDefaultPrincipalForCurrentUser();

        // first action : XML generation
        act = payload.createAction();

         // List of sobject types for meddev and all its child objects.
        List<Schema.SObjectType> sObjectTypeList = new List<Schema.SObjectType>{SQX_Meddev__c.SObjectType};
      
        
        // get the map linking api name of the fields with the label for Medwatch
        labelByApiName = this.getFieldApiNameLabelMap(sObjectTypeList);
    
        // get the report type of the Meddev report
        reportType = getReportType(medDevId);

        // now adding additional data required for XML generation which is the record json
        submissionJobDetail = new Submission_Job_Detail(recordJSON,labelByApiName, reportType);

        job1 = act.addJob(SQX_External_Service_Payload.JobType.GENERATE_XML, null, submissionJobDetail);

        return payload.convertToJSONString();
    }



    private SQX_MedDev__c recordDetailCache = null;

    /**
     * MedDev record query
    */
    protected override SObject getRecordDetail(String medDevId) {
        if(recordDetailCache == null) {
            recordDetailCache = [SELECT Name, SQX_Regulatory_Report__c, Admin_Info_ReportType__c FROM SQX_MedDev__c WHERE Id =: medDevId];
        }
        return recordDetailCache;
    }
    
    /*
     * Get the reportType of the meddev record.
     */
    private String getReportType(String medDevId){
        
        SObject meddevRecord = getRecordDetail(medDevId);
        return (String) meddevRecord.get('compliancequest__Admin_Info_ReportType__c');
    }

    /**
     * Job Detail type of class specific to MedDev submission job
    */
    private class Submission_Job_Detail implements SQX_External_Service_Payload.JobDetail {

        /**
         * medDev record (along with the children records) as JSON
         * Used to map values to XML/PDF
        */
        public String recordJSON;

        /**
         * Instructs the external service to use MedDev schema for xml generation
        */
        public String recordType;
        
        public Map<String,String> labelByApiName;
        
        public String reportType;

        public Submission_Job_Detail(String recordJSON, Map<String,String> labelsByApiName, String reportType) {
            this.recordJSON = recordJSON;
            this.recordType = RECORD_TYPE_MEDDEV;
            this.labelByApiName = labelsByApiName;
            this.reportType = reportType;
        }
    }



    /**
     * Trigger handler for MedDev Report trigger actions
    */
    public with sharing class SQX_MedDev_Trigger_Handler extends SQX_Trigger_Handler {

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newValues equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public SQX_MedDev_Trigger_Handler(List<SObject> newValues, Map<Id, SObject> oldMap) {
            super(newValues, oldMap);
        }

        /**
        * Trigger methods:
        * a. After Insert/Update [clearActivityCode]: clears out activity code field value
        */
        protected override void onAfter() {
            clearActivityCode();
        }

        /**
         * Method clears out activity code field value
        */
        private void clearActivityCode() {

            final String ACTION_NAME = 'clearActivityCode';

            Rule recordsWithActivityCodeRule = new Rule();
            recordsWithActivityCodeRule.addRule(SQX_MedDev__c.Activity_Code__c, RuleOperator.NotEquals, null);

            String objName = this.getObjName();

            this.resetView()
                .removeProcessedRecordsFor(objName, ACTION_NAME)
                .applyFilter(recordsWithActivityCodeRule, RuleCheckMethod.OnCreateAndEveryEdit);

            List<SQX_MedDev__c> recordsToUpdate = new List<SQX_MedDev__c>();

            if(this.view.size() > 0){
                this.addToProcessedRecordsFor(objName, ACTION_NAME, this.view);

                for(SQX_MedDev__c record : (List<SQX_MedDev__c>) this.view) {
                    SQX_MedDev__c recordToUpdate = new SQX_MedDev__c(Id = record.Id);
                    recordToUpdate.Activity_Code__c = null;
                    recordsToUpdate.add(recordToUpdate);
                }

                new SQX_DB().op_update(recordsToUpdate, new List<SObjectField> { SQX_MedDev__c.Activity_Code__c });
            }

        }
    }
}