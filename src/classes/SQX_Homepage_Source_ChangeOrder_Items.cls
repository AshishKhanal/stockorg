/**
 * This class acts as an item source for Change Order related items
 * @author  Anuj Bhandari
 * @date    19-04-2017
 */
public with sharing class SQX_Homepage_Source_ChangeOrder_Items extends SQX_Homepage_ItemSource {

    /*
        Change Order Items : Draft/Open/Triage/Completed items
    */

    private final String module = SQX_Homepage_Constants.MODULE_TYPE_CHANGE_ORDER;

    /*
        This is the boundary value that separates urgency for an action on this item from high to critical
        e.x. if the item's due date is less than or equal to LIMIT_FOR_CRITICALITY away from today's date, the urgency is set to High
    */
    public static final Integer LIMIT_FOR_CRITICALITY_CHANGE_ORDER = 3;

    private static final User loggedInUser = SQX_Homepage_Helper.getLoggedInUserProfile();

    /**
     *   Constructor method
     *   @param filter - instance of SQX_Homepage_Filter class that defines the filters to be applied across records
     */
    public SQX_Homepage_Source_ChangeOrder_Items() {
        super();
    }


    /**
    *   Method returns all the sobject types and the corresponding fields being queried in this class(by this source)
    */
    protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
        return new Map<SObjectType, List<SObjectField>>{
            SQX_Change_Order__c.SObjectType => new List<SObjectField> 
            {
                SQX_Change_Order__c.Name,
                SQX_Change_Order__c.Title__c,
                SQX_Change_Order__c.CreatedDate
            },
            User.SObjectType => new List<SObjectField>
            {
                User.Name,
                User.SmallPhotoUrl
            }
        };
    }

    /**
     *   returns a list of homepage items for the current user related to Audit module
     */
    protected override List < SObject > getUserRecords() {

        List < SObject > allItems = new List < SObject > ();
        List < String > coStatuses = new list < String > {
            SQX_Change_Order.STATUS_DRAFT,
            SQX_Change_Order.STATUS_OPEN
        };

        List < SQX_Change_Order__c > coItems = [SELECT  Id,
                                                        Name,
                                                        Title__c,
                                                        Status__c,Record_Stage__c,
                                                        Approval_Status__c,
                                                        Effective_Date__c,
                                                        OwnerId,
                                                        CreatedDate
                                                FROM SQX_Change_Order__c
                                                WHERE(
                                                        Status__c IN: coStatuses  OR (Status__c = : SQX_Change_Order.STATUS_COMPLETE AND Record_Stage__c=:SQX_Change_Order.STAGE_COMPLETE AND Effective_Date__c = null)
                                                      )
                                                AND OwnerID IN: SQX_Homepage_Helper.getAllIdsForLoggedInUser()
                                               ];

        return coItems;
    }



    /**
     *   Method returns a SQX_Homepage_Item type item from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item) {

        return getChangeOrderItem((SQX_Change_Order__c) item);

    }

    /**
     *   Method returns a SQX_Homepage_Item with correct values for item of different statuses from the given sobject record
     *   @param item - Record to be converted to home page item
     */
    private SQX_Homepage_Item getChangeOrderItem(SQX_Change_Order__c item) {

        SQX_Homepage_Item hItem = new SQX_Homepage_Item();

        hItem.itemId = (Id) item.get('Id');

        hItem.createdDate = Date.valueOf(item.get('CreatedDate'));

        // set module type
        hItem.moduleType = this.module;

        hItem.creator = loggedInUser;

        boolean isDraft, isOpen, isTriage, isPlanRejected, isCompleted;

        isDraft = (
            ((String) item.Status__c == SQX_Change_Order.STATUS_DRAFT) && ((String) item.Record_Stage__c == SQX_Change_Order.STATUS_DRAFT) && 
            ((String) item.Approval_Status__c == null)
        ) ? true : false;

        isOpen = (String) item.Status__c == SQX_Change_Order.STATUS_OPEN ? true : false;

        isTriage = ((String) item.Status__c == SQX_Change_Order.STATUS_DRAFT && (String) item.Record_Stage__c == SQX_Change_Order.STAGE_TRIAGE) ? true : false;

        isPlanRejected = (
            ((string) item.Approval_Status__c == SQX_Change_Order.APPROVAL_STATUS_PLAN_REJECTED) &&
            (
                ((string) item.Status__c != SQX_Change_Order.STATUS_CLOSED) ||
                ((string) item.Status__c != SQX_Change_Order.STATUS_VOID) ||
                ((string) item.Status__c != SQX_Change_Order.STATUS_COMPLETE)
            )
        ) ? true : false;

        isCompleted = (
            ((String) item.Status__c == SQX_Change_Order.STATUS_COMPLETE) &&  ((String) item.Record_Stage__c == SQX_Change_Order.STAGE_COMPLETE) && (item.Effective_Date__c == null)
        ) ? true : false;


        // add actions
        SQX_Homepage_Item_Action itemViewAction = getViewAction(hItem.itemId);
        if(isTriage)
        {
            // Using 'Initiate' as action name for triage actions
            itemViewAction.name = SQX_Homepage_Constants.ACTION_INITIATE;
        }

        hItem.actions = new List < SQX_Homepage_Item_Action > {
            itemViewAction
        };

        if (isPlanRejected) {

            hItem.feedText = getRejectedDraftFeedText(item);

            // set action type
            hItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

        } else if (isDraft) {

            // set feed text for the current Draft CO item
            hItem.feedText = getDraftFeedText(item);

            // this item falls into the Draft Items List
            hItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_DRAFT_ITEMS;

        } else if (isOpen) {

            // set feed text for the Triage CO item
            hItem.feedText = getOpenFeedText(item);

            // set action type
            hItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_OPEN_RECORDS;

        } else if (isTriage) {

            // set feed text for the Triage CO item
            hItem.feedText = getTriageFeedText(item);

            // set action type
            hItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;

        } else if (isCompleted) {
            // set feed text for the Triage CO item
            hItem.feedText = getCompletedFeedText(item);

            // set action type
            hItem.actionType = SQX_Homepage_Constants.ACTION_TYPE_NEEDS_ATTENTION;
        }

        return hItem;
    }


    /**
     *   Returns the view action
     *   @param itemId - id of the record for which action is to be set
     */
    private SQX_Homepage_Item_Action getViewAction(string itemId) {

        SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

        action.name = SQX_Homepage_Constants.ACTION_VIEW;

        PageReference pr = new PageReference('/' + itemId);
        pr.getParameters().put('retUrl',SQX_Utilities.getHomePageRetUrlBasedOnUserMode(true));
        action.actionUrl = getHomePageItemActionUrl(pr);

        action.actionIcon = SQX_Homepage_Constants.ICON_UTILITY_VIEW;

        action.supportsBulk = false;

        return action;
    }


    /**
     *   Method returns the Draft feed text for the given item
     */
    private String getDraftFeedText(SQX_Change_Order__c item) {

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_DRAFT_ITEMS;
        return String.format(template, new String[] {
            (String) item.Name,
                (String) item.Title__c

        });
    }

    /**
     *   Method returns the Open feed text for the given item
     */
    private String getOpenFeedText(SQX_Change_Order__c item) {

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_OPEN_RECORDS;
        return String.format(template, new String[] {
            (String) item.Name,
                (String) item.Title__c

        });
    }

    /**
     *   Method returns Triage feed text for the given item
     */
    private String getTriageFeedText(SQX_Change_Order__c item) {

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_TRIAGE_ITEMS;

        return String.format(template, new String[] {
            (String) item.Name,
                (String) item.Title__c
        });
    }

    /**
     *   Method returns Rejected feed text for the given item
     */
    private String getRejectedDraftFeedText(SQX_Change_Order__c item) {

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_REJECTED_ITEMS;

        return String.format(template, new String[] {
            (String) item.Name,
                (String) item.Title__c

        });
    }

    /**
     *   Method returns Completed feed text for the item without effective date
     */
    private String getCompletedFeedText(SQX_Change_Order__c item) {

        String template = SQX_Homepage_Constants.FEED_TEMPLATE_CHANGE_ORDER_COMPLETED;

        return String.format(template, new String[] {
            (String) item.Name,
                (String) item.Title__c
        });
    }

}