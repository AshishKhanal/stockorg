/**
* It contains the Status Containment object
*/
public with sharing class SQX_Containment{
    
    /* Statuses pick list field listing for use in code  */
    public static final String STATUS_DRAFT = 'Draft';
    public static final String STATUS_APPROVED_DRAFT = 'Approved Draft';
    public static final String STATUS_OPEN = 'Open';
    public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_COMPLETED = 'Completed';
    
}