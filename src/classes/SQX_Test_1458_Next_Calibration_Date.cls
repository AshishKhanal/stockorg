/**
* Unit tests related to Calibration dates and interval for Equipment object
*/
@isTest
public class SQX_Test_1458_Next_Calibration_Date{

    static Boolean runAllTests = true,
                   run_givenUpdateCalibrationDatesAndInterval_SavedWithRecordActivity = false,
                   run_givenRequiredPerodicCalibratationAsFalse_NextCalibrationAsNull = false,
                   run_givenSaveChangeDate_WithoutAnyChanges_NotSaved = false;

    /**
    * This rule ensures that if a new next calibration date is updated or not, when last calibration date and calibration interval is updated
    */
    public testmethod static void givenUpdateCalibrationDatesAndInterval_SavedWithRecordActivity(){

        if (!runAllTests && !run_givenUpdateCalibrationDatesAndInterval_SavedWithRecordActivity){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){ 

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c equipment = new SQX_Equipment__c();
            equipment.Name = '123test';
            equipment.Equipment_Description__c = 'This is test equipment';
            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Next_Calibration_Date__c = equipment.Last_Calibration_Date__c.addDays(10); //next calibration is after 10 days
            equipment.Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE ;
            equipment.Equipment_Status__c = SQX_Equipment.STATUS_NEW;
            equipment.Requires_Periodic_Calibration__c = true;


            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert( equipment, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr.isSuccess(), 'Expecting equipment reocord had been inserted');

            Id equipmentId = sr.getId();

            Test.setCurrentPage(Page.SQX_Equipment_Change_Calibration_Date); //setting SQX_Equipment_Change_Calibration_Date for Test

            ApexPages.StandardController controller = new ApexPages.StandardController(equipment);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);

            //Act: updating above created equipment record field
            //we will update the Last Calibration Date, Calibration Interval, and Comment
            //next calibration date should be updated

            equipment.Requires_Periodic_Calibration__c = true;
            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Calibration_Interval__c = 50; //we have added calibration interval as 50 days
            equipment.Next_Calibration_Date__c = equipment.Last_Calibration_Date__c.addDays((Integer)equipment.Calibration_Interval__c);
            extension.recordActivityComment = 'We have updated last calibration date and calibration interval, so our next calibration date need to be updated';
            extension.changeCalibrationDate();

            //Act: retriving updated equipment record
            //select above updated equipment
            SQX_Equipment__c updatedEquipment = [SELECT Name, Equipment_Description__c, Last_Calibration_Date__c, Next_Calibration_Date__c, Equipment_Type__c, Calibration_Interval__c FROM SQX_Equipment__c WHERE Id = :equipmentId];


            Date expectedNextCalibrationDate = updatedEquipment.Last_Calibration_Date__c.addDays(50);

            //Assert: Assert if updating Last Calibration Date, Calibration Interval update Next Calibration Date
            System.assertEquals(expectedNextCalibrationDate, updatedEquipment.Next_Calibration_Date__c, 'Expected Next Calibration Date had been Updated');

            //Act: retriving equipment record activity record created by above equipment record
            //select equipment record activity
            SQX_Equipment_Record_Activity__c[] createdEra = [SELECT Id, Purpose_Of_Signature__c, Comment__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :equipmentId];

            //Assert: Assert if record activity had been created or not
            System.assertEquals(1, createdEra.size(), 'Expected number of record Activity created is 1');
            
            System.assertEquals(Label.SQX_PS_Equipment_Changing_Calibration_Date, createdEra[0].Purpose_Of_Signature__c,
                'Expected purpose of signature of record Activity created to be "' + Label.SQX_PS_Equipment_Changing_Calibration_Date + '".');
            
            String expectedStatusChangeInfo = SQX_Equipment__c.Requires_Periodic_Calibration__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo) == false,
                'Expected comment of record Activity created not to contain status change information of Requires Periodic Calibration.');
            
            expectedStatusChangeInfo = SQX_Equipment__c.Last_Calibration_Date__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo) == false,
                'Expected comment of record Activity created not to contain status change information of Last Calibration Date.');
            
            expectedStatusChangeInfo = SQX_Equipment__c.Calibration_Interval__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo),
                'Expected comment of record Activity created to contain status change information of Calibration Interval.');
            
            expectedStatusChangeInfo = SQX_Equipment__c.Next_Calibration_Date__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo),
                'Expected comment of record Activity created not to contain status change information of Next Calibration Date.');
                
            System.assert(createdEra[0].Comment__c.endsWith(extension.recordActivityComment),
                'Expected comment of record Activity created to end with user comment.');
        }
    }


    /**
    * This rule ensures that if a Required Perodic Calibration is set false, then Next Calibration Date will be null
    */
    public testmethod static void givenRequiredPerodicCalibratationAsFalse_NextCalibrationAsNull(){

        if (!runAllTests && !run_givenRequiredPerodicCalibratationAsFalse_NextCalibrationAsNull){
            return;
        }

        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runas(standardUser){ 

            //Arrange: Create new Equipment Management field with all required field
            SQX_Equipment__c equipment = new SQX_Equipment__c();
            equipment.Name = '123test';
            equipment.Equipment_Description__c = 'This is test equipment';
            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Next_Calibration_Date__c = equipment.Last_Calibration_Date__c.addDays(10); //next calibration is after 10 days
            equipment.Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE ;
            equipment.Equipment_Status__c = SQX_Equipment.STATUS_NEW;
            equipment.Requires_Periodic_Calibration__c = false;


            //Act: inserting new equipment record
            Database.SaveResult sr = Database.insert( equipment, false);

            //Assert: Assert if new equipment record had been inserted
            System.assertEquals(true, sr.isSuccess(), 'Expecting equipment reocord had been inserted');

            Id equipmentId = sr.getId();

            Test.setCurrentPage(Page.SQX_Equipment_Change_Calibration_Date); //setting SQX_Equipment_Change_Calibration_Date for Test

            ApexPages.StandardController controller = new ApexPages.StandardController(equipment);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);

            //Act: updating above created equipment record field
            //we will update the Last Calibration Date, Calibration Interval, and Comment
            //next calibration date should not be null

            equipment.Requires_Periodic_Calibration__c = false;
            equipment.Last_Calibration_Date__c = Date.today();
            equipment.Calibration_Interval__c = 50; //we have added calibration interval as 50 days
            extension.recordActivityComment = 'We have updated last calibration date and calibration interval, so our next calibration date need to be updated';
            extension.changeCalibrationDate();


            //Act: retriving updated equipment record
            //select above updated equipment
            SQX_Equipment__c updatedEquipment = [SELECT Name, Equipment_Description__c, Last_Calibration_Date__c, Next_Calibration_Date__c, Equipment_Type__c, Calibration_Interval__c FROM SQX_Equipment__c WHERE Id = :equipmentId];


            Date expectedNextCalibrationDate = null;

            //Assert: Assert if updating Last Calibration Date, Calibration Interval update Next Calibration Date
            System.assertEquals(expectedNextCalibrationDate, updatedEquipment.Next_Calibration_Date__c, 'Expected Next Calibration Date had been NULL');

            //Act: retriving equipment record activity record created by above equipment record
            //select equipment record activity
            SQX_Equipment_Record_Activity__c[] createdEra = [SELECT Id, Comment__c FROM SQX_Equipment_Record_Activity__c WHERE SQX_Equipment__c = :equipmentId];

            //Assert: Assert if record activity had been created or not
            System.assertEquals(1, createdEra.size(), 'Expected number of record Activity created is 1');
            
            String expectedStatusChangeInfo = SQX_Equipment__c.Next_Calibration_Date__c.getDescribe().getLabel() + ' changed from ';
            System.assert(createdEra[0].Comment__c.contains(expectedStatusChangeInfo), 'Expected comment of record Activity created to contain status change information of Next Calibration Date.');
            
            System.assert(createdEra[0].Comment__c.endsWith(extension.recordActivityComment), 'Expected comment of record Activity created to end with user comment.');

        }
    }
    
    
    /**
    * Unit tests for Equipment Object change date with no changes
    * @story SQX-1621
    */
    public testmethod static void givenSaveChangeDate_WithoutAnyChanges_NotSaved() {
        if (!runAllTests && !run_givenSaveChangeDate_WithoutAnyChanges_NotSaved) {
            return;
        }
        
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        
        System.runas(standardUser){
            // add new required equipment
            SQX_Equipment__c eqp1 = new SQX_Equipment__c(
                Name = 'eqp-samestatus',
                Equipment_Description__c = 'eqp-samestatus desc',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Last_Calibration_Date__c = Date.today(),
                Next_Calibration_Date__c = Date.today().addDays(10)
            );
            Database.insert(eqp1, false);
            
            Test.setCurrentPage(Page.SQX_Equipment_Status_Change);
            ApexPages.StandardController controller = new ApexPages.StandardController(eqp1);
            SQX_Extension_Equipment extension = new SQX_Extension_Equipment(controller);
            extension.ES_Enabled = false;
            
            // ACT: submit change equipment status without changing
            extension.recordActivityComment = 'testing no changes validation';
            extension.changeCalibrationDate();
            
            // check for error message
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            Boolean errFound = false;
            for (ApexPages.Message msg : msgs) {
                if (msg.getDetail().contains(Label.SQX_ERR_MSG_EQUIPMENT_CHANGE_DATE_FIELDS_NOT_CHANGED)) {
                    errFound = true;
                    break;
                }
            }
            
            System.assert(errFound, 'Expected error message not found for no changes.');
        }
    }
}