/**
* this is class that handles trigger functionality for all the objects
*/
public with sharing class SQX_Approved_Investigation{

    /**
    * Trigger handler for approved investigation
    */
    public with sharing class Bulkified extends SQX_BulkifiedBase{
        
        /**
        * default constructor
        */
        public Bulkified(List<SQX_Approved_Investigation__c> newObjects, Map<Id, SQX_Approved_Investigation__c> oldObjects){
        
            super(newObjects, oldObjects);
        }
        
        /**
        * this method copies all the approved investigation's root causes to the corresponding
        * approved root causes object
        */
        public Bulkified copyApprovedInvestigationRootCauses(){
            /**/
            List<SQX_Approved_Root_Cause__c> rootCauses = new List<SQX_Approved_Root_Cause__c>();
            
            Set<Id> allInvestigations = this.getIdsForField(SQX_Approved_Investigation__c.SQX_Investigation__c);
            
            //build investigation to Approved investigation map
            Map<Id, List<SQX_Approved_Investigation__c>> inv2AppInv = new Map<Id, List<SQX_Approved_Investigation__c>>();
            for(SQX_Approved_Investigation__c appInv : (List<SQX_Approved_Investigation__c>)this.view){
                List<SQX_Approved_Investigation__c> appInvs = inv2AppInv.get(appInv.SQX_Investigation__c);
                if(appInvs == null){
                    appInvs = new List<SQX_Approved_Investigation__c>();
                    inv2AppInv.put(appInv.SQX_Investigation__c, appInvs);
                }
                
                appInvs.add(appInv);
            }
            
            
            for(SQX_Root_Cause__c rootCause: 
                    [SELECT Id, SQX_Investigation__c 
                    FROM SQX_Root_Cause__c
                    WHERE SQX_Investigation__c IN : allInvestigations]){
                    
                    List<SQX_Approved_Investigation__c> appInvs = inv2AppInv.get(rootCause.SQX_Investigation__c);
                    for(SQX_Approved_Investigation__c appInv : appInvs){
                        SQX_Approved_Root_Cause__c appRC = new SQX_Approved_Root_Cause__c();
                        appRC.SQX_Finding__c = appInv.SQX_Finding__c;
                        appRC.SQX_Root_Cause__c = rootCause.Id;
                        
                        rootCauses.add(appRC);
                    }
            }
            
            if(rootCauses.size() > 0){
                /*
                * Without sharing has been used to cover the following scenario:
                * ------------------------------------------ 
                * Scenario:
                *    As a supplier submit an investigation to the customer, when the investigation is published without being approved (no approval is required)
                *    Then, the submitted root causes must be added to the finding
                *
                *    To avoid using without sharing in Apex we could change sharing setting in master-detail relationship(Finding) to requires 'Read only' access
                *    This will allow the supplier to add approved root causes without any issue
                *
                *    However, this would open up a hole when approval is required i.e. as a supplier I could still add approved root causes when they are not actually approved.
                *    
                *    Alternate option could be to create a custom validation rule that checks that there is read write access on finding when approval is required and 
                *    prevents the user from adding any record directly
                *
                *
                */
                new SQX_DB().withoutSharing().op_insert(rootCauses, new List<SObjectField>{
                    SQX_Approved_Root_Cause__c.fields.SQX_Finding__c,
                    SQX_Approved_Root_Cause__c.fields.SQX_Root_Cause__c
                });
            }
            
            
            
            return this;
        }
    }

}