/**
 * Test class for Canada Report object
*/


@isTest
public class SQX_Test_Canada_Report {

    private final static String TYPE_OF_REPORT_PRELIMINARY = 'Preliminary',
                                TYPE_OF_REPORT_UPDATE = 'Update',
                                TYPE_OF_REPORT_FINAL = 'Final',
                                TYPE_OF_REPORT_PRELIMINARY_AND_FINAL = 'Preliminary & Final';

    private final static String DEVICE_AVAILABILITY_DESTROYED = 'Destroyed',
                                DEVICE_AVAILABILITY_RETURNED_TO_MFR_OR_IMPORTER = 'Returned to Manufacturer/Importer',
                                DEVICE_AVAILABILITY_NEITHER = 'Neither (with explanation)';

    private final static String VALIDATION_ERR_MSG_Prvt_Anticipated_Date_For_Final_Report = 'Anticipated final report date is only required for Preliminary reports',
                                VALIDATION_ERR_MSG_Prevent_Prev_Sub_Date_For_Initial_Report = 'Previous report submission date is only required for Update or Final type reports.',
                                VALIDATION_ERR_MSG_Prevt_Explanation_For_Known_Availability = 'Device Availability Explanation is only required if Availability of Device has been set as \'Neither\'',
                                VALIDATION_ERR_MSG_Prevent_Root_Cause_For_Initial_Report = 'Root cause can only be specified for Preliminary &amp; Final or Final reports.',
                                VALIDATION_ERR_MSG_Prevt_CorrectiveAction_In_Initial_Report = 'Corrective actions can only be specified for Preliminary &amp; Final or Final reports.',
                                VALIDATION_ERR_MSG_Prevent_Record_Edit_Once_Locked = 'Record locked. Cannot perform the desired action.';

    /**
     * Common setup
    */
    @testSetup
    public static void commonSetup() {

        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        System.runAs(standardUser) {
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            complaint.save();

            //ARRANGE: Create regulatory report
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c();
            regReport.Reg_Body__c = SQX_Regulatory_Report.REGULATORY_BODY_HEALTH_CANADA;
            regReport.Report_Name__c = SQX_Regulatory_Report.REPORT_NAME_10_DAY_MDPR;
            regReport.SQX_Complaint__c=complaint.complaint.Id;
            regReport.Due_Date__c= Date.today().addDays(3);
            insert regReport;


            SQX_Canada_Report__c canadaReport = new SQX_Canada_Report__c(
                Type_of_Report__c = TYPE_OF_REPORT_PRELIMINARY,
                Reporter_Type__c = 'Manufacturer',
                Reporter_Contact_Info__c = 'Test contact',
                Mfr_Name__c = 'Mfr Name',
                Mfr_Address__c = 'Mfr Address',
                Mfr_Id__c = 'Mfr Id',
                Mfr_Licence_Number__c = 'Mfr Licence No',
                Type_of_Incident__c = '10-Day',
                Source_of_Incident__c = 'Canadian',
                Special_Incident_Circumstances__c = 'Investigational Testing',
                Date_of_Incident__c = Date.today(),
                Reporter_Awareness_Date__c = Date.today(),
                Patient_Consequences__c = 'Consequences',
                Brand_Name__c = 'Brand Name',
                Serial_Number__c = 'L100',
                Expiration_Date__c = Date.today().addDays(30),
                Device_Classification__c = 'I',
                Device_Id__c = 'Device Id',
                Device_Licence_Number__c = 'Device L',
                Software_Version__c = '2.0',
                Mfr_Device_Id__c = 'Mfr Dev Id',
                Age_of_Device__c = '10 years',
                Device_Usage_Period__c = '30 days',
                Device_Was_Sterile__c = 'Yes',
                Availability_Of_Device__c = 'Destroyed',
                Complainant_Type__c = 'Consumer',
                Name_of_Complainant__c = 'CName',
                Health_Care_Facility_Name__c = 'Facility Name',
                Complainant_Address__c = 'CAddress',
                Complainant_Email__c = 'Email@email.com',
                Inv_Actions_and_Timeline__c = 'Something',
                SQX_Regulatory_Report__c = regReport.Id
            );
            insert canadaReport;

            regReport.Report_Id__c = canadaReport.Id;
            update regReport;
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: User tries to set anticipated final report date for non preliminary report
     * THEN: Error is thrown
    */
    static testmethod void givenCanadaReport_WhenUserTriesToSetAnticipatedFinalReportDateForNonPreliminaryReport_ThenErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : Create a Canada report
            SQX_Canada_Report__c report = [SELECT Id FROM SQX_Canada_Report__c];
            report.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            report.Final_Report_Date__c = Date.today();

            // ACT : Set report type to anything other than Premliminary
            report.Type_Of_Report__c = TYPE_OF_REPORT_UPDATE;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prvt_Anticipated_Date_For_Final_Report), 'Expected error message not found');

            // ACT : Set report type to anything other than Premliminary
            report.Type_Of_Report__c = TYPE_OF_REPORT_FINAL;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prvt_Anticipated_Date_For_Final_Report), 'Expected error message not found');

            // ACT : Set report type to anything other than Premliminary
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY_AND_FINAL;

            Test.startTest();

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prvt_Anticipated_Date_For_Final_Report), 'Expected error message not found');

            // ACT : Set report type to Premliminary
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY;

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            // ACT : Validate the record
            Map<String, Object> validationInfo = SQX_Regulatory_Report_Submit_Controller.validateInSF(report.Id);

            // ASSERT : Validation should pass
            System.assertEquals(0, ((List<String>) validationInfo.get('errors')).size(), 'Expected no errors in validation');
            System.assertEquals(false, Boolean.valueOf(validationInfo.get('supportsXMLValidation')), 'Canada report should not support XML validation');

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: User tries to set previous report submission date for preliminary report type
     * THEN: Error is thrown
    */
    static testmethod void givenCanadaReport_WhenUserTriesToSetPreviousSubmissionDateForPreliminaryReport_ThenErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : Create a Canada report
            SQX_Canada_Report__c report = [SELECT Id FROM SQX_Canada_Report__c];
            report.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            report.Previous_Submitted_Date__c = Date.today();

            // ACT : Set report type to Premliminary
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevent_Prev_Sub_Date_For_Initial_Report), 'Expected error message not found');

            // ACT : Set report type to Premliminary & Final
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY_AND_FINAL;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevent_Prev_Sub_Date_For_Initial_Report), 'Expected error message not found');

            // ACT : Set report type to Final
            report.Type_Of_Report__c = TYPE_OF_REPORT_FINAL;
            report.Health_Canada_File_Number__c = 'HC File Number';
            report.Reporter_File_Number__c = 'R File Number';

            Test.startTest();

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            // ACT : Set report type to 'Update'
            report.Type_Of_Report__c = TYPE_OF_REPORT_UPDATE;

            result = Database.update(report, false);

            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: User tries to set device availability explanation when Availaiblity is known
     * THEN: Error is thrown
    */
    static testmethod void givenCanadaReport_WhenUserTriesToProvideDeviceAvailabilityExplanationWhenAvailabilityIsKnown_ThenErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : Create a Canada report
            SQX_Canada_Report__c report = [SELECT Id FROM SQX_Canada_Report__c];
            report.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;

            // ACT : Set device explanation as well as device availability to some known value
            report.Availability_Of_Device__c = DEVICE_AVAILABILITY_DESTROYED;
            report.Device_Availability_Explanation__c = 'Wonder why I am explaning';

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevt_Explanation_For_Known_Availability), 'Expected error message not found');

            // ACT : Set device explanation as well as device availability to some known value
            report.Availability_Of_Device__c = DEVICE_AVAILABILITY_RETURNED_TO_MFR_OR_IMPORTER;
            report.Device_Availability_Explanation__c = 'Wonder why I am explaning';

            Test.startTest();

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevt_Explanation_For_Known_Availability), 'Expected error message not found');

            // ACT : Set device explanation when device availability is unknown
            report.Availability_Of_Device__c = DEVICE_AVAILABILITY_NEITHER;
            report.Device_Availability_Explanation__c = 'Wonder why I am explaning';

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: User tries to set root cause for Preliminary or Update type reports
     * THEN: Error is thrown
    */
    static testmethod void givenCanadaReport_WhenUserTriesToSetRootCauseForNonFinalReports_ThenErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : Create a Canada report
            SQX_Canada_Report__c report = [SELECT Id FROM SQX_Canada_Report__c];
            report.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            report.Root_Cause__c = 'Negligence';

            // ACT : Set root cause for Non-final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevent_Root_Cause_For_Initial_Report), 'Expected error message not found');

            // ACT : Set root cause for Non-final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_UPDATE;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevent_Root_Cause_For_Initial_Report), 'Expected error message not found');

            // ACT : Set root cause for Preliminary & Final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY_AND_FINAL;

            Test.startTest();

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            // ACT : Set root cause for Final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_FINAL;
            report.Health_Canada_File_Number__c = 'HC File Number';
            report.Reporter_File_Number__c = 'R File Number';

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: User tries to set Corrective action for Preliminary or Update type reports
     * THEN: Error is thrown
    */
    static testmethod void givenCanadaReport_WhenUserTriesToSetCorrectiveActionForNonFinalReports_ThenErrorIsThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : Create a Canada report
            SQX_Canada_Report__c report = [SELECT Id FROM SQX_Canada_Report__c];
            report.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            report.Corrective_Actions__c = 'Fired the employee';

            // ACT : Set Corrective action for Non-final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevt_CorrectiveAction_In_Initial_Report), 'Expected error message not found');

            // ACT : Set Corrective action for Non-final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_UPDATE;

            result = Database.update(report, false);

            // ASSERT : Update should fail with validation error msg
            System.assertEquals(false, result.isSuccess(), 'Expected validation rule to prevent update');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), VALIDATION_ERR_MSG_Prevt_CorrectiveAction_In_Initial_Report), 'Expected error message not found');

            // ACT : Set Corrective action for Preliminary & Final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_PRELIMINARY_AND_FINAL;

            Test.startTest();

            result = Database.update(report, false);

            // ASSERT : Record should be updated
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            // ACT : Set Corrective action for Final report type
            report.Type_Of_Report__c = TYPE_OF_REPORT_FINAL;
            report.Health_Canada_File_Number__c = 'HC File Number';
            report.Reporter_File_Number__c = 'R File Number';

            result = Database.update(report, false);

            // ASSERT : Record should be inserted
            System.assertEquals(true, result.isSuccess(), 'Expected update to pass but got error ' + result);

            Test.stopTest();
        }
    }


    /**
     * GIVEN: A Canada Report
     * WHEN: It's PDF asset is generated (based on heroku payload)
     * THEN: Submission record is created
    */
    static testmethod void givenCanadaReport_WhenItsAssetIsGenerated_ThenSubmissionRecordHasToBeCreated() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminuser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){

            Database.SaveResult result;

            // ARRANGE : A Canada Report
            SQX_Canada_Report__c report = [SELECT Id, Name, SQX_Regulatory_Report__c FROM SQX_Canada_Report__c];

            // ACT: Invoke presetting method
            List<String> errors = SQX_Regulatory_Report_Submit_Controller.prepareRecordForSubmission(report.Id);

            // ASSERT: Submission date is set
            System.assertEquals(0, errors.size(), 'Expected no errors on submit but got ' + errors);

            report = [SELECT Id, Name, SQX_Regulatory_Report__c, Date_Submitted__c FROM SQX_Canada_Report__c WHERE Id =: report.Id];

            System.assertEquals(Date.today(), report.Date_Submitted__c, 'Expected submission date to be populated in Canada Report on submit');


            // ACT : Insert content which will induce submission record generation
            Test.startTest();

            ContentVersion cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { report.SQX_Regulatory_Report__c });
            cv.Title = 'Test-Title';
            cv.PathOnClient = report.Name + '.pdf';
            cv.VersionData = Blob.valueOf('This is the test data');
            insert cv;

            // ASSERT: Submission history is created.
            SQX_Submission_History__c subHistory = [SELECT Id, SQX_Canada_Report__c, Status__c, SQX_Regulatory_Report__c FROM SQX_Submission_History__c LIMIT 1];
            System.assertEquals(report.Id, subHistory.SQX_Canada_Report__c, 'Expected a link to be established between Canada Report and Submission record');
            System.assertEquals(SQX_Submission_History.STATUS_COMPLETE, subHistory.Status__c, 'Expected submission history record status to be complete');
            System.assertEquals(report.SQX_Regulatory_Report__c, subHistory.SQX_Regulatory_Report__c, 'Expected reg report to be linked with submission history record');

            // ASSERT: Content has been linked with the submission record
            ContentDocumentLink link = [SELECT Id FROM ContentDocumentLink where LinkedEntityId =: subHistory.Id];
            System.assertNotEquals(null, link);

            // Assert: Reg Report should have the new submission history linked
            SQX_Regulatory_Report__c regReport = [SELECT SQX_Submission_History__c, Status__c FROM SQX_Regulatory_Report__c WHERE Id =: report.SQX_Regulatory_Report__c];
            System.assertEquals(subHistory.Id, regReport.SQX_Submission_History__c, 'Expected submission history record to be linked with regulatory report');
            System.assertEquals(SQX_Regulatory_Report.STATUS_COMPLETE, regReport.Status__c, 'Expected reg report to be complete as well');

            // ACT : Update the Canada Report record
            report.Mfr_Name__c = 'Test';
            result = Database.update(report, false);

            // ASSERT: Canada Report record is locked for editing
            System.assertEquals(false, result.isSuccess(), 'Expected record update to fail after submission is complete');
            System.assertEquals(VALIDATION_ERR_MSG_Prevent_Record_Edit_Once_Locked, result.getErrors()[0].getMessage());

            // ACT: Mocking submission by inserting content version again despite the pending submission
            cv = new ContentVersion();
            cv.CQ_Actions__c= String.format(SQX_ContentVersion.CUSTOM_ACTION_ESUBMISSION, new String[] { report.SQX_Regulatory_Report__c });
            cv.Title = 'Test-Title';
            cv.PathOnClient = report.Name + '.pdf';
            cv.VersionData = Blob.valueOf('This is the test data');
            result = Database.insert(cv, false);

            // Assert: Error is thrown as pending submission already exists
            System.assertEquals(false, result.isSuccess());
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'A submission record already exists for the Regulatory Report.'), 'Unexpected error received ' + result.getErrors());

            Test.stopTest();
        }
    }

}