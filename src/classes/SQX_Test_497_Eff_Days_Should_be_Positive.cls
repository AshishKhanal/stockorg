/**
* Unit tests for SQX_Effectiveness_Review
*
* @author Sagar Shrestha
* @date 2014/08/04
* 
*/
@isTest
public class SQX_Test_497_Eff_Days_Should_be_Positive{

    /**
    *   Setup: Create A CAPA (C1)
    *   Action: Change Effective Monitoring Days to negative value
    *   Expected: Update should fail
    *
    * @author Sagar Shrestha
    * @date 2014/08/04
    * 
    */
    public static testmethod void addCAPA_EffectivenessMonitoringDaysShouldNotBeNegative(){

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser){
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility()
                                                .save();
            capa.CAPA.Effectiveness_Monitoring_Days__c=-100;

            Database.SaveResult negativeDaysResult = Database.Update(capa.CAPA,false);

            System.assert(negativeDaysResult.isSuccess() == false,
                 'Expected CAPA save to be failed as Eff Monitoring Days is updated to negative value'+capa.CAPA);

            capa.CAPA.Effectiveness_Monitoring_Days__c=0;

            Database.SaveResult zeroDaysResult = Database.Update(capa.CAPA,false);

            System.assert(zeroDaysResult.isSuccess() == true,
                 'Expected CAPA save to be success as Eff Monitoring Days is updated to zero'+capa.CAPA);

            capa.CAPA.Effectiveness_Monitoring_Days__c=30;

            Database.SaveResult positiveDaysResult = Database.Update(capa.CAPA,false);

            System.assert(positiveDaysResult.isSuccess() == true,
                 'Expected CAPA save to be success as Eff Monitoring Days is updated to positive value'+capa.CAPA);
        }
        

    } 
}