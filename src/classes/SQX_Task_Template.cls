/**
* @author Pradhanta Bhandari
* @date 2014/2/19
* @description Task templates for various purposes
*/
public with sharing class SQX_Task_Template {

    public enum TaskType{
        CAPAOpened,
        EffectivenessReviewToDo,
        CAPAClosureToDo,
        ImplementationCompletionToDo,
        ControlledDocReview,
        RegulatoryReportNeeded,
        ComplaintOpened,
        ActionOpened,
        InspectonCompleted,
        ImpactedDocumentOpened
    }


    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description returns a task template of a given type
    * @param related the object for which the task is being created
    * @param owner the user for whom the task is meant
    * @param contextVariables map of variables that are to be passed to template for replacing text with values
    * @param dueDate due date for the task
    * @param templateType the name of template
    * @param Task returns a task object based on the template
    */
    public static Task createTaskFor(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate, TaskType templateType){
        
        if(templateType == TaskType.CAPAOpened){
            return getCAPAOpenedTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.EffectivenessReviewToDo){
            return getEffectivenessReviewTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.CAPAClosureToDo){
            return getCAPAClosureTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.ControlledDocReview){
            return getControlledDocReviewTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.RegulatoryReportNeeded){
            return getRegulatoryReportNeeded(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.ComplaintOpened){
            return getComplaintOpenedTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.ActionOpened){
            return getActionOpenedTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.InspectonCompleted){
            return getInspectionCompletedTask(related, ownerId, contextVariables, dueDate);
        }
        else if(templateType == TaskType.ImpactedDocumentOpened){
            return getImpactedDocumentOpenedTask(related, ownerId, contextVariables, dueDate);
        }
        //else if(templateType == TaskType.ImplementationCompletionToDo){
        //    return getImplementationTodoTask(related, ownerId, contextVariables, dueDate);
        //}
        else{
            System.assert(false, 'Undefined type of task found ' + templateType);
        }

        return null;
    }
    
    /**
    * Creates a SF task related to complaint that is being sent in related field.
    * @param related the object that is related to the task
    * @param ownerId the owner of the task
    * @contextVariables map of additional values that are to be sent to use to configure the task params
    * @dueDate the date the task is due.
    * @return returns the task using the defined template.
    */
    private static Task getRegulatoryReportNeeded(SObject related, Id ownerId, Map<String, SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Regulatory_Report__c, 'Expected related object to be of type Complaint doc.');

        Id taskRelatedID = (Id) related.Id;
        SQX_Regulatory_Report__c report = (SQX_Regulatory_Report__c) related;

        Task task = new Task(
            WhatId = report.SQX_Complaint__c,
            OwnerId = ownerId,
            Subject = String.format(SQX_Task.TASK_SUBJECT_REGULATORY_REPORT, new List<String> { report.Name }),
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
        );


        return task;
    }



    /**
    * Creates a SF task related to complaint that is being sent in related field.
    * @param related the object that is related to the task
    * @param ownerId the owner of the task
    * @contextVariables map of additional values that are to be sent to use to configure the task params
    * @dueDate the date the task is due.
    * @return returns the task using the defined template.
    */
    private static Task getComplaintOpenedTask(SObject related, Id ownerId, Map<String, SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Complaint__c, 'Expected related object to be of type Complaint doc.');

        Id taskRelatedID = (Id) related.Id;
        SQX_Complaint_Task__c complaintTask = (SQX_Complaint_Task__c) contextVariables.get('Task');

        Task task = new Task(
            WhatId = taskRelatedID,
            OwnerId = ownerId,
            Subject = complaintTask.Name,
            Description = complaintTask.Task_Description__c,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED,
            Child_What_Id__c = complaintTask.Id
        );


        return task;
    }

    /**
    * Creates a SF task related to action that is being sent in related field.
    * @param related the object that is related to the task
    * @param ownerId the owner of the task
    * @contextVariables map of additional values that are to be sent to use to configure the task params
    * @dueDate the date the task is due.
    * @return returns the task using the defined template.
    */
    private static Task getActionOpenedTask(SObject related, Id ownerId, Map<String, SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Change_Order__c, 'Expected related object to be of type Change Order.');

        Id taskRelatedID = (Id) related.Id;
        SQX_Implementation__c actionTask = (SQX_Implementation__c) contextVariables.get('Task');
        
        Task task = new Task(
            WhatId = taskRelatedID,
            OwnerId = ownerId,
            Subject = actionTask.Title__c,
            Description = actionTask.Description__c,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED,
            Child_What_Id__c = actionTask.Id
        );


        return task;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2015/6/10
    * @description returns a task that is added when the doc is to be reviewed.
    */
    private static Task getControlledDocReviewTask(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Controlled_Document__c, 'Expected related object to be of type Controlled Doc but found it as ' + related);

        Id taskRelatedID = (Id) related.get('Id');        

        Task task = new Task(
            WhatId = taskRelatedID,
            OwnerId = ownerId,
            Subject = SQX_Task.TASK_SUBJECT_DOCUMENT_REVIEW,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
            );

        return task;
    }



    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description returns a task stating that CAPA has been opened against a supplier by a customer
    */
    private static Task getCAPAOpenedTask(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Capa__c, 'Expected related object to be of type cAPA but found it as ' + related);

        SQX_Capa__c capa = (SQX_Capa__c)related;

        String tasktype = 'CAPA';
        Id taskRelatedID = CAPA.Id;        

        Task task = new Task(
            WhatId = taskRelatedID,
            OwnerId = ownerId,
            Subject = SQX_Task.TASK_SUBJECT_OPEN_CAPA,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
            );

        return task;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description returns a task stating that a effectiveness review is due
    * @lastmodifiedby Pradhanta [2014/2/25]
    */
    private static Task getEffectivenessReviewTask(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Effectiveness_Review__c, 
        'Expected related object to be of type effectiveness review but found it as ' + related);

        SQX_Effectiveness_Review__c review = (SQX_Effectiveness_Review__c )related;
        //changed from finding to review
        //because each individiual review will have a related task
        //described in SQX-169
        //modified by Pradhanta [2014/2/25]
    
        Task task = new Task(
            WhatId = review.Id,
            OwnerId = ownerId,
            Subject = SQX_Task.TASK_SUBJECT_EFFECTIVENESS_REVIEW,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
            );

        return task;
    }



    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description returns a task stating that a closure of CAPA is due
    */
    private static Task getCAPAClosureTask(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Capa__c, 'Expected related object to be of type CAPA but found it as ' + related);

        SQX_Capa__c capa = (SQX_Capa__c)related;

        Id taskRelatedID = capa.Id;

        Task task = new Task(
            WhatId = taskRelatedID,
            OwnerId = ownerId,
            Subject = SQX_Task.TASK_SUBJECT_CLOSURE_REVIEW,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
        );

        return task;
    }
    
    /**
    * Creates a SF task related to inspection that is being sent in related field.
    * @param related the object that is related to the task
    * @param ownerId the owner of the task
    * @contextVariables map of additional values that are to be sent to use to configure the task params
    * @dueDate the date the task is due.
    * @return returns the task using the defined template.
    */
    private static Task getInspectionCompletedTask(SObject related, Id ownerId, Map<String, SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Inspection__c, 'Expected related object to be of type inspection doc.');

        SQX_Inspection__c inspection = (SQX_Inspection__c) related;
        String taskTitle = Label.CQ_UI_Closure_Review_Task_For_Inspection;
        taskTitle = taskTitle.replace('{inspectionNumber}', inspection.Name);
        Task task = new Task(
            WhatId = inspection.Id,
            OwnerId = ownerId,
            Subject = taskTitle,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
        );


        return task;
    }
    
    /**
    * Creates a SF task related to inspection that is being sent in related field.
    * @param related the object that is related to the task
    * @param ownerId the owner of the task
    * @contextVariables map of additional values that are to be sent to use to configure the task params
    * @dueDate the date the task is due.
    * @return returns the task using the defined template.
    */
    private static Task getImpactedDocumentOpenedTask(SObject related, Id ownerId, Map<String, SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Impacted_Document__c, 'Expected related object to be of type inspection doc.');

        SQX_Impacted_Document__c impactedDocument = (SQX_Impacted_Document__c) related;
        String taskTitle = Label.CQ_UI_Please_Completed_Impacted_Document;
        taskTitle = taskTitle;
        Task task = new Task(
            WhatId = impactedDocument.SQX_CAPA__c,
            Child_What_Id__c = impactedDocument.Id,
            OwnerId = ownerId,
            Subject = taskTitle,
            ActivityDate = dueDate,
            Priority = SQX_Task.PRIORITY_NORMAL,
            Status = SQX_Task.STATUS_NOT_STARTED
        );


        return task;
    }

    /* * disabled because implementation has to be removed.
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description returns a task stating that an implementation has to be completed by the supplier
    */
    /*
    private static Task getImplementationTodoTask(SObject related, Id ownerId, Map<String,SObject> contextVariables, Date dueDate){
        System.assert(related instanceof SQX_Implementation__c , 'Expected related object to be of type implementation but found it as ' + related);

        SQX_Implementation__c implementation = (SQX_Implementation__c )related;

        /*System.assert(contextVariables != null && contextVariables.containsKey('Investigation') 
             && contextVariables.get('Investigation') != null &&
             contextVariables.get('Investigation') instanceof SQX_Investigation__c,
                'Expected a contextVariables to be set to the investigation but found none. Debug: ' + contextVariables);


        SQX_Investigation__c investigation = (SQX_Investigation__c)contextVariables.get('Investigation');
        * /

        Task task = new Task(
            WhatId = implementation.Id,
            OwnerId = ownerId,
            Subject = 'Implementation is pending',
            ActivityDate = dueDate,
            Priority = 'Normal', 
            Status = 'Not Started'
            );

        return task;
    } */

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/19
    * @description this method checks whether a given type of task exists for a specific object. It should be called from test methods only
    *              this is so because it uses text based matching. If using production code to check if a task has been created by system
    *              query the task tracker object
    * @param related the related object id
    * @param taskFor task for the user with id
    * @param typeOfTask the type of task to validate
    * @return returns <code>true</code> if a task of type is found else returns <code>false</code>
    * @lastmodified Pradhanta [2014/2/25]
    */
    public static boolean taskExistsFor(SObject related, Id taskFor, TaskType typeOfTask){
        boolean exists = false;
        String textToMatch = '';

        if(typeOfTask == TaskType.CAPAOpened){
            textToMatch = SQX_Task.TASK_SUBJECT_OPEN_CAPA;
        }
        else if(typeOfTask == TaskType.EffectivenessReviewToDo){
            
            //was initially designed for review task to be assigned to finding
            //thats why finding is being passed
            //moreover provides a central check mechanism where a review is asserted to exist
            //modified by Pradhanta [2014/2/25]
            List<SQX_Effectiveness_Review__c> reviews = [SELECT Id, Effectiveness_Reviewer__c FROM SQX_Effectiveness_Review__c  WHERE SQX_Capa__c = : (Id)related.get('Id') ];
            
            if(reviews.size() == 0)
                return false;
            
            related = reviews.get(0);
            taskFor = reviews.get(0).Effectiveness_Reviewer__c;
            textToMatch = SQX_Task.TASK_SUBJECT_EFFECTIVENESS_REVIEW;
        }
        else if(typeOfTask == TaskType.CAPAClosureToDo){
            textToMatch = SQX_Task.TASK_SUBJECT_CLOSURE_REVIEW;
        }
        // else if(typeOfTask == TaskType.ImplementationCompletionToDo){
        //     textToMatch = 'Implementation is pending';
        // }
        else{
            System.assert(false, 'Undefined type of task found ' + typeOfTask);
        }

        List<Task> tasks = [SELECT Id, Subject
                          FROM Task
                          WHERE WhatId = : (Id)related.get('Id')
                          AND OwnerId = : taskFor];
                          //can't filter using SOQL because SF doesnt support
                          //query on long text fields.
        
        for(Task t: tasks){
            if(t.subject.startsWith(textToMatch)){
                exists = true;
                break;
            }
        }
        

        return exists;
    }




}