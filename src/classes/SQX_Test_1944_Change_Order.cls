/**
* Unit tests for:
* i.    creation of change order approval using approval matrix setup for a change order when opened [givenPopulateApprovalMatrixApprovalsWhenOpen]
* ii.   setting of last approval status when approval status is recalled [givenSetLastApprovalStatusWhenRecalled]
* iii.  validation for any empty user value in change order approvals of a change order [givenCheckAnyEmptyApproverWhenSubmittingForApproval]
* iv.   validation for any step in change order approvals exceeding maximum approvers i.e. 5 [givenCheckAnyStepExceedingMaximumApproversWhenSubmittingForApproval]
* v.    setting of all 5 approver fields in change order object for a current approval step [givenCheckApproverFieldsProperlySetForCurrentApprovalStepWhenInApproval]
* vi.   Ensures that validation rule for approval matrix steps are correctly set [givenApprovalMatrixStep_ValidationWorks]
*       a. When both job function and user isn't provided error is thrown
*       b. When both job function and user is provided error is thrown
*       c. When only job function is provided no error is thrown
*       d. When only user is provided no error is thrown
*/
@IsTest
public class SQX_Test_1944_Change_Order {
    
    /*
    * ensures change order approvals are populated using linked approval matrix when status is changed to open and no approvals exists
    * ensures active approver is set when only one approver exists for a job function
    * related method: SQX_Change_Order.copyApprovalMatrixStepsWhenOpen()
    */
    public static testmethod void givenPopulateApprovalMatrixApprovalsWhenOpen() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // add required users as approvers
        List<User> approvers = new List<User>();
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        
        System.runas(user1) {
            // add required job functions
            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs = SQX_Test_Job_Function.createJFs(3, true);
            insert jfs;
            
            // add required personnel records
            List<SQX_Personnel__c> psns = new List<SQX_Personnel__c>();
            psns.add(SQX_Personnel.copyFieldsFromUser(approvers[0], new SQX_Personnel__c())); psns[0].Active__c = true;
            psns.add(SQX_Personnel.copyFieldsFromUser(approvers[1], new SQX_Personnel__c())); psns[1].Active__c = true;
            psns.add(SQX_Personnel.copyFieldsFromUser(approvers[2], new SQX_Personnel__c())); psns[2].Active__c = true;
            psns.add(SQX_Personnel.copyFieldsFromUser(approvers[3], new SQX_Personnel__c())); psns[3].Active__c = true;
            psns.add(SQX_Personnel.copyFieldsFromUser(approvers[4], new SQX_Personnel__c())); psns[4].Active__c = true;
            insert psns;
            
            // add required PJF records
            List<SQX_Personnel_Job_Function__c> pjfs = new List<SQX_Personnel_Job_Function__c>();
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = psns[0].Id, SQX_Job_Function__c = jfs[0].Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = psns[1].Id, SQX_Job_Function__c = jfs[1].Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = psns[2].Id, SQX_Job_Function__c = jfs[1].Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = psns[3].Id, SQX_Job_Function__c = jfs[2].Id, Active__c = true ));
            pjfs.add(new SQX_Personnel_Job_Function__c( SQX_Personnel__c = psns[4].Id, SQX_Job_Function__c = jfs[2].Id, Active__c = true ));
            insert pjfs;
            
            // add required approval matrix
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'Approval Matrix 1' );
            insert am1;
            
            // add approval matrix steps
            List<SQX_Approval_Matrix_Step__c> amSteps = new List<SQX_Approval_Matrix_Step__c>();
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[0].Id, Step__c = 1 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[1].Id, Step__c = 2 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[2].Id, Step__c = 3 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[2].Id, Step__c = 3 ));

            // SQX-2057 test
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_User__c = user1.Id, Step__c = 4 ));
            insert amSteps;
            
            // add required draft change order
            SQX_Change_Order__c co1 = (new SQX_Test_Change_Order()).changeOrder;
            co1.SQX_Approval_Matrix__c = am1.Id;
            insert co1;
            
            // ACT: update change order status to open when no approvals exists
            co1.Status__c = SQX_Change_Order.STATUS_OPEN;
            update co1;
            
            Map<Id, SQX_Change_Order_Approval__c> coApprovals = new Map<Id, SQX_Change_Order_Approval__c>(
                [SELECT Id, Name, Step__c, SQX_Job_Function__c, SQX_User__c FROM SQX_Change_Order_Approval__c WHERE SQX_Change_Order__c = :co1.Id]
            );
            
            System.assertEquals(5, coApprovals.size());

            
            Set<Id> mappedIds = new Set<Id>();
            for (SQX_Approval_Matrix_Step__c ams : amSteps) {
                SQX_Change_Order_Approval__c mappedTo;
                
                for (Id coaId : coApprovals.keySet()) {
                    SQX_Change_Order_Approval__c coa = coApprovals.get(coaId);
                    if (coa.Step__c == ams.Step__c && !mappedIds.contains(coaId)){
                        if(    (coa.SQX_Job_Function__c == ams.SQX_Job_Function__c && coa.SQX_Job_Function__c != null)
                            || (coa.SQX_User__c == ams.SQX_User__c && coa.SQX_User__c != null) ){

                            mappedTo = coa;
                            mappedIds.add(coaId);
                            break;
                        }
                    }
                }
                
                System.assert(mappedTo != null, 'Approval matrix step is expected to be populated to change order approval list for ' + ams + coApprovals);
                
                if (ams.SQX_Job_Function__c == jfs[0].Id || ams.SQX_User__c != null) {
                    System.assert(mappedTo.SQX_User__c != null, 'User of the change order approval is expected to be set for job function with only one active user.');
                } else {
                    System.assert(mappedTo.SQX_User__c == null, 'User of the change order approval is not expected to be set for job function with many active users.');
                }
            }
            
            
            
            // setting change order status to status other than open
            co1.Status__c = SQX_Change_Order.STATUS_IMPLEMENTATION;
            update co1;
            
            // ACT: update change order status to open when approvals exists
            co1.Status__c = SQX_Change_Order.STATUS_OPEN;
            update co1;
            
            List<SQX_Change_Order_Approval__c> coApprovals2 = [SELECT Id, Name FROM SQX_Change_Order_Approval__c WHERE SQX_Change_Order__c = :co1.Id];
            
            System.assertEquals(5, coApprovals.size());
            
            for (SQX_Change_Order_Approval__c coa : coApprovals2) {
                System.assert(coApprovals.get(coa.Id) != null, 'New change order approval is not expected.');
            }
        }
    }
    
    /*
    * ensures last approval status is set when approval status is set to recalled
    * related method: SQX_Change_Order.setApprovalStatusToLastValueWhenRecalled()
    */
    public static testmethod void givenSetLastApprovalStatusWhenRecalled() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required draft change order with last approval status
            SQX_Change_Order__c co1 = (new SQX_Test_Change_Order()).changeOrder;
            co1.Last_Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_REPLAN;
            insert co1;
            
            // ACT: update approval status to recalled
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_RECALLED;
            update co1;
            
            co1 = [SELECT Id, Approval_Status__c FROM SQX_Change_Order__c WHERE Id = :co1.Id];
            
            System.assertEquals(SQX_Change_Order.APPROVAL_STATUS_PLAN_REPLAN, co1.Approval_Status__c,
                'Approval Status is expected to be set to last approval status when recalled.');
            
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: update approval status to status other than recalled
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED;
            update co1;
            
            co1 = [SELECT Id, Approval_Status__c FROM SQX_Change_Order__c WHERE Id = :co1.Id];
            
            System.assertEquals(SQX_Change_Order.APPROVAL_STATUS_PLAN_APPROVED, co1.Approval_Status__c,
                'Approval Status is not expected to be set to last approval status when not recalled.');
        }
    }
    
    /*
    * ensures to check for any empty user in change order approvals when change order is submitted for approval
    * related method: SQX_Change_Order.checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval()
    */
    public static testmethod void givenCheckAnyEmptyApproverWhenSubmittingForApproval() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // add required users as approvers
        List<User> approvers = new List<User>();
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        
        System.runas(user1) {
            // add required draft change order
            SQX_Change_Order__c co1 = (new SQX_Test_Change_Order()).changeOrder;
            insert co1;
            
            // add required change order approvals with some empty approver
            List<SQX_Change_Order_Approval__c> coApprovals = new List<SQX_Change_Order_Approval__c>();
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[0].Id ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[1].Id ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 3 ));
            insert coApprovals;
            
            // ACT: update approval status to Plan Approval
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            Database.SaveResult result1 = Database.update(co1, false);
            
            System.assert(result1.isSuccess() == false, 'Change order is not expected to be saved when submitted for approval with empty approver(s).');
            
            String expectedErrMsg = Label.SQX_ERR_MSG_CHANGE_ORDER_APPROVALS_WITH_EMPTY_APPROVER;
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg),
                'Error message for empty user/approver is expected when change order is submitted for approval.');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: update approval status to Implementation Approval
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            result1 = Database.update(co1, false);
            
            System.assert(result1.isSuccess() == false, 'Change order is not expected to be saved when submitted for approval with empty approver(s).');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg),
                'Error message for empty user/approver is expected when change order is submitted for approval.');
        }
    }
    
    /*
    * ensures to check for any change order approval step exceeding maximum number of approvers (5) when change order is submitted for approval i.e. Approval Status = Plan Approval
    * related method: SQX_Change_Order.checkAnyEmptyApproverOrStepExceedingMaximumApproversWhenSubmittingForApproval()
    */
    public static testmethod void givenCheckAnyStepExceedingMaximumApproversWhenSubmittingForApproval() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // add required users as approvers
        List<User> approvers = new List<User>();
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        
        System.runas(user1) {
            // add required draft change order
            SQX_Change_Order__c co1 = (new SQX_Test_Change_Order()).changeOrder;
            insert co1;
            
            // add required change order approvals with a step exceeding max approvers
            List<SQX_Change_Order_Approval__c> coApprovals1 = new List<SQX_Change_Order_Approval__c>();
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[0].Id ));
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[1].Id ));
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[2].Id ));
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[3].Id ));
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[4].Id ));
            coApprovals1.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = approvers[5].Id ));
            insert coApprovals1;
            
            // ACT: update approval status to Plan Approval
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            Database.SaveResult result1 = Database.update(co1, false);
            
            System.assert(result1.isSuccess() == false, 'Change order is not expected to be saved when submitted for approval with a step exceeding maximum 5 approvers.');
            
            String expectedErrMsg = 'Change Order Approvals with Step number(s) [1] exceeds maximum 5 approvers limit for each step.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg),
                'Error message for a step exceeding maximum 5 approvers is expected when change order is submitted for approval.');
            
            
            // add required change order approvals with another step exceeding max approvers
            List<SQX_Change_Order_Approval__c> coApprovals2 = new List<SQX_Change_Order_Approval__c>();
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[0].Id ));
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[1].Id ));
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[2].Id ));
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[3].Id ));
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[4].Id ));
            coApprovals2.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = approvers[5].Id ));
            insert coApprovals2;
            
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: update approval status to Plan Approval
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_PLAN_APPROVAL;
            result1 = Database.update(co1, false);
            
            System.assert(result1.isSuccess() == false, 'Change order is not expected to be saved when submitted for approval with any step exceeding maximum 5 approvers.');
            
            expectedErrMsg = 'Change Order Approvals with Step number(s) [1, 2] exceeds maximum 5 approvers limit for each step.';
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg),
                'Error message for steps exceeding maximum 5 approvers is expected when change order is submitted for approval.');
            
            
            // clear processed entries
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: update approval status to Plan Approval
            co1.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            result1 = Database.update(co1, false);
            
            System.assert(result1.isSuccess() == false, 'Change order is not expected to be saved when submitted for approval with any step exceeding maximum 5 approvers.');
            
            System.assert(SQX_Utilities.checkErrorMessage(result1.getErrors(), expectedErrMsg),
                'Error message for steps exceeding maximum 5 approvers is expected when change order is submitted for approval.');
        }
    }
    
    /*
    * sets current approval step and approval status to in approval and updates change order
    * then tests for expected approvers
    */
    private static void testCurrentApprovalStepApprovers(SQX_Change_Order__c co, Decimal currentApprovalStep, List<Id> expectedApprovers) {
        // clear processed entries
        SQX_BulkifiedBase.clearAllProcessedEntities();
        
        // ACT: update current approval step
        co.Current_Approval_Step__c = currentApprovalStep;
        update co;
        
        // get change order record
        SQX_Change_Order__c co1 = [SELECT Id, Approval_Status__c, Has_All_Approvers__c, Current_Approval_Step__c, SQX_Approver_1__c, SQX_Approver_2__c, SQX_Approver_3__c, SQX_Approver_4__c, SQX_Approver_5__c
                                   FROM SQX_Change_Order__c WHERE Id = :co.Id];
        
        // ensure current approval step
        System.assertEquals(currentApprovalStep, co.Current_Approval_Step__c);
        
        Set<Id> uniqueCOApprovers = new Set<Id>();
        if (co1.SQX_Approver_1__c != null) {
            uniqueCOApprovers.add(co1.SQX_Approver_1__c);
        }
        if (co1.SQX_Approver_2__c != null && !uniqueCOApprovers.contains(co1.SQX_Approver_2__c)) {
            uniqueCOApprovers.add(co1.SQX_Approver_2__c);
        }
        if (co1.SQX_Approver_3__c != null && !uniqueCOApprovers.contains(co1.SQX_Approver_3__c)) {
            uniqueCOApprovers.add(co1.SQX_Approver_3__c);
        }
        if (co1.SQX_Approver_4__c != null && !uniqueCOApprovers.contains(co1.SQX_Approver_4__c)) {
            uniqueCOApprovers.add(co1.SQX_Approver_4__c);
        }
        if (co1.SQX_Approver_5__c != null && !uniqueCOApprovers.contains(co1.SQX_Approver_5__c)) {
            uniqueCOApprovers.add(co1.SQX_Approver_5__c);
        }
        
        Set<Id> expectedUniqueApprovers = new Set<Id>();
        if (expectedApprovers != null)
            expectedUniqueApprovers = new Set<Id>(expectedApprovers);
        
        // ensure all unique approvers for a step defined in change order approvals are set
        System.assertEquals(expectedUniqueApprovers.size(), uniqueCOApprovers.size());
        
        // ensure all approver fields are set
        Boolean hasAllApprovers = uniqueCOApprovers.size() > 0;
        System.assertEquals(hasAllApprovers, co1.Has_All_Approvers__c);
    }
    
    /*
    * ensures approver fields are set properly for current approval step when change order is in approval
    * related method: SQX_Change_Order.setApproversForCurrentApprovalStep()
    */
    public static testmethod void givenCheckApproverFieldsProperlySetForCurrentApprovalStepWhenInApproval() {
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        // add required users as approvers
        List<User> approvers = new List<User>();
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        approvers.add(SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole));
        
        System.runas(user1) {
            // add required draft change order
            SQX_Change_Order__c co1 = (new SQX_Test_Change_Order()).changeOrder;
            insert co1;
            
            // map approvers to steps with a skipped step
            Map<Decimal, List<Id>> stepApprovers = new Map<Decimal, List<Id>>{
                1 => new List<Id>{ approvers[0].Id, approvers[1].Id, approvers[2].Id, approvers[3].Id, approvers[4].Id },
                2 => new List<Id>{ approvers[0].Id, approvers[1].Id, approvers[3].Id, approvers[4].Id },
                3 => new List<Id>{ approvers[0].Id, approvers[2].Id, approvers[4].Id },
                4 => new List<Id>{ approvers[1].Id, approvers[3].Id },
                5 => new List<Id>{ approvers[4].Id },
                6 => new List<Id>{ approvers[5].Id },
                8 => new List<Id>{ approvers[2].Id }
            };
            
            // add required change order approvals
            List<SQX_Change_Order_Approval__c> coApprovals = new List<SQX_Change_Order_Approval__c>();
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = stepApprovers.get(1).get(4) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = stepApprovers.get(1).get(3) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = stepApprovers.get(1).get(1) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = stepApprovers.get(1).get(0) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 1, SQX_User__c = stepApprovers.get(1).get(2) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = stepApprovers.get(2).get(3) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = stepApprovers.get(2).get(1) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = stepApprovers.get(2).get(2) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 2, SQX_User__c = stepApprovers.get(2).get(0) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 3, SQX_User__c = stepApprovers.get(3).get(2) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 3, SQX_User__c = stepApprovers.get(3).get(1) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 3, SQX_User__c = stepApprovers.get(3).get(0) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 4, SQX_User__c = stepApprovers.get(4).get(0) ));
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 4, SQX_User__c = stepApprovers.get(4).get(1) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 5, SQX_User__c = stepApprovers.get(5).get(0) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 6, SQX_User__c = stepApprovers.get(6).get(0) ));
            
            coApprovals.add(new SQX_Change_Order_Approval__c( SQX_Change_Order__c = co1.Id, Step__c = 8, SQX_User__c = stepApprovers.get(8).get(0) ));
            
            insert coApprovals;
            
            testCurrentApprovalStepApprovers(co1, 1, stepApprovers.get(1));
            testCurrentApprovalStepApprovers(co1, 2, stepApprovers.get(2));
            testCurrentApprovalStepApprovers(co1, 3, stepApprovers.get(3));
            testCurrentApprovalStepApprovers(co1, 4, stepApprovers.get(4));
            testCurrentApprovalStepApprovers(co1, 5, stepApprovers.get(5));
            testCurrentApprovalStepApprovers(co1, 6, stepApprovers.get(6));
            testCurrentApprovalStepApprovers(co1, 7, stepApprovers.get(7));
            testCurrentApprovalStepApprovers(co1, 8, stepApprovers.get(8));
        }
    }


    /**
    * This rule checks if validation rules for approval matrix steps are configured properly
    *   a. When both job function and user isn't provided error is thrown
    *   b. When both job function and user is provided error is thrown
    *   c. When only job function is provided no error is thrown
    *   d. When only user is provided no error is thrown
    */
    static testmethod void givenApprovalMatrixStep_ValidationWorks(){
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        System.runas(user1){

            List<SQX_Job_Function__c> jfs = new List<SQX_Job_Function__c>();
            jfs.add(new SQX_Job_Function__c( Name = 'job function 1' ));
            jfs.add(new SQX_Job_Function__c( Name = 'job function 2' ));
            jfs.add(new SQX_Job_Function__c( Name = 'job function 3' ));
            insert jfs;
            

            // add required approval matrix
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'Approval Matrix 1' );
            new SQX_DB().op_insert(new List<SObject> { am1 }, new List<SObjectField> { });
            
            // add approval matrix steps
            List<SQX_Approval_Matrix_Step__c> amSteps = new List<SQX_Approval_Matrix_Step__c>();
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[0].Id, SQX_User__c = user1.Id, Step__c = 1 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, Step__c = 2 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_Job_Function__c = jfs[2].Id, Step__c = 3 ));
            amSteps.add(new SQX_Approval_Matrix_Step__c( SQX_Approval_Matrix__c = am1.Id, SQX_User__c = user1.Id, Step__c = 3 ));

            Database.SaveResult [] results = new SQX_DB().continueOnError().op_insert(amSteps, new List<SObjectField> {
                    Schema.SQX_Approval_Matrix_Step__c.SQX_Job_Function__c,
                    Schema.SQX_Approval_Matrix_Step__c.Step__c,
                    Schema.SQX_Approval_Matrix_Step__c.SQX_User__c 
                });

            System.assert(!results[0].isSuccess(), 'Expected the save to fail but went through');
            System.assert(!results[1].isSuccess(), 'Expected the save to fail but went through');
            System.assert(results[2].isSuccess(), 'Expected the save to go through but failed ' + results[2]);
            System.assert(results[3].isSuccess(), 'Expected the save to go through but failed ' + results[3]);
    
        }
        
    }
}