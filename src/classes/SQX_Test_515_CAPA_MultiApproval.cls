/**
 * This class ensures that multiple approval tests are met
 * As a CAPA Owner,
 * I want to have additional users approve responses that contain investigations, implementations and effectiveness reviews
 * @story SQX-515
 */
@IsTest
public class SQX_Test_515_CAPA_MultiApproval{


    // Mock parameter for response submission
    static final Map<String, String> params = new Map<String, String> {
                'comment' =>'Mock comment', 
                'purposeOfSignature' =>'Mock purposeOfSignature',  
                'nextAction' => 'submitresponse'};

    /*
    1.
    Given: that a CAPA Is Open
    And: investigation approval is required
    And: different user than CAPA owner is assigned for investigation approval
    And: response contains investigation(s)
    When: Response is submitted
    Then: Both CAPA Owner and Investigation approver will receive approval request at the same time -- Configured using SF approval process, no further testing necessary

    2.
    Given: that a CAPA Is Open
    And: Implementation Actions require approval (pre/post)
    And: different user than CAPA owner is assigned for Implementation approval
    And: response contains Implementation Actions (pre/post)
    When: Response is submitted
    Then: Both CAPA Owner and Implementation approver will receive approval request at the same time -- Configured using SF approval process, no further testing necessary

    3.
    Given: that a CAPA Is Open
    And: Implementation Actions require approval (pre/post)
    And Investigation Approval is required
    And: response contains Investigation(s) and Implementation Actions (pre/post)
    And: different users than CAPA owner is assigned for Investigation and Implementation approval
    When: Response is submitted
    Then: CAPA Owner, Investigation and Implementation approver will receive approval request at the same time -- Configured using SF approval process, no further testing necessary

    4.
    Given: that a CAPA Is Open
    And: Implementation Actions require approval (pre/post)
    And Investigation Approval is required
    And: response contains Investigation(s) and Implementation Actions (pre/post)
    And: same user other than CAPA Owner is assigned for Investigation and Implementation approval
    When: Response is submitted
    Then: CAPA Owner, Investigation/Implementation approver will receive approval request at the same time. There will only be 1 approval request for each user 
    -- Handled by SF, duplicate requests aren't sent, no further testing necessary

    5.
    Given: that a response is routed to multiple people for approval
    When: any one approver rejects the response 
    Then: The response and its approve-able inclusions are considered rejected and would no longer wait for additional approvers
    -- Configured using SF approval process, as 'Unanimous approval', single rejection will update the approval status fields to 'Rejected' thereby triggering rejection action

    6.
    Given: that a response is routed to multiple people for approval
    When: all approvers accept the response 
    Then: The response and its inclusions are considered accepted and move to subsequent step per workflow
    -- Configured using SF approval process, as 'Unanimous approval', single rejection will update the approval status fields to 'Rejected' thereby triggering rejection action

    7.
    Given: that a response is routed to multiple people for approval
    When: any one approver partially approves the response by rejecting select response inclusions
    Then: The rejected response inclusions are locked for further approval. Subsequent approvers can only partially approve the response. 
    And: Each approver will be able to add remark to each response inclusion.
    -- Configured using SF approval process, as 'Unanimous approval', single rejection will update the approval status fields to 'Rejected' thereby triggering rejection action

    8.
    Given: that a response is routed for approval
    When: one or more approver has to sign off on the response
    Then: Approver delegates for the user may signoff on the response
    And: System will track who signed off and who was originally assigned for approval

    */

    /* test run support */
    static boolean  runAllTests = true,
                    run_givenACapaResponse_AllApprovalApprovesResponse = false,
                    run_givenACapaResponse_SingleRejectionRejectsResponse = false,
                    run_givenAResponseIsPublished_PublishedDateIsSet = false,
                    run_givenACapaResponse_WhenCapaIsVoided_ResponseIsRecalled = false;

    /**
    * ensures that a given a CAPA response is sent to multiple approval, when both approve the inclusion is approved
    */
    public testmethod static void givenACapaResponse_AllApprovalApprovesResponse(){

        if(!runAllTests && !run_givenACapaResponse_AllApprovalApprovesResponse)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User investigationApprover = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER , null);
        DateTime startTime = DateTime.now();
        
        System.runas(capaOwner){
            Account supplier = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(supplier);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
        
            SQX_Test_CAPA_Utility capa= new  SQX_Test_CAPA_Utility(true, capaOwner, primaryContactUser).save();
            capa.capa.Investigation_Approval__c = true;
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();

            /* construct a response and submit for approval */
            System.runas(primaryContactUser){
                ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
                SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
                controller.initializeTemporaryStorage();
            
            
                
                SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.capa.Id,
                                                                              Response_Summary__c = 'Response');
                
                SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_CAPA__c = capa.capa.Id,
                                                                             Investigation_Summary__c = 'Is');
                
                SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');
                
                Map<String,Object>  changeSet = new Map<String, Object>(),
                                    responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                    investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                    inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped
                
                //set virtual IDSs
                responseUT.put('Id', 'Response-1');
                investigationUT.put('Id', 'Investigation-1');
                inclusion2UT.put('Id', 'Inclusion-2');
                
                inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
                inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
                
                List<Object> changes = new List<Object>();
                
                changes.add(responseUT);
                changes.add(investigationUT);
                changes.add(inclusion2UT);
                
                changeSet.put('changeSet', changes);
            
            
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
            
                System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_Finding__c = : capa.capa.SQX_Finding__c].size());
            }

            /* ensure we are not very close to the limit */
            System.assert(((double)Limits.getDMLStatements() / Limits.getLimitDMLStatements()) < 0.5,
             'Limit too close ' + Limits.getDMLStatements() + ' out of ' + Limits.getLimitDMLStatements() );

            SQX_Finding_Response__c response = [SELECT Id, Status__c, (SELECT Id, SQX_Investigation__c FROM SQX_Response_Inclusions__r ) FROM SQX_Finding_Response__c WHERE SQX_CAPA__c =: capa.capa.Id ];
            SQX_Response_Inclusion__c inclusion1 = response.SQX_Response_Inclusions__r.get(0); //investigation response inclusion

            Test.startTest();
                /* approve as capa owner */
                System.runas(capaOwner){

                    SQX_BulkifiedBase.clearAllProcessedEntities();
                    
                    //approve the investigation
                    SQX_Resp_Inclusion_Approval__c approvalInc1 = new SQX_Resp_Inclusion_Approval__c(SQX_Response_Inclusion__c = inclusion1.Id, Name = 'approved');

                    Map<String,Object>  changeSet = new Map<String, Object>();
                    List<Object> changes = new List<Object>();
                
                    Map<String, Object> approvalInc1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(approvalInc1));
                    approvalInc1UT.put('Id', 'Approval-1');
                    changes.add(approvalInc1UT);
                    
                    changeSet.put('changeSet', changes);

                    Map<String, String> param = new Map<String, String>();
                    param.put('recordID', response.Id);
                    param.put('nextAction', 'approve');
                    param.put('originalApproverId', capaOwner.Id);

                    SQX_Extension_CAPA.processChangeSetWithAction('' + capa.capa.Id, JSON.serialize(changeSet), new SObject[]{}, param);
                }    

                /* approve as investigation approver */
                System.runas(investigationApprover){
                    SQX_BulkifiedBase.clearAllProcessedEntities();
                    //approve the investigation
                    SQX_Resp_Inclusion_Approval__c approvalInc1 = new SQX_Resp_Inclusion_Approval__c(SQX_Response_Inclusion__c = inclusion1.Id, Name = 'approved');

                    Map<String,Object>  changeSet = new Map<String, Object>();
                    List<Object> changes = new List<Object>();
                
                    Map<String, Object> approvalInc1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(approvalInc1));
                    approvalInc1UT.put('Id', 'Approval-1');
                    changes.add(approvalInc1UT);
                    
                    changeSet.put('changeSet', changes);

                    Map<String, String> param = new Map<String, String>();
                    param.put('recordID', response.Id);
                    param.put('nextAction', 'approve');
                    param.put('originalApproverId', investigationApprover.Id);

                    SQX_Extension_CAPA.processChangeSetWithAction('' + capa.capa.Id, JSON.serialize(changeSet), new SObject[]{}, param);
                }
            Test.stopTest();

            System.assertEquals(SQX_Finding_Response.STATUS_PUBLISHED, [SELECT Id, Status__c FROM SQX_Finding_Response__c WHERE Id = : response.Id].Status__c );

            DateTime endTime = DateTime.now();

            List<SQX_Resp_Inclusion_Approval__c> approvals = [SELECT Id, Approver_Name__c, SQX_Approving_User__c, Approved_On__c FROM SQX_Resp_Inclusion_Approval__c
                                                            WHERE SQX_Response_Inclusion__c = : inclusion1.Id];

            //we have two approvals, ensure that they are from correct approver's 
            System.assertEquals(2, approvals.size());
            System.assert(investigationApprover.Id == approvals.get(0).SQX_Approving_User__c || investigationApprover.Id == approvals.get(1).SQX_Approving_User__c,
                'Could not find approval information of response by investigation approver ' + approvals);
            System.assert(capaOwner.Id == approvals.get(0).SQX_Approving_User__c || capaOwner.Id == approvals.get(1).SQX_Approving_User__c,
                'Could not find approval information of response by investigation approver ' + approvals);

            //assert that approval date is in between startTime and endTime
            System.assert(approvals.get(0).Approved_On__c >= startTime && approvals.get(0).Approved_On__c <= endTime,
                'Approval time is not set correctly');


            System.assertEquals(SQX_Investigation.APPROVAL_STATUS_APPROVED, [SELECT Approval_Status__c FROM SQX_Investigation__c WHERE Id = : inclusion1.SQX_Investigation__c].Approval_Status__c);
            
        
        }


    }


    /**
    * ensures that a given a CAPA response is sent to multiple approval, when one rejects the inclusion is rejected
    */
    public testmethod static void givenACapaResponse_SingleRejectionRejectsResponse(){

        if(!runAllTests && !run_givenACapaResponse_SingleRejectionRejectsResponse)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        User investigationApprover = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER , null);
        DateTime startTime = DateTime.now();
        
        System.runas(capaOwner){
            SQX_Test_CAPA_Utility capa=  new SQX_Test_CAPA_Utility(true, investigationApprover, investigationApprover).save();
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();

            /* construct a response and submit for approval */
            System.runas(capaOwner){
                ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
                SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
                controller.initializeTemporaryStorage();
            
            
                
                SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.capa.Id,
                                                                              Response_Summary__c = 'Response');
                
                SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_CAPA__c = capa.capa.Id,
                                                                             Investigation_Summary__c = 'Is');
                
                SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');
                
                Map<String,Object>  changeSet = new Map<String, Object>(),
                                    responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                    investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                    inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped
                
                //set virtual IDSs
                responseUT.put('Id', 'Response-1');
                investigationUT.put('Id', 'Investigation-1');
                inclusion2UT.put('Id', 'Inclusion-2');
                
                inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
                inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
                
                List<Object> changes = new List<Object>();
                
                changes.add(responseUT);
                changes.add(investigationUT);
                changes.add(inclusion2UT);
                
                changeSet.put('changeSet', changes);
            
            
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
            
                System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].size());
            }

            /* ensure we are not very close to the limit */
            System.assert(((double)Limits.getDMLStatements() / Limits.getLimitDMLStatements()) < 0.5,
             'Limit too close ' + Limits.getDMLStatements() + ' out of ' + Limits.getLimitDMLStatements() );

            SQX_Finding_Response__c response = [SELECT Id, Status__c, (SELECT Id, SQX_Investigation__c FROM SQX_Response_Inclusions__r ) FROM SQX_Finding_Response__c WHERE SQX_CAPA__c =: capa.capa.Id ];
            SQX_Response_Inclusion__c inclusion1 = response.SQX_Response_Inclusions__r.get(0); //investigation response inclusion

            Test.startTest();
                /* approve as capa owner */
                System.runas(capaOwner){
                    SQX_BulkifiedBase.clearAllProcessedEntities();
                    
                    //approve the investigation
                    SQX_Resp_Inclusion_Approval__c approvalInc1 = new SQX_Resp_Inclusion_Approval__c(SQX_Response_Inclusion__c = inclusion1.Id, Name = 'rejected');

                    Map<String,Object>  changeSet = new Map<String, Object>();
                    List<Object> changes = new List<Object>();
                
                    Map<String, Object> approvalInc1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(approvalInc1));
                    approvalInc1UT.put('Id', 'Approval-1');
                    changes.add(approvalInc1UT);
                    
                    changeSet.put('changeSet', changes);

                    Map<String, String> param = new Map<String, String>();
                    param.put('recordID', response.Id);
                    param.put('nextAction', 'approve');
                    param.put('originalApproverId', capaOwner.Id);

                    SQX_Extension_CAPA.processChangeSetWithAction('' + capa.capa.Id, JSON.serialize(changeSet), new SObject[]{}, param);
                }    

                /* approve as investigation approver */
                System.runas(investigationApprover){
                    SQX_BulkifiedBase.clearAllProcessedEntities();

                    //approve the investigation
                    SQX_Resp_Inclusion_Approval__c approvalInc1 = new SQX_Resp_Inclusion_Approval__c(SQX_Response_Inclusion__c = inclusion1.Id, Name = 'approved');

                    Map<String,Object>  changeSet = new Map<String, Object>();
                    List<Object> changes = new List<Object>();
                
                    Map<String, Object> approvalInc1UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(approvalInc1));
                    approvalInc1UT.put('Id', 'Approval-1');
                    changes.add(approvalInc1UT);
                    
                    changeSet.put('changeSet', changes);

                    Map<String, String> param = new Map<String, String>();
                    param.put('recordID', response.Id);
                    param.put('nextAction', 'approve');
                    param.put('originalApproverId', investigationApprover.Id);

                    SQX_Extension_CAPA.processChangeSetWithAction('' + capa.capa.Id, JSON.serialize(changeSet), new SObject[]{}, param);
                }
            Test.stopTest();

            System.assertEquals(SQX_Finding_Response.STATUS_PUBLISHED, [SELECT Id, Status__c FROM SQX_Finding_Response__c WHERE Id = : response.Id].Status__c );

            DateTime endTime = DateTime.now();

            List<SQX_Resp_Inclusion_Approval__c> approvals = [SELECT Id, Approver_Name__c, SQX_Approving_User__c, Approved_On__c FROM SQX_Resp_Inclusion_Approval__c
                                                            WHERE SQX_Response_Inclusion__c = : inclusion1.Id];

            //we have two approvals, ensure that they are from correct approver's 
            System.assertEquals(2, approvals.size());
            System.assert(investigationApprover.Id == approvals.get(0).SQX_Approving_User__c || investigationApprover.Id == approvals.get(1).SQX_Approving_User__c,
                'Could not find approval information of response by investigation approver ' + approvals);
            System.assert(capaOwner.Id == approvals.get(0).SQX_Approving_User__c || capaOwner.Id == approvals.get(1).SQX_Approving_User__c,
                'Could not find approval information of response by investigation approver ' + approvals);

            //assert that approval date is in between startTime and endTime
            System.assert(approvals.get(0).Approved_On__c >= startTime && approvals.get(0).Approved_On__c <= endTime,
                'Approval time is not set correctly');


            System.assertEquals(SQX_Investigation.APPROVAL_STATUS_REJECTED, [SELECT Approval_Status__c FROM SQX_Investigation__c WHERE Id = : inclusion1.SQX_Investigation__c].Approval_Status__c);
            
        
        }


    }

    /**
     * Given : Capa with response for investigation is in approval
     * When : Capa is voided 
     * Then : Response is recalled
     */
    public testmethod static void givenACapaResponse_WhenCapaIsVoided_ResponseIsRecalled(){

        if(!runAllTests && !run_givenACapaResponse_WhenCapaIsVoided_ResponseIsRecalled)
            return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(capaOwner){
            SQX_Test_CAPA_Utility capa = new  SQX_Test_CAPA_Utility().save();
            capa.capa.Status__c = SQX_CAPA.STATUS_OPEN;
            capa.save();

            // Arrange : create capa, construct a response and submit for approval
            System.runas(capa.Finding.primaryContactUser){
                ApexPages.StandardController sc = new ApexPages.StandardController(capa.Capa);
                SQX_Extension_CAPA_Supplier controller = new SQX_Extension_CAPA_Supplier(sc);
            
                controller.initializeTemporaryStorage();
                
                SQX_Finding_Response__c response = new SQX_Finding_Response__c(SQX_CAPA__c = capa.capa.Id,
                                                                              Response_Summary__c = 'Response');
                
                SQX_Investigation__c investigation = new SQX_Investigation__c(SQX_CAPA__c = capa.capa.Id,
                                                                             Investigation_Summary__c = 'Is');
                
                SQX_Response_Inclusion__c inclusion2 = new SQX_Response_Inclusion__c(Type__c = 'Investigation');
                
                Map<String,Object>  changeSet = new Map<String, Object>(),
                                    responseUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(response)),
                                    investigationUT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(investigation)),
                                    inclusion2UT = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(inclusion2)); // UT: untyped
                
                // set virtual IDSs
                responseUT.put('Id', 'Response-1');
                investigationUT.put('Id', 'Investigation-1');
                inclusion2UT.put('Id', 'Inclusion-2');
                
                inclusion2UT.put(SQX.getNSNameFor('SQX_Investigation__c'), 'Investigation-1');
                inclusion2UT.put(SQX.getNSNameFor('SQX_Response__c'), 'Response-1');
                
                List<Object> changes = new List<Object>();
                
                changes.add(responseUT);
                changes.add(investigationUT);
                changes.add(inclusion2UT);
                
                changeSet.put('changeSet', changes);
            
                System.assertEquals(SQX_Extension_UI.OK_STATUS,
                    SQX_Extension_UI.submitResponse(capa.capa.Id, '{"changeSet": []}', JSON.serialize(changeSet), false, null, params ));
            
                System.assertEquals(1, [SELECT Id FROM SQX_Finding_Response__c WHERE SQX_CAPA__c = : capa.capa.Id].size());
            }

            /* ensure we are not very close to the limit */
            System.assert(((double)Limits.getDMLStatements() / Limits.getLimitDMLStatements()) < 0.5,
             'Limit too close ' + Limits.getDMLStatements() + ' out of ' + Limits.getLimitDMLStatements() );

            SQX_Finding_Response__c response = [SELECT Id, Status__c, (SELECT Id, SQX_Investigation__c FROM SQX_Response_Inclusions__r ) FROM SQX_Finding_Response__c WHERE SQX_CAPA__c =: capa.capa.Id ];
            SQX_Response_Inclusion__c inclusion1 = response.SQX_Response_Inclusions__r.get(0); //investigation response inclusion

            Test.startTest();
            SQX_BulkifiedBase.clearAllProcessedEntities();
                
            // Act : void capa
            capa.capa.Resolution__c = SQX_CAPA.RESOLUTION_VOID;
            capa.capa.Status__c = SQX_CAPA.STATUS_CLOSED;
            capa.capa.Closure_Comment__c = 'Capa voided';
            new SQX_DB().op_update(new List<SQX_CAPA__c> {capa.capa}, new LiST<Schema.SObjectField> {});

            // Assert : response should be recalled
            System.assertEquals(SQX_Finding_Response.APPROVAL_STATUS_RECALLED, [SELECT Id, Approval_Status__c FROM SQX_Finding_Response__c WHERE Id = : response.Id].Approval_Status__c );
        }
    }
    
    /**
    * this test ensures that once a response is published the publish date is set
    */
    public testmethod static void givenAResponseIsPublished_PublishedDateIsSet(){
		if(!runAllTests && !run_givenAResponseIsPublished_PublishedDateIsSet)
			return;

        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User capaOwner = SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);

        System.runas(capaOwner){

            //setup: Lets create a finding (Response's can't be added without finding) and insert different kinds of response's to the database
            SQX_Test_Finding finding = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
                                        .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
                                        .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
                                        .setStatus(SQX_Finding.STATUS_OPEN)
                                        .save();
            Id findingId = finding.finding.Id;
                                           
            SQX_Finding_Response__c responsePublished = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_PUBLISHED, 
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_NONE,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId),
                                    
                                    responseWithApproval = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_PUBLISHED, 
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_APPROVED,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId),
                                    
                                    responseWithRejection = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_PUBLISHED,
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_REJECTED,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId),
                                                                                        
                                    responseWithPartialApproval = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_PUBLISHED,
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PARTIAL_APPROVAL,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId),
                                                                                        
                                    responseInDraft = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_DRAFT,
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_NONE,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId),
                                                                                        
                                    responseInApproval = new SQX_Finding_Response__c(Status__c = SQX_Finding_Response.STATUS_IN_APPROVAL,
                                                                                        Approval_Status__c = SQX_Finding_Response.APPROVAL_STATUS_PENDING,
                                                                                        Response_Summary__c = 'Mock',
                                                                                        SQX_Finding__c  = findingId);
                                                                                        
            insert new SQX_Finding_Response__c[]{responsePublished, responseWithApproval, responseWithRejection, responseWithPartialApproval, responseInDraft, responseInApproval };
            
            
            //actual test: lets get the saved records from the database and assert that published date is set
            Map<Id, SQX_Finding_Response__c> allResponses = new Map<Id, SQX_Finding_Response__c>
                            ([SELECT Id, Published_Date__c FROM SQX_Finding_Response__c WHERE SQX_Finding__c =: findingId ]);
                            
            
            //all published responses must have there Published date set, irrespective of their approval statuses               
            System.assertNotEquals(allResponses.get(responsePublished.Id).Published_Date__c, null);
            System.assertNotEquals(allResponses.get(responseWithApproval.Id).Published_Date__c, null);
            System.assertNotEquals(allResponses.get(responseWithRejection.Id).Published_Date__c, null);
            System.assertNotEquals(allResponses.get(responseWithPartialApproval.Id).Published_Date__c, null);
            
            
            //all in approval and draft responses must not have there published date set
            System.assertEquals(allResponses.get(responseInDraft.Id).Published_Date__c, null);
            System.assertEquals(allResponses.get(responseInApproval.Id).Published_Date__c, null);

        }
		
    }
}