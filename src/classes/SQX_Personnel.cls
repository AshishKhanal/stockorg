/**
* This class will handles trigger for Personnel object
*/
public with sharing class SQX_Personnel {
    
    // Training status
    public static final String  TRAINING_STATUS_CURRENT = 'Current',
                                TRAINING_STATUS_PENDING = 'Pending',
                                
    // Personnel record create actions
                                CREATE_ACTION_IMPORT_ALL_NEW_ACTIVE_USERS = 'IMPORT_ALL_NEW_ACTIVE_USERS',
                                CREATE_ACTION_IMPORT_USER = 'IMPORT_USER',
                                CREATE_ACTION_NON_USER = 'CREATE_NON_USER';
    
    /**
    * generates customized messages that are proper to display to users
    */
    public static String getCustomizedErrorMessage(String message) {
        String msg = message;
        
        // customized duplicate error
        if (msg.contains('duplicate value found:')) {
            // get duplicate record id
            String dupId = msg.substring(msg.indexOf('with id: ') + 9, msg.indexOf('with id: ') + 24);
            List<SQX_Personnel__c> dupRec = [SELECT Id, Name FROM SQX_Personnel__c WHERE Id = :dupId];
            if (dupRec.size() == 1) {
                // get proper related field label
                String fieldLabel = '';
                if (msg.contains('Unique_User_Constraint__c')) {
                    fieldLabel = SQX_Personnel__c.Unique_User_Constraint__c.getDescribe().getLabel();
                } else if (msg.contains('Identification_Number__c')) {
                    fieldLabel = SQX_Personnel__c.Identification_Number__c.getDescribe().getLabel();
                }
                
                // customize message with duplicate record view link
                PageReference viewRef = new ApexPages.StandardController(dupRec[0]).view();
                msg = 'Duplicate value on record: <a target="_blank" href="' + viewRef.getUrl() + '">' + dupRec[0].Name + '</a> (Related field: ' + fieldLabel + ')';
            }
        }
        
        return msg;
    }
    
    /**
    * copies user details to personnel when importing
    */
    public static SQX_Personnel__c copyFieldsFromUser(User source, SQX_Personnel__c target) {
        target.SQX_User__c = source.Id;
        target.Full_Name__c = source.FirstName + ' ' + source.LastName;
        target.Identification_Number__c = source.Username;
        target.Email_Address__c = source.Email;
        target.Active__c = source.IsActive;
        
        return target;
    }
    
    /**
    * imports active users that are not mapped to personnel
    */
    public static List<DataBase.SaveResult> importAllUsers() {
        // get users that are not mapped to personnel, also avoid sf internal user as it's email is invalid and in api version 44.0 exception is thrown.
        List<User> users = [SELECT Id, FirstName, LastName, Username, Email, IsActive
                            FROM User
                            WHERE IsActive = true
                                  AND Id NOT IN (SELECT SQX_User__c FROM SQX_Personnel__c)
                                  AND UserType != :SQX_User.USER_TYPE_AUTOMATEDPROCESS];
        
        List<SQX_Personnel__c> newRecords = new List<SQX_Personnel__c>();
        for (User u : users) {
            // create personnel record for active users who are not mapped
            SQX_Personnel__c newRec = copyFieldsFromUser(u, new SQX_Personnel__c());
            // add to insert list
            newRecords.add(newRec);
        }
        
        List<DataBase.SaveResult> results = new List<DataBase.SaveResult>();
        
        if (newRecords.size() > 0) {
            results = new SQX_DB().continueOnError().op_insert(newRecords, new List<Schema.SObjectField>{ });
        }
        
        return results;
    }
    
    /**
    * imports single user to personnel
    */
    public static DataBase.SaveResult importUser(Id userId) {
        if (userId == null) {
            throw new SQX_ApplicationGenericException('User is required.');
        }
        
        // get user details from userId
        List<User> users = [SELECT Id, FirstName, LastName, Username, Email, IsActive
                            FROM User
                            WHERE Id = :userId];
        
        if (users.size() == 0) {
            throw new SQX_ApplicationGenericException('User could not be found to import.');
        } else {
            // create personnel record for active users who are not mapped
            SQX_Personnel__c newRec = copyFieldsFromUser(users[0], new SQX_Personnel__c());
            
            return new SQX_DB().continueOnError().op_insert(new List<SQX_Personnel__c> { newRec }, new List<Schema.SObjectField>{ }).get(0);
        }
    }
    
    /**
    * returns personnel document training list of given personnel ids with training statuses
    */
    public static List<SQX_Personnel_Document_Training__c> getPersonnelDocumentTrainingList(Set<Id> personnelIds, Set<String> trainingStatuses) {
        return [SELECT Id, Name, SQX_Controlled_Document__c, SQX_Controlled_Document__r.Name, Title__c, SQX_Training_Session__c,
                       Document_Number__c, Document_Revision__c, Document_Title__c, Document_Number_Rev__c,
                       SQX_Personnel__c, Personnel_Name__c, SQX_Trainer__c, SQX_Trainer__r.Name, Level_Of_Competency__c, Optional__c,
                       Trainer_Approval_Needed__c, Due_Date__c, Status__c, CreatedDate, Completion_Date__c,
                       SQX_User_Signed_Off_By__c, SQX_User_Signed_Off_By__r.Name, User_SignOff_Date__c, Is_Training_Session_Complete__c,
                       SQX_Training_Approved_By__c, SQX_Training_Approved_By__r.Name, Trainer_SignOff_Date__c,
                       Can_Retrain__c, Is_Retrain__c, Retrain_Comment__c, SQX_Assessment__c, Assessment_Name__c, Can_View_And_Sign_Off__c,Can_Take_Assessment__c, SQX_Personnel_Assessment__c
                FROM SQX_Personnel_Document_Training__c
                WHERE SQX_Personnel__c IN :personnelIds AND Status__c IN :trainingStatuses
                ORDER BY Name];
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        public Bulkified() {
        }
        
        public Bulkified(List<SQX_Personnel__c> newList , Map<Id, SQX_Personnel__c> oldMap) {
            super(newList, oldMap);
        }
        
        /**
        * activates not activated personnel job function when personnel is activated
        */ 
        public Bulkified activatePersonnelJobFunctionWhenPersonnelActivated() {
            final String ACTION_NAME = 'activatePersonnelJobFunctionWhenPersonnelActivated';
            
            // remove all previously processed records
            this.resetView().removeProcessedRecordsFor(SQX.Personnel, ACTION_NAME);

            if (this.view.size() > 0) {
                // find all activated personnel
                Rule activationRule = new Rule();
                boolean activeValue = true;
                activationRule.addRule(Schema.SQX_Personnel__c.Active__c, RuleOperator.Equals, (Object)activeValue);

                this.applyFilter(activationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

                if (activationRule.evaluationResult.size() > 0) {
                    this.addToProcessedRecordsFor(SQX.Personnel, ACTION_NAME, activationRule.evaluationResult);
                    
                    Set<Id> personnelIds = new Set<Id>();
                    for (SQX_Personnel__c p : (List<SQX_Personnel__c>)this.view) {
                        personnelIds.add(p.Id);
                    }
                    
                    List<SQX_Personnel_Job_Function__c> updateList = [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c
                                                                      WHERE SQX_Personnel__c IN :personnelIds AND Active__c = false AND Activation_Date__c = null AND SQX_Job_Function__r.Active__c = true];
                    
                    if (updateList.size() > 0) {
                        // activate not activated records
                        for (SQX_Personnel_Job_Function__c i : updateList) {
                            i.Active__c = true;
                        }
                        
                        new SQX_DB().op_update(updateList, new List<Schema.SObjectField>{ Schema.SQX_Personnel_Job_Function__c.Active__c });
                    }
                }
            }
            
            return this;
        }
        
        /**
        * deactivates active personnel job functions when personnel is deactivated
        */
        public Bulkified deactivateActivePersonnelJobFunctionWhenPersonnelDeactivated() {
            final String ACTION_NAME = 'deactivateActivePersonnelJobFunctionWhenPersonnelDeactivated';
            
            // remove all previously processed records
            this.resetView().removeProcessedRecordsFor(SQX.Personnel, ACTION_NAME);

            if (this.view.size() > 0) {
                // find all deactivated personnel
                Rule deactivationRule = new Rule();
                boolean activeValue = false;
                deactivationRule.addRule(Schema.SQX_Personnel__c.Active__c, RuleOperator.Equals, (Object)activeValue);

                this.applyFilter(deactivationRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

                if (deactivationRule.evaluationResult.size() > 0) {
                    this.addToProcessedRecordsFor(SQX.Personnel, ACTION_NAME, deactivationRule.evaluationResult);
                    
                    Set<Id> personnelIds = new Set<Id>();
                    for (SQX_Personnel__c p : (List<SQX_Personnel__c>)this.view) {
                        personnelIds.add(p.Id);
                    }
                    
                    List<SQX_Personnel_Job_Function__c> updateList = [SELECT Id, Active__c FROM SQX_Personnel_Job_Function__c
                                                                      WHERE SQX_Personnel__c IN :personnelIds AND Active__c = true];
                    
                    if (updateList.size() > 0) {
                        // deactivate active records
                        for (SQX_Personnel_Job_Function__c i : updateList) {
                            i.Active__c = false;
                        }
                        
                        new SQX_DB().op_update(updateList, new List<Schema.SObjectField>{ Schema.SQX_Personnel_Job_Function__c.Active__c });
                    }
                }
            }
            
            return this;
        }
        
        /**
        * adds readonly sharing rule to the active related user on new or updated personnel records when required
        * and deletes sharing rule from old related user if it has been changed
        */
        public Bulkified shareRecordWithActiveRelatedUser() {
            final String ACTION_NAME = 'shareRecordWithActiveRelatedUser';
            
            this.resetView().removeProcessedRecordsFor(SQX.Personnel, ACTION_NAME);
            
            if (this.view.size() > 0) {
                Map<Id, SQX_Personnel__c> personnels = new Map<Id, SQX_Personnel__c>((List<SQX_Personnel__c>)this.view);
                
                PrivilegeEscalator escalator = new PrivilegeEscalator();
                // add sharing rule for related user if needed
                // PrivilegeEscalator is used to allow addition of sharing rule while changing owner of a personnel record
                Map<Id, SQX_Personnel__c> processedPsns = escalator.shareRecordWithActiveRelatedUser(personnels.keySet(), oldValues.values());
                
                this.addToProcessedRecordsFor(SQX.Personnel, ACTION_NAME, processedPsns.values());
            }
            
            return this;
        }
    }
    
    /**
    * system operations for personnel object that require to query and update personnel records
    * eg: add read access to the related personnel user during ownership change when old record owner has no access to the personnel record
    */
    private without sharing class PrivilegeEscalator {
        /**
        * adds read access to related personnel user if sharing rule for the user does not exist on the updated personnel records
        * @param personnelIds   personnel ids to add read access sharing rule for the related user
        * @param oldPersonnels  old values of personnelIds obtained from trigger
        * @return               returns map of processed personnel records
        */
        public Map<Id, SQX_Personnel__c> shareRecordWithActiveRelatedUser(Set<Id> personnelIds, List<SQX_Personnel__c> oldPersonnels) {
            Map<Id, SQX_Personnel__c> oldPsnMap;
            if (oldPersonnels == null) {
                oldPsnMap = new Map<Id, SQX_Personnel__c>();
            }
            else {
                oldPsnMap = new Map<Id, SQX_Personnel__c>(oldPersonnels);
            }
            
            // read updated personnel records
            Map<Id, SQX_Personnel__c> personnelsWithDetails = new Map<Id, SQX_Personnel__c>(
               [SELECT Id, SQX_User__c, Active__c, OwnerId, SQX_User__r.isActive,
                    (SELECT Id, ParentId, UserOrGroupId FROM Shares)
                FROM SQX_Personnel__c
                WHERE Id IN :personnelIds]
            );
            
            Map<Id, SQX_Personnel__c> processedPsns = new Map<Id, SQX_Personnel__c>();
            List<SQX_Personnel__Share> personnelSharesToAdd = new List<SQX_Personnel__Share>();
            List<SQX_Personnel__Share> personnelSharesToDel = new List<SQX_Personnel__Share>();
            
            for (SQX_Personnel__c psn : personnelsWithDetails.values()) {
                Boolean needSharingRule = true;
                SQX_Personnel__c oldPsn = oldPsnMap.get(psn.Id);
                
                for (SQX_Personnel__Share s : psn.Shares) {
                    if (s.UserOrGroupId == psn.SQX_User__c) {
                        // related user already has sharing rules
                        needSharingRule = false;
                    }
                    if (oldPsn != null && s.UserOrGroupId == oldPsn.SQX_User__c && psn.SQX_User__c != oldPsn.SQX_User__c && oldPsn.SQX_User__c != psn.OwnerId) {
                        // delete existing sharing rule for old related user when the user is not owner
                        personnelSharesToDel.add(s);
                        
                        processedPsns.put(psn.Id, psn);
                    }
                }
                
                if (needSharingRule && psn.SQX_User__c != null && psn.SQX_User__c != psn.OwnerId && psn.SQX_User__r.isActive == true) {
                    // add sharing rule for the related user only when required
                    // it also avoids overriding high level access provided for the user
                    personnelSharesToAdd.add(new SQX_Personnel__Share(
                        ParentId = psn.Id,
                        UserOrGroupId = psn.SQX_User__c,
                        AccessLevel = 'read'
                    ));
                    
                    processedPsns.put(psn.Id, psn);
                }
            }
            
            if (processedPsns.size() > 0) {
                if (personnelSharesToAdd.size() > 0) {
                    // ignore errors as read access cannot be added when personnel object has public read access through sharing model setting
                    // without sharing is required for ownership change
                    new SQX_DB().continueOnError().withoutSharing().op_insert(personnelSharesToAdd, new List<SObjectField>{ });
                }
                
                if (personnelSharesToDel.size() > 0) {
                    // delete sharing rules for old related users
                    // without sharing is required when related user has full access and the related user changed user value to another user
                    new SQX_DB().withoutSharing().op_delete(personnelSharesToDel);
                }
            }
            
            return processedPsns;
        }
    }
}