/**
 * Test class for Data Table Query Controller
*/

@isTest
public class SQX_Test_Data_Table_Controller {


    /**
     * GIVEN : Data Table Query Controller
     * WHEN : SaveRecords is Invoked
     * THEN : Records are saved
    */
    static testmethod void givenController_WhenSaveIsInvoked_ThenRecordsAreSaved() {

        Account acc = new Account();

        AuraHandledException auraEx;
        try {
            SQX_Data_Table_Query_Controller.saveRecords(new List<SObject> { acc });
        } catch (AuraHandledException ex) {
            auraEx = ex;
        }

        System.assertNotEquals(null, auraEx, 'Expected error to be thrown');

        acc.Name = 'Random Account';
        insert acc;

        acc.BillingCity = 'KTM';

        SQX_Data_Table_Query_Controller.saveRecords(new List<SObject> { acc });

        System.assertEquals('KTM', [SELECT BillingCity FROM Account].BillingCity, 'Expected account to be updated');
    }

}