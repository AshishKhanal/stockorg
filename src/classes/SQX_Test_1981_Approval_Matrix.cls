/**
* Unit tests for Approval Matrix object
*/
@IsTest
public class SQX_Test_1981_Approval_Matrix {

    static Boolean runAllTests = true,
                   run_givenAddApprovalMatrix_DuplicateName_NotSaved = false,
                   run_givenUpdateApprovalMatrixName_Duplicate_NoSaved = false,
                   run_givenUpdateApprovalMatrixName_NoDuplicate_Saved = false,
                   run_givenUpdateApprovalMatrixName_SameNameDifferentCase_Saved = false;
    
    /**
    * ensures addition of new approval matrix with duplicate name (case insensitive) is not saved
    * related vaidation rule Name_Should_Be_Unique
    */
    public testmethod static void givenAddApprovalMatrix_DuplicateName_NotSaved() {
        if (!runAllTests && !run_givenAddApprovalMatrix_DuplicateName_NotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // add required approval matrix
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 1' );
            Database.SaveResult result1 = Database.insert(am1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            // ACT: add duplicate approval matrix
            SQX_Approval_Matrix__c am2 = new SQX_Approval_Matrix__c( Name = 'approval matrix 1' );
            result1 = Database.insert(am2, false);
            
            System.assert(result1.isSuccess() == false,
                'New approval matrix with duplicate name is not expected to be saved.');
        }
    }
    
    /**
    * ensures modification of approval matrix name with duplicate name is not saved
    * related vaidation rule Name_Should_Be_Unique
    */
    public testmethod static void givenUpdateApprovalMatrixName_Duplicate_NoSaved() {
        if (!runAllTests && !run_givenUpdateApprovalMatrixName_Duplicate_NoSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // add required approval matrices
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 1' );
            Database.SaveResult result1 = Database.insert(am1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            SQX_Approval_Matrix__c am2 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 2' );
            result1 = Database.insert(am2, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            // ACT: update approval matrix name with duplciate value
            am2.Name = 'approval matrix 1';
            result1 = Database.update(am2, false);
            
            System.assert(result1.isSuccess() == false,
                'Approval matrix with duplicate name is not expected to be saved.');
        }
    }
    
    /**
    * ensures modification of approval matrix name with new name is saved
    * related vaidation rule Name_Should_Be_Unique
    */
    public testmethod static void givenUpdateApprovalMatrixName_NoDuplicate_Saved() {
        if (!runAllTests && !run_givenUpdateApprovalMatrixName_NoDuplicate_Saved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // add required approval matrices
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 1' );
            Database.SaveResult result1 = Database.insert(am1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            SQX_Approval_Matrix__c am2 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 2' );
            result1 = Database.insert(am2, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            // ACT: update approval matrix name with new value
            am2.Name = 'approval matrix 3';
            result1 = Database.update(am2, false);
            
            System.assert(result1.isSuccess() == true,
                'Approval matrix with new name is expected to be saved.');
        }
    }
    
    /**
    * ensures modification of approval matrix name with same name but with different case is saved
    * related vaidation rule Name_Should_Be_Unique
    */
    public testmethod static void givenUpdateApprovalMatrixName_SameNameDifferentCase_Saved() {
        if (!runAllTests && !run_givenUpdateApprovalMatrixName_SameNameDifferentCase_Saved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        
        System.runas(adminUser) {
            // add required approval matrices
            SQX_Approval_Matrix__c am1 = new SQX_Approval_Matrix__c( Name = 'APPROVAL MATRIX 1' );
            Database.SaveResult result1 = Database.insert(am1, false);
            System.assert(result1.isSuccess() == true, 'Required approval matrix is expected to be saved.');
            
            // ACT: update approval matrix name with same value but with different case
            am1.Name = 'Approval Matrix 1';
            result1 = Database.update(am1, false);
            
            System.assert(result1.isSuccess() == true,
                'Approval matrix with same name and different case is expected to be saved.');
        }
    }
}