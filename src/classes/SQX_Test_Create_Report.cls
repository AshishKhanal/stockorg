@IsTest
public class SQX_Test_Create_Report {
    
    public static final String EXCEPTION_MSG_MULTIPLE_IDS_NOT_ALLOWED='Multiple configurations not allowed';
    
    /**
    * Given: A regulatory Report.
    * When : Multiple Reports are tried to be created.
    * Then : Exceptions are thrown.
    */
    @IsTest
    public static void givenRegulatoryReport_WhenMultipeReportsAreTriedToBeCreated_ExceptionIsThrown(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c regReport1 = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                compliancequest__SQX_Complaint__c=complaint.complaint.Id,
                compliancequest__Due_Date__c=Date.today() +1
            );
           insert regReport1;

             
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c regReport2 = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                compliancequest__SQX_Complaint__c=complaint.complaint.Id,
                compliancequest__Due_Date__c=Date.today() +1
            );
            
            insert regReport2;
            
            SQX_Create_Report_Configuration config1 = new SQX_Create_Report_Configuration();
            SQX_Create_Report_Configuration config2 = new SQX_Create_Report_Configuration();
            
            config1.reportId =regReport1.Id;
            config2.reportId = regReport2.Id;
          
            String actualException ='';
            
            try{
                  // Act: Call the method that creates the report.
                List<Id> ids =  SQX_Create_Report.createReport(new List<SQX_Create_Report_Configuration>{config1,config2});
            }catch(Exception ex){
                actualException =  ex.getMessage();
            }
            // Assert: Exception should be a desired one.
            System.assert(actualException.contains(EXCEPTION_MSG_MULTIPLE_IDS_NOT_ALLOWED));  
          
       
        }
    }
    
     /**
    * Given: A regulatory Report.
    * When : Report is created.
    * Then : Id of the newly created report is returned.
    */
    @IsTest
    public static void givenRegulatoryReport_WhenReportofCertainTypeIsCreated_IdOfTheNewlyCreatedReportIsReturned(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
            	Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                compliancequest__SQX_Complaint__c=complaint.complaint.Id,
                compliancequest__Due_Date__c=Date.today() +1
            );
            
            insert regReport;
            
            SQX_Create_Report_Configuration config = new SQX_Create_Report_Configuration();
            config.reportId = regReport.Id;
            
            // Act: Call the method that created the report.
            List<Id> ids =  SQX_Create_Report.createReport(new List<SQX_Create_Report_Configuration>{config});
            
            // Assert: Query report
            SQX_Medwatch__c medwatch = [SELECT Id FROM SQX_Medwatch__c where SQX_Regulatory_Report__c =:regReport.Id ];
			
            // Assert: The report ids should be same.
            System.assertEquals(ids[0], medwatch.Id);
        }
    }
    
      /**
    * Given: A regulatory Report.
    * When : Multiple medwatch records are tried to be created.
    * Then : Exceptions are thrown.
    */
    @IsTest
    public static void givenRegulatoryReport_WhenMultipleMedwatchReportsAreTriedToBeCreated_ExceptionIsThrown(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c regReport = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_FDA,
                Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_5_DAY_MDR,
                compliancequest__SQX_Complaint__c=complaint.complaint.Id,
                compliancequest__Due_Date__c=Date.today() +1
            );
            insert regReport;
            
            SQX_Medwatch__c medwatch = new SQX_Medwatch__c();
            medwatch.compliancequest__SQX_Regulatory_Report__c=regReport.Id;
            insert medwatch;
            
            regReport.Report_Id__c=medwatch.Id;
            update regReport;
            
            SQX_Create_Report_Configuration config = new SQX_Create_Report_Configuration();
            config.reportId = regReport.Id;
            String actualException='';
            try{
                // Act: Call the method that creates the report.
                List<Id> ids =  SQX_Create_Report.createReport(new List<SQX_Create_Report_Configuration>{config});
            }catch(Exception ex){
                 actualException=ex.getMessage();
            }
            // Assert: Exception should be a desired one.
            System.assert(actualException.contains(Label.CQ_ERR_MSG_REGULATORY_REPORT_ALREADY_EXISTS)); 
        }
    }
    
    /**
    * Given: A regulatory Report.
    * When : Multiple Reports are tried to be created.
    * Then : Exceptions are thrown.
    */
    @IsTest
    public static void givenFinalRegulatoryReport_WhenReportsIsCreated_InitialReportShouldBeLinkedWithFinal(){
        
        //Arrange: Create an open complaint
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runAs(standardUser){
            
            // Arrange: Create Complaint
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser);
            
            complaint.setStatus(SQX_Complaint.STATUS_OPEN).save();
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c initialReport = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_EMA,
                Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Report_Type__c='Initial Report'
            );

            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c> { initialReport }, new List<SObjectField> {}).get(0);
          
            SQX_Meddev__c meddev = new SQX_Meddev__c();
            meddev.SQX_Regulatory_Report__c=initialReport.Id;
            new SQX_DB().op_insert(new List<SQX_Meddev__c> { meddev }, new List<SObjectField> {}).get(0);
            
            initialReport.Report_Id__c = meddev.Id;
            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c> { initialReport }, new List<SObjectField> {}).get(0);
            
            // Arrange: Create Regulatory Report for medwatch
            SQX_Regulatory_Report__c finalReport = new SQX_Regulatory_Report__c( 
                Reg_Body__c=SQX_Regulatory_Report.REGULATORY_BODY_EMA,
                Report_Name__c=SQX_Regulatory_Report.REPORT_NAME_2_DAY_EMA,
                SQX_Complaint__c=complaint.complaint.Id,
                Due_Date__c=Date.today() +1,
                Report_Type__c='Final Report'
            );
            
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c> { finalReport }, new List<SObjectField> {}).get(0);            
            SQX_Create_Report_Configuration config = new SQX_Create_Report_Configuration();
            
            config.reportId =finalReport.Id;
            config.initialReportId = initialReport.Id;
            
            // Act: Call the method that creates the report.
            List<Id> ids =  SQX_Create_Report.createReport(new List<SQX_Create_Report_Configuration>{config});
            
            // Assert: Intial report Id should be equal to the id of the initial report linked with final.
            SQX_Regulatory_Report__c finalRep = [SELECT Id,SQX_Initial_Regulatory_Report__c FROM SQX_Regulatory_Report__c WHERE Report_Id__c =: ids[0]]; 
            System.assertEquals(initialReport.Id,finalRep.SQX_Initial_Regulatory_Report__c);
        }
    }
}