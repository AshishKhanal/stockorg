/*
* Class for creating followup of the regulatory report
*/
public with sharing class SQX_Create_Followup {
    
    public static final Boolean USE_INCLUSION_FIELDSET=true;
    
    /**
     * Method that creates the follow up of regulatory report.
     * @param ids : Ids of the regulatory report to be cloned.
     */ 
    @InvocableMethod(label='CQ Create Followup' description='Creates Followup of a Regulatory Report')
    public static List<Id> createFollowup(List<String> ids){
        
        SavePoint sp;
        
        try{
            // Throw error in case multiple ids are passed.
            if(ids.size() > 1){
                throw new SQX_ApplicationGenericException('Multiple ids not allowed');   
            } 
            
            // Query the regulatory report to be followed up
            SQX_Regulatory_Report__c regReport = [SELECT SQX_Complaint__c,Due_Date__c,
                                                  Report_Id__c,Reg_Body__c,
                                                  Follow_Up_Number_Internal__c,SQX_Submission_History__c,Report_Name__c 
                                                  FROM SQX_Regulatory_Report__c where Id =: ids[0]]; 
            
            // Configuration for cloning of the parent regulatory report.
            SQX_Clone_Configuration config = new SQX_Clone_Configuration();
            
            Map<String, Object> defaultMap = new Map<String, Object>();
            
            /*
            * In the cloned record,SQX_Follow_Up_Of__c must be set to the id of the regualatory report being cloned
            * and the internal must be incremented by 1 to get the next follow up number. This will be the follow up
            * number of the cloned regulatory report.      
            */ 
            defaultMap.put('compliancequest__SQX_Follow_Up_Of__c', regReport.Id);
            defaultMap.put('compliancequest__Follow_Up_Number_Internal__c',regReport.Follow_Up_Number_Internal__c+1);
            config.jsonDefaults = JSON.Serialize(defaultMap);
            
            config.ids = ids;
            config.useInclusionFieldset=USE_INCLUSION_FIELDSET;
            
            sp = Database.setSavepoint();
            
            // Clone the regulatory report.        
            List<Id> regReportCloneIds = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{config});
            
            /*
            * Update the parent regulatory report.
            * (1) Follow_Up_Number_Internal__c: Sync with the follow up number of latest follow up created.
            * (2) SQX_Last_Followup__c : Keep track of the last follow up created. This is done for the ease of nagivation.
            */
            regReport.Follow_Up_Number_Internal__c+=1;
            regReport.SQX_Last_Followup__c = regReportCloneIds[0];
            
            /*
            * Now, the related Medwatch,Meddev also need to be cloned. Which fields are included while cloning are
            * driven from the default fieldset for Medwatch/Meddev/Canada etc.
            */ 
            SQX_Clone_Configuration configs = new SQX_Clone_Configuration();
            
            Map<String, String> jsonDefaultMap = new Map<String, String>();
            jsonDefaultMap.put('compliancequest__SQX_Regulatory_Report__c', regReportCloneIds[0]);
            configs.jsonDefaults = JSON.Serialize(jsonDefaultMap);
            configs.ids = new List<Id>{regReport.Report_Id__c};
            configs.useInclusionFieldset=USE_INCLUSION_FIELDSET;
            
            // Clone medwatch/meddev/canada etc.
            List<Id> reportCloneIds = SQX_Clone_Records.cloneRecord(new List<SQX_Clone_Configuration>{configs});
            
            SQX_Regulatory_Report__c regReportClone = [SELECT Id, Report_Id__c FROM SQX_Regulatory_Report__c where Id =: regReportCloneIds[0]];
            regReportClone.Report_Id__c = reportCloneIds[0];
            
            new SQX_DB().op_update(new List<SObject> { regReport, regReportClone }, new List<SObjectField> {});
            
            return reportCloneIds;   
        }catch (Exception ex) {
            
            if(sp != null) {
                Database.rollback(sp);
            }
            
            throw ex;
        }
    }
}