/**
*   This class contains test cases for ensuring that homepage component is working correctly
*   @author - Piyush Subedi
*   @date - 17/03/2017
*   @story - SQX-3207
*/
@isTest
public class SQX_Test_HomePage_Component{

    final static String MOCK_ACTION_ASSIGNED_CONTRACTS  = 'ASSIGNED CONTRACTS',
                        BADGE_TEXT_ASSIGNED_CONTRACTS = 'AC',
                        MOCK_MODULE_CONTRACT = 'CONTRACT',
                        BADGE_TEXT_CONTRACT = 'CT',
                        MOCK_CONTRACT_FEED_TEMPLATE = 'Contract: {0} is active',
                        MOCK_ACTION_DELETE = 'Delete',
                        MOCK_CONTRACT_TITLE = 'Mock Contract',
                        MOCK_ICON_REMOVE = 'utility:remove';

    final static Integer LIMIT_FOR_CRITICALITY = 3;


    static final Integer DEFAULT_ITEM_AGE = 5,
                         DEFAULT_DAYS_TO_DUE = 2;

    // ################## TODO : MOVE THE FOLLOWING SECTION TO COMMON PLACE ################################ //
    
    /**
    * Count for the object type. Note: That a single counter is applied to all object types
    */
    static Integer count = 1;

    /**
     * Useful for generating fake ID for objects without actually saving it
     * Refer to : http://salesforce.stackexchange.com/a/21297
     *            https://foobarforce.com/2013/11/25/df13return/
     */
    public static String getFakeId(SObjectType objType) {
        String result = String.valueOf(count++);
        
        return objType.getDescribe().getKeyPrefix() + 
            '0'.repeat(12-result.length()) + result;
    }
    
    // #################################################################################################### //


    /**
    *   Testing if the framework is behaving accordingly
    *   Given - A mock home page items source
    *   When - Home page component requests items from the given source
    *   Then - the mock source should return a list of homepage items with desired values
    *   @story - SQX-3207
    */
    testmethod static void givenMockSource_WhenComponentRequestsItems_RecordsAreReturned(){
 
        // Arrange: Create a mock source
        SQX_Mock_Contract_Source mck = new SQX_Mock_Contract_Source();

        // add mock source to list of sources
        SQX_Consolidated_Home_Page_Component.allItemSources = new List<SQX_Homepage_ItemSource> { mck };

        // Act : Request home page items from the mock source
        Map<String, Object> homepageItems = (Map<String, Object>) JSON.deserializeUntyped(SQX_Consolidated_Home_Page_Component.getHomePageItems(null));

        List<Object> hpItems = (List<Object>) homepageItems.get('items');

        List<Object> errors = (List<Object>) homepageItems.get('errors');

        // Assert : Expecting no errors
        System.assertEquals(0, errors.size());

        // Assert: Filter has been set correctly
        System.assertNotEquals(mck.filter, null);
        System.assertEquals(mck.filter.sourceType, SQX_Homepage_Constants.DEFAULT_SOURCE_TYPE);
        System.assertEquals(mck.filter.pageSize, SQX_Homepage_Constants.DEFAULT_PAGE_SIZE);
        System.assertEquals(mck.filter.sortOrder, SQX_Homepage_Constants.DEFAULT_SORT_ORDER);


        // Assert: Items are returned from the mock source as desired
        System.assertNotEquals(hpItems, null);

        SQX_Homepage_Item hpItem = (SQX_Homepage_Item) JSON.deserialize(JSON.serialize(hpItems.get(0)), SQX_Homepage_Item.class);

        System.assertEquals(MOCK_ACTION_ASSIGNED_CONTRACTS, hpItem.actionType);

        System.assertEquals(Date.valueOf(hpItem.createdDate).addDays(DEFAULT_DAYS_TO_DUE), hpItem.dueDate);

        System.assertEquals(DEFAULT_ITEM_AGE, hpItem.itemAge);

        System.assertEquals(DEFAULT_ITEM_AGE - DEFAULT_DAYS_TO_DUE, hpItem.overdueDays);

        System.assertNotEquals(null, hpItem.urgency);

        System.assertEquals(MOCK_MODULE_CONTRACT, hpItem.moduleType);

        System.assertNotEquals(hpItem.actions, null);

        System.assert(hpItem.actions.size() >= 1, 'Expected atleast one item in action');

        SQX_Homepage_Item_Action action = hpItem.actions.get(0);

        System.assertEquals(MOCK_ACTION_DELETE, action.name);

        System.assertEquals(MOCK_ICON_REMOVE, action.actionIcon);

        System.assertEquals( String.format(MOCK_CONTRACT_FEED_TEMPLATE, new String[] { MOCK_CONTRACT_TITLE } ), hpItem.feedText);

        System.assertEquals( UserInfo.getUserId(), hpItem.creator.Id);
        
        System.assertEquals(false, hpItem.supportsBulk);
    }

    /**
    *   Mock source - Contract class that returns a list of assigned contracts in the form of SQX_Homepage_Item 
    */
    public class SQX_Mock_Contract_Source extends SQX_Homepage_ItemSource {

        User currentUser;

        /**
        *   Constructor method
        *   @param sources - list of sources for which to fetch open items
        */
        public SQX_Mock_Contract_Source(){
            super();
            currentUser = [SELECT Id, Name FROM User Where Id =: UserInfo.getUserId()];
        }

        protected override Map<SObjectType, List<SObjectField>> getProcessableEntities(){
            // nothing being updated or read here in this test class
            return new Map<SObjectType, List<SObjectField>>{};
        }

        /**
        *   Method returns a SQX_Homepage_Item type item from the given sobject record
        *   @param rec - Record to be converted to home page item
        */
        protected override SQX_Homepage_Item getHomePageItemFromRecord(SObject item){

            SQX_Homepage_Item mockItem = new SQX_Homepage_Item();

            mockItem.itemId = (Id) item.get('Id');
            mockItem.actionType = MOCK_ACTION_ASSIGNED_CONTRACTS;
            mockItem.createdDate = (Date) item.get('StartDate');

            // set due date, overdue days and days before due
            Date startDate = (Date) item.get('StartDate');
            mockItem.dueDate = startDate.addDays(DEFAULT_DAYS_TO_DUE);

            // set urgency
            mockItem.urgency = this.getItemUrgency(mockItem.dueDate, LIMIT_FOR_CRITICALITY);

            // set module badge
            mockItem.moduleType = MOCK_MODULE_CONTRACT;

            // set actions for the current item
            mockItem.actions = getMockContractActions(mockItem.itemId);

            // set feed text for the current item
            mockItem.feedText = String.format( MOCK_CONTRACT_FEED_TEMPLATE, new String[] { (String)item.get('CustomerSignedTitle') });

            // set user details of the user related to this item
            mockItem.creator = currentUser;

            return mockItem;
        }

        /**
        *   retrieves the list of contracts for the logged in user
        */
        protected override List<SObject> getUserRecords(){

            Account act1 = new Account(Name = 'MCK_A');

            Contract ct = new Contract(Id = getFakeId(Contract.SObjectType));
            ct.Status = 'Activated';
            ct.StartDate = Date.today().addDays(-1 * DEFAULT_ITEM_AGE);
            ct.CustomerSignedTitle = MOCK_CONTRACT_TITLE;
            ct.Account = act1;

            return new List<SObject> { ct };
        }
        
        /**
        *   Method returns a list of actions associated with the given action type
        */
        private List<SQX_Homepage_Item_Action> getMockContractActions(String itemId){

            List<SQX_Homepage_Item_Action> actions = new List<SQX_Homepage_Item_Action>();

            SQX_Homepage_Item_Action action = new SQX_Homepage_Item_Action();

            action.actionUrl = '/' +  itemId;

            action.name = MOCK_ACTION_DELETE;

            action.actionIcon = MOCK_ICON_REMOVE;

            action.supportsBulk = false;

            actions.add(action);

            return actions;
        }


    }

}