@isTest
public class SQX_Test_2321_NC_Disposition_Deletion{

    static Boolean runAllTests = true,
                   run_givenANC_WhenDispostionIsCompleted_DispositionCannotBeDeleted = false;


    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
    * Given : NC with dispostion is completed
    * When  : Disposition is deleted
    * Then : Error is thrown
    */
    public testmethod static void givenANC_WhenDispostionIsCompleted_DispositionCannotBeDeleted(){

        if(!runAllTests && !run_givenANC_WhenDispostionIsCompleted_DispositionCannotBeDeleted){
            return;
        }

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        System.runas(standardUser){ 

            // Arrange : Create a NC with two disposition with one status open and other completed
            SQX_Test_NC nc = new SQX_Test_NC();

            nc.setDispositionRules(true, Date.today(), false, null)
              .save();

            nc.setStatus(SQX_NC.STATUS_TRIAGE).save();
            nc.setStatus(SQX_NC.STATUS_OPEN).save();

            SQX_Part__c part = SQX_Test_Part.insertPart(null, new User(Id = UserInfo.getUserId()), true, '');

            SQX_NC_Impacted_Product__c product = new SQX_NC_Impacted_Product__c();
            product.SQX_Impacted_Part__c = part.Id;
            product.SQX_Nonconformance__c = nc.nc.Id;
            product.Lot_Number__c = 'asd';
            product.Lot_Quantity__c = 100;
            
            new SQX_DB().op_insert(new List<SQX_NC_Impacted_Product__c>{product}, new List<Schema.SObjectField>{
                    Schema.SQX_NC_Impacted_Product__c.SQX_Impacted_Part__c,
                    Schema.SQX_NC_Impacted_Product__c.SQX_Nonconformance__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Number__c,
                    Schema.SQX_NC_Impacted_Product__c.Lot_Quantity__c
                });

            SQX_Disposition__c disposition = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         Status__c = 'Open',
                                                         Lot_Number__c = 'asd',
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 50);
            
            SQX_Disposition__c disposition2 = new SQX_Disposition__c(SQX_Nonconformance__c = nc.nc.Id,
                                                         Status__c = 'Complete',
                                                         Lot_Number__c = 'asd',
                                                         SQX_Part__c =  part.Id, 
                                                         Disposition_Quantity__c = 50,
                                                         Completed_On__c = Date.Today().addDays(-1),
                                                         Completed_by__c = 'Test User');



            new SQX_DB().op_insert(new List<SQX_Disposition__c>{disposition, disposition2}, new List<Schema.SObjectField>{
                    SQX_Disposition__c.fields.SQX_NonConformance__c,
                    SQX_Disposition__c.fields.Status__c,
                    SQX_Disposition__c.fields.Lot_Number__c,
                    SQX_Disposition__c.fields.SQX_Part__c ,
                    SQX_Disposition__c.fields.Disposition_Quantity__c,
                    SQX_Disposition__c.fields.Completed_On__c,
                    SQX_Disposition__c.fields.Completed_by__c
                });

            // Act 1: Delete dispostion with status open
            Database.DeleteResult deleteResult1 = Database.delete(disposition, false);

            // Assert 1: Disposition deletion should be successful
            System.assertEquals(true, deleteResult1.isSuccess(), 'Expected the delete to be successful');

            // Act 2: Delete dispostion with status complete
            Database.DeleteResult deleteResult2 = Database.delete(disposition2, false);

            // Assert 2: Disposition delete should not be successful
            System.assertEquals(false, deleteResult2.isSuccess(),'Expected delete to be unsuccessful');

            // Assert 3 : Unsuccessful deletion should give correct error message.
            System.assert(SQX_Utilities.getFormattedErrorMessage(deleteResult2.getErrors()).contains(Label.SQX_ERR_MSG_COMPLETED_DISPOSITION_CANT_BE_DELETED),'Expected to get correct error message');
            

        }
    }
}