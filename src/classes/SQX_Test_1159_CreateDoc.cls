/**
* This test covers the following scenarios discussed in SQX-1159
* a) Create a controlled doc with valid file
*/
@IsTest
public class SQX_Test_1159_CreateDoc {

	/*
	Helper methods to fill in required field in controlled doc and get it ready for insertion
	*/
	private static void setupControlledDoc(SQX_Controlled_Document__c doc){
		
		integer randomNumber = (Integer)( Math.random() * 1000000 );
		doc.Document_Number__c = 'Doc' + randomNumber;
		doc.Revision__c = 'REV-1';
		doc.Title__c = 'Document Title ' + randomNumber;
		doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
        doc.Document_Status__c = SQX_Controlled_Document.STATUS_DRAFT;
	}

	public static boolean run_allTests = true,
						  run_givenControlledDocIsCreatedWithAFile_FileIsUsed = false,
						  run_givenControlledDocIsCreatedFromTemplate_TemplateIsUsed = false,
						  run_givenControlledDocIsCreatedWithoutAFile_ItIsNotSaved = false,
						  run_gettersCoverageOnly = false,
						  run_givenNumberAndRevIsChanged_NameIsChanged = false;
	
	/**
	* This test ensures that when a file is provided the provided file is used.
	* a) A file is only provided, file is used
	* b) A file and has large file is provided, file is used
	*/
	public testmethod static void givenControlledDocIsCreatedWithAFile_FileIsUsed(){

		if(!run_allTests && !run_givenControlledDocIsCreatedWithAFile_FileIsUsed){
			return;
		}


		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){

        	//A:
        	//Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
			Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';
			setupControlledDoc(doc);

			//Act: Call Create Method/Action on the extension to Create a new Controlled Document
			PageReference returnUrl = null;
			try{
				returnUrl = extension.create();
			}
			catch(Exception ex){
				//ignore any exception, because any exception will cause the assertion to fail
				System.assert(false, ex);
			}


			//Assertion:
			//1. Assert that returnUrl is not null, because a null PageReference is returned on an error only
			System.assertNotEquals(null, returnUrl, ApexPages.getMessages());

			//2. Assert that the Content Version's Version Data was set to the set text.
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			ContentDocument savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	ContentVersion versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertEquals(extension.fileName, versionedContent.PathOnClient);
            System.assertEquals(extension.file + '', versionedContent.VersionData + '');


            //B:
            //Arrange: create a doc with has large file checked and file provided.
            doc = new SQX_Controlled_Document__c();
            controller = new ApexPages.StandardController(doc);
            extension = new SQX_Extension_Controlled_Document(controller);

         	extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';
			setupControlledDoc(doc);

			//Act:
			extension.create();

			//Assert: that the saved file has same content as file
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertEquals(extension.fileName, versionedContent.PathOnClient);
            System.assertEquals(extension.file + '', versionedContent.VersionData + '');
		}
	}


	/**
	* This test ensures that when a template the provided template is used
	* a) When template is provided, template is used
	* b) When template is provided with has large file set, template is used
	* c) When template is provided with file set, file is used
	*/
	public testmethod static void givenControlledDocIsCreatedFromTemplate_TemplateIsUsed(){

		if(!run_allTests && !run_givenControlledDocIsCreatedFromTemplate_TemplateIsUsed){
			return;
		}


		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
        	//A:
        	//Arrange: 

        	//Step 1: Create a templated document and use that to create a new document 
			Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
			
			SQX_Controlled_Document__c tempDoc = new SQX_Controlled_Document__c();
			ApexPages.StandardController tempController = new ApexPages.StandardController(tempDoc);
			SQX_Extension_Controlled_Document tempExtension = new SQX_Extension_Controlled_Document(tempController);

			tempExtension.file = Blob.valueOf('Hello World');
			tempExtension.fileName = 'Hello.txt';
			setupControlledDoc(tempDoc);
			tempDoc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();

			tempExtension.create();

			//Step 2: Create a doc to use that template

			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);
			doc.Document_Type__c = tempDoc.Id;


			//Act: Call Create Method/Action on the extension to Create a new Controlled Document
			PageReference returnUrl = null;
			try{
				returnUrl = extension.create();
			}
			catch(Exception ex){
				//ignore any exception, because any exception will cause the assertion to fail
				System.assert(false, ex);
			}

			//Additional assertion
			System.assert(true, extension.getIsStandardDoc());


			//Assertion:
			//1. Assert that returnUrl is not null, because a null PageReference is returned on an error only
			System.assertNotEquals(null, returnUrl, ApexPages.getMessages());

			//2. Assert that the Content Version's Version Data was set to the set text.
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			ContentDocument savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	ContentVersion versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertEquals(tempExtension.fileName, versionedContent.PathOnClient);
            System.assertEquals(tempExtension.file + '', versionedContent.VersionData + '');

            //B:
            //Arrange: Create a controlled doc with has large file checked and template set
            doc = new SQX_Controlled_Document__c();
			controller = new ApexPages.StandardController(doc);
			extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);
			doc.Document_Type__c = tempDoc.Id;
			extension.hasLargeFile = true;

			//Act:
			extension.create();

			//Assert: assert that the content match the template not the filler text
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertEquals(tempExtension.fileName, versionedContent.PathOnClient);
            System.assertEquals(tempExtension.file + '', versionedContent.VersionData + '');


            //C:
            //Arrange: Create a controlled doc with template and has file
            doc = new SQX_Controlled_Document__c();
			controller = new ApexPages.StandardController(doc);
			extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);
			doc.Document_Type__c = tempDoc.Id;
			extension.file = Blob.valueOf('Hello World FROM File');
			extension.fileName = 'Hello_File.txt';

			//Act:
			extension.create();


			//Assert: assert that the content match the provided file and not template
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertEquals(extension.fileName, versionedContent.PathOnClient);
            System.assertEquals(extension.file + '', versionedContent.VersionData + '');


         
		}
	}


	/**
	* This test ensures that when a large file is checked the filler text is used
	*/
	public testmethod static void givenControlledDocIsCreatedWithALargeFile_FillerIsUsed(){

		if(!run_allTests && !run_givenControlledDocIsCreatedWithAFile_FileIsUsed){
			return;
		}


		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
        	//Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
			Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);
			extension.hasLargeFile = true;

			//Act: Call Create Method/Action on the extension to Create a new Controlled Document
			PageReference returnUrl = null;
			try{
				returnUrl = extension.create();
			}
			catch(Exception ex){
				//ignore any exception, because any exception will cause the assertion to fail
				System.assert(false, ex);
			}


			//Assertion:
			//1. Assert that returnUrl is not null, because a null PageReference is returned on an error only
			System.assertNotEquals(null, returnUrl, ApexPages.getMessages());

			//2. Assert that the Content Version's Version Data was set to the set text.
			doc = [SELECT Id, Content_Reference__c FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];
			ContentDocument savedContentDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE 
                                              Id =: doc.Content_Reference__c];

          	ContentVersion versionedContent = [SELECT ID, VersionData, PathOnClient
                                            FROM ContentVersion
                                            WHERE Id = : savedContentDoc.LatestPublishedVersionId];

            System.assertNotEquals(null, versionedContent.PathOnClient);
            System.assertNotEquals(null, versionedContent.VersionData);
         
		}
	}

	/**
	* This test ensures that when nothing is provided an exception is thrown.
	*/
	public testmethod static void givenControlledDocIsCreatedWithoutAFile_ItIsNotSaved(){

		if(!run_allTests && !run_givenControlledDocIsCreatedWithoutAFile_ItIsNotSaved){
			return;
		}

		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
        	//Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
			Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);

			//Act: Call Create Method/Action on the extension to Create a new Controlled Document
			PageReference returnUrl = null;
			try{
				returnUrl = extension.create();
			}
			catch(Exception ex){
				//ignore any exception, because any exception will cause the assertion to fail
				System.assert(false, ex);
			}

			//Assert: An exception message was set
			System.assertNotEquals(0, ApexPages.getMessages().size());
			
			//Assert that doc id is null i.e. it is not saved
			System.assertEquals(null, doc.Id);
		}

	}



	/**
	* This test ensures that the getters return the data correctly. Provide correct information
	*/
	public testmethod static void gettersCoverageOnly(){
		if(!run_allTests && !run_gettersCoverageOnly){
			return;
		}

		Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
		SQX_Controlled_Document__c doc = new SQX_Test_Controlled_Document().doc;
		ApexPages.StandardController controller = new ApexPages.StandardController(doc);
		SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);


		//Act: Get the list of distribution library(Workspaces). Note: No arrange steps because Workspaces can't be created.
		List<SelectOption> options = extension.getLibraryNames();
		boolean hasValidOptions = true;

		//Assert: Ensure that all items in selected list are of workspace type.
		for(SelectOption option : options){
			if(!String.isBlank(option.getValue())){
				String sobjectType = Id.valueOf(option.getValue()).getSObjectType() + '';
				if(!sobjectType.equals('ContentWorkspace')){
					hasValidOptions = false;
					break;
				}
			}
		}

		//Assert: that valid data was returned.
		System.assertEquals(true, hasValidOptions);


		//Act: Get the list of draft library(Workspaces). Note: No arrange steps because Workspaces can't be created.
		options = extension.getDraftLibraryNames();
		hasValidOptions = true;

		//Assert: Ensure that all items in selected list are of workspace type.
		for(SelectOption option : options){
			if(!String.isBlank(option.getValue())){
				String sobjectType = Id.valueOf(option.getValue()).getSObjectType() + '';
				if(!sobjectType.equals('ContentWorkspace')){
					hasValidOptions = false;
					break;
				}
			}
		}

		//Assert: that valid data was returned.
		System.assertEquals(true, hasValidOptions);


		//Act: Get the list of release library(Workspaces). Note: No arrange steps because Workspaces can't be created.
		options = extension.getReleaseLibraryNames();
		hasValidOptions = true;

		//Assert: Ensure that all items in selected list are of workspace type.
		for(SelectOption option : options){
			if(!String.isBlank(option.getValue())){
				String sobjectType = Id.valueOf(option.getValue()).getSObjectType() + '';
				if(!sobjectType.equals('ContentWorkspace')){
					hasValidOptions = false;
					break;
				}
			}
		}

		//Assert: that valid data was returned.
		System.assertEquals(true, hasValidOptions);


		//Arrange: insert a new doc
		extension.file = Blob.valueOf('Hello World');
		extension.fileName = 'Hello.txt';
		setupControlledDoc(doc);

		extension.create();


		controller = new ApexPages.StandardController([SELECT Id, Content_Reference__c, Valid_Content_Reference__c, Document_Status__c, Is_Scorm_Content__c 
														FROM SQX_Controlled_Document__c WHERE Id = : doc.Id]);
		extension = new SQX_Extension_Controlled_Document(controller);


		//Act: Get the content link
		PageReference link = extension.getContentPage();

		//Assert: Ensure the valid id is present in the link
		System.assertNotEquals(null, link);

	}


	/**
	* this test ensures that the name is combination of number and rev.
	*/
	public static testmethod void givenNumberAndRevIsChanged_NameIsChanged(){
		if(!run_allTests && !run_givenNumberAndRevIsChanged_NameIsChanged){
			return;
		}

		User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);

        System.runAs(standardUser){
        	//Arrange: Set the current page to editor page, and fill in all the relevant data similar to a form fillup
			Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
			SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
			ApexPages.StandardController controller = new ApexPages.StandardController(doc);
			SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

			setupControlledDoc(doc);

			extension.file = Blob.valueOf('Hello World');
			extension.fileName = 'Hello.txt';
			
			//Act: Save the document
			extension.create();

			//Assert: Ensure that name is combination of number and rev
			SQX_Controlled_Document__c fetchedDoc = [SELECT Id, Name FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];

			System.assertEquals(doc.Document_Number__c + '[' + doc.Revision__c + ']', fetchedDoc.Name);


			//Act: update the doc
			doc.Name = 'XYZ';

			new SQX_DB().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
					Schema.SQX_Controlled_Document__c.Name
				});

			//Assert: ensure name is same
			fetchedDoc = [SELECT Id, Name FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];

			System.assertEquals(doc.Document_Number__c + '[' + doc.Revision__c + ']', fetchedDoc.Name);

			//Act: change revision
			doc.Revision__c = 'RevisionChanged';
			new SQX_DB().op_update(new List<SQX_Controlled_Document__c> {doc}, new List<Schema.SObjectField>{
					Schema.SQX_Controlled_Document__c.Revision__c
				});

			//Assert: ensure name is same
			fetchedDoc = [SELECT Id, Name FROM SQX_Controlled_Document__c WHERE Id = : doc.Id];

			System.assertEquals(doc.Document_Number__c + '[' + doc.Revision__c + ']', fetchedDoc.Name);



		}



	}



}