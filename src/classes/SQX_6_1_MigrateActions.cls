/**
* Batch processor to sync owner of an action (SQX_Action__c) to realted assignee.
* Note: Action trigger SQX_Action_Trigger will add sharing rule for main record owner.
* Added in version  : 6.1.0
* Used for versions : all earlier versions
*/
global with sharing class SQX_6_1_MigrateActions implements Database.Batchable<SObject> {
    
    /**
    * debug message for limits
    */
    void logLimits(boolean isStart) {
        System.debug(String.format('SQX_6_1_MigrateActions.execute() {0} limits, cpu time: {1}, soql: {2}/{3}, dml: {4}/{5}',
                                    new String[]{
                                        (isStart ? 'starting' : 'ending'),
                                        '' + Limits.getCpuTime(), 
                                        '' + Limits.getQueries(),
                                        '' + Limits.getLimitQueries(),
                                        '' + Limits.getDMLStatements(),
                                        '' + Limits.getLimitDMLStatements()
                                    })
        );
    }
    
    global SQX_6_1_MigrateActions() { }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, SQX_User__c, OwnerId FROM SQX_Action__c WHERE SQX_User__c != null';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> objList) {
        logLimits(true);
        
        if (objList.size() > 0) {
            List<SQX_Action__c> objsToUpdate = new List<SQX_Action__c>();
            
            for (SQX_Action__c act : (List<SQX_Action__c>)objList) {
                if (act.OwnerId != act.SQX_User__c) {
                    // set owner as asignee
                    act.OwnerId = act.SQX_User__c;
                    objsToUpdate.add(act);
                }
            }
            
            if (objsToUpdate.size() > 0) {
                // owner can be set manually by organization admin
                /**
                * WITHOUT SHARING has been used 
                * ---------------------------------
                * Without sharing has been used because the running user may not have permission changing the actions' owner
                * This will cause and error.
                */
                new SQX_DB().continueOnError().withoutSharing().op_update(objsToUpdate, new List<Schema.SObjectField>{SQX_Action__c.fields.OwnerId});

            }
        }
        
        logLimits(false);
    }
    
    global void finish(Database.BatchableContext bc) { }
}