/**
 * This class provides invocable method which is invoked from process builder to create regulatory report.
 */
public with sharing class SQX_Create_Regulatory_Report {

    /**
     * This method creates regulatory report for Decision Trees which have been run.
     * @param dts collection of Decision Trees for which regulatory report needs to be created.
     */
    @InvocableMethod(label='Create Regulatory Report')
    public static void createRegulatoryReportBasedOnAnswerOptionAttribute(List<SQX_Decision_Tree__c> dts) {
        
        List<Id> taskIds = new List<Id>();
        Set<Id> decisionTreeIds = new Set<Id>();
        List<Id> answerOptionIds = new List<Id>();
        Set<Id> complaintIds = new Set<Id>();
        Map<String, SQX_Regulatory_Report__c> reportMap = new Map<String, SQX_Regulatory_Report__c>();
        
        for(SQX_Decision_Tree__c dt: dts) {
            decisionTreeIds.add(dt.Id);
            complaintIds.add(dt.SQX_Complaint__c);
        }

        List<SQX_Decision_Tree__c> decisionTrees = [SELECT Id, SQX_Task__c, CreatedById, SQX_Complaint__c,SQX_Complaint__r.OwnerId, SQX_Complaint__r.Reporting_Aware_Date__c, SQX_Complaint__r.SQX_Regulatory_Owner__c, SQX_Complaint__r.Country_of_Origin__c, (SELECT Id, SQX_Answer_Option__c FROM SQX_Decision_Tree_Answers__r ORDER BY Answer_Sequence__c ASC) FROM SQX_Decision_Tree__c WHERE Id IN : decisionTreeIds];
        for (SQX_Decision_Tree__c dt : decisionTrees) {
            taskIds.add(dt.SQX_Task__c);
            for (SQX_DT_Answer__c answer : dt.SQX_Decision_Tree_Answers__r) {
                answerOptionIds.add(answer.SQX_Answer_Option__c);
            }
        }
        
        Map<Id, SQX_Answer_Option__c> answerOptionsMap = new Map<Id, SQX_Answer_Option__c>([SELECT Id, 
                                                                                                    (SELECT Id,
                                                                                                        Attribute_Type__c,
                                                                                                        Name,
                                                                                                        Due_In_Days__c,
                                                                                                        Report_Name__c,
                                                                                                        toLabel(Report_Name__c) reportLabel,
                                                                                                        Reportable__c,
                                                                                                        Reg_Body__c,
                                                                                                        SQX_Answer_Option__c,
                                                                                                        Value__c,
                                                                                                        Country__c,
                                                                                                        Report_Type__c
                                                                                                     FROM SQX_Answer_Option_Attributes__r) 
                                                                                            FROM SQX_Answer_Option__c WHERE Id IN : answerOptionIds]);
        
        for (SQX_Decision_Tree__c dt : decisionTrees) {
            for (SQX_DT_Answer__c dtAnswer : dt.SQX_Decision_Tree_Answers__r) {
                if (answerOptionsMap.get(dtAnswer.SQX_Answer_Option__c) != null) {
                    for (SQX_Answer_Option_Attribute__c attribute : answerOptionsMap.get(dtAnswer.SQX_Answer_Option__c).SQX_Answer_Option_Attributes__r) {
                        
                        Date awareDate = dt.SQX_Complaint__r.Reporting_Aware_Date__c;
                        Integer dueInDays = attribute.Due_In_Days__c != null ? (Integer)attribute.Due_In_Days__c : 0;
                        Id assigneeId = dt.SQX_Complaint__r.SQX_Regulatory_Owner__c != null ? dt.SQX_Complaint__r.SQX_Regulatory_Owner__c : dt.CreatedById;

                        // create report if answer option attribute is reportable.
                        if (attribute.Reportable__c && (attribute.Country__c == null || attribute.Country__c == dt.SQX_Complaint__r.Country_of_Origin__c)) {

                            SQX_Regulatory_Report__c report = new SQX_Regulatory_Report__c(
                                                                    SQX_Complaint__c = dt.SQX_Complaint__c,
                                                                    SQX_Decision_Tree__c = dt.Id,
                                                                    Due_Date__c = awareDate != null ? awareDate.addDays(dueInDays) : System.today().addDays(dueInDays),
                                                                    Name = String.valueOf(attribute.get('reportLabel')),
                                                                    Report_Name__c = attribute.Report_Name__c,
                                                                    Comment__c = '',
                                                                    SQX_User__c = assigneeId,
                                                                    Reg_Body__c = attribute.Reg_Body__c,
                                                                    Report_Type__c = attribute.Report_Type__c);

                            // this key makes unique regulatory report
                            String key = '' + dt.Id + attribute.Name + attribute.Value__c;
                            reportMap.put(key,report);
                        }
                    }
                }
            }
        }
        
        // save regulatory report.
        new SQX_DB().op_insert(reportMap.values(), new List<Schema.SObjectField> {Schema.SQX_Regulatory_Report__c.SQX_Complaint__c,
                                                                                  Schema.SQX_Regulatory_Report__c.SQX_Decision_Tree__c,
                                                                                  Schema.SQX_Regulatory_Report__c.Due_Date__c,
                                                                                  Schema.SQX_Regulatory_Report__c.Name,
                                                                                  Schema.SQX_Regulatory_Report__c.Comment__c,
                                                                                  Schema.SQX_Regulatory_Report__c.SQX_User__c,
                                                                                  Schema.SQX_Regulatory_Report__c.Reg_Body__c
                                                                                });
    }
}