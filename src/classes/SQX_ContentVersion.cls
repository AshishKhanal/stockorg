/**
*/
public with sharing class SQX_ContentVersion {

    public final static String RT_CONTROLLED_CONTENT = 'Controlled_Content',
                               RT_CONTROLLED_DOCUMENT = 'Controlled_Document',
                               RT_TEMPLATE_DOCUMENT = 'Template_Document',
                               RT_AUDIT_CRITERIA = 'Audit_Criteria',
                               RT_INSPECTION_CRITERIA = 'Inspection_Criteria',
                               RT_COURSE = 'Course',
                               RT_CQCollaboration = 'CQ_Collaboration',
                               RT_SCORM_CONTENT = 'SCORM_Content',
                               RT_CQ_CONTENT = 'CQ_Content',
                               RT_CQ_Regulatory_Report='CQ_Reg_Report';

    public final static String ACTION_CHECK_IN_SECONDARY_CONTENT = 'checkInSecondaryContent',
                               ACTION_REMOVE_RENDITION_LOG = 'removeRenditionLog',
                               ACTION_SUBMIT_FOR_APPROVAL = 'submitForApproval';

    private static final String ESUBMISSION_IDENTIFIER = 'ESUBMISSION_';

    public static final String CUSTOM_ACTION_ESUBMISSION = ESUBMISSION_IDENTIFIER + '{0}',  // {0} to be replaced by Regulatory Report Id
                               CUSTOM_ACTION_ESUBMISSION_INITIAL = ESUBMISSION_IDENTIFIER + '_INITIAL_{0}',
                               CUSTOM_ACTION_ESUBMISSION_ADDITIONAL = ESUBMISSION_IDENTIFIER + '_ADDITIONAL_{0}',
                               CUSTOM_ACTION_ESUBMISSION_FINAL = ESUBMISSION_IDENTIFIER + '_FINAL_{0}';

    /**
    * Returns controlled content type that we have to
    */
    public static Set<Id> getControlledRecordTypes(){
        Map<String, Map<String, Id>> recordTypes = SQX_Utilities.getRecordTypeIDsByDevelopernameFor(new String[] { SQX.ContentVersion });
        Map<String, Id> contentRecordTypes =  recordTypes.get(SQX.ContentVersion);
        
        return new Set<Id> { contentRecordTypes.get(RT_CONTROLLED_DOCUMENT), 
                                contentRecordTypes.get(RT_TEMPLATE_DOCUMENT), 
                                contentRecordTypes.get(RT_CONTROLLED_CONTENT), 
                                contentRecordTypes.get(RT_AUDIT_CRITERIA), 
                                contentRecordTypes.get(RT_INSPECTION_CRITERIA),
                                contentRecordTypes.get(RT_COURSE)};
    }

    private static Id scormContentRecordTypeId;
    private static Id cqContentRecordTypeId;
    private static Id cqRegContentRecordTypeId;

    /**
    *   Returns SCORM Content Record Type Id
    */
    public static Id getScormContentRecordTypeId(){
        if(scormContentRecordTypeId == null){
            scormContentRecordTypeId = Id.valueOf(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, RT_SCORM_CONTENT));
        }
        
        return scormContentRecordTypeId;
    }

    /**
    *   Returns CQ Content Record Type Id
    */
    public static Id getCQContentRecordTypeId(){
        if(cqContentRecordTypeId == null){
            cqContentRecordTypeId = Id.valueOf(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, RT_CQ_CONTENT));
        }
        
        return cqContentRecordTypeId;
    }

    /**
     * Method returns record type id for regulatory contents.
    */
    public static Id getRegulatoryContentRecordType(){
        if(cqRegContentRecordTypeId == null){
            cqRegContentRecordTypeId = Id.valueOf(SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, RT_CQ_Regulatory_Report));
        }

        return cqRegContentRecordTypeId;
    }



    public with sharing class Bulkified extends SQX_BulkifiedBase {
    
        
        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newVersions equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<ContentVersion> newVersions, Map<Id,ContentVersion> oldMap){
        
            super(newVersions, oldMap);
        }

        /**
        * enforce one to one relation between content version and controlled document
        */
        public Bulkified preventMultipleContentForSingleDocument() {
            List<ContentVersion> allContentVersion= this.resetView().view;

            Set<Id> controlledDocIds = getIdsForField(allContentVersion, Schema.ContentVersion.Controlled_Document__c);
            
            if(controlledDocIds.size() > 0){            
                Set<Id> controlledContentTypes = getControlledRecordTypes();
                
                

                //create a map to get content reference of related controlled document
                Map<Id, SQX_Controlled_Document__c> relatedControlledDouments= new Map<Id, SQX_Controlled_Document__c>([SELECT Content_Reference__c,Secondary_Content_Reference__c  FROM  SQX_Controlled_Document__c WHERE Id IN :controlledDocIds ]);
    
    
                for(ContentVersion contentVersion: allContentVersion){
                    
                    if(controlledContentTypes.contains(contentVersion.RecordTypeId) && contentVersion.Controlled_Document__c != null){
                        SQX_Controlled_Document__c doc = relatedControlledDouments.get(contentVersion.Controlled_Document__c);
    
                        /*
                        * OLD : Previously content was saved after doc so content reference should be null when refering to controlled document
                        * NEW: In the new method content reference is always saved before controlled document so controlled document should
                        * have reference to the Content
                        */
                        if(!SQX_Controlled_Document.allowReferencingOf(doc, contentVersion)){
    
                            contentVersion.addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_CANT_BE_REFERENCED);
                        }
    
                    }
    
                        
                       
                }
            }
            
            
            return this;
            
        }
    
        /**
        * this method prevents changing the controlled document related content version's Controlled Document link
        * and its record type.
        */
        public Bulkified preventChangesToControlledDoc(){
            List<ContentVersion> newVersion = (List<ContentVersion>) this.newValues;
            Map<Id, ContentVersion> oldVersions = (Map<Id, ContentVersion>) this.oldValues;
            
            Set<Id> controlledContentTypes = getControlledRecordTypes();

            
            for(ContentVersion version : newVersion){
                ContentVersion oldVersion = oldVersions.get(version.Id);
                if (controlledContentTypes.contains(oldVersion.RecordTypeId) && oldVersion.RecordTypeId != version.RecordTypeId) {
                    // prevent record type change of CQ contents
                    version.addError(Label.SQX_ERR_MSG_CANNOT_CHANGE_CONTENTVERSION_RECORD_TYPE);
                }
            }

            return this;
        }


        
        /**
        * Prevents any content from being uploaded
        * a) if the user doesn't have enough access in controlled document.
        * b) if the related controlled document is not in DRAFT state.
        */
        public Bulkified preventDocUpload(){

            Set<Id> controlledContentTypes = getControlledRecordTypes();

            Rule newDocRule = new Rule();

            for(Id controlledId : controlledContentTypes){
                newDocRule.addRule(Schema.ContentVersion.RecordTypeId, RuleOperator.Equals, controlledId);
            }

            newDocRule.operator = RuleCombinationOperator.OpOr;
             
            this.resetView().applyFilter(newDocRule, RuleCheckMethod.OnCreate);
            
            if(newDocRule.evaluationResult.size() > 0){
                List<ContentVersion> contents = new List<ContentVersion>();
                Map<Id, SQX_Controlled_Document__c> relatedDocs = new Map<Id, SQX_Controlled_Document__c>();
                Map<Id, ContentDocument> contentDocument = new Map<Id, ContentDocument>();

                for(ContentVersion version : (List<ContentVersion>)  newDocRule.evaluationResult){
                    if(!SQX_Controlled_Document.allowReferencingOf(version)){
                        contents.add(version);

                        if(version.Controlled_Document__c != null){
                            relatedDocs.put(version.Controlled_Document__c, null);
                        }

                        contentDocument.put(version.ContentDocumentId, null);
                    }
                }

                if(contents.size() > 0){
                    Set<Id> controlledDocIds = getIdsForField(newDocRule.evaluationResult, Schema.ContentVersion.Controlled_Document__c);

                    List<Id> relatedDocsIds = new List<Id>( relatedDocs.keySet() );
                    if(relatedDocsIds.size() > 0){
                        relatedDocs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Is_Modifiable__c, Checked_Out__c, SQX_Checked_Out_By__c, Approval_Status__c
                                       FROM SQX_Controlled_Document__c
                                       WHERE Id IN :relatedDocsIds ]);
                    }

                    List<Id> contentDcIds = new List<Id>( contentDocument.keySet() );
                    contentDocument = new Map<Id, ContentDocument>([SELECT Id, ParentId, PublishStatus, LatestPublishedVersionId
                                                                    FROM ContentDocument
                                                                    WHERE Id IN : contentDcIds ]);

                    
                    Map<Id, UserRecordAccess> access = new Map<Id, UserRecordAccess>([SELECT RecordId, HasEditAccess
                                               FROM UserRecordAccess
                                               WHERE RecordId IN : controlledDocIds AND UserId = : UserInfo.getUserId()]);


                    for(ContentVersion version : (List<ContentVersion>)newDocRule.evaluationResult){

                        //add a error message to notify that default record type has to be add other than controlled content
                        if(version.Controlled_Document__c==null){
                            version.addError(Label.SQX_ERR_MSG_CONTENT_NO_ASSOCIATED_DOC);
                            continue;
                        }

                        ContentDocument content = contentDocument.get(version.ContentDocumentId);
                        SQX_Controlled_Document__c doc = relatedDocs.get(version.Controlled_Document__c);

                        if (doc != null) {
                            if(doc.Checked_Out__c && doc.SQX_Checked_Out_By__c != UserInfo.getUserId()){
                                 version.addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_IS_CHECKOUT);
                            }else if(!SQX_Controlled_Document.isDocumentContentModifiable(doc)){
                                    version.addError(Label.SQX_ERR_MSG_CONTROLLED_DOC_IS_LOCKED);
                            }
                        }


                        UserRecordAccess rac = access.get(version.Controlled_Document__c);

                        if(Test.isRunningTest() && rac == null)
                            continue;

                        // Previously: RW access on controlled document was enforced because the document was pushed to library where others could change content
                        // Currently: Document is initially in private library, we want the collaborators with whom the user wants to share the doc
                        // to be able to upload if they have write access.
                        if(content.ParentId != null && !access.get(version.Controlled_Document__c).HasEditAccess){
                            version.addError(Label.SQX_ERR_MSG_INSUFFICIENT_ACCESS_ON_DOC);
                        }
                    }
                }
                
            }
            
            return this;
        }

        /**
        * Do not allow direct update of title and description of content doc if it is related to conrolled doc
        * and changed title or description doesn't match the related controlled doc's title or description
        * TODO : Please add unit test for this after working on SQX-1796
        */
        public Bulkified preventTitleorDescriptionUpdate(){
            //Get all content versions whose title or description are changed
            Rule titleOrDescriptionChanged = new Rule();
            titleOrDescriptionChanged.addRule(Schema.ContentVersion.Title, RuleOperator.HasChanged);
            titleOrDescriptionChanged.addRule(Schema.ContentVersion.Description, RuleOperator.HasChanged);
            titleOrDescriptionChanged.operator = RuleCombinationOperator.OpOr;
            
            this.resetView().applyFilter(titleOrDescriptionChanged, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);

            
            if(titleOrDescriptionChanged.evaluationResult.size()>0){
                List<ContentVersion> titleOrDescriptionChangedContentDocs = new List<ContentVersion>();

                Set<Id> controlledDocIds = new Set<Id>();
                Set<Id> controlledContentTypes = getControlledRecordTypes();

                for(ContentVersion cv : (List<ContentVersion>) titleOrDescriptionChanged.evaluationResult){

                    // even though record type could match controlled document can be null
                    // so lets filter out contents whose record type isn't content type or doc is null
                    if(cv.Controlled_Document__c != null && controlledContentTypes.contains(cv.RecordTypeId)){
                        controlledDocIds.add(cv.Controlled_Document__c);
                        titleOrDescriptionChangedContentDocs.add(cv);
                    }
                }

                if(controlledDocIds.size() > 0){
                    // if title or description has changed, allow the change if it matches with controlled document.
                    // this will let the administrator/migration script sync the name if by any case the two are out of sync

                    Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>([SELECT Id, Title__c, Description__c FROM SQX_Controlled_Document__c WHERE Id IN : controlledDocIds]);

                    for(ContentVersion cv : titleOrDescriptionChangedContentDocs){
                        SQX_Controlled_Document__c relatedDoc = docs.get(cv.Controlled_Document__c);
                        //dont allow to change title directly from content doc if it is related to controlled content 
                        //and changed title or description doesn't match the related controlled doc's title or description
                        // Salesforce trims the Description of controlled document by default so to match the description trim is used 
                        
                        String truncatedDocDescription = SQX_Utilities.getTruncatedText(relatedDoc.Description__c, ContentVersion.Description.getDescribe().getLength(), true);
                        
                        if(cv.Title != relatedDoc.Title__c || cv.Description != truncatedDocDescription){
                            cv.addError(Label.SQX_ERR_MSG_CONTENT_DOC_TITLE_DESC_CANT_CHANGE);
                        }
                        
                    }  
                }

                
            }
            
            return this;
        }


        /**
        * This method synchronizes the change in content with the related controlled document. When the content is uploaded
        * completely. When using salesforce UI to upload the content it has Publish status 'U' or upload interrupted. Once
        * the upload is saved it changed to 'P' or 'R' i.e Public or Private Library. 
        */
        public Bulkified synchronizeContent(){
            final String UPLOAD_INTERRUPTED = 'U',
                         ACTION_NAME = 'synchronizeContent';

            List<ContentVersion> contents = new List<ContentVersion>();
            Set<Id> controlledContentTypes = getControlledRecordTypes();

            this.resetView()
                .removeProcessedRecordsFor(SQX.ContentVersion, ACTION_NAME);

            
            // only select content that have been uploaded completely i.e. have publish status private or public
            for(ContentVersion version : (List<ContentVersion>) this.view){
                if(controlledContentTypes.contains(version.RecordTypeId) && version.Controlled_Document__c != null){
                    if(!this.oldValues.containsKey(version.Id) && version.PublishStatus != UPLOAD_INTERRUPTED){
                        contents.add(version);
                    }
                    else if(this.oldValues.containsKey(version.Id) && this.oldValues.get(version.Id).get(ContentVersion.PublishStatus) == UPLOAD_INTERRUPTED){
                        contents.add(version);
                    }
                }
            }


            if(contents.size()  > 0){
                this.addToProcessedRecordsFor(SQX.ContentVersion, ACTION_NAME, contents);


                List<ContentVersion> contentsToSync = [SELECT Controlled_Document__r.Id, Controlled_Document__r.Document_Status__c, Controlled_Document__r.Approval_Status__c, Controlled_Document__r.Secondary_Content__c,
                                                                   Controlled_Document__r.Secondary_Content_Reference__c, Controlled_Document__r.Content_Reference__c,  Controlled_Document__r.Document_Number__c,
                                                                   ContentDocument.ParentId, ContentDocument.PublishStatus, ContentDocument.Id, CQ_Actions__c
                                                            FROM ContentVersion
                                                            WHERE Id IN : contents];

                Map<SQX_Controlled_Document__c, List<ContentDocument>> docsWithChanges = new Map<SQX_Controlled_Document__c, List<ContentDocument>>();
                Map<Id, SQX_Controlled_Document__c> docs = new Map<Id, SQX_Controlled_Document__c>();

                Set<Id> docsWithAutoCheckIn = new Set<Id>();

                for(ContentVersion version : contentsToSync){
                    if(version.Controlled_Document__r != null){

                        SQX_Controlled_Document__c doc = docs.get(version.Controlled_Document__r.Id);
                        
                        if(doc == null){
                            doc = version.Controlled_Document__r;
                            docs.put(doc.Id, doc);
                        }

                        List<ContentDocument> changedContents = docsWithChanges.get(doc);


                        if(changedContents == null){
                            changedContents = new List<ContentDocument>();
                            if(version.CQ_Actions__c != null) {
                                docsWithAutoCheckIn.add(doc.Id);
                            }
                            docsWithChanges.put(doc, changedContents);
                        }

                        changedContents.add(version.ContentDocument);
                    }
                }


                if(docsWithChanges.keySet().size() > 0){
                    if(docsWithAutoCheckIn.size() > 0){
                        SQX_Controlled_Document.addToAutoContents(docsWithAutoCheckIn);
                    }
                    SQX_Controlled_Document.notifyContentChange(docsWithChanges);
                }

            }

            return this;
            
        }

        /**
        * This method is post feed when members upload new version of main file in collaboration group
        */
        public Bulkified postFeedWhenUploadNewVersion(){
            this.resetView();
            Map<id,id> groupIds = new Map<id,id>();//group and content document link
            Map<id,ContentVersion> mCDIds = new Map<id,ContentVersion>(); //content document id and contentVersion
            Map<id,id> docIDs = new Map<id,id>();//doc id and owner
            Set<Id> contentDocIds = new SQX_BulkifiedBase().getIdsForField((List<ContentVersion>)this.view, Schema.ContentVersion.ContentDocumentId);
            List<ContentVersion> cvList = [SELECT ReasonForChange, RecordTypeId, ContentDocumentID FROM ContentVersion 
                                                    WHERE ContentDocumentId IN: contentDocIds ORDER BY CreatedDate ];
            Id cqCollaborationRecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ContentVersion, SQX_ContentVersion.RT_CQCollaboration);
            for (ContentVersion cv : cvList){ //create contentType to filter only CQ Collab Content
                if (cv.RecordTypeId == cqCollaborationRecordTypeId){
                    mCDIds.put(cv.ContentDocumentID,cv);
                } 
            }
            Set<id> cdIDs = mCDIds.keySet();
            if (cdIDs.size()>0){
                List<ContentDocumentLink> lstCDL = [SELECT Id,ContentDocumentId,ShareType,LinkedEntityId FROM ContentDocumentLink 
                                                    WHERE ContentDocumentId IN :cdIDs];
                for (ContentDocumentLink cdl : lstCDL){
                    //check if linked entity is group (id.sobjecttype?)             
                    Schema.sObjectType entityType = cdl.LinkedEntityId.getSObjectType();
                    if(entityType == CollaborationGroup.sObjectType){
                        groupIds.put(cdl.LinkedEntityId,cdl.ContentDocumentId);
                    }
                }
            }

            if (groupIDs.size()>0){
                List<CollaborationGroup> mCGs = [SELECT id,name,OwnerId FROM CollaborationGroup where id in :groupIDs.keySet()];
                List<ConnectApi.BatchInput> lstFI = new List<ConnectApi.BatchInput>();
                ContentVersion cvToUse;
                for (CollaborationGroup cg: mCGs){
                    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                    messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                    mentionSegmentInput.id = cg.ownerId;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                    cvToUse = mCDIds.get(groupIds.get(cg.id));
                    textSegmentInput.text = ' '+Label.CQ_UI_Document_Review_uploaded+' ';           
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    textSegmentInput = new ConnectApi.TextSegmentInput(); 
                    textSegmentInput.text = cvToUse.ReasonForChange;
                    if (String.isNotEmpty(textSegmentInput.text)){
                        messageBodyInput.messageSegments.add(textSegmentInput);
                    }
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = cg.id;
                    ConnectApi.BatchInput bi  = new ConnectApi.BatchInput(feedItemInput);
                    lstFI.add(bi);
                }
                if(lstFI!=null){
                    ConnectApi.ChatterFeeds.postFeedElementBatch(null,lstFI);
                }
             }
            return this;
        }

        /**
         *  Method prevents content version whose associated assesment is locked from getting updated 
        */
        public Bulkified preventContentUploadOnceAssessmenIsLocked(){

            String scormTypeId = getScormContentRecordTypeId();

            Rule assessmentAssociatedContents = new Rule();
            assessmentAssociatedContents.addRule(Schema.ContentVersion.Assessment__c, RuleOperator.NotEquals, null);
            assessmentAssociatedContents.addRule(Schema.ContentVersion.RecordTypeId, RuleOperator.Equals, scormTypeId);

            this.resetView()
                .applyFilter(assessmentAssociatedContents, RuleCheckMethod.OnCreateAndEveryEdit);

            List<ContentVersion> updatedContents = (List<ContentVersion>) assessmentAssociatedContents.evaluationResult;

            if(updatedContents.size() > 0) {

                Set<Id> relatedAssessmentIds = this.getIdsForField(updatedContents, ContentVersion.Assessment__c);

                Map<Id, Boolean> canUpdateContentMap = SQX_Assessment.canUpdateContent([SELECT Id, Is_Locked__c FROM SQX_Assessment__c WHERE Id IN: relatedAssessmentIds]);

                for(ContentVersion updatedContent : updatedContents){
                    if(canUpdateContentMap.containsKey(updatedContent.Assessment__c) &&
                       !canUpdateContentMap.get(updatedContent.Assessment__c)){
                           updatedContent.addError(Label.SQX_ERR_MSG_CANNOT_UPDATE_LOCKED_ASSESSMENT_CONTENT);
                       }
                }
            }

            return this;
        }

        /**
        * This method is to execute actions that are listed in content version like 'checkInSecondaryContent, removeRenditionLog and submitForApproval'
        */
        public Bulkified performAdditionalActions() {

            Rule allVersionWithActionsAndControlledDocument = new Rule();
            allVersionWithActionsAndControlledDocument.addRule(ContentVersion.CQ_Actions__c, RuleOperator.NotEquals, null);
            allVersionWithActionsAndControlledDocument.addRule(ContentVersion.Controlled_Document__c, RuleOperator.NotEquals, null);

            this.resetView().applyFilter(allVersionWithActionsAndControlledDocument, RuleCheckMethod.OnCreate);
            
            List<ContentVersion> newVersion = (List<ContentVersion>) allVersionWithActionsAndControlledDocument.evaluationResult;
            List<SQX_Controlled_Document__c> docsToUpdate = new List<SQX_Controlled_Document__c>();
            List<SQX_Controlled_Document__c> docsWhoseLogsToRemove = new List<SQX_Controlled_Document__c>();

            Map<Id, ContentVersion> docContentMap = new Map<Id, ContentVersion>();
            for(ContentVersion version : newVersion) {
                List<String> actions = version.CQ_Actions__c.split(',');
                List<String>trimmedActions = new List<String>();
                for(String action : actions) {
                    trimmedActions.add(action.trim());
                }
                String relatedDocId = version.Controlled_Document__c;
                if(trimmedActions.contains(ACTION_CHECK_IN_SECONDARY_CONTENT)) {
                    docContentMap.put(relatedDocId, version);
                }
                
                if(trimmedActions.contains(ACTION_REMOVE_RENDITION_LOG)) {
                    SQX_Controlled_Document__c docToUpdate = new SQX_Controlled_Document__c(Id = relatedDocId);

                    docsWhoseLogsToRemove.add(docToUpdate);
                }

                if(trimmedActions.contains(ACTION_SUBMIT_FOR_APPROVAL)) {
                    SQX_Controlled_Document__c docToUpdate = new SQX_Controlled_Document__c(Id = relatedDocId);

                    docsToUpdate.add(docToUpdate);
                }
            }

            if(docContentMap.keySet().size() > 0) {
                SQX_Controlled_Document.checkInSecondaryContents(docContentMap, false);
            }

            if(docsWhoseLogsToRemove.size() > 0) {
                new SQX_DB().op_delete(SQX_Rendition_Log.getLogsFor(docsWhoseLogsToRemove).values());
            }

            if(docsToUpdate.size() > 0) {
                SQX_Controlled_Document.updateSubmissionStatusOfDocuments(docsToUpdate);
            }

            return this;
        }

        /**
         * This method created submission history and associates it with the related content version.
         */
        public Bulkified associateContentVersionWithSubmissionHistory(){
            
            this.resetView();
            
            Map<Id, ContentVersion> reportAndContentMap = new Map<Id, ContentVersion>();
            
            String recordId = null;
            
            List<ContentVersion> contentVersionsToClear= new List<ContentVersion>();
            
            for(ContentVersion cv : (List<ContentVersion>) this.view) {
                if(cv.CQ_Actions__c != null &&
                   cv.CQ_Actions__c.startsWith(ESUBMISSION_IDENTIFIER)){ 
                       
                       recordId = cv.CQ_Actions__c.substringAfterLast('_');
                       
                       reportAndContentMap.put(recordId, cv);
                       
                       contentVersionsToClear.add(new ContentVersion(Id = cv.Id));
                   }
            }
            
            if(reportAndContentMap.size() > 0) {

                SQX_Submission_History.createSubmissionAndLinkContentFor(reportAndContentMap);

                for(ContentVersion cv:contentVersionsToClear){
                    cv.CQ_Actions__c='';
                }

                new SQX_DB().op_update(contentVersionsToClear, new List<SObjectField> {ContentVersion.CQ_Actions__c});

            }
            return this;
        }

        /**
        * This method prevents the update and deletion of existing submission history attachments.
        */ 
        public Bulkified preventUpdateOfSubmissionHistoryAttachments(){
                        
            Rule allRegulatoryReportsContents = new Rule ();
            allRegulatoryReportsContents.addRule(ContentVersion.VersionNumber, RuleOperator.NotEquals, 1);

            this.resetView().applyFilter(allRegulatoryReportsContents, RuleCheckMethod.OnCreate);
            
            if(this.view.size() > 0) {
                
                Id regReportRTId = SQX_ContentVersion.getRegulatoryContentRecordType();
                
                Map<Id, List<ContentVersion>> versionsByDocumentId = new Map<Id, List<ContentVersion>>();
                
                // New content version we are trying to upload
                for(ContentVersion cv : (List<ContentVersion>) this.view) {
                    
                    if(!versionsByDocumentId.containsKey(cv.ContentDocumentId)) {
                        versionsByDocumentId.put(cv.ContentDocumentId, new List<ContentVersion>());
                    }
                    versionsByDocumentId.get(cv.ContentDocumentId).add(cv);
                }
                
                // original content version
                for(ContentVersion cv : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: versionsByDocumentId.keySet() AND IsLatest=true AND RecordTypeId =: regReportRTId]) {
                    for(ContentVersion viewRecord : versionsByDocumentId.get(cv.ContentDocumentId)) {
                        viewRecord.addError(Label.CQ_ERR_MSG_REGULATORY_CONTENT_CANNOT_BE_UPDATED_OR_DELETED);
                    }
                }
            }
            
            return this;
            
        }
    }




}