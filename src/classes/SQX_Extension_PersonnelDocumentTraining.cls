/**
* extension class of personnel document training object
*/
public with sharing class SQX_Extension_PersonnelDocumentTraining {
    
    ApexPages.StandardController controller;
    SQX_Personnel_Document_Training__c mainRecord;
    PageReference saveAndNewReturnURL;
    
    void setSaveAndNewReturnURL() {
        // return url to SQX_Personnel_Document_Training_Create page with original query params
        saveAndNewReturnURL = Page.SQX_Personnel_Document_Training_Create;
        
        for (String param : ApexPages.currentPage().getParameters().keySet()) {
            saveAndNewReturnURL.getParameters().put(param, ApexPages.currentPage().getParameters().get(param));
        }
    }
    
    PageReference getReturnURL(boolean isSaveAndNew) {
        PageReference retUrl;
        
        if (isSaveAndNew) {
            retUrl = saveAndNewReturnURL;
        } else if (mainRecord.Id != null) {
            // return url to view page of saved record
            retUrl = new ApexPages.StandardController(mainRecord).view();
        } else {
            retUrl = new PageReference(SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));
        }
        
        retUrl.setRedirect(true);
        return retUrl;
    }
    
    public SQX_Extension_PersonnelDocumentTraining(ApexPages.StandardController controller) {
        this.controller = controller;
        this.mainRecord = (SQX_Personnel_Document_Training__c)controller.getRecord();
        setSaveAndNewReturnURL();
        
        // set passed personnel and controlled document values for new record
        if (mainRecord.Id == null) {
            // set personnel value
            String personnelQueryParam = Apexpages.currentPage().getParameters().get('personnel');
            if (String.isNotBlank(personnelQueryParam)) {
                List<SQX_Personnel__c> lPersonnel = [SELECT Id FROM SQX_Personnel__c WHERE Id = :personnelQueryParam LIMIT 1];
                
                if (lPersonnel.size() == 1) {
                    // set personnel field value when passed personnel id value is valid
                    mainRecord.SQX_Personnel__c = lPersonnel[0].Id;
                }
            }
            
            // set document value
            String documentQueryParam = Apexpages.currentPage().getParameters().get('document');
            if (String.isNotBlank(documentQueryParam)) {
                List<SQX_Controlled_Document__c> lDocument = [SELECT Id FROM SQX_Controlled_Document__c WHERE Id = :documentQueryParam LIMIT 1];
                
                if (lDocument.size() == 1) {
                    // set controlled document field value when passed controlled document id value is valid
                    mainRecord.SQX_Controlled_Document__c = lDocument[0].Id;
                }
            }
        }
    }
    
    PageReference saveRecord(boolean isSaveAndNew) {
        PageReference returnTo = null;
        
        Database.SaveResult result = new SQX_DB().continueOnError().op_insert(new List<SQX_Personnel_Document_Training__c> { mainRecord }, new List<Schema.SObjectField>{ }).get(0);
        
        if (result.isSuccess()) {
            returnTo = getReturnURL(isSaveAndNew);
        } else {
            // customize error messages, if required, and return error messages to same page
            List<String> customizedErrors = SQX_Personnel_Document_Training.getCustomizedErrorMessage(mainRecord, result.getErrors());
            for (String msg : customizedErrors) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
            }
        }
        
        return returnTo;
    }
    
    public PageReference save() {
        return saveRecord(false);
    }
    
    public PageReference saveAndNew() {
        return saveRecord(true);
    }

    /**
    * Method to override cancel button to return back to the controlled document detail page or personnel page
    */
    public PageReference cancel() {
        PageReference returnUrl = new PageReference(SQX_Utilities.getHomePageRetUrlBasedOnUserMode(false));

        // Check whether page is accessed through personnel or controlled document
        if(this.mainRecord.SQX_Personnel__c != null){
            returnUrl = new PageReference('/' + this.mainRecord.SQX_Personnel__c);
        } else {
            Pattern urlPattern = Pattern.compile('^[a-zA-Z0-9]{17}_lkid');
            Map<String, String> urlParameters = ApexPages.currentPage().getParameters();
            for(String urlParameter : urlParameters.keySet()){
                Matcher myMatcher = urlPattern.matcher(urlParameter);
                if(myMatcher.matches()){
                    returnUrl = new PageReference('/' + Id.valueOf(urlParameters.get(urlParameter)));
                    break;
                }
            }
        }
        return returnUrl;
    }
}