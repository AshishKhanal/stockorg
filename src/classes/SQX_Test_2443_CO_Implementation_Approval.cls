/**
* tests for CO Implementation Approval
*/
@isTest
public class SQX_Test_2443_CO_Implementation_Approval {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
    * ensures open implementation status is set to "Ready" when related controlled document to create and to revise are submitted for change approval
    * ensures in change approval controlled documents are approved when related change order implementation approval is approved
    * ensures ready implementations are completed when related controlled documents are released
    * ensures related document cannot be added when controlled document is in change approval [validation rule: Ensure_Controlled_Doc_Is_Not_Locked]
    */
    public static testmethod void givenImpWithCreateReviseDocs_ApproveCOImplementation_DocsAreApprovedAndImpCompletes() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser) {
            // add required CO
            SQX_Test_Change_Order co = new SQX_Test_Change_Order().save();
            
            // add required controlled documents
            List<SQX_Controlled_Document__c> docs = new List<SQX_Controlled_Document__c>();
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs.add(docs[0].clone()); // cloned to revise
            docs[0].SQX_Change_Order__c = co.changeOrder.Id;
            docs[1].Auto_Release__c = false;
            docs[1].Revision__c = SQX_Utilities.getNextRev(docs[0].Revision__c);
            docs[1].SQX_Change_Order__c = co.changeOrder.Id;
            insert docs;
            
            // add required new and revise document CO implementations
            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(co.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'New Doc', Date.Today().addDays(1)));
            actions.add(co.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'Revise Doc', Date.Today().addDays(1)));
            actions[0].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[0].SQX_Controlled_Document_New__c = docs[0].Id;
            actions[1].Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            actions[1].SQX_Controlled_Document__c = docs[0].Id;
            actions[1].SQX_Controlled_Document_New__c = docs[1].Id;
            insert actions;
            
            // submit controlled documents for change approval
            docs[0].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            docs[1].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            update docs;
            
            List<SQX_Implementation__c> coActions = [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id IN :actions];
            
            System.assertEquals(SQX_Implementation.STATUS_READY, coActions[0].Status__c,
                '"Open" Implementation status is expected to be "Ready" when document is submitted for change approval.');
            System.assertEquals(SQX_Implementation.STATUS_READY, coActions[1].Status__c,
                '"Open" Implementation status is expected to be "Ready" when document is submitted for change approval.');
            
            // ACT: add related document when controlled document is in change approval
            SQX_Related_Document__c relDoc = new SQX_Related_Document__c(
                Controlled_Document__c = docs[1].Id,
                Referenced_Document__c = docs[0].Id
            );
            Database.SaveResult sr = Database.insert(relDoc, false);
            
            System.assertEquals(false, sr.isSuccess(), 'Related document is not expected to be added when ');
            // Ensure_Controlled_Doc_Is_Not_Locked err msg
            String expectedErrMsg = 'Related Document cannot be added or edited when Controlled Document is in \'Release Approval\', \'In Change Approval\' or \'Obsolete\'';
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expectedErrMsg), 'Expected error: ' + expectedErrMsg);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: approve CO implementation approval
            co.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IMPLEMENTATION_APPROVED;
            update co.changeOrder;
            
            Map<Id, SQX_Controlled_Document__c> coDocs = new Map<Id, SQX_Controlled_Document__c>(
                [SELECT Id, Document_Status__c, Approval_Status__c, Date_Approved__c FROM SQX_Controlled_Document__c WHERE Id IN :docs]);
            
            System.assertEquals(SQX_Controlled_Document.APPROVAL_APPROVED, coDocs.get(docs[0].Id).Approval_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.APPROVAL_APPROVED, coDocs.get(docs[1].Id).Approval_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.STATUS_CURRENT, coDocs.get(docs[0].Id).Document_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(SQX_Controlled_Document.STATUS_APPROVED, coDocs.get(docs[1].Id).Document_Status__c,
                'In change approval document is expected to be approved when related CO implementation is approved.');
            System.assertEquals(System.today(), coDocs.get(docs[0].Id).Date_Approved__c,
                'Document Date Approved field is expected to be todays date when related CO implementation is approved.');
            System.assertEquals(System.today(), coDocs.get(docs[1].Id).Date_Approved__c,
                'Document Date Approved field is expected to be todays date when related CO implementation is approved.');
            
            // ACT: release controlled document approved for change approval
            update new SQX_Controlled_Document__c(
                Id = docs[1].Id,
                Auto_Release__c = true,
                Effective_Date__c = System.today() + 20
            );
            
            coActions = [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id IN :actions];
            
            System.assertEquals(SQX_Implementation.STATUS_COMPLETE, coActions[0].Status__c,
                '"Ready" Implementation status is expected to be "Complete" when document is released.');
            System.assertEquals(SQX_Implementation.STATUS_COMPLETE, coActions[1].Status__c,
                '"Ready" Implementation status is expected to be "Complete" when document is released.');
        }
    }
    
    /**
    * ensures "Ready" implementation status is set to "Open" when related controlled document to create and to revise are recalled from change approval
    * ensures SQX_Extension_Controlled_Document to return proper value when calling getCanRecallFromChangeApproval()
    * ensures validation rule Prevent_Recall_When_CO_In_Impl_Approval to work when controlled document in change approval with CO implementation approval is recalled
    */
    public static testmethod void givenRecallFromChangeApproval_ImpStatusChangesToOpen() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser) {
            // add required COs
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order(); // not implementation approval
            SQX_Test_Change_Order co2 = new SQX_Test_Change_Order();
            co2.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            insert new List<SQX_Change_Order__c>{ co1.changeOrder, co2.changeOrder };
            
            // add required controlled documents
            List<SQX_Controlled_Document__c> docs = new List<SQX_Controlled_Document__c>();
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs.add(new SQX_Test_Controlled_Document().doc); // not in change approval
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs[0].SQX_Change_Order__c = co1.changeOrder.Id;
            docs[0].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            docs[0].Is_Locked__c = true;
            docs[1].SQX_Change_Order__c = co1.changeOrder.Id;
            docs[2].SQX_Change_Order__c = co2.changeOrder.Id;
            docs[2].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            docs[2].Is_Locked__c = true;
            insert docs;
            
            // add required new and revise document CO implementations
            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(co1.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'New Doc 1', Date.Today().addDays(1)));
            actions.add(co1.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'New Doc 2', Date.Today().addDays(1)));
            actions.add(co2.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'New Doc 3', Date.Today().addDays(1)));
            actions[0].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[0].SQX_Controlled_Document_New__c = docs[0].Id;
            actions[0].Status__c = SQX_Implementation.STATUS_READY;
            actions[1].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[1].SQX_Controlled_Document_New__c = docs[1].Id;
            actions[2].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[2].SQX_Controlled_Document_New__c = docs[2].Id;
            actions[2].Status__c = SQX_Implementation.STATUS_READY;
            insert actions;
            
            // ACT: read recall from change approval for doc in change approval with CO not implementation approval
            SQX_Extension_Controlled_Document ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(docs[0]));
            Boolean canRecall = ext.getCanRecallFromChangeApproval();
            
            System.assertEquals(true, canRecall, 'Document in change approval with related CO not implementation approval is expected to be allowed to recall.');
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: recall controlled document from change approval
            PageReference retUrl = ext.recallFromChangeApproval();
            
            System.assertNotEquals(null, retUrl, 'Expected to return url for valid recall from change approval action');
            
            SQX_Controlled_Document__c doc = [SELECT Id, Approval_Status__c, Is_Locked__c FROM SQX_Controlled_Document__c WHERE Id = :docs[0].Id];
            
            System.assertEquals(null, doc.Approval_Status__c,
                'Approval status of controlled document is expected to be cleared when recalled from change approval');
            System.assertEquals(false, doc.Is_Locked__c,
                'Is Locked value of controlled document is expected to be false when recalled from change approval');
            
            SQX_Implementation__c coAction = [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id = :actions[0].Id];
            
            System.assertEquals(SQX_Implementation.STATUS_OPEN, coAction.Status__c,
                '"Ready" Implementation status is expected to be "Open" when document is recalled from change approval.');
            
            // ACT: read recall from change approval for doc not in change approval
            ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(docs[1]));
            canRecall = ext.getCanRecallFromChangeApproval();
            
            System.assertEquals(false, canRecall, 'Document not in change approval is expected not to be allowed to recall.');
            
            // ACT: read recall from change approval for doc in change approval with CO implementation approval
            ext = new SQX_Extension_Controlled_Document(new ApexPages.StandardController(docs[2]));
            canRecall = ext.getCanRecallFromChangeApproval();
            
            System.assertEquals(false, canRecall, 'Document in change approval with related CO implementation approval is expected not to be allowed to recall.');
            
            // ACT: recall document in change approval with related CO implementation approval
            Database.SaveResult sr = Database.update(new SQX_Controlled_Document__c( Id = docs[2].Id, Approval_Status__c = null ), false);
            
            System.assertEquals(false, sr.isSuccess(), 'Recall controlled document from change approval is expected to fail when CO is implementation approval.');
            // Prevent_Recall_When_CO_In_Impl_Approval err msg
            String expectedErrMsg = 'Change Order related to this controlled document is implementation approval.';
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expectedErrMsg), 'Error expected: ' + expectedErrMsg);
        }
    }
    
    /**
    * ensures content cannot be uploaded without document supervisor permission when controlled document is locked using internal field Is_Locked__c
    */
    public static testmethod void givenUploadContentVersion_DocIsLocked_ErrorThrown() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser) {
            // add required controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save(true);
            cDoc1.doc.Is_Locked__c = true;
            cDoc1.save();
            
            ContentVersion cv1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Controlled_Document__c = :cDoc1.doc.Id];
            
            ContentVersion cv2 = cv1.clone();
            cv2.VersionData = Blob.valueOf('New version');
            cv2.PathOnClient = 'v2.txt';
            
            // ACT: update new content when controlled document is locked
            Database.SaveResult sr = Database.insert(cv2, false);
            
            System.assertEquals(false, sr.isSuccess(), 'Update content is expected to fail when controlled document is locked using internal field Is_Locked__c.');
            String expectedErrMsg = Label.SQX_ERR_MSG_CONTROLLED_DOC_IS_LOCKED;
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expectedErrMsg), 'Error expected: ' + expectedErrMsg);
        }
    }
    
    /**
    * ensures CO with new or revised controlled document in change approval can be submitted for implementation approval
    * ensures CO with incomplete generic task cannot be submitted for implementation approval
    * ensures CO with incomplete new or revise controlled document task cannot be submitted for implementation approval
    * ensures CO with incomplete obsolete controlled document task cannot be submitted for implementation approval
    */
    public static testmethod void givenSubmitForImplementationApproval_ValidationOccursAndErrorIsThrownIfInvalid() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser) {
            // add required COs
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order();
            SQX_Test_Change_Order co2 = new SQX_Test_Change_Order();
            SQX_Test_Change_Order co3 = new SQX_Test_Change_Order();
            SQX_Test_Change_Order co4 = new SQX_Test_Change_Order();
            List<SQX_Change_Order__c> changeOrders = new List<SQX_Change_Order__c>();
            changeOrders.add(co1.changeOrder);
            changeOrders.add(co2.changeOrder);
            changeOrders.add(co3.changeOrder);
            changeOrders.add(co4.changeOrder);
            insert changeOrders;
            
            // add required controlled documents
            List<SQX_Controlled_Document__c> docs = new List<SQX_Controlled_Document__c>();
            
            docs.add(new SQX_Test_Controlled_Document().doc); // change approval
            docs[0].SQX_Change_Order__c = changeOrders[0].Id;
            docs[0].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs[1].SQX_Change_Order__c = changeOrders[1].Id;
            
            docs.add(new SQX_Test_Controlled_Document().doc); // change approval
            docs[2].SQX_Change_Order__c = changeOrders[1].Id;
            docs[2].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            
            docs.add(docs[0].clone()); // add revision
            docs[3].Approval_Status__c = null;
            docs[3].Revision__c = SQX_Utilities.getNextRev(docs[0].Revision__c);
            docs[3].SQX_Change_Order__c = changeOrders[2].Id;
            
            docs.add(docs[2].clone()); // add revision for change approval
            docs[4].Approval_Status__c = null;
            docs[4].Revision__c = SQX_Utilities.getNextRev(docs[2].Revision__c);
            docs[4].SQX_Change_Order__c = changeOrders[2].Id;
            docs[4].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            
            docs.add(new SQX_Test_Controlled_Document().doc);
            docs[5].SQX_Change_Order__c = changeOrders[3].Id;
            
            docs.add(new SQX_Test_Controlled_Document().doc); // change approval
            docs[6].SQX_Change_Order__c = changeOrders[3].Id;
            docs[6].Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            insert docs;
            
            // add required new and revise document CO implementations
            List<SQX_Implementation__c> actions = new List<SQX_Implementation__c>();
            actions.add(co1.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'generic tsk', Date.Today().addDays(1)));
            
            actions.add(co1.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'doc in change approval 1', Date.Today().addDays(1))); // added to prevent co to auto complete
            actions[1].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[1].Status__c = SQX_Implementation.STATUS_OPEN;
            
            actions.add(co2.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'new doc 1', Date.Today().addDays(1)));
            actions[2].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[2].SQX_Controlled_Document_New__c = docs[1].Id;
            
            actions.add(co2.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'doc in change approval 2', Date.Today().addDays(1))); // added to prevent co to auto complete
            actions[3].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[3].SQX_Controlled_Document_New__c = docs[2].Id;
            actions[3].Status__c = SQX_Implementation.STATUS_READY;
            
            actions.add(co3.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'revise doc 1', Date.Today().addDays(1)));
            actions[4].Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            actions[4].SQX_Controlled_Document__c = docs[0].Id;
            actions[4].SQX_Controlled_Document_New__c = docs[3].Id;
            
            actions.add(co3.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'revise doc in change approval 3', Date.Today().addDays(1))); // added to prevent co to auto complete
            actions[5].Type__c = SQX_Implementation.AWARE_DOCUMENT_REVISE;
            actions[5].SQX_Controlled_Document__c = docs[2].Id;
            actions[5].Status__c = SQX_Implementation.STATUS_OPEN;
            
            actions.add(co4.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'obsolete doc', Date.Today().addDays(1)));
            actions[6].Type__c = SQX_Implementation.AWARE_DOCUMENT_OBSOLETE;
            actions[6].SQX_Controlled_Document__c = docs[5].Id;
            
            actions.add(co4.addAction(SQX_Implementation.RECORD_TYPE_ACTION, adminUser, 'doc in change approval 4', Date.Today().addDays(1))); // added to prevent co to auto complete
            actions[7].Type__c = SQX_Implementation.AWARE_DOCUMENT_NEW;
            actions[7].SQX_Controlled_Document_New__c = docs[6].Id;
            actions[7].Status__c = SQX_Implementation.STATUS_READY;
            
            insert actions;
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: link in change approval controlled document
            actions[1].SQX_Controlled_Document_New__c = docs[0].Id; // new document task
            actions[5].SQX_Controlled_Document_New__c = docs[4].Id; // revise document task
            
            update new List<SQX_Implementation__c>{ actions[1], actions[5] };
            
            // check action[1] status
            System.assertEquals(SQX_Implementation.STATUS_READY, [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id = : actions[1].Id].Status__c,
                'Implementation status is expected to be ready when in change approval document is linked.');
            // check action[5] status
            System.assertEquals(SQX_Implementation.STATUS_READY, [SELECT Id, Status__c FROM SQX_Implementation__c WHERE Id = : actions[5].Id].Status__c,
                'Implementation status is expected to be ready when in change approval document is linked.');
            
            // ACT: submit not ready COs for implementation approval
            changeOrders[0].Status__c = SQX_Change_Order.STATUS_OPEN;
            changeOrders[0].Record_Stage__c  = SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrders[0].Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            changeOrders[1].Status__c = SQX_Change_Order.STATUS_OPEN;
            changeOrders[1].Record_Stage__c  = SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrders[1].Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            changeOrders[2].Status__c = SQX_Change_Order.STATUS_OPEN;
            changeOrders[2].Record_Stage__c  = SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrders[2].Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            changeOrders[3].Status__c = SQX_Change_Order.STATUS_OPEN;
            changeOrders[3].Record_Stage__c  = SQX_Change_Order.STATUS_IMPLEMENTATION;
            changeOrders[3].Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            Database.SaveResult[] sr = Database.update(changeOrders, false);
            
            String expectedErrMsg = Label.SQX_ERR_MSG_INVALID_CHANGE_ORDER_FOR_IMPLEMENTATION_APPROVAL;
            System.assertEquals(false, sr[0].isSuccess(), 'Submit CO for implementation approval is expected to fail when generic task is open.');
            System.assert(SQX_Utilities.checkErrorMessage(sr[0].getErrors(), expectedErrMsg), 'Expected error: ' + expectedErrMsg);
            System.assertEquals(false, sr[1].isSuccess(), 'Submit CO for implementation approval is expected to fail when new document task is either not complete or not ready.');
            System.assert(SQX_Utilities.checkErrorMessage(sr[1].getErrors(), expectedErrMsg), 'Expected error: ' + expectedErrMsg);
            System.assertEquals(false, sr[2].isSuccess(), 'Submit CO for implementation approval is expected to fail when revise document task is either not complete or not ready.');
            System.assert(SQX_Utilities.checkErrorMessage(sr[2].getErrors(), expectedErrMsg), 'Expected error: ' + expectedErrMsg);
            System.assertEquals(false, sr[3].isSuccess(), 'Submit CO for implementation approval is expected to fail when obsolete document task is incomplete.');
            System.assert(SQX_Utilities.checkErrorMessage(sr[3].getErrors(), expectedErrMsg), 'Expected error: ' + expectedErrMsg);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // complete generic task
            actions[0].Completed_By__c = 'AUser';
            actions[0].Completion_Date__c = System.today() - 1;
            actions[0].Status__c = SQX_Implementation.STATUS_COMPLETE;
            update actions[0];
            
            // release new and revised controlled documents
            docs[1].Effective_Date__c = System.today() + 20;
            docs[1].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            docs[3].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            
            // obsolete controlled document
            docs[5].Expiration_Date__c = System.today() + 15;
            docs[5].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_EXPIRE;
            
            update new List<SQX_Controlled_Document__c>{ docs[1], docs[3], docs[5] };
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT: submit ready COs for implementation approval
            sr = Database.update(changeOrders, false);
            
            System.assertEquals(true, sr[0].isSuccess(), 'Submit CO for implementation approval is expected to succeed when generic task is complete.');
            System.assertEquals(true, sr[1].isSuccess(), 'Submit CO for implementation approval is expected to succeed when new document task is either complete or ready.');
            System.assertEquals(true, sr[2].isSuccess(), 'Submit CO for implementation approval is expected to succeed when revise document task is either complete or ready.');
            System.assertEquals(true, sr[3].isSuccess(), 'Submit CO for implementation approval is expected to succeed when obsolete document task is complete.');
        }
    }
    /**
     * Given: Control doc record in 'In Change Approval' created from change order 
     * When: document is recalled from aura component
     * Then: document is recalled and approval status of doc is set to blank
     */
    public static testmethod void givenDocCreatedFromChangeOrder_WhenDocRecalled_DocApprovalStatusBlanks() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(standardUser) {
            final String BLANK_VALUE = '';
            //Arrange: Create Change Order and control doc record
            SQX_Test_Change_Order co1 = new SQX_Test_Change_Order();
            co1.changeOrder.Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_IN_IMPLEMENTATION_APPROVAL;
            insert co1.changeOrder;
            
            SQX_Test_Controlled_Document contDoc = new SQX_Test_Controlled_Document();
            contDoc.doc.SQX_Change_Order__c = co1.changeOrder.Id;
            contDoc.doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL;
            contDoc.doc.Is_Locked__c = true;
            contDoc.save();
            
            //Pre-Assert: Make sure approval status is In Change Approval
            String approvalStatus = [Select Approval_Status__c from SQX_Controlled_Document__c where id = :contDoc.doc.id].Approval_Status__c;
            System.assertEquals(SQX_Controlled_Document.APPROVAL_IN_CHANGE_APPROVAL, approvalStatus, 'Expected approval status to be In Change Approval');
            
            //Action: Recall document
            SQX_Action_Response response = SQX_Recall_Reassign_ApprovalRequest.recallChangeOrderApproval(contDoc.doc.id);
            SQX_Controlled_Document__c docRecord = [Select Approval_Status__c from SQX_Controlled_Document__c where id = :contDoc.doc.id];
            
            //Assert: Make sure approval status is blank
            System.assertNotEquals(BLANK_VALUE, docRecord.Approval_Status__c, 'Expected approval status to change from In Change Approval to blank');
            System.assertEquals(BLANK_VALUE, BLANK_VALUE, 'Expected approval status to be blank');
        }
    }
}