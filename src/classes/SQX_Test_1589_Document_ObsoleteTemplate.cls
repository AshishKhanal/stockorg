/**
* Unit tests to ensure validation error for using obsolete template document while creating or updating template of a controlled document.
* Tests for Prevent_Obsolete_Template_Document validation rule of Controlled Document.
*/
@IsTest
public class SQX_Test_1589_Document_ObsoleteTemplate {
    
    static Boolean runAllTests = true,
                   run_givenCreateControlledDocument_ObsoleteTemplateDocument_NotSaved = false,
                   run_givenEditControlledDocument_ObsoleteTemplateDocument_NotSaved = false;
    
    // Error message of the Prevent_Obsolete_Template_Document validation rule
    static String ErrMsg_PreventObsoleteTemplateDocument = 'You cannot use obsolete template document for Document Type when creating or modifying a controlled document.';
    
    /**
    * Unit test to ensure Prevent_Obsolete_Template_Document validation error while creating controlled document
    */
    public static testmethod void givenCreateControlledDocument_ObsoleteTemplateDocument_NotSaved() {
        if (!runAllTests && !run_givenCreateControlledDocument_ObsoleteTemplateDocument_NotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required obsolete template document
            SQX_Test_Controlled_Document tDoc1 = new SQX_Test_Controlled_Document();
            tDoc1.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            tDoc1.save(true);
            tDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // ACT: create new controlled document using obsolete template document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.doc.Document_Type__c = tDoc1.doc.Id;
            cDoc1.save(); // uses SQX_Extension_Controlled_Document extension to create new controlled document
            
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            
            boolean errMsgFound = false;
            for (ApexPages.Message msg : msgs) {
                if (msg.getDetail().contains(ErrMsg_PreventObsoleteTemplateDocument)) {
                    errMsgFound = true;
                    break;
                }
            }
            
            System.assert(errMsgFound,
                'Expected new controlled document using obsolete template document is not expected to be created with proper error message.\n' + msgs);
        }
    }
    
    /**
    * Unit test to ensure Prevent_Obsolete_Template_Document validation error while updating template document of a controlled document
    */
    public static testmethod void givenEditControlledDocument_ObsoleteTemplateDocument_NotSaved() {
        if (!runAllTests && !run_givenEditControlledDocument_ObsoleteTemplateDocument_NotSaved) {
            return;
        }
        
        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        System.runas(user1) {
            // add required obsolete template document
            SQX_Test_Controlled_Document tDoc1 = new SQX_Test_Controlled_Document();
            tDoc1.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            tDoc1.save();
            tDoc1.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();
            
            // add required controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            
            // ACT: update controlled document using obsolete template document
            cDoc1.doc.Document_Type__c = tDoc1.doc.Id;
            Database.SaveResult result1 = Database.update(cDoc1.doc, false);
            
            System.assert(result1.isSuccess() == false,
                'Existing controlled document updated with obsolete template document is not expected to be saved.');
            
            boolean errMsgFound = false;
            for (Database.Error err : result1.getErrors()) {
                if (err.getMessage().contains(ErrMsg_PreventObsoleteTemplateDocument)) {
                    errMsgFound = true;
                    break;
                }
            }
            
            System.assert(errMsgFound,
                'Expected error message was not found.\n' + result1.getErrors());
        }
    }
}