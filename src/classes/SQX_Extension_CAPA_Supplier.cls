/**
* @author Pradhanta Bhandari, Sagar Shrestha
* @description this class is the extension for both SCAR response and SCAR creation form
*/
public with sharing class SQX_Extension_CAPA_Supplier extends SQX_Extension_CAPA {

    /**
    * @author Pradhanta Bhandari
    * @date 2014/4/3
    * @description This is the main entry for the constructor, it copies the sent controller to mainController for further reference
    */
    public SQX_Extension_CAPA_Supplier(ApexPages.StandardController controller) {
        super(controller);
        this.customerScriptLayout = '_CuScript_Layout';
    }

    /**
    * Returns the expectation of the response with number of object counts that are supported.
    * @param sendForApproval value provided by caller specifying whether or not submission for approval is necessary
    */
    public override SQX_Upserter.SQX_Upserter_Interceptor getResponseExpectation(boolean sendForApproval) {

        SQX_ResponseExpectation expectation = new SQX_ResponseExpectation(SQX.Response);
        final Integer ANY_NUMBER = expectation.ANY_NUMBER;
        
        Map<String, Integer> objectsCount = new Map<String, Integer>();
        objectsCount.put(SQX.Response, 1);
        objectsCount.put(SQX.Containment, ANY_NUMBER);
        objectsCount.put(SQX.Investigation, ANY_NUMBER);
        objectsCount.put(SQX.InvestigationTool , ANY_NUMBER);
        objectsCount.put(SQX.RootCause, ANY_NUMBER);
        objectsCount.put(SQX.ActionPlan, ANY_NUMBER);
        objectsCount.put(SQX.Action, ANY_NUMBER);
        objectsCount.put(SQX.ResponseInclusion, ANY_NUMBER);
        
        SQX_CAPA__c capa = [SELECT Id, SQX_Finding__c FROM SQX_CAPA__c WHERE Id = : mainRecord.Id];

        expectation.valueChecks.put(SQX.Response, new Map<SObjectField, Id> {
          SQX_Finding_Response__c.SQX_CAPA__c => capa.Id
        });

        expectation.valueChecks.put(SQX.Action, new Map<SObjectField, Id> {
          SQX_Action__c.SQX_CAPA__c => capa.Id
        });

        expectation.requiredCount = new Map<String, Integer> {
            SQX.Response => 1
        };

        expectation.sendForApproval = sendForApproval;
        expectation.numberOfObjectsToBeCreated = objectsCount;

        return expectation;
    }

    /**
    * Returns the configuration for response persistence. It returns a configuration with list of permitted fields
    */
    public override SQX_Upserter.SQX_Upserter_Config getResponseConfig(){

        /*configure to ignore black listed fields, i.e. fields that should not be manipulated directly*/
        SQX_Upserter.SQX_Upserter_Config config = new SQX_Upserter.SQX_Upserter_Config();
        config.omit(SQX.Response, SQX_Finding_Response__c.Status__c)
            .omit(SQX.Response, SQX_Finding_Response__c.Approval_Status__c)
            .omit(SQX.Response, SQX_Finding_Response__c.Published_Date__c)
            .omit(SQX.Investigation, SQX_Investigation__c.Approval_Status__c)
            .omit(SQX.Investigation, SQX_Investigation__c.Status__c)
            .omit(SQX.Investigation, SQX_Investigation__c.SQX_Primary_Contact__c)
            .omit(SQX.Investigation, SQX_Investigation__c.SQX_Account__c)
            .omit(SQX.ActionPlan, SQX_Action_Plan__c.Approved__c)
            .omit(SQX.Action, SQX_Action__c.SQX_Action_Plan__c)
            .omit(SQX.Action, SQX_Action__c.Is_Approved__c)
            .omit(SQX.Action, SQX_Action__c.Status__c)
            .omit(SQX.Containment, SQX_Containment__c.Status__c);

        return config;
    }
}