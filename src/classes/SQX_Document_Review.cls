/**
* This class will contain the common methods and static strings required for the Document Review object 
*/
public with sharing class SQX_Document_Review {

    // static value for the document review decision type
    public static final String  REVIEW_DECISION_CONTINUE_USE = 'Continue Use',
                                REVIEW_DECISION_RETIRE = 'Retire',
                                REVIEW_DECISION_REVISE = 'Revise';

    /**
    * returns change order statuses that will be used while retiring documents in document review
    */
    public static Set<String> getChangeOrderStatusesRetiringDocument() {
        // CO statuses other than not closed or complete (i.e. incomplete) are used for retiring documents
        return SQX_Change_Order.getIncompleteStatuses();
    }
    
    public with sharing class Bulkified extends SQX_BulkifiedBase{

        public Bulkified(){
        }

        /**
        * Default constructor for this class, that accepts old and new map from the trigger
        * @param newDocumentReviews equivalent to trigger.New in salesforce
        * @param oldMap equivalent to trigger.OldMap in salesforce
        */
        public Bulkified(List<SQX_Document_Review__c> newDocumentReviews, Map<Id,SQX_Document_Review__c> oldMap){
        
            super(newDocumentReviews, oldMap);
        }
        
        /**
        * When the document reivew is to retire or continue using the doc,
        * its master controlled doc should be updated
        * @author Anish Shrestha
        * @date 2016/02/08
        * @story [SQX-1967]
        */ 
        public Bulkified changeControlledDocAsPerReviewDecision() {
            final string ACTION_NAME = 'changeControlledDocAsPerReviewDecision';
            Rule reviewNeedingDocumentChangeRule = new Rule();
            reviewNeedingDocumentChangeRule.addRule(Schema.SQX_Document_Review__c.Review_Decision__c, RuleOperator.Equals, REVIEW_DECISION_CONTINUE_USE);
            reviewNeedingDocumentChangeRule.addRule(Schema.SQX_Document_Review__c.Review_Decision__c, RuleOperator.Equals, REVIEW_DECISION_RETIRE);
            reviewNeedingDocumentChangeRule.operator = RuleCombinationOperator.OpOr;
            
            this.resetView()
                .removeProcessedRecordsFor(SQX.DocumentReview, ACTION_NAME)
                .applyFilter(reviewNeedingDocumentChangeRule, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
                
            if (reviewNeedingDocumentChangeRule.evaluationResult.size() > 0) {
                this.addToProcessedRecordsFor(SQX.DocumentReview, ACTION_NAME, reviewNeedingDocumentChangeRule.evaluationResult);
                
                //collect all document number
                List<SQX_Document_Review__c> relatedReviewForDocumentUpdate = reviewNeedingDocumentChangeRule.evaluationResult;
                List<SQX_Controlled_Document__c> documentToBeUpdated =  new List<SQX_Controlled_Document__c>();

                Set<Id> docIds = getIdsForField(this.view, Schema.SQX_Document_Review__c.Controlled_Document__c);
                
                Map<Id, SQX_Controlled_Document__c> documentsWithReviewUpdate = new Map<Id, SQX_Controlled_Document__c>([SELECT Document_Status__c FROM SQX_Controlled_Document__c WHERE Id IN: docIds]);

                for(SQX_Document_Review__c docReview : relatedReviewForDocumentUpdate){
                    if (documentsWithReviewUpdate.containsKey(docReview.Controlled_Document__c)) {
                        SQX_Controlled_Document__c controlledDoc = documentsWithReviewUpdate.get(docReview.Controlled_Document__c);
                        if(docReview.Review_Decision__c ==  SQX_Document_Review.REVIEW_DECISION_RETIRE){
                            controlledDoc.Document_Status__c = docReview.SQX_Obsolete_Document_Change_Order__c == null ? controlledDoc.Document_Status__c : SQX_Controlled_Document.STATUS_PRE_EXPIRE;
                            controlledDoc.Expiration_Date__c = docReview.Expiration_Date__c;
                            controlledDoc.Next_Review_Date__c = null;
                        }
                        else{
                            if(controlledDoc.Document_Status__c == SQX_Controlled_Document.STATUS_PRE_EXPIRE){
                                controlledDoc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
                                controlledDoc.Expiration_Date__c = null;
                            }
                        }
                        documentToBeUpdated.add(controlledDoc);
                    }
                }
                
                //update all the documents
                //WITHOUT SHARING must be used because the document may have been created
                //by someother user.
                new SQX_DB().withoutSharing().op_update(documentToBeUpdated, new List<Schema.SObjectField>{ SQX_Controlled_Document__c.Fields.Document_Status__c,
                                                                                                            SQX_Controlled_Document__c.Fields.Expiration_Date__c  });
            
            }
            
            return this;
        }
        
        /**
        * validates linked change order of retire document review to be have an action to retire the related document
        */
        public Bulkified checkChangeOrderWithRetireDocumentAction() {
            final string ACTION_NAME = 'checkChangeOrderWithRetireDocumentAction';
            
            // get new doc reviews with linked change order or updated doc reviews with changed change order
            Rule filter = new Rule();
            filter.addRule(Schema.SQX_Document_Review__c.Review_Decision__c, RuleOperator.Equals, SQX_Document_Review.REVIEW_DECISION_RETIRE);
            filter.addRule(Schema.SQX_Document_Review__c.SQX_Obsolete_Document_Change_Order__c, RuleOperator.NotEquals, null);
            filter.addRule(Schema.SQX_Document_Review__c.SQX_Obsolete_Document_Change_Order__c, RuleOperator.HasChanged);
            
            this.resetView()
                .applyFilter(filter, RuleCheckMethod.OnCreateAndEditThatMakesItMeet);
                
            if (filter.evaluationResult.size() > 0) {
                // get related change order ids
                Set<Id> coIds = getIdsForField(SQX_Document_Review__c.SQX_Obsolete_Document_Change_Order__c);
                
                // get change orders with implementations to obsolete controlled document
                Map<Id, SQX_Change_Order__c> changeOrders = new Map<Id, SQX_Change_Order__c>(
                   [SELECT Id, Name, Status__c,
                       (SELECT Id, SQX_Controlled_Document__c FROM SQX_Implementations__r WHERE (CQ_Aware_Type__c = :SQX_Implementation.AWARE_DOCUMENT_OBSOLETE OR compliancequest__Type__c=:SQX_Implementation.AWARE_DOCUMENT_OBSOLETE))
                    FROM SQX_Change_Order__c
                    WHERE Id IN :coIds]
                );
                
                Set<String> openCOStatuses = getChangeOrderStatusesRetiringDocument();
                
                for (SQX_Document_Review__c review : (List<SQX_Document_Review__c>)filter.evaluationResult) {
                    SQX_Change_Order__c co = changeOrders.get(review.SQX_Obsolete_Document_Change_Order__c);
                    
                    Boolean hasActions = false;
                    // verify whether retiring document is listed in change order obsolete document implementations or not
                    for (SQX_Implementation__c action : co.SQX_Implementations__r) {
                        if (action.SQX_Controlled_Document__c == review.Controlled_Document__c) {
                            hasActions = true;
                            break;
                        }
                    }
                    if (hasActions == false) {
                        // change order with no actions to retire the document
                        review.addError(Label.SQX_ERR_MSG_CHANGE_ORDER_HAS_NO_ACTIONS_TO_RETIRE_THE_CONTROLLED_DOCUMENT);
                    }
                    else if (!openCOStatuses.contains(co.Status__c)) {
                        // change order is not open for users to work
                        String errMsg = Label.SQX_ERR_MSG_CHANGE_ORDER_WITH_INVALID_STATUS_TO_RETIRE_THE_CONTROLLED_DOCUMENT;
                        errMsg = errMsg.replace('{ChangeOrderStatus}', co.Status__c);
                        review.addError(errMsg);
                    }
                }
            }
            
            return this;
        }
    }
}