/**
 * @story : SQX-3682
 * @description : Unit tests for validating functionalities related to SCORM Assessments
*/
@isTest
public class SQX_Test_3682_Scorm_For_Assessment {
    
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser1');
        User standardUser2 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser2');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');

        SQX_Test_Personnel personnel1, personnel2;
        SQX_Test_Assessment tAss;
        SQX_Test_Controlled_Document cDoc;
        System.runAs(adminUser) {

            // creating random file
            ContentVersion cv = new ContentVersion();
            cv.VersionData = Blob.valueOf('Random Data');
            cv.PathOnClient = 'RandomFile.zip';
            cv.Title = 'FILE';

            new SQX_DB().op_insert(new List<ContentVersion> { cv }, new List<Schema.SObjectField> { ContentVersion.VersionData, ContentVersion.PathOnClient });

            cv = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id];

            // creating SCORM assessment
            tAss = new SQX_Test_Assessment(SQX_Assessment.RT_SCORM_ASSESMENT_API_NAME)
                        .setName('ASSESSMENT_1')
                        .setContent(cv)
                        .setStatus(SQX_Assessment.STATUS_CURRENT)
                        .save();

            ContentDocumentLink link1 = new ContentDocumentLink(LinkedEntityId=standardUser1.Id, ContentDocumentId=cv.ContentDocumentId, ShareType='V'),
                                link2 = new ContentDocumentLink(LinkedEntityId=standardUser2.Id, ContentDocumentId=cv.ContentDocumentId, ShareType='V');

            new SQX_DB().op_insert(new List<ContentDocumentLink> { link1, link2 }, new List<SObjectField> {});


            // creating Controlled Document
            cDoc = new SQX_Test_Controlled_Document()
                        .setStatus(SQX_Controlled_Document.STATUS_CURRENT)
                        .save();

            // creating personnels
            personnel1 = new SQX_Test_Personnel(standardUser1);
            personnel1.save();

            personnel2 = new SQX_Test_Personnel(standardUser2);
            personnel2.save();


            // create pdts, since dml cannot be performed before callout thus creating this pdt with current assessment
            SQX_Personnel_Document_Training__c pdt1 = new SQX_Personnel_Document_Training__c(
                                                            SQX_Personnel__c = personnel1.mainRecord.Id,
                                                            SQX_Controlled_Document__c = cDoc.doc.Id,
                                                            Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                                                            Due_Date__c = System.today(),
                                                            Optional__c = false,
                                                            Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                                                            SQX_Assessment__c = tAss.assessment.Id
                                                        );

            SQX_Personnel_Document_Training__c pdt2 = new SQX_Personnel_Document_Training__c(
                                                            SQX_Personnel__c = personnel2.mainRecord.Id,
                                                            SQX_Controlled_Document__c = cDoc.doc.Id,
                                                            Level_of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_EXHIBIT_COMPETENCY,
                                                            Due_Date__c = System.today(),
                                                            Optional__c = false,
                                                            Status__c = SQX_Personnel_Document_Training.STATUS_PENDING,
                                                            SQX_Assessment__c = tAss.assessment.Id
                                                        );

            new SQX_DB().op_insert( new List<SQX_Personnel_Document_Training__c> { pdt1, pdt2 },
                                       new List<Schema.SObjectField>{
                                            SQX_Personnel_Document_Training__c.SQX_Personnel__c,
                                               SQX_Personnel_Document_Training__c.SQX_Controlled_Document__c,
                                               SQX_Personnel_Document_Training__c.Level_of_Competency__c,
                                               SQX_Personnel_Document_Training__c.Due_Date__c,
                                               SQX_Personnel_Document_Training__c.Optional__c,
                                               SQX_Personnel_Document_Training__c.Status__c,
                                               SQX_Personnel_Document_Training__c.SQX_Assessment__c} );

        }

        System.runAs(standardUser1) {
            // Creating personnel assessments
            SQX_Personnel_Assessment__c personnelAssessment = new SQX_Personnel_Assessment__c (
                                                                    SQX_Personnel__c = personnel1.mainRecord.Id,
                                                                    SQX_Assessment__c = tAss.assessment.Id );

            new SQX_DB().withoutSharing().op_insert(new List<SQX_Personnel_Assessment__c> { personnelAssessment }, 
                                   new List<Schema.SObjectField> {
                                                Schema.SQX_Personnel_Assessment__c.SQX_Personnel__c,
                                                Schema.SQX_Personnel_Assessment__c.SQX_Assessment__c
                                            });

            // Creating personnel assessment attempt
            SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt = new SQX_Personnel_Assessment_Attempt__c(
                                                                                    SQX_Personnel_Assessment__c = personnelAssessment.Id);

            new SQX_DB().withoutSharing().op_insert(new List<SQX_Personnel_Assessment_Attempt__c> { personnelAssessmentAttempt },
                                   new List<Schema.SObjectField> {
                                       SQX_Personnel_Assessment_Attempt__c.SQX_Personnel_Assessment__c });

            personnelAssessment.SQX_Last_Assessment_Attempt__c = personnelAssessmentAttempt.Id;

            new SQX_DB().withoutSharing().op_update(new List<SQX_Personnel_Assessment__c>{personnelAssessment },
                                   new List<Schema.SObjectField>{
                                       SQX_Personnel_Assessment__c.SQX_Last_Assessment_Attempt__c });
        }

        System.runAs(standardUser2) {
            // Creating personnel assessments
            SQX_Personnel_Assessment__c personnelAssessment = new SQX_Personnel_Assessment__c (
                                                                    SQX_Personnel__c = personnel2.mainRecord.Id,
                                                                    SQX_Assessment__c = tAss.assessment.Id );

            new SQX_DB().withoutSharing().op_insert(new List<SQX_Personnel_Assessment__c> { personnelAssessment }, 
                                   new List<Schema.SObjectField> {
                                                Schema.SQX_Personnel_Assessment__c.SQX_Personnel__c,
                                                Schema.SQX_Personnel_Assessment__c.SQX_Assessment__c
                                            });

            // Creating personnel assessment attempt
            SQX_Personnel_Assessment_Attempt__c personnelAssessmentAttempt = new SQX_Personnel_Assessment_Attempt__c(
                                                                                    SQX_Personnel_Assessment__c = personnelAssessment.Id);

            new SQX_DB().withoutSharing().op_insert(new List<SQX_Personnel_Assessment_Attempt__c> { personnelAssessmentAttempt },
                                   new List<Schema.SObjectField> {
                                       SQX_Personnel_Assessment_Attempt__c.SQX_Personnel_Assessment__c });

            personnelAssessment.SQX_Last_Assessment_Attempt__c = personnelAssessmentAttempt.Id;

            new SQX_DB().withoutSharing().op_update(new List<SQX_Personnel_Assessment__c>{personnelAssessment },
                                   new List<Schema.SObjectField>{
                                       SQX_Personnel_Assessment__c.SQX_Last_Assessment_Attempt__c });
        }
    }


    /**
    *   PREVIEW SERVICE
    *   Given : SCORM Assessment
    *   When : Preview Url is requested
    *   Then : Desired url is returned
    */
    static testmethod void givenSCORMAssessment_WhenPreviewUrlIsRequsted_ThenDesiredUrlIsRetrieved(){

        System.runAs(SQX_Test_Account_Factory.getUsers().get('adminUser')){
                    
            // Arrange : Create a SCORM Assessment
            SQX_Test_Assessment tAss = new SQX_Test_Assessment([SELECT Id, Name, RecordTypeId FROM SQX_Assessment__c]);
            tAss.synchronize();

            PageReference expectedPage = Page.SQX_eContent;
            expectedPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, tAss.assessment.Id);

            // Act : request preview url
            Test.startTest();

            SQX_Extension_Assessment ext = new SQX_Extension_Assessment(new ApexPages.StandardController(tAss.assessment));
            String generatedUrl = ext.getScormPreviewUrl();

            Test.stopTest();

            // Assert : Preview url is same
            System.assertEquals(expectedPage.getURL(), generatedUrl);
        }
    }


    
    /**
    *   LAUNCH SERVICE
    *   Given : Document Training With SCORM Assessment
    *   When : Assessment is launched
    *   Then : DT is user signed off
    */
    static testmethod void givenDocumentTrainingWithScormAssessment_WhenAssessmentIsLaunchedAndCompleted_ThenTrainingIsUserSignOff(){

        User standardUser1 = SQX_Test_Account_Factory.getUsers().get('standardUser1'),
            adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser1){

            // Arrange : Get DT with scorm assessment
            SQX_Personnel_Assessment__c pa = [SELECT Id FROM SQX_Personnel_Assessment__c];

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pa.Id);

            Test.setCurrentPage(launchPage);

            // Act : assessment is completed
            Test.startTest();

            SQX_Controller_eContent ct = new SQX_Controller_eContent();
            ct.IsPassed = true;
            ct.signOff();

            Test.stopTest();
        }

        // Assert : Training is user-signed off
        System.runAs(adminUser) {
            List<SQX_Personnel_Document_Training__c> pdts = [SELECT Status__c
                                                                FROM SQX_Personnel_Document_Training__c
                                                                WHERE SQX_Personnel__r.SQX_User__c =: standardUser1.Id];

            System.assertEquals(1, pdts.size());
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING, pdts.get(0).Status__c);
        }
    }

    /**
    *   Given : Document Training On SCORM Assessment for User A
    *   When : User B tries to launch assessment
    *   Then : Error is thrown
    */
    static testmethod void givenDocumentTrainingWithScormAssessment_WhenSomeOtherUserTriesToLaunchTheAssessment_ErrorIsThrown(){

        User standardUser1 = SQX_Test_Account_Factory.getUsers().get('standardUser1'),
            standardUser2 = SQX_Test_Account_Factory.getUsers().get('standardUser2');

        SQX_Personnel_Assessment__c pa;
        System.runAs(standardUser1){

            // Arrange : create a document training on scorm content
            pa = [SELECT Id FROM SQX_Personnel_Assessment__c];

        }

        System.runAs(standardUser2){

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pa.Id);

            // Act : Get page
            Test.setCurrentPage(launchPage);
     
            SQX_Controller_eContent ct = new SQX_Controller_eContent();
 
            // Assert : Expected invalid access error
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD), 'Expected error message : ' + Label.SQX_ERR_MSG_NO_READ_ACCESS_ON_RECORD);
        }
    }

    /**
     * This testmethod makes use of esig policies and validates sign off
     * @story [SQX-6188]
     */
    static testmethod void givenVariousEsigPolicies_TheSignOffWorksAsExpected(){

        SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getOrgDefaults();
        cqES.Consumer_Key__c = 'Mock';
        cqES.Consumer_Secret__c = 'Mock';
        cqES.Enabled__c = true;
        cqES.Ask_Username__c = true;
        Database.insert(cqES, true);

        User standardUser1 = SQX_Test_Account_Factory.getUsers().get('standardUser1'),
             standardUser2 = SQX_Test_Account_Factory.getUsers().get('standardUser2'),
             adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        System.runAs(standardUser1){

            // Arrange : Get Scorm assessment
            SQX_Personnel_Assessment__c pa = [SELECT Id FROM SQX_Personnel_Assessment__c];

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pa.Id);

            Test.setCurrentPage(launchPage);

            Test.setMock(HttpCalloutMock.class, new MockAuthClass());

            // ACT : Sign off Assessment
            SQX_Controller_eContent ct = new SQX_Controller_eContent();
            ct.IsPassed = true;
            PageReference pr = ct.signOff();

            // Assert : Sign off is not completed and invalid username/password error is thrown
            System.assertEquals(null, pr);
            System.assert(SQX_Utilities.checkPageMessage(ApexPages.getMessages(), Label.SQX_INVALID_USERNAME_PASSWORD));
        }

        System.runAs(standardUser2){

            // Arrange : Get another scorm assessment
            SQX_Personnel_Assessment__c pa = [SELECT Id FROM SQX_Personnel_Assessment__c];

            PageReference launchPage = Page.SQX_eContent;
            launchPage.getParameters().put(SQX_Controller_eContent.PARAM_ID, pa.Id);

            Test.setCurrentPage(launchPage);

            // Arrange : Set policy to avoid password
            SQX_CQ_Esig_Policies__c policy = new SQX_CQ_Esig_Policies__c(
                Name = 'Policy1',
                Action__c = SQX_Controller_eContent.CODE_OF_ESIG_SCORM_ASSESSMENT_SIGN_OFF,
                Required__c = false,
                Object_Type__c = '' + SQX_Personnel_Assessment__c.SObjectType
            );
            insert policy;
            

           // ACT : Sign off Assessment
            SQX_Controller_eContent ct = new SQX_Controller_eContent();
            ct.IsPassed = true;
            PageReference pr = ct.signOff();

        }

        // Assert : Training is user-signed off
        System.runAs(adminUser) {
            List<SQX_Personnel_Document_Training__c> pdts = [SELECT Status__c
                                                                FROM SQX_Personnel_Document_Training__c
                                                                WHERE SQX_Personnel__r.SQX_User__c =: standardUser2.Id];

            System.assertEquals(1, pdts.size());
            System.assertEquals(SQX_Personnel_Document_Training.STATUS_TRAINER_APPROVAL_PENDING, pdts.get(0).Status__c);
        }
    }



    /**
     * Mock class to handle esig validation
     */
    public class MockAuthClass implements HttpCalloutMock {
        /**
        * @description returns a mock response
        * @param HTTPRequest mock request
        */
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(200);
            
            String url = req.getEndpoint();
            if(url.contains('rightPassword')){
                response.setBody('{"id":"allcorrect"}');
            }
            else{
                response.setBody('{"error":"incorrect password"}');
            }
            return response;
        }
    }
}
