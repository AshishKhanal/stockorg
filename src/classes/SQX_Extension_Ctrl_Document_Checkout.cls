/*
* This class used for controlled document checkout, checkin and undo checkout in lightning
*/
public with sharing  class SQX_Extension_Ctrl_Document_Checkout {
    /**
     * This method return user permissions
     * @param 'docId' controlled document record Id
     */
    @AuraEnabled
    public static SQX_Permissions getPermissions(String docId){
        SQX_Controlled_Document__c doc = [SELECT Id, SQX_Checked_Out_By__c FROM SQX_Controlled_Document__c WHERE Id =: docId];
        
        SQX_Permissions obj= new SQX_Permissions();
        obj.isUserCheckOutUser = SQX_Controlled_Document_Collaboration.isUserCheckOutUser(doc);
        obj.isSupervisorUser = SQX_Controlled_Document_Collaboration.userHasSupervisoryPermission();
        obj.hasEditAccess = SQX_Approval_Util.getUserRecordAccess(new Set<Id> { doc.Id },UserInfo.getUserId(),true).get(doc.Id).hasEditAccess;
        return obj;
    }
    
    /**
     * wrapper class for user permissions
     */
    public class SQX_Permissions{
        @AuraEnabled
        public Boolean isUserCheckOutUser { get; set; }
        @AuraEnabled
        public Boolean isSupervisorUser { get; set; }
        @AuraEnabled
        public Boolean hasEditAccess { get; set; }
    }
}