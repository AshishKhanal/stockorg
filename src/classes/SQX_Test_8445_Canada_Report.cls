/**
 * Test Class to ensure the Type of Report Is Set
 */
@isTest
public class SQX_Test_8445_Canada_Report {
    
    static final String ADMIN_USER = 'adminUser',
                        STANDARD_USER = 'standardUser';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, ADMIN_USER);
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, STANDARD_USER);
    }
    
    /**
     * GIVEN: Initial Regulatory Report of a Complaint
     * WHEN: Canada Report for Follow-up Regulatory Report is created
     * THEN: Type of report of Canada Report should be Update
     */
    static testMethod void givenInitialRegulatoryReportOfComplaint_WhenCanadaReportForFollowUpRegulatoryReportIsCreated_ThenTypeOfReportOfCanadaShouldBeUpdate(){
        User standardUser = SQX_Test_Account_Factory.getUsers().get(STANDARD_USER);
        System.runAs(standardUser) {
            
            // Create Complaint record
            SQX_Test_Complaint complaint = new SQX_Test_Complaint(standardUser);
            complaint.save();
            // ARRANGE: Create Initial Regulatory Report for the Complainte
            SQX_Regulatory_Report__c regulatoryReport = new SQX_Regulatory_Report__c();
            regulatoryReport.Name = 'CA-10 Day Reportable Test2018812';
            regulatoryReport.Report_Name__c = 'CA:10-day MDPR';
            regulatoryReport.Due_Date__c =  Date.today() + 21;
            regulatoryReport.SQX_Complaint__c = complaint.complaint.Id;
            regulatoryReport.Reg_Body__c = 'Health Canada';
            regulatoryReport.Report_Type__c = 'Initial report';
            regulatoryReport.SQX_User__c = standardUser.Id;
            
            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c, SQX_Regulatory_Report__c.Name, SQX_Regulatory_Report__c.Reg_Body__c, SQX_Regulatory_Report__c.SQX_Complaint__c});            
            
            // Create Canada Report for the Regulatory Report
            SQX_Canada_Report__c canadaReport = new SQX_Canada_Report__c();
            canadaReport.SQX_Regulatory_Report__c = regulatoryReport.Id;
            canadaReport.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            canadaReport.Type_of_Report__c = 'Preliminary';
            canadaReport.Reporter_Type__c = 'Manufacturer';
            canadaReport.Reporter_Contact_Info__c = 'Test contact';
            canadaReport.Mfr_Name__c = 'Mfr Name';
            canadaReport.Mfr_Address__c = 'Mfr Address';
            canadaReport.Mfr_Id__c = 'Mfr Id';
            canadaReport.Mfr_Licence_Number__c = 'Mfr Licence No';
            canadaReport.Type_of_Incident__c = '10-Day';
            canadaReport.Source_of_Incident__c = 'Canadian';
            canadaReport.Special_Incident_Circumstances__c = 'Investigational Testing';
            canadaReport.Date_of_Incident__c = Date.today();
            canadaReport.Reporter_Awareness_Date__c = Date.today();
            canadaReport.Patient_Consequences__c = 'Consequences';
            canadaReport.Brand_Name__c = 'Brand Name';
            canadaReport.Serial_Number__c = 'L100';
            canadaReport.Expiration_Date__c = Date.today().addDays(30);
            canadaReport.Device_Classification__c = 'I';
            canadaReport.Device_Id__c = 'Device Id';
            canadaReport.Device_Licence_Number__c = 'Device L';
            canadaReport.Software_Version__c = '2.0';
            canadaReport.Mfr_Device_Id__c = 'Mfr Dev Id';
            canadaReport.Age_of_Device__c = '10 years';
            canadaReport.Device_Usage_Period__c = '30 days';
            canadaReport.Device_Was_Sterile__c = 'Yes';
            canadaReport.Availability_Of_Device__c = 'Destroyed';
            canadaReport.Complainant_Type__c = 'Consumer';
            canadaReport.Name_of_Complainant__c = 'CName';
            canadaReport.Health_Care_Facility_Name__c = 'Facility Name';
            canadaReport.Complainant_Address__c = 'CAddress';
            canadaReport.Complainant_Email__c = 'Email@email.com';
            canadaReport.Inv_Actions_and_Timeline__c = 'Something';
            
            new SQX_DB().op_insert(new List<SQX_Canada_Report__c>{canadaReport}, new List<SObjectField>{SQX_Canada_Report__c.SQX_Regulatory_Report__c});
            
            // creating a report submission record
            SQX_Submission_History__c submission = new SQX_Submission_History__c(
                SQX_Complaint__c = complaint.complaint.Id,
                Name = 'CA:10-day MDPR',
                Submitted_Date__c = Date.today(),
                Submitted_By__c = 'Some submitter',
                Status__c = SQX_Submission_History.STATUS_COMPLETE
            );
            
            new SQX_DB().op_insert(new List<SQX_Submission_History__c>{submission}, 
                                   new List<SObjectField>{SQX_Submission_History__c.SQX_Complaint__c,SQX_Submission_History__c.Name,SQX_Submission_History__c.Submitted_Date__c,SQX_Submission_History__c.Submitted_By__c,SQX_Submission_History__c.Status__c});
            
            regulatoryReport.Status__c = 'Complete';
            regulatoryReport.Submitted_Date__c = Date.today();
            regulatoryReport.compliancequest__SQX_Submission_History__c = submission.Id;
            
            new SQX_DB().op_update(new List<SQX_Regulatory_Report__c>{regulatoryReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Status__c,SQX_Regulatory_Report__c.Submitted_Date__c});
            
            // Create Follow-Up Regulatory Report
            SQX_Regulatory_Report__c followUpRegReport = new SQX_Regulatory_Report__c();
            followUpRegReport.Name = 'CA-10 Day Follow Test2018812';
            followUpRegReport.Report_Name__c = 'CA:10-day MDPR';
            followUpRegReport.Due_Date__c =  Date.today() + 21;
            followUpRegReport.SQX_Complaint__c = complaint.complaint.Id;
            followUpRegReport.Reg_Body__c = 'Health Canada';
            followUpRegReport.SQX_User__c = standardUser.Id;
            followUpRegReport.SQX_Follow_Up_Of__c = regulatoryReport.Id;

            new SQX_DB().op_insert(new List<SQX_Regulatory_Report__c>{followUpRegReport}, new List<SObjectField>{SQX_Regulatory_Report__c.Due_Date__c, SQX_Regulatory_Report__c.Name, SQX_Regulatory_Report__c.Reg_Body__c, SQX_Regulatory_Report__c.SQX_Complaint__c}); 
            
            // ACT: Create Canada Report for Follow-Up Regulatory Report
            SQX_Canada_Report__c canadaFollowUpReport = new SQX_Canada_Report__c();
            canadaFollowUpReport.SQX_Regulatory_Report__c = followUpRegReport.Id;
            canadaReport.Activity_Code__c = SQX_Regulatory_Report_eSubmitter.ACTIVITY_CODE_VALIDATE;
            new SQX_DB().op_insert(new List<SQX_Canada_Report__c>{canadaFollowUpReport}, new List<SObjectField>{SQX_Canada_Report__c.SQX_Regulatory_Report__c});
            
            SQX_Canada_Report__c mappedCanadaReport = [SELECT Id, Type_of_Report__c FROM SQX_Canada_Report__c WHERE Id = :canadaFollowUpReport.Id];     
            
            
            // ASSERT: Ensure Type Of Report is set Update
            System.assertEquals('Update', mappedCanadaReport.Type_of_Report__c, 'Type Of Report Not set to Update');
        }
    }

}