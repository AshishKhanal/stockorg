/**
* Unit tests for CAPA Close SQX-1465 
*
* @author Sagar Shrestha
* @date 2015/09/15
* 
*/
@isTest
public class SQX_Test_1465_Close_CAPA{

    /**
    *   Setup: Create a CAPA (C1) and complete it
    *   Action: Close CAPA
    *   Expected: Close CAPA should be successful
    *
    * @author Sagar Shrestha
    * @date 2015/09/15
    * 
    */
    public static testmethod void GivenCAPACompleteAndERNotRequired_ClosureSuccessful(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            //Arrange
            //Save capa and make all responses not required and initiate the capa
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility();
            capa.finding.setRequired(false, false, false, false, false);//response not required, containment not reqd, investigation not required, ca not required, pa not required
            capa.CAPA.Needs_Effectiveness_Review__c = false;
            capa.CAPA.Status__c= SQX_CAPA.STATUS_COMPLETE;
            capa.save();

            //Assert that capa goes to completion as none of responses are required
            System.AssertEquals(SQX_CAPA.STATUS_COMPLETE, [Select Status__c from SQX_CAPA__c WHERE Id = :capa.capa.Id].Status__c);
            
            //Act
            //Close the CAPA
            capa.CAPA.Status__c= SQX_CAPA.STATUS_CLOSED;
            capa.CAPA.Resolution__c = SQX_CAPA.RESOLUTION_EFFECTIVE;
            capa.CAPA.Closure_Comment__c = 'CAPA CLosed';
            capa.CAPA.Rating__c = 3;

            Database.SaveResult closeCAPAResult = Database.Update(capa.CAPA,false);

            //Assert : CAPA Close is successful as CAPA is complete and ER not required
            System.AssertEquals(true ,closeCAPAResult.isSuccess(),
                 'Expected CAPA close to be successful '+capa.CAPA);




        }
    }


    /**
    *   Setup: Create a CAPA with Effectiveness Review Required (C1) and complete it
    *   Action: Close CAPA without ER proovided
    *   Expected: Close CAPA should not be successful
    *
    *   Setup: Create a CAPA with Effectiveness Review Required (C1) and complete it
    *   Action: Close CAPA with ER saved which has resolution as Schedule Another Review
    *   Expected: Close CAPA should not be successful
    *
    *   Setup: Create a CAPA with Effectiveness Review Required (C1) and complete it
    *   Action: Close CAPA with ER saved which has resolution as Effective
    *   Expected: Close CAPA should be successful
    *
    * @author Sagar Shrestha
    * @date 2015/09/15
    * 
    */
    public static testmethod void GivenCAPACompleteAndERRequired_ClosureNotSuccessful(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            //Arrange
            //Save capa and make all responses not required and complete the capa
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility();
            capa.finding.setRequired(false, false, false, false, false);//response not required, containment not reqd, investigation not required, ca not required, pa not required
            capa.CAPA.Needs_Effectiveness_Review__c = true;
            capa.CAPA.Status__c= SQX_CAPA.STATUS_COMPLETE;
            capa.save();

            //Act
            //Close the CAPA
            capa.CAPA.Status__c= SQX_CAPA.STATUS_CLOSED;
            capa.CAPA.Resolution__c = SQX_CAPA.RESOLUTION_EFFECTIVE;
            capa.CAPA.Closure_Comment__c = 'CAPA CLosed';
            capa.CAPA.Rating__c = 3;

            //Act : CLose CAPA
            Database.SaveResult closeCAPAResultWithoutER = Database.Update(capa.CAPA,false);

            //Assert: CAPA close unsuccessful as ER is not provided
            System.AssertEquals(false ,closeCAPAResultWithoutER.isSuccess(),
                 'Expected CAPA close to be unsuccessful '+capa.CAPA);

            SQX_Effectiveness_Review__c reviewScheduleAnother = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_SCHEDULE_ANOTHER_REVIEW,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today(),
                                                                                 Next_Review_Date__c=Date.today()
                                                                                 );
            
            insert reviewScheduleAnother;


            //Act : CLose CAPA
            Database.SaveResult closeCAPAResultWithScheduledER = Database.Update(capa.CAPA,false);

            //Assert: CAPA close unsuccessful as ER is not effective 
            System.AssertEquals(false ,closeCAPAResultWithScheduledER.isSuccess(),
                 'Expected CAPA close to be unsuccessful '+capa.CAPA);

            SQX_Effectiveness_Review__c reviewEffective = new SQX_Effectiveness_Review__c(SQX_CAPA__c=capa.CAPA.Id, 
                                                                                 Status__c = SQX_Effectiveness_Review.STATUS_COMPLETED,
                                                                                 Resolution__c=SQX_Effectiveness_Review.RESOLUTION_CLOSED_EFFECTIVE,
                                                                                 Rating__c = '3',
                                                                                 Reviewed_By__c='Sagar',
                                                                                 Review_Started_On__c=Date.today(),
                                                                                 Review__c='effective and complete',
                                                                                 Review_Completed_On__c = Date.today()
                                                                                 );
            
            insert reviewEffective;


            //Act : CLose CAPA
            Database.SaveResult closeCAPAResultWithEffectiveER = Database.Update(capa.CAPA,false);

            //Assert: CAPA close successful as ER is effective 
            System.AssertEquals(true ,closeCAPAResultWithEffectiveER.isSuccess(),
                 'Expected CAPA close to be successful '+capa.CAPA);
        }
    }

    /**
    *   Setup: Create a CAPA with Effectiveness Review Required (C1) and Status Open
    *   Action: Close CAPA 
    *   Expected: Close CAPA should not be successful
    *
    * @author Sagar Shrestha
    * @date 2015/09/15
    * 
    */

    public static testmethod void GivenCAPAOpen_ClosureNotSuccessful(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            //Arrange
            //Save capa and make all responses not required and initiate the capa
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility();
            capa.finding.setRequired(false, false, false, false, false);//response not required, containment not reqd, investigation not required, ca not required, pa not required
            capa.CAPA.Needs_Effectiveness_Review__c = true;
            capa.CAPA.Status__c= SQX_CAPA.STATUS_OPEN;
            capa.save();

            //Act
            //Close the CAPA
            capa.CAPA.Status__c= SQX_CAPA.STATUS_CLOSED;
            capa.CAPA.Resolution__c = SQX_CAPA.RESOLUTION_EFFECTIVE;
            capa.CAPA.Closure_Comment__c = 'CAPA CLosed';
            capa.CAPA.Rating__c = 3;

            Database.SaveResult closeCAPAResult = Database.Update(capa.CAPA,false);

            //Assert: CLose CAPA should be unsuccessful
            System.AssertEquals(false ,closeCAPAResult.isSuccess(),
                 'Expected CAPA close to be unsuccessful '+capa.CAPA);




        }
    }

    /**
    *   Setup: Create a CAPA with Effectiveness Review Required (C1) and Status Open
    *   Action: Void CAPA 
    *   Expected: Close CAPA should be successful
    *
    * @author Sagar Shrestha
    * @date 2015/09/15
    * 
    */

    public static testmethod void GivenCAPAOpen_VoidSuccessful(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            //Arrange
            //Save capa and make all responses not required and initiate the capa
            SQX_Test_CAPA_Utility capa= new SQX_Test_CAPA_Utility();
            capa.finding.setRequired(false, false, false, false, false);//response not required, containment not reqd, investigation not required, ca not required, pa not required
            capa.CAPA.Needs_Effectiveness_Review__c = true;
            capa.CAPA.Status__c= SQX_CAPA.STATUS_OPEN;
            capa.save();

            //Act
            //Void the CAPA
            capa.CAPA.Status__c= SQX_CAPA.STATUS_CLOSED;
            capa.CAPA.Resolution__c = SQX_CAPA.RESOLUTION_VOID;
            capa.CAPA.Closure_Comment__c = 'CAPA Voided';
            capa.CAPA.Rating__c = 3;

            Database.SaveResult closeCAPAResult = Database.Update(capa.CAPA,false);

            //Assert : Void CAPA Should be successful
            System.AssertEquals(true ,closeCAPAResult.isSuccess(),
                 'Expected Void CAPA to be successful '+capa.CAPA);




        }
    }
}