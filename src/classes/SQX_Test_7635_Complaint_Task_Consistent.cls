/**
 * This test class is for complaint task to have appropriate status 
 */
@isTest
public class SQX_Test_7635_Complaint_Task_Consistent {
    
    public static Database.SaveResult result;
    
    @testSetup
    public static void commonSetup() {
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
    }
    
    /**
     * GIVEN : A complaint in draft status with associated complaint task
     * WHEN  : Complaint is opened
     * THEN  : Associated complaint task also opened and corresponding Salesforce task created
     */
    public static testmethod void givenComplaintInDraftStatusWithAssociatedComplaintTask_WhenComplaintOpen_ThenAssociatedComplaintTaskAlsoInOpenStatus(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task, false);
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            
            // ACT : Open Complaint
            complaint.initiate();
            
            complaintTask = [SELECT Id, Start_Date__c, Status__c FROM SQX_Complaint_Task__c WHERE Id =: complaintTask.Id];
           	Task sfTask = [SELECT Id, Status, ActivityDate FROM Task WHERE Child_What_Id__c =: complaintTask.Id];
            
            // ASSERT : Associated complaint task is in open status and related sf task is created with not started status
			System.assertequals(SQX_Complaint_Task.STATUS_OPEN, complaintTask.Status__c, 'Status of complaint task should be ' + SQX_Complaint_Task.STATUS_OPEN + ' but was ' + complaintTask.Status__c);
            System.assertNotEquals(null, sfTask, 'New Salesforce Task should be inserted.');
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, sfTask.Status, 'Status of Salesforce Task should be ' + SQX_Task.STATUS_NOT_STARTED + ' but was ' + sfTask.Status);
            System.assertEquals(dueDate, sfTask.ActivityDate, 'Due date of Salesforce Task should be ' + dueDate + ' but was ' + sfTask.ActivityDate);
        }
	}
	
    /**
     * GIVEN : A complaint task with allowed days
     * WHEN  : Complaint task is opened
     * THEN  : Associated Salesforce task should be created with due date equals sum of today date and allowed days
     */
    public static testmethod void givenComplaintTaskWithAllowedDays_WhenComplaintTaskIsOpened_ThenAssociatedSalesforceTaskShouldBeCreatedWithAppropriateDueDate() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task, false);
            
            Integer allowedDays = 5;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.SQX_User__c = standardUser.Id;
            complaintTask.Allowed_Days__c = allowedDays;
            
            insert complaintTask;
            
            // ACT : Open complaint
            complaint.initiate();
            
            Task sfTask = [SELECT Id, Status, ActivityDate FROM Task WHERE Child_What_Id__c =: complaintTask.Id];
            
            // ASSERT : Appropriate due date should be set in new created salesforce task
            System.assertNotEquals(null, sfTask, 'New Salesforce Task should be inserted.');
            System.assertEquals(Date.today() + allowedDays, sfTask.ActivityDate, 'Activity date of Salesforce task shoud be sum of today date and allowed days of complaint task but was ' + sfTask.ActivityDate);
        }
    }
    
    /**
     * GIVEN : Not completed Salesforce Task which is associated to complaint task
     * WHEN  : Salesforce Task is completed
     * THEN  : Status of parent complaint task should be completed
     */
    public static testmethod void givenNotCompletedSFTaskAssociatedToComplaintTask_WhenSFTaskIsCompleted_ThenParentComplaintTaskIsCompleted() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = standardUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TYPE_SAMPLE_REQUEST);
            
            result = Database.insert(task, false);
            
            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();
            
           	Task sfTask = [SELECT Id, Status, ActivityDate FROM Task WHERE WhatId =: complaint.complaint.Id];
            
            // ACT : Complete Salesforce Task
            sfTask.Status = SQX_Task.STATUS_COMPLETED;
            update sfTask;
            
            SQX_Complaint_Task__c complaintTask = [SELECT Id, Status__c, Completion_Date__c, Result__c FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            
            // ASSERT : Complaint Task should be completed with completion date equals to today
            System.assertequals(SQX_Complaint_Task.STATUS_COMPLETE, complaintTask.Status__c, 'Complaint Task should be completed but was ' + complaintTask.Status__c);
            System.assertequals(Date.today(), complaintTask.Completion_Date__c, 'Completion date should be ' + Date.Today() + ' but was ' + complaintTask.Completion_Date__c);
            System.assertEquals(SQX_Steps_Trigger_Handler.RESULT_GO, complaintTask.Result__c, 'Result of Complaint Poilcy should be : ' + SQX_Steps_Trigger_Handler.RESULT_GO + ' but was : '+  complaintTask.Result__c);
            
        }
    }
	
    /**
     * GIVEN : Complaint with open status
     * WHEN  : Complaint task is inserted associated to open complaint
     * THEN  : Status of newly inserted complaint task should be open
     */
    public static testmethod void givenOpenComplaint_WhenComplaintTaskInserted_ThenComplaintTaskShouldBeOpen() {
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            // ARRANGE : Create Complaint in open status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            // ARRANGE : Create CQ task
            SQX_Task__c task1 = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task1, false);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            Date dueDate = Date.today() + 2;
            
            // ACT : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask1 = new SQX_Complaint_Task__c();
            complaintTask1.SQX_Task__c = task1.Id;
            complaintTask1.Name = 'Task Test';
            complaintTask1.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask1.Due_Date__c = dueDate;
            complaintTask1.SQX_User__c = standardUser.Id;
            
            insert complaintTask1;
            
            complaint.initiate();
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task2 = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task2, false);
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // ACT : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask2 = new SQX_Complaint_Task__c();
            complaintTask2.SQX_Task__c = task2.Id;
            complaintTask2.Name = 'Task Test';
            complaintTask2.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask2.Due_Date__c = dueDate;
            complaintTask2.SQX_User__c = standardUser.Id;
            
            insert complaintTask2;
            
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
           // system.assert(false, [SELECT Id, compliancequest__Current_Step__c,(SELECT Id, compliancequest__Result__c,compliancequest__Step__c FROM compliancequest__SQX_Complaint_Tasks__r WHERE compliancequest__Applicable__c = TRUE AND compliancequest__Archived__c = FALSE AND (compliancequest__Status__c != 'Complete' OR compliancequest__Result__c != 'Go') ORDER BY compliancequest__Step__c ASC,compliancequest__Result__c DESC LIMIT 1) FROM compliancequest__SQX_Complaint__c WHERE Id =: complaint.complaint.Id]);
            
            complaintTask2 = [SELECT Id, Result__c, Step__c, Applicable__c, Archived__c, Start_Date__c, Status__c FROM SQX_Complaint_Task__c WHERE Id =: complaintTask2.Id];
            
            // ASSERT : Status of newly created complaintTask should be open with start date equals today
            System.assertEquals(SQX_Complaint_Task.STATUS_OPEN, complaintTask2.Status__c, 'Status of complaint task should be ' + SQX_Complaint_Task.STATUS_OPEN + ' but was ' + complaintTask2.Status__c);
            System.assertEquals(Date.today(), complaintTask2.Start_Date__c, 'Start date should be ' + Date.today() + ' but was ' + complaintTask2.Start_Date__c);
            
        }
    }

    /**
     * GIVEN : Complaint task with related Salesforce task
     * WHEN  : Complaint task is completed
     * THEN  : Related Salesforce task is also completed
     */
    public static testmethod void givenComplaintTaskWithRelatedSFTask_WhenComplaintTaskIsCompleted_ThenRelatedSFTaskIsCompleted() {

        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){

            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task, false);
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            
            complaint.initiate();

            complaintTask = [SELECT Id, Status__c, Completion_Date__c FROM SQX_Complaint_Task__c WHERE Id =: complaintTask.Id];

            // ACT : Complete Complaint Task
            complaintTask.Status__c = SQX_Complaint_Task.STATUS_COMPLETE;
            complaintTask.Completion_Date__c = Date.today();

            update complaintTask;
            
           	Task sfTask = [SELECT Id, Status FROM Task WHERE Child_What_Id__c =: complaintTask.Id];
            
            // ASSERT : Salesforce Task should be completed
            System.assertequals(SQX_Task.STATUS_COMPLETED, sfTask.Status, 'Salesforce Task should be completed but was ' + sfTask.Status);

        }

    }
    
    /**
     * GIVEN : A complaint in draft status with associated complaint task
     * WHEN  : Complaint is opened after submitting
     * THEN  : Associated complaint task also opened and corresponding Salesforce task created
     */
    public static testmethod void givenComplaintInDraftStatusWithAssociatedComplaintTask_WhenComplaintOpenAfterSubmitting_ThenAssociatedComplaintTaskAlsoInOpenStatus(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');
        System.runAs(standardUser){
            
            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            
            // ARRANGE : Create CQ task
            SQX_Task__c task = new SQX_Task__c(Allowed_Days__c = 30, 
                                               SQX_User__c = adminUser.Id, 
                                               Description__c = 'Test_DESP', 
                                               Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                               Step__c = 1,
                                               Name = 'NSI_1',
                                               Task_Type__c = SQX_Task.TASK_TYPE_TASK);
            
            result = Database.insert(task, false);
            
            Date dueDate = Date.today() + 2;
            
            // ARRANGE : Create associated complaint tasks
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.SQX_Task__c = task.Id;
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = dueDate;
            complaintTask.SQX_User__c = standardUser.Id;
            
            insert complaintTask;
            
            // ACT : Open Complaint
            complaint.submit();
            complaint.takeOwnershipAndInitiate();
            
            complaintTask = [SELECT Id, Start_Date__c, Status__c FROM SQX_Complaint_Task__c WHERE Id =: complaintTask.Id];
           	Task sfTask = [SELECT Id, Status, ActivityDate FROM Task WHERE Child_What_Id__c =: complaintTask.Id];
            
            // ASSERT : Associated complaint task is in open status and related sf task is created with not started status
			System.assertequals(SQX_Complaint_Task.STATUS_OPEN, complaintTask.Status__c, 'Status of complaint task should be ' + SQX_Complaint_Task.STATUS_OPEN + ' but was ' + complaintTask.Status__c);
            System.assertNotEquals(null, sfTask, 'New Salesforce Task should be inserted.');
            System.assertEquals(SQX_Task.STATUS_NOT_STARTED, sfTask.Status, 'Status of Salesforce Task should be ' + SQX_Task.STATUS_NOT_STARTED + ' but was ' + sfTask.Status);
            System.assertEquals(dueDate, sfTask.ActivityDate, 'Due date of Salesforce Task should be ' + dueDate + ' but was ' + sfTask.ActivityDate);
        }
    }
    
    /**
     * GIVEN : A complaint with script execution step
     * WHEN : Script is executed
     * THEN : Script Execution task is completed
     * 
     * WHEN : Complaint is closed
     * THEN : script execution record cannot be deleted
     */
    public static testmethod void givenComplaintWithScript_WhenCompleted_TaskIsCompletedAndCannotBeDeleted(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');

        SQX_Task__c cqTask = null;
        SQX_Defect_Code__c complaintConclusion = null;
        
        System.runAs(adminUser){
            
            cqTask = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = adminUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'NSI_1',
                                    Task_Type__c = SQX_Task.TYPE_SCRIPT);
            
            result = Database.insert(cqTask, false);

            complaintConclusion = new SQX_Defect_Code__c(Name = 'Test Defect Code 5', 
                                                        Active__c = true,
                                                        Defect_Category__C = 'Test_Category5',
                                                        Description__c = 'Test Description',
                                                        Type__c = 'Complaint Conclusion');
            insert complaintConclusion;
        }
        System.runAs(standardUser){
            
            // ARRANGE : Create Complaint in draft status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();
            
            // ACT : Script is executed
            SQX_Script_Execution__c scriptExecution = new SQX_Script_Execution__c();
            scriptExecution.Name = 'Sample Execution';
            scriptExecution.Responses_Saved__c = true;
            scriptExecution.SQX_Complaint__c = complaint.complaint.Id;
            scriptExecution.SQX_CQ_Task__c = cqTask.Id;
            insert scriptExecution; 

            SQX_Complaint_Task__c complaintTask = [SELECT Id, Start_Date__c, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c = :complaint.complaint.Id];
           	Task sfTask = [SELECT Id, Status, ActivityDate FROM Task WHERE Child_What_Id__c =: complaintTask.Id];

            // ASSERT : Complaint Task and sf task is completed
            System.assertEquals(SQX_Complaint_Task.STATUS_COMPLETE, complaintTask.Status__c);
            System.assertEquals(SQX_Task.STATUS_COMPLETED, sfTask.Status);

            // ACT : Complaint is closed
            complaint.complaint.SQX_Conclusion_Code__c = complaintConclusion.Id;
            complaint.complaint.Resolution__c = SQX_Complaint.RESOLUTION_COMPLAINT;
            complaint.close();
            System.assertEquals(SQX_Complaint.STATUS_CLOSED, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id].Status__c);

            // ACT : Script Execution is delted
            Database.DeleteResult dResult = Database.delete(scriptExecution, false);

            // ASSERT : Error is thrown
            System.assert(!dResult.isSuccess(), 'Delete is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(dResult.getErrors(), Label.SQX_Err_Msg_Cannot_Modify_Record_Once_Locked), result.getErrors());
            
        }
	}
    
    /**
     * GIVEN : A complaint in is created in open status
     * WHEN  : Name and Allowed Date is changed
     * THEN  : Error is thrown
     */
    public static testmethod void givenComplaintWithOpenComplaintTask_WhenComplaintTaskIsChanged_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');

        SQX_Task__c task = null;
        System.runAs(adminUser){
            
            // ARRANGE : Create CQ task
            task = new SQX_Task__c(Allowed_Days__c = 30, 
                                    SQX_User__c = adminUser.Id, 
                                    Description__c = 'Test_DESP', 
                                    Record_Type__c = SQX_Task.RTYPE_COMPLAINT,
                                    Step__c = 1,
                                    Name = 'Complaint Task',
                                    Task_Type__c = SQX_Task.TYPE_SAMPLE_REQUEST);
            
            result = Database.insert(task, false);
        }
        System.runAs(standardUser){
            
            // ARRANGE : Complaint is created in open status
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();
            
            SQX_Complaint_Task__c complaintTask = [SELECT Id, Name, Allowed_Days__c, Due_Date__c, Start_Date__c, Status__c FROM SQX_Complaint_Task__c WHERE SQX_Complaint__c =: complaint.complaint.Id];
            
            // ACT : Name is updated
            SQX_Complaint_Task__c updatedComplaintTask = complaintTask.clone(true);
            updatedComplaintTask.Name = 'Updated Name';
            Database.SaveResult result = Database.update(updatedComplaintTask, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Complaint Task in open status cannot be edited.'), result.getErrors());
            
            // ACT : Allowed Days is changed
            updatedComplaintTask = complaintTask.clone(true);
            updatedComplaintTask.Allowed_Days__c = 3;
            result = Database.update(updatedComplaintTask, false);

            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Complaint Task in open status cannot be edited.'), result.getErrors());
            
            // ACT : Comment is changed
            updatedComplaintTask = complaintTask.clone(true);
            updatedComplaintTask.Comment__c = 'Comment';
            result = Database.update(updatedComplaintTask, false);

            // ASSERT : Save is successful
            System.assert(result.isSuccess(), 'Save is not successful' + result.getErrors());
        }
	}
    
    /**
     * GIVEN : A complaint in completed
     * WHEN  : Complaint Task is added
     * THEN  : Error is thrown
     */
    public static testmethod void givenCompletedComplaint_WhenComplaintTaskIsAdded_ErrorIsThrown(){
        
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        User standardUser = allUsers.get('standardUser');

        System.runAs(standardUser){
            
            // ARRANGE : Complaint is created and completed
        	SQX_Test_Complaint complaint = new SQX_Test_Complaint(adminUser).save();
            complaint.initiate();

            System.assertEquals(SQX_Complaint.STATUS_COMPLETE, [SELECT Id, Status__c FROM SQX_Complaint__c WHERE Id = :complaint.complaint.Id].Status__c);

            // ACT : Complaint Task is added
            SQX_Complaint_Task__c complaintTask = new SQX_Complaint_Task__c();
            complaintTask.Name = 'Task Test';
            complaintTask.SQX_Complaint__c = complaint.complaint.Id;
            complaintTask.Due_Date__c = Date.today().addDays(10);
            complaintTask.SQX_User__c = standardUser.Id;
            Database.SaveResult result = Database.insert( complaintTask, false);
            
            // ASSERT : Error is thrown
            System.assert(!result.isSuccess(), 'Save is successful');
            System.assert(SQX_Utilities.checkErrorMessageContainingTexts(result.getErrors(), 'Record status or Step status does not support the action performed.'), result.getErrors());

        }
	}
    
}