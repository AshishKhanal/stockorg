/**
* Unit tests for Document Training with complete status
*
* @author Dibya Shrestha
* @date 2015/09/07
* 
*/
@IsTest
public class SQX_Test_1582_DocumentTraining_Complete {
    // tests removed for obsolete object SQX_Document_Training__c
    // TODO: remove this file while doing cleanup
}