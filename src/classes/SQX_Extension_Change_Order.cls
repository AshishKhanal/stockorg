/**
* This the extension used by change orders view/edit overriden page. It contains various
* properties and methods that are required for the page. 
*/
public with sharing class SQX_Extension_Change_Order extends SQX_Extension_UI {
	
	/**
	* A static list of supported next actions
	*/
	public static 	String 	NXTACT_SAVE = 'save',
							NXTACT_SUBMIT = 'submit',
                            NXTACT_TAKE_OWNERSHIP = 'takeownership',
							NXTACT_TAKE_OWNERSHIP_AND_INITIATE = 'takeownershipandinitiate',
							NXTACT_INITIATE = 'initiate',
							NXTACT_VOID = 'void',
							NXTACT_REOPEN = 'reopen',
                            NXTACT_SUBMIT_FOR_APPROVAL = 'submitforapproval',
                            NXTACT_APPROVE_REJECT_REQUEST = 'approverejectrequest',
                            NXTACT_APPROVE_APPROVAL = 'approveapproval',
                            NXTACT_REJECT_APPROVAL = 'rejectapproval',
                            NXTACT_SUBMIT_FOR_IMPLEMENTATION_APPROVAL = 'submitforimplementationapproval',
                            NXTACT_IMPLEMENTATION_APPROVAL_REQUEST = 'implementationapprovalrequest',
                            NXTACT_APPROVE_IMPLEMENTATION_APPROVAL = 'approveimplementationapproval',
                            NXTACT_REJECT_IMPLEMENTATION_APPROVAL = 'rejectimplementationapproval',
                            NXTACT_REPLAN = 'replan',
                            NXTACT_RELEASE = 'release';
	/**
	* Constructor for the apex page that inits the static esig policies, purpose of sig map, security matrix and other tasks
	*/
	public SQX_Extension_Change_Order(ApexPages.StandardController controller){
        super(controller, new List<String> {'Status__c'});
        
        SQX_Change_Order__c change = (SQX_Change_Order__c)controller.getRecord();
        
        
        //we do not need Change esig validation if Complaint is new or is in draft state.
        Boolean isRecInDraftOrNew = !(change.Id != null && change.Status__c != SQX_Change_Order.STATUS_DRAFT);
        addStaticPolicy(NXTACT_SAVE, getPolicySaveRecordAfterDraft(isRecInDraftOrNew,SQX.ChangeOrder));
        
        Map<String, SQX_CQ_Esig_Policies__c> allPolicies = getEsigPolicy();
        SQX_CQ_Esig_Policies__c approveRejectPolicy = allPolicies.get('approverejectrequest');
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        Boolean esigApproval, esigComment = false;
        if (approveRejectPolicy != null) {
            esigApproval = approveRejectPolicy.Required__c;
            esigComment = approveRejectPolicy.Comment__c;
        } else {
            esigApproval = instance.Enabled__c;
            esigComment = false;
        }
        addStaticPolicy(NXTACT_APPROVE_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
        addStaticPolicy(NXTACT_REJECT_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
        
        approveRejectPolicy = allPolicies.get(NXTACT_IMPLEMENTATION_APPROVAL_REQUEST);
        esigApproval = esigComment = false;
        if (approveRejectPolicy != null) {
            esigApproval = approveRejectPolicy.Required__c;
            esigComment = approveRejectPolicy.Comment__c;
        } else {
            esigApproval = instance.Enabled__c;
            esigComment = false;
        }
        addStaticPolicy(NXTACT_APPROVE_IMPLEMENTATION_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
        addStaticPolicy(NXTACT_REJECT_IMPLEMENTATION_APPROVAL, new SQX_CQ_Esig_Policies__c(Required__c = esigApproval, Comment__c = esigComment));
        
        this.purposeOfSigMap.putAll(new Map<String, String>{
                NXTACT_SAVE => Label.SQX_PS_CO_Saving_the_record,
                NXTACT_SUBMIT => Label.SQX_PS_CO_Submitting_the_record,
                NXTACT_TAKE_OWNERSHIP => Label.SQX_PS_CO_Taking_Ownership,
                NXTACT_TAKE_OWNERSHIP_AND_INITIATE => Label.SQX_PS_CO_Taking_Ownership_and_Initiating_the_record,
                NXTACT_INITIATE => Label.SQX_PS_CO_Inititating_the_record,
                NXTACT_VOID => Label.SQX_PS_CO_Voiding_the_record,
                NXTACT_REOPEN => Label.SQX_PS_CO_Reopening_the_record,
                NXTACT_SUBMIT_FOR_APPROVAL => Label.SQX_PS_CO_Submitting_for_Approval,
                NXTACT_APPROVE_REJECT_REQUEST => Label.SQX_PS_CO_Approving_Rejecting_the_record,
                NXTACT_APPROVE_APPROVAL => Label.SQX_PS_CO_Approving_the_record,
                NXTACT_REJECT_APPROVAL => Label.SQX_PS_CO_Rejecting_the_record,
                NXTACT_SUBMIT_FOR_IMPLEMENTATION_APPROVAL => Label.SQX_PS_CO_Submitting_for_Implementation_Approval,
                NXTACT_IMPLEMENTATION_APPROVAL_REQUEST => Label.SQX_PS_CO_Approving_Rejecting_Implementation_Approval,
                NXTACT_APPROVE_IMPLEMENTATION_APPROVAL => Label.SQX_PS_CO_Approving_Implementation_Approval,
                NXTACT_REJECT_IMPLEMENTATION_APPROVAL => Label.SQX_PS_CO_Rejecting_Implementation_Approval,
                NXTACT_REPLAN => Label.SQX_PS_CO_Adjusting_Plan_of_the_record,
                NXTACT_RELEASE => Label.SQX_PS_CO_Releasing_the_record
            });
        
        this.addActionSaveAfterDraft();
        this.fetchRecordTypesOf.add(SQX.Implementation); // allows the page to access the specific record type using RecordTypes[<ObjectType>][<RecordType>] property
        this.fetchRecordTypesOf.add(SQX.Impact);
    }

    protected override String getApplicableApproveRejectPurpose() {
        SQX_Change_Order__c change = (SQX_Change_Order__c)mainRecord;
        return change.Record_Stage__c == SQX_Change_Order.STAGE_PLAN ?  getPurposeOfSig().get(NXTACT_APPROVE_REJECT_REQUEST) : getPurposeOfSig().get(NXTACT_IMPLEMENTATION_APPROVAL_REQUEST) ;
    }

    protected override SQX_CQ_Esig_Policies__c getApprovalPolicy() {
        SQX_Change_Order__c change = (SQX_Change_Order__c)mainRecord;
        return change.Record_Stage__c == SQX_Change_Order.STAGE_PLAN ?  getEsigPolicy().get(NXTACT_APPROVE_REJECT_REQUEST) : getEsigPolicy().get(NXTACT_IMPLEMENTATION_APPROVAL_REQUEST);
    }
    
    /**
    * Add default policies for the various next actions
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {
        SQX_CQ_Electronic_Signature__c instance = SQX_CQ_Electronic_Signature__c.getInstance();
        
        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>();
        
        defaultEsigPolicies.put(NXTACT_VOID, this.POLICY_COMMENT_REQUIRED);
        defaultEsigPolicies.put(NXTACT_REOPEN, this.POLICY_COMMENT_REQUIRED);
        
        return defaultEsigPolicies;
    }
	
	/**
	* Defines the list of entities whose data are to be exported along with the main record type
	*/
	public override SObjectDataLoader.SerializeConfig getDataLoaderConfig(){
		return super.getDataLoaderConfig()
                    .followChild(SQX_Change_Order_Approval__c.SQX_Change_Order__c)
                    .followChild(SQX_Change_Order_Record_Activity__c.SQX_Change_Order__c)
                    .followChild(SQX_Implementation__c.SQX_Change_Order__c)
                    .followChild(SQX_Impact__c.SQX_Change_Order__c)
                    .followNoteOf(SQX_Change_Order__c.getSObjectType())
                    .followAttachmentOf(SQX_Change_Order__c.getSObjectType())
                    .followAttachmentOf(SQX_Implementation__c.getSObjectType())
                    .followApprovalOf(SQX_Change_Order__c.getSObjectType())

                    .getFieldsFor(SQX_Part__c.getSObjectType(), new List<SObjectField> {SQX_Part__c.Part_Number_and_Name__c, SQX_Part__c.Name, SQX_Part__c.Part_Number__c});

	}
	

	/**
	* Performs the next action after the save/default action has taken place
	*/
	public override void processNextAction(Map<String, String> params){
        SQX_Change_Order__c change = (SQX_Change_Order__c) mainRecord;
        
        //declaring a list at the top helps us pass the change to extension without newing every time
        List<SQX_Change_Order__c> changes = new List<SQX_Change_Order__c> { change }; 
        
        if(NXTACT_SUBMIT.equalsIgnoreCase(nextAction)){
            SQX_Change_Order.submitChanges(changes);
        }
        else if(NXTACT_TAKE_OWNERSHIP.equalsIgnoreCase(nextAction)){
            SQX_Change_Order.takeOwnership(changes);
        }
        else if(NXTACT_TAKE_OWNERSHIP_AND_INITIATE.equalsIgnoreCase(nextAction)){
        	SQX_Change_Order.takeOwnershipAndInitiate(changes);
        }
        else if(NXTACT_INITIATE.equalsIgnoreCase(nextAction)){
        	SQX_Change_Order.initiate(changes);
        }
        else if(NXTACT_VOID.equalsIgnoreCase(nextAction)){
        	SQX_Change_Order.voidRecords(changes);
        }
        else if(NXTACT_REOPEN.equalsIgnoreCase(nextAction)){
        	SQX_Change_Order.reopen(changes);
        }
        else if(NXTACT_SUBMIT_FOR_APPROVAL.equalsIgnoreCase(nextAction)
                || NXTACT_SUBMIT_FOR_IMPLEMENTATION_APPROVAL.equalsIgnoreCase(nextAction)) {
            SQX_Approval_Util.submitForApproval(changes, (String)params.get(SQX_Extension_UI.PARAM_COMMENT_KEY));
        }
        else if (NXTACT_APPROVE_APPROVAL.equalsIgnoreCase(nextAction)
                || NXTACT_REJECT_APPROVAL.equalsIgnoreCase(nextAction)
                || NXTACT_APPROVE_IMPLEMENTATION_APPROVAL.equalsIgnoreCase(nextAction)
                || NXTACT_REJECT_IMPLEMENTATION_APPROVAL.equalsIgnoreCase(nextAction)) {
            
            boolean isApprove = NXTACT_APPROVE_APPROVAL.equalsIgnoreCase(nextAction) || NXTACT_APPROVE_IMPLEMENTATION_APPROVAL.equalsIgnoreCase(nextAction);
            Id workItemId = params.get('workItemId');
            ProcessInstanceWorkitem item = SQX_Approval_Util.getProcessInstanceWorkitem(workItemId);
            if (item == null) {
                throw new SQX_ApplicationGenericException('Approval request item not available for approval.');
            } else {
                approveRecord(isApprove, change.Id, (String)params.get('comment'), item.OriginalActorId);
            }
        }
        else if(NXTACT_REPLAN.equalsIgnoreCase(nextAction)){
            // TODO: move the code to SQX_Change_Order
            // SQX_Change_Order.replan(changes);
            SQX_Change_Order__c changeOrderToUpdate = new SQX_Change_Order__c(Id = change.Id,
                                                                      Activity_Code__c=SQX_Change_Order.ACTIVITY_CODE_ADJUST_PLAN,
                                                                      Approval_Status__c = SQX_Change_Order.APPROVAL_STATUS_PLAN_REPLAN);
            
            Database.SaveResult[] results = new SQX_DB().continueOnError().op_update(new List<SObject> { changeOrderToUpdate }, new List<SObjectField> { SQX_Change_Order__c.Activity_Code__c,SQX_Change_Order__c.Approval_Status__c  });
            if(!results[0].isSuccess()) {
                throw new SQX_ApplicationGenericException(results[0].getErrors()[0].getMessage());
            }
        }
        else if(NXTACT_RELEASE.equalsIgnoreCase(nextAction)){
            // TODO: move the code to change order class

            if(change.Status__c != SQX_Change_Order.STATUS_COMPLETE && change.Record_Stage__c!=SQX_Change_Order.STAGE_COMPLETE  ){
                SQX_Change_Order__c changeOrderToUpdate = new SQX_Change_Order__c(Id = change.Id,Activity_Code__c =SQX_Change_Order.ACTIVITY_CODE_RELEASE);
                Database.SaveResult[] results = new SQX_DB().continueOnError().op_update(new List<SObject> { changeOrderToUpdate }, new List<SObjectField> { SQX_Change_Order__c.Activity_Code__c });
                if(!results[0].isSuccess()) {
                    throw new SQX_ApplicationGenericException(results[0].getErrors()[0].getMessage());
                }
            }
        }
    }
	
	
	/**
	* Default remote action for the extension to send any data that needs to be persisted or when certain next action is to be performed on the main record
	* @param changeId the Id of the change order record
	* @param changeSet the changes that are to be saved
	* @param objectsToDelete the list of ids that are to be removed from SF
	* @param params the list of extra params such as next action is to be used for processing
	* @param esigData the electronic signature data that must be submitted inorder to process the record
	*/
	@RemoteAction
    public static String processChangeSetWithAction(String changeId, String changeset, sObject[] objectsToDelete, Map<String, String> params, SQX_OauthEsignatureValidation esigData) {
        Boolean isNewRecord = !SQX_Upserter.isValidId(changeId);
        SQX_Change_Order__c  change;
        SQX_Extension_Change_Order changeExtension;

        if(isNewRecord){
            change = new SQX_Change_Order__c();
        }
        else{
            change = [SELECT 	Id, 
	                            Status__c, 
	                            Approval_Status__c
                        FROM SQX_Change_Order__c WHERE Id = : changeId];
        }

        changeExtension = new SQX_Extension_Change_Order(new ApexPages.StandardController(change));
        SQX_Upserter.SQX_Upserter_Config config =  new SQX_Upserter.SQX_Upserter_Config()
        														   .omit(SQX.ChangeOrder, SQX_Change_Order__c.Status__c)
        														   .omit(SQX.ChangeOrder, SQX_Change_Order__c.Approval_Status__c);
        
        return changeExtension.executeTransaction(changeId, changeset, objectsToDelete, params, esigData, config, null);
    }
    
    /**
    * remote action to get active users related to a job function for Change Order Approval object.
    * @param  jobFunctionId the Id of a job function
    * @param  sfObjectName  name of the object to be returned
    * @param  fields        list of fields to return
    * @param  pageNumber    page number to return
    * @param  filter        filter to apply when querying for records
    * @return               returns list of active SDFC users linked to active personnel for a job function. if jobFunctionId is null, it returns all active users
    */
    @RemoteAction
    public static List<SQX_Job_Function.SQX_JobFunctionUser_Wrapper> getActiveUsersForJobFunction(Id jobFunctionId, String sfObjectName, List<String> fields, Integer pageNumber, SQX_DynamicQuery.Filter filter){
        return SQX_Job_Function.getActiveUsers(jobFunctionId, filter);
    }
    
    /**
    * remote action to return list of valid controlled documents for the field
    * @param currentDocId the doc that is to be revised
    * @param typeOfAction the action type for the implementation
    * @param sfObjectName name of the object to be returned
    * @param fields the list of fields to return
    * @param pageNumber the page number to return
    * @param filter the filter to apply when querying for records
    * @return returns list of valid controlled documents
    */
    @RemoteAction
    public static List<SObject> getNewControlledDocs(Id currentDocId, String typeOfAction, String sfObjectName, List<String> fields, Integer pageNumber, SQX_DynamicQuery.Filter filter){
        SQX_DynamicQuery.Filter filterToPass = filter, documentNumberFilter, notSameDocFilter;
        
        if(typeOfAction == SQX_Implementation.AWARE_DOCUMENT_REVISE ){
            // If action is of revise type, we need to return list of all documents having the same document number as that of
            // the original document
            SQX_Controlled_Document__c doc = [SELECT Id, Document_Status__c, Document_Number__c
                                              FROM SQX_Controlled_Document__c
                                              WHERE Id = : currentDocId];
                
            filterToPass = new SQX_DynamicQuery.Filter();
            filterToPass.logic = SQX_DynamicQuery.FILTER_LOGIC_AND;
            
            // add a filter to get list of docs with matching document number
            documentNumberFilter = new SQX_DynamicQuery.Filter();
            documentNumberFilter.field = SQX.NsPrefix + 'Document_Number__c';
            documentNumberFilter.operator = SQX_DynamicQuery.FILTER_OPERATOR_EQUALS;
            documentNumberFilter.value = doc.Document_Number__c;
            filterToPass.filters.add(documentNumberFilter);
            
            // add another filter to get list of docs except the original doc
            notSameDocFilter = new SQX_DynamicQuery.Filter();
            notSameDocFilter.field = 'Id';
            notSameDocFilter.operator = SQX_DynamicQuery.FILTER_OPERATOR_NOT_EQUALS;
            notSameDocFilter.value = doc.Id;
            filterToPass.filters.add(notSameDocFilter);
                                  
            // add any other filter that has been passed.
            if(filter.field != null || (filter.filters != null && filter.filters.size() != 0)){
                filterToPass.filters.add(filter);
            }
        }
        
        return SQX_Extension_UI.getObjects(sfObjectName, fields, pageNumber, filterToPass);
        
    }
}