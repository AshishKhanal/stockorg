/**
* Unit tests for Equipment Object
* which includes Duplication check of Equipment Number;
*                Required fields check for Owner Type, Equipment Type, Equipment Status;
*                Conditional Required fields as Next Calibration Date and Calibration Interval, negative value check for Calibration Interval,Calibration Supplier;
*                Only Controlled Document record type are to be added;
*                Deleteing Equipment;
*
* @author Shailesh Maharjan
* @date 2015/09/10
* 
*/
@IsTest
public class SQX_Test_1450_Equipment {
    static Boolean runAllTests = true,
                   run_givenAddEquipment = false,
                   run_givenDeleteEquipment = false;
    
    private static Boolean checkErrorMessage(Database.SaveResult sr, String errmsg) {
        for (Database.Error err : sr.getErrors()) {
            if (err.getMessage().equals(errmsg)) {
                return true;
            }
        }
        return false;
    }
    
    /**
    * This test ensures addition of new Equipment to work as expected.
    */
    public testmethod static void givenAddEquipment(){
        if (!runAllTests && !run_givenAddEquipment){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
        //Creating required test user
        User testUser1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        
          
        System.runas(standardUser) {
        
        // add required account for supplier
            Account supplier = SQX_Test_Account_Factory.createAccount();
        //Duplication Check for Equipment Number
            // creating required Equipment
            SQX_Equipment__c Eqp001 = new SQX_Equipment__c(
                Name = '1A',
                Equipment_Description__c = 'Test',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            
            //Act
            Database.SaveResult result1 = Database.insert(Eqp001, false); 
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Equipment to be Created');

            // creating required Equipment
            SQX_Equipment__c Eqp002 = new SQX_Equipment__c(
                Name = '1a',
                Equipment_Description__c = 'Test1',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            
            //Act
            result1 = Database.insert(Eqp002, false); 
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment to be Created');

            System.assert(checkErrorMessage(result1, 'Equipment Number is already present in the System.'),
                'Expected error message to be "Equipment Number is already present in the System."');
       
            // Required field check
            // Arrange: creating required Equipment without Equipment Description
            SQX_Equipment__c Eqp01 = new SQX_Equipment__c(
                Name = '2',
                Equipment_Description__c = 'Test2',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Owner_Type__c = null,
                Requires_Periodic_Calibration__c = false
            );
            //Act
            result1 = Database.insert(Eqp01, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not to be Created');

            System.assert(checkErrorMessage(result1, 'Owner Type is required.'),
                'Expected error message to be "Owner Type is required."');
            
             // Arrange: creating required Equipment without Equipment Type
            SQX_Equipment__c Eqp02 = new SQX_Equipment__c(
                Name = '3',
                Equipment_Description__c = 'Test3',
                Equipment_Type__c = null,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Owner_Type__c = SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Requires_Periodic_Calibration__c = false
            );
            //Act
            result1 = Database.insert(Eqp02, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not to be Created');
            
            System.assert(checkErrorMessage(result1, 'Equipment Type is required.'),
                'Expected error message to be "Equipment Type is required."' + result1.geterrors());
            
            // Arrange: creating required Equipment without Equipment Status
            SQX_Equipment__c Eqp03 = new SQX_Equipment__c(
                Name = '4',
                Equipment_Description__c = 'Test4',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Equipment_Status__c = null,
                Owner_Type__c = SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Requires_Periodic_Calibration__c = false
            );
            //Act
            result1 = Database.insert(Eqp03, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not to be Created');
            
            System.assert(checkErrorMessage(result1, 'Equipment Status is required.'),
                'Expected error message to be "Equipment Status is required."' + result1.geterrors());
            


            // Arrange: creating required Equipment when Requires Periodic Calibration is not selected
            SQX_Equipment__c Eqp2 = new SQX_Equipment__c(
                Name = '5',
                Equipment_Description__c = 'Test5',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            //Act
            result1 = Database.insert(Eqp2, false);
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Equipment to be Created');

            //Arrange: creating required Equipment when Requires Periodic Calibration is selected
            SQX_Equipment__c Eqp3 = new SQX_Equipment__c(
                Name = '6',
                Equipment_Description__c = 'Test6',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true
            );
            //Act
            result1 = Database.insert(Eqp3, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not created without required fields when Requires Periodic Calibration is checked');
            
            // check expected error message
            String errorMessage = 'Next Calibration Date and Calibration Interval values are required when Requires Periodic Calibration is checked.';
            System.assert(checkErrorMessage(result1, errorMessage),
                'Expected error message to be "' + errorMessage + '".');

            //Act: Adding Next Calibration Date and Calibration Interval with negative value
            Eqp3.Next_Calibration_Date__c = Date.today();
            Eqp3.Calibration_Interval__c = -1; 
            result1 = Database.insert(Eqp3, false);
            
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not created when Calibration Interval is negative');

            // check expected error message
            String errorMessage1 = 'Calibration Interval should not be negative';
            System.assert(checkErrorMessage(result1, errorMessage1),
                'Expected error message to be "' + errorMessage1 + '".');

            //Act: Adding Next Calibration Date and Calibration Interval fields
            Eqp3.Next_Calibration_Date__c = Date.today();
            Eqp3.Calibration_Interval__c = 11; 
            result1 = Database.insert(Eqp3, false);
            
            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Equipment to be saved.');

            //Arrange : Creating Equipment while Calibration Source is External
            SQX_Equipment__c Eqp4 = new SQX_Equipment__c(
                Name = '7',
                Equipment_Description__c = 'Test7',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Next_Calibration_Date__c = Date.today(),
                Calibration_Interval__c = 1,
                Calibration_Source__c = SQX_Equipment.SOURCE_EXTERNAL
            );
            //Act
            result1 = Database.insert(Eqp4, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not created without required fields when Calibration Source is External');
            
            // check expected error message
            errorMessage = 'Calibration Supplier is required for external Calibration Source.';
            System.assert(checkErrorMessage(result1, errorMessage),
                'Expected error message to be "' + errorMessage + '".');

            //Act: Adding Calibration Supplier
            Eqp4.SQX_Calibration_Supplier__c= supplier.Id; 
            result1 = Database.insert(Eqp4, false);

            //Assert
            System.assert(result1.isSuccess() == true,
                'Expected Equipment to be saved.');

            // Arrange: creating required controlled doc
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document().save();
            // Arrange: creating required Template doc
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document();
            cDoc2.doc.RecordTypeId = SQX_Controlled_Document.getTemplateDocTypeId();
            cDoc2.save();

             //Arrange : Creating Equipment with Applicable Procedure/Rev is Controlled doc/Template doc
            SQX_Equipment__c Eqp5 = new SQX_Equipment__c(
                Name = '8',
                Equipment_Description__c = 'Test8',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = true,
                Next_Calibration_Date__c = Date.today(),
                Calibration_Interval__c = 1,
                Calibration_Source__c = SQX_Equipment.SOURCE_INTERNAL,
                SQX_Applicable_Procedure_Rev__c = cDoc2.doc.Id
            );
            //Act
            result1 = Database.insert(Eqp5, false);
            //Assert
            System.assert(result1.isSuccess() == false,
                'Expected Equipment not created, only Controlled Document record type can only be used.');

            // check expected error message
            errorMessage = 'Document with Controlled Document record type and that are not obsolete can only be used for Applicable Procedure/Rev.';
            System.assert(checkErrorMessage(result1, errorMessage),
                'Expected error message to be "' + errorMessage + '".');

            // add obsolete controlled document
            SQX_Test_Controlled_Document cDoc3 = new SQX_Test_Controlled_Document().save();
            cDoc3.setStatus(SQX_Controlled_Document.STATUS_OBSOLETE).save();

             //Act: link obsolete controlled document
            Eqp5.SQX_Applicable_Procedure_Rev__c = cDoc3.doc.Id; 
            result1 = Database.insert(Eqp5, false);

            System.assert(result1.isSuccess() == false,
                'Expected equipment not created when obsolete controlled document is linked.');

            // check expected error message
            System.assert(checkErrorMessage(result1, errorMessage),
                'Expected error message to be "' + errorMessage + '".');

             //Act: Adding Applicable Procedure/Rev 
            Eqp5.SQX_Applicable_Procedure_Rev__c = cDoc1.doc.Id; 
            result1 = Database.insert(Eqp5, false);

            //Assert : Equipment Created Successfully 
            System.assert(result1.isSuccess() == true,
                'Expected Equipment to be saved.');
        }
    }

    /**
    * This test ensures deletion of a Equipment to work as expected.
    */
    public testmethod static void givenDeleteEquipment(){
        if (!runAllTests && !run_givenDeleteEquipment){
            return;
        }

        UserRole mockRole = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);

        System.runas(standardUser) {
           
            // creating required Equipment
            SQX_Equipment__c Eqp1 = new SQX_Equipment__c(
                Name = '10',
                Equipment_Description__c = 'Test10',
                Equipment_Type__c = SQX_Equipment.TYPE_ANALOG_GAUGE,
                Owner_Type__c =  SQX_Equipment.OWNER_TYPE_COMPANY_OWNED,
                Equipment_Status__c = SQX_Equipment.STATUS_NEW,
                Requires_Periodic_Calibration__c = false
            );
            
            //Act
            Database.SaveResult saveResult1 = Database.insert(Eqp1, false);
            
            Database.DeleteResult delResult1 = Database.delete(Eqp1, false);
            
            //Assert: ensuring Equipment to be deleted
            System.assert(delResult1.isSuccess() == true,
                'Expected  Equipment to be deleted.');
        }
    }
    
}