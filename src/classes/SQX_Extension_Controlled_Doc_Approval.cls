/**
* Extension method to perform approve or reject action on approval request of controlled document
*/
global with sharing class SQX_Extension_Controlled_Doc_Approval extends SQX_Controller_Sign_Off{

    private ApexPages.StandardController stdController;

    private SQX_Controlled_Document__c doc { get; set; }
    private ContentDocument activeCD, primaryCD, secondaryCD;

    global ProcessInstanceWorkitem ApprovalRequestItem { get; set; }
    global UserRecordAccess ApprovalRequestItemPermission { get; set; }

    public static final String  CODE_OF_SIG_APPROVAL = 'approval';

    private Id delegatedApproverId;

    /*
    * Add default policies
    */
    protected override Map<String, SQX_CQ_Esig_Policies__c> getDefaultEsigPolicies() {

        Map<String, SQX_CQ_Esig_Policies__c> defaultEsigPolicies = new Map<String, SQX_CQ_Esig_Policies__c>
        {
            CODE_OF_SIG_APPROVAL => this.POLICY_NO_COMMENT_REQUIRED
        };
        return defaultEsigPolicies;
    }

    /*
    * set purpose of sig for Sign Off Controller
    */
    private void setPurposeOfSig() {

        this.purposeOfSigMap.putAll( new Map<String, String>{
                                        CODE_OF_SIG_APPROVAL => Label.SQX_PS_ControlledDocument_Approve_Reject_Approval_Request
                                    });

    }

    /**
    * default constructor to load esig policies for CQ Admin app
    */
    public SQX_Extension_Controlled_Doc_Approval() {

        super(null);

        setPurposeOfSig();
        this.actionName = CODE_OF_SIG_APPROVAL;

        this.purposeOfSignature = this.purposeOfSigMap.get(CODE_OF_SIG_APPROVAL);
    }

    public SQX_Extension_Controlled_Doc_Approval(ApexPages.StandardController controller, String prmReqId) {
        super(controller);

        stdController = controller;

        setPurposeOfSig();

        this.actionName = CODE_OF_SIG_APPROVAL;

        this.purposeOfSignature = this.purposeOfSigMap.get(CODE_OF_SIG_APPROVAL);

        try{
            controller.addFields(new List<String> { 'Secondary_Content_Reference__c', 'Content_Reference__c', 'Secondary_Content__c', 'Valid_Content_Reference__c'});
        }
        catch(Exception ex){
            // ignore exception that can occur when adding fields
        }

        doc = (SQX_Controlled_Document__c)controller.getRecord();

        SQX_Action_Response response = initialize(doc, prmReqId);
        response.addMessages();
    }

    global SQX_Extension_Controlled_Doc_Approval(ApexPages.StandardController controller) {
        this(controller, ApexPages.currentPage().getParameters().get('reqId'));
    }

    /**
    * Initialize document and approval item id and validate approval permission
    */
    public SQX_Action_Response initialize(SQX_Controlled_Document__c doc,String prmReqId) {
        if(this.doc == null && doc != null) {
            this.doc = doc;
        }
        SQX_Action_Response actionResponse = new SQX_Action_Response();
        this.recordId = doc.Id;

        if (String.isBlank(prmReqId)) {
            if (doc.Id != null) {
                // if reqId is blank then search for workitems assigned to the current user or current user queues
                List<ProcessInstanceWorkitem> docWorkItems = SQX_Approval_Util.getProcessInstanceWorkitems(UserInfo.getUserId(), doc.Id, true);
                if (docWorkItems.size() > 0) {
                    ApprovalRequestItem = docWorkItems[0];
                }
            }
        } else {
            try {
                Id reqId = prmReqId;
                ApprovalRequestItem = SQX_Approval_Util.getProcessInstanceWorkitem(reqId);
            } catch (Exception ex) {
                // ignore invalid reqId data
            }
        }

        if (doc.Id == null || ApprovalRequestItem == null) {
            if (doc.Id != null && String.isBlank(prmReqId)) {
                // show error when reqId is blank and the current user has no assigned items for the controlled document
                actionResponse.errorMessages.add(Label.SQX_ERR_MSG_FOR_NO_PENDING_APPROVAL_ASSIGNED_TO_USER);
            } else {
                // show error if current document is not set or pending approval request is not found/valid
                actionResponse.errorMessages.add(Label.SQX_ERR_MSG_APPROVAL_REQUEST_DATA_NOT_FOUND);
            }

            // set no permission on approval request
            ApprovalRequestItemPermission = new UserRecordAccess();
        } else {
            // get permission on approval request
            ApprovalRequestItemPermission = SQX_Approval_Util.getUserRecordAccess(new Set<Id> { ApprovalRequestItem.Id }, UserInfo.getUserId(), true).get(ApprovalRequestItem.Id);

            //check if actual approver has delegated approver or not
            User usr = [SELECT DelegatedApproverId FROM User WHERE id =: ApprovalRequestItem.OriginalActorId LIMIT 1];
            if(usr.DelegatedApproverId != null){
                delegatedApproverId = usr.DelegatedApproverId;
            }

            SQX_Action_Response validatedResponse = validateApprovalPermissionAPI();
            if(validatedResponse.hasMessages()) {
                actionResponse.mergeResponse(validatedResponse);
            }
        }

        // load latest content version
        List<Id> contentDocIds = new List<Id>();
        contentDocIds.add(doc.Content_Reference__c);
        if(!String.isBlank(doc.Secondary_Content_Reference__c) && doc.Secondary_Content__c != SQX_Controlled_Document.SEC_CONTENT_SYNC_DISABLED
          && doc.Content_Reference__c != doc.Secondary_Content_Reference__c){
            contentDocIds.add(doc.Secondary_Content_Reference__c);
        }

        Map<Id, ContentDocument> docs = new Map<Id, ContentDocument>([SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Id IN : contentDocIds]);

        if (docs.size() > 0) {
            activeCD = docs.get(doc.Valid_Content_Reference__c);
            primaryCD = docs.get(doc.Content_Reference__c);
            secondaryCD = docs.get(doc.Secondary_Content_Reference__c);
        }

        // get electronic signature settings
        SQX_CQ_Electronic_Signature__c cqES = SQX_CQ_Electronic_Signature__c.getInstance();
        ES_Enabled = cqES.Enabled__c;
        ES_Ask_Username = false;
        if (ES_Enabled) {
            ES_Ask_Username = cqES.Ask_Username__c;
            if (ES_Ask_Username == false) {
                // set username of the current user when not asked
                SigningOffUsername = UserInfo.getUserName();
            }
        }
        return actionResponse;
    }

    /**
     * This method returns a map of record type by developer name for controlled document object.
     */
    global Map<String, String> getRecordTypes() {
        return SQX_Utilities.getRecordTypeIdsByDevelopernameFor(new List<String> { SQX.ControlledDoc }).get(SQX.ControlledDoc);
    }

    /*
    * validates approve/reject permission of current user
    */
    private SQX_Action_Response validateApprovalPermissionAPI() {
        SQX_Action_Response response = new SQX_Action_Response();

        if (!ApprovalRequestItemPermission.HasEditAccess || !(UserInfo.getUserId() != delegatedApproverId)){
             response.errorMessages.add(Label.SQX_ERR_MSG_INSUFFICIENT_PERMISSION_ON_APPROVAL_REQUEST);
        }
        return response;
    }

    /*
    * approve / reject current approval request verifying esig
    */
    public SQX_Action_Response approveRejectDocumentAPI(Boolean isApprove) {
        SQX_Action_Response response = new SQX_Action_Response();
        if (this.hasValidCredentials()) {
            response = validateApprovalPermissionAPI();
            if (!response.hasMessages()) {
                // approve or reject on behalf of original actor
                Id originalActorId = ApprovalRequestItem.OriginalActorId;
                SavePoint startPoint = Database.setSavePoint();
                try {
                    if (isApprove) {
                        // approve approval request
                        SQX_Approval_Util.approveRejectRecallRecords(new List<sObject> { doc }, null, null,  this.RecordActivityComment, null, null, originalActorId);
                    } else {
                        // reject approval request
                        SQX_Approval_Util.approveRejectRecallRecords(null, new List<sObject> { doc }, null, null,  this.RecordActivityComment, null, originalActorId);
                    }


                } catch (Exception ex) {
                    Database.rollBack(startPoint);
                    response.errorMessages.add(ex.getMessage());
                }
            }
        } else {
            response.errorMessages.add(Label.SQX_INVALID_USERNAME_PASSWORD);
        }

        response.addMessages();
        return response;
        }

    /*
    * approve / reject current approval request verifying esig
    */
    public PageReference approveRejectDocument(Boolean isApprove) {
        approveRejectDocumentAPI(isApprove);
        PageReference retURL = null;
        PageReference defRetURL = new ApexPages.StandardController(doc).View();
        retURL =  SQX_Utilities.getValidReturnUrl(null, defRetURL);
        return retURL;
    }

    /*
    * page action to approve approval request
    */
    global PageReference approveDocument() {
        return approveRejectDocument(true);
    }

    /*
    * page action to reject approval request
    */
    global PageReference rejectDocument() {
        return approveRejectDocument(false);
    }

    /*
    * returns content link using vf page "SQX_View_Controlled_Document"
    */
    global String getContentLink() {
        return 'SQX_View_Controlled_Document?id=' + doc.Id;
    }

    /*
    * returns salesfoce content server url path
    */
    global String getContentServer() {
        return SQX_Utilities.getContentServer();
    }

    /*
    * returns salesforce content server native url to download the document content revision
    */
    global String getContentServerNativeLink() {
        return (primaryCD == null ? '' : SQX_Utilities.getContentServer() + 'download/' + primaryCD.LatestPublishedVersionId + '?asPdf=false');
    }

    /*
    * returns salesforce content server native url for secondary content to download revision
    */
    global String getSecondaryContentServerNativeLink() {
        return getSecondaryContentLink().getUrl();
    }

    /**
     * Internally used method to get the actual page reference that is to be used for redirecting to secondary native link
     */
    public PageReference getSecondaryContentLink() {
        PageReference ref = Page.SQX_View_Controlled_Document;
        Map<String, String> getParam = ref.getParameters();
        getParam.put('id', doc.Id);
        getParam.put('secondary', String.valueOf(true));
        getParam.put('download', String.valueOf(true));

        return ref;
    }

    /*
    * returns content server url to view the document content revision as pdf
    */
    global String getContentServerPdfLink() {
        return (primaryCD == null ? '' : SQX_Utilities.getContentServer() + 'renditionDownload?versionId=' + primaryCD.LatestPublishedVersionId + '&operationContext=CONTENT&rendition=PDF');
    }

    /*
    * returns flash vars while previewing the document content revision
    */
    global String getFlashVars() {
        return (activeCD == null ? '' : 'shepherd_prefix=/sfc/servlet.shepherd&v=' + activeCD.LatestPublishedVersionId + '&mode=details');
    }

    /*
    * returns approval history of the document
    */
    global List<ProcessInstanceHistory> getApprovalHistoryItems() {
        return SQX_Approval_Util.getProcessInstanceHistoryItems(doc.Id);
    }

    /**
     * This method returns approval step name of the current pending approval process for the document
     */
    global String getCurrentApprovalProcessStepName() {
        List<ProcessInstanceNode> nodes = SQX_Approval_Util.getPendingProcessInstanceNode( doc.Id );
        return nodes.size() > 0 ? nodes[0].ProcessNodeName : '';
    }
}