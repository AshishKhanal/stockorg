/**
 * This test class ensures that the data created by this class for setting field audit trail policy is valid.
 */
@isTest
public class SQX_Test_Audit_Trail_Retention_Policy {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_SYS_ADMIN, role, 'adminUser');
    }
    
    /**
      * This test method ensures that the list of cq custom objects are valid.
      * @story SQX-3462
      */
    public testmethod static void ensureConstructedListForCQCustomObjectsAreValid(){
        System.runas(SQX_Test_Account_Factory.getUsers().get('adminUser')){
            SQX_Controller_AuditTrailRetentionPolicy ctrl = new SQX_Controller_AuditTrailRetentionPolicy();
            
            // Act : get the list of all cq custom object, list of archive after months value, list of archive retention year and list of grace periods value
            List<SelectOption> objectNameList = ctrl.getObjectNameList();
            List<SelectOption> archiveAfterMonthList = ctrl.getArchiveAfterMonth();
            List<SelectOption> archiveRetentionYearList = ctrl.getArchiveRetentionYear();
            List<SelectOption> gracePeriodList = ctrl.getGracePeriods();
            
            // Assert : list should not be null
            System.assert(objectNameList != null && objectNameList.size() > 0, 'Expected the list of object should not be null but is'+objectNameList );
            
            // Assert : label first elment should be 'None' and value to be blank
            System.assert(objectNameList[0].getLabel() == Label.CQ_UI_Dropdown_None, 'Expected the label of first item of object name list to be None but is '+objectNameList[0].getValue());
            System.assert(objectNameList[0].getValue() == '', 'Expected the value of first item of object name list to be empty but is '+objectNameList[0].getValue());
            for (Integer i = 1; i < objectNameList.size(); i++) {
                System.assert(objectNameList[i].getValue().contains( SQX.NSPrefix ) || SQX_Controller_AuditTrailRetentionPolicy.cqSharedStandardObjectSet.contains(objectNameList[i].getValue()), 'Object cannot belong out of cq name space :'+objectNameList[i].getValue());
            }
            
            // Assert : the value of archive after month has minimum value 1 and maximum 18
            System.assert(archiveAfterMonthList.size() == SQX_Controller_AuditTrailRetentionPolicy.ARCHIVE_AFTER_MONTH_MAX_VAL, 'Expected the size of archiveAfterMonthList to be 18 but is '+archiveAfterMonthList.size());
            for(Integer i = 0; i < SQX_Controller_AuditTrailRetentionPolicy.ARCHIVE_AFTER_MONTH_MAX_VAL; i++) {
                System.assertEquals(''+ (i+1), archiveAfterMonthList[i].getValue());
            }
            
            // Assert : the value of archive retention year has minimum value 0 and maximum 10
            System.assert(archiveRetentionYearList.size() == SQX_Controller_AuditTrailRetentionPolicy.ARCHIVE_RETENTION_YEAR_MAX_VAL + 1, 'Expected the size of archiveRetentionYearList to be 10 but is '+archiveRetentionYearList.size());
            for(Integer i = 0; i <= SQX_Controller_AuditTrailRetentionPolicy.ARCHIVE_RETENTION_YEAR_MAX_VAL; i++) {
                System.assertEquals(''+i, archiveRetentionYearList[i].getValue());
            }
            
            // Assert : the value of gracePeriodList has minimum value 0 and maximum 10
            System.assert(gracePeriodList.size() == SQX_Controller_AuditTrailRetentionPolicy.GRACE_PERIOD_MAX_VAL + 1, 'Expected the size of gracePeriodList to be 10 but is '+gracePeriodList.size());
            for(Integer i = 1; i <= SQX_Controller_AuditTrailRetentionPolicy.GRACE_PERIOD_MAX_VAL; i++) {
                System.assertEquals(''+i, gracePeriodList[i].getValue());
            }
        }
    }
}