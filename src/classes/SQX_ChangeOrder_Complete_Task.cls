/*
*This method is used to go through the  implementation action type and its related task
*/
public with Sharing class SQX_ChangeOrder_Complete_Task {
    
    //Param that determine which type is selected by user  when user click in action link which is implementation action type
    final public static  String PARAM_IMPL_URL_TYPE ='type';
    public ApexPages.StandardController stdCntrlr {get; set;}
    public SQX_ChangeOrder_Complete_Task(ApexPages.StandardController controller) {
        stdCntrlr = controller;
    }
    /*
    *This method is used to validate change order implementation tasks
    */
    public pageReference validateImplementationTask(){
        
        Id ImplementationId =Id.valueOf(ApexPages.currentPage().getParameters().get(SQX_Extension_Controlled_Document.PARAM_IMPL_RecordId));
        String urlTypeVal=String.valueOf(ApexPages.currentPage().getParameters().get(PARAM_IMPL_URL_TYPE));
        SQX_Implementation__c implementation = [SELECT SQX_Change_Order__c,SQX_Controlled_Document__c, Type__c, Title__c,Status__c, Description__c, SQX_Change_Order__r.Status__c FROM SQX_Implementation__c WHERE Id = : ImplementationId ];
        if(implementation.SQX_Change_Order__r.Status__c == SQX_Change_Order.STATUS_VOID){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.SQX_ERR_CANNOT_COMPLETE_VOIDED_CHANGE_ORDER_TASK));
            return null;
        }
        if( urlTypeVal!=null && urlTypeVal!=implementation.Type__c){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.SQX_Err_When_Action_Link_Click_Other_Than_Plan_Type));
            return null;
        }
        if(implementation!=null && implementation.Status__c==SQX_Implementation.STATUS_OPEN){
            PageReference redirectToCompleteTask=checkActionTypeImplementation(implementation);
            redirectToCompleteTask.getParameters().put('retURL',implementation.SQX_Change_Order__c);
            return redirectToCompleteTask;
        }
        if(implementation.Status__c != SQX_Implementation.STATUS_OPEN){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_Change_Order_Action_Complete_Task));
        }
        return null;
    }

    /*
    *check the implementation action type and gets it related page reference
    */
    public PageReference checkActionTypeImplementation(SQX_Implementation__c impl){
        
        PageReference changeOrdercompleteTask=null;
        if(impl.Type__c=='Generic'){
            changeOrdercompleteTask = Page.SQX_Change_Imp;
            changeOrdercompleteTask.getParameters().put('id',impl.Id );
            
        }
        if(impl.Type__c==Label.CQ_UI_New_Document){
            changeOrdercompleteTask = Page.SQX_Controlled_Document_Editor;
            changeOrdercompleteTask.getParameters().put('implementation',impl.Id );
        }
        
        if(impl.Type__c==Label.CQ_UI_Revise_Document && impl.compliancequest__SQX_Controlled_Document__c!=null){
            changeOrdercompleteTask = Page.SQX_Revise_Controlled_Doc;
            changeOrdercompleteTask.getParameters().put('implementation', impl.Id);
            changeOrdercompleteTask.getParameters().put('id', impl.SQX_Controlled_Document__c);
            
        }
        
        if(impl.Type__c==Label.CQ_UI_Obsolete_Document && impl.compliancequest__SQX_Controlled_Document__c!=null){
            changeOrdercompleteTask = Page.SQX_Document_Review_Editor;
            changeOrdercompleteTask.getParameters().put('implementation', impl.Id);
            changeOrdercompleteTask.getParameters().put('controlleddocid', impl.SQX_Controlled_Document__c);
        }
        
        return changeOrdercompleteTask;
        
    }
    
    
}