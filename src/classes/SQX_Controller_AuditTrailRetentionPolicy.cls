/**
 * This controller is used to fetch all objects to set audit trail history retention policy.
 */
public class SQX_Controller_AuditTrailRetentionPolicy {
    
    // holds the serialized string of all fetched object's map
    private String serializedJSON;

    public static final String CUSTOM_META_DATA_TYPE_IDENTIFIER = '__mdt';

    public static final Integer ARCHIVE_AFTER_MONTH_MAX_VAL = 18,
                         ARCHIVE_RETENTION_YEAR_MAX_VAL = 10,
                         GRACE_PERIOD_MAX_VAL = 10;
    
    // holds list of select option for api name and its label of objects.
    private List<SelectOption> objectNameList { get; set; }
    
    // holds list of select option for archive after month
    private List<SelectOption> archiveAfterMonth { get; set; }
    
    // holds list of select option for archive retention year
    private List<SelectOption> archiveRetentionYear { get; set; }
    
    // holds list of select option for archive grace period
    private List<SelectOption> gracePeriods { get; set; }

    // List of shared standard objects.
    public static Set<String> cqSharedStandardObjectSet = new Set<String> {'Account', 'Activity', 'Contact', 'ContentVersion'};
    
    /**
     * constructor for page
     */
    public SQX_Controller_AuditTrailRetentionPolicy() {
        objectNameList = new List<SelectOption>();
        archiveAfterMonth = new List<SelectOption>();
        archiveRetentionYear = new List<SelectOption>();
        gracePeriods = new List<SelectOption>();
        
        
        for(Integer i = 0; i<= ARCHIVE_AFTER_MONTH_MAX_VAL; i++) {
            // add select list data for archive after month. min value for archiveAfterMonth is 1.
            if( i > 0 ) {
                archiveAfterMonth.add(new SelectOption(''+ i,''+ i));
            }
            
            // add select list data for archive retention year
            if( i <= ARCHIVE_RETENTION_YEAR_MAX_VAL ) {
                archiveRetentionYear.add(new SelectOption(''+ i,''+ i));
            }
            
            // add select list data for grace periods
            if( i <= GRACE_PERIOD_MAX_VAL ) {
                gracePeriods.add(new SelectOption(''+ i,''+ i));
            }
        }
        
        // add None as first element in the drop down list
        objectNameList.add(new SelectOption('', Label.CQ_UI_Dropdown_None));
        
        Map<String, Schema.SObjectType> allObjectMap = Schema.getGlobalDescribe();
        List<Map<String,String>> allAvailableObjectsToProcess = new List<Map<String,String>>();
        for(String objName : allObjectMap.keySet()) {

            Schema.DescribeSObjectResult objectDescription = allObjectMap.get(objName).getDescribe();
                
            if ( objectDescription.isCustom() && !objectDescription.isCustomSetting() ) {
                if ( objName.startsWith( SQX.NSPrefix ) && !objName.endsWith( CUSTOM_META_DATA_TYPE_IDENTIFIER ) ) {
                    // add the cq object
                    objectNameList.add( new SelectOption( objectDescription.getName(), objectDescription.getLabel() ) );
                }

            } else if (cqSharedStandardObjectSet.contains(objectDescription.getName())){
                // add the shared standard object
                objectNameList.add( new SelectOption( objectDescription.getName(), objectDescription.getLabel() ) );
            }
        }
        
        // sorting the array in alphabetical order
        for (Integer i = 1; i < objectNameList.size(); i++) {
            for (Integer j = i+1; j < objectNameList.size(); j++) {
                if ( objectNameList[i].getLabel() > objectNameList[j].getLabel() ) {
                    SelectOption temp = objectNameList[i];
                    objectNameList[i] = objectNameList[j];
                    objectNameList[j] = temp;
                }
            }
        }
    }
    
    /**
     * This method returns list of select option for api name and its label of objects.
     */
    public List<SelectOption> getObjectNameList() {
    
        return objectNameList;
    }
    
    /**
     * This method returns list of select option for archive after month.
     */
    public List<SelectOption> getArchiveAfterMonth() {
    
        return archiveAfterMonth;
    }
    
    /**
     * This method returns list of select option for archive retention year.
     */
    public List<SelectOption> getArchiveRetentionYear() {
    
        return archiveRetentionYear;
    }
    
    /**
     * This method returns list of select option for archive grace period.
     */
    public List<SelectOption> getGracePeriods() {
    
        return gracePeriods;
    }
}