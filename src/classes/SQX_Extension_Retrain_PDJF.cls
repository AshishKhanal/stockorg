/**
* extension class of SQX_Personnel_Document_Job_Function__c to retrain
*/
public with sharing class SQX_Extension_Retrain_PDJF {
    
    /**
    * main personnel document job function record to retrain
    */
    SQX_Personnel_Document_Job_Function__c mainRecord;
    
    /**
    * determines whether retrain action is performed or not and is used for avoiding next action in VF pages
    */
    public Boolean retrainPerformed { get; set; }
    
    /**
    * user input object to get retrain comment
    */
    public SQX_Personnel_Document_Job_Function__c retrainInput { get; set; }
    
    /**
    * determines whether retrain can be performed on PDJF or not
    */
    public Boolean canRetrain {
        get {
            return (mainRecord.Is_Archived__c == false
                    && SQX_Controlled_Document.VALID_DOCUMENT_STATUSES_FOR_TRAINING.contains(mainRecord.Controlled_Document_Status__c));
        }
    }
    
    /**
    * extension constructor for standard controller
    */
    public SQX_Extension_Retrain_PDJF(ApexPages.StandardController controller) {
        // initialize input object
        retrainInput = new SQX_Personnel_Document_Job_Function__c();
        retrainPerformed = false;
        
        if (!Test.isRunningTest()) {
            // these fields are requried to process retrain
            // bypass this in tests as these fields are passed with the controller
            controller.addFields(new String[] {
                'Name',
                'SQX_Personnel_Job_Function__c',
                'SQX_Requirement__c',
                'Personnel_Id__c',
                'Controlled_Document_Id__c',
                'Job_Function_Id__c',
                'Controlled_Document_Status__c',
                'Is_Archived__c'
            });
        }
        
        // get main record
        mainRecord = (SQX_Personnel_Document_Job_Function__c)controller.getRecord();
    }
    
    /**
    * action method to process retrain
    * @return generated retrain record view link on success, else <code>null</code>
    */
    public PageReference retrain() {
        PageReference retUrl = null;
        
        if (canRetrain) {
            // process valid PDJF
            try {
                SQX_Personnel_Document_Training__c retrainPdt = SQX_Personnel_Document_Job_Function.retrain(mainRecord, retrainInput.Comment__c);
                
                if (retrainPdt != null) {
                    // return retrain record view link on success
                    retUrl = new ApexPages.StandardController(retrainPdt).view();
                }
                else {
                    // show message for no trainings generated to retrain
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.SQX_MSG_RETRAIN_PJF_NO_DOCUMENT_TRAININGS_GENERATED));
                }
                
                retrainPerformed = true;
            }
            catch (Exception ex) {
                // show unknown error message in VF page
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            }
        }
        else {
            // show invalid PDJF error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.SQX_ERR_MSG_RETRAIN_INVALID_PERSONNEL_DOCUMENT_JOB_FUNCTION));
        }
        
        return retUrl;
    }
    
}