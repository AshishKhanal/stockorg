/**
* Batch processor to change Audit Program (SQX_Audit_Program__c) Status to Status and Approval Status.
* Added in version  : 6.4.0
* Used for versions : all earlier versions
*/
global without sharing class SQX_6_3_MigrateAuditProgramStatus implements Database.Batchable<sObject> {
   //Map to set old and new Status 
   public static final Map<String, String[]> OLD_STATUS_TO_NEW_STATUS_MAP = 
                new Map<String, String[]> {
                    'Draft' => new String[] { SQX_Audit_Program.STATUS_DRAFT, null },
                    'In Approval' => new String[] { SQX_Audit_Program.STATUS_DRAFT, SQX_Audit_Program.APPROVAL_STATUS_PLAN_APPROVAL },
                    'Rejected' => new String[] { SQX_Audit_Program.STATUS_DRAFT, SQX_Audit_Program.APPROVAL_STATUS_REJECTED },
                    'Active (Approved)' => new String[] { SQX_Audit_Program.STATUS_ACTIVE, SQX_Audit_Program.APPROVAL_STATUS_APPROVED },
                    'Closed' => new String[] { SQX_Audit_Program.STATUS_CLOSED, SQX_Audit_Program.APPROVAL_STATUS_APPROVED },    
                    'Void' => new String[] { SQX_Audit_Program.STATUS_VOID, null }     
                };
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Status & Approval Status of all Audit Program
        return Database.getQueryLocator([SELECT Id, Status__c, Approval_Status__c FROM SQX_Audit_Program__c]);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        if(scope.size()>0){
            //List of Audit Programs
            List<SQX_Audit_Program__c> auditPrograms = new List<SQX_Audit_Program__c>();
            
            //loop through each programs and set the Status and Approval Status
            for(SQX_Audit_Program__c auditProgram: (List<SQX_Audit_Program__c>)scope){
                String oldStatus = auditProgram.Status__c;
                if(OLD_STATUS_TO_NEW_STATUS_MAP.containsKey(oldStatus)){
                    auditProgram.Status__c = OLD_STATUS_TO_NEW_STATUS_MAP.get(oldStatus)[0];
                    auditProgram.Approval_Status__c = OLD_STATUS_TO_NEW_STATUS_MAP.get(oldStatus)[1];
                    auditPrograms.add(auditProgram); 
                }
            }
            //if any Audit Program exist update Audit Program
            if(!auditPrograms.isEmpty()){
                Database.update(auditPrograms, true);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){}

}