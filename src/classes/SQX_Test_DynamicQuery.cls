/**
 * Test class to ensure the functionality of SQX_DynamicQuery classes functionality
 */
@IsTest
public with sharing class SQX_Test_DynamicQuery {

    /**
     * Sets up account and contacts for subsequent queries.
     */
    @testSetup
    static void setupData() {
        Account[] acts = new Account[] {
            new Account(Name = 'Test Dynamic Query 1', Website = 'https://account1.sqx.test.dynamiquery.com'),
            new Account(Name = 'Test Dynamic Query 2', Website = 'https://account2.sqx.test.dynamiquery.com'),
            new Account(Name = 'Test Dynamic Query 3', Website = 'https://account3.sqx.test.dynamiquery.com')
        };

        insert acts;

        Contact[] contacts = new Contact[] {
            new Contact(LastName = 'Contact Dynamic Query 1', AccountId = acts[0].Id),
            new Contact(LastName = 'Contact Dynamic Query 2', AccountId = acts[1].Id),
            new Contact(LastName = 'Contact Dynamic Query 3', AccountId = acts[2].Id),
            new Contact(LastName = 'Contact Dynamic Query 4')
        };

        insert contacts;
    }

    /**
     * Ensures that when querying with current record, correct value is returned
     */
    static testmethod void queryWithCurrentRecord() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.usv_param = 'Name';
        filter.usv_function = SQX_DynamicQuery.FUNCTION_CURRENT_RECORD;
        filter.operator = 'eq';
        filter.field = 'LastName';

        // Act: Query Contact with matching last name with the current record(account)
        List<SObject> contacts = SQX_DynamicQuery.evaluateWithCurrentRecord(new Account(Name = 'Contact Dynamic Query 4'), 1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that one record is returned
        System.assertEquals(1, contacts.size());
        System.assertEquals('Contact Dynamic Query 4', (String) contacts.get(0).get('LastName'));
    }

    /**
     * Ensures that current record can be used in conjunction with reference field
     */
    static testmethod void queryWithCurrentRecordReference() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.usv_param = 'Name';
        filter.usv_function = SQX_DynamicQuery.FUNCTION_REFERENCE_CURRENT_RECORD;
        filter.operator = 'eq';
        filter.s_value = 'Name';
        filter.field = 'AccountId';

        // Act: Query Contact with account name same as current account name
        List<SObject> contacts = SQX_DynamicQuery.evaluateWithCurrentRecord(new Account(Name = 'Test Dynamic Query 1'), 1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that one record is returned
        System.assertEquals(1, contacts.size());
        System.assertEquals('Contact Dynamic Query 1', (String) contacts.get(0).get('LastName'));
    }

    /**
     * Ensures that the refernce match function works correctly.
     */
    static testmethod void queryWithMatchingReferenceField() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.usv_param = 'Name';
        filter.usv_function = SQX_DynamicQuery.FUNCTION_REFERENCE;
        filter.operator = 'eq';
        filter.s_value = 'Test Dynamic Query 2';
        filter.field = 'AccountId';

        // Act: Query Contact with matching account name
        List<SObject> contacts = SQX_DynamicQuery.evaluate(1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that one record is returned
        System.assertEquals(1, contacts.size());
        System.assertEquals('Contact Dynamic Query 2', (String) contacts.get(0).get('LastName'));
    }

    /**
     * Ensures that the querying for created date greater than null works correctly
     */
    static testmethod void queryWithDateGreaterThanNull() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.operator = 'gte';
        filter.value = null;
        filter.field = 'CreatedDate';

        // Act: Query Contact with created date greater than null
        List<SObject> contacts = SQX_DynamicQuery.evaluate(1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that all 4 records are returned
        System.assertEquals(4, contacts.size(), 'Expected all contacts to be selected because they have non null date');

        // Act: Query Contact with created date greater than today + 1
        filter.value = DateTime.now().addDays(1);
        contacts = SQX_DynamicQuery.evaluate(1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that all no records are returned
        System.assertEquals(0, contacts.size(), 'Expected no contacts to be created one day ahead');
    }

    /**
     * Ensures that the querying for created date less than null works correctly
     */
    static testmethod void queryWithDateLessThanNull() {
        SQX_DynamicQuery.Filter filter = new SQX_DynamicQuery.Filter();
        filter.operator = 'lte';
        filter.value = null;
        filter.field = 'CreatedDate';

        // Act: Query Contact with created date less than null
        List<SObject> contacts = SQX_DynamicQuery.evaluate(1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that all 4 records are returned
        System.assertEquals(4, contacts.size(), 'Expected all contacts to be selected because they have non null date');

        // Act: Query Contact with created date less than today + 1
        filter.value = DateTime.now().addDays(1);
        contacts = SQX_DynamicQuery.evaluate(1, 10, Contact.SObjectType.getDescribe(), new SObjectField[] { Contact.Id, Contact.LastName }, filter, new SObjectField[] {}, true );

        // Assert: Ensure that all records are returned
        System.assertEquals(4, contacts.size(), 'Expected all contacts to be created one day ahead');
    }

    /**
     * Smoke test for validating the results are returned correctly
     */
    static testmethod void getRecordsUsingController() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        // Act: Get All the contacts with the account
        SQX_Data_Table_Query_Controller.SQX_ObjectsWithSchema res = SQX_Data_Table_Query_Controller.getObjects(accountId, 'Contact', 'Id,Name,CreatedDate', '{"field":"AccountId","operator":"eq","usv_function":"' + SQX_DynamicQuery.FUNCTION_CURRENT_RECORD +'","usv_param":"Id"}','{"field":"CreatedDate","operator":"neq","dtt_value":"2000-01-01T18:25:43.511Z"}', null, true, 1,30, 'test');

        // Assert: Ensure that contacts are returned
        System.assertEquals(1, res.sobjects.size());


        //Approval process smoke test
        res = SQX_Data_Table_Query_Controller.getObjects(accountId, 'ProcessInstanceWorkItem', '', '', '', '', true,1,30, 'test');
        System.assertEquals(0, res.sobjects.size(),'test');
    }

    /**
     * Given: Accounts are present in the database
     * When: dynamic query is executed to fetch specific fields
     * Then: queried fields are present in result
     */
    static testMethod void givenAccounts_whenDynamicQueryIsExecutedToFetchFields_thenQueriedFieldsAreReturned() {
        Id [] fixedSearchResults= new List<Id>(new Map<Id, Account>([SELECT Id FROM Account]).keySet());
        Test.setFixedSearchResults(fixedSearchResults);

        // Act: Query objects
        Account [] accounts = (Account[])SQX_DynamicQuery.findSObject('S', 1, 100, Account.SObjectType.getDescribe(), new SObjectField[] { Account.Id, Account.Name, Account.Website }, null, new SObjectField[] {Account.Name}, true);

        // Assert: Ensure all added accounts are returned and website is returned without exception
        System.assertEquals(fixedSearchResults.size(), accounts.size());
        System.assertNotEquals(null, accounts[0].Website);
    }

    /**
     * Given: Accounts are present in the database
     * When: dynamic query is executed to return result with descending order
     * Then: queried fields are present in result
     */
    static testMethod void givenAccounts_whenDynamicQueryIsExecutedWithOrderByDescending_thenResultIsSortedAppropriately() {
        Id [] fixedSearchResults= new List<Id>(new Map<Id, Account>([SELECT Id FROM Account ORDER By Name Asc]).keySet());
        Test.setFixedSearchResults(fixedSearchResults);

        // Act: Query objects
        Account [] accounts = (Account[])SQX_DynamicQuery.findSObject('S', 1, 100, Account.SObjectType.getDescribe(), new SObjectField[] { Account.Id, Account.Name, Account.Website }, null, new SObjectField[] {Account.Name}, false);

        // Assert: Ensure all added accounts are returned and website is returned without exception
        System.assertEquals(fixedSearchResults.size(), accounts.size());
        System.assertNotEquals(fixedSearchResults[fixedSearchResults.size() - 1], accounts[1].Id);
    }

    /**
     * Given: Accounts are present in the database
     * When: dynamic query is executed to return 1st page of 1 size
     * Then: queried result contains 1 item only
     */
    static testMethod void givenAccounts_whenDynamicQueryIsExecutedFixedPageSize_thenResultHasFixedSize() {
        Id [] fixedSearchResults= new List<Id>(new Map<Id, Account>([SELECT Id FROM Account ORDER By Name DESC]).keySet());
        Test.setFixedSearchResults(fixedSearchResults);
        //iterate to at least second page
        Integer pageNumber = 1, maxPage = fixedSearchResults.size() > 3 ? 3 : fixedSearchResults.size();

        while(pageNumber <= maxPage) {
            // Act: Query objects page 1
            Account [] accounts = (Account[])SQX_DynamicQuery.findSObject('S', pageNumber, 1, Account.SObjectType.getDescribe(), new SObjectField[] { Account.Id, Account.Name, Account.Website }, null, new SObjectField[] {Account.Name}, false);

            // Assert: Ensure all added accounts are returned and website is returned without exception
            System.assertEquals(1, accounts.size());
            System.assertEquals(fixedSearchResults[pageNumber - 1], accounts[0].Id, pageNumber + ' ' + accounts + ' '  + fixedSearchResults);
            pageNumber++;
        }
    }

    /**
     * Given: Accounts are present in the database
     * When: dynamic query is executed to without a particular field
     * Then: accessing the field in result, results in an error. (Ensures field list works correctly)
     */
    static testMethod void givenAccounts_whenDynamicQueryIsExecutedWithoutField_thenAccessingResultForFieldResultInException() {
        Id [] fixedSearchResults= new List<Id>(new Map<Id, Account>([SELECT Id FROM Account ORDER By Name Asc]).keySet());
        Test.setFixedSearchResults(fixedSearchResults);

        // Act: Query objects via Extension
        Account [] accounts = (Account[])SQX_Extension_UI.searchObjects('S', 'Account', new String[] { 'Id', 'Name' }, null, 1, new String[]{'Name'}, true);

        // Assert: Ensure only all accounts are returned and accessing website gives an exception
        System.assertEquals(fixedSearchResults.size(), accounts.size());
        try {
            String website = accounts[0].Website;
            System.assert(false, 'Exception should have occurred when trying to query website field');
        } catch(SObjectException ex){

        }
    }
}