/**
* unit tests for batch jobs to process training session rosters and related document trainings when training session is completed and closed
*/
@isTest
public class SQX_Test_6729_RosterBatchProcessing {
    
    /**
    * constants
    */
    static final Integer TOTAL_PSNS = 100;
    static final String ROSTER_RESULT_COMPLETE = SQX_Training_Session_Roster.RESULT_COMPLETE,
        ROSTER_RESULT_INCOMPLETE = SQX_Training_Session_Roster.RESULT_INCOMPLETE,
        MODE_TRAINING_GENERATION = SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_GENERATION,
        MODE_TRAINING_COMPLETION = SQX_Training_Session_Roster.ACTIVITY_CODE_DOCUMENT_TRAINING_COMPLETION;
    
    /**
    * private variables
    */
    static SQX_Test_Training_Session testTs;
    static List<SQX_Controlled_Document__c> docs;
    static List<SQX_Personnel__c> psns;
    static List<SQX_Training_Session_Roster__c> rosters;
    static Map<String, Map<Id, SQX_Training_Session_Roster__c>> resultPsnRosterMap;
    
    /**
    * method to enable SQX_Custom_Settings_Public__c.Use_Batch_For_Session_Roster_Processing__c used for processing training session rosters
    */
    static void enableRosterBatchJobCustomSetting(User admin) {
        System.runas(admin) {
            SQX_Test_Utilities.setCustomSettingSpecifiedFieldValue(new Map<SObjectField, Object> {
                SQX_Custom_Settings_Public__c.Use_Batch_For_Session_Roster_Processing__c => true
            });
        }
    }
    
    /**
    * method for setting up test data
    */
    static void setupTestData() {
        // ARRANGE: add required personnels
        psns = new List<SQX_Personnel__c>();
        String prefix = 'TEST_6729_PSN_';
        for (Integer i = 0; i < TOTAL_PSNS; i++) {
            psns.add(new SQX_Personnel__c(
                Full_Name__c = prefix + i,
                Identification_Number__c = prefix + i
            ));
        }
        insert psns;
        
        // ARRANGE: add documents for training session subject and fulfill requirements
        docs = new List<SQX_Controlled_Document__c>();
        Map<String, String> docNumStatusMap = new Map<String, String>{
            'TEST_6729_DOC_0' => SQX_Controlled_Document.STATUS_CURRENT,
            'TEST_6729_DOC_1' => SQX_Controlled_Document.STATUS_CURRENT,
            'TEST_6729_DOC_2' => SQX_Controlled_Document.STATUS_CURRENT,
            'TEST_6729_DOC_3' => SQX_Controlled_Document.STATUS_PRE_RELEASE,
            'TEST_6729_DOC_4' => SQX_Controlled_Document.STATUS_PRE_RELEASE
        };
        for (String docNum : docNumStatusMap.keySet()) {
            SQX_Controlled_Document__c doc = new SQX_Test_Controlled_Document().doc;
            String docStatus = docNumStatusMap.get(docNum);
            
            doc.Document_Number__c = docNum;
            doc.Document_Status__c = docStatus;
            if (docStatus != SQX_Controlled_Document.STATUS_DRAFT) {
                doc.Date_Issued__c = System.Today();
            }
            
            docs.add(doc);
        }
        insert docs;
        
        // ARRANGE: add required open training session
        testTs = new SQX_Test_Training_Session();
        testTs.trainingSession.SQX_Controlled_Document__c = docs[0].Id;
        testTs.setStatus(SQX_Training_Session.STATUS_OPEN);
        testTs.save();
        
        // ARRANGE: add required fulfill requirements for the session
        List<SQX_Training_Fulfilled_Requirement__c> reqs = new List<SQX_Training_Fulfilled_Requirement__c>();
        for (Integer i = 1; i < docs.size(); i++) {
            reqs.add(new SQX_Training_Fulfilled_Requirement__c(SQX_Training_Session__c = testTs.trainingSession.Id, SQX_Controlled_Document__c = docs[i].Id));
        }
        insert reqs;
        
        // add required rosters
        rosters = new List<SQX_Training_Session_Roster__c>();
        resultPsnRosterMap = new Map<String, Map<Id, SQX_Training_Session_Roster__c>>{
            ROSTER_RESULT_COMPLETE => new Map<Id, SQX_Training_Session_Roster__c>(),
            ROSTER_RESULT_INCOMPLETE => new Map<Id, SQX_Training_Session_Roster__c>()
        };
        for (Integer i = 0; i < psns.size(); i++) {
            String result = math.mod(i, 2) == 0 ? ROSTER_RESULT_COMPLETE : ROSTER_RESULT_INCOMPLETE; // alternately set complete and incomplete roster result
            SQX_Training_Session_Roster__c r = new SQX_Training_Session_Roster__c(
                SQX_Training_Session__c = testTs.trainingSession.Id,
                SQX_Personnel__c = psns[i].Id,
                Result__c = result
            );
            rosters.add(r);
            resultPsnRosterMap.get(result).put(psns[i].Id, r);
        }
        insert rosters;
    }
    
    
    @testSetup
    static void commonSetup() {
        // add required users
        User admin1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, 'admin1');
        User user1 = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, 'user1');
    }
    
    /**
    * ensures Batch_Processed__c returns proper values as per roster batch job status, roster processing queued on, roster activity code
    */
    @isTest static void ensureRosterBatchProcessedFormulaFieldValue() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('admin1')) {
            /*
                Test data:
                [activity codes: x = null, g = generation, c = completion, Q = queuedOn, P = queuedOn0]
                session roster batch job status p0  p1  p2  p3  p4
                ======= ======================= ==  ==  ==  ==  ==
                ts0     x                       x   gQ  gP  cQ  cP
                ts1     Generation Queued       x   gQ  gP  cQ  cP
                ts2     Generation Error        x   gQ  gP  cQ  cP
                ts3     Completion Queued       x   gQ  gP  cQ  cP
                ts4     Completion Error        x   gQ  gP  cQ  cP
            */
            
            // ARRANGE: add personnels
            psns = new List<SQX_Personnel__c>();
            psns.add(new SQX_Test_Personnel().mainRecord);
            psns.add(new SQX_Test_Personnel().mainRecord);
            psns.add(new SQX_Test_Personnel().mainRecord);
            psns.add(new SQX_Test_Personnel().mainRecord);
            psns.add(new SQX_Test_Personnel().mainRecord);
            insert psns;
            
            DateTime queuedOn = System.now();
            DateTime queuedOn0 = queuedOn.addSeconds(-1);
            
            // ARRANGE: add sessions
            List<SQX_Training_Session__c> sessions = new List<SQX_Training_Session__c>();
            sessions.add(new SQX_Test_Training_Session().trainingSession);
            
            SQX_Training_Session__c ts = new SQX_Test_Training_Session().trainingSession;
            ts.Roster_Batch_Job_Status__c = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_QUEUED;
            ts.Roster_Processing_Queued_On__c = queuedOn;
            sessions.add(ts);
            
            ts = new SQX_Test_Training_Session().trainingSession;
            ts.Roster_Batch_Job_Status__c = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_ERROR;
            ts.Roster_Processing_Queued_On__c = queuedOn;
            sessions.add(ts);
            
            ts = new SQX_Test_Training_Session().trainingSession;
            ts.Roster_Batch_Job_Status__c = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED;
            ts.Roster_Processing_Queued_On__c = queuedOn;
            sessions.add(ts);
            
            ts = new SQX_Test_Training_Session().trainingSession;
            ts.Roster_Batch_Job_Status__c = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_ERROR;
            ts.Roster_Processing_Queued_On__c = queuedOn;
            sessions.add(ts);
            insert sessions;
            
            // ARRANGE: add rosters with activity codes
            rosters = new List<SQX_Training_Session_Roster__c>();
            for (Integer i = 0; i < sessions.size(); i++) {
                for (Integer j = 0; j < psns.size(); j++) {
                    SQX_Training_Session_Roster__c r = new SQX_Training_Session_Roster__c(
                        SQX_Training_Session__c = sessions[i].Id,
                        SQX_Personnel__c = psns[j].Id
                    );
                    
                    if (j == 1) {
                        r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_TRAINING_GENERATION, queuedOn);
                    }
                    if (j == 2) {
                        r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_TRAINING_GENERATION, queuedOn0);
                    }
                    if (j == 3) {
                        r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_TRAINING_COMPLETION, queuedOn);
                    }
                    if (j == 4) {
                        r.Activity_Code__c = SQX_Training_Session_Roster.formatActivityCode(MODE_TRAINING_COMPLETION, queuedOn0);
                    }
                    
                    rosters.add(r);
                }
            }
            
            // ACT: insert rosters
            insert rosters;
            
            // ASSERT: verify Batch_Processed__c value
            Map<Id, Set<Id>> expTrueValues = new Map<Id, Set<Id>>{
                sessions[0].Id => new Set<Id>{ },
                sessions[1].Id => new Set<Id>{ psns[1].Id },
                sessions[2].Id => new Set<Id>{ psns[1].Id },
                sessions[3].Id => new Set<Id>{ psns[3].Id },
                sessions[4].Id => new Set<Id>{ psns[3].Id }
            };
            for (SQX_Training_Session_Roster__c r : [SELECT SQX_Training_Session__c, SQX_Personnel__c, Batch_Processed__c, Activity_Code__c,
                                                        SQX_Training_Session__r.Roster_Batch_Job_Status__c, SQX_Training_Session__r.Roster_Processing_Queued_On__c
                                                     FROM SQX_Training_Session_Roster__c]) {
                Boolean expVal = expTrueValues.get(r.SQX_Training_Session__c).contains(r.SQX_Personnel__c);
                System.assertEquals(expVal, r.Batch_Processed__c,
                    'Batch_Processed__c value does not match for roster batch job status: ' + r.SQX_Training_Session__r.Roster_Batch_Job_Status__c
                    + ', queued on date: ' + r.SQX_Training_Session__r.Roster_Batch_Job_Status__c + ', activity code: ' + r.Activity_Code__c);
            }
        }
    }
    
    /**
    * ensures batch job error is captured in respective rosters without failing whole batch
    * for this test case, we depend on following validation rule that prevents pending document training to complete when linked training session is not complete
    * Given:
    *   add 2 sessions with open status and rosters with complete result
    *   complete 1st session to generate pending document training
    *   add pending document training related to personnel and document of open training session and set the training session link in the pdt
    *   set batch job statuses of both sessions to queue for document training completion without changing session status
    * When:
    *   batch job is executed in document training completion mode
    * Then:
    *   roster of 1st session should be processed without error
    *   roster of 2nd session should record error message and batch job status should be set to error status
    */
    @isTest static void ensureBatchProcessingErrorIsRecordedInRelatedRoster() {
        System.runas(SQX_Test_Account_Factory.getUsers().get('admin1')) {
            DateTime queuedOn = System.now().addDays(-1);
            
            // ARRANGE: add personnels
            psns = new List<SQX_Personnel__c>();
            psns.add(new SQX_Test_Personnel().mainRecord);
            psns.add(new SQX_Test_Personnel().mainRecord);
            insert psns;
            
            // ARRANGE: add documents
            docs = new List<SQX_Controlled_Document__c>();
            for (Integer i = 0; i < 2; i++) {
                SQX_Controlled_Document__c doc = new SQX_Test_Controlled_Document().doc;
                doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
                doc.Date_Issued__c = System.today();
                docs.add(doc);
            }
            insert docs;
            
            // ARRANGE: add sessions
            List<SQX_Test_Training_Session> sessions = new List<SQX_Test_Training_Session>();
            for (Integer i = 0; i < 2; i++) {
                sessions.add(new SQX_Test_Training_Session());
                sessions[i].trainingSession.SQX_Controlled_Document__c = docs[i].Id;
                sessions[i].trainingSession.Status__c = SQX_Training_Session.STATUS_OPEN;
                sessions[i].save();
            }
            
            // ARRANGE: add rosters with complete result
            rosters = new List<SQX_Training_Session_Roster__c>();
            for (Integer i = 0; i < 2; i++) {
                rosters.add(new SQX_Training_Session_Roster__c(
                    SQX_Training_Session__c = sessions[i].trainingSession.Id,
                    SQX_Personnel__c = psns[i].Id,
                    Result__c = SQX_Training_Session_Roster.RESULT_COMPLETE
                ));
            }
            insert rosters;
            
            // ARRANGE: complete 1st session
            sessions[0].setStatus(SQX_Training_Session.STATUS_COMPLETE).save();
            
            // ARRANGE: add pdt for 2nd session
            SQX_Personnel_Document_Training__c pdt = new SQX_Personnel_Document_Training__c(
                SQX_Training_Session__c = sessions[1].trainingSession.Id,
                SQX_Personnel__c = psns[1].Id,
                SQX_Controlled_Document__c = docs[1].Id,
                Level_Of_Competency__c = SQX_Requirement.LEVEL_OF_COMPETENCY_READ_AND_UNDERSTAND,
                Due_Date__c = System.today(),
                Status__c = SQX_Personnel_Document_Training.STATUS_PENDING
            );
            insert pdt;
            
            // ARRANGE: queue sessions for document training completeion
            for (Integer i = 0; i < 2; i++) {
                sessions[i].synchronize();
                sessions[i].trainingSession.Roster_Batch_Job_Status__c = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED;
                sessions[i].trainingSession.Roster_Processing_Queued_On__c = queuedOn;
                if (i == 0) sessions[0].setStatus(SQX_Training_Session.STATUS_CLOSED);
                sessions[i].save();
            }
            
            Test.startTest();
            
            // ACT: execute batch job in document training completion mode
            SQX_TrainingSessionRoster_Processor processor = new SQX_TrainingSessionRoster_Processor();
            processor.mode = MODE_TRAINING_COMPLETION;
            Database.executeBatch(processor);
            
            Test.stopTest();
            
            List<SQX_Training_Session__c> tsns = [
                SELECT Status__c, Roster_Batch_Job_Status__c, Roster_Processing_Queued_On__c,
                    (SELECT Status__c FROM SQX_Document_Trainings__r WHERE Status__c IN :SQX_Personnel_Document_Training.PENDING_STATUSES)
                FROM SQX_Training_Session__c
                WHERE Id = :sessions[0].trainingSession.Id OR Id = :sessions[1].trainingSession.Id
                ORDER BY Name
            ];
            rosters = [SELECT Id, Activity_Code__c, Batch_Job_Error__c FROM SQX_Training_Session_Roster__c WHERE SQX_Training_Session__c IN :tsns ORDER BY Name];
            
            // ASSERT: verify processed completed session has no errors and session fields are set properly
            System.assertEquals(null, tsns[0].Roster_Batch_Job_Status__c, 'Expected batch job status to be null when all rosters are processed without any errors.');
            System.assertEquals(null, tsns[0].Roster_Processing_Queued_On__c, 'Expected batch job queued date to be null when all rosters are processed without any errors.');
            System.assertEquals(0, tsns[0].SQX_Document_Trainings__r.size(),
                'Expected no pending document training exists for completed session when rosters are processed for document training completion without errors.');
            
            // ASSERT: verify rosters of completed session are processed without errors and fields values as set properly
            System.assertNotEquals(null, rosters[0].Activity_Code__c,
                'Expected Activity_Code__c to be set when roster is processed without errors using batch job.');
            System.assertEquals(null, rosters[0].Batch_Job_Error__c,
                'Expected Batch_Job_Error__c to be null when roster is processed without errors using batch job.');
            
            // ASSERT: verify processed open session has errors and session fields are set properly
            System.assertEquals(SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_ERROR, tsns[1].Roster_Batch_Job_Status__c,
                'Expected error status to be set by batch job when processing open training session for document training completion.');
            System.assertEquals(queuedOn, tsns[1].Roster_Processing_Queued_On__c, 'Expected batch job queued date not to be changed when some or all rosters failed to process.');
            System.assertEquals(1, tsns[1].SQX_Document_Trainings__r.size(),
                'Expected pending document training to exist for open session when rosters are processed for document training completion with errors.');
            
            // ASSERT: verify processed rosters of open session have errors and fields values as set properly
            System.assertEquals(null, rosters[1].Activity_Code__c,
                'Expected Activity_Code__c not to be set when errors occurred while processing roster using batch job.');
            System.assertNotEquals(null, rosters[1].Batch_Job_Error__c,
                'Expected Batch_Job_Error__c to have error message when errors occurred while processing roster using batch job.');
            String expErrMsg = Label.SQX_TRIGGER_ERR_MSG_FOR_FAILED_UPDATE_ACTION_ON_RELATED_RECORD
                                    .replace(SQX_Trigger_Handler.ACTION_ERR_KEY_RECORD_ID, ''+pdt.Id)
                                    .replace(SQX_Trigger_Handler.ACTION_ERR_KEY_ERRORS, ''); // skipping actual errors
            System.assert(rosters[1].Batch_Job_Error__c.contains(expErrMsg),
                'Expected Batch_Job_Error__c value to contain "' + expErrMsg +'" in the error message while processing roster using batch job. Actual value: ' + rosters[1].Batch_Job_Error__c);
        }
    }
    
    /**
    * Given:
    *   batch job for processing roster is enabled
    *   training session is created with rosters and fulfill requirements
    * When:
    *   session is completed
    * Then:
    *   roster batch job status is propery set in session object with completed session queued status
    *   rosters are not processed i.e. pending document trainings are not generated
    * When:
    *   session is closed
    * Then:
    *   roster batch job status is not changed
    *   rosters are not processed
    * When:
    *   roster processing batch job with completed session mode is executed
    * Then:
    *   pending document trainings are generated for rosters with complete result
    *   roster batch job status is set with closed session queued status
    */
    @isTest static void givenCompleteAndCloseTrainingSession_RosterBatchJobWithCompletedSessionModeIsExexuted_PendingDocumentTrainingsAreGeneratedForRosters() {
        // get users
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User admin1 = users.get('admin1'),
            user1 = users.get('user1');
        
        // ARRANGE: enable use batch job custom setting to process training
        enableRosterBatchJobCustomSetting(admin1);
        
        System.runas(user1) {
            // ARRANGE: add required training session and rosters
            setupTestData();
            
            // ACT: complete training session
            testTs.setStatus(SQX_Training_Session.STATUS_COMPLETE).save();
            testTs.synchronize();
            
            // ASSERT: Roster_Batch_Job_Status__c field of training session when batch job is used
            String expJobStatus = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_GENERATION_QUEUED;
            System.assertEquals(expJobStatus, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Roster Batch Job Status is expected to be set properly when using batch job for training session completion.');
            // ASSERT: no rosters are processed for completed session when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c != null].size(),
                'Expected no rosters are processed for completed session when batch job is not executed.');
            // ASSERT: no document trainings are generated when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Personnel_Document_Training__c].size(),
                'Expected no document trainings to be generated when batch job is not executed.');
            
            // ACT: close training session
            testTs.setStatus(SQX_Training_Session.STATUS_CLOSED).save();
            testTs.synchronize();
            
            // ASSERT: Roster_Batch_Job_Status__c field of training session when batch job is used
            System.assertEquals(expJobStatus, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Roster Batch Job Status is expected not to change when training session is closed and session completion processing is still queued.');
            // ASSERT: no rosters are processed for completed session when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c != null].size(),
                'Expected no rosters are processed for completed session when batch job is not executed.');
            // ASSERT: no document trainings are generated when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Personnel_Document_Training__c].size(),
                'Expected no document trainings to be generated when batch job is not executed.');
        }
        
        System.runas(admin1) {
            Test.startTest();
            
            // ACT: execute batch job with document training generation mode for completed session
            SQX_TrainingSessionRoster_Processor processor = new SQX_TrainingSessionRoster_Processor();
            processor.mode = MODE_TRAINING_GENERATION;
            Database.executeBatch(processor);
            
            Test.stopTest();
        }
        
        System.runas(user1) {
            // ASSERT: all rosters are processed for completed session when batch job is executed
            System.assertEquals(rosters.size(), [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c LIKE :(MODE_TRAINING_GENERATION + '%')].size(),
                'Expected all rosters are processed for completed session when batch job is executed.' + [SELECT Activity_Code__c FROM SQX_Training_Session_Roster__c ]);
            // ASSERT: check roster batch job status of training session after completed session processing is complete
            testTs.synchronize();
            System.assertEquals(SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Expected Roster_Batch_Job_Status__c field to be set properly to queue for closed session processing when all rosters have been processed for completed session.');
            // ASSERT: pending document trainings are generated for rosters with complete result when batch job is executed
            Integer expPendingPDTCount = resultPsnRosterMap.get(ROSTER_RESULT_COMPLETE).size() * 5;
            System.assertEquals(expPendingPDTCount, [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING].size(),
                'Expected pending document trainings to be generated for rosters with complete result when batch job is executed.');
            // ASSERT: document trainings are not generated for rosters with incomplete result when batch job is executed
            System.assertEquals(0, [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE SQX_Personnel__c IN :resultPsnRosterMap.get(ROSTER_RESULT_INCOMPLETE).keySet()].size(),
                'Expected no document trainings to be generated for rosters with incomplete result when batch job is executed.');
        }
    }
    
    /**
    * Given:
    *   training session is created with rosters and fulfill requirements
    * When:
    *   session is completed
    * Then:
    *   pending document trainings are generated
    *   roster batch job status is not set in session object and is blank
    *   rosters are processed immediately i.e. pending document trainings are generated
    * When:
    *   batch job for processing roster is enabled
    *   session is closed
    * Then:
    *   roster batch job status is propery set in session object with closed session queued status
    *   rosters are not processed i.e. document trainings are signed off and completed
    * When:
    *   roster processing batch job with closed session mode is executed
    * Then:
    *   rosters are processed
    *   roster batch job status is set to blank
    */
    @isTest static void givenClosedTrainingSession_RunRosterBatchJob_PendingDocumentTrainingsAreSignedOffAndCompletedForRostersWithCompleteResult() {
        // get users
        Map<String, User> users = SQX_Test_Account_Factory.getUsers();
        User admin1 = users.get('admin1'),
            user1 = users.get('user1');
        
        Integer pendingPDTCount;
        
        System.runas(user1) {
            // ARRANGE: add required training session and rosters
            setupTestData();
            
            // ACT: complete training session without enabling batch job
            testTs.setStatus(SQX_Training_Session.STATUS_COMPLETE).save();
            testTs.synchronize();
            
            // ASSERT: Roster_Batch_Job_Status__c field of completed training session when batch job is not used
            System.assertEquals(null, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Roster Batch Job Status is expected to be blank when batch job is not used for training session completion.');
            // ASSERT: all rosters are processed for completed session immediately when session is closed without using batch job
            System.assertEquals(rosters.size(), [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c LIKE :(MODE_TRAINING_GENERATION + '%')].size(),
                'Expected all rosters are processed for completed session immediately when session is closed without using batch job.');
            // ASSERT: pending document trainings are generated for rosters with complete result when session is completed
            pendingPDTCount = resultPsnRosterMap.get(ROSTER_RESULT_COMPLETE).size() * 5;
            System.assertEquals(pendingPDTCount, [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_PENDING].size(),
                'Expected pending document trainings to be generated for rosters with complete result when session is completed.');
            
            
            // ARRANGE: enable use batch job custom setting to process closed training session
            enableRosterBatchJobCustomSetting(admin1);
            
            
            // ACT: close training session
            testTs.setStatus(SQX_Training_Session.STATUS_CLOSED).save();
            testTs.synchronize();
            
            // ASSERT: Roster_Batch_Job_Status__c field of training session when batch job is used
            String expJobStatus = SQX_Training_Session.ROSTER_BATCH_JOB_STATUS_DOCUMENT_TRAINING_COMPLETION_QUEUED;
            System.assertEquals(expJobStatus, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Roster Batch Job Status is expected to set properly when training session is closed using roster batch job processing.');
            // ASSERT: no rosters are processed for closed session when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c LIKE :(MODE_TRAINING_COMPLETION + '%')].size(),
                'Expected no rosters are processed for closed session when batch job is not executed.');
            // ASSERT: no pending document trainings are signed-off or completed when batch job is not executed
            System.assertEquals(0, [SELECT Id FROM SQX_Personnel_Document_Training__c WHERE Status__c != :SQX_Personnel_Document_Training.STATUS_PENDING].size(),
                'Expected pending document trainings are not signed-off or completed when batch job is not executed.');
        }
        
        System.runas(admin1) {
            Test.startTest();
            
            // ACT: execute batch job with closed session processing mode
            SQX_TrainingSessionRoster_Processor processor = new SQX_TrainingSessionRoster_Processor();
            processor.mode = MODE_TRAINING_COMPLETION;
            Database.executeBatch(processor);
            
            Test.stopTest();
        }
        
        System.runas(user1) {
            // ASSERT: all rosters are processed for closed session when batch job is executed
            System.assertEquals(rosters.size(), [SELECT Id FROM SQX_Training_Session_Roster__c WHERE Activity_Code__c LIKE :(MODE_TRAINING_COMPLETION + '%')].size(),
                'Expected all rosters are processed for closed session when batch job is executed.');
            // ASSERT: check roster batch job status of training session after closed session processing is complete
            testTs.synchronize();
            System.assertEquals(null, testTs.trainingSession.Roster_Batch_Job_Status__c,
                'Expected Roster_Batch_Job_Status__c field to be set properly to blank when all rosters have been processed for closed session.');
            // ASSERT: all pending document trainings are signed-off and completed when batch job is executed
            List<SQX_Personnel_Document_Training__c> pdts = [
                SELECT Id FROM SQX_Personnel_Document_Training__c
                WHERE Status__c = :SQX_Personnel_Document_Training.STATUS_COMPLETE
                    AND SQX_Training_Approved_By__c != null
                    AND Trainer_SignOff_Date__c != null
            ];
            System.assertEquals(pendingPDTCount, pdts.size(), 'Expected all pending document trainings are signed off and completed when batch job is executed.');
        }
    }
    
}