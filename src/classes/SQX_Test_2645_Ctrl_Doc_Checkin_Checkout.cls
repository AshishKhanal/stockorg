/**
* This test class checks checkin/checkout activities
*/
@isTest
public class SQX_Test_2645_Ctrl_Doc_Checkin_Checkout {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
   /**
    * When : when checked out a controlled document
    * Given: pass controlled document id
    * Then : update checkout information
    */
    testmethod static void WhenCheckoutDocument_GivenControlledDocumentId_ThenUpdateCheckOutInfo() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.save();
            
            // Act: submit to checkout
            SQX_Extension_Controlled_Doc_Checkout obj=new SQX_Extension_Controlled_Doc_Checkout(new ApexPages.StandardController(cDoc1.doc));
            obj.checkoutDocument();
            cDoc1.synchronize();
            
            // Assert: Ensure that checkout returned true and checkoutBy,checkedOutOn should not returned null
            System.assertEquals(true, cDoc1.doc.Checked_Out__c);
            System.assertNotEquals(null, cDoc1.doc.SQX_Checked_Out_By__c);
            System.assertNotEquals(null, cDoc1.doc.Checked_Out_On__c);
        }
    }
    
    /**
    * When : when checked in a controlled document
    * Given: pass controlled document id
    * Then : update checkin information
    */
    testmethod static void WhenCheckinDocument_GivenControlledDocumentId_ThenUpdateCheckinInfo() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.doc.Checked_Out__c=true;
            cDoc1.doc.SQX_Checked_Out_By__c=adminUser.Id;
            cDoc1.doc.Checked_Out_On__c=date.today();
            cDoc1.save();
            
            // Act: submit to checkin when KeepCheckedOut false
            SQX_Extension_Controlled_Doc_Checkout obj=new SQX_Extension_Controlled_Doc_Checkout(new ApexPages.StandardController(cDoc1.doc));
            obj.KeepCheckedOut = false;
            obj.checkinDocument();
            cDoc1.synchronize();
            
            // Assert: Ensure that checkout returned false and checkoutBy,checkedOutOn returned null
            System.assertEquals(false, cDoc1.doc.Checked_Out__c);
            System.assertEquals(null, cDoc1.doc.SQX_Checked_Out_By__c);
            System.assertEquals(null, cDoc1.doc.Checked_Out_On__c);
            
            // Arrange: Create controlled document
            SQX_Test_Controlled_Document cDoc2 = new SQX_Test_Controlled_Document();
            cDoc2.updateContent(true, 'Content-2.txt');
            cDoc2.doc.Document_Number__c = 'CDOC_2_NUMBER';
            cDoc2.doc.Checked_Out__c=true;
            cDoc2.doc.SQX_Checked_Out_By__c=adminUser.Id;
            cDoc2.doc.Checked_Out_On__c=date.today();
            cDoc2.save();
            
            // Act: submit to checkin when KeepCheckedOut true
            SQX_Extension_Controlled_Doc_Checkout obj1=new SQX_Extension_Controlled_Doc_Checkout(new ApexPages.StandardController(cDoc2.doc));
            obj1.KeepCheckedOut = true;
            obj1.checkinDocument();
            cDoc2.synchronize();
            
            // Assert: Ensure that checkout returned true and checkoutBy,checkedOutOn should not returned null
            System.assertEquals(true, cDoc2.doc.Checked_Out__c);
            System.assertNotEquals(null, cDoc2.doc.SQX_Checked_Out_By__c);
            System.assertNotEquals(null, cDoc2.doc.Checked_Out_On__c);
        }
    }
    
    /**
    * When : When undo checkout a controlled document
    * Given: pass controlled document id
    * Then : update undo checkout information
    */
    testmethod static void WhenUndoCheckoutDocument_GivenControlledDocumentId_ThenUpdateUndoCheckoutInfo() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.doc.Checked_Out__c=true;
            cDoc1.doc.SQX_Checked_Out_By__c=adminUser.Id;
            cDoc1.doc.Checked_Out_On__c=date.today();
            cDoc1.save();
            
            // Act: submit to undo checkout
            SQX_Extension_Controlled_Doc_Checkout obj=new SQX_Extension_Controlled_Doc_Checkout(new ApexPages.StandardController(cDoc1.doc));
            obj.undoCheckoutDocument();
            cDoc1.synchronize();
            
            // Assert: Ensure that checkout returned false and checkoutBy,checkedOutOn returned null
            System.assertEquals(false, cDoc1.doc.Checked_Out__c);
            System.assertEquals(null, cDoc1.doc.SQX_Checked_Out_By__c);
            System.assertEquals(null, cDoc1.doc.Checked_Out_On__c);
        }
    }
}