/**
* Batch processor for processing active PDJFs of deactivated document requirements
*/
global without sharing class SQX_Deactivated_Requirement_Processor implements Database.Batchable<sObject> {
    
    // Instance specific configurable variable for batch job name
    global String JOB_NAME = 'CQ-Deactivated Requirement Training Processor';
    // Instance specific configurable variable for batch size that can be processed
    global Integer BATCH_SIZE = 200;
    // Instance specific configurable variable for next schedule in minutes
    global Integer SCHEDULE_AFTER_MIN = 1;
    
    // this flag can be used in tests to disable rescheduling
    // for normal usage this value is always true, for tests it is set to false but can be changed
    global boolean RESCHEDULE = !Test.isRunningTest();
    
    /**
    * Returns the query that will fetch all the active PDJFs of deactivated document requirements to process
    */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // PDJFs with null PDTs are processed first to avoid training generations
        return Database.getQueryLocator([
            SELECT Id,
                SQX_Requirement__c,
                Controlled_Document_Id__c
            FROM SQX_Personnel_Document_Job_Function__c
            WHERE Is_Archived__c = false
                AND Active_Job_Function__c = false
                AND SQX_Requirement__r.Active__c = false
                AND SQX_Requirement__r.Training_Job_Status__c = :SQX_Requirement.TRAINING_JOB_STATUS_DEACTIVATION_PENDING
            ORDER BY SQX_Personnel_Document_Training__c NULLS FIRST, SQX_Requirement__c
        ]);
    }
    
    /**
    * batch method that processes batch items
    */
    global void execute(Database.BatchableContext bc, List<sObject> scope) {
        processBatch(scope);
    }
    
    /**
    * process batch items to process active PDJFs of deactivated requirements
    */
    public void processBatch(List<SQX_Personnel_Document_Job_Function__c> pdjfs) {
        SQX_Personnel_Document_Job_Function.processDeactivatedRequirementsUsingBatch(pdjfs);
    }
    
    /**
    * This method is called by the batch executor to perform cleanups. In case of CQ this method reschedules the batch job processing staging
    */
    global void finish(Database.BatchableContext bc) {
        // reschedule job
        if (RESCHEDULE) {
            SQX_Deactivated_Requirement_Processor processor = new SQX_Deactivated_Requirement_Processor();
            processor.JOB_NAME = JOB_NAME;
            processor.RESCHEDULE = RESCHEDULE;
            processor.SCHEDULE_AFTER_MIN = SCHEDULE_AFTER_MIN;
            
            System.scheduleBatch(processor, JOB_NAME, SCHEDULE_AFTER_MIN, BATCH_SIZE);
        }
    }
}