/*
 * test for Revision History and Referenced By Documents 
 * author: Anish Shrestha
 * date: 2015/06/12
 * story: SQX-1240
 **/
@isTest
public class SQX_Test_1240_Revision_History{

    static boolean runAllTests = false,
                    run_givenUser_WheRelatedDocIsAdded_ReferredDocShouldBeAdded = true,
                    run_givenUser_WhenControlledDocIsRevised_RevisionHistoryShouldBeAdded = true;
    /**
    * Helper methods to setup controlled doc
    */
    private static void setupControlledDoc(SQX_Controlled_Document__c doc){

        integer randomNumber = (Integer)( Math.random() * 1000000 );
        doc.Document_Number__c = 'Doc' + randomNumber;
        doc.Revision__c = 'REV-1';
        doc.Title__c = 'Document Title ' + randomNumber;
        doc.Date_Issued__c = Date.Today();
        doc.RecordTypeId = SQX_Controlled_Document.getControlledDocTypeId();
        doc.Document_Status__c = SQX_Controlled_Document.STATUS_DRAFT;
        doc.Effective_Date__c = Date.Today().addDays(15);
    }

    /*
    * helper method to insert related doc in the controlled doc
    */
    private static void setupRelatedDoc(Id docId1, Id docId2, Boolean specificRev){
        SQX_Related_Document__c relatedDoc = new SQX_Related_Document__c(
                                                    Controlled_Document__c = docId1,
                                                    Referenced_Document__c = docId2,
                                                    Specific_Rev__c = specificRev,
                                                    Comment__c = 'Random_Comments');
        new SQX_DB().op_insert(new List<SQX_Related_Document__c> { relatedDoc },  new List<Schema.SObjectField>{});
    }

    /**
    * ACTION: When related doc is added, revision history should be added
    * EXPECTED: Revision history to be added
    * @author Anish Shrestha
    * @date 2015/06/11
    * @story [SQX-1240]
    */ 
    public testMethod static void givenUser_WheRelatedDocIsAdded_ReferredDocShouldBeAdded(){

        if(!runAllTests && !run_givenUser_WheRelatedDocIsAdded_ReferredDocShouldBeAdded){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){ 
            /**
            * Arrange:
            * Setup two controlled doc
            */
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();

            SQX_Controlled_Document__c referredDoc = new SQX_Controlled_Document__c();
            ApexPages.StandardController referredcontroller = new ApexPages.StandardController(referredDoc);
            SQX_Extension_Controlled_Document referredextension = new SQX_Extension_Controlled_Document(referredcontroller);

            referredextension.file = Blob.valueOf('Hello Referred World');
            referredextension.fileName = 'HelloReferred.txt';
            setupControlledDoc(referredDoc);
            referredextension.create();

            /**
            * Act: refer one doc to another
            */
            System.assert(doc.Id != referredDoc.Id, 'Controlling Doc and Referenced Doc should not have same id but found controlling doc with id: ' + doc.Id + 'referring doc with id: '+ referredDoc.Id);
            setupRelatedDoc(doc.Id, referredDoc.Id, false);

            // Assert : Revised doc can be seen in revision history
            System.assert(referredextension.getReferringDocuments().size() == 1, 'Expected to have one related doc but found: ' + referredextension.getDocRevisions().size());

        }
    }

    /**
    * When the document is revised and related doc is added revision history and referred doc should be added
    * @author Anish Shrestha
    * @date 2015/06/11
    * @story [SQX-1240]
    */ 
    public testMethod static void givenUser_WhenControlledDocIsRevised_RevisionHistoryShouldBeAdded(){

        if(!runAllTests && !run_givenUser_WhenControlledDocIsRevised_RevisionHistoryShouldBeAdded ){
            return;
        }
        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        System.runas(standardUser){ 
            /**
            * Arrange:
            * Setup a controlled doc
            */
            Test.setCurrentPage(Page.SQX_Controlled_Document_Editor);
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
            ApexPages.StandardController controller = new ApexPages.StandardController(doc);
            SQX_Extension_Controlled_Document extension = new SQX_Extension_Controlled_Document(controller);

            extension.file = Blob.valueOf('Hello World');
            extension.fileName = 'Hello.txt';
            setupControlledDoc(doc);
            extension.create();

            // Make doc pre-release before revising otherwise there it will be mutlitple draft version of document and we have added duplicate rule for it.[SQX-3572]
            doc.Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            update doc;

            /**
             * Act: doc is revised and revised doc is updated so that its status become current 
             */
            extension.Revise();

            List<SQX_Controlled_Document__c> controlledDocs = new List<SQX_Controlled_Document__c>();
            controlledDocs = [SELECT Id, Document_Status__c, Effective_Date__c FROM SQX_Controlled_Document__c ORDER BY Revision__c DESC];
            
            controlledDocs[0].Effective_Date__c = Date.Today().addDays(15);
            controlledDocs[0].Document_Status__c = SQX_Controlled_Document.STATUS_PRE_RELEASE;
            update controlledDocs;

            // Assert: Revised doc appears in revision history of original doc
            System.assert(extension.getDocRevisions().size() == 1,'Expected to have one revision in revision history but found: ' + extension.getDocRevisions().size());

        }
    }

}