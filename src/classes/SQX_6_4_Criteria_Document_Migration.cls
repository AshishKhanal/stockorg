/**
* Batch processor to migrate Audit Criteria into Audit Criteria Document in the following setps:
*   1. All Audit Criteria that are not voided should be copied over to new document of record type Audit Criteria
*   2. Checklist should be moved instead of being coped over
*   3. Draft/Release library should be parameterized
*   4. All references to criteria have to be replaced with reference to Controlled Document
* Added in version  : 6.4.0
* Used for versions : less than 6.4
*/
global with sharing class SQX_6_4_Criteria_Document_Migration implements Database.Batchable<SObject>{
    
    global SQX_6_4_Criteria_Document_Migration(){}
    
    global String draftLibrary{get;set;}
    global String releaseLibrary{get;set;}
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator([SELECT Id,
                                                OwnerId,
                                                Checklist_Summary_Scope__c,
                                                Checklist_Title__c,
                                                Checklist_Type__c,
                                                Criteria_No__c,
                                                Is_Locked__c,
                                                Revision__c, 
                                                Status__c
                                                FROM SQX_Audit_Criteria__c WHERE Status__c !=: SQX_Audit_Criteria.STATUS_OBSOLETED 
                                                                                AND SQX_Controlled_Document__c = null]);
    }

    global void execute(Database.BatchableContext bc, List<SObject> objectList){
        List<SQX_Audit_Criteria__c> allOldAuditCriterion = (List<SQX_Audit_Criteria__c>)objectList;
        
        //This list will hold all the doc criterion requirements that are equivalent to criterion requirements
        List<SQX_Doc_Criterion_Requirement__c> allDocCriteriaRequirements = new List<SQX_Doc_Criterion_Requirement__c>();

        //This map hold the relationship between audit criteria and its equivalent controlled doc.
        Map<Id,Id> auditCriteria2CriteriaDoc = new Map<Id,Id>();

        Set<Id> currnetDocsId = new Set<Id>();//This set will hold ids of currnet migrated docs
        
        /*
        * All audit Criteria is being copied to new Controlled Doc of record type audit criteria
        */
        if(allOldAuditCriterion.size() > 0){
                       
            for(SQX_Audit_Criteria__c auditCriteria : allOldAuditCriterion){
                SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c();
                doc.RecordTypeId = SQX_Utilities.getRecordTypeIDByDevelopernameFor(SQX.ControlledDoc, SQX_Controlled_Document.RT_AUDIT_CRITERIA_API_NAME);
                doc.Document_Number__c = auditCriteria.Criteria_No__c;
                doc.Document_Category__c = auditCriteria.Checklist_Type__c;
                doc.Revision__c = auditCriteria.Revision__c;
                doc.Description__c = auditCriteria.Checklist_Summary_Scope__c;
                doc.Title__c = auditCriteria.Checklist_Title__c;
                doc.Draft_Vault__c = draftLibrary;
                doc.Release_Vault__c = releaseLibrary;
                doc.OwnerId = auditCriteria.OwnerId;
                

                //if approval staus is rejected then make the approval status of doc to be rejected 
                if(auditCriteria.Status__c == SQX_Audit_Criteria.STATUS_REJECTED){
                    doc.Approval_Status__c = SQX_Controlled_Document.APPROVAL_REJECTED;
                }

                //Note : if the audit criteria is in approval then its equivalent doc will be in draft status which is bydefault for new doc

                //Saving doc using builder which will add the default content for the created doc
                SQX_Controlled_Document_Builder builder = new SQX_Controlled_Document_Builder(doc);
                Id docId = builder.save();
                System.assert(docId != null, 'Null controlled document Id found while saving new record.' + builder.lastSaveResult);
                
                auditCriteria2CriteriaDoc.put(auditCriteria.Id, docId);
                auditCriteria.SQX_Controlled_Document__c = docId;//Add doc link to audit criteria. This is usefull for getting relation between doc and criteria

                //if status is active then store the Ids of the Doc so that it can be updated latter in to Currnet and Approved
                if(auditCriteria.Status__c == SQX_Audit_Criteria.STATUS_ACTIVE){ 
                    currnetDocsId.add(docId);
                }
            }

            //update old audit criterias with linked controlled document
            new SQX_DB().op_update(allOldAuditCriterion, new List<SObjectField>{SQX_Audit_Criteria__c.SQX_Controlled_Document__c});
        
            //get all the criterion requirements to copy it into equivalent doc criterion requirements
            List<SQX_Criterion_Requirement__c> auditCriterionRequirements = [SELECT Id,
                                                                                    SQX_Audit_Criteria__c, 
                                                                                    Objective__c, 
                                                                                    SQX_Result_Type__c, 
                                                                                    Section__c, Standard__c, 
                                                                                    Sub_Section__c, 
                                                                                    Topic__c 
                                                                                FROM SQX_Criterion_Requirement__c
                                                                                WHERE SQX_Audit_Criteria__c IN: allOldAuditCriterion];
            
            /*
            * Copy criterion requirements to doc criterion requirements
            */
            if(auditCriterionRequirements.size() > 0){
                for(SQX_Criterion_Requirement__c auditCriteriaRequirement : auditCriterionRequirements){

                    SQX_Doc_Criterion_Requirement__c allDocCriteriaRequirement = new SQX_Doc_Criterion_Requirement__c();

                    allDocCriteriaRequirement.Objective__c = auditCriteriaRequirement.Objective__c;
                    allDocCriteriaRequirement.SQX_Result_Type__c = auditCriteriaRequirement.SQX_Result_Type__c;
                    allDocCriteriaRequirement.Section__c = auditCriteriaRequirement.Section__c;
                    allDocCriteriaRequirement.Standard__c = auditCriteriaRequirement.Standard__c;
                    allDocCriteriaRequirement.Sub_Section__c = auditCriteriaRequirement.Sub_Section__c;
                    allDocCriteriaRequirement.Topic__c = auditCriteriaRequirement.Topic__c;
                    allDocCriteriaRequirement.SQX_Controlled_Document__c = auditCriteria2CriteriaDoc.get(auditCriteriaRequirement.SQX_Audit_Criteria__c);
                  
                    allDocCriteriaRequirements.add(allDocCriteriaRequirement);
                }
                
                //insert the doc criterion requirements
                new SQX_DB().op_insert(allDocCriteriaRequirements, new List<SObjectField> {
                    SQX_Doc_Criterion_Requirement__c.SQX_Controlled_Document__c,
                    SQX_Doc_Criterion_Requirement__c.Objective__c,
                    SQX_Doc_Criterion_Requirement__c.SQX_Result_Type__c,
                    SQX_Doc_Criterion_Requirement__c.Section__c,
                    SQX_Doc_Criterion_Requirement__c.Standard__c,
                    SQX_Doc_Criterion_Requirement__c.Sub_Section__c,
                    SQX_Doc_Criterion_Requirement__c.Topic__c});
            }
        
            /*
            * Update checklist that consisting Doc criterion requirements
            */
            List<SQX_Audit_Checklist__c> audit_Checklists = [SELECT  Id,
                                                                    SQX_Audit__c,
                                                                    SQX_Audit_Criterion__c,
                                                                    SQX_Controlled_Document__c,
                                                                    Objective__c,
                                                                    Rationale__c,
                                                                    SQX_Result_Value__c,
                                                                    SQX_Result_Type__c,
                                                                    Section__c,
                                                                    Standard__c,
                                                                    Sub_Section__c,
                                                                    Target_Intiailized__c,
                                                                    Topic__c
                                                                FROM SQX_Audit_Checklist__c
                                                                WHERE SQX_Controlled_Document__c = null];
            
            if(audit_Checklists.size() > 0){
                for(SQX_Audit_Checklist__c audit_Checklist :  audit_Checklists){

                    //we need to add a custom field in audit checklist lookup to SQX_Controlled_Document__c but this field is usefull only for migration
                    //since we now have doc criteria requirements in checklist so we don't need controlled doc further in real scenarios
                    if(auditCriteria2CriteriaDoc.containsKey(audit_Checklist.SQX_Audit_Criterion__c)){
                        audit_Checklist.SQX_Controlled_Document__c = auditCriteria2CriteriaDoc.get(audit_Checklist.SQX_Audit_Criterion__c);
                    }
                }

                //update checklist. while updating checklist validaion rule will be fired where we used the custom permission to bypass the validation rule for migration.
                new SQX_DB().op_update(audit_Checklists, new List<SObjectField> {SQX_Audit_Checklist__c.SQX_Controlled_Document__c});
            }

            /*
            * Now change reference of audit criteria to controlled doc in Audit Program Scope
            * Since only active audit criteria is there in program socpe so we only have current controlled doc which satisfies the validation rule
            * But since the validation rules says that we can only modify the record if the status of program socop is draft,So we needed to disable the validation rule for migration
            */
            List<SQX_Audit_Program_Scope__c> programScopes = [SELECT  Id, 
                                                                      SQX_Audit_Criteria__c, 
                                                                      SQX_Audit_Criteria_Document__c, 
                                                                      SQX_Audit_Program__c 
                                                                FROM  SQX_Audit_Program_Scope__c
                                                                WHERE SQX_Audit_Criteria__c IN: allOldAuditCriterion];

            if(programScopes.size() > 0){
                for(SQX_Audit_Program_Scope__c programScope : programScopes){
                    programScope.SQX_Audit_Criteria_Document__c = auditCriteria2CriteriaDoc.get(programScope.SQX_Audit_Criteria__c);
                }
                new SQX_DB().op_update(programScopes, new List<SObjectField> {SQX_Audit_Program_Scope__c.SQX_Audit_Criteria_Document__c});
            }
        }
        
        //update the ContentDocument so that it should also have the same original owner of audit criteria
        List<ContentDocument> contents = new List<ContentDocument>();
        List<SQX_Controlled_Document__c> docs = [SELECT Id, OwnerId, Is_Locked__c, Document_Status__c, Content_Reference__c, Release_Vault__c FROM SQX_Controlled_Document__c WHERE Id IN: auditCriteria2CriteriaDoc.values()];
        for(SQX_Controlled_Document__c d : docs){
            ContentDocument content = new ContentDocument();
            content.Id = d.Content_Reference__c;
            content.OwnerId = d.OwnerId;
            contents.add(content);
        }
        //update Content of Document
        new SQX_DB().op_update(contents, new List<SObjectField>{ });

        //this list only cotains current doc
        List<SQX_Controlled_Document__c> currentDocs = new List<SQX_Controlled_Document__c>();
        for(SQX_Controlled_Document__c d :  docs){
            if(currnetDocsId.contains(d.Id)){
                d.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
                d.Approval_Status__c = SQX_Controlled_Document.STATUS_APPROVED;
                d.Is_Locked__c = true;//lock the docs which is current
            }
        }
        
        //Update the migrated doc
        new SQX_DB().op_update(docs, new List<SObjectField>{SQX_Controlled_Document__c.Is_Locked__c });
        
    }
    global void finish(Database.BatchableContext bc){

    }
}