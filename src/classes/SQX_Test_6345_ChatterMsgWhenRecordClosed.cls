/*
 * Unit test to check if FeedItem is posted on chatter or not under supplier deviation record
 * when record is rejected and closed.
 */
@isTest
public class SQX_Test_6345_ChatterMsgWhenRecordClosed {
    final static String chatterMsg = 'Record has been closed and rejected.';
    final static String closeAction = 'Closing the record';
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User assigneeUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'assigneeUser');
        SQX_Test_Supplier_Deviation.addUserToQueue(new List<User> {  adminUser, standardUser, assigneeUser});

    }
    
    /*
     * Given: A Supplier Deviation Record
     * When: Any Supplier Deviation record is rejected and closed
     * Then: a chatter notification is sent with closure_comment as the body of the message.
     *       and closure_comment is copied comment_long of record activity
     */
    @isTest
    public static void givenSDRecord_WhenRecordIsClosedRejected_ChatterMsgIsSent() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        User standardUser = SQX_Test_Account_Factory.getUsers().get('standardUser');
        User assigneeUser = SQX_Test_Account_Factory.getUsers().get('assigneeUser');

        SQX_Test_Supplier_Deviation sdRecord;
        System.runAs(adminUser) {
            SQX_Test_Supplier_Deviation.createPolicyTasks(1, SQX_Supplier_Deviation.TASK_TYPE_TASK, assigneeUser, 1);
        }
        
        System.runAs(standardUser) {
            //Act: SD Record initiated and ownership taken
            sdRecord = new SQX_Test_Supplier_Deviation().save();
            sdRecord.submit();
            sdRecord.initiate();
            
            //Assert: Make sure that SD record is in 'Inprogress' Stage
            System.assertEquals(SQX_Supplier_Deviation.STAGE_IN_PROGRESS, [Select record_stage__c from sqx_supplier_deviation__c where id = :sdRecord.sd.id].record_stage__c);
        }
        
        System.runAs(assigneeUser) {
            //Arrange: Get salesforce task
            List<Task> sfTasks = [SELECT Id FROM Task WHERE WhatId =: sdRecord.sd.Id];
            Task sfTask = sfTasks.get(0);
            
            //Act: Complete the task
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(sfTask.Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            
            SQX_Supplier_Deviation__c suppRecord = [Select id, record_stage__c, status__c from sqx_supplier_deviation__c];
            System.assertEquals(SQX_Supplier_Deviation.STATUS_COMPLETE, suppRecord.status__c, 'Expected sdRecord to have complete status.');
            System.assertEquals(SQX_Supplier_Deviation.STAGE_VERIFICATION, suppRecord.record_stage__c, 'Expected sdRecord to have verification stage.');
            
        }
        System.runAs(standardUser) {
            SQX_BulkifiedBase.clearAllProcessedEntities();
            sdRecord.sd.result__c = SQX_Supplier_Deviation.SD_RESULT_REJECTED;
            sdRecord.sd.closure_comment__c = 'Record has been closed and rejected.';
            sdRecord.close();
            
            //Assert: Make sure record activity has the comment, 'Record has been closed and rejected.'
            SQX_Supplier_Deviation_Record_Activity__c activitiesRecorded = [SELECT Id, activity__c, comment_long__c FROM SQX_Supplier_Deviation_Record_Activity__c WHERE SQX_Supplier_Deviation__c =: sdRecord.sd.Id and Activity__c = :closeAction ORDER BY CreatedDate ASC];
            System.assertEquals(sdRecord.sd.closure_comment__c, activitiesRecorded.comment_long__c);
        }
        
         //Assert: Make sure that feeditem with body 'Record has been closed and rejected.' is created.    
        FeedItem feed = [SELECT parentid, body FROM FeedItem WHERE parentId = :sdRecord.sd.id AND type = 'TextPost' ORDER BY createdDate DESC NULLS LAST];
        System.assertEquals(true, feed != null, 'Expected, a feedMessage to be present in chatter');
        System.assertEquals(true, feed.body.contains(chatterMsg));
        
    }
    
    /*
     * Given: NSI Record
     * When: NSI Record is closed with closure comment
     * Then: Closure comment is copied in record activity
     */
    @isTest
    public static void givenNSIRecord_WhenRecordIsClosedWithClosureComment_ThenCommentIsCopiedInRecordActivity() {
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');

        SQX_Test_NSI nsiRecord;
       
        System.runAs(adminUser) {
            //Arrange: NSI Record submitted
            nsiRecord = new SQX_Test_NSI().save();
            nsiRecord.submit();
            nsiRecord.initiate();
            //Act: Close the record
            nsiRecord.nsi.result__c = SQX_Supplier_Deviation.SD_RESULT_REJECTED;
            nsiRecord.nsi.closure_comment__c = 'NSI Record has been closed and rejected.';
            nsiRecord.close();
            
            //Assert: Make sure record activity has the comment, 'Record has been closed and rejected.'
            SQX_NSI_Record_Activity__c activitiesRecorded = [SELECT Id, activity__c, comment_long__c FROM SQX_NSI_Record_Activity__c WHERE SQX_New_Supplier_Introduction__c =: nsiRecord.nsi.Id and Activity__c = :closeAction ORDER BY CreatedDate ASC];
            System.assertEquals(nsiRecord.nsi.closure_comment__c, activitiesRecorded.comment_long__c);
        }
        
        
        
    }
}