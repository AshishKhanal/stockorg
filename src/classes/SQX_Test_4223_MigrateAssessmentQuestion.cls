@IsTest
public class SQX_Test_4223_MigrateAssessmentQuestion {
	
    @testSetup
    public static void commonSetup() {
        //Arrange: Create users.
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
    }
    
    /**
    * Helper funtion to setup Assessment and Assessment Question.
    * @return List<Id> is the list of Ids of Assessment Questions created. 
    **/ 
    public static List<Id> arrangeAssessmentQuestion() {
        /**
         * Arrange Assessment as follows:
         * assessment1 : Assessment with Status Draft
         * assessment2 : Assessment with Status Pre-Release
         * assessment3 : Assessment with Status Current
         * assessment4 : Assessment with Status Pre-Expire
         * assessment5 : Assessment with Status Obsolete
         */
        
        SQX_Test_Assessment assessment1 = new SQX_Test_Assessment();
		assessment1.save();
        Id assessmentQuestionId1 = createAssessmentQuestions(assessment1.assessment.Id);
        
        SQX_Test_Assessment assessment2 = new SQX_Test_Assessment();
        assessment2.assessment.Status__c = SQX_Assessment.STATUS_PRE_RELEASE;
        assessment2.save();
        Id assessmentQuestionId2 = createAssessmentQuestions(assessment2.assessment.Id);
        
        SQX_Test_Assessment assessment3 = new SQX_Test_Assessment();
        assessment3.assessment.Status__c = SQX_Assessment.STATUS_CURRENT;
        assessment3.save();
        Id assessmentQuestionId3 = createAssessmentQuestions(assessment3.assessment.Id);
        
        SQX_Test_Assessment assessment4 = new SQX_Test_Assessment();
        assessment4.assessment.Status__c = SQX_Assessment.STATUS_PRE_EXPIRE;
        assessment4.save();
        Id assessmentQuestionId4 = createAssessmentQuestions(assessment4.assessment.Id);
        
        SQX_Test_Assessment assessment5 = new SQX_Test_Assessment();
        assessment5.assessment.Status__c = SQX_Assessment.STATUS_OBSOLETE;
        assessment5.save();
        Id assessmentQuestionId5 = createAssessmentQuestions(assessment5.assessment.Id);
        
        return new List<Id>{
            assessmentQuestionId1,
            assessmentQuestionId2,
			assessmentQuestionId3,
            assessmentQuestionId4,
            assessmentQuestionId5
        };
    }
    
    /**
    * Helper funtion to create Assessment Question.
	* @param {assessmentId} is Id of Assessment to which Assessment Question is to be linked. 
	* @return {Id} of Assessment Question created.
    **/ 
    public static Id createAssessmentQuestions(Id assessmentId) {
        SQX_Assessment_Question__c question = new SQX_Assessment_Question__c();
        question.compliancequest__SQX_Assessment__c = assessmentId;
        question.compliancequest__Question_Number__c = 1;
        question.compliancequest__Question__c = 'Are you a developer ?';
        question.compliancequest__Question_Long__c = 'Are you a developer ?';
        question.compliancequest__Answer_Options__c = 'A)Yes\r\nB)No\r\nC)Not Sure\r\nD)Who Cares';
        question.compliancequest__Answer_Options_Long__c = 'A)Yes\r\nB)No\r\nC)Not Sure\r\nD)Who Cares';
        question.compliancequest__Correct_Answer__c = 'A';
        question.compliancequest__Navigation__c = 'A=2\r\nB=3\r\nC=4\r\nD=Next';
        question.compliancequest__Type_In_Answer__c = false;
        
        new SQX_DB().op_insert(new List<SQX_Assessment_Question__c> {question}, new List<Schema.SObjectField>{});
        
        return question.Id;
    }
    
    /**
    * Given: Assessment Question linked with Assessment with different status.
    * When:  SQX_7_4_Migrate_Assessment_Question runs.
    * Then:  Question is copied to Question Long and Answer Options is copied to Answer Options Long.
    * 
    * @author: Sanjay Maharjan
    * @date: 1/18/2018
    * @story: [SQX-4223]
    */
    public testmethod static void whenSQX_7_4_Migrate_Assessment_QuestionClassRuns_ThenQuestionIsCopiedToQuestionLongAndAnswerOptionsIsCopiedToAnswerOptionsLong() {
        
        User adminUser = SQX_Test_Account_Factory.getUsers().get('adminUser');
        
        System.runAs(adminUser) {
            
            //Give custom permission set of data migration to admin user.
            SQX_Test_Account_Factory.addPermissionToPermissionSet(SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, SQX_Test_Account_Factory.DATA_MIGRATION_CUSTOM_PERMISSION_NAME);
            
            //Arrange: Create different Assessment Question link with Assessment with different status.
            List<Id> assessmentQuestionIds = arrangeAssessmentQuestion();
            
            //Act: Run SQX_7_4_Migrate_Assessment_Question batch class.
            Test.startTest();
            SQX_7_4_Migrate_Assessment_Question job = new SQX_7_4_Migrate_Assessment_Question();
            ID batchProcessId = Database.executeBatch(job);
            Test.stopTest();
            
            //Assert: Question is copied to Question Long and Answer Options is copied to Answer Options Long.
            for( SQX_Assessment_Question__c assessmentQuestion : [SELECT Id, Question__c, Question_Long__c, Answer_Options__c, Answer_Options_Long__c
                                                                  FROM SQX_Assessment_Question__c WHERE Id IN: assessmentQuestionIds ]){
            	System.assertEquals(assessmentQuestion.compliancequest__Question__c, assessmentQuestion.compliancequest__Question_Long__c, 'Question and Question Long are not same.');
				System.assertEquals(assessmentQuestion.compliancequest__Answer_Options__c, assessmentQuestion.compliancequest__Answer_Options_Long__c, 'Answer Options and Answer Options Long are not same.');
			}
        }
    }
    
}