/**
* This class will contain the constants and static strings required by Consolidated Homepage component
* @story   SQX-3159 Homepage UI and Framework Implementation
* @author  Anuj Bhandari
* @version 1.0
* @date    2017-06-03
*/
public with sharing class SQX_Homepage_Constants {

    public static final String SF_BASE_URL = SQX_Utilities.getBaseUrlForUser();

    /* Short Description field length */
    public static final Integer SHORT_DESC_MAX_CHARACTERS = SObjectType.Task.Fields.Subject.Length;

    public static final SQX_Homepage_Filter.Source DEFAULT_SOURCE_TYPE = SQX_Homepage_Filter.Source.User;

    public static final Integer DEFAULT_PAGE_SIZE = 100;

    public static final SQX_Homepage_Filter.SortOrder DEFAULT_SORT_ORDER = SQX_Homepage_Filter.SortOrder.DESCENDING;

    public static final String  ORDERIND_FIELD_CREATED_DATE = 'createdDate',
                                ORDERING_FIELD_DUE_DATE = 'dueDate';

    public static final String  ACTION_APPROVE_REJECT = Label.CQ_UI_Homepage_Action_Approve_Reject,
                                ACTION_RESPOND = Label.CQ_UI_Homepage_Action_Respond,
                                ACTION_VIEW = Label.CQ_UI_Homepage_Action_View,
                                ACTION_VIEW_CONTENT =  Label.CQ_UI_View_Content,
                                ACTION_CLOSE = Label.CQ_UI_Homepage_Action_Close,
                                ACTION_INITIATE = Label.CQ_UI_Homepage_Action_Initiate,
                                ACTION_USER_SIGNOFF = Label.CQ_UI_Homepage_Action_User_Signoff,
                                ACTION_TRAINER_SIGNOFF = Label.CQ_UI_Homepage_Action_Trainer_Signoff,
                                ACTION_TAKE_ASSESSMENT = Label.CQ_UI_Homepage_Action_Take_Assessment,
                                ACTION_LAUNCH_CONTENT = Label.CQ_UI_Homepage_Action_Launch_Content,
                                ACTION_REASSIGN = Label.CQ_UI_Homepage_Action_Reassign;

    public static final String  ACTION_TYPE_NEEDING_RESPONSE = Label.CQ_UI_Homepage_Action_Type_Needing_Response,
                                ACTION_TYPE_ACTION_TO_COMPLETE = Label.CQ_UI_Homepage_Action_Type_Action_To_Complete,
                                ACTION_TYPE_OPEN_RECORDS = Label.CQ_UI_Homepage_ActionFilter_Open_Records,
                                ACTION_TYPE_DRAFT_ITEMS = Label.CQ_UI_Homepage_ActionFilter_Draft_Items,
                                ACTION_TYPE_NEEDS_ATTENTION = Label.CQ_UI_Homepage_ActionFilter_Needs_Attention,
                                ACTION_TYPE_NEEDS_REVIEW = Label.CQ_UI_Homepage_ActionFilter_Needs_Review,
                                ACTION_TYPE_ITEMS_TO_APPROVE = Label.CQ_UI_Homepage_ActionFilter_Items_To_Approve,
                                ACTION_TYPE_ACTION_TO_CLOSE = Label.CQ_UI_Homepage_Action_Type_Action_To_Close;

    public static final String  MODULE_TYPE_CAPA = SObjectType.SQX_CAPA__c.getLabel(),
                                MODULE_TYPE_AUDIT = SObjectType.SQX_Audit__c.getLabel(),
                                MODULE_TYPE_NC = SObjectType.SQX_Nonconformance__c.getLabel(),
                                MODULE_TYPE_FINDING = SObjectType.SQX_Finding__c.getLabel(),
                                MODULE_TYPE_COMPLAINT = SObjectType.SQX_Complaint__c.getLabel(),
                                MODULE_TYPE_DOCUMENT_TRAINING = SObjectType.SQX_Personnel_Document_Training__c.getLabel(),
                                MODULE_TYPE_DOCUMENT_MANAGEMENT =  SObjectType.SQX_Controlled_Document__c.getLabel(),
                                MODULE_TYPE_CHANGE_ORDER = SObjectType.SQX_Change_Order__c.getLabel(),
                                MODULE_TYPE_INSPECTION = SObjectType.SQX_Inspection__c.getLabel(),
                                MODULE_TYPE_SUPPLIER_MANAGEMENT = Label.CQ_UI_Supplier;

    public static final String  FEED_TEMPLATE_RESPOND = Label.CQ_UI_Homepage_FeedText_Respond,
                                FEED_TEMPLATE_RESPOND_WITHOUT_TITLE = Label.CQ_UI_Homepage_FeedText_Respond_Without_Title,
                                FEED_TEMPLATE_OPEN_ACTION_WITH_SUBJECT = Label.CQ_UI_Homepage_FeedText_Open_Action_With_Subject,
                                FEED_TEMPLATE_OPEN_ACTION_WITHOUT_SUBJECT = Label.CQ_UI_Homepage_FeedText_Open_Action_Without_Subject,
                                FEED_TEMPLATE_OPEN_RECORDS = Label.CQ_UI_Homepage_FeedText_Open_Records,
                                FEED_TEMPLATE_OPEN_RECORDS_WITHOUT_TITLE = Label.CQ_UI_Homepage_FeedText_Open_Records_Without_Title,
                                FEED_TEMPLATE_TRIAGE_ITEMS = Label.CQ_UI_Homepage_FeedText_Triage_Items,
                                FEED_TEMPLATE_DRAFT_ITEMS = Label.CQ_UI_Homepage_FeedText_Draft_Items,
                                FEED_TEMPLATE_DRAFT_ITEMS_WITHOUT_TITLE = Label.CQ_UI_Homepage_FeedText_Draft_Items_Without_Title,
                                FEED_TEMPLATE_COMPLETED_ITEMS = Label.CQ_UI_Homepage_FeedText_Completed_Items,
                                FEED_TEMPLATE_COMPLETED_ITEMS_WITHOUT_TITLE = Label.CQ_UI_Homepage_FeedText_Completed_Items_Without_Title,
                                FEED_TEMPLATE_REJECTED_ITEMS =  Label.CQ_UI_Homepage_FeedText_Rejected_Items,
                                FEED_TEMPLATE_TRAINING_SIGNOFF_USER = Label.CQ_UI_Homepage_Feedtext_User_Signoff,
                                FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER = Label.CQ_UI_Homepage_Feedtext_Trainer_Signoff,
                                FEED_TEMPLATE_TRAINING_SIGNOFF_USER_REFRESHER = Label.CQ_UI_Homepage_Feedtext_User_Signoff_Refresher,
                                FEED_TEMPLATE_TRAINING_SIGNOFF_TRAINER_REFRESHER = Label.CQ_UI_Homepage_Feedtext_Trainer_Signoff_Refresher,
                                FEED_TEMPLATE_DOCUMENT_MANAGEMENT_DRAFT = Label.CQ_UI_Homepage_Feedtext_Doc_Mgmt_Draft,
                                FEED_TEMPLATE_DOCUMENT_MANAGEMENT_APPROVED = Label.CQ_UI_Homepage_Feedtext_Doc_Mgmt_Approved,
                                FEED_TEMPLATE_DOCUMENT_MANAGEMENT_REJECTED = Label.CQ_UI_Homepage_Feedtext_Doc_Mgmt_Rejected,
                                FEED_TEMPLATE_CHANGE_ORDER_COMPLETED = Label.CQ_UI_Homepage_FeedText_ChangeOrder_Completed_Items,
                                FEED_TEMPLATE_ITEMS_TO_APPROVE_WITH_TITLE = Label.CQ_UI_Homepage_FeedText_Items_To_Approve,
                                FEED_TEMPLATE_ITEMS_TO_APPROVE_WITHOUT_TITLE = Label.CQ_UI_Homepage_FeedText_Items_To_Approve_Without_Title;


    public static final String  ICON_UTILITY_REPLY = 'utility:reply',
                                ICON_UTILITY_VIEW = 'utility:preview',
                                ICON_UTILITY_QA = 'utility:questions_and_answers',
                                ICON_UTILITY_PEOPLE = 'utility:people',
                                ICON_UTILITY_CLOSE = 'utility:close',
                                ICON_UTILITY_INITIATE = 'utility:change_owner',
                                ICON_UTILITY_POWER = 'utility:power',
                                ICON_UTILITY_APPROVE_REJECT = 'utility:edit_form',
                                ICON_UTILITY_CREATE_RECORD = 'utility:record_create',
                                ICON_UTILITY_TOUCH_ACTION = 'utility:touch_action',
                                ICON_UTILITY_REASSIGN = 'utility:retweet';

    /*
        Holds a map of action types and their corresponding icons to be presented in the UI
    */
    public static final Map<String, String> ACTION_ICON_MAP = new Map<String, String>
    {
        ACTION_TYPE_NEEDING_RESPONSE => 'standard:assigned_resource',
        ACTION_TYPE_ACTION_TO_COMPLETE => 'standard:task',
        ACTION_TYPE_NEEDS_REVIEW => 'standard:answer_public',
        ACTION_TYPE_ITEMS_TO_APPROVE => 'standard:work_order_item',
        ACTION_TYPE_NEEDS_ATTENTION => 'standard:coaching',
        ACTION_TYPE_DRAFT_ITEMS => 'standard:drafts',
        ACTION_TYPE_OPEN_RECORDS => 'standard:article',
        ACTION_TYPE_ACTION_TO_CLOSE => 'standard:portal'
    };

    public static final Map<String, String> MODULE_ICON_MAP = new Map<String, String>
    {
        MODULE_TYPE_CAPA => 'capa.png',
        MODULE_TYPE_AUDIT => 'audit.png',
        MODULE_TYPE_NC => 'nc.png',
        MODULE_TYPE_FINDING => 'finding.png',
        MODULE_TYPE_COMPLAINT => 'complaint.png',
        MODULE_TYPE_DOCUMENT_TRAINING => 'training.png',
        MODULE_TYPE_DOCUMENT_MANAGEMENT =>  'document.png',
        MODULE_TYPE_CHANGE_ORDER => 'change.png',
        MODULE_TYPE_INSPECTION => 'inspection.png',
        MODULE_TYPE_SUPPLIER_MANAGEMENT => 'supplier.png'
    };
}
