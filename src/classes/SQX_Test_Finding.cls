/**
* This is a helper class that provides primitive functionality to setup and test finding 
* @author Pradhanta Bhandari
* @date 2014/2/18
*/
@IsTest
public class SQX_Test_Finding extends SQX_Test_Base_Class{
    public enum MockObjectType{
        CAPAType, AuditType, NCType, ComplaintType
    }

    public SQX_Finding__c finding;
    public Account supplier;
    public Contact primaryContact;
    public User  primaryContactUser;

    /**
    * Default constructor to create finding
    */
    public SQX_Test_Finding(){
        finding = new SQX_Finding__c();
        finding.Response_Required__c = false;
        finding.compliancequest__Containment_Required__c = false;
        finding.compliancequest__Investigation_Required__c = false;
        finding.compliancequest__Corrective_Action_Required__c = false;
        finding.compliancequest__Preventive_Action_Required__c = false;
        finding.compliancequest__CAPA_Required__c = false;
        finding.RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName =: SQX_Finding.ISSUE_RECORD_TYPE_DEVELOPER_NAME AND SObjectType =: SQX.Finding].Id;
    }


    /**
    * Initializes a finding of given type
    * @param objectType the type of finding that is to be created.
    * @param associateUsers if true creates accounts, contacts and user necessary for the finding, else creates none
    */
    public SQX_Test_Finding(MockObjectType objectType, boolean associateUsers){
        Account supplier = null;
        Contact primaryContact = null;
        User primaryContactUser = null;
        
        if(associateUsers){
            
            supplier = SQX_Test_Account_Factory.createAccount();
            primaryContact = SQX_Test_Account_Factory.createContact(supplier);
            primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
        }
        
        initializeFinding(objectType, supplier, primaryContact, primaryContactUser);
    }
    
    
    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description creates a finding object of given type, creates a new supplier account, contact and user as required
    */
    public SQX_Test_Finding(MockObjectType objectType){
        Account supplier =  SQX_Test_Account_Factory.createAccount();
        Contact primaryContact = SQX_Test_Account_Factory.createContact(supplier);
        User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
        
        initializeFinding(objectType, supplier, primaryContact, primaryContactUser);
    }
    
    private void initializeFinding(MockObjectType objectType, Account supplier, Contact primaryContact, User primaryContactUser){
        this.supplier = supplier;
        this.primaryContact = primaryContact;
        this.primaryContactUser = primaryContactUser;

        this.finding = prepareMockObject(objectType, this.supplier, this.primaryContact);
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description creates a finding object for testing of given type for give supplier, contact and user
    */
    public SQX_Test_Finding(MockObjectType objectType, Account supplier, Contact primaryContact, User primaryContactUser){
        initializeFinding(objectType, supplier, primaryContact, primaryContactUser);
    }

    /**
    * @author Pradhanta Bhandari
    * @date  2014/2/18
    * @description this method sets the approval flags.
    * @param investigationApproval true if investigation approval is required
    * @param changesInAPApproval true if changes in Action Plan requires approval
    * @param effectivenessReview true if effectiveness review is required
    */
    public SQX_Test_Finding setApprovals(boolean investigationApproval, boolean changesInAPApproval, boolean effectivenessReview){
        this.finding.Investigation_Approval__c = investigationApproval;
        //this.finding.Implementation_Approval__c = changesInAPApproval;
        /*this.finding.Effectiveness_Review__c = effectivenessReview;*/

        return this;
    }

    /**
    * @author Pradhanta Bhandari
    * @date  2014/2/18
    * @description this method sets the required flags.
    * @param responseRequired true if response is required, which is almost always true
    * @param containmentRequired true if contaiment is required
    * @param investigationRequired true if investigation is required
    * @param correctiveActionRequired true if corrective action is required
    * @param preventiveActionRequired true if preventive action is required
    */
    public SQX_Test_Finding setRequired(boolean responseRequired, boolean containmentRequired, boolean investigationRequired,
            boolean correctiveActionRequired, boolean preventiveActionRequired){

        return setRequired( responseRequired, containmentRequired, investigationRequired, correctiveActionRequired, preventiveActionRequired, false);
        
    }

    /**
    * @author Sudeep Maharjan
    * @date  2014/2/18
    * @description this method sets the required flags.(Method Overloaded)
    * @param responseRequired true if response is required, which is almost always true
    * @param containmentRequired true if contaiment is required
    * @param investigationRequired true if investigation is required
    * @param correctiveActionRequired true if corrective action is required
    * @param preventiveActionRequired true if preventive action is required
    * @param capaRequired true if CAPA is required
    */
    public SQX_Test_Finding setRequired(boolean responseRequired, boolean containmentRequired, boolean investigationRequired,
            boolean correctiveActionRequired, boolean preventiveActionRequired, boolean capaRequired){
        
        this.finding.Response_Required__c = responseRequired;
        this.finding.Containment_Required__c = containmentRequired;
        this.finding.Investigation_Required__c = investigationRequired;
        this.finding.Corrective_Action_Required__c = correctiveActionRequired;
        this.finding.Preventive_Action_Required__c = preventiveActionRequired;
        this.finding.CAPA_Required__c = capaRequired;

        //2014-4-28 [Pradhanta Bhandari] : added to prevent test written before SQX-333 implementation, from failing
        this.finding.Due_Date_Response__c = responseRequired  ? Date.Today() : null; //set today's date as due date by default
        this.finding.Due_Date_Containment__c = responseRequired  ? Date.Today() : null; //set today's date as due date by default
        this.finding.Due_Date_Investigation__c = responseRequired  ? Date.Today() : null; //set today's date as due date by default
        

        return this;
    }

    /**
    * @author Pradhanta Bhandari
    * @date  2014/4/28
    * @description this method sets the due date for various requirements .
    * @param responseDueDate true if response is required, which is almost always true
    * @param containmentDueDate true if contaiment is required
    * @param investigationDueDate true if investigation is required
    */
    public SQX_Test_Finding setRequiredDueDate(Date responseDueDate, Date containmentDueDate, Date investigationDueDate){
        
        this.finding.Due_Date_Response__c = responseDueDate;
        this.finding.Due_Date_Containment__c = containmentDueDate;
        this.finding.Due_Date_Investigation__c = investigationDueDate;

        return this;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description changes the status of the finding to given on
    * @param status the new status to be set to  
    */
    public SQX_Test_Finding setStatus(String status){
        this.finding.Status__c = status;

        return this;
    }

    /**
    * @author Sanjay Maharjan
    * @date 2018/8/8
    * @description changes the stage of the finding
    * @param stage the new stage to be set
    */
    public SQX_Test_Finding setStage(String stage){
        this.finding.Stage__c = stage;

        return this;
    }
    
    
    /**
    * @author Sagar Shrestha
    * @date 2014/03/07
    * @description adds closure comments
    * @param closure comment 
    */
    public SQX_Test_Finding setClosureComments(String closureComment){
        this.finding.Closure_Comments__c = closureComment;

        return this;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description save (update/insert) the finding to the data store 
    */
    public SQX_Test_Finding save(){
        upsert finding;
        
        synchronize();

        return this;
    }

    /**
    * @author Pradhanta Bhandari
    * @date 2014/2/18
    * @description synchronize with the data store to update the memory object
    */
    public SQX_Test_Finding synchronize(){
        this.finding = [SELECT ID, Status__c, Has_Approved_Investigation__c, SQX_Investigation_Approver__c,
        Has_Containment__c,Has_Corrective_Action__c,Has_Investigation__c,
        Has_Preventive_Action__c,Has_Response__c, Rating__c,Closure_Comments__c,
        Open_Date__c, Complete_Date__c, Closed_Date__c, Reopen_Date__c, Age__c
                        FROM SQX_Finding__c
                        WHERE Id = : this.finding.Id];

        return this;
    }


    
    

    
    
    /***** Creates a new mock finding object of given type for test *****/ 
    public static SQX_Finding__c prepareMockObject(MockObjectType objectType, Account account, Contact primaryInvestigator){
        SQX_Finding__c finding = null;
        
        
        if(objectType == MockObjectType.CAPAType){
            finding = new SQX_Finding__c();
            finding.RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Finding,SQX_Finding.RECORD_TYPE_CAPA);
            finding.SQX_Supplier_Account__c = account == null ? null : account.ID;
            finding.SQX_Investigator_Contact__c = primaryInvestigator == null ? null : primaryInvestigator.ID;
        }
        else if(objectType == MockObjectType.AuditType){
            finding = new SQX_Finding__c();
            finding.RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Finding,SQX_Finding.RECORD_TYPE_AUDIT);
            finding.SQX_Supplier_Account__c = account == null ? null : account.ID;
            finding.SQX_Investigator_Contact__c = primaryInvestigator == null ? null : primaryInvestigator.ID;
        }
        else if(objectType == MockObjectType.NCType){
            finding = new SQX_Finding__c();
            finding.Title__c = 'Finding Title';
            finding.RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Finding,SQX_Finding.RECORD_TYPE_NON_CONFORMANCE);
        }
        else if(objectType == MockObjectType.ComplaintType){
            finding = new SQX_Finding__c();
            finding.RecordTypeId = SQX_Utilities.getRecordTypeIDFor(SQX.Finding,SQX_Finding.RECORD_TYPE_COMPLAINT );
        }
        
        return finding;
    }
    

    /***************Unit test *********************/
    final static String ADMIN_USER = 'Test_Finding_Admin_User',
                        STD_USER = 'Test_Finding_Std_User',
                        STD_USER_2 = 'Test_Finding_Std_User_2';
    
    @testSetup
    public static void commonSetup() {
        // add required users
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, null, ADMIN_USER);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, STD_USER);
        SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, null, STD_USER_2);
    }
    
    /**
    * ensures finding ownership is transferref to the provided assignee when the finding is initiated
    */
    public static testmethod void givenFindingIsInitiated_OwnershipTransferredToAssignee() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        
        System.runAs(allUsers.get(STD_USER)) {
            List<SQX_Finding__c> findings = new List<SQX_Finding__c>();
            // add draft finding with assignee other than creator
            SQX_Finding__c finding = new SQX_Test_Finding().finding; //finding with no policy required
            finding.SQX_Assignee__c = allUsers.get(STD_USER_2).Id;
            finding.Status__c = SQX_Finding.STATUS_DRAFT;
            insert finding;
            
            // ACT: initiate finding
            finding.Status__c = SQX_Finding.STATUS_OPEN;
            update finding;
            
            SQX_Finding__c initiatedFinding = [ SELECT Id, SQX_Assignee__c, OwnerId, Status__c FROM SQX_Finding__c WHERE Id = :finding.Id ];
            
            System.assertEquals(finding.SQX_Assignee__c, initiatedFinding.OwnerId,
                'Expected owner of the finding with assignee to be changed to assignee when initiated. ' + initiatedFinding);
        }
    }
    
    /**
    * Given : incomplete audit with finding
    * When  : when draft finding is initiated
    * Then  : validation error occurs
    */ 
    static testmethod void givenIncompleteAuditWithFinding_WhenFindingIsInitiated_ErrorOccurs(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get(ADMIN_USER);
        
        System.runAs(adminUser){
            // create audit in progress i.e. incomplete audit
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c = adminUser.Id;
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.audit.Stage__c = SQX_Audit.STAGE_IN_PROGRESS;
            audit.audit.Status__c = SQX_Audit.STATUS_OPEN;
            audit.save();
            
            // add draft finding
            SQX_Test_Finding auditFinding1 = audit.addAuditFinding()
                .setRequired(true, false, false, false, false) //response, containment, and investigation required
                .save();
            
            // ACT: initiate finding of incomplete audit
            auditFinding1.finding.Status__c = SQX_Finding.STATUS_OPEN;
            Database.SaveResult sr = Database.update(auditFinding1.finding, false);
            
            String expectedErr = 'Finding cannot be processed when audit is not complete.';
            System.assertEquals(false, sr.isSuccess(), 'Expected finding of an incomplete audit to show error when initiated.');
            System.assert(SQX_Utilities.checkErrorMessage(sr.getErrors(), expectedErr), 'Expected error message "' + expectedErr + '" not found. Errors: ' + sr.getErrors());
        }
    }
    
    public testmethod static void givenAFindingIsPublished_ItIsShared(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_Finding findingContainer = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
            .setRequired(true, true, true, true, true)//response, containment, investigation, ca, pa
            .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
            .setStatus(SQX_Finding.STATUS_DRAFT)
            .save();
    
            //set to open
            findingContainer.setStatus(SQX_Finding.STATUS_OPEN).save();
        }
    }

    public testmethod static void givenAudit_FindingsContactAndAccountIsSet(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            Account supplier = SQX_Test_Account_Factory.createAccount();
            Contact primaryContact = SQX_Test_Account_Factory.createContact(supplier);
            User primaryContactUser = SQX_Test_Account_Factory.createUser(primaryContact, SQX_Test_Account_Factory.PROFILE_TYPE_PARTNER);
            User adminUser =  SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);
            
            integer randomNumber = (integer)(Math.random() * 1000000);
            SQX_Department__c department = new SQX_Department__c();
            SQX_Division__c division = new SQX_Division__c();

            System.runas(adminUser){

            department.Name = 'Department' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Department__c> {department}, new List<Schema.SObjectField>{
                    Schema.SQX_Department__c.Name
                } ) ;

            division.Name = 'Division' + randomNumber;

            new SQX_DB().op_insert(new List<SQX_Division__c> {division}, new List<Schema.SObjectField>{
                    Schema.SQX_Division__c.Name
                } ) ;
            }    
            SQX_Audit__c audit = new SQX_Audit__c(
                                        Account__c = supplier.Id , 
                                        Title__c = 'Random Title',
                                        Primary_Contact_Name__c = primaryContact.Id, 
                                        Lead_Auditor__c =  'Test ', 
                                        Audited_On__c = Date.Today(),
                                        Audit_Type__c = 'Internal',
                                        Audit_Category__c = 'ISO',
                                        Start_Date__c = Date.Today(),
                                        SQX_Auditee_Contact__c = adminUser.Id,
                                        SQX_Division__c = division.Id,
                                        Org_Division__c = 'Sample Division',
                                        SQX_Department__c = department.Id);
    
            insert audit;
    
            SQX_Test_Finding finding = new SQX_Test_Finding(MockObjectType.AuditType, supplier, primaryContact, primaryContactUser)
                                       .setRequired(false, false, false, false, false);
    
            finding.finding.SQX_Audit__c = audit.Id;
    
            finding.save();
        }
    }

    public testmethod static void givenAFindingMeetsRequirement_ItIsCompleted(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        
        System.runas(newUser){
            SQX_Test_Finding findingContainer = new SQX_Test_Finding(SQX_Test_Finding.MockObjectType.CAPAType)
            .setRequired(false, false, false, false, false)//response, containment, investigation, ca, pa
            .setApprovals(true, true, true) //inv. approval, changes in ap approval, eff review
            .setStatus(SQX_Finding.STATUS_DRAFT)
            .save();
    
            //set to open
            findingContainer.setStatus(SQX_Finding.STATUS_OPEN).save();
    
            System.assert(findingContainer.finding.Status__c == SQX_Finding.STATUS_COMPLETE, 
                'Expected finding to complete because all requirement are met');
        }

    }

    /**
    * Given : audit with Finding  
    * When  : when audit is saving
    * Then  : check investigation requirement validation rule
    * @Author : Bhuvan Joshi
    * @Date   : 04/10/2017
    * @Story  : [SQX-1679]
    */ 
    static testmethod void givenAuditWithFinding_WhenAuditSaving_ThenInvestigationCheckValidationRule(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User adminUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role);
        System.runAs(adminUser){
            //Arrange : Create Audit with investigation approval required
            SQX_Test_Audit audit = new SQX_Test_Audit(adminUser);
            audit.audit.SQX_Auditee_Contact__c= adminUser.Id;
            audit.audit.Start_Date__c = Date.Today();
            audit.audit.End_Date__c = Date.Today().addDays(10);
            audit.save();
            try{
                //Act 1: Save audit finding with investigation off and CA/PA on
                SQX_Test_Finding auditFinding1 =  audit.addAuditFinding()
                    .setRequired(false, false, false, true, true) //response, containment, and investigation required
                    .save(); 
            }catch(Exception ex){
                //Assert 1: Ensured that validation error
                system.assertEquals(true,ex.getMessage().Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            }
            
            //Act 2: Save audit finding with investigation on and CA/PA off
            SQX_Test_Finding auditFinding2 =  audit.addAuditFinding()
                .setRequired(false, false, true, false, false) //response, containment, and investigation required
                .setRequiredDueDate(null,null,Date.today())
                .save();
            
            //Assert 3: Ensured that audit finding saved when investigation required on
            system.assertNotEquals(null,auditFinding2.finding.id);
            
            //Act 3: Save audit finding with investigation on and CA/PA on
            SQX_Test_Finding auditFinding3 =  audit.addAuditFinding()
                .setRequired(false, false, true, true, true) //response, containment, and investigation required
                .setRequiredDueDate(null,null,Date.today())
                .save();
            
            //Assert 3: Ensured that audit finding saved when investigation required on
            system.assertNotEquals(null,auditFinding2.finding.id);

        }
    } 

    /**
    * Given : new finding
    * When  : when open finding is escalated to create CAPA
    * Then  : new CAPA is created with Finding reference
    * @Author : Sajal Joshi
    * @modified by : Anish Shrestha
    * @Date   : 07/06/2017
    * @Story  : [SQX-3541]
    * @modified story : [SQX-4204]
    */ 
    static testmethod void whenOpenFindingIsEscalated_NewCapaIsCreatedWithFindingReference(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        System.runas(newUser){
            // Arrange: create finding
            SQX_Test_Finding finding = new SQX_Test_Finding();
            finding.save();
            
            finding.setStatus(SQX_Finding.STATUS_OPEN);
            finding.finding.Create_New_CAPA__c = true;
            finding.save();

            /*
            * Act : Escalate Finding to CAPA  
            */

            List<SQX_Finding__c> findingList = [SELECT Id, 
                                                       Create_New_CAPA__c, 
                                                       SQX_CAPA__c
                                                FROM SQX_Finding__c WHERE Id =: finding.finding.Id];
            
            SQX_Finding.escalateToCAPA(findingList, 'Escalate CAPA');

            
            findingList = [SELECT SQX_CAPA__c FROM SQX_Finding__c WHERE Id =: finding.finding.Id];
            
            // Assert: New CAPA should be create with Finding link and field values are copied from finding
            list<SQX_CAPA__c> capaList = [SELECT Id, Status__c FROM SQX_CAPA__c WHERE SQX_Finding__c =: findingList];
            System.assert(!capaList.isEmpty(), 'CAPA list should not empty');
            
            // Assert: New CAPA reference is added to Finding
            System.assert(String.isNotBlank(findingList[0].SQX_CAPA__c), 'CAPA reference should be added to Finding');
            
            SQX_Finding_CAPA__c findingCAPA = [SELECT Id, Resolves_by_CAPA__c, SQX_Finding__c, SQX_Capa__c FROM SQX_Finding_CAPA__c WHERE SQX_Finding__c =: finding.finding.Id];
            System.assertEquals(true, findingCAPA.Resolves_by_CAPA__c);
            System.assertEquals(SQX_CAPA.STATUS_DRAFT, capaList[0].Status__c);
        }
    }
    
    
    /**
    * Given : a new finding in draft state
    * When  : when finding states are changed
    * Then  : finding's open, complete, reopen, closed dates are set
    * @Author : Sajal Joshi
    * @Date   : 10/12/2017
    * @Story  : [SQX-3958]
    */
    static testmethod void whenFindingStatesAreChanged_ThenFindingDatesAreSet(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        String closureComment = 'Closing finding';
        System.runas(newUser){
            // Arrange: create a new finding
            SQX_Test_Finding finding = new SQX_Test_Finding();
            finding.finding.Done_Responding__c = false;
            finding.save();
			
            // Act: Set the status to Open and save the finding    
            finding.setStatus(SQX_Finding.STATUS_OPEN);
            finding.save();
            
            // Assert: Open Date is set to the finding and age is calculated based on (TODAY() - Open_Date__c)
            System.assertEquals(Date.today(), finding.finding.Open_Date__c);
            System.assertEquals(finding.finding.Open_Date__c.daysBetween(Date.today()), finding.finding.Age__c);
            
            // Act: Set the status to Complete and save the finding
            finding.setStatus(SQX_Finding.STATUS_COMPLETE);
            finding.save();
            
            // Assert: Complete date is set to the finding
            System.assertEquals(Date.today(), finding.finding.Complete_Date__c);
            
            // Act: Reopen the finding
            finding.setStatus(SQX_Finding.STATUS_OPEN);
            finding.save();
            
            finding.finding.Open_Date__c = Date.today() - 5;
            finding.save();
            
            //Assert: Reopen date is set and complete date is removed
            System.assertEquals(Date.today(), finding.finding.Reopen_Date__c);
            System.assert(finding.finding.Complete_Date__c == null);
            
            // Act: Complete the finding
            finding.setStatus(SQX_Finding.STATUS_COMPLETE);
            finding.save();
            
            // Assert: Finding age is calculated based upon ( Complete_Date__c - Open_Date__c )
            System.assertEquals(finding.finding.Open_Date__c.daysBetween(finding.finding.Complete_Date__c), finding.finding.Age__c);
            
            //Act: Close the finding
            finding.finding.Closure_Comments__c = closureComment;
            finding.setStatus(SQX_Finding.STATUS_CLOSED);
            finding.save();
            
            //Assert: Closed date is set to the finding
            System.assertEquals(Date.today(), finding.finding.Closed_Date__c);
        }
    }
    
    /**
    * Given : a new finding
    * When  : future issue date is set
    * Then  : validation issue is thrown
    * @Author : Sajal Joshi
    * @Date   : 12/01/2017
    * @Story  : [SQX-4511]
    */
    static testmethod void whenIssueDateIsFuture_ThenValidationErrorIsThrown(){
        UserRole role = SQX_Test_Account_Factory.createRole(); 
        User newUser= SQX_Test_Account_Factory.createUser(null,SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role);
        SQX_DB db = new SQX_DB();
        String errMsg = 'Issue date can not be in future.';
        System.runas(newUser){
            // Arrange: create a new finding with future issue date
            SQX_Test_Finding finding = new SQX_Test_Finding();
            finding.finding.Issue_Date__c = Date.today() + 5;
            
            // Act: Insert the finding with future issue date
            List<Database.SaveResult> result = db.continueOnError().op_insert(new List<SQX_Finding__c>{finding.finding}, new List<Schema.SObjectField>{});
            
            //Assert: Insertion fails and validation error is thrown
            System.assertEquals(false, result[0].isSuccess(), 'Insert should fail');
            System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), errMsg), 'Actual Error: '+ result[0].getErrors());
            
            // Insert finding with valid fields
            finding.finding.Issue_Date__c = Date.today() - 5;
            finding.save();
            
            // Act: Update the finding with future issue date
            finding.finding.Issue_Date__c = Date.today() + 5;
            result = db.continueOnError().op_update(new List<SQX_Finding__c>{finding.finding}, new List<Schema.SObjectField>{});
            
            //Assert: Update fails and validation error is thrown
            System.assertEquals(false, result[0].isSuccess(), 'Update should fail');
            System.assertEquals(true, SQX_Utilities.checkErrorMessageContainingTexts(result[0].getErrors(), errMsg), 'Actual Error: '+ result[0].getErrors());
            
        }
    }
}