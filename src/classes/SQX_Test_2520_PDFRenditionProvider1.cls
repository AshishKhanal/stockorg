/**
* This test class checks easyPDF API
*/
@isTest
public class SQX_Test_2520_PDFRenditionProvider1 {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        System.runAs(adminUser){
            SQX_Test_Controlled_Document cDoc1 = new SQX_Test_Controlled_Document();
            cDoc1.updateContent(true, 'Content-1.txt');
            cDoc1.doc.Document_Number__c = 'CDOC_1_NUMBER';
            cDoc1.save();
        }
    }
    /*
    * return controlled documents information
    * @param documentNumber is controlled document number
    */
    static SQX_Controlled_Document__c getDoc(String documentNumber){
        return [SELECT Id, Content_Reference__c, Description__c, Title__c, 
                Synchronization_Status__c, Approval_Status__c, Document_Status__c, Secondary_Content_Reference__c, OwnerId
                FROM SQX_Controlled_Document__c
                WHERE Document_Number__c = : documentNumber];
    }
    final static Integer MODE_SUBMIT = 10,
        MODE_SUBMIT_CONTINUATION = 20,
        MODE_SUBMIT_ERROR = 30,
        MODE_POLL_ERROR = 40,
        MODE_POLL_CONTINUATION = 50,
        MODE_POLL_READY = 60,
        MODE_FETCH = 70,
        MODE_NOT_SUPPORTED = 80,
        MODE_FAILED = 90;
    final static String clientId='72ca1f2337e2455fb2643d0ae1a2e92a',
        client_secret='ED5A9539AF843A11E42254BA9D01E21F08EFE90CE0AD9CB21DB306794B384538',
        SALESFORCE_HOST = 'https://api.easypdfcloud.com/',
        CALLOUT='callout:Easy_Cloud_PDF',
        HTTP_GET = 'GET',
        HTTP_POST = 'POST',
        HTTP_PUT = 'PUT',
        HTTP_DELETE='DELETE',JOB_ID='00000000005E8B5C';
    final Static integer HTTP_ERROR_STATUS = 404, HTTP_CONTINUE_STATUS = 202, HTTP_OK_STATUS = 200, HTTP_CREATED_STATUS = 201;
    
    /*
    * When: submit the http request to easyPDF
    * Then: create a new job, return job information
    */ 
    testmethod static void WhenSubmitHttpRequest_ThenCreateNewJob() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document, rendition request and mock http callout
            SQX_Controlled_Document__c doc = getDoc('CDOC_1_NUMBER');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.Content = [SELECT Id, PathOnClient, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            SQX_RenditionProvider provider = new SQX_PDFRenditionProvider1();
            //MODE_SUBMIT
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_SUBMIT;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            
            //Act: submit the http request to easyPDF API
            SQX_RenditionResponse res=provider.submitRequest(request);
            
            //Assert: Ensure that jobid is returned and the status was processing 
            System.assertEquals(SQX_RenditionResponse.Status.Processing, res.status);
            System.assertEquals(JOB_ID, res.jobId);

            //MODE_SUBMIT_ERROR           
            MockHttpResponseGenerator mockHttpObj1= new MockHttpResponseGenerator();
            mockHttpObj1.Mode=MODE_SUBMIT_ERROR;
            Test.setMock(HttpCalloutMock.class, mockHttpObj1);
            //Act: submit the http request to easyPDF API
            SQX_RenditionResponse res1=provider.submitRequest(request);
            //Assert: Ensure that jobid returned null and the status was Error 
            System.assertEquals(SQX_RenditionResponse.Status.Error, res1.status); 
            System.assertEquals(null, res1.jobId);
            Test.stopTest();
        }
    }
    
    /*
    * When: submit to poll request
    * Then: get the status of the job
    */ 
    testmethod static void WhenSubmitToPoll_ThenGetStatus(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document, rendition request and mock http callout
            SQX_Controlled_Document__c doc = getDoc('CDOC_1_NUMBER');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.jobId=JOB_ID;
            request.Content = [SELECT Id, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            List<SQX_RenditionRequest> requests=new  List<SQX_RenditionRequest>();
            requests.add(request);
            SQX_RenditionProvider provider = new SQX_PDFRenditionProvider1();
            //MODE_POLL_READY
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_POLL_READY;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            //Act: submit the http request to poll
            Map<SQX_RenditionRequest, SQX_RenditionResponse> retMap=provider.poll(requests);
            for(SQX_RenditionRequest req : retMap.keySet()){
                SQX_RenditionResponse resp = retMap.get(req);
                //Assert: Ensure that jobid is returned and the status was RenditionReady 
                System.assertEquals(SQX_RenditionResponse.Status.RenditionReady, resp.status);
                system.assertNotEquals(null, resp.jobId);
            }
            //MODE_POLL_ERROR
            MockHttpResponseGenerator mockHttpObj1= new MockHttpResponseGenerator();
            mockHttpObj1.Mode=MODE_POLL_ERROR;
            Test.setMock(HttpCalloutMock.class, mockHttpObj1);
            //Act: submit the http request to poll
            Map<SQX_RenditionRequest, SQX_RenditionResponse> retMap1=provider.poll(requests);
            for(SQX_RenditionRequest req1 : retMap1.keySet()){
                SQX_RenditionResponse resp1 = retMap1.get(req1);
                //Assert: Ensure the status was Error 
                System.assertEquals(SQX_RenditionResponse.Status.Error, resp1.status);
            }
            Test.stopTest();
        }
    }
    
    /*
    * When: submit to fetch the data
    * Then: get the data
    */ 
    testmethod static void WhenSubmitToFetch_ThenGetData(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        
        System.runAs(adminUser){
            // Arrange: Create controlled document, rendition request and mock http callout
            SQX_Controlled_Document__c doc = getDoc('CDOC_1_NUMBER');
            
            SQX_RenditionRequest request = new SQX_RenditionRequest();
            request.Document = doc;
            request.jobId=JOB_ID;
            request.Content = [SELECT Id, FileExtension, ContentDocumentId, Controlled_Document__c 
                               FROM ContentVersion WHERE IsLatest =true AND ContentDocumentId =: 
                               doc.Content_Reference__c];
            SQX_RenditionProvider provider = new SQX_PDFRenditionProvider1();
            //MODE_FETCH
            Test.startTest();
            MockHttpResponseGenerator mockHttpObj= new MockHttpResponseGenerator();
            mockHttpObj.Mode=MODE_FETCH;
            Test.setMock(HttpCalloutMock.class, mockHttpObj);
            
            //Act: submit the retrive data
            SQX_RenditionResponse res=provider.retrieve(request);
            //Assert: Ensure the status was RenditionReady and content was returned
            System.assertNotEquals(null,res.result.VersionData);
            System.assertEquals(SQX_RenditionResponse.Status.RenditionReady, res.status);
            Test.stopTest();
        }
    }
    
    /**
    * Rendition Mock class mimicking the behaviour of easyPDF rendition provider.
    */
    public class MockHttpResponseGenerator implements HttpCalloutMock {
        public Integer Mode {get; set;}
        
        public HTTPResponse respond(HTTPRequest req) { 
            String url = req.getEndPoint();
            HttpResponse res = new HttpResponse();
            if(this.Mode == MODE_SUBMIT) {
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"access_token": "AFFZquARFkcZC","token_type": "bearer","expires_in": "3600","scope": "epc.api"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('workflows')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"jobID": "00000000005E8B5C","workflowID": "0000000005200C07"}');
                    res.setStatusCode(201);   
                }
                else if(url.contains('input')){
                    res.setStatusCode(200);
                }
                else if(url.contains('/v1/jobs/')){
                    res.setStatusCode(200);
                }else if(url.endsWith('/v1/jobs')){
                    res.setBody('{"jobID": "00000000005E8B5C","workflowID": "0000000000000000"}');
                    res.setStatusCode(201);
                }
            }
            else if(this.Mode == MODE_SUBMIT_ERROR){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"error": "invalid_client"}');
                    res.setStatusCode(400); 
                }
                else if(url.contains('workflows')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"error":"invalid_token"}');
                    res.setStatusCode(401);   
                }
                else if(url.contains('input')){
                    res.setStatusCode(404);
                }
                else if(url.contains('/v1/jobs')){
                    res.setStatusCode(404);
                }
            }
            else if(this.Mode == MODE_FETCH){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"access_token": "AFFZquARFkcZC","token_type": "bearer","expires_in": "3600","scope": "epc.api"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('output')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"status": "OK"}');
                    res.setStatusCode(200);
                }
            }
            else if(this.Mode == MODE_POLL_ERROR){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"error": "invalid_client"}');
                    res.setStatusCode(400); 
                }
                else if(url.contains('/v1/jobs/')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"message": "The job record does not exist"}');
                    res.setStatusCode(404);
                }
            }
            else if(this.Mode== MODE_POLL_READY){
                if(url.contains('token')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"access_token": "AFFZquARFkcZC","token_type": "bearer","expires_in": "3600","scope": "epc.api"}');
                    res.setStatusCode(200);
                }
                else if(url.contains('/v1/jobs/')){
                    res.setHeader('Content-Type', 'application/json');
                    res.setBody('{"jobID": "00000000005E8B5C","workflowID": "0000000005200C07","status": "completed","finished": true,"progress": 100}');
                    res.setStatusCode(200);
                } 
            }
            return res;
        }  
    }

}