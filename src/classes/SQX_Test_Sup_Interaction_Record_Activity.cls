/*
 * unit test for Supplier Interaction Record Activities
 */

@isTest
public class SQX_Test_Sup_Interaction_Record_Activity {
    public SQX_Supplier_Interaction__c si;
    
    static final String ACCOUNT_NAME = 'SQX_Test_Supplier_Interaction_Account';
    
    @testSetup
    public static void commonSetup(){
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser, adminUser});
        
        System.runAs(standardUser) {
            // create account record
            List<Account> accounts = new List<Account>();
            accounts.add(new Account(
                Name = ACCOUNT_NAME 
            ));
            insert accounts;
            
            //Create contact for Account
            List<Contact> contacts = new List<Contact>();
            contacts.add(new Contact(
                FirstName = 'Bruce',
                Lastname = 'Wayne',
                AccountId = accounts[0].Id,
                Email = System.now().millisecond() + 'test@test.com'
            ));
            insert contacts;
        }
    }
    
    /*
     * returns account record created from commonSetup()
     */
    private static List<Account> getAccountAndContactRecords(){
        return [SELECT Id, Name, (SELECT Id, Name FROM Contacts) FROM Account WHERE Name =: ACCOUNT_NAME];
    }
    
    /*
     * Given: Supplier Interaction record
     * When: Record is processed
     * Then: Capture record activity
     */
    public static testMethod void givenSupplierInteraction_WhenRecordProcessed_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Interaction record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Arrange: Save the record
            supInt.si.Activity_Code__c = null;
            supInt.si.Part_Name__c = 'Test Part';
            supInt.save(); 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act: Initiate the record
            supInt.initiate();

            //Act: Close the record
            supInt.close();
            
            //Assert: Capture submit record activity
            List<SQX_Supplier_Interaction_Record_Activity__c> intRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Interaction_Record_Activity__c WHERE SQX_Supplier_Interaction__c =: supInt.si.Id Order By CreatedDate Asc];
            System.assertEquals(4, intRecordActivity.Size(), 'Supplier Interaction record activity must be captured.' + intRecordActivity);
            System.assertEquals(Label.SQX_PS_INTERACTION_SUBMITTING_THE_RECORD, intRecordActivity.get(0).Activity__c, 'Expected activity to be properly saved when supplier escalation is submitted.');
            System.assertEquals(Label.SQX_PS_INTERACTION_INITIATING_THE_RECORD, intRecordActivity.get(1).Activity__c, 'Expected activity to be properly saved when supplier escalation is initiated.');
            System.assertEquals(Label.SQX_PS_INTERACTION_COMPLETING_THE_RECORD, intRecordActivity.get(2).Activity__c, 'Expected activity to be properly saved when supplier escalation is completed.');
            System.assertEquals(Label.SQX_PS_INTERACTION_CLOSING_THE_RECORD, intRecordActivity.get(3).Activity__c, 'Expected activity to be properly saved when supplier escalation is closed.');
        }
    }
    
    /*
     * Given: Supplier Interaction record
     * When: Record is restarted
     * Then: Capture record activity
     */
    public static testMethod void givenSupplierInteraction_WhenRecordRestarted_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Interaction record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Arrange: Save the record
            supInt.si.Activity_Code__c = null;
            supInt.si.Part_Name__c = 'Test Part';
            supInt.save(); 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act: Initiate the record
            supInt.initiate();

            //Act: Close the record
            supInt.restart();
            
            //Assert: Capture submit record activity
            List<SQX_Supplier_Interaction_Record_Activity__c> intRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Interaction_Record_Activity__c WHERE SQX_Supplier_Interaction__c =: supInt.si.Id Order By CreatedDate Asc];
            System.assertEquals(5, intRecordActivity.Size(), 'Supplier Interaction record activity must be captured.' + intRecordActivity);
            System.assertEquals(Label.SQX_PS_INTERACTION_RESTARTING_THE_RECORD, intRecordActivity.get(3).Activity__c, 'Expected activity to be properly saved when supplier escalation is closed.');
            System.assertEquals(Label.SQX_PS_INTERACTION_COMPLETING_THE_RECORD, intRecordActivity.get(4).Activity__c, 'Expected activity to be properly saved when supplier escalation is completed.');
        }
    }
    
    /*
     * Given: Supplier Interaction record
     * When: Record is voided
     * Then: Capture record activity
     */
    public static testMethod void givenSupplierInteraction_WhenRecordVoided_CaptureRecordActivity(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        
        System.runAs(standardUser){
            //Arrange: get account record
            List<Account> accounts = getAccountAndContactRecords();
            
            //Arrange: create Supplier Interaction record
            SQX_Test_Supplier_Interaction supInt = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            
            //Act: Submit the record
            supInt.submit();
            
            //Arrange: Save the record
            supInt.si.Activity_Code__c = null;
            supInt.si.Part_Name__c = 'Test Part';
            supInt.save(); 
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            //Act: Initiate the record
            supInt.void();

            //Assert: Capture submit record activity
            List<SQX_Supplier_Interaction_Record_Activity__c> intRecordActivity = [SELECT Id, Activity__c FROM SQX_Supplier_Interaction_Record_Activity__c WHERE SQX_Supplier_Interaction__c =: supInt.si.Id Order By CreatedDate Asc];
            System.assertEquals(2, intRecordActivity.Size(), 'Supplier Interaction record activity must be captured.' + intRecordActivity);
            System.assertEquals(Label.SQX_PS_INTERACTION_VOIDING_THE_RECORD, intRecordActivity.get(1).Activity__c, 'Expected activity to be properly saved when supplier escalation is closed.');
        }
    }
}