/**
* Batch processor to delete duplicate related capas from NC.
* Added in version  : 6.2.0
* Used for versions : less than 6.2
*/

global with sharing class SQX_6_2_MigrateRelatedCapasOfNC implements Database.Batchable<SObject>{

    global SQX_6_2_MigrateRelatedCapasOfNC(){}

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator([SELECT ID, SQX_Nonconformance__c, SQX_CAPA__c from SQX_NC_CAPA__c]);
    }

    global void execute(Database.BatchableContext bc, List<SObject> objectList){

        List<SQX_NC_CAPA__c> objectToUpdateList = (List<SQX_NC_CAPA__c>)objectList;

        //this list will hold the duplicate SQX_NC_CAPA__c
        List<SQX_NC_CAPA__c> duplicateNcCapaList = new List<SQX_NC_CAPA__c>();

        /**
        * WITHOUT SHARING has been used 
        * ---------------------------------
        * Without sharing has been used because the running user might not have access  to Update the objectList
        * This will cause and error.
        */
        List<Database.SaveResult> results = new SQX_DB().continueOnError().withoutSharing().op_update(objectToUpdateList, new List<Schema.SObjectField>{
            SQX_NC_CAPA__c.fields.SQX_Nonconformance__c,
            SQX_NC_CAPA__c.fields.SQX_CAPA__c
        });

        for (Integer i = 0; i< objectToUpdateList.size(); i++) {

            //since getId() only returns Id for success so we can't get the id of the unsuccess record
            Database.SaveResult sr = results[i];
            SQX_NC_CAPA__c originalNcCapa = objectToUpdateList[i];

            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()){
                    //make sure that the error is due to uniqueness constraint
                    //if error is from something else then we don't do anything
                    if(err.getStatusCode() == StatusCode.DUPLICATE_VALUE){
                        duplicateNcCapaList.add(originalNcCapa);
                    }
                }
            }
        }

        if(duplicateNcCapaList.size() > 0){
            /**
            * WITHOUT SHARING has been used 
            * ---------------------------------
            * Without sharing has been used because the running user might not have access to delete all records that are duplicate
            * This will cause and error.
            */            
            new SQX_DB().withoutSharing().op_delete(duplicateNcCapaList);
        }
    }

    global void finish(Database.BatchableContext bc){

    }

}