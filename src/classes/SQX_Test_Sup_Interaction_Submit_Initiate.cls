/**
 * This class tests the submit and initiate functionality of Supplier Interaction record.
 */
@isTest
public class SQX_Test_Sup_Interaction_Submit_Initiate {
    @testSetup
    public static void commonSetup() {
        
        //Add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        
        System.runAs(adminUser) {
            // create auto number record for supplier interaction
            insert new SQX_Auto_Number__c(
                Name = 'Supplier Interaction',
                Next_Number__c = 1,
                Number_Format__c = 'INT-{1}',
                Numeric_Format__c = '0000'
            );
        }
        System.runAs(standardUser) {
            // Create accounts
            SQX_Test_Supplier_Interaction.createAccounts();
        }
    }
    
    /**
     * Given : Supplier Interaction Record with two policy tasks of type Document Request and Task
     * When : Record is submitted 
     * Then : Two Supplier Interaction Step record with recor type Document Request and Task respectively are generated.
     * 
     * When : Record is initiated
     * Then : Record is open and Task for first step is generated
     */
    public static testMethod void ensureSubmitOfInteractionCreatesInteractionStepsAndInitiateOfInteractionCreatesTaskForOpenSteps() {
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User standardUser = allUsers.get('standardUser');
        SQX_Test_Supplier_Interaction.addUserToQueue(new List<User> {standardUser});
        System.runAs(standardUser) {
            
            // Arrange : Create current doc
            SQX_Test_Controlled_Document doc = new SQX_Test_Controlled_Document();
            doc.doc.Document_Status__c = SQX_Controlled_Document.STATUS_CURRENT;
            doc.save(true);
            
            // get account record
            List<Account> accounts = SQX_Test_Supplier_Interaction.getAccountAndContactRecords();
            
            // create two pollicy task so that it will create 2 steps later on submitting escalation record
            List<SQX_Task__c> policyTasks = SQX_Test_Supplier_Interaction.createPolicyTasks(1, SQX_Task.TASK_TYPE_DOC_REQUEST, standardUser, 1);
            SQX_Test_Supplier_Interaction.createPolicyTasks(1, SQX_Task.TASK_TYPE_TASK, standardUser, 2);
            policyTasks[0].SQX_Controlled_Document__c = doc.doc.Id;
            update policyTasks;
            
            // Arrange : create Supplier Interaction record
            SQX_Test_Supplier_Interaction interaction = new SQX_Test_Supplier_Interaction(accounts[0]).save();
            
            // Act : Submit record
            interaction.submit();
            
            SQX_Supplier_Interaction__c si = [SELECT Id, Record_Stage__c FROM SQX_Supplier_Interaction__c WHERE Id = :interaction.si.Id];
            System.assert(si.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_TRIAGE, 'Expected interaction to be in ' + SQX_Supplier_Common_Values.STAGE_TRIAGE + ' after submit but is in ' + si.Record_Stage__c);
            
            List<SQX_Supplier_Interaction_Step__c> steps = [SELECT Id, Status__c, SQX_Controlled_Document__c, RecordType.DeveloperName, Allowed_Days__c FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c = :interaction.si.Id];
            System.assert(steps.size() == 2, 'Expected two steps to be created while submitting interaction as there are two policy tasks.');
            System.assertEquals(policyTasks.get(0).Allowed_Days__c, steps.get(0).Allowed_Days__c, 'Expected Allowed Days to be same but is different');
            Map<String, SQX_Supplier_Interaction_Step__c> recordTypeToStep = new Map<String, SQX_Supplier_Interaction_Step__c>();
            for (SQX_Supplier_Interaction_Step__c step : steps) {
                recordTypeToStep.put(step.RecordType.DeveloperName, step);
            }
            System.assert(recordTypeToStep.get(SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST) != null && recordTypeToStep.get(SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST).SQX_Controlled_Document__c == doc.doc.Id, 'Expected Document Request type step should contain Controlled Document but is ' + recordTypeToStep.get(SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST).SQX_Controlled_Document__c);
            System.assert(recordTypeToStep.get(SQX_Supplier_Common_Values.RT_TYPE_TASK) != null, 'Expected Task type step to be created but is null');
            
            // Act : initiate
            SQX_BulkifiedBase.clearAllProcessedEntities();
            interaction.initiate();
            
            // Assert : Ensure interaction step with step 1 is open
            steps = [SELECT Id, Status__c, SQX_Controlled_Document__c, RecordType.DeveloperName FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c = :interaction.si.Id AND Status__c =: SQX_Supplier_Common_Values.STATUS_OPEN AND Step__c = 1];
            System.assert(steps.size() == 1, 'Expected first step to be opened');
            System.assert(steps[0].RecordType.DeveloperName == SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST, 'Expected record type of step to be ' + SQX_Supplier_Common_Values.RT_TYPE_DOCUMENT_REQUEST + ' but is ' + steps[0].RecordType.DeveloperName);
            
            // Assert : Ensure the task is created for step 1
            List<Task> tsks = [SELECT Id, Status, Child_What_Id__c FROM Task WHERE WhatId = :interaction.si.Id AND Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assert(tsks.size() == 1, 'Expected only one step to be created but got ' + tsks.size());
            System.assert(tsks[0].Child_What_Id__c == steps[0].Id, 'Expected task to be created for step 2 as step 1 is completed');
            
            // query the created task and complete with result Go
            String description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(tsks[0].Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_DOCUMENT_REQUEST);
            
            // clear all processed records
            SQX_BulkifiedBase.clearAllProcessedEntities();
            
            // Assert : new step is opened
            steps = [SELECT Id, Status__c, SQX_Controlled_Document__c, RecordType.DeveloperName FROM SQX_Supplier_Interaction_Step__c WHERE SQX_Parent__c = :interaction.si.Id AND Status__c =: SQX_Supplier_Common_Values.STATUS_OPEN AND Step__c = 2];
            System.assert(steps.size() == 1, 'Expected first step to be opened');
            System.assert(steps[0].RecordType.DeveloperName == SQX_Supplier_Common_Values.RT_TYPE_TASK, 'Expected record type of step to be '  + SQX_Supplier_Common_Values.RT_TYPE_TASK +  'but is ' + steps[0].RecordType.DeveloperName);
            
            // Assert : Ensure the task is created for step 2
            tsks = [SELECT Id, Status, Child_What_Id__c FROM Task WHERE WhatId = :interaction.si.Id AND Status = :SQX_Task.STATUS_NOT_STARTED];
            System.assert(tsks.size() == 1, 'Expected only one step to be created but got ' + tsks.size());
            System.assert(tsks[0].Child_What_Id__c == steps[0].Id, 'Expected task to be created for step 2 as step 1 is completed');
            
            // complete task with result Go
            description = 'Completing task with result as Go';
            SQX_Test_Utilities.completeSupplierStep(tsks[0].Id, SQX_Steps_Trigger_Handler.RESULT_GO, description, SQX_Steps_Trigger_Handler.RT_TASK);
            
            
            // Assert : Ensure interaction is complete since all task got completed
            si = [SELECT Id, Status__c, Record_Stage__c FROM SQX_Supplier_Interaction__c WHERE Id = :si.Id];
            System.assert(si.Status__c == SQX_Supplier_Common_Values.STATUS_COMPLETE, 'Expected status of Interaction to be ' + SQX_Supplier_Common_Values.STATUS_COMPLETE +' but got ' + si.Status__c);
            System.assert(si.Record_Stage__c == SQX_Supplier_Common_Values.STAGE_VERIFICATION, 'Expected stage of Interaction to be ' + SQX_Supplier_Common_Values.STAGE_VERIFICATION +' but got ' + si.Status__c);
        }
    }
}