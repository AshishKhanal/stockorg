/**
 * This migration test case for to update inactive cq task to active
 */
@isTest
public class SQX_Test_7136_Migration {
    @testSetup
    public static void commonSetup() {

        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, role, 'standardUser');
        System.runAs(adminUser){
            SQX_Test_NSI.createPolicyTasks(3, SQX_Task.TASK_TYPE_TASK, adminUser, 1);
        }
    }

    /**
    *Given: given inactive cq tasks
    *When: migrated
    *Then: active cq tasks
    */
    public static testmethod void givenInactiveCQTasks_WhenMigrate_ThenActiveCQTasks(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Create inactive CQ Tasks
            List<SQX_Task__c> uptTasks = new List<SQX_Task__c>();
            for(SQX_Task__c task :[SELECT Id, Active__c FROm SQX_Task__c]){
                SQX_Task__c taskRecord = new SQX_Task__c();
                taskRecord.Id = task.Id;
                taskRecord.Active__c = false;
                uptTasks.add(taskRecord);
            }
            update uptTasks;
            List<SQX_Task__c> taskList = [SELECT Id, Active__c FROm SQX_Task__c WHERE Active__c = false];
            system.assertEquals(3, taskList.size());

            //Act: execute migration
            Test.startTest();
            SQX_9_0_Active_CQ_Task_Migration job = new SQX_9_0_Active_CQ_Task_Migration();
            Id batchProcessId = Database.executeBatch(job);
            Test.stopTest();

            //Assert: Ensured that inactive CQ tasks should be activated
            List<SQX_Task__c> tasks = [SELECT Id, Active__c FROm SQX_Task__c WHERE Active__c = false];
            system.assertEquals(0, tasks.size());
        }
    }
}