/**
* unit test for auto numbering
*/
@isTest
public class SQX_Test_5600_Auto_Numbering {
    @testSetup
    public static void commonSetup() {
        // add required users
        UserRole role = SQX_Test_Account_Factory.createRole();
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, role, 'adminUser');
                System.runas(adminUser) {
            // add required auto numbers
            List<SQX_Auto_Number__c> autoNums = new List<SQX_Auto_Number__c>();
            autoNums.add(new SQX_Auto_Number__c(
                Name = 'Supplier Introduction',
                Next_Number__c = 0,
                Number_Format__c = 'NSI-{1}',
                Numeric_Format__c = '0'
            ));
            new SQX_DB().op_insert(autoNums,  new List<Schema.SObjectField>{});
        }
    }
    
    /**
    * GIVEN : given NSI record 
    * WHEN : save the record
    * THEN : update auto number in NSI number field
    */
    public static testMethod void givenNSI_WhenSaveTheRecord_ThenUpdateAutoNumber(){
        Map<String, User> allUsers = SQX_Test_Account_Factory.getUsers();
        User adminUser = allUsers.get('adminUser');
        System.runAs(adminUser){
            //Arrange: Create NSI record
            //Act: Save the record
            SQX_Test_NSI nsi= new SQX_Test_NSI().save();
            
            //Assert: Ensured that auto number is reflected
            SQX_New_Supplier_Introduction__c nsi1 = [SELECT Name FROM SQX_New_Supplier_Introduction__c WHERE Id =: nsi.nsi.Id];
            System.assertEquals('NSI-0', nsi1.Name);
            
        }
    }
}