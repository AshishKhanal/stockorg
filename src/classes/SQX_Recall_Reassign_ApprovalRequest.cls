/**
* This extension class to handle recall or reassign approval request.
*/
public with sharing class SQX_Recall_Reassign_ApprovalRequest {

    /**
     * Method to recall document created from change order.
     */
    @AuraEnabled
    public static SQX_Action_Response recallChangeOrderApproval(Id documentId) {
        try { 
            SQX_Controlled_Document__c doc = new SQX_Controlled_Document__c(Id = documentId);
            SQX_Extension_Controlled_Document docExtension = new SQX_Extension_Controlled_Document();
            
            SQX_Action_Response response = docExtension.initialize(doc);
            response.mergeResponse(docExtension.recallFromChangeApprovalAPI());
            return response;
            } catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
    }
    /**
     * This method is used to recall or reasign approval request
     * @param 'recordId' main recordId
     * @param 'comment' comment of approval request
     */ 
    @AuraEnabled
    public static void approvalReq(String recordId, String comment){
        try {
            Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
            pwr.setComments(comment);
            pwr.setAction('Removed');
            pwr.setWorkItemId(getWorkItemId(recordId));
            Approval.ProcessResult result = Approval.process(pwr);
        }catch(Exception ex) {
            SQX_Document_Preview_Controller.exceptionHandler(ex.getMessage());
        }
    }
    
    /**
	* Get ProcessInstanceWorkItemId using SOQL
	*/
    private static Id getWorkItemId(Id targetObjectId)
    {
        Id retVal = null;
        for(ProcessInstanceWorkitem workItem  : [SELECT Id FROM ProcessInstanceWorkitem 
                                                 WHERE ProcessInstance.TargetObjectId =: targetObjectId])
        {
            retVal  =  workItem.Id;
        }
        return retVal;
    }
}