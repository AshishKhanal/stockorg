@istest
public class SQX_Test_TriggerAfterUpdateSupPartActive{
    Static testmethod void testtrigger3()
    {
        Account acc = SQX_Test_Account_Factory.createAccount();
        
        SQX_Part_Family__c newPF1 = new SQX_Part_Family__c();
        newPF1.Name = 'dummyFamily';
        insert newPF1;
        
        SQX_Part__c myPart1 = new SQX_Part__c();
        mypart1.Name = 'newPart1';
        mypart1.Part_Number__c = 'prt111';
        mypart1.Part_Risk_Level__c = 3;
        mypart1.Active__c = TRUE;
        mypart1.Part_Family__c = newPF1.id;
        Insert myPart1;       
        
        SQX_Part__c myPart2 = new SQX_Part__c();
        mypart2.Name = 'newPart2';
        mypart2.Part_Number__c = 'prt222';
        mypart2.Part_Risk_Level__c = 2;
        mypart2.Active__c = TRUE;
        mypart2.Part_Family__c = newPF1.id;
        
        Insert myPart2;       
        
        SQX_Supplier_Part__c SP1 = new SQX_Supplier_Part__c();
        SP1.Account__c = Acc.id;
        SP1.Part__c = mypart1.id;
        SP1.Active__c = TRUE;
        SP1.Approved__c = TRUE;
        Insert SP1;
                
        SQX_Supplier_Part__c SP2 = new SQX_Supplier_Part__c();
        SP2.Account__c = acc.ID;
        SP2.Part__c = mypart2.id;
        SP2.Active__c = FALSE;
        SP2.Approved__c = TRUE;
        Insert SP2;
        
        Account Acc2 = [SELECT id, Supplier_Risk_Level__c 
                       FROM Account 
                       WHERE id =: acc.Id];       
        System.assert(Acc2.Supplier_Risk_Level__c == 3, 'Account Risk level not correctly updated');
        
        SP2.Active__c = TRUE;
        Update SP2;
        
        Acc2 = [SELECT id, Supplier_Risk_Level__c 
                        FROM Account 
                        WHERE id =: acc.Id];        
        System.assert(Acc2.Supplier_Risk_Level__c == 2, 'Account Risk level not correctly updated');
        
        SP2.Active__c = FALSE;
        Update SP2;
        
        Acc2 = [SELECT id, Supplier_Risk_Level__c 
                        FROM Account 
                        WHERE id =: acc.Id];         
        System.assert(Acc2.Supplier_Risk_Level__c == 3, 'Account Risk level not correctly updated');
        
          
        
    }
    
}