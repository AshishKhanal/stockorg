/* Test for NC Type */
@isTest
public class SQX_Test_1567_NC_Type{

    static boolean runAllTests = true,
                    run_givenAUser_NCTypeIsInternal_NCCanBeSaved = false,
                    run_givenAUser_NCTypeIsSupplier_NCCanBeSaved = false,
                    run_givenAUser_NCTypeIsCustomer_NCCanBeSaved = false;
    /*                
    * this test ensures when the NC Type is Interal
    * and if all the required fields are provided 
    * theb NC can be saved
    */               
    public testmethod static void givenAUser_NCTypeIsInternal_NCCanBeSaved(){

        if(!runAllTests && !run_givenAUser_NCTypeIsInternal_NCCanBeSaved){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){

            departmentResearch = new SQX_Department__c(
                                         Name='Research1'
                                        );
            insert departmentResearch;
        }

        System.runas(standardUser){

            SQX_Nonconformance__c nonConformance = new SQX_Nonconformance__c (
                                            Type__c = SQX_NC.TYPE_INTERNAL,
                                            Type_Of_Issue__c = SQX_NC.ISSUE_TYPE_PRODUCT,
                                            Disposition_Required__c = false,
                                            Occurrence_Date__c = Date.Today(),
                                            SQX_Department__c = departmentResearch.Id);
            
            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));
                                         
                                         
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
            
            Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance ));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            ncMap.put('Id', 'NC-1');
            ncMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
                
            changeSet.add(ncMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> paramssave = new Map<String, String>();
            paramsSave.put('nextAction', 'Save');
            Id result= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,paramsSave);
            
            System.assert(string.valueOf(result.getsObjectType()) == SQX.NonConformance, 
                    'Expected to process changeset'+ changeSet);

            List<SQX_Nonconformance__c> currentNC= [Select Id from SQX_Nonconformance__c WHERE Id= :result];
            System.assert(currentNC.size() == 1, 'Expected the NC to be saved');
        }
    }

    /*                
    * this test ensures when the NC Type is Customer
    * and if all the required fields are provided 
    * theb NC can be saved
    */               
    public testmethod static void givenAUser_NCTypeIsCustomer_NCCanBeSaved(){

        if(!runAllTests && !run_givenAUser_NCTypeIsCustomer_NCCanBeSaved){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER, mockRole);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN, mockRole);

        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){

            departmentResearch = new SQX_Department__c(
                                         Name='Research1'
                                        );
            insert departmentResearch;
        }

        System.runas(standardUser){

            SQX_Nonconformance__c nonConformance = new SQX_Nonconformance__c (
                                            Type__c = SQX_NC.TYPE_INTERNAL,
                                            Type_Of_Issue__c = SQX_NC.TYPE_CUSTOMER,
                                            Disposition_Required__c = false,
                                            Occurrence_Date__c = Date.Today(),
                                            SQX_Department__c = departmentResearch.Id);
            
            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));
                                         
                                         
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
            
            Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance ));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            ncMap.put('Id', 'NC-1');
            ncMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
                
            changeSet.add(ncMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> paramssave = new Map<String, String>();
            paramsSave.put('nextAction', 'Save');
            Id result= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,paramsSave);
            
            System.assert(string.valueOf(result.getsObjectType()) == SQX.NonConformance, 
                    'Expected to process changeset'+ changeSet);

            List<SQX_Nonconformance__c> currentNC= [Select Id from SQX_Nonconformance__c WHERE Id= :result];
            System.assert(currentNC.size() == 1, 'Expected the NC to be saved');
        }
    }

    /*                
    * this test ensures when the NC Type is Supplier
    * and if all the required fields are provided 
    * theb NC can be saved
    */               
    public testmethod static void givenAUser_NCTypeIsSupplier_NCCanBeSaved(){

        if(!runAllTests && !run_givenAUser_NCTypeIsSupplier_NCCanBeSaved){
            return;
        }


        UserRole mockRole = SQX_Test_Account_Factory.createRole();  
        User standardUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER);
        User adminUser = SQX_Test_Account_Factory.createUser(null, SQX_Test_Account_Factory.PROFILE_TYPE_CUSTOMER_ADMIN);

        Account supplier = null;
        Contact supplierContact = null;
        SQX_Department__c departmentResearch = new SQX_Department__c();
        
        System.runas(adminUser){
            supplier = SQX_Test_Account_Factory.createAccount();
            supplierContact = SQX_Test_Account_Factory.createContact(supplier);
            departmentResearch = new SQX_Department__c(
                                         Name='Research1'
                                        );
            insert departmentResearch;
        }

        System.runas(standardUser){

            SQX_Nonconformance__c nonConformance = new SQX_Nonconformance__c (
                                            Type__c = SQX_NC.TYPE_SUPPLIER,
                                            Type_Of_Issue__c = SQX_NC.TYPE_CUSTOMER,
                                            Disposition_Required__c = false,
                                            Occurrence_Date__c = Date.Today(),
                                            SQX_Department__c = departmentResearch.Id,
                                            SQX_Account__c = supplier.Id,
                                            SQX_External_Contact__c = supplierContact.Id);
            
            SQX_Finding__c finding = new SQX_Finding__c(
                                     Title__c = 'Random',
                                     Containment_Required__c = true,
                                     Investigation_Required__c = false,
                                     Corrective_Action_Required__c = false,
                                     Preventive_Action_Required__c = false,
                                     Due_Date_Containment__c = Date.Today().addDays(15),
                                     Response_Required__c = false,
                                     Severity__c = '2 - Low',
                                     Probability__c = '2 - Remote',
                                     Finding_Type__c = 'Major',
                                     RecordTypeId =  SQX_Utilities.getRecordTypeIDFor(SQX.Finding, SQX_Finding.RECORD_TYPE_CAPA));
                                         
                                         
            Map<String, Object> submissionSet = new Map<String, Object>();
            List<Map<String, Object>> changeSet = new List<Map<String, Object>>();
            
            Map<String, Object> ncMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nonConformance ));
            Map<String, Object> findingMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(finding));

            ncMap.put('Id', 'NC-1');
            ncMap.put(SQX.getNSNameFor('SQX_Finding__c'), 'FINDING-1');
            findingMap.put('Id', 'FINDING-1');
            
                
            changeSet.add(ncMap);
            changeSet.add(findingMap);

            submissionSet.put('changeSet', changeSet);

            String stringChangeSet= JSON.serialize(submissionSet);       
            Map<String, String> paramssave = new Map<String, String>();
            paramsSave.put('nextAction', 'Save');
            Id result= SQX_Extension_NC.processChangeSetWithAction('', stringChangeSet, null,paramsSave);
            
            System.assert(string.valueOf(result.getsObjectType()) == SQX.NonConformance, 
                    'Expected to process changeset'+ changeSet);

            List<SQX_Nonconformance__c> currentNC= [Select Id from SQX_Nonconformance__c WHERE Id= :result];
            System.assert(currentNC.size() == 1, 'Expected the NC to be saved');
        }
    }
}